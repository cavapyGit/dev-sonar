package com.pradera.core.component.accounts.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.helperui.to.IssuerTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.EconomicActivityType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountMovement;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.SecuritiesManager;

// TODO: Auto-generated Javadoc
/**
 * The Class ParticipantServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 24/09/2015
 */
@Stateless
public class ParticipantServiceBean extends CrudDaoServiceBean{
	
	/** The depositary setup. */
	@Inject @DepositarySetup IdepositarySetup depositarySetup;
	

	/**
	 * Gets the lis participant service bean.
	 *
	 * @return the lis participant service bean
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Participant> getLisParticipantServiceBean() throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		
		sbQuery.append(" Select Distinct P.idParticipantPk," );//0
		sbQuery.append(" P.description," ); 	//1
		sbQuery.append(" P.mnemonic,");		//2
		sbQuery.append(" P.state  ");	//3
		sbQuery.append(" From Participant P  ");
		sbQuery.append("Inner Join P.holderAccountBalances hab	");
		sbQuery.append(" Where (hab.totalBalance > 0 Or hab.reportedBalance > 0)	");
		sbQuery.append("ORDER BY P.mnemonic");
		
		List<Participant> participantList = new ArrayList<Participant>();
		
		List<Object[]> dataList = findListByQueryString(sbQuery.toString(), parameters);
		for(Object[] data: dataList){
			Participant p = new Participant();
			p.setIdParticipantPk((Long) data[0]);
			p.setDescription((String) data[1]);
			p.setMnemonic((String) data[2]);
			p.setState((Integer) data[3]);
			participantList.add(p);
		}
		
		return participantList; 
	}
	
	/**
	 * Gets the lis participant service bean.
	 *
	 * @param filter the filter
	 * @return the lis participant service bean
	 * @throws ServiceException the service exception
	 */

	@SuppressWarnings("unchecked")
	public List<Issuer> getLisIssuerServiceBean(Issuer filter) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		
		sbQuery.append(" select i From Issuer i  ");
		sbQuery.append(" Where 1 = 1 ");
		if(Validations.validateIsNotNullAndNotEmpty(filter.getStateIssuer())){
			sbQuery.append(" and i.stateIssuer = :stateIssuer ");
			parameters.put("stateIssuer", filter.getStateIssuer());
		}
		sbQuery.append("ORDER BY i.mnemonic");
		
		List<Issuer> lstIssuer = findListByQueryString(sbQuery.toString(), parameters);
		return lstIssuer;
	}
	
	@SuppressWarnings("unchecked")
	public List<Participant> getLisParticipantServiceBean(Participant filter) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		
		sbQuery.append(" Select new Participant (P.idParticipantPk," );//0
		sbQuery.append(" P.description," ); 	//1
		sbQuery.append(" P.mnemonic,");		//2
		sbQuery.append(" P.state ) ");	//3
		sbQuery.append(" From Participant P  ");
		

		if(filter.getHolderAccountBalances()!=null){
			sbQuery.append("Inner Join P.holderAccountBalances hab	");
		}
		
		sbQuery.append(" Where 1 = 1 ");

		//Si el participante tiene ID, se necesita consultar solo este participante
		if(filter.getIdParticipantPk()!=null){
			sbQuery.append(" and P.idParticipantPk = :idParticipantParam ");
			parameters.put("idParticipantParam", filter.getIdParticipantPk());
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getState())){
			sbQuery.append(" and P.state = :statePrm ");
			parameters.put("statePrm", filter.getState());
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(filter.getLstParticipantStates())){
			sbQuery.append(" and P.state in (:lstStatePrm) ");
			parameters.put("lstStatePrm", filter.getLstParticipantStates());
		}
		
		if(filter.getHolderAccountBalances()!=null){
			sbQuery.append(" and (hab.totalBalance > 0 Or hab.reportedBalance > 0)	");
		}
		
		if(filter.getIndDepositary()!=null) {
			if(filter.getIndDepositary().equals(BooleanType.YES.getCode())) {
				sbQuery.append(" and P.indDepositary = :indDepositaryPrm ");
				parameters.put("indDepositaryPrm", filter.getIndDepositary());
			}else {
				sbQuery.append(" and P.indDepositary is null OR P.indDepositary = :indDepositaryPrm ");
				parameters.put("indDepositaryPrm", filter.getIndDepositary());
			}
		}
		
		sbQuery.append("ORDER BY P.mnemonic");
		
		List<Participant> lstParticipant = findListByQueryString(sbQuery.toString(), parameters);
		return lstParticipant;
	}
	
	/**
	 * Gets the lis participant service bean.
	 *
	 * @param filter the filter
	 * @return the lis participant service bean
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Participant> getLisParticipantForIssuerServiceFacade(Participant filter) throws ServiceException{
		List<Participant> lstParticipant = new ArrayList<Participant>();
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();		
		sbQuery.append(" Select DISTINCT P.idParticipantPk," );//0
		sbQuery.append(" P.description," ); 	//1
		sbQuery.append(" P.mnemonic,");		//2
		sbQuery.append(" P.state  ");	//3
		sbQuery.append(" From Participant P ,HolderAccountBalance hab,Security se, ");
		sbQuery.append(" Issuance iss ,Issuer isss ");
		sbQuery.append(" Where 1 = 1 ");
		sbQuery.append(" and P.idParticipantPk = hab.participant.idParticipantPk ");
		sbQuery.append(" and hab.security.idSecurityCodePk = se.idSecurityCodePk ");
		sbQuery.append(" and se.issuance.idIssuanceCodePk = iss.idIssuanceCodePk ");
		sbQuery.append(" and iss.issuer.idIssuerPk = isss.idIssuerPk ");
		
		if(filter.getIssuer()!=null && filter.getIssuer().getIdIssuerPk()!=null){
			sbQuery.append(" and isss.idIssuerPk = :idIssuerPk ");
			parameters.put("idIssuerPk", filter.getIssuer().getIdIssuerPk());
		}		

		//Si el participante tiene ID, se necesita consultar solo este participante
		if(filter.getIdParticipantPk()!=null){
			sbQuery.append(" and P.idParticipantPk = :idParticipantParam ");
			parameters.put("idParticipantParam", filter.getIdParticipantPk());
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getState())){
			sbQuery.append(" and P.state = :statePrm ");
			parameters.put("statePrm", filter.getState());
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(filter.getLstParticipantStates())){
			sbQuery.append(" and P.state in (:lstStatePrm) ");
			parameters.put("lstStatePrm", filter.getLstParticipantStates());
		}
		
		sbQuery.append("ORDER BY P.mnemonic");
		
		List<Object[]> lstObject = findListByQueryString(sbQuery.toString(), parameters);
		if(lstObject!=null && lstObject.size()>0){
			Participant objParticipant = null;
			for(int i = 0; i < lstObject.size(); i++){
				objParticipant = new Participant();
				Object obj[] = lstObject.get(i);
				if(obj[0]!=null){
					objParticipant.setIdParticipantPk((Long)obj[0]);
				}
				if(obj[1]!=null){
					objParticipant.setDescription((String)obj[1]);
				}
				if(obj[2]!=null){
					objParticipant.setMnemonic((String)obj[2]);			
				}
				if(obj[3]!=null){
					objParticipant.setState((Integer)obj[3]);
				}
				lstParticipant.add(objParticipant);
			}
		}
		return lstParticipant;
	}
	
	/**
	 * Gets the lis international depository service bean.
	 *
	 * @param filter the filter
	 * @return the lis international depository service bean
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<InternationalDepository> getLisInternationalDepositoryServiceBean(InternationalDepository filter) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		//sbQuery.append(" Select P "+
		
		sbQuery.append(" Select P.idInternationalDepositoryPk," +//0
							   "P.description," +//1
							   "P.stateIntDepositary " +//2
							   
					   " From InternationalDepository P ");
		sbQuery.append(" Where 1 = 1 ");

		//Si el participante tiene ID, se necesita consultar solo este participante
		if( Validations.validateIsNotNullAndPositive(filter.getIdInternationalDepositoryPk()) )
			sbQuery.append(" and P.idInternationalDepositoryPk = :idInternationalDepositoryPkParam ");
		
		sbQuery.append("ORDER BY P.description");
		Query query = em.createQuery(sbQuery.toString()); 
		
		
		if( Validations.validateIsNotNullAndPositive(filter.getIdInternationalDepositoryPk()) )
			query.setParameter("idInternationalDepositoryPkParam", filter.getIdInternationalDepositoryPk());
    	
		List<InternationalDepository> lstInternationalDepository = new ArrayList<InternationalDepository>();
		InternationalDepository internationalDepository = null;
		List<Object[]> lstObject = query.getResultList();
		for (int i = 0; i < lstObject.size(); i++) {
			Object obj[] = lstObject.get(i);
			internationalDepository = new InternationalDepository();
			internationalDepository.setIdInternationalDepositoryPk((Long)obj[0]);
			internationalDepository.setDescription((String)obj[1]);
			internationalDepository.setStateIntDepositary((Integer)obj[2]);
			lstInternationalDepository.add(internationalDepository);
		}
		
		//List<Participant> lstParticipant = query.getResultList();
		return lstInternationalDepository;
	}

	
	/**
	 * Find participants by filter.
	 *
	 * @param filter the filter
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<Participant> findParticipantsByFilter(Participant filter){
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> mapParam = new HashMap<String, Object>();
		
		sbQuery.append(	" select new Participant( " +
						"	pa.idParticipantPk,	pa.description,	" + //0 1
						"	pa.mnemonic, pa.state)	" + // 2 3
						" from	Participant pa  where 1=1	" );
		if (Validations.validateIsNotNullAndPositive(filter.getState())) {
			sbQuery.append(" and pa.state = :state ");
			mapParam.put("state", filter.getState());
		}
		if (Validations.validateIsNotNullAndPositive(filter.getIdParticipantPk())) {
			sbQuery.append(" and pa.idParticipantPk = :partCode ");
			mapParam.put("partCode", filter.getIdParticipantPk());
		}
		if (StringUtils.isNotBlank(filter.getDescription())) {
			sbQuery.append(" and pa.description like :description ");
			mapParam.put("description", "%"+filter.getDescription()+"%");
		}
		if (StringUtils.isNotBlank(filter.getMnemonic())) {
			sbQuery.append(" and pa.mnemonic like :mnemonic ");
			mapParam.put("description", "%"+filter.getMnemonic()+"%");
		}
		if(filter.getAccountClass()!=null){
			sbQuery.append(" and pa.accountClass = :class ");
			mapParam.put("class", filter.getAccountClass());
		}
		
		sbQuery.append(" order by pa.mnemonic ");
	
		return (List<Participant>)findListByQueryString(sbQuery.toString(), mapParam);
	}
	
	/**
	 * Find participants were change the status to start charging for rates 5 and 6.
	 *
	 * @param filter the filter
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<Participant> findParticipantChangeStatusRates(Participant filter){
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> mapParam = new HashMap<String, Object>();
		
		sbQuery.append(	" select new Participant( " +
						"	pa.idParticipantPk,	pa.description,	" + //0 1
						"	pa.mnemonic, pa.state)	" + // 2 3
						" from	Participant pa  where 1=1	" );
		if (Validations.validateIsNotNullAndPositive(filter.getState())) {
			sbQuery.append(" and pa.state = :state ");
			mapParam.put("state", filter.getState());
		}
		if (Validations.validateIsNotNullAndPositive(filter.getIdParticipantPk())) {
			sbQuery.append(" and pa.idParticipantPk = :partCode ");
			mapParam.put("partCode", filter.getIdParticipantPk());
		}
		if (StringUtils.isNotBlank(filter.getDescription())) {
			sbQuery.append(" and pa.description like :description ");
			mapParam.put("description", "%"+filter.getDescription()+"%");
		}
		if (StringUtils.isNotBlank(filter.getMnemonic())) {
			sbQuery.append(" and pa.mnemonic like :mnemonic ");
			mapParam.put("description", "%"+filter.getMnemonic()+"%");
		}
		if(filter.getAccountClass()!=null){
			sbQuery.append(" and pa.accountClass = :class ");
			mapParam.put("class", filter.getAccountClass());
		}
		if(filter.getPaymentState()!=null){
			sbQuery.append(" and pa.paymentState = :payment ");
			mapParam.put("payment", filter.getPaymentState());
		}
		
		sbQuery.append(" order by pa.mnemonic ");
	
		return (List<Participant>)findListByQueryString(sbQuery.toString(), mapParam);
	}
	
	/**
	 * update change the status to start charging for rates 5 and 6.
	 *
	 * @param participant
	 */
	public void updateChangeStatusChargingRates (Long participant) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" update PARTICIPANT set PAYMENT_STATE = 1 ");
		stringBuilder.append(" where ID_PARTICIPANT_PK = :idParticipant ");

		Query query = em.createNativeQuery(stringBuilder.toString());
		query.setParameter("idParticipant", participant);
		query.executeUpdate();
		
	}
	
	/**
	 * Gets the combo participants by map filter.
	 *
	 * @param filter the filter
	 * @return the combo participants by map filter
	 */
	@SuppressWarnings("unchecked")
	public List<Participant> getComboParticipantsByMapFilter(Map<String,Object> filter) {
		
		Object states = filter.get("states");
		Object accountClass = filter.get("accountClass");
		Object accountType = filter.get("accountType");
		Object idParticipantPk = filter.get("idParticipantPk");
		Object indDepositary = filter.get("indDepositary");
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select p.idParticipantPk, p.description, p.accountClass, p.accountType, p.mnemonic, p.state from Participant p where 1 = 1 ");

		if (states!=null) 
			sbQuery.append(" and p.state in :states ");
		
		if (idParticipantPk!=null) 
			sbQuery.append(" and p.idParticipantPk in :idParticipantPk ");
		
		if (accountClass!=null) 
			sbQuery.append(" and p.accountClass in :accountClass ");

		if (accountType!=null) 
			sbQuery.append(" and p.accountType in :accountType ");
		
		if(indDepositary!=null) {
			sbQuery.append(" and p.indDepositary = :indDepositary ");
		}
		
		sbQuery.append("order by p.mnemonic ");
		
		Query query = em.createQuery(sbQuery.toString());
		
		if (states!=null) 
			query.setParameter("states", states);
		
		if (idParticipantPk!=null) 
			query.setParameter("idParticipantPk", idParticipantPk);
		
		if (idParticipantPk!=null) 
			query.setParameter("accountClass", accountClass);

		if (accountType!=null) 
			query.setParameter("accountType", accountType);
		
		if(indDepositary!=null) {
			query.setParameter("indDepositary", indDepositary);
		}
		
		List<Participant> lstParticipant = new ArrayList<Participant>();
		Participant participant = null;
		List<Object[]> lstObject = query.getResultList();
		for (int i = 0; i < lstObject.size(); i++) {
			Object obj[] = lstObject.get(i);
			participant = new Participant();
			participant.setIdParticipantPk((Long)obj[0]);
			participant.setDescription(obj[1].toString());
			participant.setAccountClass((Integer)obj[2]);
			participant.setAccountType((Integer)obj[3]);
			participant.setMnemonic(obj[4].toString());
			participant.setState((Integer)obj[5]);
			
			lstParticipant.add(participant);
		}
		
		return lstParticipant;
	}
	
	/**
	 * Find Participant By Filters 
	 * @param filter
	 * @return
	 * @throws ServiceException
	 */
	public Participant findParticipantByFilters(Participant filter) throws ServiceException{
		Participant participant=new Participant();
		StringBuilder sbQuery = new StringBuilder();
		try{
			
			sbQuery.append(	" select new Participant( " +
							"	pa.idParticipantPk,	pa.description,	" + //0 1
							"	pa.mnemonic, pa.state)	" + // 2 3
							" from	Participant pa  where 1=1	" );
			if (Validations.validateIsNotNullAndPositive(filter.getState())) {
				sbQuery.append(" and pa.state = :state ");
			}
			if (Validations.validateIsNotNullAndPositive(filter.getIdParticipantPk())) {
				sbQuery.append(" and pa.idParticipantPk = :partCode ");
			}
			if (StringUtils.isNotBlank(filter.getDescription())) {
				sbQuery.append(" and pa.description like :description ");
			}
			if (StringUtils.isNotBlank(filter.getMnemonic())) {
				sbQuery.append(" and pa.mnemonic like :mnemonic ");
			}
			if (StringUtils.isNotBlank(filter.getIdBcbCode())) {
				sbQuery.append(" and pa.idBcbCode = :idBcbCode ");
			}
			
			sbQuery.append(" order by pa.mnemonic ");
		
			Query query = em.createQuery(sbQuery.toString());
			if (Validations.validateIsNotNullAndPositive(filter.getState())) {
				query.setParameter("state", filter.getState());
			}
			if (Validations.validateIsNotNullAndPositive(filter.getIdParticipantPk())) {
				query.setParameter("partCode", filter.getIdParticipantPk());
			}
			if (StringUtils.isNotBlank(filter.getDescription())) {
				query.setParameter("description", "%"+filter.getDescription()+"%");
			}
			if (StringUtils.isNotBlank(filter.getMnemonic())) {
				query.setParameter("mnemonic", "%"+filter.getMnemonic()+"%");
			}			
			if (StringUtils.isNotBlank(filter.getIdBcbCode())) {
				query.setParameter("idBcbCode", filter.getIdBcbCode());
			}
			
			participant=(Participant) query.getSingleResult();
			
		} catch(NoResultException ex){
			return null;
		} catch(NonUniqueResultException ex){
			return null;
		}
		return participant;
		
	}
	/**
	 * Find participant by filters service bean.
	 *
	 * @param filter the filter
	 * @return the participant
	 * @throws ServiceException the service exception
	 */
	public Participant findParticipantByFiltersServiceBean(Participant filter) throws ServiceException{
		try {
			CriteriaBuilder criteriaBuilder=em.getCriteriaBuilder();
			CriteriaQuery<Participant> criteriaQuery=criteriaBuilder.createQuery(Participant.class);
			Root<Participant> participantRoot=criteriaQuery.from(Participant.class);
			List<Predicate> criteriaParameters=new ArrayList<Predicate>();
			
			criteriaQuery.select(participantRoot);
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getIdParticipantPk()) &&
					Validations.validateIsPositiveNumber(filter.getIdParticipantPk())){
				ParameterExpression<Long> param = criteriaBuilder.parameter(Long.class,"idPrm");
				criteriaParameters.add(criteriaBuilder.equal(participantRoot.get("idParticipantPk"), param));			
			}
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getState()) &&
					Validations.validateIsPositiveNumber(filter.getState())){
				ParameterExpression<Integer> param = criteriaBuilder.parameter(Integer.class,"statePrm");
				criteriaParameters.add(criteriaBuilder.equal(participantRoot.get("state"), param));			
			}
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getDocumentType()) &&
					Validations.validateIsPositiveNumber(filter.getDocumentType())){
				ParameterExpression<Integer> param = criteriaBuilder.parameter(Integer.class,"documentTypePrm");
				criteriaParameters.add(criteriaBuilder.equal(participantRoot.get("documentType"), param));	
			}
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getDocumentNumber() )){
				ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"documentNumberPrm");
				criteriaParameters.add(criteriaBuilder.equal(participantRoot.get("documentNumber"), param));	
			}
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getDescription() )){
				ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"descriptionPrm");
				criteriaParameters.add(criteriaBuilder.equal(participantRoot.get("description"), param));	
			}
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getMnemonic() )){
				ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"mnemonicPrm");
				criteriaParameters.add(criteriaBuilder.equal(participantRoot.get("mnemonic"), param));	
			}
			
			if(!criteriaParameters.isEmpty()){
				if (criteriaParameters.size() == 1) {
					criteriaQuery.where(criteriaParameters.get(0));
			    } else {
			    	criteriaQuery.where(criteriaBuilder.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()])));
			    }
			}
			
			TypedQuery<Participant> participantCriteria = em.createQuery(criteriaQuery);
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getIdParticipantPk()) &&
					Validations.validateIsPositiveNumber(filter.getIdParticipantPk())){
				participantCriteria.setParameter("idPrm", filter.getIdParticipantPk());
			}
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getState()) &&
					Validations.validateIsPositiveNumber(filter.getState())){
				participantCriteria.setParameter("statePrm", filter.getState());
			}
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getDocumentType()) &&
					Validations.validateIsPositiveNumber(filter.getDocumentType())){
				participantCriteria.setParameter("documentTypePrm", filter.getDocumentType());
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentNumber())){
				participantCriteria.setParameter("documentNumberPrm", filter.getDocumentNumber());
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getDescription())){
				participantCriteria.setParameter("descriptionPrm", filter.getDescription());
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getMnemonic())){
				participantCriteria.setParameter("mnemonicPrm", filter.getMnemonic());
			}
			
			return (Participant)participantCriteria.getSingleResult();
		
		} catch(NoResultException ex){
		   return null;
		} catch(NonUniqueResultException ex){
		   return null;
		}
	}
	
	/**
	 * Gets the participant state.
	 *
	 * @param idParticipantPk the id participant pk
	 * @return the participant state
	 * @throws ServiceException the service exception
	 */
	public Participant getParticipantState(Long idParticipantPk) throws ServiceException{
		Map<String,Object> mapParam = new HashMap<String, Object>();
		mapParam.put("idParticipantPkParam", idParticipantPk);
		return findObjectByNamedQuery(Participant.PARTICIPANT_STATE, mapParam);
	}
	
	/**
	 * Gets the participant by pk.
	 *
	 * @param idParticipant the id participant
	 * @return the participant by pk
	 */
	public Participant getParticipantByPk(Long idParticipant) {
		Participant objParticipant= null;
		try {
			StringBuilder stringBuilder= new StringBuilder();
			stringBuilder.append(" SELECT PA FROM Participant PA ");
			stringBuilder.append(" INNER JOIN FETCH PA.holder ");
			stringBuilder.append(" WHERE PA.idParticipantPk = :idParticipant ");			
			TypedQuery<Participant> typedQuery= em.createQuery(stringBuilder.toString(), Participant.class);
			typedQuery.setParameter("idParticipant", idParticipant);
			objParticipant= typedQuery.getSingleResult();
		} catch (NoResultException e) {
			
		}
		return objParticipant;
	}
	
	/**
	 * Gets the list holder account balance.
	 *
	 * @param idParticipant the id participant
	 * @param idSecudityCode the id secudity code
	 * @param isReversal the is reversal
	 * @return the list holder account balance
	 */
	public List<HolderAccountBalance> getListHolderAccountBalance(Long idParticipant, String idSecudityCode, boolean isReversal) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" SELECT HAB ");
		stringBuilder.append(" FROM HolderAccountBalance HAB ");
		stringBuilder.append(" JOIN FETCH HAB.holderAccount HA ");
		stringBuilder.append(" JOIN FETCH HA.holderAccountDetails HAD ");
		stringBuilder.append(" JOIN FETCH HAD.holder H ");
		stringBuilder.append(" WHERE 1=1 ");
		if(Validations.validateIsNotNullAndNotEmpty(idParticipant)){
			stringBuilder.append(" and HAB.participant.idParticipantPk = :idParticipant ");
		}
		stringBuilder.append(" and HAB.security.idSecurityCodePk = :idSecudityCode ");
		if(!isReversal){
			stringBuilder.append(" and HAB.totalBalance > 0 ");
		}		
		TypedQuery<HolderAccountBalance> typedQuery= em.createQuery(stringBuilder.toString(), HolderAccountBalance.class);
		if(Validations.validateIsNotNullAndNotEmpty(idParticipant)){
			typedQuery.setParameter("idParticipant", idParticipant);
		}		
		typedQuery.setParameter("idSecudityCode", idSecudityCode);		
		List<HolderAccountBalance> lstReturn = typedQuery.getResultList(); 
		/*if(lstReturn!=null && lstReturn.size()>0) {
			lstReturn.get(0).getHolderAccount();
		}*/
		return lstReturn;
	}
	
	/**
	 * Gets the holder account movement renewal.
	 *
	 * @param idSecurityCode the id security code
	 * @return the holder account movement renewal
	 */
	public HolderAccountMovement getHolderAccountMovementRenewal(String idSecurityCode){
		HolderAccountMovement objHolderAccountMovement = new HolderAccountMovement();
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT ham ");
		sbQuery.append(" FROM HolderAccountMovement ham  ");
		sbQuery.append(" JOIN FETCH ham.holderAccountBalance hab ");
		sbQuery.append(" WHERE hab.security.idSecurityCodePk = :parSecurityCode  ");
		sbQuery.append(" ORDER BY ham.idHolderAccountMovementPk desc  ");		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("parSecurityCode", idSecurityCode);
		objHolderAccountMovement = (HolderAccountMovement) query.getResultList().get(0);		
		return objHolderAccountMovement;
	}
	
	/**
	 * Gets the holder account balance by retirement.
	 *
	 * @param strIdSecurityCode the str id security code
	 * @param lngIdParticipant the lng id participant
	 * @param lngIdHolderAccount the lng id holder account
	 * @return the holder account balance by retirement
	 */
	public HolderAccountBalance getHolderAccountBalanceByRetirement(String strIdSecurityCode,
			Long lngIdParticipant, Long lngIdHolderAccount){
		HolderAccountBalance objHolderAccountBalance = new HolderAccountBalance();				
		try {
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(" SELECT HAB ");
			stringBuilder.append(" FROM HolderAccountBalance HAB  ");
			stringBuilder.append(" WHERE HAB.participant.idParticipantPk = :parIdParticipant ");
			stringBuilder.append(" and HAB.security.idSecurityCodePk = :parIdSecurityCode ");
			stringBuilder.append(" and HAB.holderAccount.idHolderAccountPk = :parIdHolderAccountCode ");			
			TypedQuery<HolderAccountBalance> typedQuery= em.createQuery(stringBuilder.toString(), HolderAccountBalance.class);
			typedQuery.setParameter("parIdParticipant", lngIdParticipant);
			typedQuery.setParameter("parIdSecurityCode", strIdSecurityCode);
			typedQuery.setParameter("parIdHolderAccountCode", lngIdHolderAccount);
			objHolderAccountBalance = typedQuery.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
		return objHolderAccountBalance;
	}
	
	/**
	 * Gets the participant for issuer dpf dpa.
	 *
	 * @param idIssuerDpfDpa the id issuer dpf dpa
	 * @return the participant for issuer dpf dpa
	 */
	public Participant getParticipantForIssuerDpfDpa(String idIssuerDpfDpa) {
//		Participant objParticipant= null;
//		try {
//			StringBuffer stringBuffer= new StringBuffer();
//			stringBuffer.append(" SELECT PA FROM Participant PA ");
//			stringBuffer.append(" INNER JOIN FETCH PA.issuer ISS ");
//			stringBuffer.append(" WHERE ISS.idIssuerPk = :idIssuer ");
//			stringBuffer.append(" and PA.economicActivity = :activityEconomicPart ");
//			stringBuffer.append(" and ISS.economicActivity = :activityEconomicIss ");
//			TypedQuery<Participant> typedQuery = em.createQuery(stringBuffer.toString(), Participant.class);
//			typedQuery.setParameter("idIssuer", idIssuerDpfDpa);
//			typedQuery.setParameter("activityEconomicPart", EconomicActivityType.BANCOS.getCode());
//			typedQuery.setParameter("activityEconomicIss", EconomicActivityType.BANCOS.getCode());
//			objParticipant = typedQuery.getSingleResult();
//		} catch(NoResultException ex){
//			return null;
//		} catch(NonUniqueResultException ex){
//			return null;
//		}
		return depositarySetup.getParticipantByIssuerCode().get(idIssuerDpfDpa);
	}
	
	/**
	 * Gets the participant is issuer.
	 *
	 * @param idIssuerDpfDpa the id issuer dpf dpa
	 * @return the participant is issuer
	 */
	public Participant getParticipantIsIssuer(String idIssuerDpfDpa) {
		Participant objParticipant= null;
		try {
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(" SELECT PA FROM Participant PA ");
			stringBuilder.append(" INNER JOIN FETCH PA.issuer ISS ");
			stringBuilder.append(" WHERE ISS.idIssuerPk = :idIssuer ");
			TypedQuery<Participant> typedQuery = em.createQuery(stringBuilder.toString(), Participant.class);
			typedQuery.setParameter("idIssuer", idIssuerDpfDpa);			
			objParticipant = typedQuery.getSingleResult();
		} catch(NoResultException ex){
			return null;
		} catch(NonUniqueResultException ex){
			return null;
		}
		return objParticipant;
	}
	
	/**
	 * Gets the issuer is participant.
	 *
	 * @param idParticipantPk the id participant pk
	 * @return the issuer is participant
	 */
	public String getIssuerIsParticipant(Long idParticipantPk){
		String idIssuerPk="";
		try {
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(" SELECT ISS.idIssuerPk FROM Participant PA ");
			stringBuilder.append(" INNER JOIN PA.issuer ISS ");
			stringBuilder.append(" WHERE PA.idParticipantPk = :idParticipant ");

			Query query = em.createQuery(stringBuilder.toString());
			query.setParameter("idParticipant", idParticipantPk);
			idIssuerPk = (String) query.getSingleResult();
			
		} catch(NoResultException ex){
			return null;
		} catch(NonUniqueResultException ex){
			return null;
		}
		return idIssuerPk;
	}
	
	/**
	 * Gets the issuer mnemonic and Description is participant.
	 *
	 * @param idParticipantPk the id participant pk
	 * @return the issuer mnemonic is participant
	 */
	public IssuerTO getIssuerMnemonicIsParticipant(Long idParticipantPk){
		IssuerTO issuerTo=new IssuerTO();
		try {
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(" SELECT ISS.idIssuerPk, ISS.mnemonic, ISS.businessName");
			stringBuilder.append("	 FROM Participant PA ");
			stringBuilder.append(" INNER JOIN PA.issuer ISS ");
			stringBuilder.append(" WHERE PA.idParticipantPk = :idParticipant ");

			Query query = em.createQuery(stringBuilder.toString());
			query.setParameter("idParticipant", idParticipantPk);
			Object objectIssuer =  query.getSingleResult();
			issuerTo.setIssuerCode(((Object[])objectIssuer)[0].toString());
			issuerTo.setMnemonic(((Object[])objectIssuer)[1].toString());
			issuerTo.setIssuerDesc(((Object[])objectIssuer)[2].toString());
		} catch(NoResultException ex){
			return null;
		} catch(NonUniqueResultException ex){
			return null;
		}
		return issuerTo;
	}
	/**
	 * Obtiene el emisor administrador segun el pk del participante
	 *
	 * @param idParticipantPk 
	 * @return issuer
	 * @author hcoarite
	 */
	public IssuerTO getIssuerByParticipant(Long idParticipantPk){
		IssuerTO issuerTo=new IssuerTO();
		try {
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(" SELECT ISS.id_Issuer_Pk, ISS.mnemonic, ISS.business_Name ");
			stringBuilder.append("	 from issuer iss ");
			stringBuilder.append("	 inner join PARTICIPANT p on p.DOCUMENT_NUMBER=iss.DOCUMENT_NUMBER ");
			stringBuilder.append(" where p.MNEMONIC=iss.MNEMONIC ");
			stringBuilder.append(" and p.ID_PARTICIPANT_PK= :idParticipant ");

			Query query = em.createNativeQuery(stringBuilder.toString());
			query.setParameter("idParticipant", idParticipantPk);
			Object objectIssuer =  query.getSingleResult();
			issuerTo.setIssuerCode(((Object[])objectIssuer)[0].toString());
			issuerTo.setMnemonic(((Object[])objectIssuer)[1].toString());
			issuerTo.setIssuerDesc(((Object[])objectIssuer)[2].toString());
		} catch(NoResultException ex){
			return null;
		} catch(NonUniqueResultException ex){
			return null;
		}
		return issuerTo;
	}
	
	/**
	 * Gets the lis participant service bean.
	 *
	 * @param filter the filter
	 * @return the lis participant service bean
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Participant> getListParticipantMnmonic(Participant filter) throws ServiceException{
		List<Participant> lstParticipant = new ArrayList<Participant>();
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();		
		sbQuery.append(" Select DISTINCT P.idParticipantPk," );//0
		sbQuery.append(" P.description," ); 	//1
		sbQuery.append(" P.mnemonic,");		//2
		sbQuery.append(" P.state  ");	//3
		sbQuery.append(" From Participant P ");		
		sbQuery.append(" Where 1 = 1 ");		
		if(filter.getIdParticipantPk()!=null){
			sbQuery.append(" and P.idParticipantPk = :idParticipantParam ");
			parameters.put("idParticipantParam", filter.getIdParticipantPk());
		}		
		if(Validations.validateIsNotNullAndPositive(filter.getState())){
			sbQuery.append(" and P.state = :statePrm ");
			parameters.put("statePrm", filter.getState());
		}		
		if(Validations.validateListIsNotNullAndNotEmpty(filter.getLstParticipantStates())){
			sbQuery.append(" and P.state in (:lstStatePrm) ");
			parameters.put("lstStatePrm", filter.getLstParticipantStates());
		}		
		sbQuery.append("ORDER BY P.mnemonic");		
		List<Object[]> lstObject = findListByQueryString(sbQuery.toString(), parameters);
		if(lstObject!=null && lstObject.size()>0){
			Participant objParticipant = null;
			for(int i = 0; i < lstObject.size(); i++){
				objParticipant = new Participant();
				Object obj[] = lstObject.get(i);
				if(obj[0]!=null){
					objParticipant.setIdParticipantPk((Long)obj[0]);
				}
				if(obj[1]!=null){
					objParticipant.setDescription((String)obj[1]);
				}
				if(obj[2]!=null){
					objParticipant.setMnemonic((String)obj[2]);			
				}
				if(obj[3]!=null){
					objParticipant.setState((Integer)obj[3]);
				}
				lstParticipant.add(objParticipant);
			}
		}
		return lstParticipant;
	}
	
	/**
	 * Gets the participant pk.
	 *
	 * @param participant the participant
	 * @return the participant pk
	 * @throws ServiceException the service exception
	 */
	public Participant getParticipantByPk(Participant participant) throws ServiceException {
		Participant objParticipant = null;
		try {
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(" SELECT part.idParticipantPk, part.state, 		"); // 0, 1
			stringBuilder.append(" part.mnemonic									"); // 2
			stringBuilder.append(" FROM Participant part  							");			
			stringBuilder.append(" WHERE part.mnemonic = :parmnemonic 				");			
			Query typedQuery= em.createQuery(stringBuilder.toString());
			typedQuery.setParameter("parmnemonic", participant.getMnemonic());
			Object [] resultSecurity = (Object[]) typedQuery.getSingleResult();	
			if(resultSecurity != null){
				objParticipant = new Participant();
				objParticipant.setIdParticipantPk(new Long(resultSecurity[0].toString()));
				objParticipant.setState(new Integer(resultSecurity[1].toString()));
				objParticipant.setMnemonic(resultSecurity[2].toString());
			}
		} catch (NoResultException ex) {
			return null;
		}
		return objParticipant;
	}
	
	/**
	 * Gets the participant pk.
	 *
	 * @param participant the participant
	 * @return the participant pk
	 * @throws ServiceException the service exception
	 */
	public Participant getParticipantByPkWithUniqueResultValidation(Participant participant) throws ServiceException {
		Participant objParticipant = null;
		try {
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(" SELECT part.idParticipantPk, part.state, 		"); // 0, 1
			stringBuilder.append(" part.mnemonic									"); // 2
			stringBuilder.append(" FROM Participant part  							");			
			stringBuilder.append(" WHERE part.mnemonic = :parmnemonic 				");			
			Query typedQuery= em.createQuery(stringBuilder.toString());
			typedQuery.setParameter("parmnemonic", participant.getMnemonic());
			Object [] resultSecurity = (Object[]) typedQuery.getSingleResult();	
			if(resultSecurity != null){
				objParticipant = new Participant();
				objParticipant.setIdParticipantPk(new Long(resultSecurity[0].toString()));
				objParticipant.setState(new Integer(resultSecurity[1].toString()));
				objParticipant.setMnemonic(resultSecurity[2].toString());
			}
		} catch (NoResultException ex) {
			return null;
		} catch (NonUniqueResultException e) {
			objParticipant = new Participant();
			objParticipant.setIdParticipantPk(GeneralConstants.ZERO_VALUE_LONG);
		}
		return objParticipant;
	}
	
	/**
	 * Gets the participant for issuer.
	 *
	 * @return the participant for issuer
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getParticipantForAgencia(Long idParticipantPk) {
		List<Object[]> listParticipant=new ArrayList<>();
		try {
			StringBuffer stringBuffer= new StringBuffer();
			stringBuffer.append(" SELECT PA.idParticipantPk,						");
			stringBuffer.append(" PA.state											");
			stringBuffer.append(" FROM Participant PA 								");
			stringBuffer.append(" WHERE 1 = 1 										");
			stringBuffer.append(" and PA.economicActivity = :activityEconomicPart 	");
			stringBuffer.append(" and PA.idParticipantPk= :idParticipantPk		 	");
			Query query = em.createQuery(stringBuffer.toString());
			query.setParameter("activityEconomicPart", EconomicActivityType.AGENCIAS_BOLSA.getCode());
			query.setParameter("idParticipantPk", idParticipantPk);
			
			listParticipant=(List<Object[]>)query.getResultList();
			
			
		} catch(NoResultException ex){
			return null;
		} catch(NonUniqueResultException ex){
			return null;
		}
		return listParticipant;
	}
	
	public List<SecuritiesManager> getSecuritiesManagers() {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT sm");
		sbQuery.append("	FROM SecuritiesManager sm");
		Query query = em.createQuery(sbQuery.toString());
		return query.getResultList();
	}
}
