package com.pradera.core.component.helperui.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.el.ELContext;
import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.helperui.HelperBean;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.AccountingAccountTo;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounting.AccountingParameter;
import com.pradera.model.accounting.type.AccountingParameterStateType;
import com.pradera.model.accounting.type.AccountingParameterType;


@HelperBean
public class AccountingAccountHelperBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 230216630609700020L;
	
	/** The log. */
	@Inject
	private transient PraderaLogger log;
	
	
	@EJB
	HelperComponentFacade helperComponentFacade;
	@EJB
	GeneralParametersFacade parametersFacade;
	@EJB
	GeneralParametersFacade generalParametersFacade;

	  
	private GenericDataModel<AccountingAccountTo> dataModelAccountingAccountToList ; 
	private List<AccountingParameter> listAccountType ;
	private List<AccountingParameter> listTypeAuxiliary ;

	private List<AccountingAccountTo> listAccountingAccountTO ;
	private AccountingAccountTo accountingAccountToTemp  = new AccountingAccountTo();
	
	private Boolean			disableAuxiliaryType;
	
	@PostConstruct
	public void init() throws ServiceException {
	  
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		
		
		dataModelAccountingAccountToList = new GenericDataModel<AccountingAccountTo>();
		listAccountType = new ArrayList<AccountingParameter>();
		listAccountingAccountTO = new ArrayList<AccountingAccountTo>();
		
		
		ValueExpression indAccumu = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.indAccumulationAccount}", Long.class);
		Object accumulation = indAccumu.getValue(elContext);
		Integer accumulationInd=Integer.parseInt(accumulation.toString());

		
		
		
		accountingAccountToTemp.setIndAccumulation(accumulationInd);
		 
	}
	
	/**
	 * Find Accounting Account by Code
	 */
	public void findAccountingAccountByCode(){
		
		
		try {
		
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
																																			             
		ValueExpression valueExpressionDescriptionAccount = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.accountingAccount.accountCode}", String.class);
		Object valueDescriptionAccount = valueExpressionDescriptionAccount.getValue(elContext);
		
		String accountCode = valueDescriptionAccount.toString() ;
		ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.accountingAccount}", Object.class);
		
		ValueExpression accountingAccountHelpMessageValueExp = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.resourceBundleMap.accountingAccountHelpMessage}", String.class);
		String accountingParameterHelpMessage= (String)accountingAccountHelpMessageValueExp.getValue(elContext);
		
		ValueExpression valueExpressionEnableActionRemote= context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.enableActionRemote}", Boolean.class);
		Boolean enableActionRemote=(Boolean)valueExpressionEnableActionRemote.getValue(elContext);
		
		ValueExpression titleValueEx = context.getApplication().getExpressionFactory()
	    .createValueExpression(elContext, "#{cc.resourceBundleMap.title}", String.class);
		String title= (String)titleValueEx.getValue(elContext);
		
		/***
		 * Add Filter Type Daily Auxiliary
		 */
		ValueExpression valueExpressionTypeDailyAux = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.typeDailyAux}", Long.class);
		Object valueTypeDailyAux = valueExpressionTypeDailyAux.getValue(elContext);
		Integer valueTypeDailyAuxInteger=Integer.parseInt(valueTypeDailyAux.toString());
 		
		if(Validations.validateIsNotNullAndNotEmpty(accountCode)){
			
			AccountingAccountTo accountingAccounTo = new AccountingAccountTo();
				
			accountingAccounTo = helperComponentFacade.findAccountingAccountrByCode(accountCode, valueTypeDailyAuxInteger);
			 	
			if(Validations.validateIsNotNull(accountingAccounTo)){
				
				valueExpression.setValue(elContext, accountingAccounTo);
				if(enableActionRemote){
					 JSFUtilities.executeJavascriptFunction("oncomplete();");
				}
			}
			else{
				accountingAccounTo = new AccountingAccountTo();
				 JSFUtilities.putViewMap("bodyMessageView", accountingParameterHelpMessage);
				 JSFUtilities.putViewMap("headerMessageView", title);
				 RequestContext.getCurrentInstance().execute("PF('cnfwMsgCustomValidationBlur').show();");
				 valueExpression.setValue(elContext, accountingAccounTo);
				 if(enableActionRemote){
					 JSFUtilities.executeJavascriptFunction("oncomplete();");
				}
			}
		}
		
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	/**
	 * Search  Accounting Account by Dialog
	 */
	public void searchAccountingAccount(){

		AccountingAccountTo  accountingAccountTO = new AccountingAccountTo();

		accountingAccountTO.setAccountCode(this.accountingAccountToTemp.getAccountCode());
		accountingAccountTO.setAccountType(this.accountingAccountToTemp.getAccountType());
		accountingAccountTO.setAuxiliaryType(this.accountingAccountToTemp.getAuxiliaryType());
		
		listAccountingAccountTO = helperComponentFacade.searchParameterAccounting(accountingAccountTO);
		
		if(Validations.validateListIsNullOrEmpty(listAccountingAccountTO)){
			dataModelAccountingAccountToList = null;
			return;
		}

		dataModelAccountingAccountToList = new GenericDataModel<AccountingAccountTo>(listAccountingAccountTO);
		
	}
	
	/**
	 * Clean Fields Helper
	 */
	public void clearHelperSearchAccountingAccount(){
		
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		
		this.accountingAccountToTemp = new AccountingAccountTo();
		
		ValueExpression indAccumu = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.indAccumulationAccount}", Long.class);
		Object accumulation = indAccumu.getValue(elContext);
		Integer accumulationInd=Integer.parseInt(accumulation.toString());
		
		dataModelAccountingAccountToList = null;
		disableAuxiliaryType=Boolean.FALSE;
		if( accumulationInd.equals(GeneralConstants.ONE_VALUE_INTEGER)){
			
			this.accountingAccountToTemp.setIndAccumulation(GeneralConstants.ONE_VALUE_INTEGER) ;
			
		}
			
			
		 
		
	}

	/**
	 * Assign Account search to Dialog
	 * @param accountingAccountTO
	 */
	public void assignAccount(AccountingAccountTo accountingAccountTO){
		
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		
		ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.accountingAccount}", Object.class);
		valueExpression.setValue(elContext, accountingAccountTO);
		 
		JSFUtilities.executeJavascriptFunction("dlgParameter.hide();");
		ValueExpression valueExpressionEnableActionRemote= context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.enableActionRemote}", Boolean.class);
		Boolean enableActionRemote=(Boolean)valueExpressionEnableActionRemote.getValue(elContext);
		if(enableActionRemote){
			JSFUtilities.executeJavascriptFunction("oncomplete();");
		}
		
		clearHelperSearchAccountingAccount();
 
	}
	
	/**
	 * Show Dialog
	 */
	public void showSearchAccountingAccount(){
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		
		
		AccountingParameter accountingParameter = new AccountingParameter();
		if (Validations.validateListIsNullOrEmpty(listAccountType)) {
			accountingParameter.setParameterType(AccountingParameterType.TIPO_DE_CUENTA.getCode());
			accountingParameter.setStatus(AccountingParameterStateType.ACTIVED.getCode());
			listAccountType = helperComponentFacade.searchParameterAccounting(accountingParameter);
		}
		if(Validations.validateListIsNullOrEmpty(listTypeAuxiliary)){
			log.debug(" listTypeAuxiliary:");
			accountingParameter.setParameterType(AccountingParameterType.TIPO_DIARIO_AUXILIAR.getCode());
			accountingParameter.setStatus(AccountingParameterStateType.ACTIVED.getCode());
			listTypeAuxiliary = helperComponentFacade.searchParameterAccounting(accountingParameter);
		}
		clearHelperSearchAccountingAccount();
		/***
		 * Add Filter Type Daily Auxiliary
		 */
		ValueExpression valueExpressionTypeDailyAux = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.typeDailyAux}", Long.class);
		Object valueTypeDailyAux = valueExpressionTypeDailyAux.getValue(elContext);
		Integer valueTypeDailyAuxInteger=Integer.parseInt(valueTypeDailyAux.toString());
		if(Validations.validateIsNotNullAndPositive(valueTypeDailyAuxInteger)){
			accountingAccountToTemp.setAuxiliaryType(valueTypeDailyAuxInteger);
			disableAuxiliaryType=Boolean.TRUE;
		}
		
	 
		JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationBlur').hide();");
		JSFUtilities.executeJavascriptFunction("PF('dlgParameter').show();");
		
		
		
	}



	/**
	 * @return the dataModelAccountingAccountToList
	 */
	public GenericDataModel<AccountingAccountTo> getDataModelAccountingAccountToList() {
		return dataModelAccountingAccountToList;
	}



	/**
	 * @param dataModelAccountingAccountToList the dataModelAccountingAccountToList to set
	 */
	public void setDataModelAccountingAccountToList(
			GenericDataModel<AccountingAccountTo> dataModelAccountingAccountToList) {
		this.dataModelAccountingAccountToList = dataModelAccountingAccountToList;
	}



	/**
	 * @return the listAccountType
	 */
	public List<AccountingParameter> getListAccountType() {
		return listAccountType;
	}



	/**
	 * @param listAccountType the listAccountType to set
	 */
	public void setListAccountType(List<AccountingParameter> listAccountType) {
		this.listAccountType = listAccountType;
	}



	/**
	 * @return the listTypeAuxiliary
	 */
	public List<AccountingParameter> getListTypeAuxiliary() {
		return listTypeAuxiliary;
	}



	/**
	 * @param listTypeAuxiliary the listTypeAuxiliary to set
	 */
	public void setListTypeAuxiliary(List<AccountingParameter> listTypeAuxiliary) {
		this.listTypeAuxiliary = listTypeAuxiliary;
	}



	/**
	 * @return the listAccountingAccountTO
	 */
	public List<AccountingAccountTo> getListAccountingAccountTO() {
		return listAccountingAccountTO;
	}



	/**
	 * @param listAccountingAccountTO the listAccountingAccountTO to set
	 */
	public void setListAccountingAccountTO(
			List<AccountingAccountTo> listAccountingAccountTO) {
		this.listAccountingAccountTO = listAccountingAccountTO;
	}



	/**
	 * @return the accountingAccountToTemp
	 */
	public AccountingAccountTo getAccountingAccountToTemp() {
		return accountingAccountToTemp;
	}



	/**
	 * @param accountingAccountToTemp the accountingAccountToTemp to set
	 */
	public void setAccountingAccountToTemp(
			AccountingAccountTo accountingAccountToTemp) {
		this.accountingAccountToTemp = accountingAccountToTemp;
	}



	/**
	 * @return the disableAuxiliaryType
	 */
	public Boolean getDisableAuxiliaryType() {
		return disableAuxiliaryType;
	}



	/**
	 * @param disableAuxiliaryType the disableAuxiliaryType to set
	 */
	public void setDisableAuxiliaryType(Boolean disableAuxiliaryType) {
		this.disableAuxiliaryType = disableAuxiliaryType;
	}
	
	
	
	
	
	


	
	
	
	
}
