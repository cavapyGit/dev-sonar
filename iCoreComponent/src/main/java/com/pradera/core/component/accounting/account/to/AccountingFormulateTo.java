package com.pradera.core.component.accounting.account.to;

import java.io.Serializable;

public class AccountingFormulateTo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2152538021402904398L;
	
	private Long idAccountingFormulateTo;
	private String formulateInput;
	
	
	/**
	 * @param formulateInput
	 */
	public AccountingFormulateTo(String formulateInput) {
		this.formulateInput = formulateInput;
	}
	/**
	 * 
	 */
	public AccountingFormulateTo() {
	}
	/**
	 * @return the idAccountingFormulateTo
	 */
	public Long getIdAccountingFormulateTo() {
		return idAccountingFormulateTo;
	}
	/**
	 * @param idAccountingFormulateTo the idAccountingFormulateTo to set
	 */
	public void setIdAccountingFormulateTo(Long idAccountingFormulateTo) {
		this.idAccountingFormulateTo = idAccountingFormulateTo;
	}
	/**
	 * @return the formulateInput
	 */
	public String getFormulateInput() {
		return formulateInput;
	}
	/**
	 * @param formulateInput the formulateInput to set
	 */
	public void setFormulateInput(String formulateInput) {
		this.formulateInput = formulateInput;
	}
	
	
	

}
