package com.pradera.core.component.accounts.to;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import com.pradera.model.accounts.Participant;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class GeographicLocationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 25/01/2013
 */
public class HolderBalanceMovementsTO implements Serializable{
	
	private Participant objParticipant;
	
	@NotNull(message = "{holderaccounts.account}")
	private Integer accountNumber;
	
	@NotNull(message = "{holderaccounts.security}")
	private String idIsinCodePk;
	
	private Date beginDate;
	
	private Date endDate;
	
	public Participant getObjParticipant() {
		return objParticipant;
	}
	public void setObjParticipant(Participant objParticipant) {
		this.objParticipant = objParticipant;
	}
	
	public Integer getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getIdIsinCodePk() {
		return idIsinCodePk;
	}
	public void setIdIsinCodePk(String idIsinCodePk) {
		this.idIsinCodePk = idIsinCodePk;
	}
	public Date getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	
}
