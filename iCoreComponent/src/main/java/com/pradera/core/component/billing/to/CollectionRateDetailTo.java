package com.pradera.core.component.billing.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.pradera.commons.view.GenericBaseBean;

public class CollectionRateDetailTo  extends GenericBaseBean implements Serializable{

	/**
	 * Detalle de las tarifas
	 */
	private static final long serialVersionUID = 1L;
	
	private List<RateMovementTo> rateMovementToList;/***/
	private Long rateCodeDetail;
	private BigDecimal amount;
	private Integer movementCount;/***/
	private String movementName;
	private Long idRateMovement;/**Id rateMovement*/
	private Integer movementNumber;/***/
	public List<RateMovementTo> getRateMovementToList() {
		return rateMovementToList;
	}
	public void setRateMovementToList(List<RateMovementTo> rateMovementToList) {
		this.rateMovementToList = rateMovementToList;
	}
	public Long getRateCodeDetail() {
		return rateCodeDetail;
	}
	public void setRateCodeDetail(Long rateCodeDetail) {
		this.rateCodeDetail = rateCodeDetail;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public Integer getMovementCount() {
		return movementCount;
	}
	public void setMovementCount(Integer movementCount) {
		this.movementCount = movementCount;
	}
	public String getMovementName() {
		return movementName;
	}
	public void setMovementName(String movementName) {
		this.movementName = movementName;
	}
	public Long getIdRateMovement() {
		return idRateMovement;
	}
	public void setIdRateMovement(Long idRateMovement) {
		this.idRateMovement = idRateMovement;
	}
	public Integer getMovementNumber() {
		return movementNumber;
	}
	public void setMovementNumber(Integer movementNumber) {
		this.movementNumber = movementNumber;
	}
	
	
	
	

}
