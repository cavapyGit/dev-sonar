/*
 * 
 */
package com.pradera.core.component.business.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.pradera.integration.component.business.to.MarketFactAccountTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SecurityOverdueTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 16-sep-2015
 */
public class SecurityConsultTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The key holder account balance. */
	private String keyHolderAccountBalance;
	
	/** The id security code pk. */
	private String idSecurityCodePk;
	
	/** The description security. */
	private String descriptionSecurity;
	
	/** The description participant. */
	private String descriptionParticipant;
	
	/** The alternate code. */
	private String alternateCode;
	
	/** The description holder. */
	private String descriptionHolder;
	
	private Integer securityClass;

	
	/** The available blance. */
	private Integer availableBalance;
	
	/** The Nominal Value*/
	private BigDecimal nominalValue;

	private Integer currency;
	
	private String descriptionCurrency;
	
	/** The block balance. */
	private Long idHolderPk;
	
	private Integer securityDaysTerm;

	private BigDecimal interestRate;
	
	/**
	 * @return the keyHolderAccountBalance
	 */
	public String getKeyHolderAccountBalance() {
		return keyHolderAccountBalance;
	}

	/**
	 * @param keyHolderAccountBalance the keyHolderAccountBalance to set
	 */
	public void setKeyHolderAccountBalance(String keyHolderAccountBalance) {
		this.keyHolderAccountBalance = keyHolderAccountBalance;
	}

	/**
	 * @return the idSecurityCodePk
	 */
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	/**
	 * @param idSecurityCodePk the idSecurityCodePk to set
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	/**
	 * @return the descriptionSecurity
	 */
	public String getDescriptionSecurity() {
		return descriptionSecurity;
	}

	/**
	 * @param descriptionSecurity the descriptionSecurity to set
	 */
	public void setDescriptionSecurity(String descriptionSecurity) {
		this.descriptionSecurity = descriptionSecurity;
	}

	/**
	 * @return the descriptionParticipant
	 */
	public String getDescriptionParticipant() {
		return descriptionParticipant;
	}

	/**
	 * @param descriptionParticipant the descriptionParticipant to set
	 */
	public void setDescriptionParticipant(String descriptionParticipant) {
		this.descriptionParticipant = descriptionParticipant;
	}

	/**
	 * @return the alternateCode
	 */
	public String getAlternateCode() {
		return alternateCode;
	}

	/**
	 * @param alternateCode the alternateCode to set
	 */
	public void setAlternateCode(String alternateCode) {
		this.alternateCode = alternateCode;
	}

	/**
	 * @return the descriptionHolder
	 */
	public String getDescriptionHolder() {
		return descriptionHolder;
	}

	/**
	 * @param descriptionHolder the descriptionHolder to set
	 */
	public void setDescriptionHolder(String descriptionHolder) {
		this.descriptionHolder = descriptionHolder;
	}

	/**
	 * @return the availableBalance
	 */
	public Integer getAvailableBalance() {
		return availableBalance;
	}

	/**
	 * @param availableBalance the availableBalance to set
	 */
	public void setAvailableBalance(Integer availableBalance) {
		this.availableBalance = availableBalance;
	}

	/**
	 * @return the nominalValue
	 */
	public BigDecimal getNominalValue() {
		return nominalValue;
	}

	/**
	 * @param nominalValue the nominalValue to set
	 */
	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}

	/**
	 * @return the idHolderPk
	 */
	public Long getIdHolderPk() {
		return idHolderPk;
	}

	/**
	 * @param idHolderPk the idHolderPk to set
	 */
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}




	/**
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * @return the descriptionCurrency
	 */
	public String getDescriptionCurrency() {
		return descriptionCurrency;
	}

	/**
	 * @param descriptionCurrency the descriptionCurrency to set
	 */
	public void setDescriptionCurrency(String descriptionCurrency) {
		this.descriptionCurrency = descriptionCurrency;
	}

	/**
	 * @return the securityClass
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}

	/**
	 * @param securityClass the securityClass to set
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}





	/**
	 * @return the securityDaysTerm
	 */
	public Integer getSecurityDaysTerm() {
		return securityDaysTerm;
	}

	/**
	 * @param securityDaysTerm the securityDaysTerm to set
	 */
	public void setSecurityDaysTerm(Integer securityDaysTerm) {
		this.securityDaysTerm = securityDaysTerm;
	}

	/**
	 * @return the interestRate
	 */
	public BigDecimal getInterestRate() {
		return interestRate;
	}

	/**
	 * @param interestRate the interestRate to set
	 */
	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}

	
	

	
	
}
