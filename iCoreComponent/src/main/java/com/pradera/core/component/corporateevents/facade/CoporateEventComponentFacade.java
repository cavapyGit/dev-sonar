package com.pradera.core.component.corporateevents.facade;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.Query;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.corporateevents.service.CorporateEventComponentServiceBean;
import com.pradera.core.component.corporateevents.to.CorporativeOperationTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.issuancesecuritie.InterestPaymentSchedule;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.ProgramAmortizationCoupon;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.CalendarDayType;
import com.pradera.model.issuancesecuritie.type.CalendarMonthType;
import com.pradera.model.issuancesecuritie.type.InstrumentType;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CoporateEventComponentFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01-sep-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)

public class CoporateEventComponentFacade extends CrudDaoServiceBean{
	
	
	/** The coporate event component service bean. */
	@EJB
	private CorporateEventComponentServiceBean coporateEventComponentServiceBean;
	
	/** The transaction registry. */
	@Resource
	TransactionSynchronizationRegistry transactionRegistry;
	
	
	/**
	 * Register corporative operation.
	 *
	 * @param operation the operation
	 * @return the corporative operation
	 * @throws ServiceException the service exception
	 */
	public CorporativeOperation registerCorporativeOperation(CorporativeOperation operation)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAdd());
 		operation.setState(CorporateProcessStateType.REGISTERED.getCode());
 		if(ImportanceEventType.INTEREST_PAYMENT.getCode().equals(operation.getCorporativeEventType().getCorporativeEventType()) 
 				|| ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(operation.getCorporativeEventType().getCorporativeEventType())){
 			if(operation.getSecurities() != null && operation.getSecurities().isDesmaterializedIssuanceForm()){
 				if(operation.getDeliveryFactor() != null && operation.getSecurities().getShareCapital() != null){
 					operation.setPaymentAmount(operation.getDeliveryFactor().divide(BigDecimal.valueOf(100))
 							.multiply(operation.getSecurities().getShareCapital()).setScale(2, BigDecimal.ROUND_HALF_DOWN));
 				} 				
 			}
 		}
		return coporateEventComponentServiceBean.saveCoprorativeOperation(operation);	
		
	}
	
	/**
	 * Gets the list id program interest coupon.
	 *
	 * @param security the security
	 * @param deliveryDate the delivery date
	 * @return the list id program interest coupon
	 */
    public List<Long> getListIdProgramInterestCouponActual(String security,Date deliveryDate)	{
		List<Long> lstIdProgramInterestCoupon= null;
		StringBuilder stringBuffer= new StringBuilder();
		stringBuffer.append(" SELECT PIC.idProgramInterestPk FROM ProgramInterestCoupon PIC ");
		stringBuffer.append(" WHERE PIC.interestPaymentSchedule.security.idSecurityCodePk = :idIsinCode ");
		stringBuffer.append(" and trunc(PIC.paymentDate) = trunc(:currentDate) ");
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("idIsinCode", security);
		query.setParameter("currentDate", deliveryDate);
		lstIdProgramInterestCoupon = query.getResultList();
		return lstIdProgramInterestCoupon;
	}
    
    /**
     * Gets the program interest coupon.
     *
     * @param idProgramInterestPk the id program interest pk
     * @return the program interest coupon
     */
    public ProgramInterestCoupon getProgramInterestCoupon(Long idProgramInterestPk){
    	Object objProgramInterestCoupon = null;
    	StringBuilder stringBuffer= new StringBuilder();
		stringBuffer.append(" SELECT PIC FROM ProgramInterestCoupon PIC ");
		stringBuffer.append(" WHERE PIC.idProgramInterestPk = :idProgramInterestPk ");
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("idProgramInterestPk", idProgramInterestPk);
		objProgramInterestCoupon = query.getSingleResult();
		if(objProgramInterestCoupon!=null){
			return (ProgramInterestCoupon)objProgramInterestCoupon;
		}
		return null;
    }
    
    /**
     * Gets the program amortization coupon.
     *
     * @param idProgramAmortizationPk the id program amortization pk
     * @return the program amortization coupon
     */
    public ProgramAmortizationCoupon getProgramAmortizationCoupon(Long idProgramAmortizationPk){
    	Object objProgramAmortizationCoupon = null;
    	StringBuilder stringBuffer= new StringBuilder();
		stringBuffer.append(" SELECT PIC FROM ProgramAmortizationCoupon PIC ");
		stringBuffer.append(" WHERE PIC.idProgramAmortizationPk = :idProgramAmortizationPk ");
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("idProgramAmortizationPk", idProgramAmortizationPk);
		objProgramAmortizationCoupon = query.getSingleResult();
		if(objProgramAmortizationCoupon!=null){
			return (ProgramAmortizationCoupon)objProgramAmortizationCoupon;
		}
		return null;
    }
    
    /**
     * Gets the list id program interest coupon.
     *
     * @param security the security
     * @param deliveryDate the delivery date
     * @return the list id program interest coupon
     */
    public List<Long> getListIdProgramAmortizationCoupon(String security,Date deliveryDate)	{
		List<Long> lstIdProgramInterestCoupon= null;
		StringBuilder stringBuffer= new StringBuilder();
		stringBuffer.append(" SELECT PIC.idProgramAmortizationPk FROM ProgramAmortizationCoupon PIC ");
		stringBuffer.append(" WHERE PIC.amortizationPaymentSchedule.security.idSecurityCodePk = :idIsinCode ");
		stringBuffer.append(" and trunc(PIC.paymentDate) = trunc(:currentDate) ");
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("idIsinCode", security);
		query.setParameter("currentDate", deliveryDate);
		lstIdProgramInterestCoupon = query.getResultList();
		return lstIdProgramInterestCoupon;
	}
	
	/**
	 * Modifiy corporative operation.
	 *
	 * @param operation the operation
	 * @return the corporative operation
	 * @throws ServiceException the service exception
	 */
	public CorporativeOperation modifiyCorporativeOperation(CorporativeOperation operation)throws ServiceException{
		//Audit Process
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
 		operation.setState(CorporateProcessStateType.MODIFIED.getCode());
		return coporateEventComponentServiceBean.update(operation);
	}
	
	/**
	 * Search corporative operation for dematerialization.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<CorporativeOperation> searchCorporativeOperationForDematerialization()throws ServiceException{
		CorporativeOperationTO operationTO = new CorporativeOperationTO();
		List<Integer> stateList = new ArrayList<Integer>();
		List<Integer> eventTypeList = new ArrayList<Integer>();
		stateList.add(CorporateProcessStateType.REGISTERED.getCode());
		stateList.add(CorporateProcessStateType.MODIFIED.getCode());
		stateList.add(CorporateProcessStateType.PRELIMINARY.getCode());
		eventTypeList.add(ImportanceEventType.SECURITIES_UNIFICATION.getCode());
		eventTypeList.add(ImportanceEventType.SECURITIES_FUSION.getCode());
		eventTypeList.add(ImportanceEventType.SECURITIES_EXCISION.getCode());
		operationTO.setEventTypeList(eventTypeList);
		operationTO.setStateList(stateList);
		return coporateEventComponentServiceBean.searchOperationForDematerializationCertificateValidation(operationTO);	
 		
	}

	
	
	/**
	 * Test payment.
	 *
	 * @param security the security
	 * @throws ServiceException the service exception
	 */
	public void testPayment(Security security)throws ServiceException{
		if(security.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode())){
			  CorporativeOperation corporativeOperation;
			  Issuer issuer;
	
		InterestPaymentSchedule interestPaymentSchedule = security.getInterestPaymentSchedule();
		  List<ProgramInterestCoupon> programInterestCouponList = interestPaymentSchedule.getProgramInterestCoupons();
		  for(ProgramInterestCoupon coupon: programInterestCouponList){
			  corporativeOperation = new CorporativeOperation();
 
			  //corporativeOperation.setCorporativeEventType(ImportanceEventType.INTEREST_PAYMENT.getCode());
			  corporativeOperation.setIssuer(security.getIssuer());
			  corporativeOperation.setSecurities(coupon.getInterestPaymentCronogram().getSecurity());
			  //corporativeOperation.setIdScheduleCouponFk(coupon.getIdProgramInterestPk());
 			  
			  
		  }
		}
		
	}
	
	/**
	 * Find corporative operation by filter.
	 *
	 * @param filter the filter
	 * @return the corporative operation
	 * @throws ServiceException the service exception
	 */
	public CorporativeOperation findCorporativeOperationByFilter(CorporativeOperationTO filter)throws ServiceException{
		CorporativeOperation operation = coporateEventComponentServiceBean.findCorporativeOperationByFilter(filter);

		if(operation!=null){
			if(operation.getCapitalIncreaseMotives()!=null){
				operation.getCapitalIncreaseMotives().size();
			}
		}

		return operation;
	}
	
	/**
	 * Gets the earned income.
	 *
	 * @param idHolderAccountPk the id holder account pk
	 * @param idIsinCodePk the id isin code pk
	 * @param cutoffDate the cutoff date
	 * @return the earned income
	 */
	public BigDecimal getEarnedIncome(Long idHolderAccountPk,String idIsinCodePk, Date cutoffDate ){
		BigDecimal earnedIncome = BigDecimal.ZERO;
		BigDecimal periodTime 	= BigDecimal.ZERO;
		
		//Getting last coupon paid
		Date lastCouponPaidDate = new Date();
		BigDecimal lastCouponPaidInterest = BigDecimal.ZERO;
		//Calling last corporative paid
		Long idCorpOperation 	= coporateEventComponentServiceBean.getLastCouponPaid(idIsinCodePk);
		
		
		//Finding security
		Security security = coporateEventComponentServiceBean.find(Security.class, idIsinCodePk);
		
		if(Validations.validateIsNotNull(idCorpOperation)){
			//Calling last corportive date and interest baseCalcule.multiply(periodTime)
			Object [] lastCouponPaidObject 	= coporateEventComponentServiceBean.getLastCouponPaidDateAndInterest(idCorpOperation);
			lastCouponPaidDate = (Date)lastCouponPaidObject[0];
			lastCouponPaidInterest = (BigDecimal)lastCouponPaidObject[1];
		}else{
			lastCouponPaidDate = security.getIssuanceDate();
			lastCouponPaidInterest = security.getInterestRate();
		}
		//
		BigDecimal custodyBalance = coporateEventComponentServiceBean.getCustodyBalance(idHolderAccountPk, idIsinCodePk);
		BigDecimal custodyAmount = custodyBalance.multiply(security.getCurrentNominalValue());
		BigDecimal calculatedPaidInterest = lastCouponPaidInterest.divide(new BigDecimal(100),3,RoundingMode.CEILING);

		Integer monthType 	= security.getCalendarMonth();
		Integer dayType 	= security.getCalendarDays();
		BigDecimal baseCalcule = getCalendarType(dayType);
		
		periodTime = getPeriodTime(lastCouponPaidDate, cutoffDate, monthType, dayType);
		
		earnedIncome = (custodyAmount.multiply(calculatedPaidInterest)).divide(baseCalcule,2, RoundingMode.HALF_DOWN).multiply(periodTime);
		return earnedIncome;
	}

	/**
	 * Gets the calendar type.
	 *
	 * @param dayType the day type
	 * @return the calendar type
	 */
	public BigDecimal getCalendarType(Integer dayType){
		BigDecimal calendarType = BigDecimal.ZERO;
		if(CalendarDayType._360.getCode().equals(dayType)){
			calendarType = new BigDecimal(CalendarDayType._360.getIntegerValue());
		}else if(CalendarDayType._365.getCode().equals(dayType)){
			calendarType = new BigDecimal(CalendarDayType._365.getIntegerValue());
		}else if(CalendarDayType.ACTUAL.getCode().equals(dayType)){
			if(validateLeapYear()){
				calendarType = new BigDecimal(366);
			}else{
				calendarType = new BigDecimal(CalendarDayType._365.getIntegerValue());
			}
		}
		return calendarType;
	}
	
	/**
	 * Gets the period time.
	 *
	 * @param lastCouponPaid the last coupon paid
	 * @param cutoffDate the cutoff date
	 * @param monthType the month type
	 * @param dayType the day type
	 * @return the period time
	 */
	public BigDecimal getPeriodTime(Date lastCouponPaid, Date cutoffDate, Integer monthType, Integer dayType ){
		BigDecimal periodTime = BigDecimal.ZERO;
		Integer days30 = 30;
		Integer month12 = 12;
		Integer year360 = 360;
		
		Long MILLSECS_PER_DAY = (long) (24 * 60 * 60 * 1000);
		
		if(CalendarMonthType._30.getCode().equals(monthType)){

			Calendar cal = Calendar.getInstance();
			cal.setTime(lastCouponPaid);
			Integer yearToday = cal.get(Calendar.YEAR);
			Integer monthToday = cal.get(Calendar.MONTH);
			Integer dayToday = cal.get(Calendar.DATE);

			cal = Calendar.getInstance();
			cal.setTime(cutoffDate);
			Integer yearMovement = cal.get(Calendar.YEAR);
			Integer monthMovement = cal.get(Calendar.MONTH);
			Integer dayMovement = cal.get(Calendar.DATE);

			if(yearToday.equals(yearMovement)){
				if(monthToday.equals(monthMovement)){
					if(dayToday.equals(dayMovement)){
						periodTime = BigDecimal.ONE;
					}else{
						periodTime = new BigDecimal(dayMovement - dayToday + 1);
					}
				}else{
					if(dayToday > dayMovement){
						Integer totalMonths = monthToday - monthMovement;
						periodTime = new BigDecimal(totalMonths * days30);
						periodTime = periodTime.add(new BigDecimal(dayToday - dayMovement + 1));
					}else{
						Integer totalMonths = monthToday - monthMovement - 1;
						periodTime = new BigDecimal(totalMonths * days30);
						periodTime = periodTime.add(new BigDecimal(days30 - dayMovement + dayToday + 1)); 
					}
				}
			}else{
				if(monthToday.equals(monthMovement)){
					if(dayToday.equals(dayMovement)){
						periodTime = new BigDecimal(year360);
					}else if(dayMovement > dayToday){
						periodTime = new BigDecimal(11 * days30);
						periodTime = periodTime.add(new BigDecimal(days30 - dayMovement + dayToday + 1));
					}
				}else if(monthMovement > monthToday){
					if(dayToday.equals(dayMovement)){
						Integer totalMonths = month12 - monthMovement + monthToday;
						periodTime = new BigDecimal(days30 * totalMonths);
					}else {
						if(dayToday > dayMovement){
							Integer totalMonths = month12 - monthMovement + monthToday;
							periodTime = new BigDecimal(days30 * totalMonths);
							periodTime = periodTime.add(new BigDecimal(dayToday - dayMovement + 1));
						}else{
							Integer totalMonths = month12 - monthMovement + monthToday - 1;
							periodTime = new BigDecimal(days30 * totalMonths);
							periodTime = periodTime.add(new BigDecimal(days30 - dayMovement + dayToday + 1));
						}
					}
				}
			}

		}else if(CalendarMonthType.ACTUAL.getCode().equals(monthType)){
			Long difference = (cutoffDate.getTime() - lastCouponPaid.getTime())/MILLSECS_PER_DAY;
			if(difference.equals(new Long(0))){
				periodTime = new BigDecimal(1);
			}else{
				periodTime = new BigDecimal(difference);
			}
		}
		
		return periodTime;
	}
	
	/**
	 * Validate leap year.
	 *
	 * @return true, if successful
	 */
	private boolean validateLeapYear(){
		Integer currentYear = CommonsUtilities.currentYear();
		if((currentYear % 4 == 0) && ((currentYear % 100 != 0) || (currentYear % 400 == 0))){
			return true;
		}else{
			return false;
		}
	}
}
