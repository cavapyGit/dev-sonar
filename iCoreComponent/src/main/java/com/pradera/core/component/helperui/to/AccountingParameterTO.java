package com.pradera.core.component.helperui.to;

import java.io.Serializable;


public class AccountingParameterTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3632201120647874452L;
	
	
	
	private Integer 	idAccountingParameterPk;
	private String 	parameterName;
	private String 	parameterNameConsult;
	private String 	description;
	private Integer parameterType;
	private Integer correlativeCode;
	private Integer status;
	
	/****/
	private Integer index;
	private String 	reference; 
	
	private String descriptionParameterType;
	private String descriptionParameterStatus;
	private String 	registerDate;
	private String	modifyDate;
	private String 	registerUser;
	private String 	modifyUser;
	 
 
 
	
	
	
	/**
	 * 
	 */
	public AccountingParameterTO() {
	}
	/**
	 * @param description
	 */
	public AccountingParameterTO(String description) {
		this.description = description;
	}
	
	
	
	/**
	 * @param idAccountingParameterPk
	 * @param parameterName
	 * @param index
	 */
	public AccountingParameterTO(Integer idAccountingParameterPk,
			String parameterName, Integer index) {
		this.idAccountingParameterPk = idAccountingParameterPk;
		this.parameterName = parameterName;
		this.index = index;
	}
	public String getDescription() {
		return description;
	}
	public String getParameterName() {
		return parameterName;
	}
	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getCorrelativeCode() {
		return correlativeCode;
	}
	public void setCorrelativeCode(Integer correlativeCode) {
		this.correlativeCode = correlativeCode;
	}
	public String getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(String registerDate) {
		this.registerDate = registerDate;
	}
	public String getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(String modifyDate) {
		this.modifyDate = modifyDate;
	}
	public String getRegisterUser() {
		return registerUser;
	}
	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}
	public String getModifyUser() {
		return modifyUser;
	}
	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}
	public Integer getParameterType() {
		return parameterType;
	}
	public void setParameterType(Integer parameterType) {
		this.parameterType = parameterType;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getIdAccountingParameterPk() {
		return idAccountingParameterPk;
	}
	public void setIdAccountingParameterPk(Integer idAccountingParameterPk) {
		this.idAccountingParameterPk = idAccountingParameterPk;
	}
	public String getParameterNameConsult() {
		return parameterNameConsult;
	}
	public void setParameterNameConsult(String parameterNameConsult) {
		this.parameterNameConsult = parameterNameConsult;
	}
	/**
	 * @return the index
	 */
	public Integer getIndex() {
		return index;
	}
	/**
	 * @param index the index to set
	 */
	public void setIndex(Integer index) {
		this.index = index;
	}
	/**
	 * @return the reference
	 */
	public String getReference() {
		return reference;
	}
	/**
	 * @param reference the reference to set
	 */
	public void setReference(String reference) {
		this.reference = reference;
	}
	/**
	 * @return the descriptionParameterType
	 */
	public String getDescriptionParameterType() {
		return descriptionParameterType;
	}
	/**
	 * @param descriptionParameterType the descriptionParameterType to set
	 */
	public void setDescriptionParameterType(String descriptionParameterType) {
		this.descriptionParameterType = descriptionParameterType;
	}
	/**
	 * @return the descriptionParameterStatus
	 */
	public String getDescriptionParameterStatus() {
		return descriptionParameterStatus;
	}
	/**
	 * @param descriptionParameterStatus the descriptionParameterStatus to set
	 */
	public void setDescriptionParameterStatus(String descriptionParameterStatus) {
		this.descriptionParameterStatus = descriptionParameterStatus;
	}
		
	

}
