package com.pradera.core.component.helperui.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.helperui.to.IssuerSearchTO;
import com.pradera.core.component.helperui.to.IssuerSearcherTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.issuancesecuritie.Issuer;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class IssuerQueryServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 06/03/2013
 */
@Stateless
public class IssuerQueryServiceBean extends CrudDaoServiceBean{
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	@Inject @Configurable
	Integer maxResultsQueryHelper;

	/**
	 * Instantiates a new issuer query service bean.
	 */
	public IssuerQueryServiceBean() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the issuer by code.
	 *
	 * @param issuerSearcher the issuer searcher
	 * @return the issuer by code
	 */
	public Issuer getIssuerByCode(IssuerSearcherTO issuerSearcher) throws ServiceException {
		
		Issuer iss = null;
		try {
			
			StringBuilder sb = new StringBuilder();
			sb.append("select i from Issuer i join fetch i.geographicLocation g  ");
			sb.append(" where 1=1 ");
			//Default state to confirmed		
			if (null != issuerSearcher.getState()) {
				//sb.append(" and i.state = :state ");
			}
			if (null != issuerSearcher.getIssuerCode()) {
				sb.append(" and i.idIssuerPk = :issuerCode ");
			}
			if (null != issuerSearcher.getMnemonic()) {
				sb.append(" and i.mnemonic = :nemonico ");
			}
			
			TypedQuery<Issuer> query = em.createQuery(sb.toString(), Issuer.class);
			
			if (StringUtils.isNotBlank(issuerSearcher.getIssuerCode())) {
				query.setParameter("issuerCode", issuerSearcher.getIssuerCode());
			}
			if (StringUtils.isNotBlank(issuerSearcher.getMnemonic())) {
				query.setParameter("nemonico", issuerSearcher.getMnemonic());
			}
			if (null != issuerSearcher.getState()) {
				//query.setParameter("state", issuerSearcher.getState());
			}
			iss =  query.getSingleResult();
			
		}catch(NoResultException nex) {
			log.info("Issuer not found");
			iss = null;
		}
		
		return iss;
	}
	
	/**
	 * Gets the issuer by helper.
	 *
	 * @param issuerSearcher the issuer searcher
	 * @return the issuer by helper
	 */
	public List<Object[]> getIssuerByHelper(IssuerSearcherTO issuerSearcher) {
		StringBuilder sb = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sb.append("select i.idIssuerPk, i.businessName , ");  // 0 //1 
		sb.append(" i.stateIssuer, (select p.parameterName from ParameterTable p where p.parameterTablePk = i.stateIssuer) , "); // 2 3 
		sb.append(" i.documentType ,(select p.indicator1 from ParameterTable p where p.parameterTablePk = i.documentType) , "); // 4 5 
		sb.append(" i.documentNumber, i.mnemonic, i.businessName, "); // 6 7 8 
		sb.append(" i.economicActivity , (select p.parameterName from ParameterTable p where p.parameterTablePk = i.economicActivity) , "); // 9 10 
		sb.append(" i.economicSector , (select p.parameterName from ParameterTable p where p.parameterTablePk = i.economicSector) "); // 11 12
		sb.append(" from Issuer i  ");
		sb.append(" where 1 = 1 ");
		
		if (StringUtils.isNotBlank(issuerSearcher.getIssuerCode())){
			sb.append(" and i.idIssuerPk like :issuerCode ");
			parameters.put("issuerCode", "%"+issuerSearcher.getIssuerCode()+"%");
		}
		if (StringUtils.isNotBlank(issuerSearcher.getIssuerDesc())){
			sb.append(" and i.businessName like :name ");
			parameters.put("name", "%"+issuerSearcher.getIssuerDesc()+"%");
		}
		if (Validations.validateIsNotNullAndPositive((issuerSearcher.getDocumentType()))){
			sb.append(" and i.documentType like :docType ");
			parameters.put("docType", issuerSearcher.getDocumentType());
		}
		if (StringUtils.isNotBlank(issuerSearcher.getDocumentNumber())){
			sb.append(" and i.documentNumber like :docNumber ");
			parameters.put("docNumber", issuerSearcher.getDocumentNumber());
		}
		if (StringUtils.isNotBlank(issuerSearcher.getMnemonic())){
			sb.append(" and i.mnemonic like :mnemonic ");
			parameters.put("mnemonic","%"+issuerSearcher.getMnemonic()+"%");
		}
		if (Validations.validateIsNotNullAndPositive((issuerSearcher.getEconomicSector()))){
			sb.append(" and i.economicSector like :economicSector ");
			parameters.put("economicSector", issuerSearcher.getEconomicSector());
		}
		if (Validations.validateIsNotNullAndPositive(issuerSearcher.getState())){
			sb.append(" and i.stateIssuer = :state ");
			parameters.put("state", issuerSearcher.getState());
		}
		
		sb.append(" order by i.businessName, i.idIssuerPk  ");
		
		return findListByQueryStringMaxResults(sb.toString(),parameters);
		
	}

	public Issuer findIssuer(String idIssuerPk, String mnemonic) {
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT ir FROM Issuer ir");
			sbQuery.append("	WHERE 1 = 1 ");
			if(idIssuerPk!=null){
				sbQuery.append(" and ir.idIssuerPk = :idIssuerPk ");
				parameters.put("idIssuerPk",idIssuerPk);
			}
			if(mnemonic!=null){
				sbQuery.append(" and ir.mnemonic = :mnemonic ");
				parameters.put("mnemonic",mnemonic);
			}
			
			return (Issuer)findObjectByQueryString(sbQuery.toString(), parameters);
		} catch (NoResultException e) {
			log.error("no se encontro emisor con el mnemonico: "+idIssuerPk);
			return null;
		} catch (NonUniqueResultException e) {
			log.error("multiples emisores con el mnemonico: "+idIssuerPk);
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Issuer> findIssuers(IssuerSearchTO issuerSearchTO) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ir.idIssuerPk,");
		sbQuery.append("			ir.businessName,");
		sbQuery.append("			ir.documentType,");
		sbQuery.append("			ir.documentNumber,");
		sbQuery.append("			ir.mnemonic");
		sbQuery.append("	FROM Issuer ir ");
		sbQuery.append(" Where 1 = 1 ");
		
		if(Validations.validateIsNotNull(issuerSearchTO.getState())){
			sbQuery.append("	And ir.stateIssuer = :stateIssuer");
			param.put("stateIssuer", issuerSearchTO.getState());
		}
		if(Validations.validateIsNotNull(issuerSearchTO.getEconomicSector())){
			sbQuery.append("	AND ir.economicSector = :economicSector");
			param.put("economicSector", issuerSearchTO.getEconomicSector());
		}
		if(Validations.validateIsNotNullAndNotEmpty(issuerSearchTO.getDocumentType())){
			sbQuery.append("	AND ir.documentType = :documentType");
			param.put("documentType", issuerSearchTO.getDocumentType());
		}
		if(Validations.validateIsNotNullAndNotEmpty(issuerSearchTO.getDocumentNumber())){
			sbQuery.append("	AND ir.documentNumber = :documentNumber");
			param.put("documentNumber", issuerSearchTO.getDocumentNumber());
		}
		if(Validations.validateIsNotNullAndNotEmpty(issuerSearchTO.getFullName())){
			sbQuery.append("	AND ir.businessName like :description");
			param.put("description", GeneralConstants.PERCENTAGE_STRING +issuerSearchTO.getFullName() +GeneralConstants.PERCENTAGE_STRING);
		}
		if(Validations.validateIsNotNullAndNotEmpty(issuerSearchTO.getMnemonic())){
			sbQuery.append("	AND ir.mnemonic like :mnemonic");
			param.put("mnemonic", GeneralConstants.PERCENTAGE_STRING + issuerSearchTO.getMnemonic() + GeneralConstants.PERCENTAGE_STRING );
		}
		if(Validations.validateIsNotNullAndNotEmpty(issuerSearchTO.getIdIssuerCode())){
			sbQuery.append("	AND ir.idIssuerPk = :issuerPk");
			param.put("issuerPk", issuerSearchTO.getIdIssuerCode());
		}
		if((Validations.validateIsNotNull(issuerSearchTO.getOrderBy()))){
			sbQuery.append("	ORDER BY ir.").append(issuerSearchTO.getOrderBy());
		} else {
			sbQuery.append("	ORDER BY ir.idIssuerPk DESC");
		}
		List<Object[]> lstResult = null;
		//check when limit max results
		if(!issuerSearchTO.isLimitMaxResult()){
			lstResult = (List<Object[]>)findListByQueryString(sbQuery.toString(),param);
		} else {
			lstResult = findListByQueryStringMaxResults(sbQuery.toString(), param);
		}
		List<Issuer> lstIssuer = null;
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
			lstIssuer = new ArrayList<Issuer>();
			for(Object[] object:lstResult){
				Issuer issuer = new Issuer();
				issuer.setIdIssuerPk(object[0].toString());
				issuer.setBusinessName(object[1].toString());
				issuer.setDocumentType((Integer)object[2]);
				issuer.setDocumentNumber(object[3].toString());
				issuer.setMnemonic(object[4].toString());
				lstIssuer.add(issuer);
			}
		}
		return lstIssuer;
	}

	public List<Object[]> getIssuerByHelperMnemonic(IssuerSearcherTO issuerSearcher) {
		StringBuilder sb = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sb.append("select i.idIssuerPk, i.businessName , ");  // 0 //1 
		sb.append(" i.stateIssuer, (select p.parameterName from ParameterTable p where p.parameterTablePk = i.stateIssuer) , "); // 2 3 
		sb.append(" i.documentType ,(select p.indicator1 from ParameterTable p where p.parameterTablePk = i.documentType) , "); // 4 5 
		sb.append(" i.documentNumber, i.mnemonic, i.businessName, "); // 6 7 8 
		sb.append(" i.economicActivity , (select p.parameterName from ParameterTable p where p.parameterTablePk = i.economicActivity) , "); // 9 10 
		sb.append(" i.economicSector , (select p.parameterName from ParameterTable p where p.parameterTablePk = i.economicSector) "); // 11 12
		sb.append(" from Issuer i  ");
		sb.append(" where 1 = 1 ");
		
		if (StringUtils.isNotBlank(issuerSearcher.getIssuerCode())){
			sb.append(" and i.idIssuerPk like :issuerCode ");
			parameters.put("issuerCode", "%"+issuerSearcher.getIssuerCode()+"%");
		}
		if (StringUtils.isNotBlank(issuerSearcher.getIssuerDesc())){
			sb.append(" and i.businessName like :name ");
			parameters.put("name", "%"+issuerSearcher.getIssuerDesc()+"%");
		}
		if (Validations.validateIsNotNullAndPositive((issuerSearcher.getDocumentType()))){
			sb.append(" and i.documentType like :docType ");
			parameters.put("docType", issuerSearcher.getDocumentType());
		}
		if (StringUtils.isNotBlank(issuerSearcher.getDocumentNumber())){
			sb.append(" and i.documentNumber like :docNumber ");
			parameters.put("docNumber", issuerSearcher.getDocumentNumber());
		}
		if (StringUtils.isNotBlank(issuerSearcher.getMnemonic())){
			sb.append(" and i.mnemonic like :mnemonic ");
			parameters.put("mnemonic","%"+issuerSearcher.getMnemonic()+"%");
		}
		if (Validations.validateIsNotNullAndPositive((issuerSearcher.getEconomicSector()))){
			sb.append(" and i.economicSector like :economicSector ");
			parameters.put("economicSector", issuerSearcher.getEconomicSector());
		}
		if (Validations.validateIsNotNullAndPositive(issuerSearcher.getState())){
			sb.append(" and i.stateIssuer = :state ");
			parameters.put("state", issuerSearcher.getState());
		}
		
		sb.append(" order by i.mnemonic  ");
		
		return findListByQueryString(sb.toString(),parameters);
		
	}
	
}

