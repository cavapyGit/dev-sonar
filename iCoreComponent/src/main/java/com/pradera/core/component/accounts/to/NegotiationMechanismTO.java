package com.pradera.core.component.accounts.to;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class NegotiationMechanismTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 06/02/2013
 */
public class NegotiationMechanismTO implements Serializable{

	/** The negotiation mechanism state. */
	private Integer negotiationMechanismState;
	
	/** The mechanism modality state. */
	private Integer mechanismModalityState;
	
	/**
	 * Instantiates a new negotiation mechanism to.
	 */
	public NegotiationMechanismTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the negotiation mechanism state.
	 *
	 * @return the negotiation mechanism state
	 */
	public Integer getNegotiationMechanismState() {
		return negotiationMechanismState;
	}

	/**
	 * Sets the negotiation mechanism state.
	 *
	 * @param negotiationMechanismState the new negotiation mechanism state
	 */
	public void setNegotiationMechanismState(Integer negotiationMechanismState) {
		this.negotiationMechanismState = negotiationMechanismState;
	}

	/**
	 * Gets the mechanism modality state.
	 *
	 * @return the mechanism modality state
	 */
	public Integer getMechanismModalityState() {
		return mechanismModalityState;
	}

	/**
	 * Sets the mechanism modality state.
	 *
	 * @param mechanismModalityState the new mechanism modality state
	 */
	public void setMechanismModalityState(Integer mechanismModalityState) {
		this.mechanismModalityState = mechanismModalityState;
	}
	
}
