package com.pradera.core.component.accounts.to;



import java.io.Serializable;
import java.util.Date;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class HolderAccountTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/02/2013
 */
public class HolderAccountTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The participant to. */
	private Long participantTO;
	
	/** The id isin code pk. */
	private String idIsinCodePk;
	
	/** The request number. */
	private Long requestNumber;
	
	/** The holder account number. */
	private Integer holderAccountNumber;
	
	/** The alternate code. */
	private String alternateCode;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/** The status. */
	private Integer status;
	
	/** The holder account request type. */
	private Integer holderAccountRequestType;
	
	/** The id holder account request pk. */
	private Long idHolderAccountRequestPk;
	
	/** The id holder account pk. */
	private Long idHolderAccountPk;
	
	/** The requests. */
	private Integer requests;
	
	/** The id holder pk. */
	private Long idHolderPk;
	
	/** The lst holder pk. */
	private List<Long> lstHolderPk;
	
	/** The holder account type. */
	private Integer holderAccountType;
	
	/** The holderaccount group type. */
	private Integer holderaccountGroupType;
	
	/** The id issuer pk. */
	private String idIssuerPk;
	
	/** The list participant. */
	private List<Integer> participantStates;		
	
	/** The related name*/
	private String relatedName;
	
	/** The holder principal */
	private String holderPrincipal;
	
	/** 
	 * The need holder. this attribute is useful to get HolderaccountDetail with Holder Or Not 
	 * */
	private boolean needHolder = Boolean.FALSE;
	
	/** 
	 * The need participant.. this attribute is useful to get HolderAccount with Participant like Object. By default is true because 
	 * is very important in mayority of cases. 
	 * */
	private boolean needParticipant = Boolean.TRUE;
	
	/** The need banks. 
	 * 	this attribute is useful to get HolderaccountBanks with Banks Or Not
	 * */
	private boolean needBanks = Boolean.TRUE;
		
	/** The need balances. */
	private boolean needBalances = Boolean.FALSE;
	
	/** The need files. */
	private boolean needFiles = Boolean.FALSE;
	
	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}
	
	/**
	 * Gets the request number.
	 *
	 * @return the request number
	 */
	public Long getRequestNumber() {
		return requestNumber;
	}

	/**
	 * Gets the holder account number.
	 *
	 * @return the holder account number
	 */
	public Integer getHolderAccountNumber() {
		return holderAccountNumber;
	}

	/**
	 * Gets the id isin code pk.
	 *
	 * @return the id isin code pk
	 */
	public String getIdIsinCodePk() {
		return idIsinCodePk;
	}

	/**
	 * Sets the id isin code pk.
	 *
	 * @param idIsinCodePk the new id isin code pk
	 */
	public void setIdIsinCodePk(String idIsinCodePk) {
		this.idIsinCodePk = idIsinCodePk;
	}

	/**
	 * Gets the id holder account request pk.
	 *
	 * @return the id holder account request pk
	 */
	public Long getIdHolderAccountRequestPk() {
		return idHolderAccountRequestPk;
	}

	/**
	 * Sets the id holder account request pk.
	 *
	 * @param idHolderAccountRequestPk the new id holder account request pk
	 */
	public void setIdHolderAccountRequestPk(Long idHolderAccountRequestPk) {
		this.idHolderAccountRequestPk = idHolderAccountRequestPk;
	}

	/**
	 * Gets the id holder account pk.
	 *
	 * @return the id holder account pk
	 */
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}

	/**
	 * Sets the id holder account pk.
	 *
	 * @param idHolderAccountPk the new id holder account pk
	 */
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	
	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}
	
	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}
	
	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	/**
	 * Instantiates a new holder account to.
	 */
	public HolderAccountTO() {
		super();
	}
	
	/**
	 * Gets the participant to.
	 *
	 * @return the participant to
	 */
	public Long getParticipantTO() {
		return participantTO;
	}

	/**
	 * Sets the participant to.
	 *
	 * @param participantTO the new participant to
	 */
	public void setParticipantTO(Long participantTO) {
		this.participantTO = participantTO;
	}

	/**
	 * Sets the request number.
	 *
	 * @param requestNumber the new request number
	 */
	public void setRequestNumber(Long requestNumber) {
		this.requestNumber = requestNumber;
	}

	/**
	 * Gets the alternate code.
	 *
	 * @return the alternate code
	 */
	public String getAlternateCode() {
		return alternateCode;
	}

	/**
	 * Sets the alternate code.
	 *
	 * @param alternateCode the new alternate code
	 */
	public void setAlternateCode(String alternateCode) {
		this.alternateCode = alternateCode;
	}

	/**
	 * Sets the holder account number.
	 *
	 * @param holderAccountNumber the new holder account number
	 */
	public void setHolderAccountNumber(Integer holderAccountNumber) {
		this.holderAccountNumber = holderAccountNumber;
	}

	/**
	 * Gets the holder account request type.
	 *
	 * @return the holder account request type
	 */
	public Integer getHolderAccountRequestType() {
		return holderAccountRequestType;
	}

	/**
	 * Sets the holder account request type.
	 *
	 * @param holderAccountRequestType the new holder account request type
	 */
	public void setHolderAccountRequestType(Integer holderAccountRequestType) {
		this.holderAccountRequestType = holderAccountRequestType;
	}

	/**
	 * Checks if is need holder.
	 *
	 * @return true, if is need holder
	 */
	public boolean isNeedHolder() {
		return needHolder;
	}

	/**
	 * Sets the need holder.
	 *
	 * @param needHolder the new need holder
	 */
	public void setNeedHolder(boolean needHolder) {
		this.needHolder = needHolder;
	}

	/**
	 * Checks if is need participant.
	 *
	 * @return true, if is need participant
	 */
	public boolean isNeedParticipant() {
		return needParticipant;
	}

	/**
	 * Sets the need participant.
	 *
	 * @param needParticipant the new need participant
	 */
	public void setNeedParticipant(boolean needParticipant) {
		this.needParticipant = needParticipant;
	}

	/**
	 * Gets the requests.
	 *
	 * @return the requests
	 */
	public Integer getRequests() {
		return requests;
	}

	/**
	 * Sets the requests.
	 *
	 * @param requests the new requests
	 */
	public void setRequests(Integer requests) {
		this.requests = requests;
	}

	/**
	 * Checks if is need banks.
	 *
	 * @return true, if is need banks
	 */
	public boolean isNeedBanks() {
		return needBanks;
	}

	/**
	 * Sets the need banks.
	 *
	 * @param needBanks the new need banks
	 */
	public void setNeedBanks(boolean needBanks) {
		this.needBanks = needBanks;
	}

	/**
	 * Gets the id holder pk.
	 *
	 * @return the id holder pk
	 */
	public Long getIdHolderPk() {
		return idHolderPk;
	}

	/**
	 * Sets the id holder pk.
	 *
	 * @param idHolderPk the new id holder pk
	 */
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}

	/**
	 * Gets the holder account type.
	 *
	 * @return the holder account type
	 */
	public Integer getHolderAccountType() {
		return holderAccountType;
	}

	/**
	 * Sets the holder account type.
	 *
	 * @param holderAccountType the new holder account type
	 */
	public void setHolderAccountType(Integer holderAccountType) {
		this.holderAccountType = holderAccountType;
	}

	/**
	 * Gets the holderaccount group type.
	 *
	 * @return the holderaccount group type
	 */
	public Integer getHolderaccountGroupType() {
		return holderaccountGroupType;
	}

	/**
	 * Sets the holderaccount group type.
	 *
	 * @param holderaccountGroupType the new holderaccount group type
	 */
	public void setHolderaccountGroupType(Integer holderaccountGroupType) {
		this.holderaccountGroupType = holderaccountGroupType;
	}

	/**
	 * Checks if is need balances.
	 *
	 * @return true, if is need balances
	 */
	public boolean isNeedBalances() {
		return needBalances;
	}

	/**
	 * Sets the need balances.
	 *
	 * @param needBalances the new need balances
	 */
	public void setNeedBalances(boolean needBalances) {
		this.needBalances = needBalances;
	}

	/**
	 * Checks if is need files.
	 *
	 * @return true, if is need files
	 */
	public boolean isNeedFiles() {
		return needFiles;
	}

	/**
	 * Sets the need files.
	 *
	 * @param needFiles the new need files
	 */
	public void setNeedFiles(boolean needFiles) {
		this.needFiles = needFiles;
	}

	/**
	 * Gets the id issuer pk.
	 *
	 * @return the id issuer pk
	 */
	public String getIdIssuerPk() {
		return idIssuerPk;
	}

	/**
	 * Sets the id issuer pk.
	 *
	 * @param idIssuerPk the new id issuer pk
	 */
	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}

	/**
	 * Gets the participant states.
	 *
	 * @return the participant states
	 */
	public List<Integer> getParticipantStates() {
		return participantStates;
	}

	/**
	 * Sets the participant states.
	 *
	 * @param participantStates the new participant states
	 */
	public void setParticipantStates(List<Integer> participantStates) {
		this.participantStates = participantStates;
	}		

	/**
	 * Gets the related name.
	 *
	 * @return the related name
	 */
	public String getRelatedName() {
		return relatedName;
	}

	/**
	 * Sets the related name.
	 *
	 * @param relatedName the new related name
	 */
	public void setRelatedName(String relatedName) {
		this.relatedName = relatedName;
	}

	/**
	 * Gets the holder Principal.
	 *
	 * @return the holder Principal
	 */
	public String getHolderPrincipal() {
		return holderPrincipal;
	}

	/**
	 * Sets the holder Principal.
	 *
	 * @param holderPrincipal the new holder Principal
	 */
	public void setHolderPrincipal(String holderPrincipal) {
		this.holderPrincipal = holderPrincipal;
	}

	/**
	 * Gets the lst holder pk.
	 *
	 * @return the lst holder pk
	 */
	public List<Long> getLstHolderPk() {
		return lstHolderPk;
	}

	/**
	 * Sets the lst holder pk.
	 *
	 * @param lstHolderPk the new lst holder pk
	 */
	public void setLstHolderPk(List<Long> lstHolderPk) {
		this.lstHolderPk = lstHolderPk;
	}
	
}