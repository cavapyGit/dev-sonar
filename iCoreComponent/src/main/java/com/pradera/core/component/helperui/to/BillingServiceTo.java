package com.pradera.core.component.helperui.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.pradera.integration.common.validation.Validations;
import com.pradera.model.billing.BillingService;

public class BillingServiceTo implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long idBillingServicePk;
	private Integer serviceType;
	private Integer entityCollection;
	private Integer stateService;
	private Integer baseCollection;
	private Integer calculationPeriod;
	private String serviceCode;
	private String serviceName;
	private Date endEffectiveDate;
	private Date initialEffectiveDate;
	
	private List<Integer> listCodeServiceRate;	
	private String descriptionCollectionEntity;	
	private String descriptionBaseCollection;	
	private String descriptionStateBillingService;	
	private String descriptionServiceType;	
	private String descriptionCollectionPeriod;
	private String descriptionClientServiceType;	
	private String descriptionCalculationPeriod;
	private String descriptionNameCollectionEntity;
	
	private Integer collectionPeriod;
	private Integer clientServiceType; 
	private Integer currencyBilling;
	private Integer inIntegratedBill;
	
	private Date 	dateCalculationPeriod;
	private Boolean calculateDaily = Boolean.FALSE;
	private Boolean calculateMonthly= Boolean.FALSE;
	private Integer taxApplied;
	private String descriptionCurrency;
	
	private Integer currencyCalculation;
	private Integer sourceInformation;
	
	private String 	referenceRate;
	
	private List<String>	listReferenceRate;
	
	private List<String> 	listServiceCode;
	
	
	public BillingServiceTo(){
		this.listCodeServiceRate= new ArrayList<Integer>();
		
	}
	
	

	public BillingServiceTo(BillingService billingService){
		this.initialEffectiveDate=billingService.getInitialEffectiveDate();
		this.endEffectiveDate=billingService.getEndEffectiveDate();		
		this.idBillingServicePk=billingService.getIdBillingServicePk();
		this.serviceCode=billingService.getServiceCode();
		this.serviceType=billingService.getServiceType();
		this.serviceName=billingService.getServiceName();
		this.descriptionStateBillingService= billingService.getDescriptionStateBillingService();
		this.baseCollection=billingService.getBaseCollection();
		this.entityCollection=billingService.getEntityCollection();
		this.listCodeServiceRate= new ArrayList<Integer>(); /** setting new rate service codes */
		this.currencyBilling=billingService.getCurrencyBilling();
		this.currencyCalculation=billingService.getCurrencyCalculation();
		this.sourceInformation=billingService.getSourceInformation();
		this.referenceRate=billingService.getReferenceRate();
	}
	
	
	
	public String getDescriptionCalculationPeriod() {
		return descriptionCalculationPeriod;
	}



	public void setDescriptionCalculationPeriod(String descriptionCalculationPeriod) {
		this.descriptionCalculationPeriod = descriptionCalculationPeriod;
	}



	public Integer getCalculationPeriod() {
		return calculationPeriod;
	}



	public void setCalculationPeriod(Integer calculationPeriod) {
		this.calculationPeriod = calculationPeriod;
	}



	public Date getDateCalculationPeriod() {
		return dateCalculationPeriod;
	}



	public void setDateCalculationPeriod(Date dateCalculationPeriod) {
		this.dateCalculationPeriod = dateCalculationPeriod;
	}



	public Boolean getCalculateDaily() {
		return calculateDaily;
	}



	public void setCalculateDaily(Boolean calculateDaily) {
		this.calculateDaily = calculateDaily;
	}



	public Boolean getCalculateMonthly() {
		return calculateMonthly;
	}



	public void setCalculateMonthly(Boolean calculateMonthly) {
		this.calculateMonthly = calculateMonthly;
	}



	public Long getIdBillingServicePk() {
		return idBillingServicePk;
	}



	public void setIdBillingServicePk(Long idBillingServicePk) {
		this.idBillingServicePk = idBillingServicePk;
	}



	
	public List<Integer> getListCodeServiceRate() {
		return listCodeServiceRate;
	}



	public void setListCodeServiceRate(List<Integer> listCodeServiceRate) {
		this.listCodeServiceRate = listCodeServiceRate;
	}



	


	public Date getEndEffectiveDate() {
		return endEffectiveDate;
	}
	public void setEndEffectiveDate(Date endEffectiveDate) {
		this.endEffectiveDate = endEffectiveDate;
	}
	public Date getInitialEffectiveDate() {
		return initialEffectiveDate;
	}
	public void setInitialEffectiveDate(Date initialEffectiveDate) {
		this.initialEffectiveDate = initialEffectiveDate;
	}
	public Integer getBaseCollection() {
		return baseCollection;
	}
	public void setBaseCollection(Integer baseCollection) {
		this.baseCollection = baseCollection;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public Integer getServiceType() {
		return serviceType;
	}
	public void setServiceType(Integer serviceType) {
		this.serviceType = serviceType;
	}
	public Integer getEntityCollection() {
		return entityCollection;
	}
	public void setEntityCollection(Integer entityCollection) {
		this.entityCollection = entityCollection;
	}
	public Integer getStateService() {
		return stateService;
	}
	public void setStateService(Integer stateService) {
		this.stateService = stateService;
	}
			
	public String getDescriptionCollectionEntity() {
		return descriptionCollectionEntity;
	}





	/**
	 * @return the currencyBilling
	 */
	public Integer getCurrencyBilling() {
		return currencyBilling;
	}



	/**
	 * @param currencyBilling the currencyBilling to set
	 */
	public void setCurrencyBilling(Integer currencyBilling) {
		this.currencyBilling = currencyBilling;
	}



	/**
	 * @return the currencyCalculation
	 */
	public Integer getCurrencyCalculation() {
		return currencyCalculation;
	}



	/**
	 * @param currencyCalculation the currencyCalculation to set
	 */
	public void setCurrencyCalculation(Integer currencyCalculation) {
		this.currencyCalculation = currencyCalculation;
	}



	/**
	 * @return the sourceInformation
	 */
	public Integer getSourceInformation() {
		return sourceInformation;
	}



	/**
	 * @param sourceInformation the sourceInformation to set
	 */
	public void setSourceInformation(Integer sourceInformation) {
		this.sourceInformation = sourceInformation;
	}



	public void setDescriptionCollectionEntity(String descriptionCollectionEntity) {
		this.descriptionCollectionEntity = descriptionCollectionEntity;
	}



	public String getDescriptionBaseCollection() {
		return descriptionBaseCollection;
	}



	public void setDescriptionBaseCollection(String descriptionBaseCollection) {
		this.descriptionBaseCollection = descriptionBaseCollection;
	}



	public String getDescriptionStateBillingService() {
		return descriptionStateBillingService;
	}



	public void setDescriptionStateBillingService(
			String descriptionStateBillingService) {
		this.descriptionStateBillingService = descriptionStateBillingService;
	}



	public String getDescriptionServiceType() {
		return descriptionServiceType;
	}



	public void setDescriptionServiceType(String descriptionServiceType) {
		this.descriptionServiceType = descriptionServiceType;
	}



	public String getDescriptionCollectionPeriod() {
		return descriptionCollectionPeriod;
	}



	public void setDescriptionCollectionPeriod(String descriptionCollectionPeriod) {
		this.descriptionCollectionPeriod = descriptionCollectionPeriod;
	}



	public String getDescriptionClientServiceType() {
		return descriptionClientServiceType;
	}



	public void setDescriptionClientServiceType(String descriptionClientServiceType) {
		this.descriptionClientServiceType = descriptionClientServiceType;
	}
	public Integer getClientServiceType() {
		return clientServiceType;
	}



	public void setClientServiceType(Integer clientServiceType) {
		this.clientServiceType = clientServiceType;
	}

	
	public Integer getCollectionPeriod() {
		return collectionPeriod;
	}



	public void setCollectionPeriod(Integer collectionPeriod) {
		this.collectionPeriod = collectionPeriod;
	} 



	
	public void addNewCodeRateService(Integer code){
		if (!Validations.validateListIsNullOrEmpty(listCodeServiceRate)){
			listCodeServiceRate.add(code);
		}
	}
	
	public void removeLastCodeRateService(Integer code){
		if (!Validations.validateListIsNullOrEmpty(listCodeServiceRate)){
			listCodeServiceRate.remove(code);
		}
	}
	
	
	
	public String getDescriptionNameCollectionEntity() {
		return descriptionNameCollectionEntity;
	}



	public void setDescriptionNameCollectionEntity(
			String descriptionNameCollectionEntity) {
		this.descriptionNameCollectionEntity = descriptionNameCollectionEntity;
	}





	public Integer getInIntegratedBill() {
		return inIntegratedBill;
	}



	public void setInIntegratedBill(Integer inIntegratedBill) {
		this.inIntegratedBill = inIntegratedBill;
	}



	public Integer getTaxApplied() {
		return taxApplied;
	}



	public void setTaxApplied(Integer taxApplied) {
		this.taxApplied = taxApplied;
	}



	public String getDescriptionCurrency() {
		return descriptionCurrency;
	}



	public void setDescriptionCurrency(String descriptionCurrency) {
		this.descriptionCurrency = descriptionCurrency;
	}



	/**
	 * @return the referenceRate
	 */
	public String getReferenceRate() {
		return referenceRate;
	}



	/**
	 * @param referenceRate the referenceRate to set
	 */
	public void setReferenceRate(String referenceRate) {
		this.referenceRate = referenceRate;
	}



	/**
	 * @return the listReferenceRate
	 */
	public List<String> getListReferenceRate() {
		return listReferenceRate;
	}



	/**
	 * @param listReferenceRate the listReferenceRate to set
	 */
	public void setListReferenceRate(List<String> listReferenceRate) {
		this.listReferenceRate = listReferenceRate;
	}



	/**
	 * @return the listServiceCode
	 */
	public List<String> getListServiceCode() {
		return listServiceCode;
	}



	/**
	 * @param listServiceCode the listServiceCode to set
	 */
	public void setListServiceCode(List<String> listServiceCode) {
		this.listServiceCode = listServiceCode;
	}
}
