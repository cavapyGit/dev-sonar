package com.pradera.core.component.swift.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.TransactionSynchronizationRegistry;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.funds.FundsOperationType;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.type.AccountCashFundsType;
import com.pradera.model.funds.type.BankType;
import com.pradera.model.funds.type.CashAccountStateType;
import com.pradera.model.funds.type.CashAccountUseType;
import com.pradera.model.funds.type.ExcecutionType;
import com.pradera.model.funds.type.FundsType;
import com.pradera.model.funds.type.OperationClassType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.ParticipantOperation;
import com.pradera.model.negotiation.constant.NegotiationConstant;
import com.pradera.model.negotiation.type.InChargeNegotiationType;
import com.pradera.model.negotiation.type.ParticipantRoleType;


@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class AutomaticReceptionFundsService extends CrudDaoServiceBean{

	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	/**
	 * METHOD WHICH WILL THE FUND_OPERATION BY THE TRN AND CURRENCY
	 * @param trn
	 * @param currency
	 * @return
	 */
	public FundsOperationType findSwiftComponent(String trn,Integer currency){
		FundsOperationType fundOperationType = null;
		TypedQuery<FundsOperationType> query = em.createQuery(
				"SELECT f FROM SwiftComponent s,FundsOperationType f " +
				"WHERE " +
				"s.fundsOperationType.idFundsOperationTypePk = f.idFundsOperationTypePk " +
				"and s.currency = :currency " +
				"and s.trnField = :trn " +
				"and f.operationClass = :opeClass " +
				"and nvl(f.excecutionType,0) <> :executionType ", 
				FundsOperationType.class);

		query.setParameter("trn",trn);
		query.setParameter("currency",currency);
		query.setParameter("opeClass",FundsType.PARAMETER_OPERATION_CLASS_DEPOSIT.getCode());
		query.setParameter("executionType",ExcecutionType.MANUAL_FUND.getCode());
		List<FundsOperationType> lst = query.getResultList();
		if(lst != null && lst.size()>0){
			fundOperationType = lst.get(0);
		}else{
			return null;
		}

		return fundOperationType;
	}

	/**
	 * METHOD WHICH WILL GET CURRENCIES PK REGISTERED IN THE SYSTEM
	 * @return
	 */
	public List<Integer> getCurrencies(){
		List<Integer> currencyList = new ArrayList<Integer>();
		TypedQuery<Integer> query = em.createQuery(
				"SELECT p.parameterTablePk FROM ParameterTable p where p.masterTable.masterTablePk = :master ", 
				Integer.class);

		query.setParameter("master",MasterTableType.CURRENCY.getCode());
		currencyList = query.getResultList();

		return currencyList;
	}

	/**
	 * METHOD WHICH WILL GET THE CENTRALIZING CASH ACCOUNT BY CURRENCY, IF IT EXIST
	 * @param currency
	 * @return
	 */
	public InstitutionCashAccount getActivatedCentralizingAccount(Integer currency){
		InstitutionCashAccount cashAccount = null;
		try{
			String strQuery = 	"SELECT ica FROM InstitutionCashAccount ica " +
								"where " +
								"ica.currency = :currency and " +
								"ica.accountState = :state and " +
								"ica.accountType = :type ";
			TypedQuery<InstitutionCashAccount> query = em.createQuery(strQuery,InstitutionCashAccount.class);
			query.setParameter("currency",currency);
			query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
			query.setParameter("type", AccountCashFundsType.CENTRALIZING.getCode());
			cashAccount = query.getSingleResult();
		}catch(NoResultException x){
			x.printStackTrace();
		}

		return cashAccount;
	}
	
	public InstitutionCashAccount getActivatedIssuerPaymentCashAccount(String idIssuerPk ,Integer currency){
		InstitutionCashAccount cashAccount = null;
		try{
			String strQuery = 	" SELECT ica FROM InstitutionCashAccount ica " +
								" where " +
								" ica.issuer.idIssuerPk = :idIssuerPk and " +
								" ica.currency = :currency and " +
								" ica.accountState = :state and " +
								" ica.accountType = :type ";
			TypedQuery<InstitutionCashAccount> query = em.createQuery(strQuery,InstitutionCashAccount.class);
			query.setParameter("idIssuerPk",idIssuerPk);
			query.setParameter("currency",currency);
			query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
			query.setParameter("type", AccountCashFundsType.BENEFIT.getCode());
			cashAccount = query.getSingleResult();
		}catch(NoResultException x){
			x.printStackTrace();
		}

		return cashAccount;
	}
	
	/**
	 * METHOD WHICH WILL GET THE CENTRALIZING CASH ACCOUNT BY CURRENCY, IF IT EXIST
	 * @param currency
	 * @return
	 */
	public boolean validateActivatedCentralizingAccount(Integer currency){
		boolean validate = false;
		Long count = null;
		try{
			String strQuery = 	"SELECT count(ica) FROM InstitutionCashAccount ica " +
								"where " +
								"ica.currency = :currency and " +
								"ica.accountState = :state and " +
								"ica.accountType = :type ";
			TypedQuery<Long> query = em.createQuery(strQuery,Long.class);
			query.setParameter("currency",currency);
			query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
			query.setParameter("type", AccountCashFundsType.CENTRALIZING.getCode());
			count = query.getSingleResult();
			if(count!=null && count>0){
				validate=true;
			}
		}catch(NoResultException x){
			x.printStackTrace();
		}

		return validate;
	}
	/**
	 * METHOD WHICH WILL GET THE CEVALDOM BANK ACCOUNT NUMBER
	 * @param currency
	 * @return
	 */
	public String getCevaldomAccountNumber(Integer currency){
		String cevaldomAccountNumber = StringUtils.EMPTY;
		TypedQuery<InstitutionCashAccount> query = em.createQuery(
				"SELECT i FROM InstitutionCashAccount i, InstitutionBankAccount b " +
				"where " +
				"i.currency = :currency and " +
				"b.institutionCashAccount.idInstitutionCashAccountPk = i.idInstitutionCashAccountPk and " +
				"i.accountState = :state and " +
				"i.accountType = :type ", 
				InstitutionCashAccount.class);
		query.setParameter("currency",currency);
		query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
		query.setParameter("type", AccountCashFundsType.CENTRALIZING.getCode());
		cevaldomAccountNumber = query.getSingleResult().toString();
		return cevaldomAccountNumber;
	}

	/**
	 * METHOD WHICH VALIDATE IF THE ISSUER SENT IS A CORRECT ONE
	 * @param issuer
	 * @return
	 */
	public Issuer validateActiveIssuer(String issuer){
		List<Issuer> issuerList = new ArrayList<Issuer>();
		Issuer issuerObj = new Issuer();

		TypedQuery<Issuer> query = em.createQuery(
				"SELECT i FROM Issuer i " +
				"where " +
				"i.idIssuerPk = :issuer and " +
				"i.stateIssuer = :state ", 
				Issuer.class);

		query.setParameter("issuer",issuer);
		query.setParameter("state",Integer.valueOf(MasterTableType.PARAMETER_TABLE_ISSUER_STATE_ACTIVATE.getLngCode().toString()));
		issuerList = query.getResultList();
		if(issuerList == null || issuerList.size() == 0){
			return null;
		}else{
			issuerObj = issuerList.get(0);
		}
		return issuerObj;
	}

	/**
	 * METHOD WHICH VALIDATE IF OPERATION EXIST BY PAYMENT REFERENCE
	 * @param paymentReference
	 * @return
	 */
	public MechanismOperation existOperationByPaymentReference(String paymentReference){
		MechanismOperation operation = null;
		List<MechanismOperation> operationList = new ArrayList<MechanismOperation>();

		TypedQuery<MechanismOperation> query = em.createQuery(
				"SELECT m FROM MechanismOperation m " +
				"where " +
				"m.paymentReference = :paymentReference ", 
				MechanismOperation.class);

		query.setParameter("paymentReference",paymentReference);
		operationList = query.getResultList();
		if(operationList != null && operationList.size() > 0){
			operation = operationList.get(0);
		}
		return operation;
	}

	/**
	 * METHOD WHICH VERIFY IF MECHANISM CODE EXIST OR NOT
	 * @param mechanism
	 * @return
	 */
	public NegotiationMechanism existMechanismCode(String mechanismCode){
		NegotiationMechanism mechanism = new NegotiationMechanism();
		try{
			TypedQuery<NegotiationMechanism> query = em.createQuery(
					"SELECT n FROM NegotiationMechanism n " +
					"where " +
					"n.mechanismCode = :mechanism ", 
					NegotiationMechanism.class);

			query.setParameter("mechanism",mechanismCode);
			mechanism = query.getSingleResult();
		}catch(NoResultException x){
			x.printStackTrace();
		}

		return mechanism;
	}

	/**
	 * METHOD WHICH VERIFY IF MODALITY CODE EXIST OR NOT
	 * @param modality
	 * @return
	 */
	public NegotiationModality existModalityCode(String modalityCode){
		NegotiationModality modality = new NegotiationModality();
		try{
			TypedQuery<NegotiationModality> query = em.createQuery(
					"SELECT n FROM NegotiationModality n " +
					"where " +
					"n.modalityCode = :modality ", 
					NegotiationModality.class);

			query.setParameter("modality",modalityCode);
			modality = query.getSingleResult();
		}catch(NoResultException x){
			x.printStackTrace();
		}

		return modality;
	}

	/**
	 * METHOD WHICH VALIDATE PARTICIPANT BY BIC CODE SENT
	 * @param bicCode
	 * @return
	 */
	public Long getParticipantByBIC(String bicCode){
		Long participant = null;
		List<Participant> participantList = new ArrayList<Participant>();

		TypedQuery<Participant> query = em.createQuery(
				"SELECT p FROM Participant p " +
				"where " +
				"p.bicCode = :bic ", 
				Participant.class);

		query.setParameter("bic",bicCode);
		participantList = query.getResultList();
		if(!participantList.isEmpty()){
			Participant participantObj = participantList.get(0);
			participant = participantObj.getIdParticipantPk();
		}
		return participant;
	}

	/**
	 * METHOD WHICH VALIDATE IF EXIST A RELATION ON PARTICIPANT,OPERATION,TERM_IND TO RECEIPT THE FUNDS
	 * @param operationPK
	 * @param participantPK
	 * @param indCash
	 * @return
	 */
	public List<ParticipantOperation> detailByParticipant(Long operationPK, Long participantPK, boolean indCash,boolean indTerm,Integer indCashTerm){
		List<ParticipantOperation> lstResult = null;

		String queryStr = "SELECT p FROM ParticipantOperation p " +
				"where " +
				"p.mechanismOperation.tradeOperation.idTradeOperationPk = :opePK " +
				"and p.role = :role " +
				"and p.operationPart = :opePart " +
				"and p.inchargeType = :inchargeType " +
				"and p.operationPart = :operationPart ";
		if(Validations.validateIsNotNull(participantPK)){
			queryStr += "and p.participant.idParticipantPk = :participant ";
		}

		TypedQuery<ParticipantOperation> query = em.createQuery(queryStr,ParticipantOperation.class);

		query.setParameter("opePK",operationPK);
		query.setParameter("inchargeType",InChargeNegotiationType.INCHARGE_FUNDS.getCode());
		query.setParameter("operationPart", indCashTerm);
		if(Validations.validateIsNotNull(participantPK)){
			query.setParameter("participant",participantPK);
		}
		if(indCash){
			query.setParameter("role",FundsType.IND_ROLE_BUY.getCode());
		}else{
			query.setParameter("role",FundsType.IND_ROLE_SELL.getCode());
		}
		if(indTerm){
			query.setParameter("opePart",FundsType.IND_TERM_PART.getCode());
		}else{
			query.setParameter("opePart",FundsType.IND_CASH_PART.getCode());
		}
		lstResult = query.getResultList();
		return lstResult;
	}

	/**
	 * METHOD WHICH VERIFY IS EXIST A PAYMENT FOR THE ISSUER AND CURRECY ENTERED
	 * @param issuer
	 * @param currency
	 * @return
	 */
	public List<CorporativeOperation> existPaymentIssuer(String issuer, Integer currency){
		String queryStr = "SELECT c FROM CorporativeOperation c " +
				"where " +
				"c.currency = :currency " +
				"and c.issuer.idIssuerPk = :issuer " +
				"and trunc(c.deliveryDate) = :today ";

		TypedQuery<CorporativeOperation> query = em.createQuery(queryStr,CorporativeOperation.class);
		query.setParameter("currency",currency);
		query.setParameter("issuer",issuer);
		query.setParameter("today",CommonsUtilities.currentDate());

		List<CorporativeOperation> lst = query.getResultList();

		return lst;
	}

	/**
	 * METHOD WHICH WE WILL GET BANK PK WITH BIC_CODE
	 * @param bicBank
	 * @return
	 */
	public Long getBankIdByBic(String bicBank){
		Long idbank = null;
		String queryStr = "SELECT b FROM Bank b " +
				"where " +
				"b.bicCode = :bic " +
				"and b.state = :state " +
				"and b.bankOrigin = :origin " +
				"and b.bankType = :type ";

		TypedQuery<Bank> query = em.createQuery(queryStr,Bank.class);
		query.setParameter("bic",bicBank);
		query.setParameter("state",MasterTableType.PARAMETER_TABLE_BANK_STATE_REGISTERED.getLngCode());
		query.setParameter("origin",MasterTableType.PARAMETER_TABLE_BANK_ORIGIN_NATIONAL.getLngCode());
		query.setParameter("type",BankType.COMMERCIAL.getCode());

		List<Bank> lst = query.getResultList();
		if(!lst.isEmpty()){
			idbank = lst.get(0).getIdBankPk();
		}
		return idbank;
	}

	/**
	 * METHOD WHICH VALIDATE IF EXIST A MANUAL FUND OPERATION REGISTERED FOR THAT DEVOLUTION
	 * @param amount
	 * @param currency
	 * @param idBank
	 * @return
	 */
	public Long validateExistPaymentBank(String amount, Integer currency, Long idBank){
		Long id = null;
		String queryStr = "SELECT f FROM FundsOperation f " +
				"where " +
				"f.operationAmount = :amount " +
				"and f.currency = :currency " +
				"and f.bank.idBankPk = :bank " +
				"and f.state = :state " +
				"and f.processingType = :processType " +
				"and f.fundsOperationType = :fundsOperationType";

		TypedQuery<FundsOperation> query = em.createQuery(queryStr,FundsOperation.class);
		query.setParameter("amount",new BigDecimal(amount));
		query.setParameter("currency",currency);
		query.setParameter("bank",idBank);
		query.setParameter("state",MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REGISTERED.getCode());
		query.setParameter("processType",FundsType.PARAMETER_PROCESS_TYPE_MANUAL.getCode());
		query.setParameter("fundsOperationType",com.pradera.model.component.type.ParameterFundsOperationType.UNPAYED_BENEFITS_RETURN_DEPOSIT.getCode());
		
		List<FundsOperation> lst = query.getResultList();
		if(!lst.isEmpty()){
			id = lst.get(0).getIdFundsOperationPk();
		}

		return id;
	}

	/**
	 * METHOD WHICH VERIFY IF THE BIC CODE SENT IS FOR CEVALDOM AND IT IS REGISTER STATE
	 * @param bic
	 * @return
	 */
	public Participant validateParticipantBIC(String bic){
		Participant result = null;
		String queryStr = "SELECT p FROM Participant p " +
				"where " +
				"p.bicCode = :bic ";

		TypedQuery<Participant> query = em.createQuery(queryStr,Participant.class);
		query.setParameter("bic",bic);

		List<Participant> lst = query.getResultList();
		if(!lst.isEmpty()){
			result = lst.get(0);
		}
		return result;
	}

	/**
	 * METHOD WHICH VERIFY IF EXIST A PAYMENT OPERATION
	 * @param amount
	 * @param currency
	 * @return
	 */
	public boolean existOperationPayment(String amount, Integer currency){
		boolean exist = false;
		String queryStr = "SELECT c FROM CorporativeOperation c " +
				"where " +
				"c.currency = :currency " +
				"and c.custodyAmount = :amount ";

		TypedQuery<CorporativeOperation> query = em.createQuery(queryStr,CorporativeOperation.class);
		query.setParameter("currency",currency);
		query.setParameter("amount",new BigDecimal(amount));

		List<CorporativeOperation> lst = query.getResultList();
		if(!lst.isEmpty()){
			exist = true;
		}
		return exist;
	}

	/**
	 * METHOD WHICH WILL GET THE TYPE OF PROCESS WHICH WILL BE IN RESPONSE OF A FUNCTIONAL VALIDATION MISTAKE
	 * @param functionalValidation
	 * @return
	 */
	public ParameterTable getValidationProcess(Long functionalValidation){
		ParameterTable parameter = null;

		String queryStr = "SELECT p FROM ParameterTable p " +
				"where " +
				"p.parameterTablePk = :pk ";

		TypedQuery<ParameterTable> query = em.createQuery(queryStr,ParameterTable.class);
		query.setParameter("pk",Integer.valueOf(functionalValidation.toString()));
		parameter = query.getSingleResult();
		return parameter;
	}

	/**
	 * METHOD WHICH WILL GET THE BANK RELATED TO THE BENEFIT ACCOUNT IN THE CURRENCY AND ISSUER ASSOCIATED
	 * @param issuer
	 * @param currency
	 * @return
	 */
	public Long getBankFromIssuer(String issuer, Integer currency){
		Long bankPK = null;
		String queryStr = 
				"select ba.providerBank.idBankPk from InstitutionCashAccount ca, InstitutionBankAccount ba " +
				"where " +
				"ca.issuer.idIssuerPk = :issuer and " +
				"ca.currency = :currency and " +
				"ca.accountState = :state and " +
				"ba.institutionCashAccount.idInstitutionCashAccountPk = ca.idInstitutionCashAccountPk and " +
				"ca.accountClass =  :class and " +
				"ca.indRelatedBcrd = :bcrdInd ";

		TypedQuery<Long> query = em.createQuery(queryStr,Long.class);
		query.setParameter("issuer",issuer);
		query.setParameter("currency",currency);
		query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
		query.setParameter("class",MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_VIRTUAL.getCode());
		query.setParameter("bcrdInd",FundsType.CASH_ACCOUNT_IND_BCRD_RELATION.getCode());
		List<Long> lst = query.getResultList();
		if(Validations.validateListIsNotNullAndNotEmpty(lst)){
			bankPK = lst.get(0);
		}

		return bankPK;
	}

	/**
	 * METHOD WHICH WILL BE THE DESTINY OF THE FUND OPERATION
	 * @param cashAccType
	 * @param currency
	 * @return
	 */
	public InstitutionCashAccount getDestinyCashAccountFunds(Integer cashAccType, Integer currency){
		List<InstitutionCashAccount> lst = new ArrayList<InstitutionCashAccount>();
		InstitutionCashAccount cashAccount = null;
		String queryStr = 
				"select c from InstitutionCashAccount c " +
				"where " +
				"c.currency = :currency and " +
				"c.accountState = :state and " +
				"c.accountClass = :class and " +
				"c.accountType = :type and " +
				"c.indRelatedBcrd = :bcrdInd ";

		TypedQuery<InstitutionCashAccount> query = em.createQuery(queryStr,InstitutionCashAccount.class);
		query.setParameter("currency",currency);
		query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
		query.setParameter("bcrdInd",FundsType.CASH_ACCOUNT_IND_BCRD_RELATION.getCode());
		query.setParameter("class",MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_VIRTUAL.getCode());
		query.setParameter("type",cashAccType);

		lst = query.getResultList();
		if(Validations.validateListIsNotNullAndNotEmpty(lst)){
			cashAccount = lst.get(0);
		}
		return cashAccount;
	}

	/**
	 * GET THE CENTRALIZER CASH ACCOUNT FOR THE ISSUER 
	 * @param cashAccType
	 * @param currency
	 * @param issuer
	 * @return
	 */
	public InstitutionCashAccount getDestinyBenefitCashAccountFunds(Integer cashAccType, Integer currency, String issuer){
		InstitutionCashAccount cashAccount = null;
		try{
			String queryStr = 
					"select c from InstitutionCashAccount c " +
					"where " +
					"c.currency = :currency and " +
					"c.accountState = :state and " +
					"c.accountClass = :class and " +
					"c.accountType = :type and " +
//					"c.indRelatedBcrd = :bcrdInd and " +
					"c.issuer.idIssuerPk = :issuer ";

			TypedQuery<InstitutionCashAccount> query = em.createQuery(queryStr,InstitutionCashAccount.class);
			query.setParameter("currency",currency);
			query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
//			query.setParameter("bcrdInd",FundsType.CASH_ACCOUNT_IND_BCRD_RELATION.getCode());
			query.setParameter("class",MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_VIRTUAL.getCode());
			query.setParameter("type",cashAccType);
			query.setParameter("issuer",issuer);

			cashAccount = query.getSingleResult();
		}catch(NoResultException x){
			x.printStackTrace();
		}

		return cashAccount;
	}
	public boolean validateDestinyBenefitCashAccountFunds(Integer cashAccType, Integer currency, String issuer){
		boolean validate = false;
		Long count = null;
		try{
			String queryStr = 
					"select count(c) from InstitutionCashAccount c " +
					"where " +
					"c.currency = :currency and " +
					"c.accountState = :state and " +
					"c.accountClass = :class and " +
					"c.accountType = :type and " +
//					"c.indRelatedBcrd = :bcrdInd and " +
					"c.issuer.idIssuerPk = :issuer ";

			TypedQuery<Long> query = em.createQuery(queryStr,Long.class);
			query.setParameter("currency",currency);
			query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
//			query.setParameter("bcrdInd",FundsType.CASH_ACCOUNT_IND_BCRD_RELATION.getCode());
			query.setParameter("class",MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_VIRTUAL.getCode());
			query.setParameter("type",cashAccType);
			query.setParameter("issuer",issuer);

			count = query.getSingleResult();
			if(count!=null && count>0){
				validate = true;
			}
		}catch(NoResultException x){
			x.printStackTrace();
		}

		return validate;
	}
	/**
	 * METHOD WHICH WILL BE THE DESTINY OF THE FUND OPERATION IN SETTLEMENT
	 * @param cashAccType
	 * @param currency
	 * @return
	 */
	public InstitutionCashAccount getDestinyCashAccountSettlement(Integer cashAccType, Integer currency, Long mechanism, Long modalityGroup, Long participant){
		InstitutionCashAccount cashAccount = null;
		try{
			String queryStr = 
					"select c from InstitutionCashAccount c " +
					"where " +
					"c.currency = :currency and " +
					"c.accountState = :state and " +
					"c.accountClass = :class and " +
					"c.accountType = :type and " +
					"c.indRelatedBcrd = :bcrdInd and " +
					"c.negotiationMechanism.idNegotiationMechanismPk = :mechanism and " +
					"c.modalityGroup.idModalityGroupPk = :modalityGroup and " +
					"c.participant.idParticipantPk = :participant ";

			TypedQuery<InstitutionCashAccount> query = em.createQuery(queryStr,InstitutionCashAccount.class);
			query.setParameter("currency",currency);
			query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
			query.setParameter("bcrdInd",FundsType.CASH_ACCOUNT_IND_BCRD_RELATION.getCode());
			query.setParameter("class",MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_VIRTUAL.getCode());
			query.setParameter("type",cashAccType);
			query.setParameter("mechanism",mechanism);
			query.setParameter("modalityGroup",modalityGroup);
			query.setParameter("participant", participant);

			cashAccount = query.getSingleResult();
		}catch(NoResultException x){
			x.printStackTrace();
		}
		return cashAccount;
	}
	
	
	/**
	 * METHOD WHICH WILL GET THE BIC CODE OF THE DEPOSITARY
	 * @return
	 */
	public String getDepositaryBicCode(){
		String bic = StringUtils.EMPTY;
		try {
			String queryStr = "select bicCode from Participant where indParticipantDepository=:indDepositary";
	
			TypedQuery<String> query = em.createQuery(queryStr,String.class);
			query.setParameter("indDepositary", FundsType.PARAMETER_CEVALDOM_PARTICIPANT.getCode());
			bic = query.getSingleResult();
		}catch(NoResultException ex){
			return "BCNAPYPA";
		}
		return bic;
	}

	/**
	 * METHOD 
	 * @param operationTypePK
	 * @param currency
	 * @param flgDeposit
	 * @return
	 */
	public String getTrn(Long operationTypePK, Integer currency){
		String trn = StringUtils.EMPTY;

		String queryStr = 	"select s.trnField from SwiftComponent s, FundsOperationType f " +
							"where " +
							"s.fundsOperationType.idFundsOperationTypePk = f.idFundsOperationTypePk " +
							"and s.currency = :currency " +
							"and f.operationClass = :class " +
							"and f.idFundsOperationTypePk = :fundOpType ";
		TypedQuery<String> query = em.createQuery(queryStr,String.class);
		query.setParameter("currency", currency);
		query.setParameter("class", FundsType.IND_RETIREMENT_PROCESS.getCode());
		query.setParameter("fundOpType", operationTypePK);
		trn = query.getSingleResult();

		return trn;
	}

	/**
	 * METHOD WHICH WILL GET THE PROPER PARTICIPANT OPERATION IN ORDER TO UPDATE INDICATORS
	 * @param participant
	 * @param operation
	 * @param indTerm
	 * @param indBuyRole
	 * @return
	 */
	public ParticipantOperation getParticipantOperation(Long participant, Long operation, boolean indTerm, boolean indBuyRole){
		ParticipantOperation partOperation = null;
		String queryStr = 
				"select po from ParticipantOperation po where " +
				"po.participant.idParticipantPk = :participant and " +
				"po.mechanismOperation.idMechanismOperationPk = :operation and " +
				"po.operationPart = :indPart and " +
				"po.role = :role and " +
				"po.inchargeType = :inchargeType ";

		TypedQuery<ParticipantOperation> query = em.createQuery(queryStr,ParticipantOperation.class);
		query.setParameter("participant",participant);
		query.setParameter("operation",operation);
		query.setParameter("inchargeType", InChargeNegotiationType.INCHARGE_FUNDS.getCode());

		if(indTerm){
			query.setParameter("indPart",FundsType.INDICATOR_TERM_PART.getCode());
		}else{
			query.setParameter("indPart",FundsType.INDICATOR_CASH_PART.getCode());
		}
		if(indBuyRole){
			query.setParameter("role",FundsType.INDICATOR_ROLE_BUYER.getCode());
		}else{
			query.setParameter("role",FundsType.INDICATOR_ROLE_SELLER.getCode());
		}
		partOperation = query.getSingleResult();
		return partOperation;
	}

	/**
	 * METHOD WHICH WILL VERIFY IF OTHER BUYER PARTICIPANTS HAVE PAID 
	 * @param operation
	 * @param indCashTerm
	 * @param partOperationPK
	 * @return
	 */
	public boolean verifyOperationPaid(Long operation,Integer indCashTerm,Long participant,Long participantOperationPK){
		boolean paidOperation = false;

		String queryStr = 
				"select po from ParticipantOperation po where " +
				"po.participant.idParticipantPk <> :participant and " +
				"po.mechanismOperation.idMechanismOperationPk = :operation and " +
				"po.operationPart = :indPart and " +
				"po.role = :role and " +
				"po.idParticipantOperationPk <> :partOperation and " +
				"po.inchargeType = :funds and " +
				"po.fundsReference IS NULL";
		TypedQuery<ParticipantOperation> query = em.createQuery(queryStr,ParticipantOperation.class);
		query.setParameter("participant",participant);
		query.setParameter("operation",operation);
		query.setParameter("indPart",indCashTerm);
		query.setParameter("funds",InChargeNegotiationType.INCHARGE_FUNDS.getCode());
		query.setParameter("role", ParticipantRoleType.BUY.getCode());
		query.setParameter("partOperation",participantOperationPK);

		List<ParticipantOperation> unpaidParticipants = query.getResultList();
		if(Validations.validateListIsNullOrEmpty(unpaidParticipants)){
			paidOperation = true;
		}

		return paidOperation;
	}

	/**
	 * THIS METHOD WILL BE INCHARGE TO GET THE MODALITY GROUP OF THE OPERATION WITH MECHANISM AND MODALITY
	 * @param mechanism
	 * @param modality
	 * @return
	 */
	public Long getModalityGroup(Long mechanism,Long modality){
		Long modalityGroup = null;
		String queryStr = 	"select mgd.modalityGroup.idModalityGroupPk from ModalityGroupDetail mgd " +
							"where mgd.negotiationModality.idNegotiationModalityPk = :modality " +
							"and mgd.negotiationMechanism.idNegotiationMechanismPk = :mechanism";

		TypedQuery<Long> query = em.createQuery(queryStr,Long.class);
		query.setParameter("modality",modality);
		query.setParameter("mechanism",mechanism);
		modalityGroup = query.getSingleResult();

		return modalityGroup;
	}

	/**
	 * METHOD TO GET THE MODALITY GROUP BY ITS CODE
	 * @param modalityGroupCode
	 * @return
	 */
	public Long getModalityGroup(String modalityGroupCode){
		Long modalityGroup = null;
		try{
			String queryStr = 	"select mg.idModalityGroupPk from ModalityGroup mg " +
					"where mg.modalityGroupCode = :modalityGroupCode ";
			TypedQuery<Long> query = em.createQuery(queryStr,Long.class);
			query.setParameter("modalityGroupCode",modalityGroupCode);
			modalityGroup = query.getSingleResult();

		}catch(NoResultException x){
			x.printStackTrace();
		}
		return modalityGroup;
	}

	/**
	 * VALIDATE IF EXISTS THE CASH ACCOUNT FOR THE PARTICIPANT IN NET SETTLEMENT
	 * @param participant
	 * @param strCurrency
	 * @param cashAccountType
	 * @return
	 */
	public InstitutionCashAccount validateExistCashAccount(Long participant, Integer currency, Integer cashAccountType, Long mechanism, Long modalityGroup){
		InstitutionCashAccount cashAccount = null;
		try{
			String queryStr = 
					"select c from InstitutionCashAccount c " +
					"where " +
					"c.currency = :currency and " +
					"c.accountState = :state and " +
					"c.accountClass = :class and " +
					"c.accountType = :type and " +
					"c.indRelatedBcrd = :bcrdInd and " +
					"c.participant.idParticipantPk = :part and " +
					"c.negotiationMechanism.idNegotiationMechanismPk = :mechanism and " +
					"c.modalityGroup.idModalityGroupPk = :modalityGroup ";

			TypedQuery<InstitutionCashAccount> query = em.createQuery(queryStr,InstitutionCashAccount.class);

			query.setParameter("currency",currency);
			query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
			query.setParameter("bcrdInd",FundsType.CASH_ACCOUNT_IND_BCRD_RELATION.getCode());
			query.setParameter("class",MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_VIRTUAL.getCode());
			query.setParameter("type",cashAccountType);
			query.setParameter("part", participant);
			query.setParameter("mechanism", mechanism);
			query.setParameter("modalityGroup", modalityGroup);

			cashAccount = query.getSingleResult();
		}catch(NoResultException x){
			x.printStackTrace();
		}

		return cashAccount;
	}

	/**
	 * METHOD WHICH WILL RETURN THE BANK ACCOUNT NUMBER FOR FUND WITHDRAWL
	 * @param mechanism
	 * @param modalityGroup
	 * @param participant
	 * @param currency
	 * @return
	 */
	public String getFundsSendSettlementCashAccount(Long mechanism, Long modalityGroup, Long participant, Integer currency, boolean blBuy){
		String bankAccountNumber = StringUtils.EMPTY;
		String queryStr = 
				"select b.accountNumber from InstitutionCashAccount c, InstitutionBankAccount b " +
				"where " +
				"c.idInstitutionCashAccountPk = b.institutionCashAccount.idInstitutionCashAccountPk and " +
				"c.currency = :currency and " +
				"c.accountState = :state and " +
				"c.accountClass = :class and " +
				"c.accountType = :type and " +
				"c.indRelatedBcrd = :bcrdInd and " +
				"c.participant.idParticipantPk = :part and " +
				"b.useTypeAccount <> :useType ";

		TypedQuery<String> query = em.createQuery(queryStr,String.class);
		query.setParameter("currency",currency);
		query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
		query.setParameter("bcrdInd",FundsType.CASH_ACCOUNT_IND_BCRD_RELATION.getLngCode());
		query.setParameter("class",MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_VIRTUAL.getCode());
		query.setParameter("type",AccountCashFundsType.SETTLEMENT.getCode());
		query.setParameter("part", participant);
		if(blBuy){
			query.setParameter("useType", CashAccountUseType.SEND.getCode());
		}else{
			query.setParameter("useType", CashAccountUseType.RECEPTION.getCode());
		}

		List<String> lst = new ArrayList<String>();
		lst = query.getResultList();
		if(Validations.validateListIsNotNullAndNotEmpty(lst)){
			bankAccountNumber = lst.get(0);
		}
		return bankAccountNumber;
	}

	/**
	 * METHOD WHICH WILL RETURN THE DESCRIPTION OF THE CORPORATIVE PROCESS WHICH IS BEING PAID
	 * @param parameterPK
	 * @return
	 */
	public String getParameterTableDescription(Integer parameterPK){
		String corporativeDescription = StringUtils.EMPTY;
		String queryStr = "select p.description from ParameterTable p where p.parameterTablePk = :pk";
		TypedQuery<String> query = em.createQuery(queryStr,String.class);
		query.setParameter("pk",parameterPK);
		corporativeDescription = query.getSingleResult();
		return corporativeDescription;
	}

	/**
	 * METHOD WHICH WILL UPDATE FUND INDICATOR BY HOLDER ACCOUNT IN OPERATION
	 * @param operation
	 * @param indCashTerm
	 */
	public void updateIndicatorHolderAccountBuyers(Long operation,Integer indCashTerm, Long participant, Long fundIndicator){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		String sqlQuery =
							"UPDATE HolderAccountOperation " +
							"SET fundsReference = :fundIndicator, " +
							"lastModifyApp = :lastApp, " +
							"lastModifyDate = :lastDate, " +
							"lastModifyIp = :lastIp, " +
							"lastModifyUser = :lastUser " +
							"WHERE mechanismOperation.idMechanismOperationPk = :operationPK " +
							"AND operationPart = :participation " +
							"AND role = :role " +
							"AND inchargeFundsParticipant.idParticipantPk = :participant ";

		javax.persistence.Query query = em.createQuery(sqlQuery);
		query.setParameter("fundIndicator",fundIndicator);
		query.setParameter("operationPK",operation);
		query.setParameter("participation",indCashTerm);
		query.setParameter("role",ParticipantRoleType.BUY.getCode());
		query.setParameter("participant",participant);
		query.setParameter("lastApp",loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastDate",loggerUser.getAuditTime());
		query.setParameter("lastIp",loggerUser.getIpAddress());
		query.setParameter("lastUser",loggerUser.getUserName());

		query.executeUpdate();
	}

	/**
	 * METHOD WHICH WILL BE INCHARGE TO GET ALL PARTICIPANT BUYERS TO EXECUTE COLLECTION OPERATIONS FOR EACH ONE
	 * @param operation
	 * @param operationPart
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	public List<Long> getOperationBuyers(Long operation, Integer operationPart) throws ServiceException{
		List<Long> lstReturn= null;
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT participant.idParticipantPk ");
		stringBuffer.append(" FROM ParticipantOperation ");
		stringBuffer.append(" WHERE mechanismOperation.idMechanismOperationPk = :idMechanismOperation ");
		stringBuffer.append(" and operationPart = :operationPart ");
		stringBuffer.append(" and role = :role ");
		stringBuffer.append(" and inchargeType = :inchargeType ");

		javax.persistence.Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idMechanismOperation", operation);
		query.setParameter("operationPart", operationPart);
		query.setParameter("role", ComponentConstant.PURCHARSE_ROLE);
		query.setParameter("inchargeType", InChargeNegotiationType.INCHARGE_FUNDS.getCode());

		lstReturn = query.getResultList();
		return lstReturn;
	}

	/**
	 * METHOD WHICH WILL GET THE CENTRALIZING CASH ACCOUNT BY CURRENCY, IF IT EXIST (PK)
	 * @param currency
	 * @return
	 */
	public Long getActivatedCentralizingAccountPK(Integer currency){
		Long cashAccountPK = null;
		try{
			String strQuery = 	"SELECT ica.idInstitutionCashAccountPk FROM InstitutionCashAccount ica " +
								"where " +
								"ica.currency = :currency and " +
								"ica.accountState = :state and " +
								"ica.accountType = :type ";
			TypedQuery<Long> query = em.createQuery(strQuery,Long.class);
			query.setParameter("currency",currency);
			query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
			query.setParameter("type", AccountCashFundsType.CENTRALIZING.getCode());
			cashAccountPK = query.getSingleResult();
		}catch(NoResultException x){
			x.printStackTrace();
		}

		return cashAccountPK;
	}

	/**
	 * METHOD IMPLEMENTED TO GET NEXT OPERATION NUMBER TO SET INTO FUND_OPERATION BY DAY - OPERATION CLASS
	 * @param fundOperationClass
	 * @return
	 */
	public Integer getNextFundsOperationNumber(Integer fundOperationClass, Integer operationGroup){
		Integer operationNumber = null;
		String strQuery = 
				"select nvl(max(f.fundOperationNumber),0) + 1 " +
				"from FundsOperation f " +
				"where " +
				"trunc(f.registryDate) = trunc(:currentDate) " +
				"and f.fundsOperationClass = :operationClass " +
				"and f.fundsOperationGroup = :fundOperationGroup ";
		TypedQuery<Integer> query = em.createQuery(strQuery,Integer.class);
		query.setParameter("currentDate",CommonsUtilities.currentDate());
		query.setParameter("operationClass",fundOperationClass);
		query.setParameter("fundOperationGroup",operationGroup);
		operationNumber = query.getSingleResult();
		return operationNumber;
	}

	/**
	 * METHOD TO GET THE NEXT CORRELATIVE FOR FUNDS WITHDRAWL PROCESS BY END DAY OR REJECT OBSERVED FUND OPERATIONS		
	 * @return
	 */
	public Long getFundOperationWithdrawlCorrelative(){
		Long correlative = null;
		String strQuery = 
				"select count(f) " +
				"from FundsOperation f " +
				"where " +
				"trunc(f.registryDate) = trunc(:currentDate) " +
				"and f.fundsOperationClass = :operationClass " +
				"and f.fundsOperation.idFundsOperationPk is not null " +
				"and f.fundsOperationType in (:fundOperationType)";

		List<Long> fundOperationTypes = new ArrayList<Long>();
		fundOperationTypes.add(com.pradera.model.component.type.ParameterFundsOperationType.DEVOLUTION_RECEPTIONS_FUNDS_DEPOSIT.getCode());
		fundOperationTypes.add(com.pradera.model.component.type.ParameterFundsOperationType.DEVOLUTION_RECEPTIONS_FUNDS_WITHDRAWL_SETTLEMENT.getCode());

		TypedQuery<Long> query = em.createQuery(strQuery,Long.class);
		query.setParameter("currentDate",CommonsUtilities.currentDate());
		query.setParameter("operationClass",FundsType.IND_FUND_OPERATION_CLASS_SEND.getCode());
		query.setParameter("fundOperationType",fundOperationTypes);

		correlative = query.getSingleResult();
		return correlative;
	}

	/**
	 * METHOD TO VALIDATE IF EXIST AN INSTITUTION CASH ACCOUNT FOR SPECIAL PAYMENT BY PARTICIPANT - CURRENCY
	 * @param participant
	 * @param currency
	 * @return
	 */
	public InstitutionCashAccount validateSpecialPaymentCashAccount(Long participant, Integer currency){
		InstitutionCashAccount specialCashAccount = null;
		String strQuery = 
				"select ica "
				+ "from InstitutionCashAccount ica "
				+ "where ica.accountType = :cashAccountType "
				+ "and ica.participant.idParticipantPk = :participant "
				+ "and ica.accountState = :state "
				+ "and ica.currency = :currency";

		javax.persistence.Query query = em.createQuery(strQuery,InstitutionCashAccount.class);
		query.setParameter("participant",participant);
		query.setParameter("currency",currency);
		query.setParameter("cashAccountType",AccountCashFundsType.SPECIAL_PAYMENT.getCode());
		query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());

		List<InstitutionCashAccount> lstAccount = query.getResultList();
		if(Validations.validateListIsNotNullAndNotEmpty(lstAccount)){
			specialCashAccount = lstAccount.get(0);
		}
		return specialCashAccount;
	}

	/**
	 * METHOD TO GET THE RETURN CASH ACCOUNT BY CURRENCY
	 * @param currency
	 * @return
	 */
	public InstitutionCashAccount getReturnCashAccount(Integer currency){
		InstitutionCashAccount returnCashAccount = null;
		try{
			String strQuery = 
					"select ica "
					+ "from InstitutionCashAccount ica "
					+ "where ica.accountType = :cashAccountType "
					+ "and ica.accountState = :state "
					+ "and ica.currency = :currency "
					+ "and ica.indAutomaticProcess = :indAutomatic ";

			javax.persistence.Query query = em.createQuery(strQuery,InstitutionCashAccount.class);
			query.setParameter("cashAccountType",AccountCashFundsType.RETURN.getCode());
			query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
			query.setParameter("currency",currency);
			query.setParameter("indAutomatic",BooleanType.YES.getCode());

			returnCashAccount = (InstitutionCashAccount) query.getSingleResult();
		}catch(NoResultException x){
			x.printStackTrace();
		}
		return returnCashAccount;
	}

	/**
	 * METHOD TO GET THE MANUALLY REGISTERED FUND OPERATION
	 * @param paymentReference
	 * @return
	 */
	public FundsOperation searchRegisterFundOperation(String paymentReference){
		FundsOperation registerOperation = null;
		try{
			String strQuery = 
					"select fo "
					+ "from FundsOperation fo "
					+ "where "
					+ "fo.paymentReference = :paymentReference "
					+ "and fo.operationState = :state ";

			javax.persistence.Query query = em.createQuery(strQuery,FundsOperation.class);
			query.setParameter("paymentReference",paymentReference);
			query.setParameter("state",MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_REGISTERED.getCode());

			registerOperation = (FundsOperation) query.getSingleResult();
		}catch(NoResultException x){
			x.printStackTrace();
		}
		return registerOperation;
	}
	
	public Integer getAutomaticReceptionFundsOperationGroup(String strTRN)
	{
		Integer operationGroup= null;
		StringBuffer stringBuffer= new StringBuffer();
		try {
			stringBuffer.append(" SELECT DISTINCT SC.fundsOperationType.operationGroup ");
			stringBuffer.append(" FROM SwiftComponent SC ");
			stringBuffer.append(" WHERE SC.trnField = :strTRN ");
			stringBuffer.append(" and SC.fundsOperationType.operationClass = :operationClass ");
			stringBuffer.append(" and (SC.fundsOperationType.excecutionType is null or SC.fundsOperationType.excecutionType = :excecutionType) ");
			
			Query query= em.createQuery(stringBuffer.toString());
			query.setParameter("strTRN", strTRN);
			query.setParameter("operationClass", OperationClassType.RECEPTION_FUND.getCode());
			query.setParameter("excecutionType", ExcecutionType.AUTOMATIC_FUND.getCode());
			operationGroup= (Integer) query.getSingleResult();
		} catch (NoResultException e) {
			e.printStackTrace();
		} catch (NonUniqueResultException e2) {
			e2.printStackTrace();
		}
		return operationGroup;
	}

	public Bank validateBankBic(String bic){
		Bank bank = null;
		String strQuery = "SELECT b FROM Bank b " +
				"where " +
				"b.bicCode = :bic";
		TypedQuery<Bank> query = em.createQuery(strQuery,Bank.class);
		query.setParameter("bic",bic);

		List<Bank> lst = query.getResultList();
		if(!lst.isEmpty()){
			bank = lst.get(0);
		}
		return bank;
	}
}