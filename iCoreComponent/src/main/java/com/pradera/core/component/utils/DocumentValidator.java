package com.pradera.core.component.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;

import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.utils.view.GeneralPropertiesConstants;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.HelperBean;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.EconomicActivityType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class DocumentValidator.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01-sep-2015
 */
@HelperBean
public class DocumentValidator implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	
	/**
	 * Gets the document type result.
	 *
	 * @param person the person
	 * @return the document type result
	 */
	public List<ParameterTable> getDocumentTypeResult(PersonTO person) {

		List<ParameterTable> listDocumentTypeResult;
		List<ParameterTable> listDocumentType = new ArrayList<ParameterTable>();
		listDocumentTypeResult = new ArrayList<ParameterTable>();
		listDocumentType = getLstDocumentType();
		ParameterTable auxParameterTable;

		if(person.getPersonType()!=null){
			if(person.isFlagHolderIndicator()||person.isFlagLegalRepresentativeIndicator()){
				switch (PersonType.get(person.getPersonType())) {
			 		case NATURAL:
			 			if (!person.isFlagMinorIndicator() && !person.isFlagInabilityIndicator()) {
					    	// para el caso en que un titular no se haya seleccionado si es o no residente.
					    	if (person.isFlagNationalResident() || (!person.isFlagNationalResident() && !person.isFlagNationalNotResident())){
								if (listDocumentType != null && listDocumentType.size() > 0) {
									for (int i = 0; i < listDocumentType.size(); i++) {
										auxParameterTable = new ParameterTable();
										if ((listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CI.getCode()))
											||(listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CID.getCode()))){
											
											auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
											auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
											auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
											listDocumentTypeResult.add(auxParameterTable);
										}
									}
								}
							}else if(person.isFlagNationalNotResident()){
								if (listDocumentType != null && listDocumentType.size() > 0) {
									for (int i = 0; i < listDocumentType.size(); i++) {
										auxParameterTable = new ParameterTable();
										if ((listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CI.getCode()))
											|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CID.getCode()))
											|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.PAS.getCode()))
											|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CIEE.getCode()))) {
											
											auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
											auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
											auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
											listDocumentTypeResult.add(auxParameterTable);
										}
									}
								}
							}else if (person.isFlagForeignResident()) {
								if (listDocumentType != null && listDocumentType.size() > 0) {
									for (int i = 0; i < listDocumentType.size(); i++) {
										auxParameterTable = new ParameterTable();
										if ((listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CIE.getCode()))
												|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DCDL.getCode()))
												|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DCD.getCode()))	
												|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DCRA.getCode()))
												|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DCRB.getCode()))	
												|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DLC.getCode()))		
												|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DCC.getCode())) 
												|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DCR.getCode()))) {
			
											auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
											auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
											auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
											listDocumentTypeResult.add(auxParameterTable);
										}
									}
								}
							}else if (person.isFlagForeignNotResident()) {
								if (listDocumentType != null && listDocumentType.size() > 0) {
									for (int i = 0; i < listDocumentType.size(); i++) {
										auxParameterTable = new ParameterTable();
										if ((listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DIO.getCode()))) {
											auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
											auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
											auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
											listDocumentTypeResult.add(auxParameterTable);
										}
									}
								}
							}
						} else if (person.isFlagMinorIndicator()) {
							if (person.isFlagNationalResident()) {
								if (listDocumentType != null && listDocumentType.size() > 0) {
									for (int i = 0; i < listDocumentType.size(); i++) {
										auxParameterTable = new ParameterTable();
										if ((listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CI.getCode()))
										  || (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CID.getCode())) 
										  || (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CDN.getCode()))){
											auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
											auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
											auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
											listDocumentTypeResult.add(auxParameterTable);
										}
									}
								}
							} else if (person.isFlagNationalNotResident()) {
								if (listDocumentType != null && listDocumentType.size() > 0) {
									for (int i = 0; i < listDocumentType.size(); i++) {
										auxParameterTable = new ParameterTable();
										if ((listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CI.getCode()))
												|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CID.getCode()))
												|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.PAS.getCode()))
												|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CDN.getCode()))) {
											auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
											auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
											auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
											listDocumentTypeResult.add(auxParameterTable);
										}
									}
								}
							} else if (person.isFlagForeignResident()) {
								if (listDocumentType != null && listDocumentType.size() > 0) {
									for (int i = 0; i < listDocumentType.size(); i++) {
										auxParameterTable = new ParameterTable();
										if ((listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CIE.getCode()))) {
											auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
											auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
											auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
											listDocumentTypeResult.add(auxParameterTable);
										}
									}
								}
							} else if (person.isFlagForeignNotResident()){
								if (listDocumentType != null && listDocumentType.size() > 0) {
									for (int i = 0; i < listDocumentType.size(); i++) {
										auxParameterTable = new ParameterTable();
										if ((listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DIO.getCode()))) {
											auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
											auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
											auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
											listDocumentTypeResult.add(auxParameterTable);
										}
									}
								}
							}
						} else if (person.isFlagInabilityIndicator()) {
							if (person.isFlagNationalResident()) {
								if (listDocumentType != null && listDocumentType.size() > 0) {
									for (int i = 0; i < listDocumentType.size(); i++) {
										auxParameterTable = new ParameterTable();
										if ((listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CI.getCode()))
											|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CID.getCode()))){
											auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
											auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
											auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
											listDocumentTypeResult.add(auxParameterTable);
										}
									}
								}
							} else if (person.isFlagNationalNotResident()) {
								if (listDocumentType != null && listDocumentType.size() > 0) {
									for (int i = 0; i < listDocumentType.size(); i++) {
										auxParameterTable = new ParameterTable();
										if ((listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CI.getCode()))
											|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CID.getCode()))											
											|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.PAS.getCode()))								
											|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CIEE.getCode()))) {
											auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
											auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
											auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
											listDocumentTypeResult.add(auxParameterTable);
										}
									}
								}
							} else if (person.isFlagForeignResident()) {
								if (listDocumentType != null && listDocumentType.size() > 0) {
									for (int i = 0; i < listDocumentType.size(); i++) {
										auxParameterTable = new ParameterTable();
										if ((listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CIE.getCode()))
										|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DCDL.getCode()))
										|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DCD.getCode()))
										|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DCRA.getCode()))
										|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DCRB.getCode()))		
										|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DLC.getCode()))
										|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DCC.getCode()))		
										|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DCR.getCode()))) {
											auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
											auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
											auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
											listDocumentTypeResult.add(auxParameterTable);
										}
									}
								}
							} else if(person.isFlagForeignNotResident()){
								if (listDocumentType != null && listDocumentType.size() > 0) {
									for (int i = 0; i < listDocumentType.size(); i++) {
										auxParameterTable = new ParameterTable();
										if ((listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DIO.getCode()))) {
											auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
											auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
											auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
											listDocumentTypeResult.add(auxParameterTable);
										}
									}
								}
							}				
						}
		    
						if(person.isFlagNaturalDocuments()){			
							if (listDocumentType != null && listDocumentType.size() > 0) {
								for (int i = 0; i < listDocumentType.size(); i++) {
									auxParameterTable = new ParameterTable();
									if ((listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CI.getCode()))
									|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CID.getCode())) 
									|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.PAS.getCode())) 
									|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CIEE.getCode())) 
									|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CDN.getCode()))
									|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DIO.getCode()))
									|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CIE.getCode()))
									|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DCDL.getCode()))			
									|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DCD.getCode()))
									|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DCRA.getCode()))
									|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DCRB.getCode()))					
									|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DLC.getCode()))
									|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DCC.getCode()))
									|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DCR.getCode()))){
										auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
										auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
										auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
										listDocumentTypeResult.add(auxParameterTable);
									}
								}
							}
						}
						break;

					case JURIDIC:
						if (person.isFlagNationalResident()) {
							if (listDocumentType != null && listDocumentType.size() > 0) {
								for (int i = 0; i < listDocumentType.size(); i++) {
									auxParameterTable = new ParameterTable();
									if ((listDocumentType.get(i).getParameterTablePk().equals(DocumentType.RUC.getCode()))) {

										auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
										auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
										auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
										listDocumentTypeResult.add(auxParameterTable);
									}
								}
							}
						} else if (person.isFlagNationalNotResident()) {
							//NOTHING
						} else if (person.isFlagForeignResident()) {
							if (listDocumentType != null && listDocumentType.size() > 0) {
								for (int i = 0; i < listDocumentType.size(); i++) {
									auxParameterTable = new ParameterTable();
									if (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.RUC.getCode())){
											auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
											auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
											auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
											listDocumentTypeResult.add(auxParameterTable);
									}
								}
							}
						} else if(person.isFlagForeignNotResident()){
							if (listDocumentType != null && listDocumentType.size() > 0) {
								for (int i = 0; i < listDocumentType.size(); i++) {
									auxParameterTable = new ParameterTable();
									if ((listDocumentType.get(i).getParameterTablePk().equals(DocumentType.EE.getCode()))) {
										auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
										auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
										auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
										listDocumentTypeResult.add(auxParameterTable);
									}
								}
							}
						}
						//if(person.isFlagJuridicClassTrust()) {
						//	if (listDocumentType != null && listDocumentType.size() > 0) {
						//		for (int i = 0; i < listDocumentType.size(); i++) {
						//			auxParameterTable = new ParameterTable();
						//			if ((listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CBANK.getCode()))) {
						//				auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
						//				auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
						//				listDocumentTypeResult.add(auxParameterTable);
						//			}
						//		}
						//	}
						//}
						if(person.isFlagJuridicDocuments()){
							if (listDocumentType != null && listDocumentType.size() > 0) {
								for (int i = 0; i < listDocumentType.size(); i++) {
									auxParameterTable = new ParameterTable();
									if ((listDocumentType.get(i).getParameterTablePk().equals(DocumentType.RUC.getCode())
									  ||(listDocumentType.get(i).getParameterTablePk().equals(DocumentType.EE.getCode())))){
											auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
											auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
											auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
											listDocumentTypeResult.add(auxParameterTable);
									}
								}
							}
						}
						
					default: break;
				}
			} else if(person.isFlagParticipantIndicator()){
				if (listDocumentType != null && listDocumentType.size() > 0) {
					for (int i = 0; i < listDocumentType.size(); i++) {
							auxParameterTable = new ParameterTable();
						if ((listDocumentType.get(i).getParameterTablePk().equals(DocumentType.RUC.getCode()))
						||(listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CI.getCode())) 
						||(listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CIE.getCode())) 
						||(listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DIO.getCode())) 
						||(listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CDN.getCode()))
						||(listDocumentType.get(i).getParameterTablePk().equals(DocumentType.PAS.getCode()))) {
							auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
							auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
							auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
							listDocumentTypeResult.add(auxParameterTable);
						}
					}
				}
			} else if(person.isFlagIssuerIndicator()){
				if(person.isFlagNational() || (person.isFlagForeign())){
					if(person.isFlagNational()){
						if (listDocumentType != null && listDocumentType.size() > 0) {
							for (int i = 0; i < listDocumentType.size(); i++) {
								auxParameterTable = new ParameterTable();
								if ((listDocumentType.get(i).getParameterTablePk().equals(DocumentType.RUC.getCode()))){
									auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
									auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
									auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
									listDocumentTypeResult.add(auxParameterTable);
								}
							}
						}
					} else if(person.isFlagForeign()){
						if (listDocumentType != null && listDocumentType.size() > 0) {
							for (int i = 0; i < listDocumentType.size(); i++) {
								auxParameterTable = new ParameterTable();
								if ((listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DIO.getCode()))){										
									auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
									auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
									auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
									listDocumentTypeResult.add(auxParameterTable);
								}
							}
						}
					}
				} else {
					if (listDocumentType != null && listDocumentType.size() > 0) {
						for (int i = 0; i < listDocumentType.size(); i++) {
								auxParameterTable = new ParameterTable();
							if (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.RUC.getCode())) {
								auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
								auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
								auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
								listDocumentTypeResult.add(auxParameterTable);
							}
						}
					}
				}
			}
		}
		return listDocumentTypeResult;
	}
	
	/**
	 * Gets the document type result by person
	 * Issue 1050
	 *
	 * @param person the person
	 * @return the document type result
	 */
	public List<ParameterTable> getDocumentTypeByPerson(PersonTO person) {

		List<ParameterTable> listDocumentTypeResult = new ArrayList<ParameterTable>();
		List<ParameterTable> listDocumentType = getLstDocumentType();
		ParameterTable auxParameterTable;

		if(person.getPersonType()!=null){
			// Verificando si es titular o representante legal
			if(person.isFlagHolderIndicator()||person.isFlagLegalRepresentativeIndicator()){
				switch (PersonType.get(person.getPersonType())) {
			 		case NATURAL:
		 				//verificando si es residente nacional
		 				if (person.isFlagNationalResident()){
		 					for(ParameterTable tipoDocumento: listDocumentType){ //TODO: verificar si tiene setParameterTablePk,setParameterName,setIndicator1
		 					// issue 1080 se pide que no haya validación y pida las 3 clases de documento siempre
		 						if (	tipoDocumento.getParameterTablePk().equals(DocumentType.CI.getCode())
										 || tipoDocumento.getParameterTablePk().equals(DocumentType.CIE.getCode())
										 || tipoDocumento.getParameterTablePk().equals(DocumentType.PAS.getCode()))
		 							listDocumentTypeResult.add(tipoDocumento);
		 						
		 						//verificando si es menor de edad
		 						if(person.isFlagMinorIndicator()){
		 							if (	tipoDocumento.getParameterTablePk().equals(DocumentType.CDN.getCode())	)
		 								listDocumentTypeResult.add(tipoDocumento);
		 						}
		 					}
			 			}
		 				//verificando si no es residente nacional
		 				if(person.isFlagNationalNotResident()){
		 					for(ParameterTable tipoDocumento: listDocumentType){
		 						if (	tipoDocumento.getParameterTablePk().equals(DocumentType.CI.getCode())
									 || tipoDocumento.getParameterTablePk().equals(DocumentType.CIE.getCode())
									 || tipoDocumento.getParameterTablePk().equals(DocumentType.PAS.getCode())	)
		 							listDocumentTypeResult.add(tipoDocumento);
		 						//verificando si es menor de edad
		 						if(person.isFlagMinorIndicator()){
		 							if (	tipoDocumento.getParameterTablePk().equals(DocumentType.CDN.getCode())	)
		 								listDocumentTypeResult.add(tipoDocumento);
		 						}
		 					}
		 				}
		 				//verificando si es residente extranjero
		 				if (person.isFlagForeignResident()){
		 					for(ParameterTable tipoDocumento: listDocumentType){
		 						//verificando si es menor de edad y no es discapacitado
		 						if(person.isFlagMinorIndicator()){
		 							if (	tipoDocumento.getParameterTablePk().equals(DocumentType.CI.getCode())	
		 									|| tipoDocumento.getParameterTablePk().equals(DocumentType.CIE.getCode())
		 									|| tipoDocumento.getParameterTablePk().equals(DocumentType.PAS.getCode()))
		 								listDocumentTypeResult.add(tipoDocumento);
		 						}else{
		 							if (	tipoDocumento.getParameterTablePk().equals(DocumentType.CI.getCode())
											|| tipoDocumento.getParameterTablePk().equals(DocumentType.CIE.getCode())
											|| tipoDocumento.getParameterTablePk().equals(DocumentType.PAS.getCode()))
				 							listDocumentTypeResult.add(tipoDocumento);
		 						}
		 						
		 					}
			 			}
		 				//verificando si no es residente extranjero
		 				if(person.isFlagForeignNotResident()){
		 					for(ParameterTable tipoDocumento: listDocumentType){
		 						if (	tipoDocumento.getParameterTablePk().equals(DocumentType.CIE.getCode())
									 || tipoDocumento.getParameterTablePk().equals(DocumentType.PAS.getCode())
									 || tipoDocumento.getParameterTablePk().equals(DocumentType.CI.getCode()))
		 							listDocumentTypeResult.add(tipoDocumento);
		 					}
		 				}
						if(person.isFlagNaturalDocuments()){		
							if (listDocumentType != null && listDocumentType.size() > 0) {
								for (int i = 0; i < listDocumentType.size(); i++) {
									auxParameterTable = new ParameterTable();
									if ((listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CI.getCode()))
									|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CID.getCode())) 
									|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.PAS.getCode())) 
									|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CIEE.getCode())) 
									|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CDN.getCode()))
									|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DIO.getCode()))
									|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CIE.getCode()))
									|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DCDL.getCode()))			
									|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DCD.getCode()))
									|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DCRA.getCode()))
									|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DCRB.getCode()))					
									|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DLC.getCode()))
									|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DCC.getCode()))
									|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DCR.getCode()))){
										auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
										auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
										auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
										listDocumentTypeResult.add(auxParameterTable);
									}
								}
							}
						}
						break;

					case JURIDIC:
						if (person.isFlagNationalResident()) {
							if (listDocumentType != null && listDocumentType.size() > 0) {
								for (int i = 0; i < listDocumentType.size(); i++) {
									auxParameterTable = new ParameterTable();
									if ((listDocumentType.get(i).getParameterTablePk().equals(DocumentType.RUC.getCode()))) {

										auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
										auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
										auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
										listDocumentTypeResult.add(auxParameterTable);
									}
								}
							}
						} 
//						else if (person.isFlagNationalNotResident()) {
//							//NOTHING
//						} 
						if (person.isFlagForeignResident()) {
							if (listDocumentType != null && listDocumentType.size() > 0) {
								for (int i = 0; i < listDocumentType.size(); i++) {
									auxParameterTable = new ParameterTable();
									if (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.RUC.getCode())){
											auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
											auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
											auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
											listDocumentTypeResult.add(auxParameterTable);
									}
								}
							}
						} if(person.isFlagForeignNotResident()){
							if (listDocumentType != null && listDocumentType.size() > 0) {
								for (int i = 0; i < listDocumentType.size(); i++) {
									auxParameterTable = new ParameterTable();
									if ((listDocumentType.get(i).getParameterTablePk().equals(DocumentType.RUC.getCode()))
										|| (listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DIO.getCode()))) {
										auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
										auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
										auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
										listDocumentTypeResult.add(auxParameterTable);
									}
								}
							}
						}
						//if(person.isFlagJuridicClassTrust()) {
						//	if (listDocumentType != null && listDocumentType.size() > 0) {
						//		for (int i = 0; i < listDocumentType.size(); i++) {
						//			auxParameterTable = new ParameterTable();
						//			if ((listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CBANK.getCode()))) {
						//				auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
						//				auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
						//				listDocumentTypeResult.add(auxParameterTable);
						//			}
						//		}
						//	}
						//}
						if(person.isFlagJuridicDocuments()){
							if (listDocumentType != null && listDocumentType.size() > 0) {
								for (int i = 0; i < listDocumentType.size(); i++) {
									auxParameterTable = new ParameterTable();
									if ((listDocumentType.get(i).getParameterTablePk().equals(DocumentType.RUC.getCode())
									  ||(listDocumentType.get(i).getParameterTablePk().equals(DocumentType.EE.getCode())))){
											auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
											auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
											auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
											listDocumentTypeResult.add(auxParameterTable);
									}
								}
							}
						}
						
					default: break;
				}
			} else if(person.isFlagParticipantIndicator()){
				if (listDocumentType != null && listDocumentType.size() > 0) {
					for (int i = 0; i < listDocumentType.size(); i++) {
							auxParameterTable = new ParameterTable();
						if ((listDocumentType.get(i).getParameterTablePk().equals(DocumentType.RUC.getCode()))
						||(listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CI.getCode())) 
						||(listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CIE.getCode())) 
						||(listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DIO.getCode())) 
						||(listDocumentType.get(i).getParameterTablePk().equals(DocumentType.CDN.getCode()))
						||(listDocumentType.get(i).getParameterTablePk().equals(DocumentType.PAS.getCode()))) {
							auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
							auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
							auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
							listDocumentTypeResult.add(auxParameterTable);
						}
					}
				}
			} else if(person.isFlagIssuerIndicator()){
				if(person.isFlagNational() || (person.isFlagForeign())){
					if(person.isFlagNational()){
						if (listDocumentType != null && listDocumentType.size() > 0) {
							for (int i = 0; i < listDocumentType.size(); i++) {
								auxParameterTable = new ParameterTable();
								if ((listDocumentType.get(i).getParameterTablePk().equals(DocumentType.RUC.getCode()))){
									auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
									auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
									auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
									listDocumentTypeResult.add(auxParameterTable);
								}
							}
						}
					} else if(person.isFlagForeign()){
						if (listDocumentType != null && listDocumentType.size() > 0) {
							for (int i = 0; i < listDocumentType.size(); i++) {
								auxParameterTable = new ParameterTable();
								if ((listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DIO.getCode()))){										
									auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
									auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
									auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
									listDocumentTypeResult.add(auxParameterTable);
								}
							}
						}
					}
				} else {
					if (listDocumentType != null && listDocumentType.size() > 0) {
						for (int i = 0; i < listDocumentType.size(); i++) {
								auxParameterTable = new ParameterTable();
							if ((listDocumentType.get(i).getParameterTablePk().equals(DocumentType.RUC.getCode()))||
								(listDocumentType.get(i).getParameterTablePk().equals(DocumentType.DIO.getCode()))) {
								auxParameterTable.setParameterTablePk(listDocumentType.get(i).getParameterTablePk());
								auxParameterTable.setParameterName(listDocumentType.get(i).getParameterName());
								auxParameterTable.setIndicator1(listDocumentType.get(i).getIndicator1());
								listDocumentTypeResult.add(auxParameterTable);
							}
						}
					}
				}
			}
		}
		return listDocumentTypeResult;
	}
	
	/**
	 * Gets the lst document type.
	 *
	 * @return the lst document type
	 */
	public List<ParameterTable> getLstDocumentType() {
		List<ParameterTable> listDocumentType = new ArrayList<ParameterTable>();
		try {
			
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
			listDocumentType = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return listDocumentType;
	}
	
	/**
	 * Load max lenght type.
	 *
	 * @param documentType the document type
	 * @return the document to
	 */
	public DocumentTO loadMaxLenghtType(Integer documentType){
	
		DocumentTO documentTO = new DocumentTO();
		
		if(documentType!=null && documentType.equals(DocumentType.CI.getCode())){
			documentTO.setNumericTypeDocument(false);
			documentTO.setMaxLenghtDocumentNumber(17);
		}else if (documentType!=null && documentType.equals(DocumentType.CID.getCode())) {
			documentTO.setNumericTypeDocument(true);
			documentTO.setMaxLenghtDocumentNumber(12);
		}else if (documentType!=null && documentType.equals(DocumentType.PAS.getCode())) {
			documentTO.setNumericTypeDocument(false);
			documentTO.setMaxLenghtDocumentNumber(20);
		}else if (documentType!=null && documentType.equals(DocumentType.CIEE.getCode())) {
			documentTO.setNumericTypeDocument(false);
			documentTO.setMaxLenghtDocumentNumber(17);	
		} else if (documentType!=null && documentType.equals(DocumentType.DIO.getCode())) {
			documentTO.setNumericTypeDocument(false);
			documentTO.setMaxLenghtDocumentNumber(15);
		}else if (documentType!=null && documentType.equals(DocumentType.CIE.getCode())) {
			documentTO.setNumericTypeDocument(false);
			documentTO.setMaxLenghtDocumentNumber(17);				
		}else if(documentType!=null && documentType.equals(DocumentType.DCDL.getCode())) {
			documentTO.setNumericTypeDocument(true);
			documentTO.setMaxLenghtDocumentNumber(20);		
		}else if(documentType!=null && documentType.equals(DocumentType.DCD.getCode())) {
			documentTO.setNumericTypeDocument(true);
			documentTO.setMaxLenghtDocumentNumber(20);		
		}else if(documentType!=null && documentType.equals(DocumentType.DCRA.getCode())) {
			documentTO.setNumericTypeDocument(true);
			documentTO.setMaxLenghtDocumentNumber(20);
		}else if(documentType!=null && documentType.equals(DocumentType.DCRB.getCode())) {
			documentTO.setNumericTypeDocument(true);
			documentTO.setMaxLenghtDocumentNumber(20);
		}else if(documentType!=null && documentType.equals(DocumentType.DLC.getCode())) {
			documentTO.setNumericTypeDocument(true);
			documentTO.setMaxLenghtDocumentNumber(20);	
		}else if(documentType!=null && documentType.equals(DocumentType.DCC.getCode())) {
			documentTO.setNumericTypeDocument(true);
			documentTO.setMaxLenghtDocumentNumber(20);
		}else if(documentType!=null && documentType.equals(DocumentType.DCR.getCode())) {
			documentTO.setNumericTypeDocument(true);
			documentTO.setMaxLenghtDocumentNumber(20);	
		} else if(documentType!=null && documentType.equals(DocumentType.CDN.getCode())) {
			documentTO.setNumericTypeDocument(false);
			documentTO.setMaxLenghtDocumentNumber(20);
		} else if(documentType!=null && documentType.equals(DocumentType.RUC.getCode())){
			documentTO.setNumericTypeDocument(true);
			documentTO.setMaxLenghtDocumentNumber(10);		
		}else if(documentType!=null && documentType.equals(DocumentType.EE.getCode())){
			documentTO.setNumericTypeDocument(false);
			documentTO.setMaxLenghtDocumentNumber(15);
		}
		

		return documentTO;
	}
	
	/**
	 * Validate emission rules.
	 *
	 * @param documentType the document type
	 * @return the document to
	 */
	public DocumentTO validateEmissionRules(Integer documentType){
		DocumentTO documentTO = new DocumentTO();
		
		if(documentType!=null){
			if(documentType.equals(DocumentType.CI.getCode())){
				documentTO.setIndicatorAlphanumericWithDash(true);
				documentTO.setIndicatorDocumentSource(true);
				documentTO.setNumericTypeDocument(false);
			}
			if(documentType.equals(DocumentType.CID.getCode())){
				documentTO.setIndicatorAlphanumericWithDash(false);
				documentTO.setIndicatorDocumentSource(true);
				documentTO.setNumericTypeDocument(true);
			}
			if(documentType.equals(DocumentType.CIEE.getCode())){
				documentTO.setIndicatorAlphanumericWithDash(true);
				documentTO.setIndicatorDocumentSource(false);
				documentTO.setNumericTypeDocument(false);
			}
			if(documentType.equals(DocumentType.PAS.getCode())){
				documentTO.setIndicatorAlphanumericWithDash(true);
				documentTO.setIndicatorDocumentSource(false);
				documentTO.setNumericTypeDocument(false);
			}
			if(documentType.equals(DocumentType.CIE.getCode())){
				documentTO.setIndicatorAlphanumericWithDash(true);
				documentTO.setIndicatorDocumentSource(false);
				documentTO.setNumericTypeDocument(false);
			}
			if(documentType.equals(DocumentType.CDN.getCode())){
				documentTO.setIndicatorAlphanumericWithDash(true);
				documentTO.setIndicatorDocumentSource(false);
				documentTO.setNumericTypeDocument(false);
				documentTO.setIndicatorDocumentIssuanceDate(true);
			}
			
			if(documentType.equals(DocumentType.DCD.getCode()) ||
			   documentType.equals(DocumentType.DCC.getCode()) ||
			   documentType.equals(DocumentType.DCR.getCode()) ||
			   documentType.equals(DocumentType.DCD.getCode())){
				documentTO.setIndicatorAlphanumericWithDash(false);
				documentTO.setIndicatorDocumentSource(false);
				documentTO.setNumericTypeDocument(true);
				documentTO.setIndicatorDocumentIssuanceDate(true);
			}
			
		}
		
		
		
		
		if(documentType!=null && documentType.equals(DocumentType.CDN.getCode())){
			documentTO.setIndicatorDocumentIssuanceDate(true);					
		}
		
		return documentTO;
	}

	/**
	 * Validate all zero.
	 *
	 * @param documentNumber the document number
	 * @param componentDocumentNumber the component document number
	 * @return true, if successful
	 */
	public boolean validateAllZero(String documentNumber,String componentDocumentNumber){
		String propertiesConstantsIsNotAllZero=GeneralPropertiesConstants.ERROR_VALIDATOR_IS_NOT_ALL_ZERO;
		
		boolean flag=true;
		Object[] bodyData;
		
		if(Long.parseLong(documentNumber)==0L){
			flag = false;
			bodyData = new Object[0];
			JSFUtilities
			.addContextMessage(
					componentDocumentNumber,
					FacesMessage.SEVERITY_ERROR,
					PropertiesUtilities
							.getValidationMessage(propertiesConstantsIsNotAllZero,bodyData),
					PropertiesUtilities
							.getValidationMessage(propertiesConstantsIsNotAllZero,bodyData));

					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							PropertiesUtilities.getValidationMessage(propertiesConstantsIsNotAllZero,bodyData));
		
					JSFUtilities.showSimpleValidationDialog();
		}
		return flag;

	}
	
	/**
	 * Validate format document number.
	 *
	 * @param documentType the document type
	 * @param documentNumber the document number
	 * @param componentDocumentNumber the component document number
	 * @return true, if successful
	 */
	public boolean validateFormatDocumentNumber(Integer documentType, String documentNumber, String componentDocumentNumber){
		
		String propertiesConstantsDocNumLenght15=GeneralPropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_15_DIGITS;
		String propertiesConstantsDocNumLenght10=GeneralPropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_10_DIGITS;
		String propertiesConstantsDocNumLenght17=GeneralPropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_17_DIGITS;
		String propertiesConstantsDocNumLenght6_20=GeneralPropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_6_20_DIGITS;
		String propertiesConstantsDocNumLenght6_15=GeneralPropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_6_15_DIGITS;
		String propertiesConstantsDocCountThreeZeroValid=GeneralPropertiesConstants.ERROR_DOC_COUNT_THREE_ZERO_VALID;
		String propertiesConstantsDocNumLenght12=GeneralPropertiesConstants.ERROR_VALIDATOR_DOCUMENT_TYPE_12_DIGITS;
		String propertiesConstantsDocIssuerMnemonicValid=GeneralPropertiesConstants.ERROR_DOC_EMISOR_MNEMONIC_VALID;
		String propertiesConstantsIsNumeric=GeneralPropertiesConstants.ERROR_VALIDATOR_IS_NUMERIC;
		
		boolean flag=true;
		
		if(Validations.validateIsNotNullAndNotEmpty(documentNumber)){
			
			Object[] bodyData;
			
			if(DocumentType.CI.getCode().equals(documentType) ||
					DocumentType.CIEE.getCode().equals(documentType) ||
					DocumentType.CIE.getCode().equals(documentType)){				
				
				if(documentNumber.length() > loadMaxLenghtType(documentType).getMaxLenghtDocumentNumber() ) {
					flag = false;
					
					JSFUtilities.addContextMessage(
							componentDocumentNumber,
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities.getValidationMessage(propertiesConstantsDocNumLenght17,null),
							PropertiesUtilities.getValidationMessage(propertiesConstantsDocNumLenght17,null));

							JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							PropertiesUtilities.getValidationMessage(propertiesConstantsDocNumLenght17,null));
				
							JSFUtilities.showSimpleValidationDialog();
									
				}
				// Se exonera a CIE de las validaciones de formato
				if(!DocumentType.CIE.getCode().equals(documentType)) {				
					if(!validateHyphenDocumentNumber(documentType,documentNumber,componentDocumentNumber)){
						return false;
					}else if(isNumeric(documentNumber)){
							if(!validateAllZero(documentNumber, componentDocumentNumber)){
								return false;
							}
					}
				}
			}
			else if(DocumentType.DCDL.getCode().equals(documentType)
					|| DocumentType.DCD.getCode().equals(documentType)
					|| DocumentType.DCRA.getCode().equals(documentType)
					|| DocumentType.DCRB.getCode().equals(documentType)
					|| DocumentType.DLC.getCode().equals(documentType)
					|| DocumentType.DCC.getCode().equals(documentType)
					|| DocumentType.DCR.getCode().equals(documentType)){
					
				if(documentNumber.length()>loadMaxLenghtType(documentType).getMaxLenghtDocumentNumber()) {
					flag = false;
					JSFUtilities
					.addContextMessage(
							componentDocumentNumber,
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities
									.getValidationMessage(propertiesConstantsDocNumLenght12,null),
							PropertiesUtilities
									.getValidationMessage(propertiesConstantsDocNumLenght12,null));

							JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
									PropertiesUtilities.getValidationMessage(propertiesConstantsDocNumLenght12,null));
				
							JSFUtilities.showSimpleValidationDialog();
							
				}
				else if(isNumeric(documentNumber)){
					if(!validateAllZero(documentNumber, componentDocumentNumber)){
					return false;
					}
				}
				
				else if(!validateHyphenDocumentNumber(documentType,documentNumber,componentDocumentNumber)){
					return false;
				}
			}
			
			else if(DocumentType.CID.getCode().equals(documentType)){
				if(documentNumber.length() < 6 || documentNumber.length() > 20) {
					flag = false;
										
					JSFUtilities.addContextMessage(componentDocumentNumber,
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities.getValidationMessage(propertiesConstantsDocNumLenght6_20,null),
							PropertiesUtilities.getValidationMessage(propertiesConstantsDocNumLenght6_20,null));

							JSFUtilities.showMessageOnDialog(
									PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
									PropertiesUtilities.getValidationMessage(propertiesConstantsDocNumLenght6_20,null));
				
							JSFUtilities.showSimpleValidationDialog();
							
				}
				else if(!isNumeric(documentNumber)){
					JSFUtilities
					.addContextMessage(
							componentDocumentNumber,
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities
									.getValidationMessage(propertiesConstantsIsNumeric,null),
							PropertiesUtilities
									.getValidationMessage(propertiesConstantsIsNumeric,null));

							JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
									PropertiesUtilities.getValidationMessage(propertiesConstantsIsNumeric,null));
				
							JSFUtilities.showSimpleValidationDialog();
							return false;
				}

			}
			else if(DocumentType.PAS.getCode().equals(documentType)){
				if(documentNumber.length() < 6 ||
						documentNumber.length() > 20) {
					flag = false;
										
					JSFUtilities
					.addContextMessage(
							componentDocumentNumber,
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities
								.getValidationMessage(propertiesConstantsDocNumLenght6_20,null),
							PropertiesUtilities
								.getValidationMessage(propertiesConstantsDocNumLenght6_20,null));

							JSFUtilities.showMessageOnDialog(
									PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
									PropertiesUtilities.getValidationMessage(propertiesConstantsDocNumLenght6_20,null));
				
							JSFUtilities.showSimpleValidationDialog();
							
				}
				else if(!validateCountZeroDocumentNumber(documentType,documentNumber)){
					
					flag = false;
					bodyData = new Object[1];
					bodyData[0]=DocumentType.get(documentType).getValue();
					JSFUtilities
					.addContextMessage(
							componentDocumentNumber,
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities
									.getValidationMessage(propertiesConstantsDocCountThreeZeroValid,bodyData),
							PropertiesUtilities
									.getValidationMessage(propertiesConstantsDocCountThreeZeroValid,bodyData));

							JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							PropertiesUtilities
									.getValidationMessage(propertiesConstantsDocCountThreeZeroValid,bodyData));
				
							JSFUtilities.showSimpleValidationDialog();
								
				}
				else if(isNumeric(documentNumber)){
					if(!validateAllZero(documentNumber, componentDocumentNumber)){
						return false;
					}
				}

			}
			else if(DocumentType.RUC.getCode().equals(documentType)){
				if(documentNumber.length()!=10){
					flag = false;
					bodyData = new Object[1];
					bodyData[0]=DocumentType.get(documentType).getValue();
					JSFUtilities
					.addContextMessage(
							componentDocumentNumber,
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities
								.getValidationMessage(propertiesConstantsDocNumLenght10,bodyData),
							PropertiesUtilities
								.getValidationMessage(propertiesConstantsDocNumLenght10,bodyData));

							JSFUtilities.showMessageOnDialog(
									PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
									PropertiesUtilities.getValidationMessage(propertiesConstantsDocNumLenght10,bodyData));
				
							JSFUtilities.showSimpleValidationDialog();
				}
				else if(!validateHyphenDocumentNumber(documentType,documentNumber,componentDocumentNumber)){
					return false;
				}
				else if(isNumeric(documentNumber)){
					if(!validateAllZero(documentNumber, componentDocumentNumber)){
						return false;
					}
				}
			}
			else if(DocumentType.DIO.getCode().equals(documentType) ||
					DocumentType.CDN.getCode().equals(documentType)){
				if(documentNumber.length() < 6 ||
						documentNumber.length() > 15) {
					flag = false;
										
					JSFUtilities
					.addContextMessage(
							componentDocumentNumber,
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities
								.getValidationMessage(propertiesConstantsDocNumLenght6_15,null),
							PropertiesUtilities
								.getValidationMessage(propertiesConstantsDocNumLenght6_15,null));

							JSFUtilities.showMessageOnDialog(
									PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
									PropertiesUtilities.getValidationMessage(propertiesConstantsDocNumLenght6_15,null));
				
							JSFUtilities.showSimpleValidationDialog();
							
				}
				else if(!validateCountZeroDocumentNumber(documentType,documentNumber)){
					
					flag = false;
					bodyData = new Object[1];
					bodyData[0]=DocumentType.get(documentType).getValue();
					JSFUtilities
					.addContextMessage(
							componentDocumentNumber,
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities
									.getValidationMessage(propertiesConstantsDocCountThreeZeroValid,bodyData),
							PropertiesUtilities
									.getValidationMessage(propertiesConstantsDocCountThreeZeroValid,bodyData));

							JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							PropertiesUtilities
									.getValidationMessage(propertiesConstantsDocCountThreeZeroValid,bodyData));
				
							JSFUtilities.showSimpleValidationDialog();
								
				}
				else if(isNumeric(documentNumber)){
					if(!validateAllZero(documentNumber, componentDocumentNumber)){
						return false;
					}
				}
				
			}
			else if(DocumentType.EE.getCode().equals(documentType)){
				if(documentNumber.length() > loadMaxLenghtType(documentType).getMaxLenghtDocumentNumber() ) {
					flag = false;
					
					JSFUtilities.addContextMessage(
							componentDocumentNumber,
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities.getValidationMessage(propertiesConstantsDocNumLenght15,null),
							PropertiesUtilities.getValidationMessage(propertiesConstantsDocNumLenght15,null));

							JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							PropertiesUtilities.getValidationMessage(propertiesConstantsDocNumLenght15,null));
				
							JSFUtilities.showSimpleValidationDialog();
									
				}
				else if(documentNumber.length() <= loadMaxLenghtType(documentType).getMaxLenghtDocumentNumber() ){
					String propertiesConstantsDocCountZeroValid=GeneralPropertiesConstants.ERROR_DOC_COUNT_ONE_ZERO_VALID;
					String charDocumentNumber = String.valueOf(documentNumber.charAt(0));
						if(charDocumentNumber.equals(String.valueOf(0))){
							flag = false;
							bodyData = new Object[1];
							bodyData[0]=DocumentType.get(documentType).getValue();
							JSFUtilities
							.addContextMessage(
								componentDocumentNumber,
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities
										.getValidationMessage(propertiesConstantsDocCountZeroValid,bodyData),
								PropertiesUtilities
										.getValidationMessage(propertiesConstantsDocCountZeroValid,bodyData));

								JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
										PropertiesUtilities.getValidationMessage(propertiesConstantsDocCountZeroValid,bodyData));
					
								JSFUtilities.showSimpleValidationDialog();
						
								return flag;
						}
				}

				else if(!validateIssuerDocumentNumber(documentType, documentNumber, componentDocumentNumber)){
					flag = false;
					bodyData = new Object[1];
					bodyData[0]=DocumentType.get(documentType).getValue();
					JSFUtilities
					.addContextMessage(
							componentDocumentNumber,
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities
									.getValidationMessage(propertiesConstantsDocIssuerMnemonicValid,bodyData),
							PropertiesUtilities
									.getValidationMessage(propertiesConstantsDocIssuerMnemonicValid,bodyData));

							JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							PropertiesUtilities
									.getValidationMessage(propertiesConstantsDocIssuerMnemonicValid,bodyData));
				
							JSFUtilities.showSimpleValidationDialog();
				 }
				else if(isNumeric(documentNumber)){
					if(!validateAllZero(documentNumber, componentDocumentNumber)){
						return false;
					}
				}
				else {
				JSFUtilities.setValidViewComponent(componentDocumentNumber);
				}
			
			
			}
		}
		
		return flag;
	}
	
	/**
	 * Validate count zero document number.
	 *
	 * @param documentType the document type
	 * @param documentNumber the document number
	 * @return true, if successful
	 */
	public boolean validateCountZeroDocumentNumber(Integer documentType, String documentNumber){
		int count = 0;
		
		boolean flag = true;
				
		if(DocumentType.DIO.getCode().equals(documentType) || DocumentType.PAS.getCode().equals(documentType)){
			
			for(int i=0;i<documentNumber.length();i++){
				
				String charDocumentNumber = String.valueOf(documentNumber.charAt(i));
				if(charDocumentNumber.equals(String.valueOf(0))){
					count++;
					
					if(count<=3 && i<=2){
						flag = true;
					}
					else if (count>3 && i==3){
						flag = false;
						i=documentNumber.length();
					}
				}
				
			}		
			
		}
		return flag;
	}
	
	/**
	 * Validate issuer document number.
	 *
	 * @param documentType the document type
	 * @param documentNumber the document number
	 * @param componentDocumentNumber the component document number
	 * @return true, if successful
	 */
	public boolean validateIssuerDocumentNumber(Integer documentType, String documentNumber, String componentDocumentNumber){
		boolean flag = true;
		
						String nmemonic  = documentNumber.substring(documentNumber.length()-3,documentNumber.length());
					    
					    Issuer issuer = helperComponentFacade.findIssuer(null,nmemonic);
					    if(!issuer.getEconomicActivity().equals(EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode())
					    		|| !issuer.getEconomicActivity().equals(EconomicActivityType.OTROS_SERVICIOS_FINANCIEROS.getCode())
					    		|| !issuer.getEconomicActivity().equals(EconomicActivityType.ORGANISMO_NO_GUBERNAMENTAL_NTERMEDIACION_FINANCIERA.getCode())
					    		|| !issuer.getEconomicActivity().equals(EconomicActivityType.MUTUALES.getCode())
					    		|| !issuer.getEconomicActivity().equals(EconomicActivityType.FONDOS_FINANCIEROS_PRIVADOS.getCode())
					    		|| !issuer.getEconomicActivity().equals(EconomicActivityType.FONDOS_INVERSION_MIXTOS.getCode())
					    		|| !issuer.getEconomicActivity().equals(EconomicActivityType.FONDOS_INVERSION_CERRADOS.getCode())
					    		|| !issuer.getEconomicActivity().equals(EconomicActivityType.FONDOS_INVERSION_ABIERTOS.getCode())
					    		|| !issuer.getEconomicActivity().equals(EconomicActivityType.COOPERATIVAS.getCode())
					    		|| !issuer.getEconomicActivity().equals(EconomicActivityType.BANCOS.getCode())
					    		|| !issuer.getEconomicActivity().equals(EconomicActivityType.ADMINISTRADORAS_FONDOS_PENSIONES.getCode())){
					    	
					    	flag = false;

					    } 
					
		return flag;
	}
	
	/**
	 * Validate hyphen document number.
	 *
	 * @param documentType the document type
	 * @param documentNumber the document number
	 * @param componentDocumentNumber the component document number
	 * @return true, if successful
	 */
	public boolean validateHyphenDocumentNumber(Integer documentType, String documentNumber, String componentDocumentNumber){
		
		int countHyphen = 0;
		boolean flag = true;
		int position = 0;
		Object[] bodyData;
		String charDocumentNumber="";
		int extensionMax=4;
		int extensionMin=2;
		
		Integer numericMin12=12;
		
		String propertiesConstantsDocNumHyphen=GeneralPropertiesConstants.ERROR_VALIDATOR_NUMBER_CHARACTER_HYPHEN;
		String propertiesConstantsDocNumHyphenCI=GeneralPropertiesConstants.ERROR_VALIDATOR_NUMBER_CHARACTER_HYPHEN_CI;
		String propertiesConstantsDocNumNoHyphen=GeneralPropertiesConstants.ERROR_VALIDATOR_NUMBER_CHARACTER_NO_HYPHEN;
		String propertiesConstantsDocNumMaxLength=GeneralPropertiesConstants.ERROR_VALIDATOR_NUMBER_MAX_LENGTH;
		String propertiesConstantsDocNumMaxLengthCI=GeneralPropertiesConstants.ERROR_VALIDATOR_NUMBER_MAX_LENGTH_CI;
		String propertiesConstantsDocExtensionMaxLength=GeneralPropertiesConstants.ERROR_VALIDATOR_EXTENSION_MAX_LENGTH;
		String propertiesConstantsDocEstensionMinLength=GeneralPropertiesConstants.ERROR_VALIDATOR_EXTENSION_MIN_LENGTH;
		String propertiesConstantsIsNumeric=GeneralPropertiesConstants.ERROR_VALIDATOR_IS_NUMERIC;
		
		if(DocumentType.CI.getCode().equals(documentType)
				|| DocumentType.CIEE.getCode().equals(documentType)
				|| DocumentType.CIE.getCode().equals(documentType)
				|| DocumentType.CID.getCode().equals(documentType)
				|| DocumentType.RUC.getCode().equals(documentType)){
			
			String documentNumberTemp = null;
			if(documentNumber.indexOf("E-") >= 0){
				documentNumberTemp = documentNumber;
				documentNumber = documentNumber.substring(2);				
			}
			
			if( DocumentType.RUC.getCode().equals(documentType)) {
				extensionMax=1;
				extensionMin=1;
				propertiesConstantsDocExtensionMaxLength = GeneralPropertiesConstants.ERROR_VALIDATOR_EXTENSION_MAX_LENGTH_RUC;
				propertiesConstantsDocEstensionMinLength = GeneralPropertiesConstants.ERROR_VALIDATOR_EXTENSION_MIN_LENGTH_RUC;
			}
			
			for(int i=0;i<documentNumber.length();i++){
				
				charDocumentNumber = String.valueOf(documentNumber.charAt(i));
				if(charDocumentNumber.equals(GeneralConstants.HYPHEN)){
					countHyphen++;
					if(countHyphen>0 && DocumentType.CI.getCode().equals(documentType)) {
						flag = false;
						bodyData = new Object[1];
						bodyData[0]=DocumentType.get(documentType).getValue();
						JSFUtilities
						.addContextMessage(
								componentDocumentNumber,
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities
										.getValidationMessage(propertiesConstantsDocNumHyphenCI,bodyData),
								PropertiesUtilities
										.getValidationMessage(propertiesConstantsDocNumHyphenCI,bodyData));

								JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
										PropertiesUtilities.getValidationMessage(propertiesConstantsDocNumHyphenCI,bodyData));
					
								JSFUtilities.showSimpleValidationDialog();
						
								i=documentNumber.length();
								
								return flag;
					}
					else if(countHyphen>1){
						flag = false;
						bodyData = new Object[1];
						bodyData[0]=DocumentType.get(documentType).getValue();
						JSFUtilities
						.addContextMessage(
								componentDocumentNumber,
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities
										.getValidationMessage(propertiesConstantsDocNumHyphen,bodyData),
								PropertiesUtilities
										.getValidationMessage(propertiesConstantsDocNumHyphen,bodyData));

								JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
										PropertiesUtilities.getValidationMessage(propertiesConstantsDocNumHyphen,bodyData));
					
								JSFUtilities.showSimpleValidationDialog();
						
								i=documentNumber.length();
								
								return flag;
					}
					else{
						position = i;
					}
				}
				
			}			
			     			
			if(countHyphen==1 && flag){
				if(position>numericMin12){
					flag = false;
					bodyData = new Object[1];
					bodyData[0]=DocumentType.get(documentType).getValue();
					JSFUtilities
					.addContextMessage(
							componentDocumentNumber,
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities
									.getValidationMessage(propertiesConstantsDocNumMaxLength,bodyData),
							PropertiesUtilities
									.getValidationMessage(propertiesConstantsDocNumMaxLength,bodyData));

							JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
									PropertiesUtilities.getValidationMessage(propertiesConstantsDocNumMaxLength,bodyData));
				
							JSFUtilities.showSimpleValidationDialog();
							return flag;

				}			
				else if(documentNumber.substring(position+1, documentNumber.length()).length()>extensionMax){
						flag = false;
						bodyData = new Object[1];
						bodyData[0]=DocumentType.get(documentType).getValue();
						JSFUtilities
						.addContextMessage(
							componentDocumentNumber,
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities
									.getValidationMessage(propertiesConstantsDocExtensionMaxLength,bodyData),
							PropertiesUtilities
									.getValidationMessage(propertiesConstantsDocExtensionMaxLength,bodyData));

							JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
									PropertiesUtilities.getValidationMessage(propertiesConstantsDocExtensionMaxLength,bodyData));
				
							JSFUtilities.showSimpleValidationDialog();
							return flag;

				}
				else if(documentNumber.substring(position+1, documentNumber.length()).length()<extensionMin){
					flag = false;
					bodyData = new Object[1];
					bodyData[0]=DocumentType.get(documentType).getValue();
					JSFUtilities
					.addContextMessage(
						componentDocumentNumber,
						FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities
								.getValidationMessage(propertiesConstantsDocEstensionMinLength,bodyData),
						PropertiesUtilities
								.getValidationMessage(propertiesConstantsDocEstensionMinLength,bodyData));

						JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
								PropertiesUtilities.getValidationMessage(propertiesConstantsDocEstensionMinLength,bodyData));
			
						JSFUtilities.showSimpleValidationDialog();
						return flag;

				}				
				else if(isNumeric(documentNumber.substring(0,documentNumber.indexOf("-")))){
					if(!validateAllZero(documentNumber.substring(0,documentNumber.indexOf("-")),componentDocumentNumber)){
						return false;
					}
				}
				else{
				
					flag = false;
					bodyData = new Object[0];
					JSFUtilities
					.addContextMessage(
							componentDocumentNumber,
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities
									.getValidationMessage(propertiesConstantsIsNumeric,bodyData),
							PropertiesUtilities
									.getValidationMessage(propertiesConstantsIsNumeric,bodyData));

							JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
									PropertiesUtilities.getValidationMessage(propertiesConstantsIsNumeric,bodyData));
				
							JSFUtilities.showSimpleValidationDialog();
							return flag;
					
				}		

			}
			
			if(countHyphen==0){
				if(DocumentType.CI.getCode().equals(documentType)){
					if(documentNumber.length()>7){
						flag = false;
						bodyData = new Object[1];
						bodyData[0]=DocumentType.get(documentType).getValue();
						JSFUtilities
						.addContextMessage(
								componentDocumentNumber,
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities
										.getValidationMessage(propertiesConstantsDocNumMaxLengthCI,bodyData),
								PropertiesUtilities
										.getValidationMessage(propertiesConstantsDocNumMaxLengthCI,bodyData));

								JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
										PropertiesUtilities.getValidationMessage(propertiesConstantsDocNumMaxLengthCI,bodyData));
					
								JSFUtilities.showSimpleValidationDialog();
								return flag;

						
					}else if(isNumeric(documentNumber)){
						if(!validateAllZero(documentNumber,componentDocumentNumber)){
							return false;						
						}
					}
					else{
						flag = false;
						bodyData = new Object[0];					
						JSFUtilities
						.addContextMessage(
								componentDocumentNumber,
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities
										.getValidationMessage(propertiesConstantsIsNumeric,bodyData),
								PropertiesUtilities
										.getValidationMessage(propertiesConstantsIsNumeric,bodyData));

								JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
										PropertiesUtilities.getValidationMessage(propertiesConstantsIsNumeric,bodyData));
					
								JSFUtilities.showSimpleValidationDialog();
								return flag;
					}
				}else if(DocumentType.RUC.getCode().equals(documentType)) {
					if(documentNumber.length()==10){
						flag = false;
						bodyData = new Object[1];
						bodyData[0]=DocumentType.get(documentType).getValue();
						JSFUtilities
						.addContextMessage(
								componentDocumentNumber,
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities
										.getValidationMessage(propertiesConstantsDocNumNoHyphen,bodyData),
								PropertiesUtilities
										.getValidationMessage(propertiesConstantsDocNumNoHyphen,bodyData));

								JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
										PropertiesUtilities.getValidationMessage(propertiesConstantsDocNumNoHyphen,bodyData));
					
								JSFUtilities.showSimpleValidationDialog();
								return flag;

						
					}
				}else {
					if(documentNumber.length()>numericMin12){
						flag = false;
						bodyData = new Object[1];
						bodyData[0]=DocumentType.get(documentType).getValue();
						JSFUtilities
						.addContextMessage(
								componentDocumentNumber,
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities
										.getValidationMessage(propertiesConstantsDocNumMaxLength,bodyData),
								PropertiesUtilities
										.getValidationMessage(propertiesConstantsDocNumMaxLength,bodyData));

								JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
										PropertiesUtilities.getValidationMessage(propertiesConstantsDocNumMaxLength,bodyData));
					
								JSFUtilities.showSimpleValidationDialog();
								return flag;

						
					}
					
					else if(isNumeric(documentNumber)){
						if(!validateAllZero(documentNumber,componentDocumentNumber)){
							return false;						
						}
					} 
					// Si es CIE no es obligatorio que sea solo numerico
					else if(!DocumentType.CIE.getCode().equals(documentType)){
						flag = false;
						bodyData = new Object[0];					
						JSFUtilities
						.addContextMessage(
								componentDocumentNumber,
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities
										.getValidationMessage(propertiesConstantsIsNumeric,bodyData),
								PropertiesUtilities
										.getValidationMessage(propertiesConstantsIsNumeric,bodyData));

								JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
										PropertiesUtilities.getValidationMessage(propertiesConstantsIsNumeric,bodyData));
					
								JSFUtilities.showSimpleValidationDialog();
								return flag;
					}
					
					// si se trata de un documento CIE y se guardo el documento en formato E-xxxxxxx
					if(documentNumberTemp != null){
						documentNumber = documentNumberTemp;
					}
				}
				
				
		  }
		
	}
	
		return flag;
	
	
	}
	
	/**
	 * Checks if is numeric.
	 *
	 * @param s the s
	 * @return true, if is numeric
	 */
	public boolean isNumeric(String s) {
	    try { 
	        Long.parseLong(s); 
	    } catch(NumberFormatException e) { 
	        return false; 
	    }
	    return true;
	}
	
}
