package com.pradera.core.component.helperui.extension.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.el.ELContext;
import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.ViewDepositaryBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.HelperResultTO;
import com.pradera.core.component.helperui.to.HolderHelperOutputTO;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportFilter;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class HolderExtensionHelperBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , Sep 24, 2013
 */
@ViewDepositaryBean
public class HolderExtensionHelperBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6764391465849218434L;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade 		helperComponentFacade;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade     generalParametersFacade;

	/** The log. */
	@Inject
	private transient PraderaLogger log;
	
	/** The User Info. */
	@Inject
	private transient UserInfo userInfo;
	
	/** The holder to. */
	private HolderTO holderTO = new HolderTO();
	
	/** The list participants. */
	private List<SelectItem> listParticipants;
	
	/** The doc type list. */
	private List<ParameterTable> docTypeList;
	
	/** The helper result to. */
	private HelperResultTO helperResultTO;
	
	/** The list empty. */
	private boolean listEmpty = false;
	
	/** The disabled participant. */
	private boolean disabledParticipant = false;
	
	/** The disabled holder document type. */
	private boolean disabledHolderDocumentType = false;
	
	/** The disabled holder document number. */
	private boolean disabledHolderDocumentNumber = false;
	
	/** The search filter. */
	private Integer	searchFilter;
	
	/** The list holders to. */
	private List<HolderHelperOutputTO> listHoldersTO = new ArrayList<HolderHelperOutputTO>();
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	private List<ParameterTable> personTypeList, stateList;
	
	private Long idReport;
	
	/** The max results query helper. */
	@Inject @Configurable
	Integer maxResultsQueryHelper;
	
	/**
	 * Instantiates a new holder extension helper bean.
	 */
	public HolderExtensionHelperBean() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Inits the.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException{
		fillComponents();
		helperResultTO= new HelperResultTO();
//		//VALIDATING USER TYPE
//		verifyUserType();
		holderTO.setPersonType(PersonType.JURIDIC.getCode());
		holderTO.setState(HolderStateType.REGISTERED.getCode());
	}
	
	/**
	 * Verify user type.
	 *
	 * @throws ServiceException the service exception
	 */
	public void verifyUserType(Long idReport) throws ServiceException{
		//RESTARTING PARTICIPANT LIST
		listParticipants = new ArrayList<SelectItem>();
		ValueExpression valueExpressionPartCode = null;
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		idParticipantPk=null;
		holderTO.setUserIssuer(false);
		holderTO.setUserIssuerDpf(false);
		if(userInfo.getUserAccountSession().isParticipantInstitucion()
				|| userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){ //IF USER TYPE IS PARTICIPANT OR PARTICIPANT INVESTOR
			
			Participant part = new Participant();

			//DISABLED PARTICIPANT COMBO
			disabledParticipant = true;
			
			//GETTING THE PARTICIPANT BY CODE
			part = helperComponentFacade.findParticipantsByCode(userInfo.getUserAccountSession().getParticipantCode());
			
			//SETTING GLOBAL PARTICIPANT
			idParticipantPk = part.getIdParticipantPk();
			
			//ADDING PARTICIPANT TO COMBO
//			listParticipants.add(new SelectItem(part.getIdParticipantPk(), part.getMnemonic()+Constants.DASH+part.getIdParticipantPk()+Constants.DASH+part.getDescription()));
			listParticipants.add(new SelectItem(part.getIdParticipantPk(), part.getDisplayDescriptionCode()));
			
			//SETTING FILTER TO SEARCH
			holderTO.setPartIdPk(idParticipantPk);
			
			//IF USER IS INSTITUTION TYPE PARTICIPANT INVESTOR
			//issue 679 tambien para participante 
			if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion() 
					|| userInfo.getUserAccountSession().isParticipantInstitucion()){
					//DISABLED DOCUMENT TYPE COMBO
				if(!userInfo.getUserAccountSession().isBcbInstitution() && !userInfo.getUserAccountSession().isParticipantInstitucion()){
					disabledHolderDocumentType = true;
					disabledHolderDocumentNumber = true;
					holderTO.setDocumentType(part.getDocumentType());
					holderTO.setDocumentNumber(part.getDocumentNumber());
				}else{
					disabledHolderDocumentType = false;
					disabledHolderDocumentNumber = false;
					holderTO.setDocumentType(null);
					holderTO.setDocumentNumber(null);
				}
			}
		}else{
			
			if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()
					&& userInfo.getUserAccountSession().getPartIssuerCode() != null
					&& generalParametersFacade.isReportInPartIssuerGroup(idReport)){
				idParticipantPk = userInfo.getUserAccountSession().getPartIssuerCode();
			}else{
				//GETTING partCode VALUE
				valueExpressionPartCode = context.getApplication().getExpressionFactory().createValueExpression(
																							elContext, "#{cc.attrs.partCode}", Long.class);
				
				if(Validations.validateIsNotNullAndPositive((Long)valueExpressionPartCode.getValue(elContext))){
					//SETTING GLOBAL PARTICIPANT
					idParticipantPk = (Long)valueExpressionPartCode.getValue(elContext);
				}
			}
			//FILL ALL PARTICIPANTS
			fillParticipants();

			if(idParticipantPk!=null || IsInstitutionTypeIssuerOrIssuerDPF()){
				disabledParticipant = true;
				if(IsInstitutionTypeIssuerOrIssuerDPF()){
					holderTO.setIssuerFk(userInfo.getUserAccountSession().getIssuerCode());
					if(userInfo.getUserAccountSession().isIssuerInstitucion()){
						holderTO.setUserIssuer(true);
					}					
					if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
						holderTO.setUserIssuerDpf(true);
					}					
				}
			}else{
				disabledParticipant = false;	//JH -11/01/2014
				disabledHolderDocumentType = false;
				disabledHolderDocumentNumber = false;
				idParticipantPk=null;			//JH -11/01/2014
			}
		}
	}
	
	public Boolean IsInstitutionTypeIssuerOrIssuerDPF(){
		Boolean condition = Boolean.FALSE;
		if(userInfo.getUserAccountSession().isIssuerInstitucion() || userInfo.getUserAccountSession().isIssuerDpfInstitucion())
			condition = Boolean.TRUE;
		return condition;
	}
	
	/**
	 * Open dialog.
	 *
	 * @throws ServiceException the service exception
	 */
	public void openDialog(Long idReport) throws ServiceException{
		this.idReport = idReport; 
		verifyUserType(this.idReport);
	}
	
	//FILL COMBOS
		/**
	 * Fill components.
	 *
	 * @throws ServiceException the service exception
	 */
	public void fillComponents() throws ServiceException{
		holderTO.setBlNatural(true);
		holderTO.setBlJuridic(true);
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		//DOCUMENT TYPES
		parameterTableTO.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
		docTypeList = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		//PERSON TYPES
		parameterTableTO.setMasterTableFk(MasterTableType.PERSON_TYPE.getCode());
		personTypeList = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		//STATES TYPES
		parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_STATE_TYPE.getCode());
		stateList = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
	}
		
	/**
	 * Fill participants.
	 *
	 * @throws ServiceException the service exception
	 */
	public void fillParticipants() throws ServiceException{
		Participant participant = new Participant();
		//PARTICIPANTS
		listParticipants = new ArrayList<SelectItem>();
		for(Participant part : helperComponentFacade.getLisParticipantServiceFacade(participant)) {
			listParticipants.add(new SelectItem(part.getIdParticipantPk(), part.getDisplayDescriptionCode()));
		}
	}

	//clearing by option selected
	/**
	 * Clear by option.
	 */
	public void clearByOption(){
		switch (searchFilter){
			case 1:
				holderTO.setName(null);
				holderTO.setFirstLastName(null);
				holderTO.setSecondLastName(null);
				
				holderTO.setFullName(null);
				break;
			case 2:
				holderTO.setDocumentType(-1);
				holderTO.setDocumentTypeDesc(null);
				holderTO.setDocumentNumber("");
				
				holderTO.setFullName(null);
				break;
			case 3:
				holderTO.setDocumentType(-1);
				holderTO.setDocumentTypeDesc(null);
				holderTO.setDocumentNumber("");
				
				holderTO.setName(null);
				holderTO.setFirstLastName(null);
				holderTO.setSecondLastName(null);
				break;
		}
	}
	
	//GETS RESULTS
	/**
	 * Search holders.
	 *
	 * @throws IllegalArgumentException the illegal argument exception
	 * @throws IllegalAccessException the illegal access exception
	 * @throws ServiceException the service exception
	 */
	public void searchHolders() throws IllegalArgumentException, IllegalAccessException, ServiceException{
		//verifyUserType();
		if(Validations.validateIsNullOrEmpty(holderTO.getPersonType())) {
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
												 PropertiesUtilities.getGenericMessage("holder.helper.alert.persontype"));
				JSFUtilities.showSimpleValidationDialog();
				return;
					
		}else if(Validations.validateIsNullOrEmpty(holderTO.getState())){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
											 PropertiesUtilities.getGenericMessage("holder.helper.alert.state"));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}else{
			if(Validations.validateIsNotNullAndPositive(idParticipantPk)){
				holderTO.setPartIdPk(idParticipantPk);
			}
			listHoldersTO = helperComponentFacade.searchHolderExtension(holderTO);
			if(listHoldersTO.size()==GeneralConstants.ZERO_VALUE_INTEGER){
				listEmpty = true;
			}else{
				listEmpty = false;
			}	
		}
	}
	
	/**
	 * Assign holder.
	 *
	 * @param holderSelected the holder selected
	 * @throws ServiceException the service exception
	 */
	public void assignHolder(HolderHelperOutputTO holderSelected) throws ServiceException{
		helperResultTO = new HelperResultTO();
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();

		//getting holder object from inputtext
		ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.holder}", Object.class);
		valueExpression.setValue(elContext, holderSelected);
		
		//update report filter
		ValueExpression valueExpressionReport = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.attrs.report}", ReportFilter.class);
		ReportFilter reportFilter =(ReportFilter)valueExpressionReport.getValue(elContext);
		if(reportFilter!=null) {
			reportFilter.setValue(holderSelected);
		}
		//update caller
		ValueExpression valueExpressionName = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.attrs.update}", String.class);
		String componentsId = (String)valueExpressionName.getValue(elContext);
		if(StringUtils.isNotBlank(componentsId)) {
			RequestContext.getCurrentInstance().update(CommonsUtilities.getListOfComponentsForUpdate(componentsId));
		}
		
		clearHelper();
	}
	
	//CLEAR HELPER
	/**
	 * Clear helper.
	 *
	 * @throws ServiceException the service exception
	 */
	public void clearHelper() throws ServiceException{
		listHoldersTO = new ArrayList<HolderHelperOutputTO>();
		holderTO 	  = new HolderTO();
		holderTO.setPersonType(PersonType.JURIDIC.getCode());
		holderTO.setState(HolderStateType.REGISTERED.getCode());
		verifyUserType(this.idReport);
	}
		
	public void changePersonType(){			
		holderTO.setName(null);
		holderTO.setFirstLastName(null);
		holderTO.setSecondLastName(null);
		holderTO.setFullName(null);
		if(PersonType.JURIDIC.getCode().equals(holderTO.getPersonType())){
			holderTO.setBlJuridic(false);
			holderTO.setBlNatural(true);
		}else if(PersonType.NATURAL.getCode().equals(holderTO.getPersonType())){
			holderTO.setBlNatural(false);
			holderTO.setBlJuridic(true);
		}
	}
	
	/**
	 * Gets the helper result to.
	 *
	 * @return the helper result to
	 */
	public HelperResultTO getHelperResultTO() {
		return helperResultTO;
	}

	/**
	 * Sets the helper result to.
	 *
	 * @param helperResultTO the new helper result to
	 */
	public void setHelperResultTO(HelperResultTO helperResultTO) {
		this.helperResultTO = helperResultTO;
	}

	/**
	 * Checks if is list empty.
	 *
	 * @return true, if is list empty
	 */
	public boolean isListEmpty() {
		return listEmpty;
	}

	/**
	 * Sets the list empty.
	 *
	 * @param listEmpty the new list empty
	 */
	public void setListEmpty(boolean listEmpty) {
		this.listEmpty = listEmpty;
	}

	/**
	 * Gets the list holders to.
	 *
	 * @return the list holders to
	 */
	public List<HolderHelperOutputTO> getListHoldersTO() {
		return listHoldersTO;
	}

	/**
	 * Sets the list holders to.
	 *
	 * @param listHoldersTO the new list holders to
	 */
	public void setListHoldersTO(List<HolderHelperOutputTO> listHoldersTO) {
		this.listHoldersTO = listHoldersTO;
	}

	/**
	 * Gets the holder to.
	 *
	 * @return the holder to
	 */
	public HolderTO getHolderTO() {
		return holderTO;
	}

	/**
	 * Sets the holder to.
	 *
	 * @param holderTO the new holder to
	 */
	public void setHolderTO(HolderTO holderTO) {
		this.holderTO = holderTO;
	}

	/**
	 * Gets the list participants.
	 *
	 * @return the list participants
	 */
	public List<SelectItem> getListParticipants() {
		return listParticipants;
	}

	/**
	 * Sets the list participants.
	 *
	 * @param listParticipants the new list participants
	 */
	public void setListParticipants(List<SelectItem> listParticipants) {
		this.listParticipants = listParticipants;
	}

	/**
	 * Gets the doc type list.
	 *
	 * @return the doc type list
	 */
	public List<ParameterTable> getDocTypeList() {
		return docTypeList;
	}

	/**
	 * Sets the doc type list.
	 *
	 * @param docTypeList the new doc type list
	 */
	public void setDocTypeList(List<ParameterTable> docTypeList) {
		this.docTypeList = docTypeList;
	}

	/**
	 * Gets the search filter.
	 *
	 * @return the search filter
	 */
	public Integer getSearchFilter() {
		return searchFilter;
	}

	/**
	 * Sets the search filter.
	 *
	 * @param searchFilter the new search filter
	 */
	public void setSearchFilter(Integer searchFilter) {
		this.searchFilter = searchFilter;
	}

	/**
	 * Checks if is disabled participant.
	 *
	 * @return true, if is disabled participant
	 */
	public boolean isDisabledParticipant() {
		return disabledParticipant;
	}

	/**
	 * Sets the disabled participant.
	 *
	 * @param disabledParticipant the new disabled participant
	 */
	public void setDisabledParticipant(boolean disabledParticipant) {
		this.disabledParticipant = disabledParticipant;
	}

	/**
	 * @return the disabledHolderDocumentType
	 */
	public boolean isDisabledHolderDocumentType() {
		return disabledHolderDocumentType;
	}

	/**
	 * @param disabledHolderDocumentType the disabledHolderDocumentType to set
	 */
	public void setDisabledHolderDocumentType(boolean disabledHolderDocumentType) {
		this.disabledHolderDocumentType = disabledHolderDocumentType;
	}

	/**
	 * @return the disabledHolderDocumentNumber
	 */
	public boolean isDisabledHolderDocumentNumber() {
		return disabledHolderDocumentNumber;
	}

	/**
	 * @param disabledHolderDocumentNumber the disabledHolderDocumentNumber to set
	 */
	public void setDisabledHolderDocumentNumber(boolean disabledHolderDocumentNumber) {
		this.disabledHolderDocumentNumber = disabledHolderDocumentNumber;
	}

	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	public List<ParameterTable> getPersonTypeList() {
		return personTypeList;
	}

	public void setPersonTypeList(List<ParameterTable> personTypeList) {
		this.personTypeList = personTypeList;
	}

	public List<ParameterTable> getStateList() {
		return stateList;
	}

	public void setStateList(List<ParameterTable> stateList) {
		this.stateList = stateList;
	}

	public Long getIdReport() {
		return idReport;
	}

	public void setIdReport(Long idReport) {
		this.idReport = idReport;
	}

	/**
	 * @return the maxResultsQueryHelper
	 */
	public Integer getMaxResultsQueryHelper() {
		return maxResultsQueryHelper;
	}

	/**
	 * @param maxResultsQueryHelper the maxResultsQueryHelper to set
	 */
	public void setMaxResultsQueryHelper(Integer maxResultsQueryHelper) {
		this.maxResultsQueryHelper = maxResultsQueryHelper;
	}
	
}
