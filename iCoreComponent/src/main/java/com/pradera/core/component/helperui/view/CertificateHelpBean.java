package com.pradera.core.component.helperui.view;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.el.ELContext;
import javax.el.ValueExpression;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.ui.UICompositeEvent;
import com.pradera.commons.view.ui.UICompositeType;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.SecurityFinancialDataHelpTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.PeriodType;
import com.pradera.model.issuancesecuritie.AmortizationPaymentSchedule;
import com.pradera.model.issuancesecuritie.InterestPaymentSchedule;
import com.pradera.model.issuancesecuritie.ProgramAmortizationCoupon;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.CapitalPaymentModalityType;
import com.pradera.model.issuancesecuritie.type.InterestPaymentModalityType;
import com.pradera.model.issuancesecuritie.type.InterestPeriodicityType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class SecuritiesHelperBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 25/01/2013
 */
@ApplicationScoped
@Named
public class CertificateHelpBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5036009796727734519L;
	
	/** The helper component facade. */
	//@EJB
	//HelperComponentFacade helperComponentFacade;
	
	@EJB
	private ParameterServiceBean paramServiceBean;
	/*
	// The security. 
	private Security security;
	
	private CommonsUtilities commonsUtilities; 
	
	// The isin code. 
	private String securityCode;//Cuando es invocada desde un link, siempre sera seteado por este dato
	
	private String name;//Cuando el nombre del help es enviado como parametro, invocado desde un <p:CommandLink
	
	Map<Integer, String> mapSecurityClass = new HashMap<Integer, String>();
	Map<Integer, String> mapSecurityInstrumentType = new HashMap<Integer, String>();
	Map<Integer, String> mapSecurityType = new HashMap<Integer, String>();
	Map<Integer, String> mapCurrencyType = new HashMap<Integer, String>();
	Map<Integer, String> mapInsterestType = new HashMap<Integer, String>();
	
	private String paymentWayDesc;
	private String amortizationTypeDesc;
	private String paymentPeriodDesc;
	private String amortizationOnDesc;
	
	private boolean chronogram;
	
	List<ProgramInterestCoupon> lstProgramInterestCoupon = null;
	
	List<ProgramAmortizationCoupon> lstProgramAmortizationCoupon = null;
	*/
	private AccountAnnotationOperation accountAnnotationOperation;
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() throws ServiceException {
		//commonsUtilities=new CommonsUtilities(); //issue 1057
		setHashParameter();
	}

	HashMap<Integer, String> hashMapParameters = new HashMap<Integer, String>();
	
	public void setHashParameter() {
		
		
		for(CurrencyType current: CurrencyType.list) {
			hashMapParameters.put(current.getCode(), current.getCodeIso());
		}
		
		for(PeriodType current: PeriodType.list) {
			hashMapParameters.put(current.getCode(), current.getValue());
		}
		
		for(InterestPeriodicityType current: InterestPeriodicityType.list) {
			hashMapParameters.put(current.getCode(), current.getValue());
		}

		for(SecurityClassType current: SecurityClassType.list) {
			hashMapParameters.put(current.getCode(), current.getValue());
		}
		
		for(CapitalPaymentModalityType current: CapitalPaymentModalityType.list) {
			hashMapParameters.put(current.getCode(), current.getValue());
		}
		
		for(InterestPaymentModalityType current: InterestPaymentModalityType.list) {
			hashMapParameters.put(current.getCode(), current.getValue());
		}
		
	}
	
	/**
	 * Search security handler.
	 */
	public void searchHandler() {
		/*
		lstProgramAmortizationCoupon = null;
		lstProgramInterestCoupon = null;
		chronogram = Boolean.FALSE.booleanValue();
		//MapsParameters();
		Security securityTemp = null;//Valor enviado como parametro al helper
		String securityCodeEntered = null;//Isin ingresado en el helper
		String name = null;//Nombre del helper
		
		if(securityCode!=null){//Si no es null, el isin es invocado desde un P:CommandLink
			securityCodeEntered = this.securityCode;
			name = this.name;
		}else{
			FacesContext context = FacesContext.getCurrentInstance();
			ELContext elContext = context.getELContext();
			ValueExpression isinValueExpresion = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.securityCode}", String.class);
			ValueExpression securityValueExpresion = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.securityObject}", Security.class);
			ValueExpression helpNameValueExpresion = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.name}", String.class);
			name = (String)helpNameValueExpresion.getValue(elContext);
			securityCodeEntered = (String)isinValueExpresion.getValue(elContext);
			securityTemp = (Security)securityValueExpresion.getValue(elContext);
		}
		if(securityTemp==null){
			try {
				securityTemp = helperComponentFacade.getSecurityHelpServiceFacade(securityCodeEntered);	
				setFieldDescriptions(securityTemp, null, null);
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		cleanData();
		this.security = securityTemp;
		*/
		
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		
		ValueExpression idAnnotationOperationPkValueExpresion = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.idAnnotationOperationPk}", String.class);
		Long idAnnotationOperationPk = Long.valueOf((String)idAnnotationOperationPkValueExpresion.getValue(elContext));
		
		ValueExpression helpNameValueExpresion = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.name}", String.class);
		String name = (String)helpNameValueExpresion.getValue(elContext);
		
		try {
			accountAnnotationOperation = paramServiceBean.searchAccountAnnotationOperation(idAnnotationOperationPk);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		JSFUtilities.executeJavascriptFunction("PF('certificatewDetail"+name+"').show()");
		JSFUtilities.executeJavascriptFunction("PF('wvTabView').select(0)");
	}

	public String hashParameter(Integer parameter) {
		return hashMapParameters.get(parameter);
	}
	public String hashParameterYesNo(Integer parameter) {
		return (parameter!=null && parameter.equals(1))?"SI":"NO";
	}

	public AccountAnnotationOperation getAccountAnnotationOperation() {
		return accountAnnotationOperation;
	}

	public void setAccountAnnotationOperation(AccountAnnotationOperation accountAnnotationOperation) {
		this.accountAnnotationOperation = accountAnnotationOperation;
	}
	
	
	/*
	
	public void MapsParameters(){
		try {
			ParameterTableTO paramTable = new ParameterTableTO();
			paramTable.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			paramTable.setState(GeneralConstants.ONE_VALUE_INTEGER);
			paramTable.setOrderbyParameterName(1);
			for(ParameterTable param : helperComponentFacade.getComboParameterTable(paramTable)){
				mapSecurityClass.put(param.getParameterTablePk(), param.getParameterName());
			}
			
			paramTable = new ParameterTableTO();
			paramTable.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
			paramTable.setState(GeneralConstants.ONE_VALUE_INTEGER);
			for(ParameterTable param : helperComponentFacade.getComboParameterTable(paramTable)){
				mapSecurityInstrumentType.put(param.getParameterTablePk(), param.getParameterName());
			}
			
			paramTable = new ParameterTableTO();
			paramTable.setMasterTableFk(MasterTableType.SECURITIES_TYPE.getCode());
			paramTable.setState(GeneralConstants.ONE_VALUE_INTEGER);
			for(ParameterTable param : helperComponentFacade.getComboParameterTable(paramTable)){
				mapSecurityType.put(param.getParameterTablePk(), param.getParameterName());
			}
	
			paramTable = new ParameterTableTO();
			paramTable.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			paramTable.setState(GeneralConstants.ONE_VALUE_INTEGER);
			for(ParameterTable param : helperComponentFacade.getComboParameterTable(paramTable)){
				mapCurrencyType.put(param.getParameterTablePk(), param.getParameterName());
			}

			paramTable = new ParameterTableTO();
			paramTable.setMasterTableFk(MasterTableType.INTEREST_TYPE.getCode());
			paramTable.setState(GeneralConstants.ONE_VALUE_INTEGER);
			for(ParameterTable param : helperComponentFacade.getComboParameterTable(paramTable)){
				mapInsterestType.put(param.getParameterTablePk(), param.getParameterName());
			}
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}*/
	/*
	public void setFieldDescriptions(Security securityTemp, InterestPaymentSchedule ips, AmortizationPaymentSchedule aps){
		try {
			if(securityTemp!=null){
				//security's fields
				securityTemp.setClassTypeDescription(paramServiceBean.getParameterTableById(securityTemp.getSecurityClass()).getParameterName());
				securityTemp.setSecurityTypeDescription(paramServiceBean.getParameterTableById(securityTemp.getSecurityType()).getParameterName());
				securityTemp.setInstrumentTypeDescription(paramServiceBean.getParameterTableById(securityTemp.getInstrumentType()).getParameterName());
				securityTemp.setCurrencyTypeDescription(paramServiceBean.getParameterTableById(securityTemp.getCurrency()).getParameterName());
				if(securityTemp.getPeriodicity()!=null){
					securityTemp.setPeriodicityDesc(paramServiceBean.getParameterTableById(securityTemp.getPeriodicity()).getParameterName());
				}
				if(Validations.validateIsNotNullAndNotEmpty(securityTemp.getInterestType())){
					securityTemp.setInterestTypeDescription(paramServiceBean.getParameterTableById(securityTemp.getInterestType()).getParameterName());
				}
				if(Validations.validateIsNotNull(securityTemp.getInterestType())){
					securityTemp.setInterestTypeDescription(paramServiceBean.getParameterTableById(securityTemp.getInterestType()).getParameterName());
				}
				//chronogram's fields
				if(securityTemp.getInterestPaymentModality()!=null){
					securityTemp.setInterestPaymentModalityDesc(paramServiceBean.getParameterTableById(securityTemp.getInterestPaymentModality()).getParameterName());
				}
			}else if(ips!=null){
				if(security.getInterestPaymentSchedule().getCashNominal()!=null){
					security.getInterestPaymentSchedule().setCashNominalDesc(paramServiceBean.getParameterTableById(
							security.getInterestPaymentSchedule().getCashNominal()).getParameterName());
				}
				
				if(security.getInterestPaymentSchedule().getPeriodicity()!=null){
					security.getInterestPaymentSchedule().setPeriodicityDesc(paramServiceBean.getParameterTableById(
							security.getInterestPaymentSchedule().getPeriodicity()).getParameterName());
				}
				
				if(security.getInterestPaymentSchedule().getCalendarType()!=null){
					security.getInterestPaymentSchedule().setCalendarTypeDesc(paramServiceBean.getParameterTableById(
							security.getInterestPaymentSchedule().getCalendarType()).getParameterName());
				}
				
				if(security.getInterestPaymentSchedule().getYield()!=null){
					security.getInterestPaymentSchedule().setYieldDesc(paramServiceBean.getParameterTableById(
							security.getInterestPaymentSchedule().getYield()).getParameterName());
				}
				
				if(security.getInterestPaymentSchedule().getInterestPeriodicity()!=null){
					security.getInterestPaymentSchedule().setInterestPeriodicityDesc(paramServiceBean.getParameterTableById(
							security.getInterestPaymentSchedule().getInterestPeriodicity()).getParameterName());
				}
			}else if(aps!=null){
				Integer paymentWay= security.getAmortizationPaymentSchedule().getPaymentModality();
				Integer amortType=security.getAmortizationPaymentSchedule().getAmortizationType();
				Integer amortOn=security.getAmortizationOn();
				Integer paymentPeriod=security.getAmortizationPaymentSchedule().getPeriodicity();
				
				this.setPaymentWayDesc(paramServiceBean.getParameterTableById(paymentWay).getParameterName());			
				if(amortType!=null){
					this.setAmortizationTypeDesc(paramServiceBean.getParameterTableById(amortType).getParameterName());
				}
				if(amortOn!=null){
					this.setAmortizationOnDesc(paramServiceBean.getParameterTableById(amortOn).getParameterName());
				}
				if(paymentPeriod!=null){
					this.setPaymentPeriodDesc(paramServiceBean.getParameterTableById(paymentPeriod).getParameterName());
				}
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	*/
	/*
	//TODO
	public void receiveEventToLoadSecurityHelp(@Observes @UICompositeEvent(UICompositeType.SECURITY_FINANCIAL_DATA) Object securityCode){
		String securityCodeId = null;
		String uiComponentName = null;
		Security securityTemp = null;//Valor enviado como parametro al helper
		if(securityCode instanceof SecurityFinancialDataHelpTO){
			securityCodeId = ((SecurityFinancialDataHelpTO) securityCode).getSecurityCodePk();
			uiComponentName = ((SecurityFinancialDataHelpTO) securityCode).getUiComponentName();
			
			if(securityCodeId!=null){
				try {
					securityTemp = helperComponentFacade.getSecurityHelpServiceFacade(securityCodeId);
					setFieldDescriptions(securityTemp, null, null);
				} catch (ServiceException e) {
					e.printStackTrace();
				}
			}
			cleanData();
			this.security = securityTemp;
			JSFUtilities.executeJavascriptFunction("PF('securitywDetail"+uiComponentName+"').show()");
			JSFUtilities.executeJavascriptFunction("PF('wvTabView').select(0)");
		}
	}
	
	public void initChronogram(){
		commonsUtilities=new CommonsUtilities(); //issue 1057
		lstProgramInterestCoupon = null;
		lstProgramAmortizationCoupon = null;
		chronogram = Boolean.TRUE.booleanValue();
		JSFUtilities.executeJavascriptFunction("PF('wvTabView').select(1)");
		
		if(lstProgramInterestCoupon==null || lstProgramAmortizationCoupon==null){
			try {
				if(security!=null && security.getIdSecurityCodePk()!=null){
					List<ProgramInterestCoupon> lstPIC = helperComponentFacade.getInterestChronogramFacade(security.getIdSecurityCodePk());
					List<ProgramAmortizationCoupon> lstPAC = helperComponentFacade.getAmortizationChronogramFacade(security.getIdSecurityCodePk());
					if(lstPIC.size()>0){
						lstProgramInterestCoupon = lstPIC;
						//security.getInterestPaymentSchedule().setProgramInterestCoupons(lstPIC);
						setFieldDescriptions(null, security.getInterestPaymentSchedule(), null);
					}
					if(lstPAC.size()>0){
						lstProgramAmortizationCoupon = lstPAC;
						//security.getAmortizationPaymentSchedule().setProgramAmortizationCoupons(lstPAC);
						setFieldDescriptions(null, null, security.getAmortizationPaymentSchedule());
					}
				}
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void goToInterestOrAmortizationChro(boolean flag){
		initChronogram();
		if(flag){
			JSFUtilities.executeJavascriptFunction("PF('wvTabView').select(2)");
		}else{
			JSFUtilities.executeJavascriptFunction("PF('wvTabView').select(1)");
		}
	}
	*/
	
	/*
	private void cleanData(){
		this.securityCode = null;
		this.name = null;
	}

	public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getCurrencyDescription(Integer ParameterTablePk){
		return mapCurrencyType.get(ParameterTablePk);
	}

	public String getInstrumentTypeDescription(Integer ParameterTablePk){
		return mapSecurityInstrumentType.get(ParameterTablePk);
	}

	public String getSecurityClassDescription(Integer ParameterTablePk){
		return mapSecurityClass.get(ParameterTablePk);
	}

	public String getSecurityTypeDescription(Integer ParameterTablePk){
		return mapSecurityType.get(ParameterTablePk);
	}

	public String getInterestTypeDescription(Integer ParameterTablePk){
		return mapInsterestType.get(ParameterTablePk);
	}

	public boolean isChronogram() {
		return chronogram;
	}

	public void setChronogram(boolean chronogram) {
		this.chronogram = chronogram;
	}

	public String getPaymentWayDesc() {
		return paymentWayDesc;
	}

	public void setPaymentWayDesc(String paymentWayDesc) {
		this.paymentWayDesc = paymentWayDesc;
	}

	public String getAmortizationTypeDesc() {
		return amortizationTypeDesc;
	}

	public void setAmortizationTypeDesc(String amortizationTypeDesc) {
		this.amortizationTypeDesc = amortizationTypeDesc;
	}

	public String getPaymentPeriodDesc() {
		return paymentPeriodDesc;
	}

	public List<ProgramInterestCoupon> getLstProgramInterestCoupon() {
		return lstProgramInterestCoupon;
	}

	public void setLstProgramInterestCoupon(
			List<ProgramInterestCoupon> lstProgramInterestCoupon) {
		this.lstProgramInterestCoupon = lstProgramInterestCoupon;
	}

	public List<ProgramAmortizationCoupon> getLstProgramAmortizationCoupon() {
		return lstProgramAmortizationCoupon;
	}

	public void setLstProgramAmortizationCoupon(
			List<ProgramAmortizationCoupon> lstProgramAmortizationCoupon) {
		this.lstProgramAmortizationCoupon = lstProgramAmortizationCoupon;
	}

	public void setPaymentPeriodDesc(String paymentPeriodDesc) {
		this.paymentPeriodDesc = paymentPeriodDesc;
	}

	public String getAmortizationOnDesc() {
		return amortizationOnDesc;
	}

	public void setAmortizationOnDesc(String amortizationOnDesc) {
		this.amortizationOnDesc = amortizationOnDesc;
	}
	
	public CommonsUtilities getCommonsUtilities() {
		return commonsUtilities;
	}

	public void setCommonsUtilities(CommonsUtilities commonsUtilities) {
		this.commonsUtilities = commonsUtilities;
	}*/

}