package com.pradera.core.component.helperui;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.enterprise.inject.Stereotype;
import javax.inject.Named;

import org.apache.myfaces.extensions.cdi.core.api.scope.conversation.ConversationScoped;

/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Stereotype HelperBean.
 * Grouping scope Conversation and Named
 * @author PraderaTechnologies.
 * @version 1.0 , 18/01/2013
 */
@Stereotype
@Inherited
@Named
@ConversationScoped
@Target({ TYPE, METHOD, FIELD })
@Retention(RUNTIME)
@Documented
public @interface HelperBean {

}
