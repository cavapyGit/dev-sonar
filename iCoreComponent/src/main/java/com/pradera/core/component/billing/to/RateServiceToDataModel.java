package com.pradera.core.component.billing.to;

import java.io.Serializable;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;


public class  RateServiceToDataModel extends ListDataModel<ServiceRateTo> implements SelectableDataModel<ServiceRateTo> , Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RateServiceToDataModel() {
		
	}
	
	public RateServiceToDataModel(List<ServiceRateTo> data) {
		super(data);	
	}
	
	@Override
	public ServiceRateTo getRowData(String rowKey) {
		@SuppressWarnings("unchecked")
		List<ServiceRateTo> serviceRateToList=(List<ServiceRateTo>)getWrappedData();
        for(ServiceRateTo serviceRateTo : serviceRateToList) {  
        	
            if(String.valueOf(serviceRateTo.getIdServiceRatePk()).equals(rowKey))
                return serviceRateTo;  
        }  
		return null;
	}

	@Override
	public Object getRowKey(ServiceRateTo arg0) {
		// TODO Auto-generated method stub
		return arg0.getIdServiceRatePk();
	}

	
}
