package com.pradera.core.component.business.to;

import java.io.Serializable;
import java.math.BigDecimal;

import com.pradera.model.generalparameter.type.CurrencyType;

// TODO: Auto-generated Javadoc
/**
 * The Class InstitutionCashAccountTO.
 */
public class InstitutionCashAccountTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6509733311059672538L;

	/** The id institution cash account pk. */
	private Long idInstitutionCashAccountPk;  
	
	/** The account type. */
	private Integer accountType;
	
	/** The currency. */
	private Integer currency;
	
	/** The id participant fk. */
	private Long idParticipantFk;
	
	/** The account state. */
	private Integer accountState;
	
	/** The settlement schema. */
	private Integer settlementSchema;
	
	/** The id modality group fk. */
	private Long idModalityGroupFk;
	
	/** The id issuer fk. */
	private String idIssuerFk;
	
	/** The bank description. */
	private String bankDescription;
	
	/** The bank account. */
	private String bankAccount;
	
	/** The currency description. */
	private String currencyDescription;
	
	/** The cash amount. */
	private BigDecimal cashAmount;
	
	/** The available cash amount. */
	private BigDecimal availableCashAmount;
	
	private Long indRelatedBcrd;
	
	private Integer bankState; 
	
	private String accountNumber;
	
	private Long idBcbCode;
	
	/**
	 * Gets the bank description.
	 *
	 * @return the bank description
	 */
	public String getBankDescription() {
		return bankDescription;
	}
	
	/**
	 * Sets the bank description.
	 *
	 * @param bankDescription the new bank description
	 */
	public void setBankDescription(String bankDescription) {
		this.bankDescription = bankDescription;
	}
	
	/**
	 * Gets the bank account.
	 *
	 * @return the bank account
	 */
	public String getBankAccount() {
		return bankAccount;
	}
	
	/**
	 * Sets the bank account.
	 *
	 * @param bankAccount the new bank account
	 */
	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}
	
	/**
	 * Gets the currency description.
	 *
	 * @return the currency description
	 */
	public String getCurrencyDescription() {
		if(currency != null)
			return CurrencyType.get(currency).getValue(); 
		else
		return "";
	}
	
	/**
	 * Sets the currency description.
	 *
	 * @param currencyDescription the new currency description
	 */
	public void setCurrencyDescription(String currencyDescription) {
		this.currencyDescription = currencyDescription;
	}
	
	/**
	 * Gets the cash amount.
	 *
	 * @return the cash amount
	 */
	public BigDecimal getCashAmount() {
		return cashAmount;
	}
	
	/**
	 * Sets the cash amount.
	 *
	 * @param cashAmount the new cash amount
	 */
	public void setCashAmount(BigDecimal cashAmount) {
		this.cashAmount = cashAmount;
	}
	
	/**
	 * Gets the available cash amount.
	 *
	 * @return the available cash amount
	 */
	public BigDecimal getAvailableCashAmount() {
		return availableCashAmount;
	}
	
	/**
	 * Sets the available cash amount.
	 *
	 * @param availableCashAmount the new available cash amount
	 */
	public void setAvailableCashAmount(BigDecimal availableCashAmount) {
		this.availableCashAmount = availableCashAmount;
	}

	/**
	 * Gets the account type.
	 *
	 * @return the account type
	 */
	public Integer getAccountType() {
		return accountType;
	}
	
	/**
	 * Sets the account type.
	 *
	 * @param accountType the new account type
	 */
	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}
	
	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}
	
	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	
	/**
	 * Gets the id participant fk.
	 *
	 * @return the id participant fk
	 */
	public Long getIdParticipantFk() {
		return idParticipantFk;
	}
	
	/**
	 * Sets the id participant fk.
	 *
	 * @param idParticipantFk the new id participant fk
	 */
	public void setIdParticipantFk(Long idParticipantFk) {
		this.idParticipantFk = idParticipantFk;
	}
	
	/**
	 * Gets the account state.
	 *
	 * @return the account state
	 */
	public Integer getAccountState() {
		return accountState;
	}
	
	/**
	 * Sets the account state.
	 *
	 * @param accountState the new account state
	 */
	public void setAccountState(Integer accountState) {
		this.accountState = accountState;
	}
	
	/**
	 * Gets the settlement schema.
	 *
	 * @return the settlement schema
	 */
	public Integer getSettlementSchema() {
		return settlementSchema;
	}
	
	/**
	 * Sets the settlement schema.
	 *
	 * @param settlementSchema the new settlement schema
	 */
	public void setSettlementSchema(Integer settlementSchema) {
		this.settlementSchema = settlementSchema;
	}
	
	/**
	 * Gets the id modality group fk.
	 *
	 * @return the id modality group fk
	 */
	public Long getIdModalityGroupFk() {
		return idModalityGroupFk;
	}
	
	/**
	 * Sets the id modality group fk.
	 *
	 * @param idModalityGroupFk the new id modality group fk
	 */
	public void setIdModalityGroupFk(Long idModalityGroupFk) {
		this.idModalityGroupFk = idModalityGroupFk;
	}
	
	/**
	 * Gets the id issuer fk.
	 *
	 * @return the id issuer fk
	 */
	public String getIdIssuerFk() {
		return idIssuerFk;
	}
	
	/**
	 * Sets the id issuer fk.
	 *
	 * @param idIssuerFk the new id issuer fk
	 */
	public void setIdIssuerFk(String idIssuerFk) {
		this.idIssuerFk = idIssuerFk;
	}
	
	/**
	 * Gets the id negotiation mechanism fk.
	 *
	 * @return the id negotiation mechanism fk
	 */
	public Long getIdNegotiationMechanismFk() {
		return idNegotiationMechanismFk;
	}
	
	/**
	 * Sets the id negotiation mechanism fk.
	 *
	 * @param idNegotiationMechanismFk the new id negotiation mechanism fk
	 */
	public void setIdNegotiationMechanismFk(Long idNegotiationMechanismFk) {
		this.idNegotiationMechanismFk = idNegotiationMechanismFk;
	}
	
	/**
	 * Gets the situation.
	 *
	 * @return the situation
	 */
	public Integer getSituation() {
		return situation;
	}
	
	/**
	 * Sets the situation.
	 *
	 * @param situation the new situation
	 */
	public void setSituation(Integer situation) {
		this.situation = situation;
	}
	
	/**
	 * Gets the account class.
	 *
	 * @return the account class
	 */
	public Integer getAccountClass() {
		return accountClass;
	}
	
	/**
	 * Sets the account class.
	 *
	 * @param accountClass the new account class
	 */
	public void setAccountClass(Integer accountClass) {
		this.accountClass = accountClass;
	}
	
	/**
	 * Gets the id institution cash account pk.
	 *
	 * @return the id institution cash account pk
	 */
	public Long getIdInstitutionCashAccountPk() {
		return idInstitutionCashAccountPk;
	}
	
	/**
	 * Sets the id institution cash account pk.
	 *
	 * @param idInstitutionCashAccountPk the new id institution cash account pk
	 */
	public void setIdInstitutionCashAccountPk(Long idInstitutionCashAccountPk) {
		this.idInstitutionCashAccountPk = idInstitutionCashAccountPk;
	}
	
	public Long getIndRelatedBcrd() {
		return indRelatedBcrd;
	}

	public void setIndRelatedBcrd(Long indRelatedBcrd) {
		this.indRelatedBcrd = indRelatedBcrd;
	}

	/** The id negotiation mechanism fk. */
	private Long idNegotiationMechanismFk;
	
	/** The situation. */
	private Integer situation;
	
	/** The account class. */
	private Integer accountClass;
	public Integer getBankState() {
		return bankState;
	}

	public void setBankState(Integer bankState) {
		this.bankState = bankState;
	}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the idBcbCode
	 */
	public Long getIdBcbCode() {
		return idBcbCode;
	}

	/**
	 * @param idBcbCode the idBcbCode to set
	 */
	public void setIdBcbCode(Long idBcbCode) {
		this.idBcbCode = idBcbCode;
	}
                        
}
