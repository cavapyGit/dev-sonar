package com.pradera.core.component.generalparameter.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.GeographicLocation;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class GeographicLocationServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 25/01/2013
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class GeographicLocationServiceBean extends CrudDaoServiceBean{
	
	/**
	 * Gets the list geographic location.
	 *
	 * @param filter the filter
	 * @return the list geographic location
	 * @throws ServiceException the service exception
	 */
	public List<GeographicLocation> getListGeographicLocation(GeographicLocationTO filter) throws ServiceException{
		CriteriaBuilder criteriaBuilder=em.getCriteriaBuilder();
		CriteriaQuery<GeographicLocation> criteriaQuery=criteriaBuilder.createQuery(GeographicLocation.class);
		Root<GeographicLocation> geographicLocationRoot=criteriaQuery.from(GeographicLocation.class);
		List<Predicate> criteriaParameters=new ArrayList<Predicate>();
		
		criteriaQuery.select(geographicLocationRoot);
		
		if(Validations.validateIsNotNullAndNotEmpty( filter.getGeographicLocationType() )){
			criteriaParameters.add(criteriaBuilder.equal(geographicLocationRoot.get("typeGeographicLocation"), filter.getGeographicLocationType()));
		}
		if(Validations.validateIsNotNullAndNotEmpty( filter.getState() )){
			criteriaParameters.add( criteriaBuilder.equal(geographicLocationRoot.get("state"), filter.getState()) );
		}
		if(Validations.validateIsNotNullAndNotEmpty( filter.getIndLocalCountry() )){
			criteriaParameters.add(criteriaBuilder.equal(geographicLocationRoot.get("localCountry"), filter.getIndLocalCountry()));
		}
		if(Validations.validateIsNotNullAndNotEmpty( filter.getIdLocationReferenceFk() )){
			criteriaParameters.add( criteriaBuilder.equal(
					geographicLocationRoot.get("geographicLocation").get("idGeographicLocationPk"), filter.getIdLocationReferenceFk()) );
		}
		if(criteriaParameters.size()==0){
			throw new RuntimeException("no criteria");
		}else if(criteriaParameters.size() == 1){
			criteriaQuery.where( criteriaParameters.get(0) );
		}else {
			criteriaQuery.where( criteriaBuilder.and( criteriaParameters.toArray(new Predicate[criteriaParameters.size()])  ) );
		}
		
		//Order By Name
		criteriaQuery.orderBy(criteriaBuilder.asc(geographicLocationRoot.get("name")));
		
		TypedQuery<GeographicLocation> geographicLocationCriteria = em.createQuery(criteriaQuery);
		return geographicLocationCriteria.getResultList();
	}
	
	/**
	 * Find geographic location by id service bean.
	 *
	 * @param filter the filter
	 * @return the geographic location
	 * @throws ServiceException the service exception
	 */
	public GeographicLocation findGeographicLocationByIdServiceBean(GeographicLocationTO filter) {
		try {
			
			CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
			CriteriaQuery<GeographicLocation> criteriaQuery=criteriaBuilder.createQuery(GeographicLocation.class);
			Root<GeographicLocation> geographicLocationRoot=criteriaQuery.from(GeographicLocation.class);
			List<Predicate> criteriaParameters=new ArrayList<Predicate>();
			
			criteriaQuery.select(geographicLocationRoot);
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getGeographicLocationCode())){
				criteriaParameters.add(criteriaBuilder.equal(geographicLocationRoot.get("codeGeographicLocation"), 
						filter.getGeographicLocationCode()));
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getIdGeographicLocationPk())){
				criteriaParameters.add(criteriaBuilder.equal(geographicLocationRoot.get("idGeographicLocationPk"), 
						filter.getIdGeographicLocationPk()));
			}
	
			if(Validations.validateIsNotNullAndNotEmpty( filter.getGeographicLocationType() )){
				criteriaParameters.add(criteriaBuilder.equal(geographicLocationRoot.get("typeGeographicLocation"), filter.getGeographicLocationType()));
			}
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getIdLocationReferenceFk() )){
				criteriaParameters.add( criteriaBuilder.equal(
						geographicLocationRoot.get("geographicLocation").get("idGeographicLocationPk"), filter.getIdLocationReferenceFk()) );
			}
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getCountryName() )) {
				criteriaParameters.add( criteriaBuilder.equal(
						geographicLocationRoot.get("name"), filter.getCountryName()) );
			}
			
			criteriaQuery.where( criteriaBuilder.and( criteriaParameters.toArray(new Predicate[criteriaParameters.size()])  ) );
			
			TypedQuery<GeographicLocation> geographicLocationCriteria = em.createQuery(criteriaQuery);
			return geographicLocationCriteria.getSingleResult();
		
		} catch(NoResultException ex){
		   return null;
		} catch(NonUniqueResultException ex) {
			return null;
		}
	}

}
