package com.pradera.core.component.helperui.to;

import java.io.Serializable;

import com.pradera.integration.common.validation.Validations;

/*
 * CREATED BY MARTIN ZARATE RAFAEL
 */

// CRITERIA SEARCH FOR ISSUANCE HELPER
public class IssuanceSearcherTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2377081760930446792L;

	/** The id issuer pk. */
	private String idIssuerPk;
	
	/** The instrument type. */
	private Integer instrumentType;
	
	/** The security type. */
	private Integer securityType;
	
	/** The security class. */
	private Integer securityClass;
	
	/** The id issuance code pk. */
	private String idIssuanceCodePk;
	
	/** The description. */
	private String description;
	
	/** The issuance state. */
	private Integer issuanceState;
	
	/** The issuer description. */
	private String issuerDescription;
	
	/** The issuer business name. */
	private String issuerBusinessName;
	
	/** The issuer document type. */
	private Integer issuerDocumentType;
	
	/** The issuer document number. */
	private String issuerDocumentNumber;
	
	/** The issuance to instance. 
	 * 	this attribute is useful when will need IssuanceSearcherTO instance only by idIssuanceCodePk*/
	private static IssuanceSearcherTO IssuanceSearcherTOInstance;
	
	/** The need securities. 
	 *  this attribute is useful to get Or Not Securities List on Issuance*/
	private boolean needSecurities = Boolean.FALSE;
	
	/** The need holder. */
	private boolean needHolder = Boolean.FALSE;
	
	
	/**
	 * Gets the id issuer pk.
	 *
	 * @return the id issuer pk
	 */
	public String getIdIssuerPk() {
		return idIssuerPk;
	}
	
	/**
	 * Sets the id issuer pk.
	 *
	 * @param idIssuerPk the new id issuer pk
	 */
	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}
	
	/**
	 * Gets the instrument type.
	 *
	 * @return the instrument type
	 */
	public Integer getInstrumentType() {
		return instrumentType;
	}
	
	/**
	 * Sets the instrument type.
	 *
	 * @param instrumentType the new instrument type
	 */
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}
	
	/**
	 * Gets the security type.
	 *
	 * @return the security type
	 */
	public Integer getSecurityType() {
		return securityType;
	}
	
	/**
	 * Sets the security type.
	 *
	 * @param securityType the new security type
	 */
	public void setSecurityType(Integer securityType) {
		this.securityType = securityType;
	}
	
	/**
	 * Gets the security class.
	 *
	 * @return the security class
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}
	
	/**
	 * Sets the security class.
	 *
	 * @param securityClass the new security class
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}
	
	/**
	 * Gets the id issuance code pk.
	 *
	 * @return the id issuance code pk
	 */
	public String getIdIssuanceCodePk() {
		return idIssuanceCodePk;
	}
	
	/**
	 * Sets the id issuance code pk.
	 *
	 * @param idIssuanceCodePk the new id issuance code pk
	 */
	public void setIdIssuanceCodePk(String idIssuanceCodePk) {
		this.idIssuanceCodePk = idIssuanceCodePk;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Gets the issuance state.
	 *
	 * @return the issuance state
	 */
	public Integer getIssuanceState() {
		return issuanceState;
	}
	
	/**
	 * Sets the issuance state.
	 *
	 * @param state the new issuance state
	 */
	public void setIssuanceState(Integer state) {
		this.issuanceState = state;
	}
	
	/**
	 * Gets the issuer description.
	 *
	 * @return the issuer description
	 */
	public String getIssuerDescription() {
		return issuerDescription;
	}
	
	/**
	 * Sets the issuer description.
	 *
	 * @param issuerDescription the new issuer description
	 */
	public void setIssuerDescription(String issuerDescription) {
		this.issuerDescription = issuerDescription;
	}
	
	/**
	 * Gets the instance from issuance pk.
	 *
	 * @param idIssuancePk the id issuance pk
	 * @return the instance from issuance pk
	 */
	public static IssuanceSearcherTO getInstanceFromIssuancePK(String idIssuancePk){
		if(Validations.validateIsNull(IssuanceSearcherTO.IssuanceSearcherTOInstance)){
			IssuanceSearcherTO.IssuanceSearcherTOInstance = new IssuanceSearcherTO();
		}
		IssuanceSearcherTO.IssuanceSearcherTOInstance.setIdIssuanceCodePk(idIssuancePk);
		return IssuanceSearcherTO.IssuanceSearcherTOInstance;
	}

	/**
	 * Checks if is need securities.
	 *
	 * @return true, if is need securities
	 */
	public boolean isNeedSecurities() {
		return needSecurities;
	}

	/**
	 * Sets the need securities.
	 *
	 * @param needSecurities the new need securities
	 */
	public void setNeedSecurities(boolean needSecurities) {
		this.needSecurities = needSecurities;
	}

	/**
	 * Gets the issuer document type.
	 *
	 * @return the issuer document type
	 */
	public Integer getIssuerDocumentType() {
		return issuerDocumentType;
	}

	/**
	 * Sets the issuer document type.
	 *
	 * @param issuerDocumentType the new issuer document type
	 */
	public void setIssuerDocumentType(Integer issuerDocumentType) {
		this.issuerDocumentType = issuerDocumentType;
	}

	/**
	 * Gets the issuer document number.
	 *
	 * @return the issuer document number
	 */
	public String getIssuerDocumentNumber() {
		return issuerDocumentNumber;
	}

	/**
	 * Sets the issuer document number.
	 *
	 * @param issuerDocumentNumber the new issuer document number
	 */
	public void setIssuerDocumentNumber(String issuerDocumentNumber) {
		this.issuerDocumentNumber = issuerDocumentNumber;
	}

	/**
	 * Gets the issuer business name.
	 *
	 * @return the issuer business name
	 */
	public String getIssuerBusinessName() {
		return issuerBusinessName;
	}

	/**
	 * Sets the issuer business name.
	 *
	 * @param issuerBusinessName the new issuer business name
	 */
	public void setIssuerBusinessName(String issuerBusinessName) {
		this.issuerBusinessName = issuerBusinessName;
	}

	/**
	 * Checks if is need holder.
	 *
	 * @return true, if is need holder
	 */
	public boolean isNeedHolder() {
		return needHolder;
	}

	/**
	 * Sets the need holder.
	 *
	 * @param needHolder the new need holder
	 */
	public void setNeedHolder(boolean needHolder) {
		this.needHolder = needHolder;
	}
	
}
