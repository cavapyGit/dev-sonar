package com.pradera.core.component.accounts.to;

import java.io.Serializable;
import java.math.BigDecimal;
// TODO: Auto-generated Javadoc
/**
 * The Class ParticipantUnionOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public class PuHolderAccountBalanceTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long idParticipantPk;
	private Long idHolderAccountPk;
	private String idSecurityCodePk;
	private BigDecimal totalBalance = new BigDecimal(0);
	private BigDecimal availableBalance = new BigDecimal(0);
	private BigDecimal banBalance = new BigDecimal(0);
	private BigDecimal pawnBalance = new BigDecimal(0);
	private BigDecimal otherBlockBalance = new BigDecimal(0);
	private BigDecimal reserveBalance = new BigDecimal(0);
	private BigDecimal oppositionBalance = new BigDecimal(0);
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
	public BigDecimal getTotalBalance() {
		return totalBalance;
	}
	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}
	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}
	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}
	public BigDecimal getBanBalance() {
		return banBalance;
	}
	public void setBanBalance(BigDecimal banBalance) {
		this.banBalance = banBalance;
	}
	public BigDecimal getPawnBalance() {
		return pawnBalance;
	}
	public void setPawnBalance(BigDecimal pawnBalance) {
		this.pawnBalance = pawnBalance;
	}
	public BigDecimal getOtherBlockBalance() {
		return otherBlockBalance;
	}
	public void setOtherBlockBalance(BigDecimal otherBlockBalance) {
		this.otherBlockBalance = otherBlockBalance;
	}
	public BigDecimal getReserveBalance() {
		return reserveBalance;
	}
	public void setReserveBalance(BigDecimal reserveBalance) {
		this.reserveBalance = reserveBalance;
	}
	public BigDecimal getOppositionBalance() {
		return oppositionBalance;
	}
	public void setOppositionBalance(BigDecimal oppositionBalance) {
		this.oppositionBalance = oppositionBalance;
	}

}
