package com.pradera.core.component.helperui.to;

import java.io.Serializable;

import com.pradera.integration.exception.type.ErrorServiceType;

public class HelperResultTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean error;
	private ErrorServiceType errorServiceType;

	public HelperResultTO() {
	}
	
	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public ErrorServiceType getErrorServiceType() {
		return errorServiceType;
	}

	public void setErrorServiceType(ErrorServiceType errorServiceType) {
		this.errorServiceType = errorServiceType;
	}

}
