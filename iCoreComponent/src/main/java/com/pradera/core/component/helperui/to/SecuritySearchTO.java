package com.pradera.core.component.helperui.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;

public class SecuritySearchTO implements Serializable {

	private static final long serialVersionUID = -1229032786660948360L;
	private Issuer issuer= new Issuer();
	private Issuance issuance;
	private List<ParameterTable> lstInstrumentType, lstSecurityType, lstSecurityClass, lstState;
	private Integer instrumentType, securityType, securityClass, state;
	private String description, mnemonic, securityCode;
	private boolean blNoResult;
	private GenericDataModel<Security> lstSecurity;
	private Participant participant;
	private List<Long> lstHolderAccountPk;
	
	public Issuer getIssuer() {
		return issuer;
	}
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}
	public Issuance getIssuance() {
		return issuance;
	}
	public void setIssuance(Issuance issuance) {
		this.issuance = issuance;
	}
	
	public List<Long> getLstHolderAccountPk() {
		return lstHolderAccountPk;
	}
	public void setLstHolderAccountPk(List<Long> lstHolderAccountPk) {
		this.lstHolderAccountPk = lstHolderAccountPk;
	}
	public List<ParameterTable> getLstInstrumentType() {
		return lstInstrumentType;
	}
	public void setLstInstrumentType(List<ParameterTable> lstInstrumentType) {
		this.lstInstrumentType = lstInstrumentType;
	}
	public List<ParameterTable> getLstSecurityType() {
		return lstSecurityType;
	}
	public void setLstSecurityType(List<ParameterTable> lstSecurityType) {
		this.lstSecurityType = lstSecurityType;
	}
	public List<ParameterTable> getLstSecurityClass() {
		return lstSecurityClass;
	}
	public void setLstSecurityClass(List<ParameterTable> lstSecurityClass) {
		this.lstSecurityClass = lstSecurityClass;
	}
	public List<ParameterTable> getLstState() {
		return lstState;
	}
	public void setLstState(List<ParameterTable> lstState) {
		this.lstState = lstState;
	}
	public Integer getInstrumentType() {
		return instrumentType;
	}
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}
	public Integer getSecurityType() {
		return securityType;
	}
	public void setSecurityType(Integer securityType) {
		this.securityType = securityType;
	}
	public Integer getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getMnemonic() {
		return mnemonic;
	}
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}
	public boolean isBlNoResult() {
		return blNoResult;
	}
	public void setBlNoResult(boolean blNoResult) {
		this.blNoResult = blNoResult;
	}
	public GenericDataModel<Security> getLstSecurity() {
		return lstSecurity;
	}
	public void setLstSecurity(GenericDataModel<Security> lstSecurity) {
		this.lstSecurity = lstSecurity;
	}
	public String getSecurityCode() {
		return securityCode;
	}
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
	public Participant getParticipant() {
		return participant;
	}
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}	
	
	
}
