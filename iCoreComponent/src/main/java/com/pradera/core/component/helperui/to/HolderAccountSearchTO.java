package com.pradera.core.component.helperui.to;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;

public class HolderAccountSearchTO implements Serializable {

	private static final long serialVersionUID = -1229032786660948360L;
	private Holder holder;
	private Participant participant;
	private List<ParameterTable> lstAccountType, lstState;
	private Integer state, accountType;
	private boolean blNoResult;
	private GenericDataModel<HolderAccount> lstHolderAccount;
	private Map<Integer, String> mpAccountType;
	private Map<Integer, String> mplstState;
	
	public Holder getHolder() {
		return holder;
	}
	public void setHolder(Holder holder) {
		this.holder = holder;
	}
	public Participant getParticipant() {
		return participant;
	}
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}
	public List<ParameterTable> getLstAccountType() {
		return lstAccountType;
	}
	public void setLstAccountType(List<ParameterTable> lstAccountType) {
		this.lstAccountType = lstAccountType;
	}
	public List<ParameterTable> getLstState() {
		return lstState;
	}
	public void setLstState(List<ParameterTable> lstState) {
		this.lstState = lstState;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public Integer getAccountType() {
		return accountType;
	}
	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}
	public boolean isBlNoResult() {
		return blNoResult;
	}
	public void setBlNoResult(boolean blNoResult) {
		this.blNoResult = blNoResult;
	}
	public GenericDataModel<HolderAccount> getLstHolderAccount() {
		return lstHolderAccount;
	}
	public void setLstHolderAccount(GenericDataModel<HolderAccount> lstHolderAccount) {
		this.lstHolderAccount = lstHolderAccount;
	}
	public Map<Integer, String> getMpAccountType() {
		return mpAccountType;
	}
	public void setMpAccountType(Map<Integer, String> mpAccountType) {
		this.mpAccountType = mpAccountType;
	}
	public Map<Integer, String> getMplstState() {
		return mplstState;
	}
	public void setMplstState(Map<Integer, String> mplstState) {
		this.mplstState = mplstState;
	}
}
