package com.pradera.core.component.helperui.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;

/**
 * 
 * @author Martin Zarate Rafael
 *
 */

public class AccountHelperInputTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1261999302485063857L;

	/** The accountNumber. */
	private Integer accountNumber;
	
	/** The idParticipantPK. */
	private Long 	idParticipantPK;
	
	/** The idHolderPk. */
	private Long	idHolderPk;
	
	/** The accountStatus. */
	private Integer	accountStatus;
	
	/** The docTypeHolder. */
	private	Integer	docTypeHolder;
	
	/** The docNumberHolder. */
	private String	docNumberHolder;
	
	/** The name. */
	private String name;
	
	/** The firts last name. */
	private String firstLastName;
	
	/** The second last name. */
	private String secondLastName;
	
	/** The full name. */
	private String fullName;
	
	private String accountType;
	private Holder holder;
	private Participant participant;

	private List<Long> lstHolderAccountPk;
	private boolean userIssuer;
	private boolean userIssuerDpf;
	private String issuerFk;
	
	//GETTERS AND SETTERS
	public Integer getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Long getIdParticipantPK() {
		return idParticipantPK;
	}

	public void setIdParticipantPK(Long idParticipantPK) {
		this.idParticipantPK = idParticipantPK;
	}

	public Long getIdHolderPk() {
		return idHolderPk;
	}

	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}

	public Integer getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(Integer accountStatus) {
		this.accountStatus = accountStatus;
	}

	public Integer getDocTypeHolder() {
		return docTypeHolder;
	}

	public void setDocTypeHolder(Integer docTypeHolder) {
		this.docTypeHolder = docTypeHolder;
	}

	public String getDocNumberHolder() {
		return docNumberHolder;
	}

	public void setDocNumberHolder(String docNumberHolder) {
		this.docNumberHolder = docNumberHolder;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstLastName() {
		return firstLastName;
	}

	public void setFirstLastName(String firstLastName) {
		this.firstLastName = firstLastName;
	}

	public String getSecondLastName() {
		return secondLastName;
	}

	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public Holder getHolder() {
		return holder;
	}

	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	public List<Long> getLstHolderAccountPk() {
		return lstHolderAccountPk;
	}

	public void setLstHolderAccountPk(List<Long> lstHolderAccountPk) {
		this.lstHolderAccountPk = lstHolderAccountPk;
	}

	public boolean isUserIssuer() {
		return userIssuer;
	}

	public void setUserIssuer(boolean userIssuer) {
		this.userIssuer = userIssuer;
	}

	public String getIssuerFk() {
		return issuerFk;
	}

	public void setIssuerFk(String issuerFk) {
		this.issuerFk = issuerFk;
	}

	public boolean isUserIssuerDpf() {
		return userIssuerDpf;
	}

	public void setUserIssuerDpf(boolean userIssuerDpf) {
		this.userIssuerDpf = userIssuerDpf;
	}
	
}
