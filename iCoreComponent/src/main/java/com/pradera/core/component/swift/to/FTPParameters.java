package com.pradera.core.component.swift.to;

import java.io.File;
import java.io.Serializable;
import java.util.List;

import com.pradera.commons.utils.FileData;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class FTPParameters.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 10/09/2015
 */
public class FTPParameters implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The server. */
	private String server;
	
	/** The user. */
	private String user;
	
	/** The pass. */
	private String pass;
	
	/** The local path. */
	private String localPath;
	
	/** The remote path. */
	private String remotePath;
	
	/** The temp interface file. */
	private File tempInterfaceFile;
	
	/** The report data. */
	private byte[] reportData;
	
	/** The list file data. */
	private List<FileData> listFileData;


	/**
	 * Gets the server.
	 *
	 * @return the server
	 */
	public String getServer() {
		return server;
	}
	
	/**
	 * Sets the server.
	 *
	 * @param server the new server
	 */
	public void setServer(String server) {
		this.server = server;
	}
	
	/**
	 * Gets the user.
	 *
	 * @return the user
	 */
	public String getUser() {
		return user;
	}
	
	/**
	 * Sets the user.
	 *
	 * @param user the new user
	 */
	public void setUser(String user) {
		this.user = user;
	}
	
	/**
	 * Gets the pass.
	 *
	 * @return the pass
	 */
	public String getPass() {
		return pass;
	}
	
	/**
	 * Sets the pass.
	 *
	 * @param pass the new pass
	 */
	public void setPass(String pass) {
		this.pass = pass;
	}
	
	/**
	 * Gets the local path.
	 *
	 * @return the local path
	 */
	public String getLocalPath() {
		return localPath;
	}
	
	/**
	 * Sets the local path.
	 *
	 * @param localPath the new local path
	 */
	public void setLocalPath(String localPath) {
		this.localPath = localPath;
	}
	
	/**
	 * Gets the remote path.
	 *
	 * @return the remote path
	 */
	public String getRemotePath() {
		return remotePath;
	}
	
	/**
	 * Sets the remote path.
	 *
	 * @param remotePath the new remote path
	 */
	public void setRemotePath(String remotePath) {
		this.remotePath = remotePath;
	}
	
	/**
	 * Gets the temp interface file.
	 *
	 * @return the temp interface file
	 */
	public File getTempInterfaceFile() {
		return tempInterfaceFile;
	}
	
	/**
	 * Sets the temp interface file.
	 *
	 * @param tempInterfaceFile the new temp interface file
	 */
	public void setTempInterfaceFile(File tempInterfaceFile) {
		this.tempInterfaceFile = tempInterfaceFile;
	}
	
	/**
	 * Gets the report data.
	 *
	 * @return the reportData
	 */
	public byte[] getReportData() {
		return reportData;
	}
	
	/**
	 * Sets the report data.
	 *
	 * @param reportData the reportData to set
	 */
	public void setReportData(byte[] reportData) {
		this.reportData = reportData;
	}

	/**
	 * @return the listFileData
	 */
	public List<FileData> getListFileData() {
		return listFileData;
	}

	/**
	 * @param listFileData the listFileData to set
	 */
	public void setListFileData(List<FileData> listFileData) {
		this.listFileData = listFileData;
	}
}