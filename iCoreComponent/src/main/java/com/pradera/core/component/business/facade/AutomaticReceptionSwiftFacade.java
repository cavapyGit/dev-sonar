package com.pradera.core.component.business.facade;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;
import javax.transaction.TransactionSynchronizationRegistry;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.business.to.SwiftDeposit;
import com.pradera.core.component.business.to.SwiftMessageJavaObject;
import com.pradera.core.component.business.to.SwiftMessagesDeposit;
import com.pradera.core.component.swift.facade.SwiftWithdrawlProcess;
import com.pradera.core.component.swift.service.AutomaticReceptionFundsService;
import com.pradera.core.component.swift.service.CashAccountManagementService;
import com.pradera.core.component.swift.to.FTPParameters;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.to.FundsComponentTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.funds.FundsOperationType;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.SwiftMessage;
import com.pradera.model.funds.SwiftMessageHistory;
import com.pradera.model.funds.type.AccountCashFundsType;
import com.pradera.model.funds.type.FundsOperationGroupType;
import com.pradera.model.funds.type.FundsType;
import com.pradera.model.funds.type.OperationClassType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.negotiation.MechanismModality;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.ParticipantOperation;
import com.pradera.model.negotiation.type.AccountOperationReferenceType;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.settlement.type.OperationPartType;


@Singleton
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class AutomaticReceptionSwiftFacade implements Serializable{


	private static final long serialVersionUID = 1L;


	private final  Logger logger = LoggerFactory.getLogger(AutomaticReceptionSwiftFacade.class);

	private final Integer CASH_CASH = 1;
	private final Integer TERM_CASH = 2;
	private final Integer TERM_TERM = 3;

	@EJB
	AutomaticReceptionFundsService receptionService;
//	@EJB
//	ExecutorComponentServiceBean executorComponentServiceBean;
	@EJB
	CashAccountManagementService cashAccountService;
	/*@EJB
	InstitutionCashAccountServiceBean beginEndDayService;*/
	@Inject
	SwiftWithdrawlProcess swiftWithdrawl;
	@Inject
	SwiftProcessFacade procesFacade;
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;

//	@EJB
//	private CollectionProcessServiceBean collectionProcessServiceBean;
	
	/**
	 * METHOD TO VALIDATE IF THE DAY HAS BEEN BEGAN
	 * @return
	 * @throws ServiceException
	 */
	/*public boolean existBeginDayProcess() throws ServiceException{
		BeginEndDay todayProcess = beginEndDayService.searchBeginEnd(CommonsUtilities.currentDate());
		if(Validations.validateIsNotNull(todayProcess)){
			if(BeginEndDaySituationType.OPEN.getCode().equals(todayProcess.getIndSituation())){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}*/

	/**
	 * METHOD TO VALIDATE IF EXIST CENTRALIZER CASH ACCOUNTS FOR ALL CURRENCIES
	 * @return
	 */
	public boolean validateCentralizerAccounts(){
		boolean centralizingValidation = true;
		List<Integer> currencyList = receptionService.getCurrencies();
		if(Validations.validateListIsNotNullAndNotEmpty(currencyList)){
			for(Integer currency : currencyList){
				if(CurrencyType.PYG.getCode().equals(currency) || CurrencyType.USD.getCode().equals(currency)){
					InstitutionCashAccount centAccount = receptionService.getActivatedCentralizingAccount(currency);
					if(centAccount == null){
						centralizingValidation = false;
						break;
					}
				}
			}
		}
		return centralizingValidation;
	}

	public List<SwiftDeposit> getSwiftFiles(){
		//WE INVOKE SWIFT COMPONENT TO GET SWIFT FILES MAPPED IN JAVA OBJECTS
		List<SwiftDeposit> swiftFileList = procesFacade.executeSwiftDeposit();
		return swiftFileList;
	}

	/**
	 * THIS METHOD WILL BE THE ONE WILL REGISTER FUNDS OPERATIONS, AND DO FUNCTIONAL VALIDATIONS
	 * @param depositMessage
	 * @throws SQLException 
	 * @throws SerialException
	 * @throws IOException
	 * @throws ServiceException 
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public synchronized SwiftMessageJavaObject processFundReception(SwiftMessagesDeposit depositMessage) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		Long functionalValidation = null;
		ParameterTable parameter = null;
		Integer indValidationProcess = null;
		SwiftMessage message = new SwiftMessage();
		SwiftMessageJavaObject objects = new SwiftMessageJavaObject();
		objects = getSwiftObjects(depositMessage);
		//1.- WE EXECUTE FUNCTIONAL VALIDATIONS ACCORDING TO SWIFT MESSAGE
		functionalValidation = functionalValidations(objects);
		if(functionalValidation != null){
			parameter = receptionService.getValidationProcess(functionalValidation);
			indValidationProcess = parameter.getShortInteger();
		}

		//2.- ACCORDING TO THE VALIDATION, WE CREATE THE FUND OPERATIONS
		if(FundsType.IND_OBSERVED_OPERATION.getCode().equals(indValidationProcess)){
			FundsOperation centralDepositFundOperation = new FundsOperation();
			//OBSERVED FUND OPERATION: ONLY GENERATE FUND MOVEMENT IN CENTRALIZER CASH ACCOUNT

			centralDepositFundOperation = saveFundOperation(objects,FundsType.IND_AUTOMATIC_DEPOSIT_TYPE_OBSERVED_OPERATION.getCode(),true, true);
			centralDepositFundOperation.setRegistryUser(loggerUser.getUserName());
			centralDepositFundOperation.setObservation(parameter.getParameterTablePk());
			receptionService.create(centralDepositFundOperation);
			executeFundComponent(centralDepositFundOperation,true);

			//WE SAVE THE RELATED FUND OPERATION TO THE SWIFT MESSAGE
			message.setFundsOperation(centralDepositFundOperation);

		}else if(FundsType.IND_AUTOMATIC_RETURN.getCode().equals(indValidationProcess)){
			FundsOperation centralWithdrawlFundOperation = new FundsOperation();
			//AUTOMATIC RETURN FUND OPERATION: GENERATE FUND MOVEMENT IN CENTRALIZER CASH ACCOUNT AND ALSO GENERATE AUTOMATIC FUND DEVOLUTION OPERATION

			//FULFILL THE FUND OPERATION WITHDRAWL 
			centralWithdrawlFundOperation = saveFundOperation(objects,FundsType.IND_AUTOMATIC_DEPOSIT_TYPE_AUTOMATIC_RETURN.getCode(),true, false);
			centralWithdrawlFundOperation.setFundsOperationClass(OperationClassType.SEND_FUND.getCode());
			centralWithdrawlFundOperation.setRegistryUser(loggerUser.getUserName());
			centralWithdrawlFundOperation.setObservation(parameter.getParameterTablePk());
			centralWithdrawlFundOperation.setPaymentReference(objects.getPaymentReference());
			//FULFILL THE FUND OPERATION DEPOSIT & CREATE THE FUND OPERATION DEPOSIT IN CENTRAL CASH ACCOUNT
			FundsOperation depositFundCentralOperation = saveCentralFundsOperationAutomaticReturn(centralWithdrawlFundOperation, parameter.getParameterTablePk(), loggerUser.getUserName());
			executeFundComponent(depositFundCentralOperation,true);
			//WE SAVE THE RELATED FUND OPERATION TO THE SWIFT MESSAGE
			message.setFundsOperation(depositFundCentralOperation);

			//CREATE WITHDRAWL FUND OPERATION, RELATE WITH AUTOMATIC DEPOSIT
			centralWithdrawlFundOperation.setFundsOperation(depositFundCentralOperation);
			receptionService.create(centralWithdrawlFundOperation);
			executeFundComponent(centralWithdrawlFundOperation,false);

			//WE REALIZE THE AUTOMATIC SWIFT WITHDRAWL MESSAGE
			centralWithdrawlFundOperation.setIndAutomaticDevolution(BooleanType.YES.getCode());
			//swiftWithdrawl.executeSwiftWithdrawl(centralWithdrawlFundOperation,depositMessage.getBeneficiaryBicSwiftCode(),depositMessage.getReferencePayment());

		}else{
			FundsOperation centralDepositFundOperation = new FundsOperation();
			//CONFIRMED FUND OPERATION: GENERATE FUND MOVEMENT IN CENTRALIZER CASH ACCOUNT AND IN INSTITUTION CASH ACCOUNT

			//CREATE THE DEPOSIT FUND OPERATION IN CENTRAL CASH ACCOUNT
			centralDepositFundOperation = saveFundOperation(objects,FundsType.IND_AUTOMATIC_DEPOSIT_TYPE_SUCCESS.getCode(),true, false);
			centralDepositFundOperation.setRegistryUser(loggerUser.getUserName());
			receptionService.create(centralDepositFundOperation);
			executeFundComponent(centralDepositFundOperation,true);

			//CREATE FUND OPERATION IN THE RESPECTIVE INSTITUTION CASH ACCOUNT
			FundsOperation cashAccountOperation = saveFundOperation(objects,FundsType.IND_AUTOMATIC_DEPOSIT_TYPE_SUCCESS.getCode(),false, false);
			InstitutionCashAccount cashAccount = getCashAccount(objects);
			cashAccountOperation.setRegistryUser(loggerUser.getUserName());
			cashAccountOperation.setInstitutionCashAccount(cashAccount);
			cashAccountOperation.setFundsOperation(centralDepositFundOperation);
			receptionService.create(cashAccountOperation);
			executeFundComponent(cashAccountOperation,true);

			//FOR RETURN FUND OPERATION, WE MUST CONFIRM THE FUND OPERATION 
			confirmPreviousManualFundOperation(objects);

			message.setFundsOperation(centralDepositFundOperation);

			//UPDATE THE INDICATOR FUNDS DEPOSITED
			updateFundsDepositedIndicator(objects);

			//THE FUND DEPOSIT WAS CONFIRMED, WE MUST TRY TO REALIZE THE COLLECTION OF OPERATIONS
			String settlementSchema = objects.getSettlementSchema();
			if(StringUtils.equalsIgnoreCase(FundsType.IND_SETTLEMENT_SCHEMA_GROSS.getStrCode(),settlementSchema)){
				objects.setCollectOperation(true);
			}
		}

		//4.- WE MUST CREATE THE SWIFT MESSAGE REFERENCE RELATED TO THE FUNDS OPERATION
		createSwiftMessageReference(message, depositMessage,indValidationProcess);

		return objects;
	}

	/**
	 * METHOD TO DO THE COLLECTION OF OPERATIONS
	 * @param object
	 * @throws ServiceException 
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public synchronized void fundOperationCollect(SwiftMessageJavaObject object) throws Exception{
		if(object.isCollectOperation()){
			Long idMechanismOperation = object.getMechanismOperation().getIdMechanismOperationPk();
			MechanismOperation mechanismOperation = receptionService.find(idMechanismOperation,MechanismOperation.class);
			Long mechanism = mechanismOperation.getMechanisnModality().getNegotiationMechanism().getIdNegotiationMechanismPk();
			Long modality = mechanismOperation.getMechanisnModality().getNegotiationModality().getIdNegotiationModalityPk();
			Long modalityGroup = receptionService.getModalityGroup(mechanism, modality);
			Long participant = object.getParticipant().getIdParticipantPk();
			Integer currency = object.getCurrency();
			Integer settlementSchema = mechanismOperation.getSettlementSchema();

//			InstitutionCashAccount objInstitutionCashAccount = collectionProcessServiceBean.getInstitutionCashAccount(mechanism,modalityGroup,participant,currency,settlementSchema);
//			if (Validations.validateIsNull(objInstitutionCashAccount)) {
//				throw new ServiceException(ErrorServiceType.CASH_ACCOUNT_NOTEXISTS, ErrorServiceType.CASH_ACCOUNT_NOTEXISTS.getMessage());
//			}

			grossSettlementPaymentOperations(object);
		}
		
	}

	/**
	 * METHOD WHICH WILL DO FUNCTIONAL VALIDATIONS ACCORDING TO THE OPERATION GROUP
	 * @param operationGroup
	 * @return
	 */
	private Long functionalValidations(SwiftMessageJavaObject depositMessage){
		Long returnValidation = null;
		Integer currency = depositMessage.getCurrency();
		String trn = depositMessage.getTrn();
		String paymentReference = depositMessage.getPaymentReference();
		String amount = depositMessage.getAmount();
		String issuerStr = depositMessage.getIssuer();


		//1.- VALIDATION IF THE TRN IS CORRECT
		if(	StringUtils.equalsIgnoreCase(FundsType.TRN_E000900.getStrCode(),trn) ||  
			StringUtils.equalsIgnoreCase(FundsType.TRN_E000901.getStrCode(),trn)){

			//FOR THIS KIND OF TRN, WE MUST VERIFIED THE ISSUER ASSOCIATED TO THE SWIFT MESSAGE IF IT IS ACTIVATED AND IT EXISTS
			Issuer issuer = receptionService.validateActiveIssuer(issuerStr);
			if(issuer == null){
				returnValidation = MasterTableType.PARAMETER_TABLE_TRN_900_901.getLngCode();
			}else{
				returnValidation = null;
			}
		}else if(	StringUtils.equalsIgnoreCase(FundsType.TRN_E000399.getStrCode(),trn) ||  
					StringUtils.equalsIgnoreCase(FundsType.TRN_E000400.getStrCode(),trn)){

			returnValidation = MasterTableType.PARAMETER_TABLE_TRN_399_400.getLngCode();
		}else if(	StringUtils.equalsIgnoreCase(FundsType.TRN_E000149.getStrCode(),trn) ||  
					StringUtils.equalsIgnoreCase(FundsType.TRN_E000340.getStrCode(),trn)){

			returnValidation = MasterTableType.PARAMETER_TABLE_WRONG_TRN.getLngCode();
		}else{
			//2.- FUNCTIONAL VALIDATIONS MUST BE IMPLEMENTED ACCORDIND TO THE FUNDS OPERATION TYPE
//			FundsOperationType fundOperationType = receptionService.findSwiftComponent(trn,currency);
//			Integer operationGroup = fundOperationType.getOperationGroup();
			//we got the operation group according the TRN field from swift message
			Integer operationGroup = receptionService.getAutomaticReceptionFundsOperationGroup(trn);
			if (Validations.validateIsNull(operationGroup)) {
				//if the operation group is null we must register the funds only in the central cash account (we don't know the main operation group)
				operationGroup= FundsOperationGroupType.GROUP_OPERATION_CENTRALACCOUNT.getCode();
				returnValidation= MasterTableType.PARAMETER_TABLE_AMBIGUOUS_GROUP.getLngCode(); 
			}
			depositMessage.setOperationGroup(operationGroup);
			//Long operationType = fundOperationType.getIdFundsOperationTypePk();

			if(MasterTableType.PARAMETER_TABLE_OPERATION_GROUP_RATES.getCode().equals(operationGroup)){
				//TARIFAS
				returnValidation = FundsType.IND_FUNCTIONAL_VALIDATIONS_OBSERVED.getLngCode();

			}else if(	MasterTableType.PARAMETER_TABLE_OPERATION_GROUP_BENEFITS.getCode().equals(operationGroup) || 
						FundsOperationGroupType.SPECIAL_OPERATION.getCode().equals(operationGroup)){
				//BENEFITS OR SPECIAL PAYMENTS
				String participantBIC = depositMessage.getParticipantBic();
				if(Validations.validateIsNotNull(issuerStr)){
					//AUTOMATIC SWIFT DEPOSIT FOR BENEFITS
					depositMessage.setBlSpecialPayment(false);
					Issuer issuer = receptionService.validateActiveIssuer(issuerStr);
					if(Validations.validateIsNotNull(issuer)){
						InstitutionCashAccount cashAccount = receptionService.getDestinyBenefitCashAccountFunds(AccountCashFundsType.BENEFIT.getCode(),currency,issuerStr);
						if(Validations.validateIsNotNull(cashAccount)){
							List<CorporativeOperation> lstCorporativeOperation = receptionService.existPaymentIssuer(issuerStr,currency);
							Date today = CommonsUtilities.currentDate();
							for(CorporativeOperation corporative : lstCorporativeOperation){
								if(Validations.validateIsNotNull(corporative)){
									if(today.compareTo(corporative.getDeliveryDate()) == FundsType.EQUAL_DATE.getCode()){
										returnValidation = null;
										break;
									}else{
										returnValidation = MasterTableType.PARAMETER_TABLE_WRONG_PAYMENT_DATE.getLngCode();
									}
								}else{
									returnValidation = MasterTableType.PARAMETER_TABLE_WRONG_ISSUER_NEMONIC.getLngCode();
								}
							}
						}else{
							returnValidation = MasterTableType.PARAMETER_TABLE_NOT_EXIST_CASH_ACCOUNT.getLngCode();
						}
					}else{
						returnValidation = MasterTableType.PARAMETER_TABLE_WRONG_ISSUER_NEMONIC.getLngCode();
					}
				}else if(Validations.validateIsNotNull(participantBIC)){
					depositMessage.setBlSpecialPayment(true);
					Long participantPK = receptionService.getParticipantByBIC(participantBIC);
					if(Validations.validateIsNotNull(participantPK)){
						InstitutionCashAccount specialCashAccount = receptionService.validateSpecialPaymentCashAccount(participantPK, currency);
						if(Validations.validateIsNull(specialCashAccount)){
							returnValidation = MasterTableType.PARAMETER_TABLE_NOT_EXIST_CASH_ACCOUNT.getLngCode();
						}else{
							returnValidation = MasterTableType.PARAMETER_TABLE_SPECIAL_PAYMENT.getLngCode();
						}
					}else{
						returnValidation = MasterTableType.PARAMETER_TABLE_WRONG_PARTICIPANT_SETTLER.getLngCode();
					}
				}else{
					
				}
			}else if(MasterTableType.PARAMETER_TABLE_OPERATION_GROUP_RETURN.getCode().equals(operationGroup)){
				//DEVOLUCIONES
				returnValidation = MasterTableType.PARAMETER_TABLE_NOT_EXIST_REPAYMENT_OPERATION.getLngCode();
				/*
				FundsOperation registerOperation = receptionService.searchRegisterFundOperation(paymentReference);
				if(Validations.validateIsNotNull(registerOperation)){
					Integer fundOpeType = Integer.parseInt(registerOperation.getFundsOperationType().toString());
					//DEPOSITO POR RESERVA DE DERECHOS DEVUELTOS
					//FUNDS GOES FROM THE COMMERCIAL CASH ACCOUNT TO THE CENTRALIZER CASH ACCOUNT
					if(fundOpeType.equals(com.pradera.model.component.type.FundsOperationType.RETURN_BENEFITS_SUPPLY_DEPOSIT.getCode())){
						//VALIDATE IF THE ISSUER IS IN ACTIVE STATE
						String issuerCode = depositMessage.getIssuer();
						Issuer issuer = receptionService.validateActiveIssuer(issuerCode);
						if(Validations.validateIsNotNull(issuer)){
							//VALIDATE IF THE INSTITUTION CASH ACCOUNT FOR DEVOLUTIONS IS ACTIVATE
							InstitutionCashAccount returnCashAccount = receptionService.getReturnCashAccount(depositMessage.getCurrency());
							if(Validations.validateIsNotNull(returnCashAccount)){
								//VALIDATE IF EXIST A CORPORATIVE OPERATION TO PAY TODAY
								List<CorporativeOperation> lstCorporativeOperation = receptionService.existPaymentIssuer(issuerStr,currency);
								if(Validations.validateListIsNotNullAndNotEmpty(lstCorporativeOperation)){
									returnValidation = null;
								}else{
									returnValidation = MasterTableType.PARAMETER_TABLE_WRONG_PAYMENT_DATE.getLngCode();
								}
							}else{
								returnValidation = MasterTableType.PARAMETER_TABLE_NOT_EXIST_CASH_ACCOUNT.getLngCode();
							}
						}else{
							returnValidation = MasterTableType.PARAMETER_TABLE_WRONG_ISSUER_NEMONIC.getLngCode();
						}
					}else if(fundOpeType.equals(com.pradera.model.component.type.FundsOperationType.UNPAYED_BENEFITS_RETURN_DEPOSIT.getCode())){
						String bic = depositMessage.getParticipantBic();
						Long idBank = receptionService.getBankIdByBic(bic);
						if(Validations.validateIsNotNull(idBank)){
							Long idAllocationPayment = receptionService.validateExistPaymentBank(amount,currency,idBank);
							if(idAllocationPayment != null){
								returnValidation = null;
							}else{
								returnValidation = MasterTableType.PARAMETER_TABLE_NOT_EXIST_REPAYMENT_OPERATION.getLngCode();
							}
						}else{
							returnValidation = MasterTableType.PARAMETER_TABLE_NOT_EXIST_REPAYMENT_OPERATION.getLngCode();
						}
					}
				}else{
					returnValidation = MasterTableType.PARAMETER_TABLE_NOT_EXIST_REPAYMENT_OPERATION.getLngCode();
				}*/
			}else if(MasterTableType.PARAMETER_TABLE_OPERATION_GROUP_SETTLEMENT.getCode().equals(operationGroup)){
				//LIQUIDACIONES
				MechanismOperation operation = depositMessage.getMechanismOperation();
				String settlementSchema = null;
				//VALIDATE THE OPERATION EXIST
				if(operation != null){
					settlementSchema = operation.getSettlementSchema().toString();
				}else{
					return MasterTableType.PARAMETER_TABLE_NOT_EXIST_OPERATION.getLngCode();
				}

				//VALIDATE THE PAYMENT REFERENCE; IN CASE ITS INCOMPLETE(DOESNT MANAGE A CORRELATIVE) WE MUST GENERATE OBSERVED OPERATION
				int index = paymentReference.indexOf(".",9);
				if(index == -1){
					return MasterTableType.PARAMETER_TABLE_PAYMENT_REFENCE_INCOMPLETE.getLngCode();
				}

				if(StringUtils.equalsIgnoreCase(FundsType.IND_SETTLEMENT_SCHEMA_GROSS.getStrCode(),settlementSchema)){
					//FOR GROSS SCHEMA, MUST VALIDATE OPERATION EXIST
					Date today = CommonsUtilities.currentDate();
					MechanismModality mechanismModality = operation.getMechanisnModality();
					NegotiationModality modality = mechanismModality.getNegotiationModality();
					//SYSTEM MUST VALIDATE THAT THIS OPERATION WILL BE SETTLED TODAY
					Integer termSettlement = modality.getIndTermSettlement();
					if(termSettlement == null || FundsType.IND_MODALITY_NOT_MANAGE_TERM.getCode().equals(termSettlement)){
						//IF OPERATION HAVE FD OR FC
//						if(	AccountOperationReferenceType.DEPOSITED_FUNDS.getCode().equals(operation.getCashFundsReference()) || 
//							AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode().equals(operation.getCashFundsReference())){
//
//							returnValidation = MasterTableType.PARAMETER_TABLE_OPERATION_PAID.getLngCode();
//						}else{
							//IF OPERATION MODALITY, DOESNT MANAGE TERM, WE MUST COMPARE WITH CASH SETTLEMENT DATE
							Integer operationState = operation.getOperationState();
							if(	MasterTableType.PARAMETER_TABLE_OPERATION_STATE_ASSIGNED.getLngCode().intValue()==(operationState.intValue()) ||
								MasterTableType.PARAMETER_TABLE_OPERATION_STATE_REGISTERED.getLngCode().intValue()==(operationState.intValue())){
								//WE SET THE INDICATOR CASH OR TERM ACCORDING TO THE SETTLEMENT TYPE
								Integer indCashTerm = OperationPartType.CASH_PART.getCode();
								//WE VERIFY THE INDICATORS PREPAID - EXTENDED PAYMENT
//								Integer indPrePayment = operation.getIndCashPrepaid();
//								Integer indExtendedPayment = operation.getIndCashExtended();
//								Date settlementDate = null;
//								if(BooleanType.YES.getCode().equals(indPrePayment)){
//									settlementDate = operation.getCashPrepaidDate();
//								}else if(BooleanType.YES.getCode().equals(indExtendedPayment)){
//									settlementDate = operation.getCashExtendedDate();
//								}else{
//									settlementDate = operation.getCashSettlementDate();
//								}
	//								depositMessage.setIndCashTerm(indCashTerm);
	//								if(	today.compareTo(settlementDate) == FundsType.EQUAL_DATE.getCode() && 
	//									today.compareTo(depositMessage.getSettlementDate()) == FundsType.EQUAL_DATE.getCode()){
	//									//IF THE DATE IS THE CORRECT FOR THE PAY, WE MUST VALIDATE THE CURRENCY OF THE OPERATION
	//									String currencyOperation = operation.getCurrency().toString();
	//									String currencySwift = currency.toString();
	//									if(StringUtils.equalsIgnoreCase(currencyOperation, currencySwift)){
	//										//WE MUST ALSO VERIFY THE BIC CODE
	//										String bicCode = depositMessage.getParticipantBic();
	//										if(Validations.validateIsNotNull(depositMessage.getParticipant())){
	//											Long participantPK = depositMessage.getParticipant().getIdParticipantPk();
	//											returnValidation = validateBuyerEntity(operation,bicCode,indCashTerm,currency,CASH_CASH,participantPK,depositMessage);
	//										}else{
	//											returnValidation = MasterTableType.PARAMETER_TABLE_WRONG_PARTICIPANT_SETTLER.getLngCode();
	//										}
	//									}else{
	//										returnValidation = MasterTableType.PARAMETER_TABLE_WRONG_CURRENCY.getLngCode();
	//									}
	//								}else{
	//									returnValidation = MasterTableType.PARAMETER_TABLE_WRONG_SETTLEMENT_DATE.getLngCode();
	//								}
							}else{
								returnValidation = MasterTableType.PARAMETER_TABLE_WRONG_OPERATION_STATE.getLngCode();
							}
//						}
					}else{
						//IF OPERATION MODALITY MANAGE TERM, WE MUST COMPARE WITH TERM SETTLEMENT DATE
						Integer operationState = operation.getOperationState();
						if(MasterTableType.PARAMETER_TABLE_OPERATION_STATE_ASSIGNED.getLngCode().intValue()==(operationState.intValue()) || 
							MasterTableType.PARAMETER_TABLE_OPERATION_STATE_REGISTERED.getLngCode().intValue()==(operationState.intValue())){
							//WE SET THE INDICATOR CASH OR TERM ACCORDING TO THE SETTLEMENT TYPE
							Integer indCashTerm = OperationPartType.CASH_PART.getCode();
							depositMessage.setIndCashTerm(indCashTerm);
							//IF OPERATION IS IN THESE STATES, IT HAS NOT BEEN SETTLED IN CASH
							//WE VERIFY THE INDICATORS PREPAID - EXTENDED PAYMENT
//							Integer indPrePayment = operation.getIndCashPrepaid();
//							Integer indExtendedPayment = operation.getIndCashExtended();
//							Date settlementDate = null;
//							if(BooleanType.YES.getCode().equals(indPrePayment)){
//								settlementDate = operation.getCashPrepaidDate();
//							}else if(BooleanType.YES.getCode().equals(indExtendedPayment)){
//								settlementDate = operation.getCashExtendedDate();
//							}else{
//								settlementDate = operation.getCashSettlementDate();
//							}
//							if(today.compareTo(settlementDate) == FundsType.EQUAL_DATE.getCode()){
//								//IF THE DATE IS THE CORRECT FOR THE PAY, WE MUST VALIDATE THE CURRENCY OF THE OPERATION
//								String currencyOperation = operation.getCurrency().toString();
//								String currencySwift = currency.toString();
//								if(StringUtils.equalsIgnoreCase(currencyOperation, currencySwift)){
//									String bicCode = depositMessage.getParticipantBic();
//									if(Validations.validateIsNotNull(depositMessage.getParticipant())){
//										Long participantPK = depositMessage.getParticipant().getIdParticipantPk();
//										returnValidation = validateBuyerEntity(operation,bicCode,indCashTerm,currency,TERM_CASH,participantPK,depositMessage);
//									}else{
//										returnValidation = MasterTableType.PARAMETER_TABLE_WRONG_PARTICIPANT_SETTLER.getLngCode();
//									}
//								}else{
//									returnValidation = MasterTableType.PARAMETER_TABLE_WRONG_CURRENCY.getLngCode();
//								}
//							}else{
//								returnValidation = MasterTableType.PARAMETER_TABLE_WRONG_SETTLEMENT_DATE.getLngCode();
//							}
						}else if(MechanismOperationStateType.CASH_SETTLED.getCode().equals(operationState)){
							//WE SET THE INDICATOR CASH OR TERM ACCORDING TO THE SETTLEMENT TYPE
							Integer indCashTerm = OperationPartType.TERM_PART.getCode();
							depositMessage.setIndCashTerm(indCashTerm);
							//IF STATES IS DIFFERENT, IT WILL BE SETTLED IN TERM PART
							//WE VERIFY THE INDICATORS PREPAID - EXTENDED PAYMENT
//							Integer indPrePayment = operation.getIndTermPrepaid();
//							Integer indExtendedPayment = operation.getIndTermExtended();
//							Date settlementDate = null;
//							if(BooleanType.YES.getCode().equals(indPrePayment)){
//								settlementDate = operation.getTermPrepaidDate();
//							}else if(BooleanType.YES.getCode().equals(indExtendedPayment)){
//								settlementDate = operation.getTermExtendedDate();
//							}else{
//								settlementDate = operation.getTermSettlementDate();
//							}
//							if(today.compareTo(settlementDate) == FundsType.EQUAL_DATE.getCode()){
//								//IF THE DATE IS THE CORRECT FOR THE PAY, WE MUST VALIDATE THE CURRENCY OF THE OPERATION
//								String currencyOperation = operation.getCurrency().toString();
//								String currencySwift = currency.toString();
//								if(StringUtils.equalsIgnoreCase(currencyOperation, currencySwift)){
//									String bicCode = depositMessage.getParticipantBic();
//									Long participantPK = depositMessage.getParticipant().getIdParticipantPk();
//									returnValidation = validateBuyerEntity(operation,bicCode,indCashTerm,currency,TERM_TERM,participantPK,depositMessage);
//								}else{
//									returnValidation = MasterTableType.PARAMETER_TABLE_WRONG_CURRENCY.getLngCode();
//								}
//							}else{
//								returnValidation = MasterTableType.PARAMETER_TABLE_WRONG_SETTLEMENT_DATE.getLngCode();
//							}
						}else{
							returnValidation = MasterTableType.PARAMETER_TABLE_WRONG_OPERATION_STATE.getLngCode();
						}
					}
				}else{
					//FOR NET SCHEMA, MUST VALIDATE MECHANISM AND MODALITY EXIST IN SYSTEM
					String mechanismCode = paymentReference.substring(9,11);
					NegotiationMechanism mechanism = receptionService.existMechanismCode(mechanismCode);
					if(Validations.validateIsNotNull(mechanism)){
						String modalityGroupCode = paymentReference.substring(11,13);
						if(Validations.validateIsNotNull(modalityGroupCode)){
							String participantBIC = depositMessage.getParticipantBic();
							Long participant = receptionService.getParticipantByBIC(participantBIC);
							if(participant != null){
								Long mechanismPK = mechanism.getIdNegotiationMechanismPk();
								Long modalityGroup = receptionService.getModalityGroup(modalityGroupCode);
								InstitutionCashAccount cashAccount = null;
								if(Validations.validateIsNotNull(modalityGroup)){
									cashAccount = receptionService.validateExistCashAccount(participant, currency,AccountCashFundsType.SETTLEMENT.getCode(),
											mechanismPK,modalityGroup);
									if(Validations.validateIsNotNull(cashAccount)){
										returnValidation = null;
									}else{
										returnValidation = MasterTableType.PARAMETER_TABLE_WRONG_PARTICIPANT_SETTLER.getLngCode();
									}
								}else{
									returnValidation = MasterTableType.PARAMETER_TABLE_WRONG_MODALITY_GROUP.getLngCode();
								}
							}else{
								returnValidation = MasterTableType.PARAMETER_TABLE_WRONG_PARTICIPANT_SETTLER.getLngCode();
							}
						}else{
							returnValidation = MasterTableType.PARAMETER_TABLE_WRONG_MODALITY_GROUP.getLngCode();
						}
					}else{
						returnValidation = MasterTableType.PARAMETER_TABLE_NOT_EXIST_MECHANISM.getLngCode();
					}
				}
			}
		}
		return returnValidation;
	}

	/**
	 * METHOD WHICH WILL SAVE FUND_OPERATION, ACCORDING TO THE PARAMETER, SAVES OBSERVED OR CONFIRMED
	 * @param depositMessage
	 */
	private FundsOperation saveFundOperation(SwiftMessageJavaObject depositMessage, Integer process, boolean centralFundOperation, boolean hasObservation){
		FundsOperation fundOperation = new FundsOperation();
		String paymentReference = depositMessage.getPaymentReference();
		Integer currency = depositMessage.getCurrency();
		String amount = depositMessage.getAmount();
		String trn = depositMessage.getTrn();
		Integer operationGroup = depositMessage.getOperationGroup();
		FundsOperationType fundOperationType = receptionService.findSwiftComponent(trn,currency);
		//Integer operationGroup = fundOperationType.getOperationGroup();
		String issuerStr = depositMessage.getIssuer();
		String participantBIC = depositMessage.getParticipantBic();
		if (centralFundOperation){
			InstitutionCashAccount centralCashAccount = receptionService.getActivatedCentralizingAccount(currency);
			fundOperation.setInstitutionCashAccount(centralCashAccount);
			fundOperation.setIndCentralized(BooleanType.YES.getCode());
		}else{
			fundOperation.setIndCentralized(BooleanType.NO.getCode());
		}
		fundOperation.setFundsOperationGroup(operationGroup);
		//fundOperation.setFundsOperationType(fundOperationType.getIdFundsOperationTypePk());
		fundOperation.setFundsOperationClass(OperationClassType.RECEPTION_FUND.getCode());
		fundOperation.setCurrency(currency);
		fundOperation.setOperationAmount(new BigDecimal(amount));
		fundOperation.setIndAutomatic(BooleanType.YES.getCode());

		if(MasterTableType.PARAMETER_TABLE_OPERATION_GROUP_RATES.getCode().equals(operationGroup)){
			fundOperation.setMechanismOperation(null);
			fundOperation.setBenefitPaymentAllocation(null);
			fundOperation.setFundsOperation(null);
			fundOperation.setGuaranteeOperation(null);
			fundOperation.setParticipant(null);
			fundOperation.setIssuer(null);
			fundOperation.setBank(null);
		}else if(MasterTableType.PARAMETER_TABLE_OPERATION_GROUP_SETTLEMENT.getCode().equals(operationGroup)){
			//String settlementSchema = paymentReference.substring(8,9);
			String settlementSchema = depositMessage.getSettlementSchema();
			Participant participant = depositMessage.getParticipant();
			fundOperation.setParticipant(participant);
			fundOperation.setFundsOperationType(com.pradera.model.component.type.ParameterFundsOperationType.SETTLEMENT_OPERATIONS_FUND_DEPOSITS.getCode());
			if(StringUtils.equalsIgnoreCase(FundsType.IND_SETTLEMENT_SCHEMA_GROSS.getStrCode(),settlementSchema)){
				//GET THE POSITION OF THE SECOND "." TO GET THE PAYMENT REFERENCE OF THE OPERATION, AND ALLOW CORRELATIVE IN REFERENCE PAYMENT IN SWIFT MESSAGE
				int index = paymentReference.indexOf(".",9);
				if(index != -1){
					paymentReference = paymentReference.substring(0,index);
				}
				MechanismOperation operation = receptionService.existOperationByPaymentReference(paymentReference);
				fundOperation.setMechanismOperation(operation);
				fundOperation.setPaymentReference(paymentReference);

				if(Validations.validateIsNotNullAndNotEmpty(depositMessage.getIndCashTerm())){
					fundOperation.setOperationPart(depositMessage.getIndCashTerm());
				}
			}else{
				fundOperation.setMechanismOperation(null);
			}
			fundOperation.setBenefitPaymentAllocation(null);
			fundOperation.setFundsOperation(null);
			fundOperation.setGuaranteeOperation(null);
			fundOperation.setIssuer(null);
			fundOperation.setBank(null);
			fundOperation.setFundsOperation(null);
		}else if(MasterTableType.PARAMETER_TABLE_OPERATION_GROUP_RETURN.getCode().equals(operationGroup)){
			fundOperation.setMechanismOperation(null);
			fundOperation.setBenefitPaymentAllocation(null);
			fundOperation.setFundsOperation(null);
			fundOperation.setGuaranteeOperation(null);
			fundOperation.setParticipant(null);
			fundOperation.setBank(null);
			fundOperation.setIssuer(null);
			String bic = depositMessage.getParticipantBic();
			Long idBank = receptionService.getBankIdByBic(bic);
			if(Validations.validateIsNotNull(idBank)){
				fundOperation.getBank().setIdBankPk(idBank);
			}
			//we don't know the correct funds operation type, so we'll modify the operation manually
			fundOperation.setFundsOperationType(com.pradera.model.component.type.ParameterFundsOperationType.REFUNDS_RECEPTION_COMMENTED.getCode());
//			FundsOperation registerFundOperation = receptionService.searchRegisterFundOperation(paymentReference);
//			Integer fundOpeType = Integer.parseInt(registerFundOperation.getFundsOperationType().toString());
			/*
			Long fundOpeType = fundOperationType.getIdFundsOperationTypePk();

			if(fundOpeType.equals(com.pradera.model.component.type.FundsOperationType.RETURN_BENEFITS_SUPPLY_DEPOSIT.getCode())){
				String issuerCode = depositMessage.getIssuer();
				Issuer issuer = receptionService.validateActiveIssuer(issuerCode);
				if(Validations.validateIsNotNull(issuer)){
					fundOperation.setIssuer(issuer);
				}else{
					fundOperation.setIssuer(null);
				}
				fundOperation.setBank(null);
			}else if(fundOpeType.equals(com.pradera.model.component.type.FundsOperationType.UNPAYED_BENEFITS_RETURN_DEPOSIT.getCode())){
				fundOperation.setIssuer(null);
				String bic = depositMessage.getParticipantBic();
				Long idBank = receptionService.getBankIdByBic(bic);
				if(Validations.validateIsNotNull(idBank)){
					fundOperation.getBank().setIdBankPk(idBank);
				}else{
					fundOperation.setBank(null);
				}
			}*/
		}else if(MasterTableType.PARAMETER_TABLE_OPERATION_GROUP_BENEFITS.getCode().equals(operationGroup) || 
					FundsOperationGroupType.SPECIAL_OPERATION.getCode().equals(operationGroup))
		{
			Bank bank = depositMessage.getBank();
			Participant participant = depositMessage.getParticipant();
			fundOperation.setBank(bank);
			fundOperation.setParticipant(participant);
			if (hasObservation) {
				//we don't know the correct funds operation type, so we'll modify the operation manually
				fundOperation.setFundsOperationType(com.pradera.model.component.type.ParameterFundsOperationType.BENEFIT_RECEPTION_COMMENTED.getCode());
			} else if(Validations.validateIsNotNull(issuerStr))
			{
				//if we know the issues the operation is to benefits
				fundOperation.setMechanismOperation(null);
				fundOperation.setBenefitPaymentAllocation(null);
				fundOperation.setFundsOperation(null);
				fundOperation.setGuaranteeOperation(null);
				fundOperation.setParticipant(null);
				fundOperation.setFundsOperationType(com.pradera.model.component.type.ParameterFundsOperationType.PAYMENT_BENEFITS_FUND_DEPOSITS.getCode());
				Issuer issuer = receptionService.validateActiveIssuer(issuerStr);
				fundOperation.setIssuer(issuer);
			}
		}

		if(FundsType.IND_AUTOMATIC_DEPOSIT_TYPE_SUCCESS.getCode().equals(process)){
			//THIS REGISTER, WILL CREATE THE OPERATION TO CENTRALIZING ACCOUNT, ITS MOVEMENTS AND OPERATION TO CASH ACCOUNT AND MOVS.
			fundOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		}else if(FundsType.IND_AUTOMATIC_DEPOSIT_TYPE_OBSERVED_OPERATION.getCode().equals(process)){
			//THIS REGISTER JUST THE CENTRALIZING ACCOUNT IN OBSERVED STATE WITH IT RESPECTIVE MOVEMENTS
			fundOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_OBSERVED.getCode());
			fundOperation.setSenderBicCode(depositMessage.getParticipantBic().substring(0,11));
		}else if(FundsType.IND_AUTOMATIC_DEPOSIT_TYPE_AUTOMATIC_RETURN.getCode().equals(process)){
			//THIS REGISTER, WILL CREATE THE OPERATION TO CENTRALIZING ACCOUNT, THE RESPECTIVE MOVEMENTS 
			//AND WILL GENERATE THE AUTOMATIC DEVOLUTION.
			fundOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
			fundOperation.setFundsOperationGroup(MasterTableType.PARAMETER_TABLE_OPERATION_GROUP_CENTRALIZER_FUNDS.getCode());
			if(MasterTableType.PARAMETER_TABLE_OPERATION_GROUP_SETTLEMENT.getCode().equals(operationGroup)){
				fundOperation.setFundsOperationType(new Long(com.pradera.model.component.type.ParameterFundsOperationType.DEVOLUTION_RECEPTIONS_FUNDS_WITHDRAWL_SETTLEMENT.getCode()));
			}else{
				fundOperation.setFundsOperationType(new Long(com.pradera.model.component.type.ParameterFundsOperationType.DEVOLUTION_RECEPTIONS_FUNDS_WITHDRAWL_BENEFITS.getCode()));
			}
		}
		fundOperation.setRegistryDate(new Date());
		Integer nextOperationNumber = receptionService.getNextFundsOperationNumber(FundsType.IND_FUND_OPERATION_CLASS_RECEPTION.getCode(), operationGroup);
		fundOperation.setFundOperationNumber(nextOperationNumber);
		fundOperation.setPaymentReference(depositMessage.getPaymentReference());
		return fundOperation;
	}

	/**
	 * METHOD WHICH WILL CREATE THE SWIFT MESSAGE HISTORY 
	 * @param parameters
	 * @param deposits
	 * @throws SerialException
	 * @throws SQLException
	 */
	public void createMessageHistory(SwiftDeposit deposits) throws SerialException, SQLException{
		SwiftMessageHistory swiftMessageHistory = new SwiftMessageHistory();
		String file = deposits.getFile();
		String fileName = deposits.getFileName();
		byte[] objByte = file.getBytes();

		Blob fileObject = new SerialBlob(objByte);
		swiftMessageHistory.setIndInputOutput(FundsType.IND_MESSAGE_TYPE_INPUT.getCode());
		FTPParameters parameters = swiftWithdrawl.getParametersConnection();
		swiftMessageHistory.setSourceUrl(parameters.getLocalPath());
		swiftMessageHistory.setTargetUrl(parameters.getRemotePath());
		swiftMessageHistory.setFileName(fileName);
		swiftMessageHistory.setFileMessage(fileObject);
		swiftMessageHistory.setHistoryDate(new Date());

		receptionService.create(swiftMessageHistory);
	}
	/**
	 * METHOD WHICH ACCORDING TO THE SWIFT MESSAGE, WILL RETURN THE DESTINY CASH ACCOUNT
	 * @param depositMessage
	 * @return
	 */
	private InstitutionCashAccount getCashAccount(SwiftMessageJavaObject depositMessage){
		InstitutionCashAccount cashAccount = null;
		String trn = depositMessage.getTrn();
		Integer currency  = depositMessage.getCurrency();
		FundsOperationType fundOperationType = receptionService.findSwiftComponent(trn,currency);
		Integer operationGroup = fundOperationType.getOperationGroup();
		Long operationType = fundOperationType.getIdFundsOperationTypePk();

		if(MasterTableType.PARAMETER_TABLE_OPERATION_GROUP_RATES.getCode().equals(operationGroup)){
			cashAccount = receptionService.getDestinyCashAccountFunds(AccountCashFundsType.RATES.getCode(),currency);
		}else if(MasterTableType.PARAMETER_TABLE_OPERATION_GROUP_SETTLEMENT.getCode().equals(operationGroup)){
			cashAccount = depositMessage.getCashAccount();

		}else if(MasterTableType.PARAMETER_TABLE_OPERATION_GROUP_RETURN.getCode().equals(operationGroup)){
			cashAccount = receptionService.getReturnCashAccount(currency);
		}else if(	MasterTableType.PARAMETER_TABLE_OPERATION_GROUP_BENEFITS.getCode().equals(operationGroup) 
					|| FundsOperationGroupType.SPECIAL_OPERATION.getCode().equals(operationGroup)){
			String issuer = depositMessage.getIssuer();
			String participantBIC = depositMessage.getParticipantBic();
			if(Validations.validateIsNotNull(issuer)){
				cashAccount = receptionService.getDestinyBenefitCashAccountFunds(AccountCashFundsType.BENEFIT.getCode(),currency,issuer);
			}else if(Validations.validateIsNotNull(participantBIC)){
				Long participantPK = receptionService.getParticipantByBIC(participantBIC);
				cashAccount = receptionService.validateSpecialPaymentCashAccount(participantPK, currency);
			}
		}
		return cashAccount;
	}

	private void confirmPreviousManualFundOperation(SwiftMessageJavaObject depositMessage) throws ServiceException{

		String trn = depositMessage.getTrn();
		Integer currency  = depositMessage.getCurrency();
		FundsOperationType fundOperationType = receptionService.findSwiftComponent(trn,currency);
		Integer operationGroup = fundOperationType.getOperationGroup();

		if(MasterTableType.PARAMETER_TABLE_OPERATION_GROUP_RETURN.getCode().equals(operationGroup)){
			FundsOperation registerFundOperation = receptionService.searchRegisterFundOperation(depositMessage.getPaymentReference());
			if(Validations.validateIsNotNull(registerFundOperation)){
				registerFundOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
				receptionService.update(registerFundOperation);
				//EJECUTAMOS EL COMPONENTE DE FONDOS
				executeFundComponent(registerFundOperation, true);
			}
		}
	}

	/**
	 * METHOD WHICH WILL SAVE THE DEPOSIT FUND OPERATION WHICH WILL HAVE AN AUTOMATIC DEVOLUTION
	 * @param centralDepositOperation
	 * @param userName 
	 * @return
	 */
	private FundsOperation saveCentralFundsOperationAutomaticReturn(FundsOperation centralDepositOperation, Integer error, String userName){
		FundsOperation fundOperation = new FundsOperation();
		fundOperation.setOperationAmount(centralDepositOperation.getOperationAmount());
		fundOperation.setCurrency(centralDepositOperation.getCurrency());
		fundOperation.setFundsOperationGroup(centralDepositOperation.getFundsOperationGroup());
		fundOperation.setFundsOperationType(com.pradera.model.component.type.ParameterFundsOperationType.DEVOLUTION_RECEPTIONS_FUNDS_DEPOSIT.getCode());
		fundOperation.setFundsOperationClass(FundsType.IND_FUND_OPERATION_CLASS_RECEPTION.getCode());
		fundOperation.setOperationState(MasterTableType.PARAMETER_TABLE_FUND_OPERATION_STATE_CONFIRMED.getCode());
		fundOperation.setIndAutomatic(FundsType.INDICATOR_FUND_PROCESS_TYPE_AUTOMATIC.getCode());
		fundOperation.setObservation(error);
		fundOperation.setParticipant(centralDepositOperation.getParticipant());
		fundOperation.setMechanismOperation(centralDepositOperation.getMechanismOperation());
		fundOperation.setGuaranteeOperation(centralDepositOperation.getGuaranteeOperation());
		fundOperation.setBank(centralDepositOperation.getBank());
		fundOperation.setIssuer(centralDepositOperation.getIssuer());
		fundOperation.setSettlementProcess(centralDepositOperation.getSettlementProcess());
		fundOperation.setInstitutionCashAccount(centralDepositOperation.getInstitutionCashAccount());
		fundOperation.setRegistryUser(userName);
		fundOperation.setRegistryDate(new Date());
		fundOperation.setIndCentralized(BooleanType.YES.getCode());
		InstitutionCashAccount centAccount = receptionService.getActivatedCentralizingAccount(centralDepositOperation.getCurrency());
		fundOperation.setInstitutionCashAccount(centAccount);
		fundOperation.setPaymentReference(centralDepositOperation.getPaymentReference());
		cashAccountService.create(fundOperation);

		return fundOperation;
	}

	/**
	 * METHOD WHICH WILL BE INCHARGE TO UPDATE PAYMENT INDICATORS AND INVOKE TO OPERATION COLLECTION METHOD
	 * @param depositMessage
	 */
	public void grossSettlementPaymentOperations(SwiftMessageJavaObject depositMessage){
		String settlementSchema = depositMessage.getSettlementSchema();
		if(Validations.validateIsNotNull(depositMessage.getMechanismOperation())){
			if(StringUtils.equalsIgnoreCase(FundsType.IND_SETTLEMENT_SCHEMA_GROSS.getStrCode(),settlementSchema)){
//				try {
					//SYSTEM MUST VALIDATE THAT THIS OPERATION WILL BE SETTLED TODAY
					MechanismOperation operation = depositMessage.getMechanismOperation();
					MechanismModality mechanismModality = operation.getMechanisnModality();
					NegotiationModality modality = mechanismModality.getNegotiationModality();
					Integer termSettlement = modality.getIndTermSettlement();

					//WE VERIFY IF THE MODALITY MANAGER TERM OR NOT
					if(termSettlement == null || termSettlement.equals(FundsType.IND_MODALITY_NOT_MANAGE_TERM.getCode())){
						Integer operationState = operation.getOperationState();
						//IF DOESNT MANAGE TERM, THE OPERATION STATE MUST BE REGISTERED OR ASSIGNED
						if(	MasterTableType.PARAMETER_TABLE_OPERATION_STATE_ASSIGNED.getLngCode().intValue()==(operationState.intValue()) || 
							MasterTableType.PARAMETER_TABLE_OPERATION_STATE_REGISTERED.getLngCode().intValue()==(operationState.intValue())){

							//IF THE PARTICIPANT OPERATION RELATED HAS INDICADOR FUNDS DEPOSITED - FD, WE MUST TRY TO DO THE COLLECT OF OPERATIONS
//							if(AccountOperationReferenceType.DEPOSITED_FUNDS.getCode().equals(operation.getCashFundsReference())){
//								Long securitiesIndicator = operation.getCashStockReference();
								//WE VERIFY IF THE OPERATION HAS THE INDICATOR OF SECURITIES TO INVOKE THE COLLECTION OF OPERATIONS
//								if(	Validations.validateIsNotNullAndNotEmpty(securitiesIndicator) && 
//									AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode().equals(securitiesIndicator)){
//
//									//WE REALIZE THE COLLECT OF OPERATIONS
//									updateCollectionIndicators(operation,false);
//								}
//							}
						}
					}else{
						//IF MODALITY MANAGE TERM, WE MUST VALIDATE WHICH PART IS GOING TO BE PAID
						Integer operationState = operation.getOperationState();
						if(MasterTableType.PARAMETER_TABLE_OPERATION_STATE_ASSIGNED.getLngCode().intValue()==(operationState.intValue()) || 
							MasterTableType.PARAMETER_TABLE_OPERATION_STATE_REGISTERED.getLngCode().intValue()==(operationState.intValue())){

							//IF THE PARTICIPANT OPERATION RELATED HAS INDICADOR FUNDS DEPOSITED - FD, WE MUST TRY TO DO THE COLLECT OF OPERATIONS
//							if(AccountOperationReferenceType.DEPOSITED_FUNDS.getCode().equals(operation.getCashFundsReference())){
//								Long securitiesIndicator = operation.getCashStockReference();
//								if(	Validations.validateIsNotNullAndNotEmpty(securitiesIndicator) && 
//									AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode().equals(securitiesIndicator)){
//
//									//WE REALIZE THE COLLECT OF OPERATIONS
//									updateCollectionIndicators(operation,false);
//								}
//							}
						}else{

							//IF THE PARTICIPANT OPERATION RELATED HAS INDICADOR FUNDS DEPOSITED - FD, WE MUST TRY TO DO THE COLLECT OF OPERATIONS
//							if(AccountOperationReferenceType.DEPOSITED_FUNDS.getCode().equals(operation.getTermFundsReference())){
////								Long securitiesIndicator = operation.getTermStockReference();
//								if(	Validations.validateIsNotNullAndNotEmpty(securitiesIndicator) && 
//									AccountOperationReferenceType.COMPLIANCE_SECURITIES.getCode().equals(securitiesIndicator)){
//
//									//WE REALIZE THE COLLECT OF OPERATIONS
//									updateCollectionIndicators(operation,true);
//								}
//							}
						}
					}
//				} catch (ServiceException e) {
//					e.printStackTrace();
//				}
			}
		}
	}

	/**
	 * WE CAST SWIFT OBJECTS INTO FUNCTIONAL OBJECTS FOR FUNCTIONAL VALIDATIONS
	 * @param depositMessage
	 * @return
	 */
	public SwiftMessageJavaObject getSwiftObjects(SwiftMessagesDeposit depositMessage){
		SwiftMessageJavaObject returnObject = new SwiftMessageJavaObject();

		String paymentReference = depositMessage.getReferencePayment();
		returnObject.setPaymentReference(paymentReference);
		if(paymentReference.contains(".DEV")){
			returnObject.setTrn(paymentReference);
		}else{
			String trn = paymentReference.substring(0,7);
			returnObject.setTrn(trn);
		}
		String currencyStr = depositMessage.getCurrency();
		Integer currency = getCurrencyParameter(currencyStr);
		returnObject.setCurrency(currency);
		String amount = depositMessage.getAmount().replaceAll(",",".");
		returnObject.setAmount(amount);
		String issuerStr = depositMessage.getIssuer();
		returnObject.setIssuer(issuerStr);
		String bic = depositMessage.getBeneficiaryBicSwiftCode();
		returnObject.setParticipantBic(bic);
		Participant participant = receptionService.validateParticipantBIC(bic);
		returnObject.setParticipant(participant);
		Bank bank = receptionService.validateBankBic(bic);
		returnObject.setBank(bank);
		if(Validations.validateIsNullOrEmpty(issuerStr)){
//			String bic = depositMessage.getBeneficiaryBicSwiftCode();
//			returnObject.setParticipantBic(bic);
//			Participant participant = receptionService.validateParticipantBIC(bic);
//			returnObject.setParticipant(participant);
			String strSettlementSchema= BooleanType.NO.getCode().toString();
			if(Validations.validateIsNotNull(participant)){
				strSettlementSchema= SettlementSchemaType.GROSS.getCode().toString();
				//returnObject.setSettlementSchema(paymentReference.substring(8,9));
			}
//			else{
//				returnObject.setSettlementSchema(BooleanType.NO.getCode().toString());
//			}
			returnObject.setSettlementSchema(strSettlementSchema);
			returnObject.setCollectOperation(false);
		}

		DateFormat formatter = new SimpleDateFormat("yyMMdd");
		String strSettlementDate = depositMessage.getSettlementDate();
		try {
			Date settlementDate = (Date)formatter.parse(strSettlementDate);
			returnObject.setSettlementDate(settlementDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		int index = paymentReference.indexOf(".",9);
		if(index == -1){
			MechanismOperation operation = receptionService.existOperationByPaymentReference(paymentReference);
			returnObject.setMechanismOperation(operation);
		}else{
			paymentReference = paymentReference.substring(0,index);
			MechanismOperation operation = receptionService.existOperationByPaymentReference(paymentReference);
			returnObject.setMechanismOperation(operation);
		}

		return returnObject;
	}

	/**
	 * METHOD WHICH WILL BE INCHARGE TO UPDATE PARTICIPANT INDICATORS BY OPERATIONS AND VERIFY IF OPERATION INDICATORS MUST BE UPDATED TOO
	 * @param participantOperation
	 * @throws ServiceException 
	 */
	private void updateCollectionIndicators(MechanismOperation mechanismOperation,boolean termPart) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		Long operation = mechanismOperation.getIdMechanismOperationPk();
		Integer indCashTerm = null;
		if(termPart){
			indCashTerm = OperationPartType.TERM_PART.getCode();
		}else{
			indCashTerm = OperationPartType.CASH_PART.getCode();
		}

		//WE CALL THE COLLECTION OF OPERATIONS
		Long mechanism = mechanismOperation.getMechanisnModality().getNegotiationMechanism().getIdNegotiationMechanismPk();
		Long modality = mechanismOperation.getMechanisnModality().getNegotiationModality().getIdNegotiationModalityPk();
		Long modalityGroup = receptionService.getModalityGroup(mechanism,modality);
		Integer currency = mechanismOperation.getCurrency();

		//WE GET THE PARTICIPANT BUYTER TO EXECUTE COLLECION OPERATIONS FOR ALL OF THEM
		List<Long> lstParticipantBuyer = receptionService.getOperationBuyers(operation,indCashTerm);
		if(Validations.validateListIsNotNullAndNotEmpty(lstParticipantBuyer)){
			for(Long participantPK : lstParticipantBuyer){
				//COLLECT BY PARTICIPANT OPERATION
				ParticipantOperation participantOperation = receptionService.getParticipantOperation(participantPK,operation,termPart,true);
				BigDecimal participantAmount = participantOperation.getCashAmount();
//				collectionProcessServiceBean.collectGrossMechanismOperation(operation, mechanism, modalityGroup, participantPK, participantAmount, currency, indCashTerm,loggerUser);

				//WE UPDATE THE PARTICIPANT OPERATION INDICATOR TO FC
				participantOperation.setFundsReference(AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode());
				receptionService.update(participantOperation);

				//WE UPDATE FUND HOLDER ACCOUNT OPERATION INDICATORS TO FC
				receptionService.updateIndicatorHolderAccountBuyers(operation,indCashTerm,participantPK,AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode());
			}
		}
		//WE MUST UPDATE THE FUND REFERENCE ACCORDING TO THE PARTICIPATION PART OF THE OPERATION
//		if(termPart){
//			mechanismOperation.setTermFundsReference(AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode());
//		}else{
//			mechanismOperation.setCashFundsReference(AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode());
//		}
		receptionService.update(mechanismOperation);
		//WE CALL THE WITHDRAWL FUND METHOD FOR EACH CASH ACCOUNT OF SELLER PARTICIPANTS
//		collectionProcessServiceBean.deliverFundsMechanismOperation(mechanism, modalityGroup, operation, indCashTerm, currency,loggerUser);
		
	}

	/**
	 * METHOD WILL BE INCHARGE TO CALL FUND COMPONENT
	 */
	private void executeFundComponent(FundsOperation fundOperation, boolean blDeposit){
//		try {
			FundsComponentTO objComponent = new FundsComponentTO();
			if(blDeposit){
				objComponent.setIdBusinessProcess(BusinessProcessType.EFFECTIVE_ACCOUNT_FOUNDS_DEPOSIT_PROCESS.getCode());
			}else{
				objComponent.setIdBusinessProcess(BusinessProcessType.EFFECTIVE_ACCOUNT_FOUNDS_RETIREMENT_PROCESS.getCode());
			}
			objComponent.setIdFundsOperationType(fundOperation.getFundsOperationType());
			objComponent.setIdFundsOperation(fundOperation.getIdFundsOperationPk());
			objComponent.setIdInstitutionCashAccount(fundOperation.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
			objComponent.setCashAmount(fundOperation.getOperationAmount());

//			executorComponentServiceBean.executeFundsComponent(objComponent);
//		} catch (ServiceException e) {
//			e.printStackTrace();
//		}
	}

	private Integer getCurrencyParameter(String currencyStr){
		Integer currency = null;
		if(StringUtils.equalsIgnoreCase(FundsType.DESCRIPTION_SHORT_CURRENCY_DOLLAR.getStrCode(),currencyStr)){
			currency = MasterTableType.PARAMETER_TABLE_CURRENCY_TYPE_DOLARES.getCode();
		}else if(StringUtils.equalsIgnoreCase(FundsType.DESCRIPTION_SHORT_CURRENCY_LOCAL.getStrCode(),currencyStr)){
			currency = MasterTableType.PARAMETER_TABLE_CURRENCY_TYPE_PESOS.getCode();
		}
		return currency;
	}


	/**
	 * METHOD WHICH WILL VALIDATE IF THE BIC CODE ENTERED IS THE CORRESPONDING TO A RELATED CASH ACCOUNT
	 * @param operation
	 * @param bicCode
	 * @param indCashTerm
	 * @param currency
	 * @param intValidationType
	 * @return
	 */
	private Long validateBuyerEntity(MechanismOperation operation, String bicCode, Integer indCashTerm, Integer currency, Integer intValidationType, Long participantPK,
			SwiftMessageJavaObject swiftMessageObject){
		Long lngReturn = null; 
		Long operationPK = operation.getIdMechanismOperationPk();
		Long mechanism = operation.getMechanisnModality().getNegotiationMechanism().getIdNegotiationMechanismPk();
		Long modalityPK = operation.getMechanisnModality().getNegotiationModality().getIdNegotiationModalityPk();
		Long modalityGroup = receptionService.getModalityGroup(mechanism, modalityPK);
		List<ParticipantOperation> lstBuyerParticipants = new ArrayList<ParticipantOperation>();

		if(CASH_CASH.equals(intValidationType)){
			lstBuyerParticipants = receptionService.detailByParticipant(operationPK,null,true,false,indCashTerm);
		}else if(TERM_CASH.equals(intValidationType)){
			lstBuyerParticipants = receptionService.detailByParticipant(operationPK,null,true,false,indCashTerm);
		}else if(TERM_TERM.equals(intValidationType)){
			lstBuyerParticipants = receptionService.detailByParticipant(operationPK,null,true,true,indCashTerm);
		}
		//VALIDATE ALL THE BUYER PARTICIPANTS
		if(Validations.validateListIsNotNullAndNotEmpty(lstBuyerParticipants)){
			for(ParticipantOperation partBuyer : lstBuyerParticipants){
				Participant participantBuyer = partBuyer.getParticipant();
				InstitutionCashAccount cashAccount = receptionService.getDestinyCashAccountSettlement(AccountCashFundsType.SETTLEMENT.getCode(), currency, 
																							mechanism, modalityGroup,participantBuyer.getIdParticipantPk());
				if(Validations.validateIsNotNull(cashAccount)){
					swiftMessageObject.setCashAccount(cashAccount);
					swiftMessageObject.setParticipant(participantBuyer);
					swiftMessageObject.setParticipantBic(participantBuyer.getBicCode());
					List<InstitutionBankAccount> bankAccounts = cashAccountService.getBankAccounts(cashAccount.getIdInstitutionCashAccountPk());
					if(Validations.validateListIsNotNullAndNotEmpty(bankAccounts)){
//						for(InstitutionBankAccount bankAccount : bankAccounts){
//							if(	CashAccountUseType.RECEPTION.getCode().equals(bankAccount.getUseTypeAccount()) || 
//								CashAccountUseType.BOTH.getCode().equals(bankAccount.getUseTypeAccount())){
//								if(MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_OWNER.getLngCode().equals(bankAccount.getBankAccountClass())){
//									//VALIDATION WITH OWNER ACCOUNT
//									if(bicCode.equals(participantBuyer.getBicCode())){
//										lngReturn = null;
//									}
//								}else{
//									//VALIDATION WITH OTHER BANK
//									Bank thirdBank = bankAccount.getProviderBank();
//									if(bicCode.equals(thirdBank.getBicCode())){
//										lngReturn = null;
//									}
//								}
//							}
//						}
						if(Validations.validateIsNull(lngReturn)){
							//IF VALIDATION IS DONE, WE MUST VALIDATE IF THE BUYER ENTITY HAS INCHARGE AND IF ITS CONFIRMED
							if(AccountOperationReferenceType.DEPOSITED_FUNDS.getCode().equals(partBuyer.getFundsReference())
								|| AccountOperationReferenceType.COMPLAINCE_FUNDS.getCode().equals(partBuyer.getFundsReference())){
								lngReturn = MasterTableType.PARAMETER_TABLE_PARTICIPANT_ALREADY_PAID.getLngCode();								
							}else{
								if(FundsType.IND_HAS_INCHARGE.getCode().equals(partBuyer.getIndIncharge())){
									if(HolderAccountOperationStateType.CONFIRMED_INCHARGE_STATE.getCode().equals(partBuyer.getInchargeState())){
										lngReturn = null;
									}else{
										lngReturn = MasterTableType.PARAMETER_TABLE_INCHARGE_NOT_CONFIRMED.getLngCode();
									}
								}else{
									lngReturn = null;
								}
							}
						}
					}
				}else{
					lngReturn = MasterTableType.PARAMETER_TABLE_WRONG_CASH_ACCOUNT.getLngCode();
				}
			}
		}else{
			lngReturn = MasterTableType.PARAMETER_TABLE_WRONG_PARTICIPANT_SETTLER.getLngCode();
		}
		return lngReturn;
	}

	/**
	 * METHOD TO CREATE THE SWIFT MESSAGE REFERENCE WITH ACCORDING FUND OPERATION
	 * @param message
	 * @param depositMessage
	 * @param indValidationProcess
	 */
	private void createSwiftMessageReference(SwiftMessage message, SwiftMessagesDeposit depositMessage, Integer indValidationProcess){
		message.setIndInputOutput(FundsType.IND_MESSAGE_TYPE_INPUT.getCode());
		message.setMessageType(Integer.valueOf(depositMessage.getMessageType()));
		message.setMessageName(depositMessage.getFileName());
		String file = depositMessage.getSwiftMessage();
		byte[] objByte = file.getBytes();
		message.setMessage(objByte);
		if(indValidationProcess == null || FundsType.IND_OBSERVED_OPERATION.getCode().equals(indValidationProcess)){
			message.setState(BooleanType.YES.getCode());
		}else{
			message.setState(BooleanType.NO.getCode());
		}
		receptionService.create(message);
	}

	/**
	 * METHOD TO UPDATE TO FUNDS_DEPOSITED INDICATOR
	 * @param depositMessage
	 */
	private void updateFundsDepositedIndicator(SwiftMessageJavaObject depositMessage){
		String settlementSchema = depositMessage.getSettlementSchema();
		if(Validations.validateIsNotNull(depositMessage.getMechanismOperation())){
			if(StringUtils.equalsIgnoreCase(FundsType.IND_SETTLEMENT_SCHEMA_GROSS.getStrCode(),settlementSchema)){
				try {
					//SYSTEM MUST VALIDATE THAT THIS OPERATION WILL BE SETTLED TODAY
					MechanismOperation operation = depositMessage.getMechanismOperation();
					MechanismModality mechanismModality = operation.getMechanisnModality();
					NegotiationModality modality = mechanismModality.getNegotiationModality();
					Integer termSettlement = modality.getIndTermSettlement();
					String amountStr = depositMessage.getAmount();
					BigDecimal amount = new BigDecimal(amountStr);
					Participant participant = depositMessage.getParticipant();

					//WE VERIFY IF THE MODALITY MANAGER TERM OR NOT
					if(termSettlement == null || termSettlement.equals(FundsType.IND_MODALITY_NOT_MANAGE_TERM.getCode())){
						Integer operationState = operation.getOperationState();
						//IF DOESNT MANAGE TERM, THE OPERATION STATE MUST BE REGISTERED OR ASSIGNED
						if(	MasterTableType.PARAMETER_TABLE_OPERATION_STATE_REGISTERED.getLngCode().intValue()==(operationState.intValue()) || 
							MasterTableType.PARAMETER_TABLE_OPERATION_STATE_ASSIGNED.getLngCode().intValue()==(operationState.intValue())){

							ParticipantOperation participantOperation = 
										receptionService.getParticipantOperation(participant.getIdParticipantPk(),operation.getIdMechanismOperationPk(),false,true);
							BigDecimal depositedAmount = BigDecimal.ZERO;
							if(Validations.validateIsNotNull(participantOperation.getDepositedAmount())){
								depositedAmount = participantOperation.getDepositedAmount();
							}
							BigDecimal operationAmount = participantOperation.getCashAmount();
							depositedAmount = depositedAmount.add(amount);
							participantOperation.setDepositedAmount(depositedAmount);
							//IF THE DEPOSITED AMOUNT IS GREATER THAN THE OPERATION AMOUNT WE MUST UPDATE THE PARTICIPANT INDICATORS
							boolean blParticipantPaid = false;
							if(operationAmount.subtract(depositedAmount).doubleValue() <= 0){
								//WE UPDATE INDICATORS BY PARTICIPANT IN OPERATION
								participantOperation.setFundsReference(AccountOperationReferenceType.DEPOSITED_FUNDS.getCode());
								blParticipantPaid = true;
							}
							cashAccountService.update(participantOperation);

							//WE UPDATE FUND HOLDER ACCOUNT OPERATION INDICATORS TO FD IF THE PARTICIPANT_OPERATION WAS UPDATED
							if(blParticipantPaid){
								receptionService.updateIndicatorHolderAccountBuyers(operation.getIdMechanismOperationPk(),FundsType.INDICATOR_CASH_PART.getCode(),
										participant.getIdParticipantPk(),AccountOperationReferenceType.DEPOSITED_FUNDS.getCode());
							}

							//WE VERIFY IF ALL THE INCHARGE BUYER PARTICIPANTS HAVE PAID TO UPDATE PAID OPERATION INDICATOR
							boolean blUpdateOperation = receptionService.verifyOperationPaid(operation.getIdMechanismOperationPk(),FundsType.INDICATOR_CASH_PART.getCode(),
										participant.getIdParticipantPk(),participantOperation.getIdParticipantOperationPk());
							if(blUpdateOperation && blParticipantPaid){
//								operation.setCashFundsReference(AccountOperationReferenceType.DEPOSITED_FUNDS.getCode());
//								cashAccountService.update(operation);
							}
						}
					}else{
						//IF MODALITY MANAGE TERM, WE MUST VALIDATE WHICH PART IS GOING TO BE PAID
						Integer operationState = operation.getOperationState();
						if(	MasterTableType.PARAMETER_TABLE_OPERATION_STATE_REGISTERED.getLngCode().intValue()==(operationState.intValue()) ||
							MasterTableType.PARAMETER_TABLE_OPERATION_STATE_ASSIGNED.getLngCode().intValue()==(operationState.intValue())){

							//WE WILL PAY THE OPERATION IN THE CASH_PART
							ParticipantOperation participantOperation = 
										receptionService.getParticipantOperation(participant.getIdParticipantPk(),operation.getIdMechanismOperationPk(),false,true);
							BigDecimal depositedAmount = participantOperation.getDepositedAmount();
							BigDecimal operationAmount = participantOperation.getCashAmount();

							if(Validations.validateIsNull(depositedAmount)){
								depositedAmount = BigDecimal.ZERO;
							}

							depositedAmount = depositedAmount.add(amount);
							participantOperation.setDepositedAmount(depositedAmount);

							boolean blParticipantPaid = false;
							if(operationAmount.subtract(depositedAmount).doubleValue() <= 0){
								//WE UPDATE INDICATORS BY PARTICIPANT IN OPERATION
								participantOperation.setFundsReference(AccountOperationReferenceType.DEPOSITED_FUNDS.getCode());
								blParticipantPaid = true;
							}
							receptionService.update(participantOperation);

							if(blParticipantPaid){
								//WE UPDATE FUND HOLDER ACCOUNT OPERATION INDICATORS TO FD 
								receptionService.updateIndicatorHolderAccountBuyers(operation.getIdMechanismOperationPk(),FundsType.INDICATOR_CASH_PART.getCode(),
										participant.getIdParticipantPk(),AccountOperationReferenceType.DEPOSITED_FUNDS.getCode());
							}

							//WE VERIFY IF ALL THE INCHARGE BUYER PARTICIPANTS HAVE PAID TO UPDATE PAID OPERATION INDICATOR
							boolean blUpdateOperation = receptionService.verifyOperationPaid(operation.getIdMechanismOperationPk(),FundsType.INDICATOR_CASH_PART.getCode(),
										participant.getIdParticipantPk(),participantOperation.getIdParticipantOperationPk());
							if(blUpdateOperation && blParticipantPaid){
//								operation.setCashFundsReference(AccountOperationReferenceType.DEPOSITED_FUNDS.getCode());
//								cashAccountService.update(operation);
							}
						}else{
							//WE WILL PAY THE OPERATION IN THE TERM_PART
							ParticipantOperation participantOperation = 
										receptionService.getParticipantOperation(participant.getIdParticipantPk(),operation.getIdMechanismOperationPk(),true,true);
							BigDecimal depositedAmount = participantOperation.getDepositedAmount();
							if(Validations.validateIsNull(depositedAmount)){
								depositedAmount = BigDecimal.ZERO;
							}
							BigDecimal operationAmount = participantOperation.getCashAmount();
							depositedAmount = depositedAmount.add(amount);
							participantOperation.setDepositedAmount(depositedAmount);

							boolean blParticipantPaid = false;
							if(operationAmount.subtract(depositedAmount).doubleValue() <= 0){
								//WE UPDATE INDICATORS BY PARTICIPANT IN OPERATION
								participantOperation.setFundsReference(AccountOperationReferenceType.DEPOSITED_FUNDS.getCode());
								blParticipantPaid = true;
							}
							receptionService.update(participantOperation);

							if(blParticipantPaid){
								//WE UPDATE FUND HOLDER ACCOUNT OPERATION INDICATORS TO FD 
								receptionService.updateIndicatorHolderAccountBuyers(operation.getIdMechanismOperationPk(),FundsType.INDICATOR_TERM_PART.getCode(),
										participant.getIdParticipantPk(),AccountOperationReferenceType.DEPOSITED_FUNDS.getCode());
							}

							//WE VERIFY IF ALL THE INCHARGE BUYER PARTICIPANTS HAVE PAID TO UPDATE PAID OPERATION INDICATOR
							boolean blUpdateOperation = receptionService.verifyOperationPaid(operation.getIdMechanismOperationPk(),FundsType.INDICATOR_TERM_PART.getCode(),
										participant.getIdParticipantPk(),participantOperation.getIdParticipantOperationPk());
							if(blUpdateOperation && blParticipantPaid){
								//operation.setTermFundsReference(AccountOperationReferenceType.DEPOSITED_FUNDS.getCode());
								cashAccountService.update(operation);
							}
						}
					}
				} catch (ServiceException e) {
					e.printStackTrace();
				}
			}
		}		
	}
}