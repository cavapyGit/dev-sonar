package com.pradera.core.component.billing.to;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

public class RateServiceScaleToDataModel extends ListDataModel<ServiceRateScaleTo> implements SelectableDataModel<ServiceRateScaleTo>, Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RateServiceScaleToDataModel() {
		
	}
	
	public RateServiceScaleToDataModel(List<ServiceRateScaleTo> data) {
		super(data);	
	}
	
	@Override
	public ServiceRateScaleTo getRowData(String rowKey) {
		@SuppressWarnings("unchecked")
		List<ServiceRateScaleTo> serviceRateScaleToList=(List<ServiceRateScaleTo>)getWrappedData();
        for(ServiceRateScaleTo serviceRateScaleTo : serviceRateScaleToList) {  
        	
            if(String.valueOf(serviceRateScaleTo.getIdServiceRateScalePk()).equals(rowKey)) {
                return serviceRateScaleTo;  
            }
        }  
		return null;
	}

	@Override
	public Object getRowKey(ServiceRateScaleTo serviceRateScaleTo) {
		// TODO Auto-generated method stub
		return serviceRateScaleTo.getIdServiceRateScalePk();
	}



}
