package com.pradera.core.component.billing.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.pradera.integration.common.validation.Validations;
import com.pradera.model.billing.BillingService;

public class BillingServiceTo implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long idBillingServicePk;
	private Integer serviceType;
	private Integer entityCollection;
	private Integer stateService;
	private Integer baseCollection;
	private Integer calculationPeriod;
	private String serviceCode;
	private String serviceName;
	private Date endEffectiveDate;
	private Date initialEffectiveDate;	
	private List<ServiceRateTo> listServiceRateTo ;
	private List<ServiceRateTo> listServiceRateToModify;
	
	private RateServiceToDataModel rateServiceToDataModel;
	private List<Integer> listCodeServiceRate;
	private ServiceRateTo serviceRateToSelected ;	
	private String descriptionCollectionEntity;	
	private String descriptionBaseCollection;	
	private String descriptionStateBillingService;	
	private String descriptionServiceType;	
	private String descriptionCollectionPeriod;
	private String descriptionClientServiceType;	
	private String descriptionCalculationPeriod;
	private String descriptionNameCollectionEntity;
	private String descriptionCurrency;
	
	private Integer collectionPeriod;
	private Integer clientServiceType; 
	private Integer currencyBilling;
	private Integer currencyCalculation;
	private Integer sourceInformation;
	private Integer inIntegratedBill;
	
	
	private Date 	dateCalculationPeriod;
	private Boolean calculateDaily = Boolean.FALSE;
	private Boolean calculateMonthly= Boolean.FALSE;
	
	
	
	public BillingServiceTo(){
		this.listServiceRateTo= new ArrayList<ServiceRateTo>();
		this.rateServiceToDataModel=  new RateServiceToDataModel(this.listServiceRateTo);
		this.listCodeServiceRate= new ArrayList<Integer>();
		
	}
	
	

	public BillingServiceTo(BillingService billingService){
		this.initialEffectiveDate=billingService.getInitialEffectiveDate();
		this.endEffectiveDate=billingService.getEndEffectiveDate();		
		this.idBillingServicePk=billingService.getIdBillingServicePk();
		this.serviceCode=billingService.getServiceCode();
		this.serviceType=billingService.getServiceType();
		this.serviceName=billingService.getServiceName();
		this.descriptionStateBillingService= billingService.getDescriptionStateBillingService();
		this.baseCollection=billingService.getBaseCollection();
		this.entityCollection=billingService.getEntityCollection();
		this.listCodeServiceRate= new ArrayList<Integer>(); /** setting new rate service codes */
		this.listServiceRateTo= new ArrayList<ServiceRateTo>();		
		this.rateServiceToDataModel=  new RateServiceToDataModel(this.listServiceRateTo);
		this.currencyBilling=billingService.getCurrencyBilling();
		this.currencyCalculation=billingService.getCurrencyCalculation();
		this.sourceInformation=billingService.getSourceInformation();
		
	}
	
	


 
 
	public void addServiceRateTo(ServiceRateTo serviceRateTo){
		
		if (Validations.validateIsNotNull(listServiceRateTo)){
			listServiceRateTo.add(serviceRateTo);
		}
	}
	
	public void deleteServiceRateTo(ServiceRateTo serviceRateTo){
		if (Validations.validateIsNotNull(listServiceRateTo)){
			listServiceRateTo.remove(serviceRateTo);
		}
	}

	
	
	public Boolean existRateServicesByStatus(Integer status){
		Boolean exist=false;
		if (Validations.validateIsNotNullAndNotEmpty(this.listServiceRateTo)){

			for (ServiceRateTo serviceRateTo : listServiceRateTo) {
				if (serviceRateTo.getRateStatus().intValue()==status.intValue()){	
					return true;
				}
			}
		}
		
		return exist;
	}
	
	public void addNewCodeRateService(Integer code){
		if (!Validations.validateListIsNullOrEmpty(listCodeServiceRate)){
			listCodeServiceRate.add(code);
		}
	}
	
	public void removeLastCodeRateService(Integer code){
		if (!Validations.validateListIsNullOrEmpty(listCodeServiceRate)){
			listCodeServiceRate.remove(code);
		}
	}



	/**
	 * @return the idBillingServicePk
	 */
	public Long getIdBillingServicePk() {
		return idBillingServicePk;
	}



	/**
	 * @param idBillingServicePk the idBillingServicePk to set
	 */
	public void setIdBillingServicePk(Long idBillingServicePk) {
		this.idBillingServicePk = idBillingServicePk;
	}



	/**
	 * @return the serviceType
	 */
	public Integer getServiceType() {
		return serviceType;
	}



	/**
	 * @param serviceType the serviceType to set
	 */
	public void setServiceType(Integer serviceType) {
		this.serviceType = serviceType;
	}



	/**
	 * @return the entityCollection
	 */
	public Integer getEntityCollection() {
		return entityCollection;
	}



	/**
	 * @param entityCollection the entityCollection to set
	 */
	public void setEntityCollection(Integer entityCollection) {
		this.entityCollection = entityCollection;
	}



	/**
	 * @return the stateService
	 */
	public Integer getStateService() {
		return stateService;
	}



	/**
	 * @param stateService the stateService to set
	 */
	public void setStateService(Integer stateService) {
		this.stateService = stateService;
	}



	/**
	 * @return the baseCollection
	 */
	public Integer getBaseCollection() {
		return baseCollection;
	}



	/**
	 * @param baseCollection the baseCollection to set
	 */
	public void setBaseCollection(Integer baseCollection) {
		this.baseCollection = baseCollection;
	}



	/**
	 * @return the calculationPeriod
	 */
	public Integer getCalculationPeriod() {
		return calculationPeriod;
	}



	/**
	 * @param calculationPeriod the calculationPeriod to set
	 */
	public void setCalculationPeriod(Integer calculationPeriod) {
		this.calculationPeriod = calculationPeriod;
	}



	/**
	 * @return the serviceCode
	 */
	public String getServiceCode() {
		return serviceCode;
	}



	/**
	 * @param serviceCode the serviceCode to set
	 */
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}



	/**
	 * @return the serviceName
	 */
	public String getServiceName() {
		return serviceName;
	}



	/**
	 * @param serviceName the serviceName to set
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}



	/**
	 * @return the endEffectiveDate
	 */
	public Date getEndEffectiveDate() {
		return endEffectiveDate;
	}



	/**
	 * @param endEffectiveDate the endEffectiveDate to set
	 */
	public void setEndEffectiveDate(Date endEffectiveDate) {
		this.endEffectiveDate = endEffectiveDate;
	}



	/**
	 * @return the initialEffectiveDate
	 */
	public Date getInitialEffectiveDate() {
		return initialEffectiveDate;
	}



	/**
	 * @param initialEffectiveDate the initialEffectiveDate to set
	 */
	public void setInitialEffectiveDate(Date initialEffectiveDate) {
		this.initialEffectiveDate = initialEffectiveDate;
	}



	/**
	 * @return the listServiceRateTo
	 */
	public List<ServiceRateTo> getListServiceRateTo() {
		return listServiceRateTo;
	}



	/**
	 * @param listServiceRateTo the listServiceRateTo to set
	 */
	public void setListServiceRateTo(List<ServiceRateTo> listServiceRateTo) {
		this.listServiceRateTo = listServiceRateTo;
	}



	/**
	 * @return the listServiceRateToModify
	 */
	public List<ServiceRateTo> getListServiceRateToModify() {
		return listServiceRateToModify;
	}



	/**
	 * @param listServiceRateToModify the listServiceRateToModify to set
	 */
	public void setListServiceRateToModify(
			List<ServiceRateTo> listServiceRateToModify) {
		this.listServiceRateToModify = listServiceRateToModify;
	}



	/**
	 * @return the rateServiceToDataModel
	 */
	public RateServiceToDataModel getRateServiceToDataModel() {
		return rateServiceToDataModel;
	}



	/**
	 * @param rateServiceToDataModel the rateServiceToDataModel to set
	 */
	public void setRateServiceToDataModel(
			RateServiceToDataModel rateServiceToDataModel) {
		this.rateServiceToDataModel = rateServiceToDataModel;
	}



	/**
	 * @return the listCodeServiceRate
	 */
	public List<Integer> getListCodeServiceRate() {
		return listCodeServiceRate;
	}



	/**
	 * @param listCodeServiceRate the listCodeServiceRate to set
	 */
	public void setListCodeServiceRate(List<Integer> listCodeServiceRate) {
		this.listCodeServiceRate = listCodeServiceRate;
	}



	/**
	 * @return the serviceRateToSelected
	 */
	public ServiceRateTo getServiceRateToSelected() {
		return serviceRateToSelected;
	}



	/**
	 * @param serviceRateToSelected the serviceRateToSelected to set
	 */
	public void setServiceRateToSelected(ServiceRateTo serviceRateToSelected) {
		this.serviceRateToSelected = serviceRateToSelected;
	}



	/**
	 * @return the descriptionCollectionEntity
	 */
	public String getDescriptionCollectionEntity() {
		return descriptionCollectionEntity;
	}



	/**
	 * @param descriptionCollectionEntity the descriptionCollectionEntity to set
	 */
	public void setDescriptionCollectionEntity(String descriptionCollectionEntity) {
		this.descriptionCollectionEntity = descriptionCollectionEntity;
	}



	/**
	 * @return the descriptionBaseCollection
	 */
	public String getDescriptionBaseCollection() {
		return descriptionBaseCollection;
	}



	/**
	 * @param descriptionBaseCollection the descriptionBaseCollection to set
	 */
	public void setDescriptionBaseCollection(String descriptionBaseCollection) {
		this.descriptionBaseCollection = descriptionBaseCollection;
	}



	/**
	 * @return the descriptionStateBillingService
	 */
	public String getDescriptionStateBillingService() {
		return descriptionStateBillingService;
	}



	/**
	 * @param descriptionStateBillingService the descriptionStateBillingService to set
	 */
	public void setDescriptionStateBillingService(
			String descriptionStateBillingService) {
		this.descriptionStateBillingService = descriptionStateBillingService;
	}



	/**
	 * @return the descriptionServiceType
	 */
	public String getDescriptionServiceType() {
		return descriptionServiceType;
	}



	/**
	 * @param descriptionServiceType the descriptionServiceType to set
	 */
	public void setDescriptionServiceType(String descriptionServiceType) {
		this.descriptionServiceType = descriptionServiceType;
	}



	/**
	 * @return the descriptionCollectionPeriod
	 */
	public String getDescriptionCollectionPeriod() {
		return descriptionCollectionPeriod;
	}



	/**
	 * @param descriptionCollectionPeriod the descriptionCollectionPeriod to set
	 */
	public void setDescriptionCollectionPeriod(String descriptionCollectionPeriod) {
		this.descriptionCollectionPeriod = descriptionCollectionPeriod;
	}



	/**
	 * @return the descriptionClientServiceType
	 */
	public String getDescriptionClientServiceType() {
		return descriptionClientServiceType;
	}



	/**
	 * @param descriptionClientServiceType the descriptionClientServiceType to set
	 */
	public void setDescriptionClientServiceType(String descriptionClientServiceType) {
		this.descriptionClientServiceType = descriptionClientServiceType;
	}



	/**
	 * @return the descriptionCalculationPeriod
	 */
	public String getDescriptionCalculationPeriod() {
		return descriptionCalculationPeriod;
	}



	/**
	 * @param descriptionCalculationPeriod the descriptionCalculationPeriod to set
	 */
	public void setDescriptionCalculationPeriod(String descriptionCalculationPeriod) {
		this.descriptionCalculationPeriod = descriptionCalculationPeriod;
	}



	/**
	 * @return the descriptionNameCollectionEntity
	 */
	public String getDescriptionNameCollectionEntity() {
		return descriptionNameCollectionEntity;
	}



	/**
	 * @param descriptionNameCollectionEntity the descriptionNameCollectionEntity to set
	 */
	public void setDescriptionNameCollectionEntity(
			String descriptionNameCollectionEntity) {
		this.descriptionNameCollectionEntity = descriptionNameCollectionEntity;
	}



	/**
	 * @return the descriptionCurrency
	 */
	public String getDescriptionCurrency() {
		return descriptionCurrency;
	}



	/**
	 * @param descriptionCurrency the descriptionCurrency to set
	 */
	public void setDescriptionCurrency(String descriptionCurrency) {
		this.descriptionCurrency = descriptionCurrency;
	}



	/**
	 * @return the collectionPeriod
	 */
	public Integer getCollectionPeriod() {
		return collectionPeriod;
	}



	/**
	 * @param collectionPeriod the collectionPeriod to set
	 */
	public void setCollectionPeriod(Integer collectionPeriod) {
		this.collectionPeriod = collectionPeriod;
	}



	/**
	 * @return the clientServiceType
	 */
	public Integer getClientServiceType() {
		return clientServiceType;
	}



	/**
	 * @param clientServiceType the clientServiceType to set
	 */
	public void setClientServiceType(Integer clientServiceType) {
		this.clientServiceType = clientServiceType;
	}



	/**
	 * @return the currencyBilling
	 */
	public Integer getCurrencyBilling() {
		return currencyBilling;
	}



	/**
	 * @param currencyBilling the currencyBilling to set
	 */
	public void setCurrencyBilling(Integer currencyBilling) {
		this.currencyBilling = currencyBilling;
	}



	/**
	 * @return the currencyCalculation
	 */
	public Integer getCurrencyCalculation() {
		return currencyCalculation;
	}



	/**
	 * @param currencyCalculation the currencyCalculation to set
	 */
	public void setCurrencyCalculation(Integer currencyCalculation) {
		this.currencyCalculation = currencyCalculation;
	}



	/**
	 * @return the sourceInformation
	 */
	public Integer getSourceInformation() {
		return sourceInformation;
	}



	/**
	 * @param sourceInformation the sourceInformation to set
	 */
	public void setSourceInformation(Integer sourceInformation) {
		this.sourceInformation = sourceInformation;
	}



	/**
	 * @return the inIntegratedBill
	 */
	public Integer getInIntegratedBill() {
		return inIntegratedBill;
	}



	/**
	 * @param inIntegratedBill the inIntegratedBill to set
	 */
	public void setInIntegratedBill(Integer inIntegratedBill) {
		this.inIntegratedBill = inIntegratedBill;
	}



	/**
	 * @return the dateCalculationPeriod
	 */
	public Date getDateCalculationPeriod() {
		return dateCalculationPeriod;
	}



	/**
	 * @param dateCalculationPeriod the dateCalculationPeriod to set
	 */
	public void setDateCalculationPeriod(Date dateCalculationPeriod) {
		this.dateCalculationPeriod = dateCalculationPeriod;
	}



	/**
	 * @return the calculateDaily
	 */
	public Boolean getCalculateDaily() {
		return calculateDaily;
	}



	/**
	 * @param calculateDaily the calculateDaily to set
	 */
	public void setCalculateDaily(Boolean calculateDaily) {
		this.calculateDaily = calculateDaily;
	}



	/**
	 * @return the calculateMonthly
	 */
	public Boolean getCalculateMonthly() {
		return calculateMonthly;
	}



	/**
	 * @param calculateMonthly the calculateMonthly to set
	 */
	public void setCalculateMonthly(Boolean calculateMonthly) {
		this.calculateMonthly = calculateMonthly;
	}
	
	
	
	

	
}
