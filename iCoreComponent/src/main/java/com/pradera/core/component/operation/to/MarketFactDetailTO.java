package com.pradera.core.component.operation.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @author USUARIO
 *
 */
public class MarketFactDetailTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long marketFactBalancePk ;
	
	private Date marketDate;
	
	private BigDecimal marketPrice;
	
	private BigDecimal marketRate;
	
	private BigDecimal totalBalance;
	
	private BigDecimal availableBalance;
	
	private BigDecimal pawnBalance;
	
	private BigDecimal banBalance;
	
	private BigDecimal otherBanBalance;
	
	private BigDecimal enteredBalance;
	
	private BigDecimal quantityBlockDocument;
	
	private Boolean isSelected;
	
	private MarketFactBalanceTO marketFactBalanceTO;
	
	private BigDecimal transitBalance;
	private BigDecimal reserveBalance;
	private BigDecimal oppositionBalance;
	private BigDecimal accreditationBalance;
	private BigDecimal reportingBalance;
	private BigDecimal reportedBalance;
	private BigDecimal marginBalance;
	private BigDecimal purchaseBalance;
	private BigDecimal saleBalance;
	private BigDecimal lenderBalance;
	private BigDecimal lonableBalance;
	private BigDecimal borrowerBalance;
	
	private BigDecimal movementQuantity;

	public Long getMarketFactBalancePk() {
		return marketFactBalancePk;
	}

	public void setMarketFactBalancePk(Long marketFactBalancePk) {
		this.marketFactBalancePk = marketFactBalancePk;
	}

	public BigDecimal getTotalBalance() {
		return totalBalance;
	}

	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	public BigDecimal getEnteredBalance() {
		return enteredBalance;
	}

	public void setEnteredBalance(BigDecimal enteredBalance) {
		this.enteredBalance = enteredBalance;
	}

	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	public MarketFactBalanceTO getMarketFactBalanceTO() {
		return marketFactBalanceTO;
	}

	public void setMarketFactBalanceTO(MarketFactBalanceTO marketFactBalanceTO) {
		this.marketFactBalanceTO = marketFactBalanceTO;
	}

	public BigDecimal getMarketRate() {
		return marketRate;
	}

	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	public Date getMarketDate() {
		return marketDate;
	}

	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	public Boolean getIsSelected() {
		return isSelected;
	}

	public void setIsSelected(Boolean isSelected) {
		this.isSelected = isSelected;
	}

	public BigDecimal getPawnBalance() {
		return pawnBalance;
	}

	public void setPawnBalance(BigDecimal pawnBalace) {
		this.pawnBalance = pawnBalace;
	}

	public BigDecimal getBanBalance() {
		return banBalance;
	}

	public void setBanBalance(BigDecimal banBalance) {
		this.banBalance = banBalance;
	}

	public BigDecimal getOtherBanBalance() {
		return otherBanBalance;
	}

	public void setOtherBanBalance(BigDecimal otherBanBalance) {
		this.otherBanBalance = otherBanBalance;
	}

	public BigDecimal getQuantityBlockDocument() {
		return quantityBlockDocument;
	}

	public void setQuantityBlockDocument(BigDecimal quantityBlockDocument) {
		this.quantityBlockDocument = quantityBlockDocument;
	}

	public BigDecimal getTransitBalance() {
		return transitBalance;
	}

	public void setTransitBalance(BigDecimal transitBalance) {
		this.transitBalance = transitBalance;
	}

	public BigDecimal getReserveBalance() {
		return reserveBalance;
	}

	public void setReserveBalance(BigDecimal reserveBalance) {
		this.reserveBalance = reserveBalance;
	}

	public BigDecimal getOppositionBalance() {
		return oppositionBalance;
	}

	public void setOppositionBalance(BigDecimal oppositionBalance) {
		this.oppositionBalance = oppositionBalance;
	}

	public BigDecimal getAccreditationBalance() {
		return accreditationBalance;
	}

	public void setAccreditationBalance(BigDecimal accreditationBalance) {
		this.accreditationBalance = accreditationBalance;
	}

	public BigDecimal getReportingBalance() {
		return reportingBalance;
	}

	public void setReportingBalance(BigDecimal reportingBalance) {
		this.reportingBalance = reportingBalance;
	}

	public BigDecimal getReportedBalance() {
		return reportedBalance;
	}

	public void setReportedBalance(BigDecimal reportedBalance) {
		this.reportedBalance = reportedBalance;
	}

	public BigDecimal getMarginBalance() {
		return marginBalance;
	}

	public void setMarginBalance(BigDecimal marginBalance) {
		this.marginBalance = marginBalance;
	}

	public BigDecimal getPurchaseBalance() {
		return purchaseBalance;
	}

	public void setPurchaseBalance(BigDecimal purchaseBalance) {
		this.purchaseBalance = purchaseBalance;
	}

	public BigDecimal getSaleBalance() {
		return saleBalance;
	}

	public void setSaleBalance(BigDecimal saleBalance) {
		this.saleBalance = saleBalance;
	}

	public BigDecimal getLenderBalance() {
		return lenderBalance;
	}

	public void setLenderBalance(BigDecimal lenderBalance) {
		this.lenderBalance = lenderBalance;
	}

	public BigDecimal getLonableBalance() {
		return lonableBalance;
	}

	public void setLonableBalance(BigDecimal lonableBalance) {
		this.lonableBalance = lonableBalance;
	}

	public BigDecimal getBorrowerBalance() {
		return borrowerBalance;
	}

	public void setBorrowerBalance(BigDecimal borrowerBalance) {
		this.borrowerBalance = borrowerBalance;
	}

	public BigDecimal getMovementQuantity() {
		return movementQuantity;
	}

	public void setMovementQuantity(BigDecimal movementQuantity) {
		this.movementQuantity = movementQuantity;
	}
	
	
}