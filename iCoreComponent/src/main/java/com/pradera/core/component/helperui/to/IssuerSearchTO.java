package com.pradera.core.component.helperui.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class IssuerSearchTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 12-jun-2015
 */
public class IssuerSearchTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1229032786660948360L;
	
	/** The lst economic sector. */
	private List<ParameterTable> lstDocumentType, lstState, lstEconomicSector;
	
	/** The economic sector. */
	private Integer state, documentType, economicSector;
	
	/** The id issuer code. */
	private String idIssuerCode;
	
	/** The mnemonic. */
	private String fullName, documentNumber, mnemonic;
	
	/** The bl no result. */
	private boolean blNoResult;
	
	/** The lst issuer. */
	private GenericDataModel<Issuer> lstIssuer;
	
	/** The order by. */
	private String orderBy;
	
	private boolean limitMaxResult = false;
	
	/**
	 * Gets the lst document type.
	 *
	 * @return the lst document type
	 */
	public List<ParameterTable> getLstDocumentType() {
		return lstDocumentType;
	}
	
	/**
	 * Sets the lst document type.
	 *
	 * @param lstDocumentType the new lst document type
	 */
	public void setLstDocumentType(List<ParameterTable> lstDocumentType) {
		this.lstDocumentType = lstDocumentType;
	}
	
	/**
	 * Gets the lst state.
	 *
	 * @return the lst state
	 */
	public List<ParameterTable> getLstState() {
		return lstState;
	}
	
	/**
	 * Sets the lst state.
	 *
	 * @param lstState the new lst state
	 */
	public void setLstState(List<ParameterTable> lstState) {
		this.lstState = lstState;
	}
	
	/**
	 * Gets the lst economic sector.
	 *
	 * @return the lst economic sector
	 */
	public List<ParameterTable> getLstEconomicSector() {
		return lstEconomicSector;
	}
	
	/**
	 * Sets the lst economic sector.
	 *
	 * @param lstEconomicSector the new lst economic sector
	 */
	public void setLstEconomicSector(List<ParameterTable> lstEconomicSector) {
		this.lstEconomicSector = lstEconomicSector;
	}
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}
	
	/**
	 * Gets the document type.
	 *
	 * @return the document type
	 */
	public Integer getDocumentType() {
		return documentType;
	}
	
	/**
	 * Sets the document type.
	 *
	 * @param documentType the new document type
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}
	
	/**
	 * Gets the economic sector.
	 *
	 * @return the economic sector
	 */
	public Integer getEconomicSector() {
		return economicSector;
	}
	
	/**
	 * Sets the economic sector.
	 *
	 * @param economicSector the new economic sector
	 */
	public void setEconomicSector(Integer economicSector) {
		this.economicSector = economicSector;
	}
	
	/**
	 * Gets the full name.
	 *
	 * @return the full name
	 */
	public String getFullName() {
		return fullName;
	}
	
	/**
	 * Sets the full name.
	 *
	 * @param fullName the new full name
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	/**
	 * Gets the document number.
	 *
	 * @return the document number
	 */
	public String getDocumentNumber() {
		return documentNumber;
	}
	
	/**
	 * Sets the document number.
	 *
	 * @param documentNumber the new document number
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	
	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}
	
	/**
	 * Sets the mnemonic.
	 *
	 * @param mnemonic the new mnemonic
	 */
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}
	
	/**
	 * Checks if is bl no result.
	 *
	 * @return true, if is bl no result
	 */
	public boolean isBlNoResult() {
		return blNoResult;
	}
	
	/**
	 * Sets the bl no result.
	 *
	 * @param blNoResult the new bl no result
	 */
	public void setBlNoResult(boolean blNoResult) {
		this.blNoResult = blNoResult;
	}
	
	/**
	 * Gets the lst issuer.
	 *
	 * @return the lst issuer
	 */
	public GenericDataModel<Issuer> getLstIssuer() {
		return lstIssuer;
	}
	
	/**
	 * Sets the lst issuer.
	 *
	 * @param lstIssuer the new lst issuer
	 */
	public void setLstIssuer(GenericDataModel<Issuer> lstIssuer) {
		this.lstIssuer = lstIssuer;
	}
	
	/**
	 * Gets the order by.
	 *
	 * @return the order by
	 */
	public String getOrderBy() {
		return orderBy;
	}
	
	/**
	 * Sets the order by.
	 *
	 * @param orderBy the new order by
	 */
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	
	/**
	 * Gets the id issuer code.
	 *
	 * @return the id issuer code
	 */
	public String getIdIssuerCode() {
		return idIssuerCode;
	}
	
	/**
	 * Sets the id issuer code.
	 *
	 * @param idIssuerCode the new id issuer code
	 */
	public void setIdIssuerCode(String idIssuerCode) {
		this.idIssuerCode = idIssuerCode;
	}

	public boolean isLimitMaxResult() {
		return limitMaxResult;
	}

	public void setLimitMaxResult(boolean limitMaxResult) {
		this.limitMaxResult = limitMaxResult;
	}
	
	
}
