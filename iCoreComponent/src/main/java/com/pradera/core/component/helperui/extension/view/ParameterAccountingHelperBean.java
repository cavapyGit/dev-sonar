package com.pradera.core.component.helperui.extension.view;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.el.ELContext;
import javax.el.ValueExpression;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounting.account.to.AccountingParameterTo;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.HelperBean;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounting.AccountingParameter;
import com.pradera.model.generalparameter.ParameterTable;


@HelperBean
public class ParameterAccountingHelperBean extends GenericBaseBean  implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 803589614802281436L;

	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	@EJB
	HelperComponentFacade  helperComponentFacade;
	
	/** The Constant log. */
	private static final Logger log = Logger.getLogger(ParameterAccountingHelperBean.class);
	
	private AccountingParameter 					accountingParameter;
	private List<ParameterTable> 					listTypeParameter;
	private GenericDataModel<AccountingParameter>	dataModelAccountingParameterList;
	private List<AccountingParameter> 				listAccountingParameter;
	
	private GenericDataModel<AccountingParameterTo>	dataModelAccountingParameterToList;
	private List<AccountingParameterTo> 				listAccountingParameterTo;
	
	
	
	
	
	@PostConstruct
	public void init() throws ServiceException{
		accountingParameter=new AccountingParameter();
	}

	public void showSearchParameter() throws ServiceException{

		ParameterTableTO parameterTableTO = new ParameterTableTO();						
		if (Validations.validateListIsNullOrEmpty(listTypeParameter)) {
			log.debug("Listado de Tipo de Parametros  :");

			parameterTableTO.setMasterTableFk(GeneralConstants.MODULE_TYPE);
			listTypeParameter = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		}
		JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationBlur').hide();");
		//JSFUtilities.executeJavascriptFunction("cnfwMsgCustomValidationBlur.hide();");
		JSFUtilities.executeJavascriptFunction("dlgParameter.show();");
		clearHelperSearchParameterAccounting();

		}
	
	public void clearHelperSearchParameterAccounting(){
	
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		
		AccountingParameterTo accountingParameterTo=new AccountingParameterTo();
		
		ValueExpression valueExpression = 
				context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.parameterAccounting}", Object.class);
		valueExpression.setValue(elContext,accountingParameterTo ); 
		dataModelAccountingParameterToList=null;
	}


	//METHOD CALLED FROM CLICK EVENT
	public void searchAccountingParameters() throws IllegalArgumentException, IllegalAccessException{
		log.info("Consulta de Tipos de movimiento por filtro :");	
		AccountingParameterTo accountingParameterTo=new AccountingParameterTo();
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		ValueExpression valueExpressionDescriptionAccount = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.parameterAccounting}", Object.class);
		accountingParameterTo=(AccountingParameterTo)valueExpressionDescriptionAccount.getValue(elContext);
		
		listAccountingParameterTo=helperComponentFacade.findParameterAccountingServiceBean(accountingParameterTo);
		
		if(Validations.validateListIsNullOrEmpty(listAccountingParameterTo)){
			dataModelAccountingParameterToList=null;
			return;
		}
		dataModelAccountingParameterToList=new GenericDataModel<AccountingParameterTo>(listAccountingParameterTo);


		}


	public void assignParameterAccount(AccountingParameterTo accountingParameterTo){
		
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		
		ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.parameterAccounting}", Object.class);
		valueExpression.setValue(elContext, accountingParameterTo);
		 
		JSFUtilities.setValidViewComponentAndChildrens("frmRateRegister:otpRatesID");
		JSFUtilities.executeJavascriptFunction("dlgMovement.hide();");

	}

	public void findParameterAccountingByCode() {
		AccountingParameterTo accountingParameterTo;
		
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		
		ValueExpression valueExpressionDescriptionAccount = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.parameterAccounting.idAccountingParameterPk}", String.class);
		Object valueDescriptionAccount = valueExpressionDescriptionAccount.getValue(elContext);
		
		Long idAccountingParameterPk=Long.parseLong( valueDescriptionAccount.toString());
		ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.parameterAccounting}", Object.class);
		
		ValueExpression accountingParameterHelpMessageValueExp = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.resourceBundleMap.accountingParameterHelpMessage}", String.class);
		String accountingParameterHelpMessage= (String)accountingParameterHelpMessageValueExp.getValue(elContext);
		
		ValueExpression titleValueEx = context.getApplication().getExpressionFactory()
	    .createValueExpression(elContext, "#{cc.resourceBundleMap.title}", String.class);
		String title= (String)titleValueEx.getValue(elContext);
		
		if(Validations.validateIsNotNullAndNotEmpty(idAccountingParameterPk)){
			
			accountingParameterTo=helperComponentFacade.findAccountingParameteByCode(idAccountingParameterPk);
			
			if(Validations.validateIsNotNull(accountingParameterTo)){
				
				
				valueExpression.setValue(elContext, accountingParameterTo);
			}
			else{
				
				accountingParameterTo=new AccountingParameterTo();
				 JSFUtilities.putViewMap("bodyMessageView", accountingParameterHelpMessage);
				 JSFUtilities.putViewMap("headerMessageView", title);
				 RequestContext.getCurrentInstance().execute("cnfwMsgCustomValidationBlur.show();");
				 valueExpression.setValue(elContext, accountingParameterTo);
			}
		}
		
	}
		
		

	public List<ParameterTable> getListTypeParameter() {
		return listTypeParameter;
	}

	public void setListTypeParameter(List<ParameterTable> listTypeParameter) {
		this.listTypeParameter = listTypeParameter;
	}

	public AccountingParameter getAccountingParameter() {
		return accountingParameter;
	}










	public void setAccountingParameter(AccountingParameter accountingParameter) {
		this.accountingParameter = accountingParameter;
	}

	public GenericDataModel<AccountingParameter> getDataModelAccountingParameterList() {
		return dataModelAccountingParameterList;
	}

	public void setDataModelAccountingParameterList(
			GenericDataModel<AccountingParameter> dataModelAccountingParameterList) {
		this.dataModelAccountingParameterList = dataModelAccountingParameterList;
	}

	public List<AccountingParameter> getListAccountingParameter() {
		return listAccountingParameter;
	}

	public void setListAccountingParameter(
			List<AccountingParameter> listAccountingParameter) {
		this.listAccountingParameter = listAccountingParameter;
	}

	public GenericDataModel<AccountingParameterTo> getDataModelAccountingParameterToList() {
		return dataModelAccountingParameterToList;
	}

	public void setDataModelAccountingParameterToList(
			GenericDataModel<AccountingParameterTo> dataModelAccountingParameterToList) {
		this.dataModelAccountingParameterToList = dataModelAccountingParameterToList;
	}

	public List<AccountingParameterTo> getListAccountingParameterTo() {
		return listAccountingParameterTo;
	}

	public void setListAccountingParameterTo(
			List<AccountingParameterTo> listAccountingParameterTo) {
		this.listAccountingParameterTo = listAccountingParameterTo;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
