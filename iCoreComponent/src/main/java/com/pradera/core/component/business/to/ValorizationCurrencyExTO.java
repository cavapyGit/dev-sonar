package com.pradera.core.component.business.to;

import java.io.Serializable;
import java.math.BigDecimal;

public class ValorizationCurrencyExTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ValorizationCurrencyExTO() {
		// TODO Auto-generated constructor stub
	}
	
	BigDecimal usd;
	BigDecimal ufv;
	BigDecimal eu;
	BigDecimal dmv;

	public BigDecimal getUsd() {
		return usd;
	}
	public void setUsd(BigDecimal usd) {
		this.usd = usd;
	}
	public BigDecimal getUfv() {
		return ufv;
	}
	public void setUfv(BigDecimal ufv) {
		this.ufv = ufv;
	}
	public BigDecimal getEu() {
		return eu;
	}
	public void setEu(BigDecimal eu) {
		this.eu = eu;
	}
	public BigDecimal getDmv() {
		return dmv;
	}
	public void setDmv(BigDecimal dmv) {
		this.dmv = dmv;
	}
	
	

}
