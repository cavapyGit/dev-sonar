package com.pradera.core.component.accounts.facade;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;

import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.accounts.service.HolderBalanceMovementsServiceBean;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.helperui.service.BlockEntityQueryServiceBean;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.HolderFile;
import com.pradera.model.accounts.InstitutionInformation;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.accounts.LegalRepresentative;
import com.pradera.model.accounts.LegalRepresentativeFile;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.PepPerson;
import com.pradera.model.accounts.RepresentativeFileHistory;
import com.pradera.model.accounts.RepresentedEntity;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.type.HolderFileStateType;
import com.pradera.model.accounts.type.RepresentativeEntityStateType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.SecuritiesManager;
import com.pradera.model.negotiation.NegotiationModality;
// TODO: Auto-generated Javadoc
//TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary. Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class GeneralParametersFacade.
 * 
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 25/01/2013
 */
@Stateless
@Performance
public class AccountsFacade {

	/** The Accounts service bean. */
	@EJB
	HolderBalanceMovementsServiceBean balanceMovementsServiceBean;

	/** The participant service bean. */
	@EJB
	ParticipantServiceBean participantServiceBean;

	/** The holder account component service bean. */
	@EJB
	HolderAccountComponentServiceBean holderAccountComponentServiceBean;

	/** The block entitiy query service bean. */
	@EJB
	BlockEntityQueryServiceBean blockEntitiyQueryServiceBean;

	/**
	 * Gets the lis participant service bean.
	 * 
	 * @param filter
	 *            the filter
	 * @return the lis participant service bean
	 * @throws ServiceException
	 *             the service exception
	 */
	public List<Issuer> getLisIssuerServiceBean(Issuer filter)
			throws ServiceException {
		return participantServiceBean.getLisIssuerServiceBean(filter);
	}
	public List<Participant> getLisParticipantServiceBean(Participant filter)
			throws ServiceException {
		return participantServiceBean.getLisParticipantServiceBean(filter);
	}
	
	public List<Participant> getParticipantsOnPortFolly()
			throws ServiceException {
		return participantServiceBean.getLisParticipantServiceBean();
	}

	public List<InternationalDepository> getLisInternationalDepositoryServiceBean(
			InternationalDepository filter) throws ServiceException {
		return participantServiceBean
				.getLisInternationalDepositoryServiceBean(filter);
	}

	/**
	 * Gets the holder account component service facade.
	 * 
	 * @param holderAccountTO
	 *            the holder account to
	 * @return the holder account component service facade
	 * @throws ServiceException
	 *             the service exception
	 */
	public HolderAccount getHolderAccountComponentServiceFacade(
			HolderAccountTO holderAccountTO) throws ServiceException {
		return holderAccountComponentServiceBean
				.getHolderAccountComponentServiceBean(holderAccountTO);
	}

	/**
	 * Gets the holder account list service facade.
	 * 
	 * @param holderAccountTO
	 *            the holder account to
	 * @return the holder account list service facade
	 * @throws ServiceException
	 *             the service exception
	 */
	public List<HolderAccount> getHolderAccountListComponentServiceFacade(
			HolderAccountTO holderAccountTO) throws ServiceException {
		return holderAccountComponentServiceBean
				.getHolderAccountListComponentServiceBean(holderAccountTO);
	}

	/**
	 * Reg hold account req component service bean.
	 * 
	 * @param holderAccountRequestHy
	 *            the holder account request hy
	 * @return true, if successful
	 * @throws ServiceException
	 *             the service exception
	 */
	// public HolderAccountRequestHy
	// regHoldAccountReqComponentServiceFacade(HolderAccountRequestHy
	// holderAccountRequestHy) throws ServiceException{
	// return
	// holderAccountComponentServiceBean.regHoldAccountReqComponentServiceBean(holderAccountRequestHy);
	// }

	/**
	 * Gets the balance holder from security.
	 * 
	 * @param filter
	 *            the filter
	 * @return the balance holder from security
	 * @throws ServiceException
	 *             the service exception
	 */
	public HolderAccountBalance holderAccountBalanceServiceBean(
			HolderAccountBalanceTO filter) throws ServiceException {
		return balanceMovementsServiceBean.holderAccountBalanceServiceBean(filter);
	}

	/**
	 * Participant state.
	 * 
	 * @param idParticipantPk
	 *            the id participant pk
	 * @return the participant
	 * @throws ServiceException
	 *             the service exception
	 */
	public Participant getParticipantStateServiceFacade(Long idParticipantPk)
			throws ServiceException {
		return participantServiceBean.getParticipantState(idParticipantPk);
	}
	
	public Participant findParticipantByFilters(Participant filter)throws ServiceException{
		Participant participant = participantServiceBean.findParticipantByFiltersServiceBean(filter);
		if(participant!=null){
			if(participant.getHolder()!=null){
				participant.getHolder().getIdHolderPk();
			}
			if(participant.getRepresentedEntities()!=null){
				participant.getRepresentedEntities().size();
				for(RepresentedEntity repreEntity :participant.getRepresentedEntities()){
					if(repreEntity.getLegalRepresentative()!=null){
						repreEntity.getLegalRepresentative().getIdLegalRepresentativePk();
						if(repreEntity.getLegalRepresentative().getLegalRepresentativeFile()!=null){
							repreEntity.getLegalRepresentative().getLegalRepresentativeFile().size();
							for(LegalRepresentativeFile lrf : repreEntity.getLegalRepresentative().getLegalRepresentativeFile()){
								LegalRepresentativeFile legalFile = getContentFileLegalRepresentativeServiceFacade(lrf.getIdLegalRepreFilePk());
								lrf.setFileLegalRepre(legalFile.getFileLegalRepre());
				    	    }
							
						}
					}
				}
			}
			if(participant.getParticipantFiles()!=null){
				participant.getParticipantFiles().size();
			}
		}
		return participant;
	}

	public List<HolderTO> getListHoldersByfilter(HolderTO filter)
			throws ServiceException {
		List<Object[]> lista = holderAccountComponentServiceBean
				.getHoldersByFilter(filter);
		List<HolderTO> listHolder = new ArrayList<HolderTO>();
		HolderTO holder;
		for (Object[] tempObject : lista) {
			holder = new HolderTO();
			if (Validations.validateIsNotNullAndNotEmpty(tempObject[0])) {
				holder.setHolderId(Long.parseLong(tempObject[0].toString()));
			}
			if (Validations.validateIsNotNullAndNotEmpty(tempObject[1])) {
				holder.setName(tempObject[1].toString());
			} else {
				holder.setName("");
			}
			if (Validations.validateIsNotNullAndNotEmpty(tempObject[2])) {
				holder.setFirstLastName(tempObject[2].toString());
			} else {
				holder.setFirstLastName("");
			}
			if (Validations.validateIsNotNullAndNotEmpty(tempObject[12])) {
				holder.setSecondLastName(tempObject[12].toString());
			} else {
				holder.setSecondLastName("");
			}
			if (Validations.validateIsNotNullAndNotEmpty(tempObject[3])) {
				holder.setFullName(tempObject[3].toString());
			} else {
				holder.setFullName("");
			}
			if (Validations.validateIsNotNullAndNotEmpty(tempObject[4])) {
				holder.setNationality(Integer.parseInt(tempObject[4].toString()));
			}
			if (Validations.validateIsNotNullAndNotEmpty(tempObject[5])) {
				holder.setLegalResidenceCountry(Integer.parseInt(tempObject[5]
						.toString()));
			}
			if (Validations.validateIsNotNullAndNotEmpty(tempObject[6])) {
				holder.setParticipantFk(Long.parseLong(tempObject[6].toString()));
			}
			if (Validations.validateIsNotNullAndNotEmpty(tempObject[7])) {
				holder.setIndParticipant(1);
			} else {
				holder.setIndParticipant(0);
			}
			if (Validations.validateIsNotNullAndNotEmpty(tempObject[8])) {
				holder.setIssuerFk(tempObject[8].toString());
			}
			if (Validations.validateIsNotNullAndNotEmpty(tempObject[9])) {
				holder.setBlockEntityFk(Long.parseLong(tempObject[9].toString()));
			}
			if (Validations.validateIsNotNullAndNotEmpty(tempObject[10])) {
				holder.setDocumentType(Integer.parseInt(tempObject[10]
						.toString()));
			}
			if (Validations.validateIsNotNullAndNotEmpty(tempObject[11])) {
				holder.setDocumentNumber(tempObject[11].toString());
			}

			listHolder.add(holder);
		}
		return listHolder;
	}

	@TransactionAttribute
	public List<LegalRepresentative> getLegalRepresentative()
			throws ServiceException {
		return holderAccountComponentServiceBean
				.getLegalRepresentative();
	}

	public Holder getHolderByDocumentTypeAndDocumentNumberServiceFacade(
			Holder holder) throws ServiceException {
		
		Holder auxHolder =  holderAccountComponentServiceBean.getHolderByDocumentTypeAndDocumentNumberServiceBean(holder);
		
		if(auxHolder==null){
			holder.setDocumentSource(null);
			auxHolder = holderAccountComponentServiceBean.getHolderByDocumentTypeAndDocumentNumberServiceBean(holder);
		}		
		return auxHolder;
	
	}
	
	public List<Holder> getHoldersServiceFacade(Holder holder){
		List<Holder> listHolder  = holderAccountComponentServiceBean.getHoldersServiceBean(holder);
		if(listHolder==null){
			holder.setDocumentSource(null);
			listHolder  = holderAccountComponentServiceBean.getHoldersServiceBean(holder);
		}
		return listHolder;
	}

	public PepPerson getPepPersonByIdHolderServiceFacade(Long idHolderPk)
			throws ServiceException {
		PepPerson pepPerson = holderAccountComponentServiceBean
				.getPepPersonByIdHolderServiceBean(idHolderPk);
		return pepPerson;
	}

	public LegalRepresentative getLegalRepresentativeByDocumentTypeAndDocumentNumberServiceFacade(
			LegalRepresentative legalRepresentative) throws ServiceException {
		LegalRepresentative legalRepre = holderAccountComponentServiceBean
				.getLegalRepresentativeByDocumentTypeAndDocumentNumberServiceBean(legalRepresentative);

		if (legalRepre != null) {
			if (legalRepre.getLegalRepresentativeFile() != null) {
				legalRepre.getLegalRepresentativeFile().size();
			}
		}
		return legalRepre;
	}

	public RepresentedEntity getRepresentedEntityByIdLegalRepresentativeFacadeBean(
			Long id) throws ServiceException {
		return holderAccountComponentServiceBean
				.getRepresentedEntityByIdLegalRepresentativeServiceBean(id);
	}

	public List<LegalRepresentativeFile> getLegalRepresentativeFileByIdLegalRepresentativeServiceFacade(
			Long id) throws ServiceException {
		List<LegalRepresentativeFile> lstLegalRepresentativeFile = holderAccountComponentServiceBean
				.getLegalRepresentativeFileByIdLegalRepresentativeServiceBean(id);

		for (int i = 0; i < lstLegalRepresentativeFile.size(); i++) {
			lstLegalRepresentativeFile.get(i).getFileLegalRepre();
		}

		return lstLegalRepresentativeFile;
	}

	public PepPerson getPepPersonByIdLegalRepresentativeServiceFacade(
			Long idLegalRepresentative) throws ServiceException {
		PepPerson pepPerson = holderAccountComponentServiceBean
				.getPepPersonByIdLegalRepresentativeServiceBean(idLegalRepresentative);

		return pepPerson;
	}

	public InstitutionInformation findInstitutionInformationByIdServiceFacade(
			InstitutionInformation rncFilter) throws ServiceException {
		return holderAccountComponentServiceBean
				.findInstitutionInformationByIdServiceBean(rncFilter);
	}

	public LegalRepresentative getLegalRepresentativeBySecondDocumentTypeAndSecondDocumentNumberServiceFacade(
			LegalRepresentative legalRepresentative) throws ServiceException {
		return holderAccountComponentServiceBean
				.getLegalRepresentativeBySecondDocumentTypeAndSecondDocumentNumberServiceBean(legalRepresentative);
	}

	public Holder getHolderBySecondDocumentTypeAndSecondDocumentNumberServiceFacade(
			Holder holder) throws ServiceException {
		return holderAccountComponentServiceBean
				.getHolderBySecondDocumentTypeAndSecondDocumentNumberServiceBean(holder);
	}

	public Integer validateExistPepPersonByIdLegalRepresentativeServiceFacade(
			Long idLegalRepresentative) throws ServiceException {
		return holderAccountComponentServiceBean
				.validateExistPepPersonByIdLegalRepresentativeServiceBean(idLegalRepresentative);
	}

	public RepresentativeFileHistory getContentFileRepresentativeHistoryServiceFacade(Long id)
			throws ServiceException {
		RepresentativeFileHistory representativeFileHistory = holderAccountComponentServiceBean
				.getContentFileRepresentativeHistoryServiceBean(id);

		if (representativeFileHistory != null) {
			representativeFileHistory.getFileRepresentative();
		}

		return representativeFileHistory;
	}

	public LegalRepresentativeFile getContentFileLegalRepresentativeServiceFacade(
			Long id) throws ServiceException {
		LegalRepresentativeFile legalRepresentativeFile = holderAccountComponentServiceBean
				.getContentFileLegalRepresentativeServiceBean(id);

		if (legalRepresentativeFile != null) {
			legalRepresentativeFile.getFileLegalRepre();
		}
		return legalRepresentativeFile;
	}
	
	public void removeLegalRepresentativeHistoryFileServiceFacade(Long id) throws ServiceException{
		holderAccountComponentServiceBean.removeLegalRepresentativeHistoryFileServiceBean(id);
	}
	public void removeLegalRepresentativeServiceFacade(Long id)throws ServiceException{
		holderAccountComponentServiceBean.removeLegalRepresentativeServiceBean(id);
	}
	
	public int deleteLegalRepresentativeFileServiceFacade(LegalRepresentative legalRepresentative, LoggerUser loggerUser)throws ServiceException {
		 return holderAccountComponentServiceBean.deleteLegalRepresentativeFileServiceBean(legalRepresentative, loggerUser);
	}
//	public boolean validityHolderAccountBalanceByHolder(Holder holder){
//	   return balanceMovementsServiceBean.verifyHolderAccountBalanceByHolder(holder);
//	  
//	}
	
	public Holder getHolderServiceFacade(Long idHolderPk) throws ServiceException{
	       Holder holder =   holderAccountComponentServiceBean.getHolderServiceBean(idHolderPk);
	       
	       List<RepresentedEntity> lstRepresentedEntity;
	       lstRepresentedEntity = holder.getRepresentedEntity();
		   holder.setRepresentedEntity(null);
		   List<RepresentedEntity> lstRepresentedEntityResult = new ArrayList<RepresentedEntity>();
		   
		   
		   for(RepresentedEntity rE : lstRepresentedEntity){
			   if(!rE.getStateRepresented().equals(RepresentativeEntityStateType.DELETED.getCode())){
				   lstRepresentedEntityResult.add(rE);
			   }
		   }
		   
		   holder.setRepresentedEntity(lstRepresentedEntityResult);
		   
		          
	       for(int i=0;i<holder.getRepresentedEntity().size();i++){
	    	   holder.getRepresentedEntity().get(i).getLegalRepresentative().getIdLegalRepresentativePk();
	    	   
	    	   if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalRepresentativeFile()!=null){
	    		   holder.getRepresentedEntity().get(i).getLegalRepresentative().getLegalRepresentativeFile().size();
	    	   }
	       }
	       if(holder.getHolderFile()!=null){    	   
	    	     
	    	     List<HolderFile> lstHolderFile;    			 
				 lstHolderFile = holder.getHolderFile();
				 holder.setHolderFile(null);
			     List<HolderFile> lstHolderFileResult  = new ArrayList<HolderFile>();
				 
				 for(HolderFile hF:lstHolderFile){
					 if(!hF.getStateFile().equals(HolderFileStateType.DELETED.getCode())){
						 lstHolderFileResult.add(hF);
					 }
				 }
				 
				 holder.setHolderFile(lstHolderFileResult);
				 holder.getHolderFile().size();
	       }
	       
	       if(holder.getParticipant()!=null){
	    	   holder.getParticipant().getIdParticipantPk();
	       }
	       
	       return holder;
	         
	     }
	
	
	public boolean validateExistHolderServiceFacade(Long idHolderPk){
		return  holderAccountComponentServiceBean.validateExistHolderServiceBean(idHolderPk);
	}
	public Participant getParticipantByPk(Long idParticipant){
		return participantServiceBean.getParticipantByPk(idParticipant);
	}
	public Participant getParticipantForIssuerDpfDpa(String idIssuerDpfDpa) throws ServiceException {
		return participantServiceBean.getParticipantForIssuerDpfDpa(idIssuerDpfDpa);
	}
	
	/**
	 * Get Participant For Agencia
	 * @param idParticipantPk
	 * @return
	 */
	public List<Object[]> getParticipantForAgencia(Long idParticipantPk) {
		return participantServiceBean.getParticipantForAgencia(idParticipantPk);
	}
	
	public List<SecuritiesManager> getSecuritiesManagers() throws ServiceException {
		return participantServiceBean.getSecuritiesManagers(); 
	}
	
	public List<NegotiationModality> getModalityByInstrumentType(Long mechanismPk, Integer instrumentType, Long participantPk) throws ServiceException{
		return holderAccountComponentServiceBean.getListNegotiationModality(mechanismPk, instrumentType, participantPk);
	}
}