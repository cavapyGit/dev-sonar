package com.pradera.core.component.helperui.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.helperui.to.IssuanceSearchTO;
import com.pradera.core.component.helperui.to.IssuanceSearcherTO;
import com.pradera.core.component.helperui.to.SecuritiesSearcherTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuance;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class IssuanceQueryServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 16/04/2013
 */

@Stateless
public class IssuanceQueryServiceBean extends CrudDaoServiceBean{
	
	@Inject @Configurable
	Integer maxResultsQueryHelper;
	
	//GETS ISSUANCES BY FILTER
	public List<Issuance> getIssuanceByFilterService(IssuanceSearcherTO issuanceSearcherTO){
		StringBuilder sb = new StringBuilder();
		sb.append(" select i from Issuance i where 1=1 ");
		
		if(StringUtils.isNotBlank(issuanceSearcherTO.getIdIssuanceCodePk())){
			sb.append(" and i.idIssuanceCodePk like  :issuanceCode ");
		}
		if(StringUtils.isNotBlank(issuanceSearcherTO.getDescription())){
			sb.append(" and i.description like  :issuanceDesc ");
		}
		if(StringUtils.isNotEmpty(issuanceSearcherTO.getIdIssuerPk())){
			sb.append(" and i.issuer.idIssuerPk like  :issuerCode ");
		}
		if(StringUtils.isNotEmpty(issuanceSearcherTO.getDescription())){
			sb.append(" and i.issuer.description like  :issuerDesc ");
		}
		if(Validations.validateIsNotNullAndPositive(issuanceSearcherTO.getIssuerDocumentType())){
			sb.append(" and i.issuer.documentType= :issuerDocType ");
		}
		if(StringUtils.isNotBlank(issuanceSearcherTO.getIssuerDocumentNumber())){
			sb.append(" and i.issuer.documentNumber like  :documentNumber ");
		}
		if(Validations.validateIsNotNullAndPositive(issuanceSearcherTO.getInstrumentType())){
			sb.append(" and i.instrumentType= :instrumentType ");
		}
		if(Validations.validateIsNotNullAndPositive(issuanceSearcherTO.getSecurityType())){
			sb.append(" and i.securityType= :securityType ");
		}
		if(Validations.validateIsNotNullAndPositive(issuanceSearcherTO.getSecurityClass())){
			sb.append(" and i.securityClass= :securityClass ");
		}
		if(Validations.validateIsNotNullAndPositive(issuanceSearcherTO.getIssuanceState())){
			sb.append(" and i.stateIssuance= :issuanceStatus ");
		}
		
		TypedQuery<Issuance> query = em.createQuery(sb.toString(), Issuance.class);
		
		if(StringUtils.isNotBlank(issuanceSearcherTO.getIdIssuanceCodePk())){
			query.setParameter("issuanceCode", "%"+issuanceSearcherTO.getIdIssuanceCodePk()+"%");
		}
		if(StringUtils.isNotBlank(issuanceSearcherTO.getDescription())){
			query.setParameter("issuanceDesc", "%"+issuanceSearcherTO.getDescription()+"%");
		}
		if(StringUtils.isNotEmpty(issuanceSearcherTO.getIdIssuerPk())){
			query.setParameter("issuerCode", "%"+issuanceSearcherTO.getIdIssuerPk()+"%");
		}
		if(StringUtils.isNotEmpty(issuanceSearcherTO.getDescription())){
			query.setParameter("issuerDesc","%"+ issuanceSearcherTO.getDescription()+"%");
		}
		if(Validations.validateIsNotNullAndPositive(issuanceSearcherTO.getIssuerDocumentType())){
			query.setParameter("issuerDocType", issuanceSearcherTO.getIssuerDocumentType());
		}
		if(StringUtils.isNotBlank(issuanceSearcherTO.getIssuerDocumentNumber())){
			query.setParameter("issuerDocNumber", "%"+issuanceSearcherTO.getIssuerDocumentNumber()+"%");
		}
		if(Validations.validateIsNotNullAndPositive(issuanceSearcherTO.getInstrumentType())){
			query.setParameter("instrumentType", issuanceSearcherTO.getInstrumentType());
		}
		if(Validations.validateIsNotNullAndPositive(issuanceSearcherTO.getSecurityType())){
			query.setParameter("securityType", issuanceSearcherTO.getSecurityType());
		}
		if(Validations.validateIsNotNullAndPositive(issuanceSearcherTO.getSecurityClass())){
			query.setParameter("securityClass", issuanceSearcherTO.getSecurityClass());
		}
		if(Validations.validateIsNotNullAndPositive(issuanceSearcherTO.getIssuanceState())){
			query.setParameter("issuanceStatus", issuanceSearcherTO.getIssuanceState());
		}
		
		return query.getResultList();
	}
	
	public List<Issuance> findIssuancesServiceBean(IssuanceSearcherTO issuanceSearcherTO) throws ServiceException{
		CriteriaBuilder cb=em.getCriteriaBuilder();
		CriteriaQuery<Issuance> cq=cb.createQuery(Issuance.class);
		Root<Issuance> issuanceRoot=cq.from(Issuance.class);
		issuanceRoot.fetch("issuer");
		issuanceRoot.fetch("geographicLocation");

		List<Predicate> criteriaParameters=new ArrayList<Predicate>();
		cq.select(issuanceRoot);
		cq.distinct(true);
		
		if(Validations.validateIsNotNullAndNotEmpty( issuanceSearcherTO.getIdIssuerPk() )){
			criteriaParameters.add(cb.equal(issuanceRoot.<String>get("issuer").get("idIssuerPk"), issuanceSearcherTO.getIdIssuerPk()));
		}
		if(Validations.validateIsNotNullAndNotEmpty( issuanceSearcherTO.getInstrumentType() ) && !issuanceSearcherTO.getInstrumentType().equals(0)){
			criteriaParameters.add(cb.equal(issuanceRoot.<Integer>get("instrumentType"), issuanceSearcherTO.getInstrumentType()));
		}
		if(Validations.validateIsNotNullAndNotEmpty( issuanceSearcherTO.getSecurityType() ) && !issuanceSearcherTO.getSecurityType().equals(0)){
			criteriaParameters.add(cb.equal(issuanceRoot.<Integer>get("securityType"), issuanceSearcherTO.getSecurityType()));
		}
		if(Validations.validateIsNotNullAndNotEmpty( issuanceSearcherTO.getSecurityClass() ) && !issuanceSearcherTO.getSecurityClass().equals(0)){
			criteriaParameters.add(cb.equal(issuanceRoot.<Integer>get("securityClass"), issuanceSearcherTO.getSecurityClass()));
		}
		if(Validations.validateIsNotNullAndNotEmpty( issuanceSearcherTO.getIdIssuanceCodePk()  )){
			criteriaParameters.add(cb.like(issuanceRoot.<String>get("idIssuanceCodePk"), issuanceSearcherTO.getIdIssuanceCodePk()));
		}
		if(Validations.validateIsNotNullAndNotEmpty( issuanceSearcherTO.getDescription()  )){
			criteriaParameters.add(cb.like(issuanceRoot.<String>get("description"), issuanceSearcherTO.getDescription()));
		}
		if(Validations.validateIsNotNullAndNotEmpty( issuanceSearcherTO.getIssuanceState() ) && !issuanceSearcherTO.getIssuanceState().equals(0)){
			criteriaParameters.add(cb.equal(issuanceRoot.<Integer>get("stateIssuance"), issuanceSearcherTO.getIssuanceState()));
		}
		cq.where( cb.and( criteriaParameters.toArray(new Predicate[criteriaParameters.size()]) ) );
		
		TypedQuery<Issuance> issuanceCriteria=em.createQuery(cq);
		return (List<Issuance>) issuanceCriteria.getResultList();
	}

	//LIST SECURITY CLASSES BY SECURITY TYPE
		public List<Object[]> searchSecurityClassBySecurityType(SecuritiesSearcherTO filter){
			StringBuilder sb = new StringBuilder();
			
			sb.append(	"	SELECT 						" +
						"		PT52.PARAMETER_TABLE_PK, PT52.PARAMETER_NAME" +
						"	FROM						" +
						"		PARAMETER_TABLE PT50, 	" +
						"		PARAMETER_TABLE PT51,	" +
						"		PARAMETER_TABLE PT52	" +
						"	WHERE						" );
			sb.append(	" 		PT52.MASTER_TABLE_FK = 	"+MasterTableType.SECURITIES_CLASS.getCode());
			sb.append(	"	AND							" +
						"		PT51.PARAMETER_TABLE_PK = PT52.ID_RELATED_PARAMETER_TABLE_FK " +
						"	AND	" +
						"		PT50.PARAMETER_TABLE_PK = PT51.ID_RELATED_PARAMETER_TABLE_FK ");	
			if(Validations.validateIsNotNullAndPositive(filter.getInstrumentType())){
				sb.append("	AND	PT50.PARAMETER_TABLE_PK = :instrumentType ");
			}
			if(Validations.validateIsNotNullAndPositive(filter.getSecuritiesType())){
				sb.append("	AND	PT51.PARAMETER_TABLE_PK = :securityType ");
			}
			
			Query query = em.createNativeQuery(sb.toString());
			
			if(Validations.validateIsNotNullAndPositive(filter.getInstrumentType())){
				query.setParameter("instrumentType", filter.getInstrumentType());
			}
			if(Validations.validateIsNotNullAndPositive(filter.getSecuritiesType())){
				query.setParameter("securityType", filter.getSecuritiesType());
			}			
			
			return query.getResultList();
		}

	public Issuance findIssuance(Issuance issuance) {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT ice");
			sbQuery.append("	FROM Issuance ice");
			sbQuery.append("	WHERE ice.idIssuanceCodePk = :idIssuanceCodePk");
			if(Validations.validateIsNotNullAndNotEmpty(issuance.getIssuer()) &&
					Validations.validateIsNotNullAndNotEmpty(issuance.getIssuer().getIdIssuerPk()))
				sbQuery.append("	AND ice.issuer.idIssuerPk = :idIssuerPk");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idIssuanceCodePk", issuance.getIdIssuanceCodePk());
			if(Validations.validateIsNotNullAndNotEmpty(issuance.getIssuer()) &&
					Validations.validateIsNotNullAndNotEmpty(issuance.getIssuer().getIdIssuerPk()))
				query.setParameter("idIssuerPk", issuance.getIssuer().getIdIssuerPk());
			return (Issuance)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Issuance> findIssuances(IssuanceSearchTO issuanceSearchTO) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ice.idIssuanceCodePk,");
		sbQuery.append("			ice.description,");
		sbQuery.append("			ice.securityType,");
		sbQuery.append("			ice.securityClass");
		sbQuery.append("	FROM Issuance ice");
		sbQuery.append("	WHERE ice.stateIssuance = :stateIssuance");
		sbQuery.append("	AND ice.instrumentType = :instrumentType");
		if(Validations.validateIsNotNullAndNotEmpty(issuanceSearchTO.getIssuer()) &&
				Validations.validateIsNotNullAndNotEmpty(issuanceSearchTO.getIssuer().getIdIssuerPk()))
			sbQuery.append("	AND ice.issuer.idIssuerPk = :idIssuerPk");
		if(Validations.validateIsNotNullAndNotEmpty(issuanceSearchTO.getSecurityType()))
			sbQuery.append("	AND ice.securityType = :securityType");
		if(Validations.validateIsNotNullAndNotEmpty(issuanceSearchTO.getSecurityClass()))
			sbQuery.append("	AND ice.securityClass = :securityClass");
		if(Validations.validateIsNotNullAndNotEmpty(issuanceSearchTO.getDescription()))
			sbQuery.append("	AND ice.description like :description");
		sbQuery.append("	ORDER BY ice.idIssuanceCodePk DESC");
		Query query = em.createQuery(sbQuery.toString());
		//set maximum result list
		query.setMaxResults(maxResultsQueryHelper);
		query.setParameter("instrumentType", issuanceSearchTO.getInstrumentType());
		query.setParameter("stateIssuance", issuanceSearchTO.getState());
		if(Validations.validateIsNotNullAndNotEmpty(issuanceSearchTO.getIssuer()) &&
				Validations.validateIsNotNullAndNotEmpty(issuanceSearchTO.getIssuer().getIdIssuerPk()))
			query.setParameter("idIssuerPk", issuanceSearchTO.getIssuer().getIdIssuerPk());
		if(Validations.validateIsNotNullAndNotEmpty(issuanceSearchTO.getSecurityType()))
			query.setParameter("securityType", issuanceSearchTO.getSecurityType());
		if(Validations.validateIsNotNullAndNotEmpty(issuanceSearchTO.getSecurityClass()))
			query.setParameter("securityClass", issuanceSearchTO.getSecurityClass());
		if(Validations.validateIsNotNullAndNotEmpty(issuanceSearchTO.getDescription()))
			query.setParameter("description", GeneralConstants.PERCENTAGE_STRING + 
												issuanceSearchTO.getDescription() +
												GeneralConstants.PERCENTAGE_STRING);
		List<Object[]> lstResult = (List<Object[]>)query.getResultList();
		List<Issuance> lstIssuance = null;
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
			lstIssuance = new ArrayList<Issuance>();
			for(Object[] object:lstResult){
				Issuance issuance = new Issuance();
				issuance.setIdIssuanceCodePk(object[0].toString());
				issuance.setDescription(object[1].toString());
				issuance.setSecurityType((Integer)object[2]);
				issuance.setSecurityClass((Integer)object[3]);
				lstIssuance.add(issuance);
			}
		}
		return lstIssuance;
	}
}
