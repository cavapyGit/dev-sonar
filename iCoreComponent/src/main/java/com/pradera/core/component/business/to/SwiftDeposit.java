package com.pradera.core.component.business.to;

import java.util.List;


public class SwiftDeposit {

	private List<SwiftMessagesDeposit> deposits;
	private String file;
	private String fileName;

	public List<SwiftMessagesDeposit> getDeposits() {
		return deposits;
	}
	public void setDeposits(List<SwiftMessagesDeposit> deposits) {
		this.deposits = deposits;
	}
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}