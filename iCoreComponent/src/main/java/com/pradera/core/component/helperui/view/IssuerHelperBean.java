package com.pradera.core.component.helperui.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.el.ELContext;
import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.HelperBean;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.IssuerSearchTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.EconomicSectorType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class IssuerHelperBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 12-jun-2015
 */
@HelperBean
public class IssuerHelperBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	/** The max results query helper. */
	@Inject @Configurable
	Integer maxResultsQueryHelper;
	
	/** The issuer search to. */
	private IssuerSearchTO issuerSearchTO;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		issuerSearchTO = new IssuerSearchTO();
	}
	
	/**
	 * Load help.
	 */
	public void loadHelp(){
		try {		
			issuerSearchTO = new IssuerSearchTO();
			fillCombos();
			issuerSearchTO.setState(IssuerStateType.REGISTERED.getCode());
			issuerSearchTO.setEconomicSector(EconomicSectorType.FINANCIAL_PUBLIC_SECTOR.getCode());
			RequestContext.getCurrentInstance().execute("PF('dlgIssuer').show();");
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Search issuers.
	 */
	public void searchIssuers(boolean disableConsolidate){
		try {
			if(Validations.validateIsNullOrEmpty(issuerSearchTO.getState())){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getGenericMessage("issuer.helper.alert.state"));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			if(Validations.validateIsNullOrEmpty(issuerSearchTO.getEconomicSector()) && !disableConsolidate){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getGenericMessage("issuer.helper.alert.economicsector"));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			issuerSearchTO.setLstIssuer(null);
			issuerSearchTO.setBlNoResult(true);
			//limit Max Result to 100
			issuerSearchTO.setLimitMaxResult(true);
			List<Issuer> lstResult = helperComponentFacade.findIssuers(issuerSearchTO);
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				issuerSearchTO.setLstIssuer(new GenericDataModel<Issuer>(lstResult));
				issuerSearchTO.setBlNoResult(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Clean result.
	 */
	public void cleanResult(){
		issuerSearchTO.setLstIssuer(null);
		issuerSearchTO.setBlNoResult(false);
	}
	
	/**
	 * Clean search.
	 */
	public void cleanSearch(){
		JSFUtilities.resetViewRoot();
		cleanResult();
		issuerSearchTO.setDocumentNumber(null);
		issuerSearchTO.setIdIssuerCode(null);
		issuerSearchTO.setDocumentType(null);
		issuerSearchTO.setMnemonic(null);
		issuerSearchTO.setFullName(null);
	}
	
	/**
	 * Gets the issuer.
	 *
	 * @param issuer the issuer
	 * @return the issuer
	 */
	public void getIssuer(Issuer issuer){
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.issuer}", Issuer.class);	
		valueExpression.setValue(elContext, helperComponentFacade.findIssuer(null,issuer.getMnemonic()));
	}
	
	/**
	 * Find issuer.
	 */
	public void findIssuer(){
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.issuer}", Issuer.class);
		Issuer issuer = (Issuer) valueExpression.getValue(elContext);		
		if(Validations.validateIsNotNullAndNotEmpty(issuer.getMnemonic())){
			Issuer issuerTemp = helperComponentFacade.findIssuer(null,issuer.getMnemonic());
			if(Validations.validateIsNotNull(issuerTemp)){
				if(IssuerStateType.PENDING.getCode().equals(issuerTemp.getStateIssuer())){
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getGenericMessage("issuer.helper.alert.statepending", new Object[]{issuer.getIdIssuerPk()}));
					JSFUtilities.showSimpleValidationDialog();
					valueExpression.setValue(elContext, new Issuer());
				}else {
					valueExpression.setValue(elContext, issuerTemp);
				}
			}else{
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getGenericMessage("issuer.helper.alert.notexist", new Object[]{issuer.getIdIssuerPk()}));
				JSFUtilities.showSimpleValidationDialog();
				valueExpression.setValue(elContext, new Issuer());
			}
		}else{
			valueExpression.setValue(elContext, new Issuer());
		}		
	}
	
	/**
	 * Fill combos.
	 *
	 * @throws ServiceException the service exception
	 */
	private void fillCombos() throws ServiceException{
		ParameterTableTO paramTabTO = new ParameterTableTO();
		paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
		paramTabTO.setMasterTableFk(MasterTableType.ISSUER_STATE.getCode());
		List<Integer> lstParameterTablePkNotIn = new ArrayList<Integer>();
		lstParameterTablePkNotIn.add(IssuerStateType.PENDING.getCode());
		paramTabTO.setLstParameterTablePkNotIn(lstParameterTablePkNotIn);
		issuerSearchTO.setLstState(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
		
		paramTabTO.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
		List<Integer> lstParameterTablePk = new ArrayList<Integer>();
		lstParameterTablePk.add(DocumentType.RUC.getCode());
		lstParameterTablePk.add(DocumentType.DIO.getCode());
		paramTabTO.setLstParameterTablePk(lstParameterTablePk);
		issuerSearchTO.setLstDocumentType(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
		paramTabTO.setLstParameterTablePk(null);
		
		paramTabTO.setMasterTableFk(MasterTableType.ECONOMIC_SECTOR.getCode());
		issuerSearchTO.setLstEconomicSector(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
	}
	
	/**
	 * Gets the issuer search to.
	 *
	 * @return the issuer search to
	 */
	public IssuerSearchTO getIssuerSearchTO() {
		return issuerSearchTO;
	}
	
	/**
	 * Sets the issuer search to.
	 *
	 * @param issuerSearchTO the new issuer search to
	 */
	public void setIssuerSearchTO(IssuerSearchTO issuerSearchTO) {
		this.issuerSearchTO = issuerSearchTO;
	}

	/**
	 * Gets the max results query helper.
	 *
	 * @return the max results query helper
	 */
	public Integer getMaxResultsQueryHelper() {
		return maxResultsQueryHelper;
	}	
}
