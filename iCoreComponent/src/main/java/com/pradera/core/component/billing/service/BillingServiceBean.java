package com.pradera.core.component.billing.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.billing.RateExclusion;
import com.pradera.model.billing.type.RateExceptionStatus;


//TODO: Auto-generated Javadoc
/**
* The Class BillingServiceBean.
*/
@Stateless
public class BillingServiceBean extends CrudDaoServiceBean {
	
	
	
     public List<RateExclusion>   findRateExclusionListByIdServiceRatePk(Long serviceRatePk){
		
		StringBuilder stringBuilderSql = new StringBuilder();	
		stringBuilderSql.append("SELECT re FROM  RateExclusion re  ");
		
		
		stringBuilderSql.append(" WHERE 1=1   ");	
		
		if (Validations.validateIsNotNull(serviceRatePk)){
			 
			stringBuilderSql.append(" and re.serviceRate.idServiceRatePk = :serviceRatePk  ");
			stringBuilderSql.append(" and re.exclusionState = :exclusionState   ");
		}
		
		Query query = em.createQuery(stringBuilderSql.toString());
		
		if (Validations.validateIsNotNull(serviceRatePk)){
			 
			query.setParameter("serviceRatePk",serviceRatePk );
			query.setParameter("exclusionState", RateExceptionStatus.REGISTER.getCode() );
		}
		
		List<RateExclusion> listObject=null;
		try {
			listObject=query.getResultList();
		
		} catch (Exception e) {
		
		}
		return listObject;
	}	
	

}
