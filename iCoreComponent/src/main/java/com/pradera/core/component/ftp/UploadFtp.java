package com.pradera.core.component.ftp;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.utils.FileData;
import com.pradera.core.component.swift.to.FTPParameters;
import com.pradera.integration.common.validation.Validations;


// TODO: Auto-generated Javadoc
/**
 * The Class UploadFtp.
 */
public class UploadFtp {
	
	/** The interface sending facade. */
	private FTPParameters ftpParameters;

	/**
	 * Instantiates a new upload ftp.
	 *
	 * @param fTPParameters the f tp parameters
	 */
	public UploadFtp(FTPParameters fTPParameters) {
		ftpParameters=new FTPParameters();
		ftpParameters.setServer(fTPParameters.getServer());
		ftpParameters.setUser(fTPParameters.getUser());
		ftpParameters.setPass(fTPParameters.getPass());
		ftpParameters.setRemotePath(fTPParameters.getRemotePath());
		ftpParameters.setTempInterfaceFile(fTPParameters.getTempInterfaceFile());
		ftpParameters.setReportData(fTPParameters.getReportData());
		ftpParameters.setListFileData(fTPParameters.getListFileData());
	}
	
	/**
	 * Register and send file.
	 *
	 * @param fileName the file name
	 */
	@LoggerAuditWeb
	public void registerAndSendFile(String fileName){
		if(Validations.validateIsNotNullAndNotEmpty(fileName)){
			uploadFileFTP(ftpParameters, fileName);			
		}		
	}
	
	
	/**
	 * Upload file by ftp.
	 *
	 * @param ftpParameters the ftp parameters
	 * @param fileName the file name
	 * @return true, if successful
	 */
	protected boolean uploadFileByFTP(FTPParameters ftpParameters, String fileName) {
		try {
			StringBuilder urlSb=new StringBuilder();
			urlSb.append("ftp://").
					append(ftpParameters.getUser()).append(":").
					append(ftpParameters.getPass()).append("@").
					append(ftpParameters.getServer()).append(ftpParameters.getRemotePath()).append(fileName).
					append(";type=i");
			URL url = new URL(urlSb.toString());
			URLConnection urlc = url.openConnection();
			OutputStream os = urlc.getOutputStream();
			BufferedReader br = new BufferedReader(new FileReader(ftpParameters.getTempInterfaceFile()));
			int c;
			while ((c = br.read()) != -1) {
				os.write(c);
			}
			os.flush();
			os.close();
			br.close();
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		
		}
	}
	
	/**
	 * Upload file FTP.
	 *
	 * @param ftpParameters the FTP parameters
	 * @param fileName the file name
	 * @return true, if successful
	 */
	public boolean uploadFileFTP(FTPParameters ftpParameters, String fileName) {
		try {
			//new ftp client
	        FTPClient ftp = new FTPClient();
	        //try to connect
	        ftp.connect(ftpParameters.getServer());
	        //login to server
	        if(!ftp.login(ftpParameters.getUser(), ftpParameters.getPass())){
	            ftp.logout();
	            return false;
	        }
	        int reply = ftp.getReplyCode();
	        //FTPReply stores a set of constants for FTP reply codes. 
	        if (!FTPReply.isPositiveCompletion(reply)) {
	            ftp.disconnect();
	            return false;
	        }	 
	        	        
	        //enter passive mode
	        ftp.enterLocalPassiveMode();	        
	        //change current directory
	        if(!ftp.changeWorkingDirectory(ftpParameters.getRemotePath())){
	        	ftp.makeDirectory(ftpParameters.getRemotePath());
	        } else {
	        	ftp.changeWorkingDirectory(ftpParameters.getRemotePath());
	        }	        
	        //get input stream
	        InputStream inputStream = new ByteArrayInputStream(ftpParameters.getReportData());	  
	        ftp.setFileType(FTP.BINARY_FILE_TYPE);      
	        ftp.setFileTransferMode(FTP.BINARY_FILE_TYPE);
	        ftp.enterLocalPassiveMode();
	        //store the file in the remote server
	        ftp.storeFile(fileName, inputStream);
	        //close the stream
	        inputStream.close();
	        ftp.logout();
	        ftp.disconnect();	        
		} catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
	}
	
	/**
	 * Register and send file massive.
	 *
	 * @param fileName the file name
	 */
	public boolean registerAndSendFileMassive(){
		return uploadFileFTPMassive(ftpParameters);		
	}
	
	/**
	 * Upload file FTP massive.
	 *
	 * @param ftpParameters the FTP parameters
	 * @param fileName the file name
	 * @return true, if successful
	 */
	public boolean uploadFileFTPMassive(FTPParameters ftpParameters) {
		try {
			//new ftp client
	        FTPClient ftp = new FTPClient();
	        //try to connect
	        ftp.connect(ftpParameters.getServer());
	        //login to server
	        if(!ftp.login(ftpParameters.getUser(), ftpParameters.getPass())){
	            ftp.logout();
	            return false;
	        }
	        int reply = ftp.getReplyCode();
	        //FTPReply stores a set of constants for FTP reply codes. 
	        if (!FTPReply.isPositiveCompletion(reply)) {
	            ftp.disconnect();
	            return false;
	        }	 
	        	        
	        //enter passive mode
	        ftp.enterLocalPassiveMode();	        	        	       
	        
	        // get initial input stream	        
	        if(Validations.validateListIsNotNullAndNotEmpty(ftpParameters.getListFileData())){
	        	for(FileData objFileData : ftpParameters.getListFileData()){
	        		if(Validations.validateIsNotNullAndNotEmpty(objFileData.getFilePath())) {
	        			// change current directory
		    	        if(!ftp.changeWorkingDirectory(objFileData.getFilePath())){
		    	        	boolean createFolder = ftp.makeDirectory(objFileData.getFilePath());
		    	        	if(createFolder){
		    	        		ftp.changeWorkingDirectory(objFileData.getFilePath());
		    	        	} else {
		    	        		return false;
		    	        	}	        	
		    	        } else {
		    	        	ftp.changeWorkingDirectory(objFileData.getFilePath());
		    	        }
	        		} else {
	        			// change current directory
		    	        if(!ftp.changeWorkingDirectory(ftpParameters.getRemotePath())){
		    	        	boolean createFolder = ftp.makeDirectory(ftpParameters.getRemotePath());
		    	        	if(createFolder){
		    	        		ftp.changeWorkingDirectory(ftpParameters.getRemotePath());
		    	        	} else {
		    	        		return false;
		    	        	}	        	
		    	        } else {
		    	        	ftp.changeWorkingDirectory(ftpParameters.getRemotePath());
		    	        }
	        		}	        			    	        
	    	        // get report Data	        		
	        		byte[] arrayByte = objFileData.getReportData();
	        		InputStream inputStream = new ByteArrayInputStream(arrayByte);	  
	     	        ftp.setFileType(FTP.BINARY_FILE_TYPE);      
	     	        ftp.setFileTransferMode(FTP.BINARY_FILE_TYPE);
	     	        ftp.enterLocalPassiveMode();
	     	        //store the file in the remote server
	     	        ftp.storeFile(objFileData.getReportName(), inputStream);
	     	        //close the stream
	     	        inputStream.close();
	        	}
	        }
	        // end input stream	        
	        ftp.logout();
	        ftp.disconnect();	        
		} catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
	}
	
	/**
	 * Gets the ftp parameters.
	 *
	 * @return the ftpParameters
	 */
	public FTPParameters getFtpParameters() {
		return ftpParameters;
	}

	/**
	 * Sets the ftp parameters.
	 *
	 * @param ftpParameters the ftpParameters to set
	 */
	public void setFtpParameters(FTPParameters ftpParameters) {
		this.ftpParameters = ftpParameters;
	}

	
}
