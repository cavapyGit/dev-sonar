package com.pradera.core.component.business.to;

import java.io.Serializable;
import java.util.Date;

import com.pradera.model.component.MovementType;


public class HolderAccountMovementTo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private MovementType movementType;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;

	
	
	public MovementType getMovementType() {
		return movementType;
	}

	public void setMovementType(MovementType movementType) {
		this.movementType = movementType;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}
	
	
	
}
