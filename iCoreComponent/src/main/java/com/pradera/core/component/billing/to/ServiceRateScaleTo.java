package com.pradera.core.component.billing.to;

import java.io.Serializable;
import java.math.BigDecimal;


public class ServiceRateScaleTo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long idServiceRateScalePk;
	private BigDecimal maximumCollectionAmount;
	private BigDecimal maximumRange;
	private BigDecimal minimumCollectionAmount;
	private BigDecimal minimumRange;
	private BigDecimal scalePercent;
	private BigDecimal scaleAmount;
	private Boolean disableMaximumRange = Boolean.TRUE;
	private Boolean disableMinimumRange = Boolean.TRUE;
	private Boolean disableCollection= Boolean.TRUE;
    
	private Integer sequenceCode;
	private Integer scaleType;
	private Integer indAccumulativeScale;
	private Integer indStaging;
	
	private Integer indOtherCurrency;
	private Integer otherCurrency;
	
	public BigDecimal getScaleAmount() {
		return scaleAmount;
	}


	public void setScaleAmount(BigDecimal scaleAmount) {
		this.scaleAmount = scaleAmount;
	}


	public Boolean getDisableMaximumRange() {
		return disableMaximumRange;
	}


	public void setDisableMaximumRange(Boolean disableMaximumRange) {
		this.disableMaximumRange = disableMaximumRange;
	}

	
	public Boolean getDisableCollection() {
		return disableCollection;
	}


	public void setDisableCollection(Boolean disableCollection) {
		this.disableCollection = disableCollection;
	}


	public Boolean getDisableMinimumRange() {
		return disableMinimumRange;
	}


	public void setDisableMinimumRange(Boolean disableMinimumRange) {
		this.disableMinimumRange = disableMinimumRange;
	}


	public Long getIdServiceRateScalePk() {
		return idServiceRateScalePk;
	}
	
	
	public Integer getSequenceCode() {
		return sequenceCode;
	}


	public void setSequenceCode(Integer sequenceCode) {
		this.sequenceCode = sequenceCode;
	}


	public void setIdServiceRateScalePk(Long idServiceRateScalePk) {
		this.idServiceRateScalePk = idServiceRateScalePk;
	}
	public BigDecimal getMaximumCollectionAmount() {
		return maximumCollectionAmount;
	}
	public void setMaximumCollectionAmount(BigDecimal maximumCollectionAmount) {
		this.maximumCollectionAmount = maximumCollectionAmount;
	}
	public BigDecimal getMaximumRange() {
		return maximumRange;
	}
	public void setMaximumRange(BigDecimal maximumRange) {
		this.maximumRange = maximumRange;
	}
	public BigDecimal getMinimumCollectionAmount() {
		return minimumCollectionAmount;
	}
	public void setMinimumCollectionAmount(BigDecimal minimumCollectionAmount) {
		this.minimumCollectionAmount = minimumCollectionAmount;
	}
	public BigDecimal getMinimumRange() {
		return minimumRange;
	}
	public void setMinimumRange(BigDecimal minimumRange) {
		this.minimumRange = minimumRange;
	}
	public BigDecimal getScalePercent() {
		return scalePercent;
	}
	public void setScalePercent(BigDecimal scalePercent) {
		this.scalePercent = scalePercent;
	}	
	
	public Integer getScaleType() {
		return scaleType;
	}

	public void setScaleType(Integer scaleType) {
		this.scaleType = scaleType;
	}

	public void enableRange(){
		disableMaximumRange = Boolean.FALSE;
		disableCollection=Boolean.FALSE;
		disableMinimumRange = Boolean.FALSE;
	}
	
	public void disableRange(){
		disableMaximumRange = Boolean.TRUE;
		disableMinimumRange = Boolean.TRUE;
		//disableCollection=Boolean.TRUE;
	}


	/**
	 * @return the indAccumulativeScale
	 */
	public Integer getIndAccumulativeScale() {
		return indAccumulativeScale;
	}


	/**
	 * @param indAccumulativeScale the indAccumulativeScale to set
	 */
	public void setIndAccumulativeScale(Integer indAccumulativeScale) {
		this.indAccumulativeScale = indAccumulativeScale;
	}


	/**
	 * @return the indStaging
	 */
	public Integer getIndStaging() {
		return indStaging;
	}


	/**
	 * @param indStaging the indStaging to set
	 */
	public void setIndStaging(Integer indStaging) {
		this.indStaging = indStaging;
	}


	/**
	 * @return the indOtherCurrency
	 */
	public Integer getIndOtherCurrency() {
		return indOtherCurrency;
	}


	/**
	 * @param indOtherCurrency the indOtherCurrency to set
	 */
	public void setIndOtherCurrency(Integer indOtherCurrency) {
		this.indOtherCurrency = indOtherCurrency;
	}


	/**
	 * @return the otherCurrency
	 */
	public Integer getOtherCurrency() {
		return otherCurrency;
	}


	/**
	 * @param otherCurrency the otherCurrency to set
	 */
	public void setOtherCurrency(Integer otherCurrency) {
		this.otherCurrency = otherCurrency;
	}
	
	
	
}
