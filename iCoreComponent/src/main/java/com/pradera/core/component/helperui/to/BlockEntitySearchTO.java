package com.pradera.core.component.helperui.to;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.generalparameter.ParameterTable;

public class BlockEntitySearchTO implements Serializable{
	
	private static final long serialVersionUID = 3392761852559476492L;
	private List<ParameterTable> lstPersonType;
	private String name, fullName, firstLastName, secondLastName;
	private Long idHolderPk;
	private Integer	searchType, personType;
	private boolean blByTypePerson, blByRNT, blJuridic, blNatural;
	private boolean blNoResult;
	private GenericDataModel<BlockEntity> lstBlockEntity;
	private Map<Integer, String> mpDocumentType;
	private Map<Integer, String> mpState;
	
	public List<ParameterTable> getLstPersonType() {
		return lstPersonType;
	}
	public void setLstPersonType(List<ParameterTable> lstPersonType) {
		this.lstPersonType = lstPersonType;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getFirstLastName() {
		return firstLastName;
	}
	public void setFirstLastName(String firstLastName) {
		this.firstLastName = firstLastName;
	}
	public String getSecondLastName() {
		return secondLastName;
	}
	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}
	public Long getIdHolderPk() {
		return idHolderPk;
	}
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}
	public Integer getSearchType() {
		return searchType;
	}
	public void setSearchType(Integer searchType) {
		this.searchType = searchType;
	}
	public boolean isBlByTypePerson() {
		return blByTypePerson;
	}
	public void setBlByTypePerson(boolean blByTypePerson) {
		this.blByTypePerson = blByTypePerson;
	}
	public boolean isBlByRNT() {
		return blByRNT;
	}
	public void setBlByRNT(boolean blByRNT) {
		this.blByRNT = blByRNT;
	}
	public boolean isBlJuridic() {
		return blJuridic;
	}
	public void setBlJuridic(boolean blJuridic) {
		this.blJuridic = blJuridic;
	}
	public boolean isBlNatural() {
		return blNatural;
	}
	public void setBlNatural(boolean blNatural) {
		this.blNatural = blNatural;
	}
	public boolean isBlNoResult() {
		return blNoResult;
	}
	public void setBlNoResult(boolean blNoResult) {
		this.blNoResult = blNoResult;
	}
	public GenericDataModel<BlockEntity> getLstBlockEntity() {
		return lstBlockEntity;
	}
	public void setLstBlockEntity(GenericDataModel<BlockEntity> lstBlockEntity) {
		this.lstBlockEntity = lstBlockEntity;
	}
	public Integer getPersonType() {
		return personType;
	}
	public void setPersonType(Integer personType) {
		this.personType = personType;
	}
	public Map<Integer, String> getMpDocumentType() {
		return mpDocumentType;
	}
	public void setMpDocumentType(Map<Integer, String> mpDocumentType) {
		this.mpDocumentType = mpDocumentType;
	}
	public Map<Integer, String> getMpState() {
		return mpState;
	}
	public void setMpState(Map<Integer, String> mpState) {
		this.mpState = mpState;
	}
}
