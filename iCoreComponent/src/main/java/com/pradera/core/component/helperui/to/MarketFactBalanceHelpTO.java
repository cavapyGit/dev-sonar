package com.pradera.core.component.helperui.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author USUARIO
 *
 */
public class MarketFactBalanceHelpTO implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String securityCodePk;
	
	private Long holderAccountPk;
	
	private Long participantPk;
	
	private BigDecimal balanceResult;
	
	private BigDecimal totalBalance;
	
	private BigDecimal availableBalance;
	
	private BigDecimal loanableBalance;
	
	private List<MarketFactDetailHelpTO> marketFacBalances;
	
	private String securityDescription;
	
	private Integer instrumentType;
	
	private String instrumentDescription;
	
	private Date lastMarketDate;
	
	private BigDecimal lastMarketPrice;
	
	private BigDecimal lastMarketRate;
	
	private String uiComponentName;
	
	private Boolean todate;
	
	private Integer indHandleDetails;
	
	/*ATRIBUTO PARA MANEJAR LA VALIDACION SOBRE EL SAlDO TOTAL, POR DEFAULT LA VALIDACION ES SOBRE EL DISPONIBLE*/
	private Integer indTotalBalance;
	/*************************************************************************/
	
	/*ATRIBUTOS PARA MANEJAR HECHOS DE MERCADO PARA SALDOS BLOQUEADOS*/
	private Integer indBlockBalance;
	
	private Long blockOperationNumber;
	
	private Long idBlockOperationPk;
	
	private Integer blockOperationType;
	/*************************************************************************/
	
	/**********ATRIBUTOS PARA MANEJAR LA VALIDACION SOBRE EL SALDO PRESTABLE*********/
	private Integer indLonableBalance;
	
	private Long lonableOperationId;
	/*************************************************************************/

	public String getSecurityCodePk() {
		return securityCodePk;
	}

	public void setSecurityCodePk(String securityCodePk) {
		this.securityCodePk = securityCodePk;
	}

	public Long getHolderAccountPk() {
		return holderAccountPk;
	}

	public void setHolderAccountPk(Long holderAccountPk) {
		this.holderAccountPk = holderAccountPk;
	}

	public Long getParticipantPk() {
		return participantPk;
	}

	public void setParticipantPk(Long participantPk) {
		this.participantPk = participantPk;
	}

	public BigDecimal getBalanceResult() {
		return balanceResult;
	}

	public void setBalanceResult(BigDecimal balanceResult) {
		this.balanceResult = balanceResult;
	}

	public List<MarketFactDetailHelpTO> getMarketFacBalances() {
		return marketFacBalances;
	}

	public void setMarketFacBalances(List<MarketFactDetailHelpTO> marketFacBalances) {
		this.marketFacBalances = marketFacBalances;
	}

	public String getSecurityDescription() {
		return securityDescription;
	}

	public void setSecurityDescription(String securityDescription) {
		this.securityDescription = securityDescription;
	}

	public Integer getInstrumentType() {
		return instrumentType;
	}

	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	public String getInstrumentDescription() {
		return instrumentDescription;
	}

	public void setInstrumentDescription(String instrumentDescription) {
		this.instrumentDescription = instrumentDescription;
	}

	public Date getLastMarketDate() {
		return lastMarketDate;
	}

	public void setLastMarketDate(Date lastMarketDate) {
		this.lastMarketDate = lastMarketDate;
	}

	public BigDecimal getLastMarketPrice() {
		return lastMarketPrice;
	}

	public void setLastMarketPrice(BigDecimal lastMarketPrice) {
		this.lastMarketPrice = lastMarketPrice;
	}

	public BigDecimal getLastMarketRate() {
		return lastMarketRate;
	}

	public void setLastMarketRate(BigDecimal lastMarketRate) {
		this.lastMarketRate = lastMarketRate;
	}

	public String getUiComponentName() {
		return uiComponentName;
	}

	public void setUiComponentName(String uiComponentName) {
		this.uiComponentName = uiComponentName;
	}

	public BigDecimal getTotalBalance() {
		return totalBalance;
	}

	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	public Long getBlockOperationNumber() {
		return blockOperationNumber;
	}

	public void setBlockOperationNumber(Long blockOperationId) {
		this.blockOperationNumber = blockOperationId;
	}

	public Integer getIndBlockBalance() {
		return indBlockBalance;
	}

	public void setIndBlockBalance(Integer indBlockBalance) {
		this.indBlockBalance = indBlockBalance;
	}

	public Integer getBlockOperationType() {
		return blockOperationType;
	}

	public void setBlockOperationType(Integer blockOperationType) {
		this.blockOperationType = blockOperationType;
	}

	public Integer getIndTotalBalance() {
		return indTotalBalance;
	}

	public void setIndTotalBalance(Integer indTotalBalance) {
		this.indTotalBalance = indTotalBalance;
	}

	public Integer getIndLonableBalance() {
		return indLonableBalance;
	}

	public void setIndLonableBalance(Integer indLonableBalance) {
		this.indLonableBalance = indLonableBalance;
	}

	public Long getLonableOperationId() {
		return lonableOperationId;
	}

	public void setLonableOperationId(Long lonableOperationId) {
		this.lonableOperationId = lonableOperationId;
	}

	public BigDecimal getLoanableBalance() {
		return loanableBalance;
	}

	public void setLoanableBalance(BigDecimal loanableBalance) {
		this.loanableBalance = loanableBalance;
	}

	public Boolean getTodate() {
		return todate;
	}

	public void setTodate(Boolean todate) {
		this.todate = todate;
	}

	public Long getIdBlockOperationPk() {
		return idBlockOperationPk;
	}

	public void setIdBlockOperationPk(Long idBlockOperationPk) {
		this.idBlockOperationPk = idBlockOperationPk;
	}

	public Integer getIndHandleDetails() {
		return indHandleDetails;
	}

	public void setIndHandleDetails(Integer indHandleDetails) {
		this.indHandleDetails = indHandleDetails;
	}

}