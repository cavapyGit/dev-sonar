package com.pradera.core.component.operation.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author USUARIO
 *
 */
public class MarketFactBalanceTO implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String securityCodePk;
	
	private Long holderAccountPk;
	
	private Long participantPk;
	
	private BigDecimal totalBalance;
	
	private BigDecimal availableBalance;
	
	private List<MarketFactDetailTO> marketFacBalances;
	
	private String securityDescription;
	
	private Integer instrumentType;
	
	private String instrumentDescription;
	
	private Date lastMarketDate;
	
	private BigDecimal lastMarketPrice;
	
	private BigDecimal lastMarketRate;
	
	private Long balanceType;
	
	private Long movementBalanceType;
	
	private Integer accreditationOperationState;

	public String getSecurityCodePk() {
		return securityCodePk;
	}

	public void setSecurityCodePk(String securityCodePk) {
		this.securityCodePk = securityCodePk;
	}

	public Long getHolderAccountPk() {
		return holderAccountPk;
	}

	public void setHolderAccountPk(Long holderAccountPk) {
		this.holderAccountPk = holderAccountPk;
	}

	public Long getParticipantPk() {
		return participantPk;
	}

	public void setParticipantPk(Long participantPk) {
		this.participantPk = participantPk;
	}

	public List<MarketFactDetailTO> getMarketFacBalances() {
		return marketFacBalances;
	}

	public void setMarketFacBalances(List<MarketFactDetailTO> marketFacBalances) {
		this.marketFacBalances = marketFacBalances;
	}

	public String getSecurityDescription() {
		return securityDescription;
	}

	public void setSecurityDescription(String securityDescription) {
		this.securityDescription = securityDescription;
	}

	public Integer getInstrumentType() {
		return instrumentType;
	}

	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	public String getInstrumentDescription() {
		return instrumentDescription;
	}

	public void setInstrumentDescription(String instrumentDescription) {
		this.instrumentDescription = instrumentDescription;
	}

	public Date getLastMarketDate() {
		return lastMarketDate;
	}

	public void setLastMarketDate(Date lastMarketDate) {
		this.lastMarketDate = lastMarketDate;
	}

	public BigDecimal getLastMarketPrice() {
		return lastMarketPrice;
	}

	public void setLastMarketPrice(BigDecimal lastMarketPrice) {
		this.lastMarketPrice = lastMarketPrice;
	}

	public BigDecimal getLastMarketRate() {
		return lastMarketRate;
	}

	public void setLastMarketRate(BigDecimal lastMarketRate) {
		this.lastMarketRate = lastMarketRate;
	}
	
	public BigDecimal getTotalBalance() {
		return totalBalance;
	}

	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	public Long getBalanceType() {
		return balanceType;
	}

	public void setBalanceType(Long balanceType) {
		this.balanceType = balanceType;
	}

	public Long getMovementBalanceType() {
		return movementBalanceType;
	}

	public void setMovementBalanceType(Long movementBalanceType) {
		this.movementBalanceType = movementBalanceType;
	}

	public Integer getAccreditationOperationState() {
		return accreditationOperationState;
	}

	public void setAccreditationOperationState(Integer accreditationOperationState) {
		this.accreditationOperationState = accreditationOperationState;
	}
	
	
}