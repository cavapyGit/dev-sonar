package com.pradera.core.component.helperui.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.core.component.accounting.account.to.AccountingParameterTo;
import com.pradera.core.component.accounting.account.to.AccountingProcessTo;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.service.AccountAccountingQueryServiceBean;
import com.pradera.core.component.helperui.service.AccountQueryServiceBean;
import com.pradera.core.component.helperui.service.BillingServiceQueryServiceBean;
import com.pradera.core.component.helperui.service.BlockEntityQueryServiceBean;
import com.pradera.core.component.helperui.service.HolderQueryServiceBean;
import com.pradera.core.component.helperui.service.IssuanceQueryServiceBean;
import com.pradera.core.component.helperui.service.IssuerQueryServiceBean;
import com.pradera.core.component.helperui.service.ParameterAccountingQueryServiceBean;
import com.pradera.core.component.helperui.service.SecuritiesQueryServiceBean;
import com.pradera.core.component.helperui.to.AccountHelperInputTO;
import com.pradera.core.component.helperui.to.AccountSearcherTO;
import com.pradera.core.component.helperui.to.AccountTO;
import com.pradera.core.component.helperui.to.AccountingAccountTo;
import com.pradera.core.component.helperui.to.BillingServiceTo;
import com.pradera.core.component.helperui.to.BlockEntitySearchTO;
import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.core.component.helperui.to.HolderAccountSearchTO;
import com.pradera.core.component.helperui.to.HolderHelperOutputTO;
import com.pradera.core.component.helperui.to.HolderSearchTO;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.core.component.helperui.to.IssuanceSearchTO;
import com.pradera.core.component.helperui.to.IssuerSearchTO;
import com.pradera.core.component.helperui.to.IssuerSearcherTO;
import com.pradera.core.component.helperui.to.IssuerTO;
import com.pradera.core.component.helperui.to.SecuritiesSearcherTO;
import com.pradera.core.component.helperui.to.SecuritiesTO;
import com.pradera.core.component.helperui.to.SecuritySearchTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounting.AccountingParameter;
import com.pradera.model.accounting.AccountingProcess;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountType;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.billing.BillingService;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.ProgramAmortizationCoupon;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class HelperComponentFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/01/2013
 */
@Stateless
public class HelperComponentFacade {
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The securities query service. */
	@EJB
	private SecuritiesQueryServiceBean securitiesQueryService;
	
	/** The partipant query service. */
	@EJB
	private ParticipantServiceBean participantService;
	
	/** The issuer query service. */
	@EJB
	private IssuerQueryServiceBean issuerQueryService;
	
	/** The account query service. */
	@EJB
	private AccountQueryServiceBean accountQueryService;
	
	/** The holder query service bean. */
	@EJB
	private HolderQueryServiceBean holderQueryServiceBean;
	
	/** The billing service query service bean. */
	@EJB
	private BillingServiceQueryServiceBean billingServiceQueryServiceBean;

	/** The general parameters service. */
	@EJB
	private ParameterServiceBean generalParametersService;
	
	/** The issuance query service bean. */
	@EJB
	private IssuanceQueryServiceBean issuanceQueryServiceBean;
	
	/** The block entity query service bean. */
	@EJB
	private BlockEntityQueryServiceBean blockEntityQueryServiceBean;
	
	/** The parameter accounting query service bean. */
	@EJB 
	private ParameterAccountingQueryServiceBean parameterAccountingQueryServiceBean;
	
	/** The account accounting query service bean. */
	@EJB
	private AccountAccountingQueryServiceBean accountAccountingQueryServiceBean;
	
	//CONSTANST FOR HELPER EXTENSIONS
	/** The id holder pk. */
	private static int ID_HOLDER_PK 	= 0;
	
	/** The holder name. */
	private static int HOLDER_NAME 		= 1;
	
	/** The first last name. */
	private static int FIRST_LAST_NAME	= 2;
	
	/** The second last name. */
	private static int SECOND_LAST_NAME	= 3;
	
	/** The full name. */
	private static int FULL_NAME	 	= 4;
	
	/** The document type. */
	private static int DOCUMENT_TYPE	= 5;
	
	/** The document number. */
	private static int DOCUMENT_NUMBER	= 6;
	
	/** The holder type. */
	private static int HOLDER_TYPE		= 7;
	
	/** The holder status. */
	private static int HOLDER_STATUS	= 8;
	
	/** The ACCOUN t_ numbe r_1. */
	private static int ACCOUNT_NUMBER_1	  	  = 0;
	
	/** The ALTRNAT e_ cod e_1. */
	private static int ALTRNATE_CODE_1 	  	  = 1;
	
	/** The NAM e_1. */
	private static int NAME_1			  	  = 2;
	
	/** The FIRS t_ las t_ nam e_1. */
	private static int FIRST_LAST_NAME_1  	  = 3;
	
	/** The SECON d_ las t_ nam e_1. */
	private static int SECOND_LAST_NAME_1 	  = 4;
	
	/** The FUL l_ nam e_1. */
	private static int FULL_NAME_1		  	  = 5;
	
	/** The STAT e_ accoun t_1. */
	private static int STATE_ACCOUNT_1		  = 6;
	
	/** The I d_ holde r_ accoun t_ p k_1. */
	private static int ID_HOLDER_ACCOUNT_PK_1 = 7;
	
	/** The ACCOUN t_ grou p_1. */
	private static int ACCOUNT_GROUP_1		  = 8;
	
	/** The HOLDE r_ typ e_1. */
	private static int HOLDER_TYPE_1		  = 9;
	
	/** The id holder. */
	private static int ID_HOLDER			  = 10;
	
	/** The id participant. */
	private static int ID_PARTICIPANT		  = 11;
	
	/** The mnemonico part. */
	private static int MNEMONICO_PART		  = 12;
	
	private static int DESCRIPTION_PART		  = 13;
	
	private static int ACCOUNT_TYPE		  = 14;
	
	/**
	 * Find security from isin.
	 *
	 * @param securities the securities
	 * @return the securities to
	 * @throws ServiceException the service exception
	 */
	public SecuritiesTO findSecurityFromIsin(SecuritiesSearcherTO securities) throws ServiceException {
		return securitiesQueryService.findSecurityFromIsin(securities);
	}
	
	/**
	 * Find securities from helper.
	 *
	 * @param securities the securities
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	 public List<SecuritiesTO> findSecuritiesFromHelper(SecuritiesSearcherTO securities) throws ServiceException {
		
		return securitiesQueryService.findSecuritiesFromHelper(securities);
	}
	
	/**
	 * Find securities from helper native query.
	 *
	 * @param securities the securities
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<SecuritiesTO> findSecuritiesFromHelperNativeQuery(SecuritiesSearcherTO securities) throws ServiceException {
		List<Object[]> objSecuritiesList = securitiesQueryService.findSecuritiesFromHelperNativeQuery(securities);
		
		List<SecuritiesTO> listSecurities = new ArrayList<SecuritiesTO>();
		
		for(Object[] obj : objSecuritiesList) {
			SecuritiesTO  secTO = new SecuritiesTO();
			if(Validations.validateIsNotNullAndNotEmpty(obj[0])){
				secTO.setSecuritiesCode(obj[0].toString());
			}
			if(Validations.validateIsNotNullAndNotEmpty(obj[1])){
				secTO.setDescription(obj[1].toString());
			}
			if(Validations.validateIsNotNullAndNotEmpty(obj[2])){
				secTO.setSecurityType(obj[2].toString());
			}
			if(Validations.validateIsNotNullAndNotEmpty(obj[3])){
				secTO.setSecurityClass(obj[3].toString());
			}
			if(Validations.validateIsNotNullAndNotEmpty(obj[4])){
				secTO.setIsinCode(obj[4].toString());
			}
			listSecurities.add(secTO);
		}
		return listSecurities;
	}
	
	/**
	 * Find participants for state.
	 *
	 * @param participant the participant
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Participant> getLisParticipantServiceFacade(Participant participant) throws ServiceException {
		return participantService.getLisParticipantServiceBean(participant);
	}
	/**
	 * Find participants for state.
	 *
	 * @param participant the participant
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Participant> getLisParticipantForIssuerServiceFacade(Participant participant) throws ServiceException {
		return participantService.getLisParticipantForIssuerServiceFacade(participant);
	}
	
	/**
	 * Gets the combo participants by map filter.
	 *
	 * @param filter the filter
	 * @return the combo participants by map filter
	 * @throws ServiceException the service exception
	 */
	public List<Participant> getComboParticipantsByMapFilter(Map<String,Object> filter) throws ServiceException {
		return participantService.getComboParticipantsByMapFilter(filter);
	}
	
	
	/**
	 * Find participant native query facade.
	 *
	 * @param filter the filter
	 * @return the list
	 */
	public List<Participant> findParticipantNativeQueryFacade(Participant filter){
		return  participantService.findParticipantsByFilter(filter);
	}
	
	/**
	 * Find participant were change the status to start charging for rates 5 and 6.
	 *
	 * @param filter the filter
	 * @return the list
	 */
	public List<Participant> findParticipantChangeStatusRates(Participant filter){
		return  participantService.findParticipantChangeStatusRates(filter);
	}
	
	/**
	 * update change the status to start charging for rates 5 and 6.
	 *
	 * @param participant
	 */
	public void updateChangeStatusChargingRates(Long participant) {
		participantService.updateChangeStatusChargingRates(participant);
	}
	
	/**
	 * Find participants by code.
	 *
	 * @param idParticipantPk the id participant pk
	 * @return the participant
	 * @throws ServiceException the service exception
	 */
	public Participant findParticipantsByCode(Long idParticipantPk) throws ServiceException {
		Participant filter = new Participant();
		filter.setIdParticipantPk(idParticipantPk);
		return participantService.findParticipantByFiltersServiceBean(filter);
	}
	
	/**
	 * Find issuer by code.
	 *
	 * @param issuerSearcher the issuer searcher
	 * @return the issuer to
	 * @throws ServiceException the service exception
	 */
	public IssuerTO findIssuerByCode(IssuerSearcherTO issuerSearcher) throws ServiceException {
		IssuerTO issuerTO = null;

		Issuer issuer = issuerQueryService.getIssuerByCode(issuerSearcher);
		if(issuer!=null){
			issuerTO = new IssuerTO();
			issuerTO.setMnemonic(issuer.getMnemonic());
			issuerTO.setIssuerCode(issuer.getIdIssuerPk());
			issuerTO.setIssuerDesc(issuer.getBusinessName());
			issuerTO.setIssuerBusinessName(issuer.getBusinessName() );
			issuerTO.setIssuerDocNumber(issuer.getDocumentNumber());
			issuerTO.setIssuerDocType(issuer.getDocumentType());
			issuerTO.setIssuerState(issuer.getStateIssuer());
			issuerTO.setIssuerGeographicLocation(issuer.getGeographicLocation());
			issuerTO.setIssuerSector( issuer.getEconomicSector() );
			issuerTO.setIssuerActivity( issuer.getEconomicActivity() );
		}
		return issuerTO;
	} 
	
	/**
	 * Find issuer by helper.
	 *
	 * @param issuerSearcher the issuer searcher
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<IssuerTO> findIssuerByHelper(IssuerSearcherTO issuerSearcher) throws ServiceException {

		List<Object[]> issuers = issuerQueryService.getIssuerByHelper(issuerSearcher);

		List<IssuerTO> issuersTO = new ArrayList<IssuerTO>();
		for (Object[] issuerObjects : issuers) {
			IssuerTO issuerTO = new IssuerTO();
			issuerTO.setIssuerCode((String)issuerObjects[0]);
			issuerTO.setIssuerDesc((String)issuerObjects[1]);
			issuerTO.setIssuerState((Integer)issuerObjects[2]);
			issuerTO.setIssuerStateDesc(issuerObjects[3] == null ? null: (String)issuerObjects[3]);
			//issuerTO.setIssuerGeographicLocation(issuer.getGeographicLocation());
			issuerTO.setIssuerDocType((Integer)issuerObjects[4]);
			issuerTO.setIssuerDocTypeDesc(issuerObjects[5] == null ? null: (String)issuerObjects[5]);
			issuerTO.setIssuerDocNumber( issuerObjects[6] == null ? null: (String)issuerObjects[6] );
			issuerTO.setMnemonic( issuerObjects[7] == null ? null: (String)issuerObjects[7]);
			issuerTO.setIssuerBusinessName(issuerObjects[8] == null ? null: (String)issuerObjects[8]);
			issuerTO.setIssuerActivity( (Integer)issuerObjects[9] );
			issuerTO.setIssuerActivityDesc(issuerObjects[10] == null ? null: (String)issuerObjects[10]);
			issuerTO.setIssuerSector( (Integer)issuerObjects[11] );
			issuerTO.setIssuerSectorDesc(issuerObjects[12] == null ? null: (String)issuerObjects[12]);
			issuersTO.add(issuerTO);
		}
		return issuersTO;
	}
	
	public List<IssuerTO> findIssuerByHelperMnemonic(IssuerSearcherTO issuerSearcher) throws ServiceException {

		List<Object[]> issuers = issuerQueryService.getIssuerByHelperMnemonic(issuerSearcher);

		List<IssuerTO> issuersTO = new ArrayList<IssuerTO>();
		for (Object[] issuerObjects : issuers) {
			IssuerTO issuerTO = new IssuerTO();
			issuerTO.setIssuerCode((String)issuerObjects[0]);
			issuerTO.setIssuerDesc((String)issuerObjects[1]);
			issuerTO.setIssuerState((Integer)issuerObjects[2]);
			issuerTO.setIssuerStateDesc(issuerObjects[3] == null ? null: (String)issuerObjects[3]);
			//issuerTO.setIssuerGeographicLocation(issuer.getGeographicLocation());
			issuerTO.setIssuerDocType((Integer)issuerObjects[4]);
			issuerTO.setIssuerDocTypeDesc(issuerObjects[5] == null ? null: (String)issuerObjects[5]);
			issuerTO.setIssuerDocNumber( issuerObjects[6] == null ? null: (String)issuerObjects[6] );
			issuerTO.setMnemonic( issuerObjects[7] == null ? null: (String)issuerObjects[7]);
			issuerTO.setIssuerBusinessName(issuerObjects[8] == null ? null: (String)issuerObjects[8]);
			issuerTO.setIssuerActivity( (Integer)issuerObjects[9] );
			issuerTO.setIssuerActivityDesc(issuerObjects[10] == null ? null: (String)issuerObjects[10]);
			issuerTO.setIssuerSector( (Integer)issuerObjects[11] );
			issuerTO.setIssuerSectorDesc(issuerObjects[12] == null ? null: (String)issuerObjects[12]);
			issuersTO.add(issuerTO);
		}
		return issuersTO;
	}
	
	
	
	/**
	 * Find account by number.
	 *
	 * @param accountSearcher the account searcher
	 * @return the account to
	 * @throws ServiceException the service exception
	 */
	public AccountTO findAccountByNumberAndParticipant(AccountSearcherTO accountSearcher) throws ServiceException {
		AccountTO accountTO 		= null;
		HolderAccount holderAccount = null;
		
		holderAccount = accountQueryService.getAccountByNumber(accountSearcher);
		
		if(holderAccount != null){
			accountTO = new AccountTO();
		
			accountTO.setAccountNumber(holderAccount.getAccountNumber());
			accountTO.setAccountStateDesc(holderAccount.getStateAccountDescription());
			accountTO.setAlternativeCode(holderAccount.getAlternateCode());
			accountTO.setAccountDescription(holderAccount.getAccountDescription());
			accountTO.setAccountGroup(holderAccount.getAccountGroup());
			accountTO.setHolderAccount(holderAccount);
		}
		
		return accountTO;
	}
	
	/**
	 * Find holder by code.
	 *
	 * @param idHolderPk the id holder pk
	 * @return the holder
	 * @throws ServiceException the service exception
	 */
	public Holder findHolderByCode(Long idHolderPk) throws ServiceException {
		return holderQueryServiceBean.findHolderByCode(idHolderPk);
	}
	
	/**
	 * Search holder extension.
	 *
	 * @param filter the filter
	 * @return the list
	 */
	@Inject
	UserInfo userInfo;
	public List<HolderHelperOutputTO> searchHolderExtension(HolderTO filter){
		List<HolderHelperOutputTO> helperResultTOlist   = new ArrayList<HolderHelperOutputTO>();
		HolderHelperOutputTO	    holderHelperResultTO;
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getFullName())){
			filter.setDocumentType(DocumentType.RUC.getCode());
		}
		//SETTING ELEMENTS TO HOLDER HELPERRESULT
		for(Object[] obj : holderQueryServiceBean.searchHolderFromHelper(filter)){
			holderHelperResultTO = new HolderHelperOutputTO();
			//IF THE HOLDER DON'T HAVE SOME DATA, THIS HOLDER NO SHOULD SHOW DATA
			if(	Validations.validateIsNotNullAndNotEmpty(ID_HOLDER_PK) 	  &&
				Validations.validateIsNotNullAndNotEmpty(HOLDER_TYPE) 	  &&
				Validations.validateIsNotNullAndNotEmpty(DOCUMENT_NUMBER) &&
				Validations.validateIsNotNullAndNotEmpty(HOLDER_TYPE) 	  &&
				Validations.validateIsNotNullAndNotEmpty(HOLDER_STATUS)){

				holderHelperResultTO.setHolderId(Long.valueOf(obj[ID_HOLDER_PK].toString()));
				
				// IF HOLDER IS NATURAL SETTING IN FULL NAME TO SHOWS IN DATATABLE
				if(Integer.valueOf(obj[HOLDER_TYPE].toString()).equals(PersonType.NATURAL.getCode())){
					if(Validations.validateIsNotNullAndNotEmpty(obj[HOLDER_NAME]) && 
					   Validations.validateIsNotNullAndNotEmpty(obj[FIRST_LAST_NAME]) && 
					   Validations.validateIsNotNullAndNotEmpty(obj[SECOND_LAST_NAME])){
							holderHelperResultTO.setFullName(obj[HOLDER_NAME].toString()+" "+obj[FIRST_LAST_NAME].toString()+" "+obj[SECOND_LAST_NAME].toString());
							holderHelperResultTO.setName(obj[HOLDER_NAME].toString());
							holderHelperResultTO.setFirstLastName(obj[FIRST_LAST_NAME].toString());
							holderHelperResultTO.setSecondLastName(obj[SECOND_LAST_NAME].toString());
					}else if(Validations.validateIsNotNullAndNotEmpty(obj[HOLDER_NAME]) && Validations.validateIsNotNullAndNotEmpty(obj[FIRST_LAST_NAME])){
						holderHelperResultTO.setFullName(obj[HOLDER_NAME].toString()+" "+obj[FIRST_LAST_NAME].toString());
						holderHelperResultTO.setName(obj[HOLDER_NAME].toString());
						holderHelperResultTO.setFirstLastName(obj[FIRST_LAST_NAME].toString());
					}
				}else{
					holderHelperResultTO.setFullName(obj[FULL_NAME].toString());
				}
				holderHelperResultTO.setDocumentType(Integer.valueOf(obj[DOCUMENT_TYPE].toString()));
				
				holderHelperResultTO.setDocumentNumber(obj[DOCUMENT_NUMBER].toString());
				
				holderHelperResultTO.setHolderType(Integer.valueOf(obj[HOLDER_TYPE].toString()));
				
				holderHelperResultTO.setHolderStatus(Integer.valueOf(obj[HOLDER_STATUS].toString()));
				holderHelperResultTO.setHolderStatusDescription(HolderStateType.get(Integer.valueOf(obj[HOLDER_STATUS].toString())).getValue());
				
				holderHelperResultTO.setHolderTypeDocumentDescription(DocumentType.get(Integer.valueOf(obj[DOCUMENT_TYPE].toString())).getValue());
	
				helperResultTOlist.add(holderHelperResultTO);
			}else{
				continue;
			}	
	}
		return helperResultTOlist;
	}
	
	
	/**
	 * Search account by filter.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountHelperResultTO> searchAccountByFilter(HolderTO filter) throws ServiceException{
		List<HolderAccountHelperResultTO> holderHelperResultTOlist = new ArrayList<HolderAccountHelperResultTO>();
		HolderAccountHelperResultTO	 holderAccountResulTO = new HolderAccountHelperResultTO();
		Map<Integer,HolderAccountHelperResultTO> mapHolderDesc = new HashMap<Integer,HolderAccountHelperResultTO>();
		ParameterTableTO parameterTableTO = new ParameterTableTO();
//		String strAlternativeCodeIssuer="";
		StringBuilder strAlternativeCode =new StringBuilder();
		//SEARCH ACCOUNTS
		Map<Integer,String> mapAccountStateDescription = new HashMap<Integer,String>();
		parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
		for(ParameterTable param : generalParametersService.getListParameterTableServiceBean(parameterTableTO)){
			mapAccountStateDescription.put(param.getParameterTablePk(), param.getParameterName());
		}
		List<Object[]> objtList = accountQueryService.searchAccountByFilter(filter);
		
		for(Object[] obj : objtList){
			Integer accountNumber = Integer.valueOf(obj[ACCOUNT_NUMBER_1].toString());
			Integer accountType = HolderAccountType.OWNERSHIP.getCode();
			Long participantpk = Long.valueOf(obj[MNEMONICO_PART].toString());
//			strAlternativeCode.append(obj[ID_HOLDER].toString());
			if((mapHolderDesc.get(accountNumber)!=null)
				&& ((participantpk.toString()).equals(holderAccountResulTO.getParticipant().getIdParticipantPk().toString()))){
					if((accountType.toString()).equals(holderAccountResulTO.getAccountType().toString())){
						HolderAccountHelperResultTO holAccTO = mapHolderDesc.get(accountNumber);
						holAccTO.getHolderDescriptionList().add(obj[ID_HOLDER].toString() + " - " + obj[FULL_NAME_1].toString());
//						strAlternativeCodeIssuer.concat(obj[ID_HOLDER].toString() );
						strAlternativeCode.append(","+obj[ID_HOLDER].toString());
						holderAccountResulTO.setAlternativeCodeIssuer(strAlternativeCode.toString());
				}
			}else{
					holderAccountResulTO = new HolderAccountHelperResultTO();
					Participant part = new Participant();
					strAlternativeCode =new StringBuilder();
					holderAccountResulTO.setStateAccount(Integer.valueOf(obj[STATE_ACCOUNT_1].toString()));
					holderAccountResulTO.setAccountNumber(Integer.valueOf(obj[ACCOUNT_NUMBER_1].toString()));
					holderAccountResulTO.setAlternateCode(obj[ALTRNATE_CODE_1].toString());
					holderAccountResulTO.setAccountGroup(Integer.valueOf(obj[ACCOUNT_GROUP_1].toString()));
					holderAccountResulTO.setAccountPk(Long.valueOf(obj[ID_HOLDER_ACCOUNT_PK_1].toString()));
					holderAccountResulTO.setHolderDescriptionList(new ArrayList<String>());
					holderAccountResulTO.getHolderDescriptionList().add(obj[ID_HOLDER].toString() + " - " + obj[FULL_NAME_1].toString());
					holderAccountResulTO.setAccountType(obj[11].toString());
					part.setIdParticipantPk(Long.valueOf(obj[12].toString()));
					holderAccountResulTO.setRelatedName(obj[13] != null ? obj[13].toString() : null);
//					strAlternativeCodeIssuer.concat(","+obj[ID_HOLDER].toString());
					strAlternativeCode.append(obj[ID_HOLDER].toString());
					holderAccountResulTO.setParticipant(part); 
					if(Validations.validateIsNotNull(obj[ACCOUNT_GROUP_1])){
						holderAccountResulTO.setAccountGroup(Integer.valueOf(obj[ACCOUNT_GROUP_1].toString()));
					}else{
						holderAccountResulTO.setAccountGroup(null);
					}
					if(Validations.validateIsNotNull(obj[STATE_ACCOUNT_1])){
						holderAccountResulTO.setAccountStatusDescription(mapAccountStateDescription.get(Integer.valueOf(obj[STATE_ACCOUNT_1].toString())));
					}else{
						holderAccountResulTO.setAccountStatusDescription(null);
					}
					holderAccountResulTO.setAlternativeCodeIssuer(strAlternativeCode.toString());
					holderHelperResultTOlist.add(holderAccountResulTO);
					mapHolderDesc.put(accountNumber, holderAccountResulTO);
			}
		}
		return holderHelperResultTOlist;
	}
	
	
	/**
	 * Search account extension by filter.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountHelperResultTO> searchAccountExtensionByFilter(AccountHelperInputTO filter) throws ServiceException{
		List<HolderAccountHelperResultTO> holderHelperResultTOlist = new ArrayList<HolderAccountHelperResultTO>();
		HolderAccountHelperResultTO	 holderAccountResulTO;
		//List<Long> 	lngAccountPkList = new ArrayList<Long>();
		List<Object[]> 	objtList ;
		//List<Object[]> 	holderList;
		Map<Integer,String> mapAccountStatus = new HashMap<Integer, String>();
		Map<Integer,String> mapAccountType = new HashMap<Integer, String>();
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		HolderTO holderTOFilter = new HolderTO();
		String strDescriptionHolder = "";
		Integer intHolderType = 0;
		
		holderTOFilter.setPartIdPk(filter.getIdParticipantPK());
		holderTOFilter.setHolderId(filter.getIdHolderPk());
		holderTOFilter.setAccountNumber(filter.getAccountNumber());
		if(filter.getAccountType()!=null){
			holderTOFilter.setAccountType(Integer.valueOf(filter.getAccountType()));
		}
		holderTOFilter.setAccountState(filter.getAccountStatus());
		holderTOFilter.setDocumentType(holderTOFilter.getDocumentType());
		holderTOFilter.setDocumentNumber(holderTOFilter.getDocumentNumber());
		holderTOFilter.setName(holderTOFilter.getName());
		holderTOFilter.setFirstLastName(holderTOFilter.getFirstLastName());
		holderTOFilter.setSecondLastName(holderTOFilter.getSecondLastName());
		holderTOFilter.setFullName(holderTOFilter.getFullName());
		holderTOFilter.setLstHolderAccountId(filter.getLstHolderAccountPk());
		holderTOFilter.setUserIssuer(filter.isUserIssuer());
		holderTOFilter.setIssuerFk(filter.getIssuerFk());
		//SEARCH ACCOUNTS
		objtList = accountQueryService.searchHolderAccountByFilter(holderTOFilter);
	
		parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
		for(ParameterTable paramTable : generalParametersService.getListParameterTableServiceBean(parameterTableTO)){
			mapAccountStatus.put(paramTable.getParameterTablePk(), paramTable.getParameterName());
		}
		
		parameterTableTO.setMasterTableFk(MasterTableType.ACCOUNTS_TYPE.getCode());
		for(ParameterTable paramTable : generalParametersService.getListParameterTableServiceBean(parameterTableTO)){
			mapAccountType.put(paramTable.getParameterTablePk(), paramTable.getParameterName());
		}
		
		List<Long> lstHolderAccountPk = new ArrayList<Long>();
		
		for(Object[] obj : objtList){
			Participant pa= new Participant();
			HolderAccount ha = new HolderAccount();

			holderAccountResulTO = new HolderAccountHelperResultTO();
			holderAccountResulTO.setAccountNumber(Integer.valueOf(obj[ACCOUNT_NUMBER_1].toString()));
			holderAccountResulTO.setAlternateCode(obj[ALTRNATE_CODE_1].toString());
			holderAccountResulTO.setAccountStatusDescription(mapAccountStatus.get(Integer.valueOf(obj[STATE_ACCOUNT_1].toString())));
			holderAccountResulTO.setAccountType(mapAccountType.get(Integer.valueOf(obj[ACCOUNT_TYPE].toString())));
			holderAccountResulTO.setAccountPk(Long.valueOf(obj[ID_HOLDER_ACCOUNT_PK_1].toString()));
			if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountPk)){
				if(lstHolderAccountPk.contains(holderAccountResulTO.getAccountPk())){
					continue;
				}
			}
			lstHolderAccountPk.add(holderAccountResulTO.getAccountPk());
			if(Validations.validateIsNotNull(obj[ACCOUNT_GROUP_1])){
				holderAccountResulTO.setAccountGroup(Integer.valueOf(obj[ACCOUNT_GROUP_1].toString()));
			}else{
				holderAccountResulTO.setAccountGroup(null);
			}
			holderAccountResulTO.setHolderDescriptionList(new ArrayList<String>());
			strDescriptionHolder="";
			intHolderType=((BigDecimal)obj[HOLDER_TYPE_1]).intValue();
			if(intHolderType.equals(PersonType.JURIDIC.getCode())){	//Juridico
				strDescriptionHolder=obj[FULL_NAME_1].toString();
			}else{	//Natural
				if(Validations.validateIsNotNull(obj[NAME_1]))
					strDescriptionHolder+=obj[NAME_1]+" ";
				if(Validations.validateIsNotNull(obj[FIRST_LAST_NAME_1]))
					strDescriptionHolder+=obj[FIRST_LAST_NAME_1]+" ";
				if(Validations.validateIsNotNull(obj[SECOND_LAST_NAME_1]))
					strDescriptionHolder+=obj[SECOND_LAST_NAME_1]+" ";	
			}
			ha.setStateAccount(Integer.valueOf(obj[STATE_ACCOUNT_1].toString()));
			holderAccountResulTO.setHolderAccount(ha);
			pa.setIdParticipantPk(Long.valueOf(obj[ID_PARTICIPANT].toString()));
			if(obj[MNEMONICO_PART]!=null){
				pa.setMnemonic(obj[MNEMONICO_PART].toString());
			}
			if(obj[DESCRIPTION_PART]!=null){
				pa.setDescription(obj[DESCRIPTION_PART].toString());
			}
			holderAccountResulTO.setParticipant(pa);
			holderAccountResulTO.setHolderDescription(strDescriptionHolder);
			holderAccountResulTO.getHolderDescriptionList().add(strDescriptionHolder);
			
			holderHelperResultTOlist.add(holderAccountResulTO);
		}
		return holderHelperResultTOlist;
	}
	
	/**
	 * Search security types by instrument.
	 *
	 * @param filter the filter
	 * @return the list
	 */
	public List<ParameterTable> searchSecurityTypesByInstrument(SecuritiesSearcherTO filter){
		List<ParameterTable> parameterTableList = new ArrayList<ParameterTable>();
		for(Object[] obj : generalParametersService.searchSecurityTypesByInstrument(filter)){
			ParameterTable pt = new ParameterTable();
			pt.setParameterTablePk(Integer.valueOf(obj[0].toString()));
			pt.setParameterName(obj[1].toString());
			parameterTableList.add(pt);
		}
		return parameterTableList;
	}
	
	/**
	 * Search security class by security type.
	 *
	 * @param filter the filter
	 * @return the list
	 */
	public List<ParameterTable> searchSecurityClassBySecurityType(SecuritiesSearcherTO filter){
		List<ParameterTable> parameterTableList = new ArrayList<ParameterTable>();
		for(Object[] obj : issuanceQueryServiceBean.searchSecurityClassBySecurityType(filter)){
			ParameterTable pt = new ParameterTable();
			pt.setParameterTablePk(Integer.valueOf(obj[0].toString()));
			pt.setParameterName(obj[1].toString());
			parameterTableList.add(pt);
		}
		return parameterTableList;
	}

	/**
	 * Find security component service facade.
	 *
	 * @param securityTO the security to
	 * @return the security
	 * @throws ServiceException the service exception
	 */
	public Security findSecurityComponentServiceFacade(SecurityTO securityTO) throws ServiceException{
		return securitiesQueryService.findSecurityComponentServiceBean(securityTO);
	}

	/**
	 * Find block entity by code.
	 *
	 * @param idBlockEntityPk the id block entity pk
	 * @return the block entity
	 */
	public BlockEntity findBlockEntityByCode(Long idBlockEntityPk) {	
		BlockEntity blockEntity = blockEntityQueryServiceBean.find(new Long(idBlockEntityPk), BlockEntity.class);
		if(Validations.validateIsNotNullAndNotEmpty(blockEntity)){
			return blockEntity;
		}else
			return null;		
	}

	
	/**
	 * Search securities by filter.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Security> searchSecuritiesByFilter(SecurityTO filter)throws ServiceException{
		return securitiesQueryService.findSecurities(filter);
		
	}
	
	/**
	 * Gets the security help service facade.
	 *
	 * @param securityCode the security code
	 * @return the security help service facade
	 * @throws ServiceException the service exception
	 */
	public Security getSecurityHelpServiceFacade(String securityCode) throws ServiceException{
		return securitiesQueryService.getSecurityHelpServiceBean(securityCode);
	}
	
	/**
	 * Gets the combo parameter table.
	 *
	 * @param paramTable the param table
	 * @return the combo parameter table
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getComboParameterTable(ParameterTableTO paramTable)  throws ServiceException{
		return generalParametersService.getComboParameterTable(paramTable);
	}

	/**
	 * getBillingService for search by code.
	 *
	 * @param code the code
	 * @return BillingService object
	 */
	public BillingServiceTo getBillingService(String code, Long participant) {
		//modificacion merge 643
		BillingService billingService = null;
		if(Validations.validateIsNotNullAndPositive(participant )&&Validations.validateIsNotNullAndNotEmpty(participant)){	
			billingService = billingServiceQueryServiceBean.getBillingServiceByCode(code, participant);
		} else {
			billingService = billingServiceQueryServiceBean.getBillingServiceByCode(code);
		}	
		//--
		BillingServiceTo billingServiceReturn=new BillingServiceTo();
		if (Validations.validateIsNotNull(billingService)){
			BillingServiceTo billingServiceTo= new BillingServiceTo();
			billingServiceTo.setIdBillingServicePk(billingService.getIdBillingServicePk());
			try {
				List<BillingServiceTo> billingServiceReturnList= billingServiceQueryServiceBean.getListParameterBillingService(billingServiceTo, participant);
				if(billingServiceReturnList.size()>0)
					billingServiceReturn= billingServiceReturnList.get(0);
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		return billingServiceReturn;
	}
	public BillingServiceTo getBillingService(String code) {
		//modificacion merge 643
		BillingService billingService = null;
		billingService = billingServiceQueryServiceBean.getBillingServiceByCode(code);
		//--
		BillingServiceTo billingServiceReturn=new BillingServiceTo();
		if (Validations.validateIsNotNull(billingService)){
			BillingServiceTo billingServiceTo= new BillingServiceTo();
			billingServiceTo.setIdBillingServicePk(billingService.getIdBillingServicePk());
			try {
				List<BillingServiceTo> billingServiceReturnList= billingServiceQueryServiceBean.getListParameterBillingService(billingServiceTo);
				if(billingServiceReturnList.size()>0)
					billingServiceReturn= billingServiceReturnList.get(0);
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		return billingServiceReturn;
	}

	/**
	 * Gets the interest chronogram facade.
	 *
	 * @param securityCode the security code
	 * @return the interest chronogram facade
	 * @throws ServiceException the service exception
	 */
	public List<ProgramInterestCoupon> getInterestChronogramFacade(String securityCode) throws ServiceException{
		return securitiesQueryService.getInterestChronogramBean(securityCode);
	}
	
	/**
	 * Gets the amortizacion chronogram facade.
	 *
	 * @param securityCode the security code
	 * @return the amortizacion chronogram facade
	 * @throws ServiceException the service exception
	 */
	public List<ProgramAmortizationCoupon> getAmortizationChronogramFacade(String securityCode) throws ServiceException{
		return securitiesQueryService.getAmortizationChronogramBean(securityCode);
	}
	
	/**
	 * Gets the list parameter billing service.
	 *
	 * @param filter the filter
	 * @return list of BillingService
	 * @throws ServiceException the service exception
	 */
	
	public List<BillingServiceTo> getListParameterBillingService(BillingServiceTo filter, Long participant) throws ServiceException{
		return billingServiceQueryServiceBean.getListParameterBillingServiceForActivityEconomic(filter, participant);
	}

	
	/**
	 * Find security.
	 *
	 * @param security the security
	 * @return the security
	 */
	public Security findSecurity(Security security) {
		return securitiesQueryService.findSecurity(security);
	}
	
	/**
	 * Find security.
	 *
	 * @param securitySearchTO the security search to
	 * @return the security
	 */
	public Security findSecurity(SecuritySearchTO securitySearchTO){
		return securitiesQueryService.findSecurity(securitySearchTO);
	}

	/**
	 * Find securities.
	 *
	 * @param securitySearchTO the security search to
	 * @return the list
	 */
	public List<Security> findSecurities(SecuritySearchTO securitySearchTO) {
		return securitiesQueryService.findSecurities(securitySearchTO);
	}

	/**
	 * Find holder.
	 *
	 * @param holder the holder
	 * @return the holder
	 */
	public Holder findHolder(Holder holder) {
		return holderQueryServiceBean.findHolder(holder);
	}

	/**
	 * Find holders.
	 *
	 * @param holderSearchTO the holder search to
	 * @return the list
	 */
	public List<Holder> findHolders(HolderSearchTO holderSearchTO) {
		return holderQueryServiceBean.findHolders(holderSearchTO);
	}
	/**
	 * Find holders for issuer.
	 *
	 * @param holderSearchTO the holder search to
	 * @return the list
	 */
	public List<Holder> findHoldersForIssuer(HolderSearchTO holderSearchTO) {
		return holderQueryServiceBean.findHoldersForIssuer(holderSearchTO);
	}
	/**
	 * Find holder account.
	 *
	 * @param holderAccount the holder account
	 * @param holder the holder
	 * @return the holder account
	 */
	public HolderAccount findHolderAccount(Integer accountNumber, Long idParticipant,	Long idHolder) {
		return holderQueryServiceBean.finfHolderAccount(accountNumber, idParticipant, idHolder);
	}
	
	/**
	 * Find participant.
	 *
	 * @param participant the participant
	 * @return the participant
	 */
	public Participant findParticipant(Long participant) {
		return participantService.find(Participant.class, participant);
	}

	/**
	 * Find holder accounts.
	 *
	 * @param holderAccountSearchTO the holder account search to
	 * @return the list
	 */
	public List<HolderAccount> findHolderAccounts(HolderAccountSearchTO holderAccountSearchTO) {
		return holderQueryServiceBean.findHolderAccounts(holderAccountSearchTO);
	}

	/**
	 * Find issuer.
	 *
	 * @param idIssuerPk the id issuer pk
	 * @param mnemonic the mnemonic
	 * @return the issuer
	 */
	public Issuer findIssuer(String idIssuerPk, String mnemonic) {
		return issuerQueryService.findIssuer(idIssuerPk,mnemonic);
	}

	/**
	 * Find issuers.
	 *
	 * @param issuerSearchTO the issuer search to
	 * @return the list
	 */
	public List<Issuer> findIssuers(IssuerSearchTO issuerSearchTO) {
		return issuerQueryService.findIssuers(issuerSearchTO);
	}

	/**
	 * Find block entities.
	 *
	 * @param blockEntitySearchTO the block entity search to
	 * @return the list
	 */
	public List<BlockEntity> findBlockEntities(BlockEntitySearchTO blockEntitySearchTO) {
		return blockEntityQueryServiceBean.findBlockEntities(blockEntitySearchTO);
	}

	/**
	 * Find issuances.
	 *
	 * @param issuanceSearchTO the issuance search to
	 * @return the list
	 */
	public List<Issuance> findIssuances(IssuanceSearchTO issuanceSearchTO) {
		return issuanceQueryServiceBean.findIssuances(issuanceSearchTO);
	}

	/**
	 * Find issuance.
	 *
	 * @param issuance the issuance
	 * @return the issuance
	 */
	public Issuance findIssuance(Issuance issuance) {
		return issuanceQueryServiceBean.findIssuance(issuance);
	}

	/**
	 * Find relation holder participant.
	 *
	 * @param idHolderPk the id holder pk
	 * @param idParticipantPk the id participant pk
	 * @return true, if successful
	 */
	public boolean findRelationHolderParticipant(Long idHolderPk, Long idParticipantPk,boolean isParticipantInv) {
		return holderQueryServiceBean.findRelationHolderParticipant(idHolderPk, idParticipantPk,isParticipantInv);
	}
	/**
	 * Find relation holder issuer participant.
	 *
	 * @param idHolderPk the id holder pk
	 * @param idParticipantPk the id participant pk
	 * @return true, if successful
	 */
	public boolean findRelationHolderIssuerParticipant(Long idHolderPk, String idIssuer,Long idParticipantPk,boolean isParticipantInv) {
		return holderQueryServiceBean.findRelationHolderIssuerParticipant(idHolderPk,idIssuer, idParticipantPk,isParticipantInv);
	}
/**
 * Find parameter accounting service bean.
 *
 * @param accountingParameterTo the accounting parameter to
 * @return the list
 */
public List<AccountingParameterTo>  findParameterAccountingServiceBean(AccountingParameterTo accountingParameterTo){
		
		List<AccountingParameterTo> listAccountingParameterTo;
		AccountingParameterTo acParameterTo=null;
		List<Object[]> lstResult =parameterAccountingQueryServiceBean.findParameterAccountingServiceBean(accountingParameterTo);
		listAccountingParameterTo=new ArrayList<AccountingParameterTo>();
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
			
			for(Object[] objResult:lstResult){
				if(Validations.validateIsNotNullAndNotEmpty(objResult[0])&&
						Validations.validateIsNotNullAndNotEmpty(objResult[1])&&
						Validations.validateIsNotNullAndNotEmpty(objResult[2])&&
						Validations.validateIsNotNullAndNotEmpty(objResult[3])&&
						Validations.validateIsNotNullAndNotEmpty(objResult[4])&&
						Validations.validateIsNotNullAndNotEmpty(objResult[5])&&
						Validations.validateIsNotNullAndNotEmpty(objResult[6])&&
						Validations.validateIsNotNullAndNotEmpty(objResult[7])){
					
					acParameterTo=new AccountingParameterTo();
					acParameterTo.setIdAccountingParameterPk(Integer.parseInt(objResult[0].toString()));

					acParameterTo.setParameterType(Integer.parseInt(objResult[1].toString()));
					acParameterTo.setDescriptionParameterType(objResult[2].toString());
					acParameterTo.setNrCorrelative(Integer.parseInt(objResult[3].toString()));
					acParameterTo.setParameterName(objResult[4].toString());
					acParameterTo.setDescription(objResult[5].toString());

					acParameterTo.setStatus(Integer.parseInt(objResult[6].toString()));
					acParameterTo.setDescriptionParamterStatus(objResult[7].toString());
					listAccountingParameterTo.add(acParameterTo);
				}
			}

		}
		return listAccountingParameterTo;
	}
	
	
	/**
	 * Find accounting paramete by code.
	 *
	 * @param idAccountingParameterPk the id accounting parameter pk
	 * @return the accounting parameter to
	 */
	public AccountingParameterTo findAccountingParameteByCode(Long idAccountingParameterPk){
		AccountingParameter accountingParameter=null;
		AccountingParameterTo acParameterTo=null;
		accountingParameter= parameterAccountingQueryServiceBean.findAccountingParameteByCode(idAccountingParameterPk);
		if(Validations.validateIsNotNull(accountingParameter)){
			
			acParameterTo=new AccountingParameterTo();
			acParameterTo.setIdAccountingParameterPk(accountingParameter.getIdAccountingParameterPk());

			acParameterTo.setParameterType(accountingParameter.getParameterType());
			acParameterTo.setDescriptionParameterType(accountingParameter.getDescriptionParameterType());
			acParameterTo.setNrCorrelative(accountingParameter.getNrCorrelative());
			acParameterTo.setParameterName(accountingParameter.getParameterName());
			acParameterTo.setDescription(accountingParameter.getDescription());

			acParameterTo.setStatus(accountingParameter.getStatus());
			acParameterTo.setDescriptionParamterStatus(accountingParameter.getDescriptionParameterStatus());
		}
		
		return acParameterTo;
	}
	
	// Use in Accouting //
		/**
	 * Search parameter accounting.
	 *
	 * @param accountingAccountTO the accounting account to
	 * @return the list
	 */
	public List<AccountingAccountTo> searchParameterAccounting(AccountingAccountTo accountingAccountTO){
			
			List<AccountingAccountTo> listAccountingAccountTO = new ArrayList<AccountingAccountTo>();
			try {
				listAccountingAccountTO = accountAccountingQueryServiceBean.findAccountingAccountServiceBean(accountingAccountTO);
			} catch (ServiceException e) {
				e.printStackTrace();
			} 
			return listAccountingAccountTO;
		}	
		
		
		/* Use in Accouting */
	 	/**
		 * Find accounting accountr by code.
		 *
		 * @param accountCode the account code
		 * @param auxiliaryTypeInd the auxiliary type ind
		 * @return the accounting account to
		 * @throws ServiceException the service exception
		 */
		public AccountingAccountTo findAccountingAccountrByCode(String accountCode, Integer auxiliaryTypeInd) throws ServiceException {
			return accountAccountingQueryServiceBean.findAccountingAccountByCode(accountCode, auxiliaryTypeInd);
		}
		
	 	
	 	//Search Accouting Parameter
		/**
	 	 * Search parameter accounting.
	 	 *
	 	 * @param parameter the parameter
	 	 * @return the list
	 	 */
	 	public List<AccountingParameter> searchParameterAccounting(AccountingParameter parameter){
			 
			List<AccountingParameter> listAccountingParameters = new ArrayList<AccountingParameter>();
			listAccountingParameters = accountAccountingQueryServiceBean.searchParameterAccounting(parameter);  
			return listAccountingParameters;
		}	 
	 	
	 	public List<AccountingProcess> findAccountingProcessWithAccuntingReceipt(AccountingProcessTo accountingProcessTo ){

	 		return accountAccountingQueryServiceBean.findAccountingProcessWithAccuntingReceipt(accountingProcessTo);
	 	}
	 	
	 	public List<Object[]> findFileContent(String securityCode, Integer cuponNumber, Integer indCupon) {
	 		
	 		return accountAccountingQueryServiceBean.findFileContentWithNativeQuery(securityCode, cuponNumber, indCupon);
	 	}
	 	
	 	public Boolean securityHasFile(String securityCode, Integer cuponNumber, Integer indCupon) {	 		
	 		return accountAccountingQueryServiceBean.securityHasFile(securityCode, cuponNumber, indCupon);
	 	}
}
