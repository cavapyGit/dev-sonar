package com.pradera.core.component.helperui.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.el.ELContext;
import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.HelperBean;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.IssuanceSearchTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.IssuanceStateType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityType;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class IssuanceHelperBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 12-jun-2015
 */
@HelperBean
public class IssuanceHelperBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The general parameters facade. */
	@Inject
	GeneralParametersFacade generalParametersFacade;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	/** The max results query helper. */
	@Inject @Configurable
	Integer maxResultsQueryHelper;
	
	/** The issuance search to. */
	private IssuanceSearchTO issuanceSearchTO;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The flag issuerdpf. */
	private boolean flagIssuerdpf;
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		issuanceSearchTO = new IssuanceSearchTO();
	}
	
	/**
	 * Load help.
	 */
	public void loadHelp(){
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			ELContext elContext = context.getELContext();
			ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.issuer}", Issuer.class);
			Issuer issuer = (Issuer) valueExpression.getValue(elContext);
			valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.blIssuer}", Boolean.class);
			Boolean blIssuer = (Boolean) valueExpression.getValue(elContext);
			if((Validations.validateIsNullOrEmpty(issuer) || Validations.validateIsNullOrEmpty(issuer.getIdIssuerPk())) && blIssuer){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getGenericMessage("issuance.helper.alert.issuer"));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}	
			issuanceSearchTO = new IssuanceSearchTO();
			issuanceSearchTO.setIssuer(issuer);
			fillCombos();
			issuanceSearchTO.setState(IssuanceStateType.AUTHORIZED.getCode());
			issuanceSearchTO.setInstrumentType(InstrumentType.FIXED_INCOME.getCode());
			changeInstrumentType();
			flagIssuerdpf = false;
			if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
				flagIssuerdpf = true;
				issuanceSearchTO.setSecurityType(SecurityType.PUBLIC.getCode());
				changeSecurityType();
				List<ParameterTable> lstSecuritieClass = new ArrayList<ParameterTable>();
				for(ParameterTable objParameterTable : issuanceSearchTO.getLstSecurityClass()){
					if(objParameterTable.getParameterTablePk().equals(SecurityClassType.DPA.getCode()) || 
							objParameterTable.getParameterTablePk().equals(SecurityClassType.DPF.getCode())){
						lstSecuritieClass.add(objParameterTable);
					}
				}
				issuanceSearchTO.setLstSecurityClass(lstSecuritieClass);
			}
			RequestContext.getCurrentInstance().execute("PF('dlgIssuance').show();");
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Change instrument type.
	 */
	public void changeInstrumentType(){
		try {
			issuanceSearchTO.setSecurityClass(null);
			issuanceSearchTO.setSecurityType(null);
			issuanceSearchTO.setLstSecurityClass(null);
			if(Validations.validateIsNotNullAndNotEmpty(issuanceSearchTO.getInstrumentType())){
				ParameterTableTO paramTabTO = new ParameterTableTO();
				paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
				paramTabTO.setMasterTableFk(MasterTableType.SECURITIES_TYPE.getCode());
				//paramTabTO.setIdRelatedParameterFk(issuanceSearchTO.getInstrumentType());
				issuanceSearchTO.setLstSecurityType(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Change security type.
	 */
	public void changeSecurityType(){
		List<ParameterTable> lstSecurityClass=new ArrayList<>();
		try {
			lstSecurityClass=generalParametersFacade.getListSecuritiesClassSetup(issuanceSearchTO.getInstrumentType(), 
							issuanceSearchTO.getIssuer().getEconomicSector(),
							issuanceSearchTO.getSecurityType());
			issuanceSearchTO.setLstSecurityClass(lstSecurityClass);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Search issuances.
	 */
	public void searchIssuances(){
		try {
			if(Validations.validateIsNullOrEmpty(issuanceSearchTO.getState())){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getGenericMessage("issuance.helper.alert.state"));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			if(Validations.validateIsNullOrEmpty(issuanceSearchTO.getInstrumentType())){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getGenericMessage("issuance.helper.alert.instrumenttype"));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			issuanceSearchTO.setLstIssuance(null);
			issuanceSearchTO.setBlNoResult(true);
			List<Issuance> lstResult = helperComponentFacade.findIssuances(issuanceSearchTO);
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				issuanceSearchTO.setLstIssuance(new GenericDataModel<Issuance>(lstResult));
				issuanceSearchTO.setBlNoResult(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Find issuance.
	 */
	public void findIssuance(){
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.issuer}", Issuer.class);
		Issuer issuer = (Issuer) valueExpression.getValue(elContext);
		valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.blIssuer}", Boolean.class);
		Boolean blIssuer = (Boolean) valueExpression.getValue(elContext);
		if((Validations.validateIsNullOrEmpty(issuer) || Validations.validateIsNullOrEmpty(issuer.getIdIssuerPk()))
				&& blIssuer){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getGenericMessage("issuance.helper.alert.issuer"));
			JSFUtilities.showSimpleValidationDialog();
			valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.issuance}", Issuance.class);
			valueExpression.setValue(elContext, new Issuance());
			return;
		}
		valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.issuance}", Issuance.class);
		Issuance issuance = (Issuance) valueExpression.getValue(elContext);
		issuance.setIssuer(issuer);
		if(Validations.validateIsNotNullAndNotEmpty(issuance.getIdIssuanceCodePk())){
			Issuance issuanceTemp = helperComponentFacade.findIssuance(issuance);
			if(Validations.validateIsNotNullAndNotEmpty(issuanceTemp)){
					valueExpression.setValue(elContext, issuanceTemp);
			}else{
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getGenericMessage("issuance.helper.alert.notexist", new Object[]{issuance.getIdIssuanceCodePk()}));
				JSFUtilities.showSimpleValidationDialog();
				valueExpression.setValue(elContext, new Issuance());
			}
		}else{
			valueExpression.setValue(elContext, new Issuance());
		}		
	}
	
	/**
	 * Clean result.
	 */
	public void cleanResult(){
		issuanceSearchTO.setLstIssuance(null);
		issuanceSearchTO.setBlNoResult(false);
	}
	
	/**
	 * Clean search.
	 */
	public void cleanSearch(){
		JSFUtilities.resetViewRoot();
		cleanResult();
		issuanceSearchTO.setState(null);
		if(!userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
			issuanceSearchTO.setInstrumentType(null);
			issuanceSearchTO.setLstSecurityClass(null);
			issuanceSearchTO.setLstSecurityType(null);
			issuanceSearchTO.setSecurityType(null);
		}		
		issuanceSearchTO.setSecurityClass(null);		
		issuanceSearchTO.setDescription(null);
	}
	
	/**
	 * Gets the issuance.
	 *
	 * @param issuance the issuance
	 * @return the issuance
	 */
	public void getIssuance(Issuance issuance){
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.issuance}", Issuance.class);	
		valueExpression.setValue(elContext, helperComponentFacade.findIssuance(issuance));
	}
	
	/**
	 * Fill combos.
	 *
	 * @throws ServiceException the service exception
	 */
	private void fillCombos() throws ServiceException{
		ParameterTableTO paramTabTO = new ParameterTableTO();
		paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
		paramTabTO.setMasterTableFk(MasterTableType.ISSUANCE_STATE.getCode());
		issuanceSearchTO.setLstState(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
		paramTabTO.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
		issuanceSearchTO.setLstInstrumentType(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
	}
	
	/**
	 * Gets the issuance search to.
	 *
	 * @return the issuance search to
	 */
	public IssuanceSearchTO getIssuanceSearchTO() {
		return issuanceSearchTO;
	}
	
	/**
	 * Sets the issuance search to.
	 *
	 * @param issuanceSearchTO the new issuance search to
	 */
	public void setIssuanceSearchTO(IssuanceSearchTO issuanceSearchTO) {
		this.issuanceSearchTO = issuanceSearchTO;
	}

	/**
	 * Gets the max results query helper.
	 *
	 * @return the max results query helper
	 */
	public Integer getMaxResultsQueryHelper() {
		return maxResultsQueryHelper;
	}

	/**
	 * Checks if is flag issuerdpf.
	 *
	 * @return true, if is flag issuerdpf
	 */
	public boolean isFlagIssuerdpf() {
		return flagIssuerdpf;
	}

	/**
	 * Sets the flag issuerdpf.
	 *
	 * @param flagIssuerdpf the new flag issuerdpf
	 */
	public void setFlagIssuerdpf(boolean flagIssuerdpf) {
		this.flagIssuerdpf = flagIssuerdpf;
	}	
	
}
