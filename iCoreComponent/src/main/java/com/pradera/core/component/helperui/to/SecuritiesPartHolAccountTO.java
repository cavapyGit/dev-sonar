package com.pradera.core.component.helperui.to;

import java.io.Serializable;

import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;

/*
 * 	CREATED BY MARTIN ZARATE RAFAEL
 */

public class SecuritiesPartHolAccountTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8617031433697988835L;
	private Long idParticipantPk;
	private HolderAccount holderAccount;
	private Security security;
	private Issuer issuer;
	
	
	//GETTERS AND SETTERS
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}
	public Security getSecurity() {
		return security;
	}
	public void setSecurity(Security security) {
		this.security = security;
	}
	public Issuer getIssuer() {
		return issuer;
	}
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}
}
