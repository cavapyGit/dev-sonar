package com.pradera.core.component.file.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.log4j.Logger;

import com.pradera.commons.configuration.StageDependent;


// TODO: Auto-generated Javadoc
/**
 * The Class FileUploadServiceBean.
 */
@Stateless
public class FileUploadServiceBean {
	
	private static final Logger log= Logger.getLogger(FileUploadServiceBean.class);
	
	
	@Inject
	@StageDependent
	private String fileRootPath;
	
//	private SimpleDateFormat dateFormat;
	
	@PostConstruct
	public void init(){
//		dateFormat = new SimpleDateFormat("yyyyMMddHHmm");
	}
	
	public Object[] processFileUploadComponent(String fileDest,String fileName, InputStream in, boolean appendTimestamp) throws IOException{
		Object[] objResponse = new Object[2];

		//Crear directorio
		String compositePath = fileRootPath + File.separator + fileDest + File.separator;
		File fileDirectory = new File(compositePath);
		if(!fileDirectory.exists()) {
			fileDirectory.mkdirs();
		}
		
		String compositeFilePath = fileRootPath + File.separator + fileDest + File.separator + fileName;
		File fileOut = new File(compositeFilePath);
		
		if(!fileOut.exists()) {
			OutputStream out = new FileOutputStream(new File(compositeFilePath));
			try {
				int read = 0;
				byte[] bytes = new byte[1024];
				while ((read = in.read(bytes)) !=-1) {
					out.write(bytes,0,read);
				}
				in.close();
				out.flush();
				out.close();
			} catch (Exception ex) {
	            log.info(ex.getMessage());
			}finally {
				out.close();
			}
		}
		
		return objResponse;
	}
	
	
}
