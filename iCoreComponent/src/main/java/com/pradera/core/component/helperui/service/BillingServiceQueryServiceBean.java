package com.pradera.core.component.helperui.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.el.ELContext;
import javax.el.ValueExpression;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.helperui.HelperBean;
import com.pradera.core.component.helperui.to.BillingServiceTo;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounting.AccountingAccount;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.EconomicActivityType;
import com.pradera.model.billing.BillingService;
import com.pradera.model.negotiation.type.ParticipantAssignmentStateType;
import com.pradera.model.report.Report;

@Stateless
public class BillingServiceQueryServiceBean extends CrudDaoServiceBean {
	
	@Inject
	private PraderaLogger log;
	
	@Inject
	@Configurable
	private Integer maxResultsQuery;
	
	@Inject
	UserInfo userInfo;
	
	public BillingServiceQueryServiceBean(){
		
	}
	/**
	 * get the Billingservice by code, participant
	 * */	
	public BillingService getBillingServiceByCode(String code, Long participant){
		BillingService billingService;
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select bs.* from BILLING_SERVICE bs  ");
		sbQuery.append(" where 1 = 1");

		
		//TODO ISSUE 789: CASE NETAMENTE PARA BANCOS========> BEGIN CODE=========================
		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		Map<String, Object> sessionMap = externalContext.getSessionMap();
		Long idReportPk = 0L;
		BigDecimal economicActivity=BigDecimal.ZERO;
		try {
			idReportPk= (long) sessionMap.get(GeneralConstants.REPORT_BY_PROCESS); 
			economicActivity= getEconomicActivity(participant);
		} catch (Exception e) {
			e.printStackTrace();
		}
		boolean correctServiceCode=false;
		if (idReportPk != 0L) {
			if (economicActivity.intValue() == EconomicActivityType.BANCOS.getCode() || participant == 114L) {
				if (idReportPk == 158L || idReportPk == 201L || idReportPk == 314L) {

					// verificando si se trata del banco BCB ==> id: 114
					if (participant == 114L) {
						switch (code) {
						case "1":
						case "2":
							correctServiceCode = true;
							break;
						default:
							return null;
						}
					} else {
						switch (code) {
						case "1":
						case "2":
						case "5":
						case "6":
							correctServiceCode = true;
							break;
						default:
							return null;
						}
					}
				}

				if (idReportPk == 152L) {
					switch (code) {
					case "13":
					case "14":
						correctServiceCode = true;
						break;
					default:
						return null;
					}
				}

				if (idReportPk == 153L) {
					switch (code) {
					case "15":
					case "16":
					case "49":
						correctServiceCode = true;
						break;
					default:
						return null;
					}
				}

				if (idReportPk == 151L) {
					switch (code) {
					case "17":
					case "18":
						correctServiceCode = true;
						break;
					default:
						return null;
					}
				}

				if (idReportPk == 154L) {
					switch (code) {
					case "19":
						correctServiceCode = true;
						break;
					default:
						return null;
					}
				}

				if (idReportPk == 156L) {
					switch (code) {
					case "24":
						correctServiceCode = true;
						break;
					default:
						return null;
					}
				}

				if (idReportPk == 155L) {
					switch (code) {
					case "26":
					case "27":
						correctServiceCode = true;
						break;
					default:
						return null;
					}
				}
				
				//reporte para la tarifa 11 comision por cambio de titularidad
				if (idReportPk == 298L) {
					switch (code) {
					case "20":
					case "21":
						correctServiceCode = true;
						break;
					default:
						return null;
					}
				}
				
				if (correctServiceCode) {
					if (Validations.validateIsNotNullAndNotEmpty(code)) {
						sbQuery.append(" and bs.SERVICE_CODE IN (:code)");
					}
					Query queryBank = em.createNativeQuery(sbQuery.toString(), BillingService.class);
					if (Validations.validateIsNotNullAndNotEmpty(code)) {
						queryBank.setParameter("code", code);
					}
					try {
						billingService = (BillingService) queryBank.getSingleResult();
						return billingService;
					} catch (NoResultException nrex) {
						log.info("Billing Service not found");
						return billingService = null;
					}
				} else {
					// si el numero de servicio no es coherente retornar null
					return null;
				}
			}
		} else {
			// se retorna nulo por que no se encontro reporte
			return null;
		}
		//ISSUE 789: CASE NETAMENTE PARA BANCOS========> END CODE=========================
		
		
		if(Validations.validateIsNotNullAndPositive(participant )&&Validations.validateIsNotNullAndNotEmpty(participant)){
			sbQuery.append(" and bs.ID_BILLING_SERVICE_PK not in (select a.ID_BILLING_SERVICE_PK");
			sbQuery.append("        from BILLING_SERVICE a");
			sbQuery.append("           join SERVICE_RATE b on a.ID_BILLING_SERVICE_PK = b.ID_BILLING_SERVICE_FK");
			sbQuery.append("           join RATE_INCLUSION c on b.ID_SERVICE_RATE_PK = c.ID_SERVICE_RATE_FK"); 
			sbQuery.append("           join RATE_EXCLUSION d on b.ID_SERVICE_RATE_PK = d.ID_SERVICE_RATE_FK");
			sbQuery.append("        where a.SERVICE_CODE in (:code)");
			sbQuery.append("          and c.INCLUSION_STATE = 1679");
			sbQuery.append("          and d.EXCLUSION_STATE = 1679");
			sbQuery.append("          and :participant not in (114,133))");
		}		
		if(Validations.validateIsNotNullAndNotEmpty(code)){
			sbQuery.append(" and bs.SERVICE_CODE = :code");
		}		
		Query query =  em.createNativeQuery(sbQuery.toString(), BillingService.class); 
		if(Validations.validateIsNotNullAndNotEmpty(code)){
			query.setParameter("code", code);			
		}		
		if(Validations.validateIsNotNullAndPositive(participant )&&Validations.validateIsNotNullAndNotEmpty(participant)){
    		query.setParameter("participant", participant);
		}	
		try{
			billingService = (BillingService) query.getSingleResult();
		} catch(NoResultException  nrex){
			log.info("Billing Service not found");
			billingService=null;
		}		
		return billingService;
	}
	
	/**
	 * get the Billingservice by code and state
	 * */	
	public BillingService getBillingServiceByCode(String code){
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select bs From BillingService bs  ");
		sbQuery.append(" Where 1 = 1");
		
		if(Validations.validateIsNotNullAndNotEmpty(code)){
			sbQuery.append(" And bs.serviceCode = :code");
		}
		
		Query query = em.createQuery(sbQuery.toString());   
		
		if(Validations.validateIsNotNullAndNotEmpty(code)){
			query.setParameter("code", code);			
		}
		
		BillingService billingService=new BillingService(); 
		try{
			Object entity=query.getSingleResult();
			billingService=(BillingService)entity;
		} catch(NoResultException  nrex){
			log.info("Billing Service not found");
			billingService=null;
		}
		
		return billingService;
	}
	
	/**
	 * get the Billingservice by code and state
	 * */	
	public ArrayList<Object[]> getRateParametersByServiceCode(String code){
		StringBuilder sbQuery = new StringBuilder();
		List<Object[]> result = new ArrayList<Object[]>();
		sbQuery.append("select DISTINCT  ");
		sbQuery.append("  sr.RATE_AMOUNT  ");
		sbQuery.append("  ,(sr.RATE_AMOUNT * 0.13) / 0.87 as IMPUESTO  ");
		sbQuery.append("  ,(sr.RATE_AMOUNT * 0.13) / 0.87 + sr.RATE_AMOUNT as TOTAL  ");
		sbQuery.append("  ,cr.COLLECTION_AMOUNT_BILLED  ");
		sbQuery.append("from PROCESSED_SERVICE ps  ");
		sbQuery.append("  join BILLING_SERVICE bs on ps.ID_BILLING_SERVICE_FK = bs.ID_BILLING_SERVICE_PK  ");
		sbQuery.append("  join SERVICE_RATE sr on bs.ID_BILLING_SERVICE_PK = sr.ID_BILLING_SERVICE_FK  ");
		sbQuery.append("  join COLLECTION_RECORD cr on ps.ID_PROCESSED_SERVICE_PK = cr.ID_PROCESSED_SERVICE_FK  ");
		sbQuery.append("where SERVICE_CODE = " + code);
				
		Query query = em.createNativeQuery(sbQuery.toString());
		try{
			result = query.getResultList();
		} catch (NoResultException  nrex){
			log.info("No data for services");
			result = null;
		}
		return (ArrayList<Object[]>) result;
		
	}
	/**
	 * 
	 * Method for get BillingService for CollectionManual
	 * 
	 * */
	
	private BigDecimal getEconomicActivity(int idUserAccountPk){
		String sbQuery ="select ECONOMIC_ACTIVITY from PARTICIPANT where id_participant_pk=(select ID_PARTICIPANT_FK from security.user_account where ID_USER_PK = "+idUserAccountPk+")";
		Query query = em.createNativeQuery(sbQuery);
		BigDecimal economicActivity=BigDecimal.ZERO;
		try {
			economicActivity=(BigDecimal) query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return economicActivity;
	}
	
	/**
	 * 
	 * Metodo que retorna las actividades economicas de un determinado participante
	 * 
	 * */
	private BigDecimal getEconomicActivity(Long participant){
		String sbQuery ="select  ECONOMIC_ACTIVITY from PARTICIPANT where ID_PARTICIPANT_PK = "+participant;
		Query query = em.createNativeQuery(sbQuery);
		BigDecimal economicActivity= BigDecimal.ZERO;
		try {
			economicActivity= (BigDecimal) query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return economicActivity;
	}
	public List<BillingServiceTo> getListParameterBillingServiceForActivityEconomic(BillingServiceTo filter, Long participant) throws ServiceException	{
		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		Map<String, Object> sessionMap = externalContext.getSessionMap();
		Long idReportPk = 0L;
		BigDecimal economicActivity=BigDecimal.ZERO;
		try {
			idReportPk= (long) sessionMap.get(GeneralConstants.REPORT_BY_PROCESS); 
			economicActivity= getEconomicActivity(participant);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append(" SELECT DISTINCT                                                                                                                           ");
		sbQuery.append(" 	 bs.id_billing_service_pk                                                                                                              ");
		sbQuery.append(" 	,bs.SERVICE_NAME                                                                                                                       ");
		sbQuery.append(" 	,bs.SERVICE_CODE                                                                                                                       ");
		sbQuery.append(" 	,BS.CLIENT_SERVICE_TYPE                                                                                                                ");
		sbQuery.append(" 	,BS.SERVICE_TYPE                                                                                                                       ");
		sbQuery.append(" 	,BS.STATE_SERVICE                                                                                                                      ");
		sbQuery.append(" 	,BS.ENTITY_COLLECTION                                                                                                                  ");
		sbQuery.append(" 	,BS.BASE_COLLECTION                                                                                                                    ");
		sbQuery.append(" 	,BS.COLLECTION_PERIOD                                                                                                                  ");
		sbQuery.append(" 	,(select pt.description from parameter_table pt where bs.SERVICE_TYPE=pt.parameter_table_pk ) as desription_service_type               ");
		sbQuery.append(" 	,(select pt.description from parameter_table pt where bs.STATE_SERVICE=pt.parameter_table_pk ) as descripion_service_state             ");
		sbQuery.append(" 	,(select pt.description from parameter_table pt where bs.ENTITY_COLLECTION=pt.parameter_table_pk) as description_entity_collection     ");
		sbQuery.append(" 	,(select pt.description from parameter_table pt where bs.BASE_COLLECTION=pt.parameter_table_pk) as description_base_collection         ");
		sbQuery.append(" 	,(select pt.description from parameter_table pt where bs.COLLECTION_PERIOD=parameter_table_pk) as description_collection_period        ");
		sbQuery.append(" 	,(select pt.description from parameter_table pt where bs.CLIENT_SERVICE_TYPE=pt.parameter_table_pk ) as desription_client_service_type ");
		sbQuery.append(" 	,BS.TAX_APPLIED                                                                                                                        ");
		sbQuery.append(" 	,BS.BUSINESS_LINE                                                                                                                      ");
		sbQuery.append(" 	,BS.FINANTIAL_CODE                                                                                                                     ");
		sbQuery.append(" 	,BS.COMMENTS                                                                                                                           ");
		sbQuery.append(" 	,BS.INITIAL_EFFECTIVE_DATE                                                                                                             ");
		sbQuery.append(" 	,BS.END_EFFECTIVE_DATE                                                                                                                 ");
		sbQuery.append(" 	,BS.CALCULATION_PERIOD                                                                                                                 ");
		sbQuery.append(" 	,BS.COLLECTION_TYPE                                                                                                                    ");
		sbQuery.append(" 	,BS.REGISTRY_DATE                                                                                                                      ");
		sbQuery.append(" 	,BS.IND_INTEGRATED_BILL                                                                                                                ");
		sbQuery.append(" 	,BS.CURRENCY_BILLING                                                                                                                   ");
		sbQuery.append(" 	,BS.CURRENCY_CALCULATION                                                                                                               ");
		sbQuery.append(" 	,BS.SOURCE_INFORMATION                                                                                                                 ");
		sbQuery.append("    ,BS.REFERENCE_RATE                                                                                                                ");
		sbQuery.append(" FROM billing_service bs                                                                                                                   ");
		//sbQuery.append(" INNER JOIN billing_economic_activity BEA ON bea.id_billing_service_fk = bs.id_billing_service_pk                                          ");
		sbQuery.append(" WHERE 1 = 1                                                                                                                               ");
		sbQuery.append(" AND (:id_participant_pk = 133) "); //si es  muestre todo
		//sbQuery.append(" AND (:id_participant_pk = 133 OR bea.economic_activity = :economic_activity) 
		 
		if(Validations.validateIsNotNullAndNotEmpty(filter.getServiceName())){
			sbQuery.append(" AND bs.SERVICE_NAME LIKE :serviceNamePrm ");
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getStateService()) && Validations.validateIsNotNullAndNotEmpty(filter.getStateService())){
			sbQuery.append(" AND bs.STATE_SERVICE= :serviceStatePrm");
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getServiceType())&&Validations.validateIsNotNullAndNotEmpty(filter.getServiceType())){
			sbQuery.append(" AND bs.SERVICE_TYPE= :serviceTypePrm");
		} 
		if(Validations.validateIsNotNullAndNotEmpty(filter.getServiceCode())){
			sbQuery.append(" AND bs.SERVICE_CODE= :serviceCodePrm");
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getEntityCollection() )&&Validations.validateIsNotNullAndNotEmpty(filter.getEntityCollection())){
			sbQuery.append(" AND bs.ENTITY_COLLECTION= :entityCollectionPrm");
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getBaseCollection() )&&Validations.validateIsNotNullAndNotEmpty(filter.getBaseCollection())){
			sbQuery.append(" AND bs.BASE_COLLECTION= :baseCollectionPrm");
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getClientServiceType() )&&Validations.validateIsNotNullAndNotEmpty(filter.getClientServiceType())){
			sbQuery.append(" AND bs.CLIENT_SERVICE_TYPE= :clientServiceTypePrm");
		}
		if(Validations.validateIsNotNullAndPositive(filter.getCollectionPeriod() )&&Validations.validateIsNotNullAndNotEmpty(filter.getCollectionPeriod())){
			sbQuery.append(" and bs.COLLECTION_PERIOD= :collectionPeriodPrm");
		}
		if(Validations.validateIsNotNullAndPositive(filter.getIdBillingServicePk() )&&Validations.validateIsNotNullAndNotEmpty(filter.getIdBillingServicePk())){
			sbQuery.append(" and bs.ID_BILLING_SERVICE_PK= :billingServicePKPrm");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getReferenceRate())){
			sbQuery.append(" and bs.REFERENCE_RATE = :referenceRatePrm");
		}
//		if(Validations.validateListIsNotNullAndNotEmpty(filter.getListReferenceRate())){
//			sbQuery.append(" and bs.REFERENCE_RATE IN (:listReferenceRatePrm)");
//		}
		if(Validations.validateListIsNotNullAndNotEmpty(filter.getListServiceCode())){
			sbQuery.append(" and bs.SERVICE_CODE IN (:listServiceCodePrm)");
		}
		
		// T1: 158 - REPORTE DE COMISIONES POR MANTENIMIENTO EN ANOTACION EN CUENTA
		if(idReportPk == 158L){
			sbQuery.append(" and bs.reference_rate in ('T01','T02') ");
		}
		if ((economicActivity.intValue() == EconomicActivityType.BANCOS.getCode() || participant == 114L) && idReportPk == 158L) {
			if (participant == 114L) {
				sbQuery.append(" and bs.service_code in(1,2) ");
			} else {
				sbQuery.append(" and bs.service_code in(1,2,5,6) ");
			}
		}
		
		// T2: 157 - REPORTE DE COMISIONES POR MANTENIMIENTO DE VALORES EN CUSTODIA FISICA
		if(idReportPk == 157L){
			sbQuery.append(" and bs.reference_rate in ('T03','T24') ");
		}
		
		// T8: 152 - REPORTE DE TRANSACCIONES POR OPERACIONES MEDIANTE SIRTEX
		if(idReportPk == 152L){
			sbQuery.append(" and bs.reference_rate like 'T08' ");
		}
		
		// T9: 153 - REPORTE DE TRANSACCIONES POR OPERACIONES MEDIANTE OTC
		if(idReportPk == 153L){
			sbQuery.append(" and bs.reference_rate like 'T09' ");
		}
		
		// T10: 151 - REPORTE DE CALCULO DE COMISION POR EMISION DE VALORES DESMATERIALIZADOS
		if(idReportPk == 151L){
			sbQuery.append(" and bs.service_code in (17,18) ");
		}
		
		// T10: 154 - REPORTE DE COMISIONES PARA EMISIONES DPF
		if(idReportPk == 154L){
			sbQuery.append(" and bs.reference_rate like 'T10' ");
		}
		if (economicActivity.intValue() == EconomicActivityType.BANCOS.getCode() && (idReportPk == 154L)) {
			sbQuery.append(" and bs.service_code in(19) ");
		}
				
		// T10 : 278 - REPORTE DE COMISION POR EMISION DE VALORES DESM. - SAFI
		if(idReportPk == 278L){
			sbQuery.append(" and bs.service_code in(18) ");
		}
		
		// T15: 156 - REPORTE DE COMISIONES POR EMISION DE CERTIFICADOS DE ACREDITACION
		if(idReportPk == 156L){
			sbQuery.append(" and bs.reference_rate like 'T15' ");
		}
		
		// T17: 155 - REPORTE DE COMISIONES POR BLOQUEOS Y DESBLOQUEOS
		if(idReportPk == 155L){
			sbQuery.append(" and bs.reference_rate like 'T17' ");
		}
		
		// 201 - REPORTE DE RESUMEN DE COMISIONES POR CARTERA DE CLIENTE
		if(idReportPk == 201L){
			sbQuery.append(" and bs.reference_rate in ('T01','T02','T03','T24') ");
		}
		if ((economicActivity.intValue() == EconomicActivityType.BANCOS.getCode() || participant==114L) && idReportPk == 201L) {
			if(participant==114L){
				sbQuery.append(" and bs.service_code in(1,2) ");
			}else{
				sbQuery.append(" and bs.service_code in(1,2,5,6) ");	
			}
		}
		
		// T25 (5 y 6): 291 - REPORTE DE COMISIONES POR MANTENIMIENTO DE CUENTA 
		if(idReportPk == 291L){
			sbQuery.append(" and bs.reference_rate like 'T25' ");
		}
		
		// T11: 298 - REPORTE COMISION POR CAMBIO DE TITULARIDAD
		if(idReportPk == 298L){
			sbQuery.append(" and bs.reference_rate like 'T11' ");
		}
		
		//314 : REPORTE CONSOLIDADO COBROS POR ENTIDAD BCB
		if ((economicActivity.intValue() == EconomicActivityType.BANCOS.getCode() || participant==114L) && idReportPk == 314L) {
			if(participant==114L){
				sbQuery.append(" and bs.service_code in(1,2) ");
			}else{
				sbQuery.append(" and bs.service_code in(1,2,5,6) ");	
			}
		}
		
		// Issues 789: Validaciones para bancos=========== end
		//issue 643 servicio 49 - inlcusiones y exclusiones por participante
		// issue 789 tambien se verifica que NO sea banco para no agregar mas condiciones al where
		if(!(economicActivity.intValue() == EconomicActivityType.BANCOS.getCode()|| participant==114L)){
			sbQuery.append(" and bs.ID_BILLING_SERVICE_PK not in ( ");
		
			sbQuery.append(" select bss.id_billing_service_pk                                                   ");
			sbQuery.append(" from billing_service bss                                                           ");
			sbQuery.append(" inner join service_rate sr on sr.id_billing_service_fk = bss.id_billing_service_pk ");
			sbQuery.append(" inner join rate_exclusion re on re.id_service_rate_fk = sr.id_service_rate_pk      ");
			sbQuery.append(" where 1=1                                ");
			if(Validations.validateIsNotNullAndNotEmpty(filter.getReferenceRate())){
				sbQuery.append(" and bss.reference_rate in (:referenceRatePrm)                                  ");
			}
			sbQuery.append(" and (re.ind_select_all = 0 or re.id_participant_fk = :participant)                 ");
			sbQuery.append(" and re.exclusion_state = 1679                                                      ");
			sbQuery.append(" and :participant not in (114,133) ");
			sbQuery.append(" and bss.id_billing_service_pk not in (                                             ");
			sbQuery.append(" select bss.id_billing_service_pk                                                   ");
			sbQuery.append(" from billing_service bss                                                           ");
			sbQuery.append(" inner join service_rate sr on sr.id_billing_service_fk = bss.id_billing_service_pk ");
			sbQuery.append(" inner join rate_inclusion rin on rin.id_service_rate_fk = sr.id_service_rate_pk    ");
			
			sbQuery.append(" where 1=1                                											");
			if(Validations.validateIsNotNullAndNotEmpty(filter.getReferenceRate())){
				sbQuery.append(" and bss.reference_rate in (:referenceRatePrm)                                  ");
			}
			sbQuery.append(" and (rin.ind_select_all = 0 or rin.id_participant_fk = :participant)               ");
			sbQuery.append(" and rin.inclusion_state = 1679)                                                    ");
			sbQuery.append(" and :participant not in (114,133) ");
			
			sbQuery.append(" ) ");
		}
		
		sbQuery.append("  ORDER BY BS.REFERENCE_RATE,TO_NUMBER( BS.SERVICE_CODE,    9999) " );
    	Query query = em.createNativeQuery(sbQuery.toString());
    	
    	// Adicionando filtro por actividad economica Issue 1065
    	query.setParameter("id_participant_pk", participant);
    	//query.setParameter("economic_activity", economicActivity);
    	
    	// issue 789 tambien se verifica que NO sea banco para no agregar mas condiciones al where
    	if(Validations.validateIsNotNullAndPositive(participant )&&Validations.validateIsNotNullAndNotEmpty(participant)&&!(economicActivity.intValue() == EconomicActivityType.BANCOS.getCode()|| participant==114L)){
    		query.setParameter("participant", participant);
		}
    	if(Validations.validateIsNotNullAndNotEmpty(filter.getServiceName())){ 
			query.setParameter("serviceNamePrm", "%" + filter.getServiceName() + "%");
		}
    	if(Validations.validateIsNotNullAndPositive(filter.getStateService() )&&Validations.validateIsNotNullAndNotEmpty(filter.getStateService())){
    		query.setParameter("serviceStatePrm", filter.getStateService());
		}
		if(Validations.validateIsNotNullAndPositive(filter.getServiceType() )&&Validations.validateIsNotNullAndNotEmpty(filter.getServiceType())){
			query.setParameter("serviceTypePrm", filter.getServiceType());
		} 
		if(Validations.validateIsNotNullAndNotEmpty(filter.getServiceCode())){
			query.setParameter("serviceCodePrm", filter.getServiceCode());
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getEntityCollection() )&&Validations.validateIsNotNullAndNotEmpty(filter.getEntityCollection())){
			query.setParameter("entityCollectionPrm", filter.getEntityCollection());
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getBaseCollection() )&&Validations.validateIsNotNullAndNotEmpty(filter.getBaseCollection())){
			query.setParameter("baseCollectionPrm", filter.getBaseCollection());
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getClientServiceType() )&&Validations.validateIsNotNullAndNotEmpty(filter.getClientServiceType())){
    		query.setParameter("clientServiceTypePrm", filter.getClientServiceType());
		}
		if(Validations.validateIsNotNullAndPositive(filter.getCollectionPeriod() )&&Validations.validateIsNotNullAndNotEmpty(filter.getCollectionPeriod())){
    		query.setParameter("collectionPeriodPrm", filter.getCollectionPeriod());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdBillingServicePk())){
    		query.setParameter("billingServicePKPrm", filter.getIdBillingServicePk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getReferenceRate())){
			query.setParameter("referenceRatePrm", filter.getReferenceRate());
		}
//		if(Validations.validateListIsNotNullAndNotEmpty(filter.getListReferenceRate())){
//			query.setParameter("listReferenceRatePrm", filter.getListReferenceRate());
//		}
		if(Validations.validateListIsNotNullAndNotEmpty(filter.getListServiceCode())){
			query.setParameter("listServiceCodePrm", filter.getListServiceCode());
		}
		
		List<Object[]>  objectList = query.getResultList();
		
		List<BillingServiceTo> billingServiceToList = new ArrayList<BillingServiceTo>();
		for(int i=0;i<objectList.size();++i){
			//Getting Results from data base
			Object[] sResults = (Object[])objectList.get(i);
			BillingServiceTo billingServiceTo = new BillingServiceTo();
			billingServiceTo.setIdBillingServicePk(Long.parseLong(sResults[0]==null?"":sResults[0].toString())); 
			billingServiceTo.setServiceName(sResults[1]==null?"":sResults[1].toString());
			billingServiceTo.setServiceCode(sResults[2]==null?"":sResults[2].toString());
			billingServiceTo.setClientServiceType(sResults[3]==null?null:Integer.parseInt(sResults[3].toString()));
			billingServiceTo.setServiceType(sResults[4]==null?null:Integer.parseInt(sResults[4].toString()));
			billingServiceTo.setStateService(sResults[5]==null?null:Integer.parseInt(sResults[5].toString()));
			billingServiceTo.setEntityCollection(sResults[6]==null?null:Integer.parseInt(sResults[6].toString()));
			billingServiceTo.setBaseCollection(sResults[7]==null?null:Integer.parseInt(sResults[7].toString()));
			billingServiceTo.setCollectionPeriod(sResults[8]==null?null:Integer.parseInt(sResults[8].toString()));
			billingServiceTo.setDescriptionServiceType(sResults[9]==null?"":sResults[9].toString()); 
			billingServiceTo.setDescriptionStateBillingService(sResults[10]==null?"":sResults[10].toString());
			billingServiceTo.setDescriptionCollectionEntity(sResults[11]==null?"":sResults[11].toString());
			billingServiceTo.setDescriptionBaseCollection(sResults[12]==null?"":sResults[12].toString()); 
			billingServiceTo.setDescriptionCollectionPeriod(sResults[13]==null?"":sResults[13].toString());
			billingServiceTo.setDescriptionClientServiceType(sResults[14]==null?"":sResults[14].toString());
			billingServiceTo.setInitialEffectiveDate((Date) sResults[19]);
			billingServiceTo.setEndEffectiveDate((Date)sResults[20]);
			billingServiceTo.setCalculationPeriod(sResults[21]==null?null:Integer.parseInt(sResults[21].toString()));
			billingServiceTo.setCurrencyBilling(sResults[25]==null?null:Integer.parseInt(sResults[25].toString()));
			billingServiceTo.setCurrencyCalculation(sResults[26]==null?null:Integer.parseInt(sResults[26].toString()));
			billingServiceTo.setSourceInformation(sResults[27]==null?null:Integer.parseInt(sResults[27].toString()));
			
			billingServiceToList.add(billingServiceTo);
			 
		}
		return billingServiceToList;
	}
	public List<BillingServiceTo> getListParameterBillingService(BillingServiceTo filter, Long participant) throws ServiceException	{
		
		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		Map<String, Object> sessionMap = externalContext.getSessionMap();
		Long idReportPk = 0L;
		int idUserSessionPk = 0;
		BigDecimal economicActivity=BigDecimal.ZERO;
		try {
			idReportPk= (long) sessionMap.get(GeneralConstants.REPORT_BY_PROCESS); 
			Long idParticipantPk = userInfo.getUserAccountSession().getParticipantCode();
			economicActivity= getEconomicActivity(participant);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select bs.id_billing_service_pk, bs.SERVICE_NAME,bs.SERVICE_CODE,CLIENT_SERVICE_TYPE, "); 
		sbQuery.append(" SERVICE_TYPE,STATE_SERVICE,ENTITY_COLLECTION,BASE_COLLECTION,COLLECTION_PERIOD,"); 
		sbQuery.append(" (select pt.description from parameter_table pt where bs.SERVICE_TYPE=pt.parameter_table_pk ) as desription_service_type ,");
		sbQuery.append(" (select pt.description from parameter_table pt where bs.STATE_SERVICE=pt.parameter_table_pk ) as descripion_service_state,");
		sbQuery.append(" (select pt.description from parameter_table pt where bs.ENTITY_COLLECTION=pt.parameter_table_pk) as description_entity_collection, ");
		sbQuery.append(" (select pt.description from parameter_table pt where bs.BASE_COLLECTION=pt.parameter_table_pk) as description_base_collection, ");
		sbQuery.append(" (select pt.description from parameter_table pt where bs.COLLECTION_PERIOD=parameter_table_pk) as description_collection_period, ");
		sbQuery.append(" ( select pt.description from parameter_table pt where bs.CLIENT_SERVICE_TYPE=pt.parameter_table_pk ) as desription_client_service_type ,");
		sbQuery.append(" TAX_APPLIED,BUSINESS_LINE,FINANTIAL_CODE , COMMENTS , INITIAL_EFFECTIVE_DATE , END_EFFECTIVE_DATE, ");//15-20
		sbQuery.append(" CALCULATION_PERIOD,COLLECTION_TYPE,REGISTRY_DATE ,IND_INTEGRATED_BILL, CURRENCY_BILLING, 	");//21-25
		sbQuery.append(" CURRENCY_CALCULATION, SOURCE_INFORMATION   ");//26 27
		sbQuery.append(" from billing_service bs   ");
		sbQuery.append(" Where 1 = 1 " );
				
		 
		if(Validations.validateIsNotNullAndNotEmpty(filter.getServiceName())){
			sbQuery.append(" AND bs.SERVICE_NAME= :serviceNamePrm");
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getStateService()) && Validations.validateIsNotNullAndNotEmpty(filter.getStateService())){
			sbQuery.append(" AND bs.STATE_SERVICE= :serviceStatePrm");
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getServiceType())&&Validations.validateIsNotNullAndNotEmpty(filter.getServiceType())){
			sbQuery.append(" AND bs.SERVICE_TYPE= :serviceTypePrm");
		} 
		if(Validations.validateIsNotNullAndNotEmpty(filter.getServiceCode())){
			sbQuery.append(" AND bs.SERVICE_CODE= :serviceCodePrm");
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getEntityCollection() )&&Validations.validateIsNotNullAndNotEmpty(filter.getEntityCollection())){
			sbQuery.append(" AND bs.ENTITY_COLLECTION= :entityCollectionPrm");
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getBaseCollection() )&&Validations.validateIsNotNullAndNotEmpty(filter.getBaseCollection())){
			sbQuery.append(" AND bs.BASE_COLLECTION= :baseCollectionPrm");
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getClientServiceType() )&&Validations.validateIsNotNullAndNotEmpty(filter.getClientServiceType())){
			sbQuery.append(" AND bs.CLIENT_SERVICE_TYPE= :clientServiceTypePrm");
		}
		if(Validations.validateIsNotNullAndPositive(filter.getCollectionPeriod() )&&Validations.validateIsNotNullAndNotEmpty(filter.getCollectionPeriod())){
			sbQuery.append(" and bs.COLLECTION_PERIOD= :collectionPeriodPrm");
		}
		if(Validations.validateIsNotNullAndPositive(filter.getIdBillingServicePk() )&&Validations.validateIsNotNullAndNotEmpty(filter.getIdBillingServicePk())){
			sbQuery.append(" and bs.ID_BILLING_SERVICE_PK= :billingServicePKPrm");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getReferenceRate())){
			sbQuery.append(" and bs.REFERENCE_RATE = :referenceRatePrm");
		}
		if(Validations.validateListIsNotNullAndNotEmpty(filter.getListReferenceRate())){
			sbQuery.append(" and bs.REFERENCE_RATE IN (:listReferenceRatePrm)");
		}
		if(Validations.validateListIsNotNullAndNotEmpty(filter.getListServiceCode())){
			sbQuery.append(" and bs.SERVICE_CODE IN (:listServiceCodePrm)");
		}
		
		// T1: 158 - REPORTE DE COMISIONES POR MANTENIMIENTO EN ANOTACION EN CUENTA
		if(idReportPk == 158L){
			sbQuery.append(" and bs.reference_rate in ('T01','T02') ");
		}
		if ((economicActivity.intValue() == EconomicActivityType.BANCOS.getCode() || participant == 114L) && idReportPk == 158L) {
			if (participant == 114L) {
				sbQuery.append(" and bs.service_code in(1,2) ");
			} else {
				sbQuery.append(" and bs.service_code in(1,2,5,6) ");
			}
		}
		
		// T2: 157 - REPORTE DE COMISIONES POR MANTENIMIENTO DE VALORES EN CUSTODIA FISICA
		if(idReportPk == 157L){
			sbQuery.append(" and bs.reference_rate in ('T03','T24') ");
		}
		
		// T8: 152 - REPORTE DE TRANSACCIONES POR OPERACIONES MEDIANTE SIRTEX
		if(idReportPk == 152L){
			sbQuery.append(" and bs.reference_rate like 'T08' ");
		}
		
		// T9: 153 - REPORTE DE TRANSACCIONES POR OPERACIONES MEDIANTE OTC
		if(idReportPk == 153L){
			sbQuery.append(" and bs.reference_rate like 'T09' ");
		}
		
		// T10: 151 - REPORTE DE CALCULO DE COMISION POR EMISION DE VALORES DESMATERIALIZADOS
		if(idReportPk == 151L){
			sbQuery.append(" and bs.service_code in (17,18) ");
		}
		
		// T10: 154 - REPORTE DE COMISIONES PARA EMISIONES DPF
		if(idReportPk == 154L){
			sbQuery.append(" and bs.reference_rate like 'T10' ");
		}
		if (economicActivity.intValue() == EconomicActivityType.BANCOS.getCode() && (idReportPk == 154L)) {
			sbQuery.append(" and bs.service_code in(19) ");
		}
				
		// T10 : 278 - REPORTE DE COMISION POR EMISION DE VALORES DESM. - SAFI
		if(idReportPk == 278L){
			sbQuery.append(" and bs.service_code in(18) ");
		}
		
		// T15: 156 - REPORTE DE COMISIONES POR EMISION DE CERTIFICADOS DE ACREDITACION
		if(idReportPk == 156L){
			sbQuery.append(" and bs.reference_rate like 'T15' ");
		}
		
		// T17: 155 - REPORTE DE COMISIONES POR BLOQUEOS Y DESBLOQUEOS
		if(idReportPk == 155L){
			sbQuery.append(" and bs.reference_rate like 'T17' ");
		}
		
		// 201 - REPORTE DE RESUMEN DE COMISIONES POR CARTERA DE CLIENTE
		if(idReportPk == 201L){
			sbQuery.append(" and bs.reference_rate in ('T01','T02','T03','T24') ");
		}
		if ((economicActivity.intValue() == EconomicActivityType.BANCOS.getCode() || participant==114L) && idReportPk == 201L) {
			if(participant==114L){
				sbQuery.append(" and bs.service_code in(1,2) ");
			}else{
				sbQuery.append(" and bs.service_code in(1,2,5,6) ");	
			}
		}
		
		// T25 (5 y 6): 291 - REPORTE DE COMISIONES POR MANTENIMIENTO DE CUENTA 
		if(idReportPk == 291L){
			sbQuery.append(" and bs.reference_rate like 'T25' ");
		}
		
		// T11: 298 - REPORTE COMISION POR CAMBIO DE TITULARIDAD
		if(idReportPk == 298L){
			sbQuery.append(" and bs.reference_rate like 'T11' ");
		}
		
		//314 : REPORTE CONSOLIDADO COBROS POR ENTIDAD BCB
		if ((economicActivity.intValue() == EconomicActivityType.BANCOS.getCode() || participant==114L) && idReportPk == 314L) {
			if(participant==114L){
				sbQuery.append(" and bs.service_code in(1,2) ");
			}else{
				sbQuery.append(" and bs.service_code in(1,2,5,6) ");	
			}
		}
		
		sbQuery.append(" ORDER BY TO_NUMBER(BS.SERVICE_CODE,9999) " );
    	Query query = em.createNativeQuery(sbQuery.toString());
    	    	
    	if(Validations.validateIsNotNullAndNotEmpty(filter.getServiceName())){ 
			query.setParameter("serviceNamePrm", filter.getServiceName());
		}
    	if(Validations.validateIsNotNullAndPositive(filter.getStateService() )&&Validations.validateIsNotNullAndNotEmpty(filter.getStateService())){
    		query.setParameter("serviceStatePrm", filter.getStateService());
		}
		if(Validations.validateIsNotNullAndPositive(filter.getServiceType() )&&Validations.validateIsNotNullAndNotEmpty(filter.getServiceType())){
			query.setParameter("serviceTypePrm", filter.getServiceType());
		} 
		if(Validations.validateIsNotNullAndNotEmpty(filter.getServiceCode())){
			query.setParameter("serviceCodePrm", filter.getServiceCode());
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getEntityCollection() )&&Validations.validateIsNotNullAndNotEmpty(filter.getEntityCollection())){
			query.setParameter("entityCollectionPrm", filter.getEntityCollection());
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getBaseCollection() )&&Validations.validateIsNotNullAndNotEmpty(filter.getBaseCollection())){
			query.setParameter("baseCollectionPrm", filter.getBaseCollection());
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getClientServiceType() )&&Validations.validateIsNotNullAndNotEmpty(filter.getClientServiceType())){
    		query.setParameter("clientServiceTypePrm", filter.getClientServiceType());
		}
		if(Validations.validateIsNotNullAndPositive(filter.getCollectionPeriod() )&&Validations.validateIsNotNullAndNotEmpty(filter.getCollectionPeriod())){
    		query.setParameter("collectionPeriodPrm", filter.getCollectionPeriod());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdBillingServicePk())){
    		query.setParameter("billingServicePKPrm", filter.getIdBillingServicePk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getReferenceRate())){
			query.setParameter("referenceRatePrm", filter.getReferenceRate());
		}
		if(Validations.validateListIsNotNullAndNotEmpty(filter.getListReferenceRate())){
			query.setParameter("listReferenceRatePrm", filter.getListReferenceRate());
		}
		if(Validations.validateListIsNotNullAndNotEmpty(filter.getListServiceCode())){
			query.setParameter("listServiceCodePrm", filter.getListServiceCode());
		}
		
		List<Object[]>  objectList = query.getResultList();
		
		List<BillingServiceTo> billingServiceToList = new ArrayList<BillingServiceTo>();
		for(int i=0;i<objectList.size();++i){
			//Getting Results from data base
			Object[] sResults = (Object[])objectList.get(i);
			BillingServiceTo billingServiceTo = new BillingServiceTo();
			billingServiceTo.setIdBillingServicePk(Long.parseLong(sResults[0]==null?"":sResults[0].toString())); 
			billingServiceTo.setServiceName(sResults[1]==null?"":sResults[1].toString());
			billingServiceTo.setServiceCode(sResults[2]==null?"":sResults[2].toString());
			billingServiceTo.setClientServiceType(sResults[3]==null?null:Integer.parseInt(sResults[3].toString()));
			billingServiceTo.setServiceType(sResults[4]==null?null:Integer.parseInt(sResults[4].toString()));
			billingServiceTo.setStateService(sResults[5]==null?null:Integer.parseInt(sResults[5].toString()));
			billingServiceTo.setEntityCollection(sResults[6]==null?null:Integer.parseInt(sResults[6].toString()));
			billingServiceTo.setBaseCollection(sResults[7]==null?null:Integer.parseInt(sResults[7].toString()));
			billingServiceTo.setCollectionPeriod(sResults[8]==null?null:Integer.parseInt(sResults[8].toString()));
			billingServiceTo.setDescriptionServiceType(sResults[9]==null?"":sResults[9].toString()); 
			billingServiceTo.setDescriptionStateBillingService(sResults[10]==null?"":sResults[10].toString());
			billingServiceTo.setDescriptionCollectionEntity(sResults[11]==null?"":sResults[11].toString());
			billingServiceTo.setDescriptionBaseCollection(sResults[12]==null?"":sResults[12].toString()); 
			billingServiceTo.setDescriptionCollectionPeriod(sResults[13]==null?"":sResults[13].toString());
			billingServiceTo.setDescriptionClientServiceType(sResults[14]==null?"":sResults[14].toString());
			billingServiceTo.setInitialEffectiveDate((Date) sResults[19]);
			billingServiceTo.setEndEffectiveDate((Date)sResults[20]);
			billingServiceTo.setCalculationPeriod(sResults[21]==null?null:Integer.parseInt(sResults[21].toString()));
			billingServiceTo.setCurrencyBilling(sResults[25]==null?null:Integer.parseInt(sResults[25].toString()));
			billingServiceTo.setCurrencyCalculation(sResults[26]==null?null:Integer.parseInt(sResults[26].toString()));
			billingServiceTo.setSourceInformation(sResults[27]==null?null:Integer.parseInt(sResults[27].toString()));
			
			billingServiceToList.add(billingServiceTo);
			 
		}
		return billingServiceToList;
	}
	/** */
	public List<BillingServiceTo> getListParameterBillingService(BillingServiceTo filter) throws ServiceException	{
		
		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		Map<String, Object> sessionMap = externalContext.getSessionMap();
		Long idReportPk = 0L;
		int idUserSessionPk = 0;
		BigDecimal economicActivity=BigDecimal.ZERO;
		try {
			idReportPk= (long) sessionMap.get(GeneralConstants.REPORT_BY_PROCESS); 
			idUserSessionPk= (int) sessionMap.get(GeneralConstants.USER_INFO_ID); 
			economicActivity=getEconomicActivity(idUserSessionPk);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select bs.id_billing_service_pk, bs.SERVICE_NAME,bs.SERVICE_CODE,CLIENT_SERVICE_TYPE, "); 
		sbQuery.append("  SERVICE_TYPE,STATE_SERVICE,ENTITY_COLLECTION,BASE_COLLECTION,COLLECTION_PERIOD,"); 
		sbQuery.append(" ( select pt.description from parameter_table pt where bs.SERVICE_TYPE=pt.parameter_table_pk ) as desription_service_type ," );
		sbQuery.append(" ( select pt.description from parameter_table pt where bs.STATE_SERVICE=pt.parameter_table_pk ) as descripion_service_state," );
		sbQuery.append(" (select pt.description from parameter_table pt where bs.ENTITY_COLLECTION=pt.parameter_table_pk) as description_entity_collection, " );
		sbQuery.append("  (select pt.description from parameter_table pt where bs.BASE_COLLECTION=pt.parameter_table_pk) as description_base_collection, " );
		sbQuery.append("  (select pt.description from parameter_table pt where bs.COLLECTION_PERIOD=parameter_table_pk) as description_collection_period, " );
		sbQuery.append(" ( select pt.description from parameter_table pt where bs.CLIENT_SERVICE_TYPE=pt.parameter_table_pk ) as desription_client_service_type ," );
		sbQuery.append(" TAX_APPLIED,BUSINESS_LINE,FINANTIAL_CODE , COMMENTS , INITIAL_EFFECTIVE_DATE , END_EFFECTIVE_DATE, ");//15-20
		sbQuery.append("	 CALCULATION_PERIOD,COLLECTION_TYPE,REGISTRY_DATE ,IND_INTEGRATED_BILL, CURRENCY_BILLING, 	" );//21-25
		sbQuery.append("  CURRENCY_CALCULATION, SOURCE_INFORMATION   ");//26 27
		sbQuery.append("  from billing_service bs   ");
		sbQuery.append(" Where 1 = 1 " );
				
		 
		if(Validations.validateIsNotNullAndNotEmpty(filter.getServiceName())){
			sbQuery.append(" AND bs.SERVICE_NAME= :serviceNamePrm");
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getStateService()) && Validations.validateIsNotNullAndNotEmpty(filter.getStateService())){
			sbQuery.append(" AND bs.STATE_SERVICE= :serviceStatePrm");
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getServiceType())&&Validations.validateIsNotNullAndNotEmpty(filter.getServiceType())){
			sbQuery.append(" AND bs.SERVICE_TYPE= :serviceTypePrm");
		} 
		if(Validations.validateIsNotNullAndNotEmpty(filter.getServiceCode())){
			sbQuery.append(" AND bs.SERVICE_CODE= :serviceCodePrm");
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getEntityCollection() )&&Validations.validateIsNotNullAndNotEmpty(filter.getEntityCollection())){
			sbQuery.append(" AND bs.ENTITY_COLLECTION= :entityCollectionPrm");
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getBaseCollection() )&&Validations.validateIsNotNullAndNotEmpty(filter.getBaseCollection())){
			sbQuery.append(" AND bs.BASE_COLLECTION= :baseCollectionPrm");
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getClientServiceType() )&&Validations.validateIsNotNullAndNotEmpty(filter.getClientServiceType())){
			sbQuery.append(" AND bs.CLIENT_SERVICE_TYPE= :clientServiceTypePrm");
		}
		if(Validations.validateIsNotNullAndPositive(filter.getCollectionPeriod() )&&Validations.validateIsNotNullAndNotEmpty(filter.getCollectionPeriod())){
			sbQuery.append(" and bs.COLLECTION_PERIOD= :collectionPeriodPrm");
		}
		if(Validations.validateIsNotNullAndPositive(filter.getIdBillingServicePk() )&&Validations.validateIsNotNullAndNotEmpty(filter.getIdBillingServicePk())){
			sbQuery.append(" and bs.ID_BILLING_SERVICE_PK= :billingServicePKPrm");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getReferenceRate())){
			sbQuery.append(" and bs.REFERENCE_RATE = :referenceRatePrm");
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(filter.getListReferenceRate())){
			sbQuery.append(" and bs.REFERENCE_RATE IN (:listReferenceRatePrm)");
		}
		if(Validations.validateListIsNotNullAndNotEmpty(filter.getListServiceCode())){
			sbQuery.append(" and bs.SERVICE_CODE IN (:listServiceCodePrm)");
		}
		//153 : REPORTE DE TRANSACCIONES POR OPERACIONES MEDIANTE OTC
		if(economicActivity.intValue()==EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode()&&idReportPk==153L){
			sbQuery.append(" and bs.service_code in(15,16) ");
		}
		//155 : REPORTE DE COMISIONES POR BLOQUEOS Y DESBLOQUEOS
		if(economicActivity.intValue()==EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode()&&idReportPk==155L){
			sbQuery.append(" and bs.service_code in(26,27) ");
		}
		//156 : REPORTE DE COMISIONES POR EMISIÓN DE CERTIFICADOS DE ACREDITACION
		if(economicActivity.intValue()==EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode()&&idReportPk==156L){
			sbQuery.append(" and bs.service_code in(24) ");
		}
		//158 : REPORTE DE COMISIONES POR MANTENIMIENTO EN ANOTACION EN CUENTA
		if(economicActivity.intValue()==EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode()&&idReportPk==158L){
			sbQuery.append(" and bs.service_code in(1,2) ");
		}
		//151 REPORTE DE CALCULO DE COMISION POR EMISION DE VALORES DESMATERIALIZADOS
		if(economicActivity.intValue()==EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode()&&idReportPk==151L){
			sbQuery.append(" and bs.service_code in(17,18) ");
		}
		
		//278 REPORTE DE CALCULO DE COMISION POR EMISION DE VALORES DESMATERIALIZADOS SAFIS
		if(economicActivity.intValue()==EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode()&&idReportPk==278L){
			sbQuery.append(" and bs.service_code in(18) ");
		}
		//157 : REPORTE DE COMISIONES POR MANTENIMIENTO DE VALORES EN CUSTODIA FISICA
		if(economicActivity.intValue()==EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode()&&idReportPk==157L){
			sbQuery.append(" and bs.service_code in(7,33,34) ");
		}		
		//201 : REPORTE DE RESUMEN DE COMISIONES POR CARTERA DE CLIENTE
		if(economicActivity.intValue()==EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode()&&idReportPk==201L){
			sbQuery.append(" and bs.service_code in(1,2,7,33,34) ");
		}		
		//643 : inlcusiones y exclusiones por participante y actividad economica
		if((economicActivity.intValue()==EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode() 
				|| economicActivity.intValue()==EconomicActivityType.AGENCIAS_BOLSA.getCode() 
				|| economicActivity.intValue()==EconomicActivityType.ADMINISTRADORAS_FONDOS_PENSIONES.getCode()) && idReportPk==153L){
			sbQuery.append(" and bs.service_code in(15,16) ");
		}
		//Valores por defecto
		if(idReportPk==278L){
			sbQuery.append(" and bs.service_code in(18) ");
		}
		//736 : cobro tarifas 5 y 6
		if(idReportPk==291L){
			sbQuery.append(" and bs.service_code in (50, 51) ");
		}
		
		sbQuery.append(" ORDER BY TO_NUMBER(BS.SERVICE_CODE,9999) " );
    	Query query = em.createNativeQuery(sbQuery.toString());
    	    	
    	if(Validations.validateIsNotNullAndNotEmpty(filter.getServiceName())){ 
			query.setParameter("serviceNamePrm", filter.getServiceName());
		}
    	if(Validations.validateIsNotNullAndPositive(filter.getStateService() )&&Validations.validateIsNotNullAndNotEmpty(filter.getStateService())){
    		query.setParameter("serviceStatePrm", filter.getStateService());
		}
		if(Validations.validateIsNotNullAndPositive(filter.getServiceType() )&&Validations.validateIsNotNullAndNotEmpty(filter.getServiceType())){
			query.setParameter("serviceTypePrm", filter.getServiceType());
		} 
		if(Validations.validateIsNotNullAndNotEmpty(filter.getServiceCode())){
			query.setParameter("serviceCodePrm", filter.getServiceCode());
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getEntityCollection() )&&Validations.validateIsNotNullAndNotEmpty(filter.getEntityCollection())){
			query.setParameter("entityCollectionPrm", filter.getEntityCollection());
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getBaseCollection() )&&Validations.validateIsNotNullAndNotEmpty(filter.getBaseCollection())){
			query.setParameter("baseCollectionPrm", filter.getBaseCollection());
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getClientServiceType() )&&Validations.validateIsNotNullAndNotEmpty(filter.getClientServiceType())){
    		query.setParameter("clientServiceTypePrm", filter.getClientServiceType());
		}
		if(Validations.validateIsNotNullAndPositive(filter.getCollectionPeriod() )&&Validations.validateIsNotNullAndNotEmpty(filter.getCollectionPeriod())){
    		query.setParameter("collectionPeriodPrm", filter.getCollectionPeriod());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdBillingServicePk())){
    		query.setParameter("billingServicePKPrm", filter.getIdBillingServicePk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getReferenceRate())){
			query.setParameter("referenceRatePrm", filter.getReferenceRate());
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(filter.getListReferenceRate())){
			query.setParameter("listReferenceRatePrm", filter.getListReferenceRate());
		}
		if(Validations.validateListIsNotNullAndNotEmpty(filter.getListServiceCode())){
			query.setParameter("listServiceCodePrm", filter.getListServiceCode());
		}
		
		List<Object[]>  objectList = query.getResultList();
		
		List<BillingServiceTo> billingServiceToList = new ArrayList<BillingServiceTo>();
		for(int i=0;i<objectList.size();++i){
			//Getting Results from data base
			Object[] sResults = (Object[])objectList.get(i);
			BillingServiceTo billingServiceTo = new BillingServiceTo();
			billingServiceTo.setIdBillingServicePk(Long.parseLong(sResults[0]==null?"":sResults[0].toString())); 
			billingServiceTo.setServiceName(sResults[1]==null?"":sResults[1].toString());
			billingServiceTo.setServiceCode(sResults[2]==null?"":sResults[2].toString());
			billingServiceTo.setClientServiceType(sResults[3]==null?null:Integer.parseInt(sResults[3].toString()));
			billingServiceTo.setServiceType(sResults[4]==null?null:Integer.parseInt(sResults[4].toString()));
			billingServiceTo.setStateService(sResults[5]==null?null:Integer.parseInt(sResults[5].toString()));
			billingServiceTo.setEntityCollection(sResults[6]==null?null:Integer.parseInt(sResults[6].toString()));
			billingServiceTo.setBaseCollection(sResults[7]==null?null:Integer.parseInt(sResults[7].toString()));
			billingServiceTo.setCollectionPeriod(sResults[8]==null?null:Integer.parseInt(sResults[8].toString()));
			billingServiceTo.setDescriptionServiceType(sResults[9]==null?"":sResults[9].toString()); 
			billingServiceTo.setDescriptionStateBillingService(sResults[10]==null?"":sResults[10].toString());
			billingServiceTo.setDescriptionCollectionEntity(sResults[11]==null?"":sResults[11].toString());
			billingServiceTo.setDescriptionBaseCollection(sResults[12]==null?"":sResults[12].toString()); 
			billingServiceTo.setDescriptionCollectionPeriod(sResults[13]==null?"":sResults[13].toString());
			billingServiceTo.setDescriptionClientServiceType(sResults[14]==null?"":sResults[14].toString());
			billingServiceTo.setInitialEffectiveDate((Date) sResults[19]);
			billingServiceTo.setEndEffectiveDate((Date)sResults[20]);
			billingServiceTo.setCalculationPeriod(sResults[21]==null?null:Integer.parseInt(sResults[21].toString()));
			billingServiceTo.setCurrencyBilling(sResults[25]==null?null:Integer.parseInt(sResults[25].toString()));
			billingServiceTo.setCurrencyCalculation(sResults[26]==null?null:Integer.parseInt(sResults[26].toString()));
			billingServiceTo.setSourceInformation(sResults[27]==null?null:Integer.parseInt(sResults[27].toString()));
			
			billingServiceToList.add(billingServiceTo);
			 
		}
		return billingServiceToList;
	
	}
}
