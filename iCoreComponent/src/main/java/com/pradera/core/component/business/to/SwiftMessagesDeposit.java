package com.pradera.core.component.business.to;

import java.sql.Blob;

public class SwiftMessagesDeposit {

	private String bcrdBicSwiftCode;
	private String cevaldomBicSwiftCode;
	private String messageType;//BLOCK2 MESSAGE TYPE
	private String beneficiaryBicSwiftCode;
	private String referencePayment;//BLOCK4 FIELD 20
	private String originalReferencePayment;//BLOCK4 FIELD 21
	private String settlementDate;//BLOCK4 FIELD 32A
	private String currency;//BLOCK4 FIELD 32A
	private String amount;//BLOCK4 FIELD 32A
	private String issuer;//BLOCK4 FIELD 70

	private String swiftMessage;//THE SWIFT MESSAGE
	private Blob swiftFile;//THE SWIFT FILE PROCESSED
	private String originUrl;//ORIGIN URL WHERE GET THE FILE
	private String targetUrl;//THE FILE WHERE FILE IS BEING SAVED
	private String fileName;//FILE NAME


	public String getBcrdBicSwiftCode() {
		return bcrdBicSwiftCode;
	}
	public void setBcrdBicSwiftCode(String bcrdBicSwiftCode) {
		this.bcrdBicSwiftCode = bcrdBicSwiftCode;
	}
	public String getCevaldomBicSwiftCode() {
		return cevaldomBicSwiftCode;
	}
	public void setCevaldomBicSwiftCode(String cevaldomBicSwiftCode) {
		this.cevaldomBicSwiftCode = cevaldomBicSwiftCode;
	}
	public String getReferencePayment() {
		return referencePayment;
	}
	public void setReferencePayment(String referencePayment) {
		this.referencePayment = referencePayment;
	}
	public String getOriginalReferencePayment() {
		return originalReferencePayment;
	}
	public void setOriginalReferencePayment(String originalReferencePayment) {
		this.originalReferencePayment = originalReferencePayment;
	}
	public String getSettlementDate() {
		return settlementDate;
	}
	public void setSettlementDate(String settlementDate) {
		this.settlementDate = settlementDate;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getBeneficiaryBicSwiftCode() {
		return beneficiaryBicSwiftCode;
	}
	public void setBeneficiaryBicSwiftCode(String beneficiaryBicSwiftCode) {
		this.beneficiaryBicSwiftCode = beneficiaryBicSwiftCode;
	}
	public String getSwiftMessage() {
		return swiftMessage;
	}
	public void setSwiftMessage(String swiftMessage) {
		this.swiftMessage = swiftMessage;
	}
	public Blob getSwiftFile() {
		return swiftFile;
	}
	public void setSwiftFile(Blob swiftFile) {
		this.swiftFile = swiftFile;
	}
	public String getOriginUrl() {
		return originUrl;
	}
	public void setOriginUrl(String originUrl) {
		this.originUrl = originUrl;
	}
	public String getTargetUrl() {
		return targetUrl;
	}
	public void setTargetUrl(String targetUrl) {
		this.targetUrl = targetUrl;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public String getIssuer() {
		return issuer;
	}
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
}