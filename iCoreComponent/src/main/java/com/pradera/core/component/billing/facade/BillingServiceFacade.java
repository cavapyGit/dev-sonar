package com.pradera.core.component.billing.facade;

import java.rmi.ServerException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.billing.service.BillingServiceBean;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.billing.RateExclusion;


/**
* <ul>
* <li>
* Project iDepositary.
* Copyright PraderaTechnologies 2013.</li>
* </ul>
* 
* The Class BillingServiceFacade.
*
* @author PraderaTechnologies.
* @version 1.0 , 19/02/2014
*/
@Stateless
@Performance
public class BillingServiceFacade {
	
	@EJB
	BillingServiceBean billingServiceBean;
	
	 public Map  findRateExclusionListByIdServiceRatePk(Map parameters, LoggerUser loggerUser) throws ServerException{
		 
	
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
		
		Long serviceRatePk=(Long)parameters.get(GeneralConstants.ID_SERVICE_RATE_PK);
		
		List<RateExclusion> rateExclusionList=billingServiceBean.findRateExclusionListByIdServiceRatePk(serviceRatePk);
	 
		Map parameter = new HashMap<String, Object>(); 
	 
		rateExclusionList.get(0).setHolder(null);
		parameter.put(GeneralConstants.RATE_EXCEPTION_EXCLUSION_LIST, rateExclusionList);
		
		return parameter;
		
	 }
	

}
