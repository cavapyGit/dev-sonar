package com.pradera.core.component.helperui.to;

import java.io.Serializable;

import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.holderaccounts.HolderAccount;

/*
 * 	CREATED BY MARTIN ZARATE RAFAEL
 * 
 * 	GETS RESULTS FROM HOLDER HELPER
 */

public class HolderHelperResultTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5266123663347388880L;
	private Holder holder= new Holder();
	private HolderAccount holderAccount = new HolderAccount();
	
	
	// GETTERS AND SETTERS
	public Holder getHolder() {
		return holder;
	}
	public void setHolder(Holder holder) {
		this.holder = holder;
	}
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	
}
