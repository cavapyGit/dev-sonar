package com.pradera.core.component.helperui.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.helperui.to.MarketFactBalanceHelpTO;
import com.pradera.core.component.helperui.to.MarketFactDetailHelpTO;
import com.pradera.core.component.operation.to.MarketFactBalanceTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.component.HolderMarketFactBalance;

@Stateless
public class MarketFactBalanceServiceBean extends CrudDaoServiceBean {

	public MarketFactBalanceHelpTO populateMarketWithData(MarketFactBalanceHelpTO marketBalanceTO){	
		List<MarketFactDetailHelpTO> marketDetailList = new ArrayList<MarketFactDetailHelpTO>();
		for(MarketFactDetailHelpTO marketDetail: marketBalanceTO.getMarketFacBalances()){
			if(marketDetail.getIsSelected() && marketDetail.getEnteredBalance()!=null && !marketDetail.getEnteredBalance().equals(BigDecimal.ZERO)){
				marketDetailList.add(marketDetail);
			}
		}
		marketBalanceTO.setMarketFacBalances(marketDetailList);
		return marketBalanceTO;
	}

	@SuppressWarnings("unchecked")
	public MarketFactBalanceHelpTO findBalanceWithMarketFact(MarketFactBalanceHelpTO marketFacTO) throws ServiceException{
		String securityCode = marketFacTO.getSecurityCodePk();
		Long participantCode = marketFacTO.getParticipantPk();
		Long holderAccountCode = marketFacTO.getHolderAccountPk();
		
		/*VARIABLE PARA MANEJAR LOS PARAMETROS DEL QUERY*/
		Map<String,Object> parameters = new HashMap<String,Object>();
		
		/*VARIABLE PARA MANEJAR SI LOS HECHOS DE MERCADO DE LA OPERACION DE BLOQUEO YA FUERON HOMOLOGADOS*/
		Boolean blockIsHomologated = null;
		
		/*SI SE SOLICITA LOS HECHOS DE MERCADO PARA EL SALDO BLOQUEADO*/
		if(marketFacTO.getIndBlockBalance()!=null && marketFacTO.getIndBlockBalance().equals(BooleanType.YES.getCode())){
			/*SI SE SOLICITA EL SALDO BLOQUEADO Y LOS PARAMETROS NO SON ENVIADOS EN SU TOTALIDAD*/
			if(marketFacTO.getIdBlockOperationPk()==null || marketFacTO.getBlockOperationType()==null){
				throw new ServiceException(ErrorServiceType.MARKETFACT_UI_ERROR);
			}
			/*VERIFICAMOS SI LA OPERACION DE BLOQUEO FUE AFECTADO POR EL PROCESO DE HOMOLOGACION, ASIGNAMOS A "blockIsHomologated"*/
//			blockIsHomologated = isBlockOperationHomologated(marketFacTO.getBlockOperationNumber());
			blockIsHomologated = Boolean.FALSE;
		}
		
		/*VERIFICAMOS SI LA SOLICITUD ES PARA SALDOS PRESTABLES*/
		if(marketFacTO.getIndLonableBalance()!=null && marketFacTO.getIndLonableBalance().equals(BooleanType.YES.getCode())){
			/*SI EL ID DE LA OPERACION DE PRESTAMO ES NULL*/
			if(marketFacTO.getLonableOperationId()==null){
				throw new ServiceException(ErrorServiceType.MARKETFACT_UI_ERROR);
			}
		}
			
		StringBuilder NselectSql = new StringBuilder();
		StringBuilder NfromSql = new StringBuilder();
		StringBuilder NjoinSql = new StringBuilder();
		StringBuilder NwhereSql = new StringBuilder();
		
		NselectSql.append("Select holderacco0_.ID_SECURITY_CODE_PK, holderacco0_.ID_PARTICIPANT_PK, holderacco0_.ID_HOLDER_ACCOUNT_PK, security2_.DESCRIPTION,		");
		NselectSql.append("security2_.INSTRUMENT_TYPE, 																												");
		NselectSql.append("(select parametert4_.PARAMETER_NAME from PARAMETER_TABLE parametert4_ where parametert4_.PARAMETER_TABLE_PK=security2_.INSTRUMENT_TYPE),	");
		NselectSql.append("holdermark1_.ID_MARKETFACT_BALANCE_PK, holdermark1_.MARKET_DATE, holdermark1_.MARKET_PRICE, holdermark1_.MARKET_RATE, 					");
		NselectSql.append("holdermark1_.TOTAL_BALANCE AS mTotal,	holdermark1_.AVAILABLE_BALANCE AS mAvail, holderacco0_.TOTAL_BALANCE, holderacco0_.PAWN_BALANCE,");
		NselectSql.append("holderacco0_.BAN_BALANCE, holderacco0_.OTHER_BLOCK_BALANCE,	holdermark1_.PAWN_BALANCE AS mPawn, holdermark1_.BAN_BALANCE AS mBan,		");
		NselectSql.append("holdermark1_.OTHER_BLOCK_BALANCE AS mOther, holdermark1_.LOANABLE_BALANCE	AS mLoan													");						

		NfromSql.append("from	HOLDER_ACCOUNT_BALANCE holderacco0_ 									");
		
		NjoinSql.append("left outer join																");
		NjoinSql.append("HOLDER_MARKETFACT_BALANCE holdermark1_											"); 
		NjoinSql.append("   on holderacco0_.ID_HOLDER_ACCOUNT_PK=holdermark1_.ID_HOLDER_ACCOUNT_FK		"); 
		NjoinSql.append("   and holderacco0_.ID_PARTICIPANT_PK=holdermark1_.ID_PARTICIPANT_FK			"); 
		NjoinSql.append("   and holderacco0_.ID_SECURITY_CODE_PK=holdermark1_.ID_SECURITY_CODE_FK		");
		

		NwhereSql.append("	,SECURITY security2_ 														");
		NwhereSql.append("Where 	holderacco0_.ID_SECURITY_CODE_PK=security2_.ID_SECURITY_CODE_PK AND	");
		NwhereSql.append("	   	holderacco0_.ID_SECURITY_CODE_PK = :securityCode	AND					");
		NwhereSql.append("	   	holderacco0_.ID_PARTICIPANT_PK= :participantCode  AND					");
		NwhereSql.append("	   	holderacco0_.ID_HOLDER_ACCOUNT_PK= :holderAccountCode   AND				");
		NwhereSql.append("	   	holdermark1_.IND_ACTIVE = :oneParam	AND");
		NwhereSql.append("	   	holdermark1_.TOTAL_BALANCE >= :oneParam  	");
		
		if(marketFacTO.getIndLonableBalance()!=null && marketFacTO.getIndLonableBalance().equals(BooleanType.YES.getCode())){
			NselectSql.append("	, loanMark.current_Market_Quantity					");//20
			
			NjoinSql.append("	Inner Join loanable_securities_marketfact loanMark	ON(						");
			NjoinSql.append("			loanmark.market_date = holdermark1_.market_date	And					");
			NjoinSql.append("		   (loanmark.market_rate = holdermark1_.market_rate	OR					");
			NjoinSql.append("			loanmark.market_price = holdermark1_.market_price)	And				");
			NjoinSql.append("			loanmark.id_loanable_securities_oper_fk = :loanableOperationId)		");
			
			parameters.put("loanableOperationId", marketFacTO.getLonableOperationId());
		}

		
		if(blockIsHomologated!=null && !blockIsHomologated){
			NselectSql.append("	,blockMark.actual_Block_Balance																									");//20
			NjoinSql.append("Inner	Join Block_MarketFact_Operation blockMark	ON (	");
//			NjoinSql.append("Inner	Join Block_MarketFact_Operation blockMark	ON (blockMark.ID_MARKETFACT_BALANCE_FK = holdermark1_.ID_MARKETFACT_BALANCE_PK	");
			NjoinSql.append("			blockMark.market_date = holdermark1_.market_date	And																	");
			NjoinSql.append("		   (blockMark.market_rate = holdermark1_.market_rate	OR																	");
			NjoinSql.append("			blockMark.market_price = holdermark1_.market_price)	)																	");
			
			NjoinSql.append("Inner Join Block_Operation block ON (block.id_block_operation_pk = blockMark.id_block_operation_fk			)						");
		}
		
		if(blockIsHomologated!=null)
			if(blockIsHomologated){
				NwhereSql.append("	And 	(blockMark.pawn_Balance + blockMark._ban_balance + blockMark.other_Block_Balance) >0 		");
			}else{
				NwhereSql.append("	And 	block.id_block_operation_pk = :blockOperationPk			");
				NwhereSql.append("	And 	blockMark.ind_active = :oneParam						");
				parameters.put("blockOperationPk", marketFacTO.getIdBlockOperationPk());
			}
		
		String querySql = NselectSql.toString().concat(NfromSql.toString()).concat(NjoinSql.toString()).concat(NwhereSql.toString());
		
		querySql = querySql.concat("Order By holdermark1_.MARKET_DATE, holdermark1_.MARKET_PRICE, holdermark1_.MARKET_RATE DESC	");

		/*ASIGNAMOS LOS PARMAETROS AL MAP*/
		parameters.put("securityCode", securityCode);
		parameters.put("participantCode", participantCode);
		parameters.put("holderAccountCode", holderAccountCode);
		parameters.put("oneParam", BooleanType.YES.getCode());
		
		return populateMarketFactBalanceTO(this.findByNativeQuery(querySql, parameters),marketFacTO);
	}
	
	/**
	 * Checks if is block operation it's homologated.
	 *
	 * @param operationNumber the operation number
	 * @return the boolean
	 * @throws ServiceException the service exception
	 */
	private Boolean isBlockOperationHomologated(Long operationNumber) throws ServiceException{
		StringBuilder querySql = new StringBuilder();
		querySql.append("Select SUM(mark.indActive) From HolderMarketFactBalance mark				");
		querySql.append("Inner Join mark.blockMarketFactOperationList blockMark 					");
		querySql.append("Where 	blockMark.blockOperation.documentNumber = :blockOperationNumber		");
		
		Query query = em.createQuery(querySql.toString());
		query.setParameter("blockOperationNumber", operationNumber);
		
		try{
			return BooleanType.NO.getCode().equals(query.getSingleResult());
		}catch(NoResultException ex){
			throw new ServiceException(ErrorServiceType.INCONSISTENCY_DATA);
		}
	}
	
	private MarketFactBalanceHelpTO populateMarketFactBalanceTO(List<Object[]> objectDataList, MarketFactBalanceHelpTO marketBalance) throws ServiceException{
		MarketFactBalanceHelpTO marketBalanceTO = new MarketFactBalanceHelpTO();
		
		marketBalanceTO.setIndBlockBalance(marketBalance.getIndBlockBalance());
		marketBalanceTO.setIdBlockOperationPk(marketBalance.getIdBlockOperationPk());
		marketBalanceTO.setBlockOperationType(marketBalance.getBlockOperationType());
		marketBalanceTO.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());
		marketBalanceTO.setIndTotalBalance(marketBalance.getIndTotalBalance());
		marketBalanceTO.setIndLonableBalance(marketBalance.getIndLonableBalance());
		marketBalanceTO.setLonableOperationId(marketBalance.getLonableOperationId());
		
		/*SI EL COMPONENTE NO FUE LLAMADO DE UNA MANERA CORRECTA, EL BALANCE RESULT DEBE INDICAR AL MENOS 1 MARKET FACT BALANCE*/
		if(marketBalance.getBalanceResult()!=null && !marketBalance.getBalanceResult().equals(BigDecimal.ZERO)){
			if(marketBalance.getMarketFacBalances()== null || marketBalance.getMarketFacBalances().isEmpty()){
				
				/*SI EL SALDO TIENE MAS DE 2 SALDOS, EL MONTO A NIVEL DE SALDO NO PUEDE SER INGRESADO*/
				if(objectDataList!=null && objectDataList.size() >= 2){
					throw new ServiceException(ErrorServiceType.MARKETFACT_UI_ERROR);
				}
			}
		}
		
		/*SI EL SALDO USADO NO TIENE HECHOS DE MERCADO ASOCIADO*/
		if(objectDataList==null || objectDataList.isEmpty()){
			throw new ServiceException(ErrorServiceType.BALANCE_WITHOUT_MARKETFACT);
		}
		
		for(Object[] rowData: objectDataList){
			//if object it's null need to set general data
			if(marketBalanceTO.getSecurityCodePk()==null ){
				marketBalanceTO.setSecurityCodePk((String) rowData[0]);
				marketBalanceTO.setParticipantPk(new Long(rowData[1].toString()));
				marketBalanceTO.setHolderAccountPk(new Long(rowData[2].toString()));
				marketBalanceTO.setSecurityDescription((String) rowData[3]);
				marketBalanceTO.setBalanceResult(BigDecimal.ZERO);
				marketBalanceTO.setInstrumentType(new Integer(rowData[4].toString()));
				marketBalanceTO.setInstrumentDescription((String) rowData[5]);
				marketBalanceTO.setTotalBalance((BigDecimal) rowData[12]);
			}
			
			//adding the detail of MarketFact
			MarketFactDetailHelpTO marketDetailTO = null;
			Object holderMarketFact = rowData[6];
			if(holderMarketFact!=null){
				marketDetailTO = new MarketFactDetailHelpTO();
				marketDetailTO.setMarketFactBalancePk(new Long(holderMarketFact.toString()));
				marketDetailTO.setMarketDate((Date)rowData[7]);
				marketDetailTO.setMarketFactBalanceTO(marketBalanceTO);

				/**When rate or price it's != null it's necessary send to businessProcess */
				if(rowData[9]!=null){
					marketDetailTO.setMarketRate((BigDecimal) rowData[9]);
				}
				if(rowData[8]!=null){
					marketDetailTO.setMarketPrice((BigDecimal) rowData[8]);
				}
				
				
				marketDetailTO.setTotalBalance((BigDecimal) rowData[10]);
				marketDetailTO.setAvailableBalance((BigDecimal) rowData[11]);

				marketDetailTO.setPawnBalance((BigDecimal) rowData[16]);
				marketDetailTO.setBanBalance((BigDecimal) rowData[17]);
				marketDetailTO.setOtherBanBalance((BigDecimal) rowData[18]);
				marketDetailTO.setLoanableBalance((BigDecimal) rowData[19]);
				
				if(marketBalance.getIndBlockBalance()!=null && marketBalance.getIndBlockBalance().equals(BooleanType.YES.getCode())){
					marketDetailTO.setQuantityBlockDocument((BigDecimal) rowData[20]);
				}else if(marketBalance.getIndLonableBalance()!=null){
					marketDetailTO.setQuantityLoanableOperation((BigDecimal) rowData[20]);
				}
				
				//validate if quantity was entered on one past time
				BigDecimal quantityEntered = finQuantityEntered(marketDetailTO, marketBalance);
				if(quantityEntered.equals(BigDecimal.ZERO)){
					marketDetailTO.setEnteredBalance(null);
					marketDetailTO.setIsSelected(Boolean.FALSE);
				}else{
					/*SI EL SALDO TOTAL ES DIFERENTE AL SALDO INGRESADO*/
					if(marketDetailTO.getTotalBalance().compareTo(quantityEntered)== -1){
						throw new ServiceException(ErrorServiceType.INCONSISTENCY_DATA);
					}
					marketDetailTO.setEnteredBalance(quantityEntered);
					marketDetailTO.setIsSelected(Boolean.TRUE);
				}
				marketBalanceTO.setBalanceResult(marketBalanceTO.getBalanceResult().add(quantityEntered));
			}
			
			if(marketDetailTO!=null){
				if(marketBalanceTO.getLastMarketDate()==null){
					marketBalanceTO.setLastMarketDate(marketDetailTO.getMarketDate());
					marketBalanceTO.setLastMarketPrice(marketDetailTO.getMarketPrice());
					marketBalanceTO.setLastMarketRate(marketDetailTO.getMarketRate());
				}
				marketBalanceTO.getMarketFacBalances().add(marketDetailTO);
			}
		}
		if(!marketBalanceTO.getMarketFacBalances().isEmpty()){
			BigDecimal quantitySubBalances = BigDecimal.ZERO;
			for(MarketFactDetailHelpTO marketFactDetail: marketBalanceTO.getMarketFacBalances()){
				quantitySubBalances = quantitySubBalances.add(marketFactDetail.getTotalBalance());
			}
			
			if(marketBalanceTO.getIndBlockBalance()!=null){
				if(marketBalanceTO.getIndBlockBalance().equals(BooleanType.NO.getCode())){
					throw new ServiceException(ErrorServiceType.INCONSISTENCY_DATA);
				}
			}else{
				if(!quantitySubBalances.equals(marketBalanceTO.getTotalBalance()) ){
					throw new ServiceException(ErrorServiceType.INCONSISTENCY_DATA);
				}
			}
		}
		/* ISSUE 2636:  NO MOSTRAR LOS HECHOS DE MERCADO CON SALDO DISPONIBLE */
//		Iterator iterator = marketBalanceTO.getMarketFacBalances().iterator();
//		while (iterator.hasNext()) {
//			MarketFactDetailHelpTO marketFactDetailHelp = (MarketFactDetailHelpTO) iterator.next();
//			if(marketFactDetailHelp.getAvailableBalance().compareTo(BigDecimal.ZERO)<=0){
//				iterator.remove();
//			}
//
//		}
		return marketBalanceTO;
	}
	
	private BigDecimal finQuantityEntered(MarketFactDetailHelpTO marketDetail, MarketFactBalanceHelpTO marketBal) throws ServiceException{
		if(marketBal.getMarketFacBalances()!=null && !marketBal.getMarketFacBalances().isEmpty()){
			for(MarketFactDetailHelpTO marketDetailBal: marketBal.getMarketFacBalances()){
				/*SI VALIDAMOS CON PK DE HOLDER_MARKETFACT_BALANCE*/
				if(marketDetailBal.getMarketFactBalancePk()!=null){
					if(marketDetailBal.getMarketFactBalancePk().equals(marketDetail.getMarketFactBalancePk())){
						return marketDetailBal.getEnteredBalance(); 
					}
				}
				
				/*SI VALIDAMOS CON FECHA, TASA, PRECIO DEL HECHO DE MERCADO*/
				if(marketDetail.getMarketDate()!=null && (marketDetail.getMarketRate()!=null || marketDetail.getMarketPrice()!=null)){
					if(marketDetail.getMarketDate().equals(marketDetailBal.getMarketDate())){ 
						try{
							if(marketDetail.getMarketPrice()!=null && marketDetail.getMarketPrice().equals(marketDetailBal.getMarketPrice())){
								return marketDetailBal.getEnteredBalance();
							}
 							
							if(marketDetail.getMarketRate()!=null && marketDetail.getMarketRate().equals(marketDetailBal.getMarketRate())){
								return marketDetailBal.getEnteredBalance();
							}
						}catch(NullPointerException ex){
							throw new ServiceException(ErrorServiceType.MARKETFACT_UI_ERROR);
						}
					}
				}
			}
		}else if(marketBal.getBalanceResult()!=null && !marketBal.getBalanceResult().equals(BigDecimal.ZERO)){
			return marketBal.getBalanceResult();
		}
		return BigDecimal.ZERO;
	}
	
	public HolderMarketFactBalance findMarketFactBalance(MarketFactBalanceTO marketFactBalanceTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("Select hmb From HolderMarketFactBalance hmb Where 1 = 1 ");
		
		if(Validations.validateIsNotNullAndPositive(marketFactBalanceTO.getHolderAccountPk())){
			sbQuery.append(" And hmb.holderAccount.idHolderAccountPk = :holderAccountPkPrm");
			parameters.put("holderAccountPkPrm", marketFactBalanceTO.getHolderAccountPk());
		}
		if(Validations.validateIsNotNullAndNotEmpty( marketFactBalanceTO.getSecurityCodePk() )){
			sbQuery.append(" And hmb.security.idSecurityCodePk = :securityCodePkPrm");
			parameters.put("securityCodePkPrm", marketFactBalanceTO.getSecurityCodePk());
		}
		if(Validations.validateIsNotNullAndPositive( marketFactBalanceTO.getParticipantPk() )){
			sbQuery.append(" And hmb.participant.idParticipantPk = :participantPkPrm");
			parameters.put("participantPkPrm", marketFactBalanceTO.getParticipantPk());
		}

		Query query = this.em.createQuery(sbQuery.toString());
		for (Entry<String, Object> entry : parameters.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		return (HolderMarketFactBalance)query.getSingleResult();
	}
	
	
	public List<HolderMarketFactBalance> findMarketFactBalanceByAccreditationAccount(MarketFactBalanceTO marketFactBalanceTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("	Select hmb							");
		
		sbQuery.append("	From HolderMarketFactBalance hmb	");
		
		sbQuery.append("	Where 1 = 1	");
		sbQuery.append("		and hmb.indActive = 1			");
		sbQuery.append("		and hmb.totalBalance > 0		");
		
		if(Validations.validateIsNotNullAndPositive(marketFactBalanceTO.getHolderAccountPk())){
			sbQuery.append(" And hmb.holderAccount.idHolderAccountPk = :holderAccountPkPrm	");
			parameters.put("holderAccountPkPrm", marketFactBalanceTO.getHolderAccountPk());
		}
		if(Validations.validateIsNotNullAndNotEmpty( marketFactBalanceTO.getSecurityCodePk() )){
			sbQuery.append(" And hmb.security.idSecurityCodePk = :securityCodePkPrm			");
			parameters.put("securityCodePkPrm", marketFactBalanceTO.getSecurityCodePk());
		}
		if(Validations.validateIsNotNullAndPositive( marketFactBalanceTO.getParticipantPk() )){
			sbQuery.append(" And hmb.participant.idParticipantPk = :participantPkPrm		");
			parameters.put("participantPkPrm", marketFactBalanceTO.getParticipantPk());
		}

		Query query = this.em.createQuery(sbQuery.toString());
		for (Entry<String, Object> entry : parameters.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		return (List<HolderMarketFactBalance>)query.getResultList();
	}
	
}