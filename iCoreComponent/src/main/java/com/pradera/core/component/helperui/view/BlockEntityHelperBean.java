package com.pradera.core.component.helperui.view;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.el.ELContext;
import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;

import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.HelperBean;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.BlockEntitySearchTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;

@HelperBean
public class BlockEntityHelperBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private BlockEntitySearchTO blockEntitySearchTO;
	@EJB
	HelperComponentFacade helperComponentFacade;
	@Inject
	GeneralParametersFacade generalParametersFacade;
	
	@PostConstruct
	public void init(){
		blockEntitySearchTO = new BlockEntitySearchTO();
	}
	
	public void loadHelp(){
		try {
			blockEntitySearchTO = new BlockEntitySearchTO();
			fillCombos();
			RequestContext.getCurrentInstance().execute("PF('dlgBlockEntity').show();");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void changePersonType(){
		blockEntitySearchTO.setBlNatural(false);
		blockEntitySearchTO.setBlJuridic(false);
		blockEntitySearchTO.setName(null);
		blockEntitySearchTO.setFirstLastName(null);
		blockEntitySearchTO.setSecondLastName(null);
		blockEntitySearchTO.setFullName(null);
		if(PersonType.NATURAL.getCode().equals(blockEntitySearchTO.getPersonType())){
			blockEntitySearchTO.setBlNatural(true);
		}else if(PersonType.JURIDIC.getCode().equals(blockEntitySearchTO.getPersonType())){
			blockEntitySearchTO.setBlJuridic(true);
		}
	}
	
	
	public void findBlockEntity(){		
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.blockEntity}", BlockEntity.class);
		BlockEntity blockEntity = (BlockEntity) valueExpression.getValue(elContext);
		if(Validations.validateIsNotNullAndNotEmpty(blockEntity.getIdBlockEntityPk())){
			BlockEntity blockEntityTemp = helperComponentFacade.findBlockEntityByCode(blockEntity.getIdBlockEntityPk());
			if(Validations.validateIsNotNullAndNotEmpty(blockEntityTemp)){
				valueExpression.setValue(elContext, blockEntityTemp);
			}else{
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getGenericMessage("blockentity.helper.alert.notexist", new Object[]{blockEntity.getIdBlockEntityPk().toString()}));
				JSFUtilities.showSimpleValidationDialog();
				valueExpression.setValue(elContext, new BlockEntity());
			}
		}else{
			valueExpression.setValue(elContext, new BlockEntity());
		}		
	}
	
	public void searchType(){
		blockEntitySearchTO.setBlByRNT(false);
		blockEntitySearchTO.setBlByTypePerson(false);
		blockEntitySearchTO.setIdHolderPk(null);
		blockEntitySearchTO.setPersonType(PersonType.JURIDIC.getCode());
		changePersonType();
		blockEntitySearchTO.setName(null);
		blockEntitySearchTO.setFirstLastName(null);
		blockEntitySearchTO.setSecondLastName(null);
		blockEntitySearchTO.setFullName(null);
		if(GeneralConstants.ZERO_VALUE_INTEGER.equals(blockEntitySearchTO.getSearchType())){
			blockEntitySearchTO.setBlByTypePerson(true);
		}else{
			blockEntitySearchTO.setBlByRNT(true);
			blockEntitySearchTO.setPersonType(null);
		}
	}
	
	public void cleanResult(){
		blockEntitySearchTO.setLstBlockEntity(null);
		blockEntitySearchTO.setBlNoResult(false);
	}
	
	public void cleanSearch(){
		JSFUtilities.resetViewRoot();
		cleanResult();
		blockEntitySearchTO.setSearchType(null);
		blockEntitySearchTO.setBlByRNT(false);
		blockEntitySearchTO.setBlByTypePerson(false);
		blockEntitySearchTO.setName(null);
		blockEntitySearchTO.setFirstLastName(null);
		blockEntitySearchTO.setSecondLastName(null);
		blockEntitySearchTO.setFullName(null);
		blockEntitySearchTO.setBlJuridic(false);
		blockEntitySearchTO.setBlNatural(false);
		blockEntitySearchTO.setIdHolderPk(null);
		blockEntitySearchTO.setPersonType(null);
	}
	
	public void findBlockEntities(){
		try {
			if(Validations.validateIsNullOrEmpty(blockEntitySearchTO.getSearchType())){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getGenericMessage("blockentity.helper.alert.searchtype"));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			if(GeneralConstants.ZERO_VALUE_INTEGER.equals(blockEntitySearchTO.getSearchType()) &&
				Validations.validateIsNullOrEmpty(blockEntitySearchTO.getPersonType())){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getGenericMessage("blockentity.helper.alert.persontype"));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			if(GeneralConstants.ONE_VALUE_INTEGER.equals(blockEntitySearchTO.getSearchType()) &&
					Validations.validateIsNullOrEmpty(blockEntitySearchTO.getIdHolderPk())){
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getGenericMessage("blockentity.helper.alert.holder"));
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
			blockEntitySearchTO.setLstBlockEntity(null);
			blockEntitySearchTO.setBlNoResult(true);
			List<BlockEntity> lstResult = helperComponentFacade.findBlockEntities(blockEntitySearchTO);
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				blockEntitySearchTO.setLstBlockEntity(new GenericDataModel<BlockEntity>(setDescriptions(lstResult)));
				blockEntitySearchTO.setBlNoResult(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	private List<BlockEntity> setDescriptions(List<BlockEntity> lstResult){
		for(BlockEntity blockEntity:lstResult){
			blockEntity.setDescriptionTypeDocument(blockEntitySearchTO.getMpDocumentType().get(blockEntity.getDocumentType()));
			blockEntity.setStateDescription(blockEntitySearchTO.getMpState().get(blockEntity.getStateBlockEntity()));
		}
		return lstResult;
	}
	
	private void fillCombos() throws ServiceException{
		ParameterTableTO paramTabTO = new ParameterTableTO();
		paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
		paramTabTO.setMasterTableFk(MasterTableType.PERSON_TYPE.getCode());
		blockEntitySearchTO.setLstPersonType(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
		paramTabTO.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
		Map<Integer, String> mpTemp = new HashMap<Integer, String>();
		for(ParameterTable paramTab:generalParametersFacade.getListParameterTableServiceBean(paramTabTO)){
			mpTemp.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		blockEntitySearchTO.setMpDocumentType(mpTemp);
		paramTabTO.setMasterTableFk(MasterTableType.BLOCK_ENTITY_STATE.getCode());
		mpTemp = new HashMap<Integer, String>();
		for(ParameterTable paramTab:generalParametersFacade.getListParameterTableServiceBean(paramTabTO)){
			mpTemp.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		blockEntitySearchTO.setMpState(mpTemp);
	}
	
	public void getBlockEntity(BlockEntity blockEntity){
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.blockEntity}", BlockEntity.class);	
		valueExpression.setValue(elContext, helperComponentFacade.findBlockEntityByCode(blockEntity.getIdBlockEntityPk()));
	}

	public BlockEntitySearchTO getBlockEntitySearchTO() {
		return blockEntitySearchTO;
	}

	public void setBlockEntitySearchTO(BlockEntitySearchTO blockEntitySearchTO) {
		this.blockEntitySearchTO = blockEntitySearchTO;
	}
}
