package com.pradera.core.component.generalparameter.service;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.component.commandbutton.CommandButton;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.configuration.ValuatorSetup;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.InterfaceResponseCodeType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.integration.common.validation.to.OperationInterfaceTO;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.common.validation.to.ValidationOperationType;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.generalparameter.externalinterface.InterfaceResponseCode;
import com.pradera.model.valuatorprocess.ValuatorProcessSetup;

@Named
@ApplicationScoped
public class DepositarySetupBean {
	
	@EJB
	private ParameterServiceBean parameterServiceBean;
	
	private IdepositarySetup setup;
	
	private ValuatorProcessSetup valuatorSetup;
	
	@Inject
	transient PraderaLogger praderaLogger;
	
	@PostConstruct
    public void init() {
		setup = parameterServiceBean.getDepositarySetup(1l);
		valuatorSetup = parameterServiceBean.getValuatorProcessSetup();
		praderaLogger.info("LOADED IDEPOSITARY SETUP SUCCESSFULLY");
		
		for (InterfaceResponseCode responseCode : parameterServiceBean.getInterfaceResponseCodes()) {
			InterfaceResponseCodeType interfaceResponseCodeType = new InterfaceResponseCodeType();
			interfaceResponseCodeType.setIdResponseCode(responseCode.getIdInterfaceRespCodePk());
			interfaceResponseCodeType.setResponseCode(responseCode.getResponseCode());
			interfaceResponseCodeType.setResponseCodeDpf(responseCode.getResponseCodeDpf());
			CommonsUtilities.mapInterfaceErrorCode.put(responseCode.getResponseDescription(), interfaceResponseCodeType); 
		}
    }
	
	@Produces @DepositarySetup
	public IdepositarySetup geIdepositarySetup() {
		return setup;
	}
	
	@Produces @ValuatorSetup
	public ValuatorProcessSetup geValuatorProcessSetup() {
		return valuatorSetup;
	}
}