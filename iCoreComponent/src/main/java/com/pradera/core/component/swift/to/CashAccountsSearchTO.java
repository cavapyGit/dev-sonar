package com.pradera.core.component.swift.to;

import java.io.Serializable;

public class CashAccountsSearchTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer cashAccountType;
	private Integer participant;
	private String issuer;
	private Integer currency;
	private Integer state;
	private Long idNegoMechanismPk, idModalityGroupPk;


	public Integer getCashAccountType() {
		return cashAccountType;
	}
	public void setCashAccountType(Integer cashAccountType) {
		this.cashAccountType = cashAccountType;
	}
	public Integer getParticipant() {
		return participant;
	}
	public void setParticipant(Integer participant) {
		this.participant = participant;
	}
	public String getIssuer() {
		return issuer;
	}
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	public Integer getCurrency() {
		return currency;
	}
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public Long getIdNegoMechanismPk() {
		return idNegoMechanismPk;
	}
	public void setIdNegoMechanismPk(Long idNegoMechanismPk) {
		this.idNegoMechanismPk = idNegoMechanismPk;
	}
	public Long getIdModalityGroupPk() {
		return idModalityGroupPk;
	}
	public void setIdModalityGroupPk(Long idModalityGroupPk) {
		this.idModalityGroupPk = idModalityGroupPk;
	}
	
}