package com.pradera.core.component.helperui.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @author USUARIO
 *
 */
public class MarketFactDetailHelpTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long marketFactBalancePk ;
	
	private Date marketDate;
	
	private BigDecimal marketPrice;
	
	private BigDecimal marketRate;
	
	private BigDecimal totalBalance;
	
	private BigDecimal availableBalance;
	
	private BigDecimal pawnBalance;
	
	private BigDecimal banBalance;
	
	private BigDecimal otherBanBalance;
	
	private BigDecimal enteredBalance;
	
	private BigDecimal quantityBlockDocument;
	
	private BigDecimal loanableBalance;
	
	private BigDecimal quantityLoanableOperation;
	
	private boolean isSelected;
	
	private boolean isDisabled;
	
	private boolean todate;
	
	private boolean isRendered = true;		//por defecto renderiza el checkbox
	
	private Integer viewOperationType;		//opcional, para ver en que modo entro
	
	private MarketFactBalanceHelpTO marketFactBalanceTO;
	public MarketFactDetailHelpTO() {
		availableBalance = BigDecimal.ZERO;
		quantityBlockDocument = BigDecimal.ZERO;
		totalBalance = BigDecimal.ZERO;
		quantityLoanableOperation = BigDecimal.ZERO;
	}

	public Long getMarketFactBalancePk() {
		return marketFactBalancePk;
	}

	public void setMarketFactBalancePk(Long marketFactBalancePk) {
		this.marketFactBalancePk = marketFactBalancePk;
	}

	public BigDecimal getTotalBalance() {
		return totalBalance;
	}

	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	public BigDecimal getEnteredBalance() {
		return enteredBalance;
	}

	public void setEnteredBalance(BigDecimal enteredBalance) {
		this.enteredBalance = enteredBalance;
	}

	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	public MarketFactBalanceHelpTO getMarketFactBalanceTO() {
		return marketFactBalanceTO;
	}

	public void setMarketFactBalanceTO(MarketFactBalanceHelpTO marketFactBalanceTO) {
		this.marketFactBalanceTO = marketFactBalanceTO;
	}

	public BigDecimal getMarketRate() {
		return marketRate;
	}

	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	public Date getMarketDate() {
		return marketDate;
	}

	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	public boolean getIsSelected() {
		return isSelected;
	}

	public void setIsSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public BigDecimal getPawnBalance() {
		return pawnBalance;
	}

	public void setPawnBalance(BigDecimal pawnBalace) {
		this.pawnBalance = pawnBalace;
	}

	public BigDecimal getBanBalance() {
		return banBalance;
	}

	public void setBanBalance(BigDecimal banBalance) {
		this.banBalance = banBalance;
	}

	public BigDecimal getOtherBanBalance() {
		return otherBanBalance;
	}

	public void setOtherBanBalance(BigDecimal otherBanBalance) {
		this.otherBanBalance = otherBanBalance;
	}

	public BigDecimal getQuantityBlockDocument() {
		return quantityBlockDocument;
	}

	public void setQuantityBlockDocument(BigDecimal quantityBlockDocument) {
		this.quantityBlockDocument = quantityBlockDocument;
	}

	public BigDecimal getLoanableBalance() {
		return loanableBalance;
	}

	public void setLoanableBalance(BigDecimal loanableBalance) {
		this.loanableBalance = loanableBalance;
	}

	public BigDecimal getQuantityLoanableOperation() {
		return quantityLoanableOperation;
	}

	public void setQuantityLoanableOperation(BigDecimal quantityLoanableOperation) {
		this.quantityLoanableOperation = quantityLoanableOperation;
	}

	public boolean getIsRendered() {
		return isRendered;
	}

	public void setIsRendered(boolean isRendered) {
		this.isRendered = isRendered;
	}

	public Integer getViewOperationType() {
		return viewOperationType;
	}

	public void setViewOperationType(Integer viewOperationType) {
		this.viewOperationType = viewOperationType;
	}

	public boolean getTodate() {
		return todate;
	}

	public void setTodate(boolean todate) {
		this.todate = todate;
	}

	public boolean getIsDisabled() {
		return isDisabled;
	}

	public void setIsDisabled(boolean isDisabled) {
		this.isDisabled = isDisabled;
	}
	
}