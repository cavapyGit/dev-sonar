package com.pradera.core.component.business.to;

import java.io.Serializable;
import java.util.Date;

import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.negotiation.MechanismOperation;

/**
 * @author PraderaTechnologies
 *
 */
public class SwiftMessageJavaObject implements Serializable {

	/**
	 * Default Serial Version ID
	 */
	private static final long serialVersionUID = 1L;
	private Integer currency;
	private String paymentReference;
	private String trn;
	private String amount;
	private String issuer;
	private String participantBic;
	private String cevaldomBIC;
	private String bcrdBIC;
	private String settlementSchema;
	private MechanismOperation mechanismOperation;
	private Participant participant;
	private Integer indCashTerm;
	private boolean collectOperation;
	private Date settlementDate;
	private boolean blSpecialPayment;
	private InstitutionCashAccount cashAccount;
	private Bank bank;
	private Integer operationGroup; //funds operation group according the TRN field


	public Integer getCurrency() {
		return currency;
	}
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	public String getPaymentReference() {
		return paymentReference;
	}
	public void setPaymentReference(String paymentReference) {
		this.paymentReference = paymentReference;
	}
	public String getTrn() {
		return trn;
	}
	public void setTrn(String trn) {
		this.trn = trn;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getIssuer() {
		return issuer;
	}
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	public String getParticipantBic() {
		return participantBic;
	}
	public void setParticipantBic(String participantBic) {
		this.participantBic = participantBic;
	}
	public String getCevaldomBIC() {
		return cevaldomBIC;
	}
	public void setCevaldomBIC(String cevaldomBIC) {
		this.cevaldomBIC = cevaldomBIC;
	}
	public String getBcrdBIC() {
		return bcrdBIC;
	}
	public void setBcrdBIC(String bcrdBIC) {
		this.bcrdBIC = bcrdBIC;
	}
	public String getSettlementSchema() {
		return settlementSchema;
	}
	public void setSettlementSchema(String settlementSchema) {
		this.settlementSchema = settlementSchema;
	}
	public MechanismOperation getMechanismOperation() {
		return mechanismOperation;
	}
	public void setMechanismOperation(MechanismOperation mechanismOperation) {
		this.mechanismOperation = mechanismOperation;
	}
	public Participant getParticipant() {
		return participant;
	}
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}
	public Integer getIndCashTerm() {
		return indCashTerm;
	}
	public void setIndCashTerm(Integer indCashTerm) {
		this.indCashTerm = indCashTerm;
	}
	public boolean isCollectOperation() {
		return collectOperation;
	}
	public void setCollectOperation(boolean collectOperation) {
		this.collectOperation = collectOperation;
	}
	public Date getSettlementDate() {
		return settlementDate;
	}
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}
	public boolean isBlSpecialPayment() {
		return blSpecialPayment;
	}
	public void setBlSpecialPayment(boolean blSpecialPayment) {
		this.blSpecialPayment = blSpecialPayment;
	}
	public InstitutionCashAccount getCashAccount() {
		return cashAccount;
	}
	public void setCashAccount(InstitutionCashAccount cashAccount) {
		this.cashAccount = cashAccount;
	}
	
	public Integer getOperationGroup() {
		return operationGroup;
	}
	public void setOperationGroup(Integer operationGroup) {
		this.operationGroup = operationGroup;
	}
	public Bank getBank() {
		return bank;
	}
	public void setBank(Bank bank) {
		this.bank = bank;
	}	
}