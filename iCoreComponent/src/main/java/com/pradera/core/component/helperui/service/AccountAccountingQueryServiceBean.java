package com.pradera.core.component.helperui.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.accounting.account.to.AccountingProcessTo;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.helperui.to.AccountingAccountTo;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounting.AccountingAccount;
import com.pradera.model.accounting.AccountingParameter;
import com.pradera.model.accounting.AccountingProcess;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountAccountingQueryServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01-sep-2015
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class AccountAccountingQueryServiceBean extends CrudDaoServiceBean {
	
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	

	
	
	/**
	 * Find accounting account service bean.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<AccountingAccountTo>  findAccountingAccountServiceBean(AccountingAccountTo filter)throws ServiceException{
		
		List<AccountingAccountTo> listAccountTo=null;
		 StringBuilder sbQuery = new StringBuilder();
	     sbQuery.append("    select AA.ID_ACCOUNTING_ACCOUNT_PK,AA.ACCOUNT_CODE, AA.DESCRIPTION,");//0 1 2
	     sbQuery.append("    NVL(AA.STATUS ,0) AS STATUS,                  " );//3
	     sbQuery.append("    NVL((select description from ACCOUNTING_PARAMETER pt where PT.ID_ACCOUNTING_PARAMETER_PK=aa.ACCOUNT_TYPE),' ') AS DESC_ACCOUNT_TYPE,                  " );//4
	     sbQuery.append("    NVL((select description from parameter_table pt where PT.PARAMETER_TABLE_PK=aa.CURRENCY_TYPE ),' ')AS DESC_CURRENCY,                 " );//5
	     sbQuery.append("    NVL((select description from parameter_table pt where PT.PARAMETER_TABLE_PK=aa.STATUS ),' ') AS DESC_STATUS,                 " );//6
	     sbQuery.append("    NVL(AA.NATURE_TYPE ,0) AS NATURE_TYPE,                " );//7
	     sbQuery.append("    NVL(AA.IND_ACCUMULATION ,0) AS IND_ACCUMULATION,                       " );//8
	     sbQuery.append("    NVL(AA.IND_BALANCE_CONTROL ,0) AS IND_BALANCE_CONTROL,                       " );//9
	     sbQuery.append("    NVL(AA.IND_DIMENSION ,0) AS IND_DIMENSION,                       " );//10
	     sbQuery.append("    NVL(AA.ADJUSTMENT_TYPE ,0) AS ADJUSTMENT_TYPE,                       " );//11
	     sbQuery.append("    NVL(AA.AUXILIARY_TYPE ,0) AS AUXILIARY_TYPE,                       " );//12
	     sbQuery.append("    NVL(AA.ACCOUNT_TYPE  ,0) AS ACCOUNT_TYPE ,                " );//13
	     sbQuery.append("    NVL( AA.CURRENCY_TYPE,0) AS CURRENCY,               " );//14
	     sbQuery.append("    NVL( AA.INSTRUMENT_TYPE,0) AS INSTRUMENT_TYPE,               " );//15
	     
	     sbQuery.append("     AA.REGISTER_DATE,                           " );//16
	     sbQuery.append("    NVL(AA.REGISTER_USER ,' ') AS REGISTER_USER  ,              " );//17
	     sbQuery.append("    AA.UPDATE_DATE,                " );//18
	     sbQuery.append("    NVL(AA.UPDATE_USER ,' ') AS UPDATE_USER   ,             " );//19
	     sbQuery.append("     AA.CANCEL_DATE,               " );//20
	     sbQuery.append("    NVL(AA.CANCEL_USER ,' ') AS CANCEL_USER   ,              " );//21
	     sbQuery.append("    NVL((select description from ACCOUNTING_PARAMETER pt where PT.ID_ACCOUNTING_PARAMETER_PK=aa.AUXILIARY_TYPE),' ') AS DESC_AUXILIARY_TYPE  " );//22

	     sbQuery.append("    from ACCOUNTING_ACCOUNT AA               " );
	     sbQuery.append("    WHERE 1=1                 " );
	     	if (Validations.validateIsNotNullAndNotEmpty(filter.getAccountCode())){
			 
	    	 sbQuery.append(" and AA.account_code = :accountCode  ");
			}
			if (Validations.validateIsNotNullAndPositive(filter.getAccountType())){
				 
				sbQuery.append(" and AA.account_type = :accountType ");
			}
			if (Validations.validateIsNotNullAndPositive(filter.getNatureType())){
				 
				sbQuery.append(" and AA.NATURE_TYPE = :natureType  ");
			}
			if (Validations.validateIsNotNullAndPositive(filter.getAdjustmentType())){
				 
				sbQuery.append(" and AA.ADJUSTMENT_TYPE = :adjustmentType  ");
			}	
			if (Validations.validateIsNotNull(filter.getIndAccumulation())){
				 
				sbQuery.append(" and AA.IND_ACCUMULATION   = :indAccumulation  ");
			}
			if (Validations.validateIsNotNullAndPositive(filter.getAuxiliaryType())){
				 
				sbQuery.append(" and AA.AUXILIARY_TYPE   = :auxiliaryType  ");
			}
			
			sbQuery.append("   ORDER BY AA.ACCOUNT_CODE, AA.AUXILIARY_TYPE, AA.ACCOUNT_TYPE      " );
			
			Query query = em.createNativeQuery(sbQuery.toString());
			
			if (Validations.validateIsNotNullAndNotEmpty(filter.getAccountCode())){
				 
				query.setParameter("accountCode",filter.getAccountCode() );
			}
			if (Validations.validateIsNotNullAndPositive(filter.getAccountType())){
				 
				query.setParameter("accountType",filter.getAccountType() );
			}
			if (Validations.validateIsNotNullAndPositive(filter.getNatureType())){
				 
				query.setParameter("natureType",filter.getNatureType() );
			}
			if (Validations.validateIsNotNullAndPositive(filter.getAdjustmentType())){
				 
				query.setParameter("adjustmentType",filter.getAdjustmentType() );
			}
			if (Validations.validateIsNotNull(filter.getIndAccumulation())){
				 
				query.setParameter("indAccumulation",filter.getIndAccumulation() );
			}
			if (Validations.validateIsNotNullAndPositive(filter.getAuxiliaryType())){
				 
				query.setParameter("auxiliaryType", filter.getAuxiliaryType() );
			}
			
			List<Object[]>  objectList = query.getResultList();
			listAccountTo=new ArrayList<AccountingAccountTo>();
			if (Validations.validateListIsNotNullAndNotEmpty(objectList)){
				AccountingAccountTo accountTo=null;
				for(int i=0;i<objectList.size();i++){
					Object[] sResults = (Object[])objectList.get(i);
					accountTo=new AccountingAccountTo();
					accountTo.setIdAccountingAccountPk(Long.parseLong(sResults[0].toString()));
					accountTo.setAccountCode(sResults[1].toString());
					accountTo.setDescription(sResults[2].toString());
					accountTo.setStatus(Integer.parseInt(sResults[3].toString()));
					accountTo.setDescriptionTypeAccount(sResults[4].toString());
					accountTo.setDescriptionCurrency(sResults[5].toString());
					accountTo.setDescriptionStateAccount(sResults[6].toString());
					
					accountTo.setNatureType(Integer.parseInt(sResults[7].toString()));
					accountTo.setIndAccumulation(Integer.parseInt(sResults[8].toString()));
					accountTo.setIndBalanceControl(Integer.parseInt(sResults[9].toString()));
					accountTo.setIndDimension(Integer.parseInt(sResults[10].toString()));
					accountTo.setAdjustmentType(Integer.parseInt(sResults[11].toString()));
					accountTo.setAuxiliaryType(Integer.parseInt(sResults[12].toString()));
					accountTo.setAccountType(Integer.parseInt(sResults[13].toString()));
					accountTo.setCurrency(Integer.parseInt(sResults[14].toString()));
					accountTo.setInstrumentType(Integer.parseInt(sResults[15].toString()));;
					
					accountTo.setRegisterDate((Date)sResults[16]);
					accountTo.setRegisterUser(sResults[17].toString());
					accountTo.setUpdateDate((Date)sResults[18]);
					accountTo.setUpdateUser(sResults[19].toString());
					accountTo.setCancelDate((Date)sResults[20]);
					accountTo.setCancelUser(sResults[21].toString());
					accountTo.setDescriptionAuxiliaryType(sResults[22].toString());

					
					listAccountTo.add(accountTo);
				}
			}
		
		return listAccountTo;
	}
	
	
	
	/**
	 * Find accounting account by code.
	 *
	 * @param accountCode the account code
	 * @param auxiliaryTypeInd the auxiliary type ind
	 * @return the accounting account to
	 * @throws ServiceException the service exception
	 */
	public AccountingAccountTo findAccountingAccountByCode(String accountCode, Integer auxiliaryTypeInd) throws ServiceException{
 
		AccountingAccount   accountingAccountTemp ;
		AccountingAccountTo accountingAccountToTemp ;
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select ac from AccountingAccount ac where ac.accountCode = :accountCode  ");
		
		if(Validations.validateIsNotNullAndPositive(auxiliaryTypeInd)){
			sbQuery.append(" and ac.auxiliaryType = :auxiliaryType  ");
		}
 
		TypedQuery<AccountingAccount> query =  em.createQuery(sbQuery.toString(),AccountingAccount.class); 
		
	 
		query.setParameter("accountCode", accountCode );
		
		if(Validations.validateIsNotNullAndPositive(auxiliaryTypeInd)){
			query.setParameter("auxiliaryType", auxiliaryTypeInd );
		}
		
		
		
		try {		 	
		 
			accountingAccountTemp = query.getSingleResult();
		
			
			accountingAccountToTemp = new AccountingAccountTo(); 
			
			accountingAccountToTemp.setIdAccountingAccountPk(accountingAccountTemp.getIdAccountingAccountPk());
			accountingAccountToTemp.setAccountCode(accountingAccountTemp.getAccountCode());
			accountingAccountToTemp.setDescription(accountingAccountTemp.getDescription());
			accountingAccountToTemp.setIndAccumulation(accountingAccountTemp.getIndAccumulation());
			accountingAccountToTemp.setNatureType(accountingAccountTemp.getNatureType());
			
			accountingAccountToTemp.setAuxiliaryType(accountingAccountTemp.getAuxiliaryType());
			accountingAccountToTemp.setAccountType(accountingAccountTemp.getAccountType());
			accountingAccountToTemp.setInstrumentType(accountingAccountTemp.getInstrumentType());
			accountingAccountToTemp.setCurrency(accountingAccountTemp.getCurrency());
			accountingAccountToTemp.setAdjustmentType(accountingAccountTemp.getAdjustmentType());
			
			return  accountingAccountToTemp ;
					
		} catch (Exception e) {

		
			return null;
		
		}
		
		
		
	}
	
	
	
	/**
	 * Search parameter accounting.
	 *
	 * @param filter the filter
	 * @return the list
	 */
	public List<AccountingParameter> searchParameterAccounting(AccountingParameter filter){
		 		
	 	
		StringBuilder stringBuilderSqlBase = new StringBuilder();		
		stringBuilderSqlBase.append(" SELECT  AC.ID_ACCOUNTING_PARAMETER_PK, ");
		stringBuilderSqlBase.append(" ( select pt.description from parameter_table pt where AC.PARAMETER_TYPE=pt.parameter_table_pk ) as description_parameter_type, " );
		stringBuilderSqlBase.append(" AC.NR_CORRELATIVE,  ");
		stringBuilderSqlBase.append(" AC.PARAMETER_NAME,  ");
		stringBuilderSqlBase.append(" AC.DESCRIPTION,     ");		 
		stringBuilderSqlBase.append(" ( select pt.description from parameter_table pt where AC.STATUS=pt.parameter_table_pk ) as description_accounting_type, " );
		stringBuilderSqlBase.append(" AC.PARAMETER_TYPE  ");
		

		
		stringBuilderSqlBase.append(" FROM ACCOUNTING_PARAMETER  AC ");
		
		stringBuilderSqlBase.append(" WHERE 1=1 ");   
		
		if ( Validations.validateIsNotNullAndPositive(filter.getParameterType()) ){
			stringBuilderSqlBase.append(" AND AC.PARAMETER_TYPE = :parameterType    ");
		}
		if ( Validations.validateIsNotNull(filter.getParameterName()) ){
			stringBuilderSqlBase.append(" AND AC.PARAMETER_NAME = :parameterName    ");
		}	
		if ( Validations.validateIsNotNullAndPositive(filter.getStatus()) ){
			stringBuilderSqlBase.append(" AND AC.STATUS = :status    ");
		}
		
		stringBuilderSqlBase.append(" ORDER BY AC.ID_ACCOUNTING_PARAMETER_PK  ");
		
		Query query = em.createNativeQuery(stringBuilderSqlBase.toString());	
		
		
		if ( Validations.validateIsNotNullAndPositive(filter.getParameterType()) ){
			query.setParameter("parameterType", filter.getParameterType() );
		}		
		if ( Validations.validateIsNotNull(filter.getParameterName()) ){
			query.setParameter("parameterName", filter.getParameterName() );
		} 
		if ( Validations.validateIsNotNullAndPositive(filter.getStatus()) ){
			query.setParameter("status", filter.getStatus() );
		} 
		
		
		
		List<Object[]>  objectList = query.getResultList();
		
		
		List<AccountingParameter> listAccountingParameters  = new ArrayList<AccountingParameter>();

		
		for(int i=0;i<objectList.size();++i){	
			Object[] sResults = (Object[])objectList.get(i);
			AccountingParameter accountingParameter = new AccountingParameter();
			
			accountingParameter.setIdAccountingParameterPk(Integer.parseInt(sResults[0]==null?"":sResults[0].toString())); 
			accountingParameter.setDescriptionParameterType(sResults[1]==null?"":sResults[1].toString()); 			
			accountingParameter.setNrCorrelative(sResults[2]==null?null:Integer.parseInt(sResults[2].toString()));
			accountingParameter.setParameterName(sResults[3]==null?"":sResults[3].toString());			
			accountingParameter.setDescription(sResults[4]==null?"":sResults[4].toString());
			accountingParameter.setDescriptionParameterStatus(sResults[5]==null?"":sResults[5].toString()); 
			accountingParameter.setParameterType(sResults[6]==null?null:Integer.parseInt(sResults[6].toString()));
			accountingParameter.setNameWithDescription(sResults[3] + " - " +sResults[4].toString());		
			

			
			
			listAccountingParameters.add(accountingParameter);
			 
		}
		return listAccountingParameters;
	}
	
	/**
	 * Find accounting account to filter for Reports iAccounting.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<AccountingAccount>  findAccountingAccountForReports(AccountingAccount filter)throws ServiceException{
		StringBuilder stringBuilderSql = new StringBuilder();	
		
		
		// AccountingAccount full Constructor
		stringBuilderSql.append("Select new AccountingAccount(aa.idAccountingAccountPk, aas , aa.accountCode ,   ");
		stringBuilderSql.append("  aa.description,  aa.natureType, aa.currency, aa.portFolio, ");
		stringBuilderSql.append("  aa.indAccumulation, aa.indReference,    ");
		stringBuilderSql.append("  aa.auxiliaryType,  aa.status, aa.indSubAccount,   ");
		stringBuilderSql.append("  aa.instrumentType )  ");
		stringBuilderSql.append(" From AccountingAccount aa    ");
		stringBuilderSql.append("  left outer join  aa.accountingAccount aas ");

		stringBuilderSql.append(" WHERE 1=1   ");	
		if (Validations.validateIsNotNullAndNotEmpty(filter.getIdAccountingAccountPk())){
			 
			stringBuilderSql.append(" and aa.idAccountingAccountPk = :idAccountingAccountPk  ");
		}
		if (Validations.validateIsNotNullAndNotEmpty(filter.getAccountCode())){
			 
			stringBuilderSql.append(" and aa.accountCode = :accountCode  ");
		}
		if (Validations.validateIsNotNullAndPositive(filter.getAccountType())){
			 
			stringBuilderSql.append(" and aa.accountType = :accountType ");
		}
		if (Validations.validateIsNotNullAndPositive(filter.getNatureType())){
			 
			stringBuilderSql.append(" and aa.natureType = :natureType  ");
		}
		if (Validations.validateIsNotNullAndPositive(filter.getAdjustmentType())){
			 
			stringBuilderSql.append(" and aa.adjustmentType = :adjustmentType  ");
		}
		if (Validations.validateIsNotNullAndPositive(filter.getAuxiliaryType())){
			 
			stringBuilderSql.append(" and aa.auxiliaryType = :auxiliaryType  ");
		}
		if (Validations.validateIsNotNullAndPositive(filter.getStatus())){
			 
			stringBuilderSql.append(" and aa.status  = :status  ");
		}
		if (Validations.validateIsNotNullAndPositive(filter.getIndEntityAccount())){
			 
			stringBuilderSql.append(" and aa.indEntityAccount  = :indEntityAccount  ");
		}
		if (Validations.validateIsNotNullAndPositive(filter.getEntityAccount())){
			 
			stringBuilderSql.append(" and aa.entityAccount  = :entityAccount  "); 
		}
		
		stringBuilderSql.append(" ORDER BY aa.accountCode, aa.natureType, aa.accountType, aa.auxiliaryType  ");
		
		Query query = em.createQuery(stringBuilderSql.toString());

		if (Validations.validateIsNotNullAndNotEmpty(filter.getIdAccountingAccountPk())){
			 
			query.setParameter("idAccountingAccountPk", filter.getIdAccountingAccountPk());
		}
		if (Validations.validateIsNotNullAndNotEmpty(filter.getAccountCode())){
			 
			query.setParameter("accountCode",filter.getAccountCode() );
		}
		if (Validations.validateIsNotNullAndPositive(filter.getAccountType())){
			 
			query.setParameter("accountType",filter.getAccountType() );
		}
		if (Validations.validateIsNotNullAndPositive(filter.getNatureType())){
			 
			query.setParameter("natureType",filter.getNatureType() );
		}
		if (Validations.validateIsNotNullAndPositive(filter.getAdjustmentType())){
			 
			query.setParameter("adjustmentType",filter.getAdjustmentType() );
		}
		if (Validations.validateIsNotNullAndPositive(filter.getAuxiliaryType())){
			 
			query.setParameter("auxiliaryType", filter.getAuxiliaryType());
		}
		if (Validations.validateIsNotNullAndPositive(filter.getStatus())){
			 
			query.setParameter("status", filter.getStatus());
		}
		if (Validations.validateIsNotNullAndPositive(filter.getIndEntityAccount())){
			 
			query.setParameter("indEntityAccount", filter.getIndEntityAccount());
		}
		if (Validations.validateIsNotNullAndPositive(filter.getEntityAccount())){
			 
			query.setParameter("entityAccount", filter.getEntityAccount());
		}
		
		List<AccountingAccount> listAccountingAccount=null;
		try{
			listAccountingAccount=query.getResultList();
		}
		 catch (Exception e) {
			log.debug(e.getMessage());
		}
		
		
		return listAccountingAccount;
		
	}
	
	/**
	 * Find accounting account by filter.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<AccountingAccount>  findAccountingAccountByFilter(AccountingAccount filter)throws ServiceException{
		StringBuilder stringBuilderSql = new StringBuilder();	
		
		
		// AccountingAccount full Constructor
		stringBuilderSql.append("Select new AccountingAccount(aa.idAccountingAccountPk, aas , aa.accountCode ,   ");
		stringBuilderSql.append("  aa.description, aa.accountType, aa.natureType, aa.currency, aa.portFolio, ");
		stringBuilderSql.append("  aa.indAccumulation,  aa.indBalanceControl, aa.indDimension,  ");
		stringBuilderSql.append("  aa.indReference, aa.expiredDate, aa.adjustmentType,  ");
		stringBuilderSql.append("  aa.auxiliaryType,  aa.status, aa.accountingType,  aa.indSubAccount,   ");
		stringBuilderSql.append("  aa.indEntityAccount, aa.entityAccount, aa.instrumentType, aa.registerDate,  ");
		stringBuilderSql.append("  aa.registerUser, aa.updateDate, aa.updateUser, aa.cancelDate,  aa.cancelUser, ");
		stringBuilderSql.append("  (select (ap.parameterName||' - '||ap.description) as parameterDescriptionAccountType from AccountingParameter ap where ap.idAccountingParameterPk = aa.accountType),  ");
		stringBuilderSql.append("  (select (ap.parameterName||' - '||ap.description) as parameterDescriptionAuxiliaryType from AccountingParameter ap where ap.idAccountingParameterPk =aa.auxiliaryType),  ");
		stringBuilderSql.append("  (select (ap.parameterName) as parameterDescriptionNatureType from AccountingParameter ap where ap.idAccountingParameterPk =aa.natureType),  ");
		stringBuilderSql.append("  (select p.parameterName from ParameterTable p where p.parameterTablePk = aa.currency) , ");
		stringBuilderSql.append("  (select p.parameterName from ParameterTable p where p.parameterTablePk = aa.status)  , ");
		stringBuilderSql.append("  (select (ap.parameterName||' - '||ap.description) as parameterDescriptionAccountType from AccountingParameter ap where ap.idAccountingParameterPk = aa.entityAccount), ");
		stringBuilderSql.append("  (select p.parameterName from ParameterTable p where p.parameterTablePk = aa.instrumentType) ) ");
		stringBuilderSql.append(" From AccountingAccount aa    ");
		stringBuilderSql.append("  left outer join  aa.accountingAccount aas ");

		stringBuilderSql.append(" WHERE 1=1   ");	
		if (Validations.validateIsNotNullAndNotEmpty(filter.getIdAccountingAccountPk())){
			 
			stringBuilderSql.append(" and aa.idAccountingAccountPk = :idAccountingAccountPk  ");
		}
		if (Validations.validateIsNotNullAndNotEmpty(filter.getAccountCode())){
			 
			stringBuilderSql.append(" and aa.accountCode = :accountCode  ");
		}
		if (Validations.validateIsNotNullAndPositive(filter.getAccountType())){
			 
			stringBuilderSql.append(" and aa.accountType = :accountType ");
		}
		if (Validations.validateIsNotNullAndPositive(filter.getNatureType())){
			 
			stringBuilderSql.append(" and aa.natureType = :natureType  ");
		}
		if (Validations.validateIsNotNullAndPositive(filter.getAdjustmentType())){
			 
			stringBuilderSql.append(" and aa.adjustmentType = :adjustmentType  ");
		}
		if (Validations.validateIsNotNullAndPositive(filter.getAuxiliaryType())){
			 
			stringBuilderSql.append(" and aa.auxiliaryType = :auxiliaryType  ");
		}
		if (Validations.validateIsNotNullAndPositive(filter.getStatus())){
			 
			stringBuilderSql.append(" and aa.status  = :status  ");
		}
		if (Validations.validateIsNotNullAndPositive(filter.getIndEntityAccount())){
			 
			stringBuilderSql.append(" and aa.indEntityAccount  = :indEntityAccount  ");
		}
		if (Validations.validateIsNotNullAndPositive(filter.getEntityAccount())){
			 
			stringBuilderSql.append(" and aa.entityAccount  = :entityAccount  "); 
		}
		
		stringBuilderSql.append(" ORDER BY aa.accountCode, aa.natureType, aa.accountType, aa.auxiliaryType  ");
		
		Query query = em.createQuery(stringBuilderSql.toString());

		if (Validations.validateIsNotNullAndNotEmpty(filter.getIdAccountingAccountPk())){
			 
			query.setParameter("idAccountingAccountPk", filter.getIdAccountingAccountPk());
		}
		if (Validations.validateIsNotNullAndNotEmpty(filter.getAccountCode())){
			 
			query.setParameter("accountCode",filter.getAccountCode() );
		}
		if (Validations.validateIsNotNullAndPositive(filter.getAccountType())){
			 
			query.setParameter("accountType",filter.getAccountType() );
		}
		if (Validations.validateIsNotNullAndPositive(filter.getNatureType())){
			 
			query.setParameter("natureType",filter.getNatureType() );
		}
		if (Validations.validateIsNotNullAndPositive(filter.getAdjustmentType())){
			 
			query.setParameter("adjustmentType",filter.getAdjustmentType() );
		}
		if (Validations.validateIsNotNullAndPositive(filter.getAuxiliaryType())){
			 
			query.setParameter("auxiliaryType", filter.getAuxiliaryType());
		}
		if (Validations.validateIsNotNullAndPositive(filter.getStatus())){
			 
			query.setParameter("status", filter.getStatus());
		}
		if (Validations.validateIsNotNullAndPositive(filter.getIndEntityAccount())){
			 
			query.setParameter("indEntityAccount", filter.getIndEntityAccount());
		}
		if (Validations.validateIsNotNullAndPositive(filter.getEntityAccount())){
			 
			query.setParameter("entityAccount", filter.getEntityAccount());
		}
		
		List<AccountingAccount> listAccountingAccount=null;
		try{
			listAccountingAccount=query.getResultList();
		}
		 catch (Exception e) {
			log.debug(e.getMessage());
		}
		
		
		return listAccountingAccount;
		
	}
	
	/**
	 * Find accounting process with accunting receipt.
	 *
	 * @param accountingProcessTo the accounting process to
	 * @return the list
	 */
	public List<AccountingProcess> findAccountingProcessWithAccuntingReceipt(AccountingProcessTo accountingProcessTo ){

		log.info(" GETTING ALL ACCOUNTING RECEIPT WITH DETAILS AND LOGGERS ");
		
        StringBuilder sbQuery = new StringBuilder();
        sbQuery.append(" Select distinct ap From AccountingProcess ap left join fetch ap.accountingReceipt ar  " );
        sbQuery.append(" left join  ar.accountingReceiptDetail ard left join ard.accountingSourceLogger asl  " );
        sbQuery.append(" left join  asl.accountingSourceDetail asd  " );
        sbQuery.append(" where 1=1 " );
        
        if(Validations.validateIsNotNull(accountingProcessTo.getProcessType())){
        
            sbQuery.append(" and   ap.processType = :processType ");
        }
        
        if(Validations.validateIsNotNull(accountingProcessTo.getExecutionDate() )){
            
            sbQuery.append(" and  trunc(ap.executionDate) = :executionDate ");
        }
        
        
        Query query = em.createQuery(sbQuery.toString());
        if(Validations.validateIsNotNull(accountingProcessTo.getProcessType())){
        
            query.setParameter("processType", accountingProcessTo.getProcessType());
        }
        
        if(Validations.validateIsNotNull(accountingProcessTo.getExecutionDate() )){
            
        	query.setParameter("executionDate",  accountingProcessTo.getExecutionDate(),TemporalType.DATE);
        }
        
        List<AccountingProcess> accountingProcessList = null;
        accountingProcessList = query.getResultList();
        
        return accountingProcessList;
              
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> findFileContentWithNativeQuery(String securityCode, Integer cuponNumber, Integer indCupon) {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT pd.FILENAME, pd.DOCUMENT_FILE ");
		sb.append(" FROM PRADERA_DEPOSITARY.PAYROLL_HEADER ph ");
		sb.append(" JOIN PRADERA_DEPOSITARY.PAYROLL_DETAIL pd ");
		sb.append(" ON ph.ID_PAYROLL_HEADER_PK = pd.ID_PAYROLL_HEADER_FK ");
		sb.append(" JOIN PRADERA_DEPOSITARY.ACCOUNT_ANNOTATION_OPERATION aao ");
		sb.append(" ON aao.ID_ANNOTATION_OPERATION_PK = pd.ID_ANNOTATION_OPERATION_FK ");
		sb.append(" JOIN PRADERA_DEPOSITARY.SECURITY s ");
		sb.append(" ON s.ID_SECURITY_CODE_PK = aao.ID_SECURITY_CODE_FK  ");
		sb.append(" AND s.ID_SECURITY_CODE_PK = :securityCode ");
		if (cuponNumber != null) {
			sb.append(" AND pd.CUPON_NUMBER = :cuponNumber ");
		}
		sb.append(" WHERE pd.IND_CUPON = :indCupon ");
		Query query = em.createNativeQuery(sb.toString());
		query.setParameter("indCupon", indCupon);
		query.setParameter("securityCode", securityCode);
		if (cuponNumber != null) {
			query.setParameter("cuponNumber", cuponNumber);
		}
		return query.getResultList();
	}

	public Boolean securityHasFile(String securityCode, Integer cuponNumber, Integer indCupon) {
		StringBuilder sb = new StringBuilder();
		
		sb.append(" SELECT COUNT(1) ");
		sb.append(" FROM PayrollHeader ph ");
		sb.append(" JOIN ph.lstPayrollDetail pd ");
		sb.append(" JOIN pd.accountAnnotationOperation aao ");
		sb.append(" join aao.security sec ");
		sb.append(" where sec.idSecurityCodePk = :idSecurityCodePk ");
		if (cuponNumber != null) {
			sb.append(" AND pd.cuponNumber = :cuponNumber ");
		}
		sb.append(" AND pd.indCupon = :indCupon ");
		Query query = em.createQuery(sb.toString());
		query.setParameter("indCupon", indCupon);
		query.setParameter("idSecurityCodePk", securityCode);
		if (cuponNumber != null) {
			query.setParameter("cuponNumber", cuponNumber);
		}
		
		try {
			Long count = (Long) query.getSingleResult();
			
			if(Validations.validateIsNotNullAndPositive(count)) {
				return Boolean.TRUE;
			} else {
				return Boolean.FALSE;
			}			
		} catch (NoResultException e) {
			return Boolean.FALSE;
		}
		
	}
	
}
