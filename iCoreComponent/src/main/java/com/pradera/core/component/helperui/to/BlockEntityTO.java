package com.pradera.core.component.helperui.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.model.generalparameter.ParameterTable;

public class BlockEntityTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3392761852559476492L;
	private String docNumber;
	private String docType;
	private List<ParameterTable> lstDocType;
	private Long idHolderPk;
	private Long idBlockEntityPk;
	private String name;
	private String fullName;
	private String firstLastName;
	private String secondLastName;
	private String state;
	private Integer personType;
	
	
	public String getDocNumber() {
		return docNumber;
	}
	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}
	public Long getIdHolderPk() {
		return idHolderPk;
	}
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}
	public Long getIdBlockEntityPk() {
		return idBlockEntityPk;
	}
	public void setIdBlockEntityPk(Long idBlockEntityPk) {
		this.idBlockEntityPk = idBlockEntityPk;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getDocType() {
		return docType;
	}
	public void setDocType(String docType) {
		this.docType = docType;
	}
	public List<ParameterTable> getLstDocType() {
		return lstDocType;
	}
	public void setLstDocType(List<ParameterTable> lstDocType) {
		this.lstDocType = lstDocType;
	}
	
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getFirstLastName() {
		return firstLastName;
	}
	public void setFirstLastName(String firstLastName) {
		this.firstLastName = firstLastName;
	}
	public String getSecondLastName() {
		return secondLastName;
	}
	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}
	public Integer getPersonType() {
		return personType;
	}
	public void setPersonType(Integer personType) {
		this.personType = personType;
	}
	
	
}
