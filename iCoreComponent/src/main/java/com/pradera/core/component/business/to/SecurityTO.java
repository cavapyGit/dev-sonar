package com.pradera.core.component.business.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class SecurityTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idIssuerCodePk;
	private String idIssuanceCodePk;
	private String idIsinCodePk;
	private String idCfiCodePk;
	private String alternativeCode;
	private String descriptionSecurity;
	private String securityState;
	private Integer instrumentType;
	private Integer classType;
	private Integer stateSecurity;
	private String idSecurityCodePk;
	private String idSecurityCodeOnly;
	private Integer securityCurrency;
	private Date issuanceDate;
	private Date expirationDate;
	private Date selectDate;
	private Integer securityType;
	private Integer securityClass;
	private Integer indIsCoupon;
	private Integer indIsDetached;
	private Integer indicator1;
	private List<Integer> lstSecurityClass;
	private List<Long> lstHolderAccountPk;
	private Long idParticipant;
	private String idFractionSecurityCodePk;
	private Date issuanceDateMax;
	
	// indicador de NO Negociabilidad
	private Integer notTraded;
	
	private Date initialDate;
	private Date finalDate;
	
	private Long idHolderPk;
	private Long idHolderAccountPk;
	
	/*Consult Security*/
	private BigDecimal amount;
	private BigDecimal rate;
	private BigDecimal term;
	
	/*Expiration Overdue*/
	/**The Security Code Excluded Filters*/
	private String excludedSecurityCode;
	/**The Security Class Excluded Filters*/
	private String excludedSecurityClass;
	/**The List Excluded security Code*/
	private List<String> listExcludedSecurityCode;
	/**The List Excluded Security Class*/
	private List<Integer> listExcludedSecurityClass;
	
	
	/*Generation File TR CR*/
	private List<String> listSecurityCode;
	
	
	public String getIdSecurityCodeOnly() {
		return idSecurityCodeOnly;
	}
	public void setIdSecurityCodeOnly(String idSecurityCodeOnly) {
		this.idSecurityCodeOnly = idSecurityCodeOnly;
	}
	public Integer getStateSecurity() {
		return stateSecurity;
	}
	public void setStateSecurity(Integer stateSecurity) {
		this.stateSecurity = stateSecurity;
	}
	public Integer getClassType() {
		return classType;
	}
	public void setClassType(Integer classType) {
		this.classType = classType;
	}
	public Integer getInstrumentType() {
		return instrumentType;
	}
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}
	public String getIdIssuerCodePk() {
		return idIssuerCodePk;
	}
	public void setIdIssuerCodePk(String idIssuerCodePk) {
		this.idIssuerCodePk = idIssuerCodePk;
	}
	public String getIdIssuanceCodePk() {
		return idIssuanceCodePk;
	}
	public void setIdIssuanceCodePk(String idIssuanceCodePk) {
		this.idIssuanceCodePk = idIssuanceCodePk;
	}
	public String getIdIsinCodePk() {
		return idIsinCodePk;
	}
	public void setIdIsinCodePk(String idIsinCodePk) {
		this.idIsinCodePk = idIsinCodePk;
	}
	public String getIdCfiCodePk() {
		return idCfiCodePk;
	}
	public void setIdCfiCodePk(String idCfiCodePk) {
		this.idCfiCodePk = idCfiCodePk;
	}
	public String getDescriptionSecurity() {
		return descriptionSecurity;
	}
	public void setDescriptionSecurity(String descriptionSecurity) {
		this.descriptionSecurity = descriptionSecurity;
	}
	public String getSecurityState() {
		return securityState;
	}
	public void setSecurityState(String securityState) {
		this.securityState = securityState;
	}
	public String getAlternativeCode() {
		return alternativeCode;
	}
	public void setAlternativeCode(String alternativeCode) {
		this.alternativeCode = alternativeCode;
	}
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
	
	public Integer getSecurityCurrency() {
		return securityCurrency;
	}
	public void setSecurityCurrency(Integer securityCurrency) {
		this.securityCurrency = securityCurrency;
	}
	
	
	public Date getIssuanceDate() {
		return issuanceDate;
	}
	public void setIssuanceDate(Date issuanceDate) {
		this.issuanceDate = issuanceDate;
	}
	public Date getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	
	
	public Integer getSecurityType() {
		return securityType;
	}
	public void setSecurityType(Integer securityType) {
		this.securityType = securityType;
	}
	
	public Integer getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}
	
	
	
	public Date getSelectDate() {
		return selectDate;
	}
	public void setSelectDate(Date selectDate) {
		this.selectDate = selectDate;
	}
	public Integer getIndIsCoupon() {
		return indIsCoupon;
	}
	public void setIndIsCoupon(Integer indIsCoupon) {
		this.indIsCoupon = indIsCoupon;
	}
	public Integer getIndIsDetached() {
		return indIsDetached;
	}
	public void setIndIsDetached(Integer indIsDetached) {
		this.indIsDetached = indIsDetached;
	}
	@Override
	public String toString() {
		return "SecurityTO [idIssuerCodePk=" + idIssuerCodePk
				+ ", idIssuanceCodePk=" + idIssuanceCodePk + ", idIsinCodePk="
				+ idIsinCodePk + ", idCfiCodePk=" + idCfiCodePk
				+ ", alternativeCode=" + alternativeCode
				+ ", descriptionSecurity=" + descriptionSecurity
				+ ", securityState=" + securityState + ", instrumentType="
				+ instrumentType + ", classType=" + classType
				+ ", stateSecurity=" + stateSecurity + ", idSecurityCodePk="
				+ idSecurityCodePk + "]";
	}
	public Integer getIndicator1() {
		return indicator1;
	}
	public void setIndicator1(Integer indicator1) {
		this.indicator1 = indicator1;
	}
	public List<Integer> getLstSecurityClass() {
		return lstSecurityClass;
	}
	public void setLstSecurityClass(List<Integer> lstSecurityClass) {
		this.lstSecurityClass = lstSecurityClass;
	}
	public List<Long> getLstHolderAccountPk() {
		return lstHolderAccountPk;
	}
	public void setLstHolderAccountPk(List<Long> lstHolderAccountPk) {
		this.lstHolderAccountPk = lstHolderAccountPk;
	}
	public Long getIdParticipant() {
		return idParticipant;
	}
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}
	/**
	 * @return the initialDate
	 */
	public Date getInitialDate() {
		return initialDate;
	}
	/**
	 * @param initialDate the initialDate to set
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	/**
	 * @return the finalDate
	 */
	public Date getFinalDate() {
		return finalDate;
	}
	/**
	 * @param finalDate the finalDate to set
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}
	/**
	 * @return the idHolderPk
	 */
	public Long getIdHolderPk() {
		return idHolderPk;
	}
	/**
	 * @param idHolderPk the idHolderPk to set
	 */
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}
	/**
	 * @return the idHolderAccountPk
	 */
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}
	/**
	 * @param idHolderAccountPk the idHolderAccountPk to set
	 */
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}
	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	/**
	 * @return the rate
	 */
	public BigDecimal getRate() {
		return rate;
	}
	/**
	 * @param rate the rate to set
	 */
	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}
	/**
	 * @return the term
	 */
	public BigDecimal getTerm() {
		return term;
	}
	/**
	 * @param term the term to set
	 */
	public void setTerm(BigDecimal term) {
		this.term = term;
	}
	/**
	 * @return the excludedSecurityCode
	 */
	public String getExcludedSecurityCode() {
		return excludedSecurityCode;
	}
	/**
	 * @param excludedSecurityCode the excludedSecurityCode to set
	 */
	public void setExcludedSecurityCode(String excludedSecurityCode) {
		this.excludedSecurityCode = excludedSecurityCode;
	}
	/**
	 * @return the excludedSecurityClass
	 */
	public String getExcludedSecurityClass() {
		return excludedSecurityClass;
	}
	/**
	 * @param excludedSecurityClass the excludedSecurityClass to set
	 */
	public void setExcludedSecurityClass(String excludedSecurityClass) {
		this.excludedSecurityClass = excludedSecurityClass;
	}
	/**
	 * @return the listExcludedSecurityCode
	 */
	public List<String> getListExcludedSecurityCode() {
		return listExcludedSecurityCode;
	}
	/**
	 * @param listExcludedSecurityCode the listExcludedSecurityCode to set
	 */
	public void setListExcludedSecurityCode(List<String> listExcludedSecurityCode) {
		this.listExcludedSecurityCode = listExcludedSecurityCode;
	}
	/**
	 * @return the listExcludedSecurityClass
	 */
	public List<Integer> getListExcludedSecurityClass() {
		return listExcludedSecurityClass;
	}
	/**
	 * @param listExcludedSecurityClass the listExcludedSecurityClass to set
	 */
	public void setListExcludedSecurityClass(List<Integer> listExcludedSecurityClass) {
		this.listExcludedSecurityClass = listExcludedSecurityClass;
	}
	/**
	 * Generation File TR CR
	 * @return the listSecurityCode
	 */
	public List<String> getListSecurityCode() {
		return listSecurityCode;
	}
	/**
	 * Generation File TR CR
	 * @param listSecurityCode the listSecurityCode to set
	 */
	public void setListSecurityCode(List<String> listSecurityCode) {
		this.listSecurityCode = listSecurityCode;
	}
	
	public Integer getNotTraded() {
		return notTraded;
	}
	
	public void setNotTraded(Integer notTraded) {
		this.notTraded = notTraded;
	}
	public String getIdFractionSecurityCodePk() {
		return idFractionSecurityCodePk;
	}
	public void setIdFractionSecurityCodePk(String idFractionSecurityCodePk) {
		this.idFractionSecurityCodePk = idFractionSecurityCodePk;
	}
	/**
	 * @return the issuanceDateMax
	 */
	public Date getIssuanceDateMax() {
		return issuanceDateMax;
	}
	/**
	 * @param issuanceDateMax the issuanceDateMax to set
	 */
	public void setIssuanceDateMax(Date issuanceDateMax) {
		this.issuanceDateMax = issuanceDateMax;
	}
	
}
