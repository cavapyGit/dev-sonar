package com.pradera.core.component.corporateevents.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class CorporativeOperationTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String sourceIsinCode;
	private String targetIsinCode;
	private Boolean needTargetSecurity =Boolean.FALSE;
	private Integer state;
	private Integer indRemanent;
	private String issuerCode;
	private Integer corporativeEventType;
	private Date cutOffDate;
	private Date registryDate;
	private Date deliveryDate;
	private Integer corporativeEventSate;
	private Integer instrumentType;
	private Integer firstState;
	private Integer secondState;
	private List<Integer> stateList;
	private Integer periodType;
	private Long idParticipantPk;
	private Long participantId;
	private Integer reportType;
	private Integer signType;
	private Long rnt;
	private Long holderAccount;
	private Long stockProcess;
	public Long getStockProcess() {
		return stockProcess;
	}
	public void setStockProcess(Long stockProcess) {
		this.stockProcess = stockProcess;
	}
	public Long getHolderAccount() {
		return holderAccount;
	}
	public void setHolderAccount(Long holderAccount) {
		this.holderAccount = holderAccount;
	}
	public Long getRnt() {
		return rnt;
	}
	public void setRnt(Long rnt) {
		this.rnt = rnt;
	}
	public Long getParticipantId() {
		return participantId;
	}
	public void setParticipantId(Long participantId) {
		this.participantId = participantId;
	}
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	public Integer getPeriodType() {
		return periodType;
	}
	public void setPeriodType(Integer periodType) {
		this.periodType = periodType;
	}
	public Integer getStockType() {
		return stockType;
	}
	public void setStockType(Integer stockType) {
		this.stockType = stockType;
	}
	public Integer getStockState() {
		return stockState;
	}
	public void setStockState(Integer stockState) {
		this.stockState = stockState;
	}
	public Integer getStockClass() {
		return stockClass;
	}
	public void setStockClass(Integer stockClass) {
		this.stockClass = stockClass;
	}
	private List<Integer> eventTypeList;
	private Integer stockType;
	private Integer stockState;
	private Integer stockClass;
	
	
	public List<Integer> getStateList() {
		return stateList;
	}
	public void setStateList(List<Integer> stateList) {
		this.stateList = stateList;
	}
	public List<Integer> getEventTypeList() {
		return eventTypeList;
	}
	public void setEventTypeList(List<Integer> eventTypeList) {
		this.eventTypeList = eventTypeList;
	}
	public Integer getFirstState() {
		return firstState;
	}
	public void setFirstState(Integer firstState) {
		this.firstState = firstState;
	}
	public Integer getSecondState() {
		return secondState;
	}
	public void setSecondState(Integer secondState) {
		this.secondState = secondState;
	}
	public Integer getInstrumentType() {
		return instrumentType;
	}
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}
	public Boolean getNeedTargetSecurity() {
		return needTargetSecurity;
	}
	public void setNeedTargetSecurity(Boolean needTargetSecurity) {
		this.needTargetSecurity = needTargetSecurity;
	}	
	
	public String getIssuerCode() {
		return issuerCode;
	}
	public void setIssuerCode(String issuerCode) {
		this.issuerCode = issuerCode;
	}
	public Integer getCorporativeEventType() {
		return corporativeEventType;
	}
	public void setCorporativeEventType(Integer corporativeEventType) {
		this.corporativeEventType = corporativeEventType;
	}
	public Date getCutOffDate() {
		return cutOffDate;
	}
	public void setCutOffDate(Date cutOffDate) {
		this.cutOffDate = cutOffDate;
	}
	public Date getRegistryDate() {
		return registryDate;
	}
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public Integer getCorporativeEventSate() {
		return corporativeEventSate;
	}
	public void setCorporativeEventSate(Integer corporativeEventSate) {
		this.corporativeEventSate = corporativeEventSate;
	}
	public Integer getCorporativeEvent() {
		return corporativeEvent;
	}
	public void setCorporativeEvent(Integer corporativeEvent) {
		this.corporativeEvent = corporativeEvent;
	}
	private Integer corporativeEvent;
	
	

	
	
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public Integer getIndRemanent() {
		return indRemanent;
	}
	public void setIndRemanent(Integer indRemanent) {
		this.indRemanent = indRemanent;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTargetIsinCode() {
		return targetIsinCode;
	}
	public void setTargetIsinCode(String targetIsinCode) {
		this.targetIsinCode = targetIsinCode;
	}
	public String getSourceIsinCode() {
		return sourceIsinCode;
	}
	public void setSourceIsinCode(String sourceIsinCode) {
		this.sourceIsinCode = sourceIsinCode;
	}
	public Integer getReportType() {
		return reportType;
	}
	public void setReportType(Integer reportType) {
		this.reportType = reportType;
	}
	public Integer getSignType() {
		return signType;
	}
	public void setSignType(Integer signType) {
		this.signType = signType;
	}

}
