package com.pradera.core.component.helperui.to;

import java.io.Serializable;

/*
 *  CREATED BY MARTIN ZARATE RAFAEL
 */

//GETS RESULTS OF ISSUANCES SEARCH
public class IssuanceResultTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2359480242009240621L;
	private String issuanceCode;
	private String issuanceDesc;
	private String issuerCode;
	private String issuerDesc;
	private String instrumentTypeDesc;
	private String securityTypeDesc;
	private String securityClass;
	private String issuanceStatusDesc;

	public String getIssuanceCode() {
		return issuanceCode;
	}
	public void setIssuanceCode(String issuanceCode) {
		this.issuanceCode = issuanceCode;
	}
	public String getIssuanceDesc() {
		return issuanceDesc;
	}
	public void setIssuanceDesc(String issuanceDesc) {
		this.issuanceDesc = issuanceDesc;
	}
	public String getIssuerCode() {
		return issuerCode;
	}
	public void setIssuerCode(String issuerCode) {
		this.issuerCode = issuerCode;
	}
	public String getIssuerDesc() {
		return issuerDesc;
	}
	public void setIssuerDesc(String issuerDesc) {
		this.issuerDesc = issuerDesc;
	}
	public String getInstrumentTypeDesc() {
		return instrumentTypeDesc;
	}
	public void setInstrumentTypeDesc(String instrumentTypeDesc) {
		this.instrumentTypeDesc = instrumentTypeDesc;
	}
	public String getSecurityTypeDesc() {
		return securityTypeDesc;
	}
	public void setSecurityTypeDesc(String securityTypeDesc) {
		this.securityTypeDesc = securityTypeDesc;
	}
	public String getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(String securityClass) {
		this.securityClass = securityClass;
	}
	public String getIssuanceStatusDesc() {
		return issuanceStatusDesc;
	}
	public void setIssuanceStatusDesc(String issuanceStatusDesc) {
		this.issuanceStatusDesc = issuanceStatusDesc;
	}
}
