package com.pradera.core.component.corporateevents.to;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class CorporativeOperationSubQueryTO.
 */
public class CorporativeOperationSubQueryTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The corporative operation id. */
	private Long corporativeOperationId;
	
	/** The corporative process result id. */
	private Long corporativeProcessResultId;
	
	/** The participant id. */
	private Long participantId;
	
	/** The holder account id. */
	private Long holderAccountId;
	
	/**
	 * Gets the corporative operation id.
	 *
	 * @return the corporative operation id
	 */
	public Long getCorporativeOperationId() {
		return corporativeOperationId;
	}
	
	/**
	 * Sets the corporative operation id.
	 *
	 * @param corporativeOperationId the new corporative operation id
	 */
	public void setCorporativeOperationId(Long corporativeOperationId) {
		this.corporativeOperationId = corporativeOperationId;
	}
	
	/**
	 * Gets the corporative process result id.
	 *
	 * @return the corporative process result id
	 */
	public Long getCorporativeProcessResultId() {
		return corporativeProcessResultId;
	}
	
	/**
	 * Sets the corporative process result id.
	 *
	 * @param corporativeProcessResultId the new corporative process result id
	 */
	public void setCorporativeProcessResultId(Long corporativeProcessResultId) {
		this.corporativeProcessResultId = corporativeProcessResultId;
	}
	
	/**
	 * Gets the participant id.
	 *
	 * @return the participant id
	 */
	public Long getParticipantId() {
		return participantId;
	}
	
	/**
	 * Sets the participant id.
	 *
	 * @param participantId the new participant id
	 */
	public void setParticipantId(Long participantId) {
		this.participantId = participantId;
	}
	
	/**
	 * Gets the holder account id.
	 *
	 * @return the holder account id
	 */
	public Long getHolderAccountId() {
		return holderAccountId;
	}
	
	/**
	 * Sets the holder account id.
	 *
	 * @param holderAccountId the new holder account id
	 */
	public void setHolderAccountId(Long holderAccountId) {
		this.holderAccountId = holderAccountId;
	}
	
	/**
	 * Gets the serialversionuid.
	 *
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
