package com.pradera.core.component.accounts.to;

import java.io.Serializable;

import com.pradera.model.accounts.HolderRequest;

public class LegalRepresentativeTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	private Integer viewOperationType;
	private String representativeFileNameDisplay;
	private Integer attachFileRepresentativeSelected;
	private boolean disabledCmbPostalMunicipalityRepresentative;
	private boolean disabledCountryResidentRepresentative;
	private boolean disabledRepresentativeResident;
	private boolean disabledInputPostalAddressRepresentative;
	private boolean disabledCmbPostalDepartamentRepresentative;
	private boolean disabledCmbPostalProvinceRepresentative;
	private boolean disabledCmbPostalCountryRepresentative;
	private boolean disabledRepresentativeNationality;
	private boolean flagCopyLegalRepresentative;
	private boolean flagRepresentativeNaturalPerson;
	private boolean flagRepresentativeJuridicPerson;
	private boolean flagRepresentativeIsPEP;
	private boolean flagAddRepresentativeModify;
	private boolean flagModifyRepresentativeModify;
	private boolean flagCmbRepresentativeClass;
	private Integer holderPersonType;
	private Integer holderJuridicClassType;
	private Integer maxLenghtDocumentNumberLegal;
	private boolean numericTypeDocumentLegal;
	private Integer maxLenghtSecondDocumentNumberLegal;
	private boolean numericSecondTypeDocumentLegal;
	private boolean flagIndicatorMinorHolder;
	private boolean flagHolderIndicatorInability;
	private boolean flagExportDataHolder;
	private boolean flagExportDataRepresentative;
	private HolderRequest holderRequest;
	private boolean flagActionModify;
	private String idFormClient;
	private boolean disabledPersonTypeRepresentative;
	private Integer rowIndicatorLegalRepresentative;
	private boolean flagIssuerOrParticipant;
	private Integer documentType;
	private String  documentNumber;
	private Integer emissionSource;
	private String firstLastName;
	
	public Integer getAttachFileRepresentativeSelected() {
		return attachFileRepresentativeSelected;
	}
	public void setAttachFileRepresentativeSelected(
			Integer attachFileRepresentativeSelected) {
		this.attachFileRepresentativeSelected = attachFileRepresentativeSelected;
	}
	public Integer getViewOperationType() {
		return viewOperationType;
	}
	public void setViewOperationType(Integer viewOperationType) {
		this.viewOperationType = viewOperationType;
	}
	public boolean isFlagAddRepresentativeModify() {
		return flagAddRepresentativeModify;
	}
	public void setFlagAddRepresentativeModify(boolean flagAddRepresentativeModify) {
		this.flagAddRepresentativeModify = flagAddRepresentativeModify;
	}
	public boolean isFlagModifyRepresentativeModify() {
		return flagModifyRepresentativeModify;
	}
	public void setFlagModifyRepresentativeModify(
			boolean flagModifyRepresentativeModify) {
		this.flagModifyRepresentativeModify = flagModifyRepresentativeModify;
	}
	public String getRepresentativeFileNameDisplay() {
		return representativeFileNameDisplay;
	}
	public void setRepresentativeFileNameDisplay(
			String representativeFileNameDisplay) {
		this.representativeFileNameDisplay = representativeFileNameDisplay;
	}
	public boolean isDisabledCmbPostalMunicipalityRepresentative() {
		return disabledCmbPostalMunicipalityRepresentative;
	}
	public void setDisabledCmbPostalMunicipalityRepresentative(
			boolean disabledCmbPostalMunicipalityRepresentative) {
		this.disabledCmbPostalMunicipalityRepresentative = disabledCmbPostalMunicipalityRepresentative;
	}
	public boolean isDisabledCountryResidentRepresentative() {
		return disabledCountryResidentRepresentative;
	}
	public void setDisabledCountryResidentRepresentative(
			boolean disabledCountryResidentRepresentative) {
		this.disabledCountryResidentRepresentative = disabledCountryResidentRepresentative;
	}
	public boolean isDisabledRepresentativeResident() {
		return disabledRepresentativeResident;
	}
	public void setDisabledRepresentativeResident(
			boolean disabledRepresentativeResident) {
		this.disabledRepresentativeResident = disabledRepresentativeResident;
	}
	public boolean isFlagCopyLegalRepresentative() {
		return flagCopyLegalRepresentative;
	}
	public void setFlagCopyLegalRepresentative(boolean flagCopyLegalRepresentative) {
		this.flagCopyLegalRepresentative = flagCopyLegalRepresentative;
	}
	public boolean isDisabledInputPostalAddressRepresentative() {
		return disabledInputPostalAddressRepresentative;
	}
	public void setDisabledInputPostalAddressRepresentative(
			boolean disabledInputPostalAddressRepresentative) {
		this.disabledInputPostalAddressRepresentative = disabledInputPostalAddressRepresentative;
	}
	public boolean isDisabledCmbPostalDepartamentRepresentative() {
		return disabledCmbPostalDepartamentRepresentative;
	}
	public void setDisabledCmbPostalDepartamentRepresentative(
			boolean disabledCmbPostalDepartamentRepresentative) {
		this.disabledCmbPostalDepartamentRepresentative = disabledCmbPostalDepartamentRepresentative;
	}
	public boolean isDisabledCmbPostalProvinceRepresentative() {
		return disabledCmbPostalProvinceRepresentative;
	}
	public void setDisabledCmbPostalProvinceRepresentative(
			boolean disabledCmbPostalProvinceRepresentative) {
		this.disabledCmbPostalProvinceRepresentative = disabledCmbPostalProvinceRepresentative;
	}
	public boolean isDisabledCmbPostalCountryRepresentative() {
		return disabledCmbPostalCountryRepresentative;
	}
	public void setDisabledCmbPostalCountryRepresentative(
			boolean disabledCmbPostalCountryRepresentative) {
		this.disabledCmbPostalCountryRepresentative = disabledCmbPostalCountryRepresentative;
	}
	public boolean isFlagRepresentativeNaturalPerson() {
		return flagRepresentativeNaturalPerson;
	}
	public void setFlagRepresentativeNaturalPerson(
			boolean flagRepresentativeNaturalPerson) {
		this.flagRepresentativeNaturalPerson = flagRepresentativeNaturalPerson;
	}
	public boolean isFlagRepresentativeJuridicPerson() {
		return flagRepresentativeJuridicPerson;
	}
	public void setFlagRepresentativeJuridicPerson(
			boolean flagRepresentativeJuridicPerson) {
		this.flagRepresentativeJuridicPerson = flagRepresentativeJuridicPerson;
	}
	public boolean isFlagRepresentativeIsPEP() {
		return flagRepresentativeIsPEP;
	}
	public void setFlagRepresentativeIsPEP(boolean flagRepresentativeIsPEP) {
		this.flagRepresentativeIsPEP = flagRepresentativeIsPEP;
	}
	public boolean isDisabledRepresentativeNationality() {
		return disabledRepresentativeNationality;
	}
	public void setDisabledRepresentativeNationality(
			boolean disabledRepresentativeNationality) {
		this.disabledRepresentativeNationality = disabledRepresentativeNationality;
	}
	public boolean isFlagCmbRepresentativeClass() {
		return flagCmbRepresentativeClass;
	}
	public void setFlagCmbRepresentativeClass(boolean flagCmbRepresentativeClass) {
		this.flagCmbRepresentativeClass = flagCmbRepresentativeClass;
	}
	
	public Integer getHolderPersonType() {
		return holderPersonType;
	}
	public void setHolderPersonType(Integer holderPersonType) {
		this.holderPersonType = holderPersonType;
	}
	public Integer getHolderJuridicClassType() {
		return holderJuridicClassType;
	}
	public void setHolderJuridicClassType(Integer holderJuridicClassType) {
		this.holderJuridicClassType = holderJuridicClassType;
	}
	public Integer getMaxLenghtDocumentNumberLegal() {
		return maxLenghtDocumentNumberLegal;
	}
	public void setMaxLenghtDocumentNumberLegal(Integer maxLenghtDocumentNumberLegal) {
		this.maxLenghtDocumentNumberLegal = maxLenghtDocumentNumberLegal;
	}
	public boolean isNumericTypeDocumentLegal() {
		return numericTypeDocumentLegal;
	}
	public void setNumericTypeDocumentLegal(boolean numericTypeDocumentLegal) {
		this.numericTypeDocumentLegal = numericTypeDocumentLegal;
	}
	public Integer getMaxLenghtSecondDocumentNumberLegal() {
		return maxLenghtSecondDocumentNumberLegal;
	}
	public void setMaxLenghtSecondDocumentNumberLegal(
			Integer maxLenghtSecondDocumentNumberLegal) {
		this.maxLenghtSecondDocumentNumberLegal = maxLenghtSecondDocumentNumberLegal;
	}
	public boolean isNumericSecondTypeDocumentLegal() {
		return numericSecondTypeDocumentLegal;
	}
	public void setNumericSecondTypeDocumentLegal(
			boolean numericSecondTypeDocumentLegal) {
		this.numericSecondTypeDocumentLegal = numericSecondTypeDocumentLegal;
	}
	public boolean isFlagIndicatorMinorHolder() {
		return flagIndicatorMinorHolder;
	}
	public void setFlagIndicatorMinorHolder(boolean flagIndicatorMinorHolder) {
		this.flagIndicatorMinorHolder = flagIndicatorMinorHolder;
	}
	public boolean isFlagHolderIndicatorInability() {
		return flagHolderIndicatorInability;
	}
	public void setFlagHolderIndicatorInability(boolean flagHolderIndicatorInability) {
		this.flagHolderIndicatorInability = flagHolderIndicatorInability;
	}
	public boolean isFlagExportDataHolder() {
		return flagExportDataHolder;
	}
	public void setFlagExportDataHolder(boolean flagExportDataHolder) {
		this.flagExportDataHolder = flagExportDataHolder;
	}
	public boolean isFlagExportDataRepresentative() {
		return flagExportDataRepresentative;
	}
	public void setFlagExportDataRepresentative(boolean flagExportDataRepresentative) {
		this.flagExportDataRepresentative = flagExportDataRepresentative;
	}
	public HolderRequest getHolderRequest() {
		return holderRequest;
	}
	public void setHolderRequest(HolderRequest holderRequest) {
		this.holderRequest = holderRequest;
	}
	public boolean isFlagActionModify() {
		return flagActionModify;
	}
	public void setFlagActionModify(boolean flagActionModify) {
		this.flagActionModify = flagActionModify;
	}
	public String getIdFormClient() {
		return idFormClient;
	}
	public void setIdFormClient(String idFormClient) {
		this.idFormClient = idFormClient;
	}
	public boolean isDisabledPersonTypeRepresentative() {
		return disabledPersonTypeRepresentative;
	}
	public void setDisabledPersonTypeRepresentative(
			boolean disabledPersonTypeRepresentative) {
		this.disabledPersonTypeRepresentative = disabledPersonTypeRepresentative;
	}
	public Integer getRowIndicatorLegalRepresentative() {
		return rowIndicatorLegalRepresentative;
	}
	public void setRowIndicatorLegalRepresentative(
			Integer rowIndicatorLegalRepresentative) {
		this.rowIndicatorLegalRepresentative = rowIndicatorLegalRepresentative;
	}
	public boolean isFlagIssuerOrParticipant() {
		return flagIssuerOrParticipant;
	}
	public void setFlagIssuerOrParticipant(boolean flagIssuerOrParticipant) {
		this.flagIssuerOrParticipant = flagIssuerOrParticipant;
	}
	public Integer getDocumentType() {
		return documentType;
	}
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}
	public String getDocumentNumber() {
		return documentNumber;
	}
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	public Integer getEmissionSource() {
		return emissionSource;
	}
	public void setEmissionSource(Integer emissionSource) {
		this.emissionSource = emissionSource;
	}
	public String getFirstLastName() {
		return firstLastName;
	}
	public void setFirstLastName(String firstLastName) {
		this.firstLastName = firstLastName;
	}
		
}
