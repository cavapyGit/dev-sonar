package com.pradera.core.component.billing.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.billing.RateMovement;
import com.pradera.model.billing.ServiceRateScale;
import com.pradera.model.billing.type.RateType;
import com.pradera.model.component.MovementType;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ServiceRateTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01-sep-2015
 */
public class ServiceRateTo extends GenericBaseBean implements Serializable{
	
/** The Constant serialVersionUID. */
private static final long serialVersionUID = 1L;
	
	/**   business's model. */
	
	private Long 	idServiceRatePk;
	
	/** The rate code. */
	private Integer  rateCode;
	
	/** The rate name. */
	private String  rateName;
	
	/** The currency rate. */
	private Integer currencyRate;
	
	/** The rate status. */
	private Integer rateStatus;
	
	/** The new rate status. */
	private Integer newRateStatus;
	
	/** The end effective date. */
	private Date endEffectiveDate;
	
	/** The initial effective date. */
	private Date initialEffectiveDate;
	
	/** The last effective date. */
	private Date lastEffectiveDate;
	
	/** The applied tax selected. */
	private Integer appliedTaxSelected;
    
    /** The description rate. */
    private String descriptionRate;
    
    /** The description status. */
    private String descriptionStatus;
	
	/** The description type. */
	private String descriptionType;
	
	/** The description currency. */
	private String descriptionCurrency;
	
	/** The rate type. */
	private Integer rateType;
	
	/** The scale type. */
	private Integer scaleType;
	
	/** The id billing service. */
	private String  idBillingService;
	
	/** The service code. */
	private String    serviceCode;
	
	/** The selected. */
	private boolean selected;	
	
	/** The fields on dtd. */
	private String fieldsOnDtd;
	
	/**  only for Fixed. */
	private BigDecimal amountRate;
	
	/**  only for Percentage. */
	private BigDecimal percentageRate;
	
	/**  only for Staggered. */
	private Integer sequenceNumber;
	
	/** The list service rate scale to. */
	private List<ServiceRateScaleTo> listServiceRateScaleTo;
	
	/** The rate service scale to data model. */
	private RateServiceScaleToDataModel rateServiceScaleToDataModel;	
	
	/** The unit. */
	private Integer unit=1; 
	
	/** The disable seq number. */
	private Boolean disableSeqNumber;
	
	/** The disable btn generate. */
	private Boolean disableBtnGenerate;
	
	/** The disable unit. */
	private Boolean disableUnit;
	
	/** The min scale value. */
	private BigDecimal minScaleValue;
	
	/** The max scale value. */
	private BigDecimal maxScaleValue;
	
	/**  only for Movement. */
	private String descriptionMovementType;
	
	/** The code movement type. */
	private Long codeMovementType;
	
	/** The rate amount. */
	private BigDecimal rateAmount;
	
	/** The movement type selected. */
	private MovementType movementTypeSelected;
	
	/** The movement type search. */
	private MovementType movementTypeSearch;	
	
	/** The list movement type. */
	private List<MovementType> listMovementType;
	
	/** The data model movement type list. */
	private GenericDataModel<MovementType> dataModelMovementTypeList;
	
	/** The list rate movement to. */
	private List<RateMovementTo> listRateMovementTo;
	
	/** The data model rate movement to list. */
	private GenericDataModel<RateMovementTo> dataModelRateMovementToList;
	
	/** The rate movement to selected. */
	private RateMovementTo rateMovementToSelected; 
	
	/** The rate percent. */
	private BigDecimal ratePercent; 
	
	/** The movement count. */
	private Integer movementCount;
	
	/**  flag for visibility on page. */
    private boolean flagFixed;
    
    /** The flag percentage. */
    private boolean flagPercentage;
    
    /** The flag staggered fixed. */
    private boolean flagStaggeredFixed;
    
    /** The flag staggered percentage. */
    private boolean flagStaggeredPercentage;
    
    /** The flag movement type fixed. */
    private boolean flagMovementTypeFixed;
    
    /** The flag movement type percentage. */
    private boolean flagMovementTypePercentage;
    
    /** The flag percentage limits. */
    private boolean flagPercentageLimits;
    
    /** The flag service rate. */
    private boolean flagServiceRate;
    
    /** The flag delete service rate. */
    private boolean flagDeleteServiceRate = Boolean.FALSE;
    
    
	/** The service rate detail to. */
	private CollectionRateDetailTo serviceRateDetailTo=new CollectionRateDetailTo();
    
    /** The list service rate detail to. */
    private List<CollectionRateDetailTo> listServiceRateDetailTo;
   
    
    /**
     * Instantiates a new service rate to.
     */
    public ServiceRateTo(){
    	
    	this.disableFlagRateServices(); 
    	/**Initialize Staggered rate*/
    	this.listServiceRateScaleTo= new ArrayList<ServiceRateScaleTo>();
		this.rateServiceScaleToDataModel=  new RateServiceScaleToDataModel(this.listServiceRateScaleTo);
		endEffectiveDate = CommonsUtilities.currentDate();
    	initialEffectiveDate = CommonsUtilities.currentDate();
		/**Initialize Movement Type rate*/
		this.movementTypeSelected= new MovementType();
		this.movementTypeSearch= new MovementType();		
		this.listRateMovementTo = new ArrayList<RateMovementTo>();
		this.dataModelRateMovementToList = new GenericDataModel<RateMovementTo>(listRateMovementTo);
		this.rateMovementToSelected = new RateMovementTo();
		listServiceRateDetailTo=new ArrayList<CollectionRateDetailTo>(); 
		
		}
    
    /**
     * Instantiates a new service rate to.
     *
     * @param billingServiceTo the billing service to
     */
    public ServiceRateTo(BillingServiceTo billingServiceTo){
    	
    	this.disableFlagRateServices(); 
    	/**Initialize Staggered rate*/
    	this.listServiceRateScaleTo= new ArrayList<ServiceRateScaleTo>();
		this.rateServiceScaleToDataModel=  new RateServiceScaleToDataModel(this.listServiceRateScaleTo);
		
		/**Initialize Movement Type rate*/
		this.movementTypeSelected= new MovementType();
		this.movementTypeSearch= new MovementType();		
		this.listRateMovementTo = new ArrayList<RateMovementTo>();
		this.dataModelRateMovementToList = new GenericDataModel<RateMovementTo>(listRateMovementTo);
		this.rateMovementToSelected = new RateMovementTo();
		listServiceRateDetailTo=new ArrayList<CollectionRateDetailTo>();
		 
		this.setInitialEffectiveDate(billingServiceTo.getInitialEffectiveDate());
    	this.setEndEffectiveDate(billingServiceTo.getEndEffectiveDate());
		
		}
    
    /**
     * Sets the service rate to.
     *
     * @param serviceRateTo the new service rate to
     */
    public void setServiceRateTo(ServiceRateTo serviceRateTo){
    	
    	this.idServiceRatePk=serviceRateTo.getIdServiceRatePk();
    	this.rateCode=serviceRateTo.getRateCode();
    	this.rateName=serviceRateTo.getRateName();
    	this.currencyRate=serviceRateTo.getCurrencyRate();
    	this.rateStatus=serviceRateTo.getRateStatus();
    	this.endEffectiveDate =serviceRateTo.getEndEffectiveDate();
    	this.initialEffectiveDate =serviceRateTo.getInitialEffectiveDate();
    	this.appliedTaxSelected=serviceRateTo.getAppliedTaxSelected();
    	this.descriptionRate=serviceRateTo.getDescriptionRate();    	
        this.descriptionStatus=serviceRateTo.getDescriptionStatus();
        this.descriptionType=serviceRateTo.getDescriptionType();
    	this.rateType=serviceRateTo.getRateType();
    	this.rateAmount=serviceRateTo.getRateAmount();
    	//this.ratePercent=serviceRateTo.getRatePercent(); 
    	this.ratePercent = serviceRateTo.getRatePercent();
    	
    	setListRateMovementTo(serviceRateTo.getListRateMovementTo());
    	setListServiceRateScaleTo(serviceRateTo.getListServiceRateScaleTo());
    	setListServiceRateDetailTo(serviceRateTo.getListServiceRateDetailTo());
    }
        
	/**
	 * Gets the list rate movement to.
	 *
	 * @return the list rate movement to
	 */
	public List<RateMovementTo> getListRateMovementTo() {
		return listRateMovementTo;
	}

	/**
	 * Sets the list rate movement to.
	 *
	 * @param listRateMovementTo the new list rate movement to
	 */
	public void setListRateMovementTo(List<RateMovementTo> listRateMovementTo) {
		this.listRateMovementTo = listRateMovementTo;
	}

	/**
	 * Gets the id billing service.
	 *
	 * @return the id billing service
	 */
	public String getIdBillingService() {
		return idBillingService;
	}
	
	/**
	 * Sets the id billing service.
	 *
	 * @param idBillingService the new id billing service
	 */
	public void setIdBillingService(String idBillingService) {
		this.idBillingService = idBillingService;
	}
	
	/**
	 * Gets the service code.
	 *
	 * @return the service code
	 */
	public String getServiceCode() {
		return serviceCode;
	}
	
	/**
	 * Sets the service code.
	 *
	 * @param serviceCode the new service code
	 */
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	
	/**
	 * Checks if is flag fixed.
	 *
	 * @return true, if is flag fixed
	 */
	public boolean isFlagFixed() {
		return flagFixed;
	}
	
	/**
	 * Sets the flag fixed.
	 *
	 * @param flagFixed the new flag fixed
	 */
	public void setFlagFixed(boolean flagFixed) {
		this.flagFixed = flagFixed;
	}
	
	/**
	 * Checks if is flag percentage.
	 *
	 * @return true, if is flag percentage
	 */
	public boolean isFlagPercentage() {
		return flagPercentage;
	}
	
	/**
	 * Sets the flag percentage.
	 *
	 * @param flagPercentage the new flag percentage
	 */
	public void setFlagPercentage(boolean flagPercentage) {
		this.flagPercentage = flagPercentage;
	} 
	

	/**
	 * Checks if is flag service rate.
	 *
	 * @return true, if is flag service rate
	 */
	public boolean isFlagServiceRate() {
		return flagServiceRate;
	}

	/**
	 * Sets the flag service rate.
	 *
	 * @param flagServiceRate the new flag service rate
	 */
	public void setFlagServiceRate(boolean flagServiceRate) {
		this.flagServiceRate = flagServiceRate;
	}

	/**
	 * Gets the rate movement to selected.
	 *
	 * @return the rate movement to selected
	 */
	public RateMovementTo getRateMovementToSelected() {
		return rateMovementToSelected;
	}

	/**
	 * Checks if is flag staggered fixed.
	 *
	 * @return true, if is flag staggered fixed
	 */
	public boolean isFlagStaggeredFixed() {
		return flagStaggeredFixed;
	}

	/**
	 * Sets the flag staggered fiexed.
	 *
	 * @param flagStaggeredFixed the new flag staggered fiexed
	 */
	public void setFlagStaggeredFiexed(boolean flagStaggeredFixed) {
		this.flagStaggeredFixed = flagStaggeredFixed;
	}

	/**
	 * Checks if is flag staggered percentage.
	 *
	 * @return true, if is flag staggered percentage
	 */
	public boolean isFlagStaggeredPercentage() {
		return flagStaggeredPercentage;
	}

	/**
	 * Sets the flag staggered percentage.
	 *
	 * @param flagStaggeredPercentage the new flag staggered percentage
	 */
	public void setFlagStaggeredPercentage(boolean flagStaggeredPercentage) {
		this.flagStaggeredPercentage = flagStaggeredPercentage;
	}

	/**
	 * Checks if is flag movement type fixed.
	 *
	 * @return true, if is flag movement type fixed
	 */
	public boolean isFlagMovementTypeFixed() {
		return flagMovementTypeFixed;
	}

	/**
	 * Sets the flag movement type fixed.
	 *
	 * @param flagMovementTypeFixed the new flag movement type fixed
	 */
	public void setFlagMovementTypeFixed(boolean flagMovementTypeFixed) {
		this.flagMovementTypeFixed = flagMovementTypeFixed;
	}

	/**
	 * Checks if is flag movement type percentage.
	 *
	 * @return true, if is flag movement type percentage
	 */
	public boolean isFlagMovementTypePercentage() {
		return flagMovementTypePercentage;
	}

	/**
	 * Sets the flag movement type percentage.
	 *
	 * @param flagMovementTypePercentage the new flag movement type percentage
	 */
	public void setFlagMovementTypePercentage(boolean flagMovementTypePercentage) {
		this.flagMovementTypePercentage = flagMovementTypePercentage;
	}

	/**
	 * Checks if is flag percentage limits.
	 *
	 * @return true, if is flag percentage limits
	 */
	public boolean isFlagPercentageLimits() {
		return flagPercentageLimits;
	}

	/**
	 * Sets the flag percentage limits.
	 *
	 * @param flagPercentageLimits the new flag percentage limits
	 */
	public void setFlagPercentageLimits(boolean flagPercentageLimits) {
		this.flagPercentageLimits = flagPercentageLimits;
	}

	/**
	 * Sets the rate movement to selected.
	 *
	 * @param rateMovementToSelected the new rate movement to selected
	 */
	public void setRateMovementToSelected(RateMovementTo rateMovementToSelected) {
		this.rateMovementToSelected = rateMovementToSelected;
	}



	/**
	 * Gets the data model rate movement to list.
	 *
	 * @return the data model rate movement to list
	 */
	public GenericDataModel<RateMovementTo> getDataModelRateMovementToList() {
		return dataModelRateMovementToList;
	}



	/**
	 * Sets the data model rate movement to list.
	 *
	 * @param dataModelRateMovementToList the new data model rate movement to list
	 */
	public void setDataModelRateMovementToList(
			GenericDataModel<RateMovementTo> dataModelRateMovementToList) {
		this.dataModelRateMovementToList = dataModelRateMovementToList;
	}



	/**
	 * Gets the rate amount.
	 *
	 * @return the rate amount
	 */
	public BigDecimal getRateAmount() {
		return rateAmount;
	}




	/**
	 * Sets the rate amount.
	 *
	 * @param rateAmount the new rate amount
	 */
	public void setRateAmount(BigDecimal rateAmount) {
		this.rateAmount = rateAmount;
	}


	

	/**
	 * Gets the movement type selected.
	 *
	 * @return the movement type selected
	 */
	public MovementType getMovementTypeSelected() {
		return movementTypeSelected;
	}




	/**
	 * Sets the movement type selected.
	 *
	 * @param movementTypeSelected the new movement type selected
	 */
	public void setMovementTypeSelected(MovementType movementTypeSelected) {
		this.movementTypeSelected = movementTypeSelected;
	}




	/**
	 * Gets the movement type search.
	 *
	 * @return the movement type search
	 */
	public MovementType getMovementTypeSearch() {
		return movementTypeSearch;
	}




	/**
	 * Sets the movement type search.
	 *
	 * @param movementTypeSearch the new movement type search
	 */
	public void setMovementTypeSearch(MovementType movementTypeSearch) {
		this.movementTypeSearch = movementTypeSearch;
	}




	/**
	 * Gets the data model movement type list.
	 *
	 * @return the data model movement type list
	 */
	public GenericDataModel<MovementType> getDataModelMovementTypeList() {
		return dataModelMovementTypeList;
	}




	/**
	 * Sets the data model movement type list.
	 *
	 * @param dataModelMovementTypeList the new data model movement type list
	 */
	public void setDataModelMovementTypeList(
			GenericDataModel<MovementType> dataModelMovementTypeList) {
		this.dataModelMovementTypeList = dataModelMovementTypeList;
	}




	/**
	 * Gets the list movement type.
	 *
	 * @return the list movement type
	 */
	public List<MovementType> getListMovementType() {
		return listMovementType;
	}




	/**
	 * Sets the list movement type.
	 *
	 * @param listMovementType the new list movement type
	 */
	public void setListMovementType(List<MovementType> listMovementType) {
		this.listMovementType = listMovementType;
	}



	/**
	 * Gets the description movement type.
	 *
	 * @return the description movement type
	 */
	public String getDescriptionMovementType() {
		return descriptionMovementType;
	}




	/**
	 * Sets the description movement type.
	 *
	 * @param descriptionMovementType the new description movement type
	 */
	public void setDescriptionMovementType(String descriptionMovementType) {
		this.descriptionMovementType = descriptionMovementType;
	}




	/**
	 * Gets the code movement type.
	 *
	 * @return the code movement type
	 */
	public Long getCodeMovementType() {
		return codeMovementType;
	}


	/**
	 * Sets the code movement type.
	 *
	 * @param codeMovementType the new code movement type
	 */
	public void setCodeMovementType(Long codeMovementType) {
		this.codeMovementType = codeMovementType;
	}




	/**
	 * Gets the sequence number.
	 *
	 * @return the sequence number
	 */
	public Integer getSequenceNumber() {
		return sequenceNumber;
	}

	
	/**
	 * Gets the disable seq number.
	 *
	 * @return the disable seq number
	 */
	public Boolean getDisableSeqNumber() {
		return disableSeqNumber;
	}

	/**
	 * Sets the disable seq number.
	 *
	 * @param disableSeqNumber the new disable seq number
	 */
	public void setDisableSeqNumber(Boolean disableSeqNumber) {
		this.disableSeqNumber = disableSeqNumber;
	}

	
	/**
	 * Gets the disable btn generate.
	 *
	 * @return the disable btn generate
	 */
	public Boolean getDisableBtnGenerate() {
		return disableBtnGenerate;
	}

	/**
	 * Sets the disable btn generate.
	 *
	 * @param disableBtnGenerate the new disable btn generate
	 */
	public void setDisableBtnGenerate(Boolean disableBtnGenerate) {
		this.disableBtnGenerate = disableBtnGenerate;
	}

	/**
	 * Gets the disable unit.
	 *
	 * @return the disable unit
	 */
	public Boolean getDisableUnit() {
		return disableUnit;
	}

	/**
	 * Sets the disable unit.
	 *
	 * @param disableUnit the new disable unit
	 */
	public void setDisableUnit(Boolean disableUnit) {
		this.disableUnit = disableUnit;
	}

	/**
	 * Gets the unit.
	 *
	 * @return the unit
	 */
	public Integer getUnit() {
		return unit;
	}

	/**
	 * Sets the unit.
	 *
	 * @param unit the new unit
	 */
	public void setUnit(Integer unit) {
		this.unit = unit;
	}

	/**
	 * Sets the sequence number.
	 *
	 * @param sequenceNumber the new sequence number
	 */
	public void setSequenceNumber(Integer sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}




	/**
	 * Gets the rate service scale to data model.
	 *
	 * @return the rate service scale to data model
	 */
	public RateServiceScaleToDataModel getRateServiceScaleToDataModel() {
		return rateServiceScaleToDataModel;
	}




	/**
	 * Sets the rate service scale to data model.
	 *
	 * @param rateServiceScaleToDataModel the new rate service scale to data model
	 */
	public void setRateServiceScaleToDataModel(
			RateServiceScaleToDataModel rateServiceScaleToDataModel) {
		this.rateServiceScaleToDataModel = rateServiceScaleToDataModel;
	}




	/**
	 * Gets the list service rate scale to.
	 *
	 * @return the list service rate scale to
	 */
	public List<ServiceRateScaleTo> getListServiceRateScaleTo() {
		return listServiceRateScaleTo;
	}




	/**
	 * Sets the list service rate scale to.
	 *
	 * @param listServiceRateScaleTo the new list service rate scale to
	 */
	public void setListServiceRateScaleTo(
			List<ServiceRateScaleTo> listServiceRateScaleTo) {
		this.listServiceRateScaleTo = listServiceRateScaleTo;
	}


	/**
	 * Gets the percentage rate.
	 *
	 * @return the percentage rate
	 */
	public BigDecimal getPercentageRate() {
		return percentageRate;
	}




	/**
	 * Sets the percentage rate.
	 *
	 * @param percentageRate the new percentage rate
	 */
	public void setPercentageRate(BigDecimal percentageRate) {
		this.percentageRate = percentageRate;
	}




	/**
	 * Gets the rate type.
	 *
	 * @return the rate type
	 */
	public Integer getRateType() {
		return rateType;
	}




	/**
	 * Sets the rate type.
	 *
	 * @param rateType the new rate type
	 */
	public void setRateType(Integer rateType) {
		this.rateType = rateType;
	}




	/**
	 * Gets the description type.
	 *
	 * @return the description type
	 */
	public String getDescriptionType() {
		return descriptionType;
	}



	/**
	 * Sets the description type.
	 *
	 * @param descriptionType the new description type
	 */
	public void setDescriptionType(String descriptionType) {
		this.descriptionType = descriptionType;
	}



	/**
	 * Gets the id service rate pk.
	 *
	 * @return the id service rate pk
	 */
	public Long getIdServiceRatePk() {
		return idServiceRatePk;
	}



	/**
	 * Sets the id service rate pk.
	 *
	 * @param idServiceRatePk the new id service rate pk
	 */
	public void setIdServiceRatePk(Long idServiceRatePk) {
		this.idServiceRatePk = idServiceRatePk;
	}



	/**
	 * Gets the amount rate.
	 *
	 * @return the amount rate
	 */
	public BigDecimal getAmountRate() {
		return amountRate;
	}



	/**
	 * Sets the amount rate.
	 *
	 * @param amountRate the new amount rate
	 */
	public void setAmountRate(BigDecimal amountRate) {
		this.amountRate = amountRate;
	}



	/**
	 * Gets the applied tax selected.
	 *
	 * @return the applied tax selected
	 */
	public Integer getAppliedTaxSelected() {
		return appliedTaxSelected;
	}



	/**
	 * Sets the applied tax selected.
	 *
	 * @param appliedTaxSelected the new applied tax selected
	 */
	public void setAppliedTaxSelected(Integer appliedTaxSelected) {
		this.appliedTaxSelected = appliedTaxSelected;
	}



	


	/**
	 * Gets the rate code.
	 *
	 * @return the rate code
	 */
	public Integer getRateCode() {
		return rateCode;
	}




	/**
	 * Sets the rate code.
	 *
	 * @param rateCode the new rate code
	 */
	public void setRateCode(Integer rateCode) {
		this.rateCode = rateCode;
	}




	/**
	 * Gets the rate name.
	 *
	 * @return the rate name
	 */
	public String getRateName() {
		return rateName;
	}



	/**
	 * Sets the rate name.
	 *
	 * @param rateName the new rate name
	 */
	public void setRateName(String rateName) {
		this.rateName = rateName;
	}



	/**
	 * Gets the currency rate.
	 *
	 * @return the currency rate
	 */
	public Integer getCurrencyRate() {
		return currencyRate;
	}



	/**
	 * Sets the currency rate.
	 *
	 * @param currencyRate the new currency rate
	 */
	public void setCurrencyRate(Integer currencyRate) {
		this.currencyRate = currencyRate;
	}

	
	/**
	 * Gets the rate status.
	 *
	 * @return the rate status
	 */
	public Integer getRateStatus() {
		return rateStatus;
	}



	/**
	 * Sets the rate status.
	 *
	 * @param rateStatus the new rate status
	 */
	public void setRateStatus(Integer rateStatus) {
		this.rateStatus = rateStatus;
	}



	/**
	 * Gets the description rate.
	 *
	 * @return the description rate
	 */
	public String getDescriptionRate() {
		return descriptionRate;
	}



	/**
	 * Sets the description rate.
	 *
	 * @param descriptionRate the new description rate
	 */
	public void setDescriptionRate(String descriptionRate) {
		this.descriptionRate = descriptionRate;
	}



	/**
	 * Gets the description status.
	 *
	 * @return the description status
	 */
	public String getDescriptionStatus() {
		return descriptionStatus;
	}



	/**
	 * Sets the description status.
	 *
	 * @param descriptionStatus the new description status
	 */
	public void setDescriptionStatus(String descriptionStatus) {
		this.descriptionStatus = descriptionStatus;
	}



	/**
	 * Gets the end effective date.
	 *
	 * @return the end effective date
	 */
	public Date getEndEffectiveDate() {
		return endEffectiveDate;
	}

	/**
	 * Gets the end effective date str.
	 *
	 * @return the end effective date str
	 */
	public String getEndEffectiveDateStr() {
		return CommonsUtilities.convertDatetoString(endEffectiveDate);
	}
	
	/**
	 * Sets the end effective date.
	 *
	 * @param endEffectiveDate the new end effective date
	 */
	public void setEndEffectiveDate(Date endEffectiveDate) {
		this.endEffectiveDate = endEffectiveDate;
	}
	
	/**
	 * Gets the initial effective date.
	 *
	 * @return the initial effective date
	 */
	public Date getInitialEffectiveDate() {
		return initialEffectiveDate;
		 
	}
	
	/**
	 * Gets the initial effective date str.
	 *
	 * @return the initial effective date str
	 */
	public String getInitialEffectiveDateStr() {
		return CommonsUtilities.convertDatetoString(initialEffectiveDate);
	}
	
	/**
	 * Sets the initial effective date.
	 *
	 * @param initialEffectiveDate the new initial effective date
	 */
	public void setInitialEffectiveDate(Date initialEffectiveDate) {
		this.initialEffectiveDate = initialEffectiveDate;
	}

	/**
	 * Disable flag rate services.
	 */
	public void disableFlagRateServices(){
		this.flagFixed=Boolean.FALSE;
		this.flagPercentage=Boolean.FALSE;
		this.flagStaggeredFixed=Boolean.FALSE;;
		this.flagStaggeredPercentage=Boolean.FALSE;
		this.flagMovementTypeFixed=Boolean.FALSE;
		this.flagMovementTypePercentage=Boolean.FALSE;
		this.flagPercentageLimits=Boolean.FALSE;
	}


	/**
	 * Generate one stage rate.
	 */
	public void generateOneStageRate(){
		
		// BigDecimal firstScaleMin= listServiceRateScaleTo.get(0).getMinimumRange();
		
		boolean flagError=false;
		BigDecimal rest; 

		if( listServiceRateScaleTo.size()<1 && !( this.minScaleValue.toString().equals("1") || this.minScaleValue.toString().equals("1.00") )) {			
			flagError = true;	 
		}
		
		if(this.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode())) {
			this.setUnit(2);
		   }
 
		int result = minScaleValue.compareTo(maxScaleValue);
		if(result==0 || result ==-1){
			if(listServiceRateScaleTo!=null && listServiceRateScaleTo.size()>0 ){
				
			for(ServiceRateScaleTo serviceRateScaleTo: listServiceRateScaleTo){
				if(listServiceRateScaleTo.size()+1<=50){
					if(Double.valueOf(minScaleValue.toString()) <= Double.valueOf(serviceRateScaleTo.getMinimumRange().toString()) || 
							   Double.valueOf(minScaleValue.toString()) <= Double.valueOf(serviceRateScaleTo.getMaximumRange().toString()) ){ 
								flagError = true;
							}
							
							if(Double.valueOf(maxScaleValue.toString()) <= Double.valueOf(serviceRateScaleTo.getMinimumRange().toString()) || 
						       Double.valueOf(maxScaleValue.toString()) <= Double.valueOf(serviceRateScaleTo.getMaximumRange().toString()) ){ 
								flagError = true;	 
							} 
				}
				else if( listServiceRateScaleTo.size()+1>50 ) {
					executeAction();
					showMessageOnDialog(PropertiesUtilities
							.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage("message.scale.excesive"));
					JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
					return ; 
				}
			}
			
			
			
			
			
			
			BigDecimal lastMaximumRange= listServiceRateScaleTo.get(listServiceRateScaleTo.size()-1).getMaximumRange();
			
			
			rest = minScaleValue.subtract(lastMaximumRange); 
			
			if( this.unit==1 && !rest.equals(new BigDecimal(1)) ){
				flagError = true;
			}
			else if(this.unit==2 && !rest.toString().equals("0.01") ){
				flagError = true;
			}
			
			
			 
			
				
			}
			if(flagError){
				executeAction();
				showMessageOnDialog(PropertiesUtilities
						.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage("message.scale.error"));
				JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
				return ;
				
			}else{
						
				
				if (Validations.validateListIsNullOrEmpty(this.listServiceRateScaleTo)){
					this.listServiceRateScaleTo= new ArrayList<ServiceRateScaleTo>();
				}
				
				ServiceRateScaleTo serviceRateScaleTo=new ServiceRateScaleTo();;
				serviceRateScaleTo.setMinimumRange(this.minScaleValue);
				serviceRateScaleTo.setMaximumRange(this.maxScaleValue);
				serviceRateScaleTo.setScaleAmount(this.amountRate);
				serviceRateScaleTo.setScalePercent(this.percentageRate);
				
				serviceRateScaleTo.setScaleType(this.unit);
				
				serviceRateScaleTo.setSequenceCode(this.listServiceRateScaleTo.size() +1);	
				serviceRateScaleTo.setScaleType(this.getUnit());
				
				this.listServiceRateScaleTo.add(serviceRateScaleTo);	
			
				this.rateServiceScaleToDataModel.setWrappedData(this.listServiceRateScaleTo);
				
				this.setMinScaleValue(null);
				this.setMaxScaleValue(null);
				this.setAmountRate(null);
				this.setPercentageRate(null);
			
			}
			
		} 
		else{
			showMessageOnDialog(PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage("message.scale.error"));
			JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
		}
			
	}
	
	/**
	 * Prepare service rate scale.
	 *
	 * @return the list
	 */
	public List<ServiceRateScale> prepareServiceRateScale(){
		List<ServiceRateScale> serviceRateScaleList= new ArrayList<ServiceRateScale>();
		ServiceRateScale serviceRateScale;
		
					for (ServiceRateScaleTo serviceRateScaleto : this.listServiceRateScaleTo) {
						serviceRateScale = new ServiceRateScale();
						serviceRateScale.setMinimumRange(serviceRateScaleto.getMinimumRange());
						serviceRateScale.setMaximumRange(serviceRateScaleto.getMaximumRange());
						
						//serviceRateScale.setScalePercent(serviceRateScaleto.getScalePercent());
						if(Validations.validateIsNotNull(serviceRateScaleto.getScalePercent())){
							BigDecimal ratePercentTo = serviceRateScaleto.getScalePercent().divide(new BigDecimal(100),10,BigDecimal.ROUND_HALF_UP);
							
							serviceRateScale.setScalePercent(ratePercentTo);	
						}
												
						
						serviceRateScale.setScaleAmount(serviceRateScaleto.getScaleAmount());
						serviceRateScale.setScaleOrder(serviceRateScaleto.getSequenceCode());
						serviceRateScale.setScaleType(serviceRateScaleto.getScaleType());
						serviceRateScaleList.add(serviceRateScale);
					}	
							
		return serviceRateScaleList;
	}
	
	/**
	 * Prepare service rate movement.
	 *
	 * @param typeRateMovement the type rate movement
	 * @return the list
	 */
	public List<RateMovement> prepareServiceRateMovement(int typeRateMovement){
		List<RateMovement> serviceRateMovementList= new ArrayList<RateMovement>();
		RateMovement rateMovement;
					for (RateMovementTo rateMovementTo : this.listRateMovementTo) {
						rateMovement = new RateMovement();
						rateMovement.setMovementType(rateMovementTo.getMovementType());
						// add conditional typeRateMovement : amount or percent
						rateMovement.setRateAmount(rateMovementTo.getRateAmount());
						
						//rateMovement.setRatePercent(rateMovementTo.getRatePercent());
						if( Validations.validateIsNotNull(rateMovementTo.getRatePercent()) ){
							BigDecimal ratePercentTo = rateMovementTo.getRatePercent().divide(new BigDecimal(100),10,BigDecimal.ROUND_HALF_UP);
							rateMovement.setRatePercent(ratePercentTo);
						}
						
						serviceRateMovementList.add(rateMovement);
					}
		return serviceRateMovementList;
	}
	
	/**
	 * Removes the movement type.
	 *
	 * @param <T> the generic type
	 * @param object the object
	 * @param listRateMovement the list rate movement
	 * @return the list
	 */
	public <T>  List<T> removeMovementType(Object object , List<T> listRateMovement ){
		if ( object instanceof RateMovementTo){			
			listRateMovement.remove(object);			
		}
	  return  listRateMovement;
	}
	
	/**
	 * Gets the min scale value.
	 *
	 * @return the min scale value
	 */
	public BigDecimal getMinScaleValue() {
		return minScaleValue;
	}
	
	/**
	 * Sets the min scale value.
	 *
	 * @param minScaleValue the new min scale value
	 */
	public void setMinScaleValue(BigDecimal minScaleValue) {
		this.minScaleValue = minScaleValue;
	}
	
	/**
	 * Gets the max scale value.
	 *
	 * @return the max scale value
	 */
	public BigDecimal getMaxScaleValue() {
		return maxScaleValue;
	}
	
	/**
	 * Sets the max scale value.
	 *
	 * @param maxScaleValue the new max scale value
	 */
	public void setMaxScaleValue(BigDecimal maxScaleValue) {
		this.maxScaleValue = maxScaleValue;
	}
	
	/**
	 * Gets the new rate status.
	 *
	 * @return the new rate status
	 */
	public Integer getNewRateStatus() {
		return newRateStatus;
	}
	
	/**
	 * Sets the new rate status.
	 *
	 * @param newRateStatus the new new rate status
	 */
	public void setNewRateStatus(Integer newRateStatus) {
		this.newRateStatus = newRateStatus;
	}
	
	/**
	 * Gets the rate percent.
	 *
	 * @return the rate percent
	 */
	public BigDecimal getRatePercent() {
		return ratePercent;
	}
	
	/**
	 * Sets the rate percent.
	 *
	 * @param ratePercent the new rate percent
	 */
	public void setRatePercent(BigDecimal ratePercent) {
		this.ratePercent = ratePercent;
	}
	
	/**
	 * Gets the description currency.
	 *
	 * @return the description currency
	 */
	public String getDescriptionCurrency() {
		return descriptionCurrency;
	}
	
	/**
	 * Sets the description currency.
	 *
	 * @param descriptionCurrency the new description currency
	 */
	public void setDescriptionCurrency(String descriptionCurrency) {
		this.descriptionCurrency = descriptionCurrency;
	}
	
	/**
	 * Checks if is flag delete service rate.
	 *
	 * @return true, if is flag delete service rate
	 */
	public boolean isFlagDeleteServiceRate() {
		return flagDeleteServiceRate;
	}
	
	/**
	 * Sets the flag delete service rate.
	 *
	 * @param flagDeleteServiceRate the new flag delete service rate
	 */
	public void setFlagDeleteServiceRate(boolean flagDeleteServiceRate) {
		this.flagDeleteServiceRate = flagDeleteServiceRate;
	}
	
	/**
	 * Gets the service rate detail to.
	 *
	 * @return the service rate detail to
	 */
	public CollectionRateDetailTo getServiceRateDetailTo() {
		return serviceRateDetailTo;
	}
	
	/**
	 * Sets the service rate detail to.
	 *
	 * @param serviceRateDetailTo the new service rate detail to
	 */
	public void setServiceRateDetailTo(CollectionRateDetailTo serviceRateDetailTo) {
		this.serviceRateDetailTo = serviceRateDetailTo;
	}
	
	/**
	 * Gets the list service rate detail to.
	 *
	 * @return the list service rate detail to
	 */
	public List<CollectionRateDetailTo> getListServiceRateDetailTo() {
		return listServiceRateDetailTo;
	}
	
	/**
	 * Gets the movement count.
	 *
	 * @return the movement count
	 */
	public Integer getMovementCount() {
		return movementCount;
	}
	
	/**
	 * Sets the movement count.
	 *
	 * @param movementCount the new movement count
	 */
	public void setMovementCount(Integer movementCount) {
		this.movementCount = movementCount;
	}
	
	/**
	 * Sets the list service rate detail to.
	 *
	 * @param listServiceRateDetailTo the new list service rate detail to
	 */
	public void setListServiceRateDetailTo(
			List<CollectionRateDetailTo> listServiceRateDetailTo) {
		this.listServiceRateDetailTo = listServiceRateDetailTo;
	} 
	
	/**
	 * Checks if is selected.
	 *
	 * @return true, if is selected
	 */
	public boolean isSelected() {
		return selected;
	}
	
	/**
	 * Sets the selected.
	 *
	 * @param selected the new selected
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	} 
	
	/**
	 * Gets the fields on dtd.
	 *
	 * @return the fields on dtd
	 */
	public String getFieldsOnDtd (){		 
		return "ServiceRateTo [rateCode=" + rateCode + ", rateName=" + rateName
				+ ", endEffectiveDate=" + endEffectiveDate
				+ ", initialEffectiveDate=" + initialEffectiveDate
				+ ", descriptionStatus=" + descriptionStatus
				+ ", descriptionType=" + descriptionType + "]";	 	 
	}	
	
	/**
	 * Sets the fields on dtd.
	 *
	 * @param fieldsOnDtd the new fields on dtd
	 */
	public void setFieldsOnDtd(String fieldsOnDtd){
		this.fieldsOnDtd=fieldsOnDtd;		
	}
	
	/**
	 * Gets the last effective date.
	 *
	 * @return the last effective date
	 */
	public Date getLastEffectiveDate() {
		return lastEffectiveDate;
	}
	
	/**
	 * Sets the last effective date.
	 *
	 * @param lastEffectiveDate the new last effective date
	 */
	public void setLastEffectiveDate(Date lastEffectiveDate) {
		this.lastEffectiveDate = lastEffectiveDate;
	}

}