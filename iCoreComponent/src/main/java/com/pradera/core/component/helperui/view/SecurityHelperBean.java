package com.pradera.core.component.helperui.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.el.ELContext;
import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.HelperBean;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.HolderSearchTO;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.core.component.helperui.to.IssuerSearchTO;
import com.pradera.core.component.helperui.to.SecuritySearchTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.type.EconomicSectorType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.issuancesecuritie.type.SecurityType;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SecurityHelperBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 12-jun-2015
 */
@HelperBean
public class SecurityHelperBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The general parameters facade. */
	@Inject
	GeneralParametersFacade generalParametersFacade;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	/** The max results query helper. */
	@Inject @Configurable
	Integer maxResultsQueryHelper;
	
	/** The disabled issuer. */
	private boolean disabledIssuer;
	
	/** The disabled issuer dpf. */
	private boolean disabledIssuerDpf;
	
	/** The required issuer. */
	private boolean requiredIssuer;
	
	/** The lst issuer. */
	private List<Issuer> lstIssuer;
	
	/** The security search to. */
	private SecuritySearchTO securitySearchTO;

	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		securitySearchTO = new SecuritySearchTO();
		securitySearchTO.setIssuer(new Issuer());
	}
	
	/**
	 * Load help.
	 */
	@SuppressWarnings("unchecked")
	public void loadHelp(boolean disableSalMovVal, Long negoModality){
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			ELContext elContext = context.getELContext();
			ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.issuer}", Issuer.class);
			Issuer issuer = (Issuer) valueExpression.getValue(elContext);
			valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.lstIssuer}", List.class);
			List<Issuer> lstIssuerTemp= (List<Issuer>) valueExpression.getValue(elContext);
			valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.blIssuer}", Boolean.class);
			Boolean blIssuer = (Boolean) valueExpression.getValue(elContext);
			valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.issuance}", Issuance.class);
			Issuance issuance = (Issuance) valueExpression.getValue(elContext);
			valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.blIssuance}", Boolean.class);
			Boolean blIssuance = (Boolean) valueExpression.getValue(elContext);
			valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.securityClass}", Integer.class);
			Integer securityClass = (Integer) valueExpression.getValue(elContext);
			valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.participant}", Participant.class);
			Participant participant = (Participant) valueExpression.getValue(elContext);			

			if((Validations.validateIsNullOrEmpty(issuance) || Validations.validateIsNullOrEmpty(issuance.getIdIssuanceCodePk()))
					&& blIssuance){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getGenericMessage("security.helper.alert.issuance"));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}				
			securitySearchTO = new SecuritySearchTO();
			//securitySearchTO.setIssuer(issuer);
			securitySearchTO.setIssuance(issuance);	
			securitySearchTO.setParticipant(participant);
			securitySearchTO.setLstHolderAccountPk(null);
			disabledIssuer = true;
			disabledIssuerDpf = false;
			requiredIssuer = blIssuer.booleanValue();
			
			if (Validations.validateListIsNotNullAndNotEmpty(lstIssuerTemp)) {
				this.lstIssuer= lstIssuerTemp;
				disabledIssuer = false;
			} else {
				//if user is Issuer institution set the issuer code
				if((userInfo.getUserAccountSession().isIssuerInstitucion() || userInfo.getUserAccountSession().isIssuerDpfInstitucion()) &&
						Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getIssuerCode())) {
					Issuer issuerFilter = new Issuer();
					issuerFilter.setIdIssuerPk(userInfo.getUserAccountSession().getIssuerCode());
					securitySearchTO.setIssuer(helperComponentFacade.findIssuer(issuerFilter.getIdIssuerPk(),null));
					
					//fill lstIssuer with one item 
					lstIssuer = new ArrayList<Issuer>();
					lstIssuer.add(securitySearchTO.getIssuer());
					
					if( userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
						disabledIssuerDpf = true;
					}					
				} else {
					
					if(Validations.validateIsNotNull(issuer) && Validations.validateIsNotNullAndNotEmpty(issuer.getIdIssuerPk())){
						issuer= helperComponentFacade.findIssuer(issuer.getIdIssuerPk(), null);
						securitySearchTO.setIssuer(issuer);
						
						//fill lstIssuer with one item 
						lstIssuer = new ArrayList<Issuer>();
						lstIssuer.add(securitySearchTO.getIssuer());
						
					} else {
						
						securitySearchTO.setIssuer(new Issuer());
						
						if(lstIssuer == null || lstIssuer.size()<=1){
							listIssuers();
						}
						
						//If user is not issuer institution, then enable issuer combo
						if(!userInfo.getUserAccountSession().isIssuerInstitucion()){
							disabledIssuer = false;
						}
					}
					
					if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
						Participant objParticipant = accountsFacade.getParticipantByPk(userInfo.getUserAccountSession().getParticipantCode());					
						securitySearchTO.setParticipant(objParticipant);
						HolderAccountTO holderAccountTO = new HolderAccountTO();
						holderAccountTO.setParticipantTO(objParticipant.getIdParticipantPk());
						List<Long> lstHolderPk = new ArrayList<Long>();
						HolderSearchTO holderSearchTO = new HolderSearchTO();
						holderSearchTO.setDocumentType(objParticipant.getDocumentType());
						holderSearchTO.setDocumentNumber(objParticipant.getDocumentNumber());
						holderSearchTO.setState(HolderStateType.REGISTERED.getCode());
						List<Holder> lstHolder = helperComponentFacade.findHolders(holderSearchTO);
						if(lstHolder!=null && lstHolder.size()>0){
							for(Holder objHolder : lstHolder){
								lstHolderPk.add(objHolder.getIdHolderPk());
							}
						}
						holderAccountTO.setLstHolderPk(lstHolderPk);
						List<HolderAccount> lstHolderAccount = null;
						if(holderAccountTO.getLstHolderPk()!=null && holderAccountTO.getLstHolderPk().size()>0){
							 lstHolderAccount = accountsFacade.getHolderAccountListComponentServiceFacade(holderAccountTO);
						}						
						List<Long> lstHolderAccountPk = new ArrayList<Long>();
						if(lstHolderAccount!=null && lstHolderAccount.size()>0){
							for(HolderAccount objHolderAccount : lstHolderAccount){
								lstHolderAccountPk.add(objHolderAccount.getIdHolderAccountPk());
							}
						}
						securitySearchTO.setLstHolderAccountPk(lstHolderAccountPk);
					}
				}
			}
			
			fillCombos();
			if(Validations.validateIsNotNullAndPositive(securityClass)){

				ParameterTable objSecurityClass = generalParametersFacade.getParamDetailServiceFacade(securityClass);
				
				securitySearchTO.setState(SecurityStateType.REGISTERED.getCode());
				securitySearchTO.setInstrumentType(objSecurityClass.getShortInteger());
				changeInstrumentType();
				securitySearchTO.setSecurityType(objSecurityClass.getRelatedParameter().getParameterTablePk());
				changeSecurityType();
				securitySearchTO.setSecurityClass(objSecurityClass.getParameterTablePk());
				
				if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){					
					List<ParameterTable> lstSecurityClassAux = new ArrayList<>();
					List<ParameterTable> lstSecurityClass = generalParametersFacade.getListSecuritiesClassSetup(securitySearchTO.getInstrumentType(), 
										securitySearchTO.getIssuer().getEconomicSector(),securitySearchTO.getSecurityType());
					if(lstSecurityClass!=null && lstSecurityClass.size()>0){
						for(ParameterTable objParameterTable : lstSecurityClass){
							if(objParameterTable.getParameterTablePk().equals(SecurityClassType.DPF.getCode()) || 
									objParameterTable.getParameterTablePk().equals(SecurityClassType.DPA.getCode())){
								lstSecurityClassAux.add(objParameterTable);
							}
						}
					}
					securitySearchTO.setLstSecurityClass(lstSecurityClassAux);					
				}
				
			} else {
				if(disableSalMovVal) {
					securitySearchTO.setState(SecurityStateType.REGISTERED.getCode());
					securitySearchTO.setInstrumentType(InstrumentType.FIXED_INCOME.getCode());
				}
				if(negoModality.equals((long) 1)||negoModality.equals((long) 3)||negoModality.equals((long) 5)||negoModality.equals((long) 12)||negoModality.equals((long) 20)) {
					securitySearchTO.setState(SecurityStateType.REGISTERED.getCode());
					securitySearchTO.setInstrumentType(InstrumentType.FIXED_INCOME.getCode());
				}else if(negoModality.equals((long) 2)||negoModality.equals((long) 4)||negoModality.equals((long) 6)||negoModality.equals((long) 13)||negoModality.equals((long) 21)) {
					securitySearchTO.setState(SecurityStateType.REGISTERED.getCode());
					securitySearchTO.setInstrumentType(InstrumentType.VARIABLE_INCOME.getCode());
				}
				changeInstrumentType();
				if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
					securitySearchTO.setSecurityType(SecurityType.PUBLIC.getCode());
					List<ParameterTable> lstSecurityClassAux = new ArrayList<>();
					List<ParameterTable> lstSecurityClass = generalParametersFacade.getListSecuritiesClassSetup(securitySearchTO.getInstrumentType(), 
										securitySearchTO.getIssuer().getEconomicSector(),securitySearchTO.getSecurityType());
					if(lstSecurityClass!=null && lstSecurityClass.size()>0){
						for(ParameterTable objParameterTable : lstSecurityClass){
							if(objParameterTable.getParameterTablePk().equals(SecurityClassType.DPF.getCode()) || 
									objParameterTable.getParameterTablePk().equals(SecurityClassType.DPA.getCode())){
								lstSecurityClassAux.add(objParameterTable);
							}
						}
					}
					securitySearchTO.setLstSecurityClass(lstSecurityClassAux);					
				}
			}
			
			RequestContext.getCurrentInstance().execute("PF('dlgSecurity').show();");
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * List issuers.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listIssuers() throws ServiceException{
		IssuerSearchTO filter = new IssuerSearchTO();
		filter.setOrderBy("mnemonic");
		
		lstIssuer = helperComponentFacade.findIssuers(filter);
	}
	
	/**
	 * Search securities.
	 */
	public void searchSecurities(){
		try {
			if (requiredIssuer) {
				if (Validations.validateIsNull(securitySearchTO.getIssuer().getIdIssuerPk())) {
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getGenericMessage("security.helper.alert.issuer"));
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
			}
			if(Validations.validateIsNullOrEmpty(securitySearchTO.getState())){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getGenericMessage("security.helper.alert.state"));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			if(Validations.validateIsNullOrEmpty(securitySearchTO.getInstrumentType())){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getGenericMessage("security.helper.alert.instrumenttype"));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			securitySearchTO.setLstSecurity(null);
			securitySearchTO.setBlNoResult(true);
			List<Security> lstResult = null;
			if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
				if(securitySearchTO.getLstHolderAccountPk()!=null && securitySearchTO.getLstHolderAccountPk().size()>0){
					lstResult = helperComponentFacade.findSecurities(securitySearchTO);
				}				
			}else{
				lstResult = helperComponentFacade.findSecurities(securitySearchTO);
			}
			
			if(userInfo.getUserAccountSession().isParticipantInstitucion()) {
				if(userInfo.getUserAccountSession().getParticipantCode() != null) {
					securitySearchTO.setParticipant(new Participant());
					securitySearchTO.getParticipant().setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
					lstResult = helperComponentFacade.findSecurities(securitySearchTO);
				}
			}
			
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				fillSecurityParametersDescriptions(lstResult);
				securitySearchTO.setLstSecurity(new GenericDataModel<Security>(lstResult));
				securitySearchTO.setBlNoResult(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Fill security parameters descriptions.
	 *
	 * @param lstSecurities the lst securities
	 * @throws ServiceException the service exception
	 */
	private void fillSecurityParametersDescriptions(List<Security> lstSecurities) throws ServiceException{
		List<ParameterTable> lstSecurityType = generalParametersFacade.getListParameterTableServiceBean(MasterTableType.SECURITIES_TYPE.getCode());
		
		List<ParameterTable> lstSecurityClass = generalParametersFacade.getListParameterTableServiceBean(MasterTableType.SECURITIES_CLASS.getCode());
		
		for (Security objSecurity : lstSecurities) {
			
			for (ParameterTable prmSecurityType : lstSecurityType) {
				if(prmSecurityType.getParameterTablePk().equals(objSecurity.getSecurityType())){
					objSecurity.setSecurityTypeDescription(prmSecurityType.getParameterName());
					break;
				}
			}
			
			for (ParameterTable prmSecurityClass : lstSecurityClass) {
				if(prmSecurityClass.getParameterTablePk().equals(objSecurity.getSecurityClass())){
					objSecurity.setClassTypeDescription(prmSecurityClass.getParameterName());
					break;
				}
			}
			
		}
	}
	
	/**
	 * Gets the security.
	 *
	 * @param security the security
	 * @return the security
	 */
	public void getSecurity(Security security){
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.security}", Security.class);	
		valueExpression.setValue(elContext, helperComponentFacade.findSecurity(security));
	}
	
	/**
	 * Clean result.
	 */
	public void cleanResult(){
		securitySearchTO.setBlNoResult(false);
		securitySearchTO.setLstSecurity(null);
	}
	
	/**
	 * Change issuer type.
	 */
	public void changeIssuerForSecurityType(){
		if(securitySearchTO.getIssuer() != null && securitySearchTO.getIssuer().getIdIssuerPk()!=null){
			Issuer objIssuer = helperComponentFacade.findIssuer(securitySearchTO.getIssuer().getIdIssuerPk(),null);
			if(EconomicSectorType.NON_FINANCIAL_PUBLIC_SECTOR.getCode().equals(objIssuer.getEconomicSector())){
				securitySearchTO.setSecurityType(SecurityType.PUBLIC.getCode());
				changeSecurityType();
			} else if (EconomicSectorType.FINANCIAL_PUBLIC_SECTOR.getCode().equals(objIssuer.getEconomicSector())){
				securitySearchTO.setSecurityType(SecurityType.PRIVATE.getCode());
				changeSecurityType();
			}
		}					
	}
	
	/**
	 * Change instrument type.
	 */
	public void changeInstrumentType(){
		try {
			securitySearchTO.setSecurityClass(null);
			securitySearchTO.setSecurityType(null);
			securitySearchTO.setLstSecurityClass(null);
			securitySearchTO.setLstSecurityType(null);
			if(Validations.validateIsNotNullAndNotEmpty(securitySearchTO.getInstrumentType())){
				ParameterTableTO paramTabTO = new ParameterTableTO();
				paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
				paramTabTO.setMasterTableFk(MasterTableType.SECURITIES_TYPE.getCode());
				paramTabTO.setIdRelatedParameterFk(securitySearchTO.getInstrumentType());
				securitySearchTO.setLstSecurityType(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Change security type.
	 */
	public void changeSecurityType(){
		
		List<ParameterTable> lstSecurityClass=new ArrayList<>();
		try {
			lstSecurityClass=generalParametersFacade.getListSecuritiesClassSetup(securitySearchTO.getInstrumentType(), 
							securitySearchTO.getIssuer().getEconomicSector(),
							securitySearchTO.getSecurityType());
			securitySearchTO.setLstSecurityClass(lstSecurityClass);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Clean search.
	 */
	public void cleanSearch(){
		JSFUtilities.resetViewRoot();
		cleanResult();
		securitySearchTO.setState(null);
		if(!userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
			securitySearchTO.setInstrumentType(null);
			securitySearchTO.setLstSecurityClass(null);			
			securitySearchTO.setLstSecurityType(null);
			securitySearchTO.setSecurityType(null);
		}
		securitySearchTO.setSecurityClass(null);
		securitySearchTO.setDescription(null);
		securitySearchTO.setSecurityCode(null);
		securitySearchTO.setMnemonic(null);
	}
	
	/**
	 * Find security.
	 */
	public void findSecurity(){
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.issuer}", Issuer.class);
		Issuer issuer = (Issuer) valueExpression.getValue(elContext);			
		valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.issuance}", Issuance.class);
		Issuance issuance = (Issuance) valueExpression.getValue(elContext);
		valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.blIssuance}", Boolean.class);
		Boolean blIssuance = (Boolean) valueExpression.getValue(elContext);
		valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.participant}", Participant.class);
		Participant participant = (Participant)valueExpression.getValue(elContext);
		
		if((Validations.validateIsNullOrEmpty(issuance) || Validations.validateIsNullOrEmpty(issuance.getIdIssuanceCodePk()))
				&& blIssuance){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getGenericMessage("security.helper.alert.issuance"));
			JSFUtilities.showSimpleValidationDialog();
			valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.security}", Security.class);
			valueExpression.setValue(elContext, new Security());
			return;
		}
		valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.security}", Security.class);
		Security security = (Security) valueExpression.getValue(elContext);
		security.setIssuance(issuance);
		
		if((userInfo.getUserAccountSession().isIssuerInstitucion() || userInfo.getUserAccountSession().isIssuerDpfInstitucion()) &&
				Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getIssuerCode())) {
			Issuer issuerFilter = new Issuer();
			issuerFilter.setIdIssuerPk(userInfo.getUserAccountSession().getIssuerCode());
			security.setIssuer(helperComponentFacade.findIssuer(issuerFilter.getIdIssuerPk(),null));
		} else {
			security.setIssuer(issuer);
		}		
		
		if((userInfo.getUserAccountSession().isParticipantInstitucion() || userInfo.getUserAccountSession().isInstitution(InstitutionType.AFP))
			&&	Validations.validateIsNotNullAndNotEmpty(participant)){
			if(Validations.validateIsNotNullAndNotEmpty(security.getIdSecurityCodePk())){
				SecuritySearchTO  securitySearchTO = new SecuritySearchTO();
				securitySearchTO.setSecurityCode(security.getIdSecurityCodePk());
				securitySearchTO.setParticipant(participant);
				Security securityTemp = helperComponentFacade.findSecurity(securitySearchTO);
			
				if(Validations.validateIsNotNull(securityTemp)){
					valueExpression.setValue(elContext, securityTemp);
				}else{
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getGenericMessage("security.helper.alert.notexist", new Object[]{security.getIdSecurityCodePk()}));
					JSFUtilities.showSimpleValidationDialog();
					valueExpression.setValue(elContext, new Security());
				}
			
			}
			else{
				valueExpression.setValue(elContext, new Security());
			}
		}else if(Validations.validateIsNotNullAndNotEmpty(security.getIdSecurityCodePk())){
			Security securityTemp=null;
			securityTemp = helperComponentFacade.findSecurity(security);
			if(Validations.validateIsNotNull(securityTemp)){
				securityTemp.getInstrumentType();
				securityTemp.getIssuanceForm();
				if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
					if(securityTemp.getSecurityClass().equals(SecurityClassType.DPF.getCode()) || 
							securityTemp.getSecurityClass().equals(SecurityClassType.DPA.getCode())){
						valueExpression.setValue(elContext, securityTemp);
					}else{
						JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getGenericMessage("security.helper.alert.not.class", new Object[]{security.getIdSecurityCodePk()}));
						JSFUtilities.showSimpleValidationDialog();
						valueExpression.setValue(elContext, new Security());
					}
				}else if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
					Participant objParticipant = accountsFacade.getParticipantByPk(userInfo.getUserAccountSession().getParticipantCode());
					SecuritySearchTO securitySearchTO = new SecuritySearchTO();
					securitySearchTO.setParticipant(objParticipant);
					HolderAccountTO holderAccountTO = new HolderAccountTO();
					holderAccountTO.setParticipantTO(objParticipant.getIdParticipantPk());
					List<Long> lstHolderPk = new ArrayList<Long>();
					HolderSearchTO holderSearchTO = new HolderSearchTO();
					holderSearchTO.setDocumentType(objParticipant.getDocumentType());
					holderSearchTO.setDocumentNumber(objParticipant.getDocumentNumber());
					holderSearchTO.setState(HolderStateType.REGISTERED.getCode());
					List<Holder> lstHolder = helperComponentFacade.findHolders(holderSearchTO);
					if(lstHolder!=null && lstHolder.size()>0){
						for(Holder objHolder : lstHolder){
							lstHolderPk.add(objHolder.getIdHolderPk());
						}
					}
					holderAccountTO.setLstHolderPk(lstHolderPk);
					List<HolderAccount> lstHolderAccount = null;
					try {
						if(holderAccountTO.getLstHolderPk()!=null && holderAccountTO.getLstHolderPk().size()>0){
							lstHolderAccount = accountsFacade.getHolderAccountListComponentServiceFacade(holderAccountTO);
						}
					} catch (ServiceException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					List<Long> lstHolderAccountPk = new ArrayList<Long>();
					if(lstHolderAccount!=null && lstHolderAccount.size()>0){
						for(HolderAccount objHolderAccount : lstHolderAccount){
							lstHolderAccountPk.add(objHolderAccount.getIdHolderAccountPk());
						}
					}
					securitySearchTO.setLstHolderAccountPk(lstHolderAccountPk);
					List<Security> lstResult = null;
					if(securitySearchTO.getLstHolderAccountPk()!=null && securitySearchTO.getLstHolderAccountPk().size()>0){
						securitySearchTO.setState(SecurityStateType.REGISTERED.getCode());
						securitySearchTO.setInstrumentType(InstrumentType.FIXED_INCOME.getCode());
						if(securityTemp!=null && securityTemp.getIdSecurityCodePk()!=null){
							securitySearchTO.setSecurityCode(securityTemp.getIdSecurityCodePk());
						}
						lstResult = helperComponentFacade.findSecurities(securitySearchTO);
					}
					if(lstResult!=null && lstResult.size()>0){
						if(lstResult.contains(securityTemp)){
							valueExpression.setValue(elContext, securityTemp);
						}else{
							JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getGenericMessage("security.helper.alert.not.participant", new Object[]{security.getIdSecurityCodePk()}));
							JSFUtilities.showSimpleValidationDialog();
							valueExpression.setValue(elContext, new Security());
						}
					}else{
						JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getGenericMessage("security.helper.alert.not.participant", new Object[]{security.getIdSecurityCodePk()}));
						JSFUtilities.showSimpleValidationDialog();
						valueExpression.setValue(elContext, new Security());
					}
				}else{
					valueExpression.setValue(elContext, securityTemp);
				}					
			}else{
				
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getGenericMessage("security.helper.alert.notexist", new Object[]{security.getIdSecurityCodePk()}));
				
				if(security.getIssuer()!=null && Validations.validateIsNotNullAndNotEmpty( security.getIssuer().getIdIssuerPk() )){
					security.setIssuer(null);
					securityTemp = helperComponentFacade.findSecurity(security);
					
					if(Validations.validateIsNotNull(securityTemp)){
						JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getGenericMessage("security.helper.alert.not.issuer", new Object[]{security.getIdSecurityCodePk()}));
						
					}
				}
				JSFUtilities.showSimpleValidationDialog();
				valueExpression.setValue(elContext, new Security());
			}
		}		
		else{
			valueExpression.setValue(elContext, new Security());
		}		
	}
	
	/**
	 * Fill combos.
	 *
	 * @throws ServiceException the service exception
	 */
	private void fillCombos() throws ServiceException{
		ParameterTableTO paramTabTO = new ParameterTableTO();
		paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
		paramTabTO.setMasterTableFk(MasterTableType.SECURITY_STATE.getCode());
		securitySearchTO.setLstState(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
		paramTabTO.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
		securitySearchTO.setLstInstrumentType(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
	}
	
	/**
	 * Gets the security search to.
	 *
	 * @return the security search to
	 */
	public SecuritySearchTO getSecuritySearchTO() {
		return securitySearchTO;
	}
	
	/**
	 * Sets the security search to.
	 *
	 * @param securitySearchTO the new security search to
	 */
	public void setSecuritySearchTO(SecuritySearchTO securitySearchTO) {
		this.securitySearchTO = securitySearchTO;
	}

	/**
	 * Checks if is disabled issuer.
	 *
	 * @return true, if is disabled issuer
	 */
	public boolean isDisabledIssuer() {
		return disabledIssuer;
	}

	/**
	 * Sets the disabled issuer.
	 *
	 * @param disabledIssuer the new disabled issuer
	 */
	public void setDisabledIssuer(boolean disabledIssuer) {
		this.disabledIssuer = disabledIssuer;
	}

	/**
	 * Gets the lst issuer.
	 *
	 * @return the lst issuer
	 */
	public List<Issuer> getLstIssuer() {
		return lstIssuer;
	}

	/**
	 * Sets the lst issuer.
	 *
	 * @param lstIssuer the new lst issuer
	 */
	public void setLstIssuer(List<Issuer> lstIssuer) {
		this.lstIssuer = lstIssuer;
	}

	/**
	 * Checks if is required issuer.
	 *
	 * @return true, if is required issuer
	 */
	public boolean isRequiredIssuer() {
		return requiredIssuer;
	}

	/**
	 * Sets the required issuer.
	 *
	 * @param requiredIssuer the new required issuer
	 */
	public void setRequiredIssuer(boolean requiredIssuer) {
		this.requiredIssuer = requiredIssuer;
	}

	/**
	 * Gets the max results query helper.
	 *
	 * @return the max results query helper
	 */
	public Integer getMaxResultsQueryHelper() {
		return maxResultsQueryHelper;
	}

	/**
	 * Checks if is disabled issuer dpf.
	 *
	 * @return true, if is disabled issuer dpf
	 */
	public boolean isDisabledIssuerDpf() {
		return disabledIssuerDpf;
	}

	/**
	 * Sets the disabled issuer dpf.
	 *
	 * @param disabledIssuerDpf the new disabled issuer dpf
	 */
	public void setDisabledIssuerDpf(boolean disabledIssuerDpf) {
		this.disabledIssuerDpf = disabledIssuerDpf;
	}
	
	
}
