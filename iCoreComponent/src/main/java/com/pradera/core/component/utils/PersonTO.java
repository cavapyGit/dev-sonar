package com.pradera.core.component.utils;

import java.io.Serializable;

public class PersonTO implements Serializable{

	private static final long serialVersionUID = 1L;
	private Integer personType;
	private String  mnemonic;
	private boolean flagMinorIndicator;
	private boolean flagInabilityIndicator;
	private boolean flagNationalResident;
	private boolean flagForeignResident;
	private boolean flagNationalNotResident;
	private boolean flagForeignNotResident;
	private boolean flagIssuerIndicator;
	private boolean flagParticipantIndicator;
	private boolean flagHolderIndicator;
	private boolean flagLegalRepresentativeIndicator;
	private boolean flagNational;
	private boolean flagForeign;
	private boolean flagJuridicClassTrust;
	private boolean flagJuridicDocuments;
	private boolean flagNaturalDocuments;
	
	
	public Integer getPersonType() {
		return personType;
	}
	public void setPersonType(Integer personType) {
		this.personType = personType;
	}
	public boolean isFlagMinorIndicator() {
		return flagMinorIndicator;
	}
	public void setFlagMinorIndicator(boolean flagMinorIndicator) {
		this.flagMinorIndicator = flagMinorIndicator;
	}
	public boolean isFlagInabilityIndicator() {
		return flagInabilityIndicator;
	}
	public void setFlagInabilityIndicator(boolean flagInabilityIndicator) {
		this.flagInabilityIndicator = flagInabilityIndicator;
	}
	
	public boolean isFlagNationalNotResident() {
		return flagNationalNotResident;
	}
	public void setFlagNationalNotResident(boolean flagNationalNotResident) {
		this.flagNationalNotResident = flagNationalNotResident;
	}
	public boolean isFlagForeignNotResident() {
		return flagForeignNotResident;
	}
	public void setFlagForeignNotResident(boolean flagForeignNotResident) {
		this.flagForeignNotResident = flagForeignNotResident;
	}
	public boolean isFlagIssuerIndicator() {
		return flagIssuerIndicator;
	}
	public void setFlagIssuerIndicator(boolean flagIssuerIndicator) {
		this.flagIssuerIndicator = flagIssuerIndicator;
	}
	public boolean isFlagParticipantIndicator() {
		return flagParticipantIndicator;
	}
	public void setFlagParticipantIndicator(boolean flagParticipantIndicator) {
		this.flagParticipantIndicator = flagParticipantIndicator;
	}
	public boolean isFlagHolderIndicator() {
		return flagHolderIndicator;
	}
	public void setFlagHolderIndicator(boolean flagHolderIndicator) {
		this.flagHolderIndicator = flagHolderIndicator;
	}
	public boolean isFlagLegalRepresentativeIndicator() {
		return flagLegalRepresentativeIndicator;
	}
	public void setFlagLegalRepresentativeIndicator(
			boolean flagLegalRepresentativeIndicator) {
		this.flagLegalRepresentativeIndicator = flagLegalRepresentativeIndicator;
	}
	public boolean isFlagNational() {
		return flagNational;
	}
	public void setFlagNational(boolean flagNational) {
		this.flagNational = flagNational;
	}
	public boolean isFlagForeign() {
		return flagForeign;
	}
	public void setFlagForeign(boolean flagForeign) {
		this.flagForeign = flagForeign;
	}
	public boolean isFlagJuridicClassTrust() {
		return flagJuridicClassTrust;
	}
	public void setFlagJuridicClassTrust(boolean flagJuridicClassTrust) {
		this.flagJuridicClassTrust = flagJuridicClassTrust;
	}
	public boolean isFlagJuridicDocuments() {
		return flagJuridicDocuments;
	}
	public void setFlagJuridicDocuments(boolean flagJuridicDocuments) {
		this.flagJuridicDocuments = flagJuridicDocuments;
	}
	public boolean isFlagNaturalDocuments() {
		return flagNaturalDocuments;
	}
	public void setFlagNaturalDocuments(boolean flagNaturalDocuments) {
		this.flagNaturalDocuments = flagNaturalDocuments;
	}
	public boolean isFlagNationalResident() {
		return flagNationalResident;
	}
	public void setFlagNationalResident(boolean flagNationalResident) {
		this.flagNationalResident = flagNationalResident;
	}
	public boolean isFlagForeignResident() {
		return flagForeignResident;
	}
	public void setFlagForeignResident(boolean flagForeignResident) {
		this.flagForeignResident = flagForeignResident;
	}
	public String getMnemonic() {
		return mnemonic;
	}
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}
	
}
