package com.pradera.core.component.helperui.extension.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.el.ELContext;
import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;

import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.HelperBean;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.BillingServiceTo;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.billing.type.BillingServiceFilter;
import com.pradera.model.generalparameter.ParameterTable;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingServiceExtensionHelperBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01-sep-2015
 */
@HelperBean
public class BillingServiceExtensionHelperBean extends GenericBaseBean  implements Serializable { 
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8979684280696810462L;
	
	/**  Bean BillingService getting for container parent. */
	private BillingServiceTo billingServiceReturn;	
	
	/**  Bean BillingServiceTo used only for filter on search dialog. */
	private BillingServiceTo billingServiceTo;
	
    /**  Parameters for search. */
	private List<ParameterTable> listCollectionInstitution;
	
	/** The list service type. */
	private List<ParameterTable> listServiceType;
	
	/** The list service status. */
	private List<ParameterTable> listServiceStatus; 
	
	/** The list collection period. */
	private List<ParameterTable> listCollectionPeriod;
	
	/** The list collection base. */
	private List<ParameterTable> listCollectionBase;
	
	/** The list collection type. */
	private List<ParameterTable> listCollectionType;
	
	/** The list service client. */
	private List<ParameterTable> listServiceClient;
	
	/** The list empty. */
	private boolean listEmpty = false;
	
	/** The helper name. */
	private String helperName;
	
	/** The billing service helper. */
	private GenericDataModel<BillingServiceTo> billingServiceHelper;
	
	/** The list billing service to. */
	private List<BillingServiceTo> listBillingServiceTo;


	/** The applied tax disable. */
	private Boolean appliedTaxDisable=Boolean.FALSE;
	
	/** The ind no integrated. */
	private Integer indNoIntegrated=GeneralConstants.ZERO_VALUE_INTEGER;
	
	/** The ind integrated. */
	private Integer indIntegrated=GeneralConstants.ONE_VALUE_INTEGER;

	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade  helperComponentFacade;
	
	@Inject
	UserInfo UserInformation;

	/**
	 * Instantiates a new billing service extension helper bean.
	 */
	public BillingServiceExtensionHelperBean(){}
	
	/**
	 * Inits the.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException{
		/** Values by Default*/
		billingServiceReturn = new BillingServiceTo();
		billingServiceTo= new BillingServiceTo();		
		listBillingServiceTo=new ArrayList<BillingServiceTo>();	
		if (!FacesContext.getCurrentInstance().isPostback()){
			loadDataSearch(); 	 
		}
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.typeService}", Long.class);
		Object valueTypeSerice = valueExpression.getValue(elContext);
		Integer typeServicePk= Integer.parseInt(((Long)valueTypeSerice).toString());
		
		if (!Validations.validateListIsNullOrEmpty(listServiceType)){
				for ( ParameterTable parameterTable : listServiceType) {
					if (parameterTable.getParameterTablePk().equals(typeServicePk)){
						billingServiceTo.setServiceType(parameterTable.getParameterTablePk());
						billingServiceTo.setDescriptionServiceType(parameterTable.getDescription());
						// si se deshabilita o no!
						return;
					}
				}
		}
	}
	
	/**
	 * Assign billing service.
	 *
	 * @param billingServiceToSelected the billing service to selected
	 */
	public void assignBillingService(BillingServiceTo billingServiceToSelected){
		
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		//getting holder object from inputText
		ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.billingService}", Object.class);
		valueExpression.setValue(elContext, billingServiceToSelected);
		clearHelper();
		ValueExpression valueExpressionEnableActionRemote= context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.enableActionRemote}", Boolean.class);
		Boolean enableActionRemote=(Boolean)valueExpressionEnableActionRemote.getValue(elContext);
		if(enableActionRemote){
			JSFUtilities.executeJavascriptFunction("oncomplete();");
		}
		clearHelper();
	}
	
	//CLEAR HELPER
		/**
	 * Clear helper.
	 */
	public void clearHelper(){

			 billingServiceHelper=null;
			 billingServiceTo.setServiceCode(null);
			 billingServiceTo.setBaseCollection(-1);
			 
			 
			 FacesContext context = FacesContext.getCurrentInstance();
			 ELContext elContext = context.getELContext();
			 
			 ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.typeService}", Long.class);
			 Long typeService = (Long)valueExpression.getValue(elContext);
			 if (typeService.intValue()==(-1)){
				 billingServiceTo.setServiceType(-1);
			 }			
			 billingServiceTo.setStateService(-1);
			 billingServiceTo.setServiceName(null);
			 billingServiceTo.setEntityCollection(-1);
			 billingServiceTo.setClientServiceType(-1);
			 
			 listBillingServiceTo.clear();
			 listEmpty = false;
	
		}
	

	

	

	/**
	 * Checks if is list empty.
	 *
	 * @return true, if is list empty
	 */
	public boolean isListEmpty() {
		return listEmpty;
	}

	/**
	 * Sets the list empty.
	 *
	 * @param listEmpty the new list empty
	 */
	public void setListEmpty(boolean listEmpty) {
		this.listEmpty = listEmpty;
	}




 
	/**
	 * Load data search.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadDataSearch() throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		if (Validations.validateListIsNullOrEmpty(listCollectionInstitution)){
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO.setMasterTableFk(BillingServiceFilter.ENTITY_COLLECTION_PK.getCode());
			listCollectionInstitution= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}
		
		if (Validations.validateListIsNullOrEmpty(listServiceStatus)){
			parameterTableTO.setMasterTableFk(BillingServiceFilter.SERVICE_STATUS_PK.getCode());
			listServiceStatus= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}
		if (Validations.validateListIsNullOrEmpty(listCollectionPeriod)){
				parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
				parameterTableTO.setOrderbyParameterTableCd(GeneralConstants.ONE_VALUE_INTEGER);
				parameterTableTO.setMasterTableFk(BillingServiceFilter.COLLECTION_PERIOD_PK.getCode());
				parameterTableTO.setState(1);
				listCollectionPeriod= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			}
		if (Validations.validateListIsNullOrEmpty(listCollectionBase)){
				parameterTableTO.setOrderbyParameterTableCd(GeneralConstants.ONE_VALUE_INTEGER);
				parameterTableTO.setMasterTableFk(BillingServiceFilter.COLLECTION_BASE_PK.getCode());
				parameterTableTO.setElementSubclasification1(null);
				listCollectionBase= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			}
		if (Validations.validateListIsNullOrEmpty(listServiceType)){
				parameterTableTO.setMasterTableFk(BillingServiceFilter.SERVICE_TYPE_PK.getCode());				
				listServiceType= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			}
		
		if (Validations.validateListIsNullOrEmpty(listServiceClient)){ 
			parameterTableTO.setMasterTableFk(BillingServiceFilter.SERVICE_CLIENT_PK.getCode());
			listServiceClient= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}
		
	} 

	 
	
	/**
	 * Check all fields not null.
	 *
	 * @return true, if successful
	 * @throws IllegalArgumentException the illegal argument exception
	 * @throws IllegalAccessException the illegal access exception
	 */
	public boolean checkAllFieldsNotNull() throws IllegalArgumentException, IllegalAccessException{
		boolean boolResult = false;
		
		if(Validations.validateIsNotNullAndPositive(billingServiceTo.getIdBillingServicePk())   ||
		   Validations.validateIsNotNullAndNotEmpty(billingServiceTo.getServiceCode()) ||
		   Validations.validateIsNotNullAndNotEmpty(billingServiceTo.getServiceName()) ||
		   Validations.validateIsNotNullAndNotEmpty(billingServiceTo.getServiceType())			  ||
		   Validations.validateIsNotNullAndNotEmpty(billingServiceTo.getClientServiceType())  ||
		   Validations.validateIsNotNullAndNotEmpty(billingServiceTo.getEntityCollection())       
		   ){
			boolResult = true;
		}
		return boolResult;
	}
		
	/**
	 * *MIGUEL DIAZ: METHOD CALLED FOR BLUR EVENT BILLING SERVICE.
	 */
	public void findBillingServiceByCode() {
		
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		ValueExpression valueExpressionServiceCode = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.billingService.serviceCode}", String.class);
		Object valueServiceCode = valueExpressionServiceCode.getValue(elContext);
		
		ValueExpression valueExpressionBillingService= context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.billingService}", Object.class);
		Object value = valueExpressionBillingService.getValue(elContext);
		
		ValueExpression valueExpressionName = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.name}", String.class);
		helperName = (String)valueExpressionName.getValue(elContext);
		
			
		ValueExpression billingServiceHelpMessageValueExp = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.resourceBundleMap.billingServiceHelpMessage}", String.class);
		String billingServiceHelpMessage= (String)billingServiceHelpMessageValueExp.getValue(elContext);
		
		ValueExpression titleValueEx = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.resourceBundleMap.title}", String.class);
		String title= (String)titleValueEx.getValue(elContext);
		
		ValueExpression titleValueNoManual = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.resourceBundleMap.servicioNoManual}", String.class);
		String noManual= (String)titleValueNoManual.getValue(elContext);
		 
		String serviceCode= (valueServiceCode).toString();
		
		ValueExpression valueExpressionEnableActionRemote= context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.enableActionRemote}", Boolean.class);
		Boolean enableActionRemote=(Boolean)valueExpressionEnableActionRemote.getValue(elContext);
		
		if(Validations.validateIsNotNullAndNotEmpty(serviceCode)){
			Long participant = UserInformation.getUserAccountSession().getParticipantCode();
			if(!Validations.validateIsNotNullAndPositive(participant )&&!Validations.validateIsNotNullAndNotEmpty(participant)){
				participant = GeneralConstants.PARTICIPANT_EDB_CODE;
			}
		 	billingServiceReturn=this.helperComponentFacade.getBillingService(serviceCode, participant);
					  
			if(Validations.validateIsNotNullAndNotEmpty(billingServiceReturn)){
				
				ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.typeService}", Long.class);
				Object valueTypeService = valueExpression.getValue(elContext);
				Integer valueTypeServiceInteger=Integer.parseInt(valueTypeService.toString());
			   
				if (valueTypeServiceInteger.equals(-1)){
					valueExpressionBillingService.setValue(elContext, billingServiceReturn);
					if(enableActionRemote){
						 JSFUtilities.executeJavascriptFunction("oncomplete();");
					}
				}else {
					if (billingServiceReturn.getServiceType().equals(valueTypeServiceInteger)){
						valueExpressionBillingService.setValue(elContext, billingServiceReturn);
						if(enableActionRemote){
							 JSFUtilities.executeJavascriptFunction("oncomplete();");
						}
					}
					else {
						 billingServiceReturn= new BillingServiceTo();
						 JSFUtilities.putViewMap("bodyMessageView", noManual);
						 JSFUtilities.putViewMap("headerMessageView", title);
						 RequestContext.getCurrentInstance().execute("PF('cnfwMsgCustomValidationBlur').show();");
						 valueExpressionBillingService.setValue(elContext, billingServiceReturn);
					}
				}	
				
				
			}
			else {
				 billingServiceReturn= new BillingServiceTo();
				 JSFUtilities.putViewMap("bodyMessageView", billingServiceHelpMessage);
				 JSFUtilities.putViewMap("headerMessageView", title);		
				 RequestContext.getCurrentInstance().execute("PF('cnfwMsgCustomValidationBlur').show();");
				 valueExpressionBillingService.setValue(elContext, billingServiceReturn);
			}
		}
		
	}
	
	/**
	 * Fill list service code and reference rate.
	 */
	public void fillListServiceCodeAndReferenceRate(){
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();

		List<String> listReferenceRate=new ArrayList<>();
		List<String> listServiceCode=new ArrayList<>();
		
		ValueExpression valueExpServiceCode = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.listServiceCode}", String.class);
		String strServiceCode= (String)valueExpServiceCode.getValue(elContext);
		ValueExpression valueExpReferenceRate = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.listReferenceRate}", String.class);
		String strReferenceRate = (String)valueExpReferenceRate.getValue(elContext);
		
		if(Validations.validateIsNotEmpty(strServiceCode)){

			String[] arrayServiceCode	=	strServiceCode.split(",");
			for (String string : arrayServiceCode) {
				listServiceCode.add(string);
			}
		}
		billingServiceTo.setListServiceCode(listServiceCode);
		
		if(Validations.validateIsNotEmpty(strReferenceRate)){

			String[] arrayReferenceRate	=	strReferenceRate.split(",");
			for (String string : arrayReferenceRate) {
				listReferenceRate.add(string);
			}
			
			if(arrayReferenceRate.length != 1) billingServiceTo.setReferenceRate("");
			else billingServiceTo.setReferenceRate(strReferenceRate);
		}
		billingServiceTo.setListReferenceRate(listReferenceRate);
		
	}

	/**
	 * Search billing services.
	 */
	
	
	public void searchBillingServices(){
		fillListServiceCodeAndReferenceRate();
		/*Seteando el id usuario*/
		try { 
			Long participant = UserInformation.getUserAccountSession().getParticipantCode();
			if(!Validations.validateIsNotNullAndPositive(participant )&&!Validations.validateIsNotNullAndNotEmpty(participant)){
				participant = GeneralConstants.PARTICIPANT_EDB_CODE;
			}
			listBillingServiceTo=helperComponentFacade.getListParameterBillingService(this.billingServiceTo, participant);
			billingServiceHelper= new GenericDataModel<BillingServiceTo>(listBillingServiceTo); 
			if(listBillingServiceTo.size()==GeneralConstants.ZERO_VALUE_INTEGER){
				listEmpty = true;
			}else{
				listEmpty = false; 
			}
		} catch (ServiceException e) {
			
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Consult billing service.
	 *
	 * @param event the event
	 * @throws ServiceException the service exception
	 */
	public void consultBillingService(ActionEvent event) throws ServiceException{
		
		if (event != null) {
			billingServiceReturn = (BillingServiceTo) event.getComponent()
					.getAttributes().get("blockInformation");

			this.billingServiceTo = this.billingServiceReturn;
		}
	
	}

	/**
	 * Gets the billing service return.
	 *
	 * @return the billing service return
	 */
	public BillingServiceTo getBillingServiceReturn() {
		return billingServiceReturn;
	}

	/**
	 * Sets the billing service return.
	 *
	 * @param billingServiceReturn the new billing service return
	 */
	public void setBillingServiceReturn(BillingServiceTo billingServiceReturn) {
		this.billingServiceReturn = billingServiceReturn;
	}

	/**
	 * Gets the billing service to.
	 *
	 * @return the billing service to
	 */
	public BillingServiceTo getBillingServiceTo() {
		return billingServiceTo;
	}

	/**
	 * Sets the billing service to.
	 *
	 * @param billingServiceTo the new billing service to
	 */
	public void setBillingServiceTo(BillingServiceTo billingServiceTo) {
		this.billingServiceTo = billingServiceTo;
	}

	/**
	 * Gets the list collection institution.
	 *
	 * @return the list collection institution
	 */
	public List<ParameterTable> getListCollectionInstitution() {
		return listCollectionInstitution;
	}

	/**
	 * Sets the list collection institution.
	 *
	 * @param listCollectionInstitution the new list collection institution
	 */
	public void setListCollectionInstitution(
			List<ParameterTable> listCollectionInstitution) {
		this.listCollectionInstitution = listCollectionInstitution;
	}

	/**
	 * Gets the list service type.
	 *
	 * @return the list service type
	 */
	public List<ParameterTable> getListServiceType() {
		return listServiceType;
	}

	/**
	 * Sets the list service type.
	 *
	 * @param listServiceType the new list service type
	 */
	public void setListServiceType(List<ParameterTable> listServiceType) {
		this.listServiceType = listServiceType;
	}

	/**
	 * Gets the list service status.
	 *
	 * @return the list service status
	 */
	public List<ParameterTable> getListServiceStatus() {
		return listServiceStatus;
	}

	/**
	 * Sets the list service status.
	 *
	 * @param listServiceStatus the new list service status
	 */
	public void setListServiceStatus(List<ParameterTable> listServiceStatus) {
		this.listServiceStatus = listServiceStatus;
	}

	/**
	 * Gets the list collection period.
	 *
	 * @return the list collection period
	 */
	public List<ParameterTable> getListCollectionPeriod() {
		return listCollectionPeriod;
	}

	/**
	 * Sets the list collection period.
	 *
	 * @param listCollectionPeriod the new list collection period
	 */
	public void setListCollectionPeriod(List<ParameterTable> listCollectionPeriod) {
		this.listCollectionPeriod = listCollectionPeriod;
	}

	/**
	 * Gets the list collection base.
	 *
	 * @return the list collection base
	 */
	public List<ParameterTable> getListCollectionBase() {
		return listCollectionBase;
	}

	/**
	 * Sets the list collection base.
	 *
	 * @param listCollectionBase the new list collection base
	 */
	public void setListCollectionBase(List<ParameterTable> listCollectionBase) {
		this.listCollectionBase = listCollectionBase;
	}

	/**
	 * Gets the list collection type.
	 *
	 * @return the list collection type
	 */
	public List<ParameterTable> getListCollectionType() {
		return listCollectionType;
	}

	/**
	 * Sets the list collection type.
	 *
	 * @param listCollectionType the new list collection type
	 */
	public void setListCollectionType(List<ParameterTable> listCollectionType) {
		this.listCollectionType = listCollectionType;
	}

	/**
	 * Gets the list service client.
	 *
	 * @return the list service client
	 */
	public List<ParameterTable> getListServiceClient() {
		return listServiceClient;
	}

	/**
	 * Sets the list service client.
	 *
	 * @param listServiceClient the new list service client
	 */
	public void setListServiceClient(List<ParameterTable> listServiceClient) {
		this.listServiceClient = listServiceClient;
	}

	/**
	 * Gets the helper name.
	 *
	 * @return the helper name
	 */
	public String getHelperName() {
		return helperName;
	}

	/**
	 * Sets the helper name.
	 *
	 * @param helperName the new helper name
	 */
	public void setHelperName(String helperName) {
		this.helperName = helperName;
	}

	/**
	 * Gets the billing service helper.
	 *
	 * @return the billing service helper
	 */
	public GenericDataModel<BillingServiceTo> getBillingServiceHelper() {
		return billingServiceHelper;
	}

	/**
	 * Sets the billing service helper.
	 *
	 * @param billingServiceHelper the new billing service helper
	 */
	public void setBillingServiceHelper(
			GenericDataModel<BillingServiceTo> billingServiceHelper) {
		this.billingServiceHelper = billingServiceHelper;
	}

	/**
	 * Gets the list billing service to.
	 *
	 * @return the list billing service to
	 */
	public List<BillingServiceTo> getListBillingServiceTo() {
		return listBillingServiceTo;
	}

	/**
	 * Sets the list billing service to.
	 *
	 * @param listBillingServiceTo the new list billing service to
	 */
	public void setListBillingServiceTo(List<BillingServiceTo> listBillingServiceTo) {
		this.listBillingServiceTo = listBillingServiceTo;
	}

	/**
	 * Gets the general parameters facade.
	 *
	 * @return the general parameters facade
	 */
	public GeneralParametersFacade getGeneralParametersFacade() {
		return generalParametersFacade;
	}

	/**
	 * Sets the general parameters facade.
	 *
	 * @param generalParametersFacade the new general parameters facade
	 */
	public void setGeneralParametersFacade(
			GeneralParametersFacade generalParametersFacade) {
		this.generalParametersFacade = generalParametersFacade;
	}

	/**
	 * Gets the helper component facade.
	 *
	 * @return the helper component facade
	 */
	public HelperComponentFacade getHelperComponentFacade() {
		return helperComponentFacade;
	}

	/**
	 * Sets the helper component facade.
	 *
	 * @param helperComponentFacade the new helper component facade
	 */
	public void setHelperComponentFacade(HelperComponentFacade helperComponentFacade) {
		this.helperComponentFacade = helperComponentFacade;
	}

	/**
	 * Gets the applied tax disable.
	 *
	 * @return the applied tax disable
	 */
	public Boolean getAppliedTaxDisable() {
		return appliedTaxDisable;
	}

	/**
	 * Sets the applied tax disable.
	 *
	 * @param appliedTaxDisable the new applied tax disable
	 */
	public void setAppliedTaxDisable(Boolean appliedTaxDisable) {
		this.appliedTaxDisable = appliedTaxDisable;
	}

	/**
	 * Gets the ind no integrated.
	 *
	 * @return the ind no integrated
	 */
	public Integer getIndNoIntegrated() {
		return indNoIntegrated;
	}

	/**
	 * Sets the ind no integrated.
	 *
	 * @param indNoIntegrated the new ind no integrated
	 */
	public void setIndNoIntegrated(Integer indNoIntegrated) {
		this.indNoIntegrated = indNoIntegrated;
	}

	/**
	 * Gets the ind integrated.
	 *
	 * @return the ind integrated
	 */
	public Integer getIndIntegrated() {
		return indIntegrated;
	}

	/**
	 * Sets the ind integrated.
	 *
	 * @param indIntegrated the new ind integrated
	 */
	public void setIndIntegrated(Integer indIntegrated) {
		this.indIntegrated = indIntegrated;
	}
	 
 
		
}
