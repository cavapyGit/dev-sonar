package com.pradera.core.component.swift.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.swift.to.CashAccountsSearchTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.type.NegotiationMechanismStateType;
import com.pradera.model.accounts.type.ParticipantMechanismStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.accounts.type.ParticipantType;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.funds.BeginEndDay;
import com.pradera.model.funds.CashAccountDetail;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.type.AccountCashFundsType;
import com.pradera.model.funds.type.BankAccountsStateType;
import com.pradera.model.funds.type.BankType;
import com.pradera.model.funds.type.CashAccountStateType;
import com.pradera.model.funds.type.FundsType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.ModalityGroupDetail;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;

@Stateless
@Performance
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class CashAccountManagementService extends CrudDaoServiceBean implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;

	/**
	 * THIS METHOD IS FOR GETTING PARAMETER PK-DESCRIPTION FROM A MASTER TABLE
	 * PK RECEIPT
	 * 
	 * @param masterTablePK
	 * @return
	 * @throws Exception
	 */
	public List<ParameterTable> getParameterTableElements(Integer masterTablePK) throws ServiceException{
		List<ParameterTable> parameters = new ArrayList<ParameterTable>();
		try {
			TypedQuery<ParameterTable> query = em.createQuery("SELECT p FROM ParameterTable p WHERE p.masterTable.masterTablePk = :pk",ParameterTable.class);
			query.setParameter("pk", masterTablePK);
			parameters = query.getResultList();
		} catch (Exception ex) {
			ex.getMessage();
		}
		return parameters;
	}

	/**
	 * THIS METHOD IF FOR GETTING ISSUER-ENTITY IN ACTIVE STATE FOR FILLING
	 * COMBOS OR OTHER FUNCIONALITY
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Issuer> getActivateIssuer() throws ServiceException {
		List<Issuer> parameters = new ArrayList<Issuer>();
		try {
			TypedQuery<Issuer> query = em.createQuery(
					"SELECT i FROM Issuer i WHERE i.stateIssuer = :state order by i.description ",Issuer.class);
			query.setParameter("state", new Integer(MasterTableType.PARAMETER_TABLE_ISSUER_STATE_ACTIVATE.getLngCode().toString()));
			parameters = query.getResultList();
		} catch (Exception ex) {
			ex.getMessage();
		}
		return parameters;
	}

	/**
	 * THIS METHOD IF FOR GETTING PARTICIPANT-ENTITY IN REGISTER STATE FOR
	 * FILLING COMBOS OR OTHER FUNCIONALITY
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Participant> getRegisterParticipant() throws ServiceException {
		List<Participant> parameters = new ArrayList<Participant>();
		TypedQuery<Participant> query = em
				.createQuery(
							"SELECT distinct p FROM Participant p, ParticipantMechanism pm " +
							"WHERE " +
							"pm.participant.idParticipantPk = p.idParticipantPk  " +
							"AND pm.stateParticipantMechanism = :stateParticipantMechanism " +
							"and p.state = :state " +
							"order by p.description ",Participant.class);

		query.setParameter("state", new Integer(MasterTableType.PARAMETER_TABLE_PARTICIPANT_STATE_REGISTER.getLngCode().toString()));
		query.setParameter("stateParticipantMechanism", ParticipantMechanismStateType.REGISTERED.getCode());
		parameters = query.getResultList();
		return parameters;
	}

	/**
	 * THIS METHOD IS FOR GETTING BANK-ENTITY IN CONFIRMED STATE FOR FILLING
	 * COMBOS OR OTHER FUNCTIONALITY
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Bank> getConfirmedBank() throws ServiceException {
		List<Bank> parameters = new ArrayList<Bank>();
		try {
			TypedQuery<Bank> query = em.createQuery("SELECT b FROM Bank b WHERE b.state = :state ORDER BY b.description", Bank.class);
			query.setParameter("state",MasterTableType.PARAMETER_TABLE_BANK_STATE_REGISTERED.getCode());
			parameters = query.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return parameters;
	}

	/**
	 * THIS METHOD IS FOR GETTING BANK-ENTITY IN CONFIRMED STATE AND COMMERCIAL
	 * ONES
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Bank> getConfirmedCommercialBank() throws ServiceException {
		List<Bank> parameters = new ArrayList<Bank>();
		try {
			TypedQuery<Bank> query = em
					.createQuery("SELECT b FROM Bank b WHERE b.state = :state " +
							//"and b.bankType = :class " +
							"and b.bankOrigin = :bankOrigin " +
							"ORDER BY b.description ASC",Bank.class);
			query.setParameter("state",MasterTableType.PARAMETER_TABLE_BANK_STATE_REGISTERED.getCode());
			//query.setParameter("class",BankType.COMMERCIAL.getCode());
			query.setParameter("bankOrigin",new Integer(MasterTableType.PARAMETER_TABLE_BANK_ORIGIN_NATIONAL.getLngCode().toString()));
			parameters = query.getResultList();
		} catch (Exception ex) {
			ex.getMessage();
		}
		return parameters;
	}
	
	public Bank getBCBBank(){
		try {
			StringBuilder sbQuery = new StringBuilder(); 
			sbQuery.append("SELECT b FROM Bank b");
			sbQuery.append("	WHERE b.bankType = :bankType");
			sbQuery.append("	AND b.state = :state");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("bankType",BankType.CENTRALIZING.getCode());
			query.setParameter("state",MasterTableType.PARAMETER_TABLE_BANK_STATE_REGISTERED.getCode());
			return (Bank)query.getSingleResult();
		} catch (NoResultException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * THIS METHOD IS FOR GETTING NEGOTIATION_MECHANISM ENTITY FOR COMBOS
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<NegotiationMechanism> getMechanism() throws ServiceException {
		List<NegotiationMechanism> parameters = new ArrayList<NegotiationMechanism>();
		TypedQuery<NegotiationMechanism> query = em.createQuery("SELECT n FROM NegotiationMechanism n",NegotiationMechanism.class);
			parameters = query.getResultList();
		return parameters;
	}

	/**
	 * THIS METHOD IS FOR GETTING NEGOTIATION_MODALITY ENTITY FOR COMBOS
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<ModalityGroup> getModalityGroup() throws ServiceException {
		List<ModalityGroup> parameters = new ArrayList<ModalityGroup>();
		TypedQuery<ModalityGroup> query = em.createQuery("SELECT n FROM ModalityGroup n", ModalityGroup.class);
		parameters = query.getResultList();
		return parameters;
	}

	/**
	 * METHOD WILL SEARCH BANK IN TABLE:BANK JUST BY TYPE
	 * 
	 * @param bankType
	 * @return
	 */
	public List<Bank> fillBankByType(Integer bankType) {
		List<Bank> parameters = new ArrayList<Bank>();
		try {
			TypedQuery<Bank> query = em.createQuery("SELECT b FROM Bank b WHERE b.bankType = :bankType " +
					"and b.bankOrigin = :bankOrigin " +
					"and b.state = :state " +
					"order by b.description ASC",Bank.class);
			query.setParameter("bankType", bankType);
			query.setParameter("state",MasterTableType.PARAMETER_TABLE_BANK_STATE_REGISTERED.getCode());
			query.setParameter("bankOrigin",new Integer(MasterTableType.PARAMETER_TABLE_BANK_ORIGIN_NATIONAL.getLngCode().toString()));
			parameters = query.getResultList();
		} catch (Exception ex) {
			ex.getMessage();
		}
		return parameters;
	}

	/**
	 * METHOD WHICH WILL BRING BCRD BANK PK
	 * 
	 * @return
	 */
	public Long getBCBBank(Integer currency,String bankId) {
		Bank bank = new Bank();
		try{		
			TypedQuery<Bank> query = em.createQuery(
					"SELECT  DISTINCT b FROM Bank b, InstitutionBankAccount i " +
					"WHERE " +
					"b.bankType = :bankType and " +
					"b.state = :state and " +
					"b.idBankPk = i.providerBank.idBankPk and " +
					"i.currency = :currency and " +
					"i.state = :instBankAccState and "+
					" i.providerBank.idBankPk=:providerBank and "+
					" i.bank.idBankPk=:providerBank ",
					Bank.class);
			query.setParameter("bankType",BankType.CENTRALIZING.getCode());
			query.setParameter("state",MasterTableType.PARAMETER_TABLE_BANK_STATE_REGISTERED.getCode());
			query.setParameter("instBankAccState", BankAccountsStateType.REGISTERED.getCode());
			query.setParameter("currency",currency);
			query.setParameter("providerBank",Long.valueOf(bankId));
			bank = query.getSingleResult();
		}catch(NoResultException x){
			x.printStackTrace();
		}
		return bank.getIdBankPk();
	}

	/**
	 * GET THE BCRD ACCOUNT IF IT EXISTS TO SET THE ACCOUNT NUMBER
	 * @param bank
	 * @param currency
	 * @return
	 */
	public InstitutionBankAccount getBcrdBank(Integer bank, Integer currency){
		InstitutionBankAccount objBank = new InstitutionBankAccount();
		try {
			TypedQuery<InstitutionBankAccount> query = em.createQuery(
					"SELECT i FROM Bank b,InstitutionBankAccount i WHERE b.bankType = :bankType and b.state = :state " +
					"	and b.idBankPk = i.clientBank.idBankPk and i.currency = :currency and b.idBankPk = :bank" +
					"	and i.state = :instBankAccState"
					,InstitutionBankAccount.class);
			query.setParameter("bankType",AccountCashFundsType.CENTRALIZING.getCode());
			query.setParameter("state",MasterTableType.PARAMETER_TABLE_BANK_STATE_REGISTERED.getLngCode());
			query.setParameter("bank",Long.valueOf(bank.toString()));
			query.setParameter("instBankAccState", new Long(CashAccountStateType.ACTIVATE.getCode()));
			query.setParameter("currency",currency);
			objBank = query.getSingleResult();
		} catch (Exception ex) {
			ex.getMessage();
		}
		return objBank;
	}

	/**
	 * METHOD WHICH WILL BRING INSTITUTION_CASH_ACCOUNT ENTITY OF BCRD IN ACTIVE
	 * STATE
	 * 
	 * @return
	 */
	public InstitutionCashAccount getBcrdIntitutionCashAccount() {
		InstitutionCashAccount bcrdCashAccount = new InstitutionCashAccount();
		try {
			TypedQuery<InstitutionCashAccount> query = em.createQuery(
							"SELECT i FROM InstitutionCashAccount i WHERE i.accountState = :state and i.accountType = :accType",
							InstitutionCashAccount.class);
			query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
			query.setParameter("accType",AccountCashFundsType.CENTRALIZING.getCode());
			bcrdCashAccount = query.getSingleResult();
		} catch (Exception ex) {
			ex.getMessage();
		}
		return bcrdCashAccount;
	}

	public List<InstitutionBankAccount> getInstitutionBankAccountData(Long bankPK, Integer currency,Long depositaryPk) {
		List<InstitutionBankAccount> result = new ArrayList<InstitutionBankAccount>();
		try {
//			StringBuilder sbQuery = new StringBuilder();
//			sbQuery.append("	SELECT i FROM InstitutionBankAccount i");
//			sbQuery.append("	WHERE i.state = :state");
//			if(blForDepositary){
//				sbQuery.append("	AND i.providerBank.idBankPk = :pk");
//				sbQuery.append("	AND i.participant.idParticipantPk = :edv");
//			}else
//				sbQuery.append("	AND i.providerBank.idBankPk = :pk");
//			sbQuery.append("	AND i.currency = :currency");
//			TypedQuery<InstitutionBankAccount> query = em.createQuery(sbQuery.toString(),InstitutionBankAccount.class);
//			query.setParameter("pk", new Long(bankPK));
//			query.setParameter("state", BankAccountsStateType.REGISTERED.getCode());
//			query.setParameter("currency", currency);
//			if(blForDepositary)
//				query.setParameter("edv", idepositarySetup.getIdParticipantDepositary());
//			result = query.getResultList();
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT iba FROM InstitutionBankAccount iba");
			sbQuery.append("	INNER JOIN FETCH iba.bank b");
			sbQuery.append("	INNER JOIN FETCH iba.providerBank pb");
			sbQuery.append("	WHERE iba.state = :state");
			sbQuery.append("	AND b.idBankPk = :bankPK");
			sbQuery.append("	AND pb.idBankPk = :depositaryPk");
			sbQuery.append("	AND iba.currency = :currency");
			TypedQuery<InstitutionBankAccount> query = em.createQuery(sbQuery.toString(),InstitutionBankAccount.class);
			query.setParameter("bankPK", bankPK);
			query.setParameter("depositaryPk", depositaryPk);
			query.setParameter("state", BankAccountsStateType.REGISTERED.getCode());
			query.setParameter("currency", currency);
			result = query.getResultList();
		} catch (Exception ex) {
			ex.getMessage();
		}
		return result;
	}
	
	public List<InstitutionBankAccount> getIssuerBankAccount(String issuerSelected, Integer currency, Long idBank) {
		List<InstitutionBankAccount> result = new ArrayList<InstitutionBankAccount>();
		try {
			StringBuilder strQuery = new StringBuilder();
			strQuery.append("	SELECT iba");
			strQuery.append("	FROM InstitutionBankAccount iba");
			strQuery.append("	INNER JOIN FETCH iba.providerBank");
			strQuery.append("	WHERE iba.currency = :currency");
			strQuery.append("	AND iba.issuer.idIssuerPk = :idIssuerPk");
			strQuery.append("	AND iba.state = :state");
			strQuery.append("	AND iba.providerBank.idBankPk = :bankprovider");
			TypedQuery<InstitutionBankAccount> query = em.createQuery(strQuery.toString(),InstitutionBankAccount.class);
			query.setParameter("currency", currency);
			query.setParameter("idIssuerPk", issuerSelected);
			query.setParameter("state", BankAccountsStateType.REGISTERED.getCode());
			query.setParameter("bankprovider", idBank);
			result=  query.getResultList();
			return result;
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public InstitutionBankAccount findIssuerBankAccount(Long idInstitutionAccountPk) {
		try {
			StringBuilder strQuery = new StringBuilder();
			strQuery.append("	SELECT iba");
			strQuery.append("	FROM InstitutionBankAccount iba");
			strQuery.append("	INNER JOIN FETCH iba.providerBank");
			strQuery.append("	WHERE iba.idInstitutionBankAccountPk = :idInstitutionBankAccountPk");
			Query query = em.createQuery(strQuery.toString());
			query.setParameter("idInstitutionBankAccountPk", idInstitutionAccountPk);
			return (InstitutionBankAccount)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<InstitutionCashAccount> getCashAccounts(CashAccountsSearchTO parameters) throws ServiceException{
		List<InstitutionCashAccount> lstResult = new ArrayList<InstitutionCashAccount>();

		String queryStr = 
				"SELECT i FROM InstitutionCashAccount i " +
				"LEFT JOIN FETCH i.negotiationMechanism " +
				"LEFT JOIN FETCH i.modalityGroup " +
				"LEFT JOIN FETCH i.participant " +
				"LEFT JOIN FETCH i.issuer " +
				"WHERE i.accountType = :accType ";

		if (Validations.validateIsNotNullAndPositive(parameters.getCurrency())){
			queryStr = queryStr.concat("and i.currency = :currency ");
		}
		if (Validations.validateIsNotNullAndPositive(parameters.getState())){
			queryStr = queryStr.concat("and i.accountState = :state ");
		}
		if (Validations.validateIsNotNullAndPositive(parameters.getParticipant())){
			queryStr = queryStr.concat("and i.participant.idParticipantPk = :participant ");
		}
		if (Validations.validateIsNotNullAndNotEmpty(parameters.getIssuer()) 
				&& !parameters.getIssuer().equals(GeneralConstants.ZERO_VALUE_STRING)){
			queryStr = queryStr.concat("and i.issuer.idIssuerPk = :issuer ");
		}
		
		if(Validations.validateIsNotNull(parameters.getIdModalityGroupPk()))
			if(Validations.validateIsNotNullAndPositive(parameters.getIdModalityGroupPk().intValue()))
				queryStr = queryStr.concat("and i.modalityGroup.idModalityGroupPk = :idModalityGroupPk ");
		if(Validations.validateIsNotNull(parameters.getIdNegoMechanismPk()))
			if(Validations.validateIsNotNullAndPositive(parameters.getIdNegoMechanismPk().intValue()))
				queryStr = queryStr.concat("and i.negotiationMechanism.idNegotiationMechanismPk = :idNegotiationMechanismPk ");
		
		queryStr = queryStr.concat("order by i.idInstitutionCashAccountPk DESC");
		TypedQuery<InstitutionCashAccount> query = em.createQuery(queryStr,InstitutionCashAccount.class);
		query.setParameter("accType",parameters.getCashAccountType());
		if (Validations.validateIsNotNullAndPositive(parameters.getCurrency())){
			query.setParameter("currency",parameters.getCurrency());
		}
		if (Validations.validateIsNotNullAndPositive(parameters.getState())){
			query.setParameter("state", parameters.getState());
		}
		if (Validations.validateIsNotNullAndPositive(parameters.getParticipant())){
			query.setParameter("participant",Long.valueOf(parameters.getParticipant()));
		}
		if (Validations.validateIsNotNullAndNotEmpty(parameters.getIssuer()) 
				&& !parameters.getIssuer().equals(GeneralConstants.ZERO_VALUE_STRING)){
			query.setParameter("issuer", parameters.getIssuer());
		}
		if(Validations.validateIsNotNull(parameters.getIdModalityGroupPk()))
			if(Validations.validateIsNotNullAndPositive(parameters.getIdModalityGroupPk().intValue()))
				query.setParameter("idModalityGroupPk", parameters.getIdModalityGroupPk());
		if(Validations.validateIsNotNull(parameters.getIdNegoMechanismPk()))
			if(Validations.validateIsNotNullAndPositive(parameters.getIdNegoMechanismPk().intValue()))
				query.setParameter("idNegotiationMechanismPk", parameters.getIdNegoMechanismPk());
		lstResult = query.getResultList();

		return lstResult;
	}

	/**
	 * METHOD WHICH GET ALL MECHANISM DESCRIPTIONS
	 * 
	 * @return
	 */
	public List<NegotiationMechanism> getMechanismElements() {
		List<NegotiationMechanism> returnLst = new ArrayList<NegotiationMechanism>();
		try {
			TypedQuery<NegotiationMechanism> query = em.createQuery("select n from NegotiationMechanism n",NegotiationMechanism.class);

			returnLst = query.getResultList();
		} catch (Exception ex) {
			ex.getMessage();
		}
		return returnLst;
	}

	/**
	 * METHOD WHICH GET ALL MODALITIES DESCRIPTIONS
	 * 
	 * @return
	 */
	public List<ModalityGroup> getModalityElements() {
		List<ModalityGroup> returnLst = new ArrayList<ModalityGroup>();
		try {
			TypedQuery<ModalityGroup> query = em.createQuery(
					"select m from ModalityGroup m", ModalityGroup.class);

			returnLst = query.getResultList();
		} catch (Exception ex) {
			ex.getMessage();
		}
		return returnLst;
	}

	/**
	 * METHOD WHICH GET ALL PARTICIPANT DESCRIPTIONS
	 * 
	 * @return
	 */
	public Map<Long, String> getParticipantDescriptions() {
		List<Participant> returnLst = new ArrayList<Participant>();
		Map<Long, String> mpReturn = new HashMap<Long, String>();
		try {
			TypedQuery<Participant> query = em.createQuery(
					"select p from Participant p", Participant.class);

			returnLst = query.getResultList();
			if (returnLst != null && returnLst.size() > 0) {
				for (Iterator<Participant> itr = returnLst.iterator(); itr
						.hasNext();) {
					Participant p = (Participant) itr.next();
					mpReturn.put(p.getIdParticipantPk(), p.getMnemonic());
				}
			}
		} catch (Exception ex) {
			ex.getMessage();
		}
		return mpReturn;
	}

	public InstitutionCashAccount getActivatedCashAccount(InstitutionCashAccount obj) {
		InstitutionCashAccount accountActivated = null;
		String sql = "select i from InstitutionCashAccount i where i.accountState = :state and i.accountType = :type and i.indRelatedBcrd = :indCentral ";
		if (obj.getCurrency() != null) {
			sql = sql.concat("and i.currency = :currency ");
		}
		if (obj.getIssuer() != null) {
			sql = sql.concat("and i.issuer.idIssuerPk = :issuer ");
		}
		if (obj.getModalityGroup() != null) {
			sql = sql.concat("and i.modalityGroup.idModalityGroupPk = :modality ");
		}
		if (obj.getNegotiationMechanism() != null) {
			sql = sql.concat("and i.negotiationMechanism.idNegotiationMechanismPk = :mechanism ");
		}
		if (obj.getParticipant() != null) {
			sql = sql.concat("and i.participant.idParticipantPk = :participant ");
		}

		TypedQuery<InstitutionCashAccount> query = em.createQuery(sql,InstitutionCashAccount.class);
		query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
		query.setParameter("type", obj.getAccountType());
		query.setParameter("indCentral", obj.getIndRelatedBcrd());

		if (obj.getCurrency() != null) {
			query.setParameter("currency", obj.getCurrency());
		}
		if (obj.getIssuer() != null) {
			query.setParameter("issuer", obj.getIssuer().getIdIssuerPk());
		}
		if (obj.getModalityGroup() != null) {
			query.setParameter("modality", obj.getModalityGroup().getIdModalityGroupPk());
		}
		if (obj.getNegotiationMechanism() != null) {
			query.setParameter("mechanism", obj.getNegotiationMechanism().getIdNegotiationMechanismPk());
		}
		if (obj.getParticipant() != null) {
			query.setParameter("participant", obj.getParticipant().getIdParticipantPk());
		}
		List<InstitutionCashAccount> lst = query.getResultList();
		if (lst != null && lst.size() > 0) {
			accountActivated = lst.get(0);
		}
		return accountActivated;
	}

	/**
	 * This method is used to get the Institution bank accounts data
	 * 
	 * @param idCashAccount
	 * @return List of Bank Accounts
	 */
	@SuppressWarnings("unchecked")
	public List<InstitutionBankAccount> getInstituteBankDetails(Long idCashAccount) {
		List<InstitutionBankAccount> lstBankAct = new ArrayList<InstitutionBankAccount>();
		String queryStr = "SELECT ib FROM InstitutionBankAccount ib,InstitutionCashAccount ic WHERE ib.institutionCashAccount.idInstitutionCashAccountPk=ic.idInstitutionCashAccountPk and ic.idInstitutionCashAccountPk = :id ";
		Query query = em.createQuery(queryStr);
		query.setParameter("id", idCashAccount);
		lstBankAct = (List<InstitutionBankAccount>) query.getResultList();
		return lstBankAct;

	}
	
	/**
	 * This method is used to get the Institution bank accounts data
	 * 
	 * @param idCashAccount
	 * @return List of Bank Accounts
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getBenefitCashActDetails(Long idCashAccount) {
		List<Object[]> lstBankAct = new ArrayList<Object[]>();
		String queryStr = "SELECT ib,ib.institutionCashAccount,ib.institutionCashAccount.issuer FROM InstitutionBankAccount ib,InstitutionCashAccount ic WHERE ib.institutionCashAccount.idInstitutionCashAccountPk=ic.idInstitutionCashAccountPk and ic.idInstitutionCashAccountPk = :id ";
		Query query = em.createQuery(queryStr);
		query.setParameter("id", idCashAccount);
		lstBankAct = (List<Object[]>) query.getResultList();
		return lstBankAct;

	}
	
	/**
	 * This method is used to get the Institution bank accounts data
	 * 
	 * @param idCashAccount
	 * @return List of Bank Accounts
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getReturnCashActDetails(Long idCashAccount) {
		List<Object[]> lstBankAct = new ArrayList<Object[]>();
		String queryStr = "SELECT ib,ib.institutionCashAccount FROM InstitutionBankAccount ib,InstitutionCashAccount ic WHERE ib.institutionCashAccount.idInstitutionCashAccountPk=ic.idInstitutionCashAccountPk and ic.idInstitutionCashAccountPk = :id ";
		Query query = em.createQuery(queryStr);
		query.setParameter("id", idCashAccount);
		lstBankAct = (List<Object[]>) query.getResultList();
		return lstBankAct;

	}

	/**
	 * This method is used to get the Institution Cash Accounts data.
	 * 
	 * @param idCashAccount
	 * @return List of Institution cash Accounts
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getInstituteCashAccountDetails(Long idCashAccount) {
		List<Object[]> lstCashAct = new ArrayList<Object[]>();
		String queryStr = "SELECT ic.modalityGroup,ic.negotiationMechanism,ic.institutionBankAccounts,ic.participant FROM InstitutionCashAccount ic where ic.idInstitutionCashAccountPk = :id ";
		Query query = em.createQuery(queryStr);
		query.setParameter("id", idCashAccount);
		lstCashAct = (List<Object[]>) query.getResultList();
		return lstCashAct;

	}

	/**
	 * This method is used to get the central bank BIC Code.
	 * 
	 * @return BCRD bank BIC Code
	 */
	public String getBcbBankBicValue(Integer currency){
		Bank bank = new Bank();
		try{
			String strQuery =
					"SELECT b FROM " +
					"Bank b, InstitutionBankAccount i " +
					"WHERE " +
					"b.bankType = :bankType and " +
					"b.state = :state and " +
					"b.idBankPk = i.bank.idBankPk ";
			if(Validations.validateIsNotNullAndPositive(currency)){
				strQuery += "and i.currency = :currency ";
			}
			TypedQuery<Bank> query = em.createQuery(strQuery,Bank.class);
			query.setParameter("bankType",BankType.CENTRALIZING.getCode());
			query.setParameter("state",MasterTableType.PARAMETER_TABLE_BANK_STATE_REGISTERED.getCode());
			if(Validations.validateIsNotNullAndPositive(currency)){
				query.setParameter("currency",currency);
			}
			bank = query.getSingleResult();
		}catch(NoResultException x){
			x.printStackTrace();
		}
		return bank.getBicCode();
	}

	/**
	 * METHOD WHICH WILL BE INCHARGE TO TAKE PARTICIPANT BIC CODE
	 * @param participant
	 * @return
	 */
	public String getParticipantBicCode(Integer participant){
		String bic = StringUtils.EMPTY;
		String sqlStr = "select p.bicCode from Participant p where p.idParticipantPk = :pk ";
		Query query =em.createQuery(sqlStr);
		query.setParameter("pk", Long.valueOf(participant.toString()));
		Object obj = query.getSingleResult();
		if(Validations.validateIsNotNull(obj)){
			bic = obj.toString();
		}

		return bic;
	}

	/**
	 * METHOD WHICH WILL VALIDATE IF THE SELECTED BANK IS A CENTRAL ONE
	 * @param bank
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public boolean isCentralBank(Integer bank){
		boolean indCentralBank = false;
		String sqlStr = "select b from Bank b where b.idBankPk = :pk and b.bankType = :bankType ";
		Query query =em.createQuery(sqlStr);
		query.setParameter("pk", Long.valueOf(bank.toString()));
		query.setParameter("bankType", AccountCashFundsType.CENTRALIZING.getCode());
		List<Bank> lstCentral = query.getResultList();
		if(Validations.validateListIsNotNullAndNotEmpty(lstCentral)){
			indCentralBank=true;
		}
		return indCentralBank;
	}

	/**
	 * METHOD WHICH WILL VALIDATE THE RELATIONSHIP MECHANISM - MODALITY TO SAVE AN INSTITUTION CASH ACCOUNT
	 * @param mechanism
	 * @param modality
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public boolean validateMechanismModality(String mechanism,String modality){
		boolean existRelationship = false;
		String sqlStr = 
					"select mgd from ModalityGroupDetail mgd " +
					"where mgd.negotiationMechanism.idNegotiationMechanismPk = :mechanism and " +
					"mgd.modalityGroup.idModalityGroupPk = :modality ";

		Query query =em.createQuery(sqlStr);
		query.setParameter("mechanism", Long.valueOf(mechanism));
		query.setParameter("modality", Long.valueOf(modality));
		List<ModalityGroupDetail> lstRelation = query.getResultList();
		if(Validations.validateListIsNotNullAndNotEmpty(lstRelation)){
			existRelationship=true;
		}
		return existRelationship;
	}

	/**
	 * METHOD WHICH WILL BRING BCB BANK
	 * 
	 * @return
	 */
	public Bank getCentralizerBank() {
		Bank bank = null;
		try{
			TypedQuery<Bank> query = em.createQuery("SELECT b FROM Bank b WHERE b.bankType = :bankType and b.state = :state",Bank.class);
			query.setParameter("bankType",BankType.CENTRALIZING.getCode());
			query.setParameter("state",MasterTableType.PARAMETER_TABLE_BANK_STATE_REGISTERED.getCode());
			bank = query.getSingleResult();
		}catch(NoResultException x){
			x.printStackTrace();
		}
		return bank;
	}

	/**
	 * METHOD WHICH WILL GET THE DEPOSITARY PARTICIPANT
	 * @return
	 */
	public Participant getDepositaryParticipant(){
		Participant cevaldom = null;
		String sqlStr = "select p from Participant p where p.indParticipantDepository = :indDepositary and p.state = :state ";
		Query query =em.createQuery(sqlStr);
		query.setParameter("indDepositary", FundsType.IND_DEPOSITARY_PARTICIPANT.getCode());
		query.setParameter("state", ParticipantStateType.REGISTERED.getCode());
		cevaldom = (Participant)query.getSingleResult();

		return cevaldom;
	}

	/**
	 * METHOD TO GET THE CENTRALIZER CASH ACCOUNT TO RELATED TO VIRTUAL CASH ACCOUNTS 
	 * @param currency
	 * @return
	 */
	public InstitutionCashAccount getCentralizerAccount (Integer currency){
		InstitutionCashAccount centralizerCashAccount = new InstitutionCashAccount();
		try{
			String sqlStr = "select i from InstitutionCashAccount i where i.currency = :currency and i.accountType = :accountType and i.accountState = :state ";
			Query query =em.createQuery(sqlStr);
			query.setParameter("currency", currency);
			query.setParameter("state", CashAccountStateType.ACTIVATE.getCode());
			query.setParameter("accountType", AccountCashFundsType.CENTRALIZING.getCode());
			centralizerCashAccount = (InstitutionCashAccount) query.getSingleResult();
		}catch(NoResultException x){
			x.printStackTrace();
		}
		return centralizerCashAccount;
	}

	public InstitutionBankAccount getBankAccountDetail(Long idCashAccount){
		InstitutionBankAccount institutionBankAccount = new InstitutionBankAccount();
		try{
			String queryStr = "SELECT ib FROM InstitutionBankAccount ib,InstitutionCashAccount ic " +
					"WHERE " +
					"ib.institutionCashAccount.idInstitutionCashAccountPk=ic.idInstitutionCashAccountPk and ic.idInstitutionCashAccountPk = :id ";
			Query query = em.createQuery(queryStr);
			query.setParameter("id", idCashAccount);
			institutionBankAccount = (InstitutionBankAccount) query.getSingleResult();
		}catch(NonUniqueResultException x){
			x.printStackTrace();
		}
		return institutionBankAccount;
	}

	/**
	 * METHOD TO GET THE BANK ACCOUNTS RELATED TO CASH ACCOUNTS
	 * @param idCashAccount
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<InstitutionBankAccount> getBankAccounts(Long idCashAccount){
		List<InstitutionBankAccount> lstBankAct = new ArrayList<InstitutionBankAccount>();
		String queryStr = 
				"SELECT ib FROM InstitutionBankAccount ib,InstitutionCashAccount ic" +
				"	INNER JOIN FETCH ib.providerBank" +
				"	WHERE ib.institutionCashAccount.idInstitutionCashAccountPk=ic.idInstitutionCashAccountPk and ic.idInstitutionCashAccountPk = :id " +
				"	and ib.state = :bankAccountState ";
		Query query = em.createQuery(queryStr);
		query.setParameter("id", idCashAccount);
		query.setParameter("bankAccountState", new Long(CashAccountStateType.ACTIVATE.getCode()));
		lstBankAct = (List<InstitutionBankAccount>) query.getResultList();
		return lstBankAct;

	}

	@SuppressWarnings("unchecked")
	public List<ModalityGroup> getLstModaGroup(Long idNegoMechanismPkSelected) throws ServiceException{
		String strQuery = "		SELECT mg" +
					"	FROM ModalityGroup mg" +
					"	WHERE mg.idModalityGroupPk in (" +
					"		SELECT mgd.modalityGroup.idModalityGroupPk" +
					"			FROM ModalityGroupDetail mgd" +
					"			WHERE mgd.negotiationMechanism.idNegotiationMechanismPk = :idNegoMechanismPk)" +
					"	ORDER BY mg.description ASC";
		Query query = em.createQuery(strQuery);
		query.setParameter("idNegoMechanismPk", idNegoMechanismPkSelected);
		return (List<ModalityGroup>)query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<NegotiationMechanism> getLstMechanism(Long idParticipantPk) throws ServiceException{
		String strQuery = "		SELECT nm" +
					"	FROM NegotiationMechanism nm" +
					"	WHERE nm.idNegotiationMechanismPk in (" +
					"		SELECT pm.mechanismModality.id.idNegotiationMechanismPk" +
					"			FROM ParticipantMechanism pm" +
					"			WHERE pm.participant.idParticipantPk = :idParticipantPk" +
					"			AND pm.stateParticipantMechanism = :stateParticipantMechanism)" +
					"	AND nm.stateMechanism = :stateMechanism";
		Query query = em.createQuery(strQuery);
		query.setParameter("idParticipantPk", idParticipantPk);
		query.setParameter("stateMechanism", NegotiationMechanismStateType.ACTIVE.getCode());
		query.setParameter("stateParticipantMechanism", ParticipantMechanismStateType.REGISTERED.getCode());
		return (List<NegotiationMechanism>)query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<ModalityGroup> getLstModaGroup(Long idNegoMechanismPk, Long idParticipantPk) throws ServiceException{
			String strQuery = "		SELECT distinct mg" +
							"	FROM ModalityGroup mg" +
							"	WHERE mg.idModalityGroupPk in (" +
							"		SELECT mgd.modalityGroup.idModalityGroupPk" +
							"			FROM ModalityGroupDetail mgd," +
							"				ParticipantMechanism pm" +
							"			WHERE mgd.negotiationMechanism.idNegotiationMechanismPk = :idNegoMechanismPk" +
							"			AND pm.participant.idParticipantPk = :idParticipantPk" +
							"			AND pm.mechanismModality.id.idNegotiationMechanismPk = mgd.mechanismModality.id.idNegotiationMechanismPk" +
							"			AND pm.mechanismModality.id.idNegotiationModalityPk = mgd.mechanismModality.id.idNegotiationModalityPk)" +
							"	ORDER BY mg.description ASC";
		Query query = em.createQuery(strQuery);
		query.setParameter("idNegoMechanismPk", idNegoMechanismPk);
		query.setParameter("idParticipantPk", idParticipantPk);
		return (List<ModalityGroup>)query.getResultList();
	}

	
	@SuppressWarnings("unchecked")
	public List<Participant> getParticipants(boolean blDirect) throws ServiceException{
		String strQuery = "		SELECT distinct pa" +
							"	FROM Participant pa, ParticipantMechanism pm" +
							"	WHERE pa.state = :state" +							
							"	AND pm.participant.idParticipantPk = pa.idParticipantPk" +
							"	AND pm.stateParticipantMechanism = :stateParticipantMechanism"+ 
							"	AND pa.indDepositary = :indDepositary";
		if(blDirect)
			strQuery += "	AND pa.accountType = :accountType";
		strQuery += "	ORDER BY pa.mnemonic ASC";
		Query query = em.createQuery(strQuery);
		query.setParameter("state", ParticipantStateType.REGISTERED.getCode());
		query.setParameter("stateParticipantMechanism", ParticipantMechanismStateType.REGISTERED.getCode());
		query.setParameter("indDepositary", ComponentConstant.ONE);
		if(blDirect)
			query.setParameter("accountType", ParticipantType.DIRECT.getCode());
		return (List<Participant>)query.getResultList();
	}

	public InstitutionCashAccount getInstitutionCashAccountData(Long idCashAccount) throws ServiceException{
		try {
			StringBuilder strQuery = new StringBuilder();
			strQuery.append("SELECT ica");
			strQuery.append("	FROM InstitutionCashAccount ica");
			strQuery.append("	LEFT JOIN FETCH ica.issuer");
			strQuery.append("	LEFT JOIN FETCH ica.negotiationMechanism");
			strQuery.append("	LEFT JOIN FETCH ica.modalityGroup");
			strQuery.append("	LEFT JOIN FETCH ica.participant");
			strQuery.append("	LEFT JOIN FETCH ica.cashAccountDetailResults cad");
			strQuery.append("	INNER JOIN FETCH cad.institutionBankAccount iba");
			strQuery.append("	LEFT JOIN FETCH iba.providerBank pb");
			strQuery.append("	LEFT JOIN FETCH iba.bank b");
			strQuery.append("	WHERE ica.idInstitutionCashAccountPk = :idInstitutionCashAccountPk");	
			Query query = em.createQuery(strQuery.toString());
			query.setParameter("idInstitutionCashAccountPk", idCashAccount);			
			return (InstitutionCashAccount)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	/**
	 * Busqueda de cuntas de efectivo para cobros de derechos economicos
	 * 
	 * */
	public InstitutionCashAccount getInstitutionCashAccountDataByCollectionEconomic(Long idCashAccount) throws ServiceException{
		try {
			StringBuilder strQuery = new StringBuilder();
			strQuery.append("SELECT ica");
			strQuery.append("	FROM InstitutionCashAccount ica");
			strQuery.append("	LEFT JOIN FETCH ica.issuer");
			//strQuery.append("	LEFT JOIN FETCH ica.negotiationMechanism");
			//strQuery.append("	LEFT JOIN FETCH ica.modalityGroup");
			strQuery.append("	LEFT JOIN FETCH ica.participant");
			strQuery.append("	LEFT JOIN FETCH ica.cashAccountDetailResults cad");
			strQuery.append("	INNER JOIN FETCH cad.institutionBankAccount iba");
			strQuery.append("	LEFT JOIN FETCH iba.providerBank pb");
			strQuery.append("	LEFT JOIN FETCH iba.bank b");
			strQuery.append("	WHERE ica.idInstitutionCashAccountPk = :idInstitutionCashAccountPk");	
			Query query = em.createQuery(strQuery.toString());
			query.setParameter("idInstitutionCashAccountPk", idCashAccount);			
			
			InstitutionCashAccount account = (InstitutionCashAccount)query.getSingleResult();
			return account;
		} catch (NoResultException e) {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<InstitutionBankAccount> getInstitutionBanksAccount(Long idInstitutionCashAccountPk) throws ServiceException{
		StringBuilder strQuery = new StringBuilder();
		strQuery.append("	SELECT iba");	
		strQuery.append("	FROM InstitutionBankAccount iba");
		strQuery.append("	INNER JOIN FETCH iba.providerBank");
		strQuery.append("	WHERE iba.institutionCashAccount.idInstitutionCashAccountPk = :idInstitutionCashAccountPk");
		strQuery.append("	AND iba.state IN (:lstStates)");
		Query query = em.createQuery(strQuery.toString());
		query.setParameter("idInstitutionCashAccountPk", idInstitutionCashAccountPk);
		List<Long> lstStates = new ArrayList<Long>();
		lstStates.add(new Long(CashAccountStateType.ACTIVATE.getCode().toString()));
		lstStates.add(new Long(CashAccountStateType.REGISTERED.getCode().toString()));
		query.setParameter("lstStates", lstStates);
		return (List<InstitutionBankAccount>)query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> totalAndDepositAmount(Long idInstitutionCashAccountPk) throws ServiceException{
		StringBuilder strQuery = new StringBuilder();
		strQuery.append("	SELECT ica.totalAmount,");
		strQuery.append("			ica.depositAmount");
		strQuery.append("	FROM InstitutionCashAccount ica");
		strQuery.append("	WHERE ica.idInstitutionCashAccountPk = :idInstitutionCashAccountPk");
		Query query = em.createQuery(strQuery.toString());
		query.setParameter("idInstitutionCashAccountPk", idInstitutionCashAccountPk);
		return query.getResultList();
	}
		
	public BeginEndDay getBeginEndDay() throws ServiceException{
		try {
			StringBuilder strQuery = new StringBuilder();
			strQuery.append("	SELECT bed");
			strQuery.append("	FROM BeginEndDay bed");
			strQuery.append("	WHERE TRUNC(bed.processDay) = :day");
			Query query = em.createQuery(strQuery.toString());
			query.setParameter("day", CommonsUtilities.currentDate());
			return (BeginEndDay) query.getSingleResult();	
		} catch (NoResultException e) {
			return null;
		}
	}

	public boolean isOtherCashAccountWithAutomaticProcess(InstitutionCashAccount cashAccount) throws ServiceException{
		try {
			StringBuilder strQuery = new StringBuilder();
			strQuery.append("	SELECT ica");
			strQuery.append("	FROM InstitutionCashAccount ica");
			strQuery.append("	WHERE ica.accountType = :accountType");
			strQuery.append("	AND ica.currency = :currency");
			strQuery.append("	AND ica.indRelatedBcrd = :no");
			strQuery.append("	AND ica.accountState = :accountState");
			strQuery.append("	AND ica.indAutomaticProcess = :yes");
			Query query = em.createQuery(strQuery.toString());
			query.setParameter("accountType", cashAccount.getAccountType());
			query.setParameter("currency", cashAccount.getCurrency());
			query.setParameter("no", BooleanType.NO.getCode());			
			query.setParameter("accountState", CashAccountStateType.ACTIVATE.getCode());
			query.setParameter("yes", BooleanType.YES.getCode());
			if(Validations.validateIsNotNullAndNotEmpty(query.getSingleResult()))
				return true;
		} catch (NoResultException e) {
			return false;
		}
		return false;
	}
	
	public InstitutionCashAccount getCentralInstitutionCashAccount(Integer currency,Integer accountCashFundsType) throws ServiceException{
		try{
			String sqlStr = "select i from InstitutionCashAccount i where i.currency = :currency and i.accountType = :accountType and accountState = :accountState";
			Query query = em.createQuery(sqlStr);
			query.setParameter("currency", currency);
			query.setParameter("accountType", accountCashFundsType);
			query.setParameter("accountState", CashAccountStateType.ACTIVATE.getCode());
			return (InstitutionCashAccount) query.getSingleResult();
		} catch (NoResultException x){
			x.printStackTrace();
			return null;
		}
	}
	
	public CashAccountDetail getCentralCashAccountDetail(Long idInstitutionCashAccountPk) throws ServiceException{
		try{
			String sqlStr = "select cad from CashAccountDetail cad where cad.institutionCashAccount.idInstitutionCashAccountPk = :idInstitutionCashAccountPk ";
			Query query = em.createQuery(sqlStr);
			query.setParameter("idInstitutionCashAccountPk", idInstitutionCashAccountPk);
			return (CashAccountDetail) query.getSingleResult();
		} catch (NoResultException x){
			x.printStackTrace();
			return null;
		}
	}

	public InstitutionBankAccount getCentralInstitutionBankAccount(Integer currency) throws ServiceException{
		try {
			StringBuilder strQuery = new StringBuilder();
			strQuery.append("		SELECT iba");
			strQuery.append("	FROM InstitutionBankAccount iba");
			strQuery.append("	INNER JOIN FETCH iba.institutionCashAccount ica");
			strQuery.append("	WHERE ica.currency = :currency");
			strQuery.append("	AND ica.accountState = :accountState");
			strQuery.append("	AND ica.accountType = :accountType");
			Query query = em.createQuery(strQuery.toString());
			query.setParameter("currency", currency);
			query.setParameter("accountState", CashAccountStateType.ACTIVATE.getCode());
			query.setParameter("accountType", AccountCashFundsType.CENTRALIZING.getCode());
			return (InstitutionBankAccount) query.getSingleResult();
		} catch (NoResultException e) {
			e.printStackTrace();
			return null;
		}
	}

	public InstitutionBankAccount getParticipantBankAccount(Long participantSelected, Integer currency, Long idBank) {
		try {
			StringBuilder strQuery = new StringBuilder();
			strQuery.append("	SELECT iba");
			strQuery.append("	FROM InstitutionBankAccount iba");
			strQuery.append("	WHERE iba.currency = :currency");
			strQuery.append("	AND iba.participant.idParticipantPk = :idParticipantPk");
			strQuery.append("	AND iba.state = :state");
			strQuery.append("	AND iba.providerBank.idBankPk = :bankprovider");
			Query query = em.createQuery(strQuery.toString());
			query.setParameter("currency", currency);
			query.setParameter("idParticipantPk", participantSelected);
			query.setParameter("state", BankAccountsStateType.REGISTERED.getCode());
			query.setParameter("bankprovider", idBank);
			return (InstitutionBankAccount) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public List<InstitutionBankAccount> getParticipantBankAccounts(Long participantSelected, Integer currency, Long idBank) {
		List<InstitutionBankAccount> result = new ArrayList<InstitutionBankAccount>();
		try {
			StringBuilder strQuery = new StringBuilder();
			strQuery.append("	SELECT iba");
			strQuery.append("	FROM InstitutionBankAccount iba");
			strQuery.append("	INNER JOIN FETCH iba.providerBank");
			strQuery.append("	WHERE iba.currency = :currency");
			strQuery.append("	AND iba.participant.idParticipantPk = :idParticipantPk");
			strQuery.append("	AND iba.state = :state");
			strQuery.append("	AND iba.providerBank.idBankPk = :bankprovider");
			TypedQuery<InstitutionBankAccount> query = em.createQuery(strQuery.toString(),InstitutionBankAccount.class);
			query.setParameter("currency", currency);
			query.setParameter("idParticipantPk", participantSelected);
			query.setParameter("state", BankAccountsStateType.REGISTERED.getCode());
			query.setParameter("bankprovider", idBank);
			result=  query.getResultList();
			return result;
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public Issuer findIssuer(String idIssuer)throws ServiceException{
		try{
		return find(Issuer.class, idIssuer);
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	
	public List<Bank> fillSettlementBank(Long idBankPk ) {
		List<Bank> parameters = new ArrayList<Bank>();
		Long idBankTemp=new Long(0);
		int count=0;
		try {
			StringBuilder strQuery = new StringBuilder();
			strQuery.append("	SELECT iba");
			strQuery.append("	FROM InstitutionBankAccount iba");
			strQuery.append("	INNER JOIN FETCH iba.providerBank pb");
			strQuery.append("	INNER JOIN FETCH iba.bank b");
			strQuery.append("	WHERE iba.state = :state");
			strQuery.append("	AND pb.idBankPk = :providerBank");
			strQuery.append("	AND b.idBankPk <> :bank");
			Query query = em.createQuery(strQuery.toString());
			query.setParameter("providerBank", idBankPk);
			query.setParameter("bank", idBankPk);
			query.setParameter("state", BankAccountsStateType.REGISTERED.getCode());
			List<InstitutionBankAccount> lstIba= query.getResultList();
			for(InstitutionBankAccount institutionBankAccount :lstIba){	
				count++;
				if(count==1){
					parameters.add(institutionBankAccount.getBank());
					idBankTemp=institutionBankAccount.getBank().getIdBankPk();
				}else{
					if(!institutionBankAccount.getBank().getIdBankPk().equals(idBankTemp)){
						if(lstIba.size()==count){
							parameters.add(institutionBankAccount.getBank());
						}
						else{
							idBankTemp=institutionBankAccount.getBank().getIdBankPk();	
							parameters.add(institutionBankAccount.getBank());
						}
					}
				}
			}
		} catch (Exception ex) {
			ex.getMessage();
		}
		return parameters;
	}
		
	
	public List<Object> getReportingInconsist() throws ServiceException{
		/**List to handle all inconsistencies on reporting*/
		List<Object> inconsistenciesList = new ArrayList<Object>();
		
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append("Select mo.ballot_number|| '/' ||mo.sequential																		");
		sbQuery.append("From settlement_account_marketfact sam                                                                              ");
		sbQuery.append("Inner Join settlement_account_operation sao on sao.id_settlement_account_pk = sam.id_settlement_account_fk          ");
		sbQuery.append("Inner Join holder_account_operation hao on hao.id_holder_account_operation_pk = sao.id_holder_account_operation_fk  ");
		sbQuery.append("Inner Join holder_account ha on ha.id_holder_account_pk = hao.id_holder_account_fk                                  ");
		sbQuery.append("Inner Join participant pa on pa.id_participant_pk = hao.id_incharge_stock_participant                               ");
		sbQuery.append("Inner Join settlement_operation so on so.id_settlement_operation_pk = sao.id_settlement_operation_fk                ");
		sbQuery.append("Inner Join mechanism_operation mo on mo.id_mechanism_operation_pk = so.id_mechanism_operation_fk                    ");
		sbQuery.append("Inner Join holder_marketfact_balance  hmb on (hmb.id_security_code_fk = mo.id_security_code_fk And                  ");
		sbQuery.append("                                              hmb.id_holder_account_fk = hao.id_holder_account_fk)                  ");
		sbQuery.append("where                                                                                                               ");
		sbQuery.append("      so.operation_part = :report And  mo.ind_reporting_balance = :oneParam And                                     ");
		sbQuery.append("      sao.role In (:role)  And sam.ind_active = :oneParam And so.operation_state = :opeState And                  	");
		sbQuery.append("      sao.operation_state = :soState  And hmb.ind_active = :oneParam And                                            ");
//		sbQuery.append("      (hmb.market_rate <> sam.market_rate or trunc(hmb.market_date) <> trunc(sam.market_date))                      ");
		sbQuery.append("      Not Exists (Select * From Holder_MarketFact_Balance market							 	                    ");
		sbQuery.append("      			  Where hmb.id_holder_account_fk = market.id_holder_account_fk 	And									");
		sbQuery.append("      			        hmb.id_security_code_fk = market.id_security_code_fk 	And									");
		sbQuery.append("      			  		NVL(sam.market_rate,0) = NVL(market.market_rate,0) 		And									"); 
		sbQuery.append("      			  		NVL(sam.market_price,0) = NVL(market.market_price,0)	And									");
		sbQuery.append("      			  		sam.market_date = market.market_date 														");
		sbQuery.append("      			  )																									");
		
		Query query = null;
		try{
			query = em.createNativeQuery(sbQuery.toString());
		}catch(NoResultException ex){
			return inconsistenciesList;
		}
		
		query.setParameter("role", Arrays.asList(ComponentConstant.SALE_ROLE,ComponentConstant.PURCHARSE_ROLE));
		query.setParameter("oneParam", ComponentConstant.ONE);
		query.setParameter("report", ComponentConstant.TERM_PART);
		query.setParameter("soState", HolderAccountOperationStateType.CONFIRMED.getCode());
		query.setParameter("opeState", MechanismOperationStateType.CASH_SETTLED.getCode());
		
		@SuppressWarnings("unchecked")
		List<Object> objectDataList = query.getResultList();
		for(Object data: objectDataList){
			inconsistenciesList.add((String) data);
		}
		return inconsistenciesList;
	}
	
	/**
	 * Buscamo el pk de institution bank account
	 * @param idInstitutionCashAccountPk
	 * @return Id de bank account pk 
	 * */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Long  searhIdInstitutionBankAccountPk(Long idInstitutionCashAccountPk) {
		StringBuilder stringBuffer = new StringBuilder();
		stringBuffer.append(" select cad.institutionBankAccount.idInstitutionBankAccountPk FROM CashAccountDetail cad ");
		stringBuffer.append(" where 1 = 1 ");
		stringBuffer.append(" and cad.institutionCashAccount.idInstitutionCashAccountPk = :idInstitutionCashAccountPk "); 
		stringBuffer.append(" and cad.indSending = :indSending ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idInstitutionCashAccountPk", idInstitutionCashAccountPk);
		query.setParameter("indSending", 1);
		
		return (Long) query.getSingleResult();
	}
	
	/**
	 * Busqueda del banco segun ID
	 * */
	public Long searchIdBankPk(Long idInstitutionCashAccountPk){
		StringBuilder stringBuffer = new StringBuilder();
		stringBuffer.append(" select cad.institutionBankAccount.bank.idBankPk FROM CashAccountDetail cad ");
		stringBuffer.append(" where 1 = 1 ");
		stringBuffer.append(" and cad.institutionCashAccount.idInstitutionCashAccountPk = :idInstitutionCashAccountPk "); 
		stringBuffer.append(" and cad.indSending = :indSending ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idInstitutionCashAccountPk", idInstitutionCashAccountPk);
		query.setParameter("indSending", 1);
		
		return (Long) query.getSingleResult();
	}
	
	/**
	 * Busqueda del banco segun ID
	 * */
	public InstitutionBankAccount searchInstitutionBankAccount(Long idInstitutionBankAccountPk){
		StringBuilder stringBuffer = new StringBuilder();
		stringBuffer.append(" select iba FROM InstitutionBankAccount iba ");
		stringBuffer.append(" where 1 = 1 ");
		stringBuffer.append(" and iba.idInstitutionBankAccountPk = :idInstitutionBankAccountPk ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idInstitutionBankAccountPk", idInstitutionBankAccountPk);
		
		return (InstitutionBankAccount) query.getSingleResult();
	}
	
	
}