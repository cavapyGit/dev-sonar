package com.pradera.core.component.swift.to;

import java.math.BigDecimal;
import java.util.Date;

public class SwiftMessagesRetirement {

	private Integer groupOperationFund;//FUNDS GROUP OPERATION
	private Long operationType;//FUNDS OPERATION TYPE
	private String fundOperationTypeDescription;//FUND OPERATION TYPE DESCRIPTION

	private String cevaldomBicCode;//BIC SWIFT CODE OF CEVALDOM BCRD ACCOUNT WHERE MONEY WILL BE TAKEN
	private String receptorBicCode;//BIC CODE OF THE BENEFICIARY ENTITY - IN CAN BE A SWIFT BIC CODE OR NOT
	private String bcrdBicCode;//BIC CODE OF BCRD - IN CASE THE RECEPTOR BIC CODE IS NO SWIFT

	private String messageType;
	private String paymentReference;
	private String correlative;

	//FIELD 20
	//SETTLEMENT
	private String trn;
	private String settlementSchema;
	private Long operationID;
	private String mechanism;
	private String modalityGroup;
	//OTHERS
	private Long idFundOperation;


	//FIELD 21
	//AUTOMATIC DEVOLUTION
	private String originalFundOperation;//FIELD 20 OF THE ORIGINAL FUND OPERATION; IN OTHER FUND OPERATION GROUPS THERES NO FIELD VALUE OR IR "NOREF"


	//FIELD 32
	private Date settlementDate;//FORMAT(AAMMDD)
	private String currency;
	private BigDecimal amount;//FORMAT(14,2)


	//FIELD 50K
	//SETTLEMENT
	private String participantBuyerCentralAccountNumber;//ACCOUNT NUMBER OF BUYER PARTICIPANT IN CENTRAL(SETTLEMENT OPERATIONS)
	private String participantBuyerDescription;//DESCRIPTION OF THE PARTICIPANT BUYER
	//DEVOLUTIONS & BENEFITS ALL FUNDS_TYPE EXCEPT BENEFIT_HOLDERS
	private String depositaryCentralBankAccount;//ACCOUNT NUMBER OF CEVALDOM
	private String depositaryDescription;//DESCRIPTION OF DEPOSITARY


	//FIELD 54A
	//SETTLEMENT & BENEFITS (PARTICIPANT) & DEVOLUTION & AUTOMATIC DEVOLUTION
 	//ITS ACCORDING TO THE FIELD 'receptorBicCode' IF IT IS NO SWIFT BIC CODE, SO MUST FILL THIS TAG


	//FIELD 58
	//BENEFITS (HOLDERS)  & 
	private String bicCodeBeneficiaryBank;


	//FIELD 59
	//SETTLEMENT
	private String participantSettlerCentralAccountNumber;
	private String participantSettlerDescription;
	//BENEFITS (PARTICIPANTS)
	private String participantSettlerCommercialAccountNumber;
	//BENEFITS (OTHERS)
	private String depositaryCommercialAccountNumber;
	private String commercialBankDescription;
	//RETURN
	private String holderDescription;


	//FIELD 70
	//SETTLEMENT
	private String operationTypeDescription;


	//FIELD 72
	private String corporativeProcessDescription;


	public Integer getGroupOperationFund() {
		return groupOperationFund;
	}


	public void setGroupOperationFund(Integer groupOperationFund) {
		this.groupOperationFund = groupOperationFund;
	}

	public Long getOperationType() {
		return operationType;
	}

	public void setOperationType(Long operationType) {
		this.operationType = operationType;
	}


	public String getFundOperationTypeDescription() {
		return fundOperationTypeDescription;
	}


	public void setFundOperationTypeDescription(String fundOperationTypeDescription) {
		this.fundOperationTypeDescription = fundOperationTypeDescription;
	}


	public String getCevaldomBicCode() {
		return cevaldomBicCode;
	}


	public void setCevaldomBicCode(String cevaldomBicCode) {
		this.cevaldomBicCode = cevaldomBicCode;
	}


	public String getReceptorBicCode() {
		return receptorBicCode;
	}


	public void setReceptorBicCode(String receptorBicCode) {
		this.receptorBicCode = receptorBicCode;
	}


	public String getBcrdBicCode() {
		return bcrdBicCode;
	}


	public void setBcrdBicCode(String bcrdBicCode) {
		this.bcrdBicCode = bcrdBicCode;
	}


	public String getTrn() {
		return trn;
	}


	public void setTrn(String trn) {
		this.trn = trn;
	}


	public String getSettlementSchema() {
		return settlementSchema;
	}


	public void setSettlementSchema(String settlementSchema) {
		this.settlementSchema = settlementSchema;
	}

	

	public Long getOperationID() {
		return operationID;
	}


	public void setOperationID(Long operationID) {
		this.operationID = operationID;
	}


	public String getMechanism() {
		return mechanism;
	}


	public void setMechanism(String mechanism) {
		this.mechanism = mechanism;
	}


	public String getModalityGroup() {
		return modalityGroup;
	}


	public void setModalityGroup(String modalityGroup) {
		this.modalityGroup = modalityGroup;
	}


	public Long getIdFundOperation() {
		return idFundOperation;
	}


	public void setIdFundOperation(Long idFundOperation) {
		this.idFundOperation = idFundOperation;
	}


	public String getOriginalFundOperation() {
		return originalFundOperation;
	}


	public void setOriginalFundOperation(String originalFundOperation) {
		this.originalFundOperation = originalFundOperation;
	}


	public Date getSettlementDate() {
		return settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}

	public String getCurrency() {
		return currency;
	}


	public void setCurrency(String currency) {
		this.currency = currency;
	}


	public BigDecimal getAmount() {
		return amount;
	}


	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getParticipantBuyerCentralAccountNumber() {
		return participantBuyerCentralAccountNumber;
	}


	public void setParticipantBuyerCentralAccountNumber(
			String participantBuyerCentralAccountNumber) {
		this.participantBuyerCentralAccountNumber = participantBuyerCentralAccountNumber;
	}


	public String getParticipantBuyerDescription() {
		return participantBuyerDescription;
	}


	public void setParticipantBuyerDescription(String participantBuyerDescription) {
		this.participantBuyerDescription = participantBuyerDescription;
	}


	public String getDepositaryCentralBankAccount() {
		return depositaryCentralBankAccount;
	}


	public void setDepositaryCentralBankAccount(String depositaryCentralBankAccount) {
		this.depositaryCentralBankAccount = depositaryCentralBankAccount;
	}


	public String getDepositaryDescription() {
		return depositaryDescription;
	}


	public void setDepositaryDescription(String depositaryDescription) {
		this.depositaryDescription = depositaryDescription;
	}


	public String getBicCodeBeneficiaryBank() {
		return bicCodeBeneficiaryBank;
	}


	public void setBicCodeBeneficiaryBank(String bicCodeBeneficiaryBank) {
		this.bicCodeBeneficiaryBank = bicCodeBeneficiaryBank;
	}


	public String getParticipantSettlerCentralAccountNumber() {
		return participantSettlerCentralAccountNumber;
	}


	public void setParticipantSettlerCentralAccountNumber(
			String participantSettlerCentralAccountNumber) {
		this.participantSettlerCentralAccountNumber = participantSettlerCentralAccountNumber;
	}


	public String getParticipantSettlerDescription() {
		return participantSettlerDescription;
	}


	public void setParticipantSettlerDescription(
			String participantSettlerDescription) {
		this.participantSettlerDescription = participantSettlerDescription;
	}


	public String getParticipantSettlerCommercialAccountNumber() {
		return participantSettlerCommercialAccountNumber;
	}


	public void setParticipantSettlerCommercialAccountNumber(
			String participantSettlerCommercialAccountNumber) {
		this.participantSettlerCommercialAccountNumber = participantSettlerCommercialAccountNumber;
	}


	public String getDepositaryCommercialAccountNumber() {
		return depositaryCommercialAccountNumber;
	}


	public void setDepositaryCommercialAccountNumber(
			String depositaryCommercialAccountNumber) {
		this.depositaryCommercialAccountNumber = depositaryCommercialAccountNumber;
	}


	public String getCommercialBankDescription() {
		return commercialBankDescription;
	}


	public void setCommercialBankDescription(String commercialBankDescription) {
		this.commercialBankDescription = commercialBankDescription;
	}


	public String getOperationTypeDescription() {
		return operationTypeDescription;
	}


	public void setOperationTypeDescription(String operationTypeDescription) {
		this.operationTypeDescription = operationTypeDescription;
	}


	public String getCorporativeProcessDescription() {
		return corporativeProcessDescription;
	}


	public void setCorporativeProcessDescription(
			String corporativeProcessDescription) {
		this.corporativeProcessDescription = corporativeProcessDescription;
	}


	public String getHolderDescription() {
		return holderDescription;
	}


	public void setHolderDescription(String holderDescription) {
		this.holderDescription = holderDescription;
	}


	public String getMessageType() {
		return messageType;
	}


	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}


	public String getPaymentReference() {
		return paymentReference;
	}
	public void setPaymentReference(String paymentReference) {
		this.paymentReference = paymentReference;
	}

	public String getCorrelative() {
		return correlative;
	}
	public void setCorrelative(String correlative) {
		this.correlative = correlative;
	}
}