package com.pradera.core.component.business.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ValorizationBalanceDetailTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ValorizationBalanceDetailTO() {
		// TODO Auto-generated constructor stub
	}
	
	private String claseClaveValor; //codigo ISIN
	
	/** The isin description. */
	private String description; //Descripcion del valor
	
	/** The nominal value. */
	private BigDecimal nominalValue;   //valor Nominal
	
	/** The currency. */
	private Integer currency;	//Moneda
	
	/** The currency. */
	private String currencyDesc;	//Moneda descricpion
	
	/** The balanc contab. */
	private BigDecimal balanceTotal; //Saldo Contable
	
	/** The balanc availab. */
	private BigDecimal balanceAvailable;//Saldo Disponible
	
	/** The balance valoriz contab dop. */
	private BigDecimal balanceValorizContabBOB;//Saldo contable valorizado DOP
	
	/** The balance valoriz contab usd. */
	private BigDecimal balanceValorizContabUSD;//Saldo contable valorizado USD
	
	/** The balance valoriz availab dop. */
	private BigDecimal balanceValorizAvailabBOB;//Saldo contable disponbible DOP
	
	/** The balance valoriz availab usd.  */
	private BigDecimal balanceValorizAvailabUSD;//Saldo contable disponbible USD
	
	private Long holderAccount;
	
	private Long participant;
	
	private String strAlternateCode;
	
	private Date dtDateDeposited;
	
	private Long lngCertNumber;
	
	private long idPhysicalCertificate;
	
	/** The certificate quantity. */
	private BigDecimal certificateQuqntity;

	public String getClaseClaveValor() {
		return claseClaveValor;
	}

	public void setClaseClaveValor(String claseClaveValor) {
		this.claseClaveValor = claseClaveValor;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getNominalValue() {
		return nominalValue;
	}

	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	
	public BigDecimal getBalanceTotal() {
		return balanceTotal;
	}

	public void setBalanceTotal(BigDecimal balanceTotal) {
		this.balanceTotal = balanceTotal;
	}

	public BigDecimal getBalanceAvailable() {
		return balanceAvailable;
	}

	public void setBalanceAvailable(BigDecimal balanceAvailable) {
		this.balanceAvailable = balanceAvailable;
	}

	public BigDecimal getBalanceValorizContabBOB() {
		return balanceValorizContabBOB;
	}

	public void setBalanceValorizContabBOB(BigDecimal balanceValorizContabBOB) {
		this.balanceValorizContabBOB = balanceValorizContabBOB;
	}

	public BigDecimal getBalanceValorizContabUSD() {
		return balanceValorizContabUSD;
	}

	public void setBalanceValorizContabUSD(BigDecimal balanceValorizContabUSD) {
		this.balanceValorizContabUSD = balanceValorizContabUSD;
	}

	public BigDecimal getBalanceValorizAvailabBOB() {
		return balanceValorizAvailabBOB;
	}

	public void setBalanceValorizAvailabBOB(BigDecimal balanceValorizAvailabBOB) {
		this.balanceValorizAvailabBOB = balanceValorizAvailabBOB;
	}

	public BigDecimal getBalanceValorizAvailabUSD() {
		return balanceValorizAvailabUSD;
	}

	public void setBalanceValorizAvailabUSD(BigDecimal balanceValorizAvailabUSD) {
		this.balanceValorizAvailabUSD = balanceValorizAvailabUSD;
	}

	public String getCurrencyDesc() {
		return currencyDesc;
	}

	public void setCurrencyDesc(String currencyDesc) {
		this.currencyDesc = currencyDesc;
	}

	public Long getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(Long holderAccount) {
		this.holderAccount = holderAccount;
	}

	public Long getParticipant() {
		return participant;
	}

	public void setParticipant(Long participant) {
		this.participant = participant;
	}

	/**
	 * @return the certificateQuqntity
	 */
	public BigDecimal getCertificateQuqntity() {
		return certificateQuqntity;
	}

	/**
	 * @param certificateQuqntity the certificateQuqntity to set
	 */
	public void setCertificateQuqntity(BigDecimal certificateQuqntity) {
		this.certificateQuqntity = certificateQuqntity;
	}

	/**
	 * @return the strAlternateCode
	 */
	public String getStrAlternateCode() {
		return strAlternateCode;
	}

	/**
	 * @param strAlternateCode the strAlternateCode to set
	 */
	public void setStrAlternateCode(String strAlternateCode) {
		this.strAlternateCode = strAlternateCode;
	}

	/**
	 * @return the dtDateDeposited
	 */
	public Date getDtDateDeposited() {
		return dtDateDeposited;
	}

	/**
	 * @param dtDateDeposited the dtDateDeposited to set
	 */
	public void setDtDateDeposited(Date dtDateDeposited) {
		this.dtDateDeposited = dtDateDeposited;
	}

	/**
	 * @return the lngCertNumber
	 */
	public Long getLngCertNumber() {
		return lngCertNumber;
	}

	/**
	 * @param lngCertNumber the lngCertNumber to set
	 */
	public void setLngCertNumber(Long lngCertNumber) {
		this.lngCertNumber = lngCertNumber;
	}

	/**
	 * @return the idPhysicalCertificate
	 */
	public long getIdPhysicalCertificate() {
		return idPhysicalCertificate;
	}

	/**
	 * @param idPhysicalCertificate the idPhysicalCertificate to set
	 */
	public void setIdPhysicalCertificate(long idPhysicalCertificate) {
		this.idPhysicalCertificate = idPhysicalCertificate;
	}

	
	
}
