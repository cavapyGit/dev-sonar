package com.pradera.core.component.swift.to;


public class SwiftConstants {

	//GROUP OPERATION:	DEPOSIT DEVOLUTIONS
	public final static Integer OPERATION_TYPE_DEVOLUTION_SETTLEMENT = 1;
	public final static Integer OPERATION_TYPE_DEVOLUTION_BENEFITS = 2;
	public final static Integer OPERATION_TYPE_DEVOLUTION_RATES = 3;

	//GROUP OPERATION:	RETURN
	public final static Long OPERATION_TYPE_RETURN_SUPPLY_BENEFITS = Long.valueOf(1);
	public final static Long OPERATION_TYPE_RETURN_BENEFITS = Long.valueOf(2);
	public final static Long OPERATION_TYPE_RETURN_PAYMENT_BLOCK_BENEFITS = Long.valueOf(3);

	//GROUP OPERATION:	SETTLEMENT
	public final static Integer OPERATION_TYPE_SETTLEMENT_DEUDOR_ACREEDOR_FUNDS = 1;
	public final static Integer OPERATION_TYPE_SETTLEMENT_NEGOTIATION_FUNDS = 3;

	//GROUP OPERATION:	RATES
	public final static Integer OPERATION_TYPE_RATES = 1;

	//GROUP OPERATIONS
	public final static Integer GROUP_OPERATION_SETTLEMENT=1;
	public final static Integer GROUP_OPERATION_BENEFITS=2;
	public final static Integer GROUP_OPERATION_RETURNS=3;
	public final static Integer GROUP_OPERATION_DEPOSIT_DEVOLUTIONS=4;
	public final static Integer GROUP_OPERATION_RATES=5;

	public final static Integer SWIFT_OPERATION_TYPE_DEPOSIT=1;
	public final static Integer SWIFT_OPERATION_TYPE_RETIREMENT=2;

	public final static String BLOCK_ONE_IDENTIFIER="1";
	public final static String BLOCK_TWO_IDENTIFIER="2";
	public final static String BLOCK_THREE_IDENTIFIER="3";
	public final static String BLOCK_FOUR_IDENTIFIER="4";

	public final static String DEFAULT_APPLICATION_IDENTIFIER="F";
	public final static String DEFAULT_SERVICE_IDENTIFIER="01";
	public final static String DEFAULT_SESSION_NUMBER="0000";
	public final static String DEFAULT_SECUENCE_NUMBER="000000";

	public static final String SWIFT_SYSTEM_OPERATION_TYPE_INPUT="I";
	public static final String SWIFT_SYSTEM_OPERATION_TYPE_OUTPUT="O";
	public static final String DEFAULT_MESSAGE_PRIORITY="N";

	public static final String DEFAULT_FIN_IDENTIFIER = "SDD";
	public static final String DEFAULT_TRN_REFERENCE_NOREF = "NOREF";

	public static final String OPEN_PARENTHESES = "{";
	public static final String CLOSE_PARENTHESES = "}";
	public static final String DOUBLE_POINT = ":";
	public static final String END_LINE = "\n";
	public static final String SIMPLE_POINT = ".";

	public static final String CURRENCY_PESOS_DOMINICANOS="DOP";
	public static final String CURRENCY_DOLLARS="USD";

	public static final String NO_SWIFT_INDICATOR = "1";

	public static final String FIELD_NAME_103 = "103";
	public static final String FIELD_NAME_20 = "20";
	public static final String FIELD_NAME_21 = "21";
	public static final String FIELD_NAME_23B = "23B";
	public static final String FIELD_NAME_32A = "32A";
	public static final String FIELD_NAME_50K = "50K";
	public static final String FIELD_NAME_53A = "53A";
	public static final String FIELD_NAME_54A = "54A";
	public static final String FIELD_NAME_57A = "57A";
	public static final String FIELD_NAME_58A = "58A";
	public static final String FIELD_NAME_59 = "59";
	public static final String FIELD_NAME_70 = "70";
	public static final String FIELD_NAME_71A = "71A";
	public static final String FIELD_NAME_72= "72";
	

	public static final String TRN_E000148 = "E000148";
	public static final String TRN_E000149 = "E000149";
	public static final String TRN_E000339 = "E000339";
	public static final String TRN_E000340 = "E000340";
	public static final String TRN_E000341 = "E000341";
	public static final String TRN_E000341DEV = "E000341.DEV";
	public static final String TRN_E000342 = "E000342";
	public static final String TRN_E000342DEV = "E000342.DEV";
	public static final String TRN_E000399 = "E000399";
	public static final String TRN_E000400 = "E000400";

	public static final String FIELD_VALUE_REC = "/REC/";
	public static final String FIELD_VALUE_CRED = "CRED";
	public static final String FIELD_VALUE_OUR = "OUR";

	public static final String OPERATION_TYPE_DESC_DEV_BENEF_FUNDS = "RETIRO POR DEVOLUCION DE FONDOS DE DERECHOS";
	public static final String OPERATION_TYPE_DESC_SUP_BENEF_PAY = "RETIRO POR RESERVA DE PAGO DE DERECHOS";
	public static final String OPERATION_TYPE_DESC_BENEF_HOLDER_PAY = "RETIRO POR PAGO DE DERECHOS A TITULARES";
	public static final String OPERATION_TYPE_DESC_BENEF_INST_PAY = "RETIRO POR PAGO DE DERECHOS A INSTITUCIONES";
	public static final String OPERATION_TYPE_DESC_TAX_BENEF_BLOCK = "RETIRO POR RETENCION DE DERECHOS BLOQUEADOS";
	public static final String OPERATION_TYPE_DESC_TAX_RETENTION = "RETIRO POR RETENCION DE IMPUESTOS";
	public static final String OPERATION_TYPE_DESC_PAY_COMMIS_ISSUER= "RETIRO POR PAGO DE COMISION DE DERECHOS EMISOR";
	public static final String OPERATION_TYPE_DESC_PAY_CUSTODY_COMMIS = "RETIR POR PAGO DE COMISION POR CUSTODIA";

	public static final String SETTLEMENT_SCHEMA_GROSS = "1";
	public static final String SETTLEMENT_SCHEMA_NET = "2";

	public static final String TEMPORAL_DEPOSIT_DIR = "/tmpDeposit";
	public static final String TEMPORAL_RETIREMENT_DIR = "/tmpRetirement";

	public static final String ACCEPTED_PROCESS = "_PROCESSED";
	public static final String FILE_TO_PROCESS = "_TO_PROCESS";

	public static final Integer COMPONENT_NUMBER_PAYMENT_DATE = 1;
	public static final Integer COMPONENT_NUMBER_CURRENCY = 2;
	public static final Integer COMPONENT_NUMBER_AMOUNT = 3;

	public static final Integer INDICATOR_REGISTER_TYPE_MESSAGE = 1;
	public static final Integer INDICATOR_REGISTER_TYPE_FILE = 2;

	public static final String FORMAT_DATE = "yyMMdd";
	public static final String FORMAT_NUMBER = "###.##";
}