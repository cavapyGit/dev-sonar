package com.pradera.core.component.corporateevents.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.corporateevents.to.CorporativeOperationTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.corporatives.CorporativeEventType;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.issuancesecuritie.Security;


@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class CorporateEventComponentServiceBean extends CrudDaoServiceBean {
	
	public CorporativeOperation saveCoprorativeOperation(CorporativeOperation operation)throws ServiceException{
		em.persist(operation);
		return operation;
	}
	
	public List<CorporativeOperation> searchOperationForDematerializationCertificateValidation(CorporativeOperationTO filter)throws ServiceException{
		Query query = em.createNamedQuery(CorporativeOperation.CORPORATIVE_OPERATION_VALIDATION_PARAMETER_DEMATERIALIZATION);
		List<CorporativeOperation> list;
		query.setParameter("eventTypeList", filter.getEventTypeList());
		query.setParameter("stateList", filter.getStateList());
		list = (List<CorporativeOperation>) query.getResultList();
		return list;
		
	}
	
	public CorporativeOperation findCorporativeOperationByFilter(CorporativeOperationTO filter)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		CorporativeOperation oper = new CorporativeOperation();
		sbQuery.append(" Select co FROM CorporativeOperation co  join fetch co.corporativeEventType join fetch co.securities join fetch co.issuer join fetch co.corporativeEventType");
		if(filter.getNeedTargetSecurity()){
			sbQuery.append(" join fetch co.targetSecurity");
		}
		sbQuery.append(" where 1 = 1");

		if(Validations.validateIsNotNull(filter.getId())){
			sbQuery.append(" and co.idCorporativeOperationPk =:idOperation");
		}

		if(Validations.validateIsNotNull(filter.getSourceIsinCode())){
			sbQuery.append(" and co.securities.idIsinCodePk =:isinCode");
		}

		if(Validations.validateIsNotNull(filter.getState())){
			sbQuery.append(" and co.state =:state");
		}

		if(Validations.validateIsNotNull(filter.getIndRemanent())){
			sbQuery.append(" and co.indRemanent =:indReman");
		}
		if(Validations.validateIsNotNull(filter.getIssuerCode())){
			sbQuery.append(" and co.issuer.idIssuerPk=:issuer");
		}

		if(Validations.validateIsNotNullAndNotEmpty(filter.getInstrumentType())){
			sbQuery.append(" and co.securities.instrumentType=:instrumentType");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getCorporativeEvent())){
			sbQuery.append(" and co.corporativeEventType=:eventType");
		}
		
	


		Query query = em.createQuery(sbQuery.toString());

		if(Validations.validateIsNotNull(filter.getId())){
			query.setParameter("idOperation", filter.getId());
		}

		if(Validations.validateIsNotNull(filter.getSourceIsinCode())){
			query.setParameter("isinCode",filter.getSourceIsinCode());			
		}

		if(Validations.validateIsNotNull(filter.getState())){
			query.setParameter("state", filter.getState());		}

		if(Validations.validateIsNotNull(filter.getIndRemanent())){
			query.setParameter("indReman", filter.getIndRemanent());
		}

		if(Validations.validateIsNotNull(filter.getIssuerCode())){
			query.setParameter("issuer", filter.getIssuerCode());
		}

		if(Validations.validateIsNotNullAndNotEmpty(filter.getInstrumentType())){
			query.setParameter("instrumentType", filter.getInstrumentType());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getCorporativeEvent())){
			query.setParameter("eventType", filter.getCorporativeEvent());
 		}
	

		CorporativeOperation operation =null;

		try{
			operation =(CorporativeOperation) query.getSingleResult();
		}catch (javax.persistence.NoResultException e) {
			return null;

		}	

		return operation;

	}
	
	public void updateCorporativeOperationServiceBean(CorporativeOperation cOperation, LoggerUser loggerUser) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append("Update CorporativeOperation c ");
		sbQuery.append("Set c.state=:statePrm, ");
		sbQuery.append("c.lastModifyDate=:lastModifyDatePrm, ");
		sbQuery.append("c.lastModifyUser=:lastModifyUserPrm, ");
		sbQuery.append("c.lastModifyIp=:lastModifyIpPrm, ");
		sbQuery.append("c.lastModifyApp=:lastModifyAppPrm ");
		sbQuery.append("where c.state = ").append(CorporateProcessStateType.REGISTERED.getCode());
		if(Validations.validateIsNotNullAndNotEmpty(cOperation.getIdCorporativeOperationPk())){
			sbQuery.append("and c.idCorporativeOperationPk=:idCorporativeOperationPrm");
		}
		if(cOperation.getProgramInterestCoupon()!=null && 
				Validations.validateIsNotNullAndNotEmpty(cOperation.getProgramInterestCoupon().getIdProgramInterestPk())){
			sbQuery.append("and c.programInterestCoupon.idProgramInterestPk=:idProgramInterestCoupPrm");
		}
		if(cOperation.getProgramAmortizationCoupon()!=null && 
				Validations.validateIsNotNullAndNotEmpty(cOperation.getProgramAmortizationCoupon().getIdProgramAmortizationPk())){
			sbQuery.append("and c.programAmortizationCoupon.idProgramAmortizationPk=:idProgramAmortizationCoupPrm");
		}
		
		Query query = em.createQuery(sbQuery.toString());
		
		if(Validations.validateIsNotNullAndNotEmpty(cOperation.getIdCorporativeOperationPk())){
			query.setParameter("idCorporativeOperationPrm",cOperation.getIdCorporativeOperationPk());
		}
		if(cOperation.getProgramInterestCoupon()!=null && 
				Validations.validateIsNotNullAndNotEmpty(cOperation.getProgramInterestCoupon().getIdProgramInterestPk())){
			query.setParameter("idProgramInterestCoupPrm",cOperation.getProgramInterestCoupon().getIdProgramInterestPk());
		}
		if(cOperation.getProgramAmortizationCoupon()!=null && 
				Validations.validateIsNotNullAndNotEmpty(cOperation.getProgramAmortizationCoupon().getIdProgramAmortizationPk())){
			query.setParameter("idProgramAmortizationCoupPrm",cOperation.getProgramAmortizationCoupon().getIdProgramAmortizationPk());
		}
		query.setParameter("statePrm",cOperation.getState());
		
		query.setParameter("lastModifyDatePrm",loggerUser.getAuditTime());
		query.setParameter("lastModifyUserPrm",loggerUser.getUserName());
		query.setParameter("lastModifyIpPrm",loggerUser.getIpAddress());
		query.setParameter("lastModifyAppPrm",loggerUser.getIdPrivilegeOfSystem());
		
	    query.executeUpdate();
	}
	public void updateCorporativeResultDetail(Long idCorporativeOperationNew, Long idCorporativeProcessOld, LoggerUser loggerUser){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Update CorporativeResultDetail c ");
		sbQuery.append("Set c.corporativeOperation.idCorporativeOperationPk = ").append(idCorporativeOperationNew+",");
		sbQuery.append("c.lastModifyDate=:lastModifyDatePrm, ");
		sbQuery.append("c.lastModifyUser=:lastModifyUserPrm, ");
		sbQuery.append("c.lastModifyIp=:lastModifyIpPrm, ");
		sbQuery.append("c.lastModifyApp=:lastModifyAppPrm ");
		sbQuery.append("where c.corporativeOperation.idCorporativeOperationPk = ").append(idCorporativeProcessOld);
		Query query = em.createQuery(sbQuery.toString());	  		
		query.setParameter("lastModifyDatePrm",loggerUser.getAuditTime());
		query.setParameter("lastModifyUserPrm",loggerUser.getUserName());
		query.setParameter("lastModifyIpPrm",loggerUser.getIpAddress());
		query.setParameter("lastModifyAppPrm",loggerUser.getIdPrivilegeOfSystem());
		
		query.executeUpdate();
	}
	public void updateCorporativeProcessResult(Long idCorporativeOperationNew, Long idCorporativeProcessOld, LoggerUser loggerUser) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Update CorporativeProcessResult c ");
		sbQuery.append("Set c.corporativeOperation.idCorporativeOperationPk = ").append(idCorporativeOperationNew+",");
		sbQuery.append("c.lastModifyDate=:lastModifyDatePrm, ");
		sbQuery.append("c.lastModifyUser=:lastModifyUserPrm, ");
		sbQuery.append("c.lastModifyIp=:lastModifyIpPrm, ");
		sbQuery.append("c.lastModifyApp=:lastModifyAppPrm ");
		sbQuery.append("where c.corporativeOperation.idCorporativeOperationPk = ").append(idCorporativeProcessOld);
		Query query = em.createQuery(sbQuery.toString());	    		
		query.setParameter("lastModifyDatePrm",loggerUser.getAuditTime());
		query.setParameter("lastModifyUserPrm",loggerUser.getUserName());
		query.setParameter("lastModifyIpPrm",loggerUser.getIpAddress());
		query.setParameter("lastModifyAppPrm",loggerUser.getIdPrivilegeOfSystem());
		
		query.executeUpdate();
	}
	public void updateSecuritiesOperation(Long idCorporativeOperation,Long idSecuritiesOperation, LoggerUser loggerUser){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Update SecuritiesOperation c ");
		sbQuery.append("Set c.corporativeOperation.idCorporativeOperationPk = ").append(idCorporativeOperation+",");
		sbQuery.append("c.lastModifyDate=:lastModifyDatePrm, ");
		sbQuery.append("c.lastModifyUser=:lastModifyUserPrm, ");
		sbQuery.append("c.lastModifyIp=:lastModifyIpPrm, ");
		sbQuery.append("c.lastModifyApp=:lastModifyAppPrm ");
		sbQuery.append("where c.idSecuritiesOperationPk = ").append(idSecuritiesOperation);
		Query query = em.createQuery(sbQuery.toString());	  		
		query.setParameter("lastModifyDatePrm",loggerUser.getAuditTime());
		query.setParameter("lastModifyUserPrm",loggerUser.getUserName());
		query.setParameter("lastModifyIpPrm",loggerUser.getIpAddress());
		query.setParameter("lastModifyAppPrm",loggerUser.getIdPrivilegeOfSystem());
		
		query.executeUpdate();
	}	
	public void updateCorporativeOperationProgramAmoPk(Long idCorporativeOperation, Long idProgramAmortizationCoupon) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Update CorporativeOperation c ");
		sbQuery.append("Set c.programAmortizationCoupon.idProgramAmortizationPk = ").append(idProgramAmortizationCoupon);
		sbQuery.append(" where c.idCorporativeOperationPk = ").append(idCorporativeOperation);
		Query query = em.createQuery(sbQuery.toString());	    
				
		query.executeUpdate();
	}
	public Long getSecuritiesOperationId(Long idCorporativeOperation){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("  SELECT C.idSecuritiesOperationPk" );
		sbQuery.append("   FROM  SecuritiesOperation C");
		sbQuery.append("  WHERE  C.corporativeOperation.idCorporativeOperationPk=:idCorporativeOperation");
		Query query = em.createQuery(sbQuery.toString());				
		query.setParameter("idCorporativeOperation", idCorporativeOperation);		
		Long idSecuritiesOperation = null;				
		try {
			idSecuritiesOperation =(Long) query.getSingleResult();
		} catch (javax.persistence.NoResultException e) {
			return null;
		}	
		return idSecuritiesOperation;
	}
	public List<Long> getProAmoCoupons (Long idAmortizationPaymentSchedule) {
		List<Long> list;		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("  SELECT C.idProgramAmortizationPk" );
		sbQuery.append("   FROM  ProgramAmortizationCoupon C");
		sbQuery.append("  WHERE  1=1 and C.amortizationPaymentSchedule.idAmoPaymentSchedulePk = ").append(idAmortizationPaymentSchedule);
		Query query = em.createQuery(sbQuery.toString());				
		list = (List<Long>) query.getResultList();
		return list;
	}
	public List<Long> getCorpOperation (String idSecurityCodePk) {
		List<Long> list;
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT co.programAmortizationCoupon.idProgramAmortizationPk ");
		sbQuery.append("	FROM CorporativeOperation co");
		sbQuery.append(" 	Where 1 = 1	");
		sbQuery.append("	and co.securities.idSecurityCodePk = :idSecurityCodePk");
		sbQuery.append("	and co.programAmortizationCoupon.idProgramAmortizationPk is not null");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		list = (List<Long>) query.getResultList();
		return list;
	}
	public Long corpOperationModified(Long idProgramAmortizationPk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("  SELECT max(C.idCorporativeOperationPk)" );
		sbQuery.append("   FROM  CorporativeOperation C");
		sbQuery.append("  WHERE  1=1 and C.programAmortizationCoupon.idProgramAmortizationPk = ").append(idProgramAmortizationPk);
		Query query = em.createQuery(sbQuery.toString());				
		Long result = null;
		try {
			result =(Long) query.getSingleResult();
		} catch (javax.persistence.NoResultException e) {
			return null;
		}	
		return result;
	}
	
	public void deleteCorporativeOperationServiceBean(CorporativeOperation cOperation) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append("Delete CorporativeOperation c where 1 = 1 ");
		
		if(Validations.validateIsNotNullAndNotEmpty(cOperation.getIdCorporativeOperationPk())){
			sbQuery.append(" and c.idCorporativeOperationPk=:idCorporativeOperationPrm");
		}
		if(cOperation.getProgramInterestCoupon()!=null && Validations.validateIsNotNullAndNotEmpty(cOperation.getProgramInterestCoupon().getIdProgramInterestPk())){
			sbQuery.append(" and c.programInterestCoupon.idProgramInterestPk=:idProgramInterestCoupPrm");
		}
		if(cOperation.getProgramAmortizationCoupon()!=null && Validations.validateIsNotNullAndNotEmpty(cOperation.getProgramAmortizationCoupon().getIdProgramAmortizationPk())){
			sbQuery.append(" and c.programAmortizationCoupon.idProgramAmortizationPk=:idProgramAmortizationCoupPrm");
		}
		
		Query query = em.createQuery(sbQuery.toString());
		
		if(Validations.validateIsNotNullAndNotEmpty(cOperation.getIdCorporativeOperationPk())){
			query.setParameter("idCorporativeOperationPrm",cOperation.getIdCorporativeOperationPk());
		}
		if(cOperation.getProgramInterestCoupon()!=null && 
				Validations.validateIsNotNullAndNotEmpty(cOperation.getProgramInterestCoupon().getIdProgramInterestPk())){
			query.setParameter("idProgramInterestCoupPrm",cOperation.getProgramInterestCoupon().getIdProgramInterestPk());
		}
		if(cOperation.getProgramAmortizationCoupon()!=null && 
				Validations.validateIsNotNullAndNotEmpty(cOperation.getProgramAmortizationCoupon().getIdProgramAmortizationPk())){
			query.setParameter("idProgramAmortizationCoupPrm",cOperation.getProgramAmortizationCoupon().getIdProgramAmortizationPk());
		}
		
		
	    query.executeUpdate();
	}
	public BigDecimal getCustodyBalance(Long idHolderAccountPk,String idIsinCodePk){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("  SELECT " );	
		sbQuery.append("         HAB.totalBalance" );
		sbQuery.append("   FROM  HolderAccountBalance HAB");
		sbQuery.append("  WHERE  HAB.holderAccount.idHolderAccountPk=:idHolderAccountPk");
		sbQuery.append("    AND  HAB.security.idIsinCodePk=:idIsinCodePk");
		Query query = em.createQuery(sbQuery.toString());
		
		query.setParameter("idHolderAccountPk", idHolderAccountPk);
		query.setParameter("idIsinCodePk", idIsinCodePk);
		
		return (BigDecimal) query.getSingleResult();
	}
	public Long getLastCouponPaid(String idIsinCodePk){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("  SELECT " );	
		sbQuery.append("         MAX(C.idCorporativeOperationPk)" );
		sbQuery.append("   FROM  CorporativeOperation C");
		sbQuery.append("  WHERE  C.securities.idIsinCodePk=:idIsinCodePk");
		sbQuery.append("    AND  C.indPayed=1");	
		Query query = em.createQuery(sbQuery.toString());
				
		query.setParameter("idIsinCodePk", idIsinCodePk);
		
		return (Long) query.getSingleResult();
	}
	
	public Object [] getLastCouponPaidDateAndInterest(Long idCorporativeOperation){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("  SELECT " );	
		sbQuery.append("         C.deliveryDate, "
				+ "				 C.securities.interestRate" );
		sbQuery.append("   FROM  CorporativeOperation C");
		sbQuery.append("  WHERE  C.idCorporativeOperationPk=:idCorporativeOperation");
		sbQuery.append("    AND  C.indPayed=1");	
		Query query = em.createQuery(sbQuery.toString());
				
		query.setParameter("idCorporativeOperation", idCorporativeOperation);
		Object [] objectList = (Object[]) query.getSingleResult();
		return objectList;
	}
	
	public CorporativeEventType findCorporativeEventType(CorporativeEventType corpEventTypeFilter) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("Select c from CorporativeEventType c where 1=1");
		if(Validations.validateIsNotNull(corpEventTypeFilter.getCorporativeEventType())){
			sbQuery.append(" And c.corporativeEventType = :idCorporativeEventPkPrm");
			parameters.put("idCorporativeEventPkPrm", corpEventTypeFilter.getCorporativeEventType());
		}
		if(Validations.validateIsNotNull(corpEventTypeFilter.getInstrumentType())){
			sbQuery.append(" And c.instrumentType= :instrumentTypePrm");
			parameters.put("instrumentTypePrm", corpEventTypeFilter.getInstrumentType());
		}
		if(Validations.validateIsNotNull(corpEventTypeFilter.getCorporativeEventClass())){
			sbQuery.append(" And c.corporativeEventClass= :corporativeEventClassPrm");
			parameters.put("corporativeEventClassPrm", corpEventTypeFilter.getCorporativeEventClass());
		}
		Query query = this.em.createQuery(sbQuery.toString());
		for (Entry<String, Object> entry : parameters.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		CorporativeEventType corporativeEventTypeRes=null;
		try {
			corporativeEventTypeRes=(CorporativeEventType)query.getSingleResult();
		} catch (NonUniqueResultException ex) {
			ex.printStackTrace();
			corporativeEventTypeRes=null;
		} catch (NoResultException ex){
			ex.printStackTrace();
			corporativeEventTypeRes=null;
		}
		return corporativeEventTypeRes;
	}
	
}
