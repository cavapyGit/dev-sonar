package com.pradera.core.component.helperui.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.el.ELContext;
import javax.el.ValueExpression;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.utils.view.GeneralPropertiesConstants;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.HelperBean;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.AccountSearcherTO;
import com.pradera.core.component.helperui.to.AccountTO;
import com.pradera.core.component.helperui.to.IssuerSearcherTO;
import com.pradera.core.component.helperui.to.IssuerTO;
import com.pradera.core.component.helperui.to.SecuritiesPartHolAccountTO;
import com.pradera.core.component.helperui.to.SecuritiesSearcherTO;
import com.pradera.core.component.helperui.to.SecuritiesTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;



/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SecuritiesPartHolAccount.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 12-jun-2015
 */
@HelperBean
public class SecuritiesPartHolAccount implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The max results query helper. */
	@Inject @Configurable
	Integer maxResultsQueryHelper;
	
	/** The Log. */
	@Inject
	private transient PraderaLogger Log;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The mnemonic. */
	private String mnemonic;
	
	/** The description. */
	private String description;
	
	/** The list participants. */
	private List<SelectItem> listParticipants;
	
	/** The list issuers. */
	private List<SelectItem> listIssuers;
	
	/** The securities searcher. */
	private SecuritiesSearcherTO securitiesSearcher = new SecuritiesSearcherTO();
	
	/** The account searcher. */
	private AccountSearcherTO accountSearcher = new AccountSearcherTO();
	
	/** The securitiesPartHolderAccount searcher. */
	private SecuritiesPartHolAccountTO securitiesPartHolderAccountTO = new SecuritiesPartHolAccountTO();
	
	/** The selected. */
	private SecuritiesTO selected;
	
	/** The list securities. */
	private List<SecuritiesTO> listSecurities;
	
	/** The holder account. */
	private HolderAccount holderAccount;
	
	/** The participant id. */
	private Long participantId;
	
	/** The list instrument type. */
	private List<ParameterTable> listInstrumentType;
	
	/** The flag issue. */
	private boolean flagIssue = true;
	
	//private boolean listEmpty = false;
	
	/**
	 * Inits the.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException {
		holderAccount = new HolderAccount();
		participantId = null;
		//Load combos
		loadFilters();
	}
	
	
	
	/**
	 * Load filters.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadFilters() throws ServiceException{
		listParticipants = new ArrayList<SelectItem>();
		listIssuers      = new ArrayList<SelectItem>();
		listInstrumentType = new ArrayList<ParameterTable>();
		ParameterTableTO paramTableHelp = new ParameterTableTO();
		paramTableHelp.setState(ParameterTableStateType.REGISTERED.getCode());
		
		Participant participant = new Participant();
		for(Participant part : helperComponentFacade.getLisParticipantServiceFacade(participant)) {
			listParticipants.add(new SelectItem(part.getIdParticipantPk(), part.getDescription()));
		}
		
		//Issuers
		IssuerSearcherTO issuerTO = new IssuerSearcherTO();
		for(IssuerTO issu : helperComponentFacade.findIssuerByHelper(issuerTO)) {
			listIssuers.add(new SelectItem(issu.getIssuerCode(), issu.getMnemonic()+" - "+issu.getIssuerDesc()));
		}
		
		paramTableHelp.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
		for(ParameterTable param : generalParametersFacade.getListParameterTableServiceBean(paramTableHelp)) {
			listInstrumentType.add(param);
		}
		if(userInfo.getUserAccountSession().isIssuerInstitucion() || userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
			flagIssue = false;
		}
	}
	
	
	/**
	 * Search securities by isin.
	 *
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public void searchSecuritiesByIsin() throws ServiceException {
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.value}", String.class);
		ValueExpression valExClientId = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.clientId}", String.class);
		
		/**EL for security class list*/
		ValueExpression valSecurityClassList = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.securityClassList}", List.class);
		List<Integer> securityClass = (List<Integer>)valSecurityClassList.getValue(elContext);
		/**If parameter at least has one element, assignment at security TO*/
		if(securityClass!=null && !securityClass.isEmpty()){
			securitiesSearcher.setListClassSecuritie(securityClass);
		}
		
		String clientId = (String) valExClientId.getValue(elContext);
		
			if (valueExpression.getValue(elContext) instanceof String) {
				 String code = (String)valueExpression.getValue(elContext);
				 if (StringUtils.isNotBlank(code)) {
					 securitiesSearcher.setIsinCode(code);
					 selected = helperComponentFacade.findSecurityFromIsin(securitiesSearcher);
					 if (selected != null) {
						 description = selected.getDescription();
						 mnemonic = selected.getMnemonic();
						//new Value
						valueExpression.setValue(elContext, selected.getSecuritiesCode());
					 } else {
						 securitiesSearcher.setIsinCode(null);
						 UIComponent uitextComp = UIComponent.getCurrentComponent(context);
						 if(uitextComp instanceof UIInput) {
							UIInput issuerText = (UIInput)uitextComp;
							issuerText.setValid(false);
						 }
						 description = null;
						 mnemonic = null;
						 valueExpression.setValue(elContext, null);
						 //TODO remover para buscar mensaje
						 JSFUtilities.showMessageOnDialog(
								 PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
								 PropertiesUtilities.getGenericMessage(GeneralPropertiesConstants.LBL_HELPER_SECURITY_NOT_FOUND));
						 JSFUtilities.showSimpleValidationDialog();
					 }
					 
				 } else {
					 //error message
					 UIComponent uitextComp = UIComponent.getCurrentComponent(context);
					 if(uitextComp instanceof UIInput) {
						UIInput issuerText = (UIInput)uitextComp;
						issuerText.setValid(true);
					 }
					 valueExpression.setValue(elContext, null);
					 JSFUtilities.resetComponent(":"+clientId+":isinPaHolCodeHelper");
					 description = null;
					 mnemonic = null;
				 }
			 } else {
				 UIComponent uitextComp = UIComponent.getCurrentComponent(context);
				 if(uitextComp instanceof UIInput) {
					UIInput issuerText = (UIInput)uitextComp;
					issuerText.setValid(true);
				 }
				 valueExpression.setValue(elContext, null);
				 description = null;
				 mnemonic = null;
			 }	
	}
	
	/**
	 * Search securities by filter.
	 */
	@SuppressWarnings("unchecked")
	public void searchSecuritiesByFilter(){
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		
		ValueExpression valExHolderAccount = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.holderAccount}", HolderAccount.class);
		HolderAccount holderAccount = (HolderAccount) valExHolderAccount.getValue(elContext);
		
		ValueExpression valExPArticipantCode = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.partCode}", Long.class);
		Long partCode = (Long)valExPArticipantCode.getValue(elContext);
		
		ValueExpression valExIssuerCode = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.issuer}", String.class);
		String issuer = (String)valExIssuerCode.getValue(elContext);
		
		/**EL for security class list*/
		ValueExpression valSecurityClassList = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.securityClassList}", List.class);
		List<Integer> securityClass = (List<Integer>)valSecurityClassList.getValue(elContext);
		/**If parameter at least has one element, assignment at security TO*/
		if(securityClass!=null && !securityClass.isEmpty()){
			securitiesSearcher.setListClassSecuritie(securityClass);
		}
		
		if(Validations.validateIsNotNullAndPositive(partCode)){
			securitiesSearcher.setParticipantCode(partCode);
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(holderAccount)){
			securitiesSearcher.setHolderAccount(holderAccount);
		}
		if(Validations.validateIsNotNullAndNotEmpty(issuer) && (issuer.compareTo("-1"))!=0){
			securitiesSearcher.setIssuerCode(issuer);
		}
		try {
			if (isInputFilters(securitiesSearcher)) {
				listSecurities = helperComponentFacade.findSecuritiesFromHelper(securitiesSearcher);

			} else {
				//set client isin code
				ValueExpression valueExpression = context.getApplication().getExpressionFactory()
				    .createValueExpression(elContext, "#{cc.resourceBundleMap.requiredMessage}", String.class);
				FacesMessage faceMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						valueExpression.getValue(elContext).toString(), valueExpression.getValue(elContext).toString());
				context.addMessage(null, faceMessage);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * Select securities from helper.
	 */
	public void selectSecuritiesFromHelper() {
		FacesContext context = FacesContext.getCurrentInstance();
		if (selected != null) {
			description = selected.getDescription();
			mnemonic = selected.getMnemonic();
			//set client isin code
			ELContext elContext = context.getELContext();
			ValueExpression valueExpression = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.attrs.value}", String.class);
			//new Value
			valueExpression.setValue(elContext, selected.getSecuritiesCode());
		} else {
			//no se encontro valor
			Log.error("No encontro el valor");
		}
		clean();
	}
	
	/**
	 * Checks if is input filters.
	 *
	 * @param filter the filter
	 * @return true, if is input filters
	 */
	private boolean isInputFilters(SecuritiesSearcherTO filter) {
		boolean check = false;
		if (StringUtils.isNotBlank(filter.getIssuerCode())
				|| StringUtils.isNotBlank(filter.getDescription())
				|| filter.getInstrumentType() != null
				|| filter.getSecuritiesType() != null
				|| filter.getStateSecuritie() != null
				|| filter.getParticipantCode() != null) {
			check = true;
		}
		return check;
	}
	
	/**
	 * Search account by number.
	 *
	 * @throws ServiceException the service exception
	 */
	public void searchAccountByNumber() throws ServiceException{
		if(Validations.validateIsNotNullAndPositive(holderAccount.getAccountNumber()) && Validations.validateIsNotNullAndPositive(holderAccount.getParticipant().getIdParticipantPk())){
			AccountTO selected = new AccountTO();
			
			selected.setAccountNumber(holderAccount.getAccountNumber());
			accountSearcher.setParticipantCode(holderAccount.getParticipant().getIdParticipantPk());
			accountSearcher.setSelected(selected);
			
			selected = helperComponentFacade.findAccountByNumberAndParticipant(accountSearcher);
			
			holderAccount.setAccountNumber(selected.getAccountNumber());
			holderAccount.setAlternateCode(selected.getAlternativeCode());
//			holderAccount.setAccountDescription(selected.getAccountDescription());
		}
	}
	
	/**
	 * Clean.
	 */
	public void clean(){
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		
		ValueExpression valExHolderAccount = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.holderAccount}", HolderAccount.class);
		HolderAccount holderAccount = (HolderAccount) valExHolderAccount.getValue(elContext);
		
		ValueExpression valExClientId = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.clientId}", String.class);
		String clientId = (String) valExClientId.getValue(elContext);
		
		ValueExpression valExParticipantCode = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.partCode}", Long.class);
		
		securitiesSearcher.setIsinCode(null);
		securitiesSearcher.setDescription(null);
		securitiesSearcher.setInstrumentType(null);
		securitiesSearcher.setIssuerCode(null);
		securitiesSearcher.setParticipantCode(null);
		
		InputText txtAccountNumber 				= 	(InputText) JSFUtilities.findViewComponent(":"+clientId+":txtAccountNumber");
		InputText txtAlternateCode 				= 	(InputText) JSFUtilities.findViewComponent(":"+clientId+":txtAlternateCode");
		SelectOneMenu cboParticipantCode		=	(SelectOneMenu) JSFUtilities.findViewComponent(":"+clientId+":cboParticipant");
		
		// CHECKING IF THE NEXT INPUT IF NOT DISABLED TO CLEAR
		if(!cboParticipantCode.isDisabled()){
			valExParticipantCode.setValue(elContext, new Long(-1));
		}
		
		if(!txtAccountNumber.isDisabled()){
			holderAccount.setAccountNumber(null);
		}
		if(!txtAlternateCode.isDisabled()){
			holderAccount.setAlternateCode(null);
		}
		securitiesSearcher.setHolderAccount(holderAccount);
		
		listSecurities = null;
	
	}
	
	/**
	 * Check holder account attribute.
	 *
	 * @param event the event
	 * @return the holder account
	 */
	public HolderAccount checkHolderAccountAttribute(ActionEvent event){
		HolderAccount holderAccount = (HolderAccount) event.getComponent().getAttributes().get("holderAccount");
		if(!Validations.validateIsNotNullAndNotEmpty(holderAccount)){
			holderAccount = new HolderAccount();
		}
		return holderAccount;
	}
	
	//GETTERS AND SETTERS
	/**
	 * Gets the securities part holder account to.
	 *
	 * @return the securities part holder account to
	 */
	public SecuritiesPartHolAccountTO getSecuritiesPartHolderAccountTO() {
		return securitiesPartHolderAccountTO;
	}
	
	/**
	 * Sets the securities part holder account to.
	 *
	 * @param securitiesPartHolderAccountTO the new securities part holder account to
	 */
	public void setSecuritiesPartHolderAccountTO(
			SecuritiesPartHolAccountTO securitiesPartHolderAccountTO) {
		this.securitiesPartHolderAccountTO = securitiesPartHolderAccountTO;
	}
	
	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}
	
	/**
	 * Sets the mnemonic.
	 *
	 * @param mnemonic the new mnemonic
	 */
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the list participants.
	 *
	 * @return the list participants
	 */
	public List<SelectItem> getListParticipants() {
		return listParticipants;
	}
	
	/**
	 * Sets the list participants.
	 *
	 * @param listParticipants the new list participants
	 */
	public void setListParticipants(List<SelectItem> listParticipants) {
		this.listParticipants = listParticipants;
	}
	
	/**
	 * Gets the list issuers.
	 *
	 * @return the list issuers
	 */
	public List<SelectItem> getListIssuers() {
		return listIssuers;
	}
	
	/**
	 * Sets the list issuers.
	 *
	 * @param listIssuers the new list issuers
	 */
	public void setListIssuers(List<SelectItem> listIssuers) {
		this.listIssuers = listIssuers;
	}
	
	/**
	 * Gets the securities searcher.
	 *
	 * @return the securities searcher
	 */
	public SecuritiesSearcherTO getSecuritiesSearcher() {
		return securitiesSearcher;
	}
	
	/**
	 * Sets the securities searcher.
	 *
	 * @param securitiesSearcher the new securities searcher
	 */
	public void setSecuritiesSearcher(SecuritiesSearcherTO securitiesSearcher) {
		this.securitiesSearcher = securitiesSearcher;
	}
	
	/**
	 * Gets the selected.
	 *
	 * @return the selected
	 */
	public SecuritiesTO getSelected() {
		return selected;
	}
	
	/**
	 * Sets the selected.
	 *
	 * @param selected the new selected
	 */
	public void setSelected(SecuritiesTO selected) {
		this.selected = selected;
	}
	
	/**
	 * Gets the list securities.
	 *
	 * @return the list securities
	 */
	public List<SecuritiesTO> getListSecurities() {
		return listSecurities;
	}
	
	/**
	 * Sets the list securities.
	 *
	 * @param listSecurities the new list securities
	 */
	public void setListSecurities(List<SecuritiesTO> listSecurities) {
		this.listSecurities = listSecurities;
	}

	/**
	 * Gets the list instrument type.
	 *
	 * @return the list instrument type
	 */
	public List<ParameterTable> getListInstrumentType() {
		return listInstrumentType;
	}
	
	/**
	 * Sets the list instrument type.
	 *
	 * @param listInstrumentType the new list instrument type
	 */
	public void setListInstrumentType(List<ParameterTable> listInstrumentType) {
		this.listInstrumentType = listInstrumentType;
	}

	/**
	 * Gets the participant id.
	 *
	 * @return the participant id
	 */
	public Long getParticipantId() {
		return participantId;
	}

	/**
	 * Sets the participant id.
	 *
	 * @param participantId the new participant id
	 */
	public void setParticipantId(Long participantId) {
		this.participantId = participantId;
	}



	/**
	 * Gets the max results query helper.
	 *
	 * @return the max results query helper
	 */
	public Integer getMaxResultsQueryHelper() {
		return maxResultsQueryHelper;
	}



	/**
	 * Checks if is flag issue.
	 *
	 * @return true, if is flag issue
	 */
	public boolean isFlagIssue() {
		return flagIssue;
	}



	/**
	 * Sets the flag issue.
	 *
	 * @param flagIssue the new flag issue
	 */
	public void setFlagIssue(boolean flagIssue) {
		this.flagIssue = flagIssue;
	}
	
}
