package com.pradera.core.component.generalparameter.facade;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.MathContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.GeographicLocationServiceBean;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.ValidationAccountAnnotationCupon;
import com.pradera.integration.common.validation.to.ValidationAccountAnnotationOperation;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.corporatives.TaxHolderRate;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.FinancialIndicator;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.issuancesecuritie.type.CalendarDayType;
import com.pradera.model.issuancesecuritie.type.InstrumentType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary. Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class GeneralParametersFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 25/01/2013
 */
@Stateless
@Performance
public class GeneralParametersFacade {

	/** The geographic location service bean. */
	@EJB
	GeographicLocationServiceBean geographicLocationServiceBean;

	/** The parameter service bean. */
	@EJB
	ParameterServiceBean parameterServiceBean;

	/** The holiday query service bean. */
	@EJB
	HolidayQueryServiceBean holidayQueryServiceBean;

	@EJB
	private GeneralParametersFacade generalParameterFacade;
	/**
	 * Gets the list geographic location service facade.
	 *
	 * @param filter the filter
	 * @return the list geographic location service facade
	 * @throws ServiceException the service exception
	 */
	public List<GeographicLocation> getListGeographicLocationServiceFacade(
			GeographicLocationTO filter) throws ServiceException {
		return geographicLocationServiceBean.getListGeographicLocation(filter);
	}

	/**
	 * Gets the list parameter table service bean.
	 *
	 * @param filter the filter
	 * @return the list parameter table service bean
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getListParameterTableServiceBean(
			ParameterTableTO filter) throws ServiceException {
		return parameterServiceBean.getListParameterTableServiceBean(filter);
	}

	/**
	 * Gets the combo parameter table.
	 *
	 * @param filter
	 *            the filter
	 * @return the combo parameter table
	 * @throws ServiceException
	 *             the service exception
	 */
	public List<ParameterTable> getComboParameterTable(ParameterTableTO filter)
			throws ServiceException {
		return parameterServiceBean.getComboParameterTable(filter);
	}

	/**
	 * Find geographic location by id service facade.
	 *
	 * @param filter
	 *            the filter
	 * @return the geographic location
	 * @throws ServiceException
	 *             the service exception
	 */
	public GeographicLocation findGeographicLocationByIdServiceFacade(
			GeographicLocationTO filter) throws ServiceException {
		return geographicLocationServiceBean
				.findGeographicLocationByIdServiceBean(filter);
	}

	/**
	 * Gets the list holiday date service facade.
	 *
	 * @param filter
	 *            the filter
	 * @return the list holiday date service facade
	 * @throws ServiceException
	 *             the service exception
	 */
	public List<Date> getListHolidayDateServiceFacade(Holiday filter)
			throws ServiceException {
		return holidayQueryServiceBean.getListHolidayDateServiceBean(filter);
	}

	/**
	 * Gets the all holidays by country.
	 *
	 * @param countryResidence
	 *            the country residence
	 * @return the all holidays by country
	 * @throws ServiceException
	 *             the service exception
	 */
	public String getAllHolidaysByCountry(Integer countryResidence)
			throws ServiceException {
		String strReturn = null;
		Holiday holidayFilter = new Holiday();
		holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		GeographicLocation countryFilter = new GeographicLocation();
		countryFilter.setIdGeographicLocationPk(countryResidence);
		holidayFilter.setGeographicLocation(countryFilter);
		strReturn = CommonsUtilities.convertListDateToString(holidayQueryServiceBean
						.getListHolidayDateServiceBean(holidayFilter));
		return strReturn;
	}

	/**
	 * Gets the list parameter table exclude pk service facade.
	 *
	 * @param filter
	 *            the filter
	 * @return the list parameter table exclude pk service facade
	 * @throws ServiceException
	 *             the service exception
	 */
	public List<ParameterTable> getListParameterTableExcludePkServiceFacade(
			ParameterTableTO filter) throws ServiceException {
		return parameterServiceBean
				.getListParameterTableExcludePkServiceBean(filter);
	}

	/**
	 * Checks if is util date service facade.
	 *
	 * @param date
	 *            the date
	 * @return true, if is util date service facade
	 * @throws ServiceException
	 *             the service exception
	 */
	public boolean isUtilDateServiceFacade(Date date) throws ServiceException {
		return CommonsUtilities.isUtilDate(date)
				&& !holidayQueryServiceBean.isHolidayDateServiceBean(date);
	}

	/**
	 * Working date calculate service facade.
	 *
	 * @param date
	 *            the date
	 * @param intervalDays
	 *            the interval days
	 * @return the date
	 * @throws ServiceException
	 *             the service exception
	 */
	public Date workingDateCalculateServiceFacade(Date date, Integer intervalDays) throws ServiceException {
		Calendar calWorkingDate = Calendar.getInstance();
		calWorkingDate.setTime(date);
		while (!CommonsUtilities.isUtilDate(calWorkingDate.getTime()) || holidayQueryServiceBean.isHolidayDateServiceBean(calWorkingDate.getTime())) {
			calWorkingDate.add(Calendar.DAY_OF_MONTH, Integer.valueOf(intervalDays));
		}
		return calWorkingDate.getTime();
	}

	/**
	 * Gets the non working days between dates service facade.
	 *
	 * @param initialDate
	 *            the initial date
	 * @param finalDate
	 *            the final date
	 * @param country
	 *            the country
	 * @return the non working days between dates service facade
	 * @throws ServiceException
	 *             the service exception
	 */
	public Integer getNonWorkingDaysBetweenDatesServiceFacade(Date initialDate,
			Date finalDate, Integer country) throws ServiceException {
		return holidayQueryServiceBean.getNonWorkingDaysBetweenDates(
				initialDate, finalDate, country);
	}

	/**
	 * Gets the tax holder rate.
	 *
	 * @return the tax holder rate
	 * @throws ServiceException
	 *             the service exception
	 */
	public TaxHolderRate getTaxHolderRate() throws ServiceException {
		List<TaxHolderRate> list = parameterServiceBean.getTaxHolderRate();
		for (TaxHolderRate thr : list) {
			if (thr.getTaxRate().compareTo(BigDecimal.ZERO) == BooleanType.YES
					.getCode()) {
				return thr;

			}
		}
		return null;
	}

	/**
	 * Find financial indicator service facade.
	 *
	 * @param indexAndRateFilter
	 *            the index and rate filter
	 * @return the financial indicator
	 * @throws ServiceException
	 *             the service exception
	 */
	public FinancialIndicator findFinancialIndicatorServiceFacade(
			Map<String, Object> indexAndRateFilter) throws ServiceException {
		return parameterServiceBean
				.findFinancialIndicatorServiceBean(indexAndRateFilter);
	}

	/**
	 * Gets the param detail service facade.
	 *
	 * @param idParameterTablePk
	 *            the id parameter table pk
	 * @return the param detail service facade
	 */
	public ParameterTable getParamDetailServiceFacade(Integer idParameterTablePk) {
		return parameterServiceBean.getParameterDetail(idParameterTablePk);
	}

	/**
	 * Gets the parameter table by id.
	 *
	 * @param parameterTablePk
	 *            the parameter table pk
	 * @return the parameter table by id
	 * @throws ServiceException
	 *             the service exception
	 */
	public ParameterTable getParameterTableById(Integer parameterTablePk)
			throws ServiceException {
		return parameterServiceBean.getParameterTableById(parameterTablePk);
	}

	/**
	 * Gets the max day for requests to cancell.
	 *
	 * @param masterTableFk
	 *            the master table fk
	 * @return the max day for requests to cancell
	 * @throws ServiceException
	 *             the service exception
	 */
	public Integer getMaxDayForRequestsToCancell(Integer masterTableFk)
			throws ServiceException {
		ParameterTableTO filter = new ParameterTableTO();
		filter.setMasterTableFk(masterTableFk);
		List<ParameterTable> days = parameterServiceBean
				.getListParameterTableServiceBean(filter);
		for (ParameterTable param : days) {
			return param.getShortInteger();
		}

		return null;
	}

	/**
	 * Gets the last working day between dates.
	 *
	 * @param initialDate
	 *            the initial date
	 * @param finalDate
	 *            the final date
	 * @return the last working day between dates
	 */
	public Date getLastWorkingDayBetweenDates(Date initialDate, Date finalDate) {
		return holidayQueryServiceBean.getLastWorkingDayBetweenDates(
				initialDate, finalDate);
	}

	/**
	 * Gets the list parameter table service bean.
	 *
	 * @param masterTableFk
	 *            the master table fk
	 * @return the list parameter table service bean
	 * @throws ServiceException
	 *             the service exception
	 */
	public List<ParameterTable> getListParameterTableServiceBean(
			Integer masterTableFk) throws ServiceException {
		return parameterServiceBean
				.getListParameterTableServiceBean(masterTableFk);
	}

	public DailyExchangeRates getdailyExchangeRate(Date currentDate,
			Integer currency) {
		return parameterServiceBean.getDayExchangeRate(currentDate, currency);
	}

	public List<ParameterTable> getListEconomicActivityBySector(
			Integer economicSector) throws ServiceException {
		return parameterServiceBean
				.getListEconomicActivityBySector(economicSector,null,null);
	}

	public List<ParameterTable> getListSocietyTypeBySector(
			Integer economicSector) throws ServiceException {
		return parameterServiceBean.getListSocietyTypeBySector(economicSector);
	}
	
	public List<ProcessFileTO> getProcessedFilesInformation(Date processDate , String interfaceName) {
		return parameterServiceBean.getPRocessedFilesInformation(processDate,interfaceName);	
	}

	public byte[] getInterfaceUploadedFileContent(Long idInterfaceReception) {
		return parameterServiceBean.getInterfaceUploadedFileContent(idInterfaceReception);
	}

	public byte[] getInterfaceResponseFileContent(Long idInterfaceReception) {
		return parameterServiceBean.getInterfaceResponseFileContent(idInterfaceReception);
	}

	public ProcessFileTO getExternalInterfaceInformation(String interfaceName) {
		return parameterServiceBean.getExternalInterfaceInformation(interfaceName);
	}


	/**
	 * Get Security Class By Parameters :
	 * @param instrumentType
	 * @param economicSector
	 * @param offerType
	 * @return
	 * @throws ServiceException
	 */
	public List<ParameterTable> getListSecuritiesClassSetup(Integer instrumentType,Integer economicSector, Integer offerType) throws ServiceException{
		Map<String,Object> params=new HashMap<String, Object>();
		params.put("economicSectorPrm",economicSector);
//		if(instrumentType.equals(InstrumentType.VARIABLE_INCOME.getCode())){
//			offerType=1762;
//		}
		params.put("offerType",offerType);
		params.put("instrumentType",instrumentType);
		
		return parameterServiceBean.getListSecuritiesClassSetup(params);
	}
	
	public Boolean isReportInPartIssuerGroup(Long reportId){
		return parameterServiceBean.isReportInPartIssuerGroup(reportId);
	}
	
public List<ValidationAccountAnnotationOperation> setXLSXToEntityAccountAnnotationGarantias(InputStream processFile,Long idParticipantPk) throws IOException, ParseException, Exception {
		
		List<ValidationAccountAnnotationOperation> lstAccountAnnotationOperations = new ArrayList<ValidationAccountAnnotationOperation>();
		ValidationAccountAnnotationOperation validationAccountAnnotationOperation = null;
		
		Integer sheet = 0;
		Map<Integer, String> mapCellNumberWithDataType = new HashedMap<Integer, String>();
		//	mapCellNumberWithDataType.put(0, "String");//NEMONICO DEPOSITANTE	//0
		mapCellNumberWithDataType.put(0, "String");//NUMERO CUENTA				//1
		mapCellNumberWithDataType.put(1, "String");//SERIE						//2
		mapCellNumberWithDataType.put(2, "String");//CERTIFICADO				//3
		mapCellNumberWithDataType.put(3, "String");//NEMONICO EMISOR			//4
		mapCellNumberWithDataType.put(4, "BigDecimal");//VALOR NOMINAL			//5
		mapCellNumberWithDataType.put(5, "String");//MONEDA						//6
		mapCellNumberWithDataType.put(6, "BigDecimal");//TASA ANUAL				//7
		mapCellNumberWithDataType.put(7, "Date");//FECHA EMISION				//8
		mapCellNumberWithDataType.put(8, "Date");//FECHA VENCIMIENTO			//9
		mapCellNumberWithDataType.put(9, "String");//PERIDIOCIDAD				//10
		mapCellNumberWithDataType.put(10, "String");//TIENE CUPONES ELECTRONICO //11
		mapCellNumberWithDataType.put(11, "String");//GUARDA
		mapCellNumberWithDataType.put(12, "String");//OBSERVACION
		mapCellNumberWithDataType.put(13, "String");//11 NEMONICO PARTICIPANTE CONTRAPARTE
		mapCellNumberWithDataType.put(14, "BigDecimal");//12 PRECIO (TASA)
		
 		XSSFWorkbook book = new XSSFWorkbook(processFile);
		XSSFSheet sheetOperation = book.getSheetAt(sheet);
		XSSFRow row; XSSFCell cell;
		int rowFirst = 1, lastRow = sheetOperation.getLastRowNum();
		int lastCell = mapCellNumberWithDataType.size();//cellFirst = 0, 
		
		for (int filaExtrac = rowFirst; filaExtrac <= lastRow; filaExtrac++) {
			
			row = sheetOperation.getRow(filaExtrac);
			
			if (Validations.validateIsNotNull(row)) {
				if(!isRowEmpty(row)) {
					validationAccountAnnotationOperation = new ValidationAccountAnnotationOperation();
					for(int cellPosition=0; cellPosition<= lastCell; cellPosition++) {
						
						cell = row.getCell(cellPosition);
						String dataType = mapCellNumberWithDataType.get(cellPosition);
						String cellValue = getCellGenericInString(cell, dataType);

						validationAccountAnnotationOperation.setParticipant(idParticipantPk.toString());	
						
						if(cellPosition == 0) {
							validationAccountAnnotationOperation.setHolderAccountNumber(cellValue);
						}else if(cellPosition == 1) {
							if(cellValue!=null) {
								validationAccountAnnotationOperation.setSerialNumber(cellValue.toUpperCase());
							}
						}else if(cellPosition == 2) {
							if(cellValue!=null) {
								validationAccountAnnotationOperation.setCertificateNumber(cellValue.toUpperCase());
							}
						}else if(cellPosition == 3) {
							validationAccountAnnotationOperation.setMnemonicIssuer(cellValue);
						}else if(cellPosition == 4) {
							validationAccountAnnotationOperation.setSecurityNominalValue(cellValue);
						}else if(cellPosition == 5) {
							validationAccountAnnotationOperation.setSecurityCurrency(cellValue);
						}else if(cellPosition == 6) {
							validationAccountAnnotationOperation.setSecurityInterestRate(cellValue);
						}else if(cellPosition == 7) {
							validationAccountAnnotationOperation.setExpeditionDate(cellValue);
						}else if(cellPosition == 8) {
							validationAccountAnnotationOperation.setSecurityExpirationDate(cellValue);
						}else if(cellPosition == 9) {
							validationAccountAnnotationOperation.setPeriodicity(cellValue);
						}else if(cellPosition == 10) {
							validationAccountAnnotationOperation.setIndElectronicCupon(cellValue);
						}else if(cellPosition == 11) {
							validationAccountAnnotationOperation.setMotive(cellValue);
						}else if(cellPosition == 12) {
							validationAccountAnnotationOperation.setObservation(cellValue);
						}else if(cellPosition == 13) {
							validationAccountAnnotationOperation.setParticipantSeller(cellValue);
						}else if(cellPosition == 14) {
							validationAccountAnnotationOperation.setAmountRate(cellValue);	//precio sucio
						}
					}
					lstAccountAnnotationOperations.add(validationAccountAnnotationOperation);
				}
			}
			
		}
		
		//**************
		mapCellNumberWithDataType = new HashedMap<Integer, String>();
		mapCellNumberWithDataType.put(0, "String");//SERIE			0
		mapCellNumberWithDataType.put(1, "String");//CERTIFICADO	1
		mapCellNumberWithDataType.put(2, "Integer");//NRO CUPON		2
		//mapCellNumberWithDataType.put(3, "Date");//F INICIO		3
		mapCellNumberWithDataType.put(3, "Date");//F VENCIMIENTO	4
		//mapCellNumberWithDataType.put(5, "Date");//F PAGO			5
		//mapCellNumberWithDataType.put(6, "BigDecimal");//TASA		6
		mapCellNumberWithDataType.put(4, "BigDecimal");//MONTO		7
		mapCellNumberWithDataType.put(5, "String");//NEMONICO EMISOR 
		mapCellNumberWithDataType.put(6, "String");//MONEDA				
		
		ValidationAccountAnnotationCupon validationAccountAnnotationCupon = null;
		List<ValidationAccountAnnotationCupon> lstValidationAccountAnnotationCupon = new ArrayList<ValidationAccountAnnotationCupon>();
		
		sheet = 1;
		sheetOperation = book.getSheetAt(sheet);
		lastRow = sheetOperation.getLastRowNum();
		lastCell = mapCellNumberWithDataType.size(); 
		
		for (int filaExtrac = rowFirst; filaExtrac <= lastRow; filaExtrac++) {
			
			row = sheetOperation.getRow(filaExtrac);
			if (Validations.validateIsNotNull(row)) {
				if(!isRowEmpty(row)) {
					validationAccountAnnotationCupon = new ValidationAccountAnnotationCupon();
					for(int cellPosition=0; cellPosition<= lastCell; cellPosition++) {
						
						cell = row.getCell(cellPosition);
						String dataType = mapCellNumberWithDataType.get(cellPosition);
						String cellValue = getCellGenericInString(cell, dataType);
	
						if(cellPosition == 0) {
							validationAccountAnnotationCupon.setSerialNumber(cellValue.toUpperCase());
						}else if(cellPosition == 1) {
							validationAccountAnnotationCupon.setCertificateNumber(cellValue);
						}else if(cellPosition == 2) {
							validationAccountAnnotationCupon.setNroCupon(cellValue);
						}else if(cellPosition == 3) {
							validationAccountAnnotationCupon.setExpirationDate(cellValue);
						}else if(cellPosition == 4) {
							validationAccountAnnotationCupon.setCuponAmount(cellValue);
						}else if(cellPosition == 5) {
							validationAccountAnnotationCupon.setMnemonicIssuer(cellValue);
						}else if(cellPosition == 6) {
							validationAccountAnnotationCupon.setSecurityCurrency(cellValue);
						}
						
						validationAccountAnnotationCupon.setBeginingDate(cellValue);
						validationAccountAnnotationCupon.setPaymentDate(cellValue);
						validationAccountAnnotationCupon.setInterestRate(cellValue);
						
					}
					lstValidationAccountAnnotationCupon.add(validationAccountAnnotationCupon);
				}
			}
		}
		
		
		
		List<ValidationAccountAnnotationOperation>  reordenValidationAccountAnnotationOperation = reordenValidationAccountAnnotationOperation(lstAccountAnnotationOperations, lstValidationAccountAnnotationCupon);
		/*List<ValidationAccountAnnotationCupon> lstTmpGenerateValidationsAccount = null;
		for(ValidationAccountAnnotationOperation current : reordenValidationAccountAnnotationOperation) {
			lstTmpGenerateValidationsAccount = lstTmpGenerateValidationsAccount( current, current.getValidationAccountAnnotationCupons().size() );	
		}
		
		lstTmpGenerateValidationsAccount.size();*/
		
		return reordenValidationAccountAnnotationOperation;
		
	}

	public List<ValidationAccountAnnotationOperation> setXLSXToEntityAccountAnnotationPagares(InputStream processFile,Long idParticipantPk) throws IOException, ParseException, Exception {

		List<ValidationAccountAnnotationOperation> lstAccountAnnotationOperations = new ArrayList<ValidationAccountAnnotationOperation>();
		ValidationAccountAnnotationOperation validationAccountAnnotationOperation = null;
		
		Integer sheet = 0;
		Map<Integer, String> mapCellNumberWithDataType = new HashedMap<Integer, String>();
		//	mapCellNumberWithDataType.put(0, "String");//NEMONICO DEPOSITANTE	//0
		mapCellNumberWithDataType.put(0, "String");//NUMERO CUENTA				//1
		mapCellNumberWithDataType.put(1, "String");//SERIE						//2
		mapCellNumberWithDataType.put(2, "String");//CERTIFICADO				//3
		mapCellNumberWithDataType.put(3, "String");//NEMONICO EMISOR			//4
		mapCellNumberWithDataType.put(4, "BigDecimal");//VALOR NOMINAL			//5
		mapCellNumberWithDataType.put(5, "String");//MONEDA						//6
		mapCellNumberWithDataType.put(6, "BigDecimal");//TASA ANUAL				//7
		mapCellNumberWithDataType.put(7, "Date");//FECHA EMISION				//8
		mapCellNumberWithDataType.put(8, "Date");//FECHA VENCIMIENTO			//9
		mapCellNumberWithDataType.put(9, "String");//PERIDIOCIDAD				//10
		mapCellNumberWithDataType.put(10, "String");//EXONERADO DE IMPUESTOS	
		mapCellNumberWithDataType.put(11, "String");//GUARDA
		mapCellNumberWithDataType.put(12, "String");//OBSERVACION

		mapCellNumberWithDataType.put(13, "String");//NUMERO CUENTA BENEFICIARIO
		mapCellNumberWithDataType.put(14, "String");//NUMERO CUENTA DEUDOR
		mapCellNumberWithDataType.put(15, "String");//NEMONICO PARTICIPANTE CONTRAPARTE
		mapCellNumberWithDataType.put(16, "BigDecimal");//PRECIO (TASA)
		
 		XSSFWorkbook book = new XSSFWorkbook(processFile);
		XSSFSheet sheetOperation = book.getSheetAt(sheet);
		XSSFRow row; XSSFCell cell;
		int rowFirst = 1, lastRow = sheetOperation.getLastRowNum();
		int lastCell = mapCellNumberWithDataType.size();//cellFirst = 0, 
		
		for (int filaExtrac = rowFirst; filaExtrac <= lastRow; filaExtrac++) {
			
			row = sheetOperation.getRow(filaExtrac);
			
			if (Validations.validateIsNotNull(row)) {
				if(!isRowEmpty(row)) {
					validationAccountAnnotationOperation = new ValidationAccountAnnotationOperation();
					for(int cellPosition=0; cellPosition<= lastCell; cellPosition++) {
						
						cell = row.getCell(cellPosition);
						String dataType = mapCellNumberWithDataType.get(cellPosition);
						String cellValue = getCellGenericInString(cell, dataType);

						validationAccountAnnotationOperation.setParticipant(idParticipantPk.toString());	
						
						if(cellPosition == 0) {
							validationAccountAnnotationOperation.setHolderAccountNumber(cellValue);
						}else if(cellPosition == 1) {
							if(cellValue!=null) {
								validationAccountAnnotationOperation.setSerialNumber(cellValue.toUpperCase());	
							}
						}else if(cellPosition == 2) {
							if(cellValue!=null) {
								validationAccountAnnotationOperation.setCertificateNumber(cellValue.toUpperCase());
							}
						}else if(cellPosition == 3) {
							validationAccountAnnotationOperation.setMnemonicIssuer(cellValue);
						}else if(cellPosition == 4) {
							validationAccountAnnotationOperation.setSecurityNominalValue(cellValue);
						}else if(cellPosition == 5) {
							validationAccountAnnotationOperation.setSecurityCurrency(cellValue);
						}else if(cellPosition == 6) {
							validationAccountAnnotationOperation.setSecurityInterestRate(cellValue);
						}else if(cellPosition == 7) {
							validationAccountAnnotationOperation.setExpeditionDate(cellValue);
						}else if(cellPosition == 8) {
							validationAccountAnnotationOperation.setSecurityExpirationDate(cellValue);
						}else if(cellPosition == 9) {
							validationAccountAnnotationOperation.setPeriodicity(cellValue);
						}else if(cellPosition == 10) {
							//validationAccountAnnotationOperation.setIndElectronicCupon(cellValue);
							validationAccountAnnotationOperation.setTaxExoneration(cellValue);
						}else if(cellPosition == 11) {
							validationAccountAnnotationOperation.setMotive(cellValue);
						}else if(cellPosition == 12) {
							validationAccountAnnotationOperation.setObservation(cellValue);
						}else if(cellPosition == 13) {
							validationAccountAnnotationOperation.setHolderAccountNumberBenefi(cellValue);
						}else if(cellPosition == 14) {
							validationAccountAnnotationOperation.setHolderAccountNumberDebtor(cellValue);
						}else if(cellPosition == 15) {
							validationAccountAnnotationOperation.setParticipantSeller(cellValue);
						}else if(cellPosition == 16) {
							validationAccountAnnotationOperation.setAmountRate(cellValue);	//precio sucio
						}
					}
					lstAccountAnnotationOperations.add(validationAccountAnnotationOperation);
				}
			}
			
		}
		
		//**************
		mapCellNumberWithDataType = new HashedMap<Integer, String>();
		mapCellNumberWithDataType.put(0, "String");//SERIE			0
		mapCellNumberWithDataType.put(1, "String");//CERTIFICADO	1
		mapCellNumberWithDataType.put(2, "Integer");//NRO CUPON		2
		//mapCellNumberWithDataType.put(3, "Date");//F INICIO		3
		mapCellNumberWithDataType.put(3, "Date");//F VENCIMIENTO	4
		//mapCellNumberWithDataType.put(5, "Date");//F PAGO			5
		//mapCellNumberWithDataType.put(6, "BigDecimal");//TASA		6
		mapCellNumberWithDataType.put(4, "BigDecimal");//MONTO		7
		mapCellNumberWithDataType.put(5, "String");//NEMONICO EMISOR 
		mapCellNumberWithDataType.put(6, "String");//MONEDA				
		
		ValidationAccountAnnotationCupon validationAccountAnnotationCupon = null;
		List<ValidationAccountAnnotationCupon> lstValidationAccountAnnotationCupon = new ArrayList<ValidationAccountAnnotationCupon>();
		
		sheet = 1;
		sheetOperation = book.getSheetAt(sheet);
		lastRow = sheetOperation.getLastRowNum();
		lastCell = mapCellNumberWithDataType.size(); 
		
		for (int filaExtrac = rowFirst; filaExtrac <= lastRow; filaExtrac++) {
			
			row = sheetOperation.getRow(filaExtrac);
			if (Validations.validateIsNotNull(row)) {
				if(!isRowEmpty(row)) {
					validationAccountAnnotationCupon = new ValidationAccountAnnotationCupon();
					for(int cellPosition=0; cellPosition<= lastCell; cellPosition++) {
						
						cell = row.getCell(cellPosition);
						String dataType = mapCellNumberWithDataType.get(cellPosition);
						String cellValue = getCellGenericInString(cell, dataType);
	
						if(cellPosition == 0) {
							validationAccountAnnotationCupon.setSerialNumber(cellValue.toUpperCase());
						}else if(cellPosition == 1) {
							validationAccountAnnotationCupon.setCertificateNumber(cellValue);
						}else if(cellPosition == 2) {
							validationAccountAnnotationCupon.setNroCupon(cellValue);
						}else if(cellPosition == 3) {
							validationAccountAnnotationCupon.setExpirationDate(cellValue);
						}else if(cellPosition == 4) {
							validationAccountAnnotationCupon.setCuponAmount(cellValue);
						}else if(cellPosition == 5) {
							validationAccountAnnotationCupon.setMnemonicIssuer(cellValue);
						}else if(cellPosition == 6) {
							validationAccountAnnotationCupon.setSecurityCurrency(cellValue);
						}
						
						validationAccountAnnotationCupon.setBeginingDate(cellValue);
						validationAccountAnnotationCupon.setPaymentDate(cellValue);
						validationAccountAnnotationCupon.setInterestRate(cellValue);
						
					}
					lstValidationAccountAnnotationCupon.add(validationAccountAnnotationCupon);
				}
			}
		}
		
		
		
		List<ValidationAccountAnnotationOperation>  reordenValidationAccountAnnotationOperation = reordenValidationAccountAnnotationOperation(lstAccountAnnotationOperations, lstValidationAccountAnnotationCupon);
		/*List<ValidationAccountAnnotationCupon> lstTmpGenerateValidationsAccount = null;
		for(ValidationAccountAnnotationOperation current : reordenValidationAccountAnnotationOperation) {
			lstTmpGenerateValidationsAccount = lstTmpGenerateValidationsAccount( current, current.getValidationAccountAnnotationCupons().size() );	
			
		}
		
		lstTmpGenerateValidationsAccount.size();*/
		
		return reordenValidationAccountAnnotationOperation;
		
	}

	public List<ValidationAccountAnnotationOperation> setXLSXToEntityAccountAnnotationCDA(InputStream processFile,Long idParticipantPk) throws IOException, ParseException, Exception {
		
		List<ValidationAccountAnnotationOperation> lstAccountAnnotationOperations = new ArrayList<ValidationAccountAnnotationOperation>();
		ValidationAccountAnnotationOperation validationAccountAnnotationOperation = null;
		
		Integer sheet = 0;
		Map<Integer, String> mapCellNumberWithDataType = new HashedMap<Integer, String>();
		//	mapCellNumberWithDataType.put(0, "String");//NEMONICO DEPOSITANTE	//0
		mapCellNumberWithDataType.put(0, "String");//NUMERO CUENTA				//1
		mapCellNumberWithDataType.put(1, "String");//SERIE						//2
		mapCellNumberWithDataType.put(2, "String");//CERTIFICADO				//3
		mapCellNumberWithDataType.put(3, "String");//NEMONICO EMISOR			//4
		mapCellNumberWithDataType.put(4, "BigDecimal");//VALOR NOMINAL			//5
		mapCellNumberWithDataType.put(5, "String");//MONEDA						//6
		mapCellNumberWithDataType.put(6, "BigDecimal");//TASA ANUAL				//7
		mapCellNumberWithDataType.put(7, "Date");//FECHA EMISION				//8
		mapCellNumberWithDataType.put(8, "Date");//FECHA VENCIMIENTO			//9
		mapCellNumberWithDataType.put(9, "String");//PERIDIOCIDAD				//10
		mapCellNumberWithDataType.put(10, "String");//TIENE CUPONES ELECTRONICO //11
		mapCellNumberWithDataType.put(11, "String");//GUARDA
		mapCellNumberWithDataType.put(12, "String");//OBSERVACION
		mapCellNumberWithDataType.put(13, "String");//11 NEMONICO PARTICIPANTE CONTRAPARTE
		mapCellNumberWithDataType.put(14, "BigDecimal");//12 PRECIO (TASA)
		
 		XSSFWorkbook book = new XSSFWorkbook(processFile);
		XSSFSheet sheetOperation = book.getSheetAt(sheet);
		XSSFRow row; XSSFCell cell;
		int rowFirst = 1, lastRow = sheetOperation.getLastRowNum();
		int lastCell = mapCellNumberWithDataType.size();//cellFirst = 0, 
		
		for (int filaExtrac = rowFirst; filaExtrac <= lastRow; filaExtrac++) {
			
			row = sheetOperation.getRow(filaExtrac);
			
			if (Validations.validateIsNotNull(row)) {
				if(!isRowEmpty(row)) {
					validationAccountAnnotationOperation = new ValidationAccountAnnotationOperation();
					for(int cellPosition=0; cellPosition<= lastCell; cellPosition++) {
						
						cell = row.getCell(cellPosition);
						String dataType = mapCellNumberWithDataType.get(cellPosition);
						String cellValue = getCellGenericInString(cell, dataType);

						validationAccountAnnotationOperation.setParticipant(idParticipantPk.toString());	
						
						if(cellPosition == 0) {
							validationAccountAnnotationOperation.setHolderAccountNumber(cellValue);
						}else if(cellPosition == 1) {
							if(cellValue!=null) {
								validationAccountAnnotationOperation.setSerialNumber(cellValue.toUpperCase());
							}
						}else if(cellPosition == 2) {
							if(cellValue!=null) {
								validationAccountAnnotationOperation.setCertificateNumber(cellValue.toUpperCase());
							}
						}else if(cellPosition == 3) {
							validationAccountAnnotationOperation.setMnemonicIssuer(cellValue);
						}else if(cellPosition == 4) {
							validationAccountAnnotationOperation.setSecurityNominalValue(cellValue);
						}else if(cellPosition == 5) {
							validationAccountAnnotationOperation.setSecurityCurrency(cellValue);
						}else if(cellPosition == 6) {
							validationAccountAnnotationOperation.setSecurityInterestRate(cellValue);
						}else if(cellPosition == 7) {
							validationAccountAnnotationOperation.setExpeditionDate(cellValue);
						}else if(cellPosition == 8) {
							validationAccountAnnotationOperation.setSecurityExpirationDate(cellValue);
						}else if(cellPosition == 9) {
							validationAccountAnnotationOperation.setPeriodicity(cellValue);
						}else if(cellPosition == 10) {
							validationAccountAnnotationOperation.setIndElectronicCupon(cellValue);
						}else if(cellPosition == 11) {
							validationAccountAnnotationOperation.setMotive(cellValue);
						}else if(cellPosition == 12) {
							validationAccountAnnotationOperation.setObservation(cellValue);
						}else if(cellPosition == 13) {
							validationAccountAnnotationOperation.setParticipantSeller(cellValue);
						}else if(cellPosition == 14) {
							validationAccountAnnotationOperation.setAmountRate(cellValue);	//precio sucio
						}
					}
					lstAccountAnnotationOperations.add(validationAccountAnnotationOperation);
				}
			}
			
		}
		
		//**************
		mapCellNumberWithDataType = new HashedMap<Integer, String>();
		mapCellNumberWithDataType.put(0, "String");//SERIE			0
		mapCellNumberWithDataType.put(1, "String");//CERTIFICADO	1
		mapCellNumberWithDataType.put(2, "Integer");//NRO CUPON		2
		//mapCellNumberWithDataType.put(3, "Date");//F INICIO		3
		mapCellNumberWithDataType.put(3, "Date");//F VENCIMIENTO	4
		//mapCellNumberWithDataType.put(5, "Date");//F PAGO			5
		//mapCellNumberWithDataType.put(6, "BigDecimal");//TASA		6
		mapCellNumberWithDataType.put(4, "BigDecimal");//MONTO		7
		mapCellNumberWithDataType.put(5, "String");//NEMONICO EMISOR 
		mapCellNumberWithDataType.put(6, "String");//MONEDA				
		
		ValidationAccountAnnotationCupon validationAccountAnnotationCupon = null;
		List<ValidationAccountAnnotationCupon> lstValidationAccountAnnotationCupon = new ArrayList<ValidationAccountAnnotationCupon>();
		
		sheet = 1;
		sheetOperation = book.getSheetAt(sheet);
		lastRow = sheetOperation.getLastRowNum();
		lastCell = mapCellNumberWithDataType.size(); 
		
		for (int filaExtrac = rowFirst; filaExtrac <= lastRow; filaExtrac++) {
			
			row = sheetOperation.getRow(filaExtrac);
			if (Validations.validateIsNotNull(row)) {
				if(!isRowEmpty(row)) {
					validationAccountAnnotationCupon = new ValidationAccountAnnotationCupon();
					for(int cellPosition=0; cellPosition<= lastCell; cellPosition++) {
						
						cell = row.getCell(cellPosition);
						String dataType = mapCellNumberWithDataType.get(cellPosition);
						String cellValue = getCellGenericInString(cell, dataType);
	
						if(cellPosition == 0) {
							validationAccountAnnotationCupon.setSerialNumber(cellValue.toUpperCase());
						}else if(cellPosition == 1) {
							validationAccountAnnotationCupon.setCertificateNumber(cellValue);
						}else if(cellPosition == 2) {
							validationAccountAnnotationCupon.setNroCupon(cellValue);
						}else if(cellPosition == 3) {
							validationAccountAnnotationCupon.setExpirationDate(cellValue);
						}else if(cellPosition == 4) {
							validationAccountAnnotationCupon.setCuponAmount(cellValue);
						}else if(cellPosition == 5) {
							validationAccountAnnotationCupon.setMnemonicIssuer(cellValue);
						}else if(cellPosition == 6) {
							validationAccountAnnotationCupon.setSecurityCurrency(cellValue);
						}
						
						validationAccountAnnotationCupon.setBeginingDate(cellValue);
						validationAccountAnnotationCupon.setPaymentDate(cellValue);
						validationAccountAnnotationCupon.setInterestRate(cellValue);
						
					}
					lstValidationAccountAnnotationCupon.add(validationAccountAnnotationCupon);
				}
			}
		}
		
		
		
		List<ValidationAccountAnnotationOperation>  reordenValidationAccountAnnotationOperation = reordenValidationAccountAnnotationOperation(lstAccountAnnotationOperations, lstValidationAccountAnnotationCupon);
		/*List<ValidationAccountAnnotationCupon> lstTmpGenerateValidationsAccount = null;
		for(ValidationAccountAnnotationOperation current : reordenValidationAccountAnnotationOperation) {
			if(current.getValidationAccountAnnotationCupons()!= null && current.getValidationAccountAnnotationCupons().size()>0) {
				lstTmpGenerateValidationsAccount = lstTmpGenerateValidationsAccount( current, current.getValidationAccountAnnotationCupons().size() );
			}
		}*/
		
		
		return reordenValidationAccountAnnotationOperation;
		
	}
	
public List<ValidationAccountAnnotationOperation> setXLSXToEntityAccountAnnotationAcciones(InputStream processFile,Long idParticipantPk) throws IOException, ParseException, Exception {
		
		List<ValidationAccountAnnotationOperation> lstAccountAnnotationOperations = new ArrayList<ValidationAccountAnnotationOperation>();
		ValidationAccountAnnotationOperation validationAccountAnnotationOperation = null;
		
		Integer sheet = 0;
		Map<Integer, String> mapCellNumberWithDataType = new HashedMap<Integer, String>();
		//	mapCellNumberWithDataType.put(0, "String");//NEMONICO DEPOSITANTE	//0
		mapCellNumberWithDataType.put(0, "String");//NUMERO CUENTA				//1
		mapCellNumberWithDataType.put(1, "String");//SERIE						//2
		mapCellNumberWithDataType.put(2, "String");//CERTIFICADO - SUBCLASE		//3

		mapCellNumberWithDataType.put(3, "String");//ID_SECURITY_CODE_PK		//4
		mapCellNumberWithDataType.put(4, "Integer");//Cantidad
		
		mapCellNumberWithDataType.put(5, "Integer");//DESDE					//5
		mapCellNumberWithDataType.put(6, "Integer");//HASTA					//6
		
		//mapCellNumberWithDataType.put(7, "String");//GUARDA						//7
		mapCellNumberWithDataType.put(7, "String");//OBSERVACION				//8
		mapCellNumberWithDataType.put(8, "String");//9 NEMONICO PARTICIPANTE CONTRAPARTE
		mapCellNumberWithDataType.put(9, "BigDecimal");//10 PRECIO (TASA)
		
 		XSSFWorkbook book = new XSSFWorkbook(processFile);
		XSSFSheet sheetOperation = book.getSheetAt(sheet);
		XSSFRow row; XSSFCell cell;
		int rowFirst = 1, lastRow = sheetOperation.getLastRowNum();
		int lastCell = mapCellNumberWithDataType.size();//cellFirst = 0, 
		
		for (int filaExtrac = rowFirst; filaExtrac <= lastRow; filaExtrac++) {
			
			row = sheetOperation.getRow(filaExtrac);
			
			if (Validations.validateIsNotNull(row)) {
				if(!isRowEmpty(row)) {
					validationAccountAnnotationOperation = new ValidationAccountAnnotationOperation();
					for(int cellPosition=0; cellPosition<= lastCell; cellPosition++) {
						
						cell = row.getCell(cellPosition);
						String dataType = mapCellNumberWithDataType.get(cellPosition);
						String cellValue = getCellGenericInString(cell, dataType);

						validationAccountAnnotationOperation.setParticipant(idParticipantPk.toString());	
						
						if(cellPosition == 0) {
							validationAccountAnnotationOperation.setHolderAccountNumber(cellValue);
						}else if(cellPosition == 1) {
							if(cellValue!=null) {
								validationAccountAnnotationOperation.setSerialNumber(cellValue.toUpperCase());
							}
						}else if(cellPosition == 2) {
							if(cellValue!=null) {
								validationAccountAnnotationOperation.setCertificateNumber(cellValue);
							}
						}
						
						else if(cellPosition == 3) {
							validationAccountAnnotationOperation.setIdSecurityCodePk(cellValue);
						}else if(cellPosition == 4) {
							validationAccountAnnotationOperation.setTotalBalance(cellValue);
						}else if(cellPosition == 5) {
							validationAccountAnnotationOperation.setCertificateFrom(cellValue);
						}else if(cellPosition == 6) {
							validationAccountAnnotationOperation.setCertificateTo(cellValue);
						}
						//else if(cellPosition == 7) {
							//validationAccountAnnotationOperation.setMotive("GUARDA EXCLUSIVA");
						//}
						else if(cellPosition == 7) {
							validationAccountAnnotationOperation.setObservation(cellValue);
						}else if(cellPosition == 8) {
							validationAccountAnnotationOperation.setParticipantSeller(cellValue);
						}else if(cellPosition == 9) {
							validationAccountAnnotationOperation.setAmountRate(cellValue);	//precio sucio
						}
					}
					validationAccountAnnotationOperation.setMotive("GUARDA EXCLUSIVA");
					lstAccountAnnotationOperations.add(validationAccountAnnotationOperation);
				}
			}
			
		}
		
		//**************
		/*
		mapCellNumberWithDataType = new HashedMap<Integer, String>();
		mapCellNumberWithDataType.put(0, "String");//SERIE			0
		mapCellNumberWithDataType.put(1, "String");//CERTIFICADO	1
		mapCellNumberWithDataType.put(2, "Integer");//NRO CUPON		2
		//mapCellNumberWithDataType.put(3, "Date");//F INICIO		3
		mapCellNumberWithDataType.put(3, "Date");//F VENCIMIENTO	4
		//mapCellNumberWithDataType.put(5, "Date");//F PAGO			5
		//mapCellNumberWithDataType.put(6, "BigDecimal");//TASA		6
		mapCellNumberWithDataType.put(4, "BigDecimal");//MONTO		7
		mapCellNumberWithDataType.put(5, "String");//NEMONICO EMISOR 
		mapCellNumberWithDataType.put(6, "String");//MONEDA				
		
		ValidationAccountAnnotationCupon validationAccountAnnotationCupon = null;
		List<ValidationAccountAnnotationCupon> lstValidationAccountAnnotationCupon = new ArrayList<ValidationAccountAnnotationCupon>();
		
		sheet = 1;
		sheetOperation = book.getSheetAt(sheet);
		lastRow = sheetOperation.getLastRowNum();
		lastCell = mapCellNumberWithDataType.size(); 
		
		for (int filaExtrac = rowFirst; filaExtrac <= lastRow; filaExtrac++) {
			
			row = sheetOperation.getRow(filaExtrac);
			if (Validations.validateIsNotNull(row)) {
				if(!isRowEmpty(row)) {
					validationAccountAnnotationCupon = new ValidationAccountAnnotationCupon();
					for(int cellPosition=0; cellPosition<= lastCell; cellPosition++) {
						
						cell = row.getCell(cellPosition);
						String dataType = mapCellNumberWithDataType.get(cellPosition);
						String cellValue = getCellGenericInString(cell, dataType);
	
						if(cellPosition == 0) {
							validationAccountAnnotationCupon.setSerialNumber(cellValue.toUpperCase());
						}else if(cellPosition == 1) {
							validationAccountAnnotationCupon.setCertificateNumber(cellValue);
						}else if(cellPosition == 2) {
							validationAccountAnnotationCupon.setNroCupon(cellValue);
						}else if(cellPosition == 3) {
							validationAccountAnnotationCupon.setExpirationDate(cellValue);
						}else if(cellPosition == 4) {
							validationAccountAnnotationCupon.setCuponAmount(cellValue);
						}else if(cellPosition == 5) {
							validationAccountAnnotationCupon.setMnemonicIssuer(cellValue);
						}else if(cellPosition == 6) {
							validationAccountAnnotationCupon.setSecurityCurrency(cellValue);
						}
						
						validationAccountAnnotationCupon.setBeginingDate(cellValue);
						validationAccountAnnotationCupon.setPaymentDate(cellValue);
						validationAccountAnnotationCupon.setInterestRate(cellValue);
						
					}
					lstValidationAccountAnnotationCupon.add(validationAccountAnnotationCupon);
				}
			}
		}
		*/
		
		
		//List<ValidationAccountAnnotationOperation>  reordenValidationAccountAnnotationOperation = reordenValidationAccountAnnotationOperation(lstAccountAnnotationOperations, lstValidationAccountAnnotationCupon);
		/*List<ValidationAccountAnnotationCupon> lstTmpGenerateValidationsAccount = null;
		for(ValidationAccountAnnotationOperation current : reordenValidationAccountAnnotationOperation) {
			if(current.getValidationAccountAnnotationCupons()!= null && current.getValidationAccountAnnotationCupons().size()>0) {
				lstTmpGenerateValidationsAccount = lstTmpGenerateValidationsAccount( current, current.getValidationAccountAnnotationCupons().size() );
			}
		}*/
		
		
		return lstAccountAnnotationOperations;
		
	}
	
	public List<ValidationAccountAnnotationCupon> lstTmpGenerateValidationsAccount(ValidationAccountAnnotationOperation accountAnnotationOperation, Integer lastCuponNumberInExcel) throws Exception{
		accountAnnotationOperation.getSecurityInterestRate();
		accountAnnotationOperation.getSecurityExpirationDate();
		accountAnnotationOperation.getPeriodicity();
		
		/*
		Date fechaEmision = CommonsUtilities.convertStringtoDate(accountAnnotationOperation.getExpeditionDate(), CommonsUtilities.DATE_PATTERN);
		Date fechaVencimiento = CommonsUtilities.convertStringtoDate(accountAnnotationOperation.getSecurityExpirationDate(), CommonsUtilities.DATE_PATTERN);
		
		BigDecimal capital = BigDecimal.valueOf( Long.valueOf(accountAnnotationOperation.getSecurityNominalValue()) );

		//BigDecimal tasaDiaria = capital.divide( BigDecimal.valueOf(365) );
		Integer cantidadTotalDiasEmision = CommonsUtilities.diffDatesInDays(fechaEmision, fechaVencimiento);
		
		Integer cantidadCortesTotal = cantidadTotalDiasEmision/365;
		Integer cantidadCortesPorAnio = CommonsUtilities.cuponesWithFixedPeridiocity(accountAnnotationOperation.getPeriodicity());
		
		Integer cantidadCupones = cantidadCortesTotal * cantidadCortesPorAnio;
		//Integer cantidadCupones = cantidadCortesPorAnio * cantidadDias;
		*/
		Integer cantidadDiasPorCuponPorDefecto = CommonsUtilities.daysDefaultWithFixedPeridiocity(accountAnnotationOperation.getPeriodicity());
		
		List<ValidationAccountAnnotationCupon> lstValidationAccountAnnotationCuponDefault = new ArrayList<ValidationAccountAnnotationCupon>();
		ValidationAccountAnnotationCupon currentCupon = null;
		
		
		if(lastCuponNumberInExcel > cantidadDiasPorCuponPorDefecto) {
			//es porque se a desprendido un cupo o por que no se a registrado
		}
		
		Date previousExpirationDate = null;
		for(int i=1; i<= cantidadDiasPorCuponPorDefecto; i++) {
			currentCupon = generateCuponWithData(lastCuponNumberInExcel, i, previousExpirationDate, accountAnnotationOperation);
			lstValidationAccountAnnotationCuponDefault.add(currentCupon);
		}
		
		return lstValidationAccountAnnotationCuponDefault;
	}
	

	public ValidationAccountAnnotationCupon generateCuponWithData(Integer lastCuponNumberInExcel, 
																  Integer i, 
																  Date previousExpirationDate, 
																  ValidationAccountAnnotationOperation accountAnnotationOperation) throws ServiceException {
		ValidationAccountAnnotationCupon current = new ValidationAccountAnnotationCupon();
		current.setCertificateNumber(i.toString());

		if(previousExpirationDate==null){
			current.setBeginingDate(accountAnnotationOperation.getExpeditionDate() );	
		}else{
			current.setBeginingDate( CommonsUtilities.convertDatetoString(previousExpirationDate) );
		}
		if( i == lastCuponNumberInExcel ){
			current.setExpirationDate(accountAnnotationOperation.getSecurityExpirationDate());
		}else{
			//Expiration Date
			if(current.getCertificateNumber().equals( GeneralConstants.ONE_VALUE_INTEGER.toString() )){
				current.setExpirationDate(accountAnnotationOperation.getExpeditionDate());
			}else{
				/*if(InterestPeriodicityType.BY_DAYS.getCode().equals( accountAnnotationOperation.getPeriodicity() )){
					current.setExpirationDate(  CommonsUtilities.addDaysToDate(current.getBeginingDate(), 
							365 ) );//security.getPeriodicityDays()	
				}else{*/
					
				Date beginigDate = CommonsUtilities.convertStringtoDate(current.getBeginingDate());
				Date expirationDateCalculate = CommonsUtilities.addMonthsToDate(  beginigDate, CommonsUtilities.representationWithPeridiocity(accountAnnotationOperation.getPeriodicity()) );
				current.setExpirationDate( CommonsUtilities.convertDatetoString(expirationDateCalculate) );
				//}
			}
		}
		
		/*if(forceExpirationDate != null) {
			current.setExpirationDate(forceExpirationDate);
		}*/
		
		previousExpirationDate= CommonsUtilities.convertStringtoDate(current.getExpirationDate());
		
		Date paymentDate = generalParameterFacade.workingDateCalculateServiceFacade( CommonsUtilities.convertStringtoDate(current.getExpirationDate()), 
				   GeneralConstants.ONE_VALUE_INTEGER);
		current.setPaymentDate( CommonsUtilities.convertDatetoString(paymentDate) );
		Integer paymentDays=null;
		if(true){//security.isExtendedTerm()
			paymentDays=CommonsUtilities.getDaysBetween( CommonsUtilities.convertStringtoDate(current.getBeginingDate()), CommonsUtilities.convertStringtoDate(current.getExpirationDate()) );
		}/*else{
			paymentDays=CommonsUtilities.getDaysBetween(current.getBeginingDate(),current.getExpirationDate());
		}*/
		current.setInterestRate(accountAnnotationOperation.getSecurityInterestRate());

		
		BigDecimal rate= new BigDecimal(accountAnnotationOperation.getSecurityInterestRate()).divide(BigDecimal.valueOf(100),MathContext.DECIMAL128);
		BigDecimal factor=null;
		factor=rate.divide( new BigDecimal(CalendarDayType._365.getIntegerValue()),MathContext.DECIMAL128 );
		factor=factor.multiply( BigDecimal.valueOf(paymentDays) );
		factor=factor.multiply(GeneralConstants.HUNDRED_VALUE_BIGDECIMAL);
		current.setInterestRate(factor.toString());
		
		BigDecimal cuponAmount = new BigDecimal(accountAnnotationOperation.getSecurityNominalValue()).multiply(new BigDecimal(current.getInterestRate())).divide(BigDecimal.valueOf(100)  , MathContext.DECIMAL128);
		
		current.setCuponAmount(cuponAmount.toString());
		current.setCertificateNumber(accountAnnotationOperation.getCertificateNumber());
		current.setSerialNumber(accountAnnotationOperation.getSerialNumber());
		
		//Date securityExpirationAddOneDay = CommonsUtilities.addDate( CommonsUtilities.convertStringtoDate(accountAnnotationOperation.getSecurityExpirationDate()), 1);
		
		
		return current;
	}
	
	
	 private boolean isRowEmpty(Row row) {
		    if (row == null) {
		        return true;
		    }
		    if (row.getLastCellNum() <= 0) {
		        return true;
		    }
		    for (int cellNum = row.getFirstCellNum(); cellNum < row.getLastCellNum(); cellNum++) {
		        Cell cell = row.getCell(cellNum);
		        if (cell != null && cell.getCellType() != XSSFCell.CELL_TYPE_BLANK && StringUtils.isNotBlank(cell.toString())) {
		            return false;
		        }
		    }
		    return true;
	}
	 
	 public String getCellGenericInString(XSSFCell cell, String dataType) throws ParseException {
			String cellValue = null;
			try {
			
				if(cell!=null) {
					if(dataType.equals("String")) {
						cellValue = getCellString(cell);
					}else if(dataType.equals("Date")) {
						Date cellDate = getCellDate(cell,CommonsUtilities.DATE_PATTERN);
						if(cellDate!=null) {
							SimpleDateFormat simpleDateFormat = new SimpleDateFormat(CommonsUtilities.DATE_PATTERN);
							cellValue = simpleDateFormat.format(cellDate);
						}
					}else if(dataType.equals("Integer")) {
						Integer cellInteger = getCellInteger(cell);
						cellValue = (cellInteger != null)?cellInteger.toString():null;
					}else if(dataType.equals("BigDecimal")) {
						BigDecimal cellBigDecimal = getCellBigDecimal(cell);
						cellValue = (cellBigDecimal != null)?""+cellBigDecimal:null;
					}
				}
				
			}catch (Exception e) {
				System.out.println("CELL: "+cell+" - DATATYPE: "+dataType);
			}
			
			return cellValue;
		}
	 
	 public String getCellString(XSSFCell cell) {
			String returnStringCell = "";
			Object returnValue = null;
			BigDecimal bigDecimal = null;
			switch (cell.getCellType()) {
				case XSSFCell.CELL_TYPE_NUMERIC:
					returnValue = cell.getNumericCellValue();
					bigDecimal = new BigDecimal(returnValue.toString());
					returnValue = bigDecimal.intValue();
					break;
				case XSSFCell.CELL_TYPE_STRING:
					returnValue = cell.getStringCellValue();
					break;
				case XSSFCell.CELL_TYPE_BLANK:
					returnValue = "";
					break;	
				case XSSFCell.CELL_TYPE_FORMULA:
					returnValue = cell.getNumericCellValue();
					bigDecimal = new BigDecimal(returnValue.toString());
					returnValue = bigDecimal.intValue();
					break;
				default:
					returnStringCell = cell.getStringCellValue();
					break;
			}
			
			if(returnValue!=null) {
				returnStringCell = returnValue.toString();
			}
			  
			return returnStringCell;
		}
		
		public Date getCellDate(XSSFCell cell, String dateFormat) throws ParseException {
			dateFormat = (dateFormat ==null)? CommonsUtilities.DATE_PATTERN : dateFormat;// : DATE_PATTERN_EN
			
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
			Object cellObject = null;
			Date date = null;
			
			switch (cell.getCellType()) {
				case XSSFCell.CELL_TYPE_NUMERIC:
					date = cell.getDateCellValue();
					break;
				case XSSFCell.CELL_TYPE_FORMULA:
					date = cell.getDateCellValue();
					break;	
				case XSSFCell.CELL_TYPE_STRING:
					cellObject = cell.getStringCellValue();
					String tmpValue = ""+cellObject;
					if(!tmpValue.matches("[a-zA-Z]+")) {
						date = (cellObject != null) ? simpleDateFormat.parse(tmpValue) : null;
					}
					break;
			}
			
			return date; 
		}
		
		public Integer getCellInteger(XSSFCell cell) {
			Integer returnIntegerCell = null;
			Object returnValue = null;
			switch (cell.getCellType()) {
				case XSSFCell.CELL_TYPE_NUMERIC:
					returnValue = cell.getNumericCellValue();
					BigDecimal bigDecimal = new BigDecimal(returnValue.toString());
					returnValue = bigDecimal.intValue();
					break;
				case XSSFCell.CELL_TYPE_STRING:
					returnValue = cell.getStringCellValue();
					break;
				case XSSFCell.CELL_TYPE_FORMULA:
					returnValue = cell.getNumericCellValue();
					break;
			}
			
			if(returnValue!=null && !returnValue.equals("")) {
				String tmpValue = returnValue.toString();
				if(!tmpValue.matches("[a-zA-Z]+")) {
					BigDecimal currentBD = new BigDecimal(returnValue.toString());
					returnIntegerCell = currentBD.intValue();
				}
			}
			  
			return returnIntegerCell;
		}

		
		public BigDecimal getCellBigDecimal(XSSFCell cell) {
			BigDecimal returnBigDecimalCell = null;
			Object returnValue = null;
			switch (cell.getCellType()) {
				case XSSFCell.CELL_TYPE_NUMERIC:
					returnValue = cell.getNumericCellValue();
					break;
				case XSSFCell.CELL_TYPE_STRING:
					returnValue = cell.getStringCellValue();
					break;
				case XSSFCell.CELL_TYPE_FORMULA:
					returnValue = cell.getNumericCellValue();
					break;
			}
			
			if(returnValue!=null && !returnValue.equals("")) {
				String tmpValue = returnValue.toString();
				if(!tmpValue.matches("[a-zA-Z]+")) {
					returnBigDecimalCell = new BigDecimal(returnValue.toString());
				}
			}
			  
			return returnBigDecimalCell;
		}

		
		public List<ValidationAccountAnnotationOperation> reordenValidationAccountAnnotationOperation(List<ValidationAccountAnnotationOperation> lstAccountAnnotationOperations, 
																											 List<ValidationAccountAnnotationCupon> lstValidationAccountAnnotationCupon){
			
			List<ValidationAccountAnnotationOperation> lstOperationWithCuponReturn = null;
			if(lstAccountAnnotationOperations != null) {
				lstOperationWithCuponReturn = new ArrayList<ValidationAccountAnnotationOperation>();
				 
				for(ValidationAccountAnnotationOperation operation: lstAccountAnnotationOperations) {
					for(ValidationAccountAnnotationCupon cupon: lstValidationAccountAnnotationCupon) {
						if(isCuponInAccountAnnotationOperation(operation, cupon)) {
							if(operation.getValidationAccountAnnotationCupons() == null) {
								operation.setValidationAccountAnnotationCupons(new ArrayList<ValidationAccountAnnotationCupon>());
							}
							operation.getValidationAccountAnnotationCupons().add(cupon);
						}
					}
					lstOperationWithCuponReturn.add(operation);
				}
			}
			
			return lstOperationWithCuponReturn;
			
		}		

		public Boolean isCuponInAccountAnnotationOperation(ValidationAccountAnnotationOperation operation, ValidationAccountAnnotationCupon cupon) {
			if(	   operation.getCertificateNumber()!=null && !operation.getCertificateNumber().equals("")
				&& operation.getSerialNumber()!=null && !operation.getSerialNumber().equals("")
				&& cupon.getCertificateNumber()!=null && !operation.getCertificateNumber().equals("")
				&& cupon.getSerialNumber()!=null && !operation.getSerialNumber().equals("")
				
				&& operation.getMnemonicIssuer()!=null && !operation.getMnemonicIssuer().equals("")
				&& operation.getSecurityCurrency()!=null && !operation.getSecurityCurrency().equals("")
				&& cupon.getMnemonicIssuer()!=null && !cupon.getMnemonicIssuer().equals("")
				&& cupon.getSecurityCurrency()!=null && !cupon.getSecurityCurrency().equals("")
			) {
				if( operation.getCertificateNumber().equalsIgnoreCase(cupon.getCertificateNumber()) &&
					operation.getSerialNumber().equalsIgnoreCase(cupon.getSerialNumber())	 &&
					operation.getMnemonicIssuer().equalsIgnoreCase(cupon.getMnemonicIssuer())	 &&
					operation.getSecurityCurrency().equalsIgnoreCase(cupon.getSecurityCurrency())	
				) {
					return true;
				}
			}
			return false;
		}
}
