package com.pradera.core.component.helperui.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.el.ELContext;
import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.HelperBean;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.IssuerSearcherTO;
import com.pradera.core.component.helperui.to.IssuerTO;
import com.pradera.core.component.helperui.to.SecuritiesSearcherTO;
import com.pradera.core.component.helperui.to.SecuritiesTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class SecuritiesHelperBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 25/01/2013
 */
@HelperBean
public class SecuritiesHelperBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5036009796727734519L;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	/** The Log. */
	@Inject
	private transient PraderaLogger Log;
	
	/** The User Info. */
	@Inject
	private transient UserInfo userInfo;
	
	/** The security description. */
	private String securityDescription;
	
	/** The security mnemonic. */
	private String securityMnemonic;
	
	/** The securities searcher. */
	private SecuritiesSearcherTO securitiesSearcher = new SecuritiesSearcherTO();
	
	/** The selected. */
	private SecuritiesTO selected;
	
	/** The list securities. */
	private List<SecuritiesTO> listSecurities;
	
	/** The searched. */
	private boolean searched;
	
	/** The list states. */
	private List<SelectItem> listStates;
	
	/** The list instrument type. */
	private List<ParameterTable> listInstrumentType;
	
	/** The list class securities. */
	private List<ParameterTable> listClassSecurities;
	
	/** The list securities type. */
	private List<ParameterTable> listSecuritiesType;
	
	/** The list participants. */
	private List<Participant> listParticipants;
	
	/** The list issuers. */
	private List<IssuerTO> listIssuers;
	
	/** The Issuer. */
	private Issuer issuer;
	
	/** The log. */
	@Inject
	private transient PraderaLogger log;
	
	
	private enum SearchType {
		SEARCH_BY_ISIN_CODE,
		SEARCH_BY_HELPER;
	}
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() throws ServiceException {
		Log.info("bean creado");
		issuer = new Issuer();
		loadFilters();
		
		//VALIDATING USER TYPE
		if(userInfo.getUserAccountSession().getInstitutionType().equals(InstitutionType.PARTICIPANT.getCode())){
			securitiesSearcher.setParticipantCode(userInfo.getUserAccountSession().getParticipantCode());
		}
		
	}
	
	private void loadFilters() throws ServiceException {
		listStates = new ArrayList<SelectItem>();
		listInstrumentType = new ArrayList<ParameterTable>();
		listSecuritiesType = new ArrayList<ParameterTable>();
		listStates = new ArrayList<SelectItem>();
		listClassSecurities = new ArrayList<ParameterTable>();
		listParticipants = new ArrayList<Participant>();
		listIssuers = new ArrayList<IssuerTO>();
		ParameterTableTO paramTable = new ParameterTableTO();
		paramTable.setState(ParameterTableStateType.REGISTERED.getCode());

		paramTable.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
		for(ParameterTable param : generalParametersFacade.getListParameterTableServiceBean(paramTable)) {
			listInstrumentType.add(param);
		}

		paramTable.setMasterTableFk(MasterTableType.SECURITIES_STATE.getCode());
		for(ParameterTable param : generalParametersFacade.getListParameterTableServiceBean(paramTable)) {
			listStates.add(new SelectItem(param.getParameterTablePk(), param.getParameterName()));
		}
		//Issuers
		IssuerSearcherTO issuerTO = new IssuerSearcherTO();
//		issuerTO.setState(IssuerStateType.REGISTERED.getCode());
		listIssuers = helperComponentFacade.findIssuerByHelper(issuerTO);
	}
	
	/**
	 * Select securities from helper.
	 */
	public void selectSecuritiesFromHelper() {
		FacesContext context = FacesContext.getCurrentInstance();
		if (selected != null) {
			securityDescription = selected.getDescription();
			securityMnemonic = selected.getMnemonic();
			//set client isin code
			ELContext elContext = context.getELContext();
			ValueExpression valueExpression = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.attrs.value}", String.class);
			//new Value
			valueExpression.setValue(elContext, selected.getSecuritiesCode());

			selected = new SecuritiesTO();
			listSecurities = new ArrayList<SecuritiesTO>();
		} else {
			//no se encontro valor
			Log.error("No encontro el valor");
		}
	}
	
	/**
	 * Search helper securities.
	 *
	 * @param event the event
	 * @throws ServiceException 
	 */
	public void searchSecuritiesByHelper(ActionEvent event) throws ServiceException {
		searched = true;
		securitiesSearcher.setIsinCode(null);
		FacesContext context = FacesContext.getCurrentInstance();
		
		ELContext elContext = context.getELContext();
		
		//GETTING PARTICIPANT CODE
		ValueExpression valueExPartRequired = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.partRequired}", boolean.class);
		boolean partRequired = (Boolean) valueExPartRequired.getValue(elContext);
		
		//GETTING HELPER NAME
		ValueExpression valueExpressionName = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.name}", String.class);
		String helperName = (String)valueExpressionName.getValue(elContext);
		
		//GETTING CLIENT ID
		ValueExpression valueExClientId = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.clientId}", String.class);
		String clientId = (String)valueExClientId.getValue(elContext);
		
		//VALIDATING IF PARTICIPANT CODE IS NOT NULL
		if(partRequired){
			if(Validations.validateIsNotNullAndPositive(securitiesSearcher.getParticipantCode())){
				listSecurities = helperComponentFacade.findSecuritiesFromHelperNativeQuery(securitiesSearcher);
			}else{
				ValueExpression valueExpression = context.getApplication().getExpressionFactory()
					    .createValueExpression(elContext, "#{cc.resourceBundleMap.partNotSelected}", String.class);
				
				JSFUtilities.addContextMessage(":"+clientId+":cboParticipant"+helperName, valueExpression.getValue(elContext).toString());
			}
		}else{
			listSecurities = helperComponentFacade.findSecuritiesFromHelperNativeQuery(securitiesSearcher);
		}
	}
	
	/**
	 * Search securities, using in event onblur of helper.
	 *
	 */
	public void searchSecurityByInput() throws ServiceException {
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		ValueExpression isinCodeExp = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.value}", String.class);
		String code = (String)isinCodeExp.getValue(elContext);
		
		selected = null;
		securitiesSearcher.setIsinCode(null);
		securityDescription = null;
		securityMnemonic = null;
		
		if(hasValidPreContitions(SearchType.SEARCH_BY_ISIN_CODE)){
			 if (StringUtils.isNotBlank(code)) {
				 securitiesSearcher.setIsinCode(code);
				 selected = helperComponentFacade.findSecurityFromIsin(securitiesSearcher);
				 if (selected != null) {
					 securityDescription = selected.getDescription();
					 securityMnemonic = selected.getMnemonic();
					 //new Value
					 isinCodeExp.setValue(elContext, selected.getSecuritiesCode());
				 } else {
					 showMessageValidation("cnfwMsgIsinNotExistsValidation");
					 securityDescription = null;
					 securityMnemonic = null;
					 isinCodeExp.setValue(elContext, null);
				 }
				 return;
			}
			
			//error message
			UIComponent uitextComp = UIComponent.getCurrentComponent(context);
			if(uitextComp instanceof UIInput) {
				UIInput issuerText = (UIInput)uitextComp;
				issuerText.setValid(true);
			}
			isinCodeExp.setValue(elContext, null);
			securityDescription = null;
			securityMnemonic = null;
		}

	}
	
	public void showHelper() throws ServiceException{
		clean();
		if(hasValidPreContitions(SearchType.SEARCH_BY_HELPER)){
			JSFUtilities.executeJavascriptFunction("PF('dlgSecuritiesHelper').show();");
		}
	}
	
	private boolean hasValidPreContitions(SearchType searchByHelper){
		boolean valid = false;
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContextAux = context.getELContext();
		
		ValueExpression valueExPartRequired = context.getApplication().getExpressionFactory().createValueExpression(elContextAux, "#{cc.attrs.partRequired}", boolean.class);
		boolean partRequired = (Boolean) valueExPartRequired.getValue(elContextAux);
		if(partRequired){
			ValueExpression valueExpressionAux = context.getApplication().getExpressionFactory().createValueExpression(elContextAux, "#{cc.attrs.partCode}", Long.class);
			Long partCode = (Long) valueExpressionAux.getValue(elContextAux);
			
			if(Validations.validateIsNotNullAndPositive(partCode)){
				if(searchByHelper.equals(SearchType.SEARCH_BY_HELPER)){
					Participant filter = new Participant();
					filter.setIdParticipantPk(partCode);
					listParticipants  = helperComponentFacade.findParticipantNativeQueryFacade(filter);
					securitiesSearcher.setParticipantCode(partCode);
				}if(searchByHelper.equals(SearchType.SEARCH_BY_ISIN_CODE)){
					securitiesSearcher.setParticipantCode(partCode);
				}
				valid= true;
			}else{
				showMessageValidation("cnfwMsgPartNotSelectedValidation");
			}
		}else{
			valid = true;
		}
		return valid;
	}
	
	
	private void showMessageValidation(String cnfDialog) {
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
			
		// show validation by helper name
		ValueExpression valueExpressionName = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.name}", String.class);
		String helperName = (String)valueExpressionName.getValue(elContext);
		
		RequestContext.getCurrentInstance().execute("PF('"+cnfDialog+helperName+"').show();");
		
	}

	/**
	 * Clean.
	 *
	 * @param event the event
	 * @throws ServiceException 
	 */
	public void clean() {
		//loadFilters();
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		securitiesSearcher.setIsinCode(null);
		securitiesSearcher.setDescription(null);
		securitiesSearcher.setSecuritiesType(null);
		securitiesSearcher.setClassSecuritie(null);
		securityMnemonic = "";
		securityDescription = "";
		ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.disableInstrument}", Boolean.class);
		boolean blDisabledInstr = false;
		if(valueExpression.getValue(elContext)!=null) {
			blDisabledInstr = (Boolean) valueExpression.getValue(elContext);
		}
			
		if(!blDisabledInstr){
			securitiesSearcher.setInstrumentType(null);
			listSecuritiesType.clear();
			listClassSecurities.clear();
		}
		
		valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.disableState}", Boolean.class);
		boolean blDisabledState = false;
		if(valueExpression.getValue(elContext)!=null) {
			blDisabledState = (Boolean) valueExpression.getValue(elContext);
		}
		if(!blDisabledState){
			securitiesSearcher.setStateSecuritie(null);
		}
		
		valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.disableIssuer}", Boolean.class);
		boolean blDisabledIssuer = (Boolean) valueExpression.getValue(elContext);
		if(!blDisabledIssuer){
			securitiesSearcher.setIssuerCode(null);
		}
		
		valueExpression = null;
		
		selected = null;
		listSecurities = null;
	}
	
	/**
	 * Exit.
	 *
	 * @param event the event
	 */
	public void exit() {
		clean();
	}
	
	public void loadCboSecuritieTypeByInstrumentHandler(){
		try {
			if(Validations.validateIsNotNull(listClassSecurities)){
				listClassSecurities.clear();
			}
			if(Validations.validateIsNotNull(listSecuritiesType)){
				listSecuritiesType.clear();
			}
			if(Validations.validateIsNotNull(securitiesSearcher.getInstrumentType())){
				SecuritiesSearcherTO secSearcherTO = new SecuritiesSearcherTO();
				secSearcherTO.setInstrumentType(securitiesSearcher.getInstrumentType());
				listSecuritiesType = helperComponentFacade.searchSecurityTypesByInstrument(secSearcherTO);
			}else{
				listSecuritiesType.clear();
			}
		} catch (Exception e) {
			log.error(e.toString());
		}
	}
	
	public void loadCboSecuritieClassBySecuritieTypeHandler(){
		try {
			if(Validations.validateIsNotNull(listClassSecurities)){
				listClassSecurities.clear();
			}
			if(Validations.validateIsNotNull(securitiesSearcher.getSecuritiesType())){
				SecuritiesSearcherTO secSearcherTO = new SecuritiesSearcherTO();
				secSearcherTO.setInstrumentType(securitiesSearcher.getInstrumentType());
				secSearcherTO.setSecuritiesType(securitiesSearcher.getSecuritiesType());
				//secSearcherTO.setClassSecuritie(securitiesSearcher.getClassSecuritie());
				listClassSecurities =  helperComponentFacade.searchSecurityClassBySecurityType(secSearcherTO);
			}	
		} catch (Exception e) {
			log.error(e.toString());
		}
	}
	
	/**
	 * Gets the security description.
	 *
	 * @return the security description
	 */
	public String getSecurityDescription() {
		return securityDescription;
	}

	/**
	 * Sets the security description.
	 *
	 * @param securityDescription the new security description
	 */
	public void setSecurityDescription(String securityDescription) {
		this.securityDescription = securityDescription;
	}

	/**
	 * Gets the security mnemonic.
	 *
	 * @return the security mnemonic
	 */
	public String getSecurityMnemonic() {
		return securityMnemonic;
	}

	/**
	 * Sets the security mnemonic.
	 *
	 * @param securityMnemonic the new security mnemonic
	 */
	public void setSecurityMnemonic(String securityMnemonic) {
		this.securityMnemonic = securityMnemonic;
	}

	/**
	 * Gets the selected.
	 *
	 * @return the selected
	 */
	public SecuritiesTO getSelected() {
		return selected;
	}

	/**
	 * Sets the selected.
	 *
	 * @param selected the new selected
	 */
	public void setSelected(SecuritiesTO selected) {
		this.selected = selected;
	}

	/**
	 * Gets the securities searcher.
	 *
	 * @return the securities searcher
	 */
	public SecuritiesSearcherTO getSecuritiesSearcher() {
		return securitiesSearcher;
	}

	/**
	 * Sets the securities searcher.
	 *
	 * @param securitiesSearcher the new securities searcher
	 */
	public void setSecuritiesSearcher(SecuritiesSearcherTO securitiesSearcher) {
		this.securitiesSearcher = securitiesSearcher;
	}

	/**
	 * Gets the list securities.
	 *
	 * @return the list securities
	 */
	public List<SecuritiesTO> getListSecurities() {
		return listSecurities;
	}

	/**
	 * Sets the list securities.
	 *
	 * @param listSecurities the new list securities
	 */
	public void setListSecurities(List<SecuritiesTO> listSecurities) {
		this.listSecurities = listSecurities;
	}

	/**
	 * Checks if is searched.
	 *
	 * @return true, if is searched
	 */
	public boolean isSearched() {
		return searched;
	}

	/**
	 * Sets the searched.
	 *
	 * @param searched the new searched
	 */
	public void setSearched(boolean searched) {
		this.searched = searched;
	}

	/**
	 * Gets the list states.
	 *
	 * @return the list states
	 */
	public List<SelectItem> getListStates() {
		return listStates;
	}

	/**
	 * Sets the list states.
	 *
	 * @param listStates the new list states
	 */
	public void setListStates(List<SelectItem> listStates) {
		this.listStates = listStates;
	}

	public List<ParameterTable> getListInstrumentType() {
		return listInstrumentType;
	}

	public void setListInstrumentType(List<ParameterTable> listInstrumentType) {
		this.listInstrumentType = listInstrumentType;
	}

	public List<ParameterTable> getListClassSecurities() {
		return listClassSecurities;
	}

	public void setListClassSecurities(List<ParameterTable> listClassSecurities) {
		this.listClassSecurities = listClassSecurities;
	}

	public List<ParameterTable> getListSecuritiesType() {
		return listSecuritiesType;
	}

	public void setListSecuritiesType(List<ParameterTable> listSecuritiesType) {
		this.listSecuritiesType = listSecuritiesType;
	}

	public Issuer getIssuer() {
		return issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}


	/**
	 * @return the listIssuers
	 */
	public List<IssuerTO> getListIssuers() {
		return listIssuers;
	}

	/**
	 * @param listIssuers the listIssuers to set
	 */
	public void setListIssuers(List<IssuerTO> listIssuers) {
		this.listIssuers = listIssuers;
	}

	/**
	 * @return the listParticipants
	 */
	public List<Participant> getListParticipants() {
		return listParticipants;
	}

	/**
	 * @param listParticipants the listParticipants to set
	 */
	public void setListParticipants(List<Participant> listParticipants) {
		this.listParticipants = listParticipants;
	}
	
	
}
