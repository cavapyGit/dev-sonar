package com.pradera.core.component.billing.facade;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.billing.service.BillingComponentService;
import com.pradera.core.component.billing.to.BillingServiceTo;
import com.pradera.core.component.billing.to.ServiceRateTo;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.billing.BillingService;


@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class BillingComponentFacade {
	@Resource
	TransactionSynchronizationRegistry transactionRegistry;
	@EJB
	private BillingComponentService billingComponentService;
	
	public Map   findBillingService(Map parameters){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAdd());
		Date calculationDate = null;
		
		BillingServiceTo billingServiceTo = new BillingServiceTo();
		String baseCollectionId	=	(String) parameters.get(GeneralConstants.BASE_COLLECTION);		
		billingServiceTo.setBaseCollection(Integer.parseInt(baseCollectionId));
		
		if(Validations.validateIsNotNull(parameters.get(GeneralConstants.CALCULATION_DATE))){
			calculationDate	= CommonsUtilities.convertStringtoDate(parameters.get(GeneralConstants.CALCULATION_DATE).toString());
		}
		
		ServiceRateTo serviceRateTo = new ServiceRateTo();
		serviceRateTo.setLastEffectiveDate(calculationDate);
		billingServiceTo.setServiceRateToSelected(serviceRateTo);
		
		List<BillingService> billingServiceList= billingComponentService.findBillingService(billingServiceTo);
		
		
		Map parameter	= new HashMap<String,Object>();
		parameter.put(GeneralConstants.BILLING_SERVICE_LIST, billingServiceList);
				
		return parameter;
		
	}
}
