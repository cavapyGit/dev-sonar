/*
 * 
 */
package com.pradera.core.component.business.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.pradera.integration.component.business.to.MarketFactAccountTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SecurityOverdueTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 16-sep-2015
 */
public class SecurityOverdueTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The key holder account balance. */
	private String keyHolderAccountBalance;
	
	/** The id issuer code pk. */
	private String idIssuerCodePk;
	
	/** The mnemonic issuer. */
	private String mnemonicIssuer;
	
	/** The id security code pk. */
	private String idSecurityCodePk;
	
	/** The description security. */
	private String descriptionSecurity;
	
	/** The description participant. */
	private String descriptionParticipant;

	/** The description participant. */
	private String mnemonicParticipant;
	
	/** The alternate code. */
	private String alternateCode;
	
	/** The description holder. */
	private String descriptionHolder;
	
	/** The total balance. */
	private Integer totalBalance;
	
	/** The available blance. */
	private Integer availableBalance;
	
	/** The Nominal Value*/
	private BigDecimal nominalValue;
	
	/** The pawn balance. */
	private Integer pawnBalance;

	/** The ban balance. */
	private Integer banBalance;
	/** The block balance. */
	private Integer blockBalance;
	
	/** The block balance. */
	private Long idHolderPk;
	
	/** The block balance. */
	private Long idHolderAccountPk;
	
	/** The Primary key Participant. */
	private Long idParticipantPK;
	
	private List<MarketFactAccountTO> listMarketFactAccountTO;
	
	/**
	 * Instantiates a new security overdue to.
	 */
	public SecurityOverdueTO() {
	}
	
	/**
	 * Gets the key holder account balance.
	 *
	 * @return the keyHolderAccountBalance
	 */
	public String getKeyHolderAccountBalance() {
		return keyHolderAccountBalance;
	}
	
	/**
	 * Sets the key holder account balance.
	 *
	 * @param keyHolderAccountBalance the keyHolderAccountBalance to set
	 */
	public void setKeyHolderAccountBalance(String keyHolderAccountBalance) {
		this.keyHolderAccountBalance = keyHolderAccountBalance;
	}
	
	/**
	 * Gets the id issuer code pk.
	 *
	 * @return the idIssuerCodePk
	 */
	public String getIdIssuerCodePk() {
		return idIssuerCodePk;
	}
	
	/**
	 * Sets the id issuer code pk.
	 *
	 * @param idIssuerCodePk the idIssuerCodePk to set
	 */
	public void setIdIssuerCodePk(String idIssuerCodePk) {
		this.idIssuerCodePk = idIssuerCodePk;
	}
	
	/**
	 * Gets the mnemonic issuer.
	 *
	 * @return the mnemonicIssuer
	 */
	public String getMnemonicIssuer() {
		return mnemonicIssuer;
	}
	
	/**
	 * Sets the mnemonic issuer.
	 *
	 * @param mnemonicIssuer the mnemonicIssuer to set
	 */
	public void setMnemonicIssuer(String mnemonicIssuer) {
		this.mnemonicIssuer = mnemonicIssuer;
	}
	
	/**
	 * Gets the id security code pk.
	 *
	 * @return the idSecurityCodePk
	 */
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}
	
	/**
	 * Sets the id security code pk.
	 *
	 * @param idSecurityCodePk the idSecurityCodePk to set
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
	
	/**
	 * Gets the description security.
	 *
	 * @return the descriptionSecurity
	 */
	public String getDescriptionSecurity() {
		return descriptionSecurity;
	}
	
	/**
	 * Sets the description security.
	 *
	 * @param descriptionSecurity the descriptionSecurity to set
	 */
	public void setDescriptionSecurity(String descriptionSecurity) {
		this.descriptionSecurity = descriptionSecurity;
	}
	
	/**
	 * Gets the description participant.
	 *
	 * @return the descriptionParticipant
	 */
	public String getDescriptionParticipant() {
		return descriptionParticipant;
	}
	
	/**
	 * Sets the description participant.
	 *
	 * @param descriptionParticipant the descriptionParticipant to set
	 */
	public void setDescriptionParticipant(String descriptionParticipant) {
		this.descriptionParticipant = descriptionParticipant;
	}
	
	/**
	 * Gets the alternate code.
	 *
	 * @return the alternateCode
	 */
	public String getAlternateCode() {
		return alternateCode;
	}
	
	/**
	 * Sets the alternate code.
	 *
	 * @param alternateCode the alternateCode to set
	 */
	public void setAlternateCode(String alternateCode) {
		this.alternateCode = alternateCode;
	}
	

	
	/**
	 * @return the descriptionHolder
	 */
	public String getDescriptionHolder() {
		return descriptionHolder;
	}

	/**
	 * @param descriptionHolder the descriptionHolder to set
	 */
	public void setDescriptionHolder(String descriptionHolder) {
		this.descriptionHolder = descriptionHolder;
	}

	/**
	 * Gets the total balance.
	 *
	 * @return the totalBalance
	 */
	public Integer getTotalBalance() {
		return totalBalance;
	}
	
	/**
	 * Sets the total balance.
	 *
	 * @param totalBalance the totalBalance to set
	 */
	public void setTotalBalance(Integer totalBalance) {
		this.totalBalance = totalBalance;
	}
	
	
	/**
	 * @return the availableBalance
	 */
	public Integer getAvailableBalance() {
		return availableBalance;
	}

	/**
	 * @param availableBalance the availableBalance to set
	 */
	public void setAvailableBalance(Integer availableBalance) {
		this.availableBalance = availableBalance;
	}

	/**
	 * Gets the block balance.
	 *
	 * @return the blockBalance
	 */
	public Integer getBlockBalance() {
		return blockBalance;
	}
	
	/**
	 * Sets the block balance.
	 *
	 * @param blockBalance the blockBalance to set
	 */
	public void setBlockBalance(Integer blockBalance) {
		this.blockBalance = blockBalance;
	}

	/**
	 * @return the idHolderPk
	 */
	public Long getIdHolderPk() {
		return idHolderPk;
	}

	/**
	 * @param idHolderPk the idHolderPk to set
	 */
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}

	/**
	 * @return the idHolderAccountPk
	 */
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}

	/**
	 * @param idHolderAccountPk the idHolderAccountPk to set
	 */
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}

	/**
	 * @return the idParticipantPK
	 */
	public Long getIdParticipantPK() {
		return idParticipantPK;
	}

	/**
	 * @param idParticipantPK the idParticipantPK to set
	 */
	public void setIdParticipantPK(Long idParticipantPK) {
		this.idParticipantPK = idParticipantPK;
	}

	/**
	 * @return the listMarketFactAccountTO
	 */
	public List<MarketFactAccountTO> getListMarketFactAccountTO() {
		return listMarketFactAccountTO;
	}

	/**
	 * @param listMarketFactAccountTO the listMarketFactAccountTO to set
	 */
	public void setListMarketFactAccountTO(
			List<MarketFactAccountTO> listMarketFactAccountTO) {
		this.listMarketFactAccountTO = listMarketFactAccountTO;
	}

	/**
	 * @return the nominalValue
	 */
	public BigDecimal getNominalValue() {
		return nominalValue;
	}

	/**
	 * @param nominalValue the nominalValue to set
	 */
	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}

	/**
	 * @return the pawnBalance
	 */
	public Integer getPawnBalance() {
		return pawnBalance;
	}

	/**
	 * @param pawnBalance the pawnBalance to set
	 */
	public void setPawnBalance(Integer pawnBalance) {
		this.pawnBalance = pawnBalance;
	}

	/**
	 * @return the banBalance
	 */
	public Integer getBanBalance() {
		return banBalance;
	}

	/**
	 * @param banBalance the banBalance to set
	 */
	public void setBanBalance(Integer banBalance) {
		this.banBalance = banBalance;
	}

	/**
	 * @return the mnemonicParticipant
	 */
	public String getMnemonicParticipant() {
		return mnemonicParticipant;
	}

	/**
	 * @param mnemonicParticipant the mnemonicParticipant to set
	 */
	public void setMnemonicParticipant(String mnemonicParticipant) {
		this.mnemonicParticipant = mnemonicParticipant;
	}

	
	
}
