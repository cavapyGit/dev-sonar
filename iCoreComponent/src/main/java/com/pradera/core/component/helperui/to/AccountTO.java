package com.pradera.core.component.helperui.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.model.accounts.holderaccounts.HolderAccount;

public class AccountTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3670021751652354870L;
	
	private Integer accountNumber;
	
	private String alternativeCode;
	
	private Integer accountState;

	private String accountStateDesc;
	
	private List<HolderTO> holders;
	
	private HolderAccount holderAccount;
	
	private Integer accountGroup;
	
	//Describes All Associated Holders from Holder Account
	private String accountDescription;
	
	public AccountTO() {
		// TODO Auto-generated constructor stub
	}



	public Integer getAccountNumber() {
		return accountNumber;
	}



	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}



	public String getAlternativeCode() {
		return alternativeCode;
	}



	public void setAlternativeCode(String alternativeCode) {
		this.alternativeCode = alternativeCode;
	}



	public Integer getAccountState() {
		return accountState;
	}



	public void setAccountState(Integer accountState) {
		this.accountState = accountState;
	}



	public String getAccountStateDesc() {
		return accountStateDesc;
	}



	public void setAccountStateDesc(String accountStateDesc) {
		this.accountStateDesc = accountStateDesc;
	}



	public List<HolderTO> getHolders() {
		return holders;
	}



	public void setHolders(List<HolderTO> holders) {
		this.holders = holders;
	}



	public String getAccountDescription() {
		return accountDescription;
	}



	public void setAccountDescription(String accountDescription) {
		this.accountDescription = accountDescription;
	}



	public HolderAccount getHolderAccount() {
		return holderAccount;
	}



	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}



	public Integer getAccountGroup() {
		return accountGroup;
	}



	public void setAccountGroup(Integer accountGroup) {
		this.accountGroup = accountGroup;
	}
	
	
	
}
