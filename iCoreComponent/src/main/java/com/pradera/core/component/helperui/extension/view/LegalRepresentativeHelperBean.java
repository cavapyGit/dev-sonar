package com.pradera.core.component.helperui.extension.view;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.el.ELContext;
import javax.el.ValueExpression;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.RegistryType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.utils.view.GeneralPropertiesConstants;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.to.LegalRepresentativeTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.HelperBean;
import com.pradera.core.component.utils.DocumentTO;
import com.pradera.core.component.utils.DocumentValidator;
import com.pradera.core.component.utils.PersonTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.HolderHistory;
import com.pradera.model.accounts.HolderRequest;
import com.pradera.model.accounts.InstitutionInformation;
import com.pradera.model.accounts.LegalRepresentative;
import com.pradera.model.accounts.LegalRepresentativeFile;
import com.pradera.model.accounts.LegalRepresentativeHistory;
import com.pradera.model.accounts.PepPerson;
import com.pradera.model.accounts.RepresentativeFileHistory;
import com.pradera.model.accounts.RepresentedEntity;
import com.pradera.model.accounts.type.AgeLessFatherDocRepreNatType;
import com.pradera.model.accounts.type.AgeLessTutorDocRepreNatType;
import com.pradera.model.accounts.type.AgeMajorDocRepreNatType;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.HolderFileStateType;
import com.pradera.model.accounts.type.HolderReqFileHisType;
import com.pradera.model.accounts.type.HolderRequestStateType;
import com.pradera.model.accounts.type.HolderRequestType;
import com.pradera.model.accounts.type.JuridicClassType;
import com.pradera.model.accounts.type.JuridicDocRepreNatType;
import com.pradera.model.accounts.type.NationalityType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.accounts.type.RepresentativeClassType;
import com.pradera.model.accounts.type.RepresentativeFileType;
import com.pradera.model.accounts.type.StatalEntityDocRepreNatType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.GeographicLocationStateType;
import com.pradera.model.generalparameter.type.GeographicLocationType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class LegalRepresentativeHelperBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01-sep-2015
 */
@HelperBean
public class LegalRepresentativeHelperBean extends GenericBaseBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1700161330330682221L;
	
	/** The list representative class. */
	private List<ParameterTable> listRepresentativeClass;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
	/** The list document type legal. */
	private List<ParameterTable> listDocumentTypeLegal;
	
	/** The list economic sector. */
	private List<ParameterTable> listEconomicSector;
	
	/** The legal representative history. */
	private LegalRepresentativeHistory legalRepresentativeHistory;
	
	/** The legal representative history detail. */
	private LegalRepresentativeHistory legalRepresentativeHistoryDetail;
	
	/** The legal representative to. */
	private LegalRepresentativeTO legalRepresentativeTO;
    
    /** The lst legal representative history. */
    private List<LegalRepresentativeHistory> lstLegalRepresentativeHistory;
    
    /** The lst legal representative. */
    private List<LegalRepresentative> lstLegalRepresentative;
    
    /** The lst representative file history. */
    private List<RepresentativeFileHistory> lstRepresentativeFileHistory;
	
	/** The list representative document type. */
	private List<ParameterTable> listRepresentativeDocumentType;
	
	/** The document validator. */
	@Inject
	DocumentValidator documentValidator;
	
	/** The country residence. */
	@Inject @Configurable Integer countryResidence;
	/** The department issuance. */
	@Inject @Configurable Integer departmentIssuance;
	
	/** The list geographic location. */
	private List<GeographicLocation> listGeographicLocation;
	
	/** The list person type. */
	private List<ParameterTable> listPersonType;
	
	/** The list boolean. */
	private List<BooleanType> listBoolean;
	
	/** The list sex. */
	private List<ParameterTable> listSex;
	
	/** The list role pep. */
	private List<ParameterTable> listRolePep;
	
	/** The list category pep. */
	private List<ParameterTable> listCategoryPep;
	
	/** The list economic activity legal representative. */
	private List<ParameterTable> listEconomicActivityLegalRepresentative;
	
	/** The list municipality postal location representative. */
	private List<GeographicLocation> listMunicipalityPostalLocationRepresentative;
	
	/** The list province postal location representative. */
	private List<GeographicLocation> listProvincePostalLocationRepresentative;
	
	/** The list department postal location representative. */
	private List<GeographicLocation> listDepartmentPostalLocationRepresentative;
	
	/** The list province location representative. */
	private List<GeographicLocation> listProvinceLocationRepresentative;
	
	/** The list municipality location representative. */
	private List<GeographicLocation> listMunicipalityLocationRepresentative;
	
	/** The list department location representative. */
	private List<GeographicLocation> listDepartmentLocationRepresentative;
	
	/** The list document type legal result. */
	private List<ParameterTable> listDocumentTypeLegalResult;
	
	/** The list second document type legal. */
	private List<ParameterTable> listSecondDocumentTypeLegal;
	
	/** The lst legal representative file. */
	private List<LegalRepresentativeFile> lstLegalRepresentativeFile;
	
	/** The list document source. */
	private List<ParameterTable> listDocumentSource;
    
    /** The user info. */
    @Inject
	private UserInfo userInfo;
    
    /** The institution result find. */
    private InstitutionInformation institutionResultFind;
	
	/** The pep person. */
	private PepPerson pepPerson;
    
    /** The id legal representative component. */
    private String idLegalRepresentativeComponent;
    
    /** The representative file name display. */
    private String representativeFileNameDisplay;
    
    /** The disabled holder history. */
    private boolean disabledHolderHistory;
	
	/** The disabled holder request. */
	private boolean disabledHolderRequest;
	
	/** The indicator issuer. */
	private boolean indicatorIssuer;
	
	/** The indicator holder. */
	private boolean indicatorHolder;
	
	/** The represented entity. */
	private RepresentedEntity representedEntity;
	
	/** The legal representative. */
	private LegalRepresentative legalRepresentative;
	
	/** The holder. */
	private Holder holder;
	
	/** The pep person legal. */
	private PepPerson pepPersonLegal;
	
	/** The streamed content file. */
	private transient StreamedContent streamedContentFile;
    
    /** The flag modify legal representantive. */
    private boolean flagModifyLegalRepresentantive;
    
    /** The lst legal representative history detail. */
    private List<LegalRepresentativeHistory>lstLegalRepresentativeHistoryDetail;
    
    /** The lst legal representative history detail before. */
    private List<LegalRepresentativeHistory> lstLegalRepresentativeHistoryDetailBefore;
    
    /** The list geographic location without national country. */
    private List<GeographicLocation> listGeographicLocationWithoutNationalCountry;
    
    /** The document to. */
    private DocumentTO documentTO;
    
    /** The list document issuance date. */
    private LinkedHashMap<String,Integer>listDocumentIssuanceDate;
    
    private Integer IdHolderRequestCreation; 
    /** The new document number. */
	private String newDocumentNumber;
	/** The new document number. */
	private String newDocumentNumberSecondNationality;

	/**
	 * Inits the.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException {
		legalRepresentativeTO = new LegalRepresentativeTO();
		lstLegalRepresentativeHistory = new ArrayList<LegalRepresentativeHistory>();
		legalRepresentativeHistoryDetail = new LegalRepresentativeHistory();	
		legalRepresentativeHistory = new LegalRepresentativeHistory();
		disabledHolderHistory=false;
		disabledHolderRequest=false;
		documentTO = new DocumentTO();
		IdHolderRequestCreation = HolderRequestType.CREATION.getCode();
		newDocumentNumberSecondNationality = null;
		newDocumentNumber = null;
	}
	
	/**
	 * Validate pep date.
	 */
	public void validatePepDate(){
		if( legalRepresentativeHistory.getPersonType() != null && legalRepresentativeHistory.getBirthDate() != null) {
			if ((legalRepresentativeHistory.getPersonType().equals(PersonType.NATURAL.getCode())) &&
					(legalRepresentativeHistory.getBeginningPeriod().getTime() < legalRepresentativeHistory.getBirthDate().getTime())) {
	
				JSFUtilities
				.addContextMessage(
						idLegalRepresentativeComponent+":tabView:calInitial",
						FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities
								.getValidationMessage(GeneralPropertiesConstants.ERROR_PEP_DATE_TO_AGE_REPRESENTATIVE,null),
						PropertiesUtilities
								.getValidationMessage(GeneralPropertiesConstants.ERROR_PEP_DATE_TO_AGE_REPRESENTATIVE,null));
	
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), PropertiesUtilities.getValidationMessage(
				GeneralPropertiesConstants.ERROR_PEP_DATE_TO_AGE_REPRESENTATIVE,null));
	
		
				JSFUtilities.showSimpleValidationDialog();
				legalRepresentativeHistory.setBeginningPeriod(null);
			}
		}
	}
	
	/**
	 * Representative remove.
	 *
	 * @param event the event
	 */
	public void representativeRemove(ActionEvent event) {

		LegalRepresentativeHistory legalRepresentativeHistory = (LegalRepresentativeHistory) event
				.getComponent().getAttributes().get("representativeObj");

		lstLegalRepresentativeHistory.remove(legalRepresentativeHistory);
	}
	
	/**
	 * Value change cmb document representative.
	 */
	public void valueChangeCmbDocumentRepresentative(){
		representativeFileNameDisplay = null;
	}
	
	/**
	 * Initialize lst legal representative history detail before.
	 */
	public void initializeLstLegalRepresentativeHistoryDetailBefore(){
		lstLegalRepresentativeHistoryDetailBefore = new ArrayList<LegalRepresentativeHistory>();
	}
	
	/**
	 * Initialize lst legal representative history detail.
	 */
	public void initializeLstLegalRepresentativeHistoryDetail(){
		lstLegalRepresentativeHistoryDetail = new ArrayList<LegalRepresentativeHistory>();
	}
	
	/**
	 * Initialize lst legal representative history.
	 */
	public void initializeLstLegalRepresentativeHistory(){
		lstLegalRepresentativeHistory = new ArrayList<LegalRepresentativeHistory>();
	}
	
	/**
	 * Initialize lst legal representative.
	 */
	public void initializeLstLegalRepresentative(){
		lstLegalRepresentative = new ArrayList<LegalRepresentative>();
	}
	
	/**
	 * Gets the streamed content file representative history.
	 *
	 * @param file the file
	 * @return the streamed content file representative history
	 */
	public StreamedContent getStreamedContentFileRepresentativeHistory(RepresentativeFileHistory file) {

		try {
			
			   try{		
				   InputStream inputStream;	
				   inputStream = new ByteArrayInputStream(file.getFileRepresentative());
				   streamedContentFile = new DefaultStreamedContent(inputStream, null,	file.getFilename());
				   inputStream.close();
			     }
			     catch(Exception e){
			    	    if(!file.isFlagByLegalRepresentative()){
			    	    	file = accountsFacade.getContentFileRepresentativeHistoryServiceFacade(file.getIdRepresentFileHisPk());
			    	    	InputStream inputStream = new ByteArrayInputStream(file.getFileRepresentative());
			    	    	streamedContentFile = new DefaultStreamedContent(inputStream, null,	file.getFilename());
			    	    	inputStream.close(); 
			    	    }
			    	    else{
			    	    	streamedContentFile = getStreamedContentFileLegalRepresentative(file.getIdRepresentFileHisPk());
			    	    }
			     }
			
		}
		catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

		return streamedContentFile;
	}
	
	/**
	 * Load departament legal representative.
	 */
	public void loadDepartamentLegalRepresentative(){
		if (legalRepresentativeHistory.getLegalResidenceCountry()!=null && !(legalRepresentativeHistory.getLegalResidenceCountry()
				.equals(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER)))) {

			if (legalRepresentativeHistory.getLegalResidenceCountry().equals(countryResidence)) {
				legalRepresentativeTO.setDisabledCountryResidentRepresentative(true);
				legalRepresentativeHistory.setIndResidence(BooleanType.YES.getCode());
			} else {
				legalRepresentativeTO.setDisabledCountryResidentRepresentative(false);
				legalRepresentativeHistory.setIndResidence(BooleanType.NO.getCode());
			}

			listDepartmentLocationRepresentative = getLstDepartmentLocation(legalRepresentativeHistory.getLegalResidenceCountry());
			legalRepresentativeHistory.setLegalProvince(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER));
			listProvinceLocationRepresentative = null;
			legalRepresentativeHistory.setLegalDistrict(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER));
			listMunicipalityLocationRepresentative = null;
		} else {
			legalRepresentativeHistory.setLegalDepartment(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER));
			listDepartmentLocationRepresentative = null;
			legalRepresentativeHistory.setLegalProvince(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER));
			listProvinceLocationRepresentative = null;
			legalRepresentativeHistory.setLegalDistrict(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER));
			listMunicipalityLocationRepresentative = null;
		}
	}
	
	/**
	 * Load municipality legal representative.
	 */
	public void loadMunicipalityLegalRepresentative() {
		
		if (legalRepresentativeHistory.getLegalProvince()!=null && !(legalRepresentativeHistory.getLegalProvince().equals(Integer
				.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER)))) {
			listMunicipalityLocationRepresentative = getLstMunicipalityLocation(legalRepresentativeHistory
					.getLegalProvince());
		} else {
			legalRepresentativeHistory.setLegalDistrict(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER));
			listMunicipalityLocationRepresentative = null;
		}
	}
	
	/**
	 * Load province legal representative.
	 */
	public void loadProvinceLegalRepresentative() {
		
		if (legalRepresentativeHistory.getLegalDepartment()!=null && !(legalRepresentativeHistory.getLegalDepartment()
				.equals(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER)))) {

			listProvinceLocationRepresentative = getLstProvinceLocation(legalRepresentativeHistory.getLegalDepartment());
			legalRepresentativeHistory.setLegalDistrict(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER));
			listMunicipalityLocationRepresentative = null;
		} else {
			
			legalRepresentativeHistory.setLegalProvince(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER));
			listProvinceLocationRepresentative = null;
			legalRepresentativeHistory.setLegalDistrict(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER));
			listMunicipalityLocationRepresentative = null;
		}

	}
	
	/**
	 * Gets the lst department location.
	 *
	 * @param referenceFk the reference fk
	 * @return the lst department location
	 */
	public List<GeographicLocation> getLstDepartmentLocation(Integer referenceFk) {

		List<GeographicLocation> lst = null;

		try {

			GeographicLocationTO geographicLocationTO = new GeographicLocationTO();

			geographicLocationTO
					.setGeographicLocationType(GeographicLocationType.DEPARTMENT
							.getCode());
			geographicLocationTO
					.setState(GeographicLocationStateType.REGISTERED.getCode());
			geographicLocationTO.setIdLocationReferenceFk(referenceFk);

			lst = generalParametersFacade
					.getListGeographicLocationServiceFacade(geographicLocationTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

		return lst;

	}
	
	/**
	 * Gets the lst province location.
	 *
	 * @param referenceFk the reference fk
	 * @return the lst province location
	 */
	public List<GeographicLocation> getLstProvinceLocation(Integer referenceFk) {

		List<GeographicLocation> lst = null;

		try {

			GeographicLocationTO geographicLocationTO = new GeographicLocationTO();

			geographicLocationTO
					.setGeographicLocationType(GeographicLocationType.PROVINCE
							.getCode());
			geographicLocationTO
					.setState(GeographicLocationStateType.REGISTERED.getCode());
			geographicLocationTO.setIdLocationReferenceFk(referenceFk);

			lst = generalParametersFacade
					.getListGeographicLocationServiceFacade(geographicLocationTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

		return lst;

	}
	
	/**
	 * Load province postal representative.
	 */
	public void loadProvincePostalRepresentative() {
	
		if (legalRepresentativeHistory.getPostalDepartment()!=null && !(legalRepresentativeHistory.getPostalDepartment()
				.equals(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER)))) {
			listProvincePostalLocationRepresentative = getLstProvinceLocation(legalRepresentativeHistory
					.getPostalDepartment());
		} else {
			legalRepresentativeHistory.setPostalProvince(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER));
			listProvincePostalLocationRepresentative = null;
			legalRepresentativeHistory.setPostalDistrict(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER));
			listMunicipalityPostalLocationRepresentative = null;
		}
	}
	
	/**
	 * Load departament postal representative.
	 */
	public void loadDepartamentPostalRepresentative() {
		
		if (legalRepresentativeHistory.getPostalResidenceCountry()!=null && !(legalRepresentativeHistory.getPostalResidenceCountry()
				.equals(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER)))) {
			listDepartmentPostalLocationRepresentative = getLstDepartmentLocation(legalRepresentativeHistory
					.getPostalResidenceCountry());
		} else {
		
			legalRepresentativeHistory.setPostalDepartment(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER));
			listDepartmentLocationRepresentative = null;
			legalRepresentativeHistory.setPostalProvince(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER));
			listProvincePostalLocationRepresentative = null;
			legalRepresentativeHistory.setPostalDistrict(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER));
			listMunicipalityPostalLocationRepresentative = null;
		}
	}
	
	/**
	 * Gets the lst municipality location.
	 *
	 * @param referenceFk the reference fk
	 * @return the lst municipality location
	 */
	public List<GeographicLocation> getLstMunicipalityLocation(Integer referenceFk) {

		List<GeographicLocation> lst = null;

		try {

			GeographicLocationTO geographicLocationTO = new GeographicLocationTO();

			geographicLocationTO
					.setGeographicLocationType(GeographicLocationType.DISTRICT
							.getCode());
			geographicLocationTO
					.setState(GeographicLocationStateType.REGISTERED.getCode());
			geographicLocationTO.setIdLocationReferenceFk(referenceFk);

			lst = generalParametersFacade
					.getListGeographicLocationServiceFacade(geographicLocationTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

		return lst;

	}
	
	/**
	 * Load municipality postal representative.
	 */
	public void loadMunicipalityPostalRepresentative() {
		
		if (legalRepresentativeHistory.getPostalProvince()!=null && !(legalRepresentativeHistory.getPostalProvince().equals(Integer
				.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER)))) {
			listMunicipalityPostalLocationRepresentative = getLstMunicipalityLocation(legalRepresentativeHistory
					.getPostalProvince());
		} else {
			legalRepresentativeHistory.setLegalDistrict(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER));
			listMunicipalityPostalLocationRepresentative = null;
	
		}
	}
	
	/**
	 * Validate same nationality legal.
	 *
	 * @param idFormClient the id form client
	 * @param idComponent the id component
	 * @return true, if successful
	 */
	public boolean validateSameNationalityLegal(String idFormClient,String idComponent){
		boolean flag = false;
		
		if (legalRepresentativeHistory.getNationality() != null
				&& legalRepresentativeHistory.getSecondNationality() != null
				&& (legalRepresentativeHistory.getNationality().equals(legalRepresentativeHistory
						.getSecondNationality()))) {

			JSFUtilities
					.addContextMessage(
							idFormClient+":tabView:"+idComponent,
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities
									.getValidationMessage(GeneralPropertiesConstants.WARNING_SAME_NATIONALITY,null),
							PropertiesUtilities
									.getValidationMessage(GeneralPropertiesConstants.WARNING_SAME_NATIONALITY,null));
			
			
			legalRepresentativeHistory.setSecondNationality(null);
			
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getValidationMessage(GeneralPropertiesConstants.WARNING_SAME_NATIONALITY,null));
			JSFUtilities.showSimpleValidationDialog();

			flag = true;
		}else{
			validateRepresentativeSecondDocumentTypeList();
		}
		
		validateSecondNationalityByIndResidentRepresentative();
		
		if(legalRepresentativeHistory.getSecondNationality()==null){
			legalRepresentativeHistory.setSecondDocumentType(null);
			legalRepresentativeHistory.setSecondDocumentNumber(null);
		}
		
		return flag;
	}

	/**
	 * Gets the lst boolean type.
	 *
	 * @return the lst boolean type
	 */
	public void getLstBooleanType() {
		listBoolean = BooleanType.list;
	}
	
	/**
	 * Change selected economic sector legal representative.
	 */
	public void changeSelectedEconomicSectorLegalRepresentative() {
		try {
			listEconomicActivityLegalRepresentative = generalParametersFacade
					.getListEconomicActivityBySector(legalRepresentativeHistory.getEconomicSector());
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Gets the lst category pep.
	 *
	 * @return the lst category pep
	 */
	public void getLstCategoryPep() {

		try {

			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(MasterTableType.ASSET_LAUNDRY_MOTIVE_PEP
							.getCode());

			listCategoryPep = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);

		}

		catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}
	
	/**
	 * Gets the lst role pep.
	 *
	 * @return the lst role pep
	 */
	public void getLstRolePep() {

		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.PEP_CHARGE
					.getCode());

			listRolePep = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);

		}

		catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}
	
	/**
	 * Gets the lst sex type.
	 *
	 * @return the lst sex type
	 */
	public void getLstSexType() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.SEX.getCode());

			listSex = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Gets the lst representative class type.
	 *
	 * @return the lst representative class type
	 */
	public void getLstRepresentativeClassType() {

		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(MasterTableType.CLASS_REPRENTATIVE
							.getCode());

			listRepresentativeClass = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}
	
	/**
	 * Gets the lst person type.
	 *
	 * @return the lst person type
	 */
	public void getLstPersonType() {

		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.PERSON_TYPE
					.getCode());
			//parameterTableTO.setParameterTableCd("1");;

			listPersonType = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}
	
	/**
	 * Gets the lst geographic location without national country.
	 *
	 * @return the lst geographic location without national country
	 */
	public void getLstGeographicLocationWithoutNationalCountry(){

		try {

			listGeographicLocationWithoutNationalCountry = new ArrayList<GeographicLocation>();
			
			GeographicLocationTO geographicLocationTO = new GeographicLocationTO();

			geographicLocationTO
					.setGeographicLocationType(GeographicLocationType.COUNTRY
							.getCode());
			geographicLocationTO
					.setState(GeographicLocationStateType.REGISTERED.getCode());

			listGeographicLocation = generalParametersFacade
					.getListGeographicLocationServiceFacade(geographicLocationTO);
			
			for(GeographicLocation geo : listGeographicLocation){
				if(!geo.getIdGeographicLocationPk().equals(countryResidence)){
					listGeographicLocationWithoutNationalCountry.add(geo);
				}
			}
			

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Gets the lst geographic location.
	 *
	 * @return the lst geographic location
	 */
	public void getLstGeographicLocation() {

		try {

			GeographicLocationTO geographicLocationTO = new GeographicLocationTO();

			geographicLocationTO
					.setGeographicLocationType(GeographicLocationType.COUNTRY
							.getCode());
			geographicLocationTO
					.setState(GeographicLocationStateType.REGISTERED.getCode());

			listGeographicLocation = generalParametersFacade
					.getListGeographicLocationServiceFacade(geographicLocationTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Gets the lst economic sector.
	 *
	 * @return the lst economic sector
	 */
	public void getLstEconomicSector() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.ECONOMIC_SECTOR
					.getCode());

			listEconomicSector = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}
	
	/**
	 * Gets the lst document type legal.
	 *
	 * @return the lst document type legal
	 */
	public void getLstDocumentTypeLegal() {

		try {
			
			
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON
							.getCode());

			listDocumentTypeLegal = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
						

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}
	
	/**
	 * Gets the lst document source.
	 *
	 * @return the lst document source
	 */
	public void getLstDocumentSource() {

		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(MasterTableType.DOCUMENT_SOURCE.getCode());
			
			
			listDocumentSource = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}
	
	/**
	 * Default holder history.
	 */
	public void defaultHolderHistory(){
		disabledHolderHistory = true;
	}
	
	/**
	 * Default holder request.
	 */
	public void defaultHolderRequest(){
		disabledHolderRequest = true;
	}
	
	/**
	 * Load values component.
	 */
	public void loadValuesComponent(){
		
		legalRepresentativeTO = new LegalRepresentativeTO();
		legalRepresentativeHistoryDetail = new LegalRepresentativeHistory();	
		legalRepresentativeHistory = new LegalRepresentativeHistory();
		lstRepresentativeFileHistory = new ArrayList<RepresentativeFileHistory>();
		lstLegalRepresentativeFile  = new ArrayList<LegalRepresentativeFile>();
		listMunicipalityPostalLocationRepresentative = new ArrayList<GeographicLocation>();
		listProvincePostalLocationRepresentative = new ArrayList<GeographicLocation>();
		listDepartmentPostalLocationRepresentative = new ArrayList<GeographicLocation>();
		listProvinceLocationRepresentative = new ArrayList<GeographicLocation>();
		listMunicipalityLocationRepresentative = new ArrayList<GeographicLocation>();
		listDepartmentLocationRepresentative = new ArrayList<GeographicLocation>();
		listGeographicLocation = new ArrayList<GeographicLocation>();
		newDocumentNumber = null;
		newDocumentNumberSecondNationality = null;
		
		HolderHistory holderHistory = null;
		HolderRequest holderRequest = null;
		
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		
		ValueExpression idLegalRepresentativeComponentValueEx = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.attrs.idLegalRepresentativeComponent}", String.class);
		String idLegalRepresentative= (String)idLegalRepresentativeComponentValueEx.getValue(elContext);
		
		ValueExpression holderHistoryValueEx = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.attrs.holderHistory}", HolderHistory.class);
		if(!disabledHolderHistory){
			holderHistory = (HolderHistory)holderHistoryValueEx.getValue(elContext);
		}
		ValueExpression holderRequestValueEx = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.attrs.holderRequest}", HolderRequest.class);
		if(!disabledHolderRequest){
			holderRequest = (HolderRequest)holderRequestValueEx.getValue(elContext);
		}
		ValueExpression viewOperationValueEx = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.attrs.viewOperation}", Integer.class);
		Integer viewOperation=  Integer.valueOf(viewOperationValueEx.getValue(elContext).toString());
		
		ValueExpression indicatorIssuerValueEx = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.attrs.indicatorIssuer}", Boolean.class);
		indicatorIssuer= Boolean.valueOf(indicatorIssuerValueEx.getValue(elContext).toString());
	
		ValueExpression indicatorHolderValueEx = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.attrs.indicatorHolder}", Boolean.class);
		indicatorHolder= Boolean.valueOf(indicatorHolderValueEx.getValue(elContext).toString());
	
		
		if(Validations.validateIsNotNullAndNotEmpty(idLegalRepresentative)){
			idLegalRepresentativeComponent= idLegalRepresentative;
		}
		
		if(!disabledHolderHistory){
			legalRepresentativeTO.setFlagIndicatorMinorHolder(holderHistory.isIndicatorMinor());
			legalRepresentativeTO.setFlagHolderIndicatorInability(holderHistory.isIndicatorDisabled());
			legalRepresentativeTO.setHolderPersonType((holderHistory.getHolderType()!=null)?holderHistory.getHolderType():null);
			legalRepresentativeTO.setHolderJuridicClassType((holderHistory.getJuridicClass()!=null)?holderHistory.getJuridicClass():null);
			if(holderHistory.getDocumentType()!=null){
				legalRepresentativeTO.setDocumentType(holderHistory.getDocumentType());
			}
			if(holderHistory.getDocumentNumber()!=null){
				legalRepresentativeTO.setDocumentNumber(holderHistory.getDocumentNumber());
			}
			if(holderHistory.getDocumentSource()!=null){
				legalRepresentativeTO.setEmissionSource(holderHistory.getDocumentSource());
			}
			if(holderHistory.getFirstLastName()!=null){
				legalRepresentativeTO.setFirstLastName(holderHistory.getFirstLastName());
			}
			
		}
		
		if(!disabledHolderRequest){
			legalRepresentativeTO.setHolderRequest(holderRequest);
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(viewOperation)){
			legalRepresentativeTO.setViewOperationType(viewOperation);
		}
		
		
	}
	
	/**
	 * Gets the lst document issuance date.
	 *
	 * @return the lst document issuance date
	 */
	public void getLstDocumentIssuanceDate(){
		listDocumentIssuanceDate = new LinkedHashMap<String,Integer>();
		Date today = new Date();
		DateFormat df = new SimpleDateFormat("yyyy");
		int year = Integer.parseInt(df.format(today));
		
		for(int i=1900;i<=year;i++){
			listDocumentIssuanceDate.put(String.valueOf(i),i);
		}
	}
	
	/**
	 * Load representative.
	 */
	public void loadRepresentative() {
		
		loadValuesComponent();
		
		if(indicatorIssuer){
			legalRepresentativeTO.setDisabledPersonTypeRepresentative(false);
			legalRepresentativeTO.setFlagCmbRepresentativeClass(true);
			legalRepresentativeHistory.setPersonType(PersonType.NATURAL.getCode());
			legalRepresentativeHistory.setRepresentativeClass(RepresentativeClassType.LEGAL.getCode());
			legalRepresentativeTO.setDisabledCmbPostalCountryRepresentative(false);
			legalRepresentativeTO.setDisabledCmbPostalProvinceRepresentative(false);
			legalRepresentativeTO.setDisabledCmbPostalDepartamentRepresentative(false);
			legalRepresentativeTO.setDisabledInputPostalAddressRepresentative(false);		
			legalRepresentativeTO.setDisabledRepresentativeNationality(false);
			legalRepresentativeTO.setDisabledRepresentativeResident(false);
			
			legalRepresentativeTO.setDisabledCountryResidentRepresentative(false);
			legalRepresentativeTO.setFlagCopyLegalRepresentative(false);
			
			listRepresentativeClass = new ArrayList<ParameterTable>();
			
			legalRepresentativeHistory.setBeginningPeriod(null);
			legalRepresentativeHistory.setEndingPeriod(null);
			legalRepresentativeTO.setFlagRepresentativeIsPEP(false);
			
			legalRepresentativeTO.setFlagRepresentativeNaturalPerson(true);
			legalRepresentativeTO.setFlagRepresentativeJuridicPerson(false);

			legalRepresentativeTO.setDisabledRepresentativeNationality(false);
			legalRepresentativeTO.setDisabledRepresentativeResident(false);
			legalRepresentativeTO.setDisabledCountryResidentRepresentative(false);
						
		}
		
		
		if(legalRepresentativeTO.getViewOperationType().equals(ViewOperationsType.REGISTER.getCode())){
			legalRepresentativeTO.setFlagAddRepresentativeModify(false);
			legalRepresentativeTO.setFlagActionModify(false);
		}
		
		else if(legalRepresentativeTO.getViewOperationType().equals(ViewOperationsType.MODIFY.getCode())){
			legalRepresentativeTO.setFlagAddRepresentativeModify(true);			
			legalRepresentativeTO.setFlagModifyRepresentativeModify(false);
			legalRepresentativeTO.setFlagActionModify(true);
		}
		
		getLstBooleanType();
		getLstPersonType();
		getLstSexType();
		getLstGeographicLocation();
		getLstGeographicLocationWithoutNationalCountry();
		getLstDocumentTypeLegal();
		getLstEconomicSector();
		getLstRolePep();
		getLstCategoryPep();
		
		legalRepresentativeTO.setFlagRepresentativeIsPEP(false);
		legalRepresentativeTO.setFlagRepresentativeJuridicPerson(false);
		legalRepresentativeTO.setFlagRepresentativeNaturalPerson(false);	
		legalRepresentativeTO.setDisabledCmbPostalCountryRepresentative(false);
		legalRepresentativeTO.setDisabledCmbPostalProvinceRepresentative(false);
		legalRepresentativeTO.setDisabledCmbPostalDepartamentRepresentative(false);
		legalRepresentativeTO.setDisabledInputPostalAddressRepresentative(false);
		legalRepresentativeTO.setFlagCopyLegalRepresentative(false);
		legalRepresentativeTO.setDisabledRepresentativeResident(false);
		legalRepresentativeTO.setDisabledCountryResidentRepresentative(false);
		legalRepresentativeTO.setDisabledCmbPostalMunicipalityRepresentative(false);
		
		legalRepresentativeTO.setRepresentativeFileNameDisplay(GeneralConstants.EMPTY_STRING);
		listRepresentativeDocumentType = null;
		lstRepresentativeFileHistory = null;
		legalRepresentativeTO.setAttachFileRepresentativeSelected(null);

		legalRepresentativeHistory.setFlagLegalImport(false);
		legalRepresentativeHistory.setFlagActiveFilesByHolderImport(true);
		getLstDocumentIssuanceDate();
		
		if(indicatorIssuer)
		{			
			legalRepresentativeTO.setFlagRepresentativeNaturalPerson(true);
			getLstRepresentativeClassType();
			legalRepresentativeHistory.setNationality(countryResidence);
			legalRepresentativeHistory.setIndResidence(BooleanType.YES.getCode());
			validateResidentRepresentative();
			legalRepresentativeHistory.setDocumentType(DocumentType.CI.getCode());
			validateTypeDocumentLegal(idLegalRepresentativeComponent,"cmbDocumentType");
			
		}
			
			JSFUtilities.executeJavascriptFunction("PF('registerRepresentativeWidget').show();");
		
		
		
	}
	
	/**
	 * Removes the legal representative request file.
	 *
	 * @param representativeFileHistory the representative file history
	 */
	public void removeLegalRepresentativeRequestFile(RepresentativeFileHistory representativeFileHistory){
		lstRepresentativeFileHistory.remove(representativeFileHistory);
		legalRepresentativeTO.setRepresentativeFileNameDisplay(null);
		legalRepresentativeTO.setAttachFileRepresentativeSelected(null);
		
	}
	
/**
 * Representative view detail.
 *
 * @param event the event
 */
public void representativeViewDetail(ActionEvent event) {
	
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
	
		ValueExpression indicatorIssuerValueEx = context.getApplication().getExpressionFactory()
				.createValueExpression(elContext, "#{cc.attrs.indicatorIssuer}", Boolean.class);
		indicatorIssuer= Boolean.valueOf(indicatorIssuerValueEx.getValue(elContext).toString());
    
		ValueExpression indicatorHolderValueEx = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.attrs.indicatorHolder}", Boolean.class);
		indicatorHolder= Boolean.valueOf(indicatorHolderValueEx.getValue(elContext).toString());
	
		
	
		representedEntity = new RepresentedEntity();
		
		legalRepresentative = new LegalRepresentative();
		
		LegalRepresentative objLegalRepresentative = (LegalRepresentative) event
				.getComponent().getAttributes().get("representativeObj");
		
		legalRepresentative = objLegalRepresentative;
		
		if(objLegalRepresentative.getLegalRepresentativeFile()!=null){
			lstLegalRepresentativeFile = legalRepresentative.getLegalRepresentativeFile();
		}
		if(legalRepresentative.getPersonType().equals(PersonType.NATURAL.getCode())){
			legalRepresentativeTO.setFlagRepresentativeNaturalPerson(true);
			legalRepresentativeTO.setFlagRepresentativeJuridicPerson(false);
		}
		else if(legalRepresentative.getPersonType().equals(PersonType.JURIDIC.getCode())){
			legalRepresentativeTO.setFlagRepresentativeJuridicPerson(true);
			legalRepresentativeTO.setFlagRepresentativeNaturalPerson(false);
		}
		
		if(indicatorHolder){
		for(int i=0;i<holder.getRepresentedEntity().size();i++){
			if(holder.getRepresentedEntity().get(i).getLegalRepresentative().getIdLegalRepresentativePk().equals(objLegalRepresentative.getIdLegalRepresentativePk())){
				representedEntity.setRepresentativeClass(holder.getRepresentedEntity().get(i).getRepresentativeClass());
			}
		}
		
		}
		if(indicatorIssuer){
			representedEntity.setRepresentativeClass(legalRepresentative.getRepresentativeClassAux());
		}
		
		listDocumentTypeLegalResult = documentValidator.getLstDocumentType();
		getLstGeographicLocation();
		getLstPersonType();
		getLstRepresentativeClassType();
		getLstBooleanType();
		getLstSexType();
		getLstRolePep();
		getLstCategoryPep();
		getLstEconomicSector();
		changeSelectedEconomicSectorLegalRepresentative();
		getLstDocumentSource();
		getLstDocumentIssuanceDate();
		
		listDepartmentLocationRepresentative = getLstDepartmentLocation(legalRepresentative.getLegalResidenceCountry());
		listProvinceLocationRepresentative = getLstProvinceLocation(legalRepresentative.getLegalDepartment());
		listMunicipalityLocationRepresentative = getLstMunicipalityLocation(legalRepresentative.getLegalProvince());
		
		
		listDepartmentPostalLocationRepresentative = getLstDepartmentLocation(legalRepresentative.getPostalResidenceCountry());
		listProvincePostalLocationRepresentative = getLstProvinceLocation(legalRepresentative.getPostalDepartment());
		listMunicipalityPostalLocationRepresentative = getLstMunicipalityLocation(legalRepresentative.getPostalProvince());
		
		loadLegalRepresentativePepExists(objLegalRepresentative.getIdLegalRepresentativePk());		
		
		
		JSFUtilities.executeJavascriptFunction("PF('detailRepresentativeWidget').show()");
		
	}
   
    /**
     * Load legal history pep exist.
     *
     * @param identificatorLegal the identificator legal
     */
    public void loadLegalHistoryPepExist(Long identificatorLegal){
    	try{
    	
    		if(identificatorLegal!=null){
			 
			Integer count =  accountsFacade.validateExistPepPersonByIdLegalRepresentativeServiceFacade(identificatorLegal);
			 
			
			if(count>0){
			
				PepPerson objPepPersonLegal = accountsFacade.getPepPersonByIdLegalRepresentativeServiceFacade(identificatorLegal) ;
				   				
				if(objPepPersonLegal!=null){
					legalRepresentativeHistory.setIndPEP(GeneralConstants.ONE_VALUE_INTEGER);
					legalRepresentativeHistory.setBeginningPeriod(objPepPersonLegal.getBeginingPeriod());
					legalRepresentativeHistory.setEndingPeriod(objPepPersonLegal.getEndingPeriod());
					legalRepresentativeHistory.setCategory(objPepPersonLegal.getCategory());
					legalRepresentativeHistory.setRole(objPepPersonLegal.getRole());					
					legalRepresentativeTO.setFlagRepresentativeIsPEP(true);
				}
				else{
					legalRepresentativeTO.setFlagRepresentativeIsPEP(false);
					legalRepresentativeHistory.setIndPEP(GeneralConstants.ZERO_VALUE_INTEGER);
					legalRepresentativeHistory.setBeginningPeriod(null);
					legalRepresentativeHistory.setEndingPeriod(null);
					legalRepresentativeHistory.setCategory(null);
					legalRepresentativeHistory.setRole(null);
				}			
			
			}
		 }
    	}
    	catch(Exception ex){
    		excepcion.fire(new ExceptionToCatchEvent(ex));
    	}
    }

	/**
	 * Load legal representative pep exists.
	 *
	 * @param identificatorLegal the identificator legal
	 */
	public void loadLegalRepresentativePepExists(Long identificatorLegal){		
	try{
		 pepPersonLegal = new PepPerson();
		 
		 if(identificatorLegal!=null){
			 
			PepPerson objPepPersonLegal = accountsFacade.getPepPersonByIdLegalRepresentativeServiceFacade(identificatorLegal) ;
		   
			if(objPepPersonLegal!=null){
				pepPersonLegal = objPepPersonLegal;
				legalRepresentativeTO.setFlagRepresentativeIsPEP(true);				
			}
			else{
				legalRepresentativeTO.setFlagRepresentativeIsPEP(false);
			}
		 
		 }
		
		}
		catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Gets the streamed content file legal representative.
	 *
	 * @param id the id
	 * @return the streamed content file legal representative
	 */
	public StreamedContent getStreamedContentFileLegalRepresentative(Long id){
	
		try {
			LegalRepresentativeFile auxLegalRepresentativeFile = accountsFacade
					.getContentFileLegalRepresentativeServiceFacade(id);
			InputStream inputStream;

			inputStream = new ByteArrayInputStream(
					auxLegalRepresentativeFile.getFileLegalRepre());
			streamedContentFile = new DefaultStreamedContent(inputStream, null,
					auxLegalRepresentativeFile.getFilename());
			inputStream.close();

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

		return streamedContentFile;
	}



	
	/**
	 * Representative history view detail.
	 *
	 * @param event the event
	 */
	public void representativeHistoryViewDetail(ActionEvent event) {
		
		Integer indicator = Integer.valueOf(event.getComponent().getAttributes().get("rowRepresentative").toString());
		Integer before =null;
		if(Validations.validateIsNotNullAndNotEmpty(event.getComponent().getAttributes().get("before")))
			before = Integer.valueOf(event.getComponent().getAttributes().get("before").toString());
		
		legalRepresentativeHistory = new LegalRepresentativeHistory();
		
		if(Validations.validateIsNotNullAndNotEmpty(before))
		{
			if(lstLegalRepresentativeHistoryDetailBefore!=null && lstLegalRepresentativeHistoryDetailBefore.size() >0
					&& before.equals(Integer.valueOf(1))){
				legalRepresentativeHistoryDetail = (LegalRepresentativeHistory)lstLegalRepresentativeHistoryDetailBefore.get(indicator);
			}
			
			if(lstLegalRepresentativeHistoryDetail!=null && lstLegalRepresentativeHistoryDetail.size() >0
					&& before.equals(Integer.valueOf(0))){
				legalRepresentativeHistoryDetail = (LegalRepresentativeHistory)lstLegalRepresentativeHistoryDetail.get(indicator);
			}
		}		
		else
		{
			if(lstLegalRepresentativeHistory!=null && lstLegalRepresentativeHistory.size()>0){
				legalRepresentativeHistoryDetail = (LegalRepresentativeHistory)lstLegalRepresentativeHistory.get(indicator);
			}
			
		}		
		
		if(Validations.validateListIsNotNullAndNotEmpty(legalRepresentativeHistoryDetail.getRepresenteFileHistory())){
			lstRepresentativeFileHistory = (List<RepresentativeFileHistory>)legalRepresentativeHistoryDetail.getRepresenteFileHistory();
		} else {
			if(legalRepresentativeTO.isFlagIssuerOrParticipant()){
				lstRepresentativeFileHistory = listRepreFileHistoryWithLegalRepreFiles(legalRepresentativeHistoryDetail.getIdRepresentativeHistoryPk());
				legalRepresentativeHistoryDetail.setRepresenteFileHistory(lstRepresentativeFileHistory);
			}else{
				lstRepresentativeFileHistory=null;
			}
		}
		
		listDocumentTypeLegalResult = documentValidator.getLstDocumentType();
		getLstGeographicLocation();
		getLstPersonType();
		getLstRepresentativeClassType();
		getLstBooleanType();
		getLstSexType();
		getLstRolePep();
		getLstCategoryPep();
		getLstEconomicSector();
		changeSelectedEconomicSectorLegalRepresentative();
		getLstDocumentSource();
		getLstDocumentIssuanceDate();
		
		if (legalRepresentativeHistoryDetail.getPersonType().equals(
				PersonType.NATURAL.getCode())) {
			legalRepresentativeTO.setFlagRepresentativeNaturalPerson(true);
			legalRepresentativeTO.setFlagRepresentativeJuridicPerson(false);
		} else if (legalRepresentativeHistoryDetail.getPersonType().equals(
				PersonType.JURIDIC.getCode())) {
			legalRepresentativeTO.setFlagRepresentativeNaturalPerson(false);
			legalRepresentativeTO.setFlagRepresentativeJuridicPerson(true);
		}
		
		if(legalRepresentativeHistoryDetail.getIndPEP()!=null && legalRepresentativeHistoryDetail.getIndPEP().equals(GeneralConstants.ONE_VALUE_INTEGER)){
			legalRepresentativeTO.setFlagRepresentativeIsPEP(true);
		} else{
			legalRepresentativeTO.setFlagRepresentativeIsPEP(false);
		}

		listDepartmentLocationRepresentative = getLstDepartmentLocation(legalRepresentativeHistoryDetail.getLegalResidenceCountry());
		listProvinceLocationRepresentative = getLstProvinceLocation(legalRepresentativeHistoryDetail.getLegalDepartment());
		listMunicipalityLocationRepresentative = getLstMunicipalityLocation(legalRepresentativeHistoryDetail.getLegalProvince());

		listDepartmentPostalLocationRepresentative = getLstDepartmentLocation(legalRepresentativeHistoryDetail.getPostalResidenceCountry());
		listProvincePostalLocationRepresentative = getLstProvinceLocation(legalRepresentativeHistoryDetail.getPostalDepartment());
		listMunicipalityPostalLocationRepresentative = getLstMunicipalityLocation(legalRepresentativeHistoryDetail.getPostalProvince());
		
		JSFUtilities.executeJavascriptFunction("PF('detailRepresentativeWidget').show()");
	}
	
	/**
	 * List repre file history with legal repre files.
	 *
	 * @param idLegalRepresentativePk the id legal representative pk
	 * @return the list
	 */
	private List<RepresentativeFileHistory> listRepreFileHistoryWithLegalRepreFiles(Long idLegalRepresentativePk){
		
		List<RepresentativeFileHistory> lstRepresentativeFileHistoryTemp = new ArrayList<RepresentativeFileHistory>();
		
		try {
			
			//First get the files registered on LegalRepresentativeFile by id of LegalRepresentative
			List<LegalRepresentativeFile> lstLegalRepresentativeFileTemp = 
						accountsFacade.getLegalRepresentativeFileByIdLegalRepresentativeServiceFacade(idLegalRepresentativePk);
		
			//Add data of list of LegalRepresentativeFile to the list of RepresentativeFileHistory
			if(Validations.validateListIsNotNullAndNotEmpty(lstLegalRepresentativeFileTemp)) {
				RepresentativeFileHistory representativeFileHistoryTemp;
				for (LegalRepresentativeFile objLegalRepresentativeFile : lstLegalRepresentativeFileTemp) {
					representativeFileHistoryTemp = new RepresentativeFileHistory();
					representativeFileHistoryTemp.setDocumentType(objLegalRepresentativeFile.getDocumentType());
					representativeFileHistoryTemp.setDescription(objLegalRepresentativeFile.getDescription());
					representativeFileHistoryTemp.setFilename(objLegalRepresentativeFile.getFilename());
					representativeFileHistoryTemp.setFileRepresentative(objLegalRepresentativeFile.getFileLegalRepre());
					lstRepresentativeFileHistoryTemp.add(representativeFileHistoryTemp);
				}
			}
		
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		
		return lstRepresentativeFileHistoryTemp;
	}
	
	/**
	 * Representative modify.
	 *
	 * @param event the event
	 */
	public void representativeModify(ActionEvent event){
		
		legalRepresentativeTO.setViewOperationType(ViewOperationsType.MODIFY.getCode());
        legalRepresentativeTO.setFlagAddRepresentativeModify(false);
        legalRepresentativeTO.setFlagModifyRepresentativeModify(true);
        
        HolderHistory holderHistory = null;
		
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		
		ValueExpression idLegalRepresentativeComponentValueEx = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.attrs.idLegalRepresentativeComponent}", String.class);
		idLegalRepresentativeComponent= (String)idLegalRepresentativeComponentValueEx.getValue(elContext);
		
        ValueExpression holderHistoryValueEx = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.attrs.holderHistory}", HolderHistory.class);
        
        ValueExpression indicatorIssuerValueEx = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.attrs.indicatorIssuer}", Boolean.class);
		indicatorIssuer= Boolean.valueOf(indicatorIssuerValueEx.getValue(elContext).toString());
        
        if(indicatorIssuer)			
			disabledHolderHistory=true;
		
        
		if(!disabledHolderHistory){
			
			ValueExpression indicatorHolderValueEx = context.getApplication().getExpressionFactory()
				    .createValueExpression(elContext, "#{cc.attrs.indicatorHolder}", Boolean.class);
			indicatorHolder= Boolean.valueOf(indicatorHolderValueEx.getValue(elContext).toString());
		
			holderHistory = (HolderHistory)holderHistoryValueEx.getValue(elContext);
			legalRepresentativeTO.setFlagIndicatorMinorHolder(holderHistory.isIndicatorMinor());
			legalRepresentativeTO.setFlagHolderIndicatorInability(holderHistory.isIndicatorDisabled());
			legalRepresentativeTO.setHolderPersonType((holderHistory.getHolderType()!=null)?holderHistory.getHolderType():null);
			legalRepresentativeTO.setHolderJuridicClassType((holderHistory.getJuridicClass()!=null)?holderHistory.getJuridicClass():null);
	
		}

		legalRepresentativeHistory = new LegalRepresentativeHistory();		
			
		int indicator = Integer.parseInt(event.getComponent().getAttributes().get("rowRepresentative").toString());
		legalRepresentativeTO.setRowIndicatorLegalRepresentative(Integer.valueOf(indicator));
		
		legalRepresentativeHistory.setRepresentativeClass(lstLegalRepresentativeHistory.get(indicator).getRepresentativeClass());
		
		if(indicatorHolder){
			legalRepresentativeHistory.setHolderRequest(lstLegalRepresentativeHistory.get(indicator).getHolderRequest());
		}
		
		legalRepresentativeHistory.setIdRepresentativeHistoryPk(lstLegalRepresentativeHistory.get(indicator).getIdRepresentativeHistoryPk());
		
		legalRepresentativeHistory.setBirthDate(lstLegalRepresentativeHistory.get(indicator).getBirthDate());
		
	    legalRepresentativeHistory.setDocumentType(lstLegalRepresentativeHistory.get(indicator).getDocumentType());
		
		legalRepresentativeHistory.setDocumentNumber(getDocumentNumber(lstLegalRepresentativeHistory.get(indicator).getDocumentNumber(), lstLegalRepresentativeHistory.get(indicator).getDocumentType()));
		
		legalRepresentativeHistory.setDocumentIssuanceDate(lstLegalRepresentativeHistory.get(indicator).getDocumentIssuanceDate());
		
	    legalRepresentativeHistory.setEconomicActivity(lstLegalRepresentativeHistory.get(indicator).getEconomicActivity());
		
		legalRepresentativeHistory.setEconomicSector(lstLegalRepresentativeHistory.get(indicator).getEconomicSector());
		
		legalRepresentativeHistory.setEmail(lstLegalRepresentativeHistory.get(indicator).getEmail());
		
		legalRepresentativeHistory.setFaxNumber(lstLegalRepresentativeHistory.get(indicator).getFaxNumber());
		
	    legalRepresentativeHistory.setFirstLastName(lstLegalRepresentativeHistory.get(indicator).getFirstLastName());
		
	    legalRepresentativeHistory.setFullName(lstLegalRepresentativeHistory.get(indicator).getFullName());
		
		legalRepresentativeHistory.setHomePhoneNumber(lstLegalRepresentativeHistory.get(indicator).getHomePhoneNumber());
			
		legalRepresentativeHistory.setIndResidence(lstLegalRepresentativeHistory.get(indicator).getIndResidence()); 
		
		legalRepresentativeHistory.setLastModifyApp(lstLegalRepresentativeHistory.get(indicator).getLastModifyApp());
		
		legalRepresentativeHistory.setLastModifyDate(lstLegalRepresentativeHistory.get(indicator).getLastModifyDate());
		
		legalRepresentativeHistory.setLastModifyIp(lstLegalRepresentativeHistory.get(indicator).getLastModifyIp());
		
		legalRepresentativeHistory.setLastModifyUser(lstLegalRepresentativeHistory.get(indicator).getLastModifyUser());
		
		legalRepresentativeHistory.setLegalAddress(lstLegalRepresentativeHistory.get(indicator).getLegalAddress());
		
		legalRepresentativeHistory.setLegalDepartment(lstLegalRepresentativeHistory.get(indicator).getLegalDepartment());
		
		legalRepresentativeHistory.setLegalDistrict(lstLegalRepresentativeHistory.get(indicator).getLegalDistrict());
		
		legalRepresentativeHistory.setLegalProvince(lstLegalRepresentativeHistory.get(indicator).getLegalProvince());
		
		legalRepresentativeHistory.setLegalResidenceCountry(lstLegalRepresentativeHistory.get(indicator).getLegalResidenceCountry());
		
		legalRepresentativeHistory.setMobileNumber(lstLegalRepresentativeHistory.get(indicator).getMobileNumber());
		
		legalRepresentativeHistory.setName(lstLegalRepresentativeHistory.get(indicator).getName()); 
		
		legalRepresentativeHistory.setNationality(lstLegalRepresentativeHistory.get(indicator).getNationality());
		
		legalRepresentativeHistory.setOfficePhoneNumber(lstLegalRepresentativeHistory.get(indicator).getOfficePhoneNumber());
		
		legalRepresentativeHistory.setPostalDepartment(lstLegalRepresentativeHistory.get(indicator).getPostalDepartment());
		
		legalRepresentativeHistory.setPostalDistrict(lstLegalRepresentativeHistory.get(indicator).getPostalDistrict());
		
		legalRepresentativeHistory.setPostalProvince(lstLegalRepresentativeHistory.get(indicator).getPostalProvince());
		
		legalRepresentativeHistory.setPostalResidenceCountry(lstLegalRepresentativeHistory.get(indicator).getPostalResidenceCountry());
		
		legalRepresentativeHistory.setPostalAddress(lstLegalRepresentativeHistory.get(indicator).getPostalAddress());
		
		legalRepresentativeHistory.setSecondLastName(lstLegalRepresentativeHistory.get(indicator).getSecondLastName());
		
		legalRepresentativeHistory.setSecondNationality(lstLegalRepresentativeHistory.get(indicator).getSecondNationality());	
		
		legalRepresentativeHistory.setSecondDocumentType(lstLegalRepresentativeHistory.get(indicator).getSecondDocumentType());
		
		legalRepresentativeHistory.setSex(lstLegalRepresentativeHistory.get(indicator).getSex());
		
		legalRepresentativeHistory.setStateRepreHistory(lstLegalRepresentativeHistory.get(indicator).getStateRepreHistory());
		
		legalRepresentativeHistory.setPersonType(lstLegalRepresentativeHistory.get(indicator).getPersonType());
		
		legalRepresentativeHistory.setIndPEP(lstLegalRepresentativeHistory.get(indicator).getIndPEP());
		
		legalRepresentativeHistory.setCategory(lstLegalRepresentativeHistory.get(indicator).getCategory());
		
		legalRepresentativeHistory.setRole(lstLegalRepresentativeHistory.get(indicator).getRole());
		
		legalRepresentativeHistory.setBeginningPeriod(lstLegalRepresentativeHistory.get(indicator).getBeginningPeriod());
		
		legalRepresentativeHistory.setEndingPeriod(lstLegalRepresentativeHistory.get(indicator).getEndingPeriod());
		
		legalRepresentativeHistory.setDocumentSource(lstLegalRepresentativeHistory.get(indicator).getDocumentSource());
		
		if(lstLegalRepresentativeHistory.get(indicator).getSecondDocumentType() != null && lstLegalRepresentativeHistory.get(indicator).getSecondDocumentNumber() != null){
			legalRepresentativeHistory.setSecondDocumentNumber(getDocumentNumber(lstLegalRepresentativeHistory.get(indicator).getSecondDocumentNumber(), lstLegalRepresentativeHistory.get(indicator).getSecondDocumentType()));
			
			if(lstLegalRepresentativeHistory.get(indicator).getSecondDocumentType().equals(DocumentType.CIE.getCode())){
				newDocumentNumberSecondNationality = "E-" + legalRepresentativeHistory.getSecondDocumentNumber();
			}
		}
		
		if(legalRepresentativeHistory.getIndPEP()!=null && legalRepresentativeHistory.getIndPEP().equals(GeneralConstants.ONE_VALUE_INTEGER)){
			legalRepresentativeTO.setFlagRepresentativeIsPEP(true);
		}
		else{
			legalRepresentativeTO.setFlagRepresentativeIsPEP(false);
		}
		
		getLstDocumentTypeLegal();		
		getLstGeographicLocation();
		getLstPersonType();
		getLstRepresentativeClassType();
		getLstBooleanType();
		getLstSexType();
		getLstRolePep();
		getLstCategoryPep();
		getLstEconomicSector();
		changeSelectedEconomicSectorLegalRepresentative();
		getLstDocumentSource();
		getLstDocumentIssuanceDate();
		loadNewDocumentNumber();
		getLstGeographicLocationWithoutNationalCountry();
		
		listDocumentTypeLegalResult= listDocumentTypeLegal;
		
		
		legalRepresentativeTO.setDisabledCmbPostalCountryRepresentative(false);
		legalRepresentativeTO.setDisabledCmbPostalProvinceRepresentative(false);
		legalRepresentativeTO.setDisabledCmbPostalDepartamentRepresentative(false);
		legalRepresentativeTO.setDisabledInputPostalAddressRepresentative(false);		
		legalRepresentativeTO.setDisabledRepresentativeNationality(false);
		legalRepresentativeTO.setDisabledRepresentativeResident(false);
		legalRepresentativeTO.setDisabledCountryResidentRepresentative(false);
		legalRepresentativeTO.setFlagCopyLegalRepresentative(false);
		
		
		listRepresentativeClass = new ArrayList<ParameterTable>();
		getLstRepresentativeClassType();
		legalRepresentativeTO.setFlagCmbRepresentativeClass(false);
		ParameterTable auxParameterTable;

		if(indicatorHolder)
		{
			if (legalRepresentativeHistory.getPersonType().equals(
					PersonType.NATURAL.getCode())) {			
				
				legalRepresentativeTO.setFlagRepresentativeNaturalPerson(true);
				legalRepresentativeTO.setFlagRepresentativeJuridicPerson(false);

				if(legalRepresentativeTO.getHolderPersonType().equals(PersonType.NATURAL.getCode())) {
					
					legalRepresentativeTO.setFlagCmbRepresentativeClass(false);

					auxParameterTable = new ParameterTable();
					List<ParameterTable> auxListPresentantiveClass = listRepresentativeClass;
					listRepresentativeClass = new ArrayList<ParameterTable>();

					for (int i = 0; i < auxListPresentantiveClass.size(); i++) {
						if (auxListPresentantiveClass.get(i).getParameterTablePk()
								.equals(RepresentativeClassType.FATHER.getCode())) {
							auxParameterTable = new ParameterTable();
							auxParameterTable
									.setParameterTablePk(auxListPresentantiveClass
											.get(i).getParameterTablePk());
							auxParameterTable
									.setParameterName(auxListPresentantiveClass
											.get(i).getParameterName());
							listRepresentativeClass.add(auxParameterTable);
						}
						if (auxListPresentantiveClass
								.get(i)
								.getParameterTablePk()
								.equals(RepresentativeClassType.TUTOR_LEGAL
										.getCode())) {
							auxParameterTable = new ParameterTable();
							auxParameterTable
									.setParameterTablePk(auxListPresentantiveClass
											.get(i).getParameterTablePk());
							auxParameterTable
									.setParameterName(auxListPresentantiveClass
											.get(i).getParameterName());
							listRepresentativeClass.add(auxParameterTable);
						}
					}

				} else  {

					legalRepresentativeHistory
							.setRepresentativeClass(RepresentativeClassType.LEGAL
									.getCode());
					legalRepresentativeTO.setFlagCmbRepresentativeClass(true);
					
					loadByRepresentativeTypePerson();

				}
				
				

			} else if (legalRepresentativeHistory.getPersonType().equals(
					PersonType.JURIDIC.getCode())) {
				
				
				SelectOneMenu cmbRC  = (SelectOneMenu)	FacesContext.getCurrentInstance().getViewRoot().findComponent(idLegalRepresentativeComponent+":tabView:"+"cmbRepresentativeClass");
				
				getListDocumentTypeLegal();
				listDocumentTypeLegalResult = new ArrayList<ParameterTable>();
				
				
				if (listDocumentTypeLegal != null
						&& listDocumentTypeLegal.size() > 0) {
					for (int i = 0; i < listDocumentTypeLegal.size(); i++) {
						auxParameterTable = new ParameterTable();
						
						if ((listDocumentTypeLegal.get(i).getParameterTablePk()
								.equals(DocumentType.RUC.getCode()))) {
							auxParameterTable
									.setParameterTablePk(listDocumentTypeLegal.get(
											i).getParameterTablePk());
							auxParameterTable
									.setParameterName(listDocumentTypeLegal.get(i)
											.getParameterName());

							listDocumentTypeLegalResult.add(auxParameterTable);
						}				
						
					}
				}
				
			if (legalRepresentativeTO.getHolderPersonType().equals(
						PersonType.JURIDIC.getCode())
						&& legalRepresentativeTO.getHolderJuridicClassType().equals(
								JuridicClassType.TRUST.getCode())) {
					
					legalRepresentativeTO.setFlagCmbRepresentativeClass(false);

					auxParameterTable = new ParameterTable();
					List<ParameterTable> auxListPresentantiveClass = listRepresentativeClass;
					listRepresentativeClass = new ArrayList<ParameterTable>();

					for (int i = 0; i < auxListPresentantiveClass.size(); i++) {
						if (auxListPresentantiveClass.get(i).getParameterTablePk()
								.equals(RepresentativeClassType.LEGAL.getCode())) {
							auxParameterTable = new ParameterTable();
							auxParameterTable
									.setParameterTablePk(auxListPresentantiveClass
											.get(i).getParameterTablePk());
							auxParameterTable
									.setParameterName(auxListPresentantiveClass
											.get(i).getParameterName());
							listRepresentativeClass.add(auxParameterTable);
						}
						if (auxListPresentantiveClass
								.get(i)
								.getParameterTablePk()
								.equals(RepresentativeClassType.FIDUCIARY.getCode())) {
							auxParameterTable = new ParameterTable();
							auxParameterTable
									.setParameterTablePk(auxListPresentantiveClass
											.get(i).getParameterTablePk());
							auxParameterTable
								.setParameterName(auxListPresentantiveClass
											.get(i).getParameterName());
							listRepresentativeClass.add(auxParameterTable);
						}
					}
				} else if(legalRepresentativeTO.getHolderPersonType().equals(
						PersonType.JURIDIC.getCode())&& !legalRepresentativeTO.getHolderJuridicClassType().equals(
								JuridicClassType.TRUST.getCode())) {

					legalRepresentativeHistory
							.setRepresentativeClass(RepresentativeClassType.LEGAL
									.getCode());
					legalRepresentativeTO.setFlagCmbRepresentativeClass(true);
					loadByRepresentativeTypePerson();
					cmbRC.setValid(true);

				}
				
				else {
					
					if(legalRepresentativeTO.getHolderPersonType().equals(
						PersonType.JURIDIC.getCode())){
					
					legalRepresentativeTO.setFlagCmbRepresentativeClass(false);

					auxParameterTable = new ParameterTable();
					List<ParameterTable> auxListPresentantiveClass = listRepresentativeClass;
					listRepresentativeClass = new ArrayList<ParameterTable>();

					for (int i = 0; i < auxListPresentantiveClass.size(); i++) {
						if (auxListPresentantiveClass.get(i).getParameterTablePk()
								.equals(RepresentativeClassType.FATHER.getCode())) {
							auxParameterTable = new ParameterTable();
							auxParameterTable
									.setParameterTablePk(auxListPresentantiveClass
											.get(i).getParameterTablePk());
							auxParameterTable
									.setParameterName(auxListPresentantiveClass
											.get(i).getParameterName());
							listRepresentativeClass.add(auxParameterTable);
						}
						if (auxListPresentantiveClass
								.get(i)
								.getParameterTablePk()
								.equals(RepresentativeClassType.TUTOR_LEGAL
										.getCode())) {
							auxParameterTable = new ParameterTable();
							auxParameterTable
									.setParameterTablePk(auxListPresentantiveClass
											.get(i).getParameterTablePk());
							auxParameterTable
									.setParameterName(auxListPresentantiveClass
											.get(i).getParameterName());
							listRepresentativeClass.add(auxParameterTable);
						}
					}

					}
				}
				
				if(legalRepresentativeTO.getHolderPersonType().equals(PersonType.NATURAL.getCode())){
					legalRepresentativeHistory
					.setRepresentativeClass(RepresentativeClassType.LEGAL
							.getCode());
						legalRepresentativeTO.setFlagCmbRepresentativeClass(true);
						cmbRC.setValid(true);
						loadByRepresentativeTypePerson();
						
						
				}

				legalRepresentativeTO.setFlagRepresentativeJuridicPerson(true);
				legalRepresentativeTO.setFlagRepresentativeNaturalPerson(false);
			}
			else if(legalRepresentativeHistory.getPersonType().equals(
					Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER))){
				cleanAllRepresentative();
			}
		}else {
			legalRepresentativeTO.setDisabledPersonTypeRepresentative(true);		
			legalRepresentativeTO.setFlagCmbRepresentativeClass(true);
		}

		
		legalRepresentativeHistory.setFlagActiveFilesByHolderImport(true);

		lstRepresentativeFileHistory = new ArrayList<RepresentativeFileHistory>();
		
		if(lstLegalRepresentativeHistory.get(indicator).getRepresenteFileHistory()!=null && lstLegalRepresentativeHistory.get(indicator).getRepresenteFileHistory().size()>0){
			for(int i=0;i<lstLegalRepresentativeHistory.get(indicator).getRepresenteFileHistory().size();i++){
				RepresentativeFileHistory representativeFileHistory = new RepresentativeFileHistory();
				
				representativeFileHistory.setIdRepresentFileHisPk(lstLegalRepresentativeHistory.get(indicator).getRepresenteFileHistory().get(i).getIdRepresentFileHisPk());
				
				representativeFileHistory.setDescription(lstLegalRepresentativeHistory.get(indicator).getRepresenteFileHistory().get(i).getDescription());
				
				representativeFileHistory.setDocumentType(lstLegalRepresentativeHistory.get(indicator).getRepresenteFileHistory().get(i).getDocumentType());
				
				representativeFileHistory.setFilename(lstLegalRepresentativeHistory.get(indicator).getRepresenteFileHistory().get(i).getFilename());				
				
				representativeFileHistory.setLegalRepresentativeHistory(lstLegalRepresentativeHistory.get(indicator).getRepresenteFileHistory().get(i).getLegalRepresentativeHistory());
				
				if(lstLegalRepresentativeHistory.get(indicator).getRepresenteFileHistory().get(i).isFlagByLegalRepresentative()){
					representativeFileHistory.setFlagByLegalRepresentative(true);
				}
				
				lstRepresentativeFileHistory.add(representativeFileHistory);
				
			}
		} else {
			if(legalRepresentativeTO.isFlagIssuerOrParticipant()){
				lstRepresentativeFileHistory = listRepreFileHistoryWithLegalRepreFiles(lstLegalRepresentativeHistory.get(indicator).getIdRepresentativeHistoryPk());
				lstLegalRepresentativeHistory.get(indicator).setRepresenteFileHistory(lstRepresentativeFileHistory);
			}
		}
		
		
		if (legalRepresentativeHistory.getPersonType().equals(
				PersonType.NATURAL.getCode())) {
			legalRepresentativeTO.setFlagRepresentativeNaturalPerson(true);
			legalRepresentativeTO.setFlagRepresentativeJuridicPerson(false);
		} else if (legalRepresentativeHistory.getPersonType().equals(
				PersonType.JURIDIC.getCode())) {
			legalRepresentativeTO.setFlagRepresentativeNaturalPerson(false);
			legalRepresentativeTO.setFlagRepresentativeJuridicPerson(true);
		}
		
		documentTO = documentValidator.loadMaxLenghtType(legalRepresentativeHistory.getDocumentType());
	
		legalRepresentativeTO.setNumericTypeDocumentLegal(documentTO.isNumericTypeDocument());
		legalRepresentativeTO.setMaxLenghtDocumentNumberLegal(documentTO.getMaxLenghtDocumentNumber());
		
		documentTO = documentValidator.loadMaxLenghtType(legalRepresentativeHistory.getSecondDocumentType());
		
		legalRepresentativeTO.setNumericSecondTypeDocumentLegal(documentTO.isNumericTypeDocument());
		legalRepresentativeTO.setMaxLenghtSecondDocumentNumberLegal(documentTO.getMaxLenghtDocumentNumber());
	
		if(legalRepresentativeHistory.getIndPEP()!=null 
				&& legalRepresentativeHistory.getIndPEP().equals(BooleanType.YES.getCode())){
			legalRepresentativeTO.setFlagRepresentativeIsPEP(true);
		}
		else{
			legalRepresentativeTO.setFlagRepresentativeIsPEP(false);
		}
		
		getLstGeographicLocation();
			
		if(indicatorHolder)
		{
			listDepartmentLocationRepresentative = getLstDepartmentLocation(legalRepresentativeHistoryDetail.getLegalResidenceCountry());
			listProvinceLocationRepresentative = getLstProvinceLocation(legalRepresentativeHistoryDetail.getLegalDepartment());
			listMunicipalityLocationRepresentative = getLstMunicipalityLocation(legalRepresentativeHistoryDetail.getLegalProvince());


			listDepartmentPostalLocationRepresentative = getLstDepartmentLocation(legalRepresentativeHistoryDetail.getPostalResidenceCountry());
			listProvincePostalLocationRepresentative = getLstProvinceLocation(legalRepresentativeHistoryDetail.getPostalDepartment());
			listMunicipalityPostalLocationRepresentative = getLstMunicipalityLocation(legalRepresentativeHistoryDetail.getPostalProvince());
		}
		else
		{
			listDepartmentLocationRepresentative = getLstDepartmentLocation(legalRepresentativeHistory.getLegalResidenceCountry());
			listProvinceLocationRepresentative = getLstProvinceLocation(legalRepresentativeHistory.getLegalDepartment());
			listMunicipalityLocationRepresentative = getLstMunicipalityLocation(legalRepresentativeHistory.getLegalProvince());


			listDepartmentPostalLocationRepresentative = getLstDepartmentLocation(legalRepresentativeHistory.getPostalResidenceCountry());
			listProvincePostalLocationRepresentative = getLstProvinceLocation(legalRepresentativeHistory.getPostalDepartment());
			listMunicipalityPostalLocationRepresentative = getLstMunicipalityLocation(legalRepresentativeHistory.getPostalProvince());
		}

		loadDocumentTypeRepresentativeParameter();
		loadRepresentativeDocumentType();
		validateRepresentativeSecondDocumentTypeList();

		JSFUtilities.executeJavascriptFunction("PF('registerRepresentativeWidget').show()");
	}
	
	/**
	 * Validate representative second document type list.
	 */
	public void validateRepresentativeSecondDocumentTypeList(){
		
		PersonTO person = new PersonTO();
		person.setPersonType(legalRepresentativeHistory.getPersonType());
		person.setFlagLegalRepresentativeIndicator(true);
		
		if(legalRepresentativeHistory.getSecondNationality()!=null && legalRepresentativeHistory.getIndResidence()!=null){
			
			if ((legalRepresentativeHistory.getSecondNationality().equals(countryResidence) 
					&& legalRepresentativeHistory.getIndResidence().equals(BooleanType.YES.getCode()))){ 
					
				person.setFlagMinorIndicator(false);
				person.setFlagInabilityIndicator(false);
			}
			else if(!legalRepresentativeHistory.getSecondNationality().equals(countryResidence) 
					&& legalRepresentativeHistory.getIndResidence().equals(BooleanType.YES.getCode())){
				person.setFlagMinorIndicator(false);
				person.setFlagInabilityIndicator(false);
				person.setFlagNationalNotResident(true);
			}
		}
		
		person.setFlagForeignResident(false);
		person.setFlagForeignNotResident(true);
		person.setFlagNationalNotResident(false);
		person.setFlagNationalResident(false);
		
		listSecondDocumentTypeLegal = documentValidator.getDocumentTypeByPerson(person);
	
		
	}
	
	/**
	 * Clean all representative.
	 */
	public void cleanAllRepresentative() {
		JSFUtilities.resetViewRoot();
		legalRepresentativeHistory = new LegalRepresentativeHistory();
		legalRepresentativeHistoryDetail = new LegalRepresentativeHistory();
		newDocumentNumber = GeneralConstants.EMPTY_STRING;
		newDocumentNumberSecondNationality = GeneralConstants.EMPTY_STRING;
		
		Integer viewOperationTypeBefore = null;
		if(legalRepresentativeTO!=null) {
			viewOperationTypeBefore = legalRepresentativeTO.getViewOperationType();
		}
		
		Integer holderPersonType = null;
		Integer holderJuridicClassType = null;
		HolderRequest holderRequest = null;
		
		if(indicatorHolder){
			if(legalRepresentativeTO.getHolderPersonType()!=null){
				holderPersonType = legalRepresentativeTO.getHolderPersonType();
			}
		
			if(legalRepresentativeTO.getHolderJuridicClassType()!=null){
				holderJuridicClassType = legalRepresentativeTO.getHolderJuridicClassType();
			}
		
			if(legalRepresentativeTO.getHolderRequest()!=null){
				holderRequest = legalRepresentativeTO.getHolderRequest();
			}		
		}
		
		legalRepresentativeTO = new LegalRepresentativeTO();
		
		if(indicatorHolder){
			if(holderPersonType!=null){
				legalRepresentativeTO.setHolderPersonType(holderPersonType);
			}
			if(holderJuridicClassType!=null){
				legalRepresentativeTO.setHolderJuridicClassType(holderJuridicClassType);
			}
			
			if(holderRequest!=null){
				legalRepresentativeTO.setHolderRequest(holderRequest);
			}
		}
		
		lstRepresentativeFileHistory = null;
		listRepresentativeDocumentType = null;
		
		
			if(indicatorIssuer){
				legalRepresentativeTO.setDisabledPersonTypeRepresentative(true);
				legalRepresentativeTO.setFlagCmbRepresentativeClass(true);
				legalRepresentativeHistory.setPersonType(PersonType.NATURAL.getCode());
				legalRepresentativeHistory.setRepresentativeClass(RepresentativeClassType.LEGAL.getCode());
				
				legalRepresentativeTO.setDisabledCmbPostalCountryRepresentative(false);
				legalRepresentativeTO.setDisabledCmbPostalProvinceRepresentative(false);
				legalRepresentativeTO.setDisabledCmbPostalDepartamentRepresentative(false);
				legalRepresentativeTO.setDisabledInputPostalAddressRepresentative(false);		
				legalRepresentativeTO.setDisabledRepresentativeNationality(false);
				legalRepresentativeTO.setDisabledRepresentativeResident(false);
				legalRepresentativeTO.setDisabledCountryResidentRepresentative(false);
				legalRepresentativeTO.setFlagCopyLegalRepresentative(false);

				listRepresentativeClass = new ArrayList<ParameterTable>();
				getLstRepresentativeClassType();
				
				legalRepresentativeHistory.setBeginningPeriod(null);
				legalRepresentativeHistory.setEndingPeriod(null);
				legalRepresentativeTO.setFlagRepresentativeIsPEP(false);

				legalRepresentativeTO.setFlagRepresentativeNaturalPerson(true);
				legalRepresentativeTO.setFlagRepresentativeJuridicPerson(false);

				legalRepresentativeTO.setDisabledRepresentativeNationality(false);
				legalRepresentativeTO.setDisabledRepresentativeResident(false);
				legalRepresentativeTO.setDisabledCountryResidentRepresentative(false);
				legalRepresentativeTO.setFlagAddRepresentativeModify(false);	
				legalRepresentativeTO.setViewOperationType(viewOperationTypeBefore);
				
				legalRepresentativeHistory.setFlagActiveFilesByHolderImport(true);
			
			}
		
	}
	
	/**
	 * Load by representative type person.
	 */
	public void loadByRepresentativeTypePerson() {
		
		
		if(legalRepresentativeHistory.getRepresentativeClass()!=null && legalRepresentativeHistory.getRepresentativeClass().equals(RepresentativeClassType.FIDUCIARY.getCode())){
			legalRepresentativeHistory.setNationality(countryResidence);
			legalRepresentativeHistory.setIndResidence(BooleanType.YES.getCode());
			legalRepresentativeHistory.setLegalResidenceCountry(countryResidence);
			legalRepresentativeTO.setDisabledRepresentativeNationality(true);
			legalRepresentativeTO.setDisabledRepresentativeResident(true);
			legalRepresentativeTO.setDisabledCountryResidentRepresentative(true);
			validateResidentRepresentative();
			validateCountryResidentLegalRepresentative();
		}else{
			legalRepresentativeTO.setDisabledRepresentativeNationality(false);
			legalRepresentativeTO.setDisabledRepresentativeResident(false);
			legalRepresentativeTO.setDisabledCountryResidentRepresentative(false);
		}
		
		loadingDocumentAttachByRepresentantive();
	}
	
	/**
	 * Validate attach representative file.
	 *
	 * @return true, if successful
	 */
	public boolean validateAttachRepresentativeFile(){
		
		if(indicatorHolder){
		boolean findCI=false;
		boolean findPASS=false;
		boolean findDIO=false;
			
		if (legalRepresentativeHistory.getPersonType()!=null && legalRepresentativeHistory.getPersonType().equals(PersonType.NATURAL.getCode())) {
			if (legalRepresentativeTO.getHolderPersonType()!=null 
					&& legalRepresentativeTO.getHolderPersonType().equals(PersonType.NATURAL.getCode()) 
					|| legalRepresentativeTO.getHolderPersonType().equals(PersonType.JURIDIC.getCode())) {
				
				if (!legalRepresentativeTO.isFlagIndicatorMinorHolder() && !legalRepresentativeTO.isFlagHolderIndicatorInability()) {
				
					for(int i=0;i<lstRepresentativeFileHistory.size();i++){
						if(lstRepresentativeFileHistory.get(i).getDocumentType().equals(AgeMajorDocRepreNatType.PASS.getCode())){
						   findPASS=true;	
						}
						if(lstRepresentativeFileHistory.get(i).getDocumentType().equals(AgeMajorDocRepreNatType.CI.getCode())){
							findCI=true;
						}
					}
					
					if(!(findPASS || findCI)){
						JSFUtilities.showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getValidationMessage(GeneralPropertiesConstants.WARNING_FILE_REQUEST_REPRESENTATIVE_NO_COMPLETE,null));
						JSFUtilities.showSimpleValidationDialog();
						return false;
					}
					
					findCI=findPASS=findDIO=false;
					
					for(int i=0;i<lstRepresentativeFileHistory.size();i++){
						if(lstRepresentativeFileHistory.get(i).getDocumentType().equals(AgeMajorDocRepreNatType.DIO.getCode())){
						   findDIO=true;	
						}
						
						if(lstRepresentativeFileHistory.get(i).getDocumentType().equals(AgeMajorDocRepreNatType.CI.getCode())){
							findCI=true;
						}
					}
					
					if(!(findDIO || findCI)){
						JSFUtilities.showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getValidationMessage(GeneralPropertiesConstants.WARNING_FILE_REQUEST_REPRESENTATIVE_NO_COMPLETE,null));
						JSFUtilities.showSimpleValidationDialog();
						return false;
	
					}
					
				} else if (legalRepresentativeTO.isFlagIndicatorMinorHolder() || legalRepresentativeTO.isFlagHolderIndicatorInability()) {

					if (legalRepresentativeHistory.getRepresentativeClass()!=null 
							&& legalRepresentativeHistory.getRepresentativeClass().equals(RepresentativeClassType.FATHER.getCode())) {
						
						for(int i=0;i<lstRepresentativeFileHistory.size();i++){
							if(lstRepresentativeFileHistory.get(i).getDocumentType().equals(AgeLessFatherDocRepreNatType.PASS.getCode())){
							   findPASS=true;	
							}
							if(lstRepresentativeFileHistory.get(i).getDocumentType().equals(AgeLessFatherDocRepreNatType.CI.getCode())){
								findCI=true;
							}
						}
						
						if(!(findPASS || findCI)){
							JSFUtilities.showMessageOnDialog(
									PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
									PropertiesUtilities.getValidationMessage(GeneralPropertiesConstants.WARNING_FILE_REQUEST_REPRESENTATIVE_NO_COMPLETE,null));
							JSFUtilities.showSimpleValidationDialog();
							return false;
						}
						
						findCI=findPASS=findDIO=false;
						
						for(int i=0;i<lstRepresentativeFileHistory.size();i++){
							if(lstRepresentativeFileHistory.get(i).getDocumentType().equals(AgeLessFatherDocRepreNatType.DIO.getCode())){
							   findDIO=true;	
							}
							
							if(lstRepresentativeFileHistory.get(i).getDocumentType().equals(AgeLessFatherDocRepreNatType.CI.getCode())){
								findCI=true;
							}
						}
						
						if(!(findDIO || findCI)){
							JSFUtilities.showMessageOnDialog(
									PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
									PropertiesUtilities.getValidationMessage(GeneralPropertiesConstants.WARNING_FILE_REQUEST_REPRESENTATIVE_NO_COMPLETE,null));
							JSFUtilities.showSimpleValidationDialog();
							return false;
						}
					}

					if (legalRepresentativeHistory.getRepresentativeClass()!=null 
							&& legalRepresentativeHistory.getRepresentativeClass().equals(RepresentativeClassType.TUTOR_LEGAL.getCode())) {
						
						for(int i=0;i<lstRepresentativeFileHistory.size();i++){
							if(lstRepresentativeFileHistory.get(i).getDocumentType().equals(AgeLessTutorDocRepreNatType.PASS.getCode())){
							   findPASS=true;	
							}
							if(lstRepresentativeFileHistory.get(i).getDocumentType().equals(AgeLessTutorDocRepreNatType.CI.getCode())){
								findCI=true;
							}
						}
						
						if(!(findPASS || findCI)){
							JSFUtilities.showMessageOnDialog(
									PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
									PropertiesUtilities.getValidationMessage(GeneralPropertiesConstants.WARNING_FILE_REQUEST_REPRESENTATIVE_NO_COMPLETE,null));
							JSFUtilities.showSimpleValidationDialog();
							return false;
						}
						
						findCI=findPASS=findDIO=false;
						
						for(int i=0;i<lstRepresentativeFileHistory.size();i++){
							if(lstRepresentativeFileHistory.get(i).getDocumentType().equals(AgeLessTutorDocRepreNatType.DIO.getCode())){
							   findDIO=true;	
							}
							if(lstRepresentativeFileHistory.get(i).getDocumentType().equals(AgeLessTutorDocRepreNatType.CI.getCode())){
								findCI=true;
							}
						}
						
						if(!(findDIO || findCI)){
							JSFUtilities.showMessageOnDialog(
									PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
									PropertiesUtilities.getValidationMessage(GeneralPropertiesConstants.WARNING_FILE_REQUEST_REPRESENTATIVE_NO_COMPLETE,null));
							JSFUtilities.showSimpleValidationDialog();
							return false;
						}
					}
					
					if(legalRepresentativeHistory.getRepresentativeClass()!=null 
							&& legalRepresentativeHistory.getRepresentativeClass().equals(RepresentativeClassType.LEGAL.getCode())){
						listRepresentativeDocumentType = new ArrayList<ParameterTable>();
						getLstDocumentTypeAttachParameter(MasterTableType.AGELESS_INABILITY_LEGAL_DOC_REPRE_NAT.getCode());
					}
				} 
			} 
		} else 
			if (legalRepresentativeHistory.getPersonType()!=null 
				&& legalRepresentativeHistory.getPersonType().equals(PersonType.JURIDIC.getCode())) {
				
				if (legalRepresentativeTO.getHolderPersonType()!=null && legalRepresentativeTO.getHolderPersonType().equals(PersonType.NATURAL.getCode())) {
					if (!legalRepresentativeTO.isFlagIndicatorMinorHolder() || legalRepresentativeTO.isFlagHolderIndicatorInability()) {
						listRepresentativeDocumentType = new ArrayList<ParameterTable>();
						getLstDocumentTypeAttachParameter(MasterTableType.AGEMAJOR_INABILITY_DOC_REPRE_JUR.getCode());
						
					}else if (legalRepresentativeTO.isFlagIndicatorMinorHolder()) {
							listRepresentativeDocumentType = new ArrayList<ParameterTable>();
							getLstDocumentTypeAttachParameter(MasterTableType.AGELESS_DOC_REPRE_JUR.getCode());
					}
				
				} else if (legalRepresentativeTO.getHolderPersonType()!=null 
						&& legalRepresentativeTO.getHolderPersonType().equals(PersonType.JURIDIC.getCode())) {
					listRepresentativeDocumentType = new ArrayList<ParameterTable>();
					getLstDocumentTypeAttachParameter(MasterTableType.PERSON_JUR_DOC_REPRE_JUR.getCode());
				}
			}
		}
		return true;
	}
	
	/**
	 * Loading document attach by representantive.
	 */
	public void loadingDocumentAttachByRepresentantive() {		
		lstRepresentativeFileHistory = new ArrayList<RepresentativeFileHistory>();		
		loadDocumentTypeRepresentativeParameter();
	}
	
	/**
	 * Load document type representative parameter.
	 */
	public void loadDocumentTypeRepresentativeParameter(){
		
		if(indicatorIssuer){
			listRepresentativeDocumentType = new ArrayList<ParameterTable>();
			Integer indicator = 0;			
			if(legalRepresentativeHistory.getIndResidence()==null);
			else
			{
				getLstDocumentTypeAttachParameter(MasterTableType.NATURAL_DOC_REPRE_NAT.getCode());
				if(legalRepresentativeHistory.getIndResidence()!=null && legalRepresentativeHistory.getIndResidence().equals(BooleanType.YES.getCode())){
					for(ParameterTable param  : listRepresentativeDocumentType){
					    if(param.getParameterTablePk().equals(RepresentativeFileType.PASSPORT.getCode())){
					    	indicator = listRepresentativeDocumentType.indexOf(param);
					    }
					}				
					listRepresentativeDocumentType.remove(indicator.intValue());
				}
			}			
		}
		else if(indicatorHolder){
		if (legalRepresentativeHistory.getPersonType()!=null && legalRepresentativeHistory.getPersonType().equals(
				PersonType.NATURAL.getCode())) {
			if (legalRepresentativeTO.getHolderPersonType()!=null && legalRepresentativeTO.getHolderPersonType().equals(
					PersonType.NATURAL.getCode()) || legalRepresentativeTO.getHolderPersonType()!=null && legalRepresentativeTO.getHolderPersonType().equals(
							PersonType.JURIDIC.getCode())) {
				
				if (!legalRepresentativeTO.isFlagIndicatorMinorHolder() && !legalRepresentativeTO.isFlagHolderIndicatorInability()) {
				
					listRepresentativeDocumentType = new ArrayList<ParameterTable>();
					Integer indicator = 0;				
					
					getLstDocumentTypeAttachParameter(MasterTableType.AGEMAJOR_DOC_REPRE_NAT.getCode());
					for(ParameterTable param  : listRepresentativeDocumentType){
					    if(param.getParameterTablePk().equals(RepresentativeFileType.PHOTOCOCOPYREPRESENTATIVETESTIMONY.getCode())){
					    	indicator = listRepresentativeDocumentType.indexOf(param);
					    } 
					}
					listRepresentativeDocumentType.remove(indicator.intValue());
					for(ParameterTable param  : listRepresentativeDocumentType){
					    if(param.getParameterTablePk().equals(RepresentativeFileType.PHOTOCOCOPYPASSPORT.getCode())){
					    	indicator = listRepresentativeDocumentType.indexOf(param);
					    } 
					}
					listRepresentativeDocumentType.remove(indicator.intValue());
					for(ParameterTable param  : listRepresentativeDocumentType){
					    if(param.getParameterTablePk().equals(RepresentativeFileType.PHOTOCOCOPYIDENTITYDOCUMENTCOUNTRYORIGIN.getCode())){
					    	indicator = listRepresentativeDocumentType.indexOf(param);
					    } 
					}
					listRepresentativeDocumentType.remove(indicator.intValue());
					for(ParameterTable param  : listRepresentativeDocumentType){
					    if(param.getParameterTablePk().equals(RepresentativeFileType.PEPAFFIDAVIT.getCode())){
					    	indicator = listRepresentativeDocumentType.indexOf(param);
					    } 
					}
					listRepresentativeDocumentType.remove(indicator.intValue());
					
				} else if (legalRepresentativeTO.isFlagIndicatorMinorHolder() || legalRepresentativeTO.isFlagHolderIndicatorInability()) {

					if (legalRepresentativeHistory.getRepresentativeClass()!=null && legalRepresentativeHistory.getRepresentativeClass()
							.equals(RepresentativeClassType.FATHER.getCode())) {
						
						listRepresentativeDocumentType = new ArrayList<ParameterTable>();
						Integer indicator = 0;
						getLstDocumentTypeAttachParameter(MasterTableType.AGELESS_INABILITY_FATHER_DOC_REPRE_NAT.getCode());
						for(ParameterTable param  : listRepresentativeDocumentType){
						    if(param.getParameterTablePk().equals(RepresentativeFileType.PHOTOCOCOPYPASSPORTDAD.getCode())){
						    	indicator = listRepresentativeDocumentType.indexOf(param);
						    } 
						}
						listRepresentativeDocumentType.remove(indicator.intValue());
						for(ParameterTable param  : listRepresentativeDocumentType){
						    if(param.getParameterTablePk().equals(RepresentativeFileType.PHOTOCOCOPYIDENTITYDOCUMENTCOUNTRYORIGINDAD.getCode())){
						    	indicator = listRepresentativeDocumentType.indexOf(param);
						    } 
						}
						listRepresentativeDocumentType.remove(indicator.intValue());
						for(ParameterTable param  : listRepresentativeDocumentType){
						    if(param.getParameterTablePk().equals(RepresentativeFileType.PEPAFFIDAVITDAD.getCode())){
						    	indicator = listRepresentativeDocumentType.indexOf(param);
						    } 
						}
						listRepresentativeDocumentType.remove(indicator.intValue());
						for(ParameterTable param  : listRepresentativeDocumentType){
						    if(param.getParameterTablePk().equals(RepresentativeFileType.BIRTHCERTIFICATEDAD.getCode())){
						    	indicator = listRepresentativeDocumentType.indexOf(param);
						    } 
						}
						listRepresentativeDocumentType.remove(indicator.intValue());
					}

					if (legalRepresentativeHistory.getRepresentativeClass()!=null && legalRepresentativeHistory.getRepresentativeClass()
							.equals(RepresentativeClassType.TUTOR_LEGAL
									.getCode())) {
						
						listRepresentativeDocumentType = new ArrayList<ParameterTable>();
						Integer indicator = 0;
						getLstDocumentTypeAttachParameter(MasterTableType.AGELESS_INABILITY_TUTOR_DOC_REPRE_NAT.getCode());
						for(ParameterTable param  : listRepresentativeDocumentType){
						    if(param.getParameterTablePk().equals(RepresentativeFileType.PHOTOCOCOPYPASSPORTREPRESENTATIVE.getCode())){
						    	indicator = listRepresentativeDocumentType.indexOf(param);
						    } 
						}
						listRepresentativeDocumentType.remove(indicator.intValue());
						for(ParameterTable param  : listRepresentativeDocumentType){
						    if(param.getParameterTablePk().equals(RepresentativeFileType.PHOTOCOCOPYIDENTITYDOCUMENTCOUNTRYORIGINREPRESENTATIVE.getCode())){
						    	indicator = listRepresentativeDocumentType.indexOf(param);
						    } 
						}
						listRepresentativeDocumentType.remove(indicator.intValue());
						for(ParameterTable param  : listRepresentativeDocumentType){
						    if(param.getParameterTablePk().equals(RepresentativeFileType.PEPAFFIDAVITREPRESENTATIVE.getCode())){
						    	indicator = listRepresentativeDocumentType.indexOf(param);
						    } 
						}
						listRepresentativeDocumentType.remove(indicator.intValue());
						for(ParameterTable param  : listRepresentativeDocumentType){
						    if(param.getParameterTablePk().equals(RepresentativeFileType.DESIGNATIONOFAUTHORITYREPRESENTATIVE.getCode())){
						    	indicator = listRepresentativeDocumentType.indexOf(param);
						    } 
						}
						listRepresentativeDocumentType.remove(indicator.intValue());
						
						
					}
					
					if(legalRepresentativeHistory.getRepresentativeClass()!=null && legalRepresentativeHistory.getRepresentativeClass()
							.equals(RepresentativeClassType.LEGAL.getCode())){
						
						listRepresentativeDocumentType = new ArrayList<ParameterTable>();
						getLstDocumentTypeAttachParameter(MasterTableType.AGELESS_INABILITY_LEGAL_DOC_REPRE_NAT.getCode());
						
					}
				} 
			} 
		} else if (legalRepresentativeHistory.getPersonType()!=null && legalRepresentativeHistory.getPersonType().equals(
				PersonType.JURIDIC.getCode())) {
			if (legalRepresentativeTO.getHolderPersonType()!=null && legalRepresentativeTO.getHolderPersonType().equals(
					PersonType.NATURAL.getCode())) {
				if (!legalRepresentativeTO.isFlagIndicatorMinorHolder() || legalRepresentativeTO.isFlagHolderIndicatorInability()) {
					listRepresentativeDocumentType = new ArrayList<ParameterTable>();
					getLstDocumentTypeAttachParameter(MasterTableType.AGEMAJOR_INABILITY_DOC_REPRE_JUR.getCode());
					
				} else if (legalRepresentativeTO.isFlagIndicatorMinorHolder()) {
						listRepresentativeDocumentType = new ArrayList<ParameterTable>();
						getLstDocumentTypeAttachParameter(MasterTableType.AGELESS_DOC_REPRE_JUR.getCode());
				}
				
			} else if (legalRepresentativeTO.getHolderPersonType()!=null && legalRepresentativeTO.getHolderPersonType().equals(
					PersonType.JURIDIC.getCode())) {

					listRepresentativeDocumentType = new ArrayList<ParameterTable>();
					getLstDocumentTypeAttachParameter(MasterTableType.PERSON_JUR_DOC_REPRE_JUR.getCode());
			}
		}
	   }
		
	}
	
	/**
	 * Gets the lst document type attach parameter.
	 *
	 * @param masterTableTypeCode the master table type code
	 * @return the lst document type attach parameter
	 */
	public List<ParameterTable> getLstDocumentTypeAttachParameter(Integer masterTableTypeCode){
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(masterTableTypeCode);
			listRepresentativeDocumentType  =  generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
   		
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		
		return listRepresentativeDocumentType;
		
	}
	
	/**
	 * Validate country resident legal representative.
	 */
	public void validateCountryResidentLegalRepresentative() {
		legalRepresentativeTO.setFlagCopyLegalRepresentative(false);
		legalRepresentativeTO.setDisabledCmbPostalCountryRepresentative(false);
		legalRepresentativeTO.setDisabledCmbPostalProvinceRepresentative(false);
		legalRepresentativeTO.setDisabledCmbPostalDepartamentRepresentative(false);
		legalRepresentativeTO.setDisabledInputPostalAddressRepresentative(false);
		
		if (legalRepresentativeHistory.getLegalResidenceCountry()!=null && legalRepresentativeHistory.getLegalResidenceCountry().equals(
				countryResidence)) {
			legalRepresentativeTO.setDisabledCountryResidentRepresentative(true);
			legalRepresentativeHistory.setIndResidence(BooleanType.YES.getCode());
			loadRepresentativeDocumentType();
			 
				legalRepresentativeHistory.setLegalDepartment(null);
			 	legalRepresentativeHistory.setLegalProvince(null);
				legalRepresentativeHistory.setLegalDistrict(null);
				legalRepresentativeHistory.setLegalAddress(null);
				
				listMunicipalityLocationRepresentative=null;
				
				legalRepresentativeHistory.setPostalResidenceCountry(null);		
				legalRepresentativeHistory.setPostalDepartment(null);
				legalRepresentativeHistory.setPostalProvince(null);
				legalRepresentativeHistory.setPostalDistrict(null);
				legalRepresentativeHistory.setPostalAddress(null);
				
				listMunicipalityPostalLocationRepresentative=null;
			
		} else {
			
			legalRepresentativeTO.setDisabledCountryResidentRepresentative(false);
			legalRepresentativeHistory.setIndResidence(BooleanType.NO.getCode());			
			listProvinceLocationRepresentative = getLstProvinceLocation(legalRepresentativeHistory.getLegalResidenceCountry());
			loadRepresentativeDocumentType();
			legalRepresentativeHistory.setLegalDepartment(null);
		    legalRepresentativeHistory.setLegalProvince(null);
			legalRepresentativeHistory.setLegalDistrict(null);
			legalRepresentativeHistory.setLegalAddress(null);
			listMunicipalityLocationRepresentative=null;						
			legalRepresentativeHistory.setPostalResidenceCountry(null);
			legalRepresentativeHistory.setPostalDepartment(null);
			legalRepresentativeHistory.setPostalProvince(null);
			legalRepresentativeHistory.setPostalDistrict(null);
			legalRepresentativeHistory.setPostalAddress(null);
			listMunicipalityPostalLocationRepresentative=null;
		}
		
		loadDepartamentLegalRepresentative();
		
	}

	/**
	 * Load representative document type.
	 */
	public void loadRepresentativeDocumentType() {

		PersonTO person = new PersonTO();
		person.setPersonType(legalRepresentativeHistory.getPersonType());
		person.setFlagLegalRepresentativeIndicator(true);
		
		if(legalRepresentativeHistory.getNationality()!=null && legalRepresentativeHistory.getIndResidence()!=null){
		
			if ((legalRepresentativeHistory.getNationality().equals(countryResidence) 
				&& legalRepresentativeHistory.getIndResidence().equals(BooleanType.YES.getCode()))){ 
				
				person.setFlagNationalResident(true);
			}
			else if(!legalRepresentativeHistory.getNationality().equals(countryResidence) 
					&& legalRepresentativeHistory.getIndResidence().equals(BooleanType.YES.getCode())){
				person.setFlagForeignResident(true);
				
			}
			else if (legalRepresentativeHistory.getNationality().equals(countryResidence)
					&& legalRepresentativeHistory.getIndResidence().equals(BooleanType.NO.getCode())) {
				person.setFlagNationalNotResident(true);
			}
		
			else if (!legalRepresentativeHistory.getNationality().equals(countryResidence)
					&& legalRepresentativeHistory.getIndResidence().equals(BooleanType.NO.getCode())) {
				person.setFlagForeignNotResident(true);
			}
		}
		
		listDocumentTypeLegalResult = new ArrayList<ParameterTable>();
		
		listDocumentTypeLegalResult = documentValidator.getDocumentTypeByPerson(person);
		
		if(legalRepresentativeHistory.getNationality()!=null && 
				!legalRepresentativeHistory.getNationality().equals(countryResidence) && 
				legalRepresentativeHistory.getIndResidence()!=null &&
				legalRepresentativeHistory.getIndResidence().equals(BooleanType.YES.getCode())){
			
			for(ParameterTable parameter : listDocumentTypeLegalResult){
			    if(parameter.getParameterTablePk().equals(DocumentType.CI.getCode())){
			    	legalRepresentativeHistory.setDocumentType(parameter.getParameterTablePk());
			    }
			}
			
			documentTO = documentValidator.loadMaxLenghtType(legalRepresentativeHistory.getDocumentType());
			
			legalRepresentativeTO.setNumericTypeDocumentLegal(documentTO.isNumericTypeDocument());
			legalRepresentativeTO.setMaxLenghtDocumentNumberLegal(documentTO.getMaxLenghtDocumentNumber());
			
			documentTO = documentValidator.validateEmissionRules(legalRepresentativeHistory.getDocumentType());
			getLstDocumentSource();				
		}
		else{
			legalRepresentativeHistory.setDocumentSource(null);
			listDocumentSource = new ArrayList<ParameterTable>();
		}
		
	}
	
	/**
	 * Validate resident representative.
	 */
	public void validateResidentRepresentative() {
		
		legalRepresentativeTO.setFlagCopyLegalRepresentative(false);		
		legalRepresentativeTO.setDisabledCmbPostalCountryRepresentative(false);
		legalRepresentativeTO.setDisabledCmbPostalProvinceRepresentative(false);
		legalRepresentativeTO.setDisabledCmbPostalDepartamentRepresentative(false);
		legalRepresentativeTO.setDisabledInputPostalAddressRepresentative(false);
		legalRepresentativeTO.setDisabledRepresentativeResident(false);
		legalRepresentativeTO.setDisabledCountryResidentRepresentative(false);
		
		
		if(legalRepresentativeHistory.getPersonType()!=null && legalRepresentativeHistory.getPersonType().equals(PersonType.JURIDIC.getCode())
			&& legalRepresentativeHistory.getNationality()!=null && legalRepresentativeHistory.getNationality().equals(countryResidence)){
			legalRepresentativeHistory.setIndResidence(BooleanType.YES.getCode());
			
			legalRepresentativeHistory.setLegalResidenceCountry(countryResidence);
			listDepartmentLocationRepresentative = getLstDepartmentLocation(legalRepresentativeHistory.getLegalResidenceCountry());
			legalRepresentativeTO.setDisabledRepresentativeResident(true);
			legalRepresentativeTO.setDisabledCountryResidentRepresentative(true);
			
		}
		else if(legalRepresentativeHistory.getPersonType()!=null && legalRepresentativeHistory.getPersonType().equals(PersonType.JURIDIC.getCode())
			   && legalRepresentativeHistory.getNationality()!=null && !legalRepresentativeHistory.getNationality().equals(countryResidence)){
			legalRepresentativeHistory.setIndResidence(BooleanType.NO.getCode());
			legalRepresentativeTO.setDisabledRepresentativeResident(true);
			listDepartmentLocationRepresentative = getLstDepartmentLocation(legalRepresentativeHistory.getLegalResidenceCountry());
		}
		
		if (BooleanType.YES.getCode().equals(
				legalRepresentativeHistory.getIndResidence())) {
			
			if(legalRepresentativeHistory.getNationality() != countryResidence) {
				legalRepresentativeTO.setDisabledCountryResidentRepresentative(false);
			}else {
				legalRepresentativeTO.setDisabledCountryResidentRepresentative(true);
			}
			
			legalRepresentativeHistory.setLegalResidenceCountry(countryResidence);
			
			SelectOneMenu cmbCR = (SelectOneMenu)	FacesContext.getCurrentInstance().getViewRoot().findComponent(idLegalRepresentativeComponent+":tabView:"+"cmbCountryResident");
            if(cmbCR!=null){cmbCR.setValid(true);}
			
			
			listDepartmentLocationRepresentative = getLstDepartmentLocation(legalRepresentativeHistory
					.getLegalResidenceCountry());
			
			SelectOneMenu cmbLC = (SelectOneMenu)	FacesContext.getCurrentInstance().getViewRoot().findComponent(idLegalRepresentativeComponent+":tabView:"+"cmbLegalCountry");
			if(cmbLC!=null){cmbLC.setValid(true);}
			
			legalRepresentativeHistory.setLegalDepartment(null);
			legalRepresentativeHistory.setLegalProvince(null);
			legalRepresentativeHistory.setLegalDistrict(null);
			legalRepresentativeHistory.setLegalAddress(null);
			
			listMunicipalityLocationRepresentative=null;
			
			legalRepresentativeHistory.setPostalResidenceCountry(null);	
			legalRepresentativeHistory.setPostalDepartment(null);
			legalRepresentativeHistory.setPostalProvince(null);
			legalRepresentativeHistory.setPostalDistrict(null);
			legalRepresentativeHistory.setPostalAddress(null);
			
			listMunicipalityPostalLocationRepresentative=null;
					
		} else if (BooleanType.NO.getCode().equals(
			legalRepresentativeHistory.getIndResidence())) {
			legalRepresentativeTO.setDisabledCountryResidentRepresentative(false);
			
			legalRepresentativeHistory.setDocumentType(null);
			
			legalRepresentativeHistory.setLegalResidenceCountry(null);
			legalRepresentativeHistory.setLegalDepartment(null);
			legalRepresentativeHistory.setLegalProvince(null);
			legalRepresentativeHistory.setLegalDistrict(null);
			legalRepresentativeHistory.setLegalAddress(null);
			
			listProvinceLocationRepresentative=null;
			listMunicipalityLocationRepresentative=null;
			
			legalRepresentativeHistory.setPostalResidenceCountry(null);
		    legalRepresentativeHistory.setPostalDepartment(null);
			legalRepresentativeHistory.setPostalProvince(null);
			legalRepresentativeHistory.setPostalDistrict(null);
			legalRepresentativeHistory.setPostalAddress(null);
			
			listProvincePostalLocationRepresentative=null;
			listMunicipalityPostalLocationRepresentative=null;

		} 

		loadRepresentativeDocumentType();
		validateSecondNationalityByIndResidentRepresentative();
		loadingDocumentAttachByRepresentantive();
	}
	
	/**
	 * Validate second nationality by ind resident representative.
	 */
	private void validateSecondNationalityByIndResidentRepresentative(){
		
		if(legalRepresentativeHistory.getNationality()!=null && !legalRepresentativeHistory.getNationality().equals(countryResidence) && BooleanType.YES.getCode().equals(legalRepresentativeHistory.getIndResidence()) &&
				countryResidence.equals(legalRepresentativeHistory.getSecondNationality())){
			
			legalRepresentativeHistory.setSecondDocumentType(null);
			legalRepresentativeHistory.setSecondDocumentNumber(null);
			
			SelectOneMenu cmbSecondDocumentType = (SelectOneMenu)  FacesContext.getCurrentInstance().getViewRoot().findComponent(idLegalRepresentativeComponent+":tabView:"+"cmbSecondDocumentType");
			if(cmbSecondDocumentType!=null){cmbSecondDocumentType.setValid(true);}
			InputText inputSecondDocumentNumber = (InputText)  FacesContext.getCurrentInstance().getViewRoot().findComponent(idLegalRepresentativeComponent+":tabView:"+"inputSecondDocumentNumber");
			if(inputSecondDocumentNumber!=null){inputSecondDocumentNumber.setValid(true);}
		}		
	}
	
//	public boolean validateIsNotNullRepresentanteFiles() {
//		
//		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
//		JSFUtilities.hideGeneralDialogues();
//
//		boolean flag = true;
//		
//		if(lstRepresentativeFileHistory==null || (lstRepresentativeFileHistory!=null && lstRepresentativeFileHistory.size()<1)){
//			flag = true;
//		}
//		else{
//			
//			if (legalRepresentativeHistory.getPersonType().equals(
//					PersonType.NATURAL.getCode())) {
//				if (legalRepresentativeTO.getHolderPersonType().equals(
//						PersonType.NATURAL.getCode())) {
//					
//					if (legalRepresentativeTO.isFlagIndicatorMinorHolder()) {
//
//						if (legalRepresentativeHistory.getRepresentativeClass()
//								.equals(RepresentativeClassType.FATHER.getCode())) {
//							
//							flag = minRepresentativeFileCaseOne(RepresentativeClassType.FATHER.getCode());
//						}
//
//						if (legalRepresentativeHistory.getRepresentativeClass()
//								.equals(RepresentativeClassType.TUTOR_LEGAL
//										.getCode())) {						
//							flag = false;
//						}
//					} else if (legalRepresentativeTO.isFlagHolderIndicatorInability()) {
//						
//						flag = minRepresentativeFileCaseOne(0);		
//					
//					}
//				} 
//				else if (legalRepresentativeTO.getHolderPersonType().equals(
//						PersonType.JURIDIC.getCode())) {
//
//					if (legalRepresentativeTO.getHolderJuridicClassType().equals(JuridicClassType.NORMAL.getCode()) || legalRepresentativeTO.getHolderJuridicClassType().equals(JuridicClassType.TRUST.getCode())) {
//						
//						flag = minRepresentativeFileCaseTwo(JuridicClassType.NORMAL.getCode());
//					
//						
//					}
//					if (legalRepresentativeTO.getHolderJuridicClassType().equals(
//							JuridicClassType.ENTITY_STATE.getCode())) {
//						
//						flag = minRepresentativeFileCaseTwo(JuridicClassType.ENTITY_STATE.getCode());
//					
//					}
//
//				}
//			}
//			else{
//				flag = false;
//			}
//			
//		}
//		
//		if(flag){
//			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
//					PropertiesUtilities.getValidationMessage(PropertiesConstants.WARNING_FILE_REQUEST_REPRESENTATIVE_NO_COMPLETE,null));
//			JSFUtilities.showSimpleValidationDialog();
//		}
//		
//		
//	return flag;
//		
//	}
	
//	public boolean minRepresentativeFileCaseOne(Integer identificator){
//		boolean flag = true;
//				
//		boolean indicator = false;
//		int count = 0;
//		
//		for(int i=0;i<lstRepresentativeFileHistory.size();i++){
//			
//			if(lstRepresentativeFileHistory.get(i).getDocumentType().equals(AgeLessFatherDocRepreNatType.CE.getCode()) || 
//					lstRepresentativeFileHistory.get(i).getDocumentType().equals(AgeLessFatherDocRepreNatType.PAS.getCode())){
//				
//				indicator = true;
//			}
//			
//			
//			if(identificator.equals(RepresentativeClassType.FATHER.getCode())){
//			
//				if(lstRepresentativeFileHistory.get(i).getDocumentType().equals(AgeLessFatherDocRepreNatType.DJCP.getCode())){
//					count++;
//				}
//				if(lstRepresentativeFileHistory.get(i).getDocumentType().equals(AgeLessFatherDocRepreNatType.OC.getCode())){
//					count++;
//				}			
//			
//			}
//			
//			if(identificator.equals(Integer.valueOf(0))){
//				if(lstRepresentativeFileHistory.get(i).getDocumentType().equals(UnableDocRepreNatType.CE.getCode()) || 
//					lstRepresentativeFileHistory.get(i).getDocumentType().equals(UnableDocRepreNatType.PAS.getCode())){
//				
//					indicator = true;
//				}
//			}
//			
//			if(identificator.equals(Integer.valueOf(0))){
//				if(lstRepresentativeFileHistory.get(i).getDocumentType().equals(UnableDocRepreNatType.CATL.getCode())){
//					count++;
//				}
//			}
//		}
//		
//		if(identificator.equals(RepresentativeClassType.FATHER.getCode())){
//			
//			if(count==2 && indicator && lstRepresentativeFileHistory.size()>=4)
//			{
//				flag = false;
//			}
//			else{
//				flag = true;
//			}
//		}
//		
//		if(identificator.equals(Integer.valueOf(0))){
//			if(count==1 && indicator && lstRepresentativeFileHistory.size()>=3)
//			{
//				flag = false;
//			}
//			else{
//				flag = true;
//			}
//		}
//	
//
//		return flag;
//	}


	
	/**
 * Action do no export data.
 */
@LoggerAuditWeb
	public void actionDoNoExportData(){
		
		legalRepresentativeHistory.setDocumentNumber(null);
		newDocumentNumber = GeneralConstants.EMPTY_STRING;
		CommandButton cmbSave = (CommandButton)FacesContext.getCurrentInstance().getViewRoot().findComponent(idLegalRepresentativeComponent+":"+"btnSave");
		cmbSave.setDisabled(true);
		legalRepresentativeHistory.setFlagLegalImport(false);
		legalRepresentativeHistory.setFlagActiveFilesByHolderImport(true);
	}
	
	/**
	 * Min representative file case two.
	 *
	 * @param identificator the identificator
	 * @return true, if successful
	 */
	public boolean minRepresentativeFileCaseTwo(Integer identificator){
		boolean flag = true;
		boolean indicator = false;
		int count = 0;
		
		for(int i=0;i<lstRepresentativeFileHistory.size();i++){
		
			if(lstRepresentativeFileHistory.get(i).getDocumentType().equals(JuridicDocRepreNatType.CE.getCode()) || 
					lstRepresentativeFileHistory.get(i).getDocumentType().equals(JuridicDocRepreNatType.PAS.getCode())){
				
				indicator = true;
			}
			
			if(identificator.equals(JuridicClassType.NORMAL.getCode())){
				if(lstRepresentativeFileHistory.get(i).getDocumentType().equals(JuridicDocRepreNatType.CDRL.getCode())){
					count++;
				}
			}
			
			
			if(lstRepresentativeFileHistory.get(i).getDocumentType().equals(StatalEntityDocRepreNatType.CE.getCode()) || 
					lstRepresentativeFileHistory.get(i).getDocumentType().equals(StatalEntityDocRepreNatType.PAS.getCode())){
				
				indicator = true;
			}
			
			if(identificator.equals(JuridicClassType.ENTITY_STATE.getCode())){
				if(lstRepresentativeFileHistory.get(i).getDocumentType().equals(StatalEntityDocRepreNatType.CDRL.getCode())){
					count++;
				}
			}
			
		}
		
		if(identificator.equals(JuridicClassType.NORMAL.getCode())){
			if(count==1 && indicator && lstRepresentativeFileHistory.size()>=2)
			{
				flag = false;
			}
			else{
				flag = true;
			}
		}
		
		if(identificator.equals(JuridicClassType.ENTITY_STATE.getCode())){
			if(count==1 && indicator && lstRepresentativeFileHistory.size()>=2)
			{
				flag = false;
			}
			else{
				flag = true;
			}
		}
		
		return flag;
	}

	
	/**
	 * Copy legal representative.
	 */
	public void copyLegalRepresentative() {
		
		if (legalRepresentativeTO.isFlagCopyLegalRepresentative()) {
			
			listDepartmentPostalLocationRepresentative = listDepartmentLocationRepresentative;
			listProvincePostalLocationRepresentative = listProvinceLocationRepresentative;
			listMunicipalityPostalLocationRepresentative = listMunicipalityLocationRepresentative;

			
			legalRepresentativeHistory.setPostalResidenceCountry(legalRepresentativeHistory.getLegalResidenceCountry());
			legalRepresentativeHistory.setPostalProvince(legalRepresentativeHistory.getLegalProvince());
			legalRepresentativeHistory.setPostalDepartment(legalRepresentativeHistory.getLegalDepartment());
			legalRepresentativeHistory.setPostalDistrict(legalRepresentativeHistory.getLegalDistrict());
			legalRepresentativeHistory.setPostalAddress(legalRepresentativeHistory.getLegalAddress());
			legalRepresentativeHistory.setPostalDistrict(legalRepresentativeHistory.getLegalDistrict());

			legalRepresentativeTO.setDisabledCmbPostalCountryRepresentative(true);
			legalRepresentativeTO.setDisabledCmbPostalProvinceRepresentative(true);
			legalRepresentativeTO.setDisabledCmbPostalDepartamentRepresentative(true);
			legalRepresentativeTO.setDisabledCmbPostalMunicipalityRepresentative(true);
			legalRepresentativeTO.setDisabledInputPostalAddressRepresentative(false);
			
			SelectOneMenu cmbPC= (SelectOneMenu)	FacesContext.getCurrentInstance().getViewRoot().findComponent(idLegalRepresentativeComponent+":tabView:"+"cmbPostalCountry");
			SelectOneMenu cmbPP= (SelectOneMenu)	FacesContext.getCurrentInstance().getViewRoot().findComponent(idLegalRepresentativeComponent+":tabView:"+"cmbPostalProvince");
			SelectOneMenu cmbPS= (SelectOneMenu)	FacesContext.getCurrentInstance().getViewRoot().findComponent(idLegalRepresentativeComponent+":tabView:"+"cmbPostalDepartament");
			InputText cmbPA= (InputText)	FacesContext.getCurrentInstance().getViewRoot().findComponent(idLegalRepresentativeComponent+":tabView:"+"inputPostalAddress");

			cmbPC.setValid(true);
			cmbPP.setValid(true);
			cmbPS.setValid(true);
			cmbPA.setValid(true);			

		} else {
			legalRepresentativeTO.setDisabledCmbPostalCountryRepresentative(false);
			legalRepresentativeTO.setDisabledCmbPostalProvinceRepresentative(false);
			legalRepresentativeTO.setDisabledCmbPostalDepartamentRepresentative(false);
			legalRepresentativeTO.setDisabledInputPostalAddressRepresentative(false);
			legalRepresentativeTO.setDisabledCmbPostalMunicipalityRepresentative(false);
		}
	}
	
	/**
	 * Action do export data.
	 */
	@LoggerAuditWeb
	public void actionDoExportData(){
		
		try{
			
		//reemplazando con el nuevo numero de documento
		legalRepresentativeHistory.setDocumentNumber(newDocumentNumber);
		legalRepresentativeHistory.setSecondDocumentNumber(newDocumentNumberSecondNationality);
		
		ParameterTable	auxParameterTable;	
		CommandButton cmbSave = (CommandButton)FacesContext.getCurrentInstance().getViewRoot().findComponent(idLegalRepresentativeComponent+":"+"btnSave");
		cmbSave.setDisabled(false);
		
		getLstRepresentativeClassType();
		
		legalRepresentativeHistory.setFlagLegalImport(false);
		legalRepresentativeHistory.setFlagActiveFilesByHolderImport(false);
		
		Integer representativeClass = legalRepresentativeHistory.getRepresentativeClass();
		
		if(legalRepresentativeTO.isFlagExportDataHolder()){
			
		  Holder auxHolder = new Holder();
		  auxHolder.setDocumentType(legalRepresentativeHistory.getDocumentType());
		  auxHolder.setDocumentNumber(legalRepresentativeHistory.getDocumentNumber());
		  auxHolder.setDocumentSource(legalRepresentativeHistory.getDocumentSource());
		  auxHolder.setFirstLastName(legalRepresentativeHistory.getFirstLastName());
		  
		  auxHolder = accountsFacade.getHolderByDocumentTypeAndDocumentNumberServiceFacade(auxHolder);
		  
		  		  	  
		  if(auxHolder!=null){
			  
		  legalRepresentativeHistory = new LegalRepresentativeHistory();
		  
		  legalRepresentativeHistory.setFlagLegalImport(true);
		  legalRepresentativeHistory.setFlagActiveFilesByHolderImport(false);
		  
		  if(auxHolder.getDocumentSource()!=null){
			  legalRepresentativeHistory.setDocumentSource(auxHolder.getDocumentSource());
		  }
		  
		  if(auxHolder.getDocumentType()!=null){
			  legalRepresentativeHistory.setDocumentType(auxHolder.getDocumentType());
		  }
		  
		  if(auxHolder.getDocumentNumber()!=null){
			  legalRepresentativeHistory.setDocumentNumber(auxHolder.getDocumentNumber());
		  }
			
		  if(auxHolder.getHolderType()!=null){
			  legalRepresentativeHistory.setPersonType(auxHolder.getHolderType());
			  
			  if(legalRepresentativeHistory.getPersonType().equals(PersonType.JURIDIC.getCode())){
	    		  getLstEconomicSector();	
	    		  
	    		  if(auxHolder.getEconomicSector()!=null){
	    			  legalRepresentativeHistory.setEconomicSector(auxHolder.getEconomicSector());
	    		  }
	    		  
	    		  if(auxHolder.getEconomicActivity()!=null){
	    			  legalRepresentativeHistory.setEconomicActivity(auxHolder.getEconomicActivity());
	    		  }
	    		
		  		  changeSelectedEconomicSectorLegalRepresentative();
	    	  }
			 
			  
			  if (legalRepresentativeTO.isFlagIndicatorMinorHolder()
						&& legalRepresentativeTO.getHolderPersonType().equals(
							PersonType.NATURAL.getCode())) {
				
				legalRepresentativeTO.setFlagCmbRepresentativeClass(false);
				auxParameterTable = new ParameterTable();
				List<ParameterTable> auxListPresentantiveClass = listRepresentativeClass;
				listRepresentativeClass = new ArrayList<ParameterTable>();

				for (int i = 0; i < auxListPresentantiveClass.size(); i++) {
					if (auxListPresentantiveClass.get(i).getParameterTablePk()
							.equals(RepresentativeClassType.FATHER.getCode())) {
						auxParameterTable = new ParameterTable();
						auxParameterTable
								.setParameterTablePk(auxListPresentantiveClass
										.get(i).getParameterTablePk());
						auxParameterTable
								.setParameterName(auxListPresentantiveClass
										.get(i).getParameterName());
						listRepresentativeClass.add(auxParameterTable);
					}
					if (auxListPresentantiveClass
							.get(i)
							.getParameterTablePk()
							.equals(RepresentativeClassType.TUTOR_LEGAL
									.getCode())) {
						auxParameterTable = new ParameterTable();
						auxParameterTable
								.setParameterTablePk(auxListPresentantiveClass
										.get(i).getParameterTablePk());
						auxParameterTable
								.setParameterName(auxListPresentantiveClass
										.get(i).getParameterName());
						listRepresentativeClass.add(auxParameterTable);
					}
				}

			} else  {

				legalRepresentativeHistory
						.setRepresentativeClass(RepresentativeClassType.LEGAL
								.getCode());
				legalRepresentativeTO.setFlagCmbRepresentativeClass(true);
			}
			  
			  
		  }
		  
		  if(Validations.validateIsNotNullAndNotEmpty(representativeClass)){
			  legalRepresentativeHistory.setRepresentativeClass(representativeClass);
		  }
	      		  
		  if(auxHolder.getNationality()!=null){
			  legalRepresentativeHistory.setNationality(auxHolder.getNationality());
		  }
		  
		  if(auxHolder.getIndResidence()!=null && auxHolder.getIndResidence().equals(BooleanType.YES.getCode())){
			  legalRepresentativeHistory.setIndResidence(auxHolder.getIndResidence());  
		  }	
		  
		  if(auxHolder.getLegalResidenceCountry()!=null){
			  legalRepresentativeHistory.setLegalResidenceCountry(auxHolder.getLegalResidenceCountry());
		  }
		  
		  if(auxHolder.getSecondNationality()!=null){
			  legalRepresentativeHistory.setSecondNationality(auxHolder.getSecondNationality());
		  }
		  
		  if(auxHolder.getSecondDocumentType()!=null){
			  legalRepresentativeHistory.setSecondDocumentType(auxHolder.getSecondDocumentType());
		  }
		  
		  if(auxHolder.getSecondDocumentNumber()!=null){
			  legalRepresentativeHistory.setSecondDocumentNumber(auxHolder.getSecondDocumentNumber());
		  }
		  
		  if(auxHolder.getName()!=null){
		  legalRepresentativeHistory.setName(auxHolder.getName());
		  }
		  if(auxHolder.getFirstLastName()!=null){
			  legalRepresentativeHistory.setFirstLastName(auxHolder.getFirstLastName());
		  }
		  
		  if(auxHolder.getSecondLastName()!=null){
			  legalRepresentativeHistory.setSecondLastName(auxHolder.getSecondLastName());
		  }
		  
		  if(auxHolder.getBirthDate()!=null){
			  legalRepresentativeHistory.setBirthDate(auxHolder.getBirthDate());
		  }
		  
		  if(auxHolder.getSex()!=null){
			  legalRepresentativeHistory.setSex(auxHolder.getSex());
		  }
		  
		  if(auxHolder.getLegalProvince()!=null){
			  legalRepresentativeHistory.setLegalProvince(auxHolder.getLegalProvince());
		  }
		  
		  if(auxHolder.getLegalDepartment()!=null){
			  legalRepresentativeHistory.setLegalDepartment(auxHolder.getLegalDepartment());
		  }
		  
		  if(auxHolder.getLegalDistrict()!=null){
			  legalRepresentativeHistory.setLegalDistrict(auxHolder.getLegalDistrict());
		  }
		  
		  if(auxHolder.getLegalAddress()!=null){
			  legalRepresentativeHistory.setLegalAddress(auxHolder.getLegalAddress());
		  }
		  
		  if(auxHolder.getPostalResidenceCountry()!=null){
			  legalRepresentativeHistory.setPostalResidenceCountry(auxHolder.getPostalResidenceCountry());
		  }
		  
		  if(auxHolder.getPostalProvince()!=null){
			  legalRepresentativeHistory.setPostalProvince(auxHolder.getPostalProvince());
		  }
		  
		  if(auxHolder.getPostalDepartment()!=null){
			  legalRepresentativeHistory.setPostalDepartment(auxHolder.getPostalDepartment());
		  }
		  
		  if(auxHolder.getPostalDistrict()!=null){
			  legalRepresentativeHistory.setPostalDistrict(auxHolder.getPostalDistrict());
		  }
		  
		  if(auxHolder.getPostalAddress()!=null){
			  legalRepresentativeHistory.setPostalAddress(auxHolder.getPostalAddress());
		  }
		  
		  if(auxHolder.getHomePhoneNumber()!=null){
			  legalRepresentativeHistory.setHomePhoneNumber(auxHolder.getHomePhoneNumber());
		  }
		  
		  if(auxHolder.getFaxNumber()!=null){
			  legalRepresentativeHistory.setFaxNumber(auxHolder.getFaxNumber());
		  }
		  
		  if(auxHolder.getOfficePhoneNumber()!=null){
			  legalRepresentativeHistory.setOfficePhoneNumber(auxHolder.getOfficePhoneNumber());
		  }
		  
		  if(auxHolder.getEmail()!=null){
			  legalRepresentativeHistory.setEmail(auxHolder.getEmail());
		  }
		  
		  PepPerson pep = accountsFacade.getPepPersonByIdHolderServiceFacade(auxHolder.getIdHolderPk());
		  if(pep!=null){
			  legalRepresentativeTO.setFlagRepresentativeIsPEP(true);
			  legalRepresentativeHistory.setBeginningPeriod(pep.getBeginingPeriod());
			  legalRepresentativeHistory.setEndingPeriod(pep.getEndingPeriod());
			  legalRepresentativeHistory.setCategory(pep.getCategory());
			  legalRepresentativeHistory.setRole(pep.getRole());
		  }
		  else if(pep==null){
			  legalRepresentativeTO.setFlagRepresentativeIsPEP(false);
			  legalRepresentativeHistory.setIndPEP(GeneralConstants.ZERO_VALUE_INTEGER);
			  legalRepresentativeHistory.setBeginningPeriod(null);
			  legalRepresentativeHistory.setEndingPeriod(null);
			  legalRepresentativeHistory.setCategory(null);
			  legalRepresentativeHistory.setRole(null);
		  }
		  
		 		 
		  }
		  
		}
		if(legalRepresentativeTO.isFlagExportDataRepresentative()){
						 
			  lstLegalRepresentativeFile = null;
			 
			  LegalRepresentative auxLegalRepresentative = new LegalRepresentative();
			  auxLegalRepresentative.setDocumentType(legalRepresentativeHistory.getDocumentType());
			  auxLegalRepresentative.setDocumentNumber(legalRepresentativeHistory.getDocumentNumber());
			  auxLegalRepresentative.setDocumentSource(legalRepresentativeHistory.getDocumentSource());
			  auxLegalRepresentative.setFirstLastName(legalRepresentativeHistory.getFirstLastName());
			  
			  auxLegalRepresentative = accountsFacade.getLegalRepresentativeByDocumentTypeAndDocumentNumberServiceFacade(auxLegalRepresentative);
			  
			  if(auxLegalRepresentative!=null){
				  
		      legalRepresentativeHistory = new LegalRepresentativeHistory();
		      
		      legalRepresentativeHistory.setFlagLegalImport(true);
			  legalRepresentativeHistory.setFlagActiveFilesByHolderImport(false);
			  
			  if(auxLegalRepresentative.getDocumentSource()!=null){
				  legalRepresentativeHistory.setDocumentSource(auxLegalRepresentative.getDocumentSource());
			  }
			  
			  if(auxLegalRepresentative.getDocumentType()!=null){
				  legalRepresentativeHistory.setDocumentType(auxLegalRepresentative.getDocumentType());
			  }
			  
			  if(auxLegalRepresentative.getDocumentNumber()!=null){
				  legalRepresentativeHistory.setDocumentNumber(auxLegalRepresentative.getDocumentNumber());
			  }
		      
		      if(auxLegalRepresentative.getPersonType()!=null){
		    	  
		    	  legalRepresentativeHistory.setPersonType(auxLegalRepresentative.getPersonType());
		    	  
		    	  if(legalRepresentativeHistory.getPersonType().equals(PersonType.JURIDIC.getCode())){
		    		  getLstEconomicSector();	
		    		  
		    		  if(auxLegalRepresentative.getEconomicSector()!=null){
		    			  legalRepresentativeHistory.setEconomicSector(auxLegalRepresentative.getEconomicSector());
		    		  }
		    		  
		    		  if(auxLegalRepresentative.getEconomicActivity()!=null){
		    			  legalRepresentativeHistory.setEconomicActivity(auxLegalRepresentative.getEconomicActivity());
		    		  }
		    		
			  		  changeSelectedEconomicSectorLegalRepresentative();
		    	  }
		    	  
		    	  if (legalRepresentativeTO.isFlagIndicatorMinorHolder()
							&& legalRepresentativeTO.getHolderPersonType().equals(
								PersonType.NATURAL.getCode())) {
					
		    		legalRepresentativeTO.setFlagCmbRepresentativeClass(false);

					auxParameterTable = new ParameterTable();
					List<ParameterTable> auxListPresentantiveClass = listRepresentativeClass;
					listRepresentativeClass = new ArrayList<ParameterTable>();

					for (int i = 0; i < auxListPresentantiveClass.size(); i++) {
						if (auxListPresentantiveClass.get(i).getParameterTablePk()
								.equals(RepresentativeClassType.FATHER.getCode())) {
							auxParameterTable = new ParameterTable();
							auxParameterTable
									.setParameterTablePk(auxListPresentantiveClass
											.get(i).getParameterTablePk());
							auxParameterTable
									.setParameterName(auxListPresentantiveClass
											.get(i).getParameterName());
							listRepresentativeClass.add(auxParameterTable);
						}
						if (auxListPresentantiveClass
								.get(i)
								.getParameterTablePk()
								.equals(RepresentativeClassType.TUTOR_LEGAL
										.getCode())) {
							auxParameterTable = new ParameterTable();
							auxParameterTable
									.setParameterTablePk(auxListPresentantiveClass
											.get(i).getParameterTablePk());
							auxParameterTable
									.setParameterName(auxListPresentantiveClass
											.get(i).getParameterName());
							listRepresentativeClass.add(auxParameterTable);
						}
					}

				} else  {

					legalRepresentativeHistory
							.setRepresentativeClass(RepresentativeClassType.LEGAL
									.getCode());
					legalRepresentativeTO.setFlagCmbRepresentativeClass(true);
				}
		    	  
		      }
		      
		      RepresentedEntity repreEntity =    accountsFacade.getRepresentedEntityByIdLegalRepresentativeFacadeBean(auxLegalRepresentative.getIdLegalRepresentativePk());
		      
		      if(repreEntity!=null){
		    	  legalRepresentativeHistory.setRepresentativeClass(repreEntity.getRepresentativeClass()); 
		      }
		      
		      if(auxLegalRepresentative.getNationality()!=null){
				  legalRepresentativeHistory.setNationality(auxLegalRepresentative.getNationality());
			  }
			  
			  if(auxLegalRepresentative.getIndResidence()!=null){
				  legalRepresentativeHistory.setIndResidence(auxLegalRepresentative.getIndResidence());  
			  }	
			  
			  if(auxLegalRepresentative.getLegalResidenceCountry()!=null){
				  legalRepresentativeHistory.setLegalResidenceCountry(auxLegalRepresentative.getLegalResidenceCountry());
			  }
			  
			  if(auxLegalRepresentative.getSecondNationality()!=null){
				  legalRepresentativeHistory.setSecondNationality(auxLegalRepresentative.getSecondNationality());
			  }
			  
			  if(auxLegalRepresentative.getSecondDocumentType()!=null){
				  legalRepresentativeHistory.setSecondDocumentType(auxLegalRepresentative.getSecondDocumentType());
			  }
			  
			  if(auxLegalRepresentative.getSecondDocumentNumber()!=null){
				  legalRepresentativeHistory.setSecondDocumentNumber(auxLegalRepresentative.getSecondDocumentNumber());
			  }
			  
			  if(auxLegalRepresentative.getName()!=null){
			  legalRepresentativeHistory.setName(auxLegalRepresentative.getName());
			  }
			  if(auxLegalRepresentative.getFirstLastName()!=null){
				  legalRepresentativeHistory.setFirstLastName(auxLegalRepresentative.getFirstLastName());
			  }
			  
			  if(auxLegalRepresentative.getSecondLastName()!=null){
				  legalRepresentativeHistory.setSecondLastName(auxLegalRepresentative.getSecondLastName());
			  }
			  
			  if(auxLegalRepresentative.getBirthDate()!=null){
				  legalRepresentativeHistory.setBirthDate(auxLegalRepresentative.getBirthDate());
			  }
			  
			  if(auxLegalRepresentative.getSex()!=null){
				  legalRepresentativeHistory.setSex(auxLegalRepresentative.getSex());
			  }
			  
			  if(auxLegalRepresentative.getLegalProvince()!=null){
				  legalRepresentativeHistory.setLegalProvince(auxLegalRepresentative.getLegalProvince());
			  }
			  
			  if(auxLegalRepresentative.getLegalDepartment()!=null){
				  legalRepresentativeHistory.setLegalDepartment(auxLegalRepresentative.getLegalDepartment());
			  }
			  
			  if(auxLegalRepresentative.getLegalDistrict()!=null){
				  legalRepresentativeHistory.setLegalDistrict(auxLegalRepresentative.getLegalDistrict());
			  }
			  
			  if(auxLegalRepresentative.getLegalAddress()!=null){
				  legalRepresentativeHistory.setLegalAddress(auxLegalRepresentative.getLegalAddress());
			  }
			  
			  if(auxLegalRepresentative.getPostalResidenceCountry()!=null){
				  legalRepresentativeHistory.setPostalResidenceCountry(auxLegalRepresentative.getPostalResidenceCountry());
			  }
			  
			  if(auxLegalRepresentative.getPostalProvince()!=null){
				  legalRepresentativeHistory.setPostalProvince(auxLegalRepresentative.getPostalProvince());
			  }
			  
			  if(auxLegalRepresentative.getPostalDepartment()!=null){
				  legalRepresentativeHistory.setPostalDepartment(auxLegalRepresentative.getPostalDepartment());
			  }
			  
			  if(auxLegalRepresentative.getPostalDistrict()!=null){
				  legalRepresentativeHistory.setPostalDistrict(auxLegalRepresentative.getPostalDistrict());
			  }
			  
			  if(auxLegalRepresentative.getPostalAddress()!=null){
				  legalRepresentativeHistory.setPostalAddress(auxLegalRepresentative.getPostalAddress());
			  }
			  
			  if(auxLegalRepresentative.getHomePhoneNumber()!=null){
				  legalRepresentativeHistory.setHomePhoneNumber(auxLegalRepresentative.getHomePhoneNumber());
			  }
			  
			  if(auxLegalRepresentative.getFaxNumber()!=null){
				  legalRepresentativeHistory.setFaxNumber(auxLegalRepresentative.getFaxNumber());
			  }
			  
			  if(auxLegalRepresentative.getOfficePhoneNumber()!=null){
				  legalRepresentativeHistory.setOfficePhoneNumber(auxLegalRepresentative.getOfficePhoneNumber());
			  }
			  
			  if(auxLegalRepresentative.getEmail()!=null){
				  legalRepresentativeHistory.setEmail(auxLegalRepresentative.getEmail());
			  }	
			  
			  if(auxLegalRepresentative.getPersonType()!=null){
				  legalRepresentativeHistory.setPersonType(auxLegalRepresentative.getPersonType());
			  }
			  
			  
			  lstLegalRepresentativeFile = accountsFacade.getLegalRepresentativeFileByIdLegalRepresentativeServiceFacade(auxLegalRepresentative.getIdLegalRepresentativePk());
			  
			  
			  lstRepresentativeFileHistory = new ArrayList<RepresentativeFileHistory>();
			  
			  RepresentativeFileHistory auxRepresentativeFileHistory;
			  
			  for(int i=0;i<lstLegalRepresentativeFile.size();i++){
				  auxRepresentativeFileHistory = new RepresentativeFileHistory();
				  auxRepresentativeFileHistory.setDescription(lstLegalRepresentativeFile.get(i).getDescription());
				  auxRepresentativeFileHistory.setDocumentType(lstLegalRepresentativeFile.get(i).getDocumentType());
				  auxRepresentativeFileHistory.setFilename(lstLegalRepresentativeFile.get(i).getFilename());
				  auxRepresentativeFileHistory.setFileRepresentative(lstLegalRepresentativeFile.get(i).getFileLegalRepre());
				  auxRepresentativeFileHistory.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
				  auxRepresentativeFileHistory.setRegistryUser(userInfo.getUserAccountSession().getUserName());
				  auxRepresentativeFileHistory.setRegistryDate(CommonsUtilities.currentDate());
				  auxRepresentativeFileHistory.setRepresentFileHistType(HolderReqFileHisType.CREATION.getCode());
				  auxRepresentativeFileHistory.setStateFile(HolderFileStateType.REGISTERED.getCode());
				  			  
				  auxRepresentativeFileHistory.setLegalRepresentativeHistory(legalRepresentativeHistory);
				  auxRepresentativeFileHistory.setFlagLegalImport(true);
				  auxRepresentativeFileHistory.setFlagActiveFilesByHolderImport(false);
				  auxRepresentativeFileHistory.setWasModified(true);
				  
				  lstRepresentativeFileHistory.add(auxRepresentativeFileHistory);
			  }
			  
			  PepPerson pep = accountsFacade.getPepPersonByIdLegalRepresentativeServiceFacade(auxLegalRepresentative.getIdLegalRepresentativePk());
			  if(pep!=null){
				  legalRepresentativeTO.setFlagRepresentativeIsPEP(true);
				  legalRepresentativeHistory.setBeginningPeriod(pep.getBeginingPeriod());
				  legalRepresentativeHistory.setEndingPeriod(pep.getEndingPeriod());
				  legalRepresentativeHistory.setCategory(pep.getCategory());
				  legalRepresentativeHistory.setRole(pep.getRole());
			  }
			  else if(pep==null){
				  legalRepresentativeTO.setFlagRepresentativeIsPEP(false);
				  legalRepresentativeHistory.setIndPEP(GeneralConstants.ZERO_VALUE_INTEGER);
				  legalRepresentativeHistory.setBeginningPeriod(null);
				  legalRepresentativeHistory.setEndingPeriod(null);
				  legalRepresentativeHistory.setCategory(null);
				  legalRepresentativeHistory.setRole(null);
			  }
		 }
			  
		}
		
		listDepartmentLocationRepresentative = getLstDepartmentLocation(legalRepresentativeHistory
				.getLegalResidenceCountry());
		listMunicipalityLocationRepresentative = getLstMunicipalityLocation(legalRepresentativeHistory.getLegalProvince());		
		listProvinceLocationRepresentative = getLstProvinceLocation(legalRepresentativeHistory.getLegalDepartment());
		
		listDepartmentPostalLocationRepresentative = getLstDepartmentLocation(legalRepresentativeHistory
				.getPostalResidenceCountry());
		listProvincePostalLocationRepresentative = getLstProvinceLocation(legalRepresentativeHistory.getPostalDepartment());
		listMunicipalityPostalLocationRepresentative = getLstMunicipalityLocation(legalRepresentativeHistory.getPostalProvince());  
		
		
		
		
		}
		catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		
		
	}
	
	/**
	 * Validate exist type document and number document representative.
	 *
	 * @param idFormClient the id form client
	 * @param idComponent the id component
	 */
	@LoggerAuditWeb
	public void validateExistTypeDocumentAndNumberDocumentRepresentative(String idFormClient,String idComponent){
		
		try{
			if(legalRepresentativeHistory.getDocumentType()!=null && 
					!legalRepresentativeHistory.getDocumentType().equals(DocumentType.DIO.getCode()) &&
					legalRepresentativeHistory.getDocumentNumber()!=null && 
					   lstLegalRepresentativeHistory!=null && 
							   lstLegalRepresentativeHistory.size()>0){
						
					   for(int i=0;i<lstLegalRepresentativeHistory.size();i++){
						   
						   if((lstLegalRepresentativeHistory.get(i).getDocumentType().equals(legalRepresentativeHistory.getDocumentType()) &&
								   lstLegalRepresentativeHistory.get(i).getDocumentNumber().equals(legalRepresentativeHistory.getDocumentNumber()) &&
								   !lstLegalRepresentativeHistory.get(i).getDocumentType().equals(DocumentType.CI.getCode()))
						      || (lstLegalRepresentativeHistory.get(i).getSecondDocumentType()!=null && lstLegalRepresentativeHistory.get(i).getSecondDocumentType().equals(legalRepresentativeHistory.getDocumentType()) &&
						    		  lstLegalRepresentativeHistory.get(i).getSecondDocumentNumber()!=null && lstLegalRepresentativeHistory.get(i).getSecondDocumentNumber().equals(legalRepresentativeHistory.getDocumentNumber()))
						      	){
						
							   	Object[] bodyData = new Object[0];
							   	
							   	JSFUtilities.showMessageOnDialog(GeneralConstants.LBL_HEADER_ALERT, PropertiesUtilities.getValidationMessage(
														GeneralPropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_EQUAL_REPRESENTATIVE,bodyData));
								JSFUtilities.showSimpleValidationDialog();
								
							   
							   JSFUtilities
								.addContextMessage(
										idFormClient+":tabView:"+idComponent,
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities
														.getValidationMessage(
														GeneralPropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_EQUAL_REPRESENTATIVE,
														bodyData),
										PropertiesUtilities
														.getValidationMessage(
														GeneralPropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_EQUAL_REPRESENTATIVE,
														bodyData));

							   legalRepresentativeHistory.setDocumentNumber(null);
							   newDocumentNumber = GeneralConstants.EMPTY_STRING;
							   return;
						   }
						   
						   if((lstLegalRepresentativeHistory.get(i).getDocumentType().equals(legalRepresentativeHistory.getDocumentType()) &&
								   lstLegalRepresentativeHistory.get(i).getDocumentNumber().equals(legalRepresentativeHistory.getDocumentNumber())
								   //&& (lstLegalRepresentativeHistory.get(i).getDocumentSource()!=null && lstLegalRepresentativeHistory.get(i).getDocumentSource().equals(legalRepresentativeHistory.getDocumentSource()))	  
								   && (lstLegalRepresentativeHistory.get(i).getDocumentType()!=null && lstLegalRepresentativeHistory.get(i).getDocumentType().equals(DocumentType.CI.getCode())))
								      
								   || (lstLegalRepresentativeHistory.get(i).getSecondDocumentType()!=null && lstLegalRepresentativeHistory.get(i).getSecondDocumentType().equals(legalRepresentativeHistory.getDocumentType()) &&
								    		  lstLegalRepresentativeHistory.get(i).getSecondDocumentNumber()!=null && lstLegalRepresentativeHistory.get(i).getSecondDocumentNumber().equals(legalRepresentativeHistory.getDocumentNumber()))
								   ){
							   
							   Object[] bodyData = new Object[0];
							   	
							   	JSFUtilities.showMessageOnDialog(GeneralConstants.LBL_HEADER_ALERT, PropertiesUtilities.getValidationMessage(
														GeneralPropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_EQUAL_REPRESENTATIVE,bodyData));
								JSFUtilities.showSimpleValidationDialog();
								
							   
							   JSFUtilities
								.addContextMessage(
										idFormClient+":tabView:"+idComponent,
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities
														.getValidationMessage(
														GeneralPropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_EQUAL_REPRESENTATIVE,
														bodyData),
										PropertiesUtilities
														.getValidationMessage(
														GeneralPropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_EQUAL_REPRESENTATIVE,
														bodyData));

							   legalRepresentativeHistory.setDocumentNumber(null);
							   newDocumentNumber = GeneralConstants.EMPTY_STRING;
							   return;
						   }
					   }
						
					}
			
			
			if(legalRepresentativeTO.getDocumentType()!=null && 
					   !legalRepresentativeTO.getDocumentType().equals(DocumentType.DIO.getCode()) &&		
					   legalRepresentativeTO.getDocumentNumber()!=null && 
							   legalRepresentativeHistory.getDocumentType()!=null && 
									   legalRepresentativeHistory.getDocumentNumber()!=null){
//issue 2613 no debe solicitar representante fiduciario	y puede tener el mismo numero de documento que el cui						
//					      if((legalRepresentativeHistory.getDocumentType().equals(legalRepresentativeTO.getDocumentType()) &&
//					    		  legalRepresentativeHistory.getDocumentNumber().equals(legalRepresentativeTO.getDocumentNumber()) &&
//					    		  !legalRepresentativeHistory.getDocumentType().equals(DocumentType.CI.getCode()))
//						      || (legalRepresentativeHistory.getSecondDocumentType()!=null && legalRepresentativeHistory.getSecondDocumentType().equals(legalRepresentativeTO.getDocumentType()) &&
//						    		  legalRepresentativeHistory.getSecondDocumentNumber()!=null && legalRepresentativeHistory.getSecondDocumentNumber().equals(legalRepresentativeTO.getDocumentNumber()))){
//						
//							   	Object[] bodyData = new Object[0];
//							   	
//							   	JSFUtilities.showMessageOnDialog(GeneralConstants.LBL_HEADER_ALERT, PropertiesUtilities.getValidationMessage(
//														GeneralPropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE,bodyData));
//								JSFUtilities.showSimpleValidationDialog();
//								
//							   
//							   JSFUtilities
//								.addContextMessage(
//										idFormClient+":tabView:"+idComponent,
//										FacesMessage.SEVERITY_ERROR,
//										PropertiesUtilities
//														.getValidationMessage(
//														GeneralPropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE,
//														bodyData),
//										PropertiesUtilities
//														.getValidationMessage(
//														GeneralPropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE,
//														bodyData));
//
//							   legalRepresentativeHistory.setDocumentNumber(null); 
//							   return;
//						   }
					     
					      if((legalRepresentativeHistory.getDocumentType().equals(legalRepresentativeTO.getDocumentType()) &&
					    	  legalRepresentativeHistory.getDocumentNumber().equals(legalRepresentativeTO.getDocumentNumber())
					    	   //&& (legalRepresentativeHistory.getDocumentSource()!=null && legalRepresentativeHistory.getDocumentSource().equals(legalRepresentativeTO.getEmissionSource()))	  
							   	&& (legalRepresentativeHistory.getDocumentType().equals(DocumentType.CI.getCode())))
						      || (legalRepresentativeHistory.getSecondDocumentType()!=null && legalRepresentativeHistory.getSecondDocumentType().equals(legalRepresentativeTO.getDocumentType()) &&
						    		  legalRepresentativeHistory.getSecondDocumentNumber()!=null && legalRepresentativeHistory.getSecondDocumentNumber().equals(legalRepresentativeTO.getDocumentNumber()))
						    	){

					    	  	Object[] bodyData = new Object[0];
							   	
							   	JSFUtilities.showMessageOnDialog(GeneralConstants.LBL_HEADER_ALERT, PropertiesUtilities.getValidationMessage(
														GeneralPropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE,bodyData));
								JSFUtilities.showSimpleValidationDialog();
								
							    JSFUtilities
								.addContextMessage(
										idFormClient+":tabView:"+idComponent,
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities
														.getValidationMessage(
														GeneralPropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE,
														bodyData),
										PropertiesUtilities
														.getValidationMessage(
														GeneralPropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE,
														bodyData));

							   legalRepresentativeHistory.setDocumentNumber(null);
							   newDocumentNumber = GeneralConstants.EMPTY_STRING;
							   return;
					      }
					   }
						
			
			legalRepresentativeTO.setFlagExportDataHolder(false);
			legalRepresentativeTO.setFlagExportDataRepresentative(false);
		    
		   if(documentValidator.validateFormatDocumentNumber(legalRepresentativeHistory.getDocumentType(),legalRepresentativeHistory.getDocumentNumber(),idFormClient+":tabView:"+idComponent)){
					if(legalRepresentativeHistory.getDocumentType()!=null && 
							legalRepresentativeHistory.getDocumentType().equals(DocumentType.CI.getCode()) &&
							legalRepresentativeHistory.getDocumentNumber()!=null 
							//&& legalRepresentativeHistory.getDocumentSource()!=null
							){
							//legalRepresentativeHistory.getFirstLastName()!=null){
					 
					  LegalRepresentative auxLegalRepresentative = new LegalRepresentative();
					  auxLegalRepresentative.setDocumentType(legalRepresentativeHistory.getDocumentType());
					  auxLegalRepresentative.setDocumentNumber(legalRepresentativeHistory.getDocumentNumber());
					  auxLegalRepresentative.setDocumentSource(legalRepresentativeHistory.getDocumentSource());
					  auxLegalRepresentative.setFirstLastName(legalRepresentativeHistory.getFirstLastName());
					  
					  
					  auxLegalRepresentative = accountsFacade.getLegalRepresentativeByDocumentTypeAndDocumentNumberServiceFacade(auxLegalRepresentative);
					  
					  if(Validations.validateIsNullOrEmpty(legalRepresentativeHistory.getRepresentativeClass())) {
						  legalRepresentativeHistory.setDocumentNumber(null);
						  newDocumentNumber = GeneralConstants.EMPTY_STRING;
						  Object[] bodyData = new Object[0];
						  JSFUtilities.showMessageOnDialog(GeneralConstants.LBL_HEADER_ALERT, PropertiesUtilities.getValidationMessage(
													GeneralPropertiesConstants.ERROR_CLASS_REPRESENTATIVE_NULL,bodyData));
						  JSFUtilities.showSimpleValidationDialog();
						  return;
					  }
					  
					  if(auxLegalRepresentative!=null){
						  legalRepresentativeTO.setFlagExportDataHolder(false);
						  legalRepresentativeTO.setFlagExportDataRepresentative(true);
						    Object[] bodyData = new Object[1];
							bodyData[0] = auxLegalRepresentative.getRepresentativeDescriptionAux();
							JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM), 
							PropertiesUtilities.getValidationMessage(GeneralPropertiesConstants.EXIST_REPRESENTATIVE_INFORMATION_EXPORT,bodyData));
							JSFUtilities.executeJavascriptFunction("PF('cnfwExportData').show()");
					  }
					  else{
						  CommandButton cmbSave = (CommandButton)FacesContext.getCurrentInstance().getViewRoot().findComponent(idLegalRepresentativeComponent+":"+"btnSave");
						  cmbSave.setDisabled(false);
					  
					      Holder auxHolder = new Holder();
						  auxHolder.setDocumentType(legalRepresentativeHistory.getDocumentType());
						  auxHolder.setDocumentNumber(legalRepresentativeHistory.getDocumentNumber());
						  auxHolder.setDocumentSource(legalRepresentativeHistory.getDocumentSource());
						  auxHolder.setFirstLastName(legalRepresentativeHistory.getFirstLastName());
						  
						  auxHolder = accountsFacade.getHolderByDocumentTypeAndDocumentNumberServiceFacade(auxHolder);
						  
						  if(auxHolder!=null){
							    legalRepresentativeTO.setFlagExportDataHolder(true);
							    legalRepresentativeTO.setFlagExportDataRepresentative(false);
							    Object[] bodyData = new Object[1];
								bodyData[0] = auxHolder.getDescriptionHolder();
								JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM), 
								PropertiesUtilities.getValidationMessage(GeneralPropertiesConstants.EXIST_HOLDER_INFORMATION_EXPORT,bodyData));
								JSFUtilities.executeJavascriptFunction("PF('cnfwExportData').show()");
						  }
					  }
					  
				     }
				
					loadNewDocumentNumber();
			}
			else{
				legalRepresentativeHistory.setDocumentNumber(null);
				newDocumentNumber = GeneralConstants.EMPTY_STRING;
			}
			
		}
		catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}	
		
	}
	
	/**
	 * Validate types documents legal.
	 *
	 * @param idFormClient the id form client
	 * @param idComponent the id component
	 * @return true, if successful
	 */
	public boolean validateTypesDocumentsLegal(String idFormClient,String idComponent) {
		
		boolean flag = true;

		if (legalRepresentativeHistory.getSecondDocumentType()!=null && legalRepresentativeHistory.getDocumentType()!=null && legalRepresentativeHistory.getDocumentType().equals(
				legalRepresentativeHistory.getSecondDocumentType())) {
			

			if(!legalRepresentativeHistory.getSecondDocumentType().equals(DocumentType.DIO.getCode())){
				
				flag = false;
				
			JSFUtilities
					.addContextMessage(
							idFormClient+":tabView:"+idComponent,
							FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities
									.getValidationMessage(GeneralPropertiesConstants.ERROR_SAME_TYPE_DOCUMENT,null),
							PropertiesUtilities
									.getValidationMessage(GeneralPropertiesConstants.ERROR_SAME_TYPE_DOCUMENT,null));

			return false;
			}

		}

		return flag;

	}
	
	/**
	 * Validate type document legal.
	 *
	 * @param idFormClient the id form client
	 * @param idComponent the id component
	 */
	public void validateTypeDocumentLegal(String idFormClient,String idComponent) {

		
		if (validateTypesDocumentsLegal(idFormClient,idComponent) && legalRepresentativeHistory.getDocumentType()!=null) {

			legalRepresentativeHistory.setDocumentNumber(GeneralConstants.EMPTY_STRING);
			newDocumentNumber = GeneralConstants.EMPTY_STRING;
			
			documentTO = documentValidator.loadMaxLenghtType(legalRepresentativeHistory.getDocumentType());
			
			legalRepresentativeTO.setNumericTypeDocumentLegal(documentTO.isNumericTypeDocument());
			legalRepresentativeTO.setMaxLenghtDocumentNumberLegal(documentTO.getMaxLenghtDocumentNumber());
			
			legalRepresentativeHistory.setDocumentIssuanceDate(null);
			
			documentTO = documentValidator.validateEmissionRules(legalRepresentativeHistory.getDocumentType());
			
			
			if(legalRepresentativeHistory.getDocumentType()!=null && 
					(legalRepresentativeHistory.getDocumentType().equals(DocumentType.CI.getCode())
					|| legalRepresentativeHistory.getDocumentType().equals(DocumentType.CID.getCode())
					|| legalRepresentativeHistory.getDocumentType().equals(DocumentType.PAS.getCode())	
					|| legalRepresentativeHistory.getDocumentType().equals(DocumentType.CDN.getCode())
					|| legalRepresentativeHistory.getDocumentType().equals(DocumentType.RUC.getCode()))){
//				SelectOneMenu cmbDocumentSource = (SelectOneMenu)FacesContext.getCurrentInstance().getViewRoot().findComponent(idFormClient+":tabView:cmbDocumentSource");
//				cmbDocumentSource.setValid(true);
			    getLstDocumentSource();
			    if(legalRepresentativeHistory.getNationality()!=null 
						&& legalRepresentativeHistory.getNationality().equals(countryResidence)
						&& legalRepresentativeHistory.getIndResidence()!=null 
						&& legalRepresentativeHistory.getIndResidence().equals(BooleanType.YES.getCode())){
			    	legalRepresentativeHistory.setDocumentSource(departmentIssuance);
				}
				else{
					legalRepresentativeHistory.setDocumentSource(null);
				}
			}
			else{
				legalRepresentativeHistory.setDocumentSource(null);
				listDocumentSource = new ArrayList<ParameterTable>();
			}
			
		}
		else{
			legalRepresentativeHistory.setDocumentType(null);
		}

	}
	
	//Segun Issue 1050
		/**
		 * Cargando el numero de documento
		 */
		public void loadNewDocumentNumber(){
			
			if(legalRepresentativeHistory.getDocumentType()!=null && legalRepresentativeHistory.getDocumentNumber() != null && !legalRepresentativeHistory.getDocumentNumber().equalsIgnoreCase(GeneralConstants.EMPTY_STRING)){
				
				newDocumentNumber = legalRepresentativeHistory.getDocumentNumber();
				
				if(legalRepresentativeHistory.getDocumentType().equals(DocumentType.CIE.getCode())){
					newDocumentNumber = "E-" + legalRepresentativeHistory.getDocumentNumber();
				}
				if(legalRepresentativeHistory.getDocumentType().equals(DocumentType.DCD.getCode())){
					newDocumentNumber = "DCD" + legalRepresentativeHistory.getDocumentNumber();
				}
				if(legalRepresentativeHistory.getDocumentType().equals(DocumentType.DCC.getCode())){
					newDocumentNumber = "DCC" + legalRepresentativeHistory.getDocumentNumber();
				}
				if(legalRepresentativeHistory.getDocumentType().equals(DocumentType.DCR.getCode())){
					newDocumentNumber = "DCR" + legalRepresentativeHistory.getDocumentNumber();
				}
				
				if(legalRepresentativeHistory.getDocumentIssuanceDate() != null){
					
					if(legalRepresentativeHistory.getDocumentType().equals(DocumentType.DCD.getCode()) ||
						legalRepresentativeHistory.getDocumentType().equals(DocumentType.DCC.getCode()) ||
						legalRepresentativeHistory.getDocumentType().equals(DocumentType.DCR.getCode()) ||
						legalRepresentativeHistory.getDocumentType().equals(DocumentType.DCD.getCode()) ){
						
						newDocumentNumber = newDocumentNumber + legalRepresentativeHistory.getDocumentIssuanceDate()+"";
					}
				}
			}
		}
	
	
//	@LoggerAuditWeb
//	public boolean validateDocumentNumberByTypeRepresentative(boolean flagSecondDocument){
//		
//		Integer documentType=null;
//		String documentNumber=null;
//		
//		if(flagSecondDocument){
//			documentType=legalRepresentativeHistory.getSecondDocumentType();
//			documentNumber=legalRepresentativeHistory.getSecondDocumentNumber();
//		}
//		else if(!flagSecondDocument){
//			documentType=legalRepresentativeHistory.getDocumentType();
//			documentNumber=legalRepresentativeHistory.getDocumentNumber();
//		}
//		
//		if(DocumentType.RUC.getCode().equals(documentType)){
//			if(Validations.validateIsNotNullAndNotEmpty(documentNumber)){
//
//				
//				InstitutionInformation nitFilter = new InstitutionInformation();
//				try {
//					nitFilter.setDocumentType(documentType);
//					nitFilter.setDocumentNumber(documentNumber);					
//					
//					institutionResultFind = accountsFacade.findInstitutionInformationByIdServiceFacade(nitFilter);
//					
//					if(!Validations.validateIsNotNull(institutionResultFind)){
//						JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
//								PropertiesUtilities.getValidationMessage(GeneralPropertiesConstants.ERROR_NIT_NO_EXIST,null));
//						JSFUtilities.showSimpleValidationDialog();
//						
//						if(!flagSecondDocument){
//							legalRepresentativeHistory.setDocumentNumber(null);
//							legalRepresentativeHistory.setFullName(null);
//							}
//						else if(flagSecondDocument){
//							legalRepresentativeHistory.setSecondDocumentNumber(null);
//							legalRepresentativeHistory.setFullName(null);
//						}
//						
//						
//						return false;
//					}
//					else{
//						if(legalRepresentativeHistory.getPersonType().equals(PersonType.JURIDIC.getCode())){
//							legalRepresentativeHistory.setFullName(institutionResultFind.getCorporateName());
//						return true;
//						}
//					}
//				}
//				catch (Exception ex) {
//					excepcion.fire(new ExceptionToCatchEvent(ex));
//				}
//			
//			}
//		}
//		return true;
//
//		
//	}
	
	/**
 * Validate exist second type document and second number document representative.
 *
 * @param idFormClient the id form client
 * @param idComponent the id component
 */
@LoggerAuditWeb
	public void validateExistSecondTypeDocumentAndSecondNumberDocumentRepresentative(String idFormClient,String idComponent){
		
		try{
			
			if(legalRepresentativeHistory.getSecondDocumentType() != null && legalRepresentativeHistory.getSecondDocumentNumber() != null){
				newDocumentNumberSecondNationality = legalRepresentativeHistory.getSecondDocumentNumber();
				
				if(legalRepresentativeHistory.getSecondDocumentType().equals(DocumentType.CIE.getCode())){
					newDocumentNumberSecondNationality = "E-" + legalRepresentativeHistory.getSecondDocumentNumber();
				}
			}
			
			if(legalRepresentativeHistory.getSecondDocumentType()!=null && 
					!legalRepresentativeHistory.getSecondDocumentType().equals(DocumentType.DIO.getCode()) &&
					legalRepresentativeHistory.getSecondDocumentNumber()!=null && 
					   lstLegalRepresentativeHistory!=null && 
							   lstLegalRepresentativeHistory.size()>0){
						
					   for(int i=0;i<lstLegalRepresentativeHistory.size();i++){
						   
						   if((lstLegalRepresentativeHistory.get(i).getDocumentType().equals(legalRepresentativeHistory.getSecondDocumentType()) &&
								   lstLegalRepresentativeHistory.get(i).getDocumentNumber().equals(legalRepresentativeHistory.getSecondDocumentNumber()))
						      || (lstLegalRepresentativeHistory.get(i).getSecondDocumentType()!=null && lstLegalRepresentativeHistory.get(i).getSecondDocumentType().equals(legalRepresentativeHistory.getSecondDocumentType()) &&
						    		  lstLegalRepresentativeHistory.get(i).getSecondDocumentNumber()!=null && lstLegalRepresentativeHistory.get(i).getSecondDocumentNumber().equals(legalRepresentativeHistory.getSecondDocumentNumber()))
								      ){
						
							   	Object[] bodyData = new Object[0];
							   	
							   	JSFUtilities.showMessageOnDialog(GeneralConstants.LBL_HEADER_ALERT, PropertiesUtilities.getValidationMessage(
														GeneralPropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_EQUAL_REPRESENTATIVE,bodyData));
								JSFUtilities.showSimpleValidationDialog();
								
							   
							   JSFUtilities
								.addContextMessage(
										idFormClient+":tabView:"+idComponent,
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities
												.getValidationMessage(
														GeneralPropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_EQUAL_REPRESENTATIVE,
														bodyData),
										PropertiesUtilities
												.getValidationMessage(
														GeneralPropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_EQUAL_REPRESENTATIVE,
														bodyData));

							   legalRepresentativeHistory.setSecondDocumentNumber(null);	 
								return;
						   }
					   }
						
					}
			
			
			if(legalRepresentativeTO.getDocumentType()!=null && 
					   !legalRepresentativeTO.getDocumentType().equals(DocumentType.DIO.getCode()) &&		
					   legalRepresentativeTO.getDocumentNumber()!=null && 
							   legalRepresentativeHistory.getDocumentType()!=null && 
									   legalRepresentativeHistory.getDocumentNumber()!=null){
						
					       if((legalRepresentativeHistory.getDocumentType().equals(legalRepresentativeTO.getDocumentType()) &&
					    		   legalRepresentativeHistory.getDocumentNumber().equals(legalRepresentativeTO.getDocumentNumber()))
						      || (legalRepresentativeHistory.getSecondDocumentType()!=null && legalRepresentativeHistory.getSecondDocumentType().equals(legalRepresentativeTO.getDocumentType()) &&
						    		  legalRepresentativeHistory.getSecondDocumentNumber()!=null && legalRepresentativeHistory.getSecondDocumentNumber().equals(legalRepresentativeTO.getDocumentNumber()))
								      ){
						
							   	Object[] bodyData = new Object[0];
							   	
							   	JSFUtilities.showMessageOnDialog(GeneralConstants.LBL_HEADER_ALERT, PropertiesUtilities.getValidationMessage(
														GeneralPropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE,bodyData));
								JSFUtilities.showSimpleValidationDialog();
								
							   
							   JSFUtilities
								.addContextMessage(
										idFormClient+":tabView:"+idComponent,
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities
														.getValidationMessage(
														GeneralPropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE,
														bodyData),
										PropertiesUtilities
														.getValidationMessage(
														GeneralPropertiesConstants.ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE,
														bodyData));

							   legalRepresentativeHistory.setSecondDocumentNumber(null); 
								return;
						   }
					   }
						
					

			
			legalRepresentativeTO.setFlagExportDataHolder(false);
			legalRepresentativeTO.setFlagExportDataRepresentative(false);
		    
		    if(legalRepresentativeHistory.getSecondDocumentType()!=null && legalRepresentativeHistory.getDocumentType()!=null
					&& legalRepresentativeHistory.getDocumentNumber()!=null && legalRepresentativeHistory.getSecondDocumentNumber()!=null){
			
			if(legalRepresentativeHistory.getSecondDocumentType().equals(legalRepresentativeHistory.getDocumentType())
					&& legalRepresentativeHistory.getDocumentNumber().equals(legalRepresentativeHistory.getSecondDocumentNumber())){
				Object[] bodyData = new Object[0];
				
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), PropertiesUtilities.getValidationMessage(
										GeneralPropertiesConstants.ERROR_DOCUMENTS_TYPE_AND_NUMBER_EQUALS,bodyData));
				JSFUtilities.showSimpleValidationDialog();
				
				JSFUtilities
				.addContextMessage(
						idFormClient+":tabView:"+idComponent,
						FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities
								.getValidationMessage(
										GeneralPropertiesConstants.ERROR_DOCUMENTS_TYPE_AND_NUMBER_EQUALS,
										bodyData),
						PropertiesUtilities
								.getValidationMessage(
										GeneralPropertiesConstants.ERROR_DOCUMENTS_TYPE_AND_NUMBER_EQUALS,
										bodyData));

				legalRepresentativeHistory.setSecondDocumentNumber(null);
				
				return;
			}
			
			}
		    
			if(documentValidator.validateFormatDocumentNumber(legalRepresentativeHistory.getSecondDocumentType(),legalRepresentativeHistory.getSecondDocumentNumber(),idFormClient+":tabView:"+idComponent)){
				  if(DocumentType.CI.getCode().equals(legalRepresentativeHistory.getSecondDocumentType())){				  
					  LegalRepresentative auxLegalRepresentative = new LegalRepresentative();
					  auxLegalRepresentative.setSecondDocumentType(legalRepresentativeHistory.getSecondDocumentType());
					  auxLegalRepresentative.setSecondDocumentNumber(legalRepresentativeHistory.getSecondDocumentNumber());
					  auxLegalRepresentative = accountsFacade.getLegalRepresentativeBySecondDocumentTypeAndSecondDocumentNumberServiceFacade(auxLegalRepresentative);
					  
					  if(auxLegalRepresentative!=null){
						  legalRepresentativeTO.setFlagExportDataHolder(false);
						  legalRepresentativeTO.setFlagExportDataRepresentative(true);
						    Object[] bodyData = new Object[1];
							bodyData[0] = auxLegalRepresentative.getRepresentativeDescriptionAux();
							JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM), 
							PropertiesUtilities.getValidationMessage(GeneralPropertiesConstants.EXIST_REPRESENTATIVE_INFORMATION_EXPORT,bodyData));
							JSFUtilities.executeJavascriptFunction("PF('cnfwExportData').show()");
							return;
					  }
					  else{
						  CommandButton cmbSave = (CommandButton)FacesContext.getCurrentInstance().getViewRoot().findComponent(idLegalRepresentativeComponent+":"+"btnSave");
						  cmbSave.setDisabled(false);
					  }
				  
					  
					    
						  Holder auxHolder = new Holder();
						  auxHolder.setSecondDocumentType(legalRepresentativeHistory.getSecondDocumentType());
						  auxHolder.setSecondDocumentNumber(legalRepresentativeHistory.getSecondDocumentNumber());
						  auxHolder = accountsFacade.getHolderBySecondDocumentTypeAndSecondDocumentNumberServiceFacade(auxHolder);
						  
						  if(auxHolder!=null){
							    legalRepresentativeTO.setFlagExportDataHolder(true);
							    legalRepresentativeTO.setFlagExportDataRepresentative(false);
							    Object[] bodyData = new Object[1];
								bodyData[0] = auxHolder.getDescriptionHolder();
								JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM), 
								PropertiesUtilities.getValidationMessage(GeneralPropertiesConstants.EXIST_HOLDER_INFORMATION_EXPORT,bodyData));
								JSFUtilities.executeJavascriptFunction("PF('cnfwExportData').show()");
								return;
						  }
						  
						  
					  }
				 
			}
				
			else{
				   legalRepresentativeHistory.setSecondDocumentNumber(null);
			}
			
				
			
		}
		catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}	
	}

	
	/**
	 * Validate second type document legal.
	 *
	 * @param idFormClient the id form client
	 * @param idComponent the id component
	 */
	public void validateSecondTypeDocumentLegal(String idFormClient,String idComponent) {

		if (validateTypesDocumentsLegal(idFormClient,idComponent)) {

			documentTO = documentValidator.loadMaxLenghtType(legalRepresentativeHistory.getSecondDocumentType());
			legalRepresentativeTO.setNumericSecondTypeDocumentLegal(documentTO.isNumericTypeDocument());
			legalRepresentativeTO.setMaxLenghtSecondDocumentNumberLegal(documentTO.getMaxLenghtDocumentNumber());
			legalRepresentativeHistory.setSecondDocumentNumber(GeneralConstants.EMPTY_STRING);
			newDocumentNumberSecondNationality = null;
		}
		else{
			legalRepresentativeHistory.setSecondDocumentType(null);
		}
	}

	/**
	 * Action listener representative class.
	 */
	public void actionListenerRepresentativeClass() {
		
		legalRepresentativeTO.setDisabledCmbPostalCountryRepresentative(false);
		legalRepresentativeTO.setDisabledCmbPostalProvinceRepresentative(false);
		legalRepresentativeTO.setDisabledCmbPostalDepartamentRepresentative(false);
		legalRepresentativeTO.setDisabledInputPostalAddressRepresentative(false);		
		legalRepresentativeTO.setDisabledRepresentativeNationality(false);
		legalRepresentativeTO.setDisabledRepresentativeResident(false);
		if(legalRepresentativeHistory.getNationality() != null && legalRepresentativeHistory.getNationality() != 18156) {
			legalRepresentativeTO.setDisabledCountryResidentRepresentative(false);
		}else {
			legalRepresentativeTO.setDisabledCountryResidentRepresentative(true);
		}
		legalRepresentativeTO.setFlagCopyLegalRepresentative(false);
		legalRepresentativeTO.setHolderPersonType(legalRepresentativeHistory.getPersonType());
		if(legalRepresentativeHistory.getRepresentativeClass() != null) {
			legalRepresentativeTO.setHolderJuridicClassType(legalRepresentativeHistory.getRepresentativeClass());
		}
		listRepresentativeClass = new ArrayList<ParameterTable>();
		getLstRepresentativeClassType();
		legalRepresentativeTO.setFlagCmbRepresentativeClass(false);
		ParameterTable auxParameterTable;

		if (legalRepresentativeHistory.getPersonType()!=null  && legalRepresentativeHistory.getPersonType().equals(PersonType.NATURAL.getCode())) {
			
			legalRepresentativeHistory = new LegalRepresentativeHistory();
			legalRepresentativeHistory.setPersonType(PersonType.NATURAL.getCode());
			legalRepresentativeHistory.setBeginningPeriod(null);
			legalRepresentativeHistory.setEndingPeriod(null);
			legalRepresentativeTO.setFlagRepresentativeIsPEP(false);
			legalRepresentativeTO.setFlagRepresentativeNaturalPerson(true);
			legalRepresentativeTO.setFlagRepresentativeJuridicPerson(false);
			
			if (legalRepresentativeTO.isFlagIndicatorMinorHolder()&& legalRepresentativeTO.getHolderPersonType().equals(PersonType.NATURAL.getCode())) {
				
				legalRepresentativeTO.setFlagCmbRepresentativeClass(false);
				auxParameterTable = new ParameterTable();
				List<ParameterTable> auxListPresentantiveClass = listRepresentativeClass;
				listRepresentativeClass = new ArrayList<ParameterTable>();

				for (int i = 0; i < auxListPresentantiveClass.size(); i++) {
					if (auxListPresentantiveClass.get(i).getParameterTablePk().equals(RepresentativeClassType.FATHER.getCode())) {
						auxParameterTable = new ParameterTable();
						auxParameterTable.setParameterTablePk(auxListPresentantiveClass.get(i).getParameterTablePk());
						auxParameterTable.setParameterName(auxListPresentantiveClass.get(i).getParameterName());
						listRepresentativeClass.add(auxParameterTable);
					}
					if (auxListPresentantiveClass.get(i).getParameterTablePk().equals(RepresentativeClassType.TUTOR_LEGAL.getCode())) {
						auxParameterTable = new ParameterTable();
						auxParameterTable.setParameterTablePk(auxListPresentantiveClass.get(i).getParameterTablePk());
						auxParameterTable.setParameterName(auxListPresentantiveClass.get(i).getParameterName());
						listRepresentativeClass.add(auxParameterTable);
					}
				}
			} else  {
				legalRepresentativeHistory.setRepresentativeClass(RepresentativeClassType.LEGAL.getCode());
				legalRepresentativeTO.setFlagCmbRepresentativeClass(true);
				loadByRepresentativeTypePerson();
			}
		} else if (legalRepresentativeHistory.getPersonType()!=null && legalRepresentativeHistory.getPersonType().equals(PersonType.JURIDIC.getCode())) {
			
			SelectOneMenu cmbRC  = (SelectOneMenu)	FacesContext.getCurrentInstance().getViewRoot().findComponent(idLegalRepresentativeComponent+":tabView:"+"cmbRepresentativeClass");
			getListDocumentTypeLegal();
			listDocumentTypeLegalResult = new ArrayList<ParameterTable>();
			
			if (listDocumentTypeLegal != null && listDocumentTypeLegal.size() > 0) {
				for (int i = 0; i < listDocumentTypeLegal.size(); i++) {
					auxParameterTable = new ParameterTable();
					if ((listDocumentTypeLegal.get(i).getParameterTablePk().equals(DocumentType.RUC.getCode()))) {
						auxParameterTable.setParameterTablePk(listDocumentTypeLegal.get(i).getParameterTablePk());
						auxParameterTable.setParameterName(listDocumentTypeLegal.get(i).getParameterName());
						listDocumentTypeLegalResult.add(auxParameterTable);
					}				
				}
			}
			
			legalRepresentativeHistory = new LegalRepresentativeHistory();
			legalRepresentativeHistory.setPersonType(PersonType.JURIDIC.getCode());

			if (legalRepresentativeTO.getHolderPersonType().equals(PersonType.JURIDIC.getCode())
					&& Validations.validateIsNotNull(legalRepresentativeTO.getHolderJuridicClassType())
					&& legalRepresentativeTO.getHolderJuridicClassType().equals(JuridicClassType.TRUST.getCode())) {
				
				legalRepresentativeTO.setFlagCmbRepresentativeClass(false);

				auxParameterTable = new ParameterTable();
				List<ParameterTable> auxListPresentantiveClass = listRepresentativeClass;
				listRepresentativeClass = new ArrayList<ParameterTable>();

				for (int i = 0; i < auxListPresentantiveClass.size(); i++) {
					if (auxListPresentantiveClass.get(i).getParameterTablePk().equals(RepresentativeClassType.LEGAL.getCode())) {
						auxParameterTable = new ParameterTable();
						auxParameterTable.setParameterTablePk(auxListPresentantiveClass.get(i).getParameterTablePk());
						auxParameterTable.setParameterName(auxListPresentantiveClass.get(i).getParameterName());
						listRepresentativeClass.add(auxParameterTable);
					}
					if (auxListPresentantiveClass.get(i).getParameterTablePk().equals(RepresentativeClassType.FIDUCIARY.getCode())) {
						auxParameterTable = new ParameterTable();
						auxParameterTable.setParameterTablePk(auxListPresentantiveClass.get(i).getParameterTablePk());
						auxParameterTable.setParameterName(auxListPresentantiveClass.get(i).getParameterName());
						listRepresentativeClass.add(auxParameterTable);
					}
				}
			} 
			else if(legalRepresentativeTO.getHolderPersonType().equals(PersonType.JURIDIC.getCode())
					&& !legalRepresentativeTO.getHolderJuridicClassType().equals(JuridicClassType.TRUST.getCode())) {

				legalRepresentativeHistory.setRepresentativeClass(RepresentativeClassType.LEGAL.getCode());
				legalRepresentativeTO.setFlagCmbRepresentativeClass(true);
				loadByRepresentativeTypePerson();
				cmbRC.setValid(true);
			}
			
			else {
				
				if(legalRepresentativeTO.getHolderPersonType().equals(PersonType.JURIDIC.getCode())){
				
					legalRepresentativeTO.setFlagCmbRepresentativeClass(false);
					auxParameterTable = new ParameterTable();
					List<ParameterTable> auxListPresentantiveClass = listRepresentativeClass;
					listRepresentativeClass = new ArrayList<ParameterTable>();
	
					for (int i = 0; i < auxListPresentantiveClass.size(); i++) {
						if (auxListPresentantiveClass.get(i).getParameterTablePk().equals(RepresentativeClassType.FATHER.getCode())) {
							auxParameterTable = new ParameterTable();
							auxParameterTable.setParameterTablePk(auxListPresentantiveClass.get(i).getParameterTablePk());
							auxParameterTable.setParameterName(auxListPresentantiveClass.get(i).getParameterName());
							listRepresentativeClass.add(auxParameterTable);
						}
						if (auxListPresentantiveClass.get(i).getParameterTablePk().equals(RepresentativeClassType.TUTOR_LEGAL.getCode())) {
							auxParameterTable = new ParameterTable();
							auxParameterTable.setParameterTablePk(auxListPresentantiveClass.get(i).getParameterTablePk());
							auxParameterTable.setParameterName(auxListPresentantiveClass.get(i).getParameterName());
							listRepresentativeClass.add(auxParameterTable);
						}
					}
				}
			}
			
			if(legalRepresentativeTO.getHolderPersonType().equals(PersonType.NATURAL.getCode())){
				legalRepresentativeHistory.setRepresentativeClass(RepresentativeClassType.LEGAL.getCode());
					legalRepresentativeTO.setFlagCmbRepresentativeClass(true);
					cmbRC.setValid(true);
					loadByRepresentativeTypePerson();
			}
			legalRepresentativeTO.setFlagRepresentativeJuridicPerson(true);
			legalRepresentativeTO.setFlagRepresentativeNaturalPerson(false);
		}
		else if(legalRepresentativeHistory.getPersonType().equals(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER))){
			cleanAllRepresentative();
		}
		
		legalRepresentativeHistory.setFlagActiveFilesByHolderImport(true);
	}
	
	/**
	 * Before save all representative.
	 */
	@LoggerAuditWeb
	public void beforeSaveAllRepresentative() {
		
		/*Issue 1231-
		 * Los procesos de adjuntar documentos que no seran obligatorios 
		 * para todos
		 * - Registro de Titular Jurídico
		 * - Registro de Titular Natural
		 * - Registro Cuenta Titular
    	 
		if(!userInfo.getUserAccountSession().isParticipantInstitucion()&&
				!validateAttachRepresentativeFile()){
			return;
		}
		* 
    	 */
		
		    if(indicatorHolder){
		    	Integer indicator=0;
		    	boolean flagFatherRepresentativeClass = false;
		    	if(legalRepresentativeTO.isFlagIndicatorMinorHolder()){
		    		for(int i=0;i<lstLegalRepresentativeHistory.size();i++){
		    			if(lstLegalRepresentativeHistory.get(i).getRepresentativeClass().equals(RepresentativeClassType.FATHER.getCode())){
		    				indicator++;
		    				flagFatherRepresentativeClass = true;
		    			}
		    		}
		    	}
		    	
		    	if(flagFatherRepresentativeClass && indicator.intValue()>1 && legalRepresentativeHistory.getRepresentativeClass().equals(RepresentativeClassType.FATHER.getCode())){
		    		Object[] bodyData = new Object[0];
					
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getValidationMessage(GeneralPropertiesConstants.WARNING_NECESSARY_TWO_FATHER_REPRESENTATIVE_CLASS,bodyData));
		
					JSFUtilities.showSimpleValidationDialog();
					
					return;
		
		    	}
		    	
		    	boolean flagTutorLegal  = false;
		    	indicator = 0;
		    	if(legalRepresentativeTO.isFlagIndicatorMinorHolder()){
		    		for(int i=0;i<lstLegalRepresentativeHistory.size();i++){
		    			if(lstLegalRepresentativeHistory.get(i).getRepresentativeClass().equals(RepresentativeClassType.TUTOR_LEGAL.getCode())){
		    				indicator++;
		    				flagTutorLegal = true;
		    			}
		    		}
		    	}
		    	
		    	if(flagTutorLegal && indicator>1 && legalRepresentativeHistory.getRepresentativeClass().equals(RepresentativeClassType.TUTOR_LEGAL.getCode())){
		    		Object[] bodyData = new Object[0];
					
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getValidationMessage(GeneralPropertiesConstants.WARNING_NECESSARY_TWO_TUTOR_LEGAL_REPRESENTATIVE_CLASS,bodyData));
		
					JSFUtilities.showSimpleValidationDialog();
					
					return;
		
		    	}
		    	
		    	Integer indicatorFather=0;
		    	Integer indicatorTutor=0;
		    	if(legalRepresentativeTO.isFlagIndicatorMinorHolder()){
		    		for(int i=0;i<lstLegalRepresentativeHistory.size();i++){
		    			if(lstLegalRepresentativeHistory.get(i).getRepresentativeClass().equals(RepresentativeClassType.FATHER.getCode())){
		    				indicatorFather++;
		    			}
		    			
		    			if(lstLegalRepresentativeHistory.get(i).getRepresentativeClass().equals(RepresentativeClassType.TUTOR_LEGAL.getCode())){
		    				indicatorTutor++;
		    			}
		    			
		    		}
		    		
		    		if(legalRepresentativeHistory.getRepresentativeClass().equals(RepresentativeClassType.FATHER.getCode())){
	    				indicatorFather++;
	    			}
		    		
		    		if(legalRepresentativeHistory.getRepresentativeClass().equals(RepresentativeClassType.TUTOR_LEGAL.getCode())){
	    				indicatorTutor++;
	    			}
		    		
		    		if(indicatorFather>0 && indicatorTutor>0){
		    			Object[] bodyData = new Object[0];
						
						JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
								PropertiesUtilities.getValidationMessage(GeneralPropertiesConstants.WARNING_FATHER_AND_TUTOR_LEGAL_REPRESENTATIVE_CLASS,bodyData));
			
						JSFUtilities.showSimpleValidationDialog();
						
						return;
			
		    		}
		    	}
		    }
			
			// Issue 1268
	    	// Validando los adjuntos de los representantes legales cuis y participantes
		    if(legalRepresentativeHistory.isFlagActiveFilesByHolderImport()){
				if(Validations.validateIsNull(lstRepresentativeFileHistory) || lstRepresentativeFileHistory.size()<1){
					Object[] bodyData = new Object[0];
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
													 PropertiesUtilities.getValidationMessage(GeneralPropertiesConstants.WARNING_FILE_REQUEST_REPRESENTATIVE_NO_COMPLETE,bodyData));
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
			}
		    
			loadRepresentativeDocumentType();
			legalRepresentativeHistory.setDocumentNumber(newDocumentNumber);
			legalRepresentativeHistory.setSecondDocumentNumber(newDocumentNumberSecondNationality);
			legalRepresentativeHistory.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			legalRepresentativeHistory.setStateRepreHistory(HolderRequestStateType.REGISTERED.getCode());
			legalRepresentativeHistory.setRepreHistoryType(HolderRequestType.CREATION.getCode());
			legalRepresentativeHistory.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
			legalRepresentativeHistory.setHolderRequest(legalRepresentativeTO.getHolderRequest());
			legalRepresentativeHistory.setRepresenteFileHistory(lstRepresentativeFileHistory);
			
			if (legalRepresentativeHistory.getSecondDocumentType() == null) {
				legalRepresentativeHistory.setSecondDocumentType(null);
				legalRepresentativeHistory.setSecondDocumentNumber(null);
			}
			
			if(legalRepresentativeHistory.getSecondDocumentType()==null){
				legalRepresentativeHistory.setSecondDocumentNumber(null);
			}
			
			if(legalRepresentativeHistory.getSecondDocumentNumber()==null){
				legalRepresentativeHistory.setSecondDocumentType(null);
			}
			newDocumentNumber = GeneralConstants.EMPTY_STRING;
			newDocumentNumberSecondNationality = GeneralConstants.EMPTY_STRING;

			if (Validations.validateIsNull(legalRepresentativeHistory.getEmail())
					|| Validations.validateIsEmpty(legalRepresentativeHistory.getEmail())) {
				InputText objInputText = (InputText) FacesContext
						.getCurrentInstance().getViewRoot()
						.findComponent(idLegalRepresentativeComponent+":tabView:"+"inputEmail");
				objInputText.setDisabled(true);
			}else{
				legalRepresentativeHistory.setEmail(legalRepresentativeHistory.getEmail().toLowerCase());
			}
			
			if(!legalRepresentativeTO.isFlagRepresentativeIsPEP()){
				
				legalRepresentativeHistory.setIndPEP(GeneralConstants.ZERO_VALUE_INTEGER);
				legalRepresentativeHistory.setCategory(null);
				legalRepresentativeHistory.setRole(null);
				legalRepresentativeHistory.setBeginningPeriod(null);
				legalRepresentativeHistory.setEndingPeriod(null);			
			}
			
			lstLegalRepresentativeHistory.add(legalRepresentativeHistory);
			
			JSFUtilities.executeJavascriptFunction("PF('registerRepresentativeWidget').hide()");
		
	}
	
	/**
	 * Before modify all representative.
	 */
	@LoggerAuditWeb
	public void beforeModifyAllRepresentative(){
		
		if(!validateAttachRepresentativeFile()){
			return;
		}
		
		if(indicatorHolder){
	    	Integer indicator=0;
	    	boolean flagFatherRepresentativeClass = false;
	    	if(legalRepresentativeTO.isFlagIndicatorMinorHolder()){
	    		for(int i=0;i<lstLegalRepresentativeHistory.size();i++){
	    			if(lstLegalRepresentativeHistory.get(i).getRepresentativeClass().equals(RepresentativeClassType.FATHER.getCode())){
	    				indicator++;
	    				flagFatherRepresentativeClass = true;
	    			}
	    		}
	    	}
	    	
	    	if(flagFatherRepresentativeClass && indicator.intValue()-1>1 && legalRepresentativeHistory.getRepresentativeClass().equals(RepresentativeClassType.FATHER.getCode())){
	    		Object[] bodyData = new Object[0];
				
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getValidationMessage(GeneralPropertiesConstants.WARNING_NECESSARY_TWO_FATHER_REPRESENTATIVE_CLASS,bodyData));
	
				JSFUtilities.showSimpleValidationDialog();
				
				return;
	
	    	}
	    	
	    	boolean flagTutorLegal  = false;
	    	indicator = 0;
	    	if(legalRepresentativeTO.isFlagIndicatorMinorHolder()){
	    		for(int i=0;i<lstLegalRepresentativeHistory.size();i++){
	    			if(lstLegalRepresentativeHistory.get(i).getRepresentativeClass().equals(RepresentativeClassType.TUTOR_LEGAL.getCode())){
	    				indicator++;
	    				flagTutorLegal = true;
	    			}
	    		}
	    	}
	    	
	    	if(flagTutorLegal && indicator.intValue()-1>1 && legalRepresentativeHistory.getRepresentativeClass().equals(RepresentativeClassType.TUTOR_LEGAL.getCode())){
	    		Object[] bodyData = new Object[0];
				
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getValidationMessage(GeneralPropertiesConstants.WARNING_NECESSARY_TWO_TUTOR_LEGAL_REPRESENTATIVE_CLASS,bodyData));
	
				JSFUtilities.showSimpleValidationDialog();
				
				return;
	
	    	}
	    	
	    	Integer indicatorFather=0;
	    	Integer indicatorTutor=0;
	    	if(legalRepresentativeTO.isFlagIndicatorMinorHolder()){
	    		for(int i=0;i<lstLegalRepresentativeHistory.size();i++){
	    			if(lstLegalRepresentativeHistory.get(i).getRepresentativeClass().equals(RepresentativeClassType.FATHER.getCode())){
	    				indicatorFather++;
	    			}
	    			
	    			if(lstLegalRepresentativeHistory.get(i).getRepresentativeClass().equals(RepresentativeClassType.TUTOR_LEGAL.getCode())){
	    				indicatorTutor++;
	    			}

	    		}
	    		
	    		if(legalRepresentativeHistory.getRepresentativeClass().equals(RepresentativeClassType.FATHER.getCode())){
    				indicatorFather++;
    			}
	    		
	    		if(legalRepresentativeHistory.getRepresentativeClass().equals(RepresentativeClassType.TUTOR_LEGAL.getCode())){
    				indicatorTutor++;
    			}
	    		
	    		if(indicatorFather>0 && indicatorTutor>0){
	    			Object[] bodyData = new Object[0];
					
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getValidationMessage(GeneralPropertiesConstants.WARNING_FATHER_AND_TUTOR_LEGAL_REPRESENTATIVE_CLASS,bodyData));
		
					JSFUtilities.showSimpleValidationDialog();
					
					return;
		
	    		}
	    	}
	    }
		
			loadRepresentativeDocumentType();
			
			legalRepresentativeHistory.setDocumentNumber(newDocumentNumber);
			legalRepresentativeHistory.setSecondDocumentNumber(newDocumentNumberSecondNationality);
			legalRepresentativeHistory.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			legalRepresentativeHistory
					.setStateRepreHistory(HolderRequestStateType.REGISTERED
							.getCode());
			legalRepresentativeHistory
					.setRepreHistoryType(HolderRequestType.CREATION.getCode());
			legalRepresentativeHistory
					.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST
							.getCode());
			legalRepresentativeHistory.setHolderRequest(legalRepresentativeTO.getHolderRequest());
			legalRepresentativeHistory
					.setRepresenteFileHistory(lstRepresentativeFileHistory);
			
			if(newDocumentNumber != null && !newDocumentNumber.equalsIgnoreCase(GeneralConstants.EMPTY_STRING)){
				legalRepresentativeHistory.setDocumentNumber(newDocumentNumber);
			}
			
			if(newDocumentNumberSecondNationality != null && !newDocumentNumberSecondNationality.equalsIgnoreCase(GeneralConstants.EMPTY_STRING)){
				legalRepresentativeHistory.setSecondDocumentNumber(newDocumentNumberSecondNationality);
			}			

			if (legalRepresentativeHistory.getSecondDocumentType() == null) {
				legalRepresentativeHistory.setSecondDocumentType(null);
				legalRepresentativeHistory.setSecondDocumentNumber(null);
			}
			
			if(legalRepresentativeHistory.getSecondDocumentType()==null){
				legalRepresentativeHistory.setSecondDocumentNumber(null);
			}
			
			if(legalRepresentativeHistory.getSecondDocumentNumber()==null){
				legalRepresentativeHistory.setSecondDocumentType(null);
			}

			if (Validations.validateIsNull(legalRepresentativeHistory
					.getEmail())
					|| Validations.validateIsEmpty(legalRepresentativeHistory
							.getEmail())) {
				InputText objInputText = (InputText) FacesContext
						.getCurrentInstance().getViewRoot()
						.findComponent(idLegalRepresentativeComponent+":tabView:"+"inputEmail");
				objInputText.setDisabled(true);
			}else{
				legalRepresentativeHistory.setEmail(legalRepresentativeHistory.getEmail().toLowerCase());
			}
			
			if(!legalRepresentativeTO.isFlagRepresentativeIsPEP()){
				legalRepresentativeHistory.setIndPEP(GeneralConstants.ZERO_VALUE_INTEGER);
				legalRepresentativeHistory.setCategory(null);
				legalRepresentativeHistory.setRole(null);
				legalRepresentativeHistory.setBeginningPeriod(null);
				legalRepresentativeHistory.setEndingPeriod(null);
			}

			int indicatorRow = legalRepresentativeTO.getRowIndicatorLegalRepresentative().intValue();
			lstLegalRepresentativeHistory.remove(indicatorRow);
			lstLegalRepresentativeHistory.add(indicatorRow,legalRepresentativeHistory);
			
			JSFUtilities.executeJavascriptFunction("PF('registerRepresentativeWidget').hide()");
	
	}

	/**
	 * Document attach representative file.
	 *
	 * @param event the event
	 */
	public void documentAttachRepresentativeFile(FileUploadEvent event){
		
		if(legalRepresentativeTO.getAttachFileRepresentativeSelected()!=null && !legalRepresentativeTO.getAttachFileRepresentativeSelected().equals(Integer.valueOf(GeneralConstants.NEGATIVE_ONE_INTEGER))){
			
			String fDisplayName=fUploadValidateFile(event.getFile(), null, null,getfUploadFileDocumentsTypes());
			
			if(fDisplayName!=null){
				 legalRepresentativeTO.setRepresentativeFileNameDisplay(fDisplayName);
			 }

			 for(int i=0;i<listRepresentativeDocumentType.size();i++){
				if(listRepresentativeDocumentType.get(i).getParameterTablePk().equals(legalRepresentativeTO.getAttachFileRepresentativeSelected())){
					
					RepresentativeFileHistory representativeFileHistory = new RepresentativeFileHistory();

					representativeFileHistory
							.setRepresentFileHistType(HolderReqFileHisType.CREATION
									.getCode());
					representativeFileHistory
							.setFilename(event.getFile().getFileName());
					representativeFileHistory
							.setFileRepresentative(event.getFile().getContents());
					representativeFileHistory.setRegistryUser(userInfo.getUserAccountSession().getUserName());
					representativeFileHistory
							.setRegistryDate(CommonsUtilities
									.currentDate());
					representativeFileHistory
							.setLegalRepresentativeHistory(legalRepresentativeHistory);
					representativeFileHistory
							.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST
									.getCode());
					representativeFileHistory
							.setStateFile(HolderFileStateType.REGISTERED
									.getCode());
					representativeFileHistory
							.setDocumentType(listRepresentativeDocumentType.get(i).getParameterTablePk());
					representativeFileHistory
							.setDescription(listRepresentativeDocumentType.get(i).getParameterName());
					
					if(legalRepresentativeTO.getViewOperationType().equals(ViewOperationsType.MODIFY.getCode())){
						representativeFileHistory.setWasModified(true);
					}
										
					insertRepresentativeFileItem(representativeFileHistory);

				}
			}
		}
			
	}
	
	/**
	 * Insert representative file item.
	 *
	 * @param representativeFileHistory the representative file history
	 */
	public void insertRepresentativeFileItem(RepresentativeFileHistory representativeFileHistory){
		
			for(int i=0;i<lstRepresentativeFileHistory.size();i++)
			{
				if(lstRepresentativeFileHistory.get(i).getDocumentType().equals(representativeFileHistory.getDocumentType())){
					
					Object[] bodyData = new Object[0];
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_ALERT_DOCUMENT_ALREADY_LOADED,
							bodyData));
					JSFUtilities.showSimpleValidationDialog();
					return;
				}	
			}
			
			lstRepresentativeFileHistory.add(representativeFileHistory);
			legalRepresentativeTO.setAttachFileRepresentativeSelected(null);
		
	}

	/**
	 * Onchange is representative pep.
	 */
	public void onchangeIsRepresentativePEP() {
		if (legalRepresentativeTO.isFlagRepresentativeIsPEP()) {
			
			legalRepresentativeHistory.setIndPEP(BooleanType.YES.getCode());
			legalRepresentativeHistory.setBeginningPeriod(CommonsUtilities
					.currentDate());
			legalRepresentativeHistory.setEndingPeriod(CommonsUtilities
					.currentDate());

		} else {
			
			legalRepresentativeHistory.setIndPEP(BooleanType.NO.getCode());
			legalRepresentativeHistory.setCategory(null);
			legalRepresentativeHistory.setRole(null);
			legalRepresentativeHistory.setBeginningPeriod(null);
			legalRepresentativeHistory.setEndingPeriod(null);
		}
	}
	
	/**
	 * Gets the list representative class.
	 *
	 * @return the list representative class
	 */
	public List<ParameterTable> getListRepresentativeClass() {
		return listRepresentativeClass;
	}

	/**
	 * Validate view operation register.
	 *
	 * @return true, if successful
	 */
	public boolean validateViewOperationRegister(){
		boolean flag=false;
		
		if(legalRepresentativeTO.getViewOperationType()!=null && legalRepresentativeTO.getViewOperationType().equals(ViewOperationsType.REGISTER.getCode())){
		   flag=true;
		}
		
		return flag;
	}
	
	/**
	 * Validate view operation modify.
	 *
	 * @return true, if successful
	 */
	public boolean validateViewOperationModify(){
		boolean flag=false;
		
		if(legalRepresentativeTO.getViewOperationType()!=null && legalRepresentativeTO.getViewOperationType().equals(ViewOperationsType.MODIFY.getCode())){
		   flag=true;
		}
		
		return flag;
	}
	
	/**
	 * Validate age representative.
	 *
	 * @return true, if successful
	 */
	public boolean validateAgeRepresentative() {

		boolean flag = true;
		
		try {

			if (legalRepresentativeHistory.getBirthDate() != null) {

				Calendar c = Calendar.getInstance();
				DateFormat df = new SimpleDateFormat("yyyy");
				c.setTime(df.parse("1900"));

				

				if (Integer.parseInt(df.format(legalRepresentativeHistory
						.getBirthDate())) < c.get(Calendar.YEAR)) {
					
					flag = false;
					
//					JSFUtilities
//							.addContextMessage(
//									idLegalRepresentativeComponent+":tabView:trCal",
//									FacesMessage.SEVERITY_ERROR,
//									PropertiesUtilities
//											.getValidationMessage(GeneralPropertiesConstants.ERROR_ADM_HOLDER_REQUEST_DATE_BIRTHDAY_DATE_ONENINEZEROZEO,null),
//									PropertiesUtilities
//											.getValidationMessage(GeneralPropertiesConstants.ERROR_ADM_HOLDER_REQUEST_DATE_BIRTHDAY_DATE_ONENINEZEROZEO,null));

					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), PropertiesUtilities.getValidationMessage(
							GeneralPropertiesConstants.ERROR_ADM_HOLDER_REQUEST_DATE_BIRTHDAY_DATE_ONENINEZEROZEO,null));
	
					
					JSFUtilities.showSimpleValidationDialog();

					return flag;
				}

				SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
				String birthday = formato.format(legalRepresentativeHistory
						.getBirthDate());

				int age = calculateAge(birthday);

				if (age < 18) {
					
					flag = false;
					legalRepresentativeHistory.setBirthDate(null);
//					JSFUtilities
//							.addContextMessage(
//									idLegalRepresentativeComponent+":tabView:trCal",
//									FacesMessage.SEVERITY_ERROR,
//									PropertiesUtilities
//											.getValidationMessage(GeneralPropertiesConstants.ERROR_REPRESENTATIVE_AGE_NO_MAJOR,null),
//									PropertiesUtilities
//											.getValidationMessage(GeneralPropertiesConstants.ERROR_REPRESENTATIVE_AGE_NO_MAJOR,null));

					JSFUtilities.showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							PropertiesUtilities.getValidationMessage(
							GeneralPropertiesConstants.ERROR_REPRESENTATIVE_AGE_NO_MAJOR,null));

					JSFUtilities.showSimpleValidationDialog();

				}

			}
			
			
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		
		return flag;

	}
	
	/**
	 * Calculate age.
	 *
	 * @param dateBirth the date birth
	 * @return the int
	 */
	public int calculateAge(String dateBirth) {

		Date dateToday = new Date();
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		String today = formato.format(dateToday);
		String[] dat1 = dateBirth.split("/");
		String[] dat2 = today.split("/");
		int years = Integer.parseInt(dat2[2]) - Integer.parseInt(dat1[2]) - 1;
		int month = Integer.parseInt(dat2[1]) - Integer.parseInt(dat1[1]);

		if (month < 0) {
			years = years + 0;
		} else if (month > 0) {
			years = years + 1;
		}

		if (month == 0) {
			int day = Integer.parseInt(dat2[0]) - Integer.parseInt(dat1[0]);
			if (day >= 0) {
				years = years + 1;
			}
		}
		return years;
	}
	
	/**
	 * Cargando el numero de documento
	 */
	public String getDocumentNumber(String documentNumber, Integer documentType){
		
		if(documentType.equals(DocumentType.CIE.getCode())){
			if(documentNumber.length() > 2){
				return documentNumber.substring(2, documentNumber.length());
			}
		}
		if(documentType.equals(DocumentType.DCD.getCode()) ||
		   documentType.equals(DocumentType.DCC.getCode()) ||
		   documentType.equals(DocumentType.DCR.getCode()) ){
			
			if(documentNumber.length() > 6){
				return documentNumber.substring(3, documentNumber.length()-4);
			}
		}
		return documentNumber;
	}
	
	
	
	/**
	 * Sets the list representative class.
	 *
	 * @param listRepresentativeClass the new list representative class
	 */
	public void setListRepresentativeClass(
			List<ParameterTable> listRepresentativeClass) {
		this.listRepresentativeClass = listRepresentativeClass;
	}

	/**
	 * Gets the list document type legal.
	 *
	 * @return the list document type legal
	 */
	public List<ParameterTable> getListDocumentTypeLegal() {
		return listDocumentTypeLegal;
	}

	/**
	 * Sets the list document type legal.
	 *
	 * @param listDocumentTypeLegal the new list document type legal
	 */
	public void setListDocumentTypeLegal(List<ParameterTable> listDocumentTypeLegal) {
		this.listDocumentTypeLegal = listDocumentTypeLegal;
	}

	/**
	 * Gets the list economic sector.
	 *
	 * @return the list economic sector
	 */
	public List<ParameterTable> getListEconomicSector() {
		return listEconomicSector;
	}

	/**
	 * Sets the list economic sector.
	 *
	 * @param listEconomicSector the new list economic sector
	 */
	public void setListEconomicSector(List<ParameterTable> listEconomicSector) {
		this.listEconomicSector = listEconomicSector;
	}

	/**
	 * Gets the legal representative history.
	 *
	 * @return the legal representative history
	 */
	public LegalRepresentativeHistory getLegalRepresentativeHistory() {
		return legalRepresentativeHistory;
	}

	/**
	 * Sets the legal representative history.
	 *
	 * @param legalRepresentativeHistory the new legal representative history
	 */
	public void setLegalRepresentativeHistory(
			LegalRepresentativeHistory legalRepresentativeHistory) {
		this.legalRepresentativeHistory = legalRepresentativeHistory;
	}

	/**
	 * Gets the legal representative history detail.
	 *
	 * @return the legal representative history detail
	 */
	public LegalRepresentativeHistory getLegalRepresentativeHistoryDetail() {
		return legalRepresentativeHistoryDetail;
	}

	/**
	 * Sets the legal representative history detail.
	 *
	 * @param legalRepresentativeHistoryDetail the new legal representative history detail
	 */
	public void setLegalRepresentativeHistoryDetail(
			LegalRepresentativeHistory legalRepresentativeHistoryDetail) {
		this.legalRepresentativeHistoryDetail = legalRepresentativeHistoryDetail;
	}

	/**
	 * Gets the legal representative to.
	 *
	 * @return the legal representative to
	 */
	public LegalRepresentativeTO getLegalRepresentativeTO() {
		return legalRepresentativeTO;
	}

	/**
	 * Sets the legal representative to.
	 *
	 * @param legalRepresentativeTO the new legal representative to
	 */
	public void setLegalRepresentativeTO(LegalRepresentativeTO legalRepresentativeTO) {
		this.legalRepresentativeTO = legalRepresentativeTO;
	}

	/**
	 * Gets the lst legal representative history.
	 *
	 * @return the lst legal representative history
	 */
	public List<LegalRepresentativeHistory> getLstLegalRepresentativeHistory() {
		return lstLegalRepresentativeHistory;
	}

	/**
	 * Sets the lst legal representative history.
	 *
	 * @param lstLegalRepresentativeHistory the new lst legal representative history
	 */
	public void setLstLegalRepresentativeHistory(
			List<LegalRepresentativeHistory> lstLegalRepresentativeHistory) {
		this.lstLegalRepresentativeHistory = lstLegalRepresentativeHistory;
	}

	/**
	 * Gets the lst representative file history.
	 *
	 * @return the lst representative file history
	 */
	public List<RepresentativeFileHistory> getLstRepresentativeFileHistory() {
		return lstRepresentativeFileHistory;
	}

	/**
	 * Sets the lst representative file history.
	 *
	 * @param lstRepresentativeFileHistory the new lst representative file history
	 */
	public void setLstRepresentativeFileHistory(
			List<RepresentativeFileHistory> lstRepresentativeFileHistory) {
		this.lstRepresentativeFileHistory = lstRepresentativeFileHistory;
	}

	/**
	 * Gets the list representative document type.
	 *
	 * @return the list representative document type
	 */
	public List<ParameterTable> getListRepresentativeDocumentType() {
		return listRepresentativeDocumentType;
	}

	/**
	 * Sets the list representative document type.
	 *
	 * @param listRepresentativeDocumentType the new list representative document type
	 */
	public void setListRepresentativeDocumentType(
			List<ParameterTable> listRepresentativeDocumentType) {
		this.listRepresentativeDocumentType = listRepresentativeDocumentType;
	}

	/**
	 * Gets the list geographic location.
	 *
	 * @return the list geographic location
	 */
	public List<GeographicLocation> getListGeographicLocation() {
		return listGeographicLocation;
	}

	/**
	 * Sets the list geographic location.
	 *
	 * @param listGeographicLocation the new list geographic location
	 */
	public void setListGeographicLocation(
			List<GeographicLocation> listGeographicLocation) {
		this.listGeographicLocation = listGeographicLocation;
	}

	/**
	 * Gets the list person type.
	 *
	 * @return the list person type
	 */
	public List<ParameterTable> getListPersonType() {
		return listPersonType;
	}

	/**
	 * Sets the list person type.
	 *
	 * @param listPersonType the new list person type
	 */
	public void setListPersonType(List<ParameterTable> listPersonType) {
		this.listPersonType = listPersonType;
	}

	/**
	 * Gets the list boolean.
	 *
	 * @return the list boolean
	 */
	public List<BooleanType> getListBoolean() {
		return listBoolean;
	}

	/**
	 * Sets the list boolean.
	 *
	 * @param listBoolean the new list boolean
	 */
	public void setListBoolean(List<BooleanType> listBoolean) {
		this.listBoolean = listBoolean;
	}

	/**
	 * Gets the list sex.
	 *
	 * @return the list sex
	 */
	public List<ParameterTable> getListSex() {
		return listSex;
	}

	/**
	 * Sets the list sex.
	 *
	 * @param listSex the new list sex
	 */
	public void setListSex(List<ParameterTable> listSex) {
		this.listSex = listSex;
	}

	/**
	 * Gets the list role pep.
	 *
	 * @return the list role pep
	 */
	public List<ParameterTable> getListRolePep() {
		return listRolePep;
	}

	/**
	 * Sets the list role pep.
	 *
	 * @param listRolePep the new list role pep
	 */
	public void setListRolePep(List<ParameterTable> listRolePep) {
		this.listRolePep = listRolePep;
	}

	/**
	 * Gets the list category pep.
	 *
	 * @return the list category pep
	 */
	public List<ParameterTable> getListCategoryPep() {
		return listCategoryPep;
	}

	/**
	 * Sets the list category pep.
	 *
	 * @param listCategoryPep the new list category pep
	 */
	public void setListCategoryPep(List<ParameterTable> listCategoryPep) {
		this.listCategoryPep = listCategoryPep;
	}

	/**
	 * Gets the list economic activity legal representative.
	 *
	 * @return the list economic activity legal representative
	 */
	public List<ParameterTable> getListEconomicActivityLegalRepresentative() {
		return listEconomicActivityLegalRepresentative;
	}

	/**
	 * Sets the list economic activity legal representative.
	 *
	 * @param listEconomicActivityLegalRepresentative the new list economic activity legal representative
	 */
	public void setListEconomicActivityLegalRepresentative(
			List<ParameterTable> listEconomicActivityLegalRepresentative) {
		this.listEconomicActivityLegalRepresentative = listEconomicActivityLegalRepresentative;
	}

	/**
	 * Gets the list municipality postal location representative.
	 *
	 * @return the list municipality postal location representative
	 */
	public List<GeographicLocation> getListMunicipalityPostalLocationRepresentative() {
		return listMunicipalityPostalLocationRepresentative;
	}

	/**
	 * Sets the list municipality postal location representative.
	 *
	 * @param listMunicipalityPostalLocationRepresentative the new list municipality postal location representative
	 */
	public void setListMunicipalityPostalLocationRepresentative(
			List<GeographicLocation> listMunicipalityPostalLocationRepresentative) {
		this.listMunicipalityLocationRepresentative = listMunicipalityPostalLocationRepresentative;
	}

	/**
	 * Gets the list province postal location representative.
	 *
	 * @return the list province postal location representative
	 */
	public List<GeographicLocation> getListProvincePostalLocationRepresentative() {
		return listProvincePostalLocationRepresentative;
	}

	/**
	 * Sets the list province postal location representative.
	 *
	 * @param listProvincePostalLocationRepresentative the new list province postal location representative
	 */
	public void setListProvincePostalLocationRepresentative(
			List<GeographicLocation> listProvincePostalLocationRepresentative) {
		this.listProvincePostalLocationRepresentative = listProvincePostalLocationRepresentative;
	}

	/**
	 * Gets the list province location representative.
	 *
	 * @return the list province location representative
	 */
	public List<GeographicLocation> getListProvinceLocationRepresentative() {
		return listProvinceLocationRepresentative;
	}

	/**
	 * Sets the list province location representative.
	 *
	 * @param listProvinceLocationRepresentative the new list province location representative
	 */
	public void setListProvinceLocationRepresentative(
			List<GeographicLocation> listProvinceLocationRepresentative) {
		this.listProvinceLocationRepresentative = listProvinceLocationRepresentative;
	}

	/**
	 * Gets the list municipality location representative.
	 *
	 * @return the list municipality location representative
	 */
	public List<GeographicLocation> getListMunicipalityLocationRepresentative() {
		return listMunicipalityLocationRepresentative;
	}

	/**
	 * Sets the list municipality location representative.
	 *
	 * @param listMunicipalityLocationRepresentative the new list municipality location representative
	 */
	public void setListMunicipalityLocationRepresentative(
			List<GeographicLocation> listMunicipalityLocationRepresentative) {
		this.listMunicipalityLocationRepresentative = listMunicipalityLocationRepresentative;
	}

	/**
	 * Gets the list document type legal result.
	 *
	 * @return the list document type legal result
	 */
	public List<ParameterTable> getListDocumentTypeLegalResult() {
		return listDocumentTypeLegalResult;
	}

	/**
	 * Sets the list document type legal result.
	 *
	 * @param listDocumentTypeLegalResult the new list document type legal result
	 */
	public void setListDocumentTypeLegalResult(
			List<ParameterTable> listDocumentTypeLegalResult) {
		this.listDocumentTypeLegalResult = listDocumentTypeLegalResult;
	}

	/**
	 * Gets the list second document type legal.
	 *
	 * @return the list second document type legal
	 */
	public List<ParameterTable> getListSecondDocumentTypeLegal() {
		return listSecondDocumentTypeLegal;
	}

	/**
	 * Sets the list second document type legal.
	 *
	 * @param listSecondDocumentTypeLegal the new list second document type legal
	 */
	public void setListSecondDocumentTypeLegal(
			List<ParameterTable> listSecondDocumentTypeLegal) {
		this.listSecondDocumentTypeLegal = listSecondDocumentTypeLegal;
	}

	/**
	 * Gets the lst legal representative file.
	 *
	 * @return the lst legal representative file
	 */
	public List<LegalRepresentativeFile> getLstLegalRepresentativeFile() {
		return lstLegalRepresentativeFile;
	}

	/**
	 * Sets the lst legal representative file.
	 *
	 * @param lstLegalRepresentativeFile the new lst legal representative file
	 */
	public void setLstLegalRepresentativeFile(
			List<LegalRepresentativeFile> lstLegalRepresentativeFile) {
		this.lstLegalRepresentativeFile = lstLegalRepresentativeFile;
	}

	/**
	 * Gets the institution result find.
	 *
	 * @return the institution result find
	 */
	public InstitutionInformation getInstitutionResultFind() {
		return institutionResultFind;
	}

	/**
	 * Sets the institution result find.
	 *
	 * @param institutionResultFind the new institution result find
	 */
	public void setInstitutionResultFind(InstitutionInformation institutionResultFind) {
		this.institutionResultFind = institutionResultFind;
	}

	/**
	 * Gets the pep person.
	 *
	 * @return the pep person
	 */
	public PepPerson getPepPerson() {
		return pepPerson;
	}

	/**
	 * Sets the pep person.
	 *
	 * @param pepPerson the new pep person
	 */
	public void setPepPerson(PepPerson pepPerson) {
		this.pepPerson = pepPerson;
	}

	/**
	 * Gets the id legal representative component.
	 *
	 * @return the id legal representative component
	 */
	public String getIdLegalRepresentativeComponent() {
		return idLegalRepresentativeComponent;
	}

	/**
	 * Sets the id legal representative component.
	 *
	 * @param idLegalRepresentativeComponent the new id legal representative component
	 */
	public void setIdLegalRepresentativeComponent(
			String idLegalRepresentativeComponent) {
		this.idLegalRepresentativeComponent = idLegalRepresentativeComponent;
	}

	/**
	 * Gets the list department location representative.
	 *
	 * @return the list department location representative
	 */
	public List<GeographicLocation> getListDepartmentLocationRepresentative() {
		return listDepartmentLocationRepresentative;
	}

	/**
	 * Sets the list department location representative.
	 *
	 * @param listDepartmentLocationRepresentative the new list department location representative
	 */
	public void setListDepartmentLocationRepresentative(
			List<GeographicLocation> listDepartmentLocationRepresentative) {
		this.listDepartmentLocationRepresentative = listDepartmentLocationRepresentative;
	}

	/**
	 * Gets the list department postal location representative.
	 *
	 * @return the list department postal location representative
	 */
	public List<GeographicLocation> getListDepartmentPostalLocationRepresentative() {
		return listDepartmentPostalLocationRepresentative;
	}

	/**
	 * Sets the list department postal location representative.
	 *
	 * @param listDepartmentPostalLocationRepresentative the new list department postal location representative
	 */
	public void setListDepartmentPostalLocationRepresentative(
			List<GeographicLocation> listDepartmentPostalLocationRepresentative) {
		this.listDepartmentPostalLocationRepresentative = listDepartmentPostalLocationRepresentative;
	}

	/**
	 * Gets the representative file name display.
	 *
	 * @return the representative file name display
	 */
	public String getRepresentativeFileNameDisplay() {
		return representativeFileNameDisplay;
	}

	/**
	 * Sets the representative file name display.
	 *
	 * @param representativeFileNameDisplay the new representative file name display
	 */
	public void setRepresentativeFileNameDisplay(
			String representativeFileNameDisplay) {
		this.representativeFileNameDisplay = representativeFileNameDisplay;
	}

	/**
	 * Checks if is disabled holder history.
	 *
	 * @return true, if is disabled holder history
	 */
	public boolean isDisabledHolderHistory() {
		return disabledHolderHistory;
	}

	/**
	 * Sets the disabled holder history.
	 *
	 * @param disabledHolderHistory the new disabled holder history
	 */
	public void setDisabledHolderHistory(boolean disabledHolderHistory) {
		this.disabledHolderHistory = disabledHolderHistory;
	}

	/**
	 * Checks if is disabled holder request.
	 *
	 * @return true, if is disabled holder request
	 */
	public boolean isDisabledHolderRequest() {
		return disabledHolderRequest;
	}

	/**
	 * Sets the disabled holder request.
	 *
	 * @param disabledHolderRequest the new disabled holder request
	 */
	public void setDisabledHolderRequest(boolean disabledHolderRequest) {
		this.disabledHolderRequest = disabledHolderRequest;
	}

	/**
	 * Gets the lst legal representative.
	 *
	 * @return the lst legal representative
	 */
	public List<LegalRepresentative> getLstLegalRepresentative() {
		return lstLegalRepresentative;
	}

	/**
	 * Sets the lst legal representative.
	 *
	 * @param lstLegalRepresentative the new lst legal representative
	 */
	public void setLstLegalRepresentative(
			List<LegalRepresentative> lstLegalRepresentative) {
		this.lstLegalRepresentative = lstLegalRepresentative;
	}

	/**
	 * Gets the represented entity.
	 *
	 * @return the represented entity
	 */
	public RepresentedEntity getRepresentedEntity() {
		return representedEntity;
	}

	/**
	 * Sets the represented entity.
	 *
	 * @param representedEntity the new represented entity
	 */
	public void setRepresentedEntity(RepresentedEntity representedEntity) {
		this.representedEntity = representedEntity;
	}

	/**
	 * Gets the legal representative.
	 *
	 * @return the legal representative
	 */
	public LegalRepresentative getLegalRepresentative() {
		return legalRepresentative;
	}

	/**
	 * Sets the legal representative.
	 *
	 * @param legalRepresentative the new legal representative
	 */
	public void setLegalRepresentative(LegalRepresentative legalRepresentative) {
		this.legalRepresentative = legalRepresentative;
	}

	/**
	 * Gets the pep person legal.
	 *
	 * @return the pep person legal
	 */
	public PepPerson getPepPersonLegal() {
		return pepPersonLegal;
	}

	/**
	 * Sets the pep person legal.
	 *
	 * @param pepPersonLegal the new pep person legal
	 */
	public void setPepPersonLegal(PepPerson pepPersonLegal) {
		this.pepPersonLegal = pepPersonLegal;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Gets the streamed content file.
	 *
	 * @return the streamed content file
	 */
	public StreamedContent getStreamedContentFile() {
		return streamedContentFile;
	}

	/**
	 * Sets the streamed content file.
	 *
	 * @param streamedContentFile the new streamed content file
	 */
	public void setStreamedContentFile(StreamedContent streamedContentFile) {
		this.streamedContentFile = streamedContentFile;
	}

	/**
	 * Checks if is flag modify legal representantive.
	 *
	 * @return true, if is flag modify legal representantive
	 */
	public boolean isFlagModifyLegalRepresentantive() {
		return flagModifyLegalRepresentantive;
	}

	/**
	 * Sets the flag modify legal representantive.
	 *
	 * @param flagModifyLegalRepresentantive the new flag modify legal representantive
	 */
	public void setFlagModifyLegalRepresentantive(
			boolean flagModifyLegalRepresentantive) {
		this.flagModifyLegalRepresentantive = flagModifyLegalRepresentantive;
	}

	/**
	 * Gets the lst legal representative history detail.
	 *
	 * @return the lst legal representative history detail
	 */
	public List<LegalRepresentativeHistory> getLstLegalRepresentativeHistoryDetail() {
		return lstLegalRepresentativeHistoryDetail;
	}

	/**
	 * Sets the lst legal representative history detail.
	 *
	 * @param lstLegalRepresentativeHistoryDetail the new lst legal representative history detail
	 */
	public void setLstLegalRepresentativeHistoryDetail(
			List<LegalRepresentativeHistory> lstLegalRepresentativeHistoryDetail) {
		this.lstLegalRepresentativeHistoryDetail = lstLegalRepresentativeHistoryDetail;
	}

	/**
	 * Gets the lst legal representative history detail before.
	 *
	 * @return the lst legal representative history detail before
	 */
	public List<LegalRepresentativeHistory> getLstLegalRepresentativeHistoryDetailBefore() {
		return lstLegalRepresentativeHistoryDetailBefore;
	}

	/**
	 * Sets the lst legal representative history detail before.
	 *
	 * @param lstLegalRepresentativeHistoryDetailBefore the new lst legal representative history detail before
	 */
	public void setLstLegalRepresentativeHistoryDetailBefore(
			List<LegalRepresentativeHistory> lstLegalRepresentativeHistoryDetailBefore) {
		this.lstLegalRepresentativeHistoryDetailBefore = lstLegalRepresentativeHistoryDetailBefore;
	}

	/**
	 * Gets the list document source.
	 *
	 * @return the list document source
	 */
	public List<ParameterTable> getListDocumentSource() {
		return listDocumentSource;
	}

	/**
	 * Sets the list document source.
	 *
	 * @param listDocumentSource the new list document source
	 */
	public void setListDocumentSource(List<ParameterTable> listDocumentSource) {
		this.listDocumentSource = listDocumentSource;
	}

	/**
	 * Gets the list geographic location without national country.
	 *
	 * @return the list geographic location without national country
	 */
	public List<GeographicLocation> getListGeographicLocationWithoutNationalCountry() {
		return listGeographicLocationWithoutNationalCountry;
	}

	/**
	 * Sets the list geographic location without national country.
	 *
	 * @param listGeographicLocationWithoutNationalCountry the new list geographic location without national country
	 */
	public void setListGeographicLocationWithoutNationalCountry(
			List<GeographicLocation> listGeographicLocationWithoutNationalCountry) {
		this.listGeographicLocationWithoutNationalCountry = listGeographicLocationWithoutNationalCountry;
	}

	/**
	 * Gets the document to.
	 *
	 * @return the document to
	 */
	public DocumentTO getDocumentTO() {
		return documentTO;
	}

	/**
	 * Sets the document to.
	 *
	 * @param documentTO the new document to
	 */
	public void setDocumentTO(DocumentTO documentTO) {
		this.documentTO = documentTO;
	}

	/**
	 * Gets the list document issuance date.
	 *
	 * @return the list document issuance date
	 */
	public LinkedHashMap<String, Integer> getListDocumentIssuanceDate() {
		return listDocumentIssuanceDate;
	}

	/**
	 * Sets the list document issuance date.
	 *
	 * @param listDocumentIssuanceDate the list document issuance date
	 */
	public void setListDocumentIssuanceDate(LinkedHashMap<String, Integer> listDocumentIssuanceDate) {
		this.listDocumentIssuanceDate = listDocumentIssuanceDate;
	}

	public Integer getIdHolderRequestCreation() {
		return IdHolderRequestCreation;
	}

	public String getNewDocumentNumber() {
		return newDocumentNumber;
	}

	public void setNewDocumentNumber(String newDocumentNumber) {
		this.newDocumentNumber = newDocumentNumber;
	}

	public String getNewDocumentNumberSecondNationality() {
		return newDocumentNumberSecondNationality;
	}

	public void setNewDocumentNumberSecondNationality(
			String newDocumentNumberSecondNationality) {
		this.newDocumentNumberSecondNationality = newDocumentNumberSecondNationality;
	}
	
}
