package com.pradera.core.component.corporateevents.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.component.corporateevents.facade.CoporateEventComponentFacade;
import com.pradera.core.component.corporateevents.to.CorporativeOperationTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.IssuerTO;
import com.pradera.core.component.helperui.view.IssuerHelperBean;
import com.pradera.core.component.helperui.view.SecuritiesHelperBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.corporatives.CapitalIncreaseMotive;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.type.CapitalIncreaeMotiveType;
import com.pradera.model.corporatives.type.CorporativeEventType;
import com.pradera.model.corporatives.type.CorporativeSecuritySourceType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.corporatives.type.NegotiationRankType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.ProgramAmortizationCoupon;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary. Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ImportanceEventBean.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , Jul 26, 2013
 */
@DepositaryWebBean
@LoggerCreateBean
public class ImportanceEventComponentBean extends GenericBaseBean implements
Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6925645773684710918L;
	
	@EJB
	private GeneralParametersFacade generalParameterFacade;
			
	private Security sourceSecurity;
	private Security targeSecurity;
	
	private CorporativeOperation selectedCorporativeOpertation;
	private Long idCorporativeOperationPk;
	
	@Inject
	private SecuritiesHelperBean securitiesHelperBean;
	
	@Inject
	private IssuerHelperBean issuerHelperBean;
	
	private Integer instrumentType;
	
	private Integer activeIndex;

	public Integer getActiveIndex() {
		return activeIndex;
	}

	public void setActiveIndex(Integer activeIndex) {
		this.activeIndex = activeIndex;
	}

	private String instrumentTypeDescription;	
	
	/** The event type description. */
	private String eventTypeDescription;

	/** The intrument type description. */
	private String intrumentTypeDescription;
	
	private Boolean renderIndTaxFactor;
	
	@EJB
	private CoporateEventComponentFacade coporateEventComponentFacade;
	
	private List<Security> lstTargetSecurities;
	
	@EJB
	private HelperComponentFacade allComponenteFacade;
	
	@EJB
	private GeneralParametersFacade generalParametersFacade;
	
	private Boolean disableTargetSecuritiesCombo;
	
	private List<ParameterTable> listInstrumentType;
	
	/** The list agreement type. */
	private List<ParameterTable> listAgreementType;

	/** The list currency type. */
	private List<ParameterTable> listCurrencyType;

	/** The list capital increase type. */
	private List<ParameterTable> listCapitalIncreaseType;

	/** The list delivery placetype. */
	private List<ParameterTable> listDeliveryPlacetype;
	
	/** The list corporative even type. */
	private List<CorporativeEventType> listCorporativeEvenType;

	/** The list negotiation rank type. */
	private List<NegotiationRankType> listNegotiationRankType;
	
	/** The render first negotiation date. */
	private Boolean renderFirstNegotiationDate;

	/** The render second negotiation date. */
	private Boolean renderSecondNegotiationDate;

	/** The render third negotiation date. */
	private Boolean renderThirdNegotiationDate;
	
	/** The revaluation excedent motive. */
	private CapitalIncreaseMotive revaluationExcedentMotive;

	/** The capital reexpresion motive. */
	private CapitalIncreaseMotive capitalReexpresionMotive;

	/** The reserve capitalization motive. */
	private CapitalIncreaseMotive reserveCapitalizationMotive;

	/** The utilities capitalization motive. */
	private CapitalIncreaseMotive utilitiesCapitalizationMotive;

	/** The fusion motive. */
	private CapitalIncreaseMotive fusionMotive;

	/** The result exposition motive. */
	private CapitalIncreaseMotive resultExpositionMotive;

	/** The others motive. */
	private CapitalIncreaseMotive othersMotive;

	/** The voluntier revaluation motive. */
	private CapitalIncreaseMotive voluntierRevaluationMotive;
	
	private Boolean renderTaxFactor;
	
	private Integer negotiationRank;
	
	private IssuerTO issuer;
	
	private Integer corporatvieSecuritySourceType;
	
	private String pageBack;
		
	/** The render Amortization. */
	private Boolean renderAmortization;
	
	/** The render Interest. */
	private Boolean renderInterest;

	
	@LoggerAuditWeb
	public String findById(){
		try {
			super.setParametersTableMap(new HashMap<Integer,Object>());
			loadDetailInfoListener();		
	 		setActiveIndex(0);
			return "importanceEventComponentDetail";
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}
		
	public String loadDetailInfoListener() throws ServiceException {
		
		sourceSecurity = new Security();
		targeSecurity = new Security();
		this.loadSelectedCorporativeOperation();
		securitiesHelperBean.getSecuritiesSearcher().setStateSecuritie(SecurityStateType.REGISTERED.getCode());
		securitiesHelperBean.getSecuritiesSearcher().setInstrumentType(selectedCorporativeOpertation.getSecurities().getInstrumentType());
		securitiesHelperBean.getSecuritiesSearcher().setIssuerCode(selectedCorporativeOpertation.getIssuer().getIdIssuerPk());
		instrumentType = selectedCorporativeOpertation.getSecurities().getInstrumentType();
		instrumentTypeDescription = InstrumentType.get(instrumentType).getValue();
		
		billParameterTableById(selectedCorporativeOpertation.getCorporativeEventType().getCorporativeEventType(),true);

		setViewOperationType(ViewOperationsType.DETAIL.getCode());
		if (Validations.validateIsNotNull(selectedCorporativeOpertation)) {
			if (Validations.validateIsNotNull(selectedCorporativeOpertation.getCorporativeEventType().getCorporativeEventType())) {
				//this.renderFields(selectedCorporativeOpertation.getCorporativeEventType());
				SecurityTO filter = new SecurityTO();
				filter.setIdIssuerCodePk(selectedCorporativeOpertation.getIssuer().getIdIssuerPk());
				this.findTargetSecurities(filter);
				this.initializeParameters(null);				
				this.determineEventTypeDescription();
				this.enableNegotiationDatesListener();
				this.initializeCapitalIncreaseMotive();
				this.loadIndicatorValues();
//				this.enableTaxFactorListener();
				this.determineDaysBetweenDatesListener();				
				loadSecuritiesInformation();
				loadAmountCoupon(selectedCorporativeOpertation);
				return "detail";
			}
		} else {
		}
		return "";
	}
	public void loadAmountCoupon(CorporativeOperation operation)throws ServiceException{
		renderInterest = Boolean.FALSE;
		renderAmortization = Boolean.FALSE;
		if(ImportanceEventType.INTEREST_PAYMENT.getCode().equals(operation.getCorporativeEventType().getCorporativeEventType())){
			renderInterest = Boolean.TRUE;
			List<Long> lstIdProgramInterestCoupon= coporateEventComponentFacade.getListIdProgramInterestCouponActual(operation.getSecurities().getIdSecurityCodePk(),operation.getDeliveryDate());
			Long idProgramInterestPk = (Long)lstIdProgramInterestCoupon.get(0);
			ProgramInterestCoupon objProgramInterestCoupon = (ProgramInterestCoupon)coporateEventComponentFacade.getProgramInterestCoupon(idProgramInterestPk);
			operation.setAmountCoupon(objProgramInterestCoupon.getCouponAmount());
		}
		if(ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(operation.getCorporativeEventType().getCorporativeEventType())){
			renderAmortization = Boolean.TRUE;
			List<Long> lstIdProgramInterestCoupon= coporateEventComponentFacade.getListIdProgramAmortizationCoupon(operation.getSecurities().getIdSecurityCodePk(),operation.getDeliveryDate());
			Long idProgramAmortizationPk = (Long)lstIdProgramInterestCoupon.get(0);
			ProgramAmortizationCoupon objProgramAmortizationCoupon = (ProgramAmortizationCoupon)coporateEventComponentFacade.getProgramAmortizationCoupon(idProgramAmortizationPk);
			operation.setAmountCoupon(objProgramAmortizationCoupon.getAmortizationAmount());
		}
	}
	public void loadSecuritiesInformation(){
			
			IssuerTO to =new IssuerTO();			
			to.setIssuerCode(selectedCorporativeOpertation.getIssuer().getIdIssuerPk());
			issuer = to;
			//issuerHelperBean.getIssuerSearcher().setIssuerDesc(selectedCorporativeOpertation.getIssuer().getDescription());
			corporatvieSecuritySourceType = CorporativeSecuritySourceType.SOURCE.getCode();
		
	}
	
	public void determineDaysBetweenDatesListener() {
		this.determineDaysBetweenDates(selectedCorporativeOpertation);		
	}
	
	private void determineDaysBetweenDates(CorporativeOperation operation) {
		if (Validations.validateIsNotNull(operation.getFirstStartDate())
				&& Validations.validateIsNotNull(operation.getFirstEndDate())) {
			Integer days = CommonsUtilities.getDaysBetween(
					operation.getFirstStartDate(), operation.getFirstEndDate());
			this.setNegotiationRank(days);			 
		} 
	}
	
	public void enableTaxFactorListener() {
		if (selectedCorporativeOpertation.isIndTaxValue()) {
			renderTaxFactor = Boolean.TRUE;
			BigDecimal value = this.findTaxFactor();
			selectedCorporativeOpertation.setTaxFactor(value);
		} else if (!selectedCorporativeOpertation.isIndTaxValue()) {
			renderTaxFactor = Boolean.FALSE; 
			selectedCorporativeOpertation.setTaxFactor(null);
		}
	}
	
	public BigDecimal findTaxFactor(){
		BigDecimal tax = BigDecimal.ZERO;		
		ParameterTableTO filter = new ParameterTableTO();
		filter.setState(ParameterTableStateType.REGISTERED.getCode());
		filter.setParameterTablePk(MasterTableType.PARAMETER_TABLE_IVA_TAX.getCode());
		List<ParameterTable> lstParameterTable= this.getParameters(filter);
		if(lstParameterTable!=null && lstParameterTable.size()>0){
			ParameterTable obj = (ParameterTable)lstParameterTable.get(0);
			if(obj.getIndicator1()!=null && 
					Validations.validateIsNotEmpty(obj.getIndicator1())){
				tax = new BigDecimal(obj.getIndicator1().toString());
			}
		}	
		return tax;
	}
	
	public void loadIndicatorValues() {
		
		if (selectedCorporativeOpertation.getIndRemanent() != null) {
			if (selectedCorporativeOpertation.getIndRemanent().equals(BooleanType.YES.getCode())) {
				selectedCorporativeOpertation.setIndRemanentValue(Boolean.TRUE);
			} else {
				selectedCorporativeOpertation.setIndRemanentValue(Boolean.FALSE);
			}
		}
		if (selectedCorporativeOpertation.getIndRound() != null) {
			if (selectedCorporativeOpertation.getIndRound().equals(BooleanType.YES.getCode())) {
				selectedCorporativeOpertation.setIndRoundValue(Boolean.TRUE);
			} else {
				selectedCorporativeOpertation.setIndRoundValue(Boolean.FALSE);
			}
		}

		if (selectedCorporativeOpertation.getIndTax() != null) {
			if (selectedCorporativeOpertation.getIndTax().equals(BooleanType.YES.getCode())) {
				selectedCorporativeOpertation.setIndTaxValue(Boolean.TRUE);
			} else {
				selectedCorporativeOpertation.setIndTaxValue(Boolean.FALSE);
			}
		}
	
	}
	
	public void initializeCapitalIncreaseMotive() {
 		revaluationExcedentMotive = new CapitalIncreaseMotive();
		capitalReexpresionMotive = new CapitalIncreaseMotive();
		reserveCapitalizationMotive = new CapitalIncreaseMotive();
		utilitiesCapitalizationMotive = new CapitalIncreaseMotive();
		fusionMotive = new CapitalIncreaseMotive();
		resultExpositionMotive = new CapitalIncreaseMotive();
		othersMotive = new CapitalIncreaseMotive();
		voluntierRevaluationMotive = new CapitalIncreaseMotive(); 
		revaluationExcedentMotive.setIncreaseMotive(CapitalIncreaeMotiveType.REVALUATION_EXCEDENT.getCode());
		capitalReexpresionMotive.setIncreaseMotive(CapitalIncreaeMotiveType.CAPITAL_REEXPRESION.getCode());
		reserveCapitalizationMotive.setIncreaseMotive(CapitalIncreaeMotiveType.RESERVE_CAPITALIZATION.getCode());
		utilitiesCapitalizationMotive.setIncreaseMotive(CapitalIncreaeMotiveType.UTILITIES_CAPITALIZATION.getCode());
		fusionMotive.setIncreaseMotive(CapitalIncreaeMotiveType.FUSION.getCode());
		resultExpositionMotive.setIncreaseMotive(CapitalIncreaeMotiveType.EXPOSITION_RESULT.getCode());
		othersMotive.setIncreaseMotive(CapitalIncreaeMotiveType.OTHERS.getCode());
		voluntierRevaluationMotive.setIncreaseMotive(CapitalIncreaeMotiveType.VOLUNTIER_REVALUATION.getCode());

	}
	
	public void enableNegotiationDatesListener() {
			if (Validations.validateIsNotNull(selectedCorporativeOpertation.getNegotiationRange())) {
				this.enableNegotiationDates(selectedCorporativeOpertation);
			}		
	}
	
	private void enableNegotiationDates(CorporativeOperation operation) {
		Long negotiationRange = operation.getNegotiationRange();
		if (Validations.validateIsNotNull(negotiationRange)) {
			if (negotiationRange.equals(NegotiationRankType.ONE.getCode())) {
				renderFirstNegotiationDate = Boolean.TRUE;
				renderSecondNegotiationDate = Boolean.FALSE;
				renderThirdNegotiationDate = Boolean.FALSE;
				operation.setSecondStartDate(null);
				operation.setSecondEndDate(null);
				operation.setThirdStartDate(null);
				operation.setThirdEndDate(null);
			} else if (negotiationRange.equals(NegotiationRankType.TWO.getCode())) {
				renderFirstNegotiationDate = Boolean.TRUE;
				renderSecondNegotiationDate = Boolean.TRUE;
				renderThirdNegotiationDate = Boolean.FALSE;
				operation.setThirdStartDate(null);
				operation.setThirdEndDate(null);
			} else if (negotiationRange.equals(NegotiationRankType.THREE.getCode())) {
				renderFirstNegotiationDate = Boolean.TRUE;
				renderSecondNegotiationDate = Boolean.TRUE;
				renderThirdNegotiationDate = Boolean.TRUE;
			} else {
				operation.setSecondStartDate(null);
				operation.setSecondEndDate(null);
				operation.setThirdStartDate(null);
				operation.setThirdEndDate(null);
				operation.setFirstEndDate(null);
				operation.setFirstStartDate(null);
				renderFirstNegotiationDate = Boolean.FALSE;
				renderSecondNegotiationDate = Boolean.FALSE;
				renderThirdNegotiationDate = Boolean.FALSE;
			}
		}

	}

	
	public void determineEventTypeDescription() {
		if (Validations.validateIsNotNull(selectedCorporativeOpertation)) {
			if (Validations.validateIsNotNull(selectedCorporativeOpertation.getSecurities())) {
				intrumentTypeDescription = InstrumentType.get(selectedCorporativeOpertation.getSecurities().getInstrumentType()).getValue();
			}

				eventTypeDescription = CorporativeEventType.RIGHTS_TO_DELIVER.getValue();

		}
	}
	
	public void findTargetSecurities(SecurityTO filter) {
		try {
			lstTargetSecurities = allComponenteFacade.searchSecuritiesByFilter(filter);
			disableTargetSecuritiesCombo = Boolean.FALSE;

		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	public void renderFields(Integer eventType) {
		if(ImportanceEventType.INTEREST_PAYMENT.getCode().equals(eventType)){
			//this.clearPanels();		
			
			//renderTaxFactor=Boolean.FALSE;
			renderIndTaxFactor=Boolean.TRUE;

			
		}
	}
		
	public void loadSelectedCorporativeOperation() {
		CorporativeOperationTO filterTO = new CorporativeOperationTO();

		filterTO.setId(idCorporativeOperationPk);

		
		filterTO.setNeedTargetSecurity(Boolean.FALSE);
//		if (Validations.validateIsNotNull(selectedCorporativeOpertation.getTargetSecurity())) {
//			filterTO.setNeedTargetSecurity(Boolean.TRUE);
//		}
		try {
			selectedCorporativeOpertation = coporateEventComponentFacade.findCorporativeOperationByFilter(filterTO);
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	public void initializeParameters(Integer state) {

		// create parameterTableTO's
		ParameterTableTO instrumentParamTo = new ParameterTableTO();
		ParameterTableTO typeAgreementParamTo = new ParameterTableTO();
		ParameterTableTO currencyTypeParamTo = new ParameterTableTO();
		ParameterTableTO increaseCapParamTo = new ParameterTableTO();
		ParameterTableTO deliveryPlaceParamTo = new ParameterTableTO();
 
		// Set parameterTableTO's values
		instrumentParamTo.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
		typeAgreementParamTo.setMasterTableFk(MasterTableType.AGREEMENT_TYPE.getCode());
		currencyTypeParamTo.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		increaseCapParamTo.setMasterTableFk(MasterTableType.INCREASE_CAPITAL_MOTIVE.getCode());
		deliveryPlaceParamTo.setMasterTableFk(MasterTableType.DELIVERY_PLACE.getCode());
		//		useFulNumberDayTo.setMasterTableFk(MasterTableType.USEFUL_NUMBER_DAYS
		//				.getCode());


		if(state!=null){
			typeAgreementParamTo.setState(state);
		}

		listInstrumentType = this.getParameters(instrumentParamTo);
		listAgreementType = this.getParameters(typeAgreementParamTo);
		listCurrencyType = this.getParameters(currencyTypeParamTo);
		listCapitalIncreaseType = this.getParameters(increaseCapParamTo);
		listDeliveryPlacetype = this.getParameters(deliveryPlaceParamTo);
		//		usefulDays = this.getParameters(useFulNumberDayTo).get(0);

		listCorporativeEvenType = CorporativeEventType.list;
		listNegotiationRankType = NegotiationRankType.list;
	}
	
	private List<ParameterTable> getParameters(ParameterTableTO filter) {
		try {
			return generalParametersFacade
					.getListParameterTableServiceBean(filter);
		} catch (ServiceException se) {
			excepcion.fire(new ExceptionToCatchEvent(se));
		}
		return null;

	}
	
	public void billParameterTableById(Integer parameterPk, boolean isObj) throws ServiceException{
		ParameterTable pTable = generalParameterFacade.getParamDetailServiceFacade(parameterPk);
		if(pTable!=null){
			if(isObj){
				super.getParametersTableMap().put(pTable.getParameterTablePk()  , pTable);
			}else{
				super.getParametersTableMap().put(pTable.getParameterTablePk()  , pTable.getParameterName());
			}
		}
	}

	public Security getSourceSecurity() {
		return sourceSecurity;
	}

	public void setSourceSecurity(Security sourceSecurity) {
		this.sourceSecurity = sourceSecurity;
	}

	public Security getTargeSecurity() {
		return targeSecurity;
	}

	public void setTargeSecurity(Security targeSecurity) {
		this.targeSecurity = targeSecurity;
	}

	public CorporativeOperation getSelectedCorporativeOpertation() {
		return selectedCorporativeOpertation;
	}

	public void setSelectedCorporativeOpertation(
			CorporativeOperation selectedCorporativeOpertation) {
		this.selectedCorporativeOpertation = selectedCorporativeOpertation;
	}

	public Integer getInstrumentType() {
		return instrumentType;
	}

	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	public String getInstrumentTypeDescription() {
		return instrumentTypeDescription;
	}

	public void setInstrumentTypeDescription(String instrumentTypeDescription) {
		this.instrumentTypeDescription = instrumentTypeDescription;
	}

	public String getEventTypeDescription() {
		return eventTypeDescription;
	}

	public void setEventTypeDescription(String eventTypeDescription) {
		this.eventTypeDescription = eventTypeDescription;
	}

	public String getIntrumentTypeDescription() {
		return intrumentTypeDescription;
	}

	public void setIntrumentTypeDescription(String intrumentTypeDescription) {
		this.intrumentTypeDescription = intrumentTypeDescription;
	}

	public Boolean getRenderIndTaxFactor() {
		return renderIndTaxFactor;
	}

	public void setRenderIndTaxFactor(Boolean renderIndTaxFactor) {
		this.renderIndTaxFactor = renderIndTaxFactor;
	}

	public List<Security> getLstTargetSecurities() {
		return lstTargetSecurities;
	}

	public void setLstTargetSecurities(List<Security> lstTargetSecurities) {
		this.lstTargetSecurities = lstTargetSecurities;
	}

	public Boolean getDisableTargetSecuritiesCombo() {
		return disableTargetSecuritiesCombo;
	}

	public void setDisableTargetSecuritiesCombo(Boolean disableTargetSecuritiesCombo) {
		this.disableTargetSecuritiesCombo = disableTargetSecuritiesCombo;
	}

	public List<ParameterTable> getListInstrumentType() {
		return listInstrumentType;
	}

	public void setListInstrumentType(List<ParameterTable> listInstrumentType) {
		this.listInstrumentType = listInstrumentType;
	}

	public List<ParameterTable> getListAgreementType() {
		return listAgreementType;
	}

	public void setListAgreementType(List<ParameterTable> listAgreementType) {
		this.listAgreementType = listAgreementType;
	}

	public List<ParameterTable> getListCurrencyType() {
		return listCurrencyType;
	}

	public void setListCurrencyType(List<ParameterTable> listCurrencyType) {
		this.listCurrencyType = listCurrencyType;
	}

	public List<ParameterTable> getListCapitalIncreaseType() {
		return listCapitalIncreaseType;
	}

	public void setListCapitalIncreaseType(
			List<ParameterTable> listCapitalIncreaseType) {
		this.listCapitalIncreaseType = listCapitalIncreaseType;
	}

	public List<ParameterTable> getListDeliveryPlacetype() {
		return listDeliveryPlacetype;
	}

	public void setListDeliveryPlacetype(List<ParameterTable> listDeliveryPlacetype) {
		this.listDeliveryPlacetype = listDeliveryPlacetype;
	}

	public List<CorporativeEventType> getListCorporativeEvenType() {
		return listCorporativeEvenType;
	}

	public void setListCorporativeEvenType(
			List<CorporativeEventType> listCorporativeEvenType) {
		this.listCorporativeEvenType = listCorporativeEvenType;
	}

	public List<NegotiationRankType> getListNegotiationRankType() {
		return listNegotiationRankType;
	}

	public void setListNegotiationRankType(
			List<NegotiationRankType> listNegotiationRankType) {
		this.listNegotiationRankType = listNegotiationRankType;
	}

	public Boolean getRenderFirstNegotiationDate() {
		return renderFirstNegotiationDate;
	}

	public void setRenderFirstNegotiationDate(Boolean renderFirstNegotiationDate) {
		this.renderFirstNegotiationDate = renderFirstNegotiationDate;
	}

	public Boolean getRenderSecondNegotiationDate() {
		return renderSecondNegotiationDate;
	}

	public void setRenderSecondNegotiationDate(Boolean renderSecondNegotiationDate) {
		this.renderSecondNegotiationDate = renderSecondNegotiationDate;
	}

	public Boolean getRenderThirdNegotiationDate() {
		return renderThirdNegotiationDate;
	}

	public void setRenderThirdNegotiationDate(Boolean renderThirdNegotiationDate) {
		this.renderThirdNegotiationDate = renderThirdNegotiationDate;
	}

	public CapitalIncreaseMotive getRevaluationExcedentMotive() {
		return revaluationExcedentMotive;
	}

	public void setRevaluationExcedentMotive(
			CapitalIncreaseMotive revaluationExcedentMotive) {
		this.revaluationExcedentMotive = revaluationExcedentMotive;
	}

	public CapitalIncreaseMotive getCapitalReexpresionMotive() {
		return capitalReexpresionMotive;
	}

	public void setCapitalReexpresionMotive(
			CapitalIncreaseMotive capitalReexpresionMotive) {
		this.capitalReexpresionMotive = capitalReexpresionMotive;
	}

	public CapitalIncreaseMotive getReserveCapitalizationMotive() {
		return reserveCapitalizationMotive;
	}

	public void setReserveCapitalizationMotive(
			CapitalIncreaseMotive reserveCapitalizationMotive) {
		this.reserveCapitalizationMotive = reserveCapitalizationMotive;
	}

	public CapitalIncreaseMotive getUtilitiesCapitalizationMotive() {
		return utilitiesCapitalizationMotive;
	}

	public void setUtilitiesCapitalizationMotive(
			CapitalIncreaseMotive utilitiesCapitalizationMotive) {
		this.utilitiesCapitalizationMotive = utilitiesCapitalizationMotive;
	}

	public CapitalIncreaseMotive getFusionMotive() {
		return fusionMotive;
	}

	public void setFusionMotive(CapitalIncreaseMotive fusionMotive) {
		this.fusionMotive = fusionMotive;
	}

	public CapitalIncreaseMotive getResultExpositionMotive() {
		return resultExpositionMotive;
	}

	public void setResultExpositionMotive(
			CapitalIncreaseMotive resultExpositionMotive) {
		this.resultExpositionMotive = resultExpositionMotive;
	}

	public CapitalIncreaseMotive getOthersMotive() {
		return othersMotive;
	}

	public void setOthersMotive(CapitalIncreaseMotive othersMotive) {
		this.othersMotive = othersMotive;
	}

	public CapitalIncreaseMotive getVoluntierRevaluationMotive() {
		return voluntierRevaluationMotive;
	}

	public void setVoluntierRevaluationMotive(
			CapitalIncreaseMotive voluntierRevaluationMotive) {
		this.voluntierRevaluationMotive = voluntierRevaluationMotive;
	}

	public Boolean getRenderTaxFactor() {
		return renderTaxFactor;
	}

	public void setRenderTaxFactor(Boolean renderTaxFactor) {
		this.renderTaxFactor = renderTaxFactor;
	}

	public Integer getNegotiationRank() {
		return negotiationRank;
	}

	public void setNegotiationRank(Integer negotiationRank) {
		this.negotiationRank = negotiationRank;
	}

	public IssuerTO getIssuer() {
		return issuer;
	}

	public void setIssuer(IssuerTO issuer) {
		this.issuer = issuer;
	}

	public Integer getCorporatvieSecuritySourceType() {
		return corporatvieSecuritySourceType;
	}

	public void setCorporatvieSecuritySourceType(
			Integer corporatvieSecuritySourceType) {
		this.corporatvieSecuritySourceType = corporatvieSecuritySourceType;
	}

	public Long getIdCorporativeOperationPk() {
		return idCorporativeOperationPk;
	}

	public void setIdCorporativeOperationPk(Long idCorporativeOperationPk) {
		this.idCorporativeOperationPk = idCorporativeOperationPk;
	}

	public String actionBack(){
		System.out.println("klk");
		setActiveIndex(4);
		return getPageBack();
	}
	
	public String getPageBack() {
		return pageBack;
	}

	public void setPageBack(String pageBack) {
		this.pageBack = pageBack;
	}

	public Boolean getRenderAmortization() {
		return renderAmortization;
	}

	public void setRenderAmortization(Boolean renderAmortization) {
		this.renderAmortization = renderAmortization;
	}

	public Boolean getRenderInterest() {
		return renderInterest;
	}

	public void setRenderInterest(Boolean renderInterest) {
		this.renderInterest = renderInterest;
	}

	
	
	}