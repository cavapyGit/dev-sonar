package com.pradera.core.component.helperui.to;

import java.io.Serializable;

import com.pradera.model.generalparameter.GeographicLocation;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class IssuerTO.
 * Transfer Object to Issuer helper
 * @author PraderaTechnologies.
 * @version 1.0 , 31/01/2013
 */
public class IssuerTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1593677526249876349L;
	
	/** The issuer code. */
	private String issuerCode;
	
	/** The issuer desc. */
	private String issuerDesc;
	
	/** The issuer state. */
	private Integer issuerState;
	
	/** The issuer state desc. */
	private String issuerStateDesc;
	
	/** The issuer doc type. */
	private Integer issuerDocType;
	
	/** The issuer doc type desc. */
	private String issuerDocTypeDesc;
	
	/** The issuer doc number. */
	private String issuerDocNumber;
	
	/** The issuer sector. */
	private Integer issuerSector;
	
	/** The issuer sector desc. */
	private String issuerSectorDesc;
	
	/** The issuer activity. */
	private Integer issuerActivity;
	
	/** The issuer activity desc. */
	private String issuerActivityDesc;
	
	/** The issuer business name. */
	private String issuerBusinessName;
	
	/** The issuer geographic location. */
	private GeographicLocation issuerGeographicLocation;
	
	/** The mnemonic. */
	private String mnemonic; 

	/**
	 * Instantiates a new issuer to.
	 */
	public IssuerTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the issuer code.
	 *
	 * @return the issuer code
	 */
	public String getIssuerCode() {
		return issuerCode;
	}

	/**
	 * Sets the issuer code.
	 *
	 * @param issuerCode the new issuer code
	 */
	public void setIssuerCode(String issuerCode) {
		this.issuerCode = issuerCode;
	}

	/**
	 * Gets the issuer desc.
	 *
	 * @return the issuer desc
	 */
	public String getIssuerDesc() {
		return issuerDesc;
	}

	/**
	 * Sets the issuer desc.
	 *
	 * @param issuerDesc the new issuer desc
	 */
	public void setIssuerDesc(String issuerDesc) {
		this.issuerDesc = issuerDesc;
	}

	/**
	 * Gets the issuer state.
	 *
	 * @return the issuer state
	 */
	public Integer getIssuerState() {
		return issuerState;
	}

	/**
	 * Sets the issuer state.
	 *
	 * @param issuerState the new issuer state
	 */
	public void setIssuerState(Integer issuerState) {
		this.issuerState = issuerState;
	}

	/**
	 * Gets the issuer doc type.
	 *
	 * @return the issuer doc type
	 */
	public Integer getIssuerDocType() {
		return issuerDocType;
	}

	/**
	 * Sets the issuer doc type.
	 *
	 * @param issuerDocType the new issuer doc type
	 */
	public void setIssuerDocType(Integer issuerDocType) {
		this.issuerDocType = issuerDocType;
	}

	/**
	 * Gets the issuer doc type desc.
	 *
	 * @return the issuer doc type desc
	 */
	public String getIssuerDocTypeDesc() {
		return issuerDocTypeDesc;
	}

	/**
	 * Sets the issuer doc type desc.
	 *
	 * @param issuerDocTypeDesc the new issuer doc type desc
	 */
	public void setIssuerDocTypeDesc(String issuerDocTypeDesc) {
		this.issuerDocTypeDesc = issuerDocTypeDesc;
	}

	/**
	 * Gets the issuer doc number.
	 *
	 * @return the issuer doc number
	 */
	public String getIssuerDocNumber() {
		return issuerDocNumber;
	}

	/**
	 * Sets the issuer doc number.
	 *
	 * @param issuerDocNumber the new issuer doc number
	 */
	public void setIssuerDocNumber(String issuerDocNumber) {
		this.issuerDocNumber = issuerDocNumber;
	}

	/**
	 * Gets the issuer sector.
	 *
	 * @return the issuer sector
	 */
	public Integer getIssuerSector() {
		return issuerSector;
	}

	/**
	 * Sets the issuer sector.
	 *
	 * @param issuerSector the new issuer sector
	 */
	public void setIssuerSector(Integer issuerSector) {
		this.issuerSector = issuerSector;
	}

	/**
	 * Gets the issuer sector desc.
	 *
	 * @return the issuer sector desc
	 */
	public String getIssuerSectorDesc() {
		return issuerSectorDesc;
	}

	/**
	 * Sets the issuer sector desc.
	 *
	 * @param issuerSectorDesc the new issuer sector desc
	 */
	public void setIssuerSectorDesc(String issuerSectorDesc) {
		this.issuerSectorDesc = issuerSectorDesc;
	}

	/**
	 * Gets the issuer activity.
	 *
	 * @return the issuer activity
	 */
	public Integer getIssuerActivity() {
		return issuerActivity;
	}

	/**
	 * Sets the issuer activity.
	 *
	 * @param issuerActivity the new issuer activity
	 */
	public void setIssuerActivity(Integer issuerActivity) {
		this.issuerActivity = issuerActivity;
	}

	/**
	 * Gets the issuer activity desc.
	 *
	 * @return the issuer activity desc
	 */
	public String getIssuerActivityDesc() {
		return issuerActivityDesc;
	}

	/**
	 * Sets the issuer activity desc.
	 *
	 * @param issuerActivityDesc the new issuer activity desc
	 */
	public void setIssuerActivityDesc(String issuerActivityDesc) {
		this.issuerActivityDesc = issuerActivityDesc;
	}

	/**
	 * Gets the issuer state desc.
	 *
	 * @return the issuer state desc
	 */
	public String getIssuerStateDesc() {
		return issuerStateDesc;
	}

	/**
	 * Sets the issuer state desc.
	 *
	 * @param issuerStateDesc the new issuer state desc
	 */
	public void setIssuerStateDesc(String issuerStateDesc) {
		this.issuerStateDesc = issuerStateDesc;
	}

	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}

	/**
	 * Sets the mnemonic.
	 *
	 * @param mnemonic the new mnemonic
	 */
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	/**
	 * Gets the issuer geographic location.
	 *
	 * @return the issuer geographic location
	 */
	public GeographicLocation getIssuerGeographicLocation() {
		return issuerGeographicLocation;
	}

	/**
	 * Sets the issuer geographic location.
	 *
	 * @param issuerGeographicLocation the new issuer geographic location
	 */
	public void setIssuerGeographicLocation(GeographicLocation issuerGeographicLocation) {
		this.issuerGeographicLocation = issuerGeographicLocation;
	}

	/**
	 * Gets the issuer business name.
	 *
	 * @return the issuer business name
	 */
	public String getIssuerBusinessName() {
		return issuerBusinessName;
	}

	/**
	 * Sets the issuer business name.
	 *
	 * @param issuerBusinessName the new issuer business name
	 */
	public void setIssuerBusinessName(String issuerBusinessName) {
		this.issuerBusinessName = issuerBusinessName;
	}

}
