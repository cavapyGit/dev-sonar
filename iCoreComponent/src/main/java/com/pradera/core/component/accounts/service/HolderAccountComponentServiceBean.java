package com.pradera.core.component.accounts.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.RegistryType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.accounts.to.PuHolderAccountBalanceTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.core.framework.entity.service.LoaderEntityServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.InstitutionInformation;
import com.pradera.model.accounts.LegalRepresentative;
import com.pradera.model.accounts.LegalRepresentativeFile;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.PepPerson;
import com.pradera.model.accounts.RepresentativeFileHistory;
import com.pradera.model.accounts.RepresentedEntity;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountBank;
import com.pradera.model.accounts.holderaccounts.HolderAccountBankRequest;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetRequest;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.HolderAccountFile;
import com.pradera.model.accounts.holderaccounts.HolderAccountRequest;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestHysStatusType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestHysType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStateBankType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStateDetailType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountType;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.HolderFileStateType;
import com.pradera.model.accounts.type.MechanismModalityStateType;
import com.pradera.model.accounts.type.ParticipantMechanismStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.type.NegotiationModalityStateType;
import com.pradera.model.operations.RequestCorrelativeType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class HolderAccountServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 28/02/2013
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class HolderAccountComponentServiceBean extends CrudDaoServiceBean{
	
	/** The parameter service bean. */
	@EJB
	ParameterServiceBean parameterServiceBean;
	
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	@EJB
	LoaderEntityServiceBean loaderEntityServiceBean;
	
	/**
	 * Registry holder account request service bean.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public HolderAccountRequest regHoldAccountReqComponentServiceBean(HolderAccountRequest holderAccountRequest) throws ServiceException{
		//Calling operation Number
        Long requestNumber = getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_BLOCK_UNBLOC_CLOSE_ACCOUNT);
		holderAccountRequest.setRequestNumber(requestNumber);
        create(holderAccountRequest);
		return holderAccountRequest;
	}
	
	/**
	 * Block holder account service bean.
	 * metodo para bloquear una cuenta a partir de una solicitud, usado para unificacion de participantes.
	 * @param holderAccountRequestHy the holder account request hy
	 * @return the holder account request hy
	 * @throws ServiceException the service exception
	 */
	public HolderAccountRequest blockHolderAccountServiceBean(HolderAccountRequest holderAccountRequest) throws ServiceException{
		holderAccountRequest.setRequestState(HolderAccountRequestHysStatusType.CONFIRM.getCode());
		update(holderAccountRequest);
		HolderAccount holderAccount = holderAccountRequest.getHolderAccount();
		holderAccount.setStateAccount(HolderAccountStatusType.BLOCK.getCode());//Bloqueamos la cuenta
		update(holderAccount);
		return holderAccountRequest;
	}
	
	public List<PuHolderAccountBalanceTO> getPuHolderAccountBalanceTo(Long idParticipant, Long idHolderAccount, List<Long> lstHolderAccountsPk){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT");
		sbQuery.append("  HAB.participant.idParticipantPk,"				//0
					 + "  HAB.holderAccount.idHolderAccountPk, "		//1
					 + "  HAB.security.idSecurityCodePk, "				//2
					 + "  HAB.totalBalance,"							//3
					 + "  HAB.availableBalance , "						//4
					 + "  HAB.banBalance , "							//5
					 + "  HAB.pawnBalance , "							//6
					 + "  HAB.otherBlockBalance , "						//7
					 + "  HAB.reserveBalance , "						//8
					 + "  HAB.oppositionBalance  ");					//9
		sbQuery.append("  FROM HolderAccountBalance HAB");
		sbQuery.append(" WHERE 1 = 1 ");
		sbQuery.append(" AND HAB.participant.idParticipantPk = :idParticipant and HAB.totalBalance>0");
	if( Validations.validateIsNotNullAndPositive(idHolderAccount) ){
		sbQuery.append(" AND HAB.holderAccount.idHolderAccountPk = :idHolderAccount");
	}
	if( lstHolderAccountsPk != null && lstHolderAccountsPk.size()>0 ){
		sbQuery.append(" AND HAB.holderAccount.idHolderAccountPk in (:lstIdHolderAccount)");
	}
		Query query = em.createQuery(sbQuery.toString());
		
		query.setParameter("idParticipant", idParticipant);
	if( Validations.validateIsNotNullAndPositive(idHolderAccount) ){
		query.setParameter("idHolderAccount", idHolderAccount);
	}
	if( lstHolderAccountsPk != null && lstHolderAccountsPk.size()>0 ){
		query.setParameter("lstIdHolderAccount", lstHolderAccountsPk);
	}
				
		List<PuHolderAccountBalanceTO> lstHolderAccountBalanceTO = new ArrayList<PuHolderAccountBalanceTO>();
		
		List<Object> objectsList = ((List<Object>)query.getResultList());
		for (int i = 0; i < objectsList.size(); ++i) {			
			Object[] objects = (Object[]) objectsList.get(i);
			PuHolderAccountBalanceTO hab = new PuHolderAccountBalanceTO();
			hab.setIdParticipantPk((Long)objects[0]);
			hab.setIdHolderAccountPk((Long)objects[1]);
			hab.setIdSecurityCodePk((String)objects[2]);
			hab.setTotalBalance((BigDecimal) objects[3]);
			hab.setAvailableBalance((BigDecimal) objects[4]);
			hab.setBanBalance((BigDecimal) objects[5]);
			hab.setPawnBalance((BigDecimal) objects[6]);
			hab.setOtherBlockBalance((BigDecimal) objects[7]);
			hab.setReserveBalance((BigDecimal) objects[8]);
			hab.setOppositionBalance((BigDecimal) objects[9]);
			
			lstHolderAccountBalanceTO.add(hab);
		}
		return lstHolderAccountBalanceTO;
	}
	
	/**
	 * Close holder account service bean.
	 * metodo para bloquear una cuenta a partir de una solicitud, Anulacion de participantes.
	 * @param holderAccountRequestHy the holder account request hy
	 * @return the holder account request hy
	 * @throws ServiceException the service exception
	 */
	public HolderAccountRequest closeHolderAccountServiceBean(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		
		holderAccountRequestHy.setRequestState(HolderAccountRequestHysStatusType.CONFIRM.getCode());
		holderAccountRequestHy.setApproveUser(loggerUser.getUserName());
		holderAccountRequestHy.setApproveDate(loggerUser.getAuditTime());
		holderAccountRequestHy.setConfirmUser(loggerUser.getUserName());
		holderAccountRequestHy.setConfirmDate(loggerUser.getAuditTime());
		
		//Actualizamos en la base de datos
		
		updateStateHolderAccountRequest(holderAccountRequestHy, loggerUser);
		
		//obtenemos la cuenta de la solicitud
		HolderAccount holderAccount = holderAccountRequestHy.getHolderAccount();
		holderAccount.setStateAccount(HolderAccountStatusType.CLOSED.getCode());//Cerramos la cuenta
		//

		updateStateHolderAccount(holderAccount, loggerUser);
		
		return holderAccountRequestHy;
	}
	
	private void updateStateHolderAccountRequest(HolderAccountRequest holderAccountRequest, LoggerUser loggerUser) {
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append(" Update HolderAccountRequest HAR Set ");
		sbQuery.append(" HAR.requestState = :requestStatePrm, ");
		sbQuery.append(" HAR.lastModifyUser = :lastModifyUserPrm, ");
		sbQuery.append(" HAR.lastModifyDate = :lastModifyDatePrm, ");
		sbQuery.append(" HAR.lastModifyApp = :lastModifyAppPrm, ");
		sbQuery.append(" HAR.lastModifyIp = :lastModifyIpPrm ");
		
		if(holderAccountRequest.getApproveUser()!=null) {
			sbQuery.append(" ,HAR.approveUser = :approveUserPrm ");
		}
		
		if(holderAccountRequest.getApproveDate()!=null) {
			sbQuery.append(" ,HAR.approveDate = :approveDatePrm ");
		}
		
		if(holderAccountRequest.getConfirmUser()!=null) {
			sbQuery.append(" ,HAR.confirmUser = :confirmUserPrm ");
		}
		
		if(holderAccountRequest.getConfirmDate()!=null) {
			sbQuery.append(" ,HAR.confirmDate = :confirmDatePrm ");
		}
		
		
		sbQuery.append(" Where HAR.idHolderAccountReqPk = :idHolderAccountReqPkPrm ");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("requestStatePrm", holderAccountRequest.getRequestState());
		query.setParameter("lastModifyUserPrm", loggerUser.getUserName());
		query.setParameter("lastModifyDatePrm", loggerUser.getAuditTime());
		query.setParameter("lastModifyAppPrm", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyIpPrm", loggerUser.getIpAddress());
		query.setParameter("idHolderAccountReqPkPrm", holderAccountRequest.getIdHolderAccountReqPk());
		
		if(holderAccountRequest.getApproveUser()!=null) {
			query.setParameter("approveUserPrm", holderAccountRequest.getApproveUser());
		}
		
		if(holderAccountRequest.getApproveDate()!=null) {
			query.setParameter("approveDatePrm", holderAccountRequest.getApproveDate());
		}
		
		if(holderAccountRequest.getConfirmUser()!=null) {
			query.setParameter("confirmUserPrm", holderAccountRequest.getConfirmUser());
		}
		
		if(holderAccountRequest.getConfirmDate()!=null) {
			query.setParameter("confirmDatePrm", holderAccountRequest.getConfirmDate());
		}
		
		query.executeUpdate();
		
	}
	
	private void updateStateHolderAccount(HolderAccount holderAccount, LoggerUser loggerUser) {
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append(" Update HolderAccount HA Set ");
		sbQuery.append(" HA.stateAccount = :stateAccountPrm, ");
		sbQuery.append(" HA.lastModifyUser = :lastModifyUserPrm, ");
		sbQuery.append(" HA.lastModifyDate = :lastModifyDatePrm, ");
		sbQuery.append(" HA.lastModifyApp = :lastModifyAppPrm, ");
		sbQuery.append(" HA.lastModifyIp = :lastModifyIpPrm ");

		sbQuery.append(" Where HA.idHolderAccountPk = :idHolderAccountPkPrm ");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("stateAccountPrm", holderAccount.getStateAccount());
		query.setParameter("lastModifyUserPrm", loggerUser.getUserName());
		query.setParameter("lastModifyDatePrm", loggerUser.getAuditTime());
		query.setParameter("lastModifyAppPrm", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyIpPrm", loggerUser.getIpAddress());
		query.setParameter("idHolderAccountPkPrm", holderAccount.getIdHolderAccountPk());
				
		query.executeUpdate();		
	}
	/**
	 * Gets the holder account list component service facade.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account list component service facade
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> getHolderAccountListComponentServiceFacade(HolderAccountTO holderAccountTO) throws ServiceException{
		List<HolderAccount> holderAccountList = this.getHolderAccountListComponentServiceBean(holderAccountTO);
		for(int i = 0; i< holderAccountList.size();++i){		
			holderAccountList.get(i).getHolderAccountBalances().size();	
			for(int j=0; j <holderAccountList.get(i).getHolderAccountRequest().size();++j){
				holderAccountList.get(i).getHolderAccountRequest().get(j).getHolderAccountBankRequest().size();
				holderAccountList.get(i).getHolderAccountRequest().get(j).getHolderAccountDetRequest().size();
			}
		}
		return holderAccountList;
	}
	/**
	 * Gets the holder account request hy service bean.
	 *
	 * @param idRequestPk the id request pk
	 * @return the holder account request hy service bean
	 * @throws ServiceException the service exception
	 */
	public HolderAccountRequest getHolderAccountRequestHyServiceBean(Long idRequestPk) throws ServiceException{
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<HolderAccountRequest> criteriaQuery = criteriaBuilder.createQuery(HolderAccountRequest.class);
		Root<HolderAccountRequest> holderAccountRequestHyRoot = criteriaQuery.from(HolderAccountRequest.class);
		List<Predicate> criteriaParameters = new ArrayList<Predicate>();
		
		holderAccountRequestHyRoot.fetch("participant");

		criteriaParameters.add(criteriaBuilder.equal(holderAccountRequestHyRoot.get("idHolderAccountReqPk"),idRequestPk));
		criteriaQuery.where(criteriaBuilder.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()])));
		
		TypedQuery<HolderAccountRequest> holderAccountRequestHyCriteria = em.createQuery(criteriaQuery);
		HolderAccountRequest holderAccountRequestHy = holderAccountRequestHyCriteria.getSingleResult();
		holderAccountRequestHy.getHolderAccountDetRequest().size();
		holderAccountRequestHy.getHolderAccountBankRequest().size();
		if(!holderAccountRequestHy.getRequestType().equals(HolderAccountRequestHysType.CREATE.getCode())){
			//We need to search HolderAccount for HolderAccountRequest
			HolderAccountTO holderAccountTO = new HolderAccountTO();
			holderAccountTO.setNeedHolder(Boolean.TRUE);
			holderAccountTO.setNeedParticipant(Boolean.FALSE);
			holderAccountTO.setIdHolderAccountPk(holderAccountRequestHy.getHolderAccount().getIdHolderAccountPk());
			holderAccountRequestHy.setHolderAccount(getHolderAccountComponentServiceBean(holderAccountTO));
			// --- End
		}
		return holderAccountRequestHy;
	}
	
	/**
	 * Registry holder account from holder account request service bean.
	 *
	 * @param holderAcccounRequest the holder acccoun request
	 * @return the holder account
	 * @throws ServiceException the service exception
	 */
	public HolderAccount regHoldAccFromHolddAccComponentReqServiceBean(HolderAccountRequest holderAcccounRequest) throws ServiceException{
		
		Query query = em.createQuery("SELECT MAX(ha.accountNumber) + 1 FROM HolderAccount ha WHERE ha.participant.idParticipantPk = :idParticipantPkParameter");
		query.setParameter("idParticipantPkParameter", holderAcccounRequest.getParticipant().getIdParticipantPk());
		Integer correlative = 0;
		try{
			correlative = (Integer) query.getSingleResult();
		}catch(Exception ex){
			correlative = null;
		}
		if(Validations.validateIsNull(correlative)){
			correlative = 1;
		}
		
		HolderAccount holderAccount = new HolderAccount();
		HolderAccountRequest holderAccountRequestHy = getHolderAccountRequestHyServiceBean(holderAcccounRequest.getIdHolderAccountReqPk());
		StringBuilder alternativeCode = new StringBuilder(holderAccountRequestHy.getParticipant().getIdParticipantPk().toString());
		holderAccount.setAccountNumber(correlative);
		Holder holderMain = null;
		
		List<HolderAccountDetail> holderAccountDetailList = new ArrayList<HolderAccountDetail>();
		alternativeCode.append("-(");
		for(HolderAccountDetRequest holderAccountReqDetHy: holderAccountRequestHy.getHolderAccountDetRequest()){
			HolderAccountDetail holderAccountDetail = new HolderAccountDetail();
			holderAccountDetail.setHolderAccount(holderAccount);
			holderAccountDetail.setHolder(holderAccountReqDetHy.getHolder());
			if(holderAccountRequestHy.getHolderAccountType().equals(HolderAccountType.OWNERSHIP.getCode())){
				holderAccountDetail.setIndRepresentative(holderAccountReqDetHy.getIndRepresentative());
				if(holderAccountReqDetHy.getIndRepresentative().equals(BooleanType.YES.getCode())){
					holderMain = holderAccountDetail.getHolder();
				}
			}else if(holderAccountRequestHy.getHolderAccountType().equals(HolderAccountType.JURIDIC.getCode()) || holderAccountRequestHy.getHolderAccountType().equals(HolderAccountType.NATURAL.getCode())){
				holderAccountDetail.setIndRepresentative(BooleanType.YES.getCode());
				holderMain = holderAccountDetail.getHolder(); 
			}
			
			holderAccountDetail.setRegistryDate(holderAccountRequestHy.getRegistryDate());
			holderAccountDetail.setRegistryUser(holderAccountRequestHy.getRegistryUser());
			holderAccountDetail.setStateAccountDetail(HolderAccountStateDetailType.REGISTERED.getCode());
			holderAccountDetailList.add(holderAccountDetail);
			
			alternativeCode.append(holderAccountReqDetHy.getHolder().getIdHolderPk());
			if(holderAccountRequestHy.getHolderAccountDetRequest().indexOf(holderAccountReqDetHy)!=holderAccountRequestHy.getHolderAccountDetRequest().size()-1){
				alternativeCode.append(",");
			}
		}
		
		List<HolderAccountBank> holderAccountBankList = new ArrayList<HolderAccountBank>();
		for(HolderAccountBankRequest holderAccountReqBankHy: holderAccountRequestHy.getHolderAccountBankRequest()){
			HolderAccountBank holderAccountBank = new HolderAccountBank();
			holderAccountBank.setHolderAccount(holderAccount);
			holderAccountBank.setBank(holderAccountReqBankHy.getBank());
			holderAccountBank.setCurrency(holderAccountReqBankHy.getCurrency());
			holderAccountBank.setBankAccountType(holderAccountReqBankHy.getBankAccountType());
			holderAccountBank.setBankAccountNumber(holderAccountReqBankHy.getAccountNumber());
			holderAccountBank.setDescriptionBank(holderAccountReqBankHy.getDescriptionBank());						
			holderAccountBank.setHaveBic((holderAccountReqBankHy.getHaveBic()==null)?0:holderAccountReqBankHy.getHaveBic());
			holderAccountBank.setRegistryUser(holderAccountRequestHy.getRegistryUser());
			holderAccountBank.setRegistryDate(holderAccountRequestHy.getRegistryDate());
			holderAccountBank.setStateAccountBank(HolderAccountStateBankType.REGISTERED.getCode());
			holderAccountBank.setHolder(holderMain);
			holderAccountBankList.add(holderAccountBank);
		}
		
		alternativeCode.append(")-");
		alternativeCode.append(holderAccount.getAccountNumber());

		
		holderAccount.setRegistryDate(holderAccountRequestHy.getRegistryDate());
		holderAccount.setRegistryUser(holderAccountRequestHy.getRegistryUser());
		holderAccount.setParticipant(holderAccountRequestHy.getParticipant());
		holderAccount.setAccountType(holderAccountRequestHy.getHolderAccountType());
		holderAccount.setAccountGroup(holderAccountRequestHy.getHolderAccountGroup());
		holderAccount.setStateAccount(HolderAccountStatusType.CLOSED.getCode());
		holderAccount.setHolderAccountDetails(holderAccountDetailList);
		holderAccount.setHolderAccountBanks(holderAccountBankList);
		holderAccount.setAlternateCode(alternativeCode.toString());		
		
		create(holderAccount);
		update(holderAccountRequestHy);
		return holderAccount;
	}
	
	
		public HolderAccount regHoldAccountComponentReqServiceBean(HolderAccount objHolderAccount, Participant participantTarget) throws ServiceException{
			
		/** settear el correlativo cuando confirme **/
		HolderAccount holderAccount = new HolderAccount();
		StringBuilder alternativeCode = new StringBuilder(participantTarget.getIdParticipantPk().toString());
		holderAccount.setAccountNumber(getCorrelativeNumberHolderAccountServiceBean(participantTarget.getIdParticipantPk()));
		Holder holderMain = null;
		
		List<HolderAccountDetail> holderAccountDetailList = new ArrayList<HolderAccountDetail>();
		alternativeCode.append("-(");
		
		List<HolderAccountDetail> holderOriginDetails = null;
		if(!loaderEntityServiceBean.isLoadedEntity(objHolderAccount, "holderAccountDetails")){
			String query = "select had from HolderAccountDetail had where had.holderAccount.idHolderAccountPk=:account ";
			Map<String,Object> param = new HashMap<>();
			param.put("account", objHolderAccount.getIdHolderAccountPk());
			holderOriginDetails =  parameterServiceBean.findListByQueryString(query, param);
		} else {
			holderOriginDetails = objHolderAccount.getHolderAccountDetails();
		}
		
		//CREO LOS DETALLES, EL RNT ES EL MISMO, Y LOS DATOS IGUAL
		for(HolderAccountDetail objHolderAccountDetail:holderOriginDetails ){
			HolderAccountDetail holderAccountDetail = new HolderAccountDetail();
			holderAccountDetail.setHolderAccount(holderAccount);
			holderAccountDetail.setHolder(objHolderAccountDetail.getHolder());
			
			if(objHolderAccount.getAccountType().equals(HolderAccountType.OWNERSHIP.getCode())){
				holderAccountDetail.setIndRepresentative(objHolderAccountDetail.getIndRepresentative());
				if(objHolderAccountDetail.getIndRepresentative().equals(BooleanType.YES.getCode())){
					holderMain = holderAccountDetail.getHolder();
				}
			}else if(objHolderAccount.getAccountType().equals(HolderAccountType.JURIDIC.getCode()) || 
					 objHolderAccount.getAccountType().equals(HolderAccountType.NATURAL.getCode())){
				holderAccountDetail.setIndRepresentative(BooleanType.YES.getCode());
				holderMain = holderAccountDetail.getHolder(); 
			}
			
			holderAccountDetail.setRegistryDate(objHolderAccount.getRegistryDate());
			holderAccountDetail.setRegistryUser(objHolderAccount.getRegistryUser());
			holderAccountDetail.setStateAccountDetail(HolderAccountStateDetailType.REGISTERED.getCode());
			holderAccountDetailList.add(holderAccountDetail);
			
			alternativeCode.append(objHolderAccountDetail.getHolder().getIdHolderPk());
			if(holderOriginDetails.indexOf(objHolderAccountDetail)!=holderOriginDetails.size()-1){
				alternativeCode.append(",");
			}
		}
		alternativeCode.append(")-");
		alternativeCode.append(holderAccount.getAccountNumber());

		//CREO LOS DATOS PARA LOS BANCOS
		List<HolderAccountBank> holderAccountBankList = new ArrayList<HolderAccountBank>();
		List<HolderAccountBank> holderOriginBanks = null;
		if(!loaderEntityServiceBean.isLoadedEntity(objHolderAccount, "holderAccountBanks")){
			String query = "select hab from HolderAccountBank hab where hab.holderAccount.idHolderAccountPk=:account ";
			Map<String,Object> param = new HashMap<>();
			param.put("account", objHolderAccount.getIdHolderAccountPk());
			holderOriginBanks =  parameterServiceBean.findListByQueryString(query, param);
		} else {
			holderOriginBanks = objHolderAccount.getHolderAccountBanks();
		}
		for(HolderAccountBank holderAccountReqBankHy: holderOriginBanks){
			HolderAccountBank holderAccountBank = new HolderAccountBank();
			holderAccountBank.setHolderAccount(holderAccount);
			holderAccountBank.setBank(holderAccountReqBankHy.getBank());
			holderAccountBank.setCurrency(holderAccountReqBankHy.getCurrency());
			holderAccountBank.setBankAccountType(holderAccountReqBankHy.getBankAccountType());
			holderAccountBank.setBankAccountNumber(holderAccountReqBankHy.getBankAccountNumber());
			holderAccountBank.setDescriptionBank(holderAccountReqBankHy.getDescriptionBank());						
			holderAccountBank.setHaveBic((holderAccountReqBankHy.getHaveBic()==null)?0:holderAccountReqBankHy.getHaveBic());
			holderAccountBank.setRegistryUser(objHolderAccount.getRegistryUser());
			holderAccountBank.setRegistryDate(objHolderAccount.getRegistryDate());
			holderAccountBank.setStateAccountBank(HolderAccountStateBankType.REGISTERED.getCode());
			holderAccountBank.setHolder(holderMain);
			holderAccountBankList.add(holderAccountBank);
		}

		//LOS ARCHIVOS SUBIDOS, NO TODOS TIENEN
		List<HolderAccountFile> lstHolderAccountFile = new ArrayList<HolderAccountFile>();
		List<HolderAccountFile> holderOriginFiles = null;
		if(!loaderEntityServiceBean.isLoadedEntity(objHolderAccount, "holderAccountFiles")){
			String query = "select haf from HolderAccountFile haf where haf.holderAccount.idHolderAccountPk=:account ";
			Map<String,Object> param = new HashMap<>();
			param.put("account", objHolderAccount.getIdHolderAccountPk());
			holderOriginFiles =  parameterServiceBean.findListByQueryString(query, param);
		} else {
			holderOriginFiles = holderAccount.getHolderAccountFiles();
		}
		if( holderOriginFiles != null && !holderOriginFiles.isEmpty()){	//SI TIENE FILES: 
			for(HolderAccountFile haf : holderOriginFiles){									//por lo general las cuentas antiguas no tienen estos archivos
				HolderAccountFile holderAccountFile = new HolderAccountFile();
				holderAccountFile.setDocumentFile(haf.getDocumentFile());
				holderAccountFile.setHolderAccount(holderAccount);
				holderAccountFile.setDocumentType(haf.getDocumentType());
				holderAccountFile.setFilename(haf.getFilename());
				holderAccountFile.setIndMain(haf.getIndMain());
				holderAccountFile.setRegistryDate(CommonsUtilities.currentDate());
				holderAccountFile.setRegistryUser(objHolderAccount.getRegistryUser());
				lstHolderAccountFile.add(holderAccountFile);
			}
		}
		
		//CREO LA CUENTA CON LOS MISMOS DATOS, SOLO CAMBIA EL PARTICIPANTE DESTINO Y EL NRO DE CUENTA
		holderAccount.setRegistryDate(CommonsUtilities.currentDate());
		holderAccount.setRegistryUser(objHolderAccount.getRegistryUser());
		holderAccount.setParticipant(participantTarget);
		holderAccount.setAccountType(objHolderAccount.getAccountType());
		holderAccount.setAccountGroup(objHolderAccount.getAccountGroup());
		holderAccount.setStateAccount(HolderAccountStatusType.CLOSED.getCode());
		holderAccount.setAlternateCode(alternativeCode.toString());	
		holderAccount.setHolderAccountDetails(holderAccountDetailList);
		holderAccount.setHolderAccountBanks(holderAccountBankList);
		holderAccount.setOpeningDate(CommonsUtilities.currentDateTime());
		//SI TENGO ARCHIVOS, LOS AGREGO PARA SER CREADOS
		if( holderOriginFiles != null && !holderOriginFiles.isEmpty() ){
			holderAccount.setHolderAccountFiles(lstHolderAccountFile);	
		}
		
		create(holderAccount);
		return holderAccount;
	}

	
	
	/**
	 * Gets the holder account service bean.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account service bean
	 */
	public HolderAccount getHolderAccountComponentServiceBean(HolderAccountTO holderAccountTO) throws ServiceException{
		
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<HolderAccount> criteriaQuery = criteriaBuilder.createQuery(HolderAccount.class);
		Root<HolderAccount> holderAccountRoot = criteriaQuery.from(HolderAccount.class);
		List<Predicate> criteriaParameters = new ArrayList<Predicate>();
		
		if(holderAccountTO.isNeedParticipant()){
			holderAccountRoot.fetch("participant");
		}
		if(Validations.validateIsNotNullAndNotEmpty(holderAccountTO.getAlternateCode())){
			criteriaParameters.add(criteriaBuilder.equal(holderAccountRoot.get("alternateCode"),holderAccountTO.getAlternateCode()));
		}
		if(Validations.validateIsNotNullAndNotEmpty(holderAccountTO.getIdHolderAccountPk())){
			criteriaParameters.add(criteriaBuilder.equal(holderAccountRoot.get("idHolderAccountPk"),holderAccountTO.getIdHolderAccountPk()));
		}
		if(Validations.validateIsNotNullAndNotEmpty(holderAccountTO.getHolderAccountNumber())){
			criteriaParameters.add(criteriaBuilder.equal(holderAccountRoot.get("accountNumber"),holderAccountTO.getHolderAccountNumber()));
		}
		if(Validations.validateIsNotNullAndNotEmpty(holderAccountTO.getParticipantTO())){
			Join<Participant,HolderAccount> participantJoin = holderAccountRoot.join("participant");
			criteriaParameters.add(criteriaBuilder.equal(participantJoin.get("idParticipantPk"),holderAccountTO.getParticipantTO()));
		}
		if(Validations.validateIsNotNullAndNotEmpty(holderAccountTO.getIdHolderPk())){
			Join<HolderAccountDetail,HolderAccount> holderAccountDetailJoin = holderAccountRoot.join("holderAccountDetails");
			Join<Holder,HolderAccountDetail> holderWithHolderAccountDetailJoin = holderAccountDetailJoin.join("holder");
			criteriaParameters.add(criteriaBuilder.equal(holderWithHolderAccountDetailJoin.get("idHolderPk"),holderAccountTO.getIdHolderPk()));
		}
		if(Validations.validateIsNotNullAndNotEmpty(holderAccountTO.getIdIssuerPk())){
			Join<HolderAccountDetail,HolderAccount> holderAccountDetailJoin = holderAccountRoot.join("holderAccountDetails");
			Join<Holder,HolderAccountDetail> holderWithHolderAccountDetailJoin = holderAccountDetailJoin.join("holder");
			Join<Issuer,Holder> issuerOnHolderJoin = holderWithHolderAccountDetailJoin.join("issuer");
			criteriaParameters.add(criteriaBuilder.equal(issuerOnHolderJoin.get("idIssuerPk"),holderAccountTO.getIdIssuerPk()));
		}
		if(Validations.validateIsNotNullAndNotEmpty(holderAccountTO.getHolderAccountType())){
			criteriaParameters.add(criteriaBuilder.equal(holderAccountRoot.get("accountType"),holderAccountTO.getHolderAccountType()));
		}
		if(Validations.validateIsNotNullAndNotEmpty(holderAccountTO.getHolderaccountGroupType())){
			criteriaParameters.add(criteriaBuilder.equal(holderAccountRoot.get("accountGroup"),holderAccountTO.getHolderaccountGroupType()));
		}
		
		if(holderAccountTO.isNeedBalances()){
			holderAccountRoot.fetch("holderAccountBalances");
		}
		
		criteriaQuery.where(criteriaBuilder.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()])));
		TypedQuery<HolderAccount> holderAccountRequestHyCriteria = em.createQuery(criteriaQuery);
		
		HolderAccount holderAccountHy = null;
		try{
			holderAccountHy = holderAccountRequestHyCriteria.getSingleResult();
		}catch(NoResultException ex){
			return null;
		}
		
		if(holderAccountTO.isNeedBanks()){
			for(HolderAccountBank holderAccountBank: holderAccountHy.getHolderAccountBanks()){
				holderAccountBank.getIdHolderAccountBankPk();
			}
		}
		for(HolderAccountDetail holderAccountDetail: holderAccountHy.getHolderAccountDetails()){
			holderAccountDetail.getHolder().toString();
		}
		
		if(holderAccountTO.isNeedFiles()){
			
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_FILES.getCode());
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			List<ParameterTable> listParameterTable = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);
			
			holderAccountHy.getHolderAccountFiles().size();
			for(HolderAccountFile holderAccountFile: holderAccountHy.getHolderAccountFiles()){
				for(ParameterTable parameter: listParameterTable){
					if(holderAccountFile.getDocumentType().equals(parameter.getParameterTablePk())){
						holderAccountFile.setDocumentTypeDesc(parameter.getDescription());
					}
				}
			}
		}
		
		//SEARCH STATES ACCOUNTS
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		Map<Integer,String> mapAccountStateDescription = new HashMap<Integer,String>();
		parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
		for(ParameterTable param : parameterServiceBean.getListParameterTableServiceBean(parameterTableTO)){
			mapAccountStateDescription.put(param.getParameterTablePk(), param.getParameterName());
		}		
		if(mapAccountStateDescription != null && Validations.validateIsNotNull(holderAccountHy.getStateAccount())){
			holderAccountHy.setStateAccountDescription(mapAccountStateDescription.get(holderAccountHy.getStateAccount()));
		}else{
			holderAccountHy.setStateAccountDescription(null);
		}
		
		return holderAccountHy;
	}
	
	public List<HolderAccount> getHolderAccountListComponentServiceBean(HolderAccountTO holderAccountTO) throws ServiceException{
		
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" select ha from HolderAccount ha " );
		
		if(holderAccountTO.isNeedParticipant()){
			sbQuery.append("	inner join fetch ha.participant pa ");
		}else{
			sbQuery.append("	inner join ha.participant pa ");
		}
		
		if(holderAccountTO.isNeedHolder()){
//			sbQuery.append("	inner join  ha.holderAccountDetails had ");
//			sbQuery.append("	inner join  had.holder ho ");	
		}
		
		sbQuery.append(" where 1 = 1 " );
		
		if(holderAccountTO.getParticipantTO()!=null || holderAccountTO.getParticipantStates()!=null){
			if(Validations.validateIsNotNullAndPositive(holderAccountTO.getParticipantTO())){
				sbQuery.append(" and pa.idParticipantPk = :idParticipant ");
				parameters.put("idParticipant", holderAccountTO.getParticipantTO() );
			}
			
			if(Validations.validateListIsNotNullAndNotEmpty(holderAccountTO.getParticipantStates())){
				sbQuery.append(" and pa.state in  :partstates");
				parameters.put("partstates", holderAccountTO.getParticipantStates() );
			}
		}
		
		if(Validations.validateIsNotNullAndPositive(holderAccountTO.getIdHolderPk())){
			sbQuery.append(" and exists ( ");
			sbQuery.append("     select inhad.holder.idHolderPk from HolderAccountDetail inhad  ");
			sbQuery.append("     where inhad.holder.idHolderPk = :idHolder ");
			sbQuery.append("     and   inhad.holderAccount.idHolderAccountPk = ha.idHolderAccountPk ");
			sbQuery.append("  ) ");
			parameters.put("idHolder", holderAccountTO.getIdHolderPk());
		}
		
		if(holderAccountTO.getLstHolderPk()!=null && holderAccountTO.getLstHolderPk().size()>0){
			sbQuery.append(" and exists ( ");
			sbQuery.append("     select inhad.holder.idHolderPk from HolderAccountDetail inhad  ");
			sbQuery.append("     where inhad.holder.idHolderPk in (:lstHolder) ");
			sbQuery.append("     and   inhad.holderAccount.idHolderAccountPk = ha.idHolderAccountPk ");
			sbQuery.append("  ) ");
			parameters.put("lstHolder", holderAccountTO.getLstHolderPk());
		}
		
		if(Validations.validateIsNotNullAndPositive(holderAccountTO.getStatus())){
			sbQuery.append(" and ha.stateAccount = :stateAccount ");
			parameters.put("stateAccount", holderAccountTO.getStatus() );
		}
		
		if(Validations.validateIsNotNullAndPositive(holderAccountTO.getHolderAccountNumber())){
			sbQuery.append(" and ha.accountNumber = :accountNumber ");
			parameters.put("accountNumber", holderAccountTO.getHolderAccountNumber());
		}
		
		if(Validations.validateIsNotNullAndPositive(holderAccountTO.getIdHolderAccountPk())){
			sbQuery.append(" and ha.idHolderAccountPk = :idHolderAccount ");
			parameters.put("idHolderAccount", holderAccountTO.getIdHolderAccountPk());
		}
		
		if(Validations.validateIsNotNullAndPositive(holderAccountTO.getHolderaccountGroupType())){
			sbQuery.append(" and ha.accountGroup = :accountGroup ");
			parameters.put("accountGroup", holderAccountTO.getHolderaccountGroupType());
		}
		
		sbQuery.append(" order by pa.mnemonic ");
		
		List<HolderAccount> result =  findListByQueryString(sbQuery.toString(), parameters);
		
		if(Validations.validateListIsNotNullAndNotEmpty(result)){
			for(HolderAccount objHolderAccount : result){
				objHolderAccount.getHolderAccountDetails().size();
				for(HolderAccountDetail objHolderAccountDetail : objHolderAccount.getHolderAccountDetails()){
					objHolderAccountDetail.getHolder().toString();
				}
			}
		}
		
		return result;
	}
	
	public List<Object[]> getHoldersByFilter(HolderTO filterTO)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select h.id_holder_pk,");
		sbQuery.append("       h.name,");
		sbQuery.append("       h.first_last_name,");
		sbQuery.append("       h.full_name,");
		sbQuery.append("       h.nationality,");
		sbQuery.append("       h.LEGAL_RESIDENCE_COUNTRY,");
		sbQuery.append("       h.id_participant_fk,");
		sbQuery.append("       p.id_participant_pk,");
		sbQuery.append("       i.id_issuer_pk,");
		sbQuery.append("       be.id_block_entity_pk, ");
		sbQuery.append("       h.document_type, ");
		sbQuery.append("       h.document_number, ");
		sbQuery.append("       h.second_last_name ");
		sbQuery.append("from holder h ");
		sbQuery.append("left join participant p ");
		sbQuery.append("on h.id_participant_fk=p.id_participant_pk ");
		sbQuery.append("left join issuer i ");
		sbQuery.append("on h.id_holder_pk=i.id_holder_fk ");
		sbQuery.append("left join block_entity be ");
		sbQuery.append("on be.id_holder_fk=h.id_holder_pk ");
		sbQuery.append(" where 1 =1");
		
		if(Validations.validateIsNotNull(filterTO.getPersonType())){
			sbQuery.append(" and h.holder_type=:type");			
		}
		
		sbQuery.append(" order by h.id_holder_pk ");

		Query query = em.createNativeQuery(sbQuery.toString());
		
		if(Validations.validateIsNotNull(filterTO.getPersonType())){
			query.setParameter("type",filterTO.getPersonType());
		}
		
	   return query.getResultList();


	}
	
	public List<LegalRepresentative> getLegalRepresentative()throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select lr.id_legal_representative_pk,lr.name,h.full_name,lr.first_last_name,lr.person_type,lr.legal_residence_country,lr.nationality,h.id_holder_pk, ");
		sbQuery.append("lr.document_type,lr.document_number ");
		sbQuery.append("from represented_entity re ");
		sbQuery.append("inner join holder h ");
		sbQuery.append("on re.id_holder_fk=h.id_holder_pk ");
		sbQuery.append("inner join legal_representative lr ");
		sbQuery.append("on re.id_legal_representative_fk=lr.id_legal_representative_pk ");
		sbQuery.append(" where 1=1 ");

		Query query = em.createNativeQuery(sbQuery.toString());
		
		
		List<Object[]> list = query.getResultList();
		List<LegalRepresentative> listLegal = new ArrayList<LegalRepresentative>();
		LegalRepresentative legalRepresentative;
		for(Object[] temp:list){
			legalRepresentative = new LegalRepresentative();
			if(Validations.validateIsNotNullAndNotEmpty(temp[0])){
				legalRepresentative.setIdLegalRepresentativePk(Long.parseLong(temp[0].toString()));
			}
			if(Validations.validateIsNotNullAndNotEmpty(temp[1])){
				legalRepresentative.setName(temp[1].toString());
			}
			if(Validations.validateIsNotNullAndNotEmpty(temp[2])){
				legalRepresentative.setFullName(temp[2].toString());
			}
			if(Validations.validateIsNotNullAndNotEmpty(temp[3])){
				legalRepresentative.setFirstLastName(temp[3].toString());
			}
			if(Validations.validateIsNotNullAndNotEmpty(temp[5])){
				legalRepresentative.setLegalResidenceCountry(Integer.parseInt(temp[5].toString()));
			}
			if(Validations.validateIsNotNullAndNotEmpty(temp[6])){
				legalRepresentative.setNationality(Integer.parseInt(temp[6].toString()));
			}
			if(Validations.validateIsNotNullAndNotEmpty(temp[7])){
				legalRepresentative.setHolderfk(Long.parseLong(temp[7].toString()));
			}
			if(Validations.validateIsNotNullAndNotEmpty(temp[8])){
				legalRepresentative.setDocumentType(Integer.parseInt(temp[8].toString()));
			}
			if(Validations.validateIsNotNullAndNotEmpty(temp[9])){
				legalRepresentative.setDocumentNumber(temp[9].toString());
			}

			listLegal.add(legalRepresentative);
		}
		return listLegal;
	}
	
	public Holder getHolderByDocumentTypeAndDocumentNumberServiceBean(Holder holder){
		
		Holder resultHolder = null;
		
		try{
		  StringBuilder stringBuilderSql = new StringBuilder();			
		  stringBuilderSql.append("SELECT h FROM Holder h WHERE  ( (h.documentType = :documentType and h.documentNumber = :documentNumber) or ");	      
		  stringBuilderSql.append(" (h.secondDocumentType = :documentType and h.secondDocumentNumber = :documentNumber) )");
		  
		  if(holder.getIdHolderPk()!=null){
			  stringBuilderSql.append(" and h.idHolderPk != :idHolderPk ");
		  }
		  
		  if(holder.getDocumentType()!=null && holder.getDocumentType().equals(DocumentType.DIO.getCode())){
			  if(holder.getNationality()!=null){
				  stringBuilderSql.append(" and (h.nationality = :nationality or h.secondNationality = :nationality)");
			  }
		  }
		  if(holder.getDocumentSource()!=null){
			  //stringBuilderSql.append(" and h.documentSource = :documentSource");
		  }
		  
		  if(holder.getListSectorActivity()!=null){
			  stringBuilderSql.append(" and h.economicActivity in :listEconomicActivity");
		  }
		  
		  stringBuilderSql.append(" and h.stateHolder != 1139 "); //que no tome en cuenta el transferido
		  
		  Query query = em.createQuery(stringBuilderSql.toString());
		  
		  query.setParameter("documentType", holder.getDocumentType());	
		  query.setParameter("documentNumber", holder.getDocumentNumber());	
		  
		  if(holder.getIdHolderPk()!=null){
			  query.setParameter("idHolderPk", holder.getIdHolderPk());
		  }
		  if(holder.getDocumentType()!=null && holder.getDocumentType().equals(DocumentType.DIO.getCode())){
			  if(holder.getNationality()!=null){
				  query.setParameter("nationality", holder.getNationality());
			  }
		  }
		  if(holder.getDocumentSource()!=null){
			  //query.setParameter("documentSource", holder.getDocumentSource());
		  }
		  if(holder.getListSectorActivity()!=null){
			  query.setParameter("listEconomicActivity",holder.getListSectorActivity());
		  }
		  
		  if(query.getResultList().size()<1){
			  resultHolder = null;
		  }
		  else if(query.getResultList().size()>0){
			  List<Holder> listHolder = (List<Holder>) query.getResultList();
			  resultHolder = listHolder.get(0);
		  }
		  
		}
		catch(Exception e){
			resultHolder = null;
		}
		
		return resultHolder;
	} 
	
	@SuppressWarnings("unchecked")
	public List<Holder> getHoldersServiceBean(Holder holder){
		
		List<Holder> resultHolder = null;
		
		try{
		  StringBuilder stringBuilderSql = new StringBuilder();			
		  stringBuilderSql.append("SELECT h FROM Holder h WHERE  ( (h.documentType = :documentType and h.documentNumber = :documentNumber) or ");	      
		  stringBuilderSql.append(" (h.secondDocumentType = :documentType and h.secondDocumentNumber = :documentNumber) )");
		  
		  if(holder.getIdHolderPk()!=null){
			  stringBuilderSql.append(" and h.idHolderPk != :idHolderPk ");
		  }
		  
		  if(holder.getDocumentType()!=null && holder.getDocumentType().equals(DocumentType.DIO.getCode())){
			  if(holder.getNationality()!=null){
				  stringBuilderSql.append(" and (h.nationality = :nationality or h.secondNationality = :nationality)");
			  }
		  }
		  if(holder.getDocumentSource()!=null){
			  //stringBuilderSql.append(" and h.documentSource = :documentSource");
		  }
		  
		  if(holder.getListSectorActivity()!=null){
			  stringBuilderSql.append(" and h.economicActivity in :listEconomicActivity");
		  }
		  
		  Query query = em.createQuery(stringBuilderSql.toString());
		  
		  query.setParameter("documentType", holder.getDocumentType());	
		  query.setParameter("documentNumber", holder.getDocumentNumber());	
		  
		  if(holder.getIdHolderPk()!=null){
			  query.setParameter("idHolderPk", holder.getIdHolderPk());
		  }
		  if(holder.getDocumentType()!=null && holder.getDocumentType().equals(DocumentType.DIO.getCode())){
			  if(holder.getNationality()!=null){
				  query.setParameter("nationality", holder.getNationality());
			  }
		  }
		  if(holder.getDocumentSource()!=null){
			  //query.setParameter("documentSource", holder.getDocumentSource());
		  }
		  if(holder.getListSectorActivity()!=null){
			  query.setParameter("listEconomicActivity",holder.getListSectorActivity());
		  }
		  
		  resultHolder = (List<Holder>)query.getResultList();
		  
		}
		catch(Exception e){
			resultHolder = null;
		}
		
		return resultHolder;
	}
	
	public PepPerson getPepPersonByIdHolderServiceBean(Long idHolderPk){
		PepPerson pepPerson;
		try{
		  StringBuilder stringBuilderSql = new StringBuilder();
		  stringBuilderSql.append("SELECT p FROM PepPerson p WHERE p.holder.idHolderPk = :idHolderPk");	      
		  Query query = em.createQuery(stringBuilderSql.toString());
		  query.setParameter("idHolderPk", idHolderPk);	 
		  pepPerson = (PepPerson) query.getSingleResult();
		}
		catch(Exception e){
			pepPerson = null;
		}
	
		return pepPerson;
		
	}
	
	public LegalRepresentative getLegalRepresentativeByDocumentTypeAndDocumentNumberServiceBean(LegalRepresentative legalRepresentative) throws ServiceException {
		
		LegalRepresentative legalRepresentativeResult = null;
		
		try{
		  StringBuilder stringBuilderSql = new StringBuilder();			
		  stringBuilderSql.append("SELECT h FROM LegalRepresentative h WHERE ( (h.documentType = :documentType and h.documentNumber = :documentNumber) or ");	      
		  stringBuilderSql.append(" (h.secondDocumentType = :documentType and h.secondDocumentNumber = :documentNumber) ) ");
		  
		  if(legalRepresentative.getIdLegalRepresentativePk()!=null){
			  stringBuilderSql.append(" and h.idLegalRepresentativePk != :idLegalRepresentativePk ");
		  }
		  
		  if(legalRepresentative.getDocumentType()!=null && legalRepresentative.getDocumentType().equals(DocumentType.DIO.getCode())){
			  if(legalRepresentative.getNationality()!=null){
				  stringBuilderSql.append(" and (h.nationality = :nationality or h.secondNationality = :nationality)");
			  }
		  }
		  
		  if(legalRepresentative.getDocumentSource()!=null){
			  //stringBuilderSql.append(" and h.documentSource = :documentSource");
		  }
		  
		  
		  Query query = em.createQuery(stringBuilderSql.toString());
		  query.setParameter("documentType", legalRepresentative.getDocumentType());	
		  query.setParameter("documentNumber", legalRepresentative.getDocumentNumber());	
		  
		  if(legalRepresentative.getIdLegalRepresentativePk()!=null){
			  query.setParameter("idLegalRepresentativePk", legalRepresentative.getIdLegalRepresentativePk());
		  }
		  if(legalRepresentative.getDocumentType()!=null && legalRepresentative.getDocumentType().equals(DocumentType.DIO.getCode())){
			  if(legalRepresentative.getNationality()!=null){
				  query.setParameter("nationality", legalRepresentative.getNationality());
			  }
		  }
		  
		  if(legalRepresentative.getDocumentSource()!=null){
			  //query.setParameter("documentSource",legalRepresentative.getDocumentSource());
		  }
		  
		  if(query.getResultList().size()<1){
			  legalRepresentativeResult = null;			  
		  }else if(query.getResultList().size()>0){
			  legalRepresentativeResult = (LegalRepresentative) query.getResultList().get(0);
		  }
		  
		}
		catch(Exception e){
			legalRepresentativeResult = null;
		}
		
		return legalRepresentativeResult;
	}
		
	public RepresentedEntity getRepresentedEntityByIdLegalRepresentativeServiceBean(Long id)throws ServiceException {
		
		  StringBuilder stringBuilderSql = new StringBuilder();			
		  stringBuilderSql.append("SELECT e FROM RepresentedEntity e WHERE e.legalRepresentative.idLegalRepresentativePk = :idLegalRepresentative ");	      
		 
		  TypedQuery<RepresentedEntity> query = em.createQuery(stringBuilderSql.toString(),RepresentedEntity.class);
		  query.setParameter("idLegalRepresentative",id);	
		 	
		  List<RepresentedEntity> resultListRepresentedEntity = (List<RepresentedEntity>) query.getResultList(); 
		 
			 if(Validations.validateListIsNotNullAndNotEmpty(resultListRepresentedEntity)){
				 return resultListRepresentedEntity.get(0);
			 } else {
				 return null;
			 }
		
	}
	
	@SuppressWarnings("unchecked")
	public List<LegalRepresentativeFile> getLegalRepresentativeFileByIdLegalRepresentativeServiceBean(Long id){
		  StringBuilder stringBuilderSql = new StringBuilder();
		  stringBuilderSql.append("SELECT r FROM LegalRepresentativeFile r WHERE r.legalRepresentative.idLegalRepresentativePk = :idLegalRepresentative and r.stateFile = :state");	
		  Query query = em.createQuery(stringBuilderSql.toString());
		  query.setParameter("idLegalRepresentative", id);
		  query.setParameter("state",HolderFileStateType.REGISTERED.getCode());
		 
		  return (List<LegalRepresentativeFile>) query.getResultList();
	}
	
	public PepPerson getPepPersonByIdLegalRepresentativeServiceBean(Long idLegalRepresentativePk){
		PepPerson pepPerson;
		try{
		  StringBuilder stringBuilderSql = new StringBuilder();
		  stringBuilderSql.append("SELECT p FROM PepPerson p WHERE p.legalRepresentative.idLegalRepresentativePk = :idLegalRepresentativePk");	      
		  Query query = em.createQuery(stringBuilderSql.toString());
		  query.setParameter("idLegalRepresentativePk", idLegalRepresentativePk);	 
	      pepPerson = (PepPerson) query.getSingleResult();
		}
		catch(Exception e){
			pepPerson = null;
		}
		return pepPerson;
	}
	
	public InstitutionInformation findInstitutionInformationByIdServiceBean(InstitutionInformation nitFilter) throws ServiceException {
		try{
			Query query = em.createNamedQuery(InstitutionInformation.INSTITUTION_INFORMATION_BY_ID);
			query.setParameter("docTyp",nitFilter.getDocumentType());
			query.setParameter("docNum", nitFilter.getDocumentNumber());
			
			return (InstitutionInformation) query.getSingleResult();
		} catch(NoResultException ex){
		   return null;
		} catch(NonUniqueResultException ex){
		   return null;
		}
	}
	
	public LegalRepresentative getLegalRepresentativeBySecondDocumentTypeAndSecondDocumentNumberServiceBean(LegalRepresentative legalRepresentative) throws ServiceException {
		
		try{
		  StringBuilder stringBuilderSql = new StringBuilder();			
		  stringBuilderSql.append("SELECT h FROM LegalRepresentative h WHERE ( (h.documentType = :secondDocumentType and h.documentNumber = :secondDocumentNumber) or ");	      
		  stringBuilderSql.append(" (h.secondDocumentType = :secondDocumentType and h.SecondDocumentNumber = :secondDocumentNumber) )");
		  
		  if(legalRepresentative.getIdLegalRepresentativePk()!=null){
			  stringBuilderSql.append(" and h.idLegalRepresentativePk != :idLegalRepresentativePk ");
		  }
		  
		  if(legalRepresentative.getSecondDocumentType()!=null && legalRepresentative.getSecondDocumentType().equals(DocumentType.DIO.getCode())){
			  if(legalRepresentative.getSecondNationality()!=null){
				  stringBuilderSql.append(" and (h.nationality = :secondNationality or h.secondNationality = :secondNationality)");
			  }
		  }
		  
		  Query query = em.createQuery(stringBuilderSql.toString());
		  query.setParameter("secondDocumentType", legalRepresentative.getSecondDocumentType());	
		  query.setParameter("secondDocumentNumber", legalRepresentative.getSecondDocumentNumber());	
		 
		  
		  if(legalRepresentative.getIdLegalRepresentativePk()!=null){
			  query.setParameter("idLegalRepresentativePk", legalRepresentative.getIdLegalRepresentativePk());
		  }
		  if(legalRepresentative.getSecondDocumentType()!=null && legalRepresentative.getSecondDocumentType().equals(DocumentType.DIO.getCode())){
			  if(legalRepresentative.getSecondNationality()!=null){
				  query.setParameter("secondNationality", legalRepresentative.getSecondNationality());
			  }
		  }
		  
		  if(query.getResultList().size()<1){
			  legalRepresentative = null;
		  }
		  else if(query.getResultList().size()>0){
			  legalRepresentative = (LegalRepresentative)query.getResultList().get(0);  
		  }
		  
		   
		}
		catch(Exception e){
			legalRepresentative = null;
		}
		
		return legalRepresentative;
	}
	
	public Holder getHolderBySecondDocumentTypeAndSecondDocumentNumberServiceBean(Holder holder){
		
		Holder resultHolder = new Holder();
		
		try{
			StringBuilder stringBuilderSql = new StringBuilder();
			stringBuilderSql.append("SELECT h FROM Holder h WHERE 1=1");
			
			if(holder.getSecondDocumentType()!=null && holder.getSecondDocumentNumber()!=null){
				stringBuilderSql.append(" and ( (h.documentType = :secondDocumentType and h.documentNumber = :secondDocumentNumber)"); 
		    
				stringBuilderSql.append(" or (h.secondDocumentType = :secondDocumentType and h.secondDocumentNumber = :secondDocumentNumber) )");
			}
			
			
		    if(holder.getIdHolderPk()!=null){
				  stringBuilderSql.append(" and h.idHolderPk != :idHolderPk ");
			  }
			  
			if(holder.getSecondDocumentType()!=null && holder.getSecondDocumentType().equals(DocumentType.DIO.getCode())){
				  if(holder.getSecondNationality()!=null){
					  stringBuilderSql.append(" and (h.nationality = :secondNationality or h.secondNationality = :secondNationality)");
				  }
			}
		    
		    Query query = em.createQuery(stringBuilderSql.toString());
		    
		    if(holder.getSecondDocumentType()!=null && holder.getSecondDocumentNumber()!=null){
		    	query.setParameter("secondDocumentType", holder.getSecondDocumentType());	
		    	query.setParameter("secondDocumentNumber", holder.getSecondDocumentNumber());	
		    }
		 
			if(holder.getIdHolderPk()!=null){
				  query.setParameter("idHolderPk", holder.getIdHolderPk());
			  }
			  if(holder.getSecondDocumentType()!=null && holder.getSecondDocumentType().equals(DocumentType.DIO.getCode())){
				  if(holder.getSecondNationality()!=null){
					  query.setParameter("secondNationality", holder.getSecondNationality());
				  }
			  }
			
			
			if(query.getResultList().size()<1){
				resultHolder = null;
			}
			else if(query.getResultList().size()>0){
				resultHolder =  (Holder)query.getResultList().get(0);
				
			}			
			
		}
		catch(Exception e){
			resultHolder = null;
		}
	
		return resultHolder;
	}

	public Integer validateExistPepPersonByIdLegalRepresentativeServiceBean(Long idLegalRepresentativePk){
		StringBuilder stringBuilderSql = new StringBuilder();
		stringBuilderSql.append("Select count(*) FROM PepPerson p Where p.legalRepresentative.idLegalRepresentativePk = :idLegalRepresentativePk");
		Query query = em.createQuery(stringBuilderSql.toString());
		query.setParameter("idLegalRepresentativePk", idLegalRepresentativePk);	 
   	    
		return Integer.parseInt(query.getSingleResult().toString());
	}
	
	@SuppressWarnings("unchecked")
	public List<HolderAccount> getAccountsHolderAndParticipant(Long holderId, Long participantId) throws ServiceException{
		StringBuilder idAccountSql = new StringBuilder();
		idAccountSql.append("Select ha.idHolderAccountPk From HolderAccount	ha							");
		idAccountSql.append("Inner Join	ha.holderAccountDetails det										");
		idAccountSql.append("Where ha.participant.idParticipantPk = :participantId And					");
		idAccountSql.append("	   det.holder.idHolderPk = :holderId									");
		Query queryAccountId = em.createQuery(idAccountSql.toString());
		queryAccountId.setParameter("participantId", participantId);
		queryAccountId.setParameter("holderId", holderId);
		
		List<Long> accountsID = (ArrayList<Long>)queryAccountId.getResultList();
		
		if(accountsID!=null && !accountsID.isEmpty()){
			StringBuilder stringBuilderSql = new StringBuilder();
			stringBuilderSql.append("Select Distinct account From HolderAccount	account						");
			stringBuilderSql.append("Inner Join Fetch	account.holderAccountDetails detailAccount			");
			stringBuilderSql.append("Inner Join Fetch	detailAccount.holder	holderAcc					");
			stringBuilderSql.append("Where account.participant.idParticipantPk = :participantId And			");
			stringBuilderSql.append("	   account.idHolderAccountPk IN :accountsID");
			
			Map<String,Object> parameters = new HashMap<String,Object>();
			parameters.put("participantId", participantId);
			parameters.put("accountsID", accountsID);
			
			return findListByQueryString(stringBuilderSql.toString(),parameters);
		}else{
			return new ArrayList<HolderAccount>();
		}
	}

	public HolderAccount getHolderAccountByParticipant(Long accountNumber, Long participantId) {
		HolderAccount objHolderAccount = null;
		try {
			StringBuilder idAccountSql = new StringBuilder();
			idAccountSql.append(" Select ha From HolderAccount	ha							");
			idAccountSql.append(" Where ha.participant.idParticipantPk = :participantId 	");
			idAccountSql.append(" And ha.accountNumber = :accountNumber						");
			Query queryAccountId = em.createQuery(idAccountSql.toString());
			queryAccountId.setParameter("participantId", participantId);
			queryAccountId.setParameter("accountNumber", new Integer(accountNumber.toString()));
			objHolderAccount = new HolderAccount();
			objHolderAccount = (HolderAccount) queryAccountId.getSingleResult();
		} catch(Exception e){
			objHolderAccount = null;
		}			
		return objHolderAccount;
	}
	
	public RepresentativeFileHistory getContentFileRepresentativeHistoryServiceBean(Long id){
		
		RepresentativeFileHistory representativeFileHistory;
		try
		{
		StringBuilder stringBuilderSql = new StringBuilder();
		  stringBuilderSql.append("SELECT r FROM RepresentativeFileHistory r WHERE r.idRepresentFileHisPk = :idRepresentFileHisPk ");	
		  Query query = em.createQuery(stringBuilderSql.toString());
		  query.setParameter("idRepresentFileHisPk", id);
		  
		  representativeFileHistory = (RepresentativeFileHistory) query.getSingleResult();
		}
		catch(Exception e){
			representativeFileHistory = null;
		}
		  
		return representativeFileHistory;
	}
	
	public LegalRepresentativeFile getContentFileLegalRepresentativeServiceBean(Long id){
		  StringBuilder stringBuilderSql = new StringBuilder();
		  stringBuilderSql.append("SELECT r FROM LegalRepresentativeFile r WHERE r.idLegalRepreFilePk = :idLegalRepreFilePk");	
		  Query query = em.createQuery(stringBuilderSql.toString());
		  query.setParameter("idLegalRepreFilePk", id);
		 return (LegalRepresentativeFile) query.getSingleResult();
	}
	
	public void removeLegalRepresentativeServiceBean(Long id){
		StringBuilder stringBuilderSql = new StringBuilder();
		stringBuilderSql.append("Delete FROM LegalRepresentativeHistory r WHERE r.holderRequest.idHolderRequestPk = :idHolderRequestPk ");	
		stringBuilderSql.append(" and r.registryType = :registryType");
		Query query = em.createQuery(stringBuilderSql.toString());
		query.setParameter("idHolderRequestPk", id);	
		query.setParameter("registryType", RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
		query.executeUpdate();
	}
	
	public void removeLegalRepresentativeHistoryFileServiceBean(Long id){
		StringBuilder stringBuilderSql = new StringBuilder();
		  stringBuilderSql.append("Delete FROM RepresentativeFileHistory r WHERE r.legalRepresentativeHistory.idRepresentativeHistoryPk = :idRepresentativeHistoryPk ");	
		  stringBuilderSql.append(" and r.registryType = :registryType");
		  Query query = em.createQuery(stringBuilderSql.toString());
		  query.setParameter("idRepresentativeHistoryPk", id);		
		  query.setParameter("registryType", RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
		  query.executeUpdate();
	}
	
	public int deleteLegalRepresentativeFileServiceBean(LegalRepresentative legalRepresentative, LoggerUser loggerUser)throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Delete FROM LegalRepresentativeFile lrf"); 
		sbQuery.append(" Where lrf.legalRepresentative.idLegalRepresentativePk = :idLegalRepresentative ");
		
		Query query = em.createQuery(sbQuery.toString());
					
		query.setParameter("idLegalRepresentative", legalRepresentative.getIdLegalRepresentativePk());

		return query.executeUpdate();
	}
	
	public Holder getHolderServiceBean(Long idHolderPk){
		Holder holder;
		try{
		  StringBuilder stringBuilderSql = new StringBuilder();
		  stringBuilderSql.append("SELECT h FROM Holder h WHERE h.idHolderPk = :idHolderPk");
	      
		  Query query = em.createQuery(stringBuilderSql.toString());
		  query.setParameter("idHolderPk", idHolderPk);	  
		  holder = (Holder) query.getSingleResult();
		}
		catch(Exception e){
			holder = null;
		}
		 
		return holder;
	}
	
	public boolean validateExistHolderServiceBean(Long idHolderPk){
		boolean exist=false;
		try{
		  StringBuilder stringBuilderSql = new StringBuilder();
		  stringBuilderSql.append(" select count(h.idHolderPk) ");
		  stringBuilderSql.append(" from Holder h ");
		  stringBuilderSql.append(" where 1 = 1 ");
		  stringBuilderSql.append(" and h.idHolderPk = :idHolderPk");
	      
		  Query query = em.createQuery(stringBuilderSql.toString());
		  query.setParameter("idHolderPk", idHolderPk);	 
		  		  
		  int count = Integer.parseInt(query.getSingleResult().toString());
		  if(count>0){
			  exist=true;
		  }
		}
		catch(Exception e){
			exist=false;
		}
		return exist;

	}
	public synchronized Integer getCorrelativeNumberHolderAccountServiceBean(Long idParticipantPK) throws ServiceException{
		Query query = em.createQuery("SELECT MAX(ha.accountNumber) + 1 FROM HolderAccount ha WHERE ha.participant.idParticipantPk = :idParticipantPkParameter");
		query.setParameter("idParticipantPkParameter", idParticipantPK);
		Integer correlative = 0;
		try{
			correlative = (Integer) query.getSingleResult();
		}catch(Exception ex){
			correlative = null;
		}
		if(Validations.validateIsNull(correlative)){
			correlative = 1;
		}
		return correlative;
	}
	
	public List<NegotiationModality> getListNegotiationModality(Long mechanismPk, Integer instrumentTyp, Long participantPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT mod FROM MechanismModality 	mm							");
		sbQuery.append("	INNER JOIN mm.negotiationModality	mod							");
		
		if(participantPk!=null){
			sbQuery.append("	INNER JOIN mm.participantMechanisms	partMech	");
		}
		
		sbQuery.append("	WHERE mm.stateMechanismModality = :state	");
		sbQuery.append("	AND mm.negotiationModality.modalityState = :modalityState	");
		sbQuery.append("	AND mm.id.idNegotiationMechanismPk = :idNegotiationMechanismPk	");
		if(instrumentTyp!=null) {
			sbQuery.append("	AND mod.instrumentType = :instrumentType						");
		}
		if(participantPk!=null){
			sbQuery.append("	 AND 	partMech.participant.idParticipantPk = :participantPk	");
			sbQuery.append("	 AND 	partMech.stateParticipantMechanism	 = :statePartMech	");
		}
		
		sbQuery.append("	ORDER BY mm.negotiationModality.modalityName ASC");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("state", MechanismModalityStateType.ACTIVE.getCode());
		query.setParameter("modalityState", NegotiationModalityStateType.ACTIVE.getCode());
		query.setParameter("idNegotiationMechanismPk", mechanismPk);
		if(instrumentTyp!=null) {
			query.setParameter("instrumentType", instrumentTyp);
		}
		if(participantPk!=null){
			query.setParameter("participantPk", participantPk);
			query.setParameter("statePartMech", ParticipantMechanismStateType.REGISTERED.getCode());
		}
		
		return (List<NegotiationModality>)query.getResultList();
	}
	
	
}