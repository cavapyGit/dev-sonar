package com.pradera.core.component.accounts.to;

import java.io.Serializable;

public class InternationalDepositoryTO implements Serializable{
	
	private Integer stateIntDepository;
	
	public InternationalDepositoryTO() {
		// TODO Auto-generated constructor stub
	}

	public Integer getStateIntDepository() {
		return stateIntDepository;
	}

	public void setStateIntDepository(Integer stateIntDepository) {
		this.stateIntDepository = stateIntDepository;
	}
	

}
