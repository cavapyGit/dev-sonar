package com.pradera.core.component.accounts.to;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class RepresentedEntityTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 22/03/2013
 */
public class RepresentedEntityTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The id represented entity pk. */
	private Long idRepresentedEntityPk;
	
	/** The id holder pk. */
	private Long idHolderPk;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The state represented entity. */
	private Integer stateRepresentedEntity;
	
	/** The state legal representative. */
	private Integer stateLegalRepresentative;
	
	/**
	 * Instantiates a new represented entity to.
	 */
	public RepresentedEntityTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the id represented entity pk.
	 *
	 * @return the id represented entity pk
	 */
	public Long getIdRepresentedEntityPk() {
		return idRepresentedEntityPk;
	}

	/**
	 * Sets the id represented entity pk.
	 *
	 * @param idRepresentedEntityPk the new id represented entity pk
	 */
	public void setIdRepresentedEntityPk(Long idRepresentedEntityPk) {
		this.idRepresentedEntityPk = idRepresentedEntityPk;
	}

	/**
	 * Gets the id holder pk.
	 *
	 * @return the id holder pk
	 */
	public Long getIdHolderPk() {
		return idHolderPk;
	}

	/**
	 * Sets the id holder pk.
	 *
	 * @param idHolderPk the new id holder pk
	 */
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}

	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * Gets the state represented entity.
	 *
	 * @return the state represented entity
	 */
	public Integer getStateRepresentedEntity() {
		return stateRepresentedEntity;
	}

	/**
	 * Sets the state represented entity.
	 *
	 * @param stateRepresentedEntity the new state represented entity
	 */
	public void setStateRepresentedEntity(Integer stateRepresentedEntity) {
		this.stateRepresentedEntity = stateRepresentedEntity;
	}

	/**
	 * Gets the state legal representative.
	 *
	 * @return the state legal representative
	 */
	public Integer getStateLegalRepresentative() {
		return stateLegalRepresentative;
	}

	/**
	 * Sets the state legal representative.
	 *
	 * @param stateLegalRepresentative the new state legal representative
	 */
	public void setStateLegalRepresentative(Integer stateLegalRepresentative) {
		this.stateLegalRepresentative = stateLegalRepresentative;
	}		

}
