package com.pradera.core.component.helperui.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;

public class HolderSearchTO implements Serializable {

	private static final long serialVersionUID = -1229032786660948360L;
	private List<ParameterTable> lstPersonType, lstDocumentType, lstState;
	private Integer personType, documentType, state;
	private String documentNumber, name, firstLastName, secondLastName, fullName;
	private boolean blNoResult, blNatural, blJuridic, blPersonType,blIssuer;
	private Issuer issuer;
	private Participant participant;
	private GenericDataModel<Holder> lstHolder;
	
	public List<ParameterTable> getLstPersonType() {
		return lstPersonType;
	}
	public void setLstPersonType(List<ParameterTable> lstPersonType) {
		this.lstPersonType = lstPersonType;
	}
	public List<ParameterTable> getLstDocumentType() {
		return lstDocumentType;
	}
	public void setLstDocumentType(List<ParameterTable> lstDocumentType) {
		this.lstDocumentType = lstDocumentType;
	}
	public List<ParameterTable> getLstState() {
		return lstState;
	}
	public void setLstState(List<ParameterTable> lstState) {
		this.lstState = lstState;
	}
	public Integer getPersonType() {
		return personType;
	}
	public void setPersonType(Integer personType) {
		this.personType = personType;
	}
	public Integer getDocumentType() {
		return documentType;
	}
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public String getDocumentNumber() {
		return documentNumber;
	}
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFirstLastName() {
		return firstLastName;
	}
	public void setFirstLastName(String firstLastName) {
		this.firstLastName = firstLastName;
	}
	public String getSecondLastName() {
		return secondLastName;
	}
	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public boolean isBlNoResult() {
		return blNoResult;
	}
	public void setBlNoResult(boolean blNoResult) {
		this.blNoResult = blNoResult;
	}
	public boolean isBlNatural() {
		return blNatural;
	}
	public void setBlNatural(boolean blNatural) {
		this.blNatural = blNatural;
	}
	public boolean isBlJuridic() {
		return blJuridic;
	}
	public void setBlJuridic(boolean blJuridic) {
		this.blJuridic = blJuridic;
	}
	public GenericDataModel<Holder> getLstHolder() {
		return lstHolder;
	}
	public void setLstHolder(GenericDataModel<Holder> lstHolder) {
		this.lstHolder = lstHolder;
	}
	public boolean isBlPersonType() {
		return blPersonType;
	}
	public void setBlPersonType(boolean blPersonType) {
		this.blPersonType = blPersonType;
	}
	public Participant getParticipant() {
		return participant;
	}
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}
	public boolean isBlIssuer() {
		return blIssuer;
	}
	public void setBlIssuer(boolean blIssuer) {
		this.blIssuer = blIssuer;
	}
	public Issuer getIssuer() {
		return issuer;
	}
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}
	
}
