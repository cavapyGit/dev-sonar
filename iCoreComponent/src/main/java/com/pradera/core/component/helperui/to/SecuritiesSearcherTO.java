package com.pradera.core.component.helperui.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.model.accounts.holderaccounts.HolderAccount;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class SecuritiesSearcherTO.
 * Transfer object for search securities
 * @author PraderaTechnologies.
 * @version 1.0 , 18/01/2013
 */
public class SecuritiesSearcherTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 876495408650965510L;
	
	/** The isin code. */
	private String isinCode;
	
	/** The description. */
	private String description;
	
	/** The issuer code. */
	private String issuerCode;
	
	/** The issuer description. */
	private String issuerDescription;
	
	/** The instrument type. */
	private Integer instrumentType;
	
	/** The state securitie. */
	private Integer stateSecuritie;
	
	/** The participant code. */
	private Long participantCode;
	
	/** The holder code. */
	private Long holderCode;
	
	/** The class securitie. */
	private Integer classSecuritie;
	
	/** The class securitie. */
	private List<Integer> listClassSecuritie;
	
	/** The securities type. */
	private Integer securitiesType;
	
	private String securityCode;	
	private String mnemonic;
	private String idSecurityCodeOnly;
	
	/** The holder account. */
	private HolderAccount holderAccount = new HolderAccount();
	
	private List<Long> lstHolderAccountPk;

	/**
	 * Instantiates a new securities searcher to.
	 */
	public SecuritiesSearcherTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the isin code.
	 *
	 * @return the isin code
	 */
	public String getIsinCode() {
		return isinCode;
	}

	/**
	 * Sets the isin code.
	 *
	 * @param isinCode the new isin code
	 */
	public void setIsinCode(String isinCode) {
		this.isinCode = isinCode;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the issuer code.
	 *
	 * @return the issuer code
	 */
	public String getIssuerCode() {
		return issuerCode;
	}

	/**
	 * Sets the issuer code.
	 *
	 * @param issuerCode the new issuer code
	 */
	public void setIssuerCode(String issuerCode) {
		this.issuerCode = issuerCode;
	}

	/**
	 * Gets the issuer description.
	 *
	 * @return the issuer description
	 */
	public String getIssuerDescription() {
		return issuerDescription;
	}

	/**
	 * Sets the issuer description.
	 *
	 * @param issuerDescription the new issuer description
	 */
	public void setIssuerDescription(String issuerDescription) {
		this.issuerDescription = issuerDescription;
	}

	/**
	 * Gets the instrument type.
	 *
	 * @return the instrument type
	 */
	public Integer getInstrumentType() {
		return instrumentType;
	}

	/**
	 * Sets the instrument type.
	 *
	 * @param instrumentType the new instrument type
	 */
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	/**
	 * Gets the state securitie.
	 *
	 * @return the state securitie
	 */
	public Integer getStateSecuritie() {
		return stateSecuritie;
	}

	/**
	 * Sets the state securitie.
	 *
	 * @param stateSecuritie the new state securitie
	 */
	public void setStateSecuritie(Integer stateSecuritie) {
		this.stateSecuritie = stateSecuritie;
	}

	/**
	 * Gets the participant code.
	 *
	 * @return the participant code
	 */
	public Long getParticipantCode() {
		return participantCode;
	}

	/**
	 * Sets the participant code.
	 *
	 * @param participantCode the new participant code
	 */
	public void setParticipantCode(Long participantCode) {
		this.participantCode = participantCode;
	}

	/**
	 * Gets the holder code.
	 *
	 * @return the holder code
	 */
	public Long getHolderCode() {
		return holderCode;
	}

	/**
	 * Sets the holder code.
	 *
	 * @param holderCode the new holder code
	 */
	public void setHolderCode(Long holderCode) {
		this.holderCode = holderCode;
	}

	/**
	 * Gets the class securitie.
	 *
	 * @return the class securitie
	 */
	public Integer getClassSecuritie() {
		return classSecuritie;
	}

	/**
	 * Sets the class securitie.
	 *
	 * @param classSecuritie the new class securitie
	 */
	public void setClassSecuritie(Integer classSecuritie) {
		this.classSecuritie = classSecuritie;
	}

	public Integer getSecuritiesType() {
		return securitiesType;
	}

	public void setSecuritiesType(Integer securitiesType) {
		this.securitiesType = securitiesType;
	}

	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	public String getMnemonic() {
		return mnemonic;
	}

	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	public String getIdSecurityCodeOnly() {
		return idSecurityCodeOnly;
	}

	public void setIdSecurityCodeOnly(String idSecurityCodeOnly) {
		this.idSecurityCodeOnly = idSecurityCodeOnly;
	}

	public List<Integer> getListClassSecuritie() {
		return listClassSecuritie;
	}

	public void setListClassSecuritie(List<Integer> listClassSecuritie) {
		this.listClassSecuritie = listClassSecuritie;
	}

	public List<Long> getLstHolderAccountPk() {
		return lstHolderAccountPk;
	}

	public void setLstHolderAccountPk(List<Long> lstHolderAccountPk) {
		this.lstHolderAccountPk = lstHolderAccountPk;
	}

}
