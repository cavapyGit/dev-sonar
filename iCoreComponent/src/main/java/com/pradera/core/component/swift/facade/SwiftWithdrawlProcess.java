package com.pradera.core.component.swift.facade;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.SocketException;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.sql.rowset.serial.SerialException;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.swift.service.AutomaticReceptionFundsService;
import com.pradera.core.component.swift.service.AutomaticSendingFundsService;
import com.pradera.core.component.swift.service.CashAccountManagementService;
import com.pradera.core.component.swift.to.FTPParameters;
import com.pradera.core.component.swift.to.SwiftConstants;
import com.pradera.core.component.swift.to.SwiftMessagesRetirement;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.funds.FundsOperationType;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.SwiftMessage;
import com.pradera.model.funds.type.AccountCashFundsType;
import com.pradera.model.funds.type.CashAccountUseType;
import com.pradera.model.funds.type.FundsOperationGroupType;
import com.pradera.model.funds.type.FundsType;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.settlement.type.OperationPartType;
import com.pradera.model.swift.SwiftMessageType;


/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class HelperComponentFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/01/2013
 */
@Stateless
public class SwiftWithdrawlProcess {
	
	@Inject
	private PraderaLogger log;
	@EJB
	AutomaticReceptionFundsService receptionService;
	@EJB
	CashAccountManagementService cashAccountService;
	@EJB
	AutomaticSendingFundsService sendingService;
	@EJB
	SwiftWithdrawlProcess swiftWithdrawl;
	@Inject @Configurable
	String ftpLocalPath, ftpPass, ftpRemotePath, ftpServer, ftpUser,withdrawlBicBCRD;
	
	
	/**
	 * METHOD WHICH WILL BE IN CHARGE TO SEND THE FILE TO 
	 * @param fundOperation
	 * @throws IOException 
	 * @throws SQLException 
	 * @throws SerialException 
	 */
	public boolean executeSwiftWithdrawl(FundsOperation fundOperation, String bicBeneficiary, String origenPaymentReference) throws ServiceException{
		boolean processReturn = true;
		SwiftMessagesRetirement retirementMessage = new SwiftMessagesRetirement();
		try {
			Bank bankCentralizer = cashAccountService.getCentralizerBank();
			//DEPOSITARY BIC CODE
			String bicDepositary= receptionService.getDepositaryBicCode();
			retirementMessage.setCevaldomBicCode(bicDepositary);
			//BCR BIC CODE
			Integer currency = fundOperation.getCurrency();
			String bcrdBic = cashAccountService.getBcbBankBicValue(currency);
			retirementMessage.setBcrdBicCode(bcrdBic);

			Integer fundOperationGroup = fundOperation.getFundsOperationGroup();
			Long fundOperationType = fundOperation.getFundsOperationType();
			retirementMessage.setGroupOperationFund(fundOperationGroup);
			retirementMessage.setOperationType(fundOperationType);
			retirementMessage.setFundOperationTypeDescription(fundOperation.getFundsOpeTypeDescription());

			if(Validations.validateIsNullOrEmpty(fundOperation.getIndAutomaticDevolution())){
				if(FundsOperationGroupType.SETTELEMENT_FUND_OPERATION.getCode().equals(fundOperationGroup)){
					if(	com.pradera.model.component.type.ParameterFundsOperationType.FUNDS_SETTLEMENT_WITHDRAWL.getCode().equals(fundOperationType) || 
						com.pradera.model.component.type.ParameterFundsOperationType.DEVOLUTION_RECEPTIONS_FUNDS_WITHDRAWL_SETTLEMENT.getCode().equals(fundOperationType)){
						//AUTOMATIC SWIFT WITHDRAWL BY END DAY PROCESS OR REJECT OF OBSERVED FUND OPERATION
						//TRN
						String trn = receptionService.getTrn(fundOperationType,currency);
						retirementMessage.setTrn(trn);
						//MESSAGE TYPE
						retirementMessage.setMessageType(SwiftMessageType.MESSAGE_202.getDescription());
						//FUND OPERATION ID
						retirementMessage.setIdFundOperation(fundOperation.getIdFundsOperationPk());
						if(com.pradera.model.component.type.ParameterFundsOperationType.DEVOLUTION_RECEPTIONS_FUNDS_WITHDRAWL_SETTLEMENT.getCode().equals(fundOperationType)){
							//AUTOMATIC SWIFT WITHDRAWL BY REJECT OF OBSERVED FUND OPERATION - SETTLEMENT
							retirementMessage.setReceptorBicCode(fundOperation.getSenderBicCode());	
						}else{
							//AUTOMATIC SWIFT WITHDRAWL BY END DAY PROCESS
							//BENEFICIARY PARTICIPANT BIC CODE
							Participant participant = fundOperation.getParticipant();
							String bankBic = participant.getBicCode();
							//AUTOMATIC SWIFT WITHDRAWL BY END DAY PROCESS
							retirementMessage.setReceptorBicCode(bankBic);
						}

						MechanismOperation operation = fundOperation.getMechanismOperation();
						if(Validations.validateIsNotNull(operation)){
							retirementMessage.setSettlementSchema(operation.getSettlementSchema().toString());
						}else{
							retirementMessage.setSettlementSchema(SettlementSchemaType.NET.getCode().toString());
						}

						//PARTICIPANT BANK ACCOUNT 
						Participant participant = fundOperation.getParticipant();
						InstitutionCashAccount cashAccount = fundOperation.getInstitutionCashAccount();
						List<InstitutionBankAccount> bankAccounts = cashAccountService.getBankAccounts(cashAccount.getIdInstitutionCashAccountPk());
						if(Validations.validateListIsNotNullAndNotEmpty(bankAccounts)){
//							for(InstitutionBankAccount bankAccount : bankAccounts){
//								if(CashAccountUseType.SEND.getCode().equals(bankAccount.getUseTypeAccount()) || 
//									CashAccountUseType.BOTH.getCode().equals(bankAccount.getUseTypeAccount())){
//									if(MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_OWNER.getLngCode().equals(bankAccount.getBankAccountClass())){
//										//VALIDATION WITH OWNER ACCOUNT
//										//DATOS DEL PARTICIPANTE LIQUIDADOR
//										if(Validations.validateIsNotNull(participant)){
//											retirementMessage.setReceptorBicCode(participant.getBicCode());
//											retirementMessage.setParticipantSettlerDescription(participant.getDescription());
//										}else{
//											retirementMessage.setReceptorBicCode(fundOperation.getSenderBicCode());
//											retirementMessage.setParticipantSettlerDescription(StringUtils.EMPTY);
//										}
//										String sellerAccountNumber = bankAccount.getAccountNumber();
//										retirementMessage.setParticipantSettlerCentralAccountNumber(sellerAccountNumber);
//									}else{
//										//VALIDATION WITH OTHER BANK
//										Bank thirdBank = bankAccount.getProviderBank();
//										retirementMessage.setReceptorBicCode(thirdBank.getBicCode());
//										retirementMessage.setParticipantSettlerDescription(thirdBank.getDescription());
//										retirementMessage.setParticipantSettlerCentralAccountNumber(bankAccount.getAccountNumber());
//									}
//								}
//							}
						}
						//REFERENCE PAYMENT
						String paymentReference = fundOperation.getPaymentReference();
						if(Validations.validateIsNotNull(paymentReference)){//GROSS SETTLEMENT
							String completePaymentReference = paymentReference.substring(7,paymentReference.length());
							Long correlative = receptionService.getFundOperationWithdrawlCorrelative();
							//WE CHANGE THE TRN AND WILL USE THE ONE FOR DEVOLUTIONS ACCORDING TO THE CURRENCY - 3686
							if(CurrencyType.PYG.getCode().equals(fundOperation.getCurrency())){
								String newPaymentReference = FundsType.TRN_E000149.getStrCode().concat(completePaymentReference).concat(".").concat(correlative.toString());
								retirementMessage.setPaymentReference(newPaymentReference);
							}else if(CurrencyType.USD.getCode().equals(fundOperation.getCurrency())){
								String newPaymentReference = FundsType.TRN_E000340.getStrCode().concat(completePaymentReference).concat(correlative.toString());
								retirementMessage.setPaymentReference(newPaymentReference);
							}
						}else{//NET SETTLEMENT
							String netPaymentReference = StringUtils.EMPTY;
							if(CurrencyType.PYG.getCode().equals(fundOperation.getCurrency())){
								netPaymentReference = FundsType.TRN_E000149.getStrCode().concat(".").concat(StringUtils.EMPTY);
							}else if(CurrencyType.USD.getCode().equals(fundOperation.getCurrency())){
								netPaymentReference = FundsType.TRN_E000340.getStrCode().concat(".").concat(StringUtils.EMPTY);
							}
							retirementMessage.setPaymentReference(netPaymentReference);
						}

						retirementMessage.setSettlementDate(fundOperation.getRegistryDate());
						retirementMessage.setCurrency(getCurrencyParameter(fundOperation.getCurrency()));
						retirementMessage.setAmount(fundOperation.getOperationAmount());
					}else{
						//AUTOMATIC SWIFT WITHDRAWL BY SETTLEMENT PROCESS
						String trn = receptionService.getTrn(fundOperationType,currency);
						retirementMessage.setMessageType(SwiftMessageType.MESSAGE_103.getDescription());
						retirementMessage.setTrn(trn);

						InstitutionCashAccount cashAccount = fundOperation.getInstitutionCashAccount();
						Integer settlementSchema = cashAccount.getSettlementSchema();

						Long mechanism = null;
						Long modality = null;
						Long modalityGroup = null;
						if(SettlementSchemaType.GROSS.getCode().equals(settlementSchema)){
							MechanismOperation operation = fundOperation.getMechanismOperation();
							Participant participantBuyer = operation.getBuyerParticipant();
							Participant participant = fundOperation.getParticipant();
							
							retirementMessage.setOperationID(operation.getIdMechanismOperationPk());
							retirementMessage.setSettlementSchema(operation.getSettlementSchema().toString());

							//PAYMENT REFERENCE
							//VALIDATE WHICH TYPE OF SETTLEMENT WE ARE DOING TO IMPLEMENT PAYMENT REFERENCE WITH "C" or "P"
							if(OperationPartType.TERM_PART.getCode().equals(fundOperation.getOperationPart())){
								//TERM PART
								retirementMessage.setPaymentReference(operation.getPaymentReference().concat(".P"));
							}else{
								//CASH PART
								retirementMessage.setPaymentReference(operation.getPaymentReference().concat(".C"));
							}

							//SETTLER PARTICIPANT ACCOUNT
							List<InstitutionBankAccount> bankAccounts = cashAccountService.getBankAccounts(cashAccount.getIdInstitutionCashAccountPk());
							if(Validations.validateListIsNotNullAndNotEmpty(bankAccounts)){
//								for(InstitutionBankAccount bankAccount : bankAccounts){
//									if(CashAccountUseType.SEND.getCode().equals(bankAccount.getUseTypeAccount()) || 
//										CashAccountUseType.BOTH.getCode().equals(bankAccount.getUseTypeAccount())){
//										if(MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_OWNER.getLngCode().equals(bankAccount.getBankAccountClass())){
//											//VALIDATION WITH OWNER ACCOUNT
//											//DATOS DEL PARTICIPANTE LIQUIDADOR
//											String participantBicCode = participant.getBicCode();
//											retirementMessage.setReceptorBicCode(participantBicCode);
//											retirementMessage.setParticipantSettlerDescription(participant.getDescription());
//											String sellerAccountNumber = bankAccount.getAccountNumber();
//											retirementMessage.setParticipantSettlerCentralAccountNumber(sellerAccountNumber);
//										}else{
//											//VALIDATION WITH OTHER BANK
//											Bank thirdBank = bankAccount.getProviderBank();
//											retirementMessage.setReceptorBicCode(thirdBank.getBicCode());
//											retirementMessage.setParticipantSettlerDescription(thirdBank.getDescription());
//											retirementMessage.setParticipantSettlerCentralAccountNumber(bankAccount.getAccountNumber());
//										}
//									}
//								}
							}
							mechanism = operation.getMechanisnModality().getNegotiationMechanism().getIdNegotiationMechanismPk();
							modality = operation.getMechanisnModality().getNegotiationModality().getIdNegotiationModalityPk();
							modalityGroup = receptionService.getModalityGroup(mechanism, modality);

							//BUYER PARTICIPANT ACCOUNT
							Long participantBuyerPK = participantBuyer.getIdParticipantPk();
							InstitutionCashAccount buyerCashAccount = receptionService.getDestinyCashAccountSettlement(AccountCashFundsType.SETTLEMENT.getCode(), currency, 
																														mechanism, modalityGroup, participantBuyerPK);
							List<InstitutionBankAccount> buyerBankAccounts = cashAccountService.getBankAccounts(buyerCashAccount.getIdInstitutionCashAccountPk());
							if(Validations.validateListIsNotNullAndNotEmpty(buyerBankAccounts)){
//								for(InstitutionBankAccount buyerBankAccount : buyerBankAccounts){
//									if(	CashAccountUseType.RECEPTION.getCode().equals(buyerBankAccount.getUseTypeAccount()) || 
//										CashAccountUseType.BOTH.getCode().equals(buyerBankAccount.getUseTypeAccount())){
//										if(MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_OWNER.getLngCode().equals(buyerBankAccount.getBankAccountClass())){
//											//VALIDATION WITH OWNER ACCOUNT
//											retirementMessage.setParticipantBuyerDescription(participantBuyer.getDescription());
//											String buyerAccountNumber = buyerBankAccount.getAccountNumber();
//											retirementMessage.setParticipantBuyerCentralAccountNumber(buyerAccountNumber);
//										}else{
//											//VALIDATION WITH OTHER BANK
//											Bank thirdBank = buyerBankAccount.getProviderBank();
//											retirementMessage.setParticipantBuyerDescription(thirdBank.getDescription());
//											String buyerAccountNumber = buyerBankAccount.getAccountNumber();
//											retirementMessage.setParticipantBuyerCentralAccountNumber(buyerAccountNumber);
//										}
//									}
//								}
							}
							retirementMessage.setSettlementDate(operation.getCashSettlementDate());
							retirementMessage.setCurrency(getCurrencyParameter(operation.getCurrency()));
	
						}else if(SettlementSchemaType.NET.getCode().equals(settlementSchema)){
							mechanism = fundOperation.getInstitutionCashAccount().getNegotiationMechanism().getIdNegotiationMechanismPk();
							modalityGroup = fundOperation.getInstitutionCashAccount().getModalityGroup().getIdModalityGroupPk();
							Long participantPK = fundOperation.getParticipant().getIdParticipantPk();
							Participant participant = receptionService.find(participantPK,Participant.class);
							String participantBicCode = participant.getBicCode();

							retirementMessage.setPaymentReference(fundOperation.getPaymentReference());
							retirementMessage.setMechanism(cashAccount.getNegotiationMechanism().getMechanismCode());
							retirementMessage.setModalityGroup(cashAccount.getModalityGroup().getModalityGroupCode());
							retirementMessage.setSettlementSchema(settlementSchema.toString());
							retirementMessage.setReceptorBicCode(participantBicCode);
							//DATOS DEL PARTICIPANTE OF THE CASH ACCOUNT
							retirementMessage.setParticipantSettlerDescription(participant.getDescription());
							String sellerAccountNumber = receptionService.getFundsSendSettlementCashAccount(mechanism,modalityGroup,participantPK,currency,false);
							retirementMessage.setParticipantSettlerCentralAccountNumber(sellerAccountNumber);

							retirementMessage.setSettlementDate(CommonsUtilities.currentDate());
							retirementMessage.setCurrency(getCurrencyParameter(cashAccount.getCurrency()));
						}
						retirementMessage.setAmount(fundOperation.getOperationAmount());
					}
	
				}else if(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode().equals(fundOperationGroup)){
					if(	com.pradera.model.component.type.ParameterFundsOperationType.HOLDER_INTERESTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundOperationType) || 
						com.pradera.model.component.type.ParameterFundsOperationType.HOLDER_CAPITAL_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundOperationType) || 
						com.pradera.model.component.type.ParameterFundsOperationType.HOLDER_DIVIDENDS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundOperationType) || 
						com.pradera.model.component.type.ParameterFundsOperationType.HOLDER_REMANENTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundOperationType) || 
						com.pradera.model.component.type.ParameterFundsOperationType.HOLDER_CONTRIBUTION_RETURN_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundOperationType)){
	
						//TRN
						retirementMessage.setTrn(fundOperation.getPaymentReference());
						//MESSAGE TYPE
						retirementMessage.setMessageType(SwiftMessageType.MESSAGE_202.getDescription());
						//FUND OPERATION ID
						retirementMessage.setIdFundOperation(fundOperation.getIdFundsOperationPk());
						//BENEFICIARY BANK BIC CODE
						String bankBic = fundOperation.getBank().getBicCode();
						retirementMessage.setReceptorBicCode(bankBic);
						retirementMessage.setBicCodeBeneficiaryBank(bankBic);
						//FUND OPERATION PK
						retirementMessage.setOperationID(fundOperation.getIdFundsOperationPk());
						//REFERENCE PAYMENT
						retirementMessage.setSettlementDate(fundOperation.getRegistryDate());
						retirementMessage.setCurrency(getCurrencyParameter(fundOperation.getCurrency()));
						retirementMessage.setAmount(fundOperation.getOperationAmount());
						//CORPORATIVE PROCESS DESCRIPTION
						String corporativeDescription = fundOperation.getCorporativeDescription();
						retirementMessage.setCorporativeProcessDescription(corporativeDescription);

					}else if(com.pradera.model.component.type.ParameterFundsOperationType.INSTITUTIONS_INTERESTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundOperationType) || 
							com.pradera.model.component.type.ParameterFundsOperationType.INSTITUTIONS_CAPITAL_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundOperationType) || 
							com.pradera.model.component.type.ParameterFundsOperationType.INSTITUTIONS_DIVIDENDS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundOperationType) || 
							com.pradera.model.component.type.ParameterFundsOperationType.INSTITUTIONS_REMANENTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundOperationType) || 
							com.pradera.model.component.type.ParameterFundsOperationType.INSTITUTIONS_CONTRIBUTION_RETURN_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(fundOperationType)){
	
						//MESSAGE TYPE
						retirementMessage.setMessageType(SwiftMessageType.MESSAGE_103.getDescription());

						//BIC CODE
						retirementMessage.setReceptorBicCode(fundOperation.getWithdrawlSwiftBic());

						//TRN
						retirementMessage.setTrn(fundOperation.getPaymentReference());

						//FUND OPERATION PK
						retirementMessage.setIdFundOperation(fundOperation.getIdFundsOperationPk());

						//REFERENCE PAYMENT
						retirementMessage.setSettlementDate(fundOperation.getRegistryDate());
						retirementMessage.setCurrency(getCurrencyParameter(fundOperation.getCurrency()));
						retirementMessage.setAmount(fundOperation.getOperationAmount());

						//CEVALDOM CENTRAL BANK ACCOUNT
						
						Long bcrdBankPK = cashAccountService.getBCBBank(currency,String.valueOf(bankCentralizer.getIdBankPk()));
						String cevaldomAccountNumber = sendingService.getCevaldomBCRDAccountNumber(currency,bcrdBankPK);
						retirementMessage.setDepositaryCentralBankAccount(cevaldomAccountNumber);

						//CEVALDOM DESCRIPTION
						Participant cevaldom = cashAccountService.getDepositaryParticipant();
						retirementMessage.setDepositaryDescription(cevaldom.getDescription());

						//PARTICIPANT BENEFIT
						retirementMessage.setParticipantSettlerCentralAccountNumber(fundOperation.getBankAccountNumber());
						retirementMessage.setParticipantSettlerDescription(fundOperation.getHolderDescription());

						//CORPORATIVE PROCESS DESCRIPTION
						retirementMessage.setCorporativeProcessDescription(fundOperation.getCorporativeDescription());
					}else if(com.pradera.model.component.type.ParameterFundsOperationType.CUSTODY_COMISSION_WITHDRAWL_BENEFIT_PAYMENTS.getCode().equals(fundOperationType) ||
							com.pradera.model.component.type.ParameterFundsOperationType.ISSUER_COMISSION_WITHDRAWL_BENEFIT_PAYMENTS.getCode().equals(fundOperationType) || 
							com.pradera.model.component.type.ParameterFundsOperationType.BLOCK_BENEFITS_WITHDRAWL_RETENTION.getCode().equals(fundOperationType) || 
							com.pradera.model.component.type.ParameterFundsOperationType.TAX_BENEFITS_WITHDRAWL_RETENTION.getCode().equals(fundOperationType)){

						//MESSAGE TYPE
						retirementMessage.setMessageType(SwiftMessageType.MESSAGE_103.getDescription());
						//TRN
						retirementMessage.setTrn(fundOperation.getPaymentReference());
						//FUND OPERATION PK
						retirementMessage.setIdFundOperation(fundOperation.getIdFundsOperationPk());
						//REFERENCE PAYMENT
						retirementMessage.setSettlementDate(fundOperation.getRegistryDate());
						retirementMessage.setCurrency(getCurrencyParameter(fundOperation.getCurrency()));
						retirementMessage.setAmount(fundOperation.getOperationAmount());

						//CEVALDOM BANK ACCOUNT
						Long bcrdBankPK = cashAccountService.getBCBBank(currency,String.valueOf(bankCentralizer.getIdBankPk()));
						String cevaldomAccountNumber = sendingService.getCevaldomBCRDAccountNumber(currency,bcrdBankPK);
						retirementMessage.setDepositaryCentralBankAccount(cevaldomAccountNumber);

						//CEVALDOM DESCRIPTION
						Participant cevaldom = cashAccountService.getDepositaryParticipant();
						retirementMessage.setDepositaryDescription(cevaldom.getDescription());

						//ACCOUNT NUMBER IN COMMERCIAL BANK
						InstitutionBankAccount devolutionCommercialAccount = sendingService.getCommercialBankAccountNumber(currency);
						retirementMessage.setDepositaryCommercialAccountNumber(devolutionCommercialAccount.getAccountNumber());
						//COMMERCIAL BANK DESCRIPTION
						retirementMessage.setCommercialBankDescription(devolutionCommercialAccount.getProviderBank().getDescription());

						//FUNDS OPERATION TYPE DESCRIPTION
						FundsOperationType fundOpeType = sendingService.find(FundsOperationType.class, fundOperationType);
						retirementMessage.setFundOperationTypeDescription(fundOpeType.getName());

						//RECEPTOR BIC CODE
						retirementMessage.setReceptorBicCode(devolutionCommercialAccount.getProviderBank().getBicCode());

					}else if(com.pradera.model.component.type.ParameterFundsOperationType.WITHDRAWL_FUND_BENEFITS.getCode().equals(fundOperationType)){
						//MESSAGE TYPE
						retirementMessage.setMessageType(SwiftMessageType.MESSAGE_103.getDescription());
						//BIC CODE
						retirementMessage.setReceptorBicCode(fundOperation.getBank().getBicCode());
						//TRN
						String trn = receptionService.getTrn(fundOperationType,currency);
						retirementMessage.setTrn(trn);
						//FUND OPERATION PK
						retirementMessage.setIdFundOperation(fundOperation.getIdFundsOperationPk());
						//REFERENCE PAYMENT
						retirementMessage.setSettlementDate(fundOperation.getRegistryDate());
						retirementMessage.setCurrency(getCurrencyParameter(fundOperation.getCurrency()));
						retirementMessage.setAmount(fundOperation.getOperationAmount());
						//CEVALDOM BANK ACCOUNT
						Long bcrdBankPK = cashAccountService.getBCBBank(currency,String.valueOf(bankCentralizer.getIdBankPk()));
						String cevaldomAccountNumber = sendingService.getCevaldomBCRDAccountNumber(currency,bcrdBankPK);
						retirementMessage.setDepositaryCentralBankAccount(cevaldomAccountNumber);
						//CEVALDOM DESCRIPTION
						Participant cevaldom = cashAccountService.getDepositaryParticipant();
						retirementMessage.setDepositaryDescription(cevaldom.getDescription());
						//BANK BENEFICIARY DATA
						retirementMessage.setParticipantSettlerCentralAccountNumber(fundOperation.getBankAccountNumber());
						retirementMessage.setParticipantSettlerDescription(fundOperation.getBank().getDescription());
						//FUND OPERATION TYPE DESCRIPTION
						FundsOperationType fundOpeType = sendingService.find(FundsOperationType.class, fundOperationType);
						retirementMessage.setFundOperationTypeDescription(fundOpeType.getName());

						retirementMessage.setPaymentReference(retirementMessage.getTrn().concat(".").concat(fundOperation.getFundOperationNumber().toString()));

					}else if(com.pradera.model.component.type.ParameterFundsOperationType.DEVOLUTION_RECEPTIONS_FUNDS_WITHDRAWL_BENEFITS.getCode().equals(fundOperationType) ||
								com.pradera.model.component.type.ParameterFundsOperationType.BENEFIT_WITHDRAWAL_COMMENTED.getCode().equals(fundOperationType)){
						if (Validations.validateIsNotNull(fundOperation.getBank())) {
							fundOperation.getBank().getIdBankPk();
							//RECEPTOR BIC CODE
							retirementMessage.setReceptorBicCode(fundOperation.getBank().getBicCode());
						} else {
							fundOperation.getParticipant().getIdParticipantPk();
							//RECEPTOR BIC CODE
							retirementMessage.setReceptorBicCode(fundOperation.getParticipant().getBicCode());
						}
						//MESSAGE TYPE
						retirementMessage.setMessageType(SwiftMessageType.MESSAGE_202.getDescription());
						//DATE - CURRENCY - AMOUNT
						retirementMessage.setSettlementDate(fundOperation.getRegistryDate());
						retirementMessage.setCurrency(getCurrencyParameter(fundOperation.getCurrency()));
						retirementMessage.setAmount(fundOperation.getOperationAmount());
						//TRN
						String trn = StringUtils.EMPTY;
						retirementMessage.setFundOperationTypeDescription(com.pradera.model.component.type.ParameterFundsOperationType.
																										DEVOLUTION_RECEPTIONS_FUNDS_WITHDRAWL_BENEFITS.getDescription());
						if(MasterTableType.PARAMETER_TABLE_CURRENCY_TYPE_DOLARES.getCode().equals(currency)){
							trn = FundsType.TRN_E000342DEV.getStrCode();
						}else{
							trn = FundsType.TRN_E000341DEV.getStrCode();
						}						
						retirementMessage.setTrn(trn);
						//ORIGINAL PAYMENT REFERENCE
						retirementMessage.setOriginalFundOperation(origenPaymentReference);
						
					}else if(com.pradera.model.component.type.ParameterFundsOperationType.BENEFIT_SUPPLY_WITHDRAWL.getCode().equals(fundOperationType)){
						//MESSAGE TYPE
						retirementMessage.setMessageType(SwiftMessageType.MESSAGE_103.getDescription());
						//BIC CODE
						retirementMessage.setReceptorBicCode(fundOperation.getBank().getBicCode());
						//TRN
						String trn = receptionService.getTrn(fundOperationType,currency);
						retirementMessage.setTrn(trn);
						//FUND OPERATION PK
						retirementMessage.setIdFundOperation(fundOperation.getIdFundsOperationPk());
						//REFERENCE PAYMENT
						retirementMessage.setSettlementDate(fundOperation.getRegistryDate());
						retirementMessage.setCurrency(getCurrencyParameter(fundOperation.getCurrency()));
						retirementMessage.setAmount(fundOperation.getOperationAmount());

						//CEVALDOM BANK ACCOUNT
						Long bcrdBankPK = cashAccountService.getBCBBank(currency,String.valueOf(bankCentralizer.getIdBankPk()));
						String cevaldomAccountNumber = sendingService.getCevaldomBCRDAccountNumber(currency,bcrdBankPK);
						retirementMessage.setDepositaryCentralBankAccount(cevaldomAccountNumber);
						//CEVALDOM DESCRIPTION
						Participant cevaldom = cashAccountService.getDepositaryParticipant();
						retirementMessage.setDepositaryDescription(cevaldom.getDescription());

						//BANK BENEFICIARY DATA
						retirementMessage.setParticipantSettlerCentralAccountNumber(fundOperation.getBankAccountNumber());
						retirementMessage.setParticipantSettlerDescription(fundOperation.getBank().getDescription());
						//FUND OPERATION TYPE DESCRIPTION
						FundsOperationType fundOpeType = sendingService.find(FundsOperationType.class, fundOperationType);
						retirementMessage.setFundOperationTypeDescription(fundOpeType.getName());
					}
	
				}else if(FundsOperationGroupType.RETURN_FUND_OPERATION.getCode().equals(fundOperationGroup)){
					//MESSAGE TYPE
					retirementMessage.setMessageType(SwiftMessageType.MESSAGE_103.getDescription());
					//BIC CODE
					String beneficiaryBicCode = StringUtils.EMPTY;
					if(com.pradera.model.component.type.ParameterFundsOperationType.RETURN_BENEFITS_SUPPLY_WITHDRAWL.getCode().equals(fundOperationType) ||
						com.pradera.model.component.type.ParameterFundsOperationType.REFUNDS_WITHDRAWAL_COMMENTED.getCode().equals(fundOperationType)){
						Object[] objReturn = sendingService.getCommercialBankAccountNumberReturn(currency);
						beneficiaryBicCode = objReturn[2].toString();
						//BANK ACCOUNT NUMBER
						retirementMessage.setDepositaryCommercialAccountNumber(fundOperation.getBankAccountNumber());
						//COMMERCIAL BANK DESCRIPTION
						retirementMessage.setCommercialBankDescription(objReturn[1].toString());
					}else if(com.pradera.model.component.type.ParameterFundsOperationType.HOLDER_RETURN_BENEFITS_WITHDRAWL_PAYMENT.getCode().equals(fundOperationType) || 
							 com.pradera.model.component.type.ParameterFundsOperationType.BLOCK_HOLDER_WITHDRAWL_PAYMENT.getCode().equals(fundOperationType) || 
							 com.pradera.model.component.type.ParameterFundsOperationType.BAN_BENEFITS_WITHDRAWL_PAYMENT.getCode().equals(fundOperationType)){
	
						Bank beneficiaryBank = fundOperation.getBank();
						beneficiaryBicCode = beneficiaryBank.getBicCode();
						//BANK ACCOUNT NUMBER
						retirementMessage.setDepositaryCommercialAccountNumber(fundOperation.getBankAccountNumber());
					}
					retirementMessage.setReceptorBicCode(beneficiaryBicCode);
					//TRN
					String trn = receptionService.getTrn(fundOperationType,currency);
					retirementMessage.setTrn(trn);
					//FUND OPERATION PK
					retirementMessage.setOperationID(fundOperation.getIdFundsOperationPk());
					//REFERENCE PAYMENT
					retirementMessage.setSettlementDate(fundOperation.getRegistryDate());
					retirementMessage.setCurrency(getCurrencyParameter(fundOperation.getCurrency()));
					retirementMessage.setAmount(fundOperation.getOperationAmount());
					//CEVALDOM BANK ACCOUNT
					Long bcrdBankPK = cashAccountService.getBCBBank(currency,String.valueOf(bankCentralizer.getIdBankPk()));
					String cevaldomAccountNumber = sendingService.getCevaldomBCRDAccountNumber(currency,bcrdBankPK);
					retirementMessage.setDepositaryCentralBankAccount(cevaldomAccountNumber);
					//CEVALDOM DESCRIPTION
					Participant cevaldom = cashAccountService.getDepositaryParticipant();
					retirementMessage.setDepositaryDescription(cevaldom.getDescription());
				}else if(FundsOperationGroupType.SPECIAL_OPERATION.getCode().equals(fundOperationGroup)){
					//TRN
					retirementMessage.setTrn(fundOperation.getPaymentReference());
					//MESSAGE TYPE
					retirementMessage.setMessageType(SwiftMessageType.MESSAGE_202.getDescription());
					//FUND OPERATION ID
					retirementMessage.setIdFundOperation(fundOperation.getIdFundsOperationPk());
					//BENEFICIARY BANK BIC CODE
					String bankBic = fundOperation.getBank().getBicCode();
					retirementMessage.setReceptorBicCode(bankBic);
					retirementMessage.setBicCodeBeneficiaryBank(bankBic);
					//FUND OPERATION PK
					retirementMessage.setOperationID(fundOperation.getIdFundsOperationPk());
					//REFERENCE PAYMENT
					retirementMessage.setSettlementDate(fundOperation.getRegistryDate());
					retirementMessage.setCurrency(getCurrencyParameter(fundOperation.getCurrency()));
					retirementMessage.setAmount(fundOperation.getOperationAmount());
					
					
				}
			}else{
				//PARAMETERS FOR AUTOMATIC DEVOLUTION
				//MESSAGE TYPE
				retirementMessage.setMessageType(SwiftMessageType.MESSAGE_202.getDescription());
				//DATE - CURRENCY - AMOUNT
				retirementMessage.setSettlementDate(fundOperation.getRegistryDate());
				retirementMessage.setCurrency(getCurrencyParameter(fundOperation.getCurrency()));
				retirementMessage.setAmount(fundOperation.getOperationAmount());
				retirementMessage.setPaymentReference(fundOperation.getPaymentReference());
				//TRN
				String trn = StringUtils.EMPTY;
				if(com.pradera.model.component.type.ParameterFundsOperationType.DEVOLUTION_RECEPTIONS_FUNDS_WITHDRAWL_SETTLEMENT.getCode().equals(fundOperation.getFundsOperationType())){
					retirementMessage.setFundOperationTypeDescription(com.pradera.model.component.type.ParameterFundsOperationType.DEVOLUTION_RECEPTIONS_FUNDS_WITHDRAWL_SETTLEMENT.getDescription());
					if(MasterTableType.PARAMETER_TABLE_CURRENCY_TYPE_DOLARES.getCode().equals(currency)){
						trn = FundsType.TRN_E000340.getStrCode();
					}else{
						trn = FundsType.TRN_E000149.getStrCode();
					}
					
				}else if(com.pradera.model.component.type.ParameterFundsOperationType.DEVOLUTION_RECEPTIONS_FUNDS_WITHDRAWL_BENEFITS.getCode().equals(fundOperation.getFundsOperationType())){
					retirementMessage.setFundOperationTypeDescription(com.pradera.model.component.type.ParameterFundsOperationType.DEVOLUTION_RECEPTIONS_FUNDS_WITHDRAWL_BENEFITS.getDescription());
					if(MasterTableType.PARAMETER_TABLE_CURRENCY_TYPE_DOLARES.getCode().equals(currency)){
						trn = FundsType.TRN_E000342DEV.getStrCode();
					}else{
						trn = FundsType.TRN_E000341DEV.getStrCode();
					}
				}
				retirementMessage.setTrn(trn);
				//RECEPTOR BIC CODE
				retirementMessage.setReceptorBicCode(bicBeneficiary);
				//ORIGINAL PAYMENT REFERENCE
				retirementMessage.setOriginalFundOperation(origenPaymentReference);
				//CORRELATIVE
				Long correlative = receptionService.getFundOperationWithdrawlCorrelative();
				retirementMessage.setCorrelative(correlative.toString());
				
			}
			//ID FUNDS OPERATION
			retirementMessage.setIdFundOperation(fundOperation.getIdFundsOperationPk());
			//EXECUTE SWIFT WITHDRAWL PROCESS
			retirementSwift(retirementMessage);
		} catch (SerialException e) {
			e.printStackTrace();
			processReturn = false;
		} catch (IOException e) {
			e.printStackTrace();
			processReturn = false;
		} catch (SQLException e) {
			e.printStackTrace();
			processReturn = false;
		}

		return processReturn;
	}

	/**
	 * ACCORDING TO THE CURRENCY MAPPED IN THE SWIFT FILE, WE GET THE CURRENCY PARAMETER
	 * @param currencyStr
	 * @return
	 */
	private String getCurrencyParameter(Integer currency){
	  String currencyStr = null;
	  if(MasterTableType.PARAMETER_TABLE_CURRENCY_TYPE_DOLARES.getCode().equals(currency)){
	  currencyStr = FundsType.DESCRIPTION_SHORT_CURRENCY_DOLLAR.getStrCode();
	  }else if(MasterTableType.PARAMETER_TABLE_CURRENCY_TYPE_PESOS.getCode().equals(currency)){
	  currencyStr = FundsType.DESCRIPTION_SHORT_CURRENCY_LOCAL.getStrCode();
	  }
	  return currencyStr;
	}

	/**
	 * METHOD THAT WILL GENERATE THE RETIREMENT SWIFT FILE WITH ALL OPERATIONS ARE RECEIPT
	 * @param retirements
	 * @throws IOException 
	 * @throws SQLException 
	 * @throws SerialException 
	 * @throws ServiceException 
	 */
	public synchronized void retirementSwift(SwiftMessagesRetirement retirement) throws IOException, SerialException, SQLException, ServiceException{
//		IConversionService conversionService = new ConversionService();
		String message = StringUtils.EMPTY;

//		SwiftMessage swiftMessage = generateSwiftMessage(retirement);
//		message = message + conversionService.getFIN(swiftMessage)+SwiftConstants.END_LINE;

		//METHOD TO PROCESS SWIFT WITHDRAWL ACCORDING TO SOME SPECIFICATIONS
		message = processWithdrawlSwiftFile(message);

		//PROCESS INFORMATION TO GENERATE THE FILE NAME
		Date dtToday = new Date();
		SimpleDateFormat daySecond = new SimpleDateFormat("ssSSS");
		SimpleDateFormat formatHour = new SimpleDateFormat("kkmm");
		SimpleDateFormat dayFormat = new SimpleDateFormat("yyMMdd");

		String hourMin = formatHour.format(dtToday);
		String dateReceipt = dayFormat.format(dtToday);
		String secondReceipt = daySecond.format(dtToday);
		String fileName = retirement.getTrn() + dateReceipt + hourMin + secondReceipt;

		//WE SAVE THE OUTPUT SWIFT MESSAGE
		createMessage(message, fileName,retirement.getMessageType(),retirement.getIdFundOperation());

		//WE GET THE SWIFT MESSAGE AND UPLOAD IT TO THE SERVER
		//uploadFileFTP(message,fileName);
	}

	/**
	 * METHOD TO UPLOAD THE SWIFT MESSAGE TO THE FTP SERVER
	 * @param parameters
	 * @param message
	 * @throws FileNotFoundException
	 */
	private synchronized void uploadFileFTP(String message,String fileName) throws FileNotFoundException, IOException, ServiceException{
		//FIRST WE MUST DOWNLOAD A FILE IN A TEMPORAL DIR
		downloadRetirementFile(message,fileName);
		//AFTER DOWNLOADING TO LOCAL DIR, WE MUST UPLOAD TO FTP SERVER
		uploadDownloadedFiles(fileName);
	}

	/**
	 * METHOD WHICH WILL DOWNLOAD RETIREMENT FUND FILE TO A TEMPORAL DIR
	 * @param message
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	private synchronized void downloadRetirementFile(String message, String fileName) throws FileNotFoundException, IOException{

		String curDir = System.getProperty("user.dir");
		File file = new File(curDir+SwiftConstants.TEMPORAL_RETIREMENT_DIR);
		File fileDownload = new File(curDir+SwiftConstants.TEMPORAL_RETIREMENT_DIR+"/"+fileName);

		if(file.exists()){
			//IF IT EXISTS WE CREATE THE FILE
			if(fileDownload.exists() || fileDownload.createNewFile()){
				if(fileDownload.canWrite()){
					OutputStream os = new FileOutputStream(fileDownload);
					byte[] data = message.getBytes();
					int bit = 256;
					for (int i = 0; i < data.length; i++) {
						bit = data[i];
						os.write(bit);
					}
					os.flush();
					os.close();
				}
			}
		}else{
			//IF IT DOESNT EXIST, WE MUST CREATE IT
			if(file.mkdir()){
				if(!fileDownload.exists() && fileDownload.createNewFile()){
					if(fileDownload.canWrite()){
						OutputStream os = new FileOutputStream(fileDownload);
						byte[] data = message.getBytes();
						int bit = 256;
						for (int i = 0; i < data.length; i++) {
							bit = data[i];
							os.write(bit);
						}
						os.flush();
						os.close();
					}
				}
			}
		}

	}

	/**
	 * METHOD WHICH WILL UPLOAD FILES FROM TEMPORAL DIR
	 * @param parameters
	 * @throws SocketException
	 * @throws IOException
	 * @throws ServiceException 
	 */
	@SuppressWarnings("resource")
	private synchronized void uploadDownloadedFiles(String fileName) throws SocketException, IOException, ServiceException{
//		FTPClient client = new FTPClient();
		String curDir = System.getProperty("user.dir");//ACTUAL ROUTE
		File directory = new File(curDir + SwiftConstants.TEMPORAL_RETIREMENT_DIR);
		File[] files = directory.listFiles();

		if(files != null && files.length > 0){
			//WE GET PARAMETERS CONNECTION
			FTPParameters parameters = getParametersConnection();
			//ONCE WE HAVE VERIFIED EXIST FILES TO UPLOAD, WE OPEN CONNECTION
//			client.connect(parameters.getServer(),21);
//			if(client.login(parameters.getUser(), parameters.getPass())){
//				int reply = client.getReplyCode();
//				if (FTPReply.isPositiveCompletion(reply)){
//					for (int i=0; i< files.length; i++) {
//						File file = files[i];
//						if(file.isFile()){
//							InputStream is = new FileInputStream(file);
//							if(client.changeWorkingDirectory(SwiftConstants.TEMPORAL_RETIREMENT_DIR)){
//								if(client.storeFile(fileName, is)){
//									log.info("El archivo: " + file.getName() + " ha sido grabado satisfactoriamente " +
//											"a la carpeta temporal del FTP server");
//									is.close();
//								}else{
//									throw new ServiceException(	ErrorServiceType.SWIFT_PROCESS_FTP_UPLOAD_TEMPORAL_DIR_PROBLEM,
//											"No se pudo guardar el archivo: " + file.getName() + " a la carpeta temporal del FTP server");
//								}
//							}else{
//								if(client.makeDirectory(SwiftConstants.TEMPORAL_RETIREMENT_DIR)){
//									if(client.changeWorkingDirectory(SwiftConstants.TEMPORAL_RETIREMENT_DIR)){
//										if(client.storeFile(fileName, is)){
//											log.info("El archivo: " + file.getName() + " ha sido grabado satisfactoriamente " +
//													"a la carpeta temporal del FTP server");
//											is.close();
//										}else{
//											throw new ServiceException(	ErrorServiceType.SWIFT_PROCESS_FTP_UPLOAD_TEMPORAL_DIR_PROBLEM,
//													"No se pudo guardar el archivo: " + file.getName() + " a la carpeta temporal del FTP server");
//										}
//									}
//								}
//							}
//							is.close();
//							if(file.delete()){
//								log.info("Archivo: " + file.getName() + " fue eliminado de la carpeta temporal luego de ser subido al FTP");
//							}
//						}
//					}
//				}
//				client.logout();
//			}else{
//				throw new ServiceException(	ErrorServiceType.SWIFT_PROCESS_FTP_CONNECTION_PROBLEM,
//											"No se pudo conectar al servidor FTP para los parámetros configurados" );
//			}
//			client.disconnect();
		}
	}

	/**
	 * METHOD TO GENERATE SWIFT FILE
	 * @param retirementOperation
	 * @return
	 */
	private SwiftMessage generateSwiftMessage(SwiftMessagesRetirement retirementOperation){

		//APPLY WIFE.JAR(API FOR SWIFT)
		SwiftMessage swiftMessage = new SwiftMessage();
		Integer operationGroup = retirementOperation.getGroupOperationFund();

		//FILL SWIFT MESSAGE FIRST BLOCK
		swiftMessage = generateFirstBlockSwiftMessage(swiftMessage, retirementOperation);
		//FILL SWIFT MESSAGE SECOND BLOCK
		swiftMessage = generateSecondBlockSwiftMessage(swiftMessage, retirementOperation);

		//FILL SWIFT MESSAGE THIRD BLOCK
		swiftMessage = generateThirdBlockSwiftMessage(swiftMessage);

		//FILL SWIFT MESSAGE FOURTH BLOCK ACCORDING TO THE GROUP OPERATION
		if(FundsOperationGroupType.SETTELEMENT_FUND_OPERATION.getCode().equals(operationGroup)){
			swiftMessage = generateFourthBlockSettlementMessage(swiftMessage, retirementOperation);
		}else if(FundsOperationGroupType.BENEFIT_FUND_OPERATION.getCode().equals(operationGroup)){
			swiftMessage = generateFourthBlockBenefitMessage(swiftMessage, retirementOperation);
		}else if(FundsOperationGroupType.RETURN_FUND_OPERATION.getCode().equals(operationGroup)){
			swiftMessage = generateFourthBlockReturnMessage(swiftMessage, retirementOperation);
		}else if(FundsOperationGroupType.SPECIAL_OPERATION.getCode().equals(operationGroup)){
			swiftMessage = generateFourthBlockSpecialMessage(swiftMessage, retirementOperation);
		}else{
			swiftMessage = generateFourthBlockDepositDevolutionsMessage(swiftMessage, retirementOperation);
		}

		return swiftMessage;
	}

	/**
	 * METHOD WHICH WILL GENERATE OUTPUT FILE FOR FIRST BLOCK BASED ON SWIFT CODE OF SENDER ENTITY
	 * @param retirementOperation
	 * @return
	 */
	private SwiftMessage generateFirstBlockSwiftMessage(SwiftMessage swiftMessage, SwiftMessagesRetirement retirementOperation){

//		SwiftBlock1 blockOne = new SwiftBlock1();
//		blockOne.setApplicationId(SwiftConstants.DEFAULT_APPLICATION_IDENTIFIER);
//		blockOne.setServiceId(SwiftConstants.DEFAULT_SERVICE_IDENTIFIER);
//		blockOne.setLogicalTerminal(retirementOperation.getCevaldomBicCode());
//		blockOne.setSessionNumber(SwiftConstants.DEFAULT_SESSION_NUMBER);
//		blockOne.setSequenceNumber(SwiftConstants.DEFAULT_SECUENCE_NUMBER);
//		swiftMessage.setBlock1(blockOne);

		return swiftMessage;
	}

	/**
	 * METHOD WHICH WILL GENERATE OUTPUT FILE FOR SECOND BLOCK BASED ON SWIFT CODE OF RECEIPTER ENTITY
	 * @param swiftMessage
	 * @param retirementOperation
	 * @return
	 */
	private SwiftMessage generateSecondBlockSwiftMessage(SwiftMessage swiftMessage, SwiftMessagesRetirement retirementOperation){
		String bicCodeDestiny= StringUtils.EMPTY;
		//WE MAP MESSAGE TYPE ACCORDING TO GROUP OPERATION FUND-OPERATION TYPE CURRENCY
		String messageType =retirementOperation.getMessageType();

		boolean isSwiftDestinyAccount= validateSwiftCode(retirementOperation.getReceptorBicCode());

		if(!isSwiftDestinyAccount){
			//IF DESTINY BCRD ACCOUNT IS NOT A SWIFT ONE, BIC CODE MUST BE THE BCRD ONE
			bicCodeDestiny = retirementOperation.getBcrdBicCode();
		}else{
			bicCodeDestiny = withdrawlBicBCRD;
		}
		Date dtToday = new Date();
		SimpleDateFormat formatHour = new SimpleDateFormat("kkmm");
		SimpleDateFormat dayFormat = new SimpleDateFormat("yyMMdd");
		String hourMin = formatHour.format(dtToday);
		String dateReceipt = dayFormat.format(dtToday);
		String mir = dateReceipt + bicCodeDestiny + "0000000000";

		//FILL INFORMATION ABOUT BLOCK 2
		String message = SwiftConstants.BLOCK_TWO_IDENTIFIER+
				SwiftConstants.DOUBLE_POINT+
				SwiftConstants.SWIFT_SYSTEM_OPERATION_TYPE_OUTPUT+
				messageType+
				hourMin+
				mir+
				dateReceipt+
				hourMin+
				SwiftConstants.DEFAULT_MESSAGE_PRIORITY;
//		SwiftBlock2 blockTwo = new SwiftBlock2Output(message);
//		swiftMessage.setBlock2(blockTwo);

		return swiftMessage;
	}

	/**
	 * METHOD WHICH WILL GENERATE OUTPUT FILE FOR THIRD BLOCK BASED ON SWIFT CODE(DEFAULT VALUES)
	 * @param swiftMessage
	 * @return
	 */
	private SwiftMessage generateThirdBlockSwiftMessage(SwiftMessage swiftMessage){

		//FILL INFORMATION ABOUT BLOCK 3
//		swiftMessage.setBlock3(new SwiftBlock3());
//		swiftMessage.getBlock3().addTag(new Tag(SwiftMessageType.MESSAGE_103.getDescription()+SwiftConstants.DOUBLE_POINT+SwiftConstants.DEFAULT_FIN_IDENTIFIER));
		return swiftMessage;
	}


	/**
	 * METHOD WHICH WILL GENERATE OUTPUT FILE OF BENEFITS FOR FOURTH BLOCK BASED ON SWIFT CODE(IT DEPENDS OF OPERATION TYPE)
	 * @param swiftMessage
	 * @param retirementOperation
	 * @return
	 */
	private SwiftMessage generateFourthBlockBenefitMessage(SwiftMessage swiftMessage, SwiftMessagesRetirement retirementOperation){

		Long operationType = retirementOperation.getOperationType();
//		swiftMessage.setBlock4(new SwiftBlock4());
		String paymentDate = StringUtils.EMPTY;
		String paymentReference = retirementOperation.getTrn();
		SimpleDateFormat formato = new SimpleDateFormat(SwiftConstants.FORMAT_DATE);
		paymentDate = formato.format(retirementOperation.getSettlementDate());
		//String formatAmount = giveFormatAmount(retirementOperation.getAmount());
		String formatAmount = retirementOperation.getAmount().toString();

		//FILL INFORMATION ABOUT BLOCK 4 FOR BENEFIT PAYMENTS BY OPERATION TYPE
		//THIS IS THE PAYMENT OF BENEFITS FOR HOLDERS
		if(	com.pradera.model.component.type.ParameterFundsOperationType.HOLDER_INTERESTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(operationType) || 
			com.pradera.model.component.type.ParameterFundsOperationType.HOLDER_CAPITAL_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(operationType) || 
			com.pradera.model.component.type.ParameterFundsOperationType.HOLDER_DIVIDENDS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(operationType) || 
			com.pradera.model.component.type.ParameterFundsOperationType.HOLDER_REMANENTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(operationType) || 
			com.pradera.model.component.type.ParameterFundsOperationType.HOLDER_CONTRIBUTION_RETURN_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(operationType)){

//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_20,paymentReference));
//
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_21,SwiftConstants.DEFAULT_TRN_REFERENCE_NOREF));
//
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_32A , paymentDate + retirementOperation.getCurrency() + formatAmount));
//
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_58A, retirementOperation.getReceptorBicCode()));
//
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_72, SwiftConstants.FIELD_VALUE_REC+
//																		retirementOperation.getCorporativeProcessDescription()));
		//THIS CASE IS WHEN PARTICIPANT'S HOLDER ARE PART OF THE BENEFIT PAYMENT
		}else if(com.pradera.model.component.type.ParameterFundsOperationType.INSTITUTIONS_INTERESTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(operationType) || 
				com.pradera.model.component.type.ParameterFundsOperationType.INSTITUTIONS_CAPITAL_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(operationType) || 
				com.pradera.model.component.type.ParameterFundsOperationType.INSTITUTIONS_DIVIDENDS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(operationType) || 
				com.pradera.model.component.type.ParameterFundsOperationType.INSTITUTIONS_REMANENTS_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(operationType) || 
				com.pradera.model.component.type.ParameterFundsOperationType.INSTITUTIONS_CONTRIBUTION_RETURN_PAYMENTS_WITHDRAWL_CONSIGNMENT.getCode().equals(operationType)){

//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_20,paymentReference));
//
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_23B, SwiftConstants.FIELD_VALUE_CRED));
//
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_32A , paymentDate + retirementOperation.getCurrency() + formatAmount));
//
//			//FIELD 50K, CEVALDOM ACCOUNT IN BCRD WHERE MONEY WILL BE TAKEN FOR PAYMENT
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_50K, 
//													retirementOperation.getDepositaryCentralBankAccount()+
//													SwiftConstants.END_LINE+
//													retirementOperation.getDepositaryDescription()));

			boolean isSwiftCode = validateSwiftCode(retirementOperation.getReceptorBicCode());
			if(!isSwiftCode){
//				swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_54A,retirementOperation.getReceptorBicCode()));
			}

			//FIELD 59 IS PARTICIPANT CENTRAL ACCOUNT NUMBER, LIKE THE DESTINY ACCOUNT
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_59,
//										retirementOperation.getParticipantSettlerCentralAccountNumber()+
//										SwiftConstants.END_LINE+
//										retirementOperation.getParticipantSettlerDescription()));
//
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_71A,SwiftConstants.FIELD_VALUE_OUR));
//
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_72, SwiftConstants.FIELD_VALUE_REC+
//					retirementOperation.getCorporativeProcessDescription()));

		//IT IS WHEN CEVALDOM RETURN CASH BECAUSE ANY REASON, SI IT GOES TO A COMMERCIAL BANK ACCOUNT
		}else if(com.pradera.model.component.type.ParameterFundsOperationType.CUSTODY_COMISSION_WITHDRAWL_BENEFIT_PAYMENTS.getCode().equals(operationType) ||
				com.pradera.model.component.type.ParameterFundsOperationType.ISSUER_COMISSION_WITHDRAWL_BENEFIT_PAYMENTS.getCode().equals(operationType) || 
				com.pradera.model.component.type.ParameterFundsOperationType.BLOCK_BENEFITS_WITHDRAWL_RETENTION.getCode().equals(operationType) || 
				com.pradera.model.component.type.ParameterFundsOperationType.TAX_BENEFITS_WITHDRAWL_RETENTION.getCode().equals(operationType)){

//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_20,paymentReference));
//
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_23B, SwiftConstants.FIELD_VALUE_CRED));
//
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_32A , paymentDate + retirementOperation.getCurrency() + formatAmount));
//
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_50K, 
//													retirementOperation.getDepositaryCentralBankAccount()+
//													SwiftConstants.END_LINE+
//													retirementOperation.getDepositaryDescription()));

			//FIELD 59 IS CEVALDOM COMERCIAL BANK ACCOUNT NUMBER FOR THIS OPERATION TYPE
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_59,
//													retirementOperation.getDepositaryCommercialAccountNumber()+
//													SwiftConstants.END_LINE+
//													retirementOperation.getDepositaryDescription()+
//													SwiftConstants.END_LINE+
//													retirementOperation.getCommercialBankDescription()));
//
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_71A, SwiftConstants.FIELD_VALUE_OUR));
//
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_72, SwiftConstants.FIELD_VALUE_REC+
//					retirementOperation.getFundOperationTypeDescription().substring(0,30)));

		}else if(com.pradera.model.component.type.ParameterFundsOperationType.WITHDRAWL_FUND_BENEFITS.getCode().equals(operationType) ||
					com.pradera.model.component.type.ParameterFundsOperationType.BENEFIT_WITHDRAWAL_COMMENTED.getCode().equals(operationType)){

//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_20,paymentReference));
//
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_23B, SwiftConstants.FIELD_VALUE_CRED));
//
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_32A , paymentDate + retirementOperation.getCurrency() + formatAmount));
//
//			//FIELD 50K, CEVALDOM ACCOUNT IN BCRD WHERE MONEY WILL BE TAKEN FOR PAYMENT
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_50K, 
//													retirementOperation.getDepositaryCentralBankAccount()+
//													SwiftConstants.END_LINE+
//													retirementOperation.getDepositaryDescription()));

			boolean isSwiftCode = validateSwiftCode(retirementOperation.getReceptorBicCode());
			if(!isSwiftCode){
//				swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_54A,retirementOperation.getReceptorBicCode()));
			}

			//FIELD 59 IS PARTICIPANT CENTRAL ACCOUNT NUMBER, LIKE THE DESTINY ACCOUNT
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_59,
//										retirementOperation.getParticipantSettlerCentralAccountNumber()+
//										SwiftConstants.END_LINE+
//										retirementOperation.getParticipantSettlerDescription()));
//
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_71A,SwiftConstants.FIELD_VALUE_OUR));
//
//			if(retirementOperation.getFundOperationTypeDescription().length() > 30){
//				swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_72, SwiftConstants.FIELD_VALUE_REC+
//						retirementOperation.getFundOperationTypeDescription().substring(0,30)));
//			}else{
//				swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_72, SwiftConstants.FIELD_VALUE_REC+retirementOperation.getFundOperationTypeDescription()));
//			}

		}else if(com.pradera.model.component.type.ParameterFundsOperationType.BENEFIT_SUPPLY_WITHDRAWL.getCode().equals(operationType)){

//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_20,paymentReference));
//
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_23B, SwiftConstants.FIELD_VALUE_CRED));
//
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_32A , paymentDate + retirementOperation.getCurrency() + formatAmount));
//
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_50K, 
//													retirementOperation.getDepositaryCentralBankAccount()+
//													SwiftConstants.END_LINE+
//													retirementOperation.getDepositaryDescription()));
//
//			//FIELD 59 IS PARTICIPANT CENTRAL ACCOUNT NUMBER, LIKE THE DESTINY ACCOUNT
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_59,
//										retirementOperation.getParticipantSettlerCentralAccountNumber()+
//										SwiftConstants.END_LINE+
//										retirementOperation.getParticipantSettlerDescription()));
//
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_71A,SwiftConstants.FIELD_VALUE_OUR));
//
//			if(retirementOperation.getFundOperationTypeDescription().length() > 30){
//				swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_72, SwiftConstants.FIELD_VALUE_REC+
//						retirementOperation.getFundOperationTypeDescription().substring(0,30)));
//			}else{
//				swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_72, SwiftConstants.FIELD_VALUE_REC+retirementOperation.getFundOperationTypeDescription()));
//			}

		}
		return swiftMessage;
	}

	/**
	 * METHOD WHICH WILL GENERATE OUTPUT FILE OF DEPOSIT DEVOLUTION FOR FOURTH BLOCK BASED ON SWIFT CODE
	 * IT IS GENERATED AUTOMATICALLY IN DEPOSIT VALIDATIOS OR WHEN AN OBSERVED SWIFT MESSAGE STATE IS REJECTED.
	 * @param swiftMessage
	 * @param retirementOperation
	 * @return
	 */
	private SwiftMessage generateFourthBlockDepositDevolutionsMessage(SwiftMessage swiftMessage,SwiftMessagesRetirement retirementOperation){

		String paymentReference = retirementOperation.getTrn() + SwiftConstants.SIMPLE_POINT + retirementOperation.getCorrelative();
		String paymentDate = StringUtils.EMPTY;
		SimpleDateFormat formato = new SimpleDateFormat(SwiftConstants.FORMAT_DATE);
		paymentDate = formato.format(retirementOperation.getSettlementDate());
		//String formatAmount = giveFormatAmount(retirementOperation.getAmount());
		String formatAmount = retirementOperation.getAmount().toString();

//		//FILL INFORMATION ABOUT BLOCK 4 FOR DEPOSIT DEVOLUTIONS
//		swiftMessage.setBlock4(new SwiftBlock4());
//		swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_20,paymentReference));
//
//		//FIELD 21 IS A REFERENTIAL ONE, WHICH INDICATES THE ORIGIN OPERATION THAT WAS REJECTED
//		swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_21,retirementOperation.getOriginalFundOperation()));
//
//		swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_32A , paymentDate + retirementOperation.getCurrency() + formatAmount));
//
//		boolean isSwiftBicCode = validateSwiftCode(retirementOperation.getReceptorBicCode());
//		if(!isSwiftBicCode){
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_54A, retirementOperation.getReceptorBicCode()));
//		}
//
//		swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_58A, retirementOperation.getReceptorBicCode()));
//
//		if(retirementOperation.getFundOperationTypeDescription().length() > 30){
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_72, SwiftConstants.FIELD_VALUE_REC+retirementOperation.getFundOperationTypeDescription().substring(0,30)));
//		}else{
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_72, SwiftConstants.FIELD_VALUE_REC+retirementOperation.getFundOperationTypeDescription()));
//		}

		return swiftMessage;
	}

	private SwiftMessage generateFourthBlockSpecialMessage(SwiftMessage swiftMessage, SwiftMessagesRetirement retirementOperation){
//
//		swiftMessage.setBlock4(new SwiftBlock4());
//		String paymentDate = StringUtils.EMPTY;
//		String paymentReference = retirementOperation.getTrn();
//		SimpleDateFormat formato = new SimpleDateFormat(SwiftConstants.FORMAT_DATE);
//		paymentDate = formato.format(retirementOperation.getSettlementDate());
//		String formatAmount = retirementOperation.getAmount().toString();
//
//		swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_20,paymentReference));
//
//		swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_21,SwiftConstants.DEFAULT_TRN_REFERENCE_NOREF));
//
//		swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_32A , paymentDate + retirementOperation.getCurrency() + formatAmount));
//
//		swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_58A, retirementOperation.getReceptorBicCode()));

		return swiftMessage;
	}

	/**
	 * METHOD WHICH WILL GENERATE OUTPUT FILE OF RETURN FOR FOURTH BLOCK BASED ON SWIFT CODE
	 * @param swiftMessage
	 * @param retirementOperation
	 * @return
	 */
	private SwiftMessage generateFourthBlockReturnMessage(SwiftMessage swiftMessage, SwiftMessagesRetirement retirementOperation){

		String paymentReference = retirementOperation.getTrn() + SwiftConstants.SIMPLE_POINT + retirementOperation.getIdFundOperation().toString();
		String paymentDate = StringUtils.EMPTY;
		SimpleDateFormat formato = new SimpleDateFormat(SwiftConstants.FORMAT_DATE);
		paymentDate = formato.format(retirementOperation.getSettlementDate());
		//String formatAmount = giveFormatAmount(retirementOperation.getAmount());
		String formatAmount = retirementOperation.getAmount().toString();

//		swiftMessage.setBlock4(new SwiftBlock4());
//		swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_20,paymentReference));
//
//		swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_23B, SwiftConstants.FIELD_VALUE_CRED));
//
//		swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_32A , paymentDate + retirementOperation.getCurrency() + formatAmount));
//
//		if(	retirementOperation.getDepositaryCentralBankAccount() != null &&
//			retirementOperation.getDepositaryDescription() != null){
//
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_50K,retirementOperation.getDepositaryCentralBankAccount()+
//					SwiftConstants.END_LINE+retirementOperation.getDepositaryDescription()));
//		}

		boolean isSwiftBicCode = validateSwiftCode(retirementOperation.getReceptorBicCode());
		if(!isSwiftBicCode){
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_54A, retirementOperation.getReceptorBicCode()));
		}

		//IF THIS IS A PAYMENT TO A BENEFICIARY(HOLDER ACCOUNT), THERE MUST ALSO BE THE COMMERCIAL BANK DESCRIPTION
		Long operationType = retirementOperation.getOperationType();
		if(com.pradera.model.component.type.ParameterFundsOperationType.RETURN_BENEFITS_SUPPLY_WITHDRAWL.getCode().equals(operationType)){

//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_59,
//					retirementOperation.getDepositaryCommercialAccountNumber()+
//					SwiftConstants.END_LINE+
//					retirementOperation.getCommercialBankDescription()));

		}else if(com.pradera.model.component.type.ParameterFundsOperationType.HOLDER_RETURN_BENEFITS_WITHDRAWL_PAYMENT.getCode().equals(operationType) || 
				com.pradera.model.component.type.ParameterFundsOperationType.BLOCK_HOLDER_WITHDRAWL_PAYMENT.getCode().equals(operationType) ||
				com.pradera.model.component.type.ParameterFundsOperationType.BAN_BENEFITS_WITHDRAWL_PAYMENT.getCode().equals(operationType)){

//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_59,
//					retirementOperation.getDepositaryCommercialAccountNumber()+
//					SwiftConstants.END_LINE+
//					retirementOperation.getHolderDescription()+
//					SwiftConstants.END_LINE+
//					retirementOperation.getCommercialBankDescription()));
		}

		if(retirementOperation.getFundOperationTypeDescription().length() > 35){
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_70, retirementOperation.getFundOperationTypeDescription().substring(0,35)));
		}else{
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_70, retirementOperation.getFundOperationTypeDescription()));
		}
//
//		swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_71A, SwiftConstants.FIELD_VALUE_OUR));
//
//		if(retirementOperation.getFundOperationTypeDescription().length() > 30){
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_72, SwiftConstants.FIELD_VALUE_REC+
//					retirementOperation.getFundOperationTypeDescription().substring(0,30)));
//		}else{
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_72, SwiftConstants.FIELD_VALUE_REC+retirementOperation.getFundOperationTypeDescription()));
//		}

		return swiftMessage;
	}

	/**
	 * METHOD WHICH WILL GENERATE OUTPUT FILE OF RETURN FOR FOURTH BLOCK BASED ON SWIFT CODE
	 * @param swiftMessage
	 * @param retirementOperation
	 * @return
	 */
	private SwiftMessage generateFourthBlockSettlementMessage(SwiftMessage swiftMessage, SwiftMessagesRetirement retirementOperation){

		String paymentDate = StringUtils.EMPTY;
		SimpleDateFormat formato = new SimpleDateFormat(SwiftConstants.FORMAT_DATE);
		paymentDate = formato.format(retirementOperation.getSettlementDate());
		String formatAmount = retirementOperation.getAmount().toString();
//		swiftMessage.setBlock4(new SwiftBlock4());
//
//		//THE PAYMENT REFERENCE WITHDRAWL SWIFT MESSAGE WILL BE THE SAME THAN THE FUND OPERATION
//		String paymentReference = retirementOperation.getPaymentReference();
//		swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_20,paymentReference));
//
//		swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_23B, SwiftConstants.FIELD_VALUE_CRED));
//		swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_32A , paymentDate + retirementOperation.getCurrency() + formatAmount));
//
//		//ACCOUNT NUMBER IN CENTRAL BANK OF BUYER PARTICIPANT
//		//ONLY WILL BE SETTED FOR GROSS SETTLEMENT OPERATIONS
//		if(Validations.validateIsNotNullAndNotEmpty(retirementOperation.getParticipantBuyerCentralAccountNumber())){
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_50K, 
//					retirementOperation.getParticipantBuyerCentralAccountNumber() + 
//					SwiftConstants.END_LINE + 
//					retirementOperation.getParticipantBuyerDescription()));
//		}

		boolean isSwiftBicCode = validateSwiftCode(retirementOperation.getReceptorBicCode());
		if(!isSwiftBicCode){
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_54A, retirementOperation.getReceptorBicCode()));
		}

//		//ACCOUNT NUMBER IN CENTRAL BANK OF SETTLER PARTICIPANT
//		swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_59,
//												retirementOperation.getParticipantSettlerCentralAccountNumber()+
//												SwiftConstants.END_LINE+
//												retirementOperation.getParticipantSettlerDescription()));
//
//		if(retirementOperation.getFundOperationTypeDescription().length() > 35){
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_70, retirementOperation.getFundOperationTypeDescription().substring(0,35)));
//		}else{
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_70, retirementOperation.getFundOperationTypeDescription()));
//		}
//
//		swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_71A, SwiftConstants.FIELD_VALUE_OUR));
//
//		if(retirementOperation.getFundOperationTypeDescription().length() > 30){
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_72, SwiftConstants.FIELD_VALUE_REC+retirementOperation.getFundOperationTypeDescription().substring(0,30)));
//		}else{
//			swiftMessage.getBlock4().addTag(new Tag(SwiftConstants.FIELD_NAME_72, SwiftConstants.FIELD_VALUE_REC+retirementOperation.getFundOperationTypeDescription()));
//		}

		return swiftMessage;
	}

	/**
	 * METHOD WHICH GIVE THE CORRECT FORMAT TO THE AMOUNT
	 * @param amountBd
	 * @return
	 */
	public String fillZeroValue(String operation, int size){

		for(int i = operation.length(); i <= size; i++){
			operation = "0"+operation;
		}

		return operation;
	}

	/**
	 * METHOD WHICH GIVE THE CORRECT FORMAT TO THE AMOUNT
	 * @param amountBd
	 * @return
	 */
	public String giveFormatAmount(BigDecimal amountBd){
		String amount = StringUtils.EMPTY;
		DecimalFormat decf = new DecimalFormat(SwiftConstants.FORMAT_NUMBER);
		amount = decf.format(amountBd);

		for(int i = amount.length(); i <= 14; i++){
			amount = "0"+amount;
		}

		return amount;
	}

	/**
	 * VALIDATE IS THE BIC CODE SENT IS A SWIFT ACCOUNT OR NOT
	 * @param bicCode
	 * @return
	 */
	public boolean validateSwiftCode(String bicCode){
		boolean isSwiftCode=true;

		Character c = bicCode.charAt(7);
		String swiftIndicator = c.toString();
		if(SwiftConstants.NO_SWIFT_INDICATOR.equals(swiftIndicator)){
			isSwiftCode = false;
		}

		return isSwiftCode;
	}

	/**
	 * METHOD WHICH WILL GET CONNECTION PARAMETERS TO GET SWIFT FILES AND PROCESS THEM
	 * @return
	 */
	public FTPParameters getParametersConnection(){
		FTPParameters parameters = new FTPParameters();
		parameters.setLocalPath(ftpLocalPath);
		parameters.setPass(ftpPass);
		parameters.setRemotePath(ftpRemotePath);
		parameters.setServer(ftpServer);
		parameters.setUser(ftpUser);
		return parameters;
	}
	
	public static void main(String[] args) {
		
		
    }

	/**
	 * METHOD TO CREATE THE MESSAGE FOR RETIREMENT
	 * @param file
	 * @param fileName
	 * @param messageType
	 * @param fundOperation
	 * @throws SerialException
	 * @throws SQLException
	 */
	private void createMessage(String file, String fileName, String messageType, Long fundOperation) throws SerialException, SQLException, ServiceException{
		com.pradera.model.funds.SwiftMessage swiftMessage = new com.pradera.model.funds.SwiftMessage();
		FundsOperation fundOperationOBJ = receptionService.find(fundOperation,FundsOperation.class);
		swiftMessage.setFundsOperation(fundOperationOBJ);
		swiftMessage.setIndInputOutput(FundsType.IND_MESSAGE_TYPE_OUTPUT.getCode());
		byte[] objByte = file.getBytes();
		swiftMessage.setMessage(objByte);
		swiftMessage.setMessageName(fileName);
		swiftMessage.setMessageType(Integer.parseInt(messageType));
		swiftMessage.setState(MasterTableType.PARAMETER_TABLE_MESSAGE_STATE_PROCESSED.getCode());

		receptionService.create(swiftMessage);
	}

	/**
	 * METHOD CREATED TO REPROCESS SWIFT WITHDRAWL FILE
	 * @param message
	 * @return
	 */
	private String processWithdrawlSwiftFile(String message){

		String finalMessage = StringUtils.EMPTY;
		String firstGroupMessage = StringUtils.EMPTY;
		String secondGroupMessage = StringUtils.EMPTY;
		String thirdGroupMessage = StringUtils.EMPTY;
		String fourthGroupMessage = StringUtils.EMPTY;

		int endFirstGroup = message.indexOf("}{2");
		firstGroupMessage = message.substring(0, endFirstGroup+1);

		int endSecondGroup = message.indexOf("}{3");
		secondGroupMessage = message.substring(endFirstGroup+1, endSecondGroup+1);

		int endThirdGroup = message.indexOf("}{4");
		thirdGroupMessage = message.substring(endSecondGroup+1, endThirdGroup+1);

		int endFourthGroup = message.indexOf("-}");
		fourthGroupMessage = message.substring(endThirdGroup+1, endFourthGroup+2);

		//GROUP 1
		//WE MUST ADD LETTER "A" IN THE POSITION 8 IN THE SENDER BIC CODE
		firstGroupMessage = firstGroupMessage.substring(0, 14).concat("A").concat(firstGroupMessage.substring(14,firstGroupMessage.length()));

		//GROUP2
		//WE MUST CHANGE INDEX FROM "O" TO "I" AND DELETE TIME WHEN WAS GENERATED THE SWIFT MESSAGE
		secondGroupMessage = secondGroupMessage.substring(0,3).concat("I").concat(secondGroupMessage.substring(4,7).concat(secondGroupMessage.substring(17,28).concat("N}")));

		//GROUP3
		

		//GROUP4
		String finalFourthGroup = StringUtils.EMPTY;
		int beginField32A = fourthGroupMessage.indexOf(":32A:");
		String restFourthMessage = fourthGroupMessage.substring(beginField32A,fourthGroupMessage.length());
		//WE MODIFY THE COMMA BY DOT
		restFourthMessage = restFourthMessage.replace(".",",");
		finalFourthGroup = fourthGroupMessage.substring(0,beginField32A).concat(restFourthMessage);

		if(finalFourthGroup.contains(":50K:")){
			int indField50K = finalFourthGroup.indexOf(":50K:");
			restFourthMessage = finalFourthGroup.substring(indField50K,finalFourthGroup.length());
			finalFourthGroup = finalFourthGroup.substring(0,indField50K).concat(restFourthMessage.substring(0,5).concat("/").concat(restFourthMessage.substring(5,restFourthMessage.length())));
		}
		if(finalFourthGroup.contains(":59:")){
			int indField59 = finalFourthGroup.indexOf(":59:");
			restFourthMessage = finalFourthGroup.substring(indField59,finalFourthGroup.length());
			finalFourthGroup = finalFourthGroup.substring(0,indField59).concat(restFourthMessage.substring(0,4).concat("/").concat(restFourthMessage.substring(5,restFourthMessage.length())));
		}
		finalMessage = firstGroupMessage.concat(secondGroupMessage.concat(thirdGroupMessage.concat(finalFourthGroup)));

		return finalMessage;
	}
}