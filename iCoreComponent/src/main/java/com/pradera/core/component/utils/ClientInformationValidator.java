package com.pradera.core.component.utils;

import java.io.Serializable;

import javax.ejb.EJB;

import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.helperui.HelperBean;
import com.pradera.model.accounts.type.ParticipantClassType;
import com.pradera.model.accounts.type.PersonType;

@HelperBean
public class ClientInformationValidator implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	public ClientInformationTO validateShowClientInformation(Integer accountClass,Integer personType){
		ClientInformationTO clientInformationTO = new ClientInformationTO();
		
		if(personType!=null && personType.equals(PersonType.NATURAL.getCode())
			&& accountClass!=null && accountClass.equals(ParticipantClassType.AGENCIAS_BOLSA.getCode())){

			            clientInformationTO.setViewMarriedLastName(true);
						clientInformationTO.setViewCivilStatus(true);
						
						clientInformationTO.setViewMainActivity(true);
						clientInformationTO.setViewJobSourceBusinessName(true);
						clientInformationTO.setViewJobDateAdmission(true);
						clientInformationTO.setViewAppointment(true);
						clientInformationTO.setViewJobAddress(true);
						clientInformationTO.setViewOfficePhone(true);
						clientInformationTO.setViewTotalIncome(true);
						
						clientInformationTO.setViewPerRefFullName(true);
						clientInformationTO.setViewPerRefLandLine(true);
						clientInformationTO.setViewPerRefCellPhone(true);
						clientInformationTO.setViewComerRefBusinessName(true);
						clientInformationTO.setViewComerRefLandLine(true);
						
						clientInformationTO.setViewEntityPublicAppointment(true);
						clientInformationTO.setViewNitNaturalPerson(true);
		}
		else if(personType!=null && personType.equals(PersonType.JURIDIC.getCode())
				&& accountClass!=null && accountClass.equals(ParticipantClassType.AGENCIAS_BOLSA.getCode())){
			
			clientInformationTO.setViewFundCompanyEnrollment(true);
			clientInformationTO.setViewConstitutionDate(true);
			clientInformationTO.setViewContactPhone1(true);
			clientInformationTO.setViewContactPhone2(true);
			clientInformationTO.setViewAuthorityLegalNumber(true);
			clientInformationTO.setViewComerRefBusinessName(true);
			clientInformationTO.setViewComerRefLandLine(true);
			clientInformationTO.setViewContactWebPage(true);		
		
		}
			
		return clientInformationTO;
	}
	
	
	
	
	
	
}
