package com.pradera.core.component.helperui.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.helperui.to.HolderAccountSearchTO;
import com.pradera.core.component.helperui.to.HolderHelperOutputTO;
import com.pradera.core.component.helperui.to.HolderSearchTO;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.accounts.to.HolderAccountObjectTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountGroupType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;



@Stateless
public class HolderQueryServiceBean extends CrudDaoServiceBean{
	
	@Inject
	private PraderaLogger log;
	
	@Inject
	@Configurable
	private Integer maxResultsQuery;
	
	@Inject @Configurable
	Integer maxResultsQueryHelper;
	
	public HolderQueryServiceBean(){}
	
	public Holder findHolderByCode(Long idHolderPk) throws ServiceException{
		Holder holder = new Holder();
		try {
			StringBuilder sb = new StringBuilder();
			sb.append(" select ho from Holder ho where ho.idHolderPk="+idHolderPk);
			Query query = em.createQuery(sb.toString());
			holder = (Holder)query.getSingleResult();
		}catch(NoResultException nex) {
			log.error("No results found for RNT code " + idHolderPk);
			holder = null;
		}
		return holder;
	}
	
	public List<Object[]> searchHolderFromHelper(HolderTO filter){
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT DISTINCT" +
				"		RESULTS.ID_HOLDER_PK,		" +
				"		RESULTS.NAME,				" +
				"		RESULTS.FIRST_LAST_NAME,	" +
				"		RESULTS.SECOND_LAST_NAME,	" +
				"		RESULTS.FULL_NAME,			" +
				"		RESULTS.DOCUMENT_TYPE,		" +
				"		RESULTS.DOCUMENT_NUMBER,	" +
				"		RESULTS.HOLDER_TYPE,		" +
				"		RESULTS.STATE_HOLDER,		" +
				"		RESULTS.STATE_NAME			" +
				"	FROM							" +
				"		(							" +
				"		 (SELECT DISTINCT			" +
				  "			HO1.ID_HOLDER_PK,		" +
				  "			HO1.NAME,				" +
				  "			HO1.FIRST_LAST_NAME,	" +
				  "			HO1.SECOND_LAST_NAME,	" +
				  "			HO1.FULL_NAME,			" +
				  "			HO1.DOCUMENT_TYPE,		" +
				  "			HO1.DOCUMENT_NUMBER,	" +
				  "			HO1.HOLDER_TYPE,		" +
				  "			HO1.STATE_HOLDER,		" + 
				  "		    (SELECT PT1.PARAMETER_NAME FROM PARAMETER_TABLE PT1 WHERE PARAMETER_TABLE_PK=HO1.STATE_HOLDER) AS STATE_NAME " +
				  "	FROM " +
				  "			HOLDER_ACCOUNT 		  HA,  " +
				  "			HOLDER_ACCOUNT_DETAIL HAD, " +
				  "			HOLDER			      HO1  " +
				  " WHERE " +
				  "			HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK " +
				  "	AND		HAD.ID_HOLDER_FK = HO1.ID_HOLDER_PK ");
	if(Validations.validateIsNotNullAndNotEmpty(filter.getPersonType())){
		  sb.append(" AND HO1.HOLDER_TYPE= :personType");
	}
	if(Validations.validateIsNotNullAndNotEmpty(filter.getState())){
		  sb.append(" AND HO1.STATE_HOLDER= :state");
	}
	if(Validations.validateIsNotNullAndNotEmpty(filter.getPartIdPk())){
		  sb.append(" AND HA.ID_PARTICIPANT_FK= :idParticipantPk");
	}
	if(Validations.validateIsNotNullAndPositive(filter.getHolderId())){
		  sb.append(" AND HO1.ID_HOLDER_PK= :idHolderPk");
	}
	if(Validations.validateIsNotNullAndPositive(filter.getDocumentType())){
		  sb.append(" AND HO1.DOCUMENT_TYPE= :docType");
	}
	if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentNumber())){
		  sb.append(" AND HO1.DOCUMENT_NUMBER = :docNumber");
	}
	if(Validations.validateIsNotNullAndNotEmpty(filter.getName())){
		  sb.append(" AND HO1.NAME like :holderName ");
	}
	if(Validations.validateIsNotNullAndNotEmpty(filter.getFirstLastName())){
		  sb.append(" AND HO1.FIRST_LAST_NAME like :holderFirstLastName ");
	}
	if(Validations.validateIsNotNullAndNotEmpty(filter.getSecondLastName())){
		  sb.append(" AND HO1.SECOND_LAST_NAME like :holderSecondLastName ");
	}
	if(Validations.validateIsNotNullAndNotEmpty(filter.getFullName())){
		  sb.append(" AND HO1.FULL_NAME like :holderFullName ");
	}
	if(filter.isUserIssuer()){
		sb.append(" AND (EXISTS(SELECT 1 FROM HOLDER_ACCOUNT_BALANCE HAB , HOLDER_ACCOUNT HA,HOLDER_ACCOUNT_DETAIL HAD,SECURITY SE ");
		sb.append("WHERE HAB.ID_HOLDER_ACCOUNT_PK = HA.ID_HOLDER_ACCOUNT_PK ");
		sb.append("and HA.ID_HOLDER_ACCOUNT_PK = HAD.ID_HOLDER_ACCOUNT_FK ");
		sb.append("and HAB.ID_SECURITY_CODE_PK = SE.ID_SECURITY_CODE_PK ");
		sb.append("and SE.ID_ISSUER_FK = :issuer ");
		sb.append("and HAD.ID_HOLDER_FK = HO1.ID_HOLDER_PK ");
		sb.append("and HAB.TOTAL_BALANCE>0)) ");
	}
	if(filter.isUserIssuerDpf()){
		sb.append(" AND (EXISTS(SELECT 1 FROM HOLDER_ACCOUNT_BALANCE HAB , HOLDER_ACCOUNT HA,HOLDER_ACCOUNT_DETAIL HAD,SECURITY SE ");
		sb.append("WHERE HAB.ID_HOLDER_ACCOUNT_PK = HA.ID_HOLDER_ACCOUNT_PK ");
		sb.append("and HA.ID_HOLDER_ACCOUNT_PK = HAD.ID_HOLDER_ACCOUNT_FK ");
		sb.append("and HAB.ID_SECURITY_CODE_PK = SE.ID_SECURITY_CODE_PK ");
		sb.append("and SE.ID_ISSUER_FK = :issuer ");
		sb.append("and SE.SECURITY_CLASS in (:lstSecurityClass) ");
		sb.append("and HAD.ID_HOLDER_FK = HO1.ID_HOLDER_PK ");
		sb.append("and HAB.TOTAL_BALANCE>0)) ");
	}
		sb.append(" ) ");
		sb.append(" UNION ALL ");
		sb.append(" (SELECT DISTINCT				" +
				  "			HO2.ID_HOLDER_PK,		" +
				  "			HO2.NAME,				" +
				  "			HO2.FIRST_LAST_NAME,	" +
				  "			HO2.SECOND_LAST_NAME,	" +
				  "			HO2.FULL_NAME,			" +
				  "			HO2.DOCUMENT_TYPE,		" +
				  "			HO2.DOCUMENT_NUMBER,	" +
				  "			HO2.HOLDER_TYPE,		" +
				  "			HO2.STATE_HOLDER,		" +
				  "		    (SELECT PT2.PARAMETER_NAME FROM PARAMETER_TABLE PT2 WHERE PARAMETER_TABLE_PK=HO2.STATE_HOLDER) AS STATE_NAME " +
				  "	FROM " +
				  "			HOLDER HO2" +
				  "	WHERE  1=1");
	if(Validations.validateIsNotNullAndNotEmpty(filter.getPersonType())){
		  sb.append(" AND HO2.HOLDER_TYPE= :personType");
	}
	if(Validations.validateIsNotNullAndNotEmpty(filter.getState())){
		  sb.append(" AND HO2.STATE_HOLDER= :state");
	}
	if(Validations.validateIsNotNullAndNotEmpty(filter.getPartIdPk())){
		  sb.append(" AND HO2.ID_PARTICIPANT_FK= :idParticipantPk");
	}
	if(Validations.validateIsNotNullAndNotEmpty(filter.getHolderId())){
		  sb.append(" AND HO2.ID_HOLDER_PK= :idHolderPk");
	}
	if(Validations.validateIsNotNullAndPositive(filter.getDocumentType())){
		  sb.append(" AND HO2.DOCUMENT_TYPE= :docType");
	}
	if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentNumber())){
		  sb.append(" AND HO2.DOCUMENT_NUMBER = :docNumber");
	}
	if(Validations.validateIsNotNullAndNotEmpty(filter.getName())){
		  sb.append(" AND HO2.NAME like :holderName ");
	}
	if(Validations.validateIsNotNullAndNotEmpty(filter.getFirstLastName())){
		  sb.append(" AND HO2.FIRST_LAST_NAME like :holderFirstLastName ");
	}
	if(Validations.validateIsNotNullAndNotEmpty(filter.getSecondLastName())){
		  sb.append(" AND HO2.SECOND_LAST_NAME like :holderSecondLastName ");
	}
	if(Validations.validateIsNotNullAndNotEmpty(filter.getFullName())){
		  sb.append(" AND HO2.FULL_NAME like :holderFullName");
	}
	if(filter.isUserIssuer()){
		sb.append(" AND (EXISTS(SELECT 1 FROM HOLDER_ACCOUNT_BALANCE HAB , HOLDER_ACCOUNT HA,HOLDER_ACCOUNT_DETAIL HAD,SECURITY SE ");
		sb.append("WHERE HAB.ID_HOLDER_ACCOUNT_PK = HA.ID_HOLDER_ACCOUNT_PK ");
		sb.append("and HA.ID_HOLDER_ACCOUNT_PK = HAD.ID_HOLDER_ACCOUNT_FK ");
		sb.append("and HAB.ID_SECURITY_CODE_PK = SE.ID_SECURITY_CODE_PK ");
		sb.append("and SE.ID_ISSUER_FK = :issuer ");
		sb.append("and HAD.ID_HOLDER_FK = HO2.ID_HOLDER_PK ");
		sb.append("and HAB.TOTAL_BALANCE>0)) ");
	}
	if(filter.isUserIssuerDpf()){
		sb.append(" AND (EXISTS(SELECT 1 FROM HOLDER_ACCOUNT_BALANCE HAB , HOLDER_ACCOUNT HA,HOLDER_ACCOUNT_DETAIL HAD,SECURITY SE ");
		sb.append("WHERE HAB.ID_HOLDER_ACCOUNT_PK = HA.ID_HOLDER_ACCOUNT_PK ");
		sb.append("and HA.ID_HOLDER_ACCOUNT_PK = HAD.ID_HOLDER_ACCOUNT_FK ");
		sb.append("and HAB.ID_SECURITY_CODE_PK = SE.ID_SECURITY_CODE_PK ");
		sb.append("and SE.ID_ISSUER_FK = :issuer ");
		sb.append("and SE.SECURITY_CLASS in (:lstSecurityClass) ");
		sb.append("and HAD.ID_HOLDER_FK = HO2.ID_HOLDER_PK ");
		sb.append("and HAB.TOTAL_BALANCE>0)) ");
	}
		sb.append(" ) ) RESULTS ORDER BY RESULTS.ID_HOLDER_PK");
	
	Query query = em.createNativeQuery(sb.toString());

	if(Validations.validateIsNotNullAndNotEmpty(filter.getPersonType())){
		query.setParameter("personType", filter.getPersonType());
	}
	if(Validations.validateIsNotNullAndNotEmpty(filter.getState())){
		query.setParameter("state", filter.getState());
	}
	if(Validations.validateIsNotNullAndNotEmpty(filter.getPartIdPk())){
		query.setParameter("idParticipantPk", filter.getPartIdPk());
	}
	if(Validations.validateIsNotNullAndNotEmpty(filter.getHolderId())){
		query.setParameter("idHolderPk", filter.getHolderId());
	}
	if(Validations.validateIsNotNullAndPositive(filter.getDocumentType())){
		query.setParameter("docType",filter.getDocumentType());
	}
	if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentNumber())){
		query.setParameter("docNumber",filter.getDocumentNumber());
	}
	if(Validations.validateIsNotNullAndNotEmpty(filter.getName())){
		query.setParameter("holderName","%"+filter.getName()+"%");
	}
	if(Validations.validateIsNotNullAndNotEmpty(filter.getFirstLastName())){
		query.setParameter("holderFirstLastName","%"+filter.getFirstLastName()+"%");
	}
	if(Validations.validateIsNotNullAndNotEmpty(filter.getSecondLastName())){
		query.setParameter("holderSecondLastName","%"+filter.getSecondLastName()+"%");
	}
	if(Validations.validateIsNotNullAndNotEmpty(filter.getFullName())){
		query.setParameter("holderFullName","%"+filter.getFullName()+"%");
	}
	if(Validations.validateIsNotNullAndNotEmpty(filter.getIssuerFk()) && filter.isUserIssuer()){
		query.setParameter("issuer",filter.getIssuerFk());
	}
	if(Validations.validateIsNotNullAndNotEmpty(filter.getIssuerFk()) && filter.isUserIssuerDpf()){
		query.setParameter("issuer", filter.getIssuerFk());
		List<Integer> lstSecurityClass = new ArrayList<Integer>();
		lstSecurityClass.add(SecurityClassType.DPA.getCode());
		lstSecurityClass.add(SecurityClassType.DPF.getCode());
		query.setParameter("lstSecurityClass", lstSecurityClass);
	}
	query.setMaxResults(maxResultsQueryHelper);
	
	List<Object[]> searchResult = (List<Object[]>)query.getResultList();
	return searchResult;
	}
	
	/**
	 * Find holder by rnt.
	 *
	 * @param rnt the rnt
	 * @return the holder
	 * @throws ServiceException the service exception
	 */
	public Holder findHolderByRnt(Long rnt) throws ServiceException{
		StringBuilder querySql = new StringBuilder();
		querySql.append("Select ho.idHolderPk, ho.fullName, ho.stateHolder From Holder ho Where ho.idHolderPk = :rntParam");
		Query query = em.createQuery(querySql.toString());
		query.setParameter("rntParam", rnt);
		try{
			Object[] data =  (Object[]) query.getSingleResult();
			Holder holder = new Holder();
			holder.setIdHolderPk((Long) data[0]);
			holder.setFullName((String) data[1]);
			holder.setStateHolder((Integer) data[2]);
			return holder;
		}catch(NoResultException ex){
			throw new ServiceException(ErrorServiceType.HOLDER_NOT_EXIST);
		}catch(NonUniqueResultException ex){
			throw new ServiceException(ErrorServiceType.INCONSISTENCY_DATA);
		}
	}
	
	public Holder findHolderByDocumentInfo(String docNumber, Integer docType) throws ServiceException{
		StringBuilder querySql = new StringBuilder();
		querySql.append("Select ho.idHolderPk, ho.fullName, ho.stateHolder From Holder ho Where ho.documentType = :docType And ho.documentNumber = :docNumber");
		Query query = em.createQuery(querySql.toString());
		query.setParameter("docNumber", docNumber);
		query.setParameter("docType", docType);
		try{
			Object[] data =  (Object[]) query.getSingleResult();
			Holder holder = new Holder();
			holder.setIdHolderPk((Long) data[0]);
			holder.setFullName((String) data[1]);
			holder.setStateHolder((Integer) data[2]);
			return holder;
		}catch(NoResultException ex){
			return null;
		}catch(NonUniqueResultException ex){
			throw new ServiceException(ErrorServiceType.INCONSISTENCY_DATA);
		}
	}

	public Holder findHolder(Holder holder) {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT ho");
			sbQuery.append("	FROM Holder ho");
			sbQuery.append("	WHERE ho.idHolderPk = :idHolderPk");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idHolderPk", holder.getIdHolderPk());
			return (Holder)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Holder> findHolders(HolderSearchTO holderSearchTO) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT DISTINCT ho.idHolderPk,");
		sbQuery.append("			ho.name,");
		sbQuery.append("			ho.firstLastName,");
		sbQuery.append("			ho.secondLastName,");
		sbQuery.append("			ho.fullName,");
		sbQuery.append("			ho.documentType,");
		sbQuery.append("			ho.documentNumber,");
		sbQuery.append("			ho.holderType");
		sbQuery.append("	FROM Holder ho");
		if(Validations.validateIsNotNull(holderSearchTO.getParticipant()) && 
		   Validations.validateIsNotNullAndPositive(holderSearchTO.getParticipant().getIdParticipantPk())){
			sbQuery.append("	, HolderAccountDetail had ");		
		}
		sbQuery.append("	WHERE ho.stateHolder = :stateHolder");
		if(Validations.validateIsNotNull(holderSearchTO.getPersonType()) && 
				Validations.validateIsNotNullAndPositive(holderSearchTO.getPersonType())){
			sbQuery.append("	AND ho.holderType = :holderType");
		}		
		if(Validations.validateIsNotNull(holderSearchTO.getParticipant()) && 
		   Validations.validateIsNotNullAndPositive(holderSearchTO.getParticipant().getIdParticipantPk())){
			sbQuery.append("	AND ho.idHolderPk = had.holder.idHolderPk ");
			sbQuery.append("	AND had.holderAccount.participant.idParticipantPk = :idParticipantPk ");
			sbQuery.append("	AND had.holderAccount.stateAccount = :stateAccount ");
			sbQuery.append("	AND had.holderAccount.accountGroup = :accountGroup ");
		}
		/*
		if(Validations.validateIsNotNull(holderSearchTO.getParticipant()) && Validations.validateIsNotNullAndPositive(holderSearchTO.getParticipant().getIdParticipantPk())){
			sbQuery.append("	AND ( ho.idHolderPk IN (SELECT had.holder.idHolderPk");
			sbQuery.append("		FROM HolderAccountDetail had");
			sbQuery.append("		WHERE had.holderAccount.participant.idParticipantPk = :idParticipantPk");
			sbQuery.append("		AND had.holderAccount.stateAccount = :stateAccount)");
			sbQuery.append("		OR ho.idHolderPk IN (SELECT hol.idHolderPk");
			sbQuery.append("			FROM Holder hol");
			sbQuery.append("			WHERE hol.participant.idParticipantPk = :idParticipantPk) )");
		}*/
		if(Validations.validateIsNotNullAndNotEmpty(holderSearchTO.getDocumentType()))
			sbQuery.append("	AND ho.documentType = :documentType");
		if(Validations.validateIsNotNullAndNotEmpty(holderSearchTO.getDocumentNumber()))
			sbQuery.append("	AND ho.documentNumber = :documentNumber");		
		if(Validations.validateIsNotNullAndNotEmpty(holderSearchTO.getName()))
			sbQuery.append("	AND ho.name like :name");
		if(Validations.validateIsNotNullAndNotEmpty(holderSearchTO.getFirstLastName()))
			sbQuery.append("	AND ho.firstLastName like :firstLastName");
		if(Validations.validateIsNotNullAndNotEmpty(holderSearchTO.getSecondLastName()))
			sbQuery.append("	AND ho.secondLastName like :secondLastName");
		if(Validations.validateIsNotNullAndNotEmpty(holderSearchTO.getFullName()))
			sbQuery.append("	AND ho.fullName like :fullName");
		sbQuery.append("	ORDER BY ho.idHolderPk DESC");
		Query query = em.createQuery(sbQuery.toString());
		//set maximum result list for query
		query.setMaxResults(maxResultsQueryHelper);
		query.setParameter("stateHolder", holderSearchTO.getState());
		if(Validations.validateIsNotNull(holderSearchTO.getPersonType()) && 
				Validations.validateIsNotNullAndPositive(holderSearchTO.getPersonType())){
			query.setParameter("holderType", holderSearchTO.getPersonType());
		}		
		if(Validations.validateIsNotNullAndNotEmpty(holderSearchTO.getDocumentType()))
			query.setParameter("documentType", holderSearchTO.getDocumentType());
		if(Validations.validateIsNotNullAndNotEmpty(holderSearchTO.getDocumentNumber()))
			query.setParameter("documentNumber", holderSearchTO.getDocumentNumber());
		if(Validations.validateIsNotNullAndNotEmpty(holderSearchTO.getName()))
			query.setParameter("name", GeneralConstants.PERCENTAGE_STRING + 
										holderSearchTO.getName() +
										GeneralConstants.PERCENTAGE_STRING);
		if(Validations.validateIsNotNullAndNotEmpty(holderSearchTO.getFirstLastName()))
			query.setParameter("firstLastName", GeneralConstants.PERCENTAGE_STRING + 
										holderSearchTO.getFirstLastName() +
										GeneralConstants.PERCENTAGE_STRING);
		if(Validations.validateIsNotNullAndNotEmpty(holderSearchTO.getSecondLastName()))
			query.setParameter("secondLastName", GeneralConstants.PERCENTAGE_STRING + 
										holderSearchTO.getSecondLastName() +
										GeneralConstants.PERCENTAGE_STRING);
		if(Validations.validateIsNotNullAndNotEmpty(holderSearchTO.getFullName()))
			query.setParameter("fullName", GeneralConstants.PERCENTAGE_STRING + 
										holderSearchTO.getFullName() +
										GeneralConstants.PERCENTAGE_STRING);
		if(Validations.validateIsNotNull(holderSearchTO.getParticipant()) && 
		   Validations.validateIsNotNullAndPositive(holderSearchTO.getParticipant().getIdParticipantPk())){
			query.setParameter("idParticipantPk", holderSearchTO.getParticipant().getIdParticipantPk());
			query.setParameter("stateAccount", HolderAccountStatusType.ACTIVE.getCode());
			query.setParameter("accountGroup", HolderAccountGroupType.INVERSTOR.getCode());
		}
		List<Object[]> lstResult = (List<Object[]>)query.getResultList();
		List<Holder> lstHolder = null;
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
			lstHolder = new ArrayList<Holder>();
			for(Object[] object:lstResult){
				Holder holder = new Holder();
				holder.setIdHolderPk((Long)object[0]);
				holder.setName(Validations.validateIsNotNullAndNotEmpty(object[1])?object[1].toString():null);
				holder.setFirstLastName(Validations.validateIsNotNullAndNotEmpty(object[2])?object[2].toString():null);
				holder.setSecondLastName(Validations.validateIsNotNullAndNotEmpty(object[3])?object[3].toString():null);
				holder.setFullName(Validations.validateIsNotNullAndNotEmpty(object[4])?object[4].toString():null);
				holder.setDocumentType((Integer)object[5]);
				holder.setDocumentNumber(object[6].toString());
				holder.setHolderType((Integer)object[7]);
				lstHolder.add(holder);
			}
		}
		return lstHolder;
	}
	@SuppressWarnings("unchecked")
	public List<Holder> findHoldersForIssuer(HolderSearchTO holderSearchTO) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT distinct ho.idHolderPk,");
		sbQuery.append("			ho.name,");
		sbQuery.append("			ho.firstLastName,");
		sbQuery.append("			ho.secondLastName,");
		sbQuery.append("			ho.fullName,");
		sbQuery.append("			ho.documentType,");
		sbQuery.append("			ho.documentNumber,");
		sbQuery.append("			ho.holderType");
		sbQuery.append("	FROM Holder ho");
		if(Validations.validateIsNotNull(holderSearchTO.getIssuer()) && 
		   Validations.validateIsNotNullAndNotEmpty(holderSearchTO.getIssuer().getIdIssuerPk())){
			sbQuery.append("	,HolderAccount hol,HolderAccountDetail had,HolderAccountBalance hab");
			sbQuery.append("	,Security se,Issuance iss,Issuer isss ");
		}
		sbQuery.append("	WHERE ho.stateHolder = :stateHolder");
		if(Validations.validateIsNotNull(holderSearchTO.getPersonType()) && 
				Validations.validateIsNotNullAndPositive(holderSearchTO.getPersonType())){
			sbQuery.append("	AND ho.holderType = :holderType");
		}		
		if(Validations.validateIsNotNull(holderSearchTO.getIssuer()) && 
		   Validations.validateIsNotNullAndNotEmpty(holderSearchTO.getIssuer().getIdIssuerPk())){
			sbQuery.append("	AND ho.idHolderPk = had.holder.idHolderPk ");
			sbQuery.append("	AND had.holderAccount.idHolderAccountPk = hol.idHolderAccountPk ");
			sbQuery.append("	AND hol.idHolderAccountPk = hab.holderAccount.idHolderAccountPk ");
			sbQuery.append("	AND hab.security.idSecurityCodePk = se.idSecurityCodePk ");
			sbQuery.append("	AND hab.participant.idParticipantPk = hol.participant.idParticipantPk ");
			sbQuery.append("	AND se.issuance.idIssuanceCodePk = iss.idIssuanceCodePk ");
			sbQuery.append("	AND iss.issuer.idIssuerPk = isss.idIssuerPk ");	
			sbQuery.append("	AND isss.idIssuerPk = :idIssuerPk ");
			sbQuery.append("	AND had.holderAccount.stateAccount = :stateAccount ");
			sbQuery.append("	AND had.holderAccount.accountGroup = :accountGroup ");		
		}
		if(Validations.validateIsNotNull(holderSearchTO.getParticipant()) && 
				Validations.validateIsNotNullAndPositive(holderSearchTO.getParticipant().getIdParticipantPk())){
			sbQuery.append("	AND hol.participant.idParticipantPk = :idParticipantPk ");		
		}
		if(Validations.validateIsNotNullAndNotEmpty(holderSearchTO.getDocumentType()))
			sbQuery.append("	AND ho.documentType = :documentType");
		if(Validations.validateIsNotNullAndNotEmpty(holderSearchTO.getDocumentNumber()))
			sbQuery.append("	AND ho.documentNumber = :documentNumber");		
		if(Validations.validateIsNotNullAndNotEmpty(holderSearchTO.getName()))
			sbQuery.append("	AND ho.name like :name");
		if(Validations.validateIsNotNullAndNotEmpty(holderSearchTO.getFirstLastName()))
			sbQuery.append("	AND ho.firstLastName like :firstLastName");
		if(Validations.validateIsNotNullAndNotEmpty(holderSearchTO.getSecondLastName()))
			sbQuery.append("	AND ho.secondLastName like :secondLastName");
		if(Validations.validateIsNotNullAndNotEmpty(holderSearchTO.getFullName()))
			sbQuery.append("	AND ho.fullName like :fullName");
		sbQuery.append("	ORDER BY ho.idHolderPk DESC");
		Query query = em.createQuery(sbQuery.toString());
		//set maximum result list
		query.setMaxResults(maxResultsQueryHelper);
		query.setParameter("stateHolder", holderSearchTO.getState());
		if(Validations.validateIsNotNull(holderSearchTO.getPersonType()) && 
				Validations.validateIsNotNullAndPositive(holderSearchTO.getPersonType())){
			query.setParameter("holderType", holderSearchTO.getPersonType());
		}		
		if(Validations.validateIsNotNullAndNotEmpty(holderSearchTO.getDocumentType()))
			query.setParameter("documentType", holderSearchTO.getDocumentType());
		if(Validations.validateIsNotNullAndNotEmpty(holderSearchTO.getDocumentNumber()))
			query.setParameter("documentNumber", holderSearchTO.getDocumentNumber());
		if(Validations.validateIsNotNullAndNotEmpty(holderSearchTO.getName()))
			query.setParameter("name", GeneralConstants.PERCENTAGE_STRING + 
										holderSearchTO.getName() +
										GeneralConstants.PERCENTAGE_STRING);
		if(Validations.validateIsNotNullAndNotEmpty(holderSearchTO.getFirstLastName()))
			query.setParameter("firstLastName", GeneralConstants.PERCENTAGE_STRING + 
										holderSearchTO.getFirstLastName() +
										GeneralConstants.PERCENTAGE_STRING);
		if(Validations.validateIsNotNullAndNotEmpty(holderSearchTO.getSecondLastName()))
			query.setParameter("secondLastName", GeneralConstants.PERCENTAGE_STRING + 
										holderSearchTO.getSecondLastName() +
										GeneralConstants.PERCENTAGE_STRING);
		if(Validations.validateIsNotNullAndNotEmpty(holderSearchTO.getFullName()))
			query.setParameter("fullName", GeneralConstants.PERCENTAGE_STRING + 
										holderSearchTO.getFullName() +
										GeneralConstants.PERCENTAGE_STRING);
		if(Validations.validateIsNotNull(holderSearchTO.getIssuer()) && 
		   Validations.validateIsNotNullAndNotEmpty(holderSearchTO.getIssuer().getIdIssuerPk())){
			query.setParameter("idIssuerPk", holderSearchTO.getIssuer().getIdIssuerPk());
			query.setParameter("stateAccount", HolderAccountStatusType.ACTIVE.getCode());
			query.setParameter("accountGroup", HolderAccountGroupType.INVERSTOR.getCode());
		}
		if(Validations.validateIsNotNull(holderSearchTO.getParticipant()) && 
				Validations.validateIsNotNullAndPositive(holderSearchTO.getParticipant().getIdParticipantPk())){
			query.setParameter("idParticipantPk", holderSearchTO.getParticipant().getIdParticipantPk());
		}
		List<Object[]> lstResult = (List<Object[]>)query.getResultList();
		List<Holder> lstHolder = null;
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
			lstHolder = new ArrayList<Holder>();
			for(Object[] object:lstResult){
				Holder holder = new Holder();
				holder.setIdHolderPk((Long)object[0]);
				holder.setName(Validations.validateIsNotNullAndNotEmpty(object[1])?object[1].toString():null);
				holder.setFirstLastName(Validations.validateIsNotNullAndNotEmpty(object[2])?object[2].toString():null);
				holder.setSecondLastName(Validations.validateIsNotNullAndNotEmpty(object[3])?object[3].toString():null);
				holder.setFullName(Validations.validateIsNotNullAndNotEmpty(object[4])?object[4].toString():null);
				holder.setDocumentType((Integer)object[5]);
				holder.setDocumentNumber(object[6].toString());
				holder.setHolderType((Integer)object[7]);
				lstHolder.add(holder);
			}
		}
		return lstHolder;
	}
	public HolderAccount finfHolderAccount(Integer accountNumber, Long idParticipant, Long idHolder) {
		Map<String,Object> parameters = new HashMap<String, Object>();
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT distinct ha ");
		sbQuery.append("	FROM HolderAccount ha");
		sbQuery.append("	inner join fetch ha.holderAccountDetails had");
		sbQuery.append("	inner join fetch had.holder ho ");
		sbQuery.append("	inner join ha.participant p");
		sbQuery.append("	where ha.accountNumber = :accountNumber");
		if(idParticipant != null){
			sbQuery.append("	and p.idParticipantPk = :idParticipant ");
			parameters.put("idParticipant", idParticipant);
		}
		if(idHolder != null){
			sbQuery.append(" and exists ( ");
			sbQuery.append("     select had2.holder.idHolderPk from HolderAccountDetail had2  ");
			sbQuery.append("     where had2.holder.idHolderPk = :idHolder ");
			sbQuery.append("     and   had2.holderAccount.idHolderAccountPk = ha.idHolderAccountPk ");
			sbQuery.append("  ) ");
			parameters.put("idHolder", idHolder);
		}
		
		parameters.put("accountNumber", accountNumber);
		
		return (HolderAccount) findObjectByQueryString(sbQuery.toString(), parameters);
	}

	@SuppressWarnings("unchecked")
	public List<HolderAccount> findHolderAccounts(HolderAccountSearchTO holderAccountSearchTO) {
		Map <String,Object> parameter=new HashMap<>();
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT distinct hadd.holderAccount.idHolderAccountPk");
		sbQuery.append("	FROM HolderAccountDetail hadd");
		sbQuery.append("	WHERE ");
		sbQuery.append("	hadd.holderAccount.participant.idParticipantPk = :idParticipantPk");
		if(Validations.validateIsNotNullAndNotEmpty(holderAccountSearchTO.getHolder())
				&& Validations.validateIsNotNullAndNotEmpty(holderAccountSearchTO.getHolder().getIdHolderPk()))
			sbQuery.append("	AND hadd.holder.idHolderPk = :idHolderPk");		
		if(Validations.validateIsNotNullAndPositive(holderAccountSearchTO.getAccountType()))
			sbQuery.append("	AND hadd.holderAccount.accountType = :accountType");
		if(Validations.validateIsNotNullAndPositive(holderAccountSearchTO.getState()))
			sbQuery.append("	AND hadd.holderAccount.stateAccount = :stateAccount");
		parameter.put("idParticipantPk", holderAccountSearchTO.getParticipant().getIdParticipantPk());
		if(Validations.validateIsNotNullAndNotEmpty(holderAccountSearchTO.getHolder())
				&& Validations.validateIsNotNullAndNotEmpty(holderAccountSearchTO.getHolder().getIdHolderPk())){
			parameter.put("idHolderPk", holderAccountSearchTO.getHolder().getIdHolderPk());
		}
		if(Validations.validateIsNotNullAndPositive(holderAccountSearchTO.getAccountType())){
			parameter.put("accountType", holderAccountSearchTO.getAccountType());
		}
		if(Validations.validateIsNotNullAndPositive(holderAccountSearchTO.getState())){
			parameter.put("stateAccount", holderAccountSearchTO.getState());
		}
			StringBuilder mainQuery = new StringBuilder();
			mainQuery.append("	SELECT distinct ha");
			mainQuery.append("	FROM HolderAccount ha");
			mainQuery.append("	INNER JOIN FETCH ha.holderAccountDetails had");
			mainQuery.append("	INNER JOIN FETCH had.holder");
			mainQuery.append("	WHERE ha.idHolderAccountPk in (");
			mainQuery.append(sbQuery.toString() + " ) ");
			mainQuery.append("	ORDER BY ha.idHolderAccountPk DESC");
			return (List<HolderAccount>)findListByQueryString(mainQuery.toString(),parameter);
	}

	public boolean findRelationHolderParticipant(Long idHolderPk, Long idParticipantPk,boolean isParticipantInv) {
		boolean blCreation = false, blAccounts = false;
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT COUNT(had)");
		sbQuery.append("	FROM HolderAccountDetail had");
		sbQuery.append("	WHERE had.holderAccount.participant.idParticipantPk = :idParticipantPk");
		sbQuery.append("	AND had.holder.idHolderPk = :idHolderPk");
		if (isParticipantInv)
			sbQuery.append("	AND had.holderAccount.participant.documentNumber=had.holder.documentNumber");
		//sbQuery.append("	AND had.holderAccount.stateAccount = :stateAccount");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idHolderPk", idHolderPk);
		query.setParameter("idParticipantPk", idParticipantPk);
		//query.setParameter("stateAccount", HolderAccountStatusType.ACTIVE.getCode());
		Long result = new Long(query.getSingleResult().toString());
		if(result > 0)
			blAccounts = true;		
		sbQuery = new StringBuilder();
		sbQuery.append("	SELECT COUNT(ho)");
		sbQuery.append("	FROM Holder ho");
		sbQuery.append("	WHERE ho.idHolderPk = :idHolderPk");
		sbQuery.append("	AND ho.participant.idParticipantPk = :idParticipantPk");
		if (isParticipantInv)
			sbQuery.append("	AND ho.participant.documentNumber=ho.documentNumber");
		query = em.createQuery(sbQuery.toString());
		query.setParameter("idHolderPk", idHolderPk);
		query.setParameter("idParticipantPk", idParticipantPk);
		result = new Long(query.getSingleResult().toString());
		if(result > 0)
			blCreation = true;
		if(blCreation || blAccounts)
			return true;
		return false;
	}
	public boolean findRelationHolderIssuerParticipant(Long idHolderPk, String idIssuer,Long idParticipantPk,boolean isParticipantInv) {
		boolean blCreation = false;
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT COUNT(ho) ");
		sbQuery.append("	FROM Holder ho");		
		if(Validations.validateIsNotNullAndNotEmpty(idIssuer)){
			sbQuery.append("	,HolderAccount hol,HolderAccountDetail had,HolderAccountBalance hab");
			sbQuery.append("	,Security se,Issuance iss,Issuer isss ");
		}
		sbQuery.append("	WHERE 1 = 1 ");
		if(Validations.validateIsNotNullAndNotEmpty(idIssuer)){
			sbQuery.append("	AND ho.idHolderPk = had.holder.idHolderPk ");
			sbQuery.append("	AND had.holderAccount.idHolderAccountPk = hol.idHolderAccountPk ");
			sbQuery.append("	AND hol.idHolderAccountPk = hab.holderAccount.idHolderAccountPk ");
			sbQuery.append("	AND hab.security.idSecurityCodePk = se.idSecurityCodePk ");
			sbQuery.append("	AND hab.participant.idParticipantPk = hol.participant.idParticipantPk ");
			sbQuery.append("	AND se.issuance.idIssuanceCodePk = iss.idIssuanceCodePk ");
			sbQuery.append("	AND iss.issuer.idIssuerPk = isss.idIssuerPk ");	
			sbQuery.append("	AND isss.idIssuerPk = :idIssuerPk ");
			sbQuery.append("	AND had.holderAccount.stateAccount = :stateAccount ");
			sbQuery.append("	AND had.holderAccount.accountGroup = :accountGroup ");		
		}
		if(	Validations.validateIsNotNullAndPositive(idParticipantPk)){
			sbQuery.append("	AND hol.participant.idParticipantPk = :idParticipantPk ");		
		}
		if(	Validations.validateIsNotNullAndPositive(idHolderPk)){
			sbQuery.append("	AND ho.idHolderPk = :idHolderPk ");		
			if (isParticipantInv)
				sbQuery.append("	AND had.holderAccount.participant.documentNumber=ho.documentNumber");
		}
		sbQuery.append("	ORDER BY ho.idHolderPk DESC");
		Query query = em.createQuery(sbQuery.toString());
		if(  Validations.validateIsNotNullAndNotEmpty(idIssuer)){
			query.setParameter("idIssuerPk", idIssuer);
			query.setParameter("stateAccount", HolderAccountStatusType.ACTIVE.getCode());
			query.setParameter("accountGroup", HolderAccountGroupType.INVERSTOR.getCode());
		}
		if(Validations.validateIsNotNullAndPositive(idParticipantPk)){
			query.setParameter("idParticipantPk", idParticipantPk);
		}
		if(Validations.validateIsNotNullAndPositive(idHolderPk)){
			query.setParameter("idHolderPk", idHolderPk);
		}
		Long result = new Long(query.getSingleResult().toString());
		if(result > 0)
			blCreation = true;
		if(blCreation)
			return true;
		return false;
	}
	
	/**
	 * Metodo que permite buscar los CUI's asociados a una determinada cuenta
	 * 
	 * @param holderAccountObjectTO
	 * @return
	 */
	public List<Holder> findHolders(HolderAccountObjectTO holderAccountObjectTO){

		List<Holder> listHolders = null;

		StringBuilder query = new StringBuilder();
		
		query.append(" SELECT h ");
		query.append(" FROM HolderAccountDetail had ");
		query.append(" JOIN had.holderAccount ha ");
		query.append(" JOIN had.holder h");
		query.append(" WHERE ha.idHolderAccountPk = :idHolderAccountPk");
		
		Query consult = em.createQuery(query.toString());
		
		consult.setParameter("idHolderAccountPk", holderAccountObjectTO.getIdHolderAccount());
		
		listHolders = consult.getResultList();		
		
		return listHolders;
	}
	
}