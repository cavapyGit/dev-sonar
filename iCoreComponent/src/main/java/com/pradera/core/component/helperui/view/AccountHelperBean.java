package com.pradera.core.component.helperui.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.el.ELContext;
import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.HelperBean;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.AccountSearcherTO;
import com.pradera.core.component.helperui.to.AccountTO;
import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountGroupType;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;

@HelperBean
public class AccountHelperBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 230216630609700020L;
	
	/** The log. */
	@Inject
	private transient PraderaLogger log;
	
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	@EJB
	GeneralParametersFacade parametersFacade;
	
	private AccountSearcherTO accountSearcher = new AccountSearcherTO();
	private AccountTO selected;
	private List<AccountTO> listAccounts;
	private String alternativeCode;
	private String accountDescription;
	private List<String> holderNames;
	private List<SelectItem> listParticipants;
	private List<SelectItem> listDocumentType;
	private Integer searchFilter;
	private boolean searched;
	private HolderTO holderTO = new HolderTO();
	private List<ParameterTable> docTypeList;
	private List<ParameterTable> accountStatusList;
	private GenericDataModel<HolderAccountHelperResultTO> holderAccountHelperResultTOlist;
	private Holder holder;
	private boolean flagAccountGroup = false;
	private String regularExpression;
	private int    charactersNumber;
	private boolean listEmpty = false;
	List<HolderAccountHelperResultTO> resultList = new ArrayList<HolderAccountHelperResultTO>();
	//private String helperName;
	
	@PostConstruct
	public void init() throws ServiceException {
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();

		regularExpression = "/^([a-z]|[A-Z]|[\u00F1\u00D1]|[\\d]|[\\s])*$/";
		charactersNumber  = 21; //MAX NUMBER OF CHARACTERS
		searchFilter      = GeneralConstants.NEGATIVE_ONE_INTEGER;
		fillComponents();
	}
	
	//FILL COMBOS
	public void fillComponents() throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		//PARTICIPANTS
		listParticipants = new ArrayList<SelectItem>();
		
		//DOCUMENT TYPES
		parameterTableTO.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
		docTypeList = parametersFacade.getListParameterTableServiceBean(parameterTableTO);
		
		//ACCOUNTS STATES
		parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
		accountStatusList = parametersFacade.getListParameterTableServiceBean(parameterTableTO);
		
		Participant participant = new Participant();
//		participant.setState(ParticipantStateType.REGISTERED.getCode());
		for(Participant part : helperComponentFacade.getLisParticipantServiceFacade(participant)) {
			listParticipants.add(new SelectItem(part.getIdParticipantPk(), part.getDisplayDescriptionCode()));
		}

	}

	public void searchAccountByNumber() {
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		ValueExpression valueExpression = context.getApplication().getExpressionFactory()
		    .createValueExpression(elContext, "#{cc.attrs.holderAccount}", HolderAccount.class);

		ValueExpression valueExpressionPart = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.attrs.partCode}", Long.class);
		
		ValueExpression titleGeneralValueEx = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.resourceBundleMap.titleGeneral}", String.class);
		String titleGeneral= (String)titleGeneralValueEx.getValue(elContext);
		
		ValueExpression accountHelpMessageValueEX = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.resourceBundleMap.accountNotExist}", String.class);
		String accountHelpMessage= (String)accountHelpMessageValueEX.getValue(elContext);
		
		ValueExpression partNotSelectedValueEX = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.resourceBundleMap.partNotSelected}", String.class);
		String partNotSelected= (String)partNotSelectedValueEX.getValue(elContext);
		
		ValueExpression valueExpressionName = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.name}", String.class);
		String helperName = (String)valueExpressionName.getValue(elContext);
		
		try {
		 if (valueExpression.getValue(elContext) instanceof HolderAccount) {
			 HolderAccount ha = (HolderAccount)valueExpression.getValue(elContext);
			 Long partCode = (Long)valueExpressionPart.getValue(elContext);
			 
			 if(Validations.validateIsNotNullAndPositive(partCode)){
				 				 
			 if (Validations.validateIsNotNullAndPositive(ha.getAccountNumber())) {
				 selected = new AccountTO();
				 selected.setAccountNumber(ha.getAccountNumber());
				 accountSearcher.setParticipantCode(partCode);
				 accountSearcher.setSelected(selected);
				 selected = helperComponentFacade.findAccountByNumberAndParticipant(accountSearcher);
				 
				 if (selected != null) {
					alternativeCode = selected.getAlternativeCode();
					accountDescription = selected.getAccountDescription();
					
					if(flagAccountGroup==true){
						
						ValueExpression holderNotInversionist = context.getApplication().getExpressionFactory()
							    .createValueExpression(elContext, "#{cc.resourceBundleMap.holderNotInversionist}", String.class);
						String holderInversionist = (String)holderNotInversionist.getValue(elContext);
						
						ValueExpression warningMessageEx = context.getApplication().getExpressionFactory()
							    .createValueExpression(elContext, "#{cc.resourceBundleMap.warningMessage}", String.class);
						String warningMessage= (String)warningMessageEx.getValue(elContext);
						
						if(Validations.validateIsNotNullAndPositive(selected.getAccountGroup())){
							if(selected.getAccountGroup().equals(HolderAccountGroupType.INVERSTOR.getCode())){
								ha.setAlternateCode(alternativeCode);
								ha.setAccountDescription(accountDescription);
								ha = selected.getHolderAccount();
								valueExpression.setValue(elContext, ha);
							}else{
								RequestContext.getCurrentInstance().execute("PF('cnfWMsgHolderInver"+helperName+"').show();");/**/
								//"la cuenta "+ha.getAccountNumber()+" No es de Tipo Inversionista"
								 JSFUtilities.putViewMap("bodyMessageView", holderInversionist );
								 JSFUtilities.putViewMap("headerMessageView", warningMessage);
								 alternativeCode = null;
								 accountDescription = null;
								 ha.setAccountNumber(null);
								 ha.setAlternateCode(null);
								 ha.setHolderAccountDetails(null);
							}
						}else{
							RequestContext.getCurrentInstance().execute("PF('cnfWMsgHolderInver"+helperName+"').show();");/**/
							 JSFUtilities.putViewMap("bodyMessageView", holderInversionist);
							 JSFUtilities.putViewMap("headerMessageView", warningMessage);
							 alternativeCode = null;
							 accountDescription = null;
							 ha.setAccountNumber(null);
							 ha.setAlternateCode(null);
							 ha.setHolderAccountDetails(null);
						}
						
					}else{
						ha.setAlternateCode(alternativeCode);
						ha.setAccountDescription(accountDescription);
						ha = selected.getHolderAccount();
						valueExpression.setValue(elContext, ha);
					}
				 } else {
						 	 JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidation"+helperName+"').show();");
							 JSFUtilities.putViewMap("bodyMessageView", accountHelpMessage);
							 JSFUtilities.putViewMap("headerMessageView", titleGeneral);
							 alternativeCode = null;
							 accountDescription = null;
							 ha.setAccountNumber(null);
							 ha.setAlternateCode(null);
							 ha.setAccountDescription(null);
							 ha.setHolderAccountDetails(null);
						 }
			 	}else{
			 		 alternativeCode = null;
					 accountDescription = null;
					 ha.setAccountNumber(null);
					 ha.setAlternateCode(null);
					 ha.setHolderAccountDetails(null);
					 ha.setAccountDescription(null);
			 	}
			 }else{
				 RequestContext.getCurrentInstance().execute("PF('cnfWMsgParticipantValidation"+helperName+"').show();");
				 JSFUtilities.putViewMap("bodyMessageView", partNotSelected);
				 JSFUtilities.putViewMap("headerMessageView", titleGeneral);
				 alternativeCode = null;
				 accountDescription = null;
				 ha.setHolderAccountDetails(null);
				 ha.setAccountNumber(null);
				 ha.setAlternateCode(null);
			 }
		 } else {
			 UIComponent uitextComp = UIComponent.getCurrentComponent(context);
			 if(uitextComp instanceof UIInput) {
				UIInput issuerText = (UIInput)uitextComp;
				issuerText.setValid(true);
			 }
			 valueExpression.setValue(elContext, null);
		 }
		} catch(ServiceException sex) {
			log.error(sex.getMessage());
		} catch (Exception ex) {
			log.error(ex.getMessage());
		}
	}
	
	public void searchHolderByCode() throws ServiceException{
		holder = helperComponentFacade.findHolderByCode(holderTO.getHolderId());
	}
	
	public void searchAccountByFilter() throws ServiceException{
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		ValueExpression valueExpression = context.getApplication().getExpressionFactory()
		    .createValueExpression(elContext, "#{cc.attrs.partCode}", Long.class);
		
		ValueExpression valueExpressionHelperName = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.attrs.name}", String.class);
		
		String helperName = (String) valueExpressionHelperName.getValue(elContext);
		
		//SETTING VALUE OF COMPONENT(PARTICIPANT PK)
		holderTO.setPartIdPk((Long) valueExpression.getValue(elContext));
		
		
		if(Validations.validateIsNullOrNotPositive(holderTO.getPartIdPk()) 		&&
			Validations.validateIsNullOrNotPositive(holderTO.getHolderId()) 	&&
			Validations.validateIsNullOrNotPositive(holderTO.getAccountState()) &&
			Validations.validateIsNullOrNotPositive(holderTO.getDocumentType()) &&
			Validations.validateIsNullOrEmpty(holderTO.getDocumentNumber()) 	&&
			Validations.validateIsNullOrEmpty(holderTO.getName()) 				&&
			Validations.validateIsNullOrEmpty(holderTO.getFirstLastName()) 		&&
			Validations.validateIsNullOrEmpty(holderTO.getSecondLastName()) 	&&
			Validations.validateIsNullOrEmpty(holderTO.getFullName())){
			
			//ALERT MESSAGE ABOUT MANDATORY FIELDS
			JSFUtilities.executeJavascriptFunction("PF('dlgWExistPendingData"+helperName+"').show();");
		}else{
				
				List<HolderAccountHelperResultTO> resultList = helperComponentFacade.searchAccountByFilter(holderTO);

				if(resultList.size() > GeneralConstants.ZERO_VALUE_INTEGER){
					listEmpty = false;
				}else{
					listEmpty = true;
				}
				
				holderAccountHelperResultTOlist = new GenericDataModel<HolderAccountHelperResultTO>(resultList);
		}
	}
	
	public void assignAccountFromHelper(HolderAccountHelperResultTO accountResultSelected) throws ServiceException{
		HolderAccount holderAccount = new HolderAccount();
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext  = context.getELContext();
		//GETTING HOLDER OBJECT FROM HOLDER HELPER
		ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext,"#{cc.attrs.holderAccount}", HolderAccount.class);

		AccountSearcherTO accountSearcher  = new AccountSearcherTO();
		accountSearcher.setSelected(new AccountTO());
		accountSearcher.setParticipantCode(holderTO.getPartIdPk());
		accountSearcher.getSelected().setAccountNumber(accountResultSelected.getAccountNumber().intValue());
		AccountTO acc = helperComponentFacade.findAccountByNumberAndParticipant(accountSearcher);
		
		holderAccount = acc.getHolderAccount();

		if(flagAccountGroup==true){
			
			ValueExpression titleGeneralValueEx = context.getApplication().getExpressionFactory()
				    .createValueExpression(elContext, "#{cc.resourceBundleMap.titleGeneral}", String.class);
			String titleGeneral= (String)titleGeneralValueEx.getValue(elContext);
			
			ValueExpression holderNotInversionist = context.getApplication().getExpressionFactory()
				    .createValueExpression(elContext, "#{cc.resourceBundleMap.holderNotInversionist}", String.class);
			String holderInversionist = (String)holderNotInversionist.getValue(elContext);
			
			ValueExpression warningMessageEx = context.getApplication().getExpressionFactory()
				    .createValueExpression(elContext, "#{cc.resourceBundleMap.warningMessage}", String.class);
			String warningMessage= (String)warningMessageEx.getValue(elContext);
			
			ValueExpression valueExpressionName = context.getApplication().getExpressionFactory()
					.createValueExpression(elContext, "#{cc.attrs.name}", String.class);
			String helperName = (String)valueExpressionName.getValue(elContext);
			
			if(Validations.validateIsNotNullAndPositive(holderAccount.getAccountGroup())){
				if(holderAccount.getAccountGroup().equals(462)){
					valueExpression.setValue(elContext, holderAccount);
					clear();
				}else{
					RequestContext.getCurrentInstance().execute("PF('cnfWMsgHolderInver"+helperName+"').show();");/**/
					 JSFUtilities.putViewMap("bodyMessageView", holderInversionist);
					 JSFUtilities.putViewMap("headerMessageView", warningMessage);
				}
			}else{
				RequestContext.getCurrentInstance().execute("PF('cnfWMsgHolderInver"+helperName+"').show();");/**/
				 JSFUtilities.putViewMap("bodyMessageView", holderInversionist);
				 JSFUtilities.putViewMap("headerMessageView", warningMessage);
			}
			
		}else{
			valueExpression.setValue(elContext, holderAccount);
			clear();
		}
		
	}
	
	/**/
	public HolderAccount findHolderAccount(String alternateCode){
		for (int i = 0; i < holderAccountHelperResultTOlist.getDataList().size(); i++) {
			if(holderAccountHelperResultTOlist.getDataList().get(i).getAlternateCode().equals(alternateCode)){
				return holderAccountHelperResultTOlist.getDataList().get(i).getHolderAccount();
			}
		}
		return new HolderAccount();
	} 
	/**/
	//clearing by option selected
	public void clearByOption(){
		switch(searchFilter){
			case 1:
				holderTO.setDocumentType(null);
				holderTO.setDocumentNumber(null);
				holderTO.setName(null);
				holderTO.setFirstLastName(null);
				holderTO.setSecondLastName(null);
				holderTO.setFullName(null);
				break;
			case 2:
				holderTO.setHolderId(null);
				holder = null;
				holderTO.setName(null);
				holderTO.setFirstLastName(null);
				holderTO.setSecondLastName(null);
				holderTO.setFullName(null);
				break;
			case 3:
				holderTO.setHolderId(null);
				holder = null;
				holderTO.setDocumentType(null);
				holderTO.setDocumentNumber(null);
				holderTO.setFullName(null);
				break;
			case 4:
				holderTO.setHolderId(null);
				holder = null;
				holderTO.setDocumentType(null);
				holderTO.setDocumentNumber(null);
				holderTO.setName(null);
				holderTO.setFirstLastName(null);
				holderTO.setSecondLastName(null);
				break;
		}
	}	
	
	public void changeRegularExpressionAndCharactersNumber(){
		holderTO.setDocumentNumber("");
		if( holderTO.getDocumentType().equals(DocumentType.CI.getCode())  ||
			holderTO.getDocumentType().equals(DocumentType.RUC.getCode())){
			regularExpression = "/^([\\d])*$/";  //NUMERIC
			charactersNumber  = 11;
		}
		if(holderTO.getDocumentType().equals(DocumentType.CDN.getCode())){
			regularExpression = "/^([\\d])*$/";  //NUMERIC
			charactersNumber  = 21;
		}
		if(holderTO.getDocumentType().equals(DocumentType.PAS.getCode()) ||
		   holderTO.getDocumentType().equals(DocumentType.DIO.getCode())){
			regularExpression = "/^([a-z]|[A-Z]|[\u00F1\u00D1]|[\\d]|[\\s])*$/";  //ALPHANUMERIC
			charactersNumber  = 15;
		}
	}
	
	//CLEAR BUTTON
	public void clear(){
		holderAccountHelperResultTOlist = null;
		selected						= new AccountTO();
		searchFilter        	   		= GeneralConstants.NEGATIVE_ONE_INTEGER;
		holder							= null;
		listEmpty = false;
		holderTO.setHolderId(null);
		holderTO.setDocumentTypeDesc(null);
		holderTO.setDocumentType(null);
		holderTO.setDocumentNumber(null);
		holderTO.setName(null);
		holderTO.setFirstLastName(null);
		holderTO.setSecondLastName(null);
		holderTO.setFullName(null);
		holderTO.setAccountState(null);
		
	}
	
	public void exit(){
		clear();
		holderTO.setHolderId(null);
	}
	public AccountHelperBean() {
		// TODO Auto-generated constructor stub
	}
	public AccountSearcherTO getAccountSearcher() {
		return accountSearcher;
	}
	public void setAccountSearcher(AccountSearcherTO accountSearcher) {
		this.accountSearcher = accountSearcher;
	}
	public AccountTO getSelected() {
		return selected;
	}
	public void setSelected(AccountTO selected) {
		this.selected = selected;
	}
	public List<AccountTO> getListAccounts() {
		return listAccounts;
	}
	public void setListAccounts(List<AccountTO> listAccounts) {
		this.listAccounts = listAccounts;
	}
	public String getAlternativeCode() {
		return alternativeCode;
	}
	public void setAlternativeCode(String alternativeCode) {
		this.alternativeCode = alternativeCode;
	}
	public List<String> getHolderNames() {
		return holderNames;
	}
	public void setHolderNames(List<String> holderNames) {
		this.holderNames = holderNames;
	}
	public boolean isSearched() {
		return searched;
	}
	public void setSearched(boolean searched) {
		this.searched = searched;
	}
	public List<SelectItem> getListParticipants() {
		return listParticipants;
	}
	public void setListParticipants(List<SelectItem> listParticipants) {
		this.listParticipants = listParticipants;
	}
	public List<SelectItem> getListDocumentType() {
		return listDocumentType;
	}
	public void setListDocumentType(List<SelectItem> listDocumentType) {
		this.listDocumentType = listDocumentType;
	}
	public Integer getSearchFilter() {
		return searchFilter;
	}
	public void setSearchFilter(Integer searchFilter) {
		this.searchFilter = searchFilter;
	}
	public HolderTO getHolderTO() {
		return holderTO;
	}
	public void setHolderTO(HolderTO holderTO) {
		this.holderTO = holderTO;
	}
	public List<ParameterTable> getDocTypeList() {
		return docTypeList;
	}
	public void setDocTypeList(List<ParameterTable> docTypeList) {
		this.docTypeList = docTypeList;
	}
	public String getAccountDescription() {
		return accountDescription;
	}
	public void setAccountDescription(String accountDescription) {
		this.accountDescription = accountDescription;
	}
	public GenericDataModel<HolderAccountHelperResultTO> getHolderAccountHelperResultTOlist() {
		return holderAccountHelperResultTOlist;
	}
	public void setHolderAccountHelperResultTOlist(
			GenericDataModel<HolderAccountHelperResultTO> holderAccountHelperResultTOlist) {
		this.holderAccountHelperResultTOlist = holderAccountHelperResultTOlist;
	}
	public List<ParameterTable> getAccountStatusList() {
		return accountStatusList;
	}
	public void setAccountStatusList(List<ParameterTable> accountStatusList) {
		this.accountStatusList = accountStatusList;
	}
	public Holder getHolder() {
		return holder;
	}
	public void setHolder(Holder holder) {
		this.holder = holder;
	}
	public boolean isFlagAccountGroup() {
		return flagAccountGroup;
	}
	public void setFlagAccountGroup(boolean flagAccountGroup) {
		this.flagAccountGroup = flagAccountGroup;
	}
	public String getRegularExpression() {
		return regularExpression;
	}
	public void setRegularExpression(String regularExpression) {
		this.regularExpression = regularExpression;
	}
	public int getCharactersNumber() {
		return charactersNumber;
	}
	public void setCharactersNumber(int charactersNumber) {
		this.charactersNumber = charactersNumber;
	}
	public boolean isListEmpty() {
		return listEmpty;
	}
	public void setListEmpty(boolean listEmpty) {
		this.listEmpty = listEmpty;
	}
	public List<HolderAccountHelperResultTO> getResultList() {
		return resultList;
	}
	public void setResultList(List<HolderAccountHelperResultTO> resultList) {
		this.resultList = resultList;
	}
}
