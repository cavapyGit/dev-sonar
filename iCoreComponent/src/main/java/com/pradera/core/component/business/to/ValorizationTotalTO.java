package com.pradera.core.component.business.to;

import java.io.Serializable;
import java.math.BigDecimal;

public class ValorizationTotalTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	String currency;
	BigDecimal totalBalance;
	BigDecimal availableBalance;
	

	public ValorizationTotalTO() {
		// TODO Auto-generated constructor stub
	}


	public String getCurrency() {
		return currency;
	}


	public void setCurrency(String currency) {
		this.currency = currency;
	}


	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}


	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}


	public BigDecimal getTotalBalance() {
		return totalBalance;
	}


	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}
	
	

}
