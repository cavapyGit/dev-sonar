package com.pradera.core.component.generalparameter.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.beanio.BeanIOConfigurationException;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericPropertiesConstants;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.to.SecuritiesSearcherTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.common.validation.to.ResponseDefaultTO;
import com.pradera.integration.common.validation.to.ValidationOperationType;
import com.pradera.integration.common.validation.to.ValidationProcessTO;
import com.pradera.integration.component.funds.to.AutomaticSendType;
import com.pradera.integration.component.generalparameters.to.MoneyExchangeTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.EconomicActivityType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.corporatives.TaxHolderRate;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.EconomicActivity;
import com.pradera.model.generalparameter.FinancialIndicator;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.SocietyType;
import com.pradera.model.generalparameter.externalinterface.ExternalInterface;
import com.pradera.model.generalparameter.externalinterface.InterfaceResponseCode;
import com.pradera.model.generalparameter.externalinterface.InterfaceTransaction;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceStateType;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.InformationSourceType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.SecurityClassSetup;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.paymentagent.type.PaymentAgentStateType;
import com.pradera.model.valuatorprocess.SecurityClassValuator;
import com.pradera.model.valuatorprocess.ValuatorMarketfactConsolidat;
import com.pradera.model.valuatorprocess.ValuatorProcessClass;
import com.pradera.model.valuatorprocess.ValuatorProcessSetup;
import com.pradera.model.valuatorprocess.ValuatorTermRange;
import com.pradera.model.valuatorprocess.type.ValuatorMarkFactActiveType;
import com.edv.collection.economic.rigth.PaymentAgent;



/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ParameterServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 28/01/2013
 */
@Stateless
public class ParameterServiceBean extends CrudDaoServiceBean{
	
	/** The Constant log. */
	private static final Logger log = Logger.getLogger(ParameterServiceBean.class);
	
	/** The holder key. */
	public final String HOLDER_KEY = "idHolderPk";

	/** The holder account key. */
	public final String HOLDER_ACCOUNT_KEY = "idHolderAccountPk";

	/** The participant key. */
	public final String PARTICIPANT_KEY = "idParticipantPk";

	/** The security key. */
	public final String SECURITY_KEY = "idSecurityCodePk";

	/** The issuer key. */
	public final String ISSUER_KEY = "idIssuerPk";
	
	/** The PARTICIPANT RELATED ISSUER GROUP. */
	/** The Constant ID_EXPIRATION_SECURITIES_BY_ISSUER_REPORT. */
	private static final Long ID_EXPIRATION_SECURITIES_BY_ISSUER_REPORT = 64L;
	/** The Constant ID_HOLDER_RELATED_DATA__REPORT. */
	private static final Long ID_HOLDER_RELATED_DATA__REPORT = 28L;
	/** The Constant ID_BALANCE_TRANSFER_AVAILABLE_REPORT. */
	private static final Long ID_BALANCE_TRANSFER_AVAILABLE_REPORT = 123L;
	/** The Constant ID_REQUESTS_ACCRE_CERTIF_HOLDER_REPORT. */
	private static final Long ID_REQUESTS_ACCRE_CERTIF_HOLDER_REPORT = 40L;
	/** The Constant ID_ENTRIES_VOLUNTARY_ACCOUNT_ISSUER_REPORT. */
	private static final Long ID_ENTRIES_VOLUNTARY_ACCOUNT_ISSUER_REPORT = 15L;
	
	/**
	 * Gets the list parameter table filtered only by master table fk and state registered.
	 *
	 * @param masterTableFk the master table fk
	 * @return the list parameter table service bean
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getListParameterTableServiceBean(Integer masterTableFk) throws ServiceException	{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(masterTableFk);
		
		return getListParameterTableServiceBean(parameterTableTO);
	}
	
	/**
	 * Gets the list parameter table service bean.
	 *
	 * @param filter the filter
	 * @return the list parameter table service bean
	 */
	public List<ParameterTable> getListParameterTableServiceBean(ParameterTableTO filter)	{
		
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("Select new ParameterTable ( P.masterTable.masterTablePk, " +//0
							  "P.parameterTablePk," +//1
							  "P.description," +//2
							  "P.parameterName," +//3
							  "P.parameterState," +//4
							  "P.indicator2, " +//5
							  "P.indicator1, " +//6
							  "P.text1, " +//7
							  "P.text2, " +//8
							  "P.text3, " +//10
							  "P.observation, " +//11
							  "P.elementSubclasification1, " +//12
							  "P.indicator4, " +//13
							  "P.indicator5, " +//14
							  "P.indicator6, " +//15
							  "P.shortInteger, " +//16
							  "P.longInteger) " +//17
						"From ParameterTable P Where 1 = 1 ");

		if(Validations.validateIsNotNull(filter.getParameterTablePk())){
			sbQuery.append(" And P.parameterTablePk = :parameterTablePkPrm");
			parameters.put("parameterTablePkPrm", filter.getParameterTablePk());
		}
		if(Validations.validateIsNotNull(filter.getState())){
			sbQuery.append(" And P.parameterState = :statePrm");
			parameters.put("statePrm", filter.getState());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getLstParameterTablePk())){
			sbQuery.append(" And P.parameterTablePk in :lstParameterTablePkPrm");
			parameters.put("lstParameterTablePkPrm", filter.getLstParameterTablePk());
		}
		if(Validations.validateListIsNotNullAndNotEmpty(filter.getLstParameterTablePkNotIn())){
			sbQuery.append(" And P.parameterTablePk not in :lstParameterTablePkNotInPrm");
			parameters.put("lstParameterTablePkNotInPrm", filter.getLstParameterTablePkNotIn());
		}
		if(Validations.validateIsNotNull(filter.getMasterTableFk())){
			sbQuery.append(" And P.masterTable.masterTablePk = :masterTableFkPrm");
			parameters.put("masterTableFkPrm", filter.getMasterTableFk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getLstMasterTableFk())){
			sbQuery.append(" And P.masterTable.masterTablePk in :lstMasterTableFkPrm");
			parameters.put("lstMasterTableFkPrm", filter.getLstMasterTableFk());
		}
		if(Validations.validateIsNotNull(filter.getParameterTableCd())){
			sbQuery.append(" And P.parameterTableCd = :parameterTableCdPrm");
			parameters.put("parameterTableCdPrm", filter.getParameterTableCd());
		}
		if(Validations.validateIsNotNull(filter.getIdRelatedParameterFk())){
			sbQuery.append(" And P.relatedParameter.parameterTablePk = :relatedParameterPrm");
			parameters.put("relatedParameterPrm", filter.getIdRelatedParameterFk());
		}
		if(Validations.validateIsNotNull(filter.getIndicator1())){
			sbQuery.append(" And P.indicator1 = :indicatorOne");
			parameters.put("indicatorOne", filter.getIndicator1());
		}
		if(Validations.validateIsNotNull(filter.getIndicator2())){
			sbQuery.append(" And P.indicator2= :indicicatorTwo");
			parameters.put("indicicatorTwo", filter.getIndicator2());
		}
		if(Validations.validateIsNotNull(filter.getShortInteger())){
			sbQuery.append(" And P.shortInteger= :shortInteger");
			parameters.put("shortInteger", filter.getShortInteger());
		}
		if(Validations.validateIsNotNull(filter.getLongInteger())){
			sbQuery.append(" And P.longInteger= :longInteger");
			parameters.put("longInteger", filter.getLongInteger());
		}
		if(Validations.validateIsNotNull(filter.getText1())){
			sbQuery.append(" And P.text1= :text1");
			parameters.put("text1", filter.getText1());
		}
		if(Validations.validateIsNotNull(filter.getText2())){
			sbQuery.append(" And P.text2= :text2");
			parameters.put("text2", filter.getText2());
		}
		if(filter.getIndicator4()!= null){
			sbQuery.append(" And P.indicator4= :indicatorFour");
			parameters.put("indicatorFour", filter.getIndicator4());
		}
		if(filter.getIndicator5()!= null){
			sbQuery.append(" And P.indicator5= :indicatorFive");
			parameters.put("indicatorFive", filter.getIndicator5());
		}
		if(filter.getIndicator6()!= null){
			sbQuery.append(" And P.indicator6= :indicatorSix");
			parameters.put("indicatorSix", filter.getIndicator6());
		}
		if(Validations.validateIsNotNull(filter.getElementSubclasification1())){
			sbQuery.append(" And P.elementSubclasification1 = :subclasification1");
			parameters.put("subclasification1", filter.getElementSubclasification1());
		}
		if(Validations.validateIsNotNull(filter.getElementSubclasification2())){
			sbQuery.append(" And P.elementSubclasification2 = :subclasification2");
			parameters.put("subclasification2", filter.getElementSubclasification2());
		}
		
		
		if(Validations.validateIsNotNullAndPositive(filter.getOrderbyParameterTableCd())){
			sbQuery.append(" order by p.parameterTableCd");
		}else{
			if(BooleanType.YES.getCode().equals(filter.getOrderByText1())){
				sbQuery.append(" order by p.text1");
			}else{
				sbQuery.append(" order by p.parameterName");
			}
		}
		Query query = this.em.createQuery(sbQuery.toString());
		for (Entry<String, Object> entry : parameters.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		query.setHint("org.hibernate.cacheable", true);
		return (List<ParameterTable>)query.getResultList();
		
	}
	
	/**
	 * Gets the combo parameter table.
	 *
	 * @param filter the filter
	 * @return the combo parameter table
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getComboParameterTable(ParameterTableTO filter) throws ServiceException	{
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select P.parameterTablePk," +//0
							  "P.description," +//1
							  "P.parameterName, " +//2
							  "P.text1 " +//3
						"From ParameterTable P ");
		sbQuery.append(" Where 1 = 1 ");

		if(Validations.validateIsNotNull(filter.getState()))
			sbQuery.append(" And P.parameterState = :statePrm");
		
		if(Validations.validateIsNotNull(filter.getParameterTablePk()))
			sbQuery.append(" And P.parameterTablePk = :parameterTablePkPrm");
		
		if(Validations.validateIsNotNull(filter.getLstParameterTablePk()) && filter.getLstParameterTablePk().size() > 0)
			sbQuery.append(" And P.parameterTablePk in :lstParameterTablePkPrm");
		
		if(Validations.validateListIsNotNullAndNotEmpty(filter.getLstParameterTablePkNotIn()))
			sbQuery.append(" And P.parameterTablePk not in :lstParameterTablePkNotInPrm");
		
		if(Validations.validateIsNotNull(filter.getMasterTableFk()))
			sbQuery.append(" And P.masterTable.masterTablePk = :masterTableFkPrm");
		
		if(Validations.validateIsNotNullAndPositive(filter.getOrderbyParameterTableCd())){
			sbQuery.append(" ORDER BY P.parameterTableCd");
		}else if(Validations.validateIsNotNullAndPositive(filter.getOrderbyParameterName())){
			sbQuery.append(" ORDER BY P.parameterName");
		}else if(Validations.validateIsNotNullAndPositive(filter.getOrderbyParameterDescription())){
			sbQuery.append(" ORDER BY P.description");
		}else if(Validations.validateIsNotNullAndPositive(filter.getOrderByText1())){
			sbQuery.append(" ORDER BY P.text1");
		}else{
			sbQuery.append(" ORDER BY P.parameterTableCd");
		}			
		
    	Query query = em.createQuery(sbQuery.toString());  

    	if(Validations.validateIsNotNull(filter.getState()))
        	query.setParameter("statePrm", filter.getState());
		
		if(Validations.validateIsNotNull(filter.getParameterTablePk()))
	    	query.setParameter("parameterTablePkPrm", filter.getParameterTablePk());
				
		if(Validations.validateIsNotNull(filter.getLstParameterTablePk()) && filter.getLstParameterTablePk().size() > 0)
			query.setParameter("lstParameterTablePkPrm", filter.getLstParameterTablePk());
				
		if(Validations.validateListIsNotNullAndNotEmpty(filter.getLstParameterTablePkNotIn()))
			query.setParameter("lstParameterTablePkNotInPrm", filter.getLstParameterTablePkNotIn());
		
		if(Validations.validateIsNotNull(filter.getMasterTableFk()))
	    	query.setParameter("masterTableFkPrm", filter.getMasterTableFk());
		
		List<ParameterTable> listParameterTable = new ArrayList<ParameterTable>();
		ParameterTable parameter = null;

		List<Object> objectList = query.getResultList();
		for (int i = 0; i < objectList.size(); i++) {
			Object[] obj =  (Object[]) objectList.get(i);
			parameter = new ParameterTable();
			parameter.setParameterTablePk((Integer)obj[0]);
			parameter.setDescription((String)obj[1]);
			parameter.setParameterName((String)obj[2]);
			parameter.setText1((String)obj[3]);
			listParameterTable.add(parameter);
		}
    	return listParameterTable;
	}
	
	/**
	 * Gets the parameter detail.
	 *
	 * @param idParameterTablePk the id parameter table pk
	 * @return the parameter detail
	 */
	public ParameterTable getParameterDetail(Integer idParameterTablePk){
		HashMap<String,Object> params = new HashMap<String, Object>();
		params.put("pk", idParameterTablePk);
		try {
			return findObjectByNamedQuery(ParameterTable.FIND_NAME_BY_PK, params);
		} catch (NoResultException e) {
			log.error("parameter table not found "+idParameterTablePk);
			return null;
		} catch (NonUniqueResultException e) {
			log.error("multiple parameter table found "+idParameterTablePk);
			return null;
		}
	}
	
	/**
	 * Gets the parameter table by id.
	 *
	 * @param parameterTablePk the parameter table pk
	 * @return the parameter table by id
	 * @throws ServiceException the service exception
	 */
	public ParameterTable getParameterTableById(Integer parameterTablePk) throws ServiceException	{
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select P.parameterTablePk," +//0
							  "P.description," +//1
							  "P.parameterName, " +//2
							  "P.indicator4, " +//3
							  "P.text1, " +//4
							  "P.text3, " +//
							  "P.indicator1 " +//
						"From ParameterTable P ");
		sbQuery.append(" Where P.parameterTablePk = :parameterTablePkPrm ");

    	Query query = em.createQuery(sbQuery.toString());  

	    query.setParameter("parameterTablePkPrm", parameterTablePk);

	    ParameterTable parameter = null;
		try {
			Object[] objectResult = (Object[])query.getSingleResult();
			
			parameter = new ParameterTable();
			parameter.setParameterTablePk((Integer)objectResult[0]);
			parameter.setDescription((String)objectResult[1]);
			parameter.setParameterName((String)objectResult[2]);
			parameter.setIndicator4(objectResult[3]!=null? (Integer)objectResult[3] : null);
			parameter.setText1( (String)objectResult[4] );
			parameter.setText3( (String)objectResult[5] );
			parameter.setIndicator1( (String)objectResult[6] );
		} catch (NoResultException e) {
			log.error("parameter not found "+parameterTablePk );
			e.printStackTrace();
		}
		
    	return parameter;
	}
	

	//LIST SECURITY TYPES BY INSTRUMENT TYPE
	/**
	 * Search security types by instrument.
	 *
	 * @param filter the filter
	 * @return the list
	 */
	public List<Object[]> searchSecurityTypesByInstrument(SecuritiesSearcherTO filter){
		StringBuilder sb = new StringBuilder();
		
		sb.append(	"	SELECT 						" +
					"		PT51.PARAMETER_TABLE_PK, PT51.PARAMETER_NAME " +
					"	FROM						" +
					"		PARAMETER_TABLE PT50, 	" +
					"		PARAMETER_TABLE PT51	" +
					"	WHERE						" );
		sb.append(	" 		PT51.MASTER_TABLE_FK = 	"+MasterTableType.SECURITIES_TYPE.getCode());
		sb.append(	"	AND							" +
					"		PT50.PARAMETER_TABLE_PK = PT51.ID_RELATED_PARAMETER_TABLE_FK");
		
		if(Validations.validateIsNotNullAndPositive(filter.getInstrumentType())){
			sb.append("	AND	PT50.PARAMETER_TABLE_PK = :instrumentType");
		}
		
		Query query = em.createNativeQuery(sb.toString());
		
		if(Validations.validateIsNotNullAndPositive(filter.getInstrumentType())){
			query.setParameter("instrumentType", filter.getInstrumentType());
		}
		
		return query.getResultList();
	}

	/**
	 * Gets the list parameter table exclude pk service bean.
	 *
	 * @param filter
	 *            the filter
	 * @return the list parameter table exclude pk service bean
	 * @throws ServiceException
	 *             the service exception
	 */
	public List<ParameterTable> getListParameterTableExcludePkServiceBean(ParameterTableTO filter) throws ServiceException {
		StringBuilder sb = new StringBuilder();
		List<ParameterTable> parameterTableList = new ArrayList<ParameterTable>();
		ParameterTable parameter;

		sb.append("	Select P.parameterTablePk, P.parameterName from ParameterTable P");
		sb.append(" Where 1 = 1 ");

		if (Validations.validateIsNotNullAndPositive(filter.getMasterTableFk())) {
			sb.append(" And P.masterTable.masterTablePk = :masterTableFk");
		}
		if (Validations.validateIsNotNullAndPositive(filter
				.getParameterTablePk())) {
			sb.append(" And P.parameterTablePk != :parameterTablePk");
		}

		Query query = em.createQuery(sb.toString());

		if (Validations.validateIsNotNullAndPositive(filter.getMasterTableFk())) {
			query.setParameter("masterTableFk", filter.getMasterTableFk());
		}
		if (Validations.validateIsNotNullAndPositive(filter
				.getParameterTablePk())) {
			query.setParameter("parameterTablePk", filter.getParameterTablePk());
		}

		List<Object> objectList = query.getResultList();

		for (int i = 0; i < objectList.size(); i++) {
			Object[] obj = (Object[]) objectList.get(i);

			parameter = new ParameterTable();

			parameter.setParameterTablePk((Integer) obj[0]);
			parameter.setParameterName((String) obj[1]);

			parameterTableList.add(parameter);
		}
		return parameterTableList;
	}

	/**
	 * Gets the tax holder rate.
	 *
	 * @return the tax holder rate
	 * @throws ServiceException
	 *             the service exception
	 */
	public List<TaxHolderRate> getTaxHolderRate() throws ServiceException {
		Query query = em.createQuery("SELECT thr From TaxHolderRate thr");
		List<TaxHolderRate> listTaxHoldeRate = (List<TaxHolderRate>) query.getResultList();
		return listTaxHoldeRate;
	}

	/**
	 * Find financial indicator service bean.
	 *
	 * @param indexAndRateFilter
	 *            the index and rate filter
	 * @return the financial indicator
	 * @throws ServiceException
	 *             the service exception
	 */
	public FinancialIndicator findFinancialIndicatorServiceBean(
			Map<String, Object> indexAndRateFilter) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select fi from FinancialIndicator fi ");
		sbQuery.append(" where  1=1 ");

		if (indexAndRateFilter.get("indicatorType") != null) {
			sbQuery.append("and fi.indicatorType= :indicatorType ");
		}
		if (indexAndRateFilter.get("indicatorCode") != null) {
			sbQuery.append("and fi.indicatorCode= :indicatorCode ");
		}

		if (indexAndRateFilter.get("periodicityType") != null) {
			sbQuery.append("and fi.periodicityType= :periodType ");
		}
		if (indexAndRateFilter.get("status") != null) {
			sbQuery.append("and fi.finantialState= :financialState ");
		}
		if (indexAndRateFilter.get("status") != null) {
			sbQuery.append("and fi.finantialState= :financialState ");
		}
		if (indexAndRateFilter.get("currentDate") != null) {
			sbQuery.append("and trunc(:currentDate) between trunc(fi.initialDate) and trunc(fi.endDate) ");
		}

		TypedQuery<FinancialIndicator> query = em.createQuery(
				sbQuery.toString(), FinancialIndicator.class);

		if (indexAndRateFilter.get("indicatorType") != null) {
			query.setParameter("indicatorType",
					indexAndRateFilter.get("indicatorType"));
		}
		if (indexAndRateFilter.get("indicatorCode") != null) {
			query.setParameter("indicatorCode",
					indexAndRateFilter.get("indicatorCode"));
		}
		if (indexAndRateFilter.get("periodicityType") != null) {
			query.setParameter("periodType",
					indexAndRateFilter.get("periodicityType"));
		}
		if (indexAndRateFilter.get("status") != null) {
			query.setParameter("financialState",
					indexAndRateFilter.get("status"));
		}
		if (indexAndRateFilter.get("currentDate") != null) {
			query.setParameter("currentDate",
					indexAndRateFilter.get("currentDate"));
		}
		FinancialIndicator financialInd = null;
		try {
			financialInd = query.getSingleResult();
		} catch (NoResultException nre) {
			throw new ServiceException(
					ErrorServiceType.FINANCIAL_INDICATOR_NOT_FOUND);
		} catch (NonUniqueResultException nue) {
			throw new ServiceException(
					ErrorServiceType.FINANCIAL_INDICATOR_NOT_UNIQUE_RES);
		}
		return financialInd;
	}

	/**
	 * Gets the guarantees margin factor param.
	 *
	 * @return the guarantees margin factor param
	 */
	public BigDecimal getGuaranteesMarginFactorParam() {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" select nvl(pt.decimalAmount1,0) from  ");
		sbQuery.append(" ParameterTable pt ");
		sbQuery.append(" where pt.masterTable.masterTablePk = :idMasterTablePk ");

		parameters.put("idMasterTablePk",MasterTableType.MASTER_TABLE_MARGIN_FACTOR.getCode());
		Double factor;
		try {
			factor = (Double) findObjectByQueryString(sbQuery.toString(),parameters);
		} catch (NoResultException e) {
			log.error("no se encontro factor de margen");
			factor = 0d;
		}
		return new BigDecimal(factor);
	}

	/**
	 * Find parameter table description.
	 *
	 * @param parameterTablePk
	 *            the parameter table pk
	 * @return the string
	 * @throws ServiceException
	 *             the service exception
	 */
	public String findParameterTableDescription(Integer parameterTablePk) throws ServiceException {
		Query query = em.createNamedQuery(ParameterTable.FIND_NAME_BY_PK);
		query.setParameter("pk", parameterTablePk);
		return ((ParameterTable) query.getSingleResult()).getParameterName();
	}

	/**
	 * Find parameters from id.
	 *
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ParameterTable> findParametersFromId(List<Integer> parameters) throws ServiceException {
		StringBuilder querySql = new StringBuilder();
		querySql.append("Select new com.pradera.model.generalparameter.ParameterTable(par.parameterTablePk, par.parameterName) ");
		querySql.append("From ParameterTable par ");
		querySql.append("Where par.parameterTablePk IN (:parametersId)");
		Query query = em.createQuery(querySql.toString());
		query.setParameter("parametersId", parameters);
		try {
			return query.getResultList();
		} catch (NoResultException ex) {
			return new ArrayList<ParameterTable>();
		}
	}

	/**
	 * Gets the depositary setup.
	 *
	 * @param setupPk the setup pk
	 * @return the depositary setup
	 */
	public IdepositarySetup getDepositarySetup(Long setupPk) {
		IdepositarySetup setup = em.find(IdepositarySetup.class, setupPk);
		setup.setParticipantByIssuerCode(getParticipantForIssuer());
		return setup;
	}
	
	/**
	 * Gets the daily exchange rates service bean.
	 *
	 * @param dateRate the date rate type
	 * @param currency the currency type
	 * @return the daily exchange rates service bean
	 */
	public DailyExchangeRates getDayExchangeRate(Date dateRate , Integer currency) {
		String sbquery = "SELECT edr FROM DailyExchangeRates edr WHERE edr.idCurrency = :currency and edr.dateRate = :dateRate and edr.idSourceinformation = :srcInfo ";
		Query query = em.createQuery(sbquery.toString());
		
		query.setParameter("currency", currency);
		query.setParameter("dateRate", dateRate);
		query.setParameter("srcInfo", InformationSourceType.BCRD.getCode());
		
		try{
			return (DailyExchangeRates) query.getSingleResult();
		}catch(NoResultException ex){
			log.error("no daily exchange found for date: " + dateRate +"; currency: "+currency);
			return null;
		}catch(NonUniqueResultException nex){
			log.error("multiple daily eschange found for date: " + dateRate +"; currency: "+currency); 
			return null;
		}
	}
	
	
	/**
	 * Gets the day exchanges rates.
	 *
	 * @param dateRate the date rate
	 * @return the day exchanges rates
	 */
	@SuppressWarnings("unchecked")
	public Map<Integer,BigDecimal> getDayExchangesRates(Date dateRate){
		
		//tipo de cambio VENTA
//		Map<String,Object> parameters = new HashMap<String, Object>();
		String querySql = "SELECT edr FROM DailyExchangeRates edr WHERE edr.dateRate = :dateRate and edr.idSourceinformation = :srcInfo ";
		Query query = em.createQuery(querySql.toString());
		query.setParameter("dateRate", dateRate);
		query.setParameter("srcInfo", InformationSourceType.BCRD.getCode());
		
		List<DailyExchangeRates> list = query.getResultList();
		BigDecimal usd = null;
		BigDecimal ufv = null;
		BigDecimal eu = null;
		BigDecimal dmv = null;
		
		
			for (DailyExchangeRates lista :list){
				if (lista.getIdCurrency().equals(CurrencyType.USD.getCode())){
					usd = lista.getBuyPrice();
				}
				if (lista.getIdCurrency().equals(CurrencyType.UFV.getCode())){
					ufv = lista.getBuyPrice();
				}
				if (lista.getIdCurrency().equals(CurrencyType.EU.getCode())){
					eu = lista.getBuyPrice();
				}
				if (lista.getIdCurrency().equals(CurrencyType.DMV.getCode())){
					dmv = lista.getBuyPrice();
				}
			}
			if (usd==null){
				DailyExchangeRates exchangeRates = exchangeRates(dateRate, CurrencyType.USD.getCode());
				if (exchangeRates!=null){
					usd=exchangeRates.getBuyPrice();
				}else{
					usd= new BigDecimal(1);
				}
			}
			
			if (ufv==null){
				DailyExchangeRates exchangeRates = exchangeRates(dateRate, CurrencyType.UFV.getCode());
				if (exchangeRates!=null){
					ufv=exchangeRates.getBuyPrice();
				}else{
					ufv = new BigDecimal(1);
				}
			}
			
			if (eu==null){
				DailyExchangeRates exchangeRates = exchangeRates(dateRate, CurrencyType.EU.getCode());
				if (exchangeRates!=null){
					eu=exchangeRates.getBuyPrice();
				}else{
					eu = new BigDecimal(1);
				}
			}
			if (dmv==null){
				DailyExchangeRates exchangeRates = exchangeRates(dateRate, CurrencyType.DMV.getCode());
				if (exchangeRates!=null){
					dmv=exchangeRates.getBuyPrice();
				}else{
					dmv = new BigDecimal(1);
				}
			}
			
			Map<Integer,BigDecimal> resultExchange = new HashMap<Integer, BigDecimal>();
			resultExchange.put(CurrencyType.USD.getCode(), usd);
			resultExchange.put(CurrencyType.UFV.getCode(), ufv);
			resultExchange.put(CurrencyType.EU.getCode(), eu);
			resultExchange.put(CurrencyType.DMV.getCode(), dmv);
			
			return resultExchange;
	}
	
	/**
	 * Exchange rates.
	 *
	 * @param fecha the fecha
	 * @param currency the currency
	 * @return the daily exchange rates
	 */
	private DailyExchangeRates exchangeRates(Date fecha, Integer currency){
		
		Map<String,Object> parameters = new HashMap<String, Object>();
		String querySql = " SELECT edr FROM DailyExchangeRates edr "
				+ " WHERE trunc(edr.dateRate) = (select trunc(max(edr2.dateRate)) from DailyExchangeRates edr2  where edr2.dateRate < :dateRate and edr2.idCurrency = :currency) "
				+ " and edr.idSourceinformation = :srcInfo "
				+ " and edr.idCurrency = :currency ";
		parameters.put("dateRate", fecha);
		parameters.put("currency", currency);
		parameters.put("srcInfo", InformationSourceType.BCRD.getCode());
		
		return (DailyExchangeRates)findObjectByQueryString(querySql.toString(), parameters);
		
	}

	/**
	 * Balance with market fact quantity.
	 *
	 * @param balancesID the balances id
	 * @return the map
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Integer> balanceWithMarketFactQuantity(List<com.pradera.integration.component.business.to.HolderAccountBalanceTO> balancesID)
			throws ServiceException {

		List<String> balancesIDFormated = new ArrayList<String>();
		for (com.pradera.integration.component.business.to.HolderAccountBalanceTO hab : balancesID) {
			balancesIDFormated.add(new StringBuilder()
					.append(hab.getIdParticipant())
					.append(hab.getIdHolderAccount())
					.append(hab.getIdSecurityCode()).toString());
		}

		StringBuilder stringSql = new StringBuilder();
		stringSql.append("SELECT	");
		stringSql
				.append("hab.id.idParticipantPk, hab.id.idHolderAccountPk, hab.id.idSecurityCodePk, COUNT(hmb) as tam	");// 0,1,2,3
		stringSql.append("FROM HolderAccountBalance hab	");
		stringSql.append("LEFT OUTER JOIN hab.holderMarketfactBalance hmb		");
		stringSql
				.append("WHERE (hab.id.idParticipantPk || hab.id.idHolderAccountPk || hab.id.idSecurityCodePk) ");
		stringSql.append("IN (:balancesId)	");
		stringSql
				.append("Group By hab.id.idParticipantPk, hab.id.idHolderAccountPk, hab.id.idSecurityCodePk");

		Query query = em.createQuery(stringSql.toString());
		query.setParameter("balancesId", balancesIDFormated);

		List<Object[]> dataList = query.getResultList();
		Map<String, Integer> mapMarketFactResult = new HashMap<String, Integer>();

		for (Object[] data : dataList) {
			StringBuilder balanceKey = new StringBuilder();
			balanceKey.append((Long) data[0]);
			balanceKey.append((Long) data[1]);
			balanceKey.append((String) data[2]);
			mapMarketFactResult.put(balanceKey.toString(),
					new Integer(data[3].toString()));
		}
		return mapMarketFactResult;
	}
	
//	@SuppressWarnings({ "rawtypes", "unchecked" })
	/**
 * Gets the valuator process setup.
 *
 * @return the valuator process setup
 */
public ValuatorProcessSetup getValuatorProcessSetup(){
		ValuatorProcessSetup valuatorSetup = em.find(ValuatorProcessSetup.class, 1L);
		valuatorSetup.setParameters(loadValuatorParameters(MasterTableType.CURRENCY.getCode()));
		valuatorSetup.setSecurityClassValuators(getSecurityClassValuator());
		valuatorSetup.setValuatorProcessClass(getValuatorProcessClass());
		valuatorSetup.setValuatorTermRange(getValuatorTermRange());
		valuatorSetup.setMarketFactConsolidat(getMarketFactConsolidat(null,null,null));
//		valuatorSetup.setSecurityResultPriceMap((ConcurrentHashMap)getMarketPriceResults(CommonsUtilities.currentDate()));
		valuatorSetup.setTheoricMarketPriceMap(new ConcurrentHashMap<String,Map<Object,BigDecimal>>());
//		valuatorSetup.setSecuritiesRatePriceTmp(new ArrayBlockingQueue<>(200000));
		return valuatorSetup;
	}
	
	/**
	 * Load valuator parameters.
	 *
	 * @param masterTable the master table
	 * @return the map
	 */
	@SuppressWarnings("unchecked")
	private Map<Integer,String> loadValuatorParameters(Integer... masterTable){
		Map<Integer,String> parameters = new HashMap<Integer,String>();
		StringBuilder sb = new StringBuilder();
		sb.append("	Select P.parameterTablePk, P.indicator3 		 ");
		sb.append("	From ParameterTable P							 ");
		sb.append(" Where P.masterTable.masterTablePk = :masterTable ");
		Query query = em.createQuery(sb.toString());
		for(Integer master: masterTable){
			query.setParameter("masterTable", master);
		}
		
		List<Object[]> parameterData = query.getResultList();
		for(Object[] data: parameterData){
			parameters.put((Integer)data[0], (String)data[1]);
		}
		return parameters;
	}
	
	/**
	 * Gets the list securities class setup.
	 *
	 * @param map the map
	 * @return the list securities class setup
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getListSecuritiesClassSetup(Map<String,Object> map) throws ServiceException{
		List<ParameterTable> lstSecuritiesClassPrm=null;
		List<SecurityClassSetup> lstSecuritiesClassSetup=null;
		List<ParameterTable> lstEconomicActivityPrmAux=new ArrayList<ParameterTable>();
		
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		filterParameterTable.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		filterParameterTable.setOrderbyParameterTableCd(BooleanType.YES.getCode());
	//	filterParameterTable.setShortInteger( Integer.parseInt( map.get("instrumentType").toString() ) );
	
		lstSecuritiesClassPrm= getListParameterTableServiceBean(filterParameterTable);
	//	map.remove("instrumentType");
		
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT s FROM SecurityClassSetup s ");
		sb.append(" where 1=1 ");
		if(map.get("instrumentType")!=null){
			sb.append(" and s.instrumentType = :instrumentType ");
		}
		if(map.get("economicSectorPrm")!=null){
			sb.append(" and s.economicSector = :economicSectorPrm ");
		}
		if(map.get("offerType")!=null){
			sb.append(" and s.offerType=:offerType ");
		}
		Query query = this.em.createQuery(sb.toString());
		for (Entry<String, Object> entry : map.entrySet()) {
			if(entry.getValue()!=null){
				query.setParameter(entry.getKey(), entry.getValue());
			}
		}
		lstSecuritiesClassSetup=query.getResultList();
		
		for(ParameterTable pTable : lstSecuritiesClassPrm){
			for(SecurityClassSetup securityClassSetup : lstSecuritiesClassSetup){
				if(securityClassSetup.getSecurityClass().compareTo( pTable.getParameterTablePk() )==0){
					lstEconomicActivityPrmAux.add(pTable);
					break;
				}
			}
		}
		return lstEconomicActivityPrmAux;
	}
	
	/**
	 * Gets the market price results.
	 *
	 * @param processDate the process date
	 * @return the market price results
	 */
	public Object getMarketPriceResults(Date processDate){
		StringBuilder sb = new StringBuilder();
		/*sb.append("	Select sec.idSecurityCodePk, pric.marketRate, pric.marketPrice, 	");
		sb.append("		   mech.idMechanismOperationPk, pric.marketDate					");
		sb.append("	From ValuatorPriceResult pric		 								");
		sb.append("	Inner Join pric.security sec 		 								");
		sb.append("	Left Join pric.mechanismOperation mech								");
		sb.append("	Where Trunc(pric.processDate) = Trunc(:processDate)					");
		sb.append("	Order By sec.idSecurityCodePk Desc									");
		Query query = em.createQuery(sb.toString());
		query.setParameter("processDate", processDate);*/
				
		// query nativo con el uso del HINT   
		sb.append(" select /*+ USE_NL(mo,s) ORDERED */  s.ID_SECURITY_CODE_PK , ");
		sb.append(" vpr.MARKET_RATE , ");
		sb.append(" vpr.MARKET_PRICE , ");
		sb.append(" mo.ID_MECHANISM_OPERATION_PK , ");
		sb.append(" vpr.MARKET_DATE ");
		sb.append(" from VALUATOR_PRICE_RESULT vpr ");
		sb.append(" inner join SECURITY s on vpr.ID_SECURITY_CODE_FK = s.ID_SECURITY_CODE_PK ");
		sb.append(" left outer join MECHANISM_OPERATION mo on vpr.ID_MECHANISM_OPERATION_FK = mo.ID_MECHANISM_OPERATION_PK ");
		sb.append(" WHERE vpr.PROCESS_DATE >= TRUNC(TO_DATE(trunc(:processDate)), 'DD') ");
		sb.append(" AND vpr.PROCESS_DATE < TRUNC(TO_DATE(trunc(:processDate)), 'DD') + 1 ");
		sb.append(" AND TRUNC(TO_DATE(trunc(:processDate)), 'DD') = trunc(:processDate) ");
		sb.append(" AND vpr.ID_PRICE_RESULT_PK >= -1E20 ");
		sb.append(" order by s.ID_SECURITY_CODE_PK Desc ");
		Query query = em.createNativeQuery(sb.toString());
		query.setParameter("processDate", processDate);
		
		return query.getResultList();
	}
	
	/**
	 * Gets the valuator process class.
	 *
	 * @return the valuator process class
	 */
	@SuppressWarnings("unchecked")
	public Map<String,ValuatorProcessClass> getValuatorProcessClass(){
		StringBuilder sb = new StringBuilder();
		sb.append("	Select val 									");
		sb.append("	From ValuatorProcessClass val 				");
		sb.append("	Inner Join Fetch val.securityClassValuators	");
		
		List<ValuatorProcessClass> processClassLst = findListByQueryString(sb.toString());
		
		Map<String,ValuatorProcessClass> valProcessClassMap = new HashMap<>();
		for(ValuatorProcessClass processClass: processClassLst){
			valProcessClassMap.put(processClass.getIdValuatorClassPk(), processClass);
		}
		return valProcessClassMap;
	}
	
	/**
	 * Gets the security class valuator.
	 *
	 * @return the security class valuator
	 */
	@SuppressWarnings("unchecked")
	public List<SecurityClassValuator> getSecurityClassValuator(){
		StringBuilder sb = new StringBuilder();
		sb.append("	Select val ");
		sb.append("	From SecurityClassValuator val ");
		return findListByQueryString(sb.toString());
	}
	
	/**
	 * Gets the valuator term range.
	 *
	 * @return the valuator term range
	 */
	@SuppressWarnings("unchecked")
	public List<ValuatorTermRange> getValuatorTermRange(){
		StringBuilder sb = new StringBuilder();
		sb.append("	Select term ");
		sb.append("	From ValuatorTermRange term ");
		return findListByQueryString(sb.toString());
	}
	
	/**
	 * *
	 * Method to get MarketFactConsolidate on HashMap by ValuatorCode.
	 *
	 * @param marketFactType the market fact type
	 * @param marketFactActive the market fact active
	 * @param valuatorDate the valuator date
	 * @return the market fact consolidat
	 */
	@SuppressWarnings("unchecked")
	public ConcurrentHashMap<String,List<ValuatorMarketfactConsolidat>> getMarketFactConsolidat(List<Integer> marketFactType, List<String> marketFactActive, 
																								Date valuatorDate) {		
		if(valuatorDate == null){
			valuatorDate = CommonsUtilities.currentDate();
		}
		
		StringBuilder selectSQL = new StringBuilder();
		StringBuilder whereSQL = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String,Object>();
		
		selectSQL.append("SELECT cnsld											");
		selectSQL.append("FROM ValuatorMarketfactConsolidat cnsld				");
		selectSQL.append("WHERE TRUNC(informationDate)  = TRUNC(:valuatorDate)	");
		parameters.put("valuatorDate", valuatorDate);
		
		if(marketFactType!=null && !marketFactType.isEmpty()){
			whereSQL.append("AND cnsld.marketfactActive  IN (:marketFactType)	");
			parameters.put("marketFactType", marketFactType);
		}
		if(marketFactActive!=null && !marketFactActive.isEmpty()){
			whereSQL.append("AND cnsld.marketfactActive  IN (:marketFactActive)	");
		}
		
		whereSQL.append("AND (cnsld.marketfactActive  = :ac Or 					");
		whereSQL.append("	  cnsld.marketfactActive  is null 					)");
		parameters.put("ac", ValuatorMarkFactActiveType.AC.getCode());
		
		
		List<ValuatorMarketfactConsolidat> markFactConsolidates = null;
		ConcurrentHashMap<String,List<ValuatorMarketfactConsolidat>> markFactConsolidatMap = new ConcurrentHashMap<String,List<ValuatorMarketfactConsolidat>>();
		try{
			markFactConsolidates = (List<ValuatorMarketfactConsolidat>) findListByQueryString(selectSQL.toString().concat(whereSQL.toString()), parameters);
			
			if(markFactConsolidates!=null){
				for(ValuatorMarketfactConsolidat markFactConsolidate: markFactConsolidates){
					String valuatorCod = markFactConsolidate.getValuatorCode();
					markFactConsolidatMap.put(valuatorCod, getMarketFactByValuatorCode(valuatorCod, markFactConsolidates));
				}
			}
		}catch(NoResultException ex){
			return null;
		}
		return markFactConsolidatMap;
	}

	/**
	 * *
	 * Method to get ValuatorMarketFact by ValuatorCode.
	 *
	 * @param valuatorCode the valuator code
	 * @param markFactList the mark fact list
	 * @return the market fact by valuator code
	 */
	private List<ValuatorMarketfactConsolidat> getMarketFactByValuatorCode(String valuatorCode, List<ValuatorMarketfactConsolidat> markFactList){
		List<ValuatorMarketfactConsolidat> marketFactByValuatorCode = new ArrayList<ValuatorMarketfactConsolidat>();
		for (ValuatorMarketfactConsolidat markFactObject : markFactList) {
			if (valuatorCode.equals(markFactObject.getValuatorCode())) {
				marketFactByValuatorCode.add(markFactObject);
			}
		}
		return marketFactByValuatorCode;
	}
	
	/**
	 * Gets the list economic activity by sector.
	 *
	 * @param economicSector the economic sector
	 * @param economicActivityNum the economic activity num
	 * @param economicActivity the economic activity
	 * @return the list economic activity by sector
	 */
	public List<ParameterTable> getListEconomicActivityBySector(Integer economicSector, Integer economicActivityNum, Integer economicActivity) {
		List<ParameterTable> lstEconomicActivityPrm=null;
		List<EconomicActivity> lstEconomicActivityTbl=null;
		List<ParameterTable> lstEconomicActivityPrmAux=new ArrayList<ParameterTable>();
		
		Map<String,Object> param=new HashMap<String,Object>();
		param.put("economicSectorPrm", economicSector);
		
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		filterParameterTable.setMasterTableFk(MasterTableType.ECONOMIC_ACTIVITY.getCode());
		filterParameterTable.setOrderbyParameterTableCd(BooleanType.YES.getCode());
		if(economicActivityNum!=null){
			filterParameterTable.setIndicator6(economicActivityNum);
		}
		if(economicActivity!=null){
			filterParameterTable.setParameterTablePk(economicActivity);
		}
		lstEconomicActivityPrm = getListParameterTableServiceBean(filterParameterTable);
		
		lstEconomicActivityTbl=(List<EconomicActivity>)findWithNamedQuery(EconomicActivity.FIND_BY_SECTOR, param);
		
		/*
		for(EconomicActivity activityTbl : lstEconomicActivityTbl){
			for(ParameterTable activityPrm : lstEconomicActivityPrm){
				if(activityPrm.getParameterTablePk().compareTo( activityTbl.getEconomicActivity() )==0){
					lstEconomicActivityPrmAux.add( activityPrm );
					break;
				}
			}
		}*/
		
		for(ParameterTable activity : lstEconomicActivityPrm) {
			if(Validations.validateIsNotNullAndNotEmpty(activity.getIndicator5()) && activity.getIndicator5() == 1){
				Integer indicador = lstEconomicActivityPrm.indexOf(activity);
				lstEconomicActivityPrmAux.add(lstEconomicActivityPrm.get(indicador));
			}
		}
		
		return lstEconomicActivityPrmAux;
	}
	
	/**
	 * Gets the list society type by sector.
	 *
	 * @param economicSector the economic sector
	 * @return the list society type by sector
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getListSocietyTypeBySector(Integer economicSector) throws ServiceException{
		List<ParameterTable> lstSocietyTypePrm=null;
		List<SocietyType> lstSocietyTypeTbl=null;
		List<ParameterTable> lstSocietyTypePrmAux=new ArrayList<ParameterTable>();
		
		Map<String,Object> param=new HashMap<String,Object>();
		param.put("economicSectorPrm", economicSector);
		
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		filterParameterTable.setMasterTableFk(MasterTableType.SOCIETY_TYPE.getCode());
		filterParameterTable.setOrderbyParameterTableCd(BooleanType.YES.getCode());
		lstSocietyTypePrm = getListParameterTableServiceBean(filterParameterTable);
		
		lstSocietyTypeTbl=(List<SocietyType>)findWithNamedQuery(SocietyType.FIND_BY_SECTOR, param);
		
		for(SocietyType socType : lstSocietyTypeTbl){
			for(ParameterTable pTable : lstSocietyTypePrm){
				if(socType.getSocietyType().compareTo( pTable.getParameterTablePk()) == 0 ){
					lstSocietyTypePrmAux.add( pTable );
					break;
				}
			}
		}
		
		return lstSocietyTypePrmAux;
	}
	
	/**
	 * Validate entities.
	 *
	 * @param mpEntities
	 *            the mp entities
	 * @return the error service type
	 */
	public ErrorServiceType validateEntities(Map<String, Object> mpEntities) {
		Map<String, Object> params = new HashMap<String, Object>();

		if (Validations
				.validateIsNotNullAndNotEmpty(mpEntities.get(HOLDER_KEY))) {
			params.put("idHolderPkParam", (Long) mpEntities.get(HOLDER_KEY));
			Holder holder = findObjectByNamedQuery(
					Holder.HOLDER_STATE, params);
			if (!HolderStateType.REGISTERED.getCode().equals(
					holder.getStateHolder()))
				return ErrorServiceType.HOLDER_BLOCK;
		}

		if (Validations.validateIsNotNullAndNotEmpty(mpEntities
				.get(HOLDER_ACCOUNT_KEY))) {
			params = new HashMap<String, Object>();
			params.put("idHolderAccountPkParam",
					(Long) mpEntities.get(HOLDER_ACCOUNT_KEY));
			HolderAccount holderAccount = findObjectByNamedQuery(HolderAccount.HOLDER_ACCOUNT_STATE,
							params);
			if (!HolderAccountStatusType.ACTIVE.getCode().equals(
					holderAccount.getStateAccount()))
				return ErrorServiceType.HOLDER_ACCOUNT_BLOCK;
		}

		if (Validations.validateIsNotNullAndNotEmpty(mpEntities
				.get(PARTICIPANT_KEY))) {
			params = new HashMap<String, Object>();
			params.put("idParticipantPkParam",
					(Long) mpEntities.get(PARTICIPANT_KEY));
			Participant participant = findObjectByNamedQuery(Participant.PARTICIPANT_STATE,
							params);
			if (!ParticipantStateType.REGISTERED.getCode().equals(
					participant.getState()))
				return ErrorServiceType.PARTICIPANT_BLOCK;
		}

		if (Validations.validateIsNotNullAndNotEmpty(mpEntities
				.get(SECURITY_KEY))) {
			params = new HashMap<String, Object>();
			params.put("idSecurityCodePkParam", mpEntities.get(SECURITY_KEY)
					.toString());
			Security security = findObjectByNamedQuery(
					Security.SECURITY_STATE, params);
			if ( !( SecurityStateType.REGISTERED.getCode().equals(security.getStateSecurity()) || SecurityStateType.GUARDA_EXCLUSIVE.getCode().equals(security.getStateSecurity()) || SecurityStateType.GUARDA_EXCLUSIVE_EXPIRATION.getCode().equals(security.getStateSecurity()) ) )
				return ErrorServiceType.SECURITY_BLOCK;
		}

		if (Validations
				.validateIsNotNullAndNotEmpty(mpEntities.get(ISSUER_KEY))) {
			params = new HashMap<String, Object>();
			params.put("idIssuerPkParam", mpEntities.get(ISSUER_KEY).toString());
			Issuer issuer = findObjectByNamedQuery(
					Issuer.ISSUER_STATE, params);
			if (!IssuerStateType.REGISTERED.getCode().equals(
					issuer.getStateIssuer()))
				return ErrorServiceType.ISSUER_BLOCK;
		}
		return null;
	}

	/**
	 * Gets the interface response codes.
	 *
	 * @return the interface response codes
	 */
	public List<InterfaceResponseCode> getInterfaceResponseCodes() {
		return findWithNamedQuery(InterfaceResponseCode.FIND_ALL, new HashMap<String, Object>());
	}
	
	/**
	 * Gets the list interface transaction.
	 *
	 * @param idParticipant the id participant
	 * @param operationNumber the operation number
	 * @param interfaceCode the interface code
	 * @return the list interface transaction
	 */
	public List<InterfaceTransaction> getListInterfaceTransaction(Long idParticipant, Long operationNumber, String interfaceCode) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT IT ");
		stringBuffer.append(" FROM InterfaceTransaction IT ");
		stringBuffer.append(" WHERE IT.participant.idParticipantPk = :idParticipant ");
		stringBuffer.append(" and IT.operationNumber = :operationNumber ");
		stringBuffer.append(" and IT.interfaceProcess.idExternalInterfaceFk.interfaceName = :interfaceCode ");
		stringBuffer.append(" and IT.transactionState = :transactionState ");
		
		TypedQuery<InterfaceTransaction> typedQuery= em.createQuery(stringBuffer.toString(), InterfaceTransaction.class);
		typedQuery.setParameter("idParticipant", idParticipant);
		typedQuery.setParameter("operationNumber", operationNumber);
		typedQuery.setParameter("interfaceCode", interfaceCode);
		typedQuery.setParameter("transactionState", BooleanType.YES.getCode());
		
		return typedQuery.getResultList();
	}
	
	/**
	 * Gets the p rocessed files information.
	 *
	 * @param processDate the process date
	 * @param interfaceName the interface name
	 * @return the p rocessed files information
	 */
	@SuppressWarnings("unchecked")
	public List<ProcessFileTO> getPRocessedFilesInformation(Date processDate, String interfaceName) {
		
		StringBuilder sb = new StringBuilder();
		sb.append("	Select 	ir.idInterfaceProcessPk, ir.fileName, ei.description, ir.processDate, ");
		sb.append("			(select pt.parameterName from ParameterTable pt where pt.parameterTablePk = ir.processType), ");
		sb.append("			(select ptst.parameterName from ParameterTable ptst where ptst.parameterTablePk = ir.processState)  ");
		sb.append("	From InterfaceProcess ir inner join ir.idExternalInterfaceFk ei ");
		sb.append(" Where trunc(ir.registryDate) = :processDate ");
		sb.append(" and   ei.interfaceName = :interfaceName ");
		sb.append(" order by ir.processDate desc ");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("processDate", processDate);
		parameters.put("interfaceName", interfaceName);

		List<Object[]> resultList = findListByQueryString(sb.toString(), parameters);
		List<ProcessFileTO> processFiles = new ArrayList<ProcessFileTO>();
		for (Object[] objects : resultList) {
			ProcessFileTO processFileTO = new ProcessFileTO();
			processFileTO.setSequenceNumber((Long)objects[0]);
			processFileTO.setIdProcessFilePk((Long)objects[0]);
			processFileTO.setFileName((String) objects[1]);
			processFileTO.setDescription((String) objects[2]);
			processFileTO.setProcessDate((Date) objects[3]);
			processFileTO.setReceptionTypeDescription((String)objects[4]);
			processFileTO.setProcessStateDescription((String)objects[5]);
			processFiles.add(processFileTO);		
		}
		
		return processFiles;
	}

	/**
	 * Gets the interface uploaded file content.
	 *
	 * @param idInterfaceReceptionPk the id interface reception pk
	 * @return the interface uploaded file content
	 */
	public byte[] getInterfaceUploadedFileContent(Long idInterfaceReceptionPk) {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" select ir.fileUpload ");
		sbQuery.append(" from InterfaceProcess ir where ir.idInterfaceProcessPk = :idInterfaceProcess ");
		parameters.put("idInterfaceProcess", idInterfaceReceptionPk);
		return (byte[])findObjectByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Gets the interface response file content.
	 *
	 * @param idInterfaceReceptionPk the id interface reception pk
	 * @return the interface response file content
	 */
	public byte[] getInterfaceResponseFileContent(Long idInterfaceReceptionPk) {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" select ir.fileResponse ");
		sbQuery.append(" from InterfaceProcess ir where ir.idInterfaceProcessPk = :idInterfaceProcess ");
		parameters.put("idInterfaceProcess", idInterfaceReceptionPk);
		return (byte[])findObjectByQueryString(sbQuery.toString(), parameters);
	}

	/**
	 * Gets the external interface information.
	 *
	 * @param interfaceName the interface name
	 * @return the external interface information
	 */
	public ProcessFileTO getExternalInterfaceInformation(String interfaceName) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuilder sb = new StringBuilder();
		sb.append("	Select ei.idExtInterfacePk, ei.description, ei.schemaDirectory , ei.idBusinessProcessFk , ei.schemaResponseDirectory, ei.schemaRootTag, ei.interfaceName  "); 
		sb.append("	From ExternalInterface ei Where ei.interfaceName = :interfaceName ");
		
		parameters.put("interfaceName", interfaceName);
		
		Object[] interfaceInformation = (Object[]) findObjectByQueryString(sb.toString(), parameters);
		
		ProcessFileTO processFile = new ProcessFileTO();
		processFile.setIdExternalInterface(((Long)interfaceInformation[0]));
		processFile.setDescription((String)interfaceInformation[1]);
		processFile.setStreamFileDir((String)interfaceInformation[2]);
		processFile.setIdBusinessProcess((Long)interfaceInformation[3]);
		processFile.setStreamResponseFileDir((String)interfaceInformation[4]);
		processFile.setRootTag((String)interfaceInformation[5]);
		processFile.setInterfaceName((String)interfaceInformation[6]);
		
		return processFile;
	}
	
	
	/**
	 * Validate interface file.
	 *
	 * @param interfaceCode the interface code
	 * @param xmlFile the xml file
	 * @param fileName the file name
	 * @param boWsInterface the bo ws interface
	 * @return the process file to
	 * @throws ServiceException the service exception
	 * @throws BeanIOConfigurationException the bean io configuration exception
	 * @throws IllegalArgumentException the illegal argument exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ParserConfigurationException the parser configuration exception
	 */
	public ProcessFileTO validateInterfaceFile(String interfaceCode, String xmlFile, String fileName, boolean boWsInterface) 
												throws ServiceException, BeanIOConfigurationException, IllegalArgumentException, 
												IOException, ParserConfigurationException {
		ProcessFileTO objProcessFileTO= null;
		ExternalInterface objExternalInterface = getExternalInterface(interfaceCode, boWsInterface);
		if(Validations.validateIsNotNull(objExternalInterface)){	
			objProcessFileTO = new ProcessFileTO();
			objProcessFileTO.setDpfInterface(Boolean.TRUE);
			objProcessFileTO.setStreamFileDir(objExternalInterface.getSchemaDirectory());
			objProcessFileTO.setStreamResponseFileDir(objExternalInterface.getSchemaResponseDirectory());
			if (Validations.validateIsNotNull(xmlFile)) {
				objProcessFileTO.setProcessFile(xmlFile.getBytes());
			}								
			objProcessFileTO.setLocale(new Locale("es"));
			objProcessFileTO.setFileName(fileName);
			objProcessFileTO.setIndAutomaticProcess(GeneralConstants.TWO_VALUE_INTEGER);
			objProcessFileTO.setIdExternalInterface(objExternalInterface.getIdExtInterfacePk());
			objProcessFileTO.setInterfaceName(objExternalInterface.getInterfaceName());
			objProcessFileTO.setRootTag(objExternalInterface.getSchemaRootTag());			
			if (BooleanType.YES.getCode().equals(objExternalInterface.getIndInterfaceType())) {
				CommonsUtilities.validateFileOperation(objProcessFileTO);
				
				if(!ExternalInterfaceStateType.CONFIRMED.getCode().equals(objExternalInterface.getExternalInterfaceState())){
					RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(null, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_QUERY_SERVICE_STATUS_WEBSERVICES, null,
							new Object[]{ objExternalInterface.getDescription() });
					objProcessFileTO.getValidationProcessTO().getLstValidations().add(recordValidationType);
					objProcessFileTO.getValidationProcessTO().setLstOperations(new ArrayList<ValidationOperationType>());
				}
			}			
		}
		return objProcessFileTO;
	}
	
	/**
	 * Gets the external interface.
	 *
	 * @param strInterfaceName the str interface name
	 * @param blInterface the bl interface
	 * @return the external interface
	 */
	public ExternalInterface getExternalInterface(String strInterfaceName, boolean blInterface) {
		ExternalInterface objExternalInterface = null;
		StringBuffer stringBuffer= new StringBuffer();
		try {
			stringBuffer.append(" SELECT EXT FROM ExternalInterface EXT ");
			stringBuffer.append(" WHERE EXT.interfaceName = :parInterfaceName ");
			if(blInterface){
				stringBuffer.append(" and EXT.indAccessWebservice = :parIndicadorZero  ");
			} else {
				stringBuffer.append(" and (EXT.indAccessWebservice = :parIndicadorZero ");
				stringBuffer.append(" or EXT.indAccessWebservice = :parIndicadorOne ) ");
			}
			TypedQuery<ExternalInterface> query = em.createQuery(stringBuffer.toString(), ExternalInterface.class);
			query.setParameter("parInterfaceName", strInterfaceName);
			if(blInterface){
				query.setParameter("parIndicadorZero", GeneralConstants.ZERO_VALUE_INTEGER);				
			} else {
				query.setParameter("parIndicadorZero", GeneralConstants.ZERO_VALUE_INTEGER);
				query.setParameter("parIndicadorOne", GeneralConstants.ONE_VALUE_INTEGER);				
			}
			objExternalInterface = query.getSingleResult();
		} catch (NoResultException e) {
			log.error(("getExternalInterface"));
			log.debug(e.getMessage());
		}
		return objExternalInterface;		
	}
	
	/**
	 * Generate error validation process.
	 *
	 * @param validationOperationType the validation operation type
	 * @param ex the ex
	 * @return the validation process to
	 */
	public ValidationProcessTO generateErrorValidationProcess(ValidationOperationType validationOperationType, Exception ex) {
		ValidationProcessTO objValidationProcessTO= new ValidationProcessTO();
		ArrayList<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
		if (Validations.validateIsNull(validationOperationType)) {
			validationOperationType= new ValidationOperationType();
			validationOperationType.setLocale(new Locale("es"));
		}
		RecordValidationType recordError = null;
		
		if(ex instanceof ServiceException){
			ServiceException serviceException = (ServiceException)ex;
			if(serviceException.getRecordValidationType()!=null){
				recordError = serviceException.getRecordValidationType();
				recordError.setOperationRef(validationOperationType);
			}else{
				recordError = CommonsUtilities.populateRecordCodeValidation(validationOperationType, GenericPropertiesConstants.MSG_INTERFACE_UNDEFINED_ERROR, null);
				recordError.setErrorDescription(PropertiesUtilities.getMessage(
						GeneralConstants.PROPERTY_FILE_EXCEPTIONS,validationOperationType.getLocale(),serviceException.getErrorService().getMessage()));
			}
		}else{
			recordError = CommonsUtilities.populateRecordCodeValidation(validationOperationType, GenericPropertiesConstants.MSG_INTERFACE_UNDEFINED_ERROR, null);
		}
		
		lstRecordValidationTypes.add(recordError);
		objValidationProcessTO.setLstValidations(lstRecordValidationTypes);
		
		return objValidationProcessTO;
	}
	
	/**
	 * Verify response file.
	 *
	 * @param xmlResponse the xml response
	 * @param processFileTO the process file to
	 * @throws ServiceException the service exception
	 * @throws BeanIOConfigurationException the bean io configuration exception
	 * @throws IllegalArgumentException the illegal argument exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ParserConfigurationException the parser configuration exception
	 */
	public void verifyResponseFile(String xmlResponse, ProcessFileTO processFileTO) 
													throws ServiceException, BeanIOConfigurationException, 
													IllegalArgumentException, IOException, ParserConfigurationException {
		
		processFileTO.setStreamFileDir(processFileTO.getStreamResponseFileDir());
		processFileTO.setProcessFile(xmlResponse.getBytes());
		CommonsUtilities.validateFileOperationStruct(processFileTO);
		
		ValidationProcessTO objValidationProcessTO = processFileTO.getValidationProcessTO();
		if (Validations.validateIsNotNull(objValidationProcessTO)) {
			if (Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstOperations())) {
				ValidationOperationType validationOperationType= objValidationProcessTO.getLstOperations().get(0);
				ResponseDefaultTO responseDefaultTO = (ResponseDefaultTO) validationOperationType;
				if (!GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS_CODE.equals(responseDefaultTO.getErrorCode())) {
					throw new ServiceException(ErrorServiceType.WEB_SERVICE_ERROR_SENDING, GeneralConstants.ASTERISK_STRING + responseDefaultTO.getErrorDescription().toString());
				}
			} else {
				throw new ServiceException(ErrorServiceType.WEB_SERVICE_ERROR_RESPONSE_FILE);
			}
		} else {
			throw new ServiceException(ErrorServiceType.WEB_SERVICE_ERROR_RESPONSE_FILE);
		}
	}
	
	/**
	 * Gets the response file lip.
	 *
	 * @param xmlResponse the xml response
	 * @param processFileTO the process file to
	 * @return the response file lip
	 * @throws ServiceException the service exception
	 * @throws BeanIOConfigurationException the bean io configuration exception
	 * @throws IllegalArgumentException the illegal argument exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ParserConfigurationException the parser configuration exception
	 */
	public List<AutomaticSendType> getResponseFileLip(String xmlResponse, ProcessFileTO processFileTO) throws ServiceException, 
			BeanIOConfigurationException, IllegalArgumentException, IOException, ParserConfigurationException {
		List<AutomaticSendType> lstAutomaticSendType = null;
		AutomaticSendType automaticSendType = null;
		processFileTO.setStreamFileDir(processFileTO.getStreamResponseFileDir());
		processFileTO.setProcessFile(xmlResponse.getBytes());
		CommonsUtilities.validateFileOperationStruct(processFileTO);
		ValidationProcessTO objValidationProcessTO = processFileTO.getValidationProcessTO();
		if (Validations.validateIsNotNull(objValidationProcessTO)) {
			if (Validations.validateListIsNotNullAndNotEmpty(objValidationProcessTO.getLstOperations())) {
				lstAutomaticSendType = new ArrayList<AutomaticSendType>();
				for(ValidationOperationType objValidationOperationType : objValidationProcessTO.getLstOperations()){
					automaticSendType = (AutomaticSendType) objValidationOperationType;
					lstAutomaticSendType.add(automaticSendType);
				}				
			} else {
				throw new ServiceException(ErrorServiceType.WEB_SERVICE_ERROR_RESPONSE_FILE);
			}
		} else {
			throw new ServiceException(ErrorServiceType.WEB_SERVICE_ERROR_RESPONSE_FILE);
		}
		return lstAutomaticSendType;
	}
	
	/**
	 * Gets the response file lip.
	 *
	 * @param xmlResponse the xml response
	 * @param processFileTO the process file to
	 * @throws ServiceException the service exception
	 * @throws BeanIOConfigurationException the bean io configuration exception
	 * @throws IllegalArgumentException the illegal argument exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ParserConfigurationException the parser configuration exception
	 */
	public MoneyExchangeTO  getResponseExchangeMoney(String xmlResponse, ProcessFileTO processFileTO) throws ServiceException, 
			BeanIOConfigurationException, IllegalArgumentException, IOException, ParserConfigurationException {
		try {
			processFileTO.setStreamFileDir(processFileTO.getStreamResponseFileDir());
			processFileTO.setProcessFile(xmlResponse.getBytes());
			CommonsUtilities.validateFileOperationStruct(processFileTO);
			ValidationProcessTO objValidationProcessTO = processFileTO.getValidationProcessTO();
			MoneyExchangeTO moneyExchangeTO= new MoneyExchangeTO();
			moneyExchangeTO= (MoneyExchangeTO) objValidationProcessTO.getLstOperations().get(0);
			if(objValidationProcessTO.getLstOperations().size()==2){
				moneyExchangeTO= (MoneyExchangeTO) objValidationProcessTO.getLstOperations().get(1);
			}
			return moneyExchangeTO;
		} catch (Exception e) {
			return null;
		}
		
	}
	
	/**
	 * Gets the participant for issuer.
	 *
	 * @return the participant for issuer
	 */
	@SuppressWarnings("unchecked")
	public Map<String,Participant> getParticipantForIssuer() {
		Map<String,Participant> mapData = new HashMap<>();
		try {
			StringBuffer stringBuffer= new StringBuffer();
			stringBuffer.append(" SELECT ISS.idIssuerPk, PA.idParticipantPk,		");
			stringBuffer.append(" PA.state											");
			stringBuffer.append(" FROM Participant PA 								");
			stringBuffer.append(" INNER JOIN PA.issuer ISS 							");
			stringBuffer.append(" WHERE 1 = 1 										");
			stringBuffer.append(" and PA.economicActivity = :activityEconomicPart 	");
			stringBuffer.append(" and ISS.economicActivity = :activityEconomicIss 	");
			Query query = em.createQuery(stringBuffer.toString());
			query.setParameter("activityEconomicPart", EconomicActivityType.BANCOS.getCode());
			query.setParameter("activityEconomicIss", EconomicActivityType.BANCOS.getCode());
			
				
			for(Object[] data: ((List<Object[]>)query.getResultList())){
				mapData.put((String) data[0], new Participant((Long)data[1],(Integer)data[2]));
			}
			
		} catch(NoResultException ex){
			return null;
		} catch(NonUniqueResultException ex){
			return null;
		}
		return mapData;
	}
	
	/**
	 * Gets the participant for issuer dpf dpa.
	 *
	 * @param idIssuerDpfDpa the id issuer dpf dpa
	 * @return the participant for issuer dpf dpa
	 */
	public Long getParticipantForIssuerDpfDpa(String idIssuerDpfDpa) {
		Participant objParticipant= null;
		Long idParticipant = null;
		try {
			StringBuffer stringBuffer= new StringBuffer();
			stringBuffer.append(" SELECT PA FROM Participant PA ");
			stringBuffer.append(" INNER JOIN PA.issuer ISS ");
			stringBuffer.append(" WHERE ISS.idIssuerPk = :idIssuer ");
			stringBuffer.append(" and PA.economicActivity = :activityEconomicPart ");
			stringBuffer.append(" and ISS.economicActivity = :activityEconomicIss ");
			TypedQuery<Participant> typedQuery = em.createQuery(stringBuffer.toString(), Participant.class);
			typedQuery.setParameter("idIssuer", idIssuerDpfDpa);
			typedQuery.setParameter("activityEconomicPart", EconomicActivityType.BANCOS.getCode());
			typedQuery.setParameter("activityEconomicIss", EconomicActivityType.BANCOS.getCode());
			objParticipant = typedQuery.getSingleResult();
			idParticipant = objParticipant.getIdParticipantPk();
		} catch(NoResultException ex){
			return null;
		} catch(NonUniqueResultException ex){
			return null;
		}
		return idParticipant;
	}
	
	/**
	 * Checks if is report in part issuer group.
	 *
	 * @param reportId the report id
	 * @return the boolean
	 */
	public Boolean isReportInPartIssuerGroup(Long reportId){
		Boolean isInGroup = Boolean.FALSE;
		if(reportId.equals(ID_EXPIRATION_SECURITIES_BY_ISSUER_REPORT)
				|| reportId.equals(ID_BALANCE_TRANSFER_AVAILABLE_REPORT)
				|| reportId.equals(ID_HOLDER_RELATED_DATA__REPORT)
				|| reportId.equals(ID_REQUESTS_ACCRE_CERTIF_HOLDER_REPORT)
				|| reportId.equals(ID_ENTRIES_VOLUNTARY_ACCOUNT_ISSUER_REPORT)){
			isInGroup = Boolean.TRUE;
		}
		return isInGroup;
	}
	
	
	public boolean definitiveCorporativeIsReady(Date processDate){
		StringBuilder builder = new StringBuilder();
		builder.append("SELECT CASE NVL(COUNT(*),0)	");
		builder.append("WHEN 0 THEN 1 				");
		builder.append("ELSE 0 END						");
		builder.append("FROM CORPORATIVE_OPERATION CO 	");
		builder.append("LEFT JOIN PROGRAM_INTEREST_COUPON PIC ON PIC.ID_PROGRAM_INTEREST_PK = CO.ID_PROGRAM_INTEREST_FK	");
		builder.append("LEFT JOIN PROGRAM_AMORTIZATION_COUPON PAC ON PAC.ID_PROGRAM_AMORTIZATION_PK = CO.ID_PROGRAM_AMORTIZATION_FK	");
		builder.append("INNER JOIN SECURITY SE ON SE.ID_SECURITY_CODE_PK = CO.ID_ORIGIN_SECURITY_CODE_FK	");
		builder.append("WHERE (PIC.EXPERITATION_DATE = TRUNC(:processDate) OR 	");
		builder.append("PAC.EXPRITATION_DATE = TRUNC(:processDate))	");
		builder.append("AND SE.SECURITY_CLASS NOT IN (420,1976)	");
		builder.append("AND CO.STATE NOT IN (1314, 1311)	");
		
		Query query = em.createNativeQuery(builder.toString());
		query.setParameter("processDate", processDate);
		
		try{
			BigDecimal quantity = (BigDecimal) query.getSingleResult();
			if(quantity.equals(BigDecimal.ONE)){
				return true;
			}else{
				return false;
			}
		}catch(NoResultException ex){
			return false;
		}
	}
	/**
	 * Lista de participantes para cobro de derechos economicos
	 * */
	public List<Participant> getListParticipantForCollection(){
		Participant participantFTB = this.find(Participant.class, 112L);
		Participant participantPRE = this.find(Participant.class, 113L);
		Participant participantGPS = this.find(Participant.class, 163L);
		
		List<Participant> listParticipant = new ArrayList<>();
		listParticipant.add(participantFTB);
		listParticipant.add(participantPRE);
		listParticipant.add(participantGPS);
		
		return listParticipant;
	}
	/**
	 * Buscamos los agentes pagadores de acuerdo a la actividad economica
	 * que esten con estado confirmado
	 * */
	public List<PaymentAgent> getListPaymentAgentContacsByEcomicActivity(Integer economicActivity){
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" Select pa from PaymentAgent pa ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and pa.mnemonic is not null ");
		queryBuilder.append(" and pa.idPaymentAgentFk is null ");
		queryBuilder.append(" and pa.state = :state ");
		if(Validations.validateIsNotNullAndNotEmpty(economicActivity))
			queryBuilder.append(" and pa.typePaymentAgent =  :economicActivity ");

		Query query = em.createQuery(queryBuilder.toString());
		query.setParameter("state", PaymentAgentStateType.CONFIRMED_PAYER_AGENT.getCode());
		if(Validations.validateIsNotNullAndNotEmpty(economicActivity))
			query.setParameter("economicActivity", economicActivity);
		@SuppressWarnings("unchecked")
		List<PaymentAgent> lstResult = query.getResultList();
		return lstResult;
	}
	
	public AccountAnnotationOperation searchAccountAnnotationOperation(Long idAnnotationOperationPk) throws ServiceException{
		
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String,Object>();  
		
		sbQuery.append("	SELECT aao FROM AccountAnnotationOperation aao");
		sbQuery.append("	LEFT JOIN FETCH aao.accountAnnotationCupons aac ");
		sbQuery.append("	INNER JOIN  aao.custodyOperation co");
		sbQuery.append("	INNER JOIN FETCH aao.holderAccount ha");
		sbQuery.append("	INNER JOIN FETCH ha.participant pa");
		sbQuery.append("	LEFT JOIN FETCH aao.issuer");	
		sbQuery.append(" 	LEFT JOIN FETCH aao.security sec");
		sbQuery.append(" 	LEFT JOIN FETCH sec.issuer isu");
		sbQuery.append("	WHERE aao.idAnnotationOperationPk = :idAnnotationOperationPk");		

		parameters.put("idAnnotationOperationPk",idAnnotationOperationPk);
		
		return (AccountAnnotationOperation) findObjectByQueryString(sbQuery.toString(),parameters);
	}
	
}