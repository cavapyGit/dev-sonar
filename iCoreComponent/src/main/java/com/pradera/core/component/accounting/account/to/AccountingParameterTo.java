package com.pradera.core.component.accounting.account.to;

import java.io.Serializable;


public class AccountingParameterTo implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 418208129259564477L;
	private Integer 	idAccountingParameterPk;
	private Integer 	parameterType;
	private Integer 	nrCorrelative;
	private String 		parameterName;
	private String 		description;
	private Integer 	status;
	 
	private String 		descriptionParameterType;
	
	private String 		descriptionParamterStatus;

	

	public Integer getIdAccountingParameterPk() {
		return idAccountingParameterPk;
	}

	public void setIdAccountingParameterPk(Integer idAccountingParameterPk) {
		this.idAccountingParameterPk = idAccountingParameterPk;
	}

	public Integer getParameterType() {
		return parameterType;
	}

	public void setParameterType(Integer parameterType) {
		this.parameterType = parameterType;
	}

	public Integer getNrCorrelative() {
		return nrCorrelative;
	}

	public void setNrCorrelative(Integer nrCorrelative) {
		this.nrCorrelative = nrCorrelative;
	}

	public String getParameterName() {
		return parameterName;
	}

	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getDescriptionParameterType() {
		return descriptionParameterType;
	}

	public void setDescriptionParameterType(String descriptionParameterType) {
		this.descriptionParameterType = descriptionParameterType;
	}

	public String getDescriptionParamterStatus() {
		return descriptionParamterStatus;
	}

	public void setDescriptionParamterStatus(String descriptionParamterStatus) {
		this.descriptionParamterStatus = descriptionParamterStatus;
	}
	
	
	
	
	
	
}
