package com.pradera.core.component.helperui.to;

import java.io.Serializable;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class IssuerSearcherTO.
 * Transfer Object to search issuers
 * @author PraderaTechnologies.
 * @version 1.0 , 31/01/2013
 */
public class IssuerSearcherTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1163608326299739815L;
	
	
	/** The issuer code. */
	private String issuerCode;
	
	/** The issuer desc. */
	private String issuerDesc;
	
	/** The document type. */
	private Integer documentType;
	
	/** The document number. */
	private String documentNumber;
	
	/** The mnemonic. */
	private String mnemonic;
	
	/** The state. */
	private Integer state;
	
	/** The economic sector. */
	private Integer economicSector;
	
	/** The ecomonic activity. */
	private Integer ecomonicActivity;

	/**
	 * Instantiates a new issuer searcher to.
	 */
	public IssuerSearcherTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the issuer code.
	 *
	 * @return the issuer code
	 */
	public String getIssuerCode() {
		return issuerCode;
	}

	/**
	 * Sets the issuer code.
	 *
	 * @param issuerCode the new issuer code
	 */
	public void setIssuerCode(String issuerCode) {
		this.issuerCode = issuerCode;
	}

	/**
	 * Gets the issuer desc.
	 *
	 * @return the issuer desc
	 */
	public String getIssuerDesc() {
		return issuerDesc;
	}

	/**
	 * Sets the issuer desc.
	 *
	 * @param issuerDesc the new issuer desc
	 */
	public void setIssuerDesc(String issuerDesc) {
		this.issuerDesc = issuerDesc;
	}

	/**
	 * Gets the document type.
	 *
	 * @return the document type
	 */
	public Integer getDocumentType() {
		return documentType;
	}

	/**
	 * Sets the document type.
	 *
	 * @param documentType the new document type
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	/**
	 * Gets the document number.
	 *
	 * @return the document number
	 */
	public String getDocumentNumber() {
		return documentNumber;
	}

	/**
	 * Sets the document number.
	 *
	 * @param documentNumber the new document number
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}

	/**
	 * Sets the mnemonic.
	 *
	 * @param mnemonic the new mnemonic
	 */
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the economic sector.
	 *
	 * @return the economic sector
	 */
	public Integer getEconomicSector() {
		return economicSector;
	}

	/**
	 * Sets the economic sector.
	 *
	 * @param economicSector the new economic sector
	 */
	public void setEconomicSector(Integer economicSector) {
		this.economicSector = economicSector;
	}

	/**
	 * Gets the ecomonic activity.
	 *
	 * @return the ecomonic activity
	 */
	public Integer getEcomonicActivity() {
		return ecomonicActivity;
	}

	/**
	 * Sets the ecomonic activity.
	 *
	 * @param ecomonicActivity the new ecomonic activity
	 */
	public void setEcomonicActivity(Integer ecomonicActivity) {
		this.ecomonicActivity = ecomonicActivity;
	}

}
