package com.pradera.core.component.helperui.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author Martin Zarate Rafael
 * 
 * 
 * THIS CLASS CONTAINS ALL POSSIBLE RESULTS TO HOLDER HELPER
 *
 */

public class HolderHelperOutputTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -69737016195967271L;

	/** The holder id. */
	private Long holderId;
	
	/** The full name. */
	/** Name or FullName from DB*/
	private String fullName;
	
	/** The name. */
	private String name;
	
	/** The first last name. */
	private String firstLastName;
	
	/** The second last name. */
	private String secondLastName;
	
	private Integer holderType;
	
	private Integer holderStatus;
	
	private String holderStatusDescription;
	
	private String holderTypeDocumentDescription;
	
	private Integer documentType;
	
	private String documentTypeDesc;
	
	private String documentNumber;
	
	private String transferNumber;

	private Long partIdPk;
	
	private Integer accountState;

	private List<String> lstFundType;
	private String strFundType;
	private Integer economicActivity;
	private List<Integer> lstEconomicActivity;
	private Date date;
	private SecuritiesTO securityTO;
	private Long fundCUI;
	
	public Long getHolderId() {
		return holderId;
	}

	public void setHolderId(Long holderId) {
		this.holderId = holderId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstLastName() {
		return firstLastName;
	}

	public void setFirstLastName(String firstLastName) {
		this.firstLastName = firstLastName;
	}

	public String getSecondLastName() {
		return secondLastName;
	}

	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}

	public Integer getDocumentType() {
		return documentType;
	}

	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	public String getDocumentTypeDesc() {
		return documentTypeDesc;
	}

	public void setDocumentTypeDesc(String documentTypeDesc) {
		this.documentTypeDesc = documentTypeDesc;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public Long getPartIdPk() {
		return partIdPk;
	}

	public void setPartIdPk(Long partIdPk) {
		this.partIdPk = partIdPk;
	}

	public Integer getAccountState() {
		return accountState;
	}

	public void setAccountState(Integer accountState) {
		this.accountState = accountState;
	}

	public Integer getHolderType() {
		return holderType;
	}

	public void setHolderType(Integer holderType) {
		this.holderType = holderType;
	}

	public Integer getHolderStatus() {
		return holderStatus;
	}

	public void setHolderStatus(Integer holderStatus) {
		this.holderStatus = holderStatus;
	}

	public String getHolderStatusDescription() {
		return holderStatusDescription;
	}

	public void setHolderStatusDescription(String holderStatusDescription) {
		this.holderStatusDescription = holderStatusDescription;
	}

	public String getHolderTypeDocumentDescription() {
		return holderTypeDocumentDescription;
	}

	public void setHolderTypeDocumentDescription(
			String holderTypeDocumentDescription) {
		this.holderTypeDocumentDescription = holderTypeDocumentDescription;
	}

	public List<String> getLstFundType() {
		return lstFundType;
	}

	public void setLstFundType(List<String> lstFundType) {
		this.lstFundType = lstFundType;
	}

	public String getStrFundType() {
		return strFundType;
	}

	public void setStrFundType(String strFundType) {
		this.strFundType = strFundType;
	}

	public Integer getEconomicActivity() {
		return economicActivity;
	}

	public void setEconomicActivity(Integer economicActivity) {
		this.economicActivity = economicActivity;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public SecuritiesTO getSecurityTO() {
		return securityTO;
	}

	public void setSecurityTO(SecuritiesTO securityTO) {
		this.securityTO = securityTO;
	}

	public Long getFundCUI() {
		return fundCUI;
	}

	public void setFundCUI(Long fundCUI) {
		this.fundCUI = fundCUI;
	}

	public List<Integer> getLstEconomicActivity() {
		return lstEconomicActivity;
	}

	public void setLstEconomicActivity(List<Integer> lstEconomicActivity) {
		this.lstEconomicActivity = lstEconomicActivity;
	}

	/**
	 * @return the transferNumber
	 */
	public String getTransferNumber() {
		return transferNumber;
	}

	/**
	 * @param transferNumber the transferNumber to set
	 */
	public void setTransferNumber(String transferNumber) {
		this.transferNumber = transferNumber;
	}

	
}
