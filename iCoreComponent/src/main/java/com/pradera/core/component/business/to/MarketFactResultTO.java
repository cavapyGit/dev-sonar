package com.pradera.core.component.business.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;



public class MarketFactResultTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MarketFactResultTO() {
	}
	
	private BigDecimal totalBalance;
	private BigDecimal totalAvailable;
	private Date marketDate;
	private BigDecimal marketRate;
	private BigDecimal marketPrice;
	
	private BigDecimal quantity;

	public BigDecimal getTotalBalance() {
		return totalBalance;
	}
	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}
	public BigDecimal getTotalAvailable() {
		return totalAvailable;
	}
	public void setTotalAvailable(BigDecimal totalAvailable) {
		this.totalAvailable = totalAvailable;
	}
	
	public Date getMarketDate() {
		return marketDate;
	}
	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}
	public BigDecimal getMarketRate() {
		return marketRate;
	}
	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}
	public BigDecimal getMarketPrice() {
		return marketPrice;
	}
	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}
	/**
	 * @return the quantity
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	
	

}
