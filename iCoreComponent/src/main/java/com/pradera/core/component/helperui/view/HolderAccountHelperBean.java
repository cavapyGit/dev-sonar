package com.pradera.core.component.helperui.view;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.el.ELContext;
import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;

import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.HelperBean;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.HolderAccountSearchTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Security;

@HelperBean
public class HolderAccountHelperBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private HolderAccountSearchTO holderAccountSearchTO;
	@Inject
	GeneralParametersFacade generalParametersFacade;
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	@PostConstruct
	public void init(){
		holderAccountSearchTO = new HolderAccountSearchTO();
	}
	
	public void loadHelp(){
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			ELContext elContext = context.getELContext();
			ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.holder}", Holder.class);
			Holder holder = (Holder) valueExpression.getValue(elContext);
			valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.blHolder}", Boolean.class);
			Boolean blHolder = (Boolean) valueExpression.getValue(elContext);
			if((Validations.validateIsNullOrEmpty(holder) || Validations.validateIsNullOrEmpty(holder.getIdHolderPk()))
					&& blHolder){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getGenericMessage("account.helper.alert.holder"));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}				
			valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.participant}", Long.class);
			Long participant = (Long) valueExpression.getValue(elContext);
			if(Validations.validateIsNullOrEmpty(participant) || GeneralConstants.ZERO_VALUE_LONG.equals(participant)){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getGenericMessage("account.helper.alert.participant"));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}				
			holderAccountSearchTO = new HolderAccountSearchTO();
			holderAccountSearchTO.setHolder(holder);
			holderAccountSearchTO.setParticipant(helperComponentFacade.findParticipant(participant));			
			fillCombos();
			RequestContext.getCurrentInstance().execute("PF('dlgHolderAccount').show();");
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public void getHolderAccount(HolderAccount holderAccount){
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.holderAccount}", Security.class);	
		valueExpression.setValue(elContext, holderAccount);
	}
	
	public void cleanResult(){
		holderAccountSearchTO.setBlNoResult(false);
		holderAccountSearchTO.setLstHolderAccount(null);
	}
	
	public void cleanSearch(){
		JSFUtilities.resetViewRoot();
		cleanResult();
		holderAccountSearchTO.setState(null);
		holderAccountSearchTO.setAccountType(null);
	}
	
	public void searchHolderAccounts(){
		try {
//			if(Validations.validateIsNotNullAndPositive(holderAccountSearchTO.getState())){
//				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//						, PropertiesUtilities.getGenericMessage("account.helper.alert.state"));
//				JSFUtilities.showSimpleValidationDialog();
//				return;
//			}			
			holderAccountSearchTO.setLstHolderAccount(null);
			holderAccountSearchTO.setBlNoResult(true);
			List<HolderAccount> lstResult = helperComponentFacade.findHolderAccounts(holderAccountSearchTO);
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				holderAccountSearchTO.setLstHolderAccount(new GenericDataModel<HolderAccount>(setDescriptions(lstResult)));
				holderAccountSearchTO.setBlNoResult(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public void findHolderAccount(){
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.holder}", Holder.class);
		Holder holder = (Holder) valueExpression.getValue(elContext);
		valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.blHolder}", Boolean.class);
		Boolean blHolder = (Boolean) valueExpression.getValue(elContext);
		
		if((Validations.validateIsNullOrEmpty(holder) || Validations.validateIsNullOrEmpty(holder.getIdHolderPk()))
				&& blHolder){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getGenericMessage("account.helper.alert.holder"));
			JSFUtilities.showSimpleValidationDialog();
			valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.holderAccount}", HolderAccount.class);
			valueExpression.setValue(elContext, new HolderAccount());
			return;
		}				
		valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.participant}", Long.class);
		Long idParticipant = (Long) valueExpression.getValue(elContext);
		if(Validations.validateIsNullOrEmpty(idParticipant) 
				|| GeneralConstants.ZERO_VALUE_LONG.equals(idParticipant)){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getGenericMessage("account.helper.alert.participant"));
			JSFUtilities.showSimpleValidationDialog();
			valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.holderAccount}", HolderAccount.class);
			valueExpression.setValue(elContext, new HolderAccount());
			return;
		}
		valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.holderAccount}", HolderAccount.class);
		HolderAccount holderAccount = (HolderAccount) valueExpression.getValue(elContext);
		Long idHolder= null;
		if (Validations.validateIsNotNull(holder)) {
			idHolder= holder.getIdHolderPk();
		}
		if(Validations.validateIsNotNullAndNotEmpty(holderAccount.getAccountNumber())){
			HolderAccount holderAccountTemp = helperComponentFacade.findHolderAccount(holderAccount.getAccountNumber(), idParticipant, idHolder);
			if(Validations.validateIsNotNullAndNotEmpty(holderAccountTemp)){
					valueExpression.setValue(elContext, holderAccountTemp);
			}else{
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getGenericMessage("account.helper.alert.notexist", new Object[]{holderAccount.getAccountNumber().toString()}));
				JSFUtilities.showSimpleValidationDialog();
				valueExpression.setValue(elContext, new HolderAccount());
			}
		}else{
			valueExpression.setValue(elContext, new HolderAccount());
		}		
	}
	
	private List<HolderAccount> setDescriptions(List<HolderAccount> lstResult){
		for(HolderAccount holderAccount:lstResult){
			holderAccount.setAccountDescription(holderAccountSearchTO.getMpAccountType().get(holderAccount.getAccountType()));
			holderAccount.setStateAccountDescription(holderAccountSearchTO.getMpAccountType().get(holderAccount.getStateAccount()));
		}
		return lstResult;
	}
	
	private void fillCombos() throws ServiceException{
		ParameterTableTO paramTabTO = new ParameterTableTO();
		paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
		paramTabTO.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
		holderAccountSearchTO.setLstState(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
		paramTabTO.setMasterTableFk(MasterTableType.ACCOUNTS_TYPE.getCode());
		holderAccountSearchTO.setLstAccountType(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
		Map<Integer, String> mpAccountType = new HashMap<Integer, String>();
		for(ParameterTable paramTab:holderAccountSearchTO.getLstAccountType()){
			mpAccountType.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		Map<Integer, String> mplstState = new HashMap<Integer, String>();
		for(ParameterTable paramTab:holderAccountSearchTO.getLstState()){
			mpAccountType.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		holderAccountSearchTO.setMplstState(mplstState);
		holderAccountSearchTO.setMpAccountType(mpAccountType);
	}
	
	public HolderAccountSearchTO getHolderAccountSearchTO() {
		return holderAccountSearchTO;
	}
	public void setHolderAccountSearchTO(HolderAccountSearchTO holderAccountSearchTO) {
		this.holderAccountSearchTO = holderAccountSearchTO;
	}

	
}
