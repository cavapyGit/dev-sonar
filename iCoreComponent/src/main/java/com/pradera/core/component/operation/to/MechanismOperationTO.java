package com.pradera.core.component.operation.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class MechanismOperationTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long idMechanismOperationPk;
	private Date operationDate;
	private Long operationNumber;
	private String paymentReference;
	private Long idNegotiationModalityPk;
	private Long idNegotiationMechanismPk;
	private Long buyerParticipant;
	
	private Long sellerParticipant;
	private Integer currency;
	private Date settlementDate;
	private Integer signType;
	private Integer settlementSchema;
	private Integer settlementType;
	private Date initialRealTermSettlementDate;
	private Date endRealTermSettlementDate;
	private Date initDate;
	private Date endDate;
	private String idSecurityCodePk;
	private String idIssuer;
	
	private String mnemonicParticipant;
	private Long idParticipant;
	private String participant;
	private Integer accountHolderNumber;
	private String nameHolder;
	private String firstLastNameHolder;
	private String secondLastNameHolder;
	private String fullNameHolder;
	private String descriptionHolder;
	private Integer operationState;
	private String modalityOperation;
	private BigDecimal stockQuantity;
	private Date termSettlementDate;
	private Double termReportedOperation;
	private Integer termInDays;
	private Boolean selected;
	private Long idHolderAccount;
	private Long idHolder;
	private Long idHolderAccountOperation;
	private Integer operationPart;
	
	public Long getIdMechanismOperationPk() {
		return idMechanismOperationPk;
	}
	public void setIdMechanismOperationPk(Long idMechanismOperationPk) {
		this.idMechanismOperationPk = idMechanismOperationPk;
	}
	public Date getOperationDate() {
		return operationDate;
	}
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}
	public Long getOperationNumber() {
		return operationNumber;
	}
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}
	public String getPaymentReference() {
		return paymentReference;
	}
	public void setPaymentReference(String paymentReference) {
		this.paymentReference = paymentReference;
	}
	public Long getBuyerParticipant() {
		return buyerParticipant;
	}
	public void setBuyerParticipant(Long buyerParticipant) {
		this.buyerParticipant = buyerParticipant;
	}
	public Long getSellerParticipant() {
		return sellerParticipant;
	}
	public void setSellerParticipant(Long sellerParticipant) {
		this.sellerParticipant = sellerParticipant;
	}
	public Long getIdNegotiationModalityPk() {
		return idNegotiationModalityPk;
	}
	public void setIdNegotiationModalityPk(Long idNegotiationModalityPk) {
		this.idNegotiationModalityPk = idNegotiationModalityPk;
	}
	public Long getIdNegotiationMechanismPk() {
		return idNegotiationMechanismPk;
	}
	public void setIdNegotiationMechanismPk(Long idNegotiationMechanismPk) {
		this.idNegotiationMechanismPk = idNegotiationMechanismPk;
	}
	public Integer getCurrency() {
		return currency;
	}
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	public Date getSettlementDate() {
		return settlementDate;
	}
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}
	public Integer getSignType() {
		return signType;
	}
	public void setSignType(Integer signType) {
		this.signType = signType;
	}
	public Date getInitDate() {
		return initDate;
	}
	public void setInitDate(Date initDate) {
		this.initDate = initDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getIdIssuer() {
		return idIssuer;
	}
	public void setIdIssuer(String idIssuer) {
		this.idIssuer = idIssuer;
	}
	public Integer getSettlementSchema() {
		return settlementSchema;
	}
	public void setSettlementSchema(Integer settlementSchema) {
		this.settlementSchema = settlementSchema;
	}
	public Integer getSettlementType() {
		return settlementType;
	}
	public void setSettlementType(Integer settlementType) {
		this.settlementType = settlementType;
	}
	public Date getInitialRealTermSettlementDate() {
		return initialRealTermSettlementDate;
	}
	public void setInitialRealTermSettlementDate(Date initialRealTermSettlementDate) {
		this.initialRealTermSettlementDate = initialRealTermSettlementDate;
	}
	public Date getEndRealTermSettlementDate() {
		return endRealTermSettlementDate;
	}
	public void setEndRealTermSettlementDate(Date endRealTermSettlementDate) {
		this.endRealTermSettlementDate = endRealTermSettlementDate;
	}
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
	public String getMnemonicParticipant() {
		return mnemonicParticipant;
	}
	public void setMnemonicParticipant(String mnemonicParticipant) {
		this.mnemonicParticipant = mnemonicParticipant;
	}
	public String getNameHolder() {
		return nameHolder;
	}
	public void setNameHolder(String nameHolder) {
		this.nameHolder = nameHolder;
	}
	public String getFirstLastNameHolder() {
		return firstLastNameHolder;
	}
	public void setFirstLastNameHolder(String firstLastNameHolder) {
		this.firstLastNameHolder = firstLastNameHolder;
	}
	public String getSecondLastNameHolder() {
		return secondLastNameHolder;
	}
	public void setSecondLastNameHolder(String secondLastNameHolder) {
		this.secondLastNameHolder = secondLastNameHolder;
	}
	public String getFullNameHolder() {
		return fullNameHolder;
	}
	public void setFullNameHolder(String fullNameHolder) {
		this.fullNameHolder = fullNameHolder;
	}
	public String getDescriptionHolder() {
		if(nameHolder!=null){
			descriptionHolder=firstLastNameHolder+" ";
		}
		if(firstLastNameHolder!=null){
			descriptionHolder+=firstLastNameHolder+" ";
		}
		if(secondLastNameHolder!=null){
			descriptionHolder+=secondLastNameHolder;
		}
		if(descriptionHolder==null){
			descriptionHolder=fullNameHolder;
		}
		
		return descriptionHolder;
	}
	public void setDescriptionHolder(String descriptionHolder) {
		this.descriptionHolder = descriptionHolder;
	}
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}
	public Date getTermSettlementDate() {
		return termSettlementDate;
	}
	public void setTermSettlementDate(Date termSettlementDate) {
		this.termSettlementDate = termSettlementDate;
	}
	public Long getIdParticipant() {
		return idParticipant;
	}
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}
	public Integer getOperationState() {
		return operationState;
	}
	public void setOperationState(Integer operationState) {
		this.operationState = operationState;
	}
	public String getModalityOperation() {
		return modalityOperation;
	}
	public void setModalityOperation(String modalityOperation) {
		this.modalityOperation = modalityOperation;
	}
	public Double getTermReportedOperation() {
		return termReportedOperation;
	}
	public void setTermReportedOperation(Double termReportedOperation) {
		this.termReportedOperation = termReportedOperation;
	}
	public Integer getTermInDays() {
		return termInDays;
	}
	public void setTermInDays(Integer termInDays) {
		this.termInDays = termInDays;
	}
	public String getParticipant() {
		return participant;
	}
	public void setParticipant(String participant) {
		this.participant = participant;
	}
	public Boolean getSelected() {
		return selected;
	}
	public void setSelected(Boolean selected) {
		this.selected = selected;
	}
	public Long getIdHolderAccount() {
		return idHolderAccount;
	}
	public void setIdHolderAccount(Long idHolderAccount) {
		this.idHolderAccount = idHolderAccount;
	}
	public Long getIdHolderAccountOperation() {
		return idHolderAccountOperation;
	}
	public void setIdHolderAccountOperation(Long idHolderAccountOperation) {
		this.idHolderAccountOperation = idHolderAccountOperation;
	}
	public Long getIdHolder() {
		return idHolder;
	}
	public void setIdHolder(Long idHolder) {
		this.idHolder = idHolder;
	}
	public Integer getAccountHolderNumber() {
		return accountHolderNumber;
	}
	public void setAccountHolderNumber(Integer accountHolderNumber) {
		this.accountHolderNumber = accountHolderNumber;
	}
	public Integer getOperationPart() {
		return operationPart;
	}
	public void setOperationPart(Integer operationPart) {
		this.operationPart = operationPart;
	}
	
}
