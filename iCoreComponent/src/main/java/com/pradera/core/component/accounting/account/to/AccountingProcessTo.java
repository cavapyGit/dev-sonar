package com.pradera.core.component.accounting.account.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounting.AccountingMatrix;
import com.pradera.model.accounting.AccountingProcess;

public class AccountingProcessTo implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Long idAccountingProcessPk;
	
	private Integer processType;
	
	private Integer status;
		
	private Date executionDate;
	
	private LoggerUser loggerUser;

	private List<AccountingMatrix> accountingMatrixList;
	
	private AccountingProcess accountingProcess;
	
	private Integer numberSchemas;

	/**
	 * @return the idAccountingProcessPk
	 */
	public Long getIdAccountingProcessPk() {
		return idAccountingProcessPk;
	}

	/**
	 * @param idAccountingProcessPk the idAccountingProcessPk to set
	 */
	public void setIdAccountingProcessPk(Long idAccountingProcessPk) {
		this.idAccountingProcessPk = idAccountingProcessPk;
	}

	/**
	 * @return the processType
	 */
	public Integer getProcessType() {
		return processType;
	}

	/**
	 * @param processType the processType to set
	 */
	public void setProcessType(Integer processType) {
		this.processType = processType;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the executionDate
	 */
	public Date getExecutionDate() {
		return executionDate;
	}

	/**
	 * @param executionDate the executionDate to set
	 */
	public void setExecutionDate(Date executionDate) {
		this.executionDate = executionDate;
	}

	/**
	 * @return the loggerUser
	 */
	public LoggerUser getLoggerUser() {
		return loggerUser;
	}

	/**
	 * @param loggerUser the loggerUser to set
	 */
	public void setLoggerUser(LoggerUser loggerUser) {
		this.loggerUser = loggerUser;
	}

	/**
	 * @return the accountingMatrixList
	 */
	public List<AccountingMatrix> getAccountingMatrixList() {
		return accountingMatrixList;
	}

	/**
	 * @param accountingMatrixList the accountingMatrixList to set
	 */
	public void setAccountingMatrixList(List<AccountingMatrix> accountingMatrixList) {
		this.accountingMatrixList = accountingMatrixList;
	}

	/**
	 * @return the accountingProcess
	 */
	public AccountingProcess getAccountingProcess() {
		return accountingProcess;
	}

	/**
	 * @param accountingProcess the accountingProcess to set
	 */
	public void setAccountingProcess(AccountingProcess accountingProcess) {
		this.accountingProcess = accountingProcess;
	}

	/**
	 * @return the numberSchemas
	 */
	public Integer getNumberSchemas() {
		return numberSchemas;
	}

	/**
	 * @param numberSchemas the numberSchemas to set
	 */
	public void setNumberSchemas(Integer numberSchemas) {
		this.numberSchemas = numberSchemas;
	}
	
	
	
	

}
