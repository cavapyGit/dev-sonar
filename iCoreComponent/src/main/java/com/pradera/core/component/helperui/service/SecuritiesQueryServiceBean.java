package com.pradera.core.component.helperui.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.helperui.to.SecuritiesSearcherTO;
import com.pradera.core.component.helperui.to.SecuritiesTO;
import com.pradera.core.component.helperui.to.SecuritySearchTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.ProgramAmortizationCoupon;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleSituationType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class SecuritiesQueryServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/01/2013
 */
@Stateless
public class SecuritiesQueryServiceBean extends CrudDaoServiceBean {
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The max results query helper. */
	@Inject @Configurable
	Integer maxResultsQueryHelper;

	/**
	 * Find security from isin.
	 *
	 * @param securities the securities
	 * @return the securities to
	 */
	public SecuritiesTO findSecurityFromIsin(SecuritiesSearcherTO securities) {
		StringBuilder sb = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		
		sb.append(" select s.idSecurityCodePk, s.cfiCode,s.mnemonic, s.description, s.stateSecurity, s.issuanceForm ");
		sb.append(" from Security s where s.idSecurityCodePk = :securityPk ");
		
		/**If filters has security class*/
		if(securities.getListClassSecuritie()!=null && !securities.getListClassSecuritie().isEmpty()){
			sb.append(" and s.securityClass In (:classList) ");
			parameters.put("classList", securities.getListClassSecuritie());
		}
		
		parameters.put("securityPk", securities.getIsinCode());
		Object[] securityObject = (Object[])findObjectByQueryString(sb.toString(), parameters);
		SecuritiesTO securiesTO = null;
		if(securityObject!=null){
			securiesTO = new SecuritiesTO();
			securiesTO.setSecuritiesCode((String)securityObject[0]);
			securiesTO.setCodeCFI((String)securityObject[1]);
			securiesTO.setMnemonic((String)securityObject[2]);
			securiesTO.setDescription((String)securityObject[3]);
			securiesTO.setState((Integer)securityObject[4]);
			securiesTO.setIssuanceForm((Integer)securityObject[5]);
		}
		
		return securiesTO;
	}

	/**
	 * Find securities from helper.
	 *
	 * @param securities the securities
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<SecuritiesTO> findSecuritiesFromHelper(SecuritiesSearcherTO securities) throws ServiceException{
		StringBuilder sb = new StringBuilder();
		
		Map<String, Object> params = new HashMap<String, Object>();
		
		sb.append(" select distinct s.cfiCode, s.description, s.mnemonic,s.idSecurityCodeOnly,  "); // 0 1 2 3
		sb.append("  s.stateSecurity,s.idSecurityCodePk, "); // 4 , 5 
		sb.append("  (select p.parameterName from ParameterTable p where p.parameterTablePk = s.stateSecurity), "); // 6
		sb.append("  (select p.parameterName from ParameterTable p where p.parameterTablePk = s.instrumentType), "); // 7
		sb.append("  hab.totalBalance, hab.availableBalance, "); // 8 9
		sb.append("  (select p.text1 from ParameterTable p where p.parameterTablePk = s.securityClass) "); // 10
		sb.append(" from HolderAccountBalance hab ");
		sb.append(" inner join hab.security s ");
		sb.append(" where 1=1 ");
		
		if(StringUtils.isNotBlank(securities.getIsinCode())) {
			sb.append(" and hab.security.idSecurityCodePk like :isin ");
			params.put("isin", "%"+securities.getIsinCode()+"%");
		}			
		if (StringUtils.isNotBlank(securities.getIssuerCode())) {
			sb.append(" and s.issuer.idIssuerPk = :issuer ");
			params.put("issuer", securities.getIssuerCode());
		}
		if (StringUtils.isNotBlank(securities.getIssuerDescription())) {
			sb.append(" and s.issuer.businessName like :issuerDesc ");
			params.put("issuerDesc", "%"+securities.getIssuerDescription()+"%");
		}
		if (StringUtils.isNotBlank(securities.getDescription())) {
			sb.append(" and s.description like :description ");
			params.put("description", "%"+securities.getDescription()+"%");
		}
		if (Validations.validateIsNotNullAndPositive(securities.getStateSecuritie())) {
			sb.append(" and s.stateSecurity = :state ");
			params.put("state", securities.getStateSecuritie());
		}
		
		//Validating than is not zero or -1 because it comes from a Combo
		if (Validations.validateIsNotNullAndPositive(securities.getInstrumentType())) {
			sb.append(" and s.instrumentType = :instrument ");
			params.put("instrument", securities.getInstrumentType());
		}
			
		if(Validations.validateIsNotNullAndPositive(securities.getSecuritiesType())) {
			sb.append(" and s.securityType = :type ");
			params.put("type", securities.getSecuritiesType());
		}
		if (Validations.validateIsNotNullAndPositive(securities.getClassSecuritie())) {
			sb.append(" and s.securityClass = :class ");
			params.put("class", securities.getClassSecuritie());
		}
		
		if (securities.getListClassSecuritie()!=null && !securities.getListClassSecuritie().isEmpty()) {
			sb.append(" and s.securityClass In (:classList) ");
			params.put("classList", securities.getListClassSecuritie());
		}

		if(Validations.validateIsNotNullAndPositive(securities.getParticipantCode())) {
			sb.append(" and hab.participant.idParticipantPk = :participantPk ");
			params.put("participantPk", securities.getParticipantCode());
		}
		if (Validations.validateIsNotNullAndNotEmpty(securities.getHolderAccount()) && 
				Validations.validateIsNotNullAndPositive(securities.getHolderAccount().getAccountNumber())) {
			sb.append(" and hab.holderAccount.accountNumber = :accountNumber ");
			params.put("accountNumber", securities.getHolderAccount().getAccountNumber());
		}

		//Using method with maximum result list for query 
		List<Object[]> resultList = findListByQueryStringMaxResults(sb.toString(), params);
		
		List<SecuritiesTO> listSecurities = new ArrayList<SecuritiesTO>();
		
		for(Object[] result : resultList) {
			SecuritiesTO  secTO = new SecuritiesTO();
			secTO.setCodeCFI(result[0] == null? null : (String)result[0]);
			secTO.setDescription(result[1] == null? null : (String)result[1]);
			secTO.setMnemonic(result[2] == null? null : (String)result[2]);
			secTO.setSecuritiesCodeOnly(result[3] == null? null : (String)result[3]);
			secTO.setState(result[4] == null? null : (Integer)result[4]);
			secTO.setSecuritiesCode(result[5] == null? null : (String)result[5]);
			secTO.setStateDesc(result[6] == null? null : (String)result[6]);
			secTO.setInstrumentTypeDesc(result[7] == null? null : (String)result[7]);
			secTO.setTotalBalance(result[8] == null? null : (BigDecimal)result[8]);
			secTO.setAvailableBalance(result[9] == null? null : (BigDecimal)result[9]);
			secTO.setSecurityClass(result[10] == null? null : (String)result[10]);
			listSecurities.add(secTO);
		}
		
		return listSecurities;
	}
	
	/**
	 * Find securities from helper native query.
	 *
	 * @param securities the securities
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	//FIND SECURITY THROUGH NATIVE QUERY
	@SuppressWarnings("unchecked")
	public List<Object[]> findSecuritiesFromHelperNativeQuery(SecuritiesSearcherTO securities) throws ServiceException{
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT	DISTINCT																								" +
				  "		 	SEC.ID_SECURITY_CODE_PK,																				" +
				  "			SEC.DESCRIPTION,																						" +
				  "			(SELECT PT1.PARAMETER_NAME FROM PARAMETER_TABLE PT1 WHERE PT1.PARAMETER_TABLE_PK=SEC.SECURITY_TYPE),	" +
				  "			(SELECT PT2.PARAMETER_NAME FROM PARAMETER_TABLE PT2 WHERE PT2.PARAMETER_TABLE_PK=SEC.SECURITY_CLASS),	" +
				  "			SEC.ID_ISIN_CODE																						" +
				  "	FROM																											" +
				  "			SECURITY SEC	");
		
		if(Validations.validateIsNotNullAndPositive(securities.getParticipantCode()) ||
				Validations.validateIsNotNullAndNotEmpty(securities.getLstHolderAccountPk())){
			sb.append(" INNER JOIN HOLDER_ACCOUNT_BALANCE HAB 	ON HAB.ID_SECURITY_CODE_PK = SEC.ID_SECURITY_CODE_PK ");
			if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
				sb.append(" INNER JOIN PARTICIPANT P 			ON P.ID_PARTICIPANT_PK = HAB.ID_PARTICIPANT_PK ");
				sb.append(" INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = HAB.ID_HOLDER_ACCOUNT_PK ");
				sb.append(" INNER JOIN HOLDER H					ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
			}
		}
		
		sb.append("	WHERE 	1=1	");		
		if (StringUtils.isNotBlank(securities.getIssuerCode())) {
			sb.append(" AND SEC.ID_ISSUER_FK = :issuer ");
		}
		if(Validations.validateIsNotNullAndPositive(securities.getParticipantCode())) {
			sb.append(" AND HAB.ID_PARTICIPANT_PK = :participantPk ");
			if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
				sb.append(" AND P.DOCUMENT_TYPE = H.DOCUMENT_TYPE ");
				sb.append(" AND P.DOCUMENT_NUMBER = H.DOCUMENT_NUMBER ");
			}
		}
		if(Validations.validateIsNotNullAndNotEmpty(securities.getLstHolderAccountPk())){
			sb.append("	AND HAB.ID_HOLDER_ACCOUNT_PK IN (:lstHolderAccountPk)");
		}
		//Validating than is not zero or -1 because it comes from a Combo
		if (Validations.validateIsNotNullAndPositive(securities.getInstrumentType())) {
			sb.append(" AND SEC.INSTRUMENT_TYPE = :instrument ");
		}
		if(Validations.validateIsNotNullAndPositive(securities.getSecuritiesType())) {
			sb.append(" AND SEC.SECURITY_TYPE = :type ");
		}
		if (Validations.validateIsNotNullAndPositive(securities.getClassSecuritie())) {
			sb.append(" AND SEC.SECURITY_CLASS = :class ");
		}
		if (Validations.validateIsNotNullAndNotEmpty(securities.getListClassSecuritie())) {
			sb.append(" AND SEC.SECURITY_CLASS in :listClass ");
		}
		if (Validations.validateIsNotNullAndPositive(securities.getStateSecuritie())) {
			sb.append(" AND SEC.STATE_SECURITY = :state ");
		}
		if (StringUtils.isNotBlank(securities.getDescription())) {
			sb.append(" AND SEC.DESCRIPTION like :description ");
		}
		if(StringUtils.isNotBlank(securities.getIsinCode())) {
			sb.append(" AND SEC.id_security_code_pk = :isin ");
		}
		if (StringUtils.isNotBlank(securities.getIdSecurityCodeOnly())) {
			sb.append(" AND SEC.ID_SECURITY_CODE_ONLY like :secOnly ");
		}
		if(StringUtils.isNotBlank(securities.getMnemonic())) {
			sb.append(" AND SEC.MNEMONIC like :mnemonic ");
		}			

		Query query = em.createNativeQuery(sb.toString());
		
		//SETTING PARAMETERS
		if (StringUtils.isNotBlank(securities.getIssuerCode())) {
			query.setParameter("issuer", securities.getIssuerCode());
		}
		if (Validations.validateIsNotNullAndPositive(securities.getParticipantCode())) {
			query.setParameter("participantPk", securities.getParticipantCode());
		}
		if(Validations.validateIsNotNullAndNotEmpty(securities.getLstHolderAccountPk())){
			query.setParameter("lstHolderAccountPk", securities.getLstHolderAccountPk());
		}
		if (Validations.validateIsNotNullAndPositive((securities.getInstrumentType()))) {
			query.setParameter("instrument", securities.getInstrumentType());
		}
		if(Validations.validateIsNotNullAndPositive(securities.getSecuritiesType())) {
			query.setParameter("type", securities.getSecuritiesType());
		}
		if (Validations.validateIsNotNullAndPositive(securities.getClassSecuritie())) {
			query.setParameter("class", securities.getClassSecuritie());
		}
		if (Validations.validateIsNotNullAndNotEmpty(securities.getListClassSecuritie())) {
			query.setParameter("listClass", securities.getListClassSecuritie());
		}
		if (Validations.validateIsNotNullAndPositive((securities.getStateSecuritie()))) {
			query.setParameter("state", securities.getStateSecuritie());
		}
		if (StringUtils.isNotBlank(securities.getDescription())) {
			query.setParameter("description", "%"+securities.getDescription()+"%");
		}
		if(StringUtils.isNotBlank(securities.getIsinCode())) {
			query.setParameter("isin", securities.getIsinCode());
		}
		if (StringUtils.isNotBlank(securities.getIdSecurityCodeOnly())) {
			query.setParameter("secOnly", securities.getIdSecurityCodeOnly());
		}
		if(StringUtils.isNotBlank(securities.getMnemonic())) {
			query.setParameter("mnemonic", securities.getMnemonic());
		}
		query.setMaxResults(maxResultsQueryHelper);		
		return query.getResultList();
	}
	
	/**
	 * Find security service bean.
	 * method useful to get Security with all his attributes
	 * @param securityTO the security to
	 * @return the security
	 * @throws ServiceException the service exception
	 */
	public Security findSecurityComponentServiceBean(SecurityTO securityTO) throws ServiceException{
		CriteriaBuilder criteriaBuilder=em.getCriteriaBuilder();
		CriteriaQuery<Security> criteriaQuery=criteriaBuilder.createQuery(Security.class);
		Root<Security> securityRoot=criteriaQuery.from(Security.class);
		List<Predicate> criteriaParameters=new ArrayList<Predicate>();
		
		securityRoot.fetch("issuer");
		securityRoot.fetch("issuance");
		
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdSecurityCodePk())){
			criteriaParameters.add(criteriaBuilder.equal(securityRoot.get("idSecurityCodePk"), securityTO.getIdSecurityCodePk() ));
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdIsinCodePk())){
			criteriaParameters.add(criteriaBuilder.equal(securityRoot.get("idIsinCode"), securityTO.getIdIsinCodePk() ));
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdSecurityCodeOnly())){
			criteriaParameters.add(criteriaBuilder.equal(securityRoot.get("idSecurityCodeOnly"), securityTO.getIdIsinCodePk() ));
		}
		criteriaQuery.where(criteriaBuilder.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()])));
		TypedQuery<Security> security=em.createQuery(criteriaQuery);
		
		Security securityTemp = null;
		try{
			securityTemp = security.getSingleResult();
		}catch(NoResultException ex){
			return null;
		}
		return securityTemp; 
	}
	
	/**
	 * Find securities.
	 *
	 * @param securityTO the security to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Security> findSecurities(SecurityTO securityTO)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select S.idSecurityCodePk," +
		                       "S.description," +
				               "S.currentNominalValue," +
		                       "S.shareCapital," +
				               "S.shareBalance," +
		                       "S.circulationBalance"+
				            " From Security S");
		sbQuery.append(" where 1 = 1");
		if(Validations.validateIsNotNull(securityTO.getIdIssuerCodePk())){
			sbQuery.append(" And S.issuer.idIssuerPk=:issu");
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getStateSecurity())){
			sbQuery.append(" And S.stateSecurity=:state");
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getInstrumentType())){
			sbQuery.append(" And S.instrumentType=:instrument");
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getClassType())){
			sbQuery.append(" And S.securityClass=:class");
		}
		
		Query query = em.createQuery(sbQuery.toString());
		
		if(Validations.validateIsNotNull(securityTO.getIdIssuerCodePk())){
			query.setParameter("issu", securityTO.getIdIssuerCodePk());
			
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getStateSecurity())){
			query.setParameter("state", securityTO.getStateSecurity());
 		}
		
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getInstrumentType())){
			query.setParameter("instrument", securityTO.getInstrumentType());
 		}
		
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getClassType())){
			query.setParameter("class", securityTO.getClassType());
 		}
		
		
		List<Security> listSecurities = new ArrayList<Security>();
		Security security = null;
		List<Object> objectList = query.getResultList();
		for(int i =0; i<objectList.size();i++){
			Object[] obj = (Object[]) objectList.get(i);
			
			security = new Security();
			security.setIdSecurityCodePk((String)obj[0]);
			security.setDescription((String)obj[1]);
			security.setCurrentNominalValue((BigDecimal) obj[2]);
			security.setShareCapital((BigDecimal) obj[3]);
			security.setShareBalance((BigDecimal) obj[4]);
			security.setCirculationBalance((BigDecimal) obj[5]);
			listSecurities.add(security);
		}
		return listSecurities;
	}
	
	/**
	 * Gets the lis security service bean.
	 *
	 * @param filter the filter
	 * @return the lis security service bean
	 * @throws ServiceException the service exception
	 */
	public List<Security> getLisSecurityServiceBean(Security filter) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select s From Security s");// join fetch ParameterTable pt ");
		sbQuery.append(" Where 1 = 1 ");

		if(Validations.validateIsNotNull(filter.getStateSecurity())){
			sbQuery.append(" and s.state = :statePrm ");
		}
		if(Validations.validateIsNotNull(filter.getIdSecurityCodePk())){
			sbQuery.append(" and s.idSecurityCodePk = :idSecurityCodePkPrm ");
		}
		
		TypedQuery<Security> query = em.createQuery(sbQuery.toString(),Security.class); 
		
		if(Validations.validateIsNotNull(filter.getStateSecurity())){
    		query.setParameter("statePrm", filter.getStateSecurity());
    	}
		if(Validations.validateIsNotNull(filter.getIdSecurityCodePk())){
			query.setParameter("idSecurityCodePkPrm",filter.getIdSecurityCodePk());
		}
		
		return query.getResultList();
	}
	
	/**
	 * Security help service bean.
	 * metodo para buscar el valor de acuerdo al ISIN ingresado,  
	 *
	 * @param securityCode the security code
	 * @return the security
	 * @throws ServiceException the service exception
	 */
	public Security getSecurityHelpServiceBean(String securityCode) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT s");
		sbQuery.append("	FROM Security s left join fetch s.interestPaymentSchedule ips join fetch s.issuer ");
		sbQuery.append("	WHERE s.idSecurityCodePk = :securityCode");
		
		Query query = em.createQuery(sbQuery.toString());
		
		query.setParameter("securityCode", securityCode);
		
		Security sec=null;
		try{
			sec = (Security)query.getSingleResult();
			sec.getInterestPaymentSchedule();
		} catch(NoResultException nrex) {
			log.info("No se encontro el codigo");
			sec=null;
		} catch (NonUniqueResultException nuex) {
			log.info("Mas de un valor tiene ese codigo");
			sec=null;
		}

		return sec;
	}
		
	/**
	 * Find security.
	 *
	 * @param securitySearchTO the security search to
	 * @return the security
	 */
	public Security findSecurity(SecuritySearchTO securitySearchTO){
		try{
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append("	SELECT se													");
		sbQuery.append(" from HolderAccountBalance hab 									");
		sbQuery.append(" inner join hab.holderAccount ha								");
		sbQuery.append(" inner join ha.holderAccountDetails had							");
		sbQuery.append(" inner join had.holder ho										");
		sbQuery.append(" inner join hab.security se										");
		sbQuery.append(" inner join hab.participant p									");
		sbQuery.append(" where 1=1 														");
		if(Validations.validateIsNotNullAndNotEmpty(securitySearchTO.getSecurityCode())){
			sbQuery.append(" and se.idSecurityCodePk =:idSecurityCodePk						");
		}
		if(Validations.validateIsNotNull(securitySearchTO.getParticipant()) &&
				Validations.validateIsNotNull(securitySearchTO.getParticipant().getIdParticipantPk())){
			sbQuery.append(" and p.idParticipantPk =:idParticipantPk						");
		}
		Query query = em.createQuery(sbQuery.toString());
		if(Validations.validateIsNotNullAndNotEmpty(securitySearchTO.getSecurityCode())){
			query.setParameter("idSecurityCodePk", securitySearchTO.getSecurityCode().trim().toUpperCase());
		}
		if(Validations.validateIsNotNull(securitySearchTO.getParticipant()) &&
				Validations.validateIsNotNull(securitySearchTO.getParticipant().getIdParticipantPk())){
			query.setParameter("idParticipantPk", securitySearchTO.getParticipant().getIdParticipantPk());
		}
		return (Security)query.getSingleResult();
	} catch (NoResultException e) {
		return null;
	}
	}
	
	/**
	 * Find securities.
	 *
	 * @param securitySearchTO the security search to
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<Security> findSecurities(SecuritySearchTO securitySearchTO) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT 	distinct se.idSecurityCodePk ,");
		sbQuery.append("			se.description,");
		sbQuery.append("			se.securityType,");
		sbQuery.append("			se.securityClass,");
		sbQuery.append("			se.idIsinCode");
		sbQuery.append("	FROM Security se");
		if((Validations.validateIsNotNull(securitySearchTO.getParticipant()) && 
				Validations.validateIsNotNull(securitySearchTO.getParticipant().getIdParticipantPk())) || 
				(securitySearchTO.getLstHolderAccountPk() !=null && 
				securitySearchTO.getLstHolderAccountPk().size()>0)){
			sbQuery.append(" Inner join se.holderAccountBalances hab");
		}
		sbQuery.append("	WHERE se.stateSecurity = :stateSecurity");
		sbQuery.append("	AND se.instrumentType = :instrumentType");
		if(Validations.validateIsNotNullAndNotEmpty(securitySearchTO.getIssuer()) &&
				Validations.validateIsNotNullAndNotEmpty(securitySearchTO.getIssuer().getIdIssuerPk()))
			sbQuery.append("	AND se.issuer.idIssuerPk = :idIssuerPk");
		if(Validations.validateIsNotNullAndNotEmpty(securitySearchTO.getIssuance()) &&
				Validations.validateIsNotNullAndNotEmpty(securitySearchTO.getIssuance().getIdIssuanceCodePk()))
			sbQuery.append("	AND se.issuance.idIssuanceCodePk = :idIssuanceCodePk");
		if(Validations.validateIsNotNullAndNotEmpty(securitySearchTO.getSecurityType()))
			sbQuery.append("	AND se.securityType = :securityType");
		if(Validations.validateIsNotNullAndNotEmpty(securitySearchTO.getSecurityClass())){
			sbQuery.append("	AND se.securityClass = :securityClass");
		}else if(securitySearchTO.getLstSecurityClass()!=null && 
				securitySearchTO.getLstSecurityClass().size()==GeneralConstants.TWO_VALUE_INTEGER.intValue()){
			sbQuery.append("	AND se.securityClass in (:securityClassList)");
		}			
		if(Validations.validateIsNotNullAndNotEmpty(securitySearchTO.getDescription()))
			sbQuery.append("	AND se.description like :description");
		if(Validations.validateIsNotNullAndNotEmpty(securitySearchTO.getSecurityCode()))
			sbQuery.append("	AND se.idSecurityCodePk like :idSecurityCodePk");
		if(Validations.validateIsNotNullAndNotEmpty(securitySearchTO.getMnemonic()))
			sbQuery.append("	AND se.mnemonic like :mnemonic");
		if(Validations.validateIsNotNull(securitySearchTO.getParticipant()) && 
				Validations.validateIsNotNull(securitySearchTO.getParticipant().getIdParticipantPk())){
			sbQuery.append("	AND hab.participant.idParticipantPk =:idParticipantPk");
		}
		if(securitySearchTO.getLstHolderAccountPk() !=null && 
				securitySearchTO.getLstHolderAccountPk().size()>0){
			sbQuery.append("	AND hab.holderAccount.idHolderAccountPk in (:lstHolderAccountPk)");
		}
		sbQuery.append("	ORDER BY se.idSecurityCodePk DESC");
		
		Query query = em.createQuery(sbQuery.toString());
		//Setting limit results
		query.setMaxResults(maxResultsQueryHelper);
		query.setParameter("stateSecurity", securitySearchTO.getState());
		query.setParameter("instrumentType", securitySearchTO.getInstrumentType());
		if(Validations.validateIsNotNullAndNotEmpty(securitySearchTO.getIssuer()) &&
				Validations.validateIsNotNullAndNotEmpty(securitySearchTO.getIssuer().getIdIssuerPk()))
			query.setParameter("idIssuerPk", securitySearchTO.getIssuer().getIdIssuerPk());
		if(Validations.validateIsNotNullAndNotEmpty(securitySearchTO.getIssuance()) &&
				Validations.validateIsNotNullAndNotEmpty(securitySearchTO.getIssuance().getIdIssuanceCodePk()))
			query.setParameter("idIssuanceCodePk", securitySearchTO.getIssuance().getIdIssuanceCodePk());
		if(Validations.validateIsNotNullAndNotEmpty(securitySearchTO.getSecurityType()))
			query.setParameter("securityType", securitySearchTO.getSecurityType());
		if(Validations.validateIsNotNullAndNotEmpty(securitySearchTO.getSecurityClass())){
			query.setParameter("securityClass", securitySearchTO.getSecurityClass());
		}else if(securitySearchTO.getLstSecurityClass()!=null && 
				securitySearchTO.getLstSecurityClass().size()==GeneralConstants.TWO_VALUE_INTEGER.intValue()){
			List<Integer> lstSecurityClassForIssuerDpf = new ArrayList<Integer>();
			lstSecurityClassForIssuerDpf.add(SecurityClassType.DPF.getCode());
			lstSecurityClassForIssuerDpf.add(SecurityClassType.DPA.getCode());
			query.setParameter("securityClassList", lstSecurityClassForIssuerDpf);
		}	
			
		if(Validations.validateIsNotNullAndNotEmpty(securitySearchTO.getDescription()))
			query.setParameter("description", GeneralConstants.PERCENTAGE_STRING + 
											securitySearchTO.getDescription() + 
											GeneralConstants.PERCENTAGE_STRING);
		if(Validations.validateIsNotNullAndNotEmpty(securitySearchTO.getSecurityCode())){
			String securityCode = securitySearchTO.getSecurityCode();
			if(!securityCode.contains(GeneralConstants.PERCENTAGE_STRING)){
				securityCode = GeneralConstants.PERCENTAGE_STRING.concat(securityCode).concat(GeneralConstants.PERCENTAGE_STRING);
			}
			query.setParameter("idSecurityCodePk",securityCode);
		}
		if(Validations.validateIsNotNullAndNotEmpty(securitySearchTO.getMnemonic()))
			query.setParameter("mnemonic", securitySearchTO.getMnemonic());
		if(Validations.validateIsNotNull(securitySearchTO.getParticipant()) && 
				Validations.validateIsNotNull(securitySearchTO.getParticipant().getIdParticipantPk())){
			query.setParameter("idParticipantPk", securitySearchTO.getParticipant().getIdParticipantPk());
		}
		if(securitySearchTO.getLstHolderAccountPk() !=null && 
				securitySearchTO.getLstHolderAccountPk().size()>0){
			query.setParameter("lstHolderAccountPk", securitySearchTO.getLstHolderAccountPk());
		}
		List<Object[]> lstResult = (List<Object[]>)query.getResultList();
		List<Security> lstSecurity = null;
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
			lstSecurity = new ArrayList<Security>();
			for(Object[] object:lstResult){
				Security security = new Security();
				security.setIdSecurityCodePk(object[0].toString());				
				security.setDescription(object[1]!=null ? object[1].toString() : null);
				security.setSecurityType((Integer)object[2]);
				security.setSecurityClass((Integer)object[3]);
				security.setIdIsinCode(Validations.validateIsNotNullAndNotEmpty(object[4])?object[4].toString():null);
				lstSecurity.add(security);
			}
		}
		return lstSecurity;
	}

	/**
	 * Find security.
	 *
	 * @param security the security
	 * @return the security
	 */
	public Security findSecurity(Security security) {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT se");
			sbQuery.append("	FROM Security se");
			sbQuery.append("	WHERE se.idSecurityCodePk = :idSecurityCodePk");
			if(Validations.validateIsNotNullAndNotEmpty(security.getIssuer()) &&
					Validations.validateIsNotNullAndNotEmpty(security.getIssuer().getIdIssuerPk()))
				sbQuery.append("	AND se.issuer.idIssuerPk = :idIssuerPk");
			if(Validations.validateIsNotNullAndNotEmpty(security.getIssuance()) &&
					Validations.validateIsNotNullAndNotEmpty(security.getIssuance().getIdIssuanceCodePk()))
				sbQuery.append("	AND se.issuance.idIssuanceCodePk = :idIssuanceCodePk");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idSecurityCodePk", security.getIdSecurityCodePk().trim().toUpperCase());
			if(Validations.validateIsNotNullAndNotEmpty(security.getIssuer()) &&
					Validations.validateIsNotNullAndNotEmpty(security.getIssuer().getIdIssuerPk()))
				query.setParameter("idIssuerPk", security.getIssuer().getIdIssuerPk());
			if(Validations.validateIsNotNullAndNotEmpty(security.getIssuance()) &&
					Validations.validateIsNotNullAndNotEmpty(security.getIssuance().getIdIssuanceCodePk()))
				query.setParameter("idIssuanceCodePk", security.getIssuance().getIdIssuanceCodePk());
			return (Security)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	/**
	 * Gets the interest chronogram bean.
	 *
	 * @param idSecurity the id security
	 * @return the interest chronogram bean
	 */
	@SuppressWarnings("unchecked")
	public List<ProgramInterestCoupon> getInterestChronogramBean(String idSecurity) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT pic");
		sbQuery.append("	FROM InterestPaymentSchedule ips inner join ips.programInterestCoupons pic");
		sbQuery.append("	WHERE ips.security.idSecurityCodePk = :securityCode");
		sbQuery.append("	ORDER BY ips.security.idSecurityCodePk, pic.couponNumber ");
		
		Query query = em.createQuery(sbQuery.toString());
		
		query.setParameter("securityCode", idSecurity);
		
		List<ProgramInterestCoupon> lstResult = (List<ProgramInterestCoupon>)query.getResultList();

		return lstResult;
	}
	
	/**
	 * Gets the amortization chronogram bean.
	 *
	 * @param idSecurity the id security
	 * @return the amortization chronogram bean
	 */
	@SuppressWarnings("unchecked")
	public List<ProgramAmortizationCoupon> getAmortizationChronogramBean(String idSecurity) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT pac");
		sbQuery.append("	FROM AmortizationPaymentSchedule aps inner join aps.programAmortizationCoupons pac");
		sbQuery.append("	WHERE aps.security.idSecurityCodePk = :securityCode");
		sbQuery.append("	ORDER BY aps.security.idSecurityCodePk, pac.couponNumber ");		
		Query query = em.createQuery(sbQuery.toString());		
		query.setParameter("securityCode", idSecurity);		
		List<ProgramAmortizationCoupon> lstResult = (List<ProgramAmortizationCoupon>)query.getResultList();
		return lstResult;
	}
	
	/**
	 * Find securities mcn operation.
	 *
	 * @param securityTO the security to
	 * @return the security
	 * @throws ServiceException the service exception
	 */
	public Security findSecuritiesById(SecurityTO securityTO) throws ServiceException{
		Security objSecurity = null;
		try {
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(" SELECT sec.idSecurityCodePk, sec.securityClass, 						"); // 0, 1
			stringBuilder.append(" sec.circulationBalance, sec.stateSecurity, sec.instrumentType,		"); // 2, 3, 4			
			stringBuilder.append(" sec.issuer.idIssuerPk, sec.issuer.stateIssuer, 						"); // 5, 6
			stringBuilder.append(" sec.issuance.idIssuanceCodePk, sec.issuance.stateIssuance,			"); // 7, 8
			stringBuilder.append(" sec.expirationDate, sec.currency, 									"); // 9, 10
			stringBuilder.append(" sec.issuance.indPrimaryPlacement, sec.issuance.numberTotalTranches, 	"); // 11, 12
			stringBuilder.append(" sec.issuance.currentTranche , sec.indTraderSecondary	,				"); // 13, 14
			stringBuilder.append(" sec.initialNominalValue, sec.currentNominalValue	,					"); // 15, 16
			stringBuilder.append(" sec.circulationAmount 												"); // 17
			stringBuilder.append(" FROM Security sec  													");			
			stringBuilder.append(" WHERE sec.idSecurityCodePk = :idSecurityCode 						");			
			Query typedQuery= em.createQuery(stringBuilder.toString());
			typedQuery.setParameter("idSecurityCode", securityTO.getIdSecurityCodePk());
			Object [] resultSecurity = (Object[]) typedQuery.getSingleResult();	
			if(resultSecurity != null){
				objSecurity = new Security();
				objSecurity.setIdSecurityCodePk(resultSecurity[0].toString());
				objSecurity.setSecurityClass(new Integer(resultSecurity[1].toString()));
				objSecurity.setCirculationBalance(new BigDecimal(resultSecurity[2].toString()));
				objSecurity.setStateSecurity(new Integer(resultSecurity[3].toString()));
				objSecurity.setInstrumentType(new Integer(resultSecurity[4].toString()));				
				
				Issuer objIssuer = new Issuer();
				objIssuer.setIdIssuerPk(resultSecurity[5].toString());
				objIssuer.setStateIssuer(new Integer(resultSecurity[6].toString()));
				objSecurity.setIssuer(objIssuer);
				
				Issuance objIssuance = new Issuance();
				objIssuance.setIdIssuanceCodePk(resultSecurity[7].toString());
				objIssuance.setStateIssuance(new Integer(resultSecurity[8].toString()));
				if(Validations.validateIsNotNullAndNotEmpty(resultSecurity[11])){
					objIssuance.setIndPrimaryPlacement(new Integer(resultSecurity[11].toString()));
				}				
				if(Validations.validateIsNotNullAndNotEmpty(resultSecurity[12])){
					objIssuance.setNumberTotalTranches(new Integer(resultSecurity[12].toString()));
				}				
				if(Validations.validateIsNotNullAndNotEmpty(resultSecurity[13])){
					objIssuance.setCurrentTranche(new Integer(resultSecurity[13].toString()));
				}
				objSecurity.setIssuance(objIssuance);
								
				objSecurity.setExpirationDate((Date) resultSecurity[9]);
				objSecurity.setCurrency(new Integer(resultSecurity[10].toString()));
				if(Validations.validateIsNotNullAndNotEmpty(resultSecurity[14])){
					objSecurity.setIndTraderSecondary(new Integer(resultSecurity[14].toString()));
				}
				objSecurity.setInitialNominalValue(new BigDecimal(resultSecurity[15].toString()));
				objSecurity.setCurrentNominalValue(new BigDecimal(resultSecurity[16].toString()));
				objSecurity.setCirculationAmount(new BigDecimal(resultSecurity[17].toString()));
			}									
		} catch(NoResultException ex) {
			return null;
		}		
		return objSecurity;
	}
	
	
	/**
	 * Find securities by dpf.
	 *
	 * @param securityTO the security to
	 * @return the security
	 * @throws ServiceException the service exception
	 */
	public Security findSecuritiesByDpf(SecurityTO securityTO) throws ServiceException {
		Security objSecurity = null;
		try {
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(" SELECT sec.idSecurityCodePk, sec.securityClass, 						"); // 0, 1
			stringBuilder.append(" sec.circulationBalance, sec.stateSecurity, sec.instrumentType,		"); // 2, 3, 4			
			stringBuilder.append(" sec.issuer.idIssuerPk, sec.issuer.stateIssuer, 						"); // 5, 6
			stringBuilder.append(" sec.issuance.idIssuanceCodePk, sec.issuance.stateIssuance,			"); // 7, 8
			stringBuilder.append(" sec.expirationDate, sec.currency, 									"); // 9, 10
			stringBuilder.append(" sec.issuance.indPrimaryPlacement, sec.issuance.numberTotalTranches, 	"); // 11, 12
			stringBuilder.append(" sec.issuance.currentTranche , sec.indTraderSecondary	,				"); // 13, 14
			stringBuilder.append(" sec.initialNominalValue, sec.currentNominalValue	,					"); // 15, 16
			stringBuilder.append(" sec.circulationAmount, sec.issuanceForm,								"); // 17, 18			
			stringBuilder.append(" sec.instrumentType, sec.securityType, 								"); // 19, 20
			stringBuilder.append(" sec.securitySource, sec.securitySerial, 								"); // 21, 22 
			stringBuilder.append(" sec.alternativeCode , sec.issuanceDate,								"); // 23, 24			
			stringBuilder.append(" sec.physicalBalance , sec.issuer.economicSector,						"); // 25, 26
			stringBuilder.append(" sec.observations	, sec.interestRate									"); // 27, 28
			stringBuilder.append(" FROM Security sec  													");			
			stringBuilder.append(" WHERE sec.idSecurityCodePk = :idSecurityCode 						");			
			Query typedQuery= em.createQuery(stringBuilder.toString());
			typedQuery.setParameter("idSecurityCode", securityTO.getIdSecurityCodePk());
			Object [] resultSecurity = (Object[]) typedQuery.getSingleResult();	
			if(resultSecurity != null){
				objSecurity = new Security();
				objSecurity.setIdSecurityCodePk(resultSecurity[0].toString());
				objSecurity.setSecurityClass(new Integer(resultSecurity[1].toString()));
				objSecurity.setCirculationBalance(new BigDecimal(resultSecurity[2].toString()));
				objSecurity.setStateSecurity(new Integer(resultSecurity[3].toString()));
				objSecurity.setInstrumentType(new Integer(resultSecurity[4].toString()));				
				
				Issuer objIssuer = new Issuer();
				objIssuer.setIdIssuerPk(resultSecurity[5].toString());
				objIssuer.setStateIssuer(new Integer(resultSecurity[6].toString()));
				if(Validations.validateIsNotNullAndNotEmpty(resultSecurity[26])){
					objIssuer.setEconomicSector(new Integer(resultSecurity[26].toString()));
				}
				objSecurity.setIssuer(objIssuer);
				
				Issuance objIssuance = new Issuance();
				objIssuance.setIdIssuanceCodePk(resultSecurity[7].toString());
				objIssuance.setStateIssuance(new Integer(resultSecurity[8].toString()));
				if(Validations.validateIsNotNullAndNotEmpty(resultSecurity[11])){
					objIssuance.setIndPrimaryPlacement(new Integer(resultSecurity[11].toString()));
				}				
				if(Validations.validateIsNotNullAndNotEmpty(resultSecurity[12])){
					objIssuance.setNumberTotalTranches(new Integer(resultSecurity[12].toString()));
				}				
				if(Validations.validateIsNotNullAndNotEmpty(resultSecurity[13])){
					objIssuance.setCurrentTranche(new Integer(resultSecurity[13].toString()));
				}
				objSecurity.setIssuance(objIssuance);
								
				objSecurity.setExpirationDate((Date) resultSecurity[9]);
				objSecurity.setCurrency(new Integer(resultSecurity[10].toString()));
				if(Validations.validateIsNotNullAndNotEmpty(resultSecurity[14])){
					objSecurity.setIndTraderSecondary(new Integer(resultSecurity[14].toString()));
				}
				objSecurity.setInitialNominalValue(new BigDecimal(resultSecurity[15].toString()));
				objSecurity.setCurrentNominalValue(new BigDecimal(resultSecurity[16].toString()));
				objSecurity.setCirculationAmount(new BigDecimal(resultSecurity[17].toString()));
				
				if(Validations.validateIsNotNullAndNotEmpty(resultSecurity[18])){
					objSecurity.setIssuanceForm(new Integer(resultSecurity[18].toString()));
				}
				if(Validations.validateIsNotNullAndNotEmpty(resultSecurity[19])){
					objSecurity.setInstrumentType(new Integer(resultSecurity[19].toString()));
				}
				if(Validations.validateIsNotNullAndNotEmpty(resultSecurity[20])){
					objSecurity.setSecurityType(new Integer(resultSecurity[20].toString()));
				}
				if(Validations.validateIsNotNullAndNotEmpty(resultSecurity[21])){
					objSecurity.setSecuritySource(new Integer(resultSecurity[21].toString()));
				}
				if(Validations.validateIsNotNullAndNotEmpty(resultSecurity[22])){
					objSecurity.setSecuritySerial(resultSecurity[22].toString());
				}
				if(Validations.validateIsNotNullAndNotEmpty(resultSecurity[23])){
					objSecurity.setAlternativeCode(resultSecurity[23].toString());
				}
				if(Validations.validateIsNotNullAndNotEmpty(resultSecurity[24])) {
					objSecurity.setIssuanceDate((Date)resultSecurity[24]);
				}				
				if(Validations.validateIsNotNullAndNotEmpty(resultSecurity[25])) {
					objSecurity.setPhysicalBalance(new BigDecimal(resultSecurity[25].toString()));
				}
				if(Validations.validateIsNotNullAndNotEmpty(resultSecurity[27])) {
					objSecurity.setObservations(resultSecurity[27].toString());
				}
				if(Validations.validateIsNotNullAndNotEmpty(resultSecurity[28])) {
					objSecurity.setInterestRate(new BigDecimal(resultSecurity[28].toString()));
				}
			}									
		} catch(NoResultException ex) {
			return null;
		}		
		return objSecurity;
	}
	
	/**
	 * Metodo que realiza la busqueda por codigo de 
	 * @param securityTO
	 * @return
	 * @throws ServiceException
	 */
	public List<Security> findSecuritiesDpfForFatherFraction(SecurityTO securityTO) throws ServiceException {
		List<Security> objSecurityList = null;
		try {
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(" SELECT sec.idSecurityCodePk, sec.securityClass, 						"); // 0, 1
			stringBuilder.append(" sec.idFractionSecurityCodePk, sec.currentNominalValue				"); // 2, 3
			stringBuilder.append(" FROM Security sec  													");			
			stringBuilder.append(" WHERE sec.idFractionSecurityCodePk = :idSecurityCodeFraction			");			
			stringBuilder.append(" and sec.stateSecurity = :stateSecurity			");
			Query typedQuery= em.createQuery(stringBuilder.toString());
			typedQuery.setParameter("idSecurityCodeFraction", securityTO.getIdFractionSecurityCodePk());
			typedQuery.setParameter("stateSecurity", SecurityStateType.REGISTERED.getCode());
			
			List<Object []> resultSecurity = (List<Object[]>) typedQuery.getResultList();	
			if(resultSecurity != null && resultSecurity.size() > 0){
				Security objSecurity;
				objSecurityList = new ArrayList<>();
				
				for(Object[] result : resultSecurity){
					
					objSecurity = new Security();
					objSecurity.setIdSecurityCodePk(result[0].toString());
					objSecurity.setSecurityClass(new Integer(result[1].toString()));
					
					if(Validations.validateIsNotNullAndNotEmpty(result[2])) {
						objSecurity.setIdFractionSecurityCodePk(result[2].toString());
					}
					
					objSecurity.setCurrentNominalValue((BigDecimal)result[3]);
					
					objSecurityList.add(objSecurity);
				}
				
			}									
		} catch(NoResultException ex) {
			return null;
		}		
		return objSecurityList;
	}
	
	public ProgramInterestCoupon getMostRecentPayedCouponBySecurityPk(String idSecurityCodePk, Date operationDate ) {
		try {
			StringBuilder sb = new StringBuilder();
			sb.append(" select pic from ProgramInterestCoupon pic");
			sb.append(" where pic.interestPaymentSchedule.security.idSecurityCodePk = :idSecurityCodePk");
			sb.append("  and pic.situationProgramInterest in :lstPayedStates");
			sb.append("  and TRUNC(pic.experitationDate) <= :operationDate");
			sb.append(" order by pic.experitationDate desc");
			
			Query q = em.createQuery(sb.toString());
			
			q.setParameter("idSecurityCodePk", idSecurityCodePk);
			q.setParameter("operationDate", CommonsUtilities.truncateDateTime(operationDate));
			
			List<Integer> lstPayedStates = new ArrayList<Integer>();
			lstPayedStates.add(ProgramScheduleSituationType.PENDING.getCode());
			lstPayedStates.add(ProgramScheduleSituationType.EDV_PAID.getCode());
			lstPayedStates.add(ProgramScheduleSituationType.ISSUER_PAID.getCode());
			q.setParameter("lstPayedStates", lstPayedStates);
			
			q.setMaxResults(1);
			
			return (ProgramInterestCoupon) q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public ProgramInterestCoupon getOldestNonPayedCouponBySecurityPk(String idSecurityCodePk, Date operationDate) {
		StringBuilder sb = new StringBuilder();
		sb.append(" select pic from ProgramInterestCoupon pic");
		sb.append(" where pic.interestPaymentSchedule.security.idSecurityCodePk = :idSecurityCodePk");
		sb.append("  and pic.situationProgramInterest in :lstPayedStates");
		sb.append("  and trunc(pic.experitationDate) >= :operationDate");
		sb.append(" order by pic.experitationDate asc");
		
		Query q = em.createQuery(sb.toString());
		
		q.setParameter("idSecurityCodePk", idSecurityCodePk);
		q.setParameter("operationDate", CommonsUtilities.truncateDateTime(operationDate));
		
		List<Integer> lstPayedStates = new ArrayList<Integer>();
		lstPayedStates.add(ProgramScheduleSituationType.PENDING.getCode());
		q.setParameter("lstPayedStates", lstPayedStates);
		
		q.setMaxResults(1);
		
		return (ProgramInterestCoupon) q.getSingleResult();
	}
}