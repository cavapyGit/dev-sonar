package com.pradera.core.component.accounting.account.to;

import java.io.Serializable;

public class AccountingSourceTo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7576375837232920490L;

	private Long idAccountingSourcePk;
	
	private byte[] fileXml;
	
	private Integer auxiliaryType; 
	
	private String fileSourceName;
	
	private String descriptionSource;
	
	private Integer status;
	/**
	 * 
	 */
	public AccountingSourceTo() {
	}
	/**
	 * @return the idAccountingSourcePk
	 */
	public Long getIdAccountingSourcePk() {
		return idAccountingSourcePk;
	}
	/**
	 * @param idAccountingSourcePk the idAccountingSourcePk to set
	 */
	public void setIdAccountingSourcePk(Long idAccountingSourcePk) {
		this.idAccountingSourcePk = idAccountingSourcePk;
	}
	/**
	 * @return the fileXml
	 */
	public byte[] getFileXml() {
		return fileXml;
	}
	/**
	 * @param fileXml the fileXml to set
	 */
	public void setFileXml(byte[] fileXml) {
		this.fileXml = fileXml;
	}
	/**
	 * @return the auxiliaryType
	 */
	public Integer getAuxiliaryType() {
		return auxiliaryType;
	}
	/**
	 * @param auxiliaryType the auxiliaryType to set
	 */
	public void setAuxiliaryType(Integer auxiliaryType) {
		this.auxiliaryType = auxiliaryType;
	}
	/**
	 * @return the fileSourceName
	 */
	public String getFileSourceName() {
		return fileSourceName;
	}
	/**
	 * @param fileSourceName the fileSourceName to set
	 */
	public void setFileSourceName(String fileSourceName) {
		this.fileSourceName = fileSourceName;
	}
	/**
	 * @return the descriptionSource
	 */
	public String getDescriptionSource() {
		return descriptionSource;
	}
	/**
	 * @param descriptionSource the descriptionSource to set
	 */
	public void setDescriptionSource(String descriptionSource) {
		this.descriptionSource = descriptionSource;
	}
	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	
}
