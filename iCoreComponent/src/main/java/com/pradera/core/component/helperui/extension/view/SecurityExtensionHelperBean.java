package com.pradera.core.component.helperui.extension.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.el.ELContext;
import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.ViewDepositaryBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.IssuerSearcherTO;
import com.pradera.core.component.helperui.to.IssuerTO;
import com.pradera.core.component.helperui.to.SecuritiesSearcherTO;
import com.pradera.core.component.helperui.to.SecuritiesTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.report.ReportFilter;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class SecurityExtensionHelperBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , Sep 24, 2013
 */
@ViewDepositaryBean
public class SecurityExtensionHelperBean implements Serializable{

	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3768656482267493732L;

	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	/** The Log. */
	@Inject
	private transient PraderaLogger Log;
	
	/** The User Info. */
	@Inject
	private transient UserInfo userInfo;
	
	/** The security description. */
	private String securityDescription;
	
	/** The security mnemonic. */
	private String securityMnemonic;
	
	/** The securities searcher. */
	private SecuritiesSearcherTO securitiesSearcher;
	
	/** The selected. */
	private SecuritiesTO selected;
	
	/** The list securities. */
	private List<SecuritiesTO> listSecurities;
	
	/** The searched. */
	private boolean searched;
	
	/** The list states. */
	private List<SelectItem> listStates;
	
	/** The list instrument type. */
	//private List<ParameterTable> listInstrumentType;
	
	private List<SelectItem> listInstrumentType;
	
	/** The list class securities. */
	private List<ParameterTable> listClassSecurities;
	
	/** The list securities type. */
	private List<ParameterTable> listSecuritiesType;
	
	/** The list participants. */
	private List<SelectItem> listParticipants;
	
	/** The list issuers. */
	private List<ParameterTable> listIssuers;
	
	/** The Issuer. */
	private Issuer issuer;
	
	/** The helper name. */
	private String helperName;
	
	/** The list empty. */
	private boolean listEmpty; //=false
	/** The log. */
	@Inject
	private transient PraderaLogger log;
	
	/** The disabled participant. */
	private boolean disabledParticipant = false;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The issuer code. */
	private String issuerPk;
	private String issuerDescription;
	private Integer sectorIssuer;
	
	private boolean disabledIssuer;
	
	/** The max results query helper. */
	@Inject @Configurable
	Integer maxResultsQueryHelper;
	
	/**
	 * Inits the.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException {
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		Log.info("bean creado");
		//Initializing variables
		issuer = new Issuer();
		ValueExpression valueExpressionName = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.name}", String.class);
		helperName = (String)valueExpressionName.getValue(elContext);
		//Load combos
		loadFilters();
		disabledIssuer=false;
		
		//VALIDATING USER TYPE
		if(userInfo.getUserAccountSession().isIssuerInstitucion()){
			disabledIssuer = true;
		}
	}
	
	/**
	 * Load filters.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadFilters() throws ServiceException {
		securitiesSearcher = new SecuritiesSearcherTO();
		listStates = new ArrayList<SelectItem>();
		//listInstrumentType = new ArrayList<ParameterTable>();
		listInstrumentType = new ArrayList<SelectItem>();
		listSecuritiesType = new ArrayList<ParameterTable>();
		listStates = new ArrayList<SelectItem>();
		listClassSecurities = new ArrayList<ParameterTable>();
		listParticipants = new ArrayList<SelectItem>();
		listIssuers = new ArrayList<ParameterTable>();
				
		ParameterTableTO paramTable = new ParameterTableTO();
		paramTable.setState(ParameterTableStateType.REGISTERED.getCode());
		
		securitiesSearcher.setStateSecuritie(SecurityStateType.REGISTERED.getCode());
		securitiesSearcher.setInstrumentType(InstrumentType.FIXED_INCOME.getCode());

		//INSTRUMENT TYPE LIST
		/*paramTable.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
		for(ParameterTable param : generalParametersFacade.getListParameterTableServiceBean(paramTable)) {
			listInstrumentType.add(param);
		}*/
		paramTable.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
		for(ParameterTable param : generalParametersFacade.getListParameterTableServiceBean(paramTable)) {
			listInstrumentType.add(new SelectItem(param.getParameterTablePk(), param.getParameterName()));
		}

		//SECURITIES STATUS LIST
		paramTable.setMasterTableFk(MasterTableType.SECURITIES_STATE.getCode());
		for(ParameterTable param : generalParametersFacade.getListParameterTableServiceBean(paramTable)) {
			listStates.add(new SelectItem(param.getParameterTablePk(), param.getParameterName()));
		}

		//PARTICIPANTS LIST
		Participant participant = new Participant();
		for(Participant part : helperComponentFacade.getLisParticipantServiceFacade(participant)) {
			listParticipants.add(new SelectItem(part.getIdParticipantPk(), part.getDescription()));
		}
		//ISSUERS LIST
		IssuerSearcherTO issuerTO = new IssuerSearcherTO();
		ParameterTable  param;
		for(IssuerTO issu : helperComponentFacade.findIssuerByHelper(issuerTO)) {
			param = new ParameterTable();
			param.setIndicator1(issu.getMnemonic());
			param.setParameterName(issu.getIssuerCode());
			param.setDescription(issu.getIssuerDesc());
			param.setIndicator4(issu.getIssuerSector());
			listIssuers.add(param);
		}
		
		//If instrument type is not null load combo security type
		if(Validations.validateIsNotNull(securitiesSearcher.getInstrumentType())){
			loadCboSecuritieTypeByInstrumentHandler();
		}
	}
	
	/**
	 * Open dialog.
	 *
	 * @throws ServiceException the service exception
	 */
	public void openDialog(String componentId) throws ServiceException{
		this.issuerPk = null;
		this.disabledIssuer = false;
		ValueExpression valueExpressionPartCode = null;
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		
		verifyUserType();
		valueExpressionPartCode = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.issuerCode}", String.class);
		String issuerCode = (String) valueExpressionPartCode.getValue(elContext);
		if(Validations.validateIsNotNullAndNotEmpty(issuerCode)){
			this.issuerPk = issuerCode;
			this.disabledIssuer = true;
		}
		for (int i = 0; i < listIssuers.size(); i++) {
			ParameterTable issuerPT= listIssuers.get(i);
			if(issuerPT.getParameterName().equals(issuerPk)){
				issuerDescription=issuerPT.getIndicator1()+" - "+issuerPT.getParameterName()+" - "+issuerPT.getDescription();
				sectorIssuer = issuerPT.getIndicator4();
				break;
			}
		}
	}

		
		/**
		 * Fill participants.
		 *
		 * @throws ServiceException the service exception
		 */
		public void fillParticipants() throws ServiceException{
			Participant participant = new Participant();
			//PARTICIPANTS
			listParticipants = new ArrayList<SelectItem>();
			
			participant.setState(ParticipantStateType.REGISTERED.getCode());
			for(Participant part : helperComponentFacade.getLisParticipantServiceFacade(participant)) {
				listParticipants.add(new SelectItem(part.getIdParticipantPk(), part.getDisplayDescriptionCode(), part.getMnemonic()));
			}
		}
	
	/**
	 * Verify user type.
	 *
	 * @throws ServiceException the service exception
	 */
	public void verifyUserType() throws ServiceException{
		//RESTARTING PARTICIPANT LIST
		listParticipants = new ArrayList<SelectItem>();
		ValueExpression valueExpressionPartCode = null;
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){ //IF USER TYPE IS PARTICIPANT
			
			Participant part = new Participant();

			//DISABLED PARTICIPANT COMBO
			disabledParticipant = true;
			
			//GETTING THE PARTICIPANT BY CODE
			part = helperComponentFacade.findParticipantsByCode(userInfo.getUserAccountSession().getParticipantCode());
			
			//SETTING GLOBAL PARTICIPANT
			idParticipantPk = part.getIdParticipantPk();
			
			//ADDING PARTICIPANT TO COMBO
			listParticipants.add(new SelectItem(part.getIdParticipantPk(), part.getDescription(),part.getMnemonic()));
			
			//SETTING FILTER TO SEARCH
//			holderTO.setPartIdPk(idParticipantPk);	
		}else{
			//FILL ALL PARTICIPANTS
			fillParticipants();
			//USER CAN SELECT ANY PARTICIPANT
			disabledParticipant = false;
			
			//GETTING partCode VALUE
			valueExpressionPartCode = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.partCode}", Long.class);
			
			if(Validations.validateIsNotNullAndPositive((Long)valueExpressionPartCode.getValue(elContext))){
				//SETTING GLOBAL PARTICIPANT
				idParticipantPk = (Long)valueExpressionPartCode.getValue(elContext);
			}
		}
		
	}

	/**
	 * Select securities from helper.
	 */
	public void selectSecuritiesFromHelper() {
		FacesContext context = FacesContext.getCurrentInstance();
		if (selected != null) {
			securityDescription = selected.getDescription();
			securityMnemonic = selected.getMnemonic();
			//set client isin code
			ELContext elContext = context.getELContext();
			ValueExpression valueExpression = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.attrs.security}", Object.class);
			//new Value
			valueExpression.setValue(elContext, selected);

			//update report filter
			ValueExpression valueExpressionReport = context.getApplication().getExpressionFactory()
				    .createValueExpression(elContext, "#{cc.attrs.report}", ReportFilter.class);
			ReportFilter reportFilter =(ReportFilter)valueExpressionReport.getValue(elContext);
			if(reportFilter!=null) {
				reportFilter.setValue(selected);
			}
			//update caller
			ValueExpression valueExpressionName = context.getApplication().getExpressionFactory()
				    .createValueExpression(elContext, "#{cc.attrs.update}", String.class);
			String componentsId = (String)valueExpressionName.getValue(elContext);
			if(StringUtils.isNotBlank(componentsId)) {
				RequestContext.getCurrentInstance().update(CommonsUtilities.getListOfComponentsForUpdate(componentsId));
			}
			
			
			selected = new SecuritiesTO();
			listSecurities = new ArrayList<SecuritiesTO>();
		} else {
			//no se encontro valor
			Log.error("No encontro el valor");
		}
	}
	
	/**
	 * Search helper securities.
	 *
	 * @param event the event
	 * @throws ServiceException the service exception
	 */
	public void searchHelperSecurities(ActionEvent event) throws ServiceException {
		searched = true;
		FacesContext context = FacesContext.getCurrentInstance();
		
		ELContext elContextAux = context.getELContext();
		
		//GETTING PARTICIPANT CODE
		ValueExpression valueExpressionAux = context.getApplication().getExpressionFactory().createValueExpression(elContextAux, "#{cc.attrs.partCode}", Long.class);
		Long partCode = (Long) valueExpressionAux.getValue(elContextAux);
		
		//If User is Participant Investor Institution Setting Security Participant Code to SecuritiesSearcherTO
		if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
			securitiesSearcher.setParticipantCode(partCode);
		}
		
		//GETTING HELPER NAME
//		ValueExpression valueExpressionName = context.getApplication().getExpressionFactory().createValueExpression(elContextAux, "#{cc.attrs.name}", String.class);
//		String helperName = (String)valueExpressionName.getValue(elContextAux);
		
		//GETTING CLIENT ID
//		ValueExpression valueExClientId = context.getApplication().getExpressionFactory().createValueExpression(elContextAux, "#{cc.clientId}", String.class);
//		String clientId = (String)valueExClientId.getValue(elContextAux);
		
		if(Validations.validateIsNullOrEmpty(securitiesSearcher.getStateSecuritie())){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
											 PropertiesUtilities.getGenericMessage("security.helper.alert.state"));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		if(Validations.validateIsNullOrEmpty(securitiesSearcher.getInstrumentType())){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
											 PropertiesUtilities.getGenericMessage("security.helper.alert.instrumenttype"));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		searchSecuritiesMethod();
}

	/**
	 * Search securities method.
	 *
	 * @throws ServiceException the service exception
	 */
	public void searchSecuritiesMethod() throws ServiceException{
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContextAux = context.getELContext();
		HolderAccount holderAccAux = new HolderAccount();
		
		//GETTING HOLDER ACCOUNT
		ValueExpression valueExpressionAux = context.getApplication().getExpressionFactory().createValueExpression(elContextAux, "#{cc.attrs.holdAcc}", HolderAccount.class);
		holderAccAux = (HolderAccount) valueExpressionAux.getValue(elContextAux);
		
		//GETTING ISSUER
		securitiesSearcher.setIssuerCode(issuerPk);
		
		ValueExpression valueExpressionAux2 = context.getApplication().getExpressionFactory().createValueExpression(elContextAux, "#{cc.attrs.partCode}", Long.class);
		Long partCode = (Long) valueExpressionAux2.getValue(elContextAux);
		
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			securitiesSearcher.setParticipantCode(partCode);
		}
				
		//Setting HolderAccount Object to SecuritiesSearcherTO
		if(Validations.validateIsNotNullAndNotEmpty(holderAccAux)){
			securitiesSearcher.setHolderAccount(holderAccAux);
		}
		
		//If User is Issue DPF Institution Setting Security Class DPA-DPF to SecuritiesSearcherTO
		if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
			List<Integer> listClassSecurity = new ArrayList<>();
			listClassSecurity.add(SecurityClassType.DPA.getCode());
			listClassSecurity.add(SecurityClassType.DPF.getCode());
			securitiesSearcher.setListClassSecuritie(listClassSecurity);
		}
		
		listSecurities = helperComponentFacade.findSecuritiesFromHelperNativeQuery(securitiesSearcher);
		if(listSecurities.size()==GeneralConstants.ZERO_VALUE_INTEGER){
			listEmpty = true;
		}else{
			listEmpty = false;
		}
	}
	
	
	/**
	 * Clean.
	 *
	 * @param event the event
	 */
	public void clean(ActionEvent event) {
		securitiesSearcher = new SecuritiesSearcherTO();
		securitiesSearcher.setStateSecuritie(SecurityStateType.REGISTERED.getCode());
		securitiesSearcher.setInstrumentType(InstrumentType.FIXED_INCOME.getCode());
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		//securitiesSearcher.setIssuerCode(null);
		//securitiesSearcher.setIsinCode(null);
		securitiesSearcher.setDescription(null);
		//securitiesSearcher.setStateSecuritie(null);
		ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.disableInstrument}", Boolean.class);
		//boolean blDisabledInstr = (Boolean) valueExpression.getValue(elContext);
		//if(!blDisabledInstr)
		//	
		securitiesSearcher.setSecuritiesType(null);
		securitiesSearcher.setClassSecuritie(null);
		securityMnemonic = "";
		securityDescription = "";
		listClassSecurities.clear();
		listSecuritiesType.clear();
		listEmpty = false;
		selected = null;
		listSecurities = null;
		//securitiesSearcher.setInstrumentType(null);
		
		//If instrument type is not null load combo security type
		if(Validations.validateIsNotNull(securitiesSearcher.getInstrumentType())){
			loadCboSecuritieTypeByInstrumentHandler();
		}
	}
	
	/**
	 * Exit.
	 *
	 * @param event the event
	 */
	public void exit(ActionEvent event) {
		clean(event);
	}
	
	/**
	 * Checks if is input filters.
	 *
	 * @param filter the filter
	 * @return true, if is input filters
	 */
	private boolean isInputFilters(SecuritiesSearcherTO filter) {
		boolean check = false;
		if (StringUtils.isNotBlank(filter.getIssuerCode())
				|| StringUtils.isNotBlank(filter.getDescription())
				|| filter.getInstrumentType() != null
				|| filter.getSecuritiesType() != null
				|| filter.getStateSecuritie() != null
				|| filter.getParticipantCode() != null) {
			check = true;
		}
		return check;
	}
	
	/**
	 * Load cbo securitie type by instrument handler.
	 */
	public void loadCboSecuritieTypeByInstrumentHandler(){
		try {
			if(Validations.validateIsNotNull(listClassSecurities)){
				listClassSecurities.clear();
			}
			if(Validations.validateIsNotNull(listSecuritiesType)){
				listSecuritiesType.clear();
			}
			if(Validations.validateIsNotNull(securitiesSearcher.getInstrumentType())){
				SecuritiesSearcherTO secSearcherTO = new SecuritiesSearcherTO();
				secSearcherTO.setInstrumentType(securitiesSearcher.getInstrumentType());
				listSecuritiesType = helperComponentFacade.searchSecurityTypesByInstrument(secSearcherTO);
			}else{
				listSecuritiesType.clear();
			}
		} catch (Exception e) {
			log.error(e.toString());
		}
	}
	
	/**
	 * Load cbo securitie class by securitie type handler.
	 */
	public void loadCboSecuritieClassBySecuritieTypeHandler(){
		List<ParameterTable> lstSecurityClass = new ArrayList<>();
		try {
			if(Validations.validateListIsNotNullAndNotEmpty(listClassSecurities)){
				listClassSecurities.clear();
			}
			if(Validations.validateIsNotNull(securitiesSearcher.getSecuritiesType())){								
				lstSecurityClass=generalParametersFacade.getListSecuritiesClassSetup(securitiesSearcher.getInstrumentType(), 
						sectorIssuer,
						securitiesSearcher.getSecuritiesType());
				listClassSecurities = lstSecurityClass;
			}	
		} catch (Exception e) {
			log.error(e.toString());
		}
	}
	
	/**
	 * Search securities, using in event onblur of helper.
	 *
	 * @throws ServiceException the service exception
	 */
	public void searchSecurities() throws ServiceException {
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		ValueExpression valueExpression = context.getApplication().getExpressionFactory()
		    .createValueExpression(elContext, "#{cc.attrs.value}", String.class);
		ValueExpression valCode = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.clientId}", String.class);
		
		ValueExpression warningMessageValueEx = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.resourceBundleMap.warningMessage}", String.class);
		String warningMessage= (String)warningMessageValueEx.getValue(elContext);
		
		ValueExpression securityHelpMessageValueEX = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.resourceBundleMap.securityHelpMessage}", String.class);
		String securityHelpMessage= (String)securityHelpMessageValueEX.getValue(elContext);
		
		ValueExpression valueExpressionName = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.name}", String.class);
		helperName = (String)valueExpressionName.getValue(elContext);
		
		String nameInput = valCode.getValue(elContext)+":"+"securityCodeHelper";
			if (valueExpression.getValue(elContext) instanceof String) {
				 String code = (String)valueExpression.getValue(elContext);
				 if (StringUtils.isNotBlank(code)) {
					 securitiesSearcher.setIsinCode(code);
					 selected = helperComponentFacade.findSecurityFromIsin(securitiesSearcher);
					 if (selected != null) {
						 securityDescription = selected.getDescription();
						 securityMnemonic = selected.getMnemonic();
						//new Value
						valueExpression.setValue(elContext, selected.getSecuritiesCode());
					 } else {
						 RequestContext.getCurrentInstance().execute("PF('cnfwMsgCustomValidation"+helperName+"').show();");
						 JSFUtilities.putViewMap("bodyMessageView", securityHelpMessage);
						 JSFUtilities.putViewMap("headerMessageView", warningMessage);
						 securityDescription = null;
						 securityMnemonic = null;
						 valueExpression.setValue(elContext, null);
					 }
					 
				 } else {
					 //error message
					 UIComponent uitextComp = UIComponent.getCurrentComponent(context);
					 if(uitextComp instanceof UIInput) {
						UIInput issuerText = (UIInput)uitextComp;
						issuerText.setValid(true);
					 }
					 valueExpression.setValue(elContext, null);
					 securityDescription = null;
					 securityMnemonic = null;
				 }
			 } else {
				 UIComponent uitextComp = UIComponent.getCurrentComponent(context);
				 if(uitextComp instanceof UIInput) {
					UIInput issuerText = (UIInput)uitextComp;
					issuerText.setValid(true);
				 }
				 valueExpression.setValue(elContext, null);
				 securityDescription = null;
				 securityMnemonic = null;
			 }	

		
	}
	
	/**
	 * Load combobox issuer.
	 */
	public void loadCboIssuerHandler(){
		try {
			if(Validations.validateListIsNotNullAndNotEmpty(listClassSecurities)){
				listClassSecurities.clear();
			}			
			if(Validations.validateIsNotNullAndNotEmpty(issuerPk)) {
				for (int i = 0; i < listIssuers.size(); i++) {
					ParameterTable issuerPT= listIssuers.get(i);
					if(issuerPT.getParameterName().equals(issuerPk)){
						sectorIssuer = issuerPT.getIndicator4();
						break;
					}
				}
			}			
		} catch (Exception e) {
			log.error(e.toString());
		}
	}

	//GETTERS AND SETTERS
	/**
	 * Gets the security description.
	 *
	 * @return the security description
	 */
	public String getSecurityDescription() {
		return securityDescription;
	}

	/**
	 * Sets the security description.
	 *
	 * @param securityDescription the new security description
	 */
	public void setSecurityDescription(String securityDescription) {
		this.securityDescription = securityDescription;
	}

	/**
	 * Gets the security mnemonic.
	 *
	 * @return the security mnemonic
	 */
	public String getSecurityMnemonic() {
		return securityMnemonic;
	}

	/**
	 * Sets the security mnemonic.
	 *
	 * @param securityMnemonic the new security mnemonic
	 */
	public void setSecurityMnemonic(String securityMnemonic) {
		this.securityMnemonic = securityMnemonic;
	}

	/**
	 * Gets the securities searcher.
	 *
	 * @return the securities searcher
	 */
	public SecuritiesSearcherTO getSecuritiesSearcher() {
		return securitiesSearcher;
	}

	/**
	 * Sets the securities searcher.
	 *
	 * @param securitiesSearcher the new securities searcher
	 */
	public void setSecuritiesSearcher(SecuritiesSearcherTO securitiesSearcher) {
		this.securitiesSearcher = securitiesSearcher;
	}

	/**
	 * Gets the selected.
	 *
	 * @return the selected
	 */
	public SecuritiesTO getSelected() {
		return selected;
	}

	/**
	 * Sets the selected.
	 *
	 * @param selected the new selected
	 */
	public void setSelected(SecuritiesTO selected) {
		this.selected = selected;
	}

	/**
	 * Gets the list securities.
	 *
	 * @return the list securities
	 */
	public List<SecuritiesTO> getListSecurities() {
		return listSecurities;
	}

	/**
	 * Sets the list securities.
	 *
	 * @param listSecurities the new list securities
	 */
	public void setListSecurities(List<SecuritiesTO> listSecurities) {
		this.listSecurities = listSecurities;
	}

	/**
	 * Checks if is searched.
	 *
	 * @return true, if is searched
	 */
	public boolean isSearched() {
		return searched;
	}

	/**
	 * Sets the searched.
	 *
	 * @param searched the new searched
	 */
	public void setSearched(boolean searched) {
		this.searched = searched;
	}

	/**
	 * Gets the list states.
	 *
	 * @return the list states
	 */
	public List<SelectItem> getListStates() {
		return listStates;
	}

	/**
	 * Sets the list states.
	 *
	 * @param listStates the new list states
	 */
	public void setListStates(List<SelectItem> listStates) {
		this.listStates = listStates;
	}

	/**
	 * Gets the list instrument type.
	 *
	 * @return the list instrument type
	 */
	/*public List<ParameterTable> getListInstrumentType() {
		return listInstrumentType;
	}*/

	/**
	 * Sets the list instrument type.
	 *
	 * @param listInstrumentType the new list instrument type
	 */
	/*public void setListInstrumentType(List<ParameterTable> listInstrumentType) {
		this.listInstrumentType = listInstrumentType;
	}*/

	public List<SelectItem> getListInstrumentType() {
		return listInstrumentType;
	}
	
	public void setListInstrumentType(List<SelectItem> listInstrumentType) {
		this.listInstrumentType = listInstrumentType;
	}
	
	/**
	 * Gets the list securities type.
	 *
	 * @return the list securities type
	 */
	public List<ParameterTable> getListSecuritiesType() {
		return listSecuritiesType;
	}

	/**
	 * Sets the list securities type.
	 *
	 * @param listSecuritiesType the new list securities type
	 */
	public void setListSecuritiesType(List<ParameterTable> listSecuritiesType) {
		this.listSecuritiesType = listSecuritiesType;
	}

	/**
	 * Gets the list participants.
	 *
	 * @return the list participants
	 */
	public List<SelectItem> getListParticipants() {
		return listParticipants;
	}

	/**
	 * Sets the list participants.
	 *
	 * @param listParticipants the new list participants
	 */
	public void setListParticipants(List<SelectItem> listParticipants) {
		this.listParticipants = listParticipants;
	}

	/**
	 * Gets the list issuers.
	 *
	 * @return the list issuers
	 */
	public List<ParameterTable> getListIssuers() {
		return listIssuers;
	}

	/**
	 * Sets the list issuers.
	 *
	 * @param listIssuers the new list issuers
	 */
	public void setListIssuers(List<ParameterTable> listIssuers) {
		this.listIssuers = listIssuers;
	}

	/**
	 * Checks if is list empty.
	 *
	 * @return true, if is list empty
	 */
	public boolean isListEmpty() {
		return listEmpty;
	}

	/**
	 * Sets the list empty.
	 *
	 * @param listEmpty the new list empty
	 */
	public void setListEmpty(boolean listEmpty) {
		this.listEmpty = listEmpty;
	}

	/**
	 * Gets the list class securities.
	 *
	 * @return the list class securities
	 */
	public List<ParameterTable> getListClassSecurities() {
		return listClassSecurities;
	}

	/**
	 * Sets the list class securities.
	 *
	 * @param listClassSecurities the new list class securities
	 */
	public void setListClassSecurities(List<ParameterTable> listClassSecurities) {
		this.listClassSecurities = listClassSecurities;
	}

	/**
	 * Checks if is disabled participant.
	 *
	 * @return true, if is disabled participant
	 */
	public boolean isDisabledParticipant() {
		return disabledParticipant;
	}

	/**
	 * Sets the disabled participant.
	 *
	 * @param disabledParticipant the new disabled participant
	 */
	public void setDisabledParticipant(boolean disabledParticipant) {
		this.disabledParticipant = disabledParticipant;
	}

	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	public boolean isDisabledIssuer() {
		return disabledIssuer;
	}

	public void setDisabledIssuer(boolean disabledIssuer) {
		this.disabledIssuer = disabledIssuer;
	}

	public String getIssuerPk() {
		return issuerPk;
	}

	public void setIssuerPk(String issuerPk) {
		this.issuerPk = issuerPk;
	}

	public String getIssuerDescription() {
		return issuerDescription;
	}

	public void setIssuerDescription(String issuerDescription) {
		this.issuerDescription = issuerDescription;
	}

	/**
	 * @return the sectorIssuer
	 */
	public Integer getSectorIssuer() {
		return sectorIssuer;
	}

	/**
	 * @param sectorIssuer the sectorIssuer to set
	 */
	public void setSectorIssuer(Integer sectorIssuer) {
		this.sectorIssuer = sectorIssuer;
	}

	/**
	 * @return the maxResultsQueryHelper
	 */
	public Integer getMaxResultsQueryHelper() {
		return maxResultsQueryHelper;
	}

	/**
	 * @param maxResultsQueryHelper the maxResultsQueryHelper to set
	 */
	public void setMaxResultsQueryHelper(Integer maxResultsQueryHelper) {
		this.maxResultsQueryHelper = maxResultsQueryHelper;
	}
	
	
	
}
