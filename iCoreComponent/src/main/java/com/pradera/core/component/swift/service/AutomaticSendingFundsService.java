package com.pradera.core.component.swift.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.swift.to.RetirementProcess;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounts.holderaccounts.HolderAccountBank;
import com.pradera.model.funds.FundsOperationType;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.type.AccountCashFundsType;
import com.pradera.model.funds.type.BankAccountsStateType;
import com.pradera.model.funds.type.BankType;
import com.pradera.model.funds.type.CashAccountStateType;
import com.pradera.model.funds.type.DetailCashAccountStateType;
import com.pradera.model.funds.type.FundsType;
import com.pradera.model.generalparameter.type.MasterTableType;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AutomaticSendingFundsService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01-sep-2015
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class AutomaticSendingFundsService extends CrudDaoServiceBean{

	/**
	 * METHOD WHICH WILL GET CEVALDOM BIC CODE.
	 *
	 * @return the cevaldom bic code
	 */
	public String getCevaldomBicCode(){
		String bicCode = StringUtils.EMPTY;
		TypedQuery<String> query = em.createQuery(
				"SELECT p.bicCode FROM Participant p " +
				"WHERE " +
				"p.state = :state " +
				"and p.indParticipantDepository = :indDepositary " +
				"and p.idUseType = :useType ", 
				String.class);

		query.setParameter("state",Integer.valueOf(MasterTableType.PARAMETER_TABLE_PARTICIPANT_STATE_REGISTER.getCode()));
		query.setParameter("indDepositary",FundsType.IND_DEPOSITARY_PARTICIPANT.getCode());
		query.setParameter("useType",MasterTableType.PARAMETER_TABLE_PARTICIPANT_ACCOUNT_OPERATIONS.getCode());
		bicCode = query.getSingleResult();

		return bicCode;
	}

	/**
	 * METHOD WHICH WILL GET THE THE BIC CODE OF A SPECIFIC PARTICIPANT.
	 *
	 * @param participantPK the participant pk
	 * @return the participant bic code
	 */
	public String getParticipantBicCode(Long participantPK){
		String bicCode = StringUtils.EMPTY;
		TypedQuery<String> query = em.createQuery(
				"SELECT p.bicCode FROM Participant p " +
				"WHERE " +
				"p.idParticipantPk = :participant ", 
				String.class);

		query.setParameter("participant",participantPK);
		bicCode = query.getSingleResult();

		return bicCode;
	}

	/**
	 * METHOD WHICH WILL GET THE BCRD BIC CODE.
	 *
	 * @return the BCRD bic code
	 */
	public String getBCRDBicCode(){
		String bicCode = StringUtils.EMPTY;
		TypedQuery<String> query = em.createQuery(
				"SELECT b.bicCode FROM Bank b " +
				"WHERE " +
				"b.bankType = :bankType " +
				"and b.state = :state ", 
				String.class);

		query.setParameter("bankType",BankType.CENTRALIZING.getCode());
		query.setParameter("state",MasterTableType.PARAMETER_TABLE_BANK_STATE_REGISTERED.getLngCode());

		bicCode = query.getSingleResult();

		return bicCode;
	}

	/**
	 * METHOD WHICH WILL GET THE TRN ACCORDING TO THE FUND OPERATION TYPE AND THE CURRENCY.
	 *
	 * @param fundOperationPK the fund operation pk
	 * @param currency the currency
	 * @return the operation trn
	 */
	public String getOperationTrn(Long fundOperationPK, Integer currency){
		String bicCode = StringUtils.EMPTY;
		TypedQuery<String> query = em.createQuery(
				"SELECT s.trnField FROM SwiftComponent s " +
				"WHERE " +
				"s.fundsOperationType.idFundsOperationTypePk = :fundOperation " +
				"and s.currency = :currency ", 
				String.class);

		query.setParameter("fundOperation",fundOperationPK);
		query.setParameter("currency",currency);
		bicCode = query.getSingleResult();

		return bicCode;
	}

	/**
	 * METHOD WHICH GET THE BANK ACCOUNT NUMBER OF THE PARTICIPANT BUYER.
	 *
	 * @param participantBuyer the participant buyer
	 * @param currency the currency
	 * @param mechanism the mechanism
	 * @param modalityGroup the modality group
	 * @return the bank account number
	 */
	public String getBankAccountNumber(Long participantBuyer, Integer currency, Long mechanism, Long modalityGroup){
		String bankAccountNumber = StringUtils.EMPTY;

		String strQuery = "select b.accountNumber " +
		"from InstitutionCashAccount c, InstitutionBankAccount b " +
		"where " +
		"b.institutionCashAccount.idInstitutionCashAccountPk = c.idInstitutionCashAccountPk " +
		"and b.currency = :currency " +
		"and b.state = :bankState " +
		"and c.accountState = :accountState " +
		"and c.accountType = :accountType " +
		"and c.indRelatedBcrd = :bcrdInd ";

		if(Validations.validateIsNotNull(mechanism)){
			strQuery = strQuery.concat("and c.negotiationMechanism.idNegotiationMechanismPk = :mechanism ");
		}
		if(Validations.validateIsNotNull(modalityGroup)){
			strQuery = strQuery.concat("and c.modalityGroup.idModalityGroupPk = :modalityGroup ");
		}

		TypedQuery<String> query = em.createQuery(strQuery,String.class);
		query.setParameter("currency",Long.valueOf(currency));
		query.setParameter("bankState",Long.valueOf(CashAccountStateType.REGISTERED.getCode().toString()));
		query.setParameter("accountState",CashAccountStateType.REGISTERED.getCode());
		query.setParameter("accountType",AccountCashFundsType.SETTLEMENT.getCode());
		query.setParameter("bcrdInd",FundsType.CASH_ACCOUNT_IND_BCRD_RELATION.getLngCode());
		if(Validations.validateIsNotNull(mechanism)){
			query.setParameter("mechanism",mechanism);
		}
		if(Validations.validateIsNotNull(modalityGroup)){
			query.setParameter("modalityGroup",modalityGroup);
		}

		bankAccountNumber = query.getSingleResult();

		return bankAccountNumber;
	}

	/**
	 * METHOD WHICH WILL GET THE BANK ACCOUNT NUMBER ACCORDING THE CURRENCY AND CASH ACCOUNT TYPE IN COMMERCIAL BANK.
	 *
	 * @param currency the currency
	 * @param cashAccountType the cash account type
	 * @return the account number by type currency
	 */
	public InstitutionBankAccount getAccountNumberByTypeCurrency(Integer currency, Long cashAccountType){
		InstitutionBankAccount bank = null;
		TypedQuery<InstitutionBankAccount> query = em.createQuery(
				"select b " +
				"from " +
				"InstitutionCashAccount c, InstitutionBankAccount b " +
				"where " +
				"c.idInstitutionCashAccountPk = b.institutionCashAccount.idInstitutionCashAccountPk " +
				"and c.accountType = :cashAccountType " +
				"and c.indRelatedBcrd = :indBCRD " +
				"and c.currency = :currency " +
				"and c.accountState = :state ", 
				InstitutionBankAccount.class);

		query.setParameter("currency",Long.valueOf(currency));
		query.setParameter("cashAccountType",cashAccountType);
		query.setParameter("indBCRD",FundsType.CASH_ACCOUNT_IND_BCRD_NO_RELATION.getLngCode());
		query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());

		bank = query.getSingleResult();
		return bank;
	}

	/**
	 * METHOD WHICH WILL GET THE BANK ACCOUNT NUMBER OF CEVALDOM FOR CURRENCY IN CENTRAL ACCOUNT.
	 *
	 * @param currency the currency
	 * @param centralBankPK the central bank pk
	 * @return the cevaldom bcrd account number
	 */
	public String getCevaldomBCRDAccountNumber(Integer currency, Long centralBankPK){
		String bankAccountNumber =  StringUtils.EMPTY;
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT CAD.institutionBankAccount.accountNumber FROM CashAccountDetail CAD ");
		stringBuffer.append(" WHERE CAD.institutionBankAccount.providerBank.idBankPk = :centralBankPK ");
		stringBuffer.append(" and CAD.institutionBankAccount.state = :ibaState ");
		stringBuffer.append(" and CAD.institutionCashAccount.accountType = :accountType ");
		stringBuffer.append(" and CAD.institutionCashAccount.currency = :currency ");
		stringBuffer.append(" and CAD.institutionCashAccount.accountState = :icaState ");
		stringBuffer.append(" and CAD.institutionCashAccount.indRelatedBcrd = :indicatorZero ");
		stringBuffer.append(" and CAD.accountState = :cadState ");
		
		TypedQuery<String> query = em.createQuery(stringBuffer.toString(), String.class);

		query.setParameter("currency",currency);
		query.setParameter("accountType",AccountCashFundsType.CENTRALIZING.getCode());
		query.setParameter("indicatorZero",BooleanType.NO.getCode());
		query.setParameter("icaState",CashAccountStateType.ACTIVATE.getCode());
		query.setParameter("ibaState",BankAccountsStateType.REGISTERED.getCode());
		query.setParameter("cadState",DetailCashAccountStateType.ACTIVATE.getCode());
		query.setParameter("centralBankPK",centralBankPK);

		bankAccountNumber = query.getSingleResult();
		return bankAccountNumber;
	}

	/**
	 * METHOD TO GET THE RESPECTIVE DEPOSITARY DESCRIPTION.
	 *
	 * @return the depositary description
	 */
	public String getDepositaryDescription(){
		String description = StringUtils.EMPTY;

		String queryStr = "SELECT p.description FROM Participant p " +
				"where " +
				"p.indParticipantDepository = :indDepositary " +
				"and p.state = :state";

		TypedQuery<String> query = em.createQuery(queryStr,String.class);
		query.setParameter("indDepositary",FundsType.IND_DEPOSITARY_PARTICIPANT.getCode());
		query.setParameter("state",Integer.valueOf(MasterTableType.PARAMETER_TABLE_PARTICIPANT_STATE_REGISTER.getLngCode().toString()));
		description = query.getSingleResult();

		return description;
	}

	/**
	 * METHOD WHICH GET THE COMMERCIAL BANK ACCOUNT OF AN ISSUER.
	 *
	 * @param currency the currency
	 * @param issuer the issuer
	 * @param cashAccountType the cash account type
	 * @return the account number currency issuer
	 */
	public InstitutionBankAccount getAccountNumberCurrencyIssuer(Integer currency, String issuer,Integer cashAccountType){
		InstitutionBankAccount commercialBank = null;
		TypedQuery<InstitutionBankAccount> query = em.createQuery(
				"select b " +
				"from " +
				"InstitutionCashAccount c, InstitutionBankAccount b " +
				"where " +
				"c.idInstitutionCashAccountPk = b.institutionCashAccount.idInstitutionCashAccountPk " +
				"and c.accountType = :cashAccountType " +
				"and c.indRelatedBcrd = :indBCRD " +
				"and c.currency = :currency " +
				"and c.accountState = :state " +
				"and c.issuer.idIssuerPk = :issuer  ", 
				InstitutionBankAccount.class);

		query.setParameter("currency",Long.valueOf(currency));
		query.setParameter("cashAccountType",cashAccountType);
		query.setParameter("indBCRD",FundsType.CASH_ACCOUNT_IND_BCRD_NO_RELATION.getLngCode());
		query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
		query.setParameter("issuer",issuer);

		commercialBank = query.getSingleResult();
		return commercialBank;
	}

	/**
	 * METHOD WITH WILL GET THE INSTITUTION CASH ACCOUNT RELATED TO THE RETIREMENT OR DEPOSIT.
	 *
	 * @param fundOpeType the fund ope type
	 * @param retirementObject the retirement object
	 * @return the related cash account
	 */
	public Long getRelatedCashAccount(FundsOperationType fundOpeType, RetirementProcess retirementObject){
		Long institutionCashAccountPK = null;
		StringBuilder queryStr = new StringBuilder("");
		queryStr.append("select i.idInstitutionCashAccountPk ").
		append("from ").
		append("InstitutionCashAccount i, FundsOperationType f ").
		append("where ").
		append("f.cashAccountType = i.accountType ").
		append("and f.idFundsOperationTypePk = :fundOpeType ").
		append("and i.accountState = :state ").
		append("and i.currency = :currency ");
		if(Validations.validateIsNotNullAndNotEmpty(retirementObject.getParticipantBuyerPK())){
			queryStr.append("and i.participant.idParticipantPk = :participant ");
		}else if(Validations.validateIsNotNullAndNotEmpty(retirementObject.getParticipantPK())){
			queryStr.append("and i.participant.idParticipantPk = :participant ");
		}else if(Validations.validateIsNotNullAndNotEmpty(retirementObject.getIssuerPK())){
			queryStr.append("and i.issuer.idIssuerPk = :issuer ");
		}else if(Validations.validateIsNotNullAndNotEmpty(retirementObject.getMechanism())){
			queryStr.append("and i.negotiationMechanism.idNegotiationMechanismPk = :mechanism ");
		}else if(Validations.validateIsNotNullAndNotEmpty(retirementObject.getModalityGroup())){
			queryStr.append("and i.modalityGroup.idModalityGroupPk = :modalityGroup ");
		}
		TypedQuery<Long> query = em.createQuery(queryStr.toString(),Long.class);
		if(Validations.validateIsNotNullAndNotEmpty(retirementObject.getParticipantBuyerPK())){
			query.setParameter("participant",retirementObject.getParticipantBuyerPK());
		}else if(Validations.validateIsNotNullAndNotEmpty(retirementObject.getParticipantPK())){
			query.setParameter("participant",retirementObject.getParticipantPK());
		}else if(Validations.validateIsNotNullAndNotEmpty(retirementObject.getIssuerPK())){
			query.setParameter("issuer",retirementObject.getIssuerPK());
		}else if(Validations.validateIsNotNullAndNotEmpty(retirementObject.getMechanism())){
			query.setParameter("mechanism",retirementObject.getMechanism());
		}else if(Validations.validateIsNotNullAndNotEmpty(retirementObject.getModalityGroup())){
			query.setParameter("modalityGroup",retirementObject.getModalityGroup());
		}
		institutionCashAccountPK = query.getSingleResult();

		return institutionCashAccountPK;
	}

	/**
	 * METHOD WHICH WILL GET THE AUTOMATIC FUND OPERATION RELATED TO A FIRST ONE.
	 *
	 * @param originFundOperationType the origin fund operation type
	 * @return the deposit operation from orig ope
	 */
	public FundsOperationType getDepositOperationFromOrigOpe(Long originFundOperationType){
		FundsOperationType manualDepositOperation = null;

		TypedQuery<FundsOperationType> query = em.createQuery(
				"select f.fundsOperationType " +
				"from " +
				"FundsOperationType f " +
				"where " +
				"f.idFundsOperationTypePk = :opePk ",FundsOperationType.class);

		query.setParameter("opePk", originFundOperationType);
		manualDepositOperation = query.getSingleResult();
		return manualDepositOperation;
	}

	/**
	 * METHOD WHICH WILL GET THE BANK ACCOUNT NUMBER PER HOLDER-BANK.
	 *
	 * @param holderPK the holder pk
	 * @param bankPK the bank pk
	 * @return the holder account number
	 */
	public HolderAccountBank getHolderAccountNumber(Long holderPK, Long bankPK){
		HolderAccountBank holderAccount = null;

		TypedQuery<HolderAccountBank> query = em.createQuery(
				"select h " +
				"from HolderAccountBank h " +
				"where " +
				"h.holder.idHolderPk = :holder and " +
				"h.bank.idBankPk = :bank",HolderAccountBank.class);

		query.setParameter("holder",holderPK);
		query.setParameter("bank",bankPK);
		holderAccount = query.getSingleResult();

		return holderAccount;
	}

	/**
	 * METHOD WHICH GET THE CURRENCY CODE FOR SWIFT MESSAGE.
	 *
	 * @param currency the currency
	 * @return the currency code
	 */
	public String getCurrencyCode(Integer currency){
		String code = StringUtils.EMPTY;

		TypedQuery<String> query = em.createQuery(
				"select p. " +
				"from ParameterTable p " +
				"where " +
				"p.parameterTablePk = :currency",String.class);

		query.setParameter("currency", currency);
		code = query.getSingleResult();

		return code;
	}

	/**
	 * METHOD WHICH WILL RETURN THE MESSAGE TYPE, ACCORDING TO THE FUNDS OPERATION TYPE AND CURRENCY.
	 *
	 * @param fundOperationType the fund operation type
	 * @param currency the currency
	 * @return the message type
	 */
	public String getMessageType(Long fundOperationType, Integer currency){
		String messageType = StringUtils.EMPTY;
		TypedQuery<Integer> query = em.createQuery(
				"select s.swiftMessageType " +
				"from SwiftComponent s " +
				"where " +
				"s.currency = :currency and " +
				"s.fundsOperationType.idFundsOperationTypePk = :fundOpeType ",Integer.class);
		query.setParameter("currency", currency);
		query.setParameter("fundOpeType", fundOperationType);
		messageType = query.getSingleResult().toString();

		return messageType;
	}

	/**
	 * METHOD WHICH WILL GET THE COMMERCIAL ACCOUNT NUMBER OF BANK.
	 *
	 * @param currency the currency
	 * @return the commercial bank account number
	 */
	public InstitutionBankAccount getCommercialBankAccountNumber(Integer currency){
		InstitutionBankAccount bankAccount = new InstitutionBankAccount();
		try{
			StringBuffer stringBuffer= new StringBuffer();
			stringBuffer.append(" SELECT CAD.institutionBankAccount FROM CashAccountDetail CAD ");
			stringBuffer.append(" WHERE CAD.institutionCashAccount.currency = :currency ");
			stringBuffer.append(" and CAD.institutionBankAccount.state = :ibaState ");
			stringBuffer.append(" and CAD.institutionCashAccount.accountType = :accountType ");
			stringBuffer.append(" and CAD.institutionCashAccount.accountState = :icaState ");
			stringBuffer.append(" and CAD.institutionCashAccount.indRelatedBcrd = :indicatorZero ");
			stringBuffer.append(" and CAD.accountState = :cadState ");

			TypedQuery<InstitutionBankAccount> query = em.createQuery(stringBuffer.toString(),InstitutionBankAccount.class);
			query.setParameter("accountType",AccountCashFundsType.RETURN.getCode());
			query.setParameter("currency",currency);
			query.setParameter("indicatorZero",BooleanType.NO.getCode());
			query.setParameter("icaState",CashAccountStateType.ACTIVATE.getCode());
			query.setParameter("ibaState",BankAccountsStateType.REGISTERED.getCode());
			query.setParameter("cadState",DetailCashAccountStateType.ACTIVATE.getCode());
			bankAccount = query.getSingleResult();
		}catch(NoResultException x){
			x.printStackTrace();
		}
		return bankAccount;
	}

	/**
	 * METHOD WHICH WILL GET THE COMMERCIAL ACCOUNT NUMBER OF BANK.
	 *
	 * @param currency the currency
	 * @return the commercial bank account number return
	 */
	public Object[] getCommercialBankAccountNumberReturn(Integer currency){
		Object[] objReturn = null;
		try{
			String strQuery = 	"select b.accountNumber,ba.description,ba.bicCode " + 
					"from " +
					"InstitutionCashAccount c, InstitutionBankAccount b, Bank ba " +
					"where " +
					"c.idInstitutionCashAccountPk = b.institutionCashAccount.idInstitutionCashAccountPk " +
					"and b.providerBank.idBankPk = ba.idBankPk " +
					"and c.accountType = :cashAccountType " +
					"and c.indRelatedBcrd = :indCentral " +
					"and c.currency = :currency " +
					"and b.currency = :bankCurrency " +
					"and c.accountState = :state " +
					"and b.state = :bankAccountState ";

			TypedQuery<Object[]> query = em.createQuery(strQuery,Object[].class);
			query.setParameter("cashAccountType",AccountCashFundsType.RETURN.getCode());
			query.setParameter("indCentral",BooleanType.NO.getCode());
			query.setParameter("currency",currency);
			query.setParameter("bankCurrency",currency);
			query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
			query.setParameter("bankAccountState",new Long(683));
			objReturn = query.getSingleResult();
		}catch(NoResultException x){
			x.printStackTrace();
		}
		return objReturn;
	}
}