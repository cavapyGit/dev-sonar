package com.pradera.core.component.helperui.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class HolderAccountHelperResultTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 24/09/2013
 */
public class HolderAccountHelperResultTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5504580903443533129L;
	
	/** The account pk. */
	private Long accountPk;
	
	/** The account number. */
	private Integer accountNumber;
	
	/** The alternate code. */
	private String  alternateCode;
	
	/** The account group. */
	private Integer  accountGroup;
	
	/** The holder accountType. */
	private String  accountType;

	/** The holder description. */
	private String  holderDescription;
	
	/** The account status description. */
	private String  accountStatusDescription;
	
	/** The holder account. */
	private HolderAccount holderAccount;
	
	private boolean disabled;
	
	/** The holder description list. */
	private List<String> holderDescriptionList;
	
	private Participant participant;
	
	/** The account number. */
	private Integer stateAccount;
	
	private String relatedName;
	
	public String getRelatedName() {
		return relatedName;
	}

	public void setRelatedName(String relatedName) {
		this.relatedName = relatedName;
	}

	/** The alternative code for user issuer */
	private String alternativeCodeIssuer;
	//GETTERS AND SETTERS
	
	/**
	 * Gets the alternate code.
	 *
	 * @return the alternate code
	 */
	public String getAlternateCode() {
		return alternateCode;
	}
	
	/**
	 * Sets the alternate code.
	 *
	 * @param alternateCode the new alternate code
	 */
	public void setAlternateCode(String alternateCode) {
		this.alternateCode = alternateCode;
	}
	
	/**
	 * Gets the holder description.
	 *
	 * @return the holder description
	 */
	public String getHolderDescription() {
		return holderDescription;
	}
	
	/**
	 * Sets the holder description.
	 *
	 * @param holderDescription the new holder description
	 */
	public void setHolderDescription(String holderDescription) {
		this.holderDescription = holderDescription;
	}
	
	/**
	 * Gets the account status description.
	 *
	 * @return the account status description
	 */
	public String getAccountStatusDescription() {
		return accountStatusDescription;
	}
	
	/**
	 * Sets the account status description.
	 *
	 * @param accountStatusDescription the new account status description
	 */
	public void setAccountStatusDescription(String accountStatusDescription) {
		this.accountStatusDescription = accountStatusDescription;
	}
	
	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}
	
	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}
	
	/**
	 * Gets the holder description list.
	 *
	 * @return the holder description list
	 */
	public List<String> getHolderDescriptionList() {
		return holderDescriptionList;
	}
	
	/**
	 * Sets the holder description list.
	 *
	 * @param holderDescriptionList the new holder description list
	 */
	public void setHolderDescriptionList(List<String> holderDescriptionList) {
		this.holderDescriptionList = holderDescriptionList;
	}
	
	/**
	 * Gets the account pk.
	 *
	 * @return the account pk
	 */
	public Long getAccountPk() {
		return accountPk;
	}
	
	/**
	 * Sets the account pk.
	 *
	 * @param accountPk the new account pk
	 */
	public void setAccountPk(Long accountPk) {
		this.accountPk = accountPk;
	}
	
	/**
	 * Gets the account group.
	 *
	 * @return the account group
	 */
	public Integer getAccountGroup() {
		return accountGroup;
	}
	
	/**
	 * Sets the account group.
	 *
	 * @param accountGroup the new account group
	 */
	public void setAccountGroup(Integer accountGroup) {
		this.accountGroup = accountGroup;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public Integer getStateAccount() {
		return stateAccount;
	}

	public void setStateAccount(Integer stateAccount) {
		this.stateAccount = stateAccount;
	}
	
	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public Integer getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the alternativeCodeIssuer
	 */
	public String getAlternativeCodeIssuer() {
		return alternativeCodeIssuer;
	}

	/**
	 * @param strAlternativeCodeIssuer the strAlternativeCodeIssuer to set
	 */
	public void setAlternativeCodeIssuer(String alternativeCodeIssuer) {
		this.alternativeCodeIssuer = alternativeCodeIssuer;
	}

}
