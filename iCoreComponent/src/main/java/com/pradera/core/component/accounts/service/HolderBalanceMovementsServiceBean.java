package com.pradera.core.component.accounts.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericPropertiesConstants;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.accounts.to.HolderBalanceMovementsTO;
import com.pradera.core.component.accounts.to.RepresentedEntityTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.GeographicLocationServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.component.accounts.to.HolderAccountObjectTO;
import com.pradera.integration.component.accounts.to.JuridicHolderObjectTO;
import com.pradera.integration.component.accounts.to.NaturalHolderObjectTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.custody.to.AccountBalanceQueryTO;
import com.pradera.integration.component.securities.to.SecurityObjectTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.LegalRepresentative;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.RepresentedEntity;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.accounts.type.RepresentativeEntityStateType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountMovement;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Security;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class GeographicLocationServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 25/01/2013
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class HolderBalanceMovementsServiceBean extends CrudDaoServiceBean{
	
	/** The parameter service bean. */
	@EJB
	private ParameterServiceBean parameterServiceBean;
	
	/** The participant service bean. */
	@EJB
	private ParticipantServiceBean participantServiceBean;
	
	/** The geographic location service bean. */
	@EJB
	private GeographicLocationServiceBean geographicLocationServiceBean;
	
	
	/**
	 * Gets the list geographic location.
	 *
	 * @param filter the filter
	 * @return the list geographic location
	 * @throws ServiceException the service exception
	 */    
	public HolderAccountBalance getBalanceHolderFromSecurity(HolderBalanceMovementsTO filter) throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("Select HAB From HolderAccountBalance HAB");
			sbQuery.append(" where HAB.participant.idParticipantPk=" + filter.getObjParticipant().getIdParticipantPk());
			sbQuery.append(" and HAB.holderAccount.idHolderAccountPk=" + filter.getAccountNumber());
			sbQuery.append(" and HAB.security.idIsinCodePk="+ filter.getIdIsinCodePk());
					
			Query query = em.createQuery(sbQuery.toString());
		
			return (HolderAccountBalance) query.getSingleResult();
		
		} catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}
		
		
		
	}
	
	/**
	 * Holder account balance service bean.
	 *
	 * @param holderAccountBalanceTO the holder account balance to
	 * @return the holder account balance
	 * @throws ServiceException the service exception
	 */
	public HolderAccountBalance holderAccountBalanceServiceBean(HolderAccountBalanceTO holderAccountBalanceTO) throws ServiceException{
		StringBuilder jpqlQuery = new StringBuilder();
		jpqlQuery.append("SELECT hab.TOTAL_BALANCE, hab.AVAILABLE_BALANCE, hab.PAWN_BALANCE, ");
		jpqlQuery.append("hab.BAN_BALANCE, hab.OTHER_BLOCK_BALANCE, hab.SALE_BALANCE, hab.PURCHASE_BALANCE, hab.REPORTED_BALANCE, hab.REPORTING_BALANCE, ");
		jpqlQuery.append("hab.MARGIN_BALANCE, hab.TRANSIT_BALANCE, hab.ACCREDITATION_BALANCE, hab.RESERVE_BALANCE, hab.OPPOSITION_BALANCE ");
		jpqlQuery.append("FROM HOLDER_ACCOUNT_BALANCE hab ");
		jpqlQuery.append("WHERE hab.ID_PARTICIPANT_PK = :idParticipantParam ");
		jpqlQuery.append(" AND hab.ID_HOLDER_ACCOUNT_PK = :idHolderAccountParam ");
		jpqlQuery.append(" AND hab.ID_SECURITY_CODE_PK = :idSecurityCodeParam");
		Query queryJpql = em.createNativeQuery(jpqlQuery.toString());
		
		queryJpql.setParameter("idParticipantParam", holderAccountBalanceTO.getIdParticipant());
		queryJpql.setParameter("idHolderAccountParam", holderAccountBalanceTO.getIdHolderAccount());
		queryJpql.setParameter("idSecurityCodeParam", holderAccountBalanceTO.getIdSecurityCode());
		
		Object[] objectData = null;
		
		try{
			objectData = (Object[]) queryJpql.getSingleResult();
		}catch(NoResultException ex){
			return null;
		}
		HolderAccountBalance holderAccountBalance = new HolderAccountBalance();
		
		holderAccountBalance.setTotalBalance((BigDecimal)objectData[0]);
		holderAccountBalance.setAvailableBalance((BigDecimal)objectData[1]);
		holderAccountBalance.setPawnBalance((BigDecimal)objectData[2]);
		holderAccountBalance.setBanBalance((BigDecimal)objectData[3]);
		holderAccountBalance.setOtherBlockBalance((BigDecimal)objectData[4]);
		holderAccountBalance.setSaleBalance((BigDecimal)objectData[5]);
		holderAccountBalance.setPurchaseBalance((BigDecimal)objectData[6]);
		holderAccountBalance.setReportedBalance((BigDecimal)objectData[7]);
		holderAccountBalance.setReportingBalance((BigDecimal)objectData[8]);
		holderAccountBalance.setMarginBalance((BigDecimal)objectData[9]);
		holderAccountBalance.setTransitBalance((BigDecimal)objectData[10]);
		holderAccountBalance.setAccreditationBalance((BigDecimal)objectData[11]);
		holderAccountBalance.setReserveBalance((BigDecimal)objectData[12]);
		holderAccountBalance.setOppositionBalance((BigDecimal)objectData[13]);
		
		return holderAccountBalance;
	}

	/**
	 * Gets the balance holder.
	 *
	 * @param filter the filter
	 * @return the balance holder
	 */
	public List<HolderAccountBalance> getBalanceHolder(HolderBalanceMovementsTO filter) {
		// TODO Auto-generated method stub
		return null;
	}
	
//	public boolean verifyHolderAccountBalanceByHolder(Holder holder){
//		boolean indBalance=false;
//		StringBuilder stringBuilderSql = new StringBuilder();
//		stringBuilderSql.append("select count(*)");		
//		stringBuilderSql.append(" from");
//		stringBuilderSql.append(" HolderAccountBalance hab join hab.holderAccount ha");
//		stringBuilderSql.append(" join ha.holderAccountDetails had");
//		stringBuilderSql.append(" join had.holder h");
//		stringBuilderSql.append(" where h.idHolderPk = :idHolderPk");
//		Query query = em.createQuery(stringBuilderSql.toString());
//		query.setParameter("idHolderPk", holder.getIdHolderPk());
//	    if(Integer.valueOf(query.getSingleResult().toString()).intValue()>0){
//	    	indBalance = true;
//	    }
//	    else{
//	    	indBalance = false;
//	    }
//	    
//	    return indBalance;
//	}
	
	/**
	 * Gets the holder account balance for dpf's.
	 *
	 * @param filter the filter
	 * @return the object holder account balance
	 * @throws ServiceException the service exception
	 */    
	public HolderAccountBalance getHolderAccountBalance(HolderBalanceMovementsTO filter) throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" Select HAB From HolderAccountBalance HAB ");
			sbQuery.append(" where HAB.participant.idParticipantPk = :parIdParticipantPk ");			
			sbQuery.append(" and HAB.security.idSecurityCodePk = :parIdSecurityCodePk ");					
			Query query = em.createQuery(sbQuery.toString());	
			query.setParameter("parIdParticipantPk", filter.getObjParticipant().getIdParticipantPk());
			query.setParameter("parIdSecurityCodePk", filter.getIdIsinCodePk());
			return (HolderAccountBalance) query.getSingleResult();		
		} catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}						
	}
	
	/**
	 * Gets the holder account balance.
	 *
	 * @param idParticipant the id participant
	 * @param idSecurityCode the id security code
	 * @return the holder account balance
	 */
	public HolderAccountBalance getHolderAccountBalance(Long idParticipant, String idSecurityCode) {
		StringBuffer stringBuffer= new StringBuffer();
		HolderAccountBalance holderAccountBalance= null;
		try {
			stringBuffer.append(" SELECT HAB ");
			stringBuffer.append(" FROM HolderAccountBalance HAB ");
			stringBuffer.append(" INNER JOIN FETCH HAB.holderAccount HA ");
			stringBuffer.append(" INNER JOIN FETCH HAB.security SE ");
			stringBuffer.append(" INNER JOIN FETCH HA.holderAccountDetails HAD ");
			stringBuffer.append(" INNER JOIN FETCH HAD.holder HO ");
			stringBuffer.append(" WHERE HAB.participant.idParticipantPk = :idParticipant ");
			stringBuffer.append(" and HAB.security.idSecurityCodePk = :idSecurityCode ");
			stringBuffer.append(" and HAB.totalBalance > 0 ");
			
			TypedQuery<HolderAccountBalance> typedQuery= em.createQuery(stringBuffer.toString(), HolderAccountBalance.class);
			typedQuery.setParameter("idParticipant", idParticipant);
			typedQuery.setParameter("idSecurityCode", idSecurityCode);
			holderAccountBalance= typedQuery.getSingleResult();
		} catch (NonUniqueResultException e) {
			return null;
		} catch (NoResultException ex) {
			return null;
		}
		return holderAccountBalance;
	}
	
	/**
	 * Validate account query information.
	 *
	 * @param accountBalanceQueryTO the account balance query to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<RecordValidationType> validateAccountQueryInformation(AccountBalanceQueryTO accountBalanceQueryTO) throws ServiceException {
		List<RecordValidationType> lstRecordValidationType= new ArrayList<RecordValidationType>();
		//OPERATION CODE
		if (!ComponentConstant.INTERFACE_SINGLE_QUERY_BALANCE_CIV.equalsIgnoreCase(accountBalanceQueryTO.getOperationCode())) {
			RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
					accountBalanceQueryTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION, null);
			lstRecordValidationType.add(recordValidationType);
			return lstRecordValidationType;
		}
		
		//PARTICIPANT
		Participant objParticipant = new Participant();
		objParticipant.setMnemonic(accountBalanceQueryTO.getEntityNemonic());
		objParticipant = participantServiceBean.findParticipantByFiltersServiceBean(objParticipant);
		if (Validations.validateIsNull(objParticipant)) {
			RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
					accountBalanceQueryTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_ENTITY_CODE, null);
			lstRecordValidationType.add(recordValidationType);
			return lstRecordValidationType;
		} else {
			accountBalanceQueryTO.setIdParticipant(objParticipant.getIdParticipantPk());
		}
		
		//SECURITY CLASS (DPF - DPA)
		boolean isDpfDpa= false;
		ParameterTableTO filter = new ParameterTableTO();
		filter.setIndicator1(GeneralConstants.ONE_VALUE_STRING);
		filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		List<ParameterTable> securityClasses = parameterServiceBean.getListParameterTableServiceBean(filter);
		for (ParameterTable parameterTable: securityClasses) {
			if (parameterTable.getText1().equalsIgnoreCase(accountBalanceQueryTO.getSecurityObjectTO().getSecurityClassDesc())) {
				isDpfDpa= true;
				break;
			}
		}
		if (!isDpfDpa) {
			RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
					accountBalanceQueryTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSTRUMENT_TYPE_CODE, null);
			lstRecordValidationType.add(recordValidationType);
			return lstRecordValidationType;
		}
		
		//SECURITY CODE
		SecurityObjectTO securityObjectTO = accountBalanceQueryTO.getSecurityObjectTO();
		String idSecurityCode = CommonsUtilities.getIdSecurityCode(securityObjectTO.getSecurityClassDesc(), securityObjectTO.getSecuritySerial());
		Security objSecurity = find(Security.class, idSecurityCode);
		accountBalanceQueryTO.getSecurityObjectTO().setIdSecurityCode(idSecurityCode);
		accountBalanceQueryTO.setIdSecurityCode(idSecurityCode);
		if (Validations.validateIsNull(objSecurity)) {
			RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
					accountBalanceQueryTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_SERIAL, null);
			lstRecordValidationType.add(recordValidationType);
			return lstRecordValidationType;
		}
		return lstRecordValidationType;
	}
	
	/**
	 * Gets the account balance query.
	 *
	 * @param accountBalanceQueryTO the account balance query to
	 * @return the account balance query
	 * @throws ServiceException the service exception
	 */
	public AccountBalanceQueryTO getAccountBalanceQuery(AccountBalanceQueryTO accountBalanceQueryTO) throws ServiceException {
		//fill up the holder information
		HolderAccountMovement objHolderAccountMovement = null;
		HolderAccountBalance holderAccountBalance = null;
		if(Validations.validateIsNotNullAndNotEmpty(accountBalanceQueryTO.getIdSecurityCode())){
			objHolderAccountMovement = getHolderAccountMovementLastHolder(accountBalanceQueryTO.getIdSecurityCode());
			if(objHolderAccountMovement != null){
				holderAccountBalance = objHolderAccountMovement.getHolderAccountBalance();
			}			
		}
		if(holderAccountBalance == null){
			holderAccountBalance = getHolderAccountBalance(accountBalanceQueryTO.getIdParticipant(), accountBalanceQueryTO.getIdSecurityCode());
		}
		
		if (Validations.validateIsNotNull(holderAccountBalance)) {
			HolderAccountObjectTO holderAccountObjectTO = populateHolderAccountObjectTO(holderAccountBalance);
			accountBalanceQueryTO.setHolderAccountObjectTO(holderAccountObjectTO);
		}		
		return accountBalanceQueryTO;
	}

	
	/**
	 * Populate holder account object to.
	 *
	 * @param holderAccountBalance the holder account balance
	 * @return the holder account object to
	 */
	public HolderAccountObjectTO populateHolderAccountObjectTO(HolderAccountBalance holderAccountBalance) {
		HolderAccountObjectTO holderAccountObjectTO= new HolderAccountObjectTO();
		holderAccountObjectTO.setAccountNumber(holderAccountBalance.getHolderAccount().getAccountNumber());
		
		ParameterTable parameterTable = parameterServiceBean.getParameterDetail(holderAccountBalance.getHolderAccount().getAccountType());
		holderAccountObjectTO.setAccountTypeCode(parameterTable.getText1());
		
		List<HolderAccountDetail> lstHolderAccountDetails= holderAccountBalance.getHolderAccount().getHolderAccountDetails();
		//if there are a list of holders then is a co-ownership of natural persons
		if (Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountDetails) && lstHolderAccountDetails.size() > 1) {
			holderAccountObjectTO.setHolderType(GeneralConstants.ONE_VALUE_INTEGER);
		} else {
			Holder holder= lstHolderAccountDetails.get(0).getHolder();
			if (PersonType.NATURAL.getCode().equals(holder.getHolderType())) {
				holderAccountObjectTO.setHolderType(GeneralConstants.ONE_VALUE_INTEGER);
			} else {
				holderAccountObjectTO.setHolderType(GeneralConstants.TWO_VALUE_INTEGER);
			}
		}
		if (Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountDetails)) {
			List<NaturalHolderObjectTO> lstNaturalHolderObjectTO= new ArrayList<NaturalHolderObjectTO>();
			for (HolderAccountDetail holderAccountDetail: lstHolderAccountDetails) {
				if (PersonType.JURIDIC.getCode().equals(holderAccountDetail.getHolder().getHolderType())) {
					JuridicHolderObjectTO juridicHolderObjectTO= populateJuridicHolderObjectTO(holderAccountDetail.getHolder(), false);
					holderAccountObjectTO.setJuridicHolderObjectTO(juridicHolderObjectTO);
				} else {
					NaturalHolderObjectTO naturalHolderObjectTO= populateNaturalHolderObjectTO(holderAccountDetail.getHolder(), false);
					lstNaturalHolderObjectTO.add(naturalHolderObjectTO);
				}
				RepresentedEntityTO representedEntityTo = new RepresentedEntityTO();
				representedEntityTo.setIdHolderPk(holderAccountDetail.getHolder().getIdHolderPk());
				representedEntityTo.setStateRepresentedEntity(RepresentativeEntityStateType.REGISTERED.getCode());
				List<RepresentedEntity> lstRepresentedEntity = searchRepresentedEntityServiceBean(representedEntityTo);
				if(Validations.validateListIsNotNullAndNotEmpty(lstRepresentedEntity)) {
					for(RepresentedEntity objRepresentedEntity : lstRepresentedEntity) {
						NaturalHolderObjectTO naturalHolderObjectTO = populateRepresentativeObjectTO(objRepresentedEntity.getLegalRepresentative());					
						lstNaturalHolderObjectTO.add(naturalHolderObjectTO);					
					}
					
				}
			}
			holderAccountObjectTO.setLstNaturalHolderObjectTOs(lstNaturalHolderObjectTO);
		}
		//fill up the securities information
		SecurityObjectTO securityObjectTO= populateSecurityObjectTO(holderAccountBalance.getSecurity());
		holderAccountObjectTO.setSecurityObjectTO(securityObjectTO);
		
		//Reporting information
		if (holderAccountBalance.getReportingBalance().compareTo(BigDecimal.ZERO) > 0) {
			holderAccountObjectTO.setIndReported(BooleanType.YES.getValue());
		} else {
			holderAccountObjectTO.setIndReported(BooleanType.NO.getValue());
		}
		
		//blocking information
		if (holderAccountBalance.getAccreditationBalance().compareTo(BigDecimal.ZERO) > 0) {
			holderAccountObjectTO.setBlockCode(GeneralConstants.DPF_BLOCK_TYPE_CAT);
		} else if (holderAccountBalance.getPawnBalance().compareTo(BigDecimal.ZERO) > 0) {
			holderAccountObjectTO.setBlockCode(GeneralConstants.DPF_BLOCK_TYPE_PAWN);
		} else if (holderAccountBalance.getBanBalance().compareTo(BigDecimal.ZERO) > 0) {
			holderAccountObjectTO.setBlockCode(GeneralConstants.DPF_BLOCK_TYPE_BAN);
		}
		return holderAccountObjectTO;
	}
	
	/**
	 * Populate security object to.
	 *
	 * @param security the security
	 * @return the security object to
	 */
	public SecurityObjectTO populateSecurityObjectTO(Security security) {
		SecurityObjectTO securityObjectTO= new SecurityObjectTO();
		ParameterTable parameterTable= parameterServiceBean.getParameterDetail(security.getSecurityClass());
		securityObjectTO.setSecurityClassDesc(parameterTable.getText1());
		securityObjectTO.setSecuritySerial(security.getIdSecurityCodeOnly());
		securityObjectTO.setIssuanceDate(security.getIssuanceDate());
		securityObjectTO.setExpirationDate(security.getExpirationDate());
		parameterTable= parameterServiceBean.getParameterDetail(security.getCurrency());
		securityObjectTO.setCurrencyMnemonic(parameterTable.getText1());
		securityObjectTO.setNominalValue(security.getCurrentNominalValue());
		securityObjectTO.setInterestRate(security.getInterestRate());
		if(Validations.validateIsNotNullAndNotEmpty(security.getNumberCoupons()) 
				&& security.getNumberCoupons().equals(GeneralConstants.ONE_VALUE_INTEGER)){
			securityObjectTO.setNumberCoupons(GeneralConstants.ZERO_VALUE_INTEGER);
		} else {
			securityObjectTO.setNumberCoupons(security.getNumberCoupons());
		}		
		securityObjectTO.setAlternativeCode(security.getAlternativeCode());		
		securityObjectTO.setRestrictions(security.getObservations());		
		return securityObjectTO;
	}
	
	/**
	 * Populate juridic holder object to.
	 *
	 * @param holder the holder
	 * @param fullInformation the full information
	 * @return the juridic holder object to
	 */
	public JuridicHolderObjectTO populateJuridicHolderObjectTO(Holder holder, boolean fullInformation) {
		ParameterTable parameterDocumentType= parameterServiceBean.getParameterDetail(holder.getDocumentType());
		JuridicHolderObjectTO juridicHolderObjectTO= new JuridicHolderObjectTO();
		juridicHolderObjectTO.setDocumentNumber(holder.getDocumentNumber());
		juridicHolderObjectTO.setDocumentType(parameterDocumentType.getShortInteger());
		
		if (fullInformation) {
			GeographicLocation geographicLocationResidence= null;
			GeographicLocation geographicLocationNationality= null;
			GeographicLocation geographicLocationDeparment= null;
			GeographicLocationTO filterGeo= new GeographicLocationTO();
			filterGeo.setIdGeographicLocationPk(holder.getLegalDepartment());
			geographicLocationDeparment= geographicLocationServiceBean.findGeographicLocationByIdServiceBean(filterGeo);
			filterGeo.setIdGeographicLocationPk(holder.getNationality());
			geographicLocationNationality= geographicLocationServiceBean.findGeographicLocationByIdServiceBean(filterGeo);
			filterGeo.setIdGeographicLocationPk(holder.getLegalResidenceCountry());
			geographicLocationResidence= geographicLocationServiceBean.findGeographicLocationByIdServiceBean(filterGeo);
			
			juridicHolderObjectTO.setFullName(holder.getFullName());
			juridicHolderObjectTO.setLegalAddress(holder.getLegalAddress());
			if (Validations.validateIsNotNull(geographicLocationDeparment)) {
				juridicHolderObjectTO.setLegalDepartmentCode(geographicLocationDeparment.getCodeGeographicLocation());
			} else {
				juridicHolderObjectTO.setLegalDepartmentCode(GeneralConstants.LEGAL_DEPARTMENT_CODE_OTHER);
			}
			if (BooleanType.YES.getCode().equals(holder.getIndResidence())) {
				juridicHolderObjectTO.setIndResidence(GeneralConstants.ONE_VALUE_INTEGER);
			} else {
				juridicHolderObjectTO.setIndResidence(GeneralConstants.TWO_VALUE_INTEGER);
			}
			if (Validations.validateIsNotNull(geographicLocationNationality)) {
				juridicHolderObjectTO.setNationalityCode(geographicLocationNationality.getCodeGeographicLocation());
			}
			ParameterTable parameterJuridicClass= parameterServiceBean.getParameterDetail(holder.getJuridicClass());
			juridicHolderObjectTO.setJuridicClass(new Integer(parameterJuridicClass.getParameterTableCd()));
			
			if(Validations.validateIsNotNullAndNotEmpty(holder.getEconomicSector())){
				ParameterTable parameterEconomicSector= parameterServiceBean.getParameterDetail(holder.getEconomicSector());
				juridicHolderObjectTO.setEconomicSector(new Integer(parameterEconomicSector.getParameterTableCd()));
			} else {
				juridicHolderObjectTO.setEconomicSector(null);
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(holder.getEconomicActivity())){
				ParameterTable parameterEconomicActivity= parameterServiceBean.getParameterDetail(holder.getEconomicActivity());
				juridicHolderObjectTO.setEconomicActivity(new Integer(parameterEconomicActivity.getParameterTableCd()));
			} else {
				juridicHolderObjectTO.setEconomicActivity(null);
			}
						
			juridicHolderObjectTO.setLegalResidenceCountryCode(geographicLocationResidence.getCodeGeographicLocation());
		}
				
		return juridicHolderObjectTO;
	}
	
	/**
	 * Populate natural holder object to.
	 *
	 * @param holder the holder
	 * @param fullInformation the full information
	 * @return the natural holder object to
	 */
	public NaturalHolderObjectTO populateNaturalHolderObjectTO(Holder holder, boolean fullInformation) {
		ParameterTable parameterDocumentType= parameterServiceBean.getParameterDetail(holder.getDocumentType());
		ParameterTable parameterDocumentSource= holder.getDocumentSource() != null ? parameterServiceBean.getParameterDetail(holder.getDocumentSource()) : null;
		NaturalHolderObjectTO naturalHolderObjectTO= new NaturalHolderObjectTO();
		
		if(parameterDocumentSource != null){
			naturalHolderObjectTO.setDocumentNumber(holder.getDocumentNumber() + " " + parameterDocumentSource.getText1());
		} else {
			naturalHolderObjectTO.setDocumentNumber(holder.getDocumentNumber());
		}
		
		naturalHolderObjectTO.setDocumentType(parameterDocumentType.getShortInteger());
		
		if (fullInformation) {
			GeographicLocation geographicLocationResidence= null;
			GeographicLocation geographicLocationNationality= null;
			GeographicLocation geographicLocation2ndNationality= null;
			GeographicLocation geographicLocationDeparment= null;
			GeographicLocationTO filterGeo= new GeographicLocationTO();
			filterGeo.setIdGeographicLocationPk(holder.getLegalDepartment());
			geographicLocationDeparment= geographicLocationServiceBean.findGeographicLocationByIdServiceBean(filterGeo);
			filterGeo.setIdGeographicLocationPk(holder.getNationality());
			geographicLocationNationality= geographicLocationServiceBean.findGeographicLocationByIdServiceBean(filterGeo);
			filterGeo.setIdGeographicLocationPk(holder.getLegalResidenceCountry());
			geographicLocationResidence= geographicLocationServiceBean.findGeographicLocationByIdServiceBean(filterGeo);
			if (Validations.validateIsNotNull(holder.getSecondNationality())) {
				filterGeo.setIdGeographicLocationPk(holder.getSecondNationality());
				geographicLocation2ndNationality= geographicLocationServiceBean.findGeographicLocationByIdServiceBean(filterGeo);
			}
			
			naturalHolderObjectTO.setName(holder.getName());
			naturalHolderObjectTO.setFirstLastName(holder.getFirstLastName());
			naturalHolderObjectTO.setSecondLastName(holder.getSecondLastName());
			naturalHolderObjectTO.setBirthDate(holder.getBirthDate());
			
			ParameterTable parameterSex= parameterServiceBean.getParameterDetail(holder.getSex());
			naturalHolderObjectTO.setSex(new Integer(parameterSex.getParameterTableCd()));
			
			naturalHolderObjectTO.setLegalAddress(holder.getLegalAddress());
			if (Validations.validateIsNotNull(geographicLocationNationality)) {
				naturalHolderObjectTO.setNationalityCode(geographicLocationNationality.getCodeGeographicLocation());
			}
			if (BooleanType.YES.getCode().equals(holder.getIndResidence())) {
				naturalHolderObjectTO.setIndResidence(GeneralConstants.ONE_VALUE_INTEGER);
			} else {
				naturalHolderObjectTO.setIndResidence(GeneralConstants.TWO_VALUE_INTEGER);
			}
			naturalHolderObjectTO.setLegalDepartmentCode(geographicLocationDeparment.getCodeGeographicLocation());
			if (BooleanType.YES.getCode().equals(holder.getIndResidence())) {
				naturalHolderObjectTO.setIndDisabled(GeneralConstants.ONE_VALUE_INTEGER);
			} else {
				naturalHolderObjectTO.setIndDisabled(GeneralConstants.TWO_VALUE_INTEGER);					
			}			
			naturalHolderObjectTO.setHomePhoneNumber(holder.getHomePhoneNumber());
			naturalHolderObjectTO.setMobileNumber(holder.getMobileNumber());
			naturalHolderObjectTO.setFaxNumber(holder.getFaxNumber());
			naturalHolderObjectTO.setEmail(holder.getEmail());
			if (Validations.validateIsNotNull(geographicLocation2ndNationality)) {
				naturalHolderObjectTO.setSecondNationalityCode(geographicLocation2ndNationality.getCodeGeographicLocation());
			}
			if (Validations.validateIsNotNull(holder.getSecondDocumentType())) {
				ParameterTable parameter2ndDocumentType= parameterServiceBean.getParameterDetail(holder.getSecondDocumentType());
				naturalHolderObjectTO.setSecondDocumentType(parameter2ndDocumentType.getShortInteger());
			}
			naturalHolderObjectTO.setSecondDocumentNumber(holder.getSecondDocumentNumber());
			naturalHolderObjectTO.setLegalResidenceCountryCode(geographicLocationResidence.getCodeGeographicLocation());
			naturalHolderObjectTO.setSignType(GeneralConstants.STR_SING_TYPE);
		}
		
		return naturalHolderObjectTO;
	}
	
	/**
	 * Gets the holder account for custody.
	 *
	 * @param ha the ha
	 * @return the holder account for custody
	 */
	public HolderAccount getHolderAccountForCustody(HolderAccount ha){
		HolderAccount objHolderAccount = null;
		try {
			StringBuilder stringBuilder= new StringBuilder();
			stringBuilder.append(" SELECT HA FROM HolderAccount HA ");
			stringBuilder.append(" 	inner join fetch HA.holderAccountDetails had ");
			stringBuilder.append(" 	inner join fetch had.holder h ");
			stringBuilder.append(" WHERE 1 = 1 ");
			stringBuilder.append(" 	and HA.idHolderAccountPk = :idHolderAcc ");
			Query query = em.createQuery(stringBuilder.toString());
			query.setParameter("idHolderAcc", ha.getIdHolderAccountPk());
			objHolderAccount  = (HolderAccount) query.getSingleResult();
			return objHolderAccount;
		} catch(NoResultException ex){
			return null;
		} catch(NonUniqueResultException ex){
			return null;
		}		
	}
	
	
	/**
	 * Gets the holder account movement renewal.
	 *
	 * @param idSecurityCode the id security code
	 * @return the holder account movement renewal
	 */
	@SuppressWarnings("unchecked")
	public HolderAccountMovement getHolderAccountMovementLastHolder(String idSecurityCode){
		HolderAccountMovement objHolderAccountMovement = new HolderAccountMovement();
		List<HolderAccountMovement> lstHolderAccountMovement = new ArrayList<HolderAccountMovement>();
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" SELECT ham ");
			sbQuery.append(" FROM HolderAccountMovement ham  ");
			sbQuery.append(" JOIN FETCH ham.holderAccountBalance hab ");
			sbQuery.append(" WHERE hab.security.idSecurityCodePk = :parSecurityCode  ");
			sbQuery.append(" ORDER BY ham.idHolderAccountMovementPk desc  ");		
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("parSecurityCode", idSecurityCode);
			lstHolderAccountMovement = (List<HolderAccountMovement>) query.getResultList();
			if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountMovement)){
				objHolderAccountMovement = lstHolderAccountMovement.get(0);
			} else {
				objHolderAccountMovement = null;
			}			
		} catch (NoResultException e) {
			return null;
		}				
		return objHolderAccountMovement;
	}
	
	/**
	 * Gets the holder account by dpf.
	 *
	 * @param objHolderAccountTO the obj holder account to
	 * @return the holder account by dpf
	 */
	@SuppressWarnings("unchecked")
	public HolderAccount getHolderAccountByDpf(HolderAccountTO objHolderAccountTO){
		HolderAccount objHolderAccount = null;
		try {
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(" SELECT ha.idHolderAccountPk, ha.accountNumber, 			");  // 0, 1
			stringBuilder.append(" ha.accountType, ha.participant.idParticipantPk, 			");  // 2, 3
			stringBuilder.append(" ha.participant.mnemonic, had.idHolderAccountDetailPk, 	");  // 4, 5
			stringBuilder.append(" ho.idHolderPk  "); 											 // 6
			stringBuilder.append(" FROM HolderAccount ha ");
			stringBuilder.append(" inner join ha.holderAccountDetails had ");
			stringBuilder.append(" inner join had.holder ho ");
			stringBuilder.append(" WHERE 1 = 1 ");
			stringBuilder.append(" and ha.idHolderAccountPk = :parIdHolderAccount ");			
			Query typedQuery= em.createQuery(stringBuilder.toString());
			typedQuery.setParameter("parIdHolderAccount", objHolderAccountTO.getIdHolderAccountPk());
			List<Object> objectsList = ((List<Object>) typedQuery.getResultList());			
			if(Validations.validateListIsNotNullAndNotEmpty(objectsList)){
				Object [] resultHolderAccount = (Object[]) objectsList.get(0);	
				if(resultHolderAccount != null){
					objHolderAccount = new HolderAccount();
					if(Validations.validateIsNotNullAndNotEmpty(resultHolderAccount[0])){
						objHolderAccount.setIdHolderAccountPk(new Long(resultHolderAccount[0].toString()));
					}
					if(Validations.validateIsNotNullAndNotEmpty(resultHolderAccount[1])){
						objHolderAccount.setAccountNumber(new Integer(resultHolderAccount[1].toString()));
					}
					if(Validations.validateIsNotNullAndNotEmpty(resultHolderAccount[2])){
						objHolderAccount.setAccountType(new Integer(resultHolderAccount[2].toString()));
					}
					Participant objParticipant = null;
					if(Validations.validateIsNotNullAndNotEmpty(resultHolderAccount[3])){
						objParticipant = new Participant();
						objParticipant.setIdParticipantPk(new Long(resultHolderAccount[3].toString()));
						if(Validations.validateIsNotNullAndNotEmpty(resultHolderAccount[4])){
							objParticipant.setMnemonic(resultHolderAccount[4].toString());
						}
						objHolderAccount.setParticipant(objParticipant);
					}										
					HolderAccountDetail objHolderAccountDetail = null;					
					if(Validations.validateIsNotNullAndNotEmpty(resultHolderAccount[5])){
						objHolderAccountDetail = new HolderAccountDetail();
						objHolderAccountDetail.setIdHolderAccountDetailPk(new Long(resultHolderAccount[5].toString()));						
						Holder objHolder = null;
						if(Validations.validateIsNotNullAndNotEmpty(resultHolderAccount[6])){
							objHolder = new Holder();
							objHolder.setIdHolderPk(new Long(resultHolderAccount[6].toString()));
							objHolderAccountDetail.setHolder(objHolder);
						}						
						objHolderAccount.setHolderAccountDetails(new ArrayList<HolderAccountDetail>());
						objHolderAccount.getHolderAccountDetails().add(objHolderAccountDetail);
					}					
				}
			}			
			return objHolderAccount;
		} catch(NoResultException ex){
			return null;
		} catch(NonUniqueResultException ex){
			return null;
		}		
	}
	
	/**
	 * Gets Number HolderAccountBalance for Security.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @param indBalance the ind balance
	 * @return the list geographic location
	 * @throws ServiceException the service exception
	 */    
	public Long getBalanceHolderFromSecurity(String idSecurityCodePk, boolean indBalance) throws ServiceException{
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("Select count(HAB) From HolderAccountBalance HAB ");
			sbQuery.append(" where HAB.security.idSecurityCodePk= :idSecurityCodePk ");
			if(indBalance){
				sbQuery.append(" and HAB.totalBalance > 0");
			}
			
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idSecurityCodePk", idSecurityCodePk);
		
			return (Long) query.getSingleResult();
		
		} catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}				
	}
	
	/**
	 * Search represented entity service bean.
	 *
	 * @param representedEntityTo the represented entity to
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<RepresentedEntity> searchRepresentedEntityServiceBean(RepresentedEntityTO representedEntityTo){
		StringBuilder stringBuilderSql = new StringBuilder();
		try {
			stringBuilderSql.append(" SELECT r FROM RepresentedEntity r 	");
			stringBuilderSql.append(" JOIN FETCH r.legalRepresentative lr 	");
			stringBuilderSql.append(" WHERE r.stateRepresented = :status 	");	
			stringBuilderSql.append(" and r.holder.idHolderPk = :idHolder 	");
			Query query = em.createQuery(stringBuilderSql.toString());
			query.setParameter("idHolder", representedEntityTo.getIdHolderPk());	
			query.setParameter("status", representedEntityTo.getStateRepresentedEntity());			 
		    return (List<RepresentedEntity>) query.getResultList();
		} catch(NoResultException ex){
			   return null;
		}		
	}	
	
	/**
	 * Populate representative object to.
	 *
	 * @param LegalRepresentative the legal representative
	 * @param fullInformation the full information
	 * @return the natural holder object to
	 */
	public NaturalHolderObjectTO populateRepresentativeObjectTO(LegalRepresentative LegalRepresentative) {
		ParameterTable parameterDocumentType= parameterServiceBean.getParameterDetail(LegalRepresentative.getDocumentType());
		NaturalHolderObjectTO naturalHolderObjectTO= new NaturalHolderObjectTO();		
		naturalHolderObjectTO.setDocumentNumber(LegalRepresentative.getDocumentNumber());
		naturalHolderObjectTO.setDocumentType(parameterDocumentType.getShortInteger());						
		return naturalHolderObjectTO;
	}
	
}
