package com.pradera.core.component.billing.to;

import java.io.Serializable;
import java.math.BigDecimal;

import com.pradera.model.component.MovementType;


public class RateMovementTo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4446264087665914645L;
	
	
	private Integer idRateMovementPk;
	private MovementType movementType;
	private BigDecimal rateAmount;
	private BigDecimal ratePercent;
	private ServiceRateTo serviceRateTo;
	private String descriptionMovementType;
	private Integer movementCount;
	private BigDecimal grossAmount;
	private String securityCode;
	private String descriptionCurrencyRate;
	private Integer currencyRate;
	private Long movementQuantity;
	private BigDecimal operationPrice;
	
	private BigDecimal dirtyAmount;

	public Integer getIdRateMovementPk() {
		return idRateMovementPk;
	}
	public void setIdRateMovementPk(Integer idRateMovementPk) {
		this.idRateMovementPk = idRateMovementPk;
	}
	
	

	public MovementType getMovementType() {
		return movementType;
	}
	public void setMovementType(MovementType movementType) {
		this.movementType = movementType;
	}
	
	public BigDecimal getRateAmount() {
		return rateAmount;
	}
	public void setRateAmount(BigDecimal rateAmount) {
		this.rateAmount = rateAmount;
	}
	public BigDecimal getRatePercent() {
		return ratePercent;
	}
	public void setRatePercent(BigDecimal ratePercent) {
		this.ratePercent = ratePercent;
	}
	public ServiceRateTo getServiceRateTo() {
		return serviceRateTo;
	}
	public void setServiceRateTo(ServiceRateTo serviceRateTo) {
		this.serviceRateTo = serviceRateTo;
	}
	public String getDescriptionMovementType() {
		return descriptionMovementType;
	}
	public Integer getMovementCount() {
		return movementCount;
	}
	public void setMovementCount(Integer movementCount) {
		this.movementCount = movementCount;
	}
	public void setDescriptionMovementType(String descriptionMovementType) {
		this.descriptionMovementType = descriptionMovementType;
	}
	public BigDecimal getGrossAmount() {
		return grossAmount;
	}
	public void setGrossAmount(BigDecimal grossAmount) {
		this.grossAmount = grossAmount;
	}
	
	/**
	 * @return the securityCode
	 */
	public String getSecurityCode() {
		return securityCode;
	}
	/**
	 * @param securityCode the securityCode to set
	 */
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
	public Integer getCurrencyRate() {
		return currencyRate;
	}
	public void setCurrencyRate(Integer currencyRate) {
		this.currencyRate = currencyRate;
	}
	public Long getMovementQuantity() {
		return movementQuantity;
	}
	public void setMovementQuantity(Long movementQuantity) {
		this.movementQuantity = movementQuantity;
	}
	public BigDecimal getOperationPrice() {
		return operationPrice;
	}
	public void setOperationPrice(BigDecimal operationPrice) {
		this.operationPrice = operationPrice;
	}
	public String getDescriptionCurrencyRate() {
		return descriptionCurrencyRate;
	}
	public void setDescriptionCurrencyRate(String descriptionCurrencyRate) {
		this.descriptionCurrencyRate = descriptionCurrencyRate;
	}
	public BigDecimal getDirtyAmount() {
		return dirtyAmount;
	}
	public void setDirtyAmount(BigDecimal dirtyAmount) {
		this.dirtyAmount = dirtyAmount;
	}
	
	
	
}
