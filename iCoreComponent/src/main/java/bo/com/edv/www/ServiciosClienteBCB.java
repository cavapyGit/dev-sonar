/**
 * ServiciosClienteBCB.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package bo.com.edv.www;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Interface ServiciosClienteBCB.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/09/2015
 */
public interface ServiciosClienteBCB extends java.rmi.Remote {
	
	
    /**
     * Enviar procesar transferencia.
     *
     * @param mensajeXml the mensaje xml
     * @return the java.lang. string
     * @throws RemoteException the remote exception
     */
    public java.lang.String enviarProcesarTransferencia(java.lang.String mensajeXml) throws java.rmi.RemoteException;
    
    /**
     * Enviare solicitar extracto.
     *
     * @param mensajeXml the mensaje xml
     * @return the java.lang. string
     * @throws RemoteException the remote exception
     */
    public java.lang.String enviareSolicitarExtracto(java.lang.String mensajeXml) throws java.rmi.RemoteException;

    
    /**
     * Solicitar tipo de cambio.
     *
     * @param mensajeXml the mensaje xml
     * @return the java.lang. string
     * @throws RemoteException the remote exception
     */
    public java.lang.String obtenerTipoCambio(int codMoneda, java.lang.String fecha) throws java.rmi.RemoteException;
    
    /**
     * Solicitar saldo
     * 
     * */
    public java.lang.String enviarSolicitarSaldo(java.lang.String mensajeXml) throws java.rmi.RemoteException;
 
}
