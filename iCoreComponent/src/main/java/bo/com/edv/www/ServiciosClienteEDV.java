/**
 * ServiciosClienteEDV.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package bo.com.edv.www;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Interface ServiciosClienteEDV.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/09/2015
 */
public interface ServiciosClienteEDV extends java.rmi.Remote {
    
    /**
     * Enviar bloqueo.
     *
     * @param mensajeXml the mensaje xml
     * @param eif the eif
     * @return the java.lang. string
     * @throws RemoteException the remote exception
     */
    public java.lang.String enviarBloqueo(java.lang.String mensajeXml, java.lang.String eif) throws java.rmi.RemoteException;
    
    /**
     * Enviar transferencia.
     *
     * @param mensajeXml the mensaje xml
     * @param eif the eif
     * @return the java.lang. string
     * @throws RemoteException the remote exception
     */
    public java.lang.String enviarTransferencia(java.lang.String mensajeXml, java.lang.String eif) throws java.rmi.RemoteException;
    
    /**
     * Issue 1310: Envio de Trasferencia Ultimo Titular
     * @param mensajeXml
     * @param eif
     * @return
     * @throws java.rmi.RemoteException
     */
    public java.lang.String enviarTransferenciaUltimoTitular(java.lang.String mensajeXml, java.lang.String eif) throws java.rmi.RemoteException;
    
    /**
     * Enviar desbloqueo.
     *
     * @param mensajeXml the mensaje xml
     * @param eif the eif
     * @return the java.lang. string
     * @throws RemoteException the remote exception
     */
    public java.lang.String enviarDesbloqueo(java.lang.String mensajeXml, java.lang.String eif) throws java.rmi.RemoteException;
    
    /**
     * Enviar anotacion cuenta voluntaria.
     *
     * @param mensajeXml the mensaje xml
     * @param eif the eif
     * @return the java.lang. string
     * @throws RemoteException the remote exception
     */
    public java.lang.String enviarAnotacionCuentaVoluntaria(java.lang.String mensajeXml, java.lang.String eif) throws java.rmi.RemoteException;
}
