/**
 * ServiciosClienteBCBService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package bo.com.edv.www;

public interface ServiciosClienteBCBService extends javax.xml.rpc.Service {
    public java.lang.String getEdvWebServicesPortAddress();

    public bo.com.edv.www.ServiciosClienteBCB getEdvWebServicesPort() throws javax.xml.rpc.ServiceException;

    public bo.com.edv.www.ServiciosClienteBCB getEdvWebServicesPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
