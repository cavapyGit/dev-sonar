package bo.com.edv.www;

import java.rmi.RemoteException;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary. Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ServiciosClienteBCBProxy.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 17/09/2015
 */
public class ServiciosClienteBCBProxy implements
		bo.com.edv.www.ServiciosClienteBCB {

	/** The _endpoint. */
	private String _endpoint = null;

	/** The servicios cliente bcb. */
	private bo.com.edv.www.ServiciosClienteBCB serviciosClienteBCB = null;

	/** The _url connection global. */
	String _urlConnectionGlobal;

	/**
	 * Instantiates a new servicios cliente bcb proxy.
	 * 
	 * @param urlConnection
	 *            the url connection
	 */
	public ServiciosClienteBCBProxy(String urlConnection) {
		this._urlConnectionGlobal = urlConnection;
		_initServiciosClienteBCBProxy(this._urlConnectionGlobal);
	}

	/**
	 * Instantiates a new servicios cliente bcb proxy.
	 * 
	 * @param endpoint
	 *            the endpoint
	 */
	public ServiciosClienteBCBProxy(String endpoint, String urlConnection) {
		this._urlConnectionGlobal = urlConnection;
		_endpoint = endpoint;
		_initServiciosClienteBCBProxy(this._urlConnectionGlobal);
	}

	/**
	 * _init servicios cliente bcb proxy.
	 */
	private void _initServiciosClienteBCBProxy(String urlConnection) {
		try {
			serviciosClienteBCB = (new bo.com.edv.www.ServiciosClienteBCBServiceLocator(urlConnection)).getEdvWebServicesPort();
			if (serviciosClienteBCB != null) {
				if (_endpoint != null)
					((javax.xml.rpc.Stub) serviciosClienteBCB)._setProperty("javax.xml.rpc.service.endpoint.address",_endpoint);
				else
					_endpoint = (String) ((javax.xml.rpc.Stub) serviciosClienteBCB)._getProperty("javax.xml.rpc.service.endpoint.address");
			}

		} catch (javax.xml.rpc.ServiceException serviceException) {
		}
	}

	/**
	 * Gets the endpoint.
	 * 
	 * @return the endpoint
	 */
	public String getEndpoint() {
		return _endpoint;
	}

	/**
	 * Sets the endpoint.
	 * 
	 * @param endpoint
	 *            the new endpoint
	 */
	public void setEndpoint(String endpoint) {
		_endpoint = endpoint;
		if (serviciosClienteBCB != null)
			((javax.xml.rpc.Stub) serviciosClienteBCB)._setProperty(
					"javax.xml.rpc.service.endpoint.address", _endpoint);

	}

	/**
	 * Gets the servicios cliente bcb.
	 * 
	 * @return the servicios cliente bcb
	 */
	public bo.com.edv.www.ServiciosClienteBCB getServiciosClienteBCB() {
		if (serviciosClienteBCB == null)
			_initServiciosClienteBCBProxy(this._urlConnectionGlobal);
		return serviciosClienteBCB;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * bo.com.edv.www.ServiciosClienteBCB#enviarProcesarTransferencia(java.lang
	 * .String)
	 */
	public java.lang.String enviarProcesarTransferencia(
			java.lang.String mensajeXml) throws java.rmi.RemoteException {
		if (serviciosClienteBCB == null)
			_initServiciosClienteBCBProxy(this._urlConnectionGlobal);
		return serviciosClienteBCB.enviarProcesarTransferencia(mensajeXml);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * bo.com.edv.www.ServiciosClienteBCB#enviareSolicitarExtracto(java.lang
	 * .String)
	 */
	public java.lang.String enviareSolicitarExtracto(java.lang.String mensajeXml)
			throws java.rmi.RemoteException {
		if (serviciosClienteBCB == null)
			_initServiciosClienteBCBProxy(this._urlConnectionGlobal);
		return serviciosClienteBCB.enviareSolicitarExtracto(mensajeXml);
	}
	
	public java.lang.String enviarSolicitarSaldo(java.lang.String mensajeXml) throws RemoteException {
		if (serviciosClienteBCB == null)
			_initServiciosClienteBCBProxy(this._urlConnectionGlobal);
		return serviciosClienteBCB.enviarSolicitarSaldo(mensajeXml);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * bo.com.edv.www.ServiciosClienteBCB#obtenerTipoCambio(java.lang
	 * .String)
	 */
	public java.lang.String obtenerTipoCambio(int codMoneda,java.lang.String fecha)
			throws java.rmi.RemoteException {
		if (serviciosClienteBCB == null)
			_initServiciosClienteBCBProxy(this._urlConnectionGlobal);
		return serviciosClienteBCB.obtenerTipoCambio(codMoneda,fecha);
	}
}