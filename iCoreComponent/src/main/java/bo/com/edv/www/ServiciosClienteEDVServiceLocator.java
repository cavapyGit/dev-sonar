/**
 * ServiciosClienteEDVServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package bo.com.edv.www;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary. Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ServiciosClienteEDVServiceLocator.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 17/09/2015
 */
public class ServiciosClienteEDVServiceLocator extends
		org.apache.axis.client.Service implements
		bo.com.edv.www.ServiciosClienteEDVService {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The Edv web services port_address. */
	// Use to get a proxy class for EdvWebServicesPort
	private java.lang.String EdvWebServicesPort_address = null;

	/**
	 * Instantiates a new servicios cliente edv service locator.
	 * 
	 * @param urlConnection
	 *            the url connection
	 */
	public ServiciosClienteEDVServiceLocator(String urlConnection) {
		EdvWebServicesPort_address = urlConnection;
	}

	/**
	 * Instantiates a new servicios cliente edv service locator.
	 * 
	 * @param config
	 *            the config
	 */
	public ServiciosClienteEDVServiceLocator(
			org.apache.axis.EngineConfiguration config) {
		super(config);
	}

	/**
	 * Instantiates a new servicios cliente edv service locator.
	 * 
	 * @param wsdlLoc
	 *            the wsdl loc
	 * @param sName
	 *            the s name
	 * @throws ServiceException
	 *             the service exception
	 */
	public ServiciosClienteEDVServiceLocator(java.lang.String wsdlLoc,
			javax.xml.namespace.QName sName)
			throws javax.xml.rpc.ServiceException {
		super(wsdlLoc, sName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * bo.com.edv.www.ServiciosClienteEDVService#getEdvWebServicesPortAddress()
	 */
	public java.lang.String getEdvWebServicesPortAddress() {
		return EdvWebServicesPort_address;
	}

	/** The Edv web services port wsdd service name. */
	// The WSDD service name defaults to the port name.
	private java.lang.String EdvWebServicesPortWSDDServiceName = "EdvWebServicesPort";

	/**
	 * Gets the edv web services port wsdd service name.
	 * 
	 * @return the edv web services port wsdd service name
	 */
	public java.lang.String getEdvWebServicesPortWSDDServiceName() {
		return EdvWebServicesPortWSDDServiceName;
	}

	/**
	 * Sets the edv web services port wsdd service name.
	 * 
	 * @param name
	 *            the new edv web services port wsdd service name
	 */
	public void setEdvWebServicesPortWSDDServiceName(java.lang.String name) {
		EdvWebServicesPortWSDDServiceName = name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see bo.com.edv.www.ServiciosClienteEDVService#getEdvWebServicesPort()
	 */
	public bo.com.edv.www.ServiciosClienteEDV getEdvWebServicesPort()
			throws javax.xml.rpc.ServiceException {
		java.net.URL endpoint;
		try {
			endpoint = new java.net.URL(EdvWebServicesPort_address);
		} catch (java.net.MalformedURLException e) {
			throw new javax.xml.rpc.ServiceException(e);
		}
		return getEdvWebServicesPort(endpoint);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * bo.com.edv.www.ServiciosClienteEDVService#getEdvWebServicesPort(java.
	 * net.URL)
	 */
	public bo.com.edv.www.ServiciosClienteEDV getEdvWebServicesPort(
			java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
		try {
			bo.com.edv.www.ServiciosClienteEDVServiceSoapBindingStub _stub = new bo.com.edv.www.ServiciosClienteEDVServiceSoapBindingStub(
					portAddress, this);
			_stub.setPortName(getEdvWebServicesPortWSDDServiceName());
			return _stub;
		} catch (org.apache.axis.AxisFault e) {
			return null;
		}
	}

	/**
	 * Sets the edv web services port endpoint address.
	 * 
	 * @param address
	 *            the new edv web services port endpoint address
	 */
	public void setEdvWebServicesPortEndpointAddress(java.lang.String address) {
		EdvWebServicesPort_address = address;
	}

	/**
	 * For the given interface, get the stub implementation. If this service has
	 * no port for the given interface, then ServiceException is thrown.
	 * 
	 * @param serviceEndpointInterface
	 *            the service endpoint interface
	 * @return the port
	 * @throws ServiceException
	 *             the service exception
	 */
	public java.rmi.Remote getPort(Class serviceEndpointInterface)
			throws javax.xml.rpc.ServiceException {
		try {
			if (bo.com.edv.www.ServiciosClienteEDV.class
					.isAssignableFrom(serviceEndpointInterface)) {
				bo.com.edv.www.ServiciosClienteEDVServiceSoapBindingStub _stub = new bo.com.edv.www.ServiciosClienteEDVServiceSoapBindingStub(
						new java.net.URL(EdvWebServicesPort_address), this);
				_stub.setPortName(getEdvWebServicesPortWSDDServiceName());
				return _stub;
			}
		} catch (java.lang.Throwable t) {
			throw new javax.xml.rpc.ServiceException(t);
		}
		throw new javax.xml.rpc.ServiceException(
				"There is no stub implementation for the interface:  "
						+ (serviceEndpointInterface == null ? "null"
								: serviceEndpointInterface.getName()));
	}

	/**
	 * For the given interface, get the stub implementation. If this service has
	 * no port for the given interface, then ServiceException is thrown.
	 * 
	 * @param portName
	 *            the port name
	 * @param serviceEndpointInterface
	 *            the service endpoint interface
	 * @return the port
	 * @throws ServiceException
	 *             the service exception
	 */
	public java.rmi.Remote getPort(javax.xml.namespace.QName portName,
			Class serviceEndpointInterface)
			throws javax.xml.rpc.ServiceException {
		if (portName == null) {
			return getPort(serviceEndpointInterface);
		}
		java.lang.String inputPortName = portName.getLocalPart();
		if ("EdvWebServicesPort".equals(inputPortName)) {
			return getEdvWebServicesPort();
		} else {
			java.rmi.Remote _stub = getPort(serviceEndpointInterface);
			((org.apache.axis.client.Stub) _stub).setPortName(portName);
			return _stub;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.axis.client.Service#getServiceName()
	 */
	public javax.xml.namespace.QName getServiceName() {
		return new javax.xml.namespace.QName("http://www.edv.com.bo/",
				"ServiciosClienteEDVService");
	}

	/** The ports. */
	private java.util.HashSet ports = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.axis.client.Service#getPorts()
	 */
	public java.util.Iterator getPorts() {
		if (ports == null) {
			ports = new java.util.HashSet();
			ports.add(new javax.xml.namespace.QName("http://www.edv.com.bo/",
					"EdvWebServicesPort"));
		}
		return ports.iterator();
	}

	/**
	 * Set the endpoint address for the specified port name.
	 * 
	 * @param portName
	 *            the port name
	 * @param address
	 *            the address
	 * @throws ServiceException
	 *             the service exception
	 */
	public void setEndpointAddress(java.lang.String portName,
			java.lang.String address) throws javax.xml.rpc.ServiceException {

		if ("EdvWebServicesPort".equals(portName)) {
			setEdvWebServicesPortEndpointAddress(address);
		} else { // Unknown Port Name
			throw new javax.xml.rpc.ServiceException(
					" Cannot set Endpoint Address for Unknown Port" + portName);
		}
	}

	/**
	 * Set the endpoint address for the specified port name.
	 * 
	 * @param portName
	 *            the port name
	 * @param address
	 *            the address
	 * @throws ServiceException
	 *             the service exception
	 */
	public void setEndpointAddress(javax.xml.namespace.QName portName,
			java.lang.String address) throws javax.xml.rpc.ServiceException {
		setEndpointAddress(portName.getLocalPart(), address);
	}

}
