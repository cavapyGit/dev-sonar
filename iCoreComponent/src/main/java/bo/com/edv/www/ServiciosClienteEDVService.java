/**
 * ServiciosClienteEDVService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package bo.com.edv.www;

public interface ServiciosClienteEDVService extends javax.xml.rpc.Service {
    public java.lang.String getEdvWebServicesPortAddress();

    public bo.com.edv.www.ServiciosClienteEDV getEdvWebServicesPort() throws javax.xml.rpc.ServiceException;

    public bo.com.edv.www.ServiciosClienteEDV getEdvWebServicesPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
