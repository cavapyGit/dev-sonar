package bo.com.edv.www;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ServiciosClienteEDVProxy.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/09/2015
 */
public class ServiciosClienteEDVProxy implements
		bo.com.edv.www.ServiciosClienteEDV {
	
	/** The _endpoint. */
	private String _endpoint = null;
	
	/** The _url connection global. */
	String _urlConnectionGlobal;
	
	/** The servicios cliente edv. */
	private bo.com.edv.www.ServiciosClienteEDV serviciosClienteEDV = null;

	/**
	 * Instantiates a new servicios cliente edv proxy.
	 *
	 * @param urlConnection the url connection
	 */
	public ServiciosClienteEDVProxy(String urlConnection) {
		this._urlConnectionGlobal = urlConnection;
		_initServiciosClienteEDVProxy(urlConnection);
	}

	/**
	 * Instantiates a new servicios cliente edv proxy.
	 *
	 * @param endpoint the endpoint
	 * @param urlConnection the url connection
	 */
	public ServiciosClienteEDVProxy(String endpoint, String urlConnection) {
		this._urlConnectionGlobal = urlConnection;
		_endpoint = endpoint;
		_initServiciosClienteEDVProxy(urlConnection);
	}
	
	/**
	 * _init servicios cliente edv proxy.
	 *
	 * @param urlConnection the url connection
	 */
	private void _initServiciosClienteEDVProxy(String urlConnection) {
		try {
			serviciosClienteEDV = (new bo.com.edv.www.ServiciosClienteEDVServiceLocator(urlConnection)).getEdvWebServicesPort();
			if (serviciosClienteEDV != null) {
				if (_endpoint != null)
					((javax.xml.rpc.Stub) serviciosClienteEDV)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
				else
					_endpoint = (String) ((javax.xml.rpc.Stub) serviciosClienteEDV)
							._getProperty("javax.xml.rpc.service.endpoint.address");
			}
		} catch (javax.xml.rpc.ServiceException serviceException) {
		}
	}

	/**
	 * Gets the endpoint.
	 *
	 * @return the endpoint
	 */
	public String getEndpoint() {
		return _endpoint;
	}

	/**
	 * Sets the endpoint.
	 *
	 * @param endpoint the new endpoint
	 */
	public void setEndpoint(String endpoint) {
		_endpoint = endpoint;
		if (serviciosClienteEDV != null)
			((javax.xml.rpc.Stub) serviciosClienteEDV)._setProperty(
					"javax.xml.rpc.service.endpoint.address", _endpoint);

	}

	/**
	 * Gets the servicios cliente edv.
	 *
	 * @return the servicios cliente edv
	 */
	public bo.com.edv.www.ServiciosClienteEDV getServiciosClienteEDV() {
		if (serviciosClienteEDV == null)
			_initServiciosClienteEDVProxy(this._urlConnectionGlobal);
		return serviciosClienteEDV;
	}

	/* (non-Javadoc)
	 * @see bo.com.edv.www.ServiciosClienteEDV#enviarBloqueo(java.lang.String, java.lang.String)
	 */
	public java.lang.String enviarBloqueo(java.lang.String mensajeXml,
			java.lang.String eif) throws java.rmi.RemoteException {
		if (serviciosClienteEDV == null)
			_initServiciosClienteEDVProxy(this._urlConnectionGlobal);
		return serviciosClienteEDV.enviarBloqueo(mensajeXml, eif);
	}

	/* (non-Javadoc)
	 * @see bo.com.edv.www.ServiciosClienteEDV#enviarTransferencia(java.lang.String, java.lang.String)
	 */
	public java.lang.String enviarTransferencia(java.lang.String mensajeXml,
			java.lang.String eif) throws java.rmi.RemoteException {
		if (serviciosClienteEDV == null)
			_initServiciosClienteEDVProxy(this._urlConnectionGlobal);
		return serviciosClienteEDV.enviarTransferencia(mensajeXml, eif);
	}
	
	public java.lang.String enviarTransferenciaUltimoTitular(java.lang.String mensajeXml,
			java.lang.String eif) throws java.rmi.RemoteException {
		if (serviciosClienteEDV == null)
			_initServiciosClienteEDVProxy(this._urlConnectionGlobal);
		return serviciosClienteEDV.enviarTransferenciaUltimoTitular(mensajeXml, eif);
	}

	/* (non-Javadoc)
	 * @see bo.com.edv.www.ServiciosClienteEDV#enviarDesbloqueo(java.lang.String, java.lang.String)
	 */
	public java.lang.String enviarDesbloqueo(java.lang.String mensajeXml,
			java.lang.String eif) throws java.rmi.RemoteException {
		if (serviciosClienteEDV == null)
			_initServiciosClienteEDVProxy(this._urlConnectionGlobal);
		return serviciosClienteEDV.enviarDesbloqueo(mensajeXml, eif);
	}

	/* (non-Javadoc)
	 * @see bo.com.edv.www.ServiciosClienteEDV#enviarAnotacionCuentaVoluntaria(java.lang.String, java.lang.String)
	 */
	public java.lang.String enviarAnotacionCuentaVoluntaria(
			java.lang.String mensajeXml, java.lang.String eif)
			throws java.rmi.RemoteException {
		if (serviciosClienteEDV == null)
			_initServiciosClienteEDVProxy(this._urlConnectionGlobal);
		return serviciosClienteEDV.enviarAnotacionCuentaVoluntaria(mensajeXml,
				eif);
	}

	/**
	 * @return the _urlConnectionGlobal
	 */
	public String get_urlConnectionGlobal() {
		return _urlConnectionGlobal;
	}

	/**
	 * @param _urlConnectionGlobal the _urlConnectionGlobal to set
	 */
	public void set_urlConnectionGlobal(String _urlConnectionGlobal) {
		this._urlConnectionGlobal = _urlConnectionGlobal;
	}

}