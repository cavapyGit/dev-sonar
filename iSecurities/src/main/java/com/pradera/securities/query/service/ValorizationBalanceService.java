package com.pradera.securities.query.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.business.to.MarketFactResultTO;
import com.pradera.core.component.business.to.ValorizationBalanceDetailTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.custody.type.CertificateDepositeEstatusType;
import com.pradera.model.custody.type.SituationType;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.ValorizationQueryType;
import com.pradera.securities.query.to.PhysicalValuedBalancesTO;
import com.pradera.securities.query.to.ValorizationBalanceFilterTO;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class ValueHoldBalance.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 28/05/2013
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@Performance
public class ValorizationBalanceService extends CrudDaoServiceBean{
	
	/** The parameter service. */
	@EJB
	private ParameterServiceBean parameterService;
	
	/**
	 * Gets the value hold balance service bean.
	 *
	 * @param valorizationBalanceFilterTO the valorization balance filter to
	 * @return the value hold balance service bean
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public Object[] getValorizationBalanceService(ValorizationBalanceFilterTO valorizationBalanceFilterTO) throws ServiceException{
		
		Query query = null;
		Object[] resultado = new Object[2];
		StringBuffer sbQuery = new StringBuffer();
		Map<Integer, BigDecimal> tipoCambio = parameterService.getDayExchangesRates(valorizationBalanceFilterTO.getDateCourte());
		
		ParameterTableTO  filter = new ParameterTableTO();
		Map<Integer,String> currencyMap = new HashMap<Integer, String>();
		filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
			currencyMap.put(param.getParameterTablePk(), param.getText1());
		}
		
		

		if(valorizationBalanceFilterTO.getDateCourte().before(CommonsUtilities.currentDate())){
			
			if (valorizationBalanceFilterTO.getValorizationType().equals(ValorizationQueryType.MARKET_FACT.getCode())){
				
				sbQuery.append(" select hab.security.idSecurityCodePk,hab.security.description, "//0,1
						+ " hab.security.currentNominalValue, hab.security.currency, " //2,3
						+ " hab.totalBalance,hab.availableBalance, "//4,5
						+ " sum(round(hmh.marketPrice,2)*hmh.totalBalance),sum(round(hmh.marketPrice,2)*hmh.availableBalance),hab.holderAccount.idHolderAccountPk ");//6,7,8
				
				sbQuery.append(" from HolderMarketfactHistory hmh ");
				sbQuery.append(" inner join hmh.holderAccountBalance hab ");
				sbQuery.append(" where hab.participant.idParticipantPk = :participant ");
				sbQuery.append(" and hab.holderAccount.idHolderAccountPk in (select had.holderAccount.idHolderAccountPk from HolderAccountDetail had where had.holder.idHolderPk = :idHolder) ");
				sbQuery.append(" and hmh.marketPrice is not null ");
				sbQuery.append(" and hmh.processDate = :processDate ");
				sbQuery.append(" and hab.totalBalance > 0 ");
				
				if (valorizationBalanceFilterTO.getSecurity().getIdSecurityCodePk() !=null){	
					sbQuery.append(" and hab.security.idSecurityCodePk = :securityCode ");
				}
				if (valorizationBalanceFilterTO.getIdHolderAccountPk() !=null){	
					sbQuery.append(" and hab.holderAccount.idHolderAccountPk = :holderAccount ");
				}
				if (valorizationBalanceFilterTO.getIssuerRequest().getIdIssuerPk()!=null){
					sbQuery.append(" and hab.security.issuer.idIssuerPk = :issuer ");
				}
				
				sbQuery.append(" group by hab.security.idSecurityCodePk, hab.security.description, hab.security.currentNominalValue, hab.security.currency, hab.totalBalance,hab.availableBalance,hab.holderAccount.idHolderAccountPk ");
				
				query = em.createQuery(sbQuery.toString());
				query.setParameter("participant", valorizationBalanceFilterTO.getIdParticipantPk());        //103
				query.setParameter("idHolder", valorizationBalanceFilterTO.getHolderRequest().getIdHolderPk()); //300101
				query.setParameter("processDate", valorizationBalanceFilterTO.getDateCourte()); //300101
				
				if (valorizationBalanceFilterTO.getIdHolderAccountPk() !=null){	
					query.setParameter("holderAccount",valorizationBalanceFilterTO.getIdHolderAccountPk());    //17014
				}
				if (valorizationBalanceFilterTO.getSecurity().getIdSecurityCodePk() !=null){	
					query.setParameter("securityCode", valorizationBalanceFilterTO.getSecurity().getIdSecurityCodePk());      //BTX-BTXN05J3812
				}
				
				if (valorizationBalanceFilterTO.getIssuerRequest().getIdIssuerPk()!=null){
					query.setParameter("issuer",valorizationBalanceFilterTO.getIssuerRequest().getIdIssuerPk());  
				}
				
			}
			
		}else{
			//PARA LA FECHA ACTUAL
			if (valorizationBalanceFilterTO.getValorizationType().equals(ValorizationQueryType.VAL_NOMINAL.getCode())){
				sbQuery.append(" Select hab.security.idSecurityCodePk,hab.security.description, "//0,1
						+ " hab.security.currentNominalValue, hab.security.currency, " //2,3
						+ " hab.totalBalance,hab.availableBalance ");//5,6
				sbQuery.append(" From HolderAccountBalance hab ");
				sbQuery.append(" where hab.participant.idParticipantPk = :participant ");
				sbQuery.append(" and hab.totalBalance > 0 ");
				sbQuery.append(" and hab.holderAccount.idHolderAccountPk in (select had.holderAccount.idHolderAccountPk from HolderAccountDetail had where had.holder.idHolderPk = :idHolder) ");
			
			}else if (valorizationBalanceFilterTO.getValorizationType().equals(ValorizationQueryType.MARKET_FACT.getCode())){
				sbQuery.append(" select hab.security.idSecurityCodePk,hab.security.description, "//0,1
						+ " hab.security.currentNominalValue, hab.security.currency, " //2,3
						+ " hab.totalBalance,hab.availableBalance, "//4,5
						+ " sum(round(hmb.marketPrice,2)*hmb.totalBalance),sum(round(hmb.marketPrice,2)*hmb.availableBalance),hab.holderAccount.idHolderAccountPk ");//6,7,8
				
				sbQuery.append(" from HolderMarketFactBalance hmb ");
				sbQuery.append(" inner join hmb.holderAccountBalance hab ");
				sbQuery.append(" where hab.participant.idParticipantPk = :participant ");
				sbQuery.append(" and hab.holderAccount.idHolderAccountPk in (select had.holderAccount.idHolderAccountPk from HolderAccountDetail had where had.holder.idHolderPk = :idHolder) ");
				sbQuery.append(" and hmb.indActive = :indActive ");
				sbQuery.append(" and hmb.marketPrice is not null ");
				
			}
				
				if (valorizationBalanceFilterTO.getSecurity().getIdSecurityCodePk() !=null){	
					sbQuery.append(" and hab.security.idSecurityCodePk = :securityCode ");
				}
				if (valorizationBalanceFilterTO.getIdHolderAccountPk() !=null){	
					sbQuery.append(" and hab.holderAccount.idHolderAccountPk = :holderAccount ");
				}
				if (valorizationBalanceFilterTO.getIssuerRequest().getIdIssuerPk()!=null){
					sbQuery.append(" and hab.security.issuer.idIssuerPk = :issuer ");
				}
				
				
				
				if (valorizationBalanceFilterTO.getValorizationType().equals(ValorizationQueryType.MARKET_FACT.getCode())){
					sbQuery.append(" group by hab.security.idSecurityCodePk,hab.security.description,hab.security.currentNominalValue, hab.security.currency,hab.totalBalance,hab.availableBalance,hab.holderAccount.idHolderAccountPk ");
				}
				
				query = em.createQuery(sbQuery.toString());
				query.setParameter("participant", valorizationBalanceFilterTO.getIdParticipantPk());        //103
				query.setParameter("idHolder", valorizationBalanceFilterTO.getHolderRequest().getIdHolderPk()); //300101
				
				if (valorizationBalanceFilterTO.getIdHolderAccountPk() !=null){	
					query.setParameter("holderAccount",valorizationBalanceFilterTO.getIdHolderAccountPk());    //17014
				}
				if (valorizationBalanceFilterTO.getSecurity().getIdSecurityCodePk() !=null){	
					query.setParameter("securityCode", valorizationBalanceFilterTO.getSecurity().getIdSecurityCodePk());      //BTX-BTXN05J3812
				}
				
				if (valorizationBalanceFilterTO.getValorizationType().equals(ValorizationQueryType.MARKET_FACT.getCode())){
					query.setParameter("indActive", BooleanType.YES.getCode());      //1
				}
				if (valorizationBalanceFilterTO.getIssuerRequest().getIdIssuerPk()!=null){
					query.setParameter("issuer",valorizationBalanceFilterTO.getIssuerRequest().getIdIssuerPk());  
				}
			}
				

				List<Object[]> result = new ArrayList<Object[]>();
				if(Validations.validateIsNotNullAndNotEmpty(query)){
					result = query.getResultList();
				}
				 
				
				List<ValorizationBalanceDetailTO> listHolderAccountMovInfoTO = new  ArrayList<ValorizationBalanceDetailTO>();
				BigDecimal tipoCambioUSD = tipoCambio.get(CurrencyType.USD.getCode());
				for(Object[] lista : result){
					ValorizationBalanceDetailTO res = new ValorizationBalanceDetailTO();
					
					res.setClaseClaveValor(lista[0].toString());
					if(Validations.validateIsNotNullAndNotEmpty(lista[1])){
						res.setDescription(lista[1].toString());
					} else {
						res.setDescription(null);
					}					
					res.setNominalValue(new BigDecimal(lista[2].toString()));     //valor nominal
					res.setCurrencyDesc((currencyMap.get(lista[3])).toString());  //moneda
					res.setBalanceTotal(new BigDecimal(lista[4].toString()));     //saldo contable
					res.setBalanceAvailable(new BigDecimal(lista[5].toString())); // saldo disponible
					
					if (valorizationBalanceFilterTO.getValorizationType().equals(ValorizationQueryType.MARKET_FACT.getCode())){
						res.setHolderAccount((Long) lista[8]);
						res.setParticipant(valorizationBalanceFilterTO.getIdParticipantPk());
					}
					
					//BalanceValorizContabBOB
					BigDecimal totalValorization = BigDecimal.ZERO;
					BigDecimal availValorization = BigDecimal.ZERO;
					Integer currency = (Integer)(Integer) lista[3];
					
					if(valorizationBalanceFilterTO.getValorizationType().equals(ValorizationQueryType.VAL_NOMINAL.getCode())){
						totalValorization = res.getNominalValue().multiply(res.getBalanceTotal());
						availValorization = res.getNominalValue().multiply(res.getBalanceAvailable());
					}else if(valorizationBalanceFilterTO.getValorizationType().equals(ValorizationQueryType.MARKET_FACT.getCode())){
						totalValorization = (BigDecimal) lista[6];
						availValorization = (BigDecimal) lista[7];
					}
					
					
					if (currency.equals(CurrencyType.UFV.getCode())){
						BigDecimal ufv = tipoCambio.get(currency);
						
						res.setBalanceValorizContabBOB(totalValorization.multiply(ufv).setScale(8, RoundingMode.HALF_UP));
						
						//BalaceValorizContabUSD
						res.setBalanceValorizContabUSD(res.getBalanceValorizContabBOB().divide(tipoCambioUSD, 8,RoundingMode.HALF_UP));
						                              
						
					}else if (currency.equals(CurrencyType.USD.getCode())){
						BigDecimal usd = tipoCambio.get(currency);
						res.setBalanceValorizContabBOB(totalValorization.multiply(usd).setScale(8, RoundingMode.HALF_UP));
						
						//BalaceValorizContabUSD
						res.setBalanceValorizContabUSD(totalValorization);
						
						
					}else if (currency.equals(CurrencyType.EU.getCode())){
						BigDecimal eu = tipoCambio.get(currency);
						res.setBalanceValorizContabBOB(totalValorization.multiply(eu).setScale(8, RoundingMode.HALF_UP));

						//BalaceValorizContabUSD
						res.setBalanceValorizContabUSD(res.getBalanceValorizContabBOB().divide(tipoCambioUSD, 8,RoundingMode.HALF_UP));
						
					}else if (currency.equals(CurrencyType.PYG.getCode())){
						res.setBalanceValorizContabBOB(totalValorization);
						
						//BalaceValorizContabUSD
						res.setBalanceValorizContabUSD(res.getBalanceValorizContabBOB().divide(tipoCambioUSD, 8,RoundingMode.HALF_UP));
					
					}else if (currency.equals(CurrencyType.DMV.getCode())){
						
						BigDecimal dmv = tipoCambio.get(currency);
						res.setBalanceValorizContabBOB(totalValorization.multiply(dmv).setScale(8, RoundingMode.HALF_UP));
						
						//BalaceValorizContabUSD
						res.setBalanceValorizContabUSD(res.getBalanceValorizContabBOB().divide(tipoCambioUSD, 8,RoundingMode.HALF_UP));
					}
					
					/**CALCULE FOR AVAILABLE BALANCE*/
					if (currency.equals(CurrencyType.UFV.getCode())){
						BigDecimal ufv = tipoCambio.get(currency);
						res.setBalanceValorizAvailabBOB(availValorization.multiply(ufv).setScale(8, RoundingMode.HALF_UP));
						
						//BalanceValorizAvailabUSD
						res.setBalanceValorizAvailabUSD(res.getBalanceValorizAvailabBOB().divide(tipoCambioUSD, 8,RoundingMode.HALF_UP));
						
					}else if (currency.equals(CurrencyType.USD.getCode())){
						BigDecimal usd = tipoCambio.get(currency);
						res.setBalanceValorizAvailabBOB(availValorization.multiply(usd).setScale(8, RoundingMode.HALF_UP));
						
						//BalanceValorizAvailabUSD
						res.setBalanceValorizAvailabUSD(availValorization);
						
					}else if (currency.equals(CurrencyType.EU.getCode())){
						BigDecimal eu = tipoCambio.get(currency);
						res.setBalanceValorizAvailabBOB(availValorization.multiply(eu).setScale(8, RoundingMode.HALF_UP));
						
						//BalanceValorizAvailabUSD
						res.setBalanceValorizAvailabUSD(res.getBalanceValorizAvailabBOB().divide(tipoCambioUSD, 8,RoundingMode.HALF_UP));
						
					}else if (currency.equals(CurrencyType.PYG.getCode())){
						res.setBalanceValorizAvailabBOB(availValorization);
						
						//BalanceValorizAvailabUSD
						res.setBalanceValorizAvailabUSD(res.getBalanceValorizAvailabBOB().divide(tipoCambioUSD, 8,RoundingMode.HALF_UP));
						
					}else if (currency.equals(CurrencyType.DMV.getCode())){
						BigDecimal dmv = tipoCambio.get(currency);
						res.setBalanceValorizAvailabBOB(availValorization.multiply(dmv).setScale(8, RoundingMode.HALF_UP));
						
						//BalanceValorizAvailabUSD
						res.setBalanceValorizAvailabUSD(res.getBalanceValorizAvailabBOB().divide(tipoCambioUSD, 8,RoundingMode.HALF_UP));
					}
					
					listHolderAccountMovInfoTO.add(res);
				}
				resultado[0]=listHolderAccountMovInfoTO;
		
		
		resultado[1]=tipoCambio;
		return resultado;
	}
	
	/**
	 * Gets the market fact detail.
	 *
	 * @param securityCode the security code
	 * @param holderAccount the holder account
	 * @param Participant the participant
	 * @return the market fact detail
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<MarketFactResultTO> getMarketFactDetail(String securityCode, Long holderAccount, Long Participant) throws ServiceException{
		
		Query query = null;
		StringBuffer sbQuery = new StringBuffer();
		
		List<MarketFactResultTO> listMarketFactResultTO = new ArrayList<MarketFactResultTO>();
		
		
		sbQuery.append(" select hmb.totalBalance, hmb.availableBalance, hmb.marketDate, hmb.marketRate, hmb.marketPrice ");
		sbQuery.append(" from HolderMarketFactBalance hmb ");
		sbQuery.append(" where hmb.participant.idParticipantPk = :participant ");
		sbQuery.append(" and  hmb.security.idSecurityCodePk = :securityCode ");
		sbQuery.append(" and  hmb.holderAccount.idHolderAccountPk = :holderAccount ");
		sbQuery.append(" and  hmb.indActive = :oneParameter ");
		
		query = em.createQuery(sbQuery.toString());
		query.setParameter("participant", Participant); 
		query.setParameter("securityCode", securityCode);
		query.setParameter("holderAccount", holderAccount);
		query.setParameter("oneParameter", BooleanType.YES.getCode());
		
		List<Object[]> result = query.getResultList(); 
		
		MarketFactResultTO marketFactResultTO;
		for(Object[] lista : result){
			marketFactResultTO = new MarketFactResultTO();
			marketFactResultTO.setTotalBalance((BigDecimal) lista[0]);
			marketFactResultTO.setTotalAvailable((BigDecimal) lista[1]);
			marketFactResultTO.setMarketDate((Date) lista[2]);
			marketFactResultTO.setMarketRate((BigDecimal) lista[3]);
			marketFactResultTO.setMarketPrice((BigDecimal) lista[4]);
			listMarketFactResultTO.add(marketFactResultTO);
		}
		
		return listMarketFactResultTO;
	}
	
	
	
	/**
	 * Gets the daily exchange rates service bean.
	 *
	 * @param currencyType the currency type
	 * @return the daily exchange rates service bean
	 * @throws ServiceException the service exception
	 */
	public DailyExchangeRates getDailyExchangeRatesServiceBean(CurrencyType currencyType) throws ServiceException{
		String query = "SELECT edr FROM DailyExchangeRates edr WHERE edr.idCurrency = :parameterCurrency ORDER BY edr.iddailyExchangepk DESC";
		Query queryJpql = em.createQuery(query);
		queryJpql.setParameter("parameterCurrency", currencyType.getCode());
		queryJpql.setMaxResults(1);
		DailyExchangeRates daileExchangeRates ;
		try{
			daileExchangeRates = (DailyExchangeRates) queryJpql.getSingleResult(); 
		}catch(NoResultException ex){
			return null;
		}
		return daileExchangeRates;
	}
	
	/**
	 * Gets Physical Valued Balances service bean.
	 *
	 * @param objPhysicalValuedBalancesTO the physical to
	 * @return the Physical Valued service bean
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public Object[] getPhysicalValuedBalancesService(PhysicalValuedBalancesTO objPhysicalValuedBalancesTO) throws ServiceException {
		Query query = null;
		Object[] resulted = new Object[2];
		StringBuffer sbQuery = new StringBuffer();
		Map<Integer, BigDecimal> mapExchangeRate = parameterService.getDayExchangesRates(objPhysicalValuedBalancesTO.getDateCourte());		
		ParameterTableTO filter = new ParameterTableTO();
		Map<Integer,String> currencyMap = new HashMap<Integer, String>();
		filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
			currencyMap.put(param.getParameterTablePk(), param.getText1());
		}		
		if (objPhysicalValuedBalancesTO.getIdValorizationType().equals(ValorizationQueryType.VAL_NOMINAL.getCode())){			
			sbQuery.append(" Select pc.securities.idSecurityCodePk,  ");
			sbQuery.append(" pc.securities.description,  ");
			sbQuery.append(" pc.securities.currentNominalValue,  ");
			sbQuery.append(" pc.securities.currency,  ");
			sbQuery.append(" pbd.certificateQuantity,  "); 			
			sbQuery.append(" pbd.holderAccount.alternateCode,  ");
			sbQuery.append(" pbd.depositeDate,  ");
			sbQuery.append(" pc.certificateNumber,  ");
			sbQuery.append(" pc.idPhysicalCertificatePk ");
			sbQuery.append(" from PhysicalBalanceDetail pbd  ");
			sbQuery.append(" inner join pbd.physicalCertificate pc  ");
			sbQuery.append(" where pbd.participant.idParticipantPk = :parParticipantPk  ");
			//sbQuery.append(" and pbd.state = :parState ");
			sbQuery.append(" and trunc(pbd.depositeDate) <= :parDateCourte ");
			sbQuery.append(" and pbd.certificateQuantity > 0 ");
			if(!objPhysicalValuedBalancesTO.getDateCourte().before(CommonsUtilities.currentDate())){
				sbQuery.append(" and pbd.state = :parState ");
			} else {
				sbQuery.append(" and trunc(NVL(pbd.retiroDate,SYSDATE)) > :parDateCourte ");
			}
		} else if(objPhysicalValuedBalancesTO.getIdValorizationType().equals(ValorizationQueryType.MARKET_FACT.getCode())){
			sbQuery.append(" Select pc.securities.idSecurityCodePk,");
			sbQuery.append(" pc.securities.description,");
			sbQuery.append(" pc.securities.currentNominalValue,");
			sbQuery.append(" pc.securities.currency,");
			sbQuery.append(" pbd.certificateQuantity,");
			sbQuery.append(" pbd.holderAccount.alternateCode,");
			sbQuery.append(" pbd.depositeDate,  ");
			sbQuery.append(" pc.certificateNumber,  ");
			sbQuery.append(" pc.idPhysicalCertificatePk, ");
			sbQuery.append(" sum(round(nvl(pcmark.marketPrice,0), 2) * pcmark.quantity),");
			sbQuery.append(" pbd.holderAccount.idHolderAccountPk ");			
			sbQuery.append(" from PhysicalBalanceDetail pbd  ");
			sbQuery.append(" inner join pbd.physicalCertificate pc ");
			sbQuery.append(" inner join pc.physicalCertMarketfacts pcmark ");
			sbQuery.append(" where pbd.participant.idParticipantPk = :parParticipantPk  ");
			//sbQuery.append(" and pbd.state = :parState ");
			sbQuery.append(" and pcmark.IdPhysicalCertMarketfactPk = ");			
			sbQuery.append(" (select max(pcm.IdPhysicalCertMarketfactPk) from PhysicalCertMarketfact pcm where  ");
			sbQuery.append(" trunc(pcm.registryDate) <= :parDateCourte and pcm.physicalCertificate.idPhysicalCertificatePk = pc.idPhysicalCertificatePk ) ");			
			sbQuery.append(" and pbd.certificateQuantity > 0 ");			
			if(!objPhysicalValuedBalancesTO.getDateCourte().before(CommonsUtilities.currentDate())){
				sbQuery.append(" and pcmark.indActive = :indActive ");
				sbQuery.append(" and pbd.state = :parState ");
			} else {
				sbQuery.append(" and trunc(NVL(pbd.retiroDate,SYSDATE)) > :parDateCourte ");
			}					
		}
			
		if (objPhysicalValuedBalancesTO.getObjHolder().getIdHolderPk() != null){
			sbQuery.append(" and pbd.holderAccount.idHolderAccountPk in ( ");
			sbQuery.append(" select had.holderAccount.idHolderAccountPk from HolderAccountDetail had where had.holder.idHolderPk = :parHolder )  ");
		}							
		if (objPhysicalValuedBalancesTO.getObjSecurity().getIdSecurityCodePk() != null){	
			sbQuery.append(" and pc.securities.idSecurityCodePk = :parSecurityCodePk ");
		}			
		if (objPhysicalValuedBalancesTO.getObjIssuer().getIdIssuerPk() != null){
			sbQuery.append(" and pc.issuer.idIssuerPk = :parIssuerPk ");
		}
		if (objPhysicalValuedBalancesTO.getIdCurrency() != null){
			sbQuery.append(" and pbd.currency = :parCurrency ");
		}
		if (objPhysicalValuedBalancesTO.getIdSecurityClass() != null){	
			sbQuery.append(" and pc.securities.securityClass = :parSecurityClass ");
		}		
		if (objPhysicalValuedBalancesTO.getIdValorizationType().equals(ValorizationQueryType.MARKET_FACT.getCode())){
			sbQuery.append(" group by pc.securities.idSecurityCodePk, pc.securities.description, "); 
			sbQuery.append(" pc.securities.currentNominalValue,  pc.securities.currency, pbd.certificateQuantity, ");
			sbQuery.append(" pbd.holderAccount.alternateCode, pbd.depositeDate, pc.certificateNumber, pbd.holderAccount.idHolderAccountPk, pc.idPhysicalCertificatePk ");
		}
		sbQuery.append(" order by pbd.depositeDate desc, pbd.holderAccount.alternateCode, pc.securities.description "); 
		
		query = em.createQuery(sbQuery.toString());
		query.setParameter("parParticipantPk", objPhysicalValuedBalancesTO.getIdParticipantPk());			
		query.setParameter("parDateCourte", objPhysicalValuedBalancesTO.getDateCourte());
		//query.setParameter("parState", new Integer(605));		
		if (objPhysicalValuedBalancesTO.getObjHolder().getIdHolderPk() != null){
			query.setParameter("parHolder", objPhysicalValuedBalancesTO.getObjHolder().getIdHolderPk());
		}
		if (objPhysicalValuedBalancesTO.getObjSecurity().getIdSecurityCodePk() != null){	
			query.setParameter("parSecurityCodePk", objPhysicalValuedBalancesTO.getObjSecurity().getIdSecurityCodePk());
		}					
		if (objPhysicalValuedBalancesTO.getObjIssuer().getIdIssuerPk() != null){
			query.setParameter("parIssuerPk", objPhysicalValuedBalancesTO.getObjIssuer().getIdIssuerPk());  
		}
		if (objPhysicalValuedBalancesTO.getIdCurrency() != null){
			query.setParameter("parCurrency", objPhysicalValuedBalancesTO.getIdCurrency().intValue()); 
		}
		if (objPhysicalValuedBalancesTO.getIdSecurityClass() != null){	
			query.setParameter("parSecurityClass", objPhysicalValuedBalancesTO.getIdSecurityClass());
		}	
		if(!objPhysicalValuedBalancesTO.getDateCourte().before(CommonsUtilities.currentDate())){
			query.setParameter("parState", new Integer(605));
			if (objPhysicalValuedBalancesTO.getIdValorizationType().equals(ValorizationQueryType.MARKET_FACT.getCode())){
				query.setParameter("indActive", BooleanType.YES.getCode());
			}
		}
		
		List<Object[]> result = query.getResultList();			
		List<ValorizationBalanceDetailTO> listHolderAccountMovInfoTO = new  ArrayList<ValorizationBalanceDetailTO>();
		BigDecimal tipoCambioUSD = mapExchangeRate.get(CurrencyType.USD.getCode());
		for(Object[] lista : result){
			ValorizationBalanceDetailTO res = new ValorizationBalanceDetailTO();				
			res.setClaseClaveValor(lista[0].toString());
			if(Validations.validateIsNotNullAndNotEmpty(lista[1])){
				res.setDescription(lista[1].toString());
			} else {
				res.setDescription(null);
			}	
			res.setNominalValue(new BigDecimal(lista[2].toString()));
			res.setCurrencyDesc((currencyMap.get(lista[3])).toString());
			res.setCertificateQuqntity(new BigDecimal(lista[4].toString()));
			res.setStrAlternateCode(lista[5].toString());	
			if(lista[6] != null){
				res.setDtDateDeposited((Date) lista[6]);
			}			
			if(lista[7] != null){
				res.setLngCertNumber(new Long(lista[7].toString()));
			} 			
			res.setIdPhysicalCertificate(new Long(lista[8].toString()));
			if (objPhysicalValuedBalancesTO.getIdValorizationType().equals(ValorizationQueryType.MARKET_FACT.getCode())){
				res.setHolderAccount((Long) lista[10]);
				res.setParticipant(objPhysicalValuedBalancesTO.getIdParticipantPk());
			}			
			BigDecimal totalValorization = BigDecimal.ZERO;
			Integer currency = (Integer)(Integer) lista[3];	
			if(objPhysicalValuedBalancesTO.getIdValorizationType().equals(ValorizationQueryType.VAL_NOMINAL.getCode())){
				totalValorization = res.getNominalValue().multiply(res.getCertificateQuqntity());				
			}else if(objPhysicalValuedBalancesTO.getIdValorizationType().equals(ValorizationQueryType.MARKET_FACT.getCode())){
				totalValorization = (BigDecimal) lista[9];				
			}					
			if (currency.equals(CurrencyType.UFV.getCode())){
				BigDecimal ufv = mapExchangeRate.get(currency);					
				res.setBalanceValorizContabBOB(totalValorization.multiply(ufv).setScale(8, RoundingMode.HALF_UP));
				res.setBalanceValorizContabUSD(res.getBalanceValorizContabBOB().divide(tipoCambioUSD, 8,RoundingMode.HALF_UP));					
			}else if (currency.equals(CurrencyType.USD.getCode())){
				BigDecimal usd = mapExchangeRate.get(currency);
				res.setBalanceValorizContabBOB(totalValorization.multiply(usd).setScale(8, RoundingMode.HALF_UP));
				res.setBalanceValorizContabUSD(totalValorization);					
			}else if (currency.equals(CurrencyType.EU.getCode())){
				BigDecimal eu = mapExchangeRate.get(currency);
				res.setBalanceValorizContabBOB(totalValorization.multiply(eu).setScale(8, RoundingMode.HALF_UP));
				res.setBalanceValorizContabUSD(res.getBalanceValorizContabBOB().divide(tipoCambioUSD, 8,RoundingMode.HALF_UP));					
			}else if (currency.equals(CurrencyType.PYG.getCode())){
				res.setBalanceValorizContabBOB(totalValorization);
				res.setBalanceValorizContabUSD(res.getBalanceValorizContabBOB().divide(tipoCambioUSD, 8,RoundingMode.HALF_UP));				
			}else if (currency.equals(CurrencyType.DMV.getCode())){					
				BigDecimal dmv = mapExchangeRate.get(currency);
				res.setBalanceValorizContabBOB(totalValorization.multiply(dmv).setScale(8, RoundingMode.HALF_UP));
				res.setBalanceValorizContabUSD(res.getBalanceValorizContabBOB().divide(tipoCambioUSD, 8,RoundingMode.HALF_UP));
			}				
			listHolderAccountMovInfoTO.add(res);
		}
		resulted[0] = listHolderAccountMovInfoTO;
		resulted[1] = mapExchangeRate;		
		return resulted;
	}
	
	/**
	 * Gets Physical Market Fact service bean.
	 *
	 * @param securityCode the security code
	 * @param holderAccount the holder account
	 * @param Participant the participant
	 * @param physicalCertificate the physical certificate
	 * @param dtDateCourte the dt date courte
	 * @return the List Physical Market Fact service bean
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<MarketFactResultTO> getPhysicalMarketFactDetail(String securityCode, Long holderAccount, Long Participant, 
			Long physicalCertificate, Date dtDateCourte) throws ServiceException{		
		Query query = null;
		StringBuffer sbQuery = new StringBuffer();		
		List<MarketFactResultTO> listMarketFactResultTO = new ArrayList<MarketFactResultTO>();		
		sbQuery.append(" Select pcmark.quantity,");					
		sbQuery.append(" pcmark.marketDate,");
		sbQuery.append(" pcmark.marketRate,");
		sbQuery.append(" pcmark.marketPrice ");		
		sbQuery.append(" from PhysicalBalanceDetail pbd  ");
		sbQuery.append(" inner join pbd.physicalCertificate pc ");
		sbQuery.append(" inner join pc.physicalCertMarketfacts pcmark ");		
		sbQuery.append(" where pbd.participant.idParticipantPk = :parParticipantPk  ");
		sbQuery.append(" and pc.securities.idSecurityCodePk = :parSecurityCodePk ");
		sbQuery.append(" and pbd.holderAccount.idHolderAccountPk = :parHolderAccountPk  ");			
		sbQuery.append(" and pc.idPhysicalCertificatePk = :parPhysicalCertificate ");				
		sbQuery.append(" and pcmark.IdPhysicalCertMarketfactPk = ");			
		sbQuery.append(" (select max(pcm.IdPhysicalCertMarketfactPk) from PhysicalCertMarketfact pcm where  ");
		sbQuery.append(" trunc(pcm.registryDate) <= :parDateCourte and pcm.physicalCertificate.idPhysicalCertificatePk = pc.idPhysicalCertificatePk ) ");		
		if(!dtDateCourte.before(CommonsUtilities.currentDate())){
			sbQuery.append(" and pcmark.indActive = :indActive ");
		}
		query = em.createQuery(sbQuery.toString());
		query.setParameter("parParticipantPk", Participant); 
		query.setParameter("parSecurityCodePk", securityCode);
		query.setParameter("parHolderAccountPk", holderAccount);
		if(!dtDateCourte.before(CommonsUtilities.currentDate())){
			query.setParameter("indActive", BooleanType.YES.getCode());
		}		
		query.setParameter("parPhysicalCertificate", physicalCertificate);	
		query.setParameter("parDateCourte", dtDateCourte);
		List<Object[]> result = query.getResultList(); 		
		MarketFactResultTO marketFactResultTO;
		for(Object[] lista : result){
			marketFactResultTO = new MarketFactResultTO();
			marketFactResultTO.setQuantity((BigDecimal) lista[0]);			
			marketFactResultTO.setMarketDate((Date) lista[1]);
			marketFactResultTO.setMarketRate((BigDecimal) lista[2]);
			marketFactResultTO.setMarketPrice((BigDecimal) lista[3]);
			listMarketFactResultTO.add(marketFactResultTO);
		}		
		return listMarketFactResultTO;
	}
	
	/**
	 * Issuer list.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Issuer> issuerList() throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		//Creating query
		sbQuery.append("	Select su	");		
		sbQuery.append("	FROM Issuer su	");
		sbQuery.append("	where su.stateIssuer=:stateIs	");
		sbQuery.append("	order by su.mnemonic	");
		Query query = em.createQuery(sbQuery.toString());
		// Setting parameter
		query.setParameter("stateIs", IssuerStateType.REGISTERED.getCode());
		return (List<Issuer>) query.getResultList();

	}
	
}