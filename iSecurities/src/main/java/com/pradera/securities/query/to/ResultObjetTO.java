package com.pradera.securities.query.to;

import java.util.Date;

public class ResultObjetTO {
	private Date startDate;
	private Date endDate;
	private Integer progress;
	private String  progressDesc;
	private boolean disabled;
	public ResultObjetTO() {
		progressDesc="SIN ESTADO";
		disabled=true;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Integer getProgress() {
		return progress;
	}
	public void setProgress(Integer progress) {
		this.progress = progress;
	}
	public String getProgressDesc() {
		return progressDesc;
	}
	public void setProgressDesc(String progressDesc) {
		this.progressDesc = progressDesc;
	}
	public boolean isDisabled() {
		return disabled;
	}
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}
	@Override
	public String toString(){
		System.out.println("PROGRESO:"+this.progress);
		System.out.println("DESC_PROG:"+this.getProgressDesc());
		return "";
	}
}
