package com.pradera.securities.query.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.business.to.MarketFactResultTO;
import com.pradera.core.component.business.to.ValorizationBalanceDetailTO;
import com.pradera.core.component.business.to.ValorizationCurrencyExTO;
import com.pradera.core.component.business.to.ValorizationTotalTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.view.SecuritiesHelperBean;
import com.pradera.core.component.helperui.view.SecurityHelpBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.ValorizationQueryType;
import com.pradera.securities.query.facade.ValorizationBalanceFacade;
import com.pradera.securities.query.to.PhysicalValuedBalancesTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PhysicalValuedBalancesBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@DepositaryWebBean
public class PhysicalValuedBalancesBean extends GenericBaseBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The participant list. */
	private List<Participant> lstParticipant;
	
	/** The List currency. */
	private List<ParameterTable> lstCurrency;
	
	/** The List Security Class. */
	private List<ParameterTable> lstSecuritieClass;
	
	/** The List Valor Type. */
	private List<ParameterTable> lstTypeValorization;
	
	/** The Physical Valued Balances TO. */
	private PhysicalValuedBalancesTO objPhysicalValuedBalancesTO;
	
	/** The security help bean. */
	@Inject
	SecurityHelpBean securityHelpBean;
	
	/** The securities help. */
	@Inject
	SecuritiesHelperBean securitiesHelp;
	
	/** The accounts facade. */
	@EJB
	AccountsFacade ejbAccountsFacade;
	
	/** The value hold balance service bean. */
	@EJB
	ValorizationBalanceFacade valorizationBalanceFacade;
	
	/** The General Parameters. */
	@EJB
	private GeneralParametersFacade ejbGeneralParametersFacade;
	
	/** The value hold balance detail data model. */
	private GenericDataModel<ValorizationBalanceDetailTO> gdmValorizationBalanceDetailTO;
	
	/** The gdm valorization total to. */
	private GenericDataModel<ValorizationTotalTO> gdmValorizationTotalTO;
	
	/** The lst valorization currency to. */
	private List<ValorizationCurrencyExTO> lstValorizationCurrencyTO;
	
	/** The map tipos cambio. */
	private Map<Integer,BigDecimal> mapTiposCambio;
	
	/** The Market fact result to model. */
	private GenericDataModel<MarketFactResultTO> MarketFactResultTOModel;
	
	/**  The issuer. */
	private List<Issuer> listIssuer;
	
	/**  The pk issuer. */
	private String issuerName;
	
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		objPhysicalValuedBalancesTO = new PhysicalValuedBalancesTO();
		objPhysicalValuedBalancesTO.setObjHolder(new Holder());
		objPhysicalValuedBalancesTO.setObjIssuer(new Issuer());
		objPhysicalValuedBalancesTO.setObjSecurity(new Security());
		objPhysicalValuedBalancesTO.setDateCourte(new Date());
		objPhysicalValuedBalancesTO.setIdValorizationType(ValorizationQueryType.VAL_NOMINAL.getCode());
		try{
			loadParticipant();
			issuerList();
			loadValorizationType();
			loadCurrency();
			loadCboFilterSecuritieClass();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			this.objPhysicalValuedBalancesTO.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
		}
		gdmValorizationTotalTO = new GenericDataModel<>(new ArrayList<ValorizationTotalTO>());
	}
	
	/**
	 * Load participant.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadParticipant() throws ServiceException{
		Participant participantTO = new Participant();
		this.lstParticipant = ejbAccountsFacade.getLisParticipantServiceBean(participantTO);
	}
	
	/**
	 * Load list Issuer.
	 *
	 * @throws ServiceException the service exception
	 */
	public void issuerList() throws ServiceException {
		this.setListIssuer(valorizationBalanceFacade.issuerList());
	}
	
	/**
	 * Load valued type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadValorizationType() throws ServiceException {
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setMasterTableFk(MasterTableType.VALORIZATION_TYPE.getCode());
		filterParameterTable.setState(1);
		lstTypeValorization = ejbGeneralParametersFacade.getListParameterTableServiceBean(filterParameterTable);
	}
	
	/**
	 * Load currency.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCurrency() throws ServiceException{		
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.CURRENCY.getCode() );
		lstCurrency = ejbGeneralParametersFacade.getListParameterTableServiceBean(parameterTableTO);				
	}
	
	/**
	 * Load cbo filter securitie class.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboFilterSecuritieClass() throws ServiceException{
		ParameterTableTO parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk( MasterTableType.SECURITIES_CLASS.getCode());
		lstSecuritieClass = ejbGeneralParametersFacade.getListParameterTableServiceBean(parameterTableTO);
	}
	
	/**
	 * Search value hold balance handler.
	 */
	@SuppressWarnings("unchecked")
	public void searchValueHoldBalanceHandler(){
		JSFUtilities.hideGeneralDialogues();
		try {						
			lstValorizationCurrencyTO = new ArrayList<ValorizationCurrencyExTO>();
			mapTiposCambio = new HashMap<Integer,BigDecimal>();
			objPhysicalValuedBalancesTO.getObjIssuer().setIdIssuerPk(issuerName);
			Object[] object = valorizationBalanceFacade.getPhysicalValuedBalancesService(objPhysicalValuedBalancesTO);			
			List<ValorizationBalanceDetailTO> lista = (List<ValorizationBalanceDetailTO>)object[0];
			mapTiposCambio = (HashMap<Integer, BigDecimal>)object[1];			
			ValorizationCurrencyExTO exTO = new ValorizationCurrencyExTO();
			exTO.setUsd(mapTiposCambio.get(CurrencyType.USD.getCode()));
			exTO.setUfv(mapTiposCambio.get(CurrencyType.UFV.getCode()));
			exTO.setEu(mapTiposCambio.get(CurrencyType.EU.getCode()));
			exTO.setDmv(mapTiposCambio.get(CurrencyType.DMV.getCode()));
			lstValorizationCurrencyTO.add(exTO);			
			if(lista != null){
				gdmValorizationBalanceDetailTO = new GenericDataModel<ValorizationBalanceDetailTO>(lista);				
				List<ValorizationTotalTO> lstValoresTotales = new ArrayList<ValorizationTotalTO>();
				ValorizationTotalTO valoresTotales = new ValorizationTotalTO();				
				BigDecimal totalBalanceBOB = new BigDecimal(0);
				BigDecimal totalBalanceUSD = new BigDecimal(0);								
				for (ValorizationBalanceDetailTO list : lista){
					totalBalanceBOB = totalBalanceBOB.add(list.getBalanceValorizContabBOB().setScale(2, RoundingMode.HALF_UP));
					totalBalanceUSD = totalBalanceUSD.add(list.getBalanceValorizContabUSD().setScale(2, RoundingMode.HALF_UP));					
				}
				valoresTotales.setCurrency(CurrencyType.PYG.getCodeIso());
				valoresTotales.setTotalBalance(totalBalanceBOB);				
				lstValoresTotales.add(valoresTotales);				
				valoresTotales = new ValorizationTotalTO();
				valoresTotales.setCurrency(CurrencyType.USD.getCodeIso());
				valoresTotales.setTotalBalance(totalBalanceUSD);
				lstValoresTotales.add(valoresTotales);					
				gdmValorizationTotalTO= new GenericDataModel<ValorizationTotalTO>(lstValoresTotales);
			}else{
				gdmValorizationBalanceDetailTO = new GenericDataModel<ValorizationBalanceDetailTO>();				
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Detail market fact.
	 *
	 * @param actionEvent the action event
	 */
	public void detailMarketFact(ActionEvent actionEvent){
		String securityCode =(String)(actionEvent.getComponent().getAttributes().get("attribIdSecurityCode"));
		Long holderAccount =(Long)(actionEvent.getComponent().getAttributes().get("attribHolderAccount"));
		Long participant =(Long)(actionEvent.getComponent().getAttributes().get("attribParticipant"));
		Long physicalCertificate =(Long)(actionEvent.getComponent().getAttributes().get("attribCertificate"));		
		List<MarketFactResultTO> lista;
		try {
			lista = valorizationBalanceFacade.getPhysicalMarketFactDetail(securityCode, holderAccount, participant,
					physicalCertificate, objPhysicalValuedBalancesTO.getDateCourte());
			MarketFactResultTOModel = new GenericDataModel<MarketFactResultTO>(lista);
		} catch (ServiceException e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Clean form.
	 */
	public void cleanFormHandler(){
		this.objPhysicalValuedBalancesTO = new PhysicalValuedBalancesTO();					
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			this.objPhysicalValuedBalancesTO.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
		}
		issuerName=null;
		this.gdmValorizationBalanceDetailTO = null;
		securitiesHelp.setSecurityDescription(null);
		gdmValorizationTotalTO = null;
		lstValorizationCurrencyTO = null;
		objPhysicalValuedBalancesTO.setObjHolder(new Holder());
		objPhysicalValuedBalancesTO.setObjIssuer(new Issuer());
		objPhysicalValuedBalancesTO.setObjSecurity(new Security());		
		objPhysicalValuedBalancesTO.setIdValorizationType(ValorizationQueryType.VAL_NOMINAL.getCode());
		objPhysicalValuedBalancesTO.setDateCourte(new Date());
	}
	
	/**
	 * Change participant.
	 */
	public void changeParticipant() {
		JSFUtilities.resetComponent("frmPhysicalValuedBalance:fldsFilters");		
		gdmValorizationBalanceDetailTO = new GenericDataModel<ValorizationBalanceDetailTO>(new ArrayList<ValorizationBalanceDetailTO>());
		gdmValorizationBalanceDetailTO = null;		
		objPhysicalValuedBalancesTO.setObjHolder(new Holder());
		objPhysicalValuedBalancesTO.setObjIssuer(new Issuer());
		objPhysicalValuedBalancesTO.setObjSecurity(new Security());		
		objPhysicalValuedBalancesTO.setIdValorizationType(ValorizationQueryType.VAL_NOMINAL.getCode());
		objPhysicalValuedBalancesTO.setDateCourte(new Date());
	}
	
	/**
	 * Change holder.
	 */
	public void searchHolderHandler() {
		JSFUtilities.resetComponent("frmPhysicalValuedBalance:fldsFilters");
		gdmValorizationBalanceDetailTO = new GenericDataModel<ValorizationBalanceDetailTO>(new ArrayList<ValorizationBalanceDetailTO>());
		gdmValorizationBalanceDetailTO = null;			
		objPhysicalValuedBalancesTO.setObjIssuer(new Issuer());
		objPhysicalValuedBalancesTO.setObjSecurity(new Security());		
		objPhysicalValuedBalancesTO.setIdValorizationType(ValorizationQueryType.VAL_NOMINAL.getCode());
		objPhysicalValuedBalancesTO.setDateCourte(new Date());		
	}
	
	/**
	 * Change issuer handler.
	 */
	public void changeIssuerHandler(){
		gdmValorizationBalanceDetailTO = new GenericDataModel<ValorizationBalanceDetailTO>(new ArrayList<ValorizationBalanceDetailTO>());
		gdmValorizationBalanceDetailTO = null;		
		objPhysicalValuedBalancesTO.setObjSecurity(new Security());		
		objPhysicalValuedBalancesTO.setIdValorizationType(ValorizationQueryType.VAL_NOMINAL.getCode());
		objPhysicalValuedBalancesTO.setDateCourte(new Date());		
	}
	
	/**
	 * Change security.
	 */
	public void validateSecurity(){
		gdmValorizationBalanceDetailTO = new GenericDataModel<ValorizationBalanceDetailTO>(new ArrayList<ValorizationBalanceDetailTO>());
		gdmValorizationBalanceDetailTO = null;		
		objPhysicalValuedBalancesTO.setIdValorizationType(ValorizationQueryType.VAL_NOMINAL.getCode());
		objPhysicalValuedBalancesTO.setDateCourte(new Date());		
	}
	
	/**
	 * Change participant.
	 */
	public void cleanResultValorized() {
		gdmValorizationBalanceDetailTO = new GenericDataModel<ValorizationBalanceDetailTO>(new ArrayList<ValorizationBalanceDetailTO>());
		gdmValorizationBalanceDetailTO = null;
	}
	
	/**
	 * Show isin detail.
	 *
	 * @param idSecurity the id security
	 */
	public void showSecurityDetail(String idSecurity){
		securityHelpBean.setSecurityCode(idSecurity);
		securityHelpBean.setName("securityHelp");
		securityHelpBean.searchSecurityHandler();
	}

	/**
	 * Gets the obj physical valued balances to.
	 *
	 * @return the objPhysicalValuedBalancesTO
	 */
	public PhysicalValuedBalancesTO getObjPhysicalValuedBalancesTO() {
		return objPhysicalValuedBalancesTO;
	}

	/**
	 * Sets the obj physical valued balances to.
	 *
	 * @param objPhysicalValuedBalancesTO the objPhysicalValuedBalancesTO to set
	 */
	public void setObjPhysicalValuedBalancesTO(
			PhysicalValuedBalancesTO objPhysicalValuedBalancesTO) {
		this.objPhysicalValuedBalancesTO = objPhysicalValuedBalancesTO;
	}

	/**
	 * Gets the user info.
	 *
	 * @return the userInfo
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the userInfo to set
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * Gets the lst participant.
	 *
	 * @return the lstParticipant
	 */
	public List<Participant> getLstParticipant() {
		return lstParticipant;
	}

	/**
	 * Sets the lst participant.
	 *
	 * @param lstParticipant the lstParticipant to set
	 */
	public void setLstParticipant(List<Participant> lstParticipant) {
		this.lstParticipant = lstParticipant;
	}

	/**
	 * Gets the lst currency.
	 *
	 * @return the lstCurrency
	 */
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}

	/**
	 * Sets the lst currency.
	 *
	 * @param lstCurrency the lstCurrency to set
	 */
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}

	/**
	 * Gets the lst securitie class.
	 *
	 * @return the lstSecuritieClass
	 */
	public List<ParameterTable> getLstSecuritieClass() {
		return lstSecuritieClass;
	}

	/**
	 * Sets the lst securitie class.
	 *
	 * @param lstSecuritieClass the lstSecuritieClass to set
	 */
	public void setLstSecuritieClass(List<ParameterTable> lstSecuritieClass) {
		this.lstSecuritieClass = lstSecuritieClass;
	}

	/**
	 * Gets the lst type valorization.
	 *
	 * @return the lstTypeValorization
	 */
	public List<ParameterTable> getLstTypeValorization() {
		return lstTypeValorization;
	}

	/**
	 * Sets the lst type valorization.
	 *
	 * @param lstTypeValorization the lstTypeValorization to set
	 */
	public void setLstTypeValorization(List<ParameterTable> lstTypeValorization) {
		this.lstTypeValorization = lstTypeValorization;
	}

	/**
	 * Gets the gdm valorization balance detail to.
	 *
	 * @return the gdmValorizationBalanceDetailTO
	 */
	public GenericDataModel<ValorizationBalanceDetailTO> getGdmValorizationBalanceDetailTO() {
		return gdmValorizationBalanceDetailTO;
	}

	/**
	 * Sets the gdm valorization balance detail to.
	 *
	 * @param gdmValorizationBalanceDetailTO the gdmValorizationBalanceDetailTO to set
	 */
	public void setGdmValorizationBalanceDetailTO(
			GenericDataModel<ValorizationBalanceDetailTO> gdmValorizationBalanceDetailTO) {
		this.gdmValorizationBalanceDetailTO = gdmValorizationBalanceDetailTO;
	}

	/**
	 * Gets the gdm valorization total to.
	 *
	 * @return the gdmValorizationTotalTO
	 */
	public GenericDataModel<ValorizationTotalTO> getGdmValorizationTotalTO() {
		return gdmValorizationTotalTO;
	}

	/**
	 * Sets the gdm valorization total to.
	 *
	 * @param gdmValorizationTotalTO the gdmValorizationTotalTO to set
	 */
	public void setGdmValorizationTotalTO(
			GenericDataModel<ValorizationTotalTO> gdmValorizationTotalTO) {
		this.gdmValorizationTotalTO = gdmValorizationTotalTO;
	}

	/**
	 * Gets the lst valorization currency to.
	 *
	 * @return the lstValorizationCurrencyTO
	 */
	public List<ValorizationCurrencyExTO> getLstValorizationCurrencyTO() {
		return lstValorizationCurrencyTO;
	}

	/**
	 * Sets the lst valorization currency to.
	 *
	 * @param lstValorizationCurrencyTO the lstValorizationCurrencyTO to set
	 */
	public void setLstValorizationCurrencyTO(
			List<ValorizationCurrencyExTO> lstValorizationCurrencyTO) {
		this.lstValorizationCurrencyTO = lstValorizationCurrencyTO;
	}

	/**
	 * Gets the market fact result to model.
	 *
	 * @return the marketFactResultTOModel
	 */
	public GenericDataModel<MarketFactResultTO> getMarketFactResultTOModel() {
		return MarketFactResultTOModel;
	}

	/**
	 * Sets the market fact result to model.
	 *
	 * @param marketFactResultTOModel the marketFactResultTOModel to set
	 */
	public void setMarketFactResultTOModel(
			GenericDataModel<MarketFactResultTO> marketFactResultTOModel) {
		MarketFactResultTOModel = marketFactResultTOModel;
	}

	/**
	 * Gets the list issuer.
	 *
	 * @return the list issuer
	 */
	public List<Issuer> getListIssuer() {
		return listIssuer;
	}

	/**
	 * Sets the list issuer.
	 *
	 * @param listIssuer the new list issuer
	 */
	public void setListIssuer(List<Issuer> listIssuer) {
		this.listIssuer = listIssuer;
	}

	/**
	 * Gets the issuer name.
	 *
	 * @return the issuer name
	 */
	public String getIssuerName() {
		return issuerName;
	}

	/**
	 * Sets the issuer name.
	 *
	 * @param issuerName the new issuer name
	 */
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	
	
	
	
}
