package com.pradera.securities.query.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * The Class SecurityMovementsResultTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class SecurityMovementsResultTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4700408954714461717L;
	
	/** The id securities movement pk. */
	private Long idSecuritiesMovementPk;
	
	/** The movement date. */
	private Date movementDate;
	
	/** The operation date. */
	private Date operationDate;
	
	/** The operation number. */
	private Long operationNumber;
	
	/** The id corporative operation pk. */
	private Long idCorporativeOperationPk;
	
	/** The movement type. */
	private Long movementType;
	
	/** The movement type description. */
	private String movementTypeDescription;
	
	/** The movement quantity. */
	private BigDecimal movementQuantity;
	
	/** The nominal value. */
	private BigDecimal nominalValue;
	
	/** The marketrice. */
	private BigDecimal marketPrice;
	
	/** The market rate. */
	private BigDecimal marketRate;
	
	/** The market date. */
	private Date marketDate;
	
	/**
	 * Instantiates a new security movements result to.
	 */
	public SecurityMovementsResultTO() {

	}

	/**
	 * Gets the movement date.
	 *
	 * @return the movement date
	 */
	public Date getMovementDate() {
		return movementDate;
	}

	/**
	 * Sets the movement date.
	 *
	 * @param movementDate the new movement date
	 */
	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}

	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return operationDate;
	}

	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	/**
	 * Gets the id corporative operation pk.
	 *
	 * @return the id corporative operation pk
	 */
	public Long getIdCorporativeOperationPk() {
		return idCorporativeOperationPk;
	}

	/**
	 * Sets the id corporative operation pk.
	 *
	 * @param idCorporativeOperationPk the new id corporative operation pk
	 */
	public void setIdCorporativeOperationPk(Long idCorporativeOperationPk) {
		this.idCorporativeOperationPk = idCorporativeOperationPk;
	}

	/**
	 * Gets the movement type.
	 *
	 * @return the movement type
	 */
	public Long getMovementType() {
		return movementType;
	}

	/**
	 * Sets the movement type.
	 *
	 * @param movementType the new movement type
	 */
	public void setMovementType(Long movementType) {
		this.movementType = movementType;
	}

	/**
	 * Gets the movement type description.
	 *
	 * @return the movement type description
	 */
	public String getMovementTypeDescription() {
		return movementTypeDescription;
	}

	/**
	 * Sets the movement type description.
	 *
	 * @param movementTypeDescription the new movement type description
	 */
	public void setMovementTypeDescription(String movementTypeDescription) {
		this.movementTypeDescription = movementTypeDescription;
	}

	/**
	 * Gets the movement quantity.
	 *
	 * @return the movement quantity
	 */
	public BigDecimal getMovementQuantity() {
		return movementQuantity;
	}

	/**
	 * Sets the movement quantity.
	 *
	 * @param movementQuantity the new movement quantity
	 */
	public void setMovementQuantity(BigDecimal movementQuantity) {
		this.movementQuantity = movementQuantity;
	}

	/**
	 * Gets the nominal value.
	 *
	 * @return the nominal value
	 */
	public BigDecimal getNominalValue() {
		return nominalValue;
	}

	/**
	 * Sets the nominal value.
	 *
	 * @param nominalValue the new nominal value
	 */
	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}

	/**
	 * Gets the marketrice.
	 *
	 * @return the marketrice
	 */
	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	/**
	 * Sets the marketrice.
	 *
	 * @param marketPrice the new market price
	 */
	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	/**
	 * Gets the market rate.
	 *
	 * @return the market rate
	 */
	public BigDecimal getMarketRate() {
		return marketRate;
	}

	/**
	 * Sets the market rate.
	 *
	 * @param marketRate the new market rate
	 */
	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	/**
	 * Gets the market date.
	 *
	 * @return the market date
	 */
	public Date getMarketDate() {
		return marketDate;
	}

	/**
	 * Sets the market date.
	 *
	 * @param marketDate the new market date
	 */
	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	/**
	 * Gets the id securities movement pk.
	 *
	 * @return the id securities movement pk
	 */
	public Long getIdSecuritiesMovementPk() {
		return idSecuritiesMovementPk;
	}

	/**
	 * Sets the id securities movement pk.
	 *
	 * @param idSecuritiesMovementPk the new id securities movement pk
	 */
	public void setIdSecuritiesMovementPk(Long idSecuritiesMovementPk) {
		this.idSecuritiesMovementPk = idSecuritiesMovementPk;
	}

	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}

	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}
	
	
		
}
