package com.pradera.securities.query.to;

import java.io.Serializable;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.model.notification.ProcessNotification;
import com.pradera.model.process.ProcessSchedule;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera. Copyright 2012.</li>
 * </ul>
 * 
 * The Class BusinessProcessTO.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 19/12/2012
 */
public class BusinessProcessTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id business process pk. */
	private Long idBusinessProcessPk;
	
	/** The process name. */
	private String processName;
	
	/** The macro process. */
	private Long macroProcess;
	
	/** The macro process name. */
	private String macroProcessName;
	
	/** The process level1. */
	private Long processLevel1;
	
	/** The process level1 name. */
	private String processLevel1Name;
	
	/** The process level2. */
	private Long processLevel2;
	
	/** The process level2 name. */
	private String processLevel2Name;
	
	/** The process type. */
	private Integer processType;
	
	/** The process type description. */
	private String processTypeDescription;
	
	/** The mnemonic. */
	private String mnemonic;
	
	/** The bean name. */
	private String beanName;
	
	/** The description. */
	private String description;
	
	/** The module name. */
	private String moduleName;
	
	/** The ind notification. */
	private Integer indNotification;
	
	/** The process schedule data model. */
	private GenericDataModel<ProcessSchedule> processScheduleDataModel;
	
	/** The process notification data model. */
	private GenericDataModel<ProcessNotification> processNotificationDataModel;

	/**
	 * Gets the id business process pk.
	 *
	 * @return the idBusinessProcessPk
	 */
	public Long getIdBusinessProcessPk() {
		return idBusinessProcessPk;
	}

	/**
	 * Sets the id business process pk.
	 *
	 * @param idBusinessProcessPk the idBusinessProcessPk to set
	 */
	public void setIdBusinessProcessPk(Long idBusinessProcessPk) {
		this.idBusinessProcessPk = idBusinessProcessPk;
	}

	/**
	 * Gets the process name.
	 *
	 * @return the processName
	 */
	public String getProcessName() {
		return processName;
	}

	/**
	 * Sets the process name.
	 *
	 * @param processName the processName to set
	 */
	public void setProcessName(String processName) {
		this.processName = processName;
	}

	/**
	 * Gets the macro process.
	 *
	 * @return the macroProcess
	 */
	public Long getMacroProcess() {
		return macroProcess;
	}

	/**
	 * Sets the macro process.
	 *
	 * @param macroProcess the macroProcess to set
	 */
	public void setMacroProcess(Long macroProcess) {
		this.macroProcess = macroProcess;
	}

	/**
	 * Gets the macro process name.
	 *
	 * @return the macroProcessName
	 */
	public String getMacroProcessName() {
		return macroProcessName;
	}

	/**
	 * Sets the macro process name.
	 *
	 * @param macroProcessName the macroProcessName to set
	 */
	public void setMacroProcessName(String macroProcessName) {
		this.macroProcessName = macroProcessName;
	}

	/**
	 * Gets the process level1.
	 *
	 * @return the processLevel1
	 */
	public Long getProcessLevel1() {
		return processLevel1;
	}

	/**
	 * Sets the process level1.
	 *
	 * @param processLevel1 the processLevel1 to set
	 */
	public void setProcessLevel1(Long processLevel1) {
		this.processLevel1 = processLevel1;
	}

	/**
	 * Gets the process level1 name.
	 *
	 * @return the processLevel1Name
	 */
	public String getProcessLevel1Name() {
		return processLevel1Name;
	}

	/**
	 * Sets the process level1 name.
	 *
	 * @param processLevel1Name the processLevel1Name to set
	 */
	public void setProcessLevel1Name(String processLevel1Name) {
		this.processLevel1Name = processLevel1Name;
	}

	/**
	 * Gets the process level2.
	 *
	 * @return the processLevel2
	 */
	public Long getProcessLevel2() {
		return processLevel2;
	}

	/**
	 * Sets the process level2.
	 *
	 * @param processLevel2 the processLevel2 to set
	 */
	public void setProcessLevel2(Long processLevel2) {
		this.processLevel2 = processLevel2;
	}

	/**
	 * Gets the process level2 name.
	 *
	 * @return the processLevel2Name
	 */
	public String getProcessLevel2Name() {
		return processLevel2Name;
	}

	/**
	 * Sets the process level2 name.
	 *
	 * @param processLevel2Name the processLevel2Name to set
	 */
	public void setProcessLevel2Name(String processLevel2Name) {
		this.processLevel2Name = processLevel2Name;
	}

	/**
	 * Gets the process type.
	 *
	 * @return the processType
	 */
	public Integer getProcessType() {
		return processType;
	}

	/**
	 * Sets the process type.
	 *
	 * @param processType the processType to set
	 */
	public void setProcessType(Integer processType) {
		this.processType = processType;
	}

	/**
	 * Gets the process type description.
	 *
	 * @return the processTypeDescription
	 */
	public String getProcessTypeDescription() {
		return processTypeDescription;
	}

	/**
	 * Sets the process type description.
	 *
	 * @param processTypeDescription the processTypeDescription to set
	 */
	public void setProcessTypeDescription(String processTypeDescription) {
		this.processTypeDescription = processTypeDescription;
	}

	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}

	/**
	 * Sets the mnemonic.
	 *
	 * @param mnemonic the mnemonic to set
	 */
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	/**
	 * Gets the bean name.
	 *
	 * @return the beanName
	 */
	public String getBeanName() {
		return beanName;
	}

	/**
	 * Sets the bean name.
	 *
	 * @param beanName the beanName to set
	 */
	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the module name.
	 *
	 * @return the moduleName
	 */
	public String getModuleName() {
		return moduleName;
	}

	/**
	 * Sets the module name.
	 *
	 * @param moduleName the moduleName to set
	 */
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	
	/**
	 * Gets the process schedule data model.
	 *
	 * @return the processScheduleDataModel
	 */
	public GenericDataModel<ProcessSchedule> getProcessScheduleDataModel() {
		return processScheduleDataModel;
	}

	/**
	 * Sets the process schedule data model.
	 *
	 * @param processScheduleDataModel the processScheduleDataModel to set
	 */
	public void setProcessScheduleDataModel(GenericDataModel<ProcessSchedule> processScheduleDataModel) {
		this.processScheduleDataModel = processScheduleDataModel;
	}

	/**
	 * Gets the process notification data model.
	 *
	 * @return the processNotificationDataModel
	 */
	public GenericDataModel<ProcessNotification> getProcessNotificationDataModel() {
		return processNotificationDataModel;
	}

	/**
	 * Sets the process notification data model.
	 *
	 * @param processNotificationDataModel the processNotificationDataModel to set
	 */
	public void setProcessNotificationDataModel(
			GenericDataModel<ProcessNotification> processNotificationDataModel) {
		this.processNotificationDataModel = processNotificationDataModel;
	}

	/**
	 * Gets the ind notification.
	 *
	 * @return the ind notification
	 */
	public Integer getIndNotification() {
		return indNotification;
	}

	/**
	 * Sets the ind notification.
	 *
	 * @param indNotification the new ind notification
	 */
	public void setIndNotification(Integer indNotification) {
		this.indNotification = indNotification;
	}
	
	/**
	 * Gets the checks for notification.
	 *
	 * @return the checks for notification
	 */
	public String getHasNotification(){
		String result =null;
		if(indNotification != null){
			if(indNotification.equals(BooleanType.YES.getCode())){
				result = BooleanType.YES.getValue();
			} else {
				result = BooleanType.NO.getValue();
			}
		}
		return result;
	}
	
}
