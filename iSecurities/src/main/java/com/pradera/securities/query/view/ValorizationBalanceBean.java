package com.pradera.securities.query.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.model.chart.ChartSeries;

import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.business.to.MarketFactResultTO;
//import com.pradera.core.component.business.to.MarketFactResultTO;
import com.pradera.core.component.business.to.ValorizationBalanceDetailTO;
import com.pradera.core.component.business.to.ValorizationCurrencyExTO;
import com.pradera.core.component.business.to.ValorizationTotalTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.core.component.helperui.view.IssuerHelperBean;
import com.pradera.core.component.helperui.view.SecuritiesHelperBean;
import com.pradera.core.component.helperui.view.SecurityHelpBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.ValorizationQueryType;
import com.pradera.securities.issuancesecuritie.util.PropertiesConstants;
import com.pradera.securities.query.facade.ValorizationBalanceFacade;
import com.pradera.securities.query.to.ValorizationBalanceFilterTO;

// TODO: Auto-generated Javadoc
/**
 * The Class Valuated Holding Balances
 * Consulta de Saldos en Tenencia Valorizados.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 28/05/2013
 */
@DepositaryWebBean
public class ValorizationBalanceBean extends GenericBaseBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The participant list. */
	private List<Participant> participantList;
	
	/**  The issuer. */
	private List<Issuer> listIssuer;
	
	/** The issuer name. */
	private String issuerName;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The value hold balance detail data model. */
	private GenericDataModel<ValorizationBalanceDetailTO> ValorizationBalanceDetailTOModel;
	
	/** The value hold balance to. */
	private ValorizationBalanceFilterTO valorizationBalanceFilterTO;
	
	/** The value hold balance service bean. */
	@EJB
	ValorizationBalanceFacade valorizationBalanceFacade;
	
	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
//	private CartesianChartModel categoryModel;
	
	/** The securities help. */
@Inject
	SecuritiesHelperBean securitiesHelp;
	
	/** The issuer help bean. */
	@Inject
	IssuerHelperBean issuerHelpBean;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	/** The general parameters facade. */
	@EJB
	private GeneralParametersFacade generalParametersFacade;
	
	/** The parameter service. */
	@EJB
	private ParameterServiceBean parameterService;
	
	
/** The lst account helper result to. */
//	private Holder holderRequest;
	private GenericDataModel<HolderAccountHelperResultTO> lstAccountHelperResultTo;
	
	/** The selected holder account helper result to. */
	private HolderAccountHelperResultTO selectedHolderAccountHelperResultTO;
	
	/** The lst type valorization. */
	private List<ParameterTable> lstTypeValorization;
	
	/** The lst valorization total data model to. */
	private GenericDataModel<ValorizationTotalTO> lstValorizationTotalDataModelTo;
	
	/** The map tipos cambio. */
	private Map<Integer,BigDecimal> mapTiposCambio;
	
	/** The currency map. */
	private Map<Integer,String> currencyMap = new HashMap<Integer, String>();
	
	/** The lst valorization currency to. */
	private List<ValorizationCurrencyExTO> lstValorizationCurrencyTO;
	
	/** The date court block. */
	private boolean dateCourtBlock;
	
	/** The Market fact result to model. */
	private GenericDataModel<MarketFactResultTO> MarketFactResultTOModel;
	
	
//	private Issuer issuerHelp = new Issuer();
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		
		beginConversation();
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		valorizationBalanceFilterTO = new ValorizationBalanceFilterTO();
		valorizationBalanceFilterTO.setHolderRequest(new Holder());
		valorizationBalanceFilterTO.setIssuerRequest(new Issuer());
		valorizationBalanceFilterTO.setSecurity(new Security());
		valorizationBalanceFilterTO.setDateCourte(new Date());
		dateCourtBlock=true;
		lstAccountHelperResultTo = null;
		
		valorizationBalanceFilterTO.setValorizationType(ValorizationQueryType.VAL_NOMINAL.getCode());

		try {
			loadParticipant();
			issuerList();
			loadCombosForSearch();
			ParameterTableTO  filter = new ParameterTableTO();
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
				currencyMap.put(param.getParameterTablePk(), param.getText1());
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		if(userInfo.getUserAccountSession().isParticipantInstitucion() ||userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){//Si el usuario es participante
			this.valorizationBalanceFilterTO.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
		}
		lstValorizationTotalDataModelTo = new GenericDataModel<>(new ArrayList<ValorizationTotalTO>());
	}
	
	/**
	 * Search value hold balance handler.
	 */
	@SuppressWarnings("unchecked")
	public void searchValueHoldBalanceHandler(){
		JSFUtilities.hideGeneralDialogues();
		if(selectedHolderAccountHelperResultTO==null){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
				    PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_REQUIRED ));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		else{
				try {
							
				lstValorizationCurrencyTO = new ArrayList<ValorizationCurrencyExTO>();
				mapTiposCambio = new HashMap<Integer,BigDecimal>();
				valorizationBalanceFilterTO.getIssuerRequest().setIdIssuerPk(issuerName);
				Object[] object = valorizationBalanceFacade.getValorizationBalanceFacade(valorizationBalanceFilterTO);
				
				List<ValorizationBalanceDetailTO> lista = (List<ValorizationBalanceDetailTO>)object[0];
				mapTiposCambio=(HashMap<Integer, BigDecimal>)object[1];
				
				ValorizationCurrencyExTO exTO = new ValorizationCurrencyExTO();
				exTO.setUsd(mapTiposCambio.get(CurrencyType.USD.getCode()));
				exTO.setUfv(mapTiposCambio.get(CurrencyType.UFV.getCode()));
				exTO.setEu(mapTiposCambio.get(CurrencyType.EU.getCode()));
				exTO.setDmv(mapTiposCambio.get(CurrencyType.DMV.getCode()));
				lstValorizationCurrencyTO.add(exTO);
				
				if(lista!=null){
					ValorizationBalanceDetailTOModel = new GenericDataModel<ValorizationBalanceDetailTO>(lista);
					
					List<ValorizationTotalTO> lstValoresTotales = new ArrayList<ValorizationTotalTO>();
					ValorizationTotalTO valoresTotales = new ValorizationTotalTO();
					
					BigDecimal totalBalanceBOB = new BigDecimal(0);
					BigDecimal totalBalanceUSD = new BigDecimal(0);
					
					BigDecimal availableBalanceBOB = new BigDecimal(0);
					BigDecimal availableBalanceUSD = new BigDecimal(0);
					
					for (ValorizationBalanceDetailTO list : lista){
						totalBalanceBOB=totalBalanceBOB.add(list.getBalanceValorizContabBOB().setScale(2, RoundingMode.HALF_UP));
						totalBalanceUSD=totalBalanceUSD.add(list.getBalanceValorizContabUSD().setScale(2, RoundingMode.HALF_UP));
						availableBalanceBOB=availableBalanceBOB.add(list.getBalanceValorizAvailabBOB().setScale(2, RoundingMode.HALF_UP));
						availableBalanceUSD=availableBalanceUSD.add(list.getBalanceValorizAvailabUSD().setScale(2, RoundingMode.HALF_UP));
					}
					valoresTotales.setCurrency(CurrencyType.PYG.getCodeIso());
					valoresTotales.setTotalBalance(totalBalanceBOB);
					valoresTotales.setAvailableBalance(availableBalanceBOB);
					lstValoresTotales.add(valoresTotales);
					
					valoresTotales = new ValorizationTotalTO();
					valoresTotales.setCurrency(CurrencyType.USD.getCodeIso());
					valoresTotales.setTotalBalance(totalBalanceUSD);
					valoresTotales.setAvailableBalance(availableBalanceUSD);
					lstValoresTotales.add(valoresTotales);
						
					lstValorizationTotalDataModelTo= new GenericDataModel<ValorizationTotalTO>(lstValoresTotales);
				}else{
					ValorizationBalanceDetailTOModel = new GenericDataModel<ValorizationBalanceDetailTO>();
					
				}
			} catch (ServiceException e) {
				e.printStackTrace();
			}
			
		}		
	}
	
	/**
	 * Creates the bar model.
	 */

	private void createBarModel() {
		// TODO Auto-generated method stub
		
		ChartSeries balancesContab = new  ChartSeries();
		balancesContab.setLabel("SALDO CONTABLE");
		
	}
	
	/**
	 * Load combos for search.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCombosForSearch() throws ServiceException {
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setMasterTableFk(MasterTableType.VALORIZATION_TYPE.getCode());
		filterParameterTable.setState(1);
		lstTypeValorization = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
	}
	
	/**
	 * Date select calendar.
	 */
	public void dateSelectCalendar(){
		ValorizationBalanceDetailTOModel = null;
	}
	
	/**
	 * Select type valorization.
	 */
	public void selectTypeValorization(){
		ValorizationBalanceDetailTOModel = null;
		valorizationBalanceFilterTO.setDateCourte(new Date());
		
		
		
		if (valorizationBalanceFilterTO.getValorizationType().equals(ValorizationQueryType.VAL_NOMINAL.getCode())){
			dateCourtBlock=true;
		}else{
			dateCourtBlock=false;
		}
		
		
	}

	/**
	 * Load list Issuer.
	 *
	 * @throws ServiceException the service exception
	 */
	public void issuerList() throws ServiceException {
		this.setListIssuer(valorizationBalanceFacade.issuerList());
	}
	
	/**
	 * Load participant.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadParticipant() throws ServiceException{
		Participant participantTO = new Participant();
		this.participantList = accountsFacade.getLisParticipantServiceBean(participantTO);
	}
	
	/**
	 * Instantiates a new placement segment mgmt bean.
	 */
	public ValorizationBalanceBean() {
		super();
	}

	/**
	 * Gets the value hold balance detail data model.
	 *
	 * @return the value hold balance detail data model
	 */
	public GenericDataModel<ValorizationBalanceDetailTO> getValueHoldBalanceDetailDataModel() {
		return ValorizationBalanceDetailTOModel;
	}

	/**
	 * Gets the valorization balance filter to.
	 *
	 * @return the valorization balance filter to
	 */
	public ValorizationBalanceFilterTO getValorizationBalanceFilterTO() {
		return valorizationBalanceFilterTO;
	}

	/**
	 * Sets the valorization balance filter to.
	 *
	 * @param valorizationBalanceFilterTO the new valorization balance filter to
	 */
	public void setValorizationBalanceFilterTO(
			ValorizationBalanceFilterTO valorizationBalanceFilterTO) {
		this.valorizationBalanceFilterTO = valorizationBalanceFilterTO;
	}

	/**
	 * Sets the value hold balance detail data model.
	 *
	 * @param valueHoldBalanceDetailDataModel the new value hold balance detail data model
	 */
	public void setValueHoldBalanceDetailDataModel(
			GenericDataModel<ValorizationBalanceDetailTO> valueHoldBalanceDetailDataModel) {
		this.ValorizationBalanceDetailTOModel = valueHoldBalanceDetailDataModel;
	}


	
	/**
	 * Validate user session.
	 */
	public void validateUserSession(){
		
	}
	
	/**
	 * Clean form.
	 */
	public void cleanFormHandler(){
		issuerName =null;
		
		this.valorizationBalanceFilterTO = new ValorizationBalanceFilterTO();
		valorizationBalanceFilterTO.setValorizationType(ValorizationQueryType.VAL_NOMINAL.getCode());
		
		
		if(userInfo.getUserAccountSession().isParticipantInstitucion() || userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){//Si el usuario es participante
			this.valorizationBalanceFilterTO.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
		}
		
		this.ValorizationBalanceDetailTOModel = null;
		securitiesHelp.setSecurityDescription(null);
		lstValorizationTotalDataModelTo=null;
		lstValorizationCurrencyTO=null;
		
		List<HolderAccountHelperResultTO> lista = new ArrayList<HolderAccountHelperResultTO>();
		lstAccountHelperResultTo= new GenericDataModel<>(lista);
		lstAccountHelperResultTo = null;
		
		valorizationBalanceFilterTO.setHolderRequest(new Holder());
		valorizationBalanceFilterTO.setIssuerRequest(new Issuer());
		valorizationBalanceFilterTO.setSecurity(new Security());
		valorizationBalanceFilterTO.setDateCourte(new Date());
		
	
	}

	/** The security help bean. */
	@Inject
	SecurityHelpBean securityHelpBean;
	
	/**
	 * Show isin detail.
	 * metodo para mostrar el detalle financiero del valor
	 *
	 * @param idSecurity the id security
	 */
	public void showSecurityDetail(String idSecurity){
		securityHelpBean.setSecurityCode(idSecurity);
		securityHelpBean.setName("securityHelp");
		securityHelpBean.searchSecurityHandler();
	}
	
	/**
	 * Change participant.
	 */
	public void changeParticipant(){
		JSFUtilities.resetComponent("frmValueHoldBalance:fldsFilters");
		
		this.valorizationBalanceFilterTO.setIdHolderAccountPk(null);
		this.valorizationBalanceFilterTO.setIdIssuerCode(null);
		lstAccountHelperResultTo = null;
		
		
		ValorizationBalanceDetailTOModel = null;
		
		valorizationBalanceFilterTO.setHolderRequest(new Holder());
		valorizationBalanceFilterTO.setIssuerRequest(new Issuer());
		valorizationBalanceFilterTO.setSecurity(new Security());
		securitiesHelp.setSecurityDescription(null);
		valorizationBalanceFilterTO.setValorizationType(ValorizationQueryType.VAL_NOMINAL.getCode());
		valorizationBalanceFilterTO.setDateCourte(new Date());
		
	}
	
	/**
	 * Search holder handler.
	 */
	public void searchHolderHandler(){
		try {
			
			JSFUtilities.resetComponent("frmValueHoldBalance:fldsFilters");
			
			
			ValorizationBalanceDetailTOModel = null;
			
			valorizationBalanceFilterTO.setIssuerRequest(new Issuer());
			valorizationBalanceFilterTO.setSecurity(new Security());
			securitiesHelp.setSecurityDescription(null);
			valorizationBalanceFilterTO.setValorizationType(ValorizationQueryType.VAL_NOMINAL.getCode());
			valorizationBalanceFilterTO.setDateCourte(new Date());
			
			Holder holder = valorizationBalanceFilterTO.getHolderRequest();			
			if (Validations.validateIsNull(holder) || Validations.validateIsNull(holder.getIdHolderPk())) {
//				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ADM_HOLDERACCOUNT_VALIDATION_HOLDERNULL,
//				new Object[] { String.valueOf(this.getHolderRequest().getIdHolderPk()) });
//				this.setHolderRequest(new Holder());
//				JSFUtilities.showSimpleValidationDialog();
//				return;
			} else {
				
				HolderTO holderTO = new HolderTO();
				holderTO.setHolderId(holder.getIdHolderPk());
				holderTO.setFlagAllHolders(true);
				holderTO.setParticipantFk(valorizationBalanceFilterTO.getIdParticipantPk());
				
				List<HolderAccountHelperResultTO> lista = helperComponentFacade.searchAccountByFilter(holderTO);
				
				if (lista==null || lista.isEmpty()){
					lstAccountHelperResultTo = new GenericDataModel<>();
				}else{
					lstAccountHelperResultTo = new GenericDataModel<>(lista);
					if(lista.size() == ComponentConstant.ONE){
						selectedHolderAccountHelperResultTO = lista.get(0);
					}
				}
				
				if(lstAccountHelperResultTo.getSize()<0){
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
							PropertiesUtilities.getGenericMessage(PropertiesConstants.COMMONS_LABEL_PARTICIPANT_CUI_INCORRECT));
					JSFUtilities.showSimpleValidationDialog();
				} 
			}
			
			if (lstAccountHelperResultTo!=null){
				JSFUtilities.executeJavascriptFunction("resultHelperAccountVar.clearFilters(); resultHelperAccountVar.unselectAllRows();");
			}
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Validate security.
	 */
	public void validateSecurity(){
		ValorizationBalanceDetailTOModel = null;
		
		valorizationBalanceFilterTO.setValorizationType(ValorizationQueryType.VAL_NOMINAL.getCode());
		valorizationBalanceFilterTO.setDateCourte(new Date());
		
		
	}
	
	/**
	 * Change select account.
	 */
	public void changeSelectAccount(){
		if(selectedHolderAccountHelperResultTO!=null){
			valorizationBalanceFilterTO.setIdHolderAccountPk(selectedHolderAccountHelperResultTO.getAccountPk());
//			joinPartHoldAccHandler();
		}
		
		
		ValorizationBalanceDetailTOModel = null;
		
		valorizationBalanceFilterTO.setIssuerRequest(new Issuer());
		valorizationBalanceFilterTO.setSecurity(new Security());
		securitiesHelp.setSecurityDescription(null);
		valorizationBalanceFilterTO.setValorizationType(ValorizationQueryType.VAL_NOMINAL.getCode());
		valorizationBalanceFilterTO.setDateCourte(new Date());
		
	}
	
	/**
	 * Fill data parameters account to.
	 *
	 * @throws ServiceException the service exception
	 */
	public void fillDataParametersAccountTO() throws ServiceException{
		List<ParameterTable> lstHolderAccountState = generalParametersFacade.getListParameterTableServiceBean(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());

		for (HolderAccountHelperResultTO objAccount : lstAccountHelperResultTo) {
			for (ParameterTable parameterTable : lstHolderAccountState) {
				if(Integer.valueOf(objAccount.getAccountStatusDescription()).equals(parameterTable.getParameterTablePk())){
					objAccount.setAccountStatusDescription(parameterTable.getParameterName());
					break;
				}
			}
		}
		
		
	}
	
	
	
	/**
	 * Change holder account.
	 */
	public void changeHolderAccount(){
		this.valorizationBalanceFilterTO.setIdHolderAccountPk(null);
		this.valorizationBalanceFilterTO.setIdIssuerCode(null);
		this.valorizationBalanceFilterTO.setValorizationType(ValorizationQueryType.VAL_NOMINAL.getCode());
		this.ValorizationBalanceDetailTOModel = null;
		securitiesHelp.setSecurityDescription(null);
	}
	
	/**
	 * Change issuer handler.
	 */
	public void changeIssuerHandler(){
		ValorizationBalanceDetailTOModel = null;		
		valorizationBalanceFilterTO.setSecurity(new Security());
		securitiesHelp.setSecurityDescription(null);
		valorizationBalanceFilterTO.setValorizationType(ValorizationQueryType.VAL_NOMINAL.getCode());
		valorizationBalanceFilterTO.setDateCourte(new Date());
		
	}
	
	/**
	 * Change security handler.
	 */
	public void changeSecurityHandler(){
		this.valorizationBalanceFilterTO.setIdHolderAccountPk(null);
		this.valorizationBalanceFilterTO.setValorizationType(ValorizationQueryType.VAL_NOMINAL.getCode());
		this.ValorizationBalanceDetailTOModel = null;
	}
	
	/**
	 * Detail market fact.
	 *
	 * @param actionEvent the action event
	 */
	public void detailMarketFact(ActionEvent actionEvent){
		String securityCode =(String)(actionEvent.getComponent().getAttributes().get("attribIdSecurityCode"));
		Long holderAccount =(Long)(actionEvent.getComponent().getAttributes().get("attribHolderAccount"));
		Long participant =(Long)(actionEvent.getComponent().getAttributes().get("attribParticipant"));
		
		List<MarketFactResultTO> lista;
		try {
			lista = valorizationBalanceFacade.listMarketFactAccountTO(securityCode,holderAccount,participant);
			MarketFactResultTOModel = new GenericDataModel<MarketFactResultTO>(lista);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


	/**
	 * Gets the lst account helper result to.
	 *
	 * @return the lst account helper result to
	 */
	public GenericDataModel<HolderAccountHelperResultTO> getLstAccountHelperResultTo() {
		return lstAccountHelperResultTo;
	}

	/**
	 * Sets the lst account helper result to.
	 *
	 * @param lstAccountHelperResultTo the new lst account helper result to
	 */
	public void setLstAccountHelperResultTo(
			GenericDataModel<HolderAccountHelperResultTO> lstAccountHelperResultTo) {
		this.lstAccountHelperResultTo = lstAccountHelperResultTo;
	}

	/**
	 * Gets the selected holder account helper result to.
	 *
	 * @return the selected holder account helper result to
	 */
	public HolderAccountHelperResultTO getSelectedHolderAccountHelperResultTO() {
		return selectedHolderAccountHelperResultTO;
	}

	/**
	 * Sets the selected holder account helper result to.
	 *
	 * @param selectedHolderAccountHelperResultTO the new selected holder account helper result to
	 */
	public void setSelectedHolderAccountHelperResultTO(
			HolderAccountHelperResultTO selectedHolderAccountHelperResultTO) {
		this.selectedHolderAccountHelperResultTO = selectedHolderAccountHelperResultTO;
	}

	/**
	 * Gets the lst type valorization.
	 *
	 * @return the lst type valorization
	 */
	public List<ParameterTable> getLstTypeValorization() {
		return lstTypeValorization;
	}

	/**
	 * Sets the lst type valorization.
	 *
	 * @param lstTypeValorization the new lst type valorization
	 */
	public void setLstTypeValorization(List<ParameterTable> lstTypeValorization) {
		this.lstTypeValorization = lstTypeValorization;
	}

	/**
	 * Gets the lst valorization total data model to.
	 *
	 * @return the lst valorization total data model to
	 */
	public GenericDataModel<ValorizationTotalTO> getLstValorizationTotalDataModelTo() {
		return lstValorizationTotalDataModelTo;
	}

	/**
	 * Sets the lst valorization total data model to.
	 *
	 * @param lstValorizationTotalDataModelTo the new lst valorization total data model to
	 */
	public void setLstValorizationTotalDataModelTo(
			GenericDataModel<ValorizationTotalTO> lstValorizationTotalDataModelTo) {
		this.lstValorizationTotalDataModelTo = lstValorizationTotalDataModelTo;
	}

	/**
	 * Gets the currency map.
	 *
	 * @return the currency map
	 */
	public Map<Integer, String> getCurrencyMap() {
		return currencyMap;
	}

	/**
	 * Sets the currency map.
	 *
	 * @param currencyMap the currency map
	 */
	public void setCurrencyMap(Map<Integer, String> currencyMap) {
		this.currencyMap = currencyMap;
	}

	/**
	 * Gets the lst valorization currency to.
	 *
	 * @return the lst valorization currency to
	 */
	public List<ValorizationCurrencyExTO> getLstValorizationCurrencyTO() {
		return lstValorizationCurrencyTO;
	}

	/**
	 * Sets the lst valorization currency to.
	 *
	 * @param lstValorizationCurrencyTO the new lst valorization currency to
	 */
	public void setLstValorizationCurrencyTO(
			List<ValorizationCurrencyExTO> lstValorizationCurrencyTO) {
		this.lstValorizationCurrencyTO = lstValorizationCurrencyTO;
	}

	/**
	 * Checks if is date court block.
	 *
	 * @return true, if is date court block
	 */
	public boolean isDateCourtBlock() {
		return dateCourtBlock;
	}

	/**
	 * Sets the date court block.
	 *
	 * @param dateCourtBlock the new date court block
	 */
	public void setDateCourtBlock(boolean dateCourtBlock) {
		this.dateCourtBlock = dateCourtBlock;
	}

	/**
	 * Gets the market fact result to model.
	 *
	 * @return the market fact result to model
	 */
	public GenericDataModel<MarketFactResultTO> getMarketFactResultTOModel() {
		return MarketFactResultTOModel;
	}

	/**
	 * Sets the market fact result to model.
	 *
	 * @param marketFactResultTOModel the new market fact result to model
	 */
	public void setMarketFactResultTOModel(
			GenericDataModel<MarketFactResultTO> marketFactResultTOModel) {
		MarketFactResultTOModel = marketFactResultTOModel;
	}


	/**
	 * Gets the participant list.
	 *
	 * @return the participant list
	 */
	public List<Participant> getParticipantList() {
		return participantList;
	}

	/**
	 * Sets the participant list.
	 *
	 * @param participantList the new participant list
	 */
	public void setParticipantList(List<Participant> participantList) {
		this.participantList = participantList;
	}

	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public List<Issuer> getListIssuer() {
		return listIssuer;
	}

	/**
	 * Sets the list issuer.
	 *
	 * @param listIssuer the new list issuer
	 */
	public void setListIssuer(List<Issuer> listIssuer) {
		this.listIssuer = listIssuer;
	}

	/**
	 * Gets the issuer name.
	 *
	 * @return the issuer name
	 */
	public String getIssuerName() {
		return issuerName;
	}

	/**
	 * Sets the issuer name.
	 *
	 * @param issuerName the new issuer name
	 */
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	
	
	
}