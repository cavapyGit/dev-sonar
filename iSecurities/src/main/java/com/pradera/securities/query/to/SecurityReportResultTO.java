package com.pradera.securities.query.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * The Class SecurityMovementsResultTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class SecurityReportResultTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4700408954714461717L;

	/** The movement type description. */
	private String securityCode;
	
	/** The marketrice. */
	private Integer instrumentType;
	
	/** The market rate. */
	private BigDecimal marketRate;
	
	/** The market date. */
	private Date marketDate;
	
	private Integer operationPart;
	
	/**
	 * Instantiates a new security movements result to.
	 */
	public SecurityReportResultTO() {

	}

	/**
	 * Gets the market rate.
	 *
	 * @return the market rate
	 */
	public BigDecimal getMarketRate() {
		return marketRate;
	}

	/**
	 * Sets the market rate.
	 *
	 * @param marketRate the new market rate
	 */
	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	/**
	 * Gets the market date.
	 *
	 * @return the market date
	 */
	public Date getMarketDate() {
		return marketDate;
	}

	/**
	 * Sets the market date.
	 *
	 * @param marketDate the new market date
	 */
	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	public Integer getInstrumentType() {
		return instrumentType;
	}

	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	public Integer getOperationPart() {
		return operationPart;
	}

	public void setOperationPart(Integer operationPart) {
		this.operationPart = operationPart;
	}

	
	
		
}
