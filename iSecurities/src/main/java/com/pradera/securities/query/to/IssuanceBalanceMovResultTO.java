package com.pradera.securities.query.to;

import java.io.Serializable;
import java.math.BigDecimal;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class ValueHoldBalance.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 28/05/2013
 */
public class IssuanceBalanceMovResultTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id issuance pk. */
	private String idIssuancePk;
	
	/** The id issuer pk. */
	private String idIssuerPk;
	
	/** The issuance amount. */
	private BigDecimal issuanceAmount;
	
	/** The amount placed. */
	private BigDecimal amountPlaced;
	
	/** The amount by placed. */
	private BigDecimal amountByPlaced;
	
	/** The amount amortization. */
	private BigDecimal amountAmortization;
	
	/** The amount by amortization. */
	private BigDecimal amountByAmortization;
	
	/** The interest payment. */
	private BigDecimal interestPayment;

	/**
	 * Gets the id issuance pk.
	 *
	 * @return the id issuance pk
	 */
	public String getIdIssuancePk() {
		return idIssuancePk;
	}

	/**
	 * Sets the id issuance pk.
	 *
	 * @param idIssuancePk the new id issuance pk
	 */
	public void setIdIssuancePk(String idIssuancePk) {
		this.idIssuancePk = idIssuancePk;
	}

	/**
	 * Gets the id issuer pk.
	 *
	 * @return the id issuer pk
	 */
	public String getIdIssuerPk() {
		return idIssuerPk;
	}

	/**
	 * Sets the id issuer pk.
	 *
	 * @param idIssuerPk the new id issuer pk
	 */
	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}

	/**
	 * Gets the issuance amount.
	 *
	 * @return the issuance amount
	 */
	public BigDecimal getIssuanceAmount() {
		return issuanceAmount;
	}

	/**
	 * Sets the issuance amount.
	 *
	 * @param issuanceAmount the new issuance amount
	 */
	public void setIssuanceAmount(BigDecimal issuanceAmount) {
		this.issuanceAmount = issuanceAmount;
	}

	/**
	 * Gets the amount placed.
	 *
	 * @return the amount placed
	 */
	public BigDecimal getAmountPlaced() {
		return amountPlaced;
	}

	/**
	 * Sets the amount placed.
	 *
	 * @param amountPlaced the new amount placed
	 */
	public void setAmountPlaced(BigDecimal amountPlaced) {
		this.amountPlaced = amountPlaced;
	}

	/**
	 * Gets the amount by placed.
	 *
	 * @return the amount by placed
	 */
	public BigDecimal getAmountByPlaced() {
		return amountByPlaced;
	}

	/**
	 * Sets the amount by placed.
	 *
	 * @param amountByPlaced the new amount by placed
	 */
	public void setAmountByPlaced(BigDecimal amountByPlaced) {
		this.amountByPlaced = amountByPlaced;
	}

	/**
	 * Gets the amount amortization.
	 *
	 * @return the amount amortization
	 */
	public BigDecimal getAmountAmortization() {
		return amountAmortization;
	}

	/**
	 * Sets the amount amortization.
	 *
	 * @param amountAmortization the new amount amortization
	 */
	public void setAmountAmortization(BigDecimal amountAmortization) {
		this.amountAmortization = amountAmortization;
	}

	/**
	 * Gets the amount by amortization.
	 *
	 * @return the amount by amortization
	 */
	public BigDecimal getAmountByAmortization() {
		return amountByAmortization;
	}

	/**
	 * Sets the amount by amortization.
	 *
	 * @param amountByAmortization the new amount by amortization
	 */
	public void setAmountByAmortization(BigDecimal amountByAmortization) {
		this.amountByAmortization = amountByAmortization;
	}

	/**
	 * Gets the interest payment.
	 *
	 * @return the interest payment
	 */
	public BigDecimal getInterestPayment() {
		return interestPayment;
	}

	/**
	 * Sets the interest payment.
	 *
	 * @param interestPayment the new interest payment
	 */
	public void setInterestPayment(BigDecimal interestPayment) {
		this.interestPayment = interestPayment;
	}
}