package com.pradera.securities.query.facade;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.business.to.MarketFactResultTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.securities.query.service.ValorizationBalanceService;
import com.pradera.securities.query.to.PhysicalValuedBalancesTO;
import com.pradera.securities.query.to.ValorizationBalanceFilterTO;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class ValueHoldBalance.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 28/05/2013
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@Performance
public class ValorizationBalanceFacade{
	
	/** The value hold balance service bean. */
	@EJB
	ValorizationBalanceService valueHoldBalanceServiceBean;
	
	
	
	/**
	 * Gets the value hold balance service facade.
	 *
	 * @param valueHoldBalanceTO the value hold balance to
	 * @return the value hold balance service facade
	 * @throws ServiceException the service exception
	 */
	public Object[] getValorizationBalanceFacade(ValorizationBalanceFilterTO valueHoldBalanceTO) throws ServiceException{
		return valueHoldBalanceServiceBean.getValorizationBalanceService(valueHoldBalanceTO);
	}
	
	/**
	 * Gets the daily exchange rates service facade.
	 *
	 * @param currencyType the currency type
	 * @return the daily exchange rates service facade
	 * @throws ServiceException the service exception
	 */
	public DailyExchangeRates getDailyExchangeRatesServiceFacade(CurrencyType currencyType) throws ServiceException{
		return valueHoldBalanceServiceBean.getDailyExchangeRatesServiceBean(currencyType);
	}
	
	/**
	 * List market fact account to.
	 *
	 * @param securityCode the security code
	 * @param holderAccount the holder account
	 * @param Participant the participant
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<MarketFactResultTO>  listMarketFactAccountTO(String securityCode, Long holderAccount, Long Participant) throws ServiceException{
		return valueHoldBalanceServiceBean.getMarketFactDetail(securityCode,holderAccount,Participant);
	}
	
	/**
	 * Gets Physical Valued Balances service bean.
	 *
	 * @param objPhysicalValuedBalancesTO the physical to
	 * @return the Physical Valued service bean
	 * @throws ServiceException the service exception
	 */
	public Object[] getPhysicalValuedBalancesService(PhysicalValuedBalancesTO objPhysicalValuedBalancesTO) throws ServiceException{
		return valueHoldBalanceServiceBean.getPhysicalValuedBalancesService(objPhysicalValuedBalancesTO);
	}
	
	/**
	 * Gets Physical Market Fact service bean.
	 *
	 * @param securityCode the security code
	 * @param holderAccount the holder account
	 * @param Participant the participant
	 * @param physicalCertificate the physical certificate
	 * @param dtDateDeposit the dt date deposit
	 * @return the List Physical Market Fact service bean
	 * @throws ServiceException the service exception
	 */
	public List<MarketFactResultTO> getPhysicalMarketFactDetail(String securityCode, Long holderAccount, 
			Long Participant, Long physicalCertificate, Date dtDateDeposit) throws ServiceException{
		return valueHoldBalanceServiceBean.getPhysicalMarketFactDetail(securityCode, holderAccount, Participant, physicalCertificate, dtDateDeposit);
	}
	
	/**
	 * Issuer list.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Issuer> issuerList()throws ServiceException{
		return valueHoldBalanceServiceBean.issuerList();
	}
	
}