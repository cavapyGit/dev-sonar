package com.pradera.securities.query.to;

import java.io.Serializable;
import java.util.Date;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class ValueHoldBalance.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 28/05/2013
 */
public class IssuanceBalanceMovTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id issuance pk. */
	private String idIssuancePk;
	
	/** The id issuer pk. */
	private String idIssuerPk;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/** The issuance type. */
	private Integer issuanceType;

	/**
	 * Gets the id issuance pk.
	 *
	 * @return the id issuance pk
	 */
	public String getIdIssuancePk() {
		return idIssuancePk;
	}

	/**
	 * Sets the id issuance pk.
	 *
	 * @param idIssuancePk the new id issuance pk
	 */
	public void setIdIssuancePk(String idIssuancePk) {
		this.idIssuancePk = idIssuancePk;
	}

	/**
	 * Gets the id issuer pk.
	 *
	 * @return the id issuer pk
	 */
	public String getIdIssuerPk() {
		return idIssuerPk;
	}

	/**
	 * Sets the id issuer pk.
	 *
	 * @param idIssuerPk the new id issuer pk
	 */
	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	/**
	 * Gets the issuance type.
	 *
	 * @return the issuance type
	 */
	public Integer getIssuanceType() {
		return issuanceType;
	}

	/**
	 * Sets the issuance type.
	 *
	 * @param issuanceType the new issuance type
	 */
	public void setIssuanceType(Integer issuanceType) {
		this.issuanceType = issuanceType;
	}
}