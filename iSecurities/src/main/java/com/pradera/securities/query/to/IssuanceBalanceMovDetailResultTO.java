package com.pradera.securities.query.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class ValueHoldBalance.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 28/05/2013
 */
public class IssuanceBalanceMovDetailResultTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id issuance pk. */
	private String idIssuancePk;
	
	/** The id issuer pk. */
	private String idIssuerPk;
	
	/** The movement date. */
	private Date movementDate;
	
	/** The operation number. */
	private Integer operationNumber;
	
	/** The isin code. */
	private String isinCode;
	
	/** The isin description. */
	private String isinDescription;
	
	/** The movement type. */
	private Integer movementType;
	
	/** The movement description. */
	private String movementDescription;
	
	/** The amount operation. */
	private BigDecimal amountOperation;
	
	/** The nominal value. */
	private BigDecimal nominalValue;
	
	/** The security quantity. */
	private BigDecimal securityQuantity;

	/**
	 * Gets the movement date.
	 *
	 * @return the movement date
	 */
	public Date getMovementDate() {
		return movementDate;
	}

	/**
	 * Sets the movement date.
	 *
	 * @param movementDate the new movement date
	 */
	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}

	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Integer getOperationNumber() {
		return operationNumber;
	}

	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Integer operationNumber) {
		this.operationNumber = operationNumber;
	}

	/**
	 * Gets the isin code.
	 *
	 * @return the isin code
	 */
	public String getIsinCode() {
		return isinCode;
	}

	/**
	 * Sets the isin code.
	 *
	 * @param isinCode the new isin code
	 */
	public void setIsinCode(String isinCode) {
		this.isinCode = isinCode;
	}

	/**
	 * Gets the isin description.
	 *
	 * @return the isin description
	 */
	public String getIsinDescription() {
		return isinDescription;
	}

	/**
	 * Sets the isin description.
	 *
	 * @param isinDescription the new isin description
	 */
	public void setIsinDescription(String isinDescription) {
		this.isinDescription = isinDescription;
	}

	/**
	 * Gets the movement type.
	 *
	 * @return the movement type
	 */
	public Integer getMovementType() {
		return movementType;
	}

	/**
	 * Sets the movement type.
	 *
	 * @param movementType the new movement type
	 */
	public void setMovementType(Integer movementType) {
		this.movementType = movementType;
	}

	/**
	 * Gets the movement description.
	 *
	 * @return the movement description
	 */
	public String getMovementDescription() {
		return movementDescription;
	}

	/**
	 * Sets the movement description.
	 *
	 * @param movementDescription the new movement description
	 */
	public void setMovementDescription(String movementDescription) {
		this.movementDescription = movementDescription;
	}

	/**
	 * Gets the amount operation.
	 *
	 * @return the amount operation
	 */
	public BigDecimal getAmountOperation() {
		return amountOperation;
	}

	/**
	 * Sets the amount operation.
	 *
	 * @param amountOperation the new amount operation
	 */
	public void setAmountOperation(BigDecimal amountOperation) {
		this.amountOperation = amountOperation;
	}

	/**
	 * Gets the nominal value.
	 *
	 * @return the nominal value
	 */
	public BigDecimal getNominalValue() {
		return nominalValue;
	}

	/**
	 * Sets the nominal value.
	 *
	 * @param nominalValue the new nominal value
	 */
	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}

	/**
	 * Gets the security quantity.
	 *
	 * @return the security quantity
	 */
	public BigDecimal getSecurityQuantity() {
		return securityQuantity;
	}

	/**
	 * Sets the security quantity.
	 *
	 * @param securityQuantity the new security quantity
	 */
	public void setSecurityQuantity(BigDecimal securityQuantity) {
		this.securityQuantity = securityQuantity;
	}

	/**
	 * Gets the id issuance pk.
	 *
	 * @return the id issuance pk
	 */
	public String getIdIssuancePk() {
		return idIssuancePk;
	}

	/**
	 * Sets the id issuance pk.
	 *
	 * @param idIssuancePk the new id issuance pk
	 */
	public void setIdIssuancePk(String idIssuancePk) {
		this.idIssuancePk = idIssuancePk;
	}

	/**
	 * Gets the id issuer pk.
	 *
	 * @return the id issuer pk
	 */
	public String getIdIssuerPk() {
		return idIssuerPk;
	}

	/**
	 * Sets the id issuer pk.
	 *
	 * @param idIssuerPk the new id issuer pk
	 */
	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}
}