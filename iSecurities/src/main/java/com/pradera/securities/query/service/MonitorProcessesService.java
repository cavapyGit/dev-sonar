package com.pradera.securities.query.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.process.ProcessMonitorDetail;
import com.pradera.model.process.ProcessMonitorLog;
		


@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class MonitorProcessesService extends CrudDaoServiceBean{
	@EJB
	ParameterServiceBean parameterServiceBean;
	@EJB
	GeneralParametersFacade generalParametersFacade;
	public Map<String, String> loadProcessParameters(Long idBusinessProcessPk) throws ServiceException {
			
			Map<String, String> parameters = new HashMap<String, String>();
			
			ParameterTableTO filter = new ParameterTableTO();
	        filter.setLongInteger(idBusinessProcessPk);
	        List<ParameterTable> params = parameterServiceBean.getListParameterTableServiceBean(filter);
	        //parameter values reside in indicator4
	        if(params.size() > 0){
	        	for (ParameterTable parameterTable : params) {
					if(Validations.validateIsNotNull(parameterTable.getIndicator4())){
						parameters.put(parameterTable.getParameterName(), parameterTable.getIndicator4().toString());
					}
				}
	        }
	        return parameters;
		}
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void saveLogProcessMonitor(ProcessMonitorLog log){
		em.persist(log);
	}
	public void updateLog(ProcessMonitorLog processMonitorLog){
		em.merge(processMonitorLog);
	}
	
	public ProcessMonitorDetail getProcessMonitorDetail(Long idBusinessProcessFk){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select pmd from ProcessMonitorDetail pmd ");
		sbQuery.append(" where pmd.idBusinessProcessFk = :idBusinessProcessFk ");
		sbQuery.append(" and pmd.state = 1 ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idBusinessProcessFk",idBusinessProcessFk);
		ProcessMonitorDetail monitorDetail=null;
		try {
			monitorDetail = (ProcessMonitorDetail) query.getSingleResult();
		} catch (Exception e) {
			System.out.println("No se puede encontrar ProcessMonitorDetail de idBusinessProcessFk : "+idBusinessProcessFk);
		}
		return monitorDetail;
	}
// Borrar este metodo despues
//	public ProcessMonitorDetail getProcessMonitorDetail(Long idBusinessProcessFk){
//		StringBuilder sbQuery = new StringBuilder();
//		sbQuery.append(" select ID_PROCESS_MONITOR_DETAIL_PK ,");
//		sbQuery.append(" ID_BUSINESS_PROCESS_FK,   PROCESS_NAME, ");
//		sbQuery.append("  QUERY_SQL,   STATE ");
//		sbQuery.append(" from PROCESS_MONITOR_DETAIL where ID_BUSINESS_PROCESS_FK="+idBusinessProcessFk+" and STATE=1 ");
//		Query query = em.createNativeQuery(sbQuery.toString());
//		ProcessMonitorDetail monitorDetail=null;
//		List<Object[]> objectData = null;
//		try{
//			objectData = (List<Object[]>)query.getResultList();
//			for (Object[] obj : objectData) {
//				monitorDetail= new ProcessMonitorDetail();
//				monitorDetail.setIdProcessMonitorDetailPk(new BigDecimal(obj[0].toString()).longValue());
//				monitorDetail.setIdBusinessProcessFk(new BigDecimal(obj[1].toString()).longValue());
//				monitorDetail.setProcessName(obj[2].toString());
//				monitorDetail.setQuerySql(obj[3].toString());
//				monitorDetail.setState(new BigDecimal(obj[4].toString()).shortValue());
//			}
//			return monitorDetail;
//			
//		} catch (Exception e) {
//			System.out.println("Error al buscar el ProcessMonitorDetail de idBusinessProcessFk: "+idBusinessProcessFk);
//		}
//		return monitorDetail;
//	}
	
	
	/**
	 * Metodo que obtiene el ultimo registro de los logs
	 * @param ProcessMonitorDetail processMonitorDetail,Date dateSearch
	 * @return ProcessMonitorLog
	 * {@link ProcessMonitorLog}
	 * */
	@SuppressWarnings("unchecked")
	public ProcessMonitorLog getProcessMonitorLog(Long idProcessMonitorDetailPk ,Date dateSearch){	
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select ID_PROCESS_MONITOR_LOG_PK,");//0
		stringBuilder.append(" ID_PROCESS_MONITOR_DETAIL_FK,");  //1
		stringBuilder.append(" PROCESS_STATUS,");                //2
		stringBuilder.append(" START_DATE,END_DATE");            //3,4
		stringBuilder.append(" from PROCESS_MONITOR_LOG ");
		stringBuilder.append(" where 1 = 1 ");
		stringBuilder.append(" and ID_PROCESS_MONITOR_DETAIL_FK = :idProcessMonitorDetailPk ");
		stringBuilder.append(" and trunc(START_DATE) = TRUNC(:dateSearch)");
		stringBuilder.append(" order by ID_PROCESS_MONITOR_LOG_PK desc");
		
		Query query = em.createNativeQuery(stringBuilder.toString());				
		query.setParameter("idProcessMonitorDetailPk", idProcessMonitorDetailPk);
		query.setParameter("dateSearch", dateSearch);
		
		List<Object[]> objectData = null;
		try{
			objectData = (List<Object[]>)query.getResultList();
			ProcessMonitorLog log = new ProcessMonitorLog();
			Date date ;
			for (Object[] object : objectData) {
				log.setIdProcessMonitorLogPk(new BigDecimal(object[0].toString()).longValue());
				log.setIdProcessMonitorDetailFk(new BigDecimal(object[1].toString()).longValue());
				log.setProcessStatus(new BigDecimal(object[2].toString()).longValue());
				//date = CommonsUtilities.convertStringtoDate(object[3].toString(),CommonsUtilities.DATE_TIMESTAMP_PATTERN); ;
				date = CommonsUtilities.convertStringtoDate(object[3].toString(),"yyyy-MM-dd HH:mm:ss"); ;
				log.setStartDate(date);
				if(object[4]!=null){
					date = CommonsUtilities.convertStringtoDate(object[4].toString(),"yyyy-MM-dd HH:mm:ss");
					log.setEndDate(date);
				}
				break;
			}
			return log;
		}catch(NoResultException ex){
			return null;
		}
	}
	/**
	 * Obtiene la descripcion de un parameter table
	 * @author hcoarite
	 * @param  idParameterPk
	 * 
	 * */
	public String getDescriptionParameterPk(Long idParameterPk){
		ParameterTable description=null;
		description=em.find(ParameterTable.class, idParameterPk.intValue());
		if(description.getDescription()!=null)
			return description.getDescription();
		if(description.getText1()!=null)
			return description.getText1();
		return null;
	}
	/**
	 * Metodo que registra un ProcessMonitorLog de acuerdo a la consulta ejecutada.
	 * Ademas de que la consulta debe recibir un parametro, esta es la feha de busqueda 
	 * La consulta retorna 0, si el proceso se termino correctamente
	 * @author hcoarite
	 * @param  ProcessMonitorDetail.class
	 * @return ProcessMonitorLog.class
	 */
	
	/**
	 * Metodo que obtiene el modulo al cual pertence un batchero
	 * @param Long idBusinessProcessPk
	 * */
	public String getNameModuleBusinessProcess(Long idBusinessProcessPk){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select moduleName from BusinessProcess where idBusinessProcessPk = :idBusinessProcessPk");
		Query query=em.createQuery(sbQuery.toString());
		query.setParameter("idBusinessProcessPk",idBusinessProcessPk);
		String processName=null;
		try {
			processName =  (String) query.getSingleResult();
		} catch (Exception e) {
			System.out.println("NO SE PUEDE ENCONTRAR EL BusinessProcess : "+idBusinessProcessPk);
		}
		return processName;
	}
	public String getBeanNameBusinessProcess(Long idBusinessProcessPk){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery = new StringBuilder();
		sbQuery.append("select beanName from BusinessProcess where idBusinessProcessPk = :idBusinessProcessPk");
		Query query=em.createQuery(sbQuery.toString());
		query.setParameter("idBusinessProcessPk",idBusinessProcessPk);
		String processName =  (String) query.getSingleResult();
		return processName;
	}
	public boolean isHoliday(Date fecha){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select ID_HOLIDAY_PK from SECURITY.HOLIDAY where DATE_HOLIDAY =:fecha");
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("fecha",fecha);
        List<BigDecimal> list= query.getResultList();
        if(list.size()==0)
        	return false;
        else 
        	return true;
	}
	/**
	 * Meotodo que ejecuta la query de MonitorProcessDetail
	 * Si retorna 0 : el proceso se ejecuto correctamente
	 * Si retorna >0 : no concluyo aun
	 * */
	public int getResultForQuery(String querySql,Date dateSearch){
		Query query=em.createNativeQuery(querySql);
		query.setParameter("dateSearch",dateSearch);
		BigDecimal result =null;
		try {
			result=(BigDecimal) query.getSingleResult();
		} catch (Exception e) {
			System.out.println("Error al ejecutar la consulta : "+querySql);
			return -1;
		}
		return  result.intValue();
	}
}
