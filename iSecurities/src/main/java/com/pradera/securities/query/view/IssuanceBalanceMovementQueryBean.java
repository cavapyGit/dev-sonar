package com.pradera.securities.query.view;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.helperui.view.IssuanceHelperBean;
import com.pradera.core.component.helperui.view.IssuerHelperBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.securities.issuancesecuritie.facade.IssuanceSecuritiesServiceFacade;
import com.pradera.securities.issuer.facade.IssuerServiceFacade;
import com.pradera.securities.issuer.to.IssuerTO;
import com.pradera.securities.query.facade.ValorizationBalanceFacade;
import com.pradera.securities.query.to.IssuanceBalanceMovDetailResultTO;
import com.pradera.securities.query.to.IssuanceBalanceMovResultTO;
import com.pradera.securities.query.to.IssuanceBalanceMovTO;

// TODO: Auto-generated Javadoc
/**
 * The Class Valuated Holding Balances
 * Consulta de Saldos y movimientos de la EMision.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 28/05/2013
 */
@DepositaryWebBean
public class IssuanceBalanceMovementQueryBean extends GenericBaseBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The holder account request. */
	private IssuanceBalanceMovTO issuanceBalanceMovTO;
	
	/** The issuer request. */
	private Issuer issuerRequest;
	
	/** The issuance request. */
	private Issuance issuanceRequest;
	
	/** The issuance bal mov session. */
	private IssuanceBalanceMovResultTO issuanceBalMovSession;
	
	private List<IssuanceBalanceMovResultTO> issuanceBalMovSessionList;
	
	/** The issuance bal mov res detail. */
	private GenericDataModel<IssuanceBalanceMovDetailResultTO> issuanceBalMovResDetail ;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The issuance securitie query service facade. */
	@EJB
	ValorizationBalanceFacade issuanceSecuritieQueryServiceFacade;
	
	/** The issuance securities service facade. */
	@EJB
	IssuanceSecuritiesServiceFacade issuanceSecuritiesServiceFacade; 
	
	/** The issuer service facade. */
	@EJB
	IssuerServiceFacade issuerServiceFacade;
	
	/** The issuer helper bean. */
	@Inject
	IssuerHelperBean issuerHelperBean;
	
	/** The issuance helper bean. */
	@Inject
	IssuanceHelperBean issuanceHelperBean;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		resetIssuanceBalTO();
		this.issuanceRequest = new Issuance();
		if (userInfo.getUserAccountSession().isIssuerInstitucion() && 
				Validations.validateIsNotNull(userInfo.getUserAccountSession().getIssuerCode())) {
			Issuer filter= new Issuer();
			filter.setIdIssuerPk(userInfo.getUserAccountSession().getIssuerCode());
			try {
				this.issuerRequest= issuerServiceFacade.findIssuerByFiltersServiceFacade(filter);
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		} else {
			this.issuerRequest = new Issuer();
		}
		
	}
	
	/**
	 * Search issuer handler.
	 */
	public void searchIssuerHandler(){
		if(this.issuanceBalanceMovTO.getIdIssuerPk()==null || this.issuanceBalanceMovTO.getIdIssuerPk().isEmpty()){
			this.issuerRequest = null;
			return;
		}
		IssuerTO issuerTO = new IssuerTO();
		issuerTO.setIdIssuerPk(this.issuanceBalanceMovTO.getIdIssuerPk());
		try {
			this.issuerRequest = issuerServiceFacade.findIssuerServiceFacade(issuerTO);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		cleanDataIssuerHandler();
	}
	
	/**
	 * Search issuance handler.
	 */
	public void searchIssuanceHandler(){
		if(this.issuanceRequest==null || this.issuanceRequest.getIdIssuanceCodePk()==null || this.issuanceRequest.getIdIssuanceCodePk().isEmpty()){
			this.issuanceRequest = new Issuance();
		}
		cleanDataIssuanceHandler();
	}
	
	/**
	 * Search issuance balance movem handler.
	 */
	public void searchIssuanceBalanceMovemHandler(){
		IssuanceBalanceMovTO issuanceBalanceMovTO = new IssuanceBalanceMovTO();
		issuanceBalanceMovTO.setIdIssuerPk(this.issuerRequest.getIdIssuerPk());
		issuanceBalanceMovTO.setIdIssuancePk(this.issuanceRequest.getIdIssuanceCodePk());
		issuanceBalanceMovTO.setInitialDate(this.issuanceBalanceMovTO.getInitialDate());
		issuanceBalanceMovTO.setFinalDate(this.issuanceBalanceMovTO.getFinalDate());
		issuanceBalanceMovTO.setIssuanceType(this.issuanceRequest.getIssuanceType());
		
		List<IssuanceBalanceMovDetailResultTO> issuanceBalMovListTmp = null;
		try { 
			this.issuanceBalMovSessionList = issuanceSecuritiesServiceFacade.getIssuanceBalanceMovement(issuanceBalanceMovTO);
			if(this.issuanceBalMovSessionList!=null && !issuanceBalMovSessionList.isEmpty()){
				issuanceBalMovListTmp = issuanceSecuritiesServiceFacade.getIssuanceBalanceMovDet(issuanceBalanceMovTO, issuanceBalMovSessionList);
				this.issuanceBalMovResDetail = new GenericDataModel<IssuanceBalanceMovDetailResultTO>(issuanceBalMovListTmp);
			}else{
				this.issuanceBalMovResDetail = new GenericDataModel<IssuanceBalanceMovDetailResultTO>();
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Clean data issuance handler.
	 */
	public void cleanDataIssuanceHandler(){
		cleanDataFormHandler();
	//	issuanceHelperBean.setLstIssuances(null);
		resetComponent();
	}
	
	/**
	 * Clean data issuer handler.
	 */
	public void cleanDataIssuerHandler(){
		//here is necesary validate when the usser is Issuer Or Cevaldom
		this.issuanceRequest = new Issuance();
		this.issuanceBalanceMovTO.setIdIssuancePk(null);
		if(!userInfo.getUserAccountSession().isIssuerInstitucion()){
			issuerRequest = new Issuer();
		}		
	//	issuanceHelperBean.setLstIssuances(null);
		cleanDataFormHandler();
		resetComponent();
	}
	
	public void cleanIssuerHandler() {
		this.issuanceRequest = new Issuance();
		this.issuanceBalanceMovTO.setIdIssuancePk(null);
		cleanDataFormHandler();
		resetComponent();
	}
	
	/**
	 * Clean data form handler.
	 */
	public void cleanAllDataFormHandler(){
		resetIssuanceBalTO();
		cleanDataIssuerHandler();
	//	issuerHelperBean.setIssuerDescription(null);
	//	issuerHelperBean.setIssuerMnemonic(null);
		resetComponent();
	}
	
	/**
	 * Reset component.
	 */
	public void resetComponent(){
		JSFUtilities.resetComponent("frmGeneral:flsSearch");
	}
	
	/**
	 * Clean data form.
	 */
	public void cleanDataFormHandler(){
		this.issuanceBalMovSession = null;
		this.issuanceBalMovResDetail = null;
	}
	
	/**
	 * Reset issuance bal to.
	 */
	public void resetIssuanceBalTO(){
		this.issuanceBalanceMovTO = new IssuanceBalanceMovTO();
		this.issuanceBalanceMovTO.setInitialDate(CommonsUtilities.currentDate());
		this.issuanceBalanceMovTO.setFinalDate(CommonsUtilities.currentDate());
	}

	/**
	 * Gets the issuance balance mov to.
	 *
	 * @return the issuance balance mov to
	 */
	public IssuanceBalanceMovTO getIssuanceBalanceMovTO() {
		return issuanceBalanceMovTO;
	}
	
	/**
	 * Sets the issuance balance mov to.
	 *
	 * @param issuanceBalanceMovTO the new issuance balance mov to
	 */
	public void setIssuanceBalanceMovTO(IssuanceBalanceMovTO issuanceBalanceMovTO) {
		this.issuanceBalanceMovTO = issuanceBalanceMovTO;
	}

	/**
	 * Gets the issuer request.
	 *
	 * @return the issuer request
	 */
	public Issuer getIssuerRequest() {
		return issuerRequest;
	}

	/**
	 * Sets the issuer request.
	 *
	 * @param issuerRequest the new issuer request
	 */
	public void setIssuerRequest(Issuer issuerRequest) {
		this.issuerRequest = issuerRequest;
	}

	/**
	 * Gets the issuance request.
	 *
	 * @return the issuance request
	 */
	public Issuance getIssuanceRequest() {
		return issuanceRequest;
	}

	/**
	 * Sets the issuance request.
	 *
	 * @param issuanceRequest the new issuance request
	 */
	public void setIssuanceRequest(Issuance issuanceRequest) {
		this.issuanceRequest = issuanceRequest;
	}

	/**
	 * Gets the issuance bal mov session.
	 *
	 * @return the issuance bal mov session
	 */
	public IssuanceBalanceMovResultTO getIssuanceBalMovSession() {
		return issuanceBalMovSession;
	}

	/**
	 * Sets the issuance bal mov session.
	 *
	 * @param issuanceBalMovSession the new issuance bal mov session
	 */
	public void setIssuanceBalMovSession(
			IssuanceBalanceMovResultTO issuanceBalMovSession) {
		this.issuanceBalMovSession = issuanceBalMovSession;
	}

	/**
	 * Gets the issuance bal mov res detail.
	 *
	 * @return the issuance bal mov res detail
	 */
	public GenericDataModel<IssuanceBalanceMovDetailResultTO> getIssuanceBalMovResDetail() {
		return issuanceBalMovResDetail;
	}

	/**
	 * Sets the issuance bal mov res detail.
	 *
	 * @param issuanceBalMovResDetail the new issuance bal mov res detail
	 */
	public void setIssuanceBalMovResDetail(
			GenericDataModel<IssuanceBalanceMovDetailResultTO> issuanceBalMovResDetail) {
		this.issuanceBalMovResDetail = issuanceBalMovResDetail;
	}

	public List<IssuanceBalanceMovResultTO> getIssuanceBalMovSessionList() {
		return issuanceBalMovSessionList;
	}

	public void setIssuanceBalMovSessionList(List<IssuanceBalanceMovResultTO> issuanceBalMovSessionList) {
		this.issuanceBalMovSessionList = issuanceBalMovSessionList;
	}

}