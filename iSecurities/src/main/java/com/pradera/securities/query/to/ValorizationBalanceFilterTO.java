package com.pradera.securities.query.to;

import java.io.Serializable;
import java.util.Date;

import com.pradera.model.accounts.Holder;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class ValueHoldBalance.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 28/05/2013
 */
public class ValorizationBalanceFilterTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The holder request. */
	private Holder holderRequest;
	
	/** The issuer request. */
	private Issuer issuerRequest;
	
	/** The id holder account pk. */
	private Long idHolderAccountPk;
	
	/** The id issuer code. */
	private String idIssuerCode;
	
	/** The valorization type. */
	private Integer valorizationType;
	
	/** The security. */
	private Security security;
	
	/** The date courte. */
	private Date dateCourte;

	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * Gets the id holder account pk.
	 *
	 * @return the id holder account pk
	 */
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}

	/**
	 * Sets the id holder account pk.
	 *
	 * @param idHolderAccountPk the new id holder account pk
	 */
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}

	/**
	 * Gets the id issuer code.
	 *
	 * @return the id issuer code
	 */
	public String getIdIssuerCode() {
		return idIssuerCode;
	}

	/**
	 * Sets the id issuer code.
	 *
	 * @param idIssuerCode the new id issuer code
	 */
	public void setIdIssuerCode(String idIssuerCode) {
		this.idIssuerCode = idIssuerCode;
	}


	/**
	 * Gets the valorization type.
	 *
	 * @return the valorization type
	 */
	public Integer getValorizationType() {
		return valorizationType;
	}

	/**
	 * Sets the valorization type.
	 *
	 * @param valorizationType the new valorization type
	 */
	public void setValorizationType(Integer valorizationType) {
		this.valorizationType = valorizationType;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets the date courte.
	 *
	 * @return the date courte
	 */
	public Date getDateCourte() {
		return dateCourte;
	}

	/**
	 * Sets the date courte.
	 *
	 * @param dateCourte the new date courte
	 */
	public void setDateCourte(Date dateCourte) {
		this.dateCourte = dateCourte;
	}

	/**
	 * Gets the holder request.
	 *
	 * @return the holder request
	 */
	public Holder getHolderRequest() {
		return holderRequest;
	}

	/**
	 * Sets the holder request.
	 *
	 * @param holderRequest the new holder request
	 */
	public void setHolderRequest(Holder holderRequest) {
		this.holderRequest = holderRequest;
	}

	/**
	 * Gets the issuer request.
	 *
	 * @return the issuer request
	 */
	public Issuer getIssuerRequest() {
		return issuerRequest;
	}

	/**
	 * Sets the issuer request.
	 *
	 * @param issuerRequest the new issuer request
	 */
	public void setIssuerRequest(Issuer issuerRequest) {
		this.issuerRequest = issuerRequest;
	}
    

	
	
}