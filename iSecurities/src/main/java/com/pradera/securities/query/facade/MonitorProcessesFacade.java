package com.pradera.securities.query.facade;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.processes.scheduler.Schedulebatch;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessMonitorDetail;
import com.pradera.model.process.ProcessMonitorLog;
import com.pradera.securities.query.service.MonitorProcessesService;
import com.pradera.securities.query.to.BusinessProcessTO;

	
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class MonitorProcessesFacade {
	
	/** The batch process service facade. */
	@EJB 
	private BatchProcessServiceFacade batchProcessServiceFacade;
	
	@Resource 
	TransactionSynchronizationRegistry transactionRegistry;
	
	@EJB
	MonitorProcessesService monitorService;
	@Inject 
    ClientRestService clientRestService;
	
	/**
	 * Search last begin end day.
	 *
	 * @param date the date
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	/** The user session. */ 
	@Inject 
	private UserInfo userInfo;
	public ProcessMonitorDetail getProcessMonitorDetail(Long idBusinessProcessFk){
		return monitorService.getProcessMonitorDetail(idBusinessProcessFk);
	}
	/**
	 * Metodo que busca el ultimo log del monitor de procesos y actualiza el estado del log
	 * @param ProcessMonitorDetail processMonitorDetail,Date dateSearch
	 * @return ProcessMonitorLog
	 * */
	@ProcessAuditLogger
	public ProcessMonitorLog getProcessMonitorLog(ProcessMonitorDetail processMonitorDetail,Date dateSearch){
		if(processMonitorDetail==null){
			System.out.println("Error; No se puede buscar LOG,s por que el objeto ProcessMonitorDetail es null ");
			return null;
		}
			
		ProcessMonitorLog log= null;
		log= monitorService.getProcessMonitorLog(processMonitorDetail.getIdProcessMonitorDetailPk(),dateSearch);
		if(log.getIdProcessMonitorLogPk()==null)
			return null;
		if(processMonitorDetail.getQuerySql()==null){
			log.setProgressDesc("DESCONOCIDO");
			return log;
		}		
		int count = monitorService.getResultForQuery(processMonitorDetail.getQuerySql(),dateSearch);
		/*Si se retorna 0, se termino el proceso; Esto es para todos los casos*/
		if(count==0){
			if(log.getProcessStatus().equals(1366L)){
				log.setProgressDesc(monitorService.getDescriptionParameterPk(log.getProcessStatus()));
				return log;
			}
			log.setProcessStatus(1366L);//FINALIZADO
			log.setEndDate( new Date());
			log.setProgressDesc(monitorService.getDescriptionParameterPk(log.getProcessStatus()));
		}
		else{
			if(log.getProcessStatus().equals(1365L)){
				log.setProgressDesc(monitorService.getDescriptionParameterPk(log.getProcessStatus()));
				return log;
			}
				
			log.setProcessStatus(1365L);//Procesando
			log.setProgressDesc(monitorService.getDescriptionParameterPk(log.getProcessStatus()));
		}
		monitorService.updateLog(log);
		return log;
	}
	@ProcessAuditLogger
	public ProcessMonitorLog executeRemoteBatchProcesses(ProcessMonitorDetail monitorDetail,BusinessProcessTO processes, Date dateSearch) {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeExecute());
        
			BusinessProcessTO process = processes;
			Schedulebatch scheduleBatch = new Schedulebatch();
	        scheduleBatch.setIdBusinessProcess(process.getIdBusinessProcessPk());
	        scheduleBatch.setUserName(loggerUser.getUserName());
	        scheduleBatch.setPrivilegeCode(loggerUser.getIdPrivilegeOfSystem());
	        
	        ProcessMonitorLog log=new ProcessMonitorLog();
			log.setIdProcessMonitorDetailFk(monitorDetail.getIdProcessMonitorDetailPk());
			log.setProcessStatus(1364L);//Registrado
			log.setProgressDesc(monitorService.getDescriptionParameterPk(log.getProcessStatus()));
			log.setStartDate(CommonsUtilities.currentDateTime());
			
	        try {
				scheduleBatch.setParameters(monitorService.loadProcessParameters(process.getIdBusinessProcessPk()));
			} catch (ServiceException e1) {
				e1.printStackTrace();
			}
	        scheduleBatch.getParameters().put(GeneralConstants.PARAMETER_DATE,
	        		CommonsUtilities.convertDatetoString(dateSearch));
			try {
				log.setProcessStatus(1365L);//Procesando
				log.setProgressDesc(monitorService.getDescriptionParameterPk(log.getProcessStatus()));
				clientRestService.excuteRemoteBatchProcess(scheduleBatch,process.getModuleName());
			} catch (Exception e) {
				e.printStackTrace();
				log.setProcessStatus(1368L);//Error
				log.setProgressDesc(monitorService.getDescriptionParameterPk(log.getProcessStatus()));
			}
			monitorService.saveLogProcessMonitor(log);
			return log;
	}
	/**
	* Metodo que ejecuta el proceso batch y registra un nuevo ProcessMonitorLog
	*/
	public ProcessMonitorLog startRemoteBatchMonitor(ProcessMonitorDetail monitorDetail, Date dateSerach){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeExecute());
        Schedulebatch scheduleBatch = new Schedulebatch();
        
        scheduleBatch.setIdBusinessProcess(monitorDetail.getIdBusinessProcessFk());
        scheduleBatch.setUserName(loggerUser.getUserName());
        scheduleBatch.setPrivilegeCode(loggerUser.getIdPrivilegeOfSystem());
        
        ProcessMonitorLog log=new ProcessMonitorLog();
		log.setIdProcessMonitorDetailFk(monitorDetail.getIdProcessMonitorDetailPk());
		log.setProcessStatus(1364L);//Registrado
		log.setProgressDesc(monitorService.getDescriptionParameterPk(log.getProcessStatus()));
		log.setStartDate(new Date());
		
        try {
			scheduleBatch.setParameters(monitorService.loadProcessParameters(monitorDetail.getIdBusinessProcessFk()));
			String processName=monitorService.getNameModuleBusinessProcess(monitorDetail.getIdBusinessProcessFk());
			if (processName==null) {
				log.setProcessStatus(1368L);//ERROR
				log.setProgressDesc(monitorService.getDescriptionParameterPk(log.getProcessStatus()));
			}
			else{
				log.setProcessStatus(1365L);//Procesando
				log.setProgressDesc(monitorService.getDescriptionParameterPk(log.getProcessStatus()));
			}
			scheduleBatch.getParameters().put(GeneralConstants.PARAMETER_DATE,CommonsUtilities.convertDatetoString(dateSerach));
			clientRestService.excuteRemoteBatchProcess(scheduleBatch, monitorService.getNameModuleBusinessProcess(scheduleBatch.getIdBusinessProcess()));
		} catch (ServiceException e) {
			e.printStackTrace();
			log.setProcessStatus(1368L);//ERROR
			log.setProgressDesc(monitorService.getDescriptionParameterPk(log.getProcessStatus()));
		}
        monitorService.saveLogProcessMonitor(log);
        return log;
	}
	public ProcessMonitorLog startRemoteBatchValuator(ProcessMonitorDetail monitorDetail, Date dateSerach){
		ProcessMonitorLog log=new ProcessMonitorLog();
		log.setIdProcessMonitorDetailFk(monitorDetail.getIdProcessMonitorDetailPk());
		log.setProcessStatus(1364L);//Registrado
		log.setProgressDesc(monitorService.getDescriptionParameterPk(log.getProcessStatus()));
		log.setStartDate(new Date());
		
        try {
        	Map<String, Object> details = new HashMap<String, Object>();
//    		details.put(ValuatorConstants.PARAM_VALUATOR_CURRENCY, CurrencyType.PYG.getCode());
//    		details.put(ValuatorConstants.PARAM_VALUATOR_DATE, CommonsUtilities.convertDatetoStringForTemplate(dateSerach,CommonsUtilities.DATETIME_PATTERN));
//    		details.put(ValuatorConstants.PARAM_USER_EMAIL, userInfo.getUserAccountSession().getEmail());
//    		details.put(ValuatorConstants.PARAM_USER_NAME, userInfo.getUserAccountSession().getUserName());
//    		details.put(ValuatorConstants.PARAM_VALUATOR_EXECUTION_TYPE, ValuatorExecutionType.MANUAL.getCode());
    		String participantPks="114,123,118,122,102,115,119,120,132,121,116,117,101,107,137,124,133,135,136,128,129,134,130,112,106,127,105,104,113,141,100,108,131,103";
//    		details.put(ValuatorConstants.PARAM_PARTICIPANT_CODE, participantPks);
			String processName=monitorService.getNameModuleBusinessProcess(monitorDetail.getIdBusinessProcessFk());
			this.registerBatch(details,monitorDetail);
			log.setProcessStatus(1365L);//Procesando
			log.setProgressDesc(monitorService.getDescriptionParameterPk(log.getProcessStatus()));
		} catch (ServiceException e) {
			e.printStackTrace();
			log.setProcessStatus(1368L);//ERROR
			log.setProgressDesc(monitorService.getDescriptionParameterPk(log.getProcessStatus()));
		}
        monitorService.saveLogProcessMonitor(log);
        return log;
	}
	@Lock(LockType.READ)
	private void registerBatch(Map<String,Object> details,ProcessMonitorDetail monitorDetail) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser.getUserAction().getIdPrivilegeExecute()!=null){
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeExecute());
		}else{
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		}
		ProcessMonitorLog log=new ProcessMonitorLog();
		log.setIdProcessMonitorDetailFk(monitorDetail.getIdProcessMonitorDetailPk());
		log.setProcessStatus(1364L);//Registrado
		log.setStartDate(CommonsUtilities.currentDateTime());
		
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.VALUATOR_EXECUTION_CORE_PROCESS.getCode());
		try {
			log.setProcessStatus(1365L);//Procesando
			log.setProgressDesc(monitorService.getDescriptionParameterPk(log.getProcessStatus()));
			batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(),businessProcess,details);
		} catch (ServiceException e) {
			log.setProcessStatus(1368L);//ERROR
			log.setProgressDesc(monitorService.getDescriptionParameterPk(log.getProcessStatus()));
			e.printStackTrace();
		}
		monitorService.saveLogProcessMonitor(log);
	}
	
	public String getNameModuleBusinessProcess(Long idBusinessProcessPk){
		return monitorService.getNameModuleBusinessProcess(idBusinessProcessPk);
	}
	public String getBeanNameBusinessProcess(Long idBusinessProcessPk){
		return monitorService.getBeanNameBusinessProcess(idBusinessProcessPk);
	}
	public boolean isHoliday(Date date){
		return monitorService.isHoliday(date);
	}
}
