package com.pradera.securities.query.to;

import java.io.Serializable;
import java.util.Date;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;

// TODO: Auto-generated Javadoc
/**
 * The Class SecurityBalanceMovTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class SecurityBalanceMovTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2576916447236721089L;

	/** The issuer. */
	private Issuer issuer;
	
	/** The issuance. */
	private Issuance issuance;
	
	/** The security. */
	private Security security;
	
	/** The issuance date. */
	private Date issuanceDate;
	
	/** The final date. */
	private Date finalDate;
	
	/**
	 * Instantiates a new security balance mov to.
	 */
	public SecurityBalanceMovTO() {
		issuer = new Issuer();
		issuance = new Issuance();
		security = new Security();
		finalDate = CommonsUtilities.currentDate();
	}

	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}

	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	/**
	 * Gets the issuance.
	 *
	 * @return the issuance
	 */
	public Issuance getIssuance() {
		return issuance;
	}

	/**
	 * Sets the issuance.
	 *
	 * @param issuance the new issuance
	 */
	public void setIssuance(Issuance issuance) {
		this.issuance = issuance;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets the issuance date.
	 *
	 * @return the issuance date
	 */
	public Date getIssuanceDate() {
		return issuanceDate;
	}

	/**
	 * Sets the issuance date.
	 *
	 * @param issuanceDate the new issuance date
	 */
	public void setIssuanceDate(Date issuanceDate) {
		this.issuanceDate = issuanceDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}		

}
