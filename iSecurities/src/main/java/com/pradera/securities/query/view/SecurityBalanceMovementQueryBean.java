package com.pradera.securities.query.view;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.IssuerSearcherTO;
import com.pradera.core.component.helperui.to.IssuerTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.securities.issuancesecuritie.facade.SecurityServiceFacade;
import com.pradera.securities.issuancesecuritie.util.PropertiesConstants;
import com.pradera.securities.query.to.SecurityBalanceMovTO;
import com.pradera.securities.query.to.SecurityMovementsResultTO;


// TODO: Auto-generated Javadoc
/**
 * The Class SecurityBalanceMovementQueryBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@DepositaryWebBean
public class SecurityBalanceMovementQueryBean extends GenericBaseBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4413636020904315445L;
	
	/** The security balance mov to. */
	private SecurityBalanceMovTO securityBalanceMovTO;
	
	/** The lst movements result. */
	private List<SecurityMovementsResultTO> lstMovementsResult;

	/** The security service facade. */
	@EJB
	SecurityServiceFacade securityServiceFacade;
	
	/** The is issuer. */
	private boolean isIssuer;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The helper component facade. */
	@EJB
	private HelperComponentFacade 		  helperComponentFacade;

	/**
	 * Instantiates a new security balance movement query bean.
	 */
	public SecurityBalanceMovementQueryBean() {
		securityBalanceMovTO = new SecurityBalanceMovTO();
	}
	
	/**
	 * Inits the.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException{
		if(validateIsIssuer()){
			searchIssuerByCode();
	    }
	}
	
	/**
	 * Search issuer by code.
	 *
	 * @throws ServiceException the service exception
	 */
	public void searchIssuerByCode() throws ServiceException{
		if(Validations.validateIsNotNullAndNotEmpty(securityBalanceMovTO.getIssuer())
				&& Validations.validateIsNotNullAndNotEmpty(securityBalanceMovTO.getIssuer().getIdIssuerPk())){
			IssuerSearcherTO issuerSearcher = new IssuerSearcherTO();
			issuerSearcher.setIssuerCode(securityBalanceMovTO.getIssuer().getIdIssuerPk());
			IssuerTO issuerTO = helperComponentFacade.findIssuerByCode(issuerSearcher);
			Issuer issuer = new Issuer();
			issuer.setIdIssuerPk(issuerTO.getIssuerCode());
			issuer.setMnemonic(issuerTO.getMnemonic());
			securityBalanceMovTO.setIssuer(issuer);
		}
	}
	
	/**
	 * Validate is issuer.
	 *
	 * @return true, if successful
	 */
	public boolean validateIsIssuer(){
		isIssuer = false;
		Issuer issuerTO = new Issuer();
		if(userInfo.getUserAccountSession().isIssuerInstitucion() && 
				Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getIssuerCode())){
			isIssuer = true;			
			issuerTO.setIdIssuerPk(userInfo.getUserAccountSession().getIssuerCode());
			securityBalanceMovTO.setIssuer(issuerTO);
		}		
		return isIssuer;
	}
	
	/**
	 * Search issuer handler.
	 */
	public void searchIssuerHandler(){
		if(Validations.validateIsNull(securityBalanceMovTO.getIssuer()) || 
				Validations.validateIsNullOrEmpty(securityBalanceMovTO.getIssuer().getIdIssuerPk())){
			securityBalanceMovTO.setIssuer(new Issuer());
		}
		securityBalanceMovTO.setIssuance(new Issuance());
		securityBalanceMovTO.setSecurity(new Security());
		cleanMovements();
	}
	
	/**
	 * Search issuance handler.
	 */
	public void searchIssuanceHandler(){
		if(Validations.validateIsNull(securityBalanceMovTO.getIssuance()) || 
				Validations.validateIsNullOrEmpty(securityBalanceMovTO.getIssuance().getIdIssuanceCodePk())){
			securityBalanceMovTO.setIssuance(new Issuance());
		}
		securityBalanceMovTO.setSecurity(new Security());
		cleanMovements();
	}
	
	/**
	 * Search security handler.
	 */
	public void searchSecurityHandler(){
		if(Validations.validateIsNull(securityBalanceMovTO.getSecurity()) || 
				Validations.validateIsNullOrEmpty(securityBalanceMovTO.getSecurity().getIdSecurityCodePk())){
			securityBalanceMovTO.setSecurity(new Security());
		} else {
			securityBalanceMovTO.setIssuanceDate(securityBalanceMovTO.getSecurity().getIssuanceDate());
			securityBalanceMovTO.setFinalDate(getCurrentSystemDate());
		}
		cleanMovements();
	}
	
	/**
	 * Search security balance movements.
	 */
	public void searchSecurityBalanceMovements(){
		try {
			
			if(securityBalanceMovTO.getSecurity().getIdSecurityCodePk() == null) {
				
				Object[] bodyData = new Object[0];
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getGenericMessage(PropertiesConstants.MESSAGE_DATA_REQUIRED, bodyData));
				JSFUtilities.showSimpleValidationDialog();
				return;
				
			}
			
			lstMovementsResult = securityServiceFacade.listSecurityMovementResult(securityBalanceMovTO);
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clean data.
	 *
	 * @throws ServiceException the service exception
	 */
	public void cleanData() throws ServiceException{
		JSFUtilities.resetViewRoot();
		securityBalanceMovTO = new SecurityBalanceMovTO();
		cleanMovements();
		if(validateIsIssuer()){
			searchIssuerByCode();
	    }
	}
	
	/**
	 * Clean movements.
	 */
	public void cleanMovements(){
		lstMovementsResult = null;
	}

	/**
	 * Gets the security balance mov to.
	 *
	 * @return the security balance mov to
	 */
	public SecurityBalanceMovTO getSecurityBalanceMovTO() {
		return securityBalanceMovTO;
	}

	/**
	 * Sets the security balance mov to.
	 *
	 * @param securityBalanceMovTO the new security balance mov to
	 */
	public void setSecurityBalanceMovTO(SecurityBalanceMovTO securityBalanceMovTO) {
		this.securityBalanceMovTO = securityBalanceMovTO;
	}

	/**
	 * Gets the lst movements result.
	 *
	 * @return the lst movements result
	 */
	public List<SecurityMovementsResultTO> getLstMovementsResult() {
		return lstMovementsResult;
	}

	/**
	 * Sets the lst movements result.
	 *
	 * @param lstMovementsResult the new lst movements result
	 */
	public void setLstMovementsResult(
			List<SecurityMovementsResultTO> lstMovementsResult) {
		this.lstMovementsResult = lstMovementsResult;
	}

	/**
	 * Checks if is issuer.
	 *
	 * @return the isIssuer
	 */
	public boolean isIssuer() {
		return isIssuer;
	}

	/**
	 * Sets the issuer.
	 *
	 * @param isIssuer the isIssuer to set
	 */
	public void setIssuer(boolean isIssuer) {
		this.isIssuer = isIssuer;
	}
			
}
