package com.pradera.securities.query.to;

import java.io.Serializable;
import java.util.Date;

import com.pradera.model.accounts.Holder;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;

// TODO: Auto-generated Javadoc
//TODO: Auto-generated Javadoc
/**
* <ul>
* <li>
* Project Pradera.
* Copyright 2013.</li>
* </ul>
* 
* The Class PhysicalValuedBalancesTO.
*
* @author PraderaTechnologies.
* @version 1.0 , 20/03/2015
*/

public class PhysicalValuedBalancesTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The id currency. */
	private Long idCurrency;
	
	/** The id security class. */
	private Integer idSecurityClass;
	
	/** The id valorization type. */
	private Integer idValorizationType;
	
	/** The date courte. */
	private Date dateCourte;
	
	/** The obj holder. */
	private Holder objHolder;
	
	/** The obj issuer. */
	private Issuer objIssuer;
	
	/** The obj security. */
	private Security objSecurity;

	/**
	 * Gets the id participant pk.
	 *
	 * @return the idParticipantPk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the idParticipantPk to set
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * Gets the obj holder.
	 *
	 * @return the objHolder
	 */
	public Holder getObjHolder() {
		return objHolder;
	}

	/**
	 * Sets the obj holder.
	 *
	 * @param objHolder the objHolder to set
	 */
	public void setObjHolder(Holder objHolder) {
		this.objHolder = objHolder;
	}

	/**
	 * Gets the obj issuer.
	 *
	 * @return the objIssuer
	 */
	public Issuer getObjIssuer() {
		return objIssuer;
	}

	/**
	 * Sets the obj issuer.
	 *
	 * @param objIssuer the objIssuer to set
	 */
	public void setObjIssuer(Issuer objIssuer) {
		this.objIssuer = objIssuer;
	}

	/**
	 * Gets the id currency.
	 *
	 * @return the idCurrency
	 */
	public Long getIdCurrency() {
		return idCurrency;
	}

	/**
	 * Sets the id currency.
	 *
	 * @param idCurrency the idCurrency to set
	 */
	public void setIdCurrency(Long idCurrency) {
		this.idCurrency = idCurrency;
	}

	/**
	 * Gets the id security class.
	 *
	 * @return the idSecurityClass
	 */
	public Integer getIdSecurityClass() {
		return idSecurityClass;
	}

	/**
	 * Sets the id security class.
	 *
	 * @param idSecurityClass the idSecurityClass to set
	 */
	public void setIdSecurityClass(Integer idSecurityClass) {
		this.idSecurityClass = idSecurityClass;
	}

	/**
	 * Gets the obj security.
	 *
	 * @return the objSecurity
	 */
	public Security getObjSecurity() {
		return objSecurity;
	}

	/**
	 * Sets the obj security.
	 *
	 * @param objSecurity the objSecurity to set
	 */
	public void setObjSecurity(Security objSecurity) {
		this.objSecurity = objSecurity;
	}

	/**
	 * Gets the id valorization type.
	 *
	 * @return the idValorizationType
	 */
	public Integer getIdValorizationType() {
		return idValorizationType;
	}

	/**
	 * Sets the id valorization type.
	 *
	 * @param idValorizationType the idValorizationType to set
	 */
	public void setIdValorizationType(Integer idValorizationType) {
		this.idValorizationType = idValorizationType;
	}

	/**
	 * Gets the date courte.
	 *
	 * @return the dateCourte
	 */
	public Date getDateCourte() {
		return dateCourte;
	}

	/**
	 * Sets the date courte.
	 *
	 * @param dateCourte the dateCourte to set
	 */
	public void setDateCourte(Date dateCourte) {
		this.dateCourte = dateCourte;
	}	

}
