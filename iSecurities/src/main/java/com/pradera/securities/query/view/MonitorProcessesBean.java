package com.pradera.securities.query.view;

import java.io.Serializable;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.model.process.ProcessMonitorDetail;
import com.pradera.model.process.ProcessMonitorLog;
import com.pradera.securities.query.facade.MonitorProcessesFacade;
import com.pradera.securities.query.to.BusinessProcessTO;
	


@DepositaryWebBean
@LoggerCreateBean
public class MonitorProcessesBean extends GenericBaseBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	MonitorProcessesFacade processesFacade;
	
	private Date finalDate;
	private Date dateSearch;
	
	private ProcessMonitorDetail closeBusinessDayTO;//1
	private ProcessMonitorDetail custodyOperationTO;//2
	private ProcessMonitorDetail reversalOperacionsExtTO;//3
	private ProcessMonitorDetail expirationCatsTO;//4
	private ProcessMonitorDetail expirationDpfsTO;//5
	private ProcessMonitorDetail executionCorporativeTO;//6
	private ProcessMonitorDetail preliminaryExeCorporativeTO;//7
	private ProcessMonitorDetail processValorationTO;//8
	private ProcessMonitorDetail reconciliationReportTO;//14
	private ProcessMonitorDetail accountingProcessTO;//9
	private ProcessMonitorDetail calculationCommissionsTO;//10
	private ProcessMonitorDetail historicalProcessVnTO;//11
	private ProcessMonitorDetail processEDV1TO;//12
	private ProcessMonitorDetail monthlyCalculationStockTO;//13
	
	private ProcessMonitorLog closeBusinessDayLog;
	private ProcessMonitorLog custodyOperationLog;
	private ProcessMonitorLog reversalOperacionsExtLog;
	private ProcessMonitorLog expirationCatsLog;
	private ProcessMonitorLog expirationDpfsLog;
	private ProcessMonitorLog preliminaryExeCorporativeLog;
	private ProcessMonitorLog executionCorporativeLog;
	private ProcessMonitorLog reconciliationReportLog;
	private ProcessMonitorLog processValorationLog;
	private ProcessMonitorLog accountingProcessLog;
	private ProcessMonitorLog calculationCommissionsLog;
	private ProcessMonitorLog historicalProcessVnLog;
	private ProcessMonitorLog processEDV1Log;
	private ProcessMonitorLog monthlyCalculationStockLog;

	@PostConstruct
	public void init() {
		 finalDate = new Date();
		 dateSearch=new Date();
		 loadProcessMonitorDetails();	
		 executedSearchProcesses();
	}
	private void loadProcessMonitorDetails(){
		closeBusinessDayTO=processesFacade.getProcessMonitorDetail(BusinessProcessType.BEGIN_END_DAY_MONITORING.getCode());
		custodyOperationTO =processesFacade.getProcessMonitorDetail(BusinessProcessType.REVERSAL_CUSTODY_PROCESS.getCode());
	    reversalOperacionsExtTO=processesFacade.getProcessMonitorDetail(BusinessProcessType.OTC_OPERATION_REJECT.getCode());
	    expirationCatsTO=processesFacade.getProcessMonitorDetail(BusinessProcessType.ACCREDITATION_CERTIFICATE_AUTOMATIC_UNBLOCK.getCode());
		expirationDpfsTO=processesFacade.getProcessMonitorDetail(BusinessProcessType.AUTOMATIC_EXPIRATION_DPF.getCode());
		preliminaryExeCorporativeTO=processesFacade.getProcessMonitorDetail(40810L);//BusinessProcessType.AUTOMATIC_CALCULATING_DAILY_VALUES.getCode();
	    executionCorporativeTO=processesFacade.getProcessMonitorDetail(40807L);//BusinessProcessType.CORPORATIVE_STOCK_PRELIMINAR_DEFINITIVO.getCode()
	    reconciliationReportTO =processesFacade.getProcessMonitorDetail(40811L);
		processValorationTO=processesFacade.getProcessMonitorDetail(BusinessProcessType.VALUATOR_EXECUTION_CORE_PROCESS.getCode());
		accountingProcessTO=processesFacade.getProcessMonitorDetail(BusinessProcessType.ACCOUNTING_DOCUMENT_GENERATE.getCode());
		calculationCommissionsTO=processesFacade.getProcessMonitorDetail(BusinessProcessType.DAILY_COLLECT_SERVICE_CALCULATION.getCode());
		historicalProcessVnTO=processesFacade.getProcessMonitorDetail(BusinessProcessType.UPDATE_PROCESS_NOMINAL_VALUE.getCode());
		processEDV1TO=processesFacade.getProcessMonitorDetail(BusinessProcessType.CALCULATING_DAILY_MOVEMENT_DESMA.getCode());
		monthlyCalculationStockTO=processesFacade.getProcessMonitorDetail(BusinessProcessType.STOCK_CALCULATION_AUTOMATIC_EXECUTION_MONTHLY.getCode());
	}
	
	public void executedSearchProcesses(){
		closeBusinessDayLog=processesFacade.getProcessMonitorLog(closeBusinessDayTO,dateSearch);
		custodyOperationLog=processesFacade.getProcessMonitorLog(custodyOperationTO,dateSearch);
		reversalOperacionsExtLog=processesFacade.getProcessMonitorLog(reversalOperacionsExtTO,dateSearch);
		expirationCatsLog=processesFacade.getProcessMonitorLog(expirationCatsTO,dateSearch);
		expirationDpfsLog=processesFacade.getProcessMonitorLog(expirationDpfsTO,dateSearch);
		preliminaryExeCorporativeLog=processesFacade.getProcessMonitorLog(preliminaryExeCorporativeTO,dateSearch);
		executionCorporativeLog=processesFacade.getProcessMonitorLog(executionCorporativeTO,dateSearch);
		reconciliationReportLog=processesFacade.getProcessMonitorLog(reconciliationReportTO, dateSearch);
		processValorationLog=processesFacade.getProcessMonitorLog(processValorationTO,dateSearch);
		accountingProcessLog=processesFacade.getProcessMonitorLog(accountingProcessTO,dateSearch);		
		calculationCommissionsLog=processesFacade.getProcessMonitorLog(calculationCommissionsTO,dateSearch);
		historicalProcessVnLog=processesFacade.getProcessMonitorLog(historicalProcessVnTO,dateSearch);
		processEDV1Log=processesFacade.getProcessMonitorLog(processEDV1TO,dateSearch);
		monthlyCalculationStockLog=processesFacade.getProcessMonitorLog(monthlyCalculationStockTO,dateSearch);
	}
	/* 1.- Inicio de dia*/
	@Asynchronous
	@LoggerAuditWeb
	public void exeCloseBusinessDay(){
		closeBusinessDayLog = processesFacade.startRemoteBatchMonitor(closeBusinessDayTO,dateSearch);
	}
	/*Reversion de operaciones de custodia*/
	@Asynchronous
	@LoggerAuditWeb
	public void exeProccesCustodyOp(){
		BusinessProcessTO businessProcessTO=new BusinessProcessTO();
		businessProcessTO.setBeanName(processesFacade.getBeanNameBusinessProcess(custodyOperationTO.getIdBusinessProcessFk()));
		businessProcessTO.setModuleName(processesFacade.getNameModuleBusinessProcess(custodyOperationTO.getIdBusinessProcessFk()));
		businessProcessTO.setIdBusinessProcessPk(custodyOperationTO.getIdBusinessProcessFk());
		businessProcessTO.setMacroProcess(BusinessProcessType.CUSTODY_OPERATION_MANAGEMENT.getCode());
		businessProcessTO.setProcessLevel1(BusinessProcessType.CUSTODY_REVERSE_MANAGEMENT.getCode());
		businessProcessTO.setProcessLevel2(BusinessProcessType.CUSTODY_OPERATION_REVERSE_PROCESS.getCode());
		custodyOperationLog = processesFacade.executeRemoteBatchProcesses(custodyOperationTO,businessProcessTO,dateSearch);
	}
	@Asynchronous
	@LoggerAuditWeb
	public void exeProccesReversalOp(){
		BusinessProcessTO businessProcessTO=new BusinessProcessTO();
		businessProcessTO.setBeanName(processesFacade.getBeanNameBusinessProcess(reversalOperacionsExtTO.getIdBusinessProcessFk()));
		businessProcessTO.setModuleName(processesFacade.getNameModuleBusinessProcess(reversalOperacionsExtTO.getIdBusinessProcessFk()));
		businessProcessTO.setIdBusinessProcessPk(reversalOperacionsExtTO.getIdBusinessProcessFk());
		businessProcessTO.setMacroProcess(BusinessProcessType.NEGOTIATION_OPERATION_MANAGEMENT.getCode());
		businessProcessTO.setMacroProcessName(BusinessProcessType.NEGOTIATION_OPERATION_MANAGEMENT.getDescription());
		reversalOperacionsExtLog = processesFacade.executeRemoteBatchProcesses(reversalOperacionsExtTO,businessProcessTO,dateSearch);
	}
	/*Vencimiento de CATS*/
	@Asynchronous
	@LoggerAuditWeb
	public void executeProccesExpirationCats(){
		BusinessProcessTO businessProcessTO=new BusinessProcessTO();
		businessProcessTO.setBeanName(processesFacade.getBeanNameBusinessProcess(expirationCatsTO.getIdBusinessProcessFk()));
		businessProcessTO.setModuleName(processesFacade.getNameModuleBusinessProcess(expirationCatsTO.getIdBusinessProcessFk()));
		businessProcessTO.setIdBusinessProcessPk(expirationCatsTO.getIdBusinessProcessFk());
		businessProcessTO.setMacroProcess(BusinessProcessType.NEGOTIATION_OPERATION_MANAGEMENT.getCode());
		expirationCatsLog = processesFacade.executeRemoteBatchProcesses(expirationCatsTO,businessProcessTO,dateSearch);
	}
	/*Vencimiento de DPFs  BusinessProcessType.EXECUTE_DPF_EXPIRATION_PROCESS */
	@Asynchronous
	@LoggerAuditWeb
	public void executeProccesExpirationDpfs(){
		BusinessProcessTO businessProcessTO=new BusinessProcessTO();
		businessProcessTO.setModuleName(processesFacade.getNameModuleBusinessProcess(expirationDpfsTO.getIdBusinessProcessFk()));
		businessProcessTO.setBeanName(processesFacade.getBeanNameBusinessProcess(expirationDpfsTO.getIdBusinessProcessFk()));
		businessProcessTO.setIdBusinessProcessPk(expirationDpfsTO.getIdBusinessProcessFk());
		businessProcessTO.setMacroProcess(BusinessProcessType.SECURITY_ISSUER_MANAGEMENT.getCode());
		expirationDpfsLog = processesFacade.executeRemoteBatchProcesses(expirationDpfsTO,businessProcessTO,dateSearch);
	}
	/*Ejecucion Preliminar de Coorporativos*/
	@Asynchronous
	@LoggerAuditWeb
	public void exePreliminarProcces(){
		BusinessProcessTO businessProcessTO=new BusinessProcessTO();
		businessProcessTO.setModuleName(processesFacade.getNameModuleBusinessProcess(preliminaryExeCorporativeTO.getIdBusinessProcessFk()));
		businessProcessTO.setBeanName(processesFacade.getBeanNameBusinessProcess(preliminaryExeCorporativeTO.getIdBusinessProcessFk()));
		businessProcessTO.setIdBusinessProcessPk(preliminaryExeCorporativeTO.getIdBusinessProcessFk());
		businessProcessTO.setMacroProcess(BusinessProcessType.CORPORATIVE_MANAGEMENT.getCode());
		preliminaryExeCorporativeLog = processesFacade.executeRemoteBatchProcesses(preliminaryExeCorporativeTO,businessProcessTO,dateSearch);
	}
	/*Ejecucion Definitivo de Coorporativos*/
	@Asynchronous
	@LoggerAuditWeb
	public void exeCorporativeFinal(){
		BusinessProcessTO businessProcessTO=new BusinessProcessTO();
		businessProcessTO.setModuleName(processesFacade.getNameModuleBusinessProcess(executionCorporativeTO.getIdBusinessProcessFk()));
		businessProcessTO.setBeanName(processesFacade.getBeanNameBusinessProcess(executionCorporativeTO.getIdBusinessProcessFk()));
		businessProcessTO.setIdBusinessProcessPk(executionCorporativeTO.getIdBusinessProcessFk());
		businessProcessTO.setMacroProcess(BusinessProcessType.CORPORATIVE_MANAGEMENT.getCode());
		executionCorporativeLog = processesFacade.executeRemoteBatchProcesses(executionCorporativeTO,businessProcessTO,dateSearch);
	}
	//Reporte de resumen de conciliacion diaria
	@LoggerAuditWeb//aqui hay que modificar ell bachero recien creado en reportes
	public void executeDailyReportConciliation(){
		BusinessProcessTO businessProcessTO=new BusinessProcessTO();
		businessProcessTO.setModuleName(processesFacade.getNameModuleBusinessProcess(reconciliationReportTO.getIdBusinessProcessFk()));
		businessProcessTO.setBeanName(processesFacade.getBeanNameBusinessProcess(reconciliationReportTO.getIdBusinessProcessFk()));
		businessProcessTO.setIdBusinessProcessPk(reconciliationReportTO.getIdBusinessProcessFk());
		businessProcessTO.setMacroProcess(BusinessProcessType.CORPORATIVE_MANAGEMENT.getCode());
		reconciliationReportLog = processesFacade.executeRemoteBatchProcesses(reconciliationReportTO,businessProcessTO,dateSearch);
	}
	@Asynchronous
	@LoggerAuditWeb
	public void exeValorationProcces(){
		processValorationLog = processesFacade.startRemoteBatchValuator(processValorationTO,dateSearch);
	}
	/*Proceos de Contabilidad*/
	@Asynchronous
	@LoggerAuditWeb
	public void exeAccountingPro(){
		BusinessProcessTO businessProcessTO=new BusinessProcessTO();
		businessProcessTO.setModuleName(processesFacade.getNameModuleBusinessProcess(accountingProcessTO.getIdBusinessProcessFk()));
		businessProcessTO.setBeanName(processesFacade.getBeanNameBusinessProcess(accountingProcessTO.getIdBusinessProcessFk()));
		businessProcessTO.setIdBusinessProcessPk(accountingProcessTO.getIdBusinessProcessFk());
		businessProcessTO.setMacroProcess(BusinessProcessType.CORPORATIVE_MANAGEMENT.getCode());
		accountingProcessLog = processesFacade.executeRemoteBatchProcesses(accountingProcessTO,businessProcessTO,dateSearch);
	}
	/*Calculo de Comisiones*/
	@Asynchronous
	@LoggerAuditWeb
	public void exeCalculationComm(){
		BusinessProcessTO businessProcessTO=new BusinessProcessTO();
		businessProcessTO.setModuleName(processesFacade.getNameModuleBusinessProcess(calculationCommissionsTO.getIdBusinessProcessFk()));
		businessProcessTO.setBeanName(processesFacade.getBeanNameBusinessProcess(calculationCommissionsTO.getIdBusinessProcessFk()));
		businessProcessTO.setIdBusinessProcessPk(calculationCommissionsTO.getIdBusinessProcessFk());
		businessProcessTO.setMacroProcess(BusinessProcessType.CORPORATIVE_MANAGEMENT.getCode());
		calculationCommissionsLog = processesFacade.executeRemoteBatchProcesses(calculationCommissionsTO,businessProcessTO,dateSearch);
	}
	/*Proceso VN Historico*/
	@Asynchronous
	@LoggerAuditWeb
	public void exechistoricalProcessVn(){
		BusinessProcessTO businessProcessTO=new BusinessProcessTO();
		businessProcessTO.setModuleName(processesFacade.getNameModuleBusinessProcess(historicalProcessVnTO.getIdBusinessProcessFk()));
		businessProcessTO.setBeanName(processesFacade.getBeanNameBusinessProcess(historicalProcessVnTO.getIdBusinessProcessFk()));
		businessProcessTO.setIdBusinessProcessPk(historicalProcessVnTO.getIdBusinessProcessFk());
		businessProcessTO.setMacroProcess(BusinessProcessType.SECURITY_ISSUER_MANAGEMENT.getCode());
		historicalProcessVnLog = processesFacade.executeRemoteBatchProcesses(historicalProcessVnTO,businessProcessTO,dateSearch);
	}
	/*Proceso EDV1*/
	@Asynchronous
	@LoggerAuditWeb
	public void exeProcessEDV1(){
		BusinessProcessTO businessProcessTO=new BusinessProcessTO();
		businessProcessTO.setModuleName(processesFacade.getNameModuleBusinessProcess(processEDV1TO.getIdBusinessProcessFk()));
		businessProcessTO.setBeanName(processesFacade.getBeanNameBusinessProcess(processEDV1TO.getIdBusinessProcessFk()));
		businessProcessTO.setIdBusinessProcessPk(processEDV1TO.getIdBusinessProcessFk());
		businessProcessTO.setMacroProcess(BusinessProcessType.CUSTODY_OPERATION_MANAGEMENT.getCode());
		processesFacade.executeRemoteBatchProcesses(processEDV1TO,businessProcessTO,dateSearch);
	}
	/*Proceso Calculo de Stock Mensual*/
	@Asynchronous
	@LoggerAuditWeb
	public void exeMonthlyCalcStock(){
		BusinessProcessTO businessProcessTO=new BusinessProcessTO();
		businessProcessTO.setModuleName(processesFacade.getNameModuleBusinessProcess(monthlyCalculationStockTO.getIdBusinessProcessFk()));
		businessProcessTO.setBeanName(processesFacade.getBeanNameBusinessProcess(monthlyCalculationStockTO.getIdBusinessProcessFk()));
		businessProcessTO.setIdBusinessProcessPk(monthlyCalculationStockTO.getIdBusinessProcessFk());
		businessProcessTO.setMacroProcess(BusinessProcessType.CUSTODY_OPERATION_MANAGEMENT.getCode());
		monthlyCalculationStockLog = processesFacade.executeRemoteBatchProcesses(monthlyCalculationStockTO,businessProcessTO,dateSearch);
	}
	public void cleanSearchPage(){
		dateSearch=new Date();
	}
	public Date getDateSearch() {
		return dateSearch;
	}

	public void setDateSearch(Date dateSearch) {
		this.dateSearch = dateSearch;
	}
	public ProcessMonitorLog getCloseBusinessDayLog() {
		return closeBusinessDayLog;
	}

	public void setCloseBusinessDayLog(ProcessMonitorLog closeBusinessDayLog) {
		this.closeBusinessDayLog = closeBusinessDayLog;
	}

	public ProcessMonitorLog getCustodyOperationLog() {
		return custodyOperationLog;
	}

	public void setCustodyOperationLog(ProcessMonitorLog custodyOperationLog) {
		this.custodyOperationLog = custodyOperationLog;
	}

	public ProcessMonitorLog getReversalOperacionsExtLog() {
		return reversalOperacionsExtLog;
	}

	public void setReversalOperacionsExtLog(
			ProcessMonitorLog reversalOperacionsExtLog) {
		this.reversalOperacionsExtLog = reversalOperacionsExtLog;
	}

	public ProcessMonitorLog getExpirationCatsLog() {
		return expirationCatsLog;
	}

	public void setExpirationCatsLog(ProcessMonitorLog expirationCatsLog) {
		this.expirationCatsLog = expirationCatsLog;
	}

	public ProcessMonitorLog getExpirationDpfsLog() {
		return expirationDpfsLog;
	}

	public void setExpirationDpfsLog(ProcessMonitorLog expirationDpfsLog) {
		this.expirationDpfsLog = expirationDpfsLog;
	}

	public ProcessMonitorLog getPreliminaryExeCorporativeLog() {
		return preliminaryExeCorporativeLog;
	}

	public void setPreliminaryExeCorporativeLog(
			ProcessMonitorLog preliminaryExeCorporativeLog) {
		this.preliminaryExeCorporativeLog = preliminaryExeCorporativeLog;
	}

	public ProcessMonitorLog getExecutionCorporativeLog() {
		return executionCorporativeLog;
	}

	public void setExecutionCorporativeLog(ProcessMonitorLog executionCorporativeLog) {
		this.executionCorporativeLog = executionCorporativeLog;
	}

	public ProcessMonitorLog getProcessValorationLog() {
		return processValorationLog;
	}

	public void setProcessValorationLog(ProcessMonitorLog processValorationLog) {
		this.processValorationLog = processValorationLog;
	}

	public ProcessMonitorLog getAccountingProcessLog() {
		return accountingProcessLog;
	}

	public void setAccountingProcessLog(ProcessMonitorLog accountingProcessLog) {
		this.accountingProcessLog = accountingProcessLog;
	}

	public ProcessMonitorLog getCalculationCommissionsLog() {
		return calculationCommissionsLog;
	}

	public void setCalculationCommissionsLog(
			ProcessMonitorLog calculationCommissionsLog) {
		this.calculationCommissionsLog = calculationCommissionsLog;
	}

	public ProcessMonitorLog getHistoricalProcessVnLog() {
		return historicalProcessVnLog;
	}

	public void setHistoricalProcessVnLog(ProcessMonitorLog historicalProcessVnLog) {
		this.historicalProcessVnLog = historicalProcessVnLog;
	}

	public ProcessMonitorLog getProcessEDV1Log() {
		return processEDV1Log;
	}

	public void setProcessEDV1Log(ProcessMonitorLog processEDV1Log) {
		this.processEDV1Log = processEDV1Log;
	}

	public ProcessMonitorLog getMonthlyCalculationStockLog() {
		return monthlyCalculationStockLog;
	}

	public void setMonthlyCalculationStockLog(
			ProcessMonitorLog monthlyCalculationStockLog) {
		this.monthlyCalculationStockLog = monthlyCalculationStockLog;
	}

	public Date getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	public ProcessMonitorLog getReconciliationReportLog() {
		return reconciliationReportLog;
	}

	public void setReconciliationReportLog(ProcessMonitorLog reconciliationReportLog) {
		this.reconciliationReportLog = reconciliationReportLog;
	}
}
