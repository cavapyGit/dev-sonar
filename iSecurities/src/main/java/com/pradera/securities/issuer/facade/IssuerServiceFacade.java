package com.pradera.securities.issuer.facade;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.security.model.type.InstitutionStateType;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.type.RegistryType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.accounts.to.LegalRepresentativeTO;
import com.pradera.core.component.accounts.to.RepresentedEntityTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.GeographicLocationServiceBean;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.HolderFile;
import com.pradera.model.accounts.HolderHistory;
import com.pradera.model.accounts.HolderHistoryState;
import com.pradera.model.accounts.HolderReqFileHistory;
import com.pradera.model.accounts.HolderRequest;
import com.pradera.model.accounts.InstitutionInformation;
import com.pradera.model.accounts.LegalRepresentative;
import com.pradera.model.accounts.LegalRepresentativeFile;
import com.pradera.model.accounts.LegalRepresentativeHistory;
import com.pradera.model.accounts.OfacPerson;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.PepPerson;
import com.pradera.model.accounts.PnaPerson;
import com.pradera.model.accounts.RepresentativeFileHistory;
import com.pradera.model.accounts.RepresentedEntity;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.EconomicActivityType;
import com.pradera.model.accounts.type.HolderFileStateType;
import com.pradera.model.accounts.type.HolderReqFileHisType;
import com.pradera.model.accounts.type.HolderRequestStateType;
import com.pradera.model.accounts.type.HolderRequestType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.IssuerRequestStateType;
import com.pradera.model.accounts.type.IssuerRequestType;
import com.pradera.model.accounts.type.JuridicClassType;
import com.pradera.model.accounts.type.LegalRepresentativeStateType;
import com.pradera.model.accounts.type.PepPersonClassificationType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.accounts.type.RepresentativeEntityStateType;
import com.pradera.model.accounts.type.RequestParticipantFileType;
import com.pradera.model.accounts.type.StateHistoryStateHolderType;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.IssuerFile;
import com.pradera.model.issuancesecuritie.IssuerFileHistory;
import com.pradera.model.issuancesecuritie.IssuerHistory;
import com.pradera.model.issuancesecuritie.IssuerHistoryState;
import com.pradera.model.issuancesecuritie.IssuerRequest;
import com.pradera.model.issuancesecuritie.type.IssuanceStateType;
import com.pradera.model.issuancesecuritie.type.IssuerDocumentType;
import com.pradera.model.issuancesecuritie.type.IssuerFileStateType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.operations.RequestCorrelativeType;
import com.pradera.securities.issuancesecuritie.facade.IssuanceSecuritiesServiceFacade;
import com.pradera.securities.issuancesecuritie.to.IssuanceTO;
import com.pradera.securities.issuer.service.IssuerServiceBean;
import com.pradera.securities.issuer.to.IssuerHistoryStateTO;
import com.pradera.securities.issuer.to.IssuerRequestTO;
import com.pradera.securities.issuer.to.IssuerTO;
import com.pradera.securities.issuer.view.InvestorTypeModelTO;
import com.pradera.securities.webservice.ServiceAPIClients;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * 
 * The Class ParticipantServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 11/02/2013
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class IssuerServiceFacade {

	/** The crud dao service bean. */
	@EJB
	CrudDaoServiceBean crudDaoServiceBean;
	
	/** The participant mgmt service bean. */
	@EJB
	IssuerServiceBean issuerServiceBean;
	
	@EJB
	IssuanceSecuritiesServiceFacade issuanceSecuritiesServiceFacade;
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	/** The participant service bean. */
	@EJB
	ParticipantServiceBean participantServiceBean;
	
	/** The geographic location service bean. */
	@EJB
    GeographicLocationServiceBean geographicLocationServiceBean;
	
	/** The country residence. */
	@Inject @Configurable Integer countryResidence;

	/** The idepositary setup. */
	@Inject @DepositarySetup IdepositarySetup idepositarySetup;
	
	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
	@EJB
	ServiceAPIClients serviceAPIClients;
	
	/** The log. */
	Logger log = Logger.getLogger(IssuerServiceFacade.class.getName());
	
	public List<InvestorTypeModelTO> getLstInvestorTypeModelTO(InvestorTypeModelTO filter) throws ServiceException {
		List<InvestorTypeModelTO> lst = issuerServiceBean.getLstInvestorTypeModelTO(filter);

		// Si la lista esta vacia, traemos la lista por defecto
		if(Validations.validateIsNullOrEmpty(lst) || lst.size() == 0) {			
			lst =  issuerServiceBean.getDefaultLstInvestorTypeModelTO();
		}
		
		// seteando su actividad economica y estado en formato texto
		for(InvestorTypeModelTO it:lst){
			for(ParameterTable economicActivity:filter.getLstEconomicActivity()){
				if(economicActivity.getParameterTablePk().equals(it.getEconomicActivity())){
					it.setEconomicActivityText(economicActivity.getParameterName());
					break;
				}
			}
			if(Validations.validateIsNotNull(it.getState())){
				it.setStateText(ParameterTableStateType.get(it.getState()).getValue());
			}
		}
		
		return lst;
	}

	/**
	 * Registry participant with representant service facade.
	 *
	 * @param legalRepresentativeHistory the legal representative history
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean registryLegalRepresentativeHistoryServiceFacade(LegalRepresentativeHistory legalRepresentativeHistory) throws ServiceException{
		crudDaoServiceBean.create(legalRepresentativeHistory);
		return true;
	}


	/**
	 * Gets the list issuer by filters service facade.
	 *
	 * @param issuerTO the issuer to
	 * @return the list issuer by filters service facade
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.ISSUER_QUERY)
	public List<Issuer> getListIssuerByFiltersServiceFacade(IssuerTO issuerTO) throws ServiceException{
		List<Issuer> lst = issuerServiceBean.getListIssuerByFiltersServiceBean(issuerTO);
		ParameterTable pt=null;
		for(Issuer i : lst){
			if(Validations.validateIsNotNullAndPositive(i.getSocietyType())){
				try {
					pt = issuerServiceBean.find(ParameterTable.class, i.getSocietyType());
					if(Validations.validateIsNotNull(pt) && Validations.validateIsNotNullAndNotEmpty(pt.getDescription()) ){
						i.setDescriptionSocietyTypePt(pt.getDescription());
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return issuerServiceBean.getListIssuerByFiltersServiceBean(issuerTO);
	}


	/**
	 * Find issuer by filters service facade.
	 *
	 * @param filter the filter
	 * @return the issuer
	 * @throws ServiceException the service exception
	 */
	public Issuer findIssuerByFiltersServiceFacade(Issuer filter) throws ServiceException{
		
		Issuer issuer =  issuerServiceBean.findIssuerByFiltersServiceBean(filter);
		if(issuer==null){
			filter.setDocumentSource(null);
			issuer =issuerServiceBean.findIssuerByFiltersServiceBean(filter);
		}
		
		return issuer;
	}
	
	/**
	 * Find institution information by id service facade.
	 *
	 * @param institutionFilter the institution filter
	 * @return the institution information
	 * @throws ServiceException the service exception
	 */
	public InstitutionInformation findInstitutionInformationByIdServiceFacade(InstitutionInformation institutionFilter) throws ServiceException {
		return issuerServiceBean.findInstitutionInformationByIdServiceBean(institutionFilter);
	}
	/**
	 * Gets the ofac person list.
	 *
	 * @param filter the filter
	 * @return the ofac person list
	 * @throws ServiceException the service exception
	 */
	public List<OfacPerson> getOfacPersonList(String filter) throws ServiceException{
		return issuerServiceBean.getOfacPersonList(filter);
	}
	
	/**
	 * Get Pep Person List.
	 *
	 * @param filter the filter
	 * @return the pep person list
	 * @throws ServiceException the service exception
	 */
	public List<PepPerson> getPepPersonList(String filter)throws ServiceException{
		return issuerServiceBean.getPepPersonList(filter);
	}
	
	/**
	 * Get Pna Person List.
	 *
	 * @param filter the filter
	 * @return the pna person list
	 * @throws ServiceException the service exception
	 */
	public List<PnaPerson> getPnaPersonList(String filter)throws ServiceException{
		return issuerServiceBean.getPnaPersonList(filter);
	}
	/**
	 * Registry legal representative service facade.
	 *
	 * @param legalRepresentative the legal representative
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean registryLegalRepresentativeServiceFacade(LegalRepresentative legalRepresentative) throws ServiceException{
		crudDaoServiceBean.create(legalRepresentative);
		return true;
	}
	
	/**
	 * Find represented entity by id service bean.
	 *
	 * @param filter the filter
	 * @return the represented entity
	 * @throws ServiceException the service exception
	 */
	public RepresentedEntity findRepresentedEntityByIdServiceBean(RepresentedEntityTO filter) throws ServiceException{
		return issuerServiceBean.findRepresentedEntityByIdServiceBean(filter);
	}
	
	/**
	 * Update holder request service facade.
	 *
	 * @param holderRequest the holder request
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean updateHolderRequestServiceFacade(HolderRequest holderRequest)throws ServiceException{	
   	 	crudDaoServiceBean.update(holderRequest);
        return true;   	 
    }


	/**
	 * Registry issuer service facade.
	 *
	 * @param issuer the issuer
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean registryIssuerServiceFacade(Issuer issuer) throws ServiceException{	
		crudDaoServiceBean.create(issuer);
		return true;
	}


	/**
	 * Update issuer service facade.
	 *
	 * @param issuer the issuer
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean updateIssuerServiceFacade(Issuer issuer) throws ServiceException{
		crudDaoServiceBean.update(issuer);
		return true;
	}


	/**
	 * Save issuer block request service facade.
	 *
	 * @param issuerRequest the issuer request
	 * @return the issuer request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.ISSUER_BLOCK_REGISTER)
	public IssuerRequest saveIssuerBlockRequestServiceFacade(IssuerRequest issuerRequest) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        issuerRequest.setRequestNumber(crudDaoServiceBean.getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_BLOCK_UNBLOCK_ISSUER));
		return crudDaoServiceBean.create(issuerRequest);
	}
	
	
	/**
	 * Confirm issuer block request service facade.
	 *
	 * @param issuerRequest the issuer request
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.ISSUER_BLOCK_CONFIRMATION)
	public boolean confirmIssuerBlockRequestServiceFacade(IssuerRequest issuerRequest) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
        
        Issuer issuerFilter = new Issuer();
		issuerFilter.setIdIssuerPk(issuerRequest.getIssuer().getIdIssuerPk());
		
		Issuer issuer = findIssuerByFiltersServiceFacade(issuerFilter);

		IssuerHistoryState issuerHistoryState = new IssuerHistoryState();			
					
		issuerHistoryState.setMotive(issuerRequest.getRequestMotive());
		issuerHistoryState.setIssuer(issuer);
		issuerHistoryState.setIssuerRequest(issuerRequest);
		issuerHistoryState.setUpdateStateDate(CommonsUtilities.currentDateTime());
		issuerHistoryState.setOldState(issuer.getStateIssuer());
		issuerHistoryState.setRegistryDate(loggerUser.getAuditTime());
		
		if(IssuerRequestType.BLOCK.getCode().equals(issuerRequest.getRequestType())){
			issuerHistoryState.setNewState(IssuerStateType.BLOCKED.getCode());
			issuer.setStateIssuer(IssuerStateType.BLOCKED.getCode());
		} else if(IssuerRequestType.UNBLOCK.getCode().equals(issuerRequest.getRequestType())){
			issuerHistoryState.setNewState(IssuerStateType.REGISTERED.getCode());
			issuer.setStateIssuer(IssuerStateType.REGISTERED.getCode());
		}				
		
		issuerServiceBean.updateStateIssuerServiceBean(issuer, loggerUser);

		issuerRequest.setState(IssuerRequestStateType.CONFIRMED.getCode());
			
		issuerHistoryState.setRegistryUser(loggerUser.getUserName());
        
		issuerServiceBean.updateStateIssuerRequestServiceBean(issuerRequest, loggerUser);
		crudDaoServiceBean.create(issuerHistoryState);
		
		return true;
	}
	
	/**
	 * Reject issuer block request service facade.
	 *
	 * @param issuerRequest the issuer request
	 * @return the issuer request
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.ISSUER_BLOCK_REJECT)
	public IssuerRequest rejectIssuerBlockRequestServiceFacade(IssuerRequest issuerRequest) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
        issuerServiceBean.updateStateIssuerRequestServiceBean(issuerRequest, loggerUser);
		return issuerRequest;
	}
	
	/**
	 * Update issuer block request service facade.
	 *
	 * @param issuerRequest the issuer request
	 * @return the issuer request
	 * @throws ServiceException the service exception
	 */
	public IssuerRequest updateIssuerBlockRequestServiceFacade(IssuerRequest issuerRequest) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		return crudDaoServiceBean.update(issuerRequest);
	}		


	/**
	 * Find issuer request by id service facade.
	 *
	 * @param issuerRequestTO the issuer request to
	 * @return the issuer request
	 * @throws ServiceException the service exception
	 */
	public IssuerRequest findIssuerRequestByIdServiceFacade(
			IssuerRequestTO issuerRequestTO) throws ServiceException {
		return issuerServiceBean.findIssuerRequestByIdServiceBean(issuerRequestTO);
	}


	/**
	 * Gets the list issuer requests by filters service facade.
	 *
	 * @param issuerTO the issuer to
	 * @return the list issuer requests by filters service facade
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.ISSUER_BLOCK_UNBLOCK_QUERY)
	public List<IssuerRequest> getListIssuerRequestsBlockUnblockByFiltersServiceFacade(
			IssuerTO issuerTO) throws ServiceException{
		return issuerServiceBean.getListIssuerRequestsByFiltersServiceBean(issuerTO);
	}
	/**
	 * Gets the list issuer requests by filters service facade.
	 *
	 * @param issuerTO the issuer to
	 * @return the list issuer requests by filters service facade
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.ISSUER_MODIFICATION_QUERY)
	public List<IssuerRequest> getListIssuerRequestsModificationByFiltersServiceFacade(
			IssuerTO issuerTO) throws ServiceException{
		return issuerServiceBean.getListIssuerRequestsByFiltersServiceBean(issuerTO);
	}

	/**
	 * Gets the list issuer history states service facade.
	 *
	 * @param issuerHistoryStateTO the issuer history state to
	 * @return the list issuer history states service facade
	 * @throws ServiceException the service exception
	 */
	public List<IssuerHistoryState> getListIssuerHistoryStatesServiceFacade(
			IssuerHistoryStateTO issuerHistoryStateTO) throws ServiceException {
		return issuerServiceBean.getListIssuerHistoryStatesServiceBean(issuerHistoryStateTO);
	}
	
	/**
	 * Gets the issuer sequence code service facade.
	 *
	 * @return the issuer sequence code service facade
	 * @throws ServiceException the service exception
	 */
	public String getIssuerSequenceCodeServiceFacade() throws ServiceException{
		return issuerServiceBean.getIssuerSequenceCodeServiceBean();
	}
	
	/**
	 * Register holder request service facade.
	 *
	 * @param holderRequest the holder request
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean registerHolderRequestServiceFacade(HolderRequest holderRequest) throws ServiceException {
		 holderRequest.setRequestNumber(crudDaoServiceBean.getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_REGISTER_RNT));
		 crudDaoServiceBean.create(holderRequest);
		 return true;
	 }	 
	
	/**
	 * Update state holder request service facade.
	 *
	 * @param holderRequest the holder request
	 * @param holder the holder
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean updateStateHolderRequestServiceFacade(HolderRequest holderRequest,Holder holder) throws ServiceException{
		 boolean flagTransaction;
		 if(holderRequest!=null){
		 crudDaoServiceBean.update(holderRequest);
		 }
		 if(holder!=null){
		 crudDaoServiceBean.update(holder);
		 }
        flagTransaction=true;
		return flagTransaction;
    }
	
	/**
	 * Register holder service facade.
	 *
	 * @param holder the holder
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean registerHolderServiceFacade(Holder holder) throws ServiceException {
		 
		 boolean flagTransaction;		 
		 crudDaoServiceBean.create(holder);
		 flagTransaction=true;
		 
		 return flagTransaction;
	 }
	/**
	 * Update holder service facade.
	 *
	 * @param holder the holder
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean updateHolderServiceFacade(Holder holder) throws ServiceException {
		 
		 boolean flagTransaction;		 
		 crudDaoServiceBean.update(holder);
		 flagTransaction=true;
		 
		 return flagTransaction;
	 }
	/**
	 * Gets the holder by document type and document number service facade.
	 *
	 * @param holder the holder
	 * @return the holder by document type and document number service facade
	 * @throws ServiceException the service exception
	 */
	public Holder getHolderByDocumentTypeAndDocumentNumberServiceFacade(Holder holder) throws ServiceException{
		Holder auxholder = issuerServiceBean.getHolderByDocumentTypeAndDocumentNumberServiceBean(holder);
		if(auxholder==null){
			holder.setDocumentSource(null);
			auxholder = issuerServiceBean.getHolderByDocumentTypeAndDocumentNumberServiceBean(holder);
		}		
		return auxholder;
	}
	
	/**
	 * Create holder.
	 *
	 * @param issuer object issuer
	 * @param loggerUser object logger User
	 * @param holder object holder
	 * @param holderRequest object holder request
	 * @return the holder
	 * @throws ServiceException  service exception
	 */
	public Holder createHolder(Issuer issuer,LoggerUser loggerUser,Holder holder,HolderRequest holderRequest)throws ServiceException
	{		
		List<HolderFile> holderFiles = null;
		
	   	holderFiles = issuer.getHolder().getHolderFile();
    	
    	Participant participantFilter = new Participant();
    	participantFilter.setIdParticipantPk(idepositarySetup.getIdParticipantDepositary());
    	
    	Participant participant = participantServiceBean.findParticipantByFiltersServiceBean(participantFilter);
    	
    	holderRequest = new HolderRequest();
    	holderRequest.setStateHolderRequest(HolderRequestStateType.CONFIRMED.getCode());
    	holderRequest.setRequestType(HolderRequestType.CREATION.getCode());
    	holderRequest.setActionDate(CommonsUtilities.currentDateTime());    	
		holderRequest.setRegistryDate(CommonsUtilities.currentDateTime());
		holderRequest.setRegistryUser(loggerUser.getUserName());
		holderRequest.setApproveDate(CommonsUtilities.currentDateTime());
		holderRequest.setApproveUser(loggerUser.getUserName());	
		holderRequest.setConfirmDate(CommonsUtilities.currentDateTime());
		holderRequest.setConfirmUser(loggerUser.getUserName());
		holderRequest.setParticipant(participant);
		
    	
    	HolderHistory holderHistory = new HolderHistory();
    	holderHistory.setBirthDate(issuer.getCreationDate());
    	holderHistory.setIndMinor(BooleanType.NO.getCode());
    	holderHistory.setIndDisabled(BooleanType.NO.getCode());
    	holderHistory.setIndPEP(BooleanType.NO.getCode());	 
    	//Set the document type Parameter of Holder instead of document tipe parameter of Participant
    	if(IssuerDocumentType.RUC.getCode().equals(issuer.getDocumentType())){
    		holderHistory.setDocumentType(DocumentType.RUC.getCode());
    	} else if(IssuerDocumentType.DIO.getCode().equals(issuer.getDocumentType())){
    		holderHistory.setDocumentType(DocumentType.DIO.getCode());
    	}else if(IssuerDocumentType.CI.getCode().equals(issuer.getDocumentType())){
    		holderHistory.setDocumentType(DocumentType.CI.getCode());
    	}else if(IssuerDocumentType.CIE.getCode().equals(issuer.getDocumentType())){
    		holderHistory.setDocumentType(DocumentType.CIE.getCode());
    	}else if(IssuerDocumentType.PASAPORTE.getCode().equals(issuer.getDocumentType())){
    		holderHistory.setDocumentType(DocumentType.PAS.getCode());
    	}else if(IssuerDocumentType.CDN.getCode().equals(issuer.getDocumentType())){
    		holderHistory.setDocumentType(DocumentType.CDN.getCode());
    	}
    	
    	holderHistory.setDocumentSource(issuer.getDocumentSource());
    	holderHistory.setRelatedCui(issuer.getRelatedCui());
    	holderHistory.setFundAdministrator(issuer.getFundAdministrator());
    	holderHistory.setFundType(issuer.getFundType());
    	holderHistory.setTransferNumber(issuer.getTransferNumber());
    	if(issuer.getEconomicActivity().equals(EconomicActivityType.AFP.getCode())
    			|| issuer.getEconomicActivity().equals(EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode())
    			|| issuer.getEconomicActivity().equals(EconomicActivityType.FONDOS_INVERSION_CERRADOS.getCode())){
    		holderHistory.setMnemonic(issuer.getMnemonic());
        }
    	
    	holderHistory.setDocumentNumber(issuer.getDocumentNumber());
    	holderHistory.setEconomicActivity(issuer.getEconomicActivity());
    	holderHistory.setEconomicSector(issuer.getEconomicSector());
    	holderHistory.setEmail(issuer.getEmail());
    	holderHistory.setFaxNumber(issuer.getFaxNumber());
    	holderHistory.setFullName(issuer.getBusinessName());
    	holderHistory.setHolderType(PersonType.JURIDIC.getCode());
    	holderHistory.setHomePhoneNumber(issuer.getPhoneNumber());
    	holderHistory.setOfficePhoneNumber(issuer.getPhoneNumber());
    	holderHistory.setJuridicClass(JuridicClassType.NORMAL.getCode());
    	holderHistory.setLegalAddress(issuer.getLegalAddress());
    	holderHistory.setLegalDepartment(issuer.getDepartment());
    	holderHistory.setLegalDistrict(issuer.getDistrict());
    	holderHistory.setLegalProvince(issuer.getProvince());
    	holderHistory.setLegalResidenceCountry(issuer.getGeographicLocation().getIdGeographicLocationPk());	    		    	
    	holderHistory.setNationality(issuer.getGeographicLocation().getIdGeographicLocationPk());
    	holderHistory.setPostalAddress(issuer.getLegalAddress());
    	holderHistory.setPostalDepartment(issuer.getDepartment());
    	holderHistory.setPostalDistrict(issuer.getDistrict());
		holderHistory.setPostalProvince(issuer.getProvince());
		holderHistory.setPostalResidenceCountry(issuer.getGeographicLocation().getIdGeographicLocationPk());
		holderHistory.setIndResidence(countryResidence.equals(issuer.getGeographicLocation().getIdGeographicLocationPk()) ? BooleanType.YES.getCode() : BooleanType.NO.getCode());
		holderHistory.setStateHolderHistory(HolderRequestStateType.CONFIRMED.getCode());
		holderHistory.setRegistryDate(CommonsUtilities.currentDateTime());
		holderHistory.setRegistryUser(loggerUser.getUserName());
		holderHistory.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
		holderHistory.setHolderRequest(holderRequest);				
		holderHistory.setIndCanNegotiate(validateIndCanTransfer(issuer.getEconomicActivity(), holderHistory.getIndCanNegotiate()));
		holderHistory.setIndSirtexNeg(BooleanType.YES.getCode());
		List<HolderHistory> holderHistories = new ArrayList<HolderHistory>();
		holderHistories.add(holderHistory);
		
		holderRequest.setHolderHistories(holderHistories);						
		
		if(Validations.validateListIsNotNullAndNotEmpty(holderFiles)){
			List<HolderReqFileHistory> holderReqFileHistories = new ArrayList<HolderReqFileHistory>();
			for (HolderFile holderFile : holderFiles) {
				HolderReqFileHistory holderReqFileHistory = new HolderReqFileHistory();

				holderReqFileHistory.setHolderReqFileHisType(HolderReqFileHisType.CREATION.getCode());
				holderReqFileHistory.setFilename(holderFile.getFilename());
				holderReqFileHistory.setFileHolderReq(holderFile.getFileHolder());
				holderReqFileHistory.setRegistryUser(loggerUser.getUserName());
				holderReqFileHistory.setRegistryDate(CommonsUtilities.currentDate());
				holderReqFileHistory.setHolderRequest(holderRequest);
				holderReqFileHistory.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
				holderReqFileHistory.setStateFile(HolderFileStateType.REGISTERED.getCode());
				holderReqFileHistory.setDocumentType(holderFile.getDocumentType());
				holderReqFileHistory.setDescription(holderFile.getDescription());
				
				holderReqFileHistories.add(holderReqFileHistory);
			}
			
			holderRequest.setHolderReqFileHistories(holderReqFileHistories);
		}
		
		registerHolderRequestServiceFacade(holderRequest);									
    	
    	holder = new Holder();	    	
    	holder.setBirthDate(issuer.getCreationDate());
    	holder.setIndMinor(BooleanType.NO.getCode());	    	
    	holder.setIndDisabled(BooleanType.NO.getCode());	
    	//Set the document type Parameter of Holder instead of document type parameter of Issuer
    	if(IssuerDocumentType.RUC.getCode().equals(issuer.getDocumentType())){
    		holder.setDocumentType(DocumentType.RUC.getCode());
    	} else if(IssuerDocumentType.DIO.getCode().equals(issuer.getDocumentType())){
    		holder.setDocumentType(DocumentType.DIO.getCode());
    	}
    	
    	holder.setDocumentSource(issuer.getDocumentSource());
    	holder.setRelatedCui(issuer.getRelatedCui());
    	holder.setFundAdministrator(issuer.getFundAdministrator());
    	holder.setFundType(issuer.getFundType());
    	holder.setTransferNumber(issuer.getTransferNumber());
    	if(issuer.getEconomicActivity().equals(EconomicActivityType.AFP.getCode())
    			|| issuer.getEconomicActivity().equals(EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode())
    			|| issuer.getEconomicActivity().equals(EconomicActivityType.FONDOS_INVERSION_CERRADOS.getCode())){
    		holder.setMnemonic(issuer.getMnemonic());
       }
    	
    	holder.setDocumentNumber(issuer.getDocumentNumber());
    	holder.setEconomicActivity(issuer.getEconomicActivity());
    	holder.setEconomicSector(issuer.getEconomicSector());
    	holder.setEmail(issuer.getEmail());
    	holder.setFaxNumber(issuer.getFaxNumber());
    	holder.setFullName(issuer.getBusinessName());
    	holder.setHolderType(PersonType.JURIDIC.getCode());
    	holder.setHomePhoneNumber(issuer.getPhoneNumber());
    	holder.setOfficePhoneNumber(issuer.getPhoneNumber());
    	holder.setJuridicClass(JuridicClassType.NORMAL.getCode());
    	holder.setLegalAddress(issuer.getLegalAddress());
    	holder.setLegalDepartment(issuer.getDepartment());
    	holder.setLegalDistrict(issuer.getDistrict());
    	holder.setLegalProvince(issuer.getProvince());
    	holder.setLegalResidenceCountry(issuer.getGeographicLocation().getIdGeographicLocationPk());	    		    	
    	holder.setNationality(issuer.getGeographicLocation().getIdGeographicLocationPk());
    	holder.setPostalAddress(issuer.getLegalAddress());
    	holder.setPostalDepartment(issuer.getDepartment());
    	holder.setPostalDistrict(issuer.getDistrict());
    	holder.setPostalProvince(issuer.getProvince());
    	holder.setPostalResidenceCountry(issuer.getGeographicLocation().getIdGeographicLocationPk());
    	holder.setIndResidence(countryResidence.equals(issuer.getGeographicLocation().getIdGeographicLocationPk()) ? BooleanType.YES.getCode() : BooleanType.NO.getCode());
    	holder.setStateHolder(HolderStateType.REGISTERED.getCode());
    	holder.setRegistryDate(CommonsUtilities.currentDateTime());
    	holder.setRegistryUser(loggerUser.getUserName());
    	holder.setParticipant(participant);
    	holder.setIndCanNegotiate(validateIndCanTransfer(issuer.getEconomicActivity(), holder.getIndCanNegotiate()));
    	holder.setIndSirtexNeg(BooleanType.YES.getCode());
    	if(Validations.validateIsNotNullAndPositive(issuer.getInvestorType())){
    		holder.setInvestorType(issuer.getInvestorType());
    	}

    	
    	Long nextVal=assignIdentityHolder(holder.getEconomicActivity());
		log.severe("VALOR NEXTVAL ASIGNAR A HOLDER ----------->"+nextVal);
		log.severe("ACTIVIDAD ECONOMICA ----------->"+holder.getEconomicActivity());
    	holder.setIdHolderPk(nextVal);    	
    	registerHolderServiceFacade(holder);
    	
    	holderRequest.setHolder(holder);
    	
    	List<HolderHistoryState> lstHolderHistoryState = new ArrayList<HolderHistoryState>();
		HolderHistoryState holderHistoryState = new HolderHistoryState();
		
		holderHistoryState.setHolder(holder);
		holderHistoryState.setHolderRequest(holderRequest);
		holderHistoryState.setRegistryUser(loggerUser.getUserName());
		holderHistoryState.setHolderRequest(holderRequest);
		holderHistoryState.setOldState(StateHistoryStateHolderType.REGISTERED.getCode());
		holderHistoryState.setNewState(StateHistoryStateHolderType.REGISTERED.getCode());
		holderHistoryState.setUpdateStateDate(CommonsUtilities.currentDateTime());
		lstHolderHistoryState.add(holderHistoryState);				
		
		holderRequest.setHolderHistoryStates(lstHolderHistoryState);
    	
    	updateHolderRequestServiceFacade(holderRequest);
    	
    	if(Validations.validateListIsNotNullAndNotEmpty(holderFiles)){
			for (HolderFile holderFile : holderFiles) {

				holderFile.setHolderFileType(HolderReqFileHisType.CREATION.getCode());
				holderFile.setRegistryUser(loggerUser.getUserName());
				holderFile.setRegistryDate(CommonsUtilities.currentDate());
				holderFile.setHolder(holder);
				holderFile.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
				holderFile.setStateFile(HolderFileStateType.REGISTERED.getCode());
				
				crudDaoServiceBean.create(holderFile);
			}								
		}
    	return holder;
	}
	
	/**
	 * Assign identity holder.
	 *
	 * @param economicActivity the economic activity
	 * @return the long
	 */
	public Long assignIdentityHolder(Integer economicActivity){
		log.severe("INICIO DE ASIGNAR PK DE HOLDER");
		Long nextVal=null;
		//Fondos de inversion abierto y cerrado ref issue : 495
		if(economicActivity!=null && (economicActivity.equals(EconomicActivityType.FONDOS_INVERSION_CERRADOS.getCode()))){
			nextVal=getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK_FI);
			log.severe("ACCOUNT_ID_HOLDER_PK_FI");
		    log.severe("NEXTVAL : "+nextVal);
		}
		if(economicActivity!=null && (economicActivity.equals(EconomicActivityType.FONDOS_INVERSION_ABIERTOS.getCode()))				){
			nextVal=getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK_FI);
			log.severe("ACCOUNT_ID_HOLDER_PK_FI");
		    log.severe("NEXTVAL : "+nextVal);
		}
    	if(economicActivity!=null && economicActivity.equals(EconomicActivityType.AGENCIAS_BOLSA.getCode())){
			nextVal=getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK_AB);
		    log.severe("RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK_AB");
		    log.severe("NEXTVAL : "+nextVal);
    	}
		else if(economicActivity!=null && economicActivity.equals(EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode())){
			nextVal=getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK_SAFI);
			log.severe("ACCOUNT_ID_HOLDER_PK_SAFI");
		    log.severe("NEXTVAL : "+nextVal);
		}
		else if(economicActivity!=null && economicActivity.equals(EconomicActivityType.FONDOS_INVERSION_CERRADOS.getCode())){
			nextVal=getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK_FIC);
			log.severe("ACCOUNT_ID_HOLDER_PK_FIC");
		    log.severe("NEXTVAL : "+nextVal);
		
		}
		else if(economicActivity!=null && economicActivity.equals(EconomicActivityType.ADMINISTRADORAS_FONDOS_PENSIONES.getCode())){
			nextVal=getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK_AFP);
			log.severe("ACCOUNT_ID_HOLDER_PK_AFP");
		    log.severe("NEXTVAL : "+nextVal);
		
		}
		else if(economicActivity!=null && economicActivity.equals(EconomicActivityType.BANCOS.getCode())){
			nextVal=getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK_B);
			log.severe("ACCOUNT_ID_HOLDER_PK_B");
		    log.severe("NEXTVAL : "+nextVal);
		
		}
		else if(economicActivity!=null && (economicActivity.equals(EconomicActivityType.MUTUALES.getCode())
			|| economicActivity.equals(EconomicActivityType.FONDOS_FINANCIEROS_PRIVADOS.getCode())
			|| economicActivity.equals(EconomicActivityType.COOPERATIVAS.getCode()))){
			nextVal=getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK_MFC);
			log.severe("ACCOUNT_ID_HOLDER_PK_MFC");
			log.severe("NEXTVAL : "+nextVal);
		
		}
		else if(economicActivity!=null && economicActivity.equals(EconomicActivityType.COMPANIAS_SEGURO.getCode())){
			nextVal=getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK_CS);
			log.severe("ACCOUNT_ID_HOLDER_PK_CS");
		    log.severe("NEXTVAL : "+nextVal);
		
		}
		else{
			
			nextVal=getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK);
			log.severe("ENTRO A SECUENCIA ORIGINAL DE HOLDER");
		    log.severe("NEXTVAL : "+nextVal);
			
		}
	    
	    if(nextVal!=null && nextVal.equals(new Long(-1))){
	    	nextVal=getNextCorrelativeSecuence(RequestCorrelativeType.ACCOUNT_ID_HOLDER_PK);
	    	log.severe("LA SECUENCIA FUERA DE RANGO");
	    	log.severe("NEXTVAL : "+nextVal);
		}
	    
	    if(nextVal!=null && !nextVal.equals(new Long(-1))){
	    	log.severe("LA SECUENCIA DENTRO DE RANGO");
	    	log.severe("NEXTVAL : "+nextVal);
				if(accountsFacade.validateExistHolderServiceFacade(nextVal)){
					log.severe("LA IDENTIFICADOR YA EXISTE");			    	
					assignIdentityHolder(economicActivity);					
				}
		}
	    
	    return nextVal;
	}
	
	/**
	 * Update holder.
	 *
	 * @param issuer object issuer
	 * @param loggerUser object logger User
	 * @throws ServiceException  service exception
	 */
	public void updateHolder(Issuer issuer,LoggerUser loggerUser)throws ServiceException
	{		
		List<HolderFile> holderFilesOrigin = null;
		List<HolderFile> holderFiles = null;
		
	   	holderFilesOrigin = issuer.getHolder().getHolderFile();
	   	holderFiles = issuer.getHolder().getHolderFile();
    	
    	Participant participantFilter = new Participant();
    	participantFilter.setIdParticipantPk(idepositarySetup.getIdParticipantDepositary());
    	
    	Participant participant = participantServiceBean.findParticipantByFiltersServiceBean(participantFilter);
    	    	
    	HolderRequest holderRequest = new HolderRequest();
    	holderRequest.setStateHolderRequest(HolderRequestStateType.CONFIRMED.getCode());
    	holderRequest.setRequestType(HolderRequestType.MODIFICATION.getCode());
    	holderRequest.setActionDate(CommonsUtilities.currentDateTime());    	
		holderRequest.setRegistryDate(CommonsUtilities.currentDateTime());
		holderRequest.setRegistryUser(loggerUser.getUserName());
		holderRequest.setApproveDate(CommonsUtilities.currentDateTime());
		holderRequest.setApproveUser(loggerUser.getUserName());	
		holderRequest.setConfirmDate(CommonsUtilities.currentDateTime());
		holderRequest.setConfirmUser(loggerUser.getUserName());
		holderRequest.setParticipant(participant);
		
    	
    	HolderHistory holderHistory = new HolderHistory();
    	holderHistory.setBirthDate(issuer.getCreationDate());
    	holderHistory.setIndMinor(BooleanType.NO.getCode());
    	holderHistory.setIndDisabled(BooleanType.NO.getCode());
    	holderHistory.setIndPEP(BooleanType.NO.getCode());	 
    	//Set the document type Parameter of Holder instead of document tipe parameter of Participant
    	if(IssuerDocumentType.RUC.getCode().equals(issuer.getDocumentType())){
    		holderHistory.setDocumentType(DocumentType.RUC.getCode());
    	} else if(IssuerDocumentType.DIO.getCode().equals(issuer.getDocumentType())){
    		holderHistory.setDocumentType(DocumentType.DIO.getCode());
    	}else if(IssuerDocumentType.CI.getCode().equals(issuer.getDocumentType())){
    		holderHistory.setDocumentType(DocumentType.CI.getCode());
    	}else if(IssuerDocumentType.CIE.getCode().equals(issuer.getDocumentType())){
    		holderHistory.setDocumentType(DocumentType.CIE.getCode());
    	}else if(IssuerDocumentType.PASAPORTE.getCode().equals(issuer.getDocumentType())){
    		holderHistory.setDocumentType(DocumentType.PAS.getCode());
    	}else if(IssuerDocumentType.CDN.getCode().equals(issuer.getDocumentType())){
    		holderHistory.setDocumentType(DocumentType.CDN.getCode());
    	}
    	holderHistory.setDocumentNumber(issuer.getDocumentNumber());
    	holderHistory.setDocumentSource(issuer.getDocumentSource());
    	holderHistory.setRelatedCui(issuer.getRelatedCui());
    	holderHistory.setFundAdministrator(issuer.getFundAdministrator());
    	holderHistory.setFundType(issuer.getFundType());
    	holderHistory.setTransferNumber(issuer.getTransferNumber());
    	holderHistory.setEconomicActivity(issuer.getEconomicActivity());
    	holderHistory.setEconomicSector(issuer.getEconomicSector());
    	holderHistory.setEmail(issuer.getEmail());
    	holderHistory.setFaxNumber(issuer.getFaxNumber());
    	holderHistory.setFullName(issuer.getBusinessName());
    	holderHistory.setHolderType(PersonType.JURIDIC.getCode());
    	holderHistory.setHomePhoneNumber(issuer.getPhoneNumber());
    	holderHistory.setOfficePhoneNumber(issuer.getPhoneNumber());
    	holderHistory.setJuridicClass(JuridicClassType.NORMAL.getCode());
    	holderHistory.setLegalAddress(issuer.getLegalAddress());
    	holderHistory.setLegalDepartment(issuer.getDepartment());
    	holderHistory.setLegalDistrict(issuer.getDistrict());
    	holderHistory.setLegalProvince(issuer.getProvince());
    	holderHistory.setLegalResidenceCountry(issuer.getGeographicLocation().getIdGeographicLocationPk());	    		    	
    	holderHistory.setNationality(issuer.getGeographicLocation().getIdGeographicLocationPk());
    	holderHistory.setPostalAddress(issuer.getLegalAddress());
    	holderHistory.setPostalDepartment(issuer.getDepartment());
    	holderHistory.setPostalDistrict(issuer.getDistrict());
		holderHistory.setPostalProvince(issuer.getProvince());
		holderHistory.setPostalResidenceCountry(issuer.getGeographicLocation().getIdGeographicLocationPk());
		holderHistory.setIndResidence(countryResidence.equals(issuer.getGeographicLocation().getIdGeographicLocationPk()) ? BooleanType.YES.getCode() : BooleanType.NO.getCode());
		holderHistory.setStateHolderHistory(HolderRequestStateType.CONFIRMED.getCode());
		holderHistory.setRegistryDate(CommonsUtilities.currentDateTime());
		holderHistory.setRegistryUser(loggerUser.getUserName());
		holderHistory.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
		holderHistory.setHolderRequest(holderRequest);		
		
		// correccion para evitar constraint de BBDD de not null IND_SIRTEX_NEG 
		if(Validations.validateIsNotNull(issuer.getIndSirtexNegotiate())){
			holderHistory.setIndSirtexNeg(issuer.getIndSirtexNegotiate());
		}else{
			holderHistory.setIndSirtexNeg(Integer.valueOf(0));
		}
					
		List<HolderHistory> holderHistories = new ArrayList<HolderHistory>();
		holderHistories.add(holderHistory);
		
		holderRequest.setHolderHistories(holderHistories);						
		
		if(Validations.validateListIsNotNullAndNotEmpty(holderFilesOrigin)){
			List<HolderReqFileHistory> holderReqFileHistories = new ArrayList<HolderReqFileHistory>();
			for (HolderFile holderFile : holderFilesOrigin) {
				HolderReqFileHistory holderReqFileHistory = new HolderReqFileHistory();

				holderReqFileHistory.setHolderReqFileHisType(HolderReqFileHisType.CREATION.getCode());
				holderReqFileHistory.setFilename(holderFile.getFilename());
				holderReqFileHistory.setFileHolderReq(holderFile.getFileHolder());
				holderReqFileHistory.setRegistryUser(loggerUser.getUserName());
				holderReqFileHistory.setRegistryDate(CommonsUtilities.currentDate());
				holderReqFileHistory.setHolderRequest(holderRequest);
				holderReqFileHistory.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
				holderReqFileHistory.setStateFile(HolderFileStateType.REGISTERED.getCode());
				holderReqFileHistory.setDocumentType(holderFile.getDocumentType());
				holderReqFileHistory.setDescription(holderFile.getDescription());
				
				holderReqFileHistories.add(holderReqFileHistory);
			}
			
			holderRequest.setHolderReqFileHistories(holderReqFileHistories);
		}
		
		registerHolderRequestServiceFacade(holderRequest);									
    	
		Holder holder = issuer.getHolder();		
		holder.setBirthDate(issuer.getCreationDate());
    	holder.setIndMinor(BooleanType.NO.getCode());	    	
    	holder.setIndDisabled(BooleanType.NO.getCode());	
    	//Set the document type Parameter of Holder instead of document type parameter of Issuer
    	if(IssuerDocumentType.RUC.getCode().equals(issuer.getDocumentType())){
    		holder.setDocumentType(DocumentType.RUC.getCode());
    	} else if(IssuerDocumentType.DIO.getCode().equals(issuer.getDocumentType())){
    		holder.setDocumentType(DocumentType.DIO.getCode());
    	}
    	holder.setDocumentNumber(issuer.getDocumentNumber());
    	holder.setDocumentSource(issuer.getDocumentSource());
    	holder.setRelatedCui(issuer.getRelatedCui());
    	holder.setFundAdministrator(issuer.getFundAdministrator());
    	holder.setFundType(issuer.getFundType());
    	holder.setTransferNumber(issuer.getTransferNumber());
    	
    	holder.setEconomicActivity(issuer.getEconomicActivity());
    	holder.setEconomicSector(issuer.getEconomicSector());
    	holder.setEmail(issuer.getEmail());
    	holder.setFaxNumber(issuer.getFaxNumber());
    	holder.setFullName(issuer.getBusinessName());
    	holder.setHolderType(PersonType.JURIDIC.getCode());
    	holder.setHomePhoneNumber(issuer.getPhoneNumber());
    	holder.setOfficePhoneNumber(issuer.getPhoneNumber());
    	holder.setJuridicClass(JuridicClassType.NORMAL.getCode());
    	holder.setLegalAddress(issuer.getLegalAddress());
    	holder.setLegalDepartment(issuer.getDepartment());
    	holder.setLegalDistrict(issuer.getDistrict());
    	holder.setLegalProvince(issuer.getProvince());
    	holder.setLegalResidenceCountry(issuer.getGeographicLocation().getIdGeographicLocationPk());	    		    	
    	holder.setNationality(issuer.getGeographicLocation().getIdGeographicLocationPk());
    	holder.setPostalAddress(issuer.getLegalAddress());
    	holder.setPostalDepartment(issuer.getDepartment());
    	holder.setPostalDistrict(issuer.getDistrict());
    	holder.setPostalProvince(issuer.getProvince());
    	holder.setPostalResidenceCountry(issuer.getGeographicLocation().getIdGeographicLocationPk());
    	holder.setIndResidence(countryResidence.equals(issuer.getGeographicLocation().getIdGeographicLocationPk()) ? BooleanType.YES.getCode() : BooleanType.NO.getCode());
    	holder.setStateHolder(HolderStateType.REGISTERED.getCode());
    	holder.setRegistryDate(CommonsUtilities.currentDateTime());
    	holder.setRegistryUser(loggerUser.getUserName());
    	holder.setParticipant(participant);
    	    	    	
    	holderRequest.setHolder(holder);
    	
    	List<HolderHistoryState> lstHolderHistoryState = new ArrayList<HolderHistoryState>();
		HolderHistoryState holderHistoryState = new HolderHistoryState();
		
		holderHistoryState.setHolder(holder);
		holderHistoryState.setHolderRequest(holderRequest);
		holderHistoryState.setRegistryUser(loggerUser.getUserName());
		holderHistoryState.setHolderRequest(holderRequest);
		holderHistoryState.setOldState(StateHistoryStateHolderType.REGISTERED.getCode());
		holderHistoryState.setNewState(StateHistoryStateHolderType.REGISTERED.getCode());
		holderHistoryState.setUpdateStateDate(CommonsUtilities.currentDateTime());
		lstHolderHistoryState.add(holderHistoryState);				
		
		holderRequest.setHolderHistoryStates(lstHolderHistoryState);
    	
    	updateHolderRequestServiceFacade(holderRequest);
    	
    	if(Validations.validateListIsNotNullAndNotEmpty(holderFiles)){
			for (HolderFile holderFile : holderFiles) {

				holderFile.setHolderFileType(HolderReqFileHisType.CREATION.getCode());
				holderFile.setRegistryUser(loggerUser.getUserName());
				holderFile.setRegistryDate(CommonsUtilities.currentDate());
				holderFile.setHolder(holder);
				holderFile.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
				holderFile.setStateFile(HolderFileStateType.REGISTERED.getCode());
				
				crudDaoServiceBean.update(holderFile);
			}								
		}
    	
    	updateHolderServiceFacade(holder);
	}
	
	/**
	 * load List legal representative .
	 *
	 * @param lstLegalRepresentativeHistory List legal representative history
	 * @return list legal representative
	 * @throws ServiceException service exception
	 */
	public List loadListLegalRepresentative( List<LegalRepresentativeHistory> lstLegalRepresentativeHistory)throws ServiceException
	{
		List<LegalRepresentative> lstLegalRepresentativeFound = new ArrayList<LegalRepresentative>();
		LegalRepresentative legalRepresentativeFound;        
        for (LegalRepresentativeHistory legalRepresentativeHistory : lstLegalRepresentativeHistory) {
        	if(legalRepresentativeHistory.isFlagLegalImport() && 
        			!legalRepresentativeHistory.isFlagActiveFilesByHolderImport()) {
        		legalRepresentativeFound = new LegalRepresentative();
            	legalRepresentativeFound.setDocumentType(legalRepresentativeHistory.getDocumentType());
            	legalRepresentativeFound.setDocumentNumber(legalRepresentativeHistory.getDocumentNumber());
            	legalRepresentativeFound = getLegalRepresentativeByDocumentTypeAndDocumentNumberServiceFacade(legalRepresentativeFound);
            	
            	if(Validations.validateIsNotNull(legalRepresentativeFound)){
            		lstLegalRepresentativeFound.add(legalRepresentativeFound);
            	}
        	}
		}
        return lstLegalRepresentativeFound;
	}
	
	/**
	 * load List Represented Entities.
	 *
	 * @param representedEntities object RepresentedEntity
	 * @param lstLegalRepresentativeHistory object LegalRepresentativeHistory
	 * @param lstLegalRepresentativeFound object LegalRepresentative
	 * @param issuer object Issuer
	 * @param loggerUser object LoggerUser
	 * @param holderRequest object HolderRequest
	 * @return the list
	 * @throws ServiceException Service Exception
	 */
	public List<RepresentedEntity> loadListRepresentedEntities(List<RepresentedEntity> representedEntities,List<LegalRepresentativeHistory> lstLegalRepresentativeHistory,
			List<LegalRepresentative> lstLegalRepresentativeFound,Issuer issuer,LoggerUser loggerUser,HolderRequest holderRequest)throws ServiceException
	{
		RepresentedEntity representedEntity = null;
		LegalRepresentative objLegalRepresentative = null;
		PepPerson pepPerson;	
		GeographicLocation countryPep;
		boolean indLegalRepresentativeFoumd;		
		for (LegalRepresentativeHistory legalRepresentativeHistory : lstLegalRepresentativeHistory) {
			
			indLegalRepresentativeFoumd = false;
			if(Validations.validateListIsNotNullAndNotEmpty(lstLegalRepresentativeFound)){
				for (LegalRepresentative legalRepresentativeAux : lstLegalRepresentativeFound) {
					if(legalRepresentativeAux.getDocumentType().equals(legalRepresentativeHistory.getDocumentType()) &&
							legalRepresentativeAux.getDocumentNumber().equals(legalRepresentativeHistory.getDocumentNumber())){
						indLegalRepresentativeFoumd = true;
						objLegalRepresentative = legalRepresentativeAux;
						break;
					}
				}
			}
			
			if(!indLegalRepresentativeFoumd) {
				legalRepresentativeHistory.setRegistryUser(loggerUser.getUserName());
				legalRepresentativeHistory.setStateRepreHistory(LegalRepresentativeStateType.REGISTERED.getCode());
				legalRepresentativeHistory.setRepreHistoryType(HolderRequestType.CREATION.getCode());
				legalRepresentativeHistory.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());								
				
				if(issuer.isIndHolderWillBeCreated()){
					legalRepresentativeHistory.setHolderRequest(holderRequest);
				}
				
				if(Validations.validateListIsNotNullAndNotEmpty(legalRepresentativeHistory.getRepresenteFileHistory())){
					for (RepresentativeFileHistory representativeFileHistory : legalRepresentativeHistory.getRepresenteFileHistory()) {
						representativeFileHistory.setRepresentFileHistType(HolderReqFileHisType.CREATION.getCode());
						representativeFileHistory.setRegistryUser(loggerUser.getUserName());
						representativeFileHistory.setRegistryDate(CommonsUtilities.currentDate());
						representativeFileHistory.setLegalRepresentativeHistory(legalRepresentativeHistory);
						representativeFileHistory.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
						representativeFileHistory.setStateFile(HolderFileStateType.REGISTERED.getCode());
					}
				}
				
				if(!Validations.validateIsNotNullAndNotEmpty(legalRepresentativeHistory.getIdRepresentativeHistoryPk()))
				registryLegalRepresentativeHistoryServiceFacade(legalRepresentativeHistory);
				
				objLegalRepresentative = new LegalRepresentative();
				objLegalRepresentative.setBirthDate(legalRepresentativeHistory.getBirthDate());
				objLegalRepresentative.setDocumentNumber(legalRepresentativeHistory.getDocumentNumber());
				objLegalRepresentative.setDocumentType(legalRepresentativeHistory.getDocumentType());
				objLegalRepresentative.setEconomicActivity(legalRepresentativeHistory.getEconomicActivity());
				objLegalRepresentative.setEconomicSector(legalRepresentativeHistory.getEconomicSector());
				objLegalRepresentative.setEmail(legalRepresentativeHistory.getEmail());
				objLegalRepresentative.setFaxNumber(legalRepresentativeHistory.getFaxNumber());
				objLegalRepresentative.setFirstLastName(legalRepresentativeHistory.getFirstLastName());
				objLegalRepresentative.setFullName(legalRepresentativeHistory.getFullName());
				objLegalRepresentative.setHomePhoneNumber(legalRepresentativeHistory.getHomePhoneNumber());
				objLegalRepresentative.setIndResidence(legalRepresentativeHistory.getIndResidence());
				objLegalRepresentative.setLegalAddress(legalRepresentativeHistory.getLegalAddress());
				objLegalRepresentative.setLegalDistrict(legalRepresentativeHistory.getLegalDistrict());
				objLegalRepresentative.setLegalProvince(legalRepresentativeHistory.getLegalProvince());
				objLegalRepresentative.setLegalDepartment(legalRepresentativeHistory.getLegalDepartment());
				objLegalRepresentative.setLegalResidenceCountry(legalRepresentativeHistory.getLegalResidenceCountry());
				objLegalRepresentative.setMobileNumber(legalRepresentativeHistory.getMobileNumber());
				objLegalRepresentative.setName(legalRepresentativeHistory.getName());
				objLegalRepresentative.setNationality(legalRepresentativeHistory.getNationality());
				objLegalRepresentative.setOfficePhoneNumber(legalRepresentativeHistory.getOfficePhoneNumber());
				objLegalRepresentative.setPersonType(legalRepresentativeHistory.getPersonType());
				objLegalRepresentative.setPostalAddress(legalRepresentativeHistory.getPostalAddress());
				objLegalRepresentative.setPostalDepartment(legalRepresentativeHistory.getPostalDepartment());
				objLegalRepresentative.setPostalDistrict(legalRepresentativeHistory.getPostalDistrict());
				objLegalRepresentative.setPostalProvince(legalRepresentativeHistory.getPostalProvince());
				objLegalRepresentative.setPostalResidenceCountry(legalRepresentativeHistory.getPostalResidenceCountry());
				objLegalRepresentative.setSecondDocumentNumber(legalRepresentativeHistory.getSecondDocumentNumber());
				objLegalRepresentative.setSecondDocumentType(legalRepresentativeHistory.getSecondDocumentType());
				objLegalRepresentative.setSecondLastName(legalRepresentativeHistory.getSecondLastName());
				objLegalRepresentative.setSecondNationality(legalRepresentativeHistory.getSecondNationality());
				objLegalRepresentative.setSex(legalRepresentativeHistory.getSex());
				objLegalRepresentative.setState(LegalRepresentativeStateType.REGISTERED.getCode());
				objLegalRepresentative.setRegistryUser(loggerUser.getUserName());
    			objLegalRepresentative.setRegistryDate(CommonsUtilities.currentDateTime());
    			
    			if(Validations.validateListIsNotNullAndNotEmpty(legalRepresentativeHistory.getRepresenteFileHistory())){
    				List<LegalRepresentativeFile> lstLegalRepresentativeFile = new ArrayList<LegalRepresentativeFile>();
    				LegalRepresentativeFile legalRepresentativeFile;
					for (RepresentativeFileHistory representativeFileHistory : legalRepresentativeHistory.getRepresenteFileHistory()) {
						legalRepresentativeFile = new LegalRepresentativeFile();
						
						legalRepresentativeFile.setDescription(representativeFileHistory.getDescription());
						legalRepresentativeFile.setDocumentType(representativeFileHistory.getDocumentType());
						legalRepresentativeFile.setFilename(representativeFileHistory.getFilename());	
						legalRepresentativeFile.setFileLegalRepre(representativeFileHistory.getFileRepresentative());					
						legalRepresentativeFile.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
						legalRepresentativeFile.setStateFile(HolderFileStateType.REGISTERED.getCode());
						legalRepresentativeFile.setLegalRepreFileType(HolderReqFileHisType.CREATION.getCode());
						legalRepresentativeFile.setLegalRepresentative(objLegalRepresentative);
						legalRepresentativeFile.setRegistryUser(loggerUser.getUserName());
						legalRepresentativeFile.setRegistryDate(CommonsUtilities.currentDateTime());
						
						lstLegalRepresentativeFile.add(legalRepresentativeFile);
						
					}
					objLegalRepresentative.setLegalRepresentativeFile(lstLegalRepresentativeFile);
				}
    			
    			registryLegalRepresentativeServiceFacade(objLegalRepresentative);
    			
    			if(BooleanType.YES.getCode().equals(legalRepresentativeHistory.getIndPEP())){
    				pepPerson = new PepPerson();
    				pepPerson.setBeginingPeriod(legalRepresentativeHistory.getBeginningPeriod());
    				pepPerson.setCategory(legalRepresentativeHistory.getCategory());
    				pepPerson.setComments(legalRepresentativeHistory.getComments());
    				pepPerson.setEndingPeriod(legalRepresentativeHistory.getEndingPeriod());
    				pepPerson.setBeginingPeriod(legalRepresentativeHistory.getBeginningPeriod());
    				pepPerson.setRole(legalRepresentativeHistory.getRole());
    				pepPerson.setLegalRepresentative(objLegalRepresentative);
    				pepPerson.setRegistryUser(loggerUser.getUserName());
    				pepPerson.setRegistryDate(CommonsUtilities.currentDateTime());
    				pepPerson.setNotificationDate(CommonsUtilities.currentDateTime());
    				countryPep = new GeographicLocation();
    				countryPep.setIdGeographicLocationPk(objLegalRepresentative.getLegalResidenceCountry());		    			
	    		    pepPerson.setGeographicLocation(countryPep);
	    		    pepPerson.setExpeditionPlace(countryPep);
	    		    pepPerson.setDocumentType(legalRepresentativeHistory.getDocumentType());
	    		    pepPerson.setDocumentNumber(legalRepresentativeHistory.getDocumentNumber());
	    		    pepPerson.setClassification(PepPersonClassificationType.HIGH.getCode());
	    			savePepPerson(pepPerson);
    			}
			}
			
			representedEntity = new RepresentedEntity();				
			representedEntity.setStateRepresented(RepresentativeEntityStateType.REGISTERED.getCode());
			representedEntity.setLegalRepresentative(objLegalRepresentative);
			representedEntity.setRepresentativeClass(legalRepresentativeHistory.getRepresentativeClass());
			representedEntities.add(representedEntity);
		}
		return representedEntities;
	}
	/**
	 * Registry issuer on cascade service facade.
	 *
	 * @param issuer the issuer
	 * @param lstLegalRepresentativeHistory the lst legal representative history
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.ISSUER_REGISTER)
	public boolean registryIssuerOnCascadeServiceFacade(Issuer issuer, List<LegalRepresentativeHistory> lstLegalRepresentativeHistory)  throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
	    
	    List<LegalRepresentative> lstLegalRepresentativeFound = loadListLegalRepresentative(lstLegalRepresentativeHistory);

	    GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
	    geographicLocationTO.setIdGeographicLocationPk(issuer.getGeographicLocation().getIdGeographicLocationPk());	    
	    GeographicLocation countryGeographicLocation = geographicLocationServiceBean.findGeographicLocationByIdServiceBean(geographicLocationTO);
	    
	    Holder holder = null;
		HolderRequest holderRequest = null;
	    if(issuer.isIndHolderWillBeCreated()){
	    	//holder=createHolder(issuer, loggerUser,holder,holderRequest);    
	    }
	    else{
	    	holder=issuer.getHolder();
	    }
		List<RepresentedEntity> representedEntities = new ArrayList<RepresentedEntity>();			
		if(!lstLegalRepresentativeHistory.isEmpty())
			representedEntities = loadListRepresentedEntities(representedEntities,lstLegalRepresentativeHistory,lstLegalRepresentativeFound, issuer, loggerUser, holderRequest);			
		
//		if(!representedEntities.isEmpty() && Validations.validateIsNotNullAndNotEmpty(holder) 
//				&& Validations.validateIsNotNullAndNotEmpty(holder.getIdHolderPk()) ){
//			RepresentedEntity objRepresentedEntityHolder;			
//			for (RepresentedEntity objRepresentedEntity : representedEntities) {				
//				objRepresentedEntityHolder = new RepresentedEntity();
//				objRepresentedEntityHolder.setRegistryUser(loggerUser.getUserName());
//				objRepresentedEntityHolder.setRegistryDate(CommonsUtilities.currentDateTime());
//				objRepresentedEntityHolder.setStateRepresented(RepresentativeEntityStateType.REGISTERED.getCode());
//				objRepresentedEntityHolder.setHolder(holder);
//				objRepresentedEntityHolder.setLegalRepresentative(objRepresentedEntity.getLegalRepresentative());
//				objRepresentedEntityHolder.setRepresentativeClass(objRepresentedEntity.getRepresentativeClass());
//				crudDaoServiceBean.create(objRepresentedEntityHolder);
//			}
//		}
		
		issuer.setRegistryUser(loggerUser.getUserName());
		issuer.setRegistryDate(CommonsUtilities.currentDateTime());		
		issuer.setStateIssuer(IssuerStateType.PENDING.getCode()); 
		issuer.setHaveBic(BooleanType.NO.getCode());
		if(issuer.isIndHolderWillBeCreated()){
			issuer.setHolder(holder);
		} else {
			//Validate state of holder
			if(Validations.validateIsNotNullAndNotEmpty(issuer.getHolder()) 
					&& Validations.validateIsNotNullAndNotEmpty(issuer.getHolder().getIdHolderPk()) ){
				Integer holderState = issuerServiceBean.getHolderStateById(issuer.getHolder().getIdHolderPk());
				if(!HolderStateType.BLOCKED.getCode().equals(holderState) &&
					!HolderStateType.REGISTERED.getCode().equals(holderState)){
					StringBuilder sbMessageHolder = new StringBuilder();
					sbMessageHolder.append(DocumentType.get(issuer.getHolder().getDocumentType()).name()).
					append(GeneralConstants.BLANK_SPACE).
					append(issuer.getHolder().getDocumentNumber());
					Object[] arrBodyData = {sbMessageHolder.toString()};
					throw new ServiceException(ErrorServiceType.HOLDER_NOT_REGISTERED_STATE, arrBodyData);
				}
			}
		}
		
		StringBuilder sbIdIssuer = new StringBuilder();
		sbIdIssuer.append(countryGeographicLocation.getCodeGeographicLocation());
		sbIdIssuer.append(getIssuerSequenceCodeServiceFacade());
		
		issuer.setIdIssuerPk(sbIdIssuer.toString());
		
		if(Validations.validateListIsNotNullAndNotEmpty(issuer.getIssuerFiles())){
			for (IssuerFile issuerFile : issuer.getIssuerFiles()) {
				issuerFile.setRegistryUser(loggerUser.getUserName());
		    	issuerFile.setRegistryDate(CommonsUtilities.currentDateTime());
			}
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(crudDaoServiceBean.find(Issuer.class,issuer.getIdIssuerPk())))
			return false;
		
    	registryIssuerServiceFacade(issuer);    	    	
    	
    	if(!representedEntities.isEmpty()){
			RepresentedEntity objRepresentedEntityIssuer;
			for (RepresentedEntity objRepresentedEntity : representedEntities) {
				
				objRepresentedEntityIssuer = new RepresentedEntity();
				objRepresentedEntityIssuer.setRegistryUser(loggerUser.getUserName());
				objRepresentedEntityIssuer.setRegistryDate(CommonsUtilities.currentDateTime());
				objRepresentedEntityIssuer.setStateRepresented(RepresentativeEntityStateType.REGISTERED.getCode());
				objRepresentedEntityIssuer.setIssuer(issuer);				
				objRepresentedEntityIssuer.setLegalRepresentative(objRepresentedEntity.getLegalRepresentative());
				objRepresentedEntityIssuer.setRepresentativeClass(objRepresentedEntity.getRepresentativeClass());
				crudDaoServiceBean.create(objRepresentedEntityIssuer);
			}
		}


    	return true;
    }
	
	/**
	 * Confirm issuer service facade.
	 *
	 * @param issuer the issuer
	 * @param lstLegalRepresentativeHistory the lst legal representative history
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.ISSUER_REGISTER)
	public boolean confirmIssuerServiceFacade(Issuer issuer, List<RepresentedEntity> lstRepresentedEntity)  throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
	    
	    Holder holder = null;
		HolderRequest holderRequest = null;
	    if(issuer.isIndHolderWillBeCreated()){
	    	holder=createHolder(issuer, loggerUser,holder,holderRequest);    
	    }else{
	    	holder=issuer.getHolder();
	    }
		List<RepresentedEntity> representedEntities = new ArrayList<RepresentedEntity>();			
		representedEntities = lstRepresentedEntity;			
		if(!representedEntities.isEmpty() && Validations.validateIsNotNullAndNotEmpty(holder) 
				&& Validations.validateIsNotNullAndNotEmpty(holder.getIdHolderPk()) ){
			RepresentedEntity objRepresentedEntityHolder;			
			for (RepresentedEntity objRepresentedEntity : representedEntities) {				
				objRepresentedEntityHolder = new RepresentedEntity();
				objRepresentedEntityHolder.setRegistryUser(loggerUser.getUserName());
				objRepresentedEntityHolder.setRegistryDate(CommonsUtilities.currentDateTime());
				objRepresentedEntityHolder.setStateRepresented(RepresentativeEntityStateType.REGISTERED.getCode());
				objRepresentedEntityHolder.setHolder(holder);
				objRepresentedEntityHolder.setLegalRepresentative(objRepresentedEntity.getLegalRepresentative());
				objRepresentedEntityHolder.setRepresentativeClass(objRepresentedEntity.getRepresentativeClass());
				crudDaoServiceBean.create(objRepresentedEntityHolder);
			}
		}
		issuer.setLastModifyUser(loggerUser.getUserName());
		issuer.setLastModifyDate(CommonsUtilities.currentDateTime());		
		issuer.setLastModifyIp(loggerUser.getIpAddress());	
		if(!Validations.validateIsNotNullAndNotEmpty(issuer.getLastModifyApp())) {
			issuer.setLastModifyApp(loggerUser.getIdPrivilegeOfSystem());
			if(!Validations.validateIsNotNullAndNotEmpty(issuer.getLastModifyApp())) {
				issuer.setLastModifyApp(0);
			}
		}
		issuer.setStateIssuer(IssuerStateType.REGISTERED.getCode()); 
		if(issuer.isIndHolderWillBeCreated()){
			issuer.setHolder(holder);
		} else {
			//Validate state of holder
			if(Validations.validateIsNotNullAndNotEmpty(issuer.getHolder()) 
					&& Validations.validateIsNotNullAndNotEmpty(issuer.getHolder().getIdHolderPk()) ){
				Integer holderState = issuerServiceBean.getHolderStateById(issuer.getHolder().getIdHolderPk());
				if(!HolderStateType.BLOCKED.getCode().equals(holderState) &&
					!HolderStateType.REGISTERED.getCode().equals(holderState)){
					StringBuilder sbMessageHolder = new StringBuilder();
					sbMessageHolder.append(DocumentType.get(issuer.getHolder().getDocumentType()).name()).
					append(GeneralConstants.BLANK_SPACE).
					append(issuer.getHolder().getDocumentNumber());
					Object[] arrBodyData = {sbMessageHolder.toString()};
					throw new ServiceException(ErrorServiceType.HOLDER_NOT_REGISTERED_STATE, arrBodyData);
				}
			}
		}
		updateIssuerServiceFacade(issuer);  
		
		this.serviceAPIClients.callWsCreateInstitutionSecurity(issuer.getBusinessName(), issuer.getMnemonic(), 
				InstitutionType.ISSUER.getCode(), issuer.getIdIssuerPk(), null);
		return true;
    }

	/**
	 * Modify issuer on cascade service facade.
	 *
	 * @param issuer the issuer
	 * @param lstLegalRepresentativeHistory the lst legal representative history
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.ISSUER_REGISTER)
	public boolean modifyIssuerOnCascadeServiceFacade(Issuer issuer, List<LegalRepresentativeHistory> lstLegalRepresentativeHistory)  throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
	    
	    //Eliminando legalRepresentative
	    disableIssuerFilesStateServiceFacade(issuer,loggerUser);
	    issuerServiceBean.disableRepresentedEntitiesStateServiceBean(issuer,loggerUser);
	  	List<LegalRepresentative> lstLegalRepresentativeAux = issuerServiceBean.getListLegalRepresentativeByIssuerServiceBean(issuer);
	  	for(LegalRepresentative objLegalRepresentative:lstLegalRepresentativeAux) {
	  		issuerServiceBean.disableLegalRepresentativeFilesServiceBean(objLegalRepresentative,loggerUser);
	  		issuerServiceBean.delete(LegalRepresentative.class, objLegalRepresentative.getIdLegalRepresentativePk());
	  	}
	  	
	    List<LegalRepresentative> lstLegalRepresentativeFound = loadListLegalRepresentative(lstLegalRepresentativeHistory);

	    GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
	    geographicLocationTO.setIdGeographicLocationPk(issuer.getGeographicLocation().getIdGeographicLocationPk());	    
	    GeographicLocation countryGeographicLocation = geographicLocationServiceBean.findGeographicLocationByIdServiceBean(geographicLocationTO);
	    
	    Holder holder = null;
		HolderRequest holderRequest = null;
	    if(issuer.isIndHolderWillBeCreated()){
	    	//holder=createHolder(issuer, loggerUser,holder,holderRequest);    
	    }
	    else{
	    	holder=issuer.getHolder();
	    }
		List<RepresentedEntity> representedEntities = new ArrayList<RepresentedEntity>();			
		if(!lstLegalRepresentativeHistory.isEmpty())
			representedEntities = loadListRepresentedEntities(representedEntities,lstLegalRepresentativeHistory,lstLegalRepresentativeFound, issuer, loggerUser, holderRequest);			
		
//		if(!representedEntities.isEmpty() && Validations.validateIsNotNullAndNotEmpty(holder) 
//				&& Validations.validateIsNotNullAndNotEmpty(holder.getIdHolderPk()) ){
//			RepresentedEntity objRepresentedEntityHolder;			
//			for (RepresentedEntity objRepresentedEntity : representedEntities) {				
//				objRepresentedEntityHolder = new RepresentedEntity();
//				objRepresentedEntityHolder.setRegistryUser(loggerUser.getUserName());
//				objRepresentedEntityHolder.setRegistryDate(CommonsUtilities.currentDateTime());
//				objRepresentedEntityHolder.setStateRepresented(RepresentativeEntityStateType.REGISTERED.getCode());
//				objRepresentedEntityHolder.setHolder(holder);
//				objRepresentedEntityHolder.setLegalRepresentative(objRepresentedEntity.getLegalRepresentative());
//				objRepresentedEntityHolder.setRepresentativeClass(objRepresentedEntity.getRepresentativeClass());
//				crudDaoServiceBean.create(objRepresentedEntityHolder);
//			}
//		}
		
		issuer.setLastModifyUser(loggerUser.getUserName());
		issuer.setLastModifyDate(CommonsUtilities.currentDateTime());		
		//issuer.setStateIssuer(IssuerStateType.PENDING.getCode()); 
		//issuer.setHaveBic(BooleanType.NO.getCode());
		if(issuer.isIndHolderWillBeCreated()){
			issuer.setHolder(holder);
		} else {
			//Validate state of holder
			if(Validations.validateIsNotNullAndNotEmpty(issuer.getHolder()) 
					&& Validations.validateIsNotNullAndNotEmpty(issuer.getHolder().getIdHolderPk()) ){
				Integer holderState = issuerServiceBean.getHolderStateById(issuer.getHolder().getIdHolderPk());
				if(!HolderStateType.BLOCKED.getCode().equals(holderState) &&
					!HolderStateType.REGISTERED.getCode().equals(holderState)){
					StringBuilder sbMessageHolder = new StringBuilder();
					sbMessageHolder.append(DocumentType.get(issuer.getHolder().getDocumentType()).name()).
					append(GeneralConstants.BLANK_SPACE).
					append(issuer.getHolder().getDocumentNumber());
					Object[] arrBodyData = {sbMessageHolder.toString()};
					throw new ServiceException(ErrorServiceType.HOLDER_NOT_REGISTERED_STATE, arrBodyData);
				}
			}
		}
		
		/*StringBuilder sbIdIssuer = new StringBuilder();
		sbIdIssuer.append(countryGeographicLocation.getCodeGeographicLocation());
		sbIdIssuer.append(getIssuerSequenceCodeServiceFacade());
		
		issuer.setIdIssuerPk(sbIdIssuer.toString());*/
		
		if(Validations.validateListIsNotNullAndNotEmpty(issuer.getIssuerFiles())){
			for (IssuerFile issuerFile : issuer.getIssuerFiles()) {
				issuerFile.setRegistryUser(loggerUser.getUserName());
		    	issuerFile.setRegistryDate(CommonsUtilities.currentDateTime());
			}
		}
		
		/*if(Validations.validateIsNotNullAndNotEmpty(crudDaoServiceBean.find(Issuer.class,issuer.getIdIssuerPk())))
			return false;*/
		
    	updateIssuerServiceFacade(issuer);    	    	
    	
    	if(!representedEntities.isEmpty()){
			RepresentedEntity objRepresentedEntityIssuer;
			for (RepresentedEntity objRepresentedEntity : representedEntities) {
				
				objRepresentedEntityIssuer = new RepresentedEntity();
				objRepresentedEntityIssuer.setRegistryUser(loggerUser.getUserName());
				objRepresentedEntityIssuer.setRegistryDate(CommonsUtilities.currentDateTime());
				objRepresentedEntityIssuer.setStateRepresented(RepresentativeEntityStateType.REGISTERED.getCode());
				objRepresentedEntityIssuer.setIssuer(issuer);				
				objRepresentedEntityIssuer.setLegalRepresentative(objRepresentedEntity.getLegalRepresentative());
				objRepresentedEntityIssuer.setRepresentativeClass(objRepresentedEntity.getRepresentativeClass());
				crudDaoServiceBean.create(objRepresentedEntityIssuer);
			}
		}


    	return true;
    }
 
	/**
	 * Save pep person.
	 *
	 * @param pepPerson the pep person
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean savePepPerson(PepPerson pepPerson) throws ServiceException {		 
		crudDaoServiceBean.create(pepPerson);
		return true;	 	
	}


	/**
	 * Validate exists issuer request service facade.
	 *
	 * @param issuerRequestTO the issuer request to
	 * @return the issuer request
	 */
	public IssuerRequest validateExistsIssuerRequestServiceFacade(
			IssuerRequestTO issuerRequestTO) {
		return issuerServiceBean.validateExistsIssuerRequestServiceBean(issuerRequestTO);	
	}


	/**
	 * Gets the list issuer files service facade.
	 *
	 * @param issuerFilter the issuer filter
	 * @return the list issuer files service facade
	 * @throws ServiceException the service exception
	 */
	public List<IssuerFile> getListIssuerFilesServiceFacade(Issuer issuerFilter) throws ServiceException{
		return issuerServiceBean.getListIssuerFilesServiceBean(issuerFilter);
	}


	/**
	 * Gets the list represented entity by issuer service facade.
	 *
	 * @param issuerFilter the issuer filter
	 * @return the list represented entity by issuer service facade
	 * @throws ServiceException the service exception
	 */
	public List<RepresentedEntity> getListRepresentedEntityByIssuerServiceFacade(Issuer issuerFilter) throws ServiceException{
		return issuerServiceBean.getListRepresentedEntityByIssuerServiceBean(issuerFilter);
	}
	
	/**
	 * Registry issuer modificacion request service facade.
	 *
	 * @param issuer the issuer
	 * @param lstLegalRepresentativeHistory the lst legal representative history
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
public IssuerRequest registryIssuerModificacionRequestServiceFacade(Issuer issuer, List<LegalRepresentativeHistory> lstLegalRepresentativeHistory) throws ServiceException{
		
		
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
	    if(Validations.validateListIsNotNullAndNotEmpty(lstLegalRepresentativeHistory) 
	    		&& Validations.validateIsNotNullAndPositive(lstLegalRepresentativeHistory.size())) {
	    	for (LegalRepresentativeHistory legalRepresentativeHistory : lstLegalRepresentativeHistory) {
		    	
	    		if(Validations.validateIsNotNullAndPositive(legalRepresentativeHistory.getIdRepresentativeHistoryPk()) &&
	    				Validations.validateListIsNullOrEmpty(legalRepresentativeHistory.getRepresenteFileHistory())){
	    			
	    			LegalRepresentative legalRepresentative = new LegalRepresentative();
					legalRepresentative.setIdLegalRepresentativePk(legalRepresentativeHistory.getIdRepresentativeHistoryPk());
					List<LegalRepresentativeFile> representativeFiles = 
							getListLegalRepresentativeFileServiceFacade(legalRepresentative);
					List<RepresentativeFileHistory> lstRepresentativeFileHistory = new ArrayList<RepresentativeFileHistory>();
					
					RepresentativeFileHistory representativeFileHistory;
					for (LegalRepresentativeFile legalRepresentativeFile : representativeFiles) {
						representativeFileHistory = new RepresentativeFileHistory();					
						representativeFileHistory.setDocumentType(legalRepresentativeFile.getDocumentType());
						representativeFileHistory.setFilename(legalRepresentativeFile.getFilename());
						representativeFileHistory.setDescription(legalRepresentativeFile.getDescription());
						representativeFileHistory.setFileRepresentative(legalRepresentativeFile.getFileLegalRepre());
						lstRepresentativeFileHistory.add(representativeFileHistory);
					}
	    			
					legalRepresentativeHistory.setRepresenteFileHistory(lstRepresentativeFileHistory);
	    		}
		    									
			}
	    }
	    
	    
		IssuerRequest issuerRequest = new IssuerRequest();
		issuerRequest.setIssuer(issuer);
		issuerRequest.setRegistryDate(CommonsUtilities.currentDateTime());
		issuerRequest.setRegistryUser(loggerUser.getUserName());
		issuerRequest.setRequestType(IssuerRequestType.MODIFICATION.getCode());
		issuerRequest.setState(IssuerRequestStateType.REGISTERED.getCode());
			
		Issuer issuerFilter = new Issuer();
		issuerFilter.setIdIssuerPk(issuer.getIdIssuerPk());
		issuerFilter.setStateIssuer(IssuerStateType.REGISTERED.getCode());			
		
		List<IssuerFileHistory> issuerRequestFiles = new ArrayList<IssuerFileHistory>();
		
		if(Validations.validateListIsNotNullAndNotEmpty(issuer.getIssuerFiles())){
			
			List<IssuerFile> lstIssuerFilesOrigin = getListIssuerFilesServiceFacade(issuerFilter);
			if(Validations.validateListIsNotNullAndNotEmpty(lstIssuerFilesOrigin))
			{
				IssuerFileHistory issuerFileHistoryRequest;
				for (IssuerFile issuerFile : lstIssuerFilesOrigin) {
					issuerFileHistoryRequest = new IssuerFileHistory();
					
					issuerFileHistoryRequest.setDocumentType(issuerFile.getDocumentType());				
					issuerFileHistoryRequest.setDescription(issuerFile.getDescription());		    	
					issuerFileHistoryRequest.setRequestFileType(issuerFile.getRequestFileType());
					issuerFileHistoryRequest.setFilename(issuerFile.getFilename());
					issuerFileHistoryRequest.setDocumentFile(issuerFile.getDocumentFile());

					issuerFileHistoryRequest.setRegistryUser(loggerUser.getUserName());
					issuerFileHistoryRequest.setRegistryDate(CommonsUtilities.currentDateTime());
					
					issuerFileHistoryRequest.setRequestFileType(RequestParticipantFileType.REGISTER.getCode());		    	
					issuerFileHistoryRequest.setIssuerRequest(issuerRequest);
			    	issuerFileHistoryRequest.setIssuer(issuer);
			    	issuerFileHistoryRequest.setStateFile(IssuerFileStateType.REGISTERED.getCode());
			    	
			    	issuerFileHistoryRequest.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
			    	
			    	issuerRequestFiles.add(issuerFileHistoryRequest);
				}			
			}
			
			IssuerFileHistory issuerFileHistoryRequest;
			for (IssuerFile issuerFile : issuer.getIssuerFiles()) {
				issuerFileHistoryRequest = new IssuerFileHistory();
				
				issuerFileHistoryRequest.setDocumentType(issuerFile.getDocumentType());				
				issuerFileHistoryRequest.setDescription(issuerFile.getDescription());		    	
				issuerFileHistoryRequest.setRequestFileType(issuerFile.getRequestFileType());
				issuerFileHistoryRequest.setFilename(issuerFile.getFilename());
				issuerFileHistoryRequest.setDocumentFile(issuerFile.getDocumentFile());

				issuerFileHistoryRequest.setRegistryUser(loggerUser.getUserName());
				issuerFileHistoryRequest.setRegistryDate(CommonsUtilities.currentDateTime());
				
				issuerFileHistoryRequest.setRequestFileType(RequestParticipantFileType.REGISTER.getCode());		    	
				issuerFileHistoryRequest.setIssuerRequest(issuerRequest);
		    	issuerFileHistoryRequest.setIssuer(issuer);
		    	issuerFileHistoryRequest.setStateFile(IssuerFileStateType.REGISTERED.getCode());
		    	
		    	issuerFileHistoryRequest.setRegistryType(RegistryType.HISTORICAL_COPY.getCode());
		    	
		    	issuerRequestFiles.add(issuerFileHistoryRequest);
			}	
			
			issuerRequest.setIssuerFileHistories(issuerRequestFiles);
			issuerRequest.setIssuerFiles(null);
		}
		
		issuerRequest.setRequestNumber(crudDaoServiceBean.getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_MODIFICATION_ISSUER));
		registryIssuerRequestServiceFacade(issuerRequest);								
		
		Issuer issuerFilterAux = new Issuer();
		issuerFilterAux.setIdIssuerPk(issuer.getIdIssuerPk());
		issuerFilterAux.setStateIssuer(IssuerStateType.REGISTERED.getCode());	
		Issuer issuerResult = findIssuerByFiltersServiceFacade(issuerFilterAux);
		
		IssuerHistory issuerHistoryAux = new IssuerHistory();
		issuerHistoryAux.setIssuerRequest(issuerRequest);
		issuerHistoryAux.setMnemonic(issuerResult.getMnemonic());
		issuerHistoryAux.setBusinessName(issuerResult.getBusinessName());
		issuerHistoryAux.setDocumentType(issuerResult.getDocumentType());
		issuerHistoryAux.setDocumentNumber(issuerResult.getDocumentNumber());
		issuerHistoryAux.setDocumentSource(issuerResult.getDocumentSource());
		issuerHistoryAux.setOfferType(issuerResult.getOfferType());
		issuerHistoryAux.setRelatedCui(issuerResult.getRelatedCui());
		issuerHistoryAux.setFundAdministrator(issuerResult.getFundAdministrator());
		issuerHistoryAux.setFundType(issuerResult.getFundType());
		issuerHistoryAux.setTransferNumber(issuerResult.getTransferNumber());
		issuerHistoryAux.setSocietyType(issuerResult.getSocietyType());
		issuerHistoryAux.setContractNumber(issuerResult.getContractNumber());
		issuerHistoryAux.setContractDate(issuerResult.getContractDate());
		issuerHistoryAux.setSuperintendentResolution(issuerResult.getSuperintendentResolution());
		issuerHistoryAux.setSocialCapital(issuerResult.getSocialCapital());			
		
		issuerHistoryAux.setGeographicLocation(issuerResult.getGeographicLocation());
		issuerHistoryAux.setDepartment(issuerResult.getDepartment());
		issuerHistoryAux.setProvince(issuerResult.getProvince());
		issuerHistoryAux.setDistrict(issuerResult.getDistrict());
		issuerHistoryAux.setLegalAddress(issuerResult.getLegalAddress());
		issuerHistoryAux.setPhoneNumber(issuerResult.getPhoneNumber());
		issuerHistoryAux.setFaxNumber(issuerResult.getFaxNumber());
		issuerHistoryAux.setWebsite(issuerResult.getWebsite());
		issuerHistoryAux.setEmail(issuerResult.getEmail());
		issuerHistoryAux.setContactPhone(issuerResult.getContactPhone());
		issuerHistoryAux.setContactName(issuerResult.getContactName());
		issuerHistoryAux.setObservation(issuerResult.getObservation());
		issuerHistoryAux.setStateIssuer(issuerResult.getStateIssuer());
		issuerHistoryAux.setCreationDate(issuerResult.getCreationDate());		
		issuerHistoryAux.setEconomicSector(issuerResult.getEconomicSector());		
		issuerHistoryAux.setEconomicActivity(issuerResult.getEconomicActivity());
		issuerHistoryAux.setContactPhone(issuerResult.getContactPhone());
		issuerHistoryAux.setCurrency(issuerResult.getCurrency());
		issuerHistoryAux.setContactEmail(issuerResult.getContactEmail());
		issuerHistoryAux.setRegistryUser(issuerResult.getRegistryUser());
		issuerHistoryAux.setIndStockExchEnrolled(issuerResult.getIndStockExchEnrolled());
		issuerHistoryAux.setIndContractDepository(issuerResult.getIndContractDepository());
		issuerHistoryAux.setRetirementDate(issuerResult.getRetirementDate());
		issuerHistoryAux.setClosingServiceDate(issuerResult.getClosingServiceDate());
		issuerHistoryAux.setEnrolledStockExchDate(issuerResult.getEnrolledStockExchDate());
		issuerHistoryAux.setSuperResolutionDate(issuerResult.getSuperResolutionDate());
		issuerHistoryAux.setIndDifferentiated(issuerResult.getIndDifferentiated());	
		issuerHistoryAux.setCreditRatingScales(issuerResult.getCreditRatingScales());
		issuerHistoryAux.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
		issuerHistoryAux.setRmvRegister(issuerResult.getRmvRegister()); //issue 708
		issuerHistoryAux.setConstitutionDate(issuerResult.getConstitutionDate());
		/*Datos de modificacion de registro original*/
		issuerHistoryAux.setIndSirtexNegotiate(issuerResult.getIndSirtexNegotiate());
		issuerHistoryAux.setSirtexNegotiateDate(issuerResult.getSirtexNegotiateDate());
		issuerHistoryAux.setInvestorType(issuerResult.getInvestorType());
		
		//issuerHistoryAux.setSecurityClassSirtexNegotiate(issuerResult.getSecurityClassSirtexNegotiate());
		issuerHistoryAux.setCurrencySirtexNegotiate(issuerResult.getCurrencySirtexNegotiate());
		issuerHistoryAux.setPersonTypeSirtexNegotiate(issuerResult.getPersonTypeSirtexNegotiate());
		issuerHistoryAux.setWithCouponsSirtexNegotiate(issuerResult.getWithCouponsSirtexNegotiate());
		issuerHistoryAux.setIssuanceDateSirtexNegotiate(issuerResult.getIssuanceDateSirtexNegotiate());
		
		registryIssuerHistoryServiceFacade(issuerHistoryAux);				
		/*Datos copia*/
		IssuerHistory issuerHistory = new IssuerHistory();
		issuerHistory.setIssuerRequest(issuerRequest);
		issuerHistory.setMnemonic(issuerRequest.getIssuer().getMnemonic());
		issuerHistory.setBusinessName(issuerRequest.getIssuer().getBusinessName());
		issuerHistory.setDocumentType(issuerRequest.getIssuer().getDocumentType());
		issuerHistory.setDocumentNumber(issuerRequest.getIssuer().getDocumentNumber());
		issuerHistory.setDocumentSource(issuerResult.getDocumentSource());
		issuerHistory.setOfferType(issuerRequest.getIssuer().getOfferType());
		issuerHistory.setRelatedCui(issuerResult.getRelatedCui());
		issuerHistory.setFundAdministrator(issuerResult.getFundAdministrator());
		issuerHistory.setFundType(issuerResult.getFundType());
		issuerHistory.setTransferNumber(issuerResult.getTransferNumber());
		issuerHistory.setSocietyType(issuerRequest.getIssuer().getSocietyType());
		issuerHistory.setContractNumber(issuerRequest.getIssuer().getContractNumber());
		issuerHistory.setContractDate(issuerRequest.getIssuer().getContractDate());
		issuerHistory.setSuperintendentResolution(issuerRequest.getIssuer().getSuperintendentResolution());
		issuerHistory.setSocialCapital(issuerRequest.getIssuer().getSocialCapital());			
				    
		GeographicLocation geographicLocation = new GeographicLocation();
		geographicLocation.setIdGeographicLocationPk(issuerRequest.getIssuer().getGeographicLocation().getIdGeographicLocationPk());
		issuerHistory.setGeographicLocation(geographicLocation);
		issuerHistory.setDepartment(issuerRequest.getIssuer().getDepartment());
		issuerHistory.setProvince(issuerRequest.getIssuer().getProvince());
		issuerHistory.setDistrict(issuerRequest.getIssuer().getDistrict());
		issuerHistory.setLegalAddress(issuerRequest.getIssuer().getLegalAddress());
		issuerHistory.setPhoneNumber(issuerRequest.getIssuer().getPhoneNumber());
		issuerHistory.setFaxNumber(issuerRequest.getIssuer().getFaxNumber());
		issuerHistory.setWebsite(issuerRequest.getIssuer().getWebsite());
		issuerHistory.setEmail(issuerRequest.getIssuer().getEmail());
		issuerHistory.setContactPhone(issuerRequest.getIssuer().getContactPhone());
		issuerHistory.setContactName(issuerRequest.getIssuer().getContactName());
		issuerHistory.setObservation(issuerRequest.getIssuer().getObservation());
		issuerHistory.setStateIssuer(issuerRequest.getIssuer().getStateIssuer());
		issuerHistory.setCreationDate(issuerRequest.getIssuer().getCreationDate());		
		issuerHistory.setEconomicSector(issuerRequest.getIssuer().getEconomicSector());		
		issuerHistory.setEconomicActivity(issuerRequest.getIssuer().getEconomicActivity());
		issuerHistory.setInvestorType(issuerRequest.getIssuer().getInvestorType());
		issuerHistory.setContactPhone(issuerRequest.getIssuer().getContactPhone());
		issuerHistory.setCurrency(issuerRequest.getIssuer().getCurrency());
		issuerHistory.setContactEmail(issuerRequest.getIssuer().getContactEmail());
		issuerHistory.setRegistryUser(loggerUser.getUserName());
		issuerHistory.setIndStockExchEnrolled(issuerRequest.getIssuer().getIndStockExchEnrolled());
		issuerHistory.setIndContractDepository(issuerRequest.getIssuer().getIndContractDepository());
		issuerHistory.setRetirementDate(issuerRequest.getIssuer().getRetirementDate());
		issuerHistory.setClosingServiceDate(issuerRequest.getIssuer().getClosingServiceDate());
		issuerHistory.setEnrolledStockExchDate(issuerRequest.getIssuer().getEnrolledStockExchDate());
		issuerHistory.setSuperResolutionDate(issuerRequest.getIssuer().getSuperResolutionDate());
		issuerHistory.setIndDifferentiated(issuerRequest.getIssuer().getIndDifferentiated());	
		issuerHistory.setCreditRatingScales(issuerRequest.getIssuer().getCreditRatingScales());
		issuerHistory.setRegistryType(RegistryType.HISTORICAL_COPY.getCode());
		issuerHistory.setRmvRegister(issuerRequest.getIssuer().getRmvRegister());  //issue 708
		issuerHistory.setConstitutionDate(issuerRequest.getIssuer().getConstitutionDate());
		/*Datos hotoricos*/
		issuerHistory.setIndSirtexNegotiate(issuer.getIndSirtexNegotiate());
		issuerHistory.setSirtexNegotiateDate(issuerRequest.getIssuer().getSirtexNegotiateDate());
		
		//issuerHistory.setSecurityClassSirtexNegotiate(issuer.getSecurityClassSirtexNegotiate());
		issuerHistory.setCurrencySirtexNegotiate(issuer.getCurrencySirtexNegotiate());
		issuerHistory.setPersonTypeSirtexNegotiate(issuer.getPersonTypeSirtexNegotiate());
		issuerHistory.setWithCouponsSirtexNegotiate(issuer.getWithCouponsSirtexNegotiate());
		issuerHistory.setIssuanceDateSirtexNegotiate(issuer.getIssuanceDateSirtexNegotiate());
		
		
		
		registryIssuerHistoryServiceFacade(issuerHistory);				
		
		if((Validations.validateListIsNotNullAndNotEmpty(lstLegalRepresentativeHistory))){
			
			List<RepresentedEntity> lstRepresentedEntityOrigin = getListRepresentedEntityByIssuerServiceFacade(issuerFilter);
			if((Validations.validateListIsNotNullAndNotEmpty(lstRepresentedEntityOrigin)))
			{
				List<LegalRepresentativeHistory> lstLegalRepresentativeOrigin = getLegalRepresentativeHistoryOrigin(lstRepresentedEntityOrigin);
				
				if((Validations.validateListIsNotNullAndNotEmpty(lstLegalRepresentativeOrigin))){
					for (LegalRepresentativeHistory legalRepresentativeOrigin : lstLegalRepresentativeOrigin) {
						legalRepresentativeOrigin.setIdRepresentativeHistoryPk(null);
						legalRepresentativeOrigin.setRegistryUser(loggerUser.getUserName());
						legalRepresentativeOrigin.setStateRepreHistory(LegalRepresentativeStateType.REGISTERED.getCode());
						legalRepresentativeOrigin.setRepreHistoryType(HolderRequestType.MODIFICATION.getCode());
						legalRepresentativeOrigin.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
						legalRepresentativeOrigin.setIssuerRequest(issuerRequest);
						
						if(Validations.validateListIsNotNullAndNotEmpty(legalRepresentativeOrigin.getRepresenteFileHistory())){
							for (RepresentativeFileHistory representativeFileHistory : legalRepresentativeOrigin.getRepresenteFileHistory()) {
								representativeFileHistory.setIdRepresentFileHisPk(null);
								representativeFileHistory.setRepresentFileHistType(HolderReqFileHisType.CREATION.getCode());
								representativeFileHistory.setRegistryUser(loggerUser.getUserName());
								representativeFileHistory.setRegistryDate(CommonsUtilities.currentDate());
								representativeFileHistory.setLegalRepresentativeHistory(legalRepresentativeOrigin);
								representativeFileHistory.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
								representativeFileHistory.setStateFile(HolderFileStateType.REGISTERED.getCode());
							}
						}
						
						registryLegalRepresentativeHistoryServiceFacade(legalRepresentativeOrigin);    			
					}
				}
			}		
			
			for (LegalRepresentativeHistory legalRepresentativeHistory : lstLegalRepresentativeHistory) {
				legalRepresentativeHistory.setIdRepresentativeHistoryPk(null);
				legalRepresentativeHistory.setRegistryUser(loggerUser.getUserName());
				legalRepresentativeHistory.setStateRepreHistory(LegalRepresentativeStateType.REGISTERED.getCode());
				legalRepresentativeHistory.setRepreHistoryType(HolderRequestType.MODIFICATION.getCode());
				legalRepresentativeHistory.setRegistryType(RegistryType.HISTORICAL_COPY.getCode());
				legalRepresentativeHistory.setIssuerRequest(issuerRequest);
				
				if(Validations.validateListIsNotNullAndNotEmpty(legalRepresentativeHistory.getRepresenteFileHistory())){
					for (RepresentativeFileHistory representativeFileHistory : legalRepresentativeHistory.getRepresenteFileHistory()) {
						representativeFileHistory.setIdRepresentFileHisPk(null);
						representativeFileHistory.setRepresentFileHistType(HolderReqFileHisType.CREATION.getCode());
						representativeFileHistory.setRegistryUser(loggerUser.getUserName());
						representativeFileHistory.setRegistryDate(CommonsUtilities.currentDate());
						representativeFileHistory.setLegalRepresentativeHistory(legalRepresentativeHistory);
						representativeFileHistory.setRegistryType(RegistryType.HISTORICAL_COPY.getCode());
						representativeFileHistory.setStateFile(HolderFileStateType.REGISTERED.getCode());
					}
				}
				
				registryLegalRepresentativeHistoryServiceFacade(legalRepresentativeHistory);    			
			}
		}
		return issuerRequest;		
	}
	
	/**
	 * Set Validate Legal Representative History.
	 *
	 * @param lstRepresentedEntity List Represented Entity
	 * @return the legal representative history origin
	 * @throws ServiceException  Service Exception
	 */
	public List<LegalRepresentativeHistory>  getLegalRepresentativeHistoryOrigin(List<RepresentedEntity> lstRepresentedEntity) throws ServiceException
	{
		List<LegalRepresentativeHistory> lstLegalRepresentativeHistory = new ArrayList<LegalRepresentativeHistory>();
		List<RepresentativeFileHistory> lstRepresentativeFileHistory=null;
		LegalRepresentativeHistory objLegalRepresentativeHistory;
		for (RepresentedEntity objRepresentedEntity : lstRepresentedEntity) {
			objLegalRepresentativeHistory = new LegalRepresentativeHistory();
			objLegalRepresentativeHistory.setIdRepresentativeHistoryPk(objRepresentedEntity.getLegalRepresentative().getIdLegalRepresentativePk());
			objLegalRepresentativeHistory.setRepresentativeClass(objRepresentedEntity.getRepresentativeClass());
			objLegalRepresentativeHistory.setBirthDate(objRepresentedEntity.getLegalRepresentative().getBirthDate());
			objLegalRepresentativeHistory.setDocumentNumber(objRepresentedEntity.getLegalRepresentative().getDocumentNumber());
			objLegalRepresentativeHistory.setDocumentSource(objRepresentedEntity.getLegalRepresentative().getDocumentSource());
			objLegalRepresentativeHistory.setDocumentIssuanceDate(objRepresentedEntity.getLegalRepresentative().getDocumentIssuanceDate());
			objLegalRepresentativeHistory.setDocumentType(objRepresentedEntity.getLegalRepresentative().getDocumentType());
			objLegalRepresentativeHistory.setEconomicActivity(objRepresentedEntity.getLegalRepresentative().getEconomicActivity());
			objLegalRepresentativeHistory.setEconomicSector(objRepresentedEntity.getLegalRepresentative().getEconomicSector());
			objLegalRepresentativeHistory.setEmail(objRepresentedEntity.getLegalRepresentative().getEmail());
			objLegalRepresentativeHistory.setFaxNumber(objRepresentedEntity.getLegalRepresentative().getFaxNumber());
			objLegalRepresentativeHistory.setFirstLastName(objRepresentedEntity.getLegalRepresentative().getFirstLastName());
			objLegalRepresentativeHistory.setFullName(objRepresentedEntity.getLegalRepresentative().getFullName());
			objLegalRepresentativeHistory.setHomePhoneNumber(objRepresentedEntity.getLegalRepresentative().getHomePhoneNumber());
			objLegalRepresentativeHistory.setIndResidence(objRepresentedEntity.getLegalRepresentative().getIndResidence());
			objLegalRepresentativeHistory.setLegalAddress(objRepresentedEntity.getLegalRepresentative().getLegalAddress());
			objLegalRepresentativeHistory.setLegalDistrict(objRepresentedEntity.getLegalRepresentative().getLegalDistrict());
			objLegalRepresentativeHistory.setLegalProvince(objRepresentedEntity.getLegalRepresentative().getLegalProvince());
			objLegalRepresentativeHistory.setLegalDepartment(objRepresentedEntity.getLegalRepresentative().getLegalDepartment());
			objLegalRepresentativeHistory.setLegalResidenceCountry(objRepresentedEntity.getLegalRepresentative().getLegalResidenceCountry());
			objLegalRepresentativeHistory.setMobileNumber(objRepresentedEntity.getLegalRepresentative().getMobileNumber());
			objLegalRepresentativeHistory.setName(objRepresentedEntity.getLegalRepresentative().getName());
			objLegalRepresentativeHistory.setNationality(objRepresentedEntity.getLegalRepresentative().getNationality());
			objLegalRepresentativeHistory.setOfficePhoneNumber(objRepresentedEntity.getLegalRepresentative().getOfficePhoneNumber());
			objLegalRepresentativeHistory.setPersonType(objRepresentedEntity.getLegalRepresentative().getPersonType());
			objLegalRepresentativeHistory.setPostalAddress(objRepresentedEntity.getLegalRepresentative().getPostalAddress());
			objLegalRepresentativeHistory.setPostalDepartment(objRepresentedEntity.getLegalRepresentative().getPostalDepartment());
			objLegalRepresentativeHistory.setPostalDistrict(objRepresentedEntity.getLegalRepresentative().getPostalDistrict());
			objLegalRepresentativeHistory.setPostalProvince(objRepresentedEntity.getLegalRepresentative().getPostalProvince());
			objLegalRepresentativeHistory.setPostalResidenceCountry(objRepresentedEntity.getLegalRepresentative().getPostalResidenceCountry());
			objLegalRepresentativeHistory.setSecondDocumentNumber(objRepresentedEntity.getLegalRepresentative().getSecondDocumentNumber());
			objLegalRepresentativeHistory.setSecondDocumentType(objRepresentedEntity.getLegalRepresentative().getSecondDocumentType());
			objLegalRepresentativeHistory.setSecondLastName(objRepresentedEntity.getLegalRepresentative().getSecondLastName());
			objLegalRepresentativeHistory.setSecondNationality(objRepresentedEntity.getLegalRepresentative().getSecondNationality());
			objLegalRepresentativeHistory.setSex(objRepresentedEntity.getLegalRepresentative().getSex());
			objLegalRepresentativeHistory.setStateRepreHistory(LegalRepresentativeStateType.REGISTERED.getCode());
			
			PepPerson pepPersonRepresentative =findPepPersonByLegalRepresentativeServiceFacade(objRepresentedEntity.getLegalRepresentative());
			if(Validations.validateIsNotNull(pepPersonRepresentative)) {
				objLegalRepresentativeHistory.setIndPEP(BooleanType.YES.getCode());
				objLegalRepresentativeHistory.setCategory(pepPersonRepresentative.getCategory());
				objLegalRepresentativeHistory.setRole(pepPersonRepresentative.getRole());
				objLegalRepresentativeHistory.setBeginningPeriod(pepPersonRepresentative.getBeginingPeriod());
				objLegalRepresentativeHistory.setEndingPeriod(pepPersonRepresentative.getEndingPeriod());
			} else {
				objLegalRepresentativeHistory.setIndPEP(BooleanType.NO.getCode());
			}
			
			if(Validations.validateIsNotNullAndPositive(objLegalRepresentativeHistory.getIdRepresentativeHistoryPk())
					&& Validations.validateListIsNullOrEmpty(objLegalRepresentativeHistory.getRepresenteFileHistory())){
				LegalRepresentative legalRepresentative = new LegalRepresentative();
				legalRepresentative.setIdLegalRepresentativePk(objLegalRepresentativeHistory.getIdRepresentativeHistoryPk());
				List<LegalRepresentativeFile> representativeFiles = getListLegalRepresentativeFileServiceFacade(legalRepresentative);
				lstRepresentativeFileHistory = new ArrayList<RepresentativeFileHistory>();
				
				RepresentativeFileHistory representativeFileHistory;
				for (LegalRepresentativeFile legalRepresentativeFile : representativeFiles) {
					representativeFileHistory = new RepresentativeFileHistory();
					representativeFileHistory.setDocumentType(legalRepresentativeFile.getDocumentType());
					representativeFileHistory.setFilename(legalRepresentativeFile.getFilename());
					representativeFileHistory.setDescription(legalRepresentativeFile.getDescription());
					representativeFileHistory.setFileRepresentative(legalRepresentativeFile.getFileLegalRepre());
					representativeFileHistory.setLegalRepresentativeHistory(objLegalRepresentativeHistory);
					lstRepresentativeFileHistory.add(representativeFileHistory);
				}
				
				objLegalRepresentativeHistory.setRepresenteFileHistory(lstRepresentativeFileHistory);
				
			} else {
				lstRepresentativeFileHistory = objLegalRepresentativeHistory.getRepresenteFileHistory();
			}		
			
			LegalRepresentativeTO legalRepresentativeTO = new LegalRepresentativeTO();
			legalRepresentativeTO.setViewOperationType(ViewOperationsType.DETAIL.getCode());			
			lstLegalRepresentativeHistory.add(objLegalRepresentativeHistory);
		}
		return lstLegalRepresentativeHistory;
	}
	/**
	 * Registry issuer request service facade.
	 *
	 * @param issuerRequest the issuer request
	 * @return the issuer request
	 * @throws ServiceException the service exception
	 */
	public IssuerRequest registryIssuerRequestServiceFacade(IssuerRequest issuerRequest) throws ServiceException {
		return crudDaoServiceBean.create(issuerRequest);
	}
	
	/**
	 * Update issuer request service facade.
	 *
	 * @param issuerRequest the issuer request
	 * @return the issuer request
	 * @throws ServiceException the service exception
	 */
	public IssuerRequest updateIssuerRequestServiceFacade(IssuerRequest issuerRequest) throws ServiceException {
		return crudDaoServiceBean.update(issuerRequest);
	}
	
	/**
	 * Registry issuer history service facade.
	 *
	 * @param issuerHistory the issuer history
	 * @return the issuer history
	 * @throws ServiceException the service exception
	 */
	public IssuerHistory registryIssuerHistoryServiceFacade(IssuerHistory issuerHistory) throws ServiceException{
		return crudDaoServiceBean.create(issuerHistory);
	}
	
	/**
	 * Find issuer modification request by id service facade.
	 *
	 * @param issuerRequestTO the issuer request to
	 * @return the issuer request
	 * @throws ServiceException the service exception
	 */
	public IssuerRequest findIssuerModificationRequestByIdServiceFacade(IssuerRequestTO issuerRequestTO) 
			throws ServiceException {
		return issuerServiceBean.findIssuerModificationRequestByIdServiceBean(issuerRequestTO);
	}

	/**Busqueda de modificaion de emisor*/
	public IssuerHistory findIssuerHistory(Long issuerRequest,Integer registryType){
		return issuerServiceBean.findIssuerHistory(issuerRequest, registryType);
	}

	/**
	 * Gets the list issuer file histories service facade.
	 *
	 * @param issuerRequest the issuer request
	 * @return the list issuer file histories service facade
	 * @throws ServiceException the service exception
	 */
	public List<IssuerFileHistory> getListIssuerFileHistoriesServiceFacade(
			IssuerRequest issuerRequest) throws ServiceException{
		return issuerServiceBean.getListIssuerFileHistoriesServiceBean(issuerRequest);
	}
	
	/**
	 * Gets the list legal representative hist by issuer request service facade.
	 *
	 * @param issuerRequest the issuer request
	 * @return the list legal representative hist by issuer request service facade
	 * @throws ServiceException the service exception
	 */
	public List<LegalRepresentativeHistory> getListLegalRepresentativeHistByIssuerRequestServiceFacade(IssuerRequest issuerRequest) throws ServiceException{
		return issuerServiceBean.getListLegalRepresentativeHistByIssuerRequestServiceBean(issuerRequest);
	}


	/**
	 * Find issuer request by id service facade.
	 *
	 * @param idIssuerRequestPk the id issuer request pk
	 * @return the issuer request
	 * @throws ServiceException the service exception
	 */
	public IssuerRequest findIssuerRequestByIdServiceFacade(Long idIssuerRequestPk) throws ServiceException{
		return issuerServiceBean.findIssuerRequestByIdServiceBean(idIssuerRequestPk);
	}


	/**
	 * Reject issuer modification request service facade.
	 *
	 * @param issuerRequest the issuer request
	 * @return the issuer request
	 * @throws ServiceException the service exception
	 */
	public IssuerRequest rejectIssuerModificationRequestServiceFacade(IssuerRequest issuerRequest) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		return crudDaoServiceBean.update(issuerRequest);
	}


	/**
	 * Confirm issuer modification request service facade.
	 *
	 * @param issuerRequest the issuer request
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean confirmIssuerModificationRequestServiceFacade(IssuerRequest issuerRequest) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
        
		IssuerRequestTO issuerRequestTO = new IssuerRequestTO();
		issuerRequestTO.setIdIssuerRequestPk(issuerRequest.getIdIssuerRequestPk());
		
		issuerRequest = findIssuerModificationRequestByIdServiceFacade(issuerRequestTO);					
		
		
		Issuer issuer = issuerRequest.getIssuer();
				
		List<LegalRepresentativeHistory> lstLegalRepresentativeHistories = getListLegalRepresentativeHistByIssuerRequestServiceFacade(issuerRequest);
		if(Validations.validateListIsNotNullAndNotEmpty(lstLegalRepresentativeHistories)){
			for (LegalRepresentativeHistory legalRepresentativeHistory : lstLegalRepresentativeHistories) {
				List<RepresentativeFileHistory> representativeFileHistories = getListLegalRepresentativeFileHistServiceFacade(legalRepresentativeHistory);
				if(Validations.validateListIsNotNullAndNotEmpty(representativeFileHistories)){
					legalRepresentativeHistory.setRepresenteFileHistory(representativeFileHistories);
				}
			}
		}
		
		List<LegalRepresentative> lstLegalRepresentativeFound = new ArrayList<LegalRepresentative>();
        LegalRepresentative legalRepresentativeFound;
        
        if(Validations.validateListIsNotNullAndNotEmpty(lstLegalRepresentativeHistories)){
	        for (LegalRepresentativeHistory legalRepresentativeHistory : lstLegalRepresentativeHistories) {
	        		legalRepresentativeFound = new LegalRepresentative();
	            	legalRepresentativeFound.setDocumentType(legalRepresentativeHistory.getDocumentType());
	            	legalRepresentativeFound.setDocumentNumber(legalRepresentativeHistory.getDocumentNumber());
	            	legalRepresentativeFound.setDocumentSource(legalRepresentativeHistory.getDocumentSource());
	            	
	            	legalRepresentativeFound = getLegalRepresentativeByDocumentTypeAndDocumentNumberServiceFacade(legalRepresentativeFound);
	            	
	            	if(Validations.validateIsNotNull(legalRepresentativeFound)){
	            		lstLegalRepresentativeFound.add(legalRepresentativeFound);
	            	}	        	
			}
        }
		
        Issuer issuerOrigin = new Issuer();
        
        for(int i=0;i<issuerRequest.getIssuerHistories().size();i++)
        {
        	IssuerHistory issuerHistory = issuerRequest.getIssuerHistories().get(i);
        	if(issuerHistory.getRegistryType().equals(RegistryType.ORIGIN_OF_THE_REQUEST.getCode()))
        	{
        		issuerOrigin.setMnemonic(issuerHistory.getMnemonic());
        		issuerOrigin.setBusinessName(issuerHistory.getBusinessName());
        		issuerOrigin.setDocumentType(issuerHistory.getDocumentType());
        		issuerOrigin.setDocumentNumber(issuerHistory.getDocumentNumber());
        		issuerOrigin.setDocumentSource(issuerHistory.getDocumentSource());
        		issuerOrigin.setOfferType(issuerHistory.getOfferType());
        		issuerOrigin.setRelatedCui(issuerHistory.getRelatedCui());
        		issuerOrigin.setFundAdministrator(issuerHistory.getFundAdministrator());
        		issuerOrigin.setFundType(issuerHistory.getFundType());
        		issuerOrigin.setTransferNumber(issuerHistory.getTransferNumber());
        		issuerOrigin.setSocietyType(issuerHistory.getSocietyType());
        		issuerOrigin.setContractNumber(issuerHistory.getContractNumber());
        		issuerOrigin.setContractDate(issuerHistory.getContractDate());
        		issuerOrigin.setSuperintendentResolution(issuerHistory.getSuperintendentResolution());
        		issuerOrigin.setSocialCapital(issuerHistory.getSocialCapital());
        		issuerOrigin.setGeographicLocation(issuerHistory.getGeographicLocation());
        		issuerOrigin.setDepartment(issuerHistory.getDepartment());
        		issuerOrigin.setProvince(issuerHistory.getProvince());
        		issuerOrigin.setDistrict(issuerHistory.getDistrict());
        		issuerOrigin.setLegalAddress(issuerHistory.getLegalAddress());
        		issuerOrigin.setPhoneNumber(issuerHistory.getPhoneNumber());
        		issuerOrigin.setFaxNumber(issuerHistory.getFaxNumber());
        		issuerOrigin.setWebsite(issuerHistory.getWebsite());
        		issuerOrigin.setEmail(issuerHistory.getEmail());
        		issuerOrigin.setContactPhone(issuerHistory.getContactPhone());
        		issuerOrigin.setContactName(issuerHistory.getContactName());
        		issuerOrigin.setObservation(issuerHistory.getObservation());
        		issuerOrigin.setStateIssuer(issuerHistory.getStateIssuer());
        		issuerOrigin.setCreationDate(issuerHistory.getCreationDate());		
        		issuerOrigin.setEconomicSector(issuerHistory.getEconomicSector());		
        		issuerOrigin.setEconomicActivity(issuerHistory.getEconomicActivity());
        		issuerOrigin.setInvestorType(issuerHistory.getInvestorType());
        		issuerOrigin.setContactPhone(issuerHistory.getContactPhone());
        		issuerOrigin.setCurrency(issuerHistory.getCurrency());
        		issuerOrigin.setContactEmail(issuerHistory.getContactEmail());
        		issuerOrigin.setRegistryUser(loggerUser.getUserName());
        		issuerOrigin.setIndStockExchEnrolled(issuerHistory.getIndStockExchEnrolled());
        		issuerOrigin.setIndContractDepository(issuerHistory.getIndContractDepository());
        		issuerOrigin.setRetirementDate(issuerHistory.getRetirementDate());
        		issuerOrigin.setClosingServiceDate(issuerHistory.getClosingServiceDate());
        		issuerOrigin.setEnrolledStockExchDate(issuerHistory.getEnrolledStockExchDate());
        		issuerOrigin.setSuperResolutionDate(issuerHistory.getSuperResolutionDate());
        		issuerOrigin.setIndDifferentiated(issuerHistory.getIndDifferentiated());
        		issuerOrigin.setCreditRatingScales(issuerHistory.getCreditRatingScales());
        		issuerOrigin.setRmvRegister(issuerHistory.getRmvRegister()); //issue 708
        		issuerOrigin.setConstitutionDate(issuerHistory.getConstitutionDate());
        		issuerOrigin.setIndSirtexNegotiate(issuerHistory.getIndSirtexNegotiate());
        		//Isuue: 1164
        		issuerOrigin.setSirtexNegotiateDate(issuerHistory.getSirtexNegotiateDate());
        		issuerOrigin.setCurrencySirtexNegotiate(issuerHistory.getCurrencySirtexNegotiate());
        		issuerOrigin.setPersonTypeSirtexNegotiate(issuerHistory.getPersonTypeSirtexNegotiate());
        		issuerOrigin.setWithCouponsSirtexNegotiate(issuerHistory.getWithCouponsSirtexNegotiate());
        		issuerOrigin.setIssuanceDateSirtexNegotiate(issuerHistory.getIssuanceDateSirtexNegotiate());
        	}
        	else{
        		if(issuerHistory.getRegistryType().equals(RegistryType.HISTORICAL_COPY.getCode()))
            	{
            		issuer.setMnemonic(issuerHistory.getMnemonic());
            		issuer.setBusinessName(issuerHistory.getBusinessName());
            		issuer.setDocumentType(issuerHistory.getDocumentType());
            		issuer.setDocumentNumber(issuerHistory.getDocumentNumber());
            		issuer.setDocumentSource(issuerHistory.getDocumentSource());
            		issuer.setOfferType(issuerHistory.getOfferType());
            		issuer.setRelatedCui(issuerHistory.getRelatedCui());
            		issuer.setFundAdministrator(issuerHistory.getFundAdministrator());
            		issuer.setFundType(issuerHistory.getFundType());
            		issuer.setTransferNumber(issuerHistory.getTransferNumber());
            		issuer.setSocietyType(issuerHistory.getSocietyType());
            		issuer.setContractNumber(issuerHistory.getContractNumber());
            		issuer.setContractDate(issuerHistory.getContractDate());
            		issuer.setSuperintendentResolution(issuerHistory.getSuperintendentResolution());
            		issuer.setSocialCapital(issuerHistory.getSocialCapital());
            		issuer.setGeographicLocation(issuerHistory.getGeographicLocation());
            		issuer.setDepartment(issuerHistory.getDepartment());
            		issuer.setProvince(issuerHistory.getProvince());
            		issuer.setDistrict(issuerHistory.getDistrict());
            		issuer.setLegalAddress(issuerHistory.getLegalAddress());
            		issuer.setPhoneNumber(issuerHistory.getPhoneNumber());
            		issuer.setFaxNumber(issuerHistory.getFaxNumber());
            		issuer.setWebsite(issuerHistory.getWebsite());
            		issuer.setEmail(issuerHistory.getEmail());
            		issuer.setContactPhone(issuerHistory.getContactPhone());
            		issuer.setContactName(issuerHistory.getContactName());
            		issuer.setObservation(issuerHistory.getObservation());
            		issuer.setStateIssuer(issuerHistory.getStateIssuer());
            		issuer.setCreationDate(issuerHistory.getCreationDate());		
            		issuer.setEconomicSector(issuerHistory.getEconomicSector());		
            		issuer.setEconomicActivity(issuerHistory.getEconomicActivity());
            		issuer.setInvestorType(issuerHistory.getInvestorType());
            		issuer.setContactPhone(issuerHistory.getContactPhone());
            		issuer.setCurrency(issuerHistory.getCurrency());
            		issuer.setContactEmail(issuerHistory.getContactEmail());
            		issuer.setRegistryUser(loggerUser.getUserName());
            		issuer.setIndStockExchEnrolled(issuerHistory.getIndStockExchEnrolled());
            		issuer.setIndContractDepository(issuerHistory.getIndContractDepository());
            		issuer.setRetirementDate(issuerHistory.getRetirementDate());
            		issuer.setClosingServiceDate(issuerHistory.getClosingServiceDate());
            		issuer.setEnrolledStockExchDate(issuerHistory.getEnrolledStockExchDate());
            		issuer.setSuperResolutionDate(issuerHistory.getSuperResolutionDate());
            		issuer.setIndDifferentiated(issuerHistory.getIndDifferentiated());
            		issuer.setCreditRatingScales(issuerHistory.getCreditRatingScales());
            		issuer.setRmvRegister(issuerHistory.getRmvRegister()); //issue 708
            		issuer.setConstitutionDate(issuerHistory.getConstitutionDate());
            		issuer.setIndSirtexNegotiate(issuerHistory.getIndSirtexNegotiate());
            		//Issue :1164
            		issuer.setSirtexNegotiateDate(issuerHistory.getSirtexNegotiateDate());
            		issuer.setCurrencySirtexNegotiate(issuerHistory.getCurrencySirtexNegotiate());
            		issuer.setPersonTypeSirtexNegotiate(issuerHistory.getPersonTypeSirtexNegotiate());
            		issuer.setWithCouponsSirtexNegotiate(issuerHistory.getWithCouponsSirtexNegotiate());
            		issuer.setIssuanceDateSirtexNegotiate(issuerHistory.getIssuanceDateSirtexNegotiate());
            	}
        	}
        }
				
		List<IssuerFileHistory> lstIssuerFileHistory = issuerRequest.getIssuerFileHistories();
		List<IssuerFile> issuerFiles = new ArrayList<IssuerFile>();
		List<IssuerFile> issuerFilesOrigin = new ArrayList<IssuerFile>();
		if(Validations.validateListIsNotNullAndNotEmpty(lstIssuerFileHistory)){
			IssuerFile issuerFile;
			for (IssuerFileHistory issuerFileHistory : lstIssuerFileHistory) {
				if(issuerFileHistory.getRegistryType().equals(RegistryType.ORIGIN_OF_THE_REQUEST.getCode()))
				{
					issuerFile = new IssuerFile();
					
					issuerFile.setDocumentType(issuerFileHistory.getDocumentType());				
					issuerFile.setDescription(issuerFileHistory.getDescription());
					issuerFile.setFilename(issuerFileHistory.getFilename());
					issuerFile.setDocumentFile(issuerFileHistory.getDocumentFile());

					issuerFile.setRegistryUser(loggerUser.getUserName());
					issuerFile.setRegistryDate(CommonsUtilities.currentDateTime());
					
					issuerFile.setRequestFileType(RequestParticipantFileType.REGISTER.getCode());		    	
					issuerFile.setIssuer(issuerOrigin);
					issuerFile.setStateFile(IssuerFileStateType.REGISTERED.getCode());
			    	
					issuerFilesOrigin.add(issuerFile);
				}
				else
				{
					if(issuerFileHistory.getRegistryType().equals(RegistryType.HISTORICAL_COPY.getCode()))
					{
						issuerFile = new IssuerFile();
						
						issuerFile.setDocumentType(issuerFileHistory.getDocumentType());				
						issuerFile.setDescription(issuerFileHistory.getDescription());
						issuerFile.setFilename(issuerFileHistory.getFilename());
						issuerFile.setDocumentFile(issuerFileHistory.getDocumentFile());

						issuerFile.setRegistryUser(loggerUser.getUserName());
						issuerFile.setRegistryDate(CommonsUtilities.currentDateTime());
						
						issuerFile.setRequestFileType(RequestParticipantFileType.REGISTER.getCode());		    	
						issuerFile.setIssuer(issuer);
						issuerFile.setStateFile(IssuerFileStateType.REGISTERED.getCode());
				    	
						issuerFiles.add(issuerFile);
					}		
				}
			}				
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(issuerFiles)){
			issuer.setIssuerFiles(issuerFiles);			
			issuerOrigin.setIssuerFiles(issuerFilesOrigin);	
		}else
		{
			Issuer issuerFilter = new Issuer();
			issuerFilter.setIdIssuerPk(issuer.getIdIssuerPk());
			issuerFilter.setStateIssuer(IssuerStateType.REGISTERED.getCode());		
			List<IssuerFile> lstIssuerFiles = getListIssuerFilesServiceFacade(issuerFilter);
			issuer.setIssuerFiles(lstIssuerFiles);	
			issuerOrigin.setIssuerFiles(lstIssuerFiles);	
		}
		
		// Se rompe la relacion entre ficha emisor y ficha titular
		//updateHolder(issuer, loggerUser);
		
		disableIssuerFilesStateServiceFacade(issuer,loggerUser);		
		
		updateIssuerServiceFacade(issuer);		
			
		RepresentedEntity representedEntity = null;			
		LegalRepresentative objLegalRepresentative = null;
		PepPerson pepPerson;
		GeographicLocation countryPep;
		boolean indLegalRepresentativeFoumd;
		if(Validations.validateListIsNotNullAndNotEmpty(lstLegalRepresentativeHistories)){
			disableRepresentedEntitiesStateServiceFacade(issuer,loggerUser);
			for (LegalRepresentativeHistory legalRepresentativeHistory : lstLegalRepresentativeHistories) {
				
				if(legalRepresentativeHistory.getRegistryType().equals(RegistryType.HISTORICAL_COPY.getCode()))
				{
					indLegalRepresentativeFoumd = false;
					if(Validations.validateListIsNotNullAndNotEmpty(lstLegalRepresentativeFound)){
						for (LegalRepresentative legalRepresentativeAux : lstLegalRepresentativeFound) {
							if(legalRepresentativeAux.getDocumentType().equals(legalRepresentativeHistory.getDocumentType()) &&
									legalRepresentativeAux.getDocumentNumber().equals(legalRepresentativeHistory.getDocumentNumber())){
								indLegalRepresentativeFoumd = true;
								objLegalRepresentative = legalRepresentativeAux;
								break;
							}
						}
					}
					
					if(!indLegalRepresentativeFoumd) {
						objLegalRepresentative = new LegalRepresentative();
					}
					
					objLegalRepresentative.setBirthDate(legalRepresentativeHistory.getBirthDate());
					objLegalRepresentative.setDocumentNumber(legalRepresentativeHistory.getDocumentNumber());
					objLegalRepresentative.setDocumentType(legalRepresentativeHistory.getDocumentType());
					objLegalRepresentative.setDocumentSource(legalRepresentativeHistory.getDocumentSource());
					objLegalRepresentative.setDocumentIssuanceDate(legalRepresentativeHistory.getDocumentIssuanceDate());
					objLegalRepresentative.setEconomicActivity(legalRepresentativeHistory.getEconomicActivity());
					objLegalRepresentative.setEconomicSector(legalRepresentativeHistory.getEconomicSector());
					objLegalRepresentative.setEmail(legalRepresentativeHistory.getEmail());
					objLegalRepresentative.setFaxNumber(legalRepresentativeHistory.getFaxNumber());
					objLegalRepresentative.setFirstLastName(legalRepresentativeHistory.getFirstLastName());
					objLegalRepresentative.setFullName(legalRepresentativeHistory.getFullName());
					objLegalRepresentative.setHomePhoneNumber(legalRepresentativeHistory.getHomePhoneNumber());
					objLegalRepresentative.setIndResidence(legalRepresentativeHistory.getIndResidence());
					objLegalRepresentative.setLegalAddress(legalRepresentativeHistory.getLegalAddress());
					objLegalRepresentative.setLegalDistrict(legalRepresentativeHistory.getLegalDistrict());
					objLegalRepresentative.setLegalProvince(legalRepresentativeHistory.getLegalProvince());
					objLegalRepresentative.setLegalDepartment(legalRepresentativeHistory.getLegalDepartment());
					objLegalRepresentative.setLegalResidenceCountry(legalRepresentativeHistory.getLegalResidenceCountry());
					objLegalRepresentative.setMobileNumber(legalRepresentativeHistory.getMobileNumber());
					objLegalRepresentative.setName(legalRepresentativeHistory.getName());
					objLegalRepresentative.setNationality(legalRepresentativeHistory.getNationality());
					objLegalRepresentative.setOfficePhoneNumber(legalRepresentativeHistory.getOfficePhoneNumber());
					objLegalRepresentative.setPersonType(legalRepresentativeHistory.getPersonType());
					objLegalRepresentative.setPostalAddress(legalRepresentativeHistory.getPostalAddress());
					objLegalRepresentative.setPostalDepartment(legalRepresentativeHistory.getPostalDepartment());
					objLegalRepresentative.setPostalDistrict(legalRepresentativeHistory.getPostalDistrict());
					objLegalRepresentative.setPostalProvince(legalRepresentativeHistory.getPostalProvince());
					objLegalRepresentative.setPostalResidenceCountry(legalRepresentativeHistory.getPostalResidenceCountry());
					objLegalRepresentative.setSecondDocumentNumber(legalRepresentativeHistory.getSecondDocumentNumber());
					objLegalRepresentative.setSecondDocumentType(legalRepresentativeHistory.getSecondDocumentType());
					objLegalRepresentative.setSecondLastName(legalRepresentativeHistory.getSecondLastName());
					objLegalRepresentative.setSecondNationality(legalRepresentativeHistory.getSecondNationality());
					objLegalRepresentative.setSex(legalRepresentativeHistory.getSex());
					objLegalRepresentative.setState(LegalRepresentativeStateType.REGISTERED.getCode());
					if(!indLegalRepresentativeFoumd) {
						objLegalRepresentative.setRegistryUser(loggerUser.getUserName());
		    			objLegalRepresentative.setRegistryDate(CommonsUtilities.currentDateTime());
					}
	    			
					if(indLegalRepresentativeFoumd) {
						issuerServiceBean.disableLegalRepresentativeFilesServiceBean(objLegalRepresentative,loggerUser);
					}
					
	    			if(Validations.validateListIsNotNullAndNotEmpty(legalRepresentativeHistory.getRepresenteFileHistory())){
	    				List<LegalRepresentativeFile> lstLegalRepresentativeFile = new ArrayList<LegalRepresentativeFile>();
	    				LegalRepresentativeFile legalRepresentativeFile;
						for (RepresentativeFileHistory representativeFileHistory : legalRepresentativeHistory.getRepresenteFileHistory()) {
							legalRepresentativeFile = new LegalRepresentativeFile();
							
							legalRepresentativeFile.setDescription(representativeFileHistory.getDescription());
							legalRepresentativeFile.setDocumentType(representativeFileHistory.getDocumentType());
							legalRepresentativeFile.setFilename(representativeFileHistory.getFilename());	
							legalRepresentativeFile.setFileLegalRepre(representativeFileHistory.getFileRepresentative());					
							legalRepresentativeFile.setRegistryType(RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
							legalRepresentativeFile.setStateFile(HolderFileStateType.REGISTERED.getCode());
							legalRepresentativeFile.setLegalRepreFileType(HolderReqFileHisType.CREATION.getCode());
							legalRepresentativeFile.setLegalRepresentative(objLegalRepresentative);
							legalRepresentativeFile.setRegistryUser(loggerUser.getUserName());
							legalRepresentativeFile.setRegistryDate(CommonsUtilities.currentDateTime());
							
							lstLegalRepresentativeFile.add(legalRepresentativeFile);
							
						}
						objLegalRepresentative.setLegalRepresentativeFile(lstLegalRepresentativeFile);
					}
	    			
	    			pepPerson = null;
	    			boolean indPepRepresentativeFound = false;
	    			if(indLegalRepresentativeFoumd){
	    				updateLegalRepresentativeServiceFacade(objLegalRepresentative);
	    				pepPerson = issuerServiceBean.getPepPersonByIdLegalRepresentativeServiceBean(objLegalRepresentative.getIdLegalRepresentativePk());
	    				if(Validations.validateIsNotNull(pepPerson)){
	    					indPepRepresentativeFound = true;
	    				}			
	    			} else {
	    				registryLegalRepresentativeServiceFacade(objLegalRepresentative);
	    			}    			    			
	    			
	    			if(BooleanType.YES.getCode().equals(legalRepresentativeHistory.getIndPEP())){
	    				if(!indPepRepresentativeFound){
	    					pepPerson = new PepPerson();
	    				}
	    				
	    				pepPerson.setBeginingPeriod(legalRepresentativeHistory.getBeginningPeriod());
	    				pepPerson.setCategory(legalRepresentativeHistory.getCategory());
	    				pepPerson.setComments(legalRepresentativeHistory.getComments());
	    				pepPerson.setEndingPeriod(legalRepresentativeHistory.getEndingPeriod());
	    				pepPerson.setBeginingPeriod(legalRepresentativeHistory.getBeginningPeriod());
	    				pepPerson.setRole(legalRepresentativeHistory.getRole());
	    				pepPerson.setLegalRepresentative(objLegalRepresentative);
	    				pepPerson.setRegistryUser(loggerUser.getUserName());
	    				pepPerson.setRegistryDate(CommonsUtilities.currentDateTime());
	    				pepPerson.setNotificationDate(CommonsUtilities.currentDateTime());
	    				countryPep = new GeographicLocation();
	    				countryPep.setIdGeographicLocationPk(objLegalRepresentative.getLegalResidenceCountry());		    			
		    		    pepPerson.setGeographicLocation(countryPep);
		    		    pepPerson.setExpeditionPlace(countryPep);
		    		    pepPerson.setDocumentType(legalRepresentativeHistory.getDocumentType());
		    		    pepPerson.setDocumentNumber(legalRepresentativeHistory.getDocumentNumber());
		    		    pepPerson.setClassification(PepPersonClassificationType.HIGH.getCode());
		    		    if(!indPepRepresentativeFound){
		    		    	crudDaoServiceBean.create(pepPerson);
		    		    } else {
		    		    	crudDaoServiceBean.update(pepPerson);
		    		    }
	    			}
					
					representedEntity = new RepresentedEntity();
					representedEntity.setRegistryUser(loggerUser.getUserName());
					representedEntity.setRegistryDate(CommonsUtilities.currentDateTime());
	    			representedEntity.setStateRepresented(RepresentativeEntityStateType.REGISTERED.getCode());
	    			representedEntity.setIssuer(issuer);    			
	    			representedEntity.setLegalRepresentative(objLegalRepresentative);
	    			representedEntity.setRepresentativeClass(legalRepresentativeHistory.getRepresentativeClass());
	    			crudDaoServiceBean.create(representedEntity);
				}
			}
		}		
		
		issuerRequest.setIssuerFiles(null);
		issuerRequest.setActionDate(CommonsUtilities.currentDateTime());
		issuerRequest.setState(IssuerRequestStateType.CONFIRMED.getCode());		

		updateIssuerRequestServiceFacade(issuerRequest);
		
		// issuer 1329: acualizar en cascada el tipo de inversionista de ISSUER -> HOLDER
		updateInvestorTypeCascadeFromIssuer(issuer);
		
		//actualizamos la clasificacion de las emisiones autorizadas del emisor
		updateIssuanceScaleByIssuer(issuer);
		
		this.serviceAPIClients.callWsUpdateInstitutionSecurity(issuer.getBusinessName(), issuer.getMnemonic(), InstitutionType.ISSUER.getCode(), 
				issuer.getIdIssuerPk(), null, InstitutionStateType.REGISTERED.getCode());
		return true;
	}
	
	private void updateIssuanceScaleByIssuer(Issuer i)  throws ServiceException{
		if (Validations.validateIsNotNull(i)) {
				IssuanceTO issuanceTO = new IssuanceTO();
				issuanceTO.setIdIssuerPk(i.getIdIssuerPk());
				issuanceTO.setIssuanceState(IssuanceStateType.AUTHORIZED.getCode());
				List<Issuance>  lstIssuance= issuanceSecuritiesServiceFacade.findIssuancesServiceFacade(issuanceTO);
				for(Issuance objIssuance : lstIssuance) {
					objIssuance.setCreditRatingScales(i.getCreditRatingScales());
					issuerServiceBean.update(objIssuance);
				}				
			}
		
	}
	
	private void updateInvestorTypeCascadeFromIssuer(Issuer i) {
		if (Validations.validateIsNotNull(i)
			&& Validations.validateIsNotNullAndPositive(i.getInvestorType())
			&& Validations.validateIsNotNull(i.getHolder())
			&& Validations.validateIsNotNullAndPositive(i.getHolder().getIdHolderPk())
			) {
			Holder h = issuerServiceBean.find(Holder.class, i.getHolder().getIdHolderPk());
			h.setInvestorType(i.getInvestorType());
			issuerServiceBean.updateInvestorTypeHolderFromIssuer(h);
		}
	}
	
	/**
	 * Validate Ind Cant Transfer
	 * 
	 */
	public Integer validateIndCanTransfer (Integer economicActivity, Integer indCanNegotiate){
		
		//ISSUE 770
		
		//cambiar si es que el indicador de la transferencia extrabursatil es 0
		if(Validations.validateIsNull(indCanNegotiate)){
			
			if (Validations.validateIsNotNull(economicActivity)){
				
				//en cualquier caso la decision es SI 1
				indCanNegotiate = 1;
				
				//exceptuando las actividades economicas:
				
				//AGENCIAS DE BOLSA
				if(economicActivity == EconomicActivityType.AGENCIAS_BOLSA.getCode()){
					indCanNegotiate = 0;
				}
				
				//COMPANIAS DE SEGUROS
				if(economicActivity == EconomicActivityType.COMPANIAS_SEGURO.getCode()){
					indCanNegotiate = 0;
				}
				
				//FONDOS DE INVERSION ABIERTOS
				if(economicActivity == EconomicActivityType.FONDOS_INVERSION_ABIERTOS.getCode()){
					indCanNegotiate = 0;
				}
				
				//FONDOS DE INVERSION CERRADOS
				if(economicActivity == EconomicActivityType.FONDOS_INVERSION_CERRADOS.getCode()){
					indCanNegotiate = 0;
				}
				
				//FONDOS DE INVERSION MIXTOS
				if(economicActivity == EconomicActivityType.FONDOS_INVERSION_MIXTOS.getCode()){
					indCanNegotiate = 0;
				}
				
				//SOCIEDADES ADMINISTRADORAS DE FONDOS DE INVERSION
				if(economicActivity == EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode()){
					indCanNegotiate = 0;
				}
			}
		}
			
		return indCanNegotiate;
	}
	
	/**
	 * Disable issuer files state service facade.
	 *
	 * @param issuer the issuer
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int disableIssuerFilesStateServiceFacade(Issuer issuer, LoggerUser loggerUser) throws ServiceException {
		return issuerServiceBean.disableIssuerFilesStateServiceBean(issuer, loggerUser);	
	}
	
	/**
	 * Disable represented entities state service facade.
	 *
	 * @param issuer the issuer
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int disableRepresentedEntitiesStateServiceFacade(Issuer issuer, LoggerUser loggerUser) throws ServiceException {
		return issuerServiceBean.disableRepresentedEntitiesStateServiceBean(issuer, loggerUser);	
	}
	
	/**
	 * Find issuer service facade.
	 *
	 * @param issuerTO the issuer to
	 * @return the issuer
	 * @throws ServiceException the service exception
	 */
	public Issuer findIssuerServiceFacade(IssuerTO issuerTO) throws ServiceException{
		return issuerServiceBean.findIssuerServiceBean(issuerTO);
	}


	/**
	 * Gets the list legal representative file service facade.
	 *
	 * @param legalRepresentative the legal representative
	 * @return the list legal representative file service facade
	 * @throws ServiceException the service exception
	 */
	public List<LegalRepresentativeFile> getListLegalRepresentativeFileServiceFacade(
			LegalRepresentative legalRepresentative) throws ServiceException{
		return issuerServiceBean.getListLegalRepresentativeFileServiceBean(legalRepresentative);
	}
	/**
	 * Gets the list legal representative file service facade.
	 *
	 * @param legalRepresentative the legal representative
	 * @return the list legal representative file service facade
	 * @throws ServiceException the service exception
	 */
	public List<RepresentativeFileHistory> getListLegalRepresentativeFileHistoryServiceBean(
			LegalRepresentative legalRepresentative) throws ServiceException{
		return issuerServiceBean.getListLegalRepresentativeFileHistoryServiceBean(legalRepresentative);
	}

	/**
	 * Gets the list legal representative file hist service facade.
	 *
	 * @param legalRepresentativeHistory the legal representative history
	 * @return the list legal representative file hist service facade
	 * @throws ServiceException the service exception
	 */
	public List<RepresentativeFileHistory> getListLegalRepresentativeFileHistServiceFacade(
			LegalRepresentativeHistory legalRepresentativeHistory) throws ServiceException {
		return issuerServiceBean.getListLegalRepresentativeFileHistServiceBean(legalRepresentativeHistory);
	}
	
	/**
	 * Find pep person by legal representative service facade.
	 *
	 * @param legalRepresentative the legal representative
	 * @return the pep person
	 * @throws ServiceException the service exception
	 */
	public PepPerson findPepPersonByLegalRepresentativeServiceFacade(
			LegalRepresentative legalRepresentative) throws ServiceException{
		return issuerServiceBean.findPepPersonByLegalRepresentativeServiceBean(legalRepresentative);
	}
	
	/**
	 * Gets the legal representative by document type and document number service facade.
	 *
	 * @param legalRepresentative the legal representative
	 * @return the legal representative by document type and document number service facade
	 * @throws ServiceException the service exception
	 */
	public LegalRepresentative getLegalRepresentativeByDocumentTypeAndDocumentNumberServiceFacade(LegalRepresentative legalRepresentative) throws ServiceException {
    	return issuerServiceBean.getLegalRepresentativeByDocumentTypeAndDocumentNumberServiceBean(legalRepresentative);
    }
	
	/**
	 * Gets the pep person by id holder service facade.
	 *
	 * @param idHolderPk the id holder pk
	 * @return the pep person by id holder service facade
	 * @throws ServiceException the service exception
	 */
	public PepPerson getPepPersonByIdHolderServiceFacade(Long idHolderPk) throws ServiceException{
   	 PepPerson pepPerson = issuerServiceBean.getPepPersonByIdHolderServiceBean(idHolderPk);
   	 return pepPerson;
    }
	
	/**
	 * Update legal representative service facade.
	 *
	 * @param legalRepresentative the legal representative
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean updateLegalRepresentativeServiceFacade(LegalRepresentative legalRepresentative) throws ServiceException{
		crudDaoServiceBean.update(legalRepresentative);
		return true;
	}
	
	/**
	 * Validate exists superintendent resolution siv service facade.
	 *
	 * @param issuer the issuer
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean validateExistsSuperintendentResolutionSivServiceFacade(Issuer issuer) throws ServiceException{	
		return issuerServiceBean.validateExistsSuperintendentResolutionSivServiceBean(issuer);
	}
	
	/**
	 * Gets the holder state by id.
	 *
	 * @param idHolderPk the id holder pk
	 * @return the holder state by id
	 * @throws ServiceException the service exception
	 */
	public Integer getHolderStateById(Long idHolderPk) throws ServiceException{
		return issuerServiceBean.getHolderStateById(idHolderPk);
	}
	
	/**
	 * Gets the next correlative secuence.
	 *
	 * @param correlativeType the correlative type
	 * @return the next correlative secuence
	 */
	public Long getNextCorrelativeSecuence(RequestCorrelativeType correlativeType){
 		return  issuerServiceBean.getNextCorrelativeOperations(correlativeType);		
 	 }
	
	/**
	 * Creates the sequence dinamic.
	 *
	 * @param mnenomicIssuer the mnenomic issuer
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int createSequenceDinamic(String mnenomicIssuer) throws ServiceException {
		return issuerServiceBean.createSequenceDinamic(mnenomicIssuer);
	}
	
	/**
	 * Verifies holder of the issuer.
	 * Issue 1234 Confirmar modificacion de emisor
	 * @param issuerRequest
	 * @return
	 * @throws ServiceException
	 */
	public boolean verifiesHolderOfTheIssuer(IssuerRequest issuerRequest) throws ServiceException{
		IssuerRequestTO issuerRequestTO = new IssuerRequestTO();
		issuerRequestTO.setIdIssuerRequestPk(issuerRequest.getIdIssuerRequestPk());
		issuerRequest = findIssuerModificationRequestByIdServiceFacade(issuerRequestTO);					
		Issuer issuer = issuerRequest.getIssuer();
		if(Validations.validateIsNotNullAndNotEmpty(issuer.getHolder())){
			return true;
		}
		return false;
		
	}
	
}
