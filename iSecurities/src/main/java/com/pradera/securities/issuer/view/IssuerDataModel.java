package com.pradera.securities.issuer.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.issuancesecuritie.Issuer;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class IssuerDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 03/04/2013
 */
public class IssuerDataModel extends ListDataModel<Issuer> implements SelectableDataModel<Issuer>, Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new issuer data model.
	 */
	public IssuerDataModel(){
		
	}
	
	/**
	 * Instantiates a new issuer data model.
	 *
	 * @param data the data
	 */
	public IssuerDataModel(List<Issuer> data){
		super(data);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public Issuer getRowData(String rowKey) {
		List<Issuer> lstIssuer = (List<Issuer>)getWrappedData();
        for(Issuer issuer : lstIssuer ) {  
        	
            if(String.valueOf(issuer.getIdIssuerPk()).equals(rowKey))
                return issuer;  
        }
		return null;
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(Issuer issuer) {
		return issuer.getIdIssuerPk();
	}		

}
