package com.pradera.securities.issuer.view;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.view.IssuerHelperBean;
import com.pradera.core.component.utils.DocumentTO;
import com.pradera.core.component.utils.DocumentValidator;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.IssuerRequestRejectMotiveType;
import com.pradera.model.accounts.type.IssuerRequestStateType;
import com.pradera.model.accounts.type.IssuerRequestType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.IssuerFile;
import com.pradera.model.issuancesecuritie.IssuerHistoryState;
import com.pradera.model.issuancesecuritie.IssuerRequest;
import com.pradera.model.issuancesecuritie.type.IssuerFileStateType;
import com.pradera.model.issuancesecuritie.type.IssuerFileType;
import com.pradera.model.issuancesecuritie.type.IssuerRequestIndicatorType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.RequestIssuerFileType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.securities.issuancesecuritie.util.PropertiesConstants;
import com.pradera.securities.issuer.facade.IssuerServiceFacade;
import com.pradera.securities.issuer.to.IssuerHistoryStateTO;
import com.pradera.securities.issuer.to.IssuerRequestTO;
import com.pradera.securities.issuer.to.IssuerTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class IssuerMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 08/02/2013
 */
@DepositaryWebBean
@LoggerCreateBean
public class IssuerBlockMgmtBean extends GenericBaseBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The country residence. */
	@Inject @Configurable Integer countryResidence;
	
	/** The notification service facade. */
	@EJB NotificationServiceFacade notificationServiceFacade;
	
	/** The issuer helper bean. */
	@Inject
	private IssuerHelperBean issuerHelperBean;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject UserPrivilege userPrivilege;
	
	/** The issuer service facade. */
	@EJB
	private IssuerServiceFacade issuerServiceFacade;
	
	/** The general parameters facade. */
	@EJB
	private GeneralParametersFacade generalParametersFacade;
	
	/** The issuer request. */
	private IssuerRequest issuerRequest;
	
	/** The issuer to. */
	private IssuerTO issuerTO;
	
	/** The lst issuer file sustent. */
	private List<IssuerFile> lstIssuerFileSustent;
	
	/** The lst issuer history states. */
	private List<IssuerHistoryState> lstIssuerHistoryStates;

	/** The lst issuer block request types. */
	private List<ParameterTable> lstIssuerBlockRequestTypes;
	
	/** The lst issuer motive types. */
	private List<ParameterTable> lstIssuerMotiveTypes = new ArrayList<ParameterTable>();
	
	/** The lst issuer document types. */
	private List<ParameterTable> lstIssuerDocumentTypes;

	/** The lst issuer request states. */
	private List<ParameterTable> lstIssuerRequestStates;
	
	/** The lst issuer request reject motives. */
	private List<ParameterTable> lstIssuerRequestRejectMotives;
	
	/** The issuer request data model. */
	private IssuerRequestDataModel issuerRequestDataModel;
	
	/** Document Validate. */
    @Inject
    private DocumentValidator documentValidatorRegister;  
    
    /** Document search. */
    private DocumentTO documentTORegister;
    
	/** The ind disable confirn reject. */
	private boolean indDisableConfirnReject;
	
	/** The ind selected other motive reject. */
	private boolean indSelectedOtherMotiveReject;
		
	/** The ind disable accept motive reject. */
	private boolean indDisableAcceptMotiveReject;
	
	/** The issuer file name display. */
	private String issuerFileNameDisplay;
	
	/** The allow search all. */
	private boolean allowSearchAll;
	
	/** The streamed content file. */
	private transient StreamedContent streamedContentFile;
	
	/** The window block issuer. */
	private String WINDOW_BLOCK_ISSUER="issuerBlockMgmtRule";
	
	/** The window block issuer detail. */
	private String WINDOW_BLOCK_ISSUER_DETAIL="issuerBlockDetailRule";
	
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	private void init() {
		try{			
			setViewOperationType(ViewOperationsType.CONSULT.getCode());
			createObjects();
			loadLstIssuerBlockRequestTypes();		
			loadLstIssuerRequestStates();
			loadLstIssuerDocumentTypes();
			loadHolidays();
			sessionValidUserAccount();		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	/**
	 * Load holidays.
	 */
	private void loadHolidays() {
		// TODO Auto-generated method stub
		 Holiday holidayFilter = new Holiday();
		 holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		 GeographicLocation countryFilter = new GeographicLocation();
		 countryFilter.setIdGeographicLocationPk(countryResidence);
		 holidayFilter.setGeographicLocation(countryFilter);
		 try {
			allHolidays = CommonsUtilities.convertListDateToString(generalParametersFacade.getListHolidayDateServiceFacade(holidayFilter));
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Create objects to store and obtain data relating to issuers.
	 */
	public void createObjects()
	{
		issuerRequest = new IssuerRequest();
		issuerRequest.setIssuer(new Issuer());
		issuerTO  = new IssuerTO();
		issuerTO.setIssuerRequestTO(new IssuerRequestTO());	
	}
	
	/**
	 * Validates user session objects to enable or disable.
	 */
	public void sessionValidUserAccount()
	{
		issuerTO.getIssuerRequestTO().setInitiaRequestDate(CommonsUtilities.currentDate());
		issuerTO.getIssuerRequestTO().setEndRequestDate(CommonsUtilities.currentDate());
		if(userInfo.getUserAccountSession().isDepositaryInstitution()) {
			allowSearchAll = true;			
		} else if((userInfo.getUserAccountSession().isIssuerInstitucion() || userInfo.getUserAccountSession().isIssuerDpfInstitucion()) &&
				Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getIssuerCode())){
			allowSearchAll = false;
			issuerTO.setIdIssuerPk(userInfo.getUserAccountSession().getIssuerCode());	    
			Issuer objIssuer = null;
			try {
				objIssuer = issuerServiceFacade.findIssuerServiceFacade(issuerTO);
				issuerTO.setBusinessName(objIssuer.getBusinessName());
				issuerTO.setDocumentType(objIssuer.getDocumentType());
				issuerTO.setDocumentNumber(objIssuer.getDocumentNumber());		
				issuerTO.setMnemonic(objIssuer.getMnemonic());
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	/**
	 * Gets the document Validator Register.
	 *
	 * @return the document Validator Register
	 */
	public DocumentValidator getDocumentValidatorRegister() {
		return documentValidatorRegister;
	}
	
	/**
	 * Sets the document Validator Register.
	 *
	 * @param documentValidatorRegister the new document validator register
	 */
	public void setDocumentValidatorRegister(DocumentValidator documentValidatorRegister) {
		this.documentValidatorRegister = documentValidatorRegister;
	}
	
	/**
	 * Gets the document Register.
	 *
	 * @return the document  Register
	 */
	public DocumentTO getDocumentTORegister() {
		return documentTORegister;
	}
	
	/**
	 * Sets the document  Register.
	 *
	 * @param documentTORegister the new document to register
	 */
	public void setDocumentTORegister(DocumentTO documentTORegister) {
		this.documentTORegister = documentTORegister;
	}
	
	/**
	 * Load lst issuer document types.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadLstIssuerDocumentTypes() throws ServiceException {
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		filterParameterTable.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());		
		List<Integer> lstParameterTablePk = new ArrayList<Integer>();
		lstParameterTablePk.add(DocumentType.RUC.getCode());
		lstParameterTablePk.add(DocumentType.DIO.getCode());
		filterParameterTable.setLstParameterTablePk(lstParameterTablePk);
		setLstIssuerDocumentTypes(generalParametersFacade.getListParameterTableServiceBean(filterParameterTable));
	}

	/**
	 * Load lst issuer block request types.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadLstIssuerBlockRequestTypes() throws ServiceException{
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		filterParameterTable.setMasterTableFk(MasterTableType.ISSUER_REQUEST_TYPE.getCode());	
		//set the indicator 1 in order to get only the parameter for block or unblock
		filterParameterTable.setIndicator1(IssuerRequestIndicatorType.BLOCK_UNBLOCK_REQUEST_TYPE.getCode());
		setLstIssuerBlockRequestTypes(generalParametersFacade.getListParameterTableServiceBean(filterParameterTable));
	}
	
	/**
	 * Load lst issuer request states.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadLstIssuerRequestStates() throws ServiceException{
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		filterParameterTable.setMasterTableFk(MasterTableType.ISSUER_BLOCK_REQUEST_STATUS_TYPE.getCode());		
		setLstIssuerRequestStates(generalParametersFacade.getListParameterTableServiceBean(filterParameterTable));
	}
	
	
	/**
	 * Load lst issuer request reject motives.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadLstIssuerRequestRejectMotives() throws ServiceException {
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		filterParameterTable.setMasterTableFk(MasterTableType.ISSUER_REQUEST_REJECT_MOTIVE_TYPE.getCode());		
		setLstIssuerRequestRejectMotives(generalParametersFacade.getListParameterTableServiceBean(filterParameterTable));
	}
	
	
	/**
	 * Clean all search.
	 */
	@LoggerAuditWeb
	public void cleanAllSearch(){
		cleanDataModel();
		validateUserAccountSession();
		JSFUtilities.resetViewRoot();
	}
	
	/**
	 * Validate User Account Session.
	 */
	public void validateUserAccountSession()
	{
		issuerRequest = new IssuerRequest();
		issuerRequest.setIssuer(new Issuer());
		issuerTO  = new IssuerTO();		
		issuerTO.setIssuerRequestTO(new IssuerRequestTO());	
		issuerTO.getIssuerRequestTO().setInitiaRequestDate(CommonsUtilities.currentDate());
		issuerTO.getIssuerRequestTO().setEndRequestDate(CommonsUtilities.currentDate());
		if((userInfo.getUserAccountSession().isIssuerInstitucion() || userInfo.getUserAccountSession().isIssuerDpfInstitucion()) &&
				Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getIssuerCode())){			
			issuerTO.setIdIssuerPk(userInfo.getUserAccountSession().getIssuerCode());
			issuerTO.setIdIssuerPk(userInfo.getUserAccountSession().getIssuerCode());	    
			Issuer objIssuer = null;
			try {
				objIssuer = issuerServiceFacade.findIssuerServiceFacade(issuerTO);
				issuerTO.setBusinessName(objIssuer.getBusinessName());
				issuerTO.setDocumentType(objIssuer.getDocumentType());
				issuerTO.setDocumentNumber(objIssuer.getDocumentNumber());		
				issuerTO.setMnemonic(objIssuer.getMnemonic());
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Load register issuer request.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String loadRegisterIssuerRequest(){	
		setViewOperationType(ViewOperationsType.REGISTER.getCode());
		cleanAll();
		return WINDOW_BLOCK_ISSUER;
	}
	
	/**
	 * get User Information.
	 *
	 * @return object User Information
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}
	
	/**
	 * set User Information.
	 *
	 * @param userInfo object User Information
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	
	/**
	 * Clean Data Model.
	 */
	public void cleanDataModel()
	{
		issuerRequestDataModel = null;
	}
	/**
	 * Change selected document type search.
	 */
	public void changeSelectedDocumentTypeSearch(){
		issuerTO.setDocumentNumber(null);		
		if(Validations.validateIsNotNullAndPositive(issuerTO.getDocumentType()))
			documentTORegister=documentValidatorRegister.loadMaxLenghtType(issuerTO.getDocumentType());				
		cleanDataModel();
	}
	/**
	 * Change selected document type search.
	 */
	public void changeSelectedDocumentTypeRegister(){
		hideDialogsListener(null);
		issuerRequest.getIssuer().setDocumentNumber(null);	
		if(Validations.validateIsNotNullAndPositive(issuerRequest.getIssuer().getDocumentType()))
			documentTORegister=documentValidatorRegister.loadMaxLenghtType(issuerRequest.getIssuer().getDocumentType());				
	}

	/**
	 * Before confirm issuer request.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String beforeConfirmIssuerRequest(){
		
		hideDialogsListener(null);
		
		if(Validations.validateIsNull(issuerRequest)){
			showMessageOnDialog(null,null,PropertiesConstants.ERROR_RECORD_NOT_SELECTED,null);
			JSFUtilities.updateComponent("cnfGeneralMsgBusinessValidation"); 
			JSFUtilities.executeJavascriptFunction("cnfwGeneralMsgBusinessValidation.show()");
    		return null;
		}
		
		Issuer issuerFilter = new Issuer();
		issuerFilter.setIdIssuerPk(issuerRequest.getIssuer().getIdIssuerPk());
		
		if(!IssuerRequestStateType.REGISTERED.getCode().equals(issuerRequest.getState())){
			showMessageOnDialog(null,null,PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_NOT_IN_REGISTERED_STATE,null);
			JSFUtilities.updateComponent("cnfGeneralMsgBusinessValidation"); 
			JSFUtilities.executeJavascriptFunction("cnfwGeneralMsgBusinessValidation.show()");
    		return null;
		}
		
		setViewOperationType(ViewOperationsType.CONFIRM.getCode());
		
		return detailIssuerRequest(issuerRequest.getIdIssuerRequestPk(),getViewOperationType());
	}

	/**
	 * Before reject issuer request.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String beforeRejectIssuerRequest(){
		
		hideDialogsListener(null);
		
		if(Validations.validateIsNull(issuerRequest)){
			showMessageOnDialog(null,null,PropertiesConstants.ERROR_RECORD_NOT_SELECTED,null);
			JSFUtilities.updateComponent("cnfGeneralMsgBusinessValidation"); 
			JSFUtilities.executeJavascriptFunction("cnfwGeneralMsgBusinessValidation.show()");
    		return null;
		}
		
		Issuer issuerFilter = new Issuer();
		issuerFilter.setIdIssuerPk(issuerRequest.getIssuer().getIdIssuerPk());
		
		if(!IssuerRequestStateType.REGISTERED.getCode().equals(issuerRequest.getState())){
			showMessageOnDialog(null,null,PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_NOT_IN_REGISTERED_STATE,null);
			JSFUtilities.updateComponent("cnfGeneralMsgBusinessValidation"); 
			JSFUtilities.executeJavascriptFunction("cnfwGeneralMsgBusinessValidation.show()");
    		return null;
		}
		
		setViewOperationType(ViewOperationsType.REJECT.getCode());
		
		return detailIssuerRequest(issuerRequest.getIdIssuerRequestPk(),getViewOperationType());
	}
	
	/**
	 * Begin confirm issuer request.
	 */
	@LoggerAuditWeb
	public void beginConfirmIssuerRequest(){
		
		try{
			hideDialogsListener(null);
			JSFUtilities.setValidViewComponentAndChildrens("frmMotiveRejectIssRequest");
						
			Object[] arrBodyData = {issuerRequest.getRequestTypeDescription(), String.valueOf(issuerRequest.getRequestNumber())};
			showMessageOnDialog(PropertiesConstants.LBL_CONFIRM_HEADER_RECORD,null,PropertiesConstants.LBL_CONFIRM_ISSUER_REQUEST_CONFIRMATION,arrBodyData);
			JSFUtilities.showComponent("cnfIssuerRequestConfirmation");
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));		
		}
	}
	
	/**
	 * Begin reject issuer request.
	 */
	@LoggerAuditWeb
	public void beginRejectIssuerRequest(){
		try {
		hideDialogsListener(null);
		JSFUtilities.setValidViewComponentAndChildrens("frmMotiveRejectIssRequest");
		
		loadLstIssuerRequestRejectMotives();
		
		issuerRequest.setActionMotive(null);
		issuerRequest.setActionOtherMotive(null);
		
		indDisableAcceptMotiveReject = Boolean.TRUE;
		indSelectedOtherMotiveReject = Boolean.FALSE;
		
		JSFUtilities.executeJavascriptFunction("PF('cnfwMotiveRejectIssRequest').show()");
		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));		
		}
			
	}
	
	/**
	 * Change motive reject.
	 */
	@LoggerAuditWeb
	public void changeMotiveReject(){
		JSFUtilities.setValidViewComponentAndChildrens("frmMotiveRejectIssRequest");
		if(IssuerRequestRejectMotiveType.OTHER_MOTIVE_REJECT.getCode().equals(issuerRequest.getActionMotive())){
			indSelectedOtherMotiveReject = Boolean.TRUE;
			indDisableAcceptMotiveReject = Boolean.TRUE;
		} else {
			indSelectedOtherMotiveReject = Boolean.FALSE;
			if(Validations.validateIsNotNullAndPositive(issuerRequest.getActionMotive())){
				indDisableAcceptMotiveReject = Boolean.FALSE;
			} else {
				indDisableAcceptMotiveReject = Boolean.TRUE;
			}			
		}
		issuerRequest.setActionOtherMotive(null);		
	}
	
	/**
	 * Accept reject motive issuer request.
	 */
	public void acceptRejectMotiveIssuerRequest(){
		hideDialogsListener(null);
		JSFUtilities.setValidViewComponentAndChildrens("frmMotiveRejectIssRequest");
		
		int countValidationRequiredErrors = 0;		

		JSFUtilities.executeJavascriptFunction("PF('cnfwMotiveRejectIssRequest').hide()");
    	
    	if(Validations.validateIsNullOrNotPositive(issuerRequest.getActionMotive())){
    		JSFUtilities.addContextMessage("frmMotiveRejectIssRequest:cboMotiveReject",
    				FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_REJECT_MOTIVE), 
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_REJECT_MOTIVE));
    		countValidationRequiredErrors++;
    	}
    	
    	if(indSelectedOtherMotiveReject &&
    			Validations.validateIsNullOrEmpty(issuerRequest.getActionOtherMotive())){
    		JSFUtilities.addContextMessage("frmMotiveRejectIssRequest:txtOtherMotiveReject",
    				FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_REJECT_OTHER_MOTIVE), 
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_REJECT_OTHER_MOTIVE));
    		countValidationRequiredErrors++;
    	}
    	
    	
    	if(countValidationRequiredErrors > 0){
    		showMessageOnDialog(
    				null,
    				null,
    				PropertiesConstants.MESSAGE_DATA_REQUIRED,							
    				null);
    		JSFUtilities.updateComponent("cnfGeneralMsgBusinessValidation"); 
    		JSFUtilities.executeJavascriptFunction("cnfwGeneralMsgBusinessValidation.show()");
    		return;
    	}
    	
    	Object[] arrBodyData = {String.valueOf(issuerRequest.getRequestNumber())};
		showMessageOnDialog(
				PropertiesConstants.LBL_CONFIRM_HEADER_RECORD,
				null,
				PropertiesConstants.LBL_CONFIRM_ISSUER_REQUEST_REJECT,							
				arrBodyData);
		JSFUtilities.executeJavascriptFunction("PF('cnfwIssuerRequestReject').show()");
    	
	}

	/**
	 * Type other motive reject.
	 */
	public void typeOtherMotiveReject(){	
		if(Validations.validateIsNotNullAndNotEmpty(issuerRequest.getActionOtherMotive())){
			setIndDisableAcceptMotiveReject(Boolean.FALSE);
		} else {
			setIndDisableAcceptMotiveReject(Boolean.TRUE); 
		}
	}
	
	/**
	 * Accept reject motive participant request.
	 */
	public void acceptRejectMotiveParticipantRequest(){
		
		JSFUtilities.setValidViewComponentAndChildrens("frmMotiveRejectIssRequest");
		
		int countValidationRequiredErrors = 0;
    	
    	if(Validations.validateIsNullOrNotPositive(issuerRequest.getActionMotive())){
    		JSFUtilities.addContextMessage("frmMotiveRejectIssRequest:cboMotiveReject",
    				FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_REJECT_MOTIVE), 
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_REJECT_MOTIVE));
    		countValidationRequiredErrors++;
    	}
    	
    	if(indSelectedOtherMotiveReject &&
    			Validations.validateIsNullOrEmpty(issuerRequest.getActionOtherMotive())){
    		JSFUtilities.addContextMessage("frmMotiveRejectIssRequest:txtOtherMotiveReject",
    				FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_REJECT_OTHER_MOTIVE), 
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_REJECT_OTHER_MOTIVE));
    		countValidationRequiredErrors++;
    	}
    	
    	
    	if(countValidationRequiredErrors > 0){
    		showMessageOnDialog(
    				null,
    				null,
    				PropertiesConstants.MESSAGE_DATA_REQUIRED,							
    				null);
    		JSFUtilities.updateComponent("cnfGeneralMsgBusinessValidation"); 
    		JSFUtilities.executeJavascriptFunction("cnfwGeneralMsgBusinessValidation.show()");
    		return;
    	}
    	
    	Object[] arrBodyData = {String.valueOf(issuerRequest.getRequestNumber())};
		showMessageOnDialog(
				PropertiesConstants.LBL_CONFIRM_HEADER_RECORD,
				null,
				PropertiesConstants.LBL_CONFIRM_ISSUER_REQUEST_REJECT,							
				arrBodyData);
		JSFUtilities.executeJavascriptFunction("cnfwMotiveRejectPartRequest.show()");
		JSFUtilities.executeJavascriptFunction("cnfwParticipantRequestReject.show()");
    	
	}
	
	/**
	 * Hide dialogs listener.
	 *
	 * @param actionEvent the action event
	 */
	public void hideDialogsListener(ActionEvent actionEvent){
		JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
		JSFUtilities.hideGeneralDialogues();
		JSFUtilities.hideComponent("cnfIssuerBlockRequest");
		JSFUtilities.hideComponent("cnfEndTransactionIssuer");
	}
	
	/**
	 * Show issuer history states.
	 *
	 * @param idIssuerPk the id issuer pk
	 */
	public void showIssuerHistoryStates(String idIssuerPk){
		try {
			IssuerHistoryStateTO issuerHistoryStateTO = new IssuerHistoryStateTO();
			issuerHistoryStateTO.setIdIssuerPk(idIssuerPk);
			lstIssuerHistoryStates = issuerServiceFacade.getListIssuerHistoryStatesServiceFacade(issuerHistoryStateTO);			
			JSFUtilities.executeJavascriptFunction("dlgwIssuerHistoryStates.show()");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));		
		}
	}
	
	/**
	 * Save confirm issuer request.
	 */
	@LoggerAuditWeb
	public void saveConfirmIssuerRequest(){
		try {
			JSFUtilities.executeJavascriptFunction("cnfwIssuerRequestConfirmation.hide()");  
			JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
			JSFUtilities.hideGeneralDialogues();
			
			boolean transactionResult = issuerServiceFacade.confirmIssuerBlockRequestServiceFacade(issuerRequest);
			
			if(transactionResult){
				
				BusinessProcess businessProcessNotification = new BusinessProcess();
				if(IssuerRequestType.BLOCK.getCode().equals(issuerRequest.getRequestType())) {
					businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.ISSUER_BLOCK_CONFIRMATION.getCode());
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
																businessProcessNotification, 
																issuerRequest.getIssuer().getIdIssuerPk(), null);
				} else if(IssuerRequestType.UNBLOCK.getCode().equals(issuerRequest.getRequestType())) {
					businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.ISSUER_UNBLOCK_CONFIRMATION.getCode());
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
																businessProcessNotification, 
																issuerRequest.getIssuer().getIdIssuerPk(), null);
				}
				
				//if(transactionResult) {
				Object[] arrBodyData = {issuerRequest.getRequestTypeDescription(), String.valueOf(issuerRequest.getRequestNumber())};
				showMessageOnDialog(
						null, null,
						PropertiesConstants.LBL_SUCCESS_ISSUER_REQUEST_CONFINRMATION, 
						arrBodyData);					
				searchIssuerRequests();
				
				if(Validations.validateIsNotNull(getViewOperationType()) && isViewOperationConfirm()){
					JSFUtilities.showComponent("cnfEndTransactionIssuer");			
				} else {
					JSFUtilities.showEndTransactionSamePageDialog();
				}
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
				//}
			}
			
			setValidButtonsListener();
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));		
		}
	}
	
	/**
	 * Save reject issuer request.
	 */
	@LoggerAuditWeb
	public void saveRejectIssuerRequest(){
		try {
			JSFUtilities.executeJavascriptFunction("cnfwIssuerRequestReject.hide()");  
			JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
			JSFUtilities.hideGeneralDialogues();
			
			issuerRequest.setActionDate(CommonsUtilities.currentDateTime());
			issuerRequest.setState(IssuerRequestStateType.REJECTED.getCode());
			
			IssuerRequest issuerRequestres = issuerServiceFacade.rejectIssuerBlockRequestServiceFacade(issuerRequest);
			
			BusinessProcess businessProcessNotification = new BusinessProcess();
			if(IssuerRequestType.BLOCK.getCode().equals(issuerRequest.getRequestType())) {
				businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.ISSUER_BLOCK_REJECT.getCode());
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
															businessProcessNotification, 
															issuerRequest.getIssuer().getIdIssuerPk(), null);
			} else if(IssuerRequestType.UNBLOCK.getCode().equals(issuerRequest.getRequestType())) {
				businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.ISSUER_UNBLOCK_REJECT.getCode());
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
															businessProcessNotification, 
															issuerRequest.getIssuer().getIdIssuerPk(), null);
			}
			
			//if(transactionResult) {
				Object[] arrBodyData = {issuerRequestres.getRequestTypeDescription(), String.valueOf(issuerRequestres.getRequestNumber())};
				showMessageOnDialog(
						null, null,
						PropertiesConstants.LBL_SUCCESS_ISSUER_REQUEST_REJECT, 
						arrBodyData);					
				searchIssuerRequests();
				
				if(Validations.validateIsNotNull(getViewOperationType()) && isViewOperationReject()){
					JSFUtilities.showComponent("cnfEndTransactionIssuer");				
				} else {
					JSFUtilities.showEndTransactionSamePageDialog();
				}
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
			//}
				setValidButtonsListener();
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));		
		}
	}
	
	/**
	 * Validate Date Search.
	 */
	public void validateDateSearch()
	{

    	//Validations of initial dare not greater than system date
    	if(issuerTO.getIssuerRequestTO().getInitiaRequestDate().after(CommonsUtilities.currentDate())){
    		JSFUtilities.addContextMessage("frmGeneral:calBeginDate",
    				FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_SYS_DATE), 
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_SYS_DATE));
    		showMessageOnDialog(
    				null,
    				null,
    				PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_SYS_DATE,							
    				null);
    		JSFUtilities.updateComponent("cnfGeneralMsgBusinessValidation"); 
    		JSFUtilities.executeJavascriptFunction("cnfwGeneralMsgBusinessValidation.show()");
    		return;
    	}
    	
    	//Validations of end dare not greater than system date
    	if(issuerTO.getIssuerRequestTO().getEndRequestDate().after(CommonsUtilities.currentDate())){
    		JSFUtilities.addContextMessage("frmGeneral:calEndDate",
    				FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_SEARCH_END_DATE_GREAT_SYS_DATE), 
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_SEARCH_END_DATE_GREAT_SYS_DATE));
    		showMessageOnDialog(
    				null,
    				null,
    				PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_SEARCH_END_DATE_GREAT_SYS_DATE,							
    				null);
    		JSFUtilities.updateComponent("cnfGeneralMsgBusinessValidation"); 
    		JSFUtilities.executeJavascriptFunction("cnfwGeneralMsgBusinessValidation.show()");
    		return;
    	}
    	
    	//Validations of initial dare not greater than end date
		if(issuerTO.getIssuerRequestTO().getInitiaRequestDate().after(issuerTO.getIssuerRequestTO().getEndRequestDate())){
    		JSFUtilities.addContextMessage("frmGeneral:calBeginDate",
    				FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_END_DATE), 
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_SEARCH_END_DATE_GREAT_SYS_DATE));
    		showMessageOnDialog(
    				null,
    				null,
    				PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_END_DATE,							
    				null);
    		JSFUtilities.updateComponent("cnfGeneralMsgBusinessValidation"); 
    		JSFUtilities.executeJavascriptFunction("cnfwGeneralMsgBusinessValidation.show()");
    		return;
    	}
		
		//Validations of end dare not less than initial date
		if(issuerTO.getIssuerRequestTO().getEndRequestDate().before(issuerTO.getIssuerRequestTO().getInitiaRequestDate())){
    		JSFUtilities.addContextMessage("frmGeneral:calEndDate",
    				FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_END_DATE), 
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_SEARCH_END_DATE_GREAT_SYS_DATE));
    		showMessageOnDialog(
    				null,
    				null,
    				PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_END_DATE,							
    				null);
    		JSFUtilities.updateComponent("cnfGeneralMsgBusinessValidation"); 
    		JSFUtilities.executeJavascriptFunction("cnfwGeneralMsgBusinessValidation.show()");
    		return;
    	}
	
	}
	/**
	 * Search issuer block - unblock request by filters.
	 */
	@LoggerAuditWeb
	public void searchIssuerRequests() {
		try{						
			hideDialogsListener(null);
			JSFUtilities.setValidViewComponentAndChildrens("frmGeneral");
			if(userInfo.getUserAccountSession().isDepositaryInstitution())
	    		validateDateSearch();
	    	firsRegisterDataTable = 0;
    		List<Integer> lstIssuerRequestTypes = new ArrayList<Integer>();
    		lstIssuerRequestTypes.add(IssuerRequestType.BLOCK.getCode());
    		lstIssuerRequestTypes.add(IssuerRequestType.UNBLOCK.getCode());   		
    		issuerTO.getIssuerRequestTO().setLstIssuerRequestTypes(lstIssuerRequestTypes);    		
			issuerRequestDataModel = new IssuerRequestDataModel(issuerServiceFacade.getListIssuerRequestsBlockUnblockByFiltersServiceFacade(issuerTO));
			if(issuerRequestDataModel.getRowCount()>0)		
				setTrueValidButtons();			
			issuerRequest = new IssuerRequest();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
	}
	/**
	 * Sets the true valid buttons.
	 */
	public void setTrueValidButtons(){
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();			
		if(userPrivilege.getUserAcctions().isConfirm()){
			privilegeComponent.setBtnConfirmView(true);
		}
		if(userPrivilege.getUserAcctions().isReject()){
			privilegeComponent.setBtnRejectView(true);
		}
		userPrivilege.setPrivilegeComponent(privilegeComponent);		
	}
	
	/**
	 * validate Operation Type.
	 *
	 * @param operationType id operation type
	 */
	public void validateOperationType(Integer operationType)
	{
		if(Validations.validateIsNotNullAndPositive(operationType)){
			setViewOperationType(operationType);
		}else{
			setViewOperationType(ViewOperationsType.DETAIL.getCode());
		}
	}
	
	/**
	 * Find Issuer Request Detail.
	 *
	 * @param idIssuerRequestPk id issuer request
	 * @return object Issuer Request
	 * @throws ServiceException Service Exception
	 */
	public IssuerRequest findIssuerRequestDetail(Long idIssuerRequestPk)  throws ServiceException
	{
		IssuerRequestTO issuerRequestTO = new IssuerRequestTO();
		issuerRequestTO.setIdIssuerRequestPk(idIssuerRequestPk);		
		return issuerServiceFacade.findIssuerRequestByIdServiceFacade(issuerRequestTO);
	}
	
	/**
	 * load List Motive Types.
	 *
	 * @throws ServiceException Service Exception
	 */
	public void loadListMotiveTypes() throws ServiceException
	{
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		if(issuerRequest.getRequestType().equals(IssuerRequestType.BLOCK.getCode())){
			filterParameterTable.setMasterTableFk(MasterTableType.ISSUER_BLOCK_REQUEST_MOTIVE_TYPE.getCode());		
			setLstIssuerMotiveTypes(generalParametersFacade.getListParameterTableServiceBean(filterParameterTable));
		}else if(issuerRequest.getRequestType().equals(IssuerRequestType.UNBLOCK.getCode())) {
			filterParameterTable.setMasterTableFk(MasterTableType.ISSUER_UNBLOCK_REQUEST_MOTIVE_TYPE.getCode());		
			setLstIssuerMotiveTypes(generalParametersFacade.getListParameterTableServiceBean(filterParameterTable));
		}
	}
	
	/**
	 * Get File Sustent.
	 *
	 * @return the file sustent
	 * @throws Exception Exception
	 */
	public void getFileSustent()throws Exception
	{
		InputStream inputStream;
		StreamedContent streamedContentFile;
		for (IssuerFile fileSustent : issuerRequest.getIssuerFiles()) {			
			inputStream = new ByteArrayInputStream(fileSustent.getDocumentFile());			
			streamedContentFile = new DefaultStreamedContent(inputStream, null, fileSustent.getFilename());
			setStreamedContentFile(streamedContentFile);
			inputStream.close();
		}			
	}
	
	/**
	 * Detail issuer request.
	 *
	 * @param idIssuerRequestPk the id issuer request pk
	 * @param operationType the operation type
	 * @return the string
	 */
	@LoggerAuditWeb
	public String detailIssuerRequest(Long idIssuerRequestPk, Integer operationType){
		try {
			validateOperationType(operationType);
			issuerRequest =findIssuerRequestDetail(idIssuerRequestPk);
			loadListMotiveTypes();		
			getFileSustent();			
			return WINDOW_BLOCK_ISSUER_DETAIL;		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}

	/**
	 * Validate Selected Issuer.
	 */
	public void validateSelectedIssuer()
	{
		if(Validations.validateIsNull(issuerRequest.getIssuer().getIdIssuerPk())){
			showMessageOnDialog(null, null, PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_ISSUER, null);
			JSFUtilities.showValidationDialog();
			issuerRequest.setRequestType(null);
			setLstIssuerMotiveTypes(null);
			return;
		}
	}
	
	/**
	 * Validate Registered State Issuer.
	 */
	public void validateRegisteredStateIssuer()
	{
		if(!issuerRequest.getIssuer().getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){
			showMessageOnDialog(null, null, PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_CONFIRM_ISSUER_NOT_REGISTERED, null);
			JSFUtilities.showValidationDialog();
			issuerRequest.setRequestType(null);
			setLstIssuerMotiveTypes(null);
			return;
		}
	}
	
	/**
	 * Load Issuer Block Request Motive.
	 *
	 * @param filterParameterTable object filter Parameter Table
	 * @throws ServiceException Service Exception
	 */
	public void loadIssuerBlockRequestMotive(ParameterTableTO filterParameterTable)throws ServiceException
	{
		filterParameterTable.setMasterTableFk(MasterTableType.ISSUER_BLOCK_REQUEST_MOTIVE_TYPE.getCode());		
		setLstIssuerMotiveTypes(generalParametersFacade.getListParameterTableServiceBean(filterParameterTable));
	}
	
	/**
	 * Validate Blocked State Issuer.
	 */
	public void validateBlockedStateIssuer()
	{
		if(!issuerRequest.getIssuer().getStateIssuer().equals(IssuerStateType.BLOCKED.getCode())){
			showMessageOnDialog(null, null, PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_CONFIRM_ISSUER_NOT_BLOCKED, null);
			JSFUtilities.showValidationDialog();
			issuerRequest.setRequestType(null);
			setLstIssuerMotiveTypes(null);
			return;
		}
	}
	
	/**
	 * Load Issuer unBlock Request Motive.
	 *
	 * @param filterParameterTable object filter Parameter Table
	 * @throws ServiceException Service Exception
	 */
	public void loadIssuerUnBlockRequestMotive(ParameterTableTO filterParameterTable)throws ServiceException
	{
		filterParameterTable.setMasterTableFk(MasterTableType.ISSUER_UNBLOCK_REQUEST_MOTIVE_TYPE.getCode());		
		setLstIssuerMotiveTypes(generalParametersFacade.getListParameterTableServiceBean(filterParameterTable));
	}
	
	/**
	 * on Change Request Type.
	 *
	 * @throws ServiceException Service Exception
	 */
	public void onChangeRequestType() throws ServiceException{		
		hideDialogsListener(null);
		validateSelectedIssuer();	
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		if(issuerRequest.getRequestType().equals(IssuerRequestType.BLOCK.getCode())){
			validateRegisteredStateIssuer();			
			loadIssuerBlockRequestMotive(filterParameterTable);			
		}else if(issuerRequest.getRequestType().equals(IssuerRequestType.UNBLOCK.getCode())) {
			validateBlockedStateIssuer();
			loadIssuerUnBlockRequestMotive(filterParameterTable);
		}
	}
	
	/**
	 * Create Objects Issuer Helper Handler.
	 */
	public void createObjectsIssuerHelperHandler()
	{
		lstIssuerMotiveTypes = new ArrayList<ParameterTable>();
		lstIssuerFileSustent = new ArrayList<IssuerFile>();
	}
	
	/**
	 * Validate if ISSUER has previous request.
	 *
	 * @return object Issuer Request
	 * @throws ServiceException the service exception
	 */
	public IssuerRequest findIssuerRequest() throws ServiceException
	{
		IssuerRequestTO issuerRequestTO = new IssuerRequestTO();
		issuerRequestTO.setIdIssuerPk(issuerRequest.getIssuer().getIdIssuerPk());
		issuerRequestTO.setStateIssuerRequest(IssuerRequestStateType.REGISTERED.getCode());		
		return  issuerServiceFacade.validateExistsIssuerRequestServiceFacade(issuerRequestTO);
	}
	
	/**
	 * View Message There Is Pending Request To Confirm.
	 *
	 * @param issuerRequestResult object Issuer Request
	 */
	public void viewMessageThereIsPendingRequestToConfirm(IssuerRequest issuerRequestResult)
	{
		Object[] arrBodyData = {issuerRequestResult.getRequestTypeDescription(),issuerRequestResult.getIssuer().getCodeMnemonic()};
		showMessageOnDialog(
				null,
				null,
				PropertiesConstants.ERROR_ADM_ISSUER_BLOCK_PREVIOUS_REQUEST,							
				arrBodyData); 
		JSFUtilities.updateComponent("cnfGeneralMsgBusinessValidation"); 
		JSFUtilities.executeJavascriptFunction("cnfwGeneralMsgBusinessValidation.show()");
		return;
	}
	
	/**
	 * Issuer Helper Handler.
	 */
	@LoggerAuditWeb
	public void beforeIssuerHelperHandler() {
	if (Validations.validateIsNotNullAndNotEmpty(issuerRequest.getIssuer().getIdIssuerPk()))
		{
			try {
			 	
				hideDialogsListener(null);
				createObjectsIssuerHelperHandler();
				IssuerRequest issuerRequestResult = findIssuerRequest();				
				if(Validations.validateIsNotNull(issuerRequestResult))				
					viewMessageThereIsPendingRequestToConfirm(issuerRequestResult);			
				if(issuerRequest.getIssuer().getStateIssuer().equals(IssuerStateType.BLOCKED.getCode())){
					issuerRequest.setRequestType(IssuerRequestType.UNBLOCK.getCode());		
				}else if(issuerRequest.getIssuer().getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){
					issuerRequest.setRequestType(IssuerRequestType.BLOCK.getCode());	
				}	 
				loadListMotiveTypes();
			} catch (Exception e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
		}else{
			issuerRequest.setRequestType(null);
			issuerRequest.setRequestMotive(null);
			lstIssuerMotiveTypes=new ArrayList<ParameterTable>();
		}		
	}
	
	/**
	 * Search issuer by doc type and number.
	 */
	public void searchIssuerByDocTypeAndNumber(){
		hideDialogsListener(null);
		try{			
			if(Validations.validateIsNotNullAndPositive(issuerRequest.getIssuer().getDocumentType()) &&
					Validations.validateIsNotNullAndNotEmpty(issuerRequest.getIssuer().getDocumentNumber())){	
				
					Issuer iss = issuerServiceFacade.findIssuerByFiltersServiceFacade(issuerRequest.getIssuer());
					if(Validations.validateIsNull(iss)){
						showMessageOnDialog(null, null, PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_ISSUER_NOTFOUND_BY_DOC, null);
						JSFUtilities.showValidationDialog();
						issuerRequest.setIssuer(new Issuer());						
					}else{
						issuerRequest.setIssuer(iss);					
					}					
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
			
	/**
	 * Upload sustent files.
	 *
	 * @param eventFile the event file
	 */
	public void uploadSustentFiles(FileUploadEvent eventFile){
		hideDialogsListener(null);
		try{			
			String fDisplayName=fUploadValidateFile(eventFile.getFile(), null,null, super.getfUploadFileDocumentsTypes());			 
			 if(fDisplayName!=null){
				 issuerFileNameDisplay=fDisplayName;
			 }			
			IssuerFile fileSustent = new IssuerFile();
			fileSustent = new IssuerFile();
			fileSustent.setRequestFileType(RequestIssuerFileType.REGISTER.getCode());
			fileSustent.setFilename(eventFile.getFile().getFileName());
			fileSustent.setDocumentFile(eventFile.getFile().getContents());
			fileSustent.setStateFile(IssuerFileStateType.REGISTERED.getCode());
			fileSustent.setRegistryUser(String.valueOf(userInfo.getUserAccountSession().getUserName()));
			fileSustent.setRegistryDate(CommonsUtilities.currentDate());
			fileSustent.setDocumentType(IssuerFileType.ISSUER_DEFAULT_DOCUMENT.getCode());
			
			if(lstIssuerFileSustent == null ) 
				lstIssuerFileSustent = new ArrayList<IssuerFile>(); 			
			lstIssuerFileSustent.add(fileSustent);
			
			InputStream inputStream = new ByteArrayInputStream(fileSustent.getDocumentFile());			
			StreamedContent streamedContentFile = new DefaultStreamedContent(inputStream, null, fileSustent.getFilename());
			setStreamedContentFile(streamedContentFile);
			inputStream.close();
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		
	}
	
	/**
	 * clean All.
	 */
	public void cleanAll(){
		hideDialogsListener(null);		
		issuerRequest = new IssuerRequest();
		issuerRequest.setIssuer(new Issuer());
		lstIssuerFileSustent = null;
		lstIssuerMotiveTypes = new ArrayList<ParameterTable>();
		issuerFileNameDisplay = null;
		JSFUtilities.resetViewRoot();	
	}
	
	/**
	 * find Issuer has Previous Request.
	 *
	 * @return object Issuer Request
	 */
	public IssuerRequest findIssuerhasPreviousRequest()
	{
		IssuerRequestTO issuerRequestTO = new IssuerRequestTO();
		issuerRequestTO.setIdIssuerPk(issuerRequest.getIssuer().getIdIssuerPk());
		issuerRequestTO.setStateIssuerRequest(IssuerRequestStateType.REGISTERED.getCode());	
		return  issuerServiceFacade.validateExistsIssuerRequestServiceFacade(issuerRequestTO);
	}
	
	/**
	 * view Message There Pending Request To Confirm.
	 *
	 * @param issuerRequestResult object Issuer Request
	 */
	public void viewMessageTherePendingRequestToConfirm(IssuerRequest issuerRequestResult)
	{
		//If exists previous issuer request, show error and return
		Object[] arrBodyData = {issuerRequestResult.getRequestTypeDescription(),issuerRequestResult.getIssuer().getCodeMnemonic()};
		showMessageOnDialog(null,null,PropertiesConstants.ERROR_ADM_ISSUER_BLOCK_PREVIOUS_REQUEST,arrBodyData);
	
		JSFUtilities.updateComponent("cnfGeneralMsgBusinessValidation"); 
		JSFUtilities.executeJavascriptFunction("PF('cnfwGeneralMsgBusinessValidation').show()");
		return;
	}
	
	/**
	 * view Message Confirm Issuer Request Register.
	 */
	public void viewMessageConfirmIssuerRequestRegister()
	{
		Object[] arrBodyData = {issuerRequest.getRequestTypeDescription()};
		showMessageOnDialog(
				PropertiesConstants.LBL_CONFIRM_HEADER_REGISTER,
				null,				
				PropertiesConstants.LBL_CONFIRM_ISSUER_REQUEST_REGISTER,							
				arrBodyData);
		JSFUtilities.showComponent("cnfIssuerBlockRequest");
	}
	
	/**
	 *  before Save Issuer Block Request.
	 */
	public void beforeSaveIssuerBlockRequest(){
		hideDialogsListener(null);
		int countValidationErrors = 0;
		
		if(Validations.validateIsNull(issuerRequest.getIssuer().getIdIssuerPk())){
    		JSFUtilities.addContextMessage("frmIssuerBlockRequest:issuerCodeHelper",FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_ISSUER), 
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_ISSUER));
    		countValidationErrors++;
    	}
		
		if(Validations.validateIsNullOrNotPositive(issuerRequest.getIssuer().getDocumentType())){
    		JSFUtilities.addContextMessage("frmIssuerBlockRequest:issuerDocumentType",FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_ISSUER_DOCUMENT_TYPE), 
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_ISSUER_DOCUMENT_TYPE));
    		countValidationErrors++;
    	}
		
		if(Validations.validateIsNullOrEmpty(issuerRequest.getIssuer().getDocumentNumber())){
    		JSFUtilities.addContextMessage("frmIssuerBlockRequest:issuerDocumentNumber",FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_ISSUER_DOCUMENT_NUMBER), 
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_ISSUER_DOCUMENT_NUMBER));
    		countValidationErrors++;
    	}
		
		if(Validations.validateIsNullOrNotPositive(issuerRequest.getRequestType())){
    		JSFUtilities.addContextMessage("frmIssuerBlockRequest:cboRequestType",FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_REQUEST_TYPE), 
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_REQUEST_TYPE));
    		countValidationErrors++;
    	}
		
		if(Validations.validateIsNullOrNotPositive(issuerRequest.getRequestMotive())){
    		JSFUtilities.addContextMessage("frmIssuerBlockRequest:cboMotive",
    				FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_MOTIVE),
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_MOTIVE));
    		countValidationErrors++;
    	}
		
		if(Validations.validateListIsNullOrEmpty(lstIssuerFileSustent)|| !Validations.validateIsNotNullAndPositive(lstIssuerFileSustent.size())){
    		JSFUtilities.addContextMessage("frmIssuerBlockRequest:fuplIssuerFile",
    				FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_FILES_SUSTENT), 
    				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_FILES_SUSTENT));
    		countValidationErrors++;
    	}
		
		//If validations greater than 0, show error message
		if(countValidationErrors > 0) {
			String message=PropertiesUtilities.getMessage(PropertiesConstants.MESSAGE_DATA_REQUIRED);
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),message);
    		JSFUtilities.showSimpleValidationDialog();
    		return;
    	}
		IssuerRequest issuerRequestResult = findIssuerhasPreviousRequest();		
		if(Validations.validateIsNotNull(issuerRequestResult))
			viewMessageTherePendingRequestToConfirm(issuerRequestResult);
		else
			viewMessageConfirmIssuerRequestRegister();
	}
	
	/**
	 * set Issuer And IssuerRequest In IssuerFile.
	 */
	public void setIssuerAndIssuerRequestInIssuerFile()
	{
		for (IssuerFile issuerFile : lstIssuerFileSustent) {
			issuerFile.setIssuerRequest(issuerRequest);
			issuerFile.setIssuer(issuerRequest.getIssuer());
		}
	}
	
	/**
	 * Save Issuer Request.
	 *
	 * @throws ServiceException Service Exception
	 */
	public void saveIssuerRequest()throws ServiceException
	{
		issuerRequest.setIssuerFiles(lstIssuerFileSustent);
		issuerRequest.setState(IssuerRequestStateType.REGISTERED.getCode());
		issuerRequest.setRegistryDate(CommonsUtilities.currentDate());
		issuerRequest.setRegistryUser(userInfo.getUserAccountSession().getUserName());		
		issuerRequest = issuerServiceFacade.saveIssuerBlockRequestServiceFacade(issuerRequest);
	}
	
	/**
	 *  Process Notification.
	 */
	public void processNotification()
	{
		BusinessProcess businessProcessNotification = new BusinessProcess();
		if(IssuerRequestType.BLOCK.getCode().equals(issuerRequest.getRequestType())) {
			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.ISSUER_BLOCK_REGISTER.getCode());
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
					businessProcessNotification,issuerRequest.getIssuer().getIdIssuerPk(), null);
		} else if(IssuerRequestType.UNBLOCK.getCode().equals(issuerRequest.getRequestType())) {
			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.ISSUER_UNBLOCK_REGISTER.getCode());
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
					businessProcessNotification,issuerRequest.getIssuer().getIdIssuerPk(), null);
		}
	}
	
	/**
	 * view Message Success Issuer Request Register().
	 */
	public void viewMessageSuccessIssuerRequestRegister()
	{
		Object[] arrBodyData = {issuerRequest.getRequestTypeDescription(),String.valueOf(issuerRequest.getRequestNumber())};
		showMessageOnDialog(null, null,PropertiesConstants.LBL_SUCCESS_ISSUER_REQUEST_REGISTER,arrBodyData);
	}
	
	/**
	 * Save issuer block request.
	 */
	@LoggerAuditWeb
	public void saveIssuerBlockRequest(){
		try {
			JSFUtilities.executeJavascriptFunction("cnfwIssuerBlockRequest.hide()");  
			JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
			JSFUtilities.hideGeneralDialogues();
			setIssuerAndIssuerRequestInIssuerFile();
			saveIssuerRequest();
			processNotification();
			viewMessageSuccessIssuerRequestRegister();			
			cleanSave();
			setViewOperationType(ViewOperationsType.CONSULT.getCode());
			cleanSearch();
			JSFUtilities.showComponent("cnfEndTransactionIssuer");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clean Search.
	 */
	public void cleanSearch()
	{
		cleanDataModel();
		validateUserAccountSession();
	}
	
	/**
	 * clean Save.
	 */
	public void cleanSave()
	{
		issuerRequest = new IssuerRequest();
		issuerRequest.setIssuer(new Issuer());
		lstIssuerFileSustent = null;
		lstIssuerMotiveTypes = new ArrayList<ParameterTable>();
		issuerFileNameDisplay = null;
	}
	
	/**
	 * Removes the file sustent.
	 *
	 * @param issuerFile the issuer file
	 */
	public void removeFileSustent(IssuerFile issuerFile){
		lstIssuerFileSustent.remove(issuerFile);
		issuerFileNameDisplay = null;
	}

	/**
	 * Sets the buttons listener.
	 */
	public void setButtonsListener(){
		
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		
		if(getViewOperationType().equals(ViewOperationsType.DETAIL.getCode()) || 
				getViewOperationType().equals(ViewOperationsType.CONSULT.getCode())){
			privilegeComponent.setBtnConfirmView(false);
			privilegeComponent.setBtnRejectView(false);
		}else if (getViewOperationType().equals(ViewOperationsType.CONFIRM.getCode())){
			privilegeComponent.setBtnConfirmView(true);
		}else if (getViewOperationType().equals(ViewOperationsType.REJECT.getCode())){
			privilegeComponent.setBtnRejectView(true);
		}
		
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}
	
	/**
	 * Sets the valid buttons listener.
	 */
	public void setValidButtonsListener(){
		
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		
		if(userPrivilege.getUserAcctions().isConfirm()){
			privilegeComponent.setBtnConfirmView(true);
		}
		if(userPrivilege.getUserAcctions().isReject()){
			privilegeComponent.setBtnRejectView(true);
		}
		
		userPrivilege.setPrivilegeComponent(privilegeComponent);			

	}
	
	/**
	 * Gets the issuer request.
	 *
	 * @return the issuer request
	 */
	public IssuerRequest getIssuerRequest() {
		return issuerRequest;
	}

	/**
	 * Sets the issuer request.
	 *
	 * @param issuerRequest the new issuer request
	 */
	public void setIssuerRequest(IssuerRequest issuerRequest) {
		this.issuerRequest = issuerRequest;
	}

	/**
	 * Gets the lst issuer block request types.
	 *
	 * @return the lst issuer block request types
	 */
	public List<ParameterTable> getLstIssuerBlockRequestTypes() {
		return lstIssuerBlockRequestTypes;
	}

	/**
	 * Sets the lst issuer block request types.
	 *
	 * @param lstIssuerBlockRequestTypes the new lst issuer block request types
	 */
	public void setLstIssuerBlockRequestTypes(
			List<ParameterTable> lstIssuerBlockRequestTypes) {
		this.lstIssuerBlockRequestTypes = lstIssuerBlockRequestTypes;
	}

	/**
	 * Gets the lst issuer motive types.
	 *
	 * @return the lst issuer motive types
	 */
	public List<ParameterTable> getLstIssuerMotiveTypes() {
		return lstIssuerMotiveTypes;
	}

	/**
	 * Sets the lst issuer motive types.
	 *
	 * @param lstIssuerMotiveTypes the new lst issuer motive types
	 */
	public void setLstIssuerMotiveTypes(List<ParameterTable> lstIssuerMotiveTypes) {
		this.lstIssuerMotiveTypes = lstIssuerMotiveTypes;
	}

	/**
	 * Gets the lst issuer file sustent.
	 *
	 * @return the lst issuer file sustent
	 */
	public List<IssuerFile> getLstIssuerFileSustent() {
		return lstIssuerFileSustent;
	}

	/**
	 * Sets the lst issuer file sustent.
	 *
	 * @param lstIssuerFileSustent the new lst issuer file sustent
	 */
	public void setLstIssuerFileSustent(List<IssuerFile> lstIssuerFileSustent) {
		this.lstIssuerFileSustent = lstIssuerFileSustent;
	}

	/**
	 * Gets the lst issuer document types.
	 *
	 * @return the lst issuer document types
	 */
	public List<ParameterTable> getLstIssuerDocumentTypes() {
		return lstIssuerDocumentTypes;
	}

	/**
	 * Sets the lst issuer document types.
	 *
	 * @param lstIssuerDocumentTypes the new lst issuer document types
	 */
	public void setLstIssuerDocumentTypes(List<ParameterTable> lstIssuerDocumentTypes) {
		this.lstIssuerDocumentTypes = lstIssuerDocumentTypes;
	}

	/**
	 * Gets the issuer to.
	 *
	 * @return the issuer to
	 */
	public IssuerTO getIssuerTO() {
		return issuerTO;
	}

	/**
	 * Sets the issuer to.
	 *
	 * @param issuerTO the new issuer to
	 */
	public void setIssuerTO(IssuerTO issuerTO) {
		this.issuerTO = issuerTO;
	}

	/**
	 * Gets the lst issuer history states.
	 *
	 * @return the lst issuer history states
	 */
	public List<IssuerHistoryState> getLstIssuerHistoryStates() {
		return lstIssuerHistoryStates;
	}

	/**
	 * Sets the lst issuer history states.
	 *
	 * @param lstIssuerHistoryStates the new lst issuer history states
	 */
	public void setLstIssuerHistoryStates(List<IssuerHistoryState> lstIssuerHistoryStates) {
		this.lstIssuerHistoryStates = lstIssuerHistoryStates;
	}

	/**
	 * Gets the lst issuer request states.
	 *
	 * @return the lst issuer request states
	 */
	public List<ParameterTable> getLstIssuerRequestStates() {
		return lstIssuerRequestStates;
	}

	/**
	 * Sets the lst issuer request states.
	 *
	 * @param lstIssuerRequestStates the new lst issuer request states
	 */
	public void setLstIssuerRequestStates(List<ParameterTable> lstIssuerRequestStates) {
		this.lstIssuerRequestStates = lstIssuerRequestStates;
	}

	/**
	 * Checks if is ind disable confirn reject.
	 *
	 * @return true, if is ind disable confirn reject
	 */
	public boolean isIndDisableConfirnReject() {
		return indDisableConfirnReject;
	}

	/**
	 * Sets the ind disable confirn reject.
	 *
	 * @param indDisableConfirnReject the new ind disable confirn reject
	 */
	public void setIndDisableConfirnReject(boolean indDisableConfirnReject) {
		this.indDisableConfirnReject = indDisableConfirnReject;
	}

	/**
	 * Checks if is ind disable accept motive reject.
	 *
	 * @return true, if is ind disable accept motive reject
	 */
	public boolean isIndDisableAcceptMotiveReject() {
		return indDisableAcceptMotiveReject;
	}

	/**
	 * Sets the ind disable accept motive reject.
	 *
	 * @param indDisableAcceptMotiveReject the new ind disable accept motive reject
	 */
	public void setIndDisableAcceptMotiveReject(boolean indDisableAcceptMotiveReject) {
		this.indDisableAcceptMotiveReject = indDisableAcceptMotiveReject;
	}

	/**
	 * Gets the issuer request data model.
	 *
	 * @return the issuer request data model
	 */
	public IssuerRequestDataModel getIssuerRequestDataModel() {
		return issuerRequestDataModel;
	}

	/**
	 * Sets the issuer request data model.
	 *
	 * @param issuerRequestDataModel the new issuer request data model
	 */
	public void setIssuerRequestDataModel(IssuerRequestDataModel issuerRequestDataModel) {
		this.issuerRequestDataModel = issuerRequestDataModel;
	}

	/**
	 * Checks if is ind selected other motive reject.
	 *
	 * @return true, if is ind selected other motive reject
	 */
	public boolean isIndSelectedOtherMotiveReject() {
		return indSelectedOtherMotiveReject;
	}

	/**
	 * Sets the ind selected other motive reject.
	 *
	 * @param indSelectedOtherMotiveReject the new ind selected other motive reject
	 */
	public void setIndSelectedOtherMotiveReject(boolean indSelectedOtherMotiveReject) {
		this.indSelectedOtherMotiveReject = indSelectedOtherMotiveReject;
	}

	/**
	 * Gets the lst issuer request reject motives.
	 *
	 * @return the lst issuer request reject motives
	 */
	public List<ParameterTable> getLstIssuerRequestRejectMotives() {
		return lstIssuerRequestRejectMotives;
	}

	/**
	 * Sets the lst issuer request reject motives.
	 *
	 * @param lstIssuerRequestRejectMotives the new lst issuer request reject motives
	 */
	public void setLstIssuerRequestRejectMotives(
			List<ParameterTable> lstIssuerRequestRejectMotives) {
		this.lstIssuerRequestRejectMotives = lstIssuerRequestRejectMotives;
	}
	
	/**
	 * Gets the issuer file name display.
	 *
	 * @return the issuer file name display
	 */
	public String getIssuerFileNameDisplay() {
		return issuerFileNameDisplay;
	}

	/**
	 * Sets the issuer file name display.
	 *
	 * @param issuerFileNameDisplay the new issuer file name display
	 */
	public void setIssuerFileNameDisplay(String issuerFileNameDisplay) {
		this.issuerFileNameDisplay = issuerFileNameDisplay;
	}
	
	/**
	 * Checks if is allow search all.
	 *
	 * @return true, if is allow search all
	 */
	public boolean isAllowSearchAll() {
		return allowSearchAll;
	}

	/**
	 * Sets the allow search all.
	 *
	 * @param allowSearchAll the new allow search all
	 */
	public void setAllowSearchAll(boolean allowSearchAll) {
		this.allowSearchAll = allowSearchAll;
	}
	
	/**
	 * Gets the streamed content file.
	 *
	 * @return the streamed content file
	 */
	public StreamedContent getStreamedContentFile() {
		return streamedContentFile;
	}
	
	/**
	 * Sets the streamed content file.
	 *
	 * @param streamedContentFile the new streamed content file
	 */
	public void setStreamedContentFile(StreamedContent streamedContentFile) {
		this.streamedContentFile = streamedContentFile;
	}	
	
}
