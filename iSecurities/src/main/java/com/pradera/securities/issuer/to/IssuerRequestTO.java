package com.pradera.securities.issuer.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class IssuerRequestTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class IssuerRequestTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id participant request pk. */
	private Long idIssuerRequestPk;
	
	/** The initia request date. */
	private Date initiaRequestDate;
	
	/** The end request date. */
	private Date endRequestDate;
	
	/** The state participant request. */
	private Integer stateIssuerRequest;
	
	/** The id participant pk. */
	private String idIssuerPk;
	
	/** The request type. */
	private Integer requestType;
	
	/** The request number. */
	private Long requestNumber;
	
	/** The lst participant request states. */
	private List<Integer> lstIssuerRequestStates;
	
	/** The lst participant request types. */
	private List<Integer> lstIssuerRequestTypes;

	/**
	 * Gets the id issuer request pk.
	 *
	 * @return the id issuer request pk
	 */
	public Long getIdIssuerRequestPk() {
		return idIssuerRequestPk;
	}

	/**
	 * Sets the id issuer request pk.
	 *
	 * @param idIssuerRequestPk the new id issuer request pk
	 */
	public void setIdIssuerRequestPk(Long idIssuerRequestPk) {
		this.idIssuerRequestPk = idIssuerRequestPk;
	}

	/**
	 * Gets the initia request date.
	 *
	 * @return the initia request date
	 */
	public Date getInitiaRequestDate() {
		return initiaRequestDate;
	}

	/**
	 * Sets the initia request date.
	 *
	 * @param initiaRequestDate the new initia request date
	 */
	public void setInitiaRequestDate(Date initiaRequestDate) {
		this.initiaRequestDate = initiaRequestDate;
	}

	/**
	 * Gets the state issuer request.
	 *
	 * @return the state issuer request
	 */
	public Integer getStateIssuerRequest() {
		return stateIssuerRequest;
	}

	/**
	 * Sets the state issuer request.
	 *
	 * @param stateIssuerRequest the new state issuer request
	 */
	public void setStateIssuerRequest(Integer stateIssuerRequest) {
		this.stateIssuerRequest = stateIssuerRequest;
	}


	/**
	 * Gets the request type.
	 *
	 * @return the request type
	 */
	public Integer getRequestType() {
		return requestType;
	}

	/**
	 * Sets the request type.
	 *
	 * @param requestType the new request type
	 */
	public void setRequestType(Integer requestType) {
		this.requestType = requestType;
	}

	/**
	 * Gets the request number.
	 *
	 * @return the request number
	 */
	public Long getRequestNumber() {
		return requestNumber;
	}

	/**
	 * Sets the request number.
	 *
	 * @param requestNumber the new request number
	 */
	public void setRequestNumber(Long requestNumber) {
		this.requestNumber = requestNumber;
	}

	/**
	 * Gets the lst issuer request states.
	 *
	 * @return the lst issuer request states
	 */
	public List<Integer> getLstIssuerRequestStates() {
		return lstIssuerRequestStates;
	}

	/**
	 * Sets the lst issuer request states.
	 *
	 * @param lstIssuerRequestStates the new lst issuer request states
	 */
	public void setLstIssuerRequestStates(List<Integer> lstIssuerRequestStates) {
		this.lstIssuerRequestStates = lstIssuerRequestStates;
	}

	/**
	 * Gets the lst issuer request types.
	 *
	 * @return the lst issuer request types
	 */
	public List<Integer> getLstIssuerRequestTypes() {
		return lstIssuerRequestTypes;
	}

	/**
	 * Sets the lst issuer request types.
	 *
	 * @param lstIssuerRequestTypes the new lst issuer request types
	 */
	public void setLstIssuerRequestTypes(List<Integer> lstIssuerRequestTypes) {
		this.lstIssuerRequestTypes = lstIssuerRequestTypes;
	}

	/**
	 * Gets the end request date.
	 *
	 * @return the end request date
	 */
	public Date getEndRequestDate() {
		return endRequestDate;
	}

	/**
	 * Sets the end request date.
	 *
	 * @param endRequestDate the new end request date
	 */
	public void setEndRequestDate(Date endRequestDate) {
		this.endRequestDate = endRequestDate;
	}

	/**
	 * Gets the id issuer pk.
	 *
	 * @return the id issuer pk
	 */
	public String getIdIssuerPk() {
		return idIssuerPk;
	}

	/**
	 * Sets the id issuer pk.
	 *
	 * @param idIssuerPk the new id issuer pk
	 */
	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}
	
	
	
}
