package com.pradera.securities.issuer.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.issuancesecuritie.IssuerRequest;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class IssuerRequestDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 08/04/2013
 */
public class IssuerRequestDataModel extends ListDataModel<IssuerRequest> implements SelectableDataModel<IssuerRequest>, Serializable{

	/**
	 * Instantiates a new participant request data model.
	 */
	public IssuerRequestDataModel(){
		
	}
	
	/**
	 * Instantiates a new participant request data model.
	 *
	 * @param data the data
	 */
	public IssuerRequestDataModel(List<IssuerRequest> data){
		super(data);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public IssuerRequest getRowData(String rowKey) {
		@SuppressWarnings("unchecked")
		List<IssuerRequest> lstIssuerRequest = (List<IssuerRequest>)getWrappedData();
        for(IssuerRequest IssuerRequest : lstIssuerRequest ) {  
            if(String.valueOf(IssuerRequest.getIdIssuerRequestPk()).equals(rowKey))
                return IssuerRequest;  
        }
		return null;
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(IssuerRequest participant) {
		return participant.getIdIssuerRequestPk();
	}		
}