package com.pradera.securities.issuer.view;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.component.calendar.Calendar;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.RegistryType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.to.LegalRepresentativeTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.extension.view.LegalRepresentativeHelperBean;
import com.pradera.core.component.utils.DocumentTO;
import com.pradera.core.component.utils.DocumentValidator;
import com.pradera.core.component.utils.PersonTO;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.InstitutionInformation;
import com.pradera.model.accounts.LegalRepresentative;
import com.pradera.model.accounts.LegalRepresentativeFile;
import com.pradera.model.accounts.LegalRepresentativeHistory;
import com.pradera.model.accounts.OfacPerson;
import com.pradera.model.accounts.PepPerson;
import com.pradera.model.accounts.PnaPerson;
import com.pradera.model.accounts.RepresentativeFileHistory;
import com.pradera.model.accounts.RepresentedEntity;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.EconomicActivityType;
import com.pradera.model.accounts.type.IssuerRequestRejectMotiveType;
import com.pradera.model.accounts.type.IssuerRequestStateType;
import com.pradera.model.accounts.type.IssuerRequestType;
import com.pradera.model.accounts.type.LegalRepresentativeStateType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.GeographicLocationStateType;
import com.pradera.model.generalparameter.type.GeographicLocationType;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.IssuerFile;
import com.pradera.model.issuancesecuritie.IssuerFileHistory;
import com.pradera.model.issuancesecuritie.IssuerHistory;
import com.pradera.model.issuancesecuritie.IssuerRequest;
import com.pradera.model.issuancesecuritie.type.IssuerDocumentType;
import com.pradera.model.issuancesecuritie.type.IssuerFileStateType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.RequestIssuerFileType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.securities.issuancesecuritie.facade.IssuanceSecuritiesServiceFacade;
import com.pradera.securities.issuancesecuritie.util.PropertiesConstants;
import com.pradera.securities.issuer.facade.IssuerServiceFacade;
import com.pradera.securities.issuer.to.IssuerRequestTO;
import com.pradera.securities.issuer.to.IssuerTO;

// TODO: Auto-generated Javadocg
/**
 * <ul>
 * <li>
 * Project iDepositary. Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ParticipantMgmtBean.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 08/02/2013
 */
@DepositaryWebBean
@LoggerCreateBean
public class IssuerModificationMgmtBean extends GenericBaseBean implements
		Serializable {

	private String WITH_COUPONS = "CON CUPONES";

	private String NO_COUPONS = "SIN CUPONES";
	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade;

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6594533496886277354L;

	/** The ind numeric second type document legal. */
	private boolean indNumericSecondTypeDocumentLegal;

	/** The lst second document type representative. */
	private List<ParameterTable> lstSecondDocumentTypeRepresentative;

	/** The lst second countries representative. */
	private List<GeographicLocation> lstSecondCountriesRepresentative;

	/** The max lenght second document number legal. */
	private int maxLenghtSecondDocumentNumberLegal;

	/** The ind representative modification. */
	private boolean indRepresentativeModification;

	/** The ind issuer differentiated. */
	private boolean indIssuerDifferentiated;

	/** The ind exists issuer history. */
	private boolean indExistsIssuerHistory;

	/** The legal representative helper bean. */
	@Inject
	private LegalRepresentativeHelperBean legalRepresentativeHelperBean;

	/** The lst cbo credit rating scales. */
	private List<ParameterTable> lstCboCreditRatingScales;

	/** The country residence. */
	@Inject
	@Configurable
	Integer countryResidence;

	/** The email legal representative. */
	private String emailLegalRepresentative;

	/** The lst legal representative history before. */
	private List<LegalRepresentativeHistory> lstLegalRepresentativeHistoryOrigin;

	/** The lst legal representative history before. */
	private List<LegalRepresentativeHistory> lstLegalRepresentativeHistoryBefore;

	/** The lst legal representative history after. */
	private List<LegalRepresentativeHistory> lstLegalRepresentativeHistoryAfter;

	/** The lst issuer attachment type. */
	private List<ParameterTable> lstIssuerAttachmentType;

	/** The issuer file name display. */
	private String issuerFileNameDisplay;

	/** The attach issuer file selected. */
	private Integer attachIssuerFileSelected;

	/** The representative file name display. */
	private String representativeFileNameDisplay;

	/** The lst representative file types. */
	private List<ParameterTable> lstRepresentativeFileTypes;

	/** The lst representative file history. */
	private List<RepresentativeFileHistory> lstRepresentativeFileHistory;

	/** Document Validate. */
	@Inject
	private DocumentValidator documentValidatorRegister;

	/** Document search. */
	private DocumentTO documentTORegister;

	/** The representative file type. */
	private Integer representativeFileType;

	/** The max lenght document number legal. */
	private int maxLenghtDocumentNumberLegal;

	/** The ind numeric type document legal. */
	private boolean indNumericTypeDocumentLegal;

	/** The lst document type representative result. */
	private List<ParameterTable> lstDocumentTypeRepresentativeResult;

	/** The lst issuer request reject motives. */
	private List<ParameterTable> lstIssuerRequestRejectMotives;

	/** The ind selected other motive reject. */
	private boolean indSelectedOtherMotiveReject;

	/** The ind disable accept motive reject. */
	private boolean indDisableAcceptMotiveReject;

	/** The issuer request. */
	private IssuerRequest issuerRequest;

	/** The lst represented entity. */
	private List<RepresentedEntity> lstRepresentedEntity;

	/** The lst legal representative. */
	private List<LegalRepresentative> lstLegalRepresentative;

	/** The legal representative history. */
	private LegalRepresentativeHistory legalRepresentativeHistory;

	/** The lst legal representative history. */
	private List<LegalRepresentativeHistory> lstLegalRepresentativeHistory;

	/** The pep person. */
	private PepPerson pepPerson;

	/** The lst representative class. */
	private List<ParameterTable> lstRepresentativeClass;

	/** The lst document type representative. */
	private List<ParameterTable> lstDocumentTypeRepresentative;

	/** The lst economic activity representative. */
	private List<ParameterTable> lstEconomicActivityRepresentative;

	/** The lst sex. */
	private List<ParameterTable> lstSex;

	/** The lst issuer request state. */
	private List<ParameterTable> lstIssuerRequestState;

	/** The ind representative natural person. */
	private boolean indRepresentativeNaturalPerson;

	/** The ind representative juridic person. */
	private boolean indRepresentativeJuridicPerson;

	/** The flag cmb representative class. */
	private boolean flagCmbRepresentativeClass;

	/** The ind representative resident. */
	private boolean indRepresentativeResident;

	/** The disabled country resident representative. */
	private boolean disabledCountryResidentRepresentative;

	/** The ind representative is pep. */
	private boolean indRepresentativeIsPEP;

	/** The ind copy legal representative. */
	private boolean indCopyLegalRepresentative;

	/** The lst person type. */
	private List<ParameterTable> lstPersonType;

	/** The lst boolean. */
	private List<BooleanType> lstBoolean;

	/** The lst postal provinces representative. */
	private List<GeographicLocation> lstPostalProvincesRepresentative;

	/** The lst postal districts representative. */
	private List<GeographicLocation> lstPostalDistrictsRepresentative;

	/** The lst legal provinces representative. */
	private List<GeographicLocation> lstLegalProvincesRepresentative;

	/** The lst legal districts representative. */
	private List<GeographicLocation> lstLegalDistrictsRepresentative;
	
	private List<InvestorTypeModelTO> listInvestorType;

	/** The issuer. */
	private Issuer issuer;

	/** The issuer origin. */
	private Issuer issuerOrigin;

	/** The lst countries. */
	private List<GeographicLocation> lstCountries;

	/** The lst Department. */
	private List<GeographicLocation> lstDepartments;

	/** The lst provinces. */
	private List<GeographicLocation> lstProvinces;

	/** The lst districts. */
	private List<GeographicLocation> lstDistricts;

	/** The lst participant document type. */
	private List<ParameterTable> lstIssuerDocumentType;

	/** The lst participant document type for search. */
	private List<ParameterTable> lstIssuerDocumentTypeForSearch;

	/** The lst economic sector. */
	private List<ParameterTable> lstEconomicSector;

	/** The lst economic sector. */
	private List<ParameterTable> lstOfferType;

	/** The lst economic sector for search. */
	private List<ParameterTable> lstEconomicSectorForSearch;

	/** The lst economic activity. */
	private List<ParameterTable> lstEconomicActivity;

	/** The lst economic activity for search. */
	private List<ParameterTable> lstEconomicActivityForSearch;

	/** The lst participant state. */
	private List<ParameterTable> lstIssuerState;

	/** The lst participant state for search. */
	private List<ParameterTable> lstIssuerStateForSearch;

	/** The lst society type. */
	private List<ParameterTable> lstSocietyType;

	/** The lst currency. */
	private List<ParameterTable> lstCurrency;

	/** The lst category pep. */
	private List<ParameterTable> lstCategoryPep;

	/** The lst role pep. */
	private List<ParameterTable> lstRolePep;

	/** The participant data model. */
	private IssuerDataModel issuerDataModel;

	/** The participant to. */
	private IssuerTO issuerTO;

	/** The id participant pk. */
	private String idIssuerPk;

	/** The state participant. */
	private Integer stateIssuer;

	/** The participant files. */
	private List<IssuerFile> lstIssuerFilesOrigin;

	/** The participant files. */
	private List<IssuerFile> lstIssuerFiles;

	/** The id represented entity pk. */
	private Long idRepresentedEntityPk;

	/** The represented entity. */
	private RepresentedEntity representedEntity;

	/** Document Document Size. */
	private String fUploadFileDocumentsSize = "16777216";// 15MB en bytes;

	/** The size document display. */
	private String fUploadFileSizeDocumentsDisplay = "15000";

	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;

	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;

	/** The participant service facade. */
	@EJB
	IssuerServiceFacade issuerServiceFacade;

	/** The user info. */
	@Inject
	UserInfo userInfo;

	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;

	@Inject
	BatchProcessServiceFacade batchProcessServiceFacade;

	/** The ind issuer depositary contract. */
	private boolean indIssuerDepositaryContract;

	/** The ind inscription stock exchange. */
	private boolean indInscriptionStockExchange;

	/** The ind issuer differentiated. */
	private boolean indIssuerSIRTEXnegociate;

	/** Lista de clases de valor para mostrar */
	private List<ParameterTable> listSecurityClass;

	/** Lista de monedas */
	private List<ParameterTable> listCurrency;

	/** Parametros para modificar valores de acuerdo al emisor */
	private Integer securityClassSelected;

	/** Moneda */
	private Integer currencySelected;

	/** Tipo de persona */
	private Integer personTypeSelected;
	
	/** Con cupones y sin cupones */
	private Integer withCouponsSelected;

	/** Lista de tipo de persona */
	private List<ParameterTable> listPersonType;
	

	private List<ParameterTable> listBooleanValues;
		
	private String descriptionSocietyType = "";

	private String descriptionSocietyTypeIssuer = "";

	/** Fecha de emmision */
	private Date issuanceDateSelected;


	public Integer getSecurityClassSelected() {
		return securityClassSelected;
	}

	public void setSecurityClassSelected(Integer securityClassSelected) {
		this.securityClassSelected = securityClassSelected;
	}

	public Integer getCurrencySelected() {
		return currencySelected;
	}

	public void setCurrencySelected(Integer currencySelected) {
		this.currencySelected = currencySelected;
	}

	public Integer getPersonTypeSelected() {
		return personTypeSelected;
	}

	public void setPersonTypeSelected(Integer personTypeSelected) {
		this.personTypeSelected = personTypeSelected;
	}

	public Integer getWithCouponsSelected() {
		return withCouponsSelected;
	}

	public void setWithCouponsSelected(Integer withCouponsSelected) {
		this.withCouponsSelected = withCouponsSelected;
	}

	public Date getIssuanceDateSelected() {
		return issuanceDateSelected;
	}

	public void setIssuanceDateSelected(Date issuanceDateSelected) {
		this.issuanceDateSelected = issuanceDateSelected;
	}

	/** The ind issuer selected modif. */
	private boolean indIssuerSelectedModif;

	/** The lst issuer request. */
	private List<IssuerRequest> lstIssuerRequest;

	/** The issuer request data model. */
	private IssuerRequestDataModel issuerRequestDataModel;

	/** The id issuer request pk. */
	private Long idIssuerRequestPk;

	/** The issuer history. */
	private IssuerHistory issuerHistory;

	/** The lst issuer file histories. */
	private List<IssuerFileHistory> lstIssuerFileHistories;

	/** The min depositary contract date. */
	private Date minDepositaryContractDate;

	/** The min inscription stock exch date. */
	private Date minInscriptionStockExchDate;

	/** The allow search all. */
	private boolean allowSearchAll;

	/** Window modify issuer. */
	private String WINDOW_MODIFY_ISSUER = "issuerMoficiationRule";

	/** The streamed content file. */
	private transient StreamedContent streamedContentFile;

	/** The id id geographic location. */
	private Integer idIdGeographicLocation;

	/** The list document source. */
	private List<ParameterTable> listDocumentSource;

	/** The issuance securities service facade. */
	@EJB
	private IssuanceSecuritiesServiceFacade issuanceSecuritiesServiceFacade;

	/** The document source description. */
	private Map<Integer, String> documentSourceDescription;

	/** The ind ficpat economic activity. */
	private boolean indFICPATEconomicActivity;

	/**
	 * Instantiates a new participant mgmt bean.
	 */
	public IssuerModificationMgmtBean() {
		issuer = new Issuer();
		issuerTO = new IssuerTO();

		IssuerRequestTO issuerRequestTO = new IssuerRequestTO();
		issuerRequestTO.setInitiaRequestDate(CommonsUtilities.currentDate());
		issuerRequestTO.setEndRequestDate(CommonsUtilities.currentDate());

		issuerTO.setIssuerRequestTO(issuerRequestTO);
		legalRepresentativeHistory = new LegalRepresentativeHistory();
		pepPerson = new PepPerson();
		documentSourceDescription = new HashMap<Integer, String>();
	}

	/**
	 * Clean search.
	 */
	public void cleanSearch() {
		cleanDataModel();
		validateUserAccountSession();
		JSFUtilities.resetViewRoot();
	}

	/**
	 * Validate User Account Session.
	 */
	public void validateUserAccountSession() {
		issuerTO = new IssuerTO();
		IssuerRequestTO issuerRequestTO = new IssuerRequestTO();
		issuerTO.setIssuerRequestTO(issuerRequestTO);
		issuerRequestTO.setInitiaRequestDate(CommonsUtilities.currentDate());
		issuerRequestTO.setEndRequestDate(CommonsUtilities.currentDate());
		if ((userInfo.getUserAccountSession().isIssuerInstitucion() || userInfo
				.getUserAccountSession().isIssuerDpfInstitucion())
				&& Validations.validateIsNotNullAndNotEmpty(userInfo
						.getUserAccountSession().getIssuerCode())) {
			issuerTO.setIdIssuerPk(userInfo.getUserAccountSession()
					.getIssuerCode());
			Issuer objIssuer = null;
			try {
				objIssuer = issuerServiceFacade
						.findIssuerServiceFacade(issuerTO);
				issuerTO.setBusinessName(objIssuer.getBusinessName());
				issuerTO.setDocumentType(objIssuer.getDocumentType());
				issuerTO.setDocumentNumber(objIssuer.getDocumentNumber());
				issuerTO.setMnemonic(objIssuer.getMnemonic());
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * Load issuer modif req action.
	 * 
	 * @return the string
	 */
	@LoggerAuditWeb
	public String loadIssuerModifReqAction() {
		try {
			setViewOperationType(ViewOperationsType.REGISTER.getCode());
			initializeObjectsRegister();
			initializeIndicators();
			/**Datos de modificaion de valores por emisor*/
			indIssuerSIRTEXnegociate = false;
			securityClassSelected = null;
			currencySelected = null;
			personTypeSelected = null;
			withCouponsSelected = null;
			
			/** Clase de valor */
			ParameterTableTO filterParameterTable = new ParameterTableTO();
			filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
			filterParameterTable.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			listSecurityClass = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
			/** Carga de moneda */
			filterParameterTable.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			listCurrency = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
			/** Carga de persona */
			filterParameterTable.setMasterTableFk(MasterTableType.PERSON_TYPE.getCode());
			listPersonType = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
			/** Agregando con cupones y sin cupones */
			ParameterTable parameterYes = new ParameterTable(BooleanType.YES.getCode());
			parameterYes.setParameterName(WITH_COUPONS);
			ParameterTable parameterNot = new ParameterTable(BooleanType.NO.getCode());
			parameterNot.setParameterName(NO_COUPONS);
			listBooleanValues = new ArrayList<>(2);

			listBooleanValues.add(parameterYes);
			listBooleanValues.add(parameterNot);
			return WINDOW_MODIFY_ISSUER;
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}

	/**
	 * Initialize Indicators.
	 */
	public void initializeIndicators() {
		indIssuerDepositaryContract = Boolean.FALSE;
		indInscriptionStockExchange = Boolean.FALSE;
		indIssuerDifferentiated = Boolean.FALSE;
		indIssuerSelectedModif = Boolean.FALSE;
	}

	/**
	 * Gets the lst document source.
	 * 
	 * @return the lst document source
	 */
	public void getLstDocumentSource() {

		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.DOCUMENT_SOURCE
					.getCode());

			listDocumentSource = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}

	/**
	 * Initialize Objects Register.
	 */
	public void initializeObjectsRegister() {
		issuer = new Issuer();
		issuer.setHolder(new Holder());
		issuer.setGeographicLocation(new GeographicLocation());
		lstIssuerFiles = new ArrayList<IssuerFile>();
		legalRepresentativeHelperBean
				.setLstLegalRepresentativeHistory(new ArrayList<LegalRepresentativeHistory>());
		legalRepresentativeHelperBean
				.setFlagModifyLegalRepresentantive(Boolean.TRUE);
		issuerRequest = new IssuerRequest();
	}

	/**
	 * Set Indicator Validate Detail Issuer.
	 */
	public void setIndicatorValidateDetailIssuer() {
		indInscriptionStockExchange = BooleanType.YES.getCode().equals(
				issuer.getIndStockExchEnrolled());
		indIssuerDepositaryContract = BooleanType.YES.getCode().equals(
				issuer.getIndContractDepository());
		indIssuerDifferentiated = BooleanType.YES.getCode().equals(
				issuer.getIndDifferentiated());
		indIssuerSelectedModif = Boolean.TRUE;
		indIssuerSIRTEXnegociate = BooleanType.YES.getCode().equals(issuer.getIndSirtexNegotiate());
		verifyCheck();
		
	}

	private void verifyCheck(){
			UIViewRoot view = FacesContext.getCurrentInstance().getViewRoot();
	        UIComponent componentPanel = view.findComponent(":frmIssuerRegister:tabViewIssuer:fieldModifySecuritiesData");
			for(UIComponent component: componentPanel.getChildren()) {
			    if(component.getId().contains("idCboxCurrency")){
			        if(component instanceof SelectOneMenu) {  
			        	if(!indIssuerSIRTEXnegociate)
			        		((SelectOneMenu) component).setDisabled(true);
			        	else
			        		((SelectOneMenu) component).setDisabled(false);
			        }
			    }
			    if(component.getId().contains("idCboxPersonType")){
			        if(component instanceof SelectOneMenu) {  
			        	if(!indIssuerSIRTEXnegociate)
			        		((SelectOneMenu) component).setDisabled(true);
			        	else
			        		((SelectOneMenu) component).setDisabled(false);
			        }
			    }
			    if(component.getId().contains("idCboxCoupons")){
			    	 if(component instanceof SelectOneMenu) {  
			        	if(!indIssuerSIRTEXnegociate)
			        		((SelectOneMenu) component).setDisabled(true);
			        	else
			        		((SelectOneMenu) component).setDisabled(false);
			        }
			    }
			    if(component.getId().contains("idCalEmitionDate")){
			        if(component instanceof Calendar) {  
			        	if(!indIssuerSIRTEXnegociate)
			        		((Calendar) component).setDisabled(true);
			        	else
			        		((Calendar) component).setDisabled(false);
			        }
			    }
			}
	}
	/**
	 * Set Validate Detail Issuer File.
	 * 
	 * @param lstIssuerFiles
	 *            List File
	 * @throws Exception
	 *             Exception
	 */
	public void setValidateDetailIssuerFile(List<IssuerFile> lstIssuerFiles)
			throws Exception {
		InputStream inputStream;
		StreamedContent streamedContentFile;
		for (IssuerFile issuerFile : lstIssuerFiles) {
			/*
			 * set the transient attribute streamedContentFile in order to shown
			 * the content of this file on participant detail.
			 */
			lstIssuerFilesOrigin.add(issuerFile);
			inputStream = new ByteArrayInputStream(issuerFile.getDocumentFile());
			streamedContentFile = new DefaultStreamedContent(inputStream, null,
					issuerFile.getFilename());
			setStreamedContentFile(streamedContentFile);
			inputStream.close();
		}
	}

	/**
	 * Set Validate Legal Representative History.
	 * 
	 * @param lstRepresentedEntity
	 *            List Represented Entity
	 * @throws ServiceException
	 *             Service Exception
	 */
	public void setValidateLegalRepresentativeHistory(
			List<RepresentedEntity> lstRepresentedEntity)
			throws ServiceException {
		LegalRepresentativeHistory objLegalRepresentativeHistory;
		for (RepresentedEntity objRepresentedEntity : lstRepresentedEntity) {
			objLegalRepresentativeHistory = new LegalRepresentativeHistory();
			objLegalRepresentativeHistory
					.setIdRepresentativeHistoryPk(objRepresentedEntity
							.getLegalRepresentative()
							.getIdLegalRepresentativePk());
			objLegalRepresentativeHistory
					.setRepresentativeClass(objRepresentedEntity
							.getRepresentativeClass());
			objLegalRepresentativeHistory.setBirthDate(objRepresentedEntity
					.getLegalRepresentative().getBirthDate());
			objLegalRepresentativeHistory
					.setDocumentNumber(objRepresentedEntity
							.getLegalRepresentative().getDocumentNumber());
			objLegalRepresentativeHistory.setDocumentType(objRepresentedEntity
					.getLegalRepresentative().getDocumentType());
			objLegalRepresentativeHistory
					.setEconomicActivity(objRepresentedEntity
							.getLegalRepresentative().getEconomicActivity());
			objLegalRepresentativeHistory
					.setEconomicSector(objRepresentedEntity
							.getLegalRepresentative().getEconomicSector());
			objLegalRepresentativeHistory.setEmail(objRepresentedEntity
					.getLegalRepresentative().getEmail());
			objLegalRepresentativeHistory.setFaxNumber(objRepresentedEntity
					.getLegalRepresentative().getFaxNumber());
			objLegalRepresentativeHistory.setFirstLastName(objRepresentedEntity
					.getLegalRepresentative().getFirstLastName());
			objLegalRepresentativeHistory.setFullName(objRepresentedEntity
					.getLegalRepresentative().getFullName());
			objLegalRepresentativeHistory
					.setHomePhoneNumber(objRepresentedEntity
							.getLegalRepresentative().getHomePhoneNumber());
			objLegalRepresentativeHistory.setIndResidence(objRepresentedEntity
					.getLegalRepresentative().getIndResidence());
			objLegalRepresentativeHistory.setLegalAddress(objRepresentedEntity
					.getLegalRepresentative().getLegalAddress());
			objLegalRepresentativeHistory.setLegalDistrict(objRepresentedEntity
					.getLegalRepresentative().getLegalDistrict());
			objLegalRepresentativeHistory.setLegalProvince(objRepresentedEntity
					.getLegalRepresentative().getLegalProvince());
			objLegalRepresentativeHistory
					.setLegalDepartment(objRepresentedEntity
							.getLegalRepresentative().getLegalDepartment());
			objLegalRepresentativeHistory
					.setLegalResidenceCountry(objRepresentedEntity
							.getLegalRepresentative()
							.getLegalResidenceCountry());
			objLegalRepresentativeHistory.setMobileNumber(objRepresentedEntity
					.getLegalRepresentative().getMobileNumber());
			objLegalRepresentativeHistory.setName(objRepresentedEntity
					.getLegalRepresentative().getName());
			objLegalRepresentativeHistory.setNationality(objRepresentedEntity
					.getLegalRepresentative().getNationality());
			objLegalRepresentativeHistory
					.setOfficePhoneNumber(objRepresentedEntity
							.getLegalRepresentative().getOfficePhoneNumber());
			objLegalRepresentativeHistory.setPersonType(objRepresentedEntity
					.getLegalRepresentative().getPersonType());
			objLegalRepresentativeHistory.setPostalAddress(objRepresentedEntity
					.getLegalRepresentative().getPostalAddress());
			objLegalRepresentativeHistory
					.setPostalDepartment(objRepresentedEntity
							.getLegalRepresentative().getPostalDepartment());
			objLegalRepresentativeHistory
					.setPostalDistrict(objRepresentedEntity
							.getLegalRepresentative().getPostalDistrict());
			objLegalRepresentativeHistory
					.setPostalProvince(objRepresentedEntity
							.getLegalRepresentative().getPostalProvince());
			objLegalRepresentativeHistory
					.setPostalResidenceCountry(objRepresentedEntity
							.getLegalRepresentative()
							.getPostalResidenceCountry());
			objLegalRepresentativeHistory
					.setSecondDocumentNumber(objRepresentedEntity
							.getLegalRepresentative().getSecondDocumentNumber());
			objLegalRepresentativeHistory
					.setSecondDocumentType(objRepresentedEntity
							.getLegalRepresentative().getSecondDocumentType());
			objLegalRepresentativeHistory
					.setSecondLastName(objRepresentedEntity
							.getLegalRepresentative().getSecondLastName());
			objLegalRepresentativeHistory
					.setSecondNationality(objRepresentedEntity
							.getLegalRepresentative().getSecondNationality());
			objLegalRepresentativeHistory.setSex(objRepresentedEntity
					.getLegalRepresentative().getSex());
			objLegalRepresentativeHistory
					.setStateRepreHistory(LegalRepresentativeStateType.REGISTERED
							.getCode());

			PepPerson pepPersonRepresentative = issuerServiceFacade
					.findPepPersonByLegalRepresentativeServiceFacade(objRepresentedEntity
							.getLegalRepresentative());
			if (Validations.validateIsNotNull(pepPersonRepresentative)) {
				objLegalRepresentativeHistory.setIndPEP(BooleanType.YES
						.getCode());
				objLegalRepresentativeHistory
						.setCategory(pepPersonRepresentative.getCategory());
				objLegalRepresentativeHistory.setRole(pepPersonRepresentative
						.getRole());
				objLegalRepresentativeHistory
						.setBeginningPeriod(pepPersonRepresentative
								.getBeginingPeriod());
				objLegalRepresentativeHistory
						.setEndingPeriod(pepPersonRepresentative
								.getEndingPeriod());
			} else {
				objLegalRepresentativeHistory.setIndPEP(BooleanType.NO
						.getCode());
			}

			if (Validations
					.validateIsNotNullAndPositive(objLegalRepresentativeHistory
							.getIdRepresentativeHistoryPk())
					&& Validations
							.validateListIsNullOrEmpty(objLegalRepresentativeHistory
									.getRepresenteFileHistory())) {
				LegalRepresentative legalRepresentative = new LegalRepresentative();
				legalRepresentative
						.setIdLegalRepresentativePk(objLegalRepresentativeHistory
								.getIdRepresentativeHistoryPk());
				List<LegalRepresentativeFile> representativeFiles = issuerServiceFacade
						.getListLegalRepresentativeFileServiceFacade(legalRepresentative);
				lstRepresentativeFileHistory = new ArrayList<RepresentativeFileHistory>();

				RepresentativeFileHistory representativeFileHistory;
				for (LegalRepresentativeFile legalRepresentativeFile : representativeFiles) {
					representativeFileHistory = new RepresentativeFileHistory();
					representativeFileHistory
							.setDocumentType(legalRepresentativeFile
									.getDocumentType());
					representativeFileHistory
							.setFilename(legalRepresentativeFile.getFilename());
					representativeFileHistory
							.setDescription(legalRepresentativeFile
									.getDescription());
					representativeFileHistory
							.setFileRepresentative(legalRepresentativeFile
									.getFileLegalRepre());
					representativeFileHistory
							.setLegalRepresentativeHistory(objLegalRepresentativeHistory);
					representativeFileHistory
							.setFileRepresentative(legalRepresentativeFile
									.getFileLegalRepre());
					lstRepresentativeFileHistory.add(representativeFileHistory);
				}

				objLegalRepresentativeHistory
						.setRepresenteFileHistory(lstRepresentativeFileHistory);

			} else {
				lstRepresentativeFileHistory = objLegalRepresentativeHistory
						.getRepresenteFileHistory();
			}

			LegalRepresentativeTO legalRepresentativeTO = new LegalRepresentativeTO();
			legalRepresentativeTO
					.setViewOperationType(ViewOperationsType.DETAIL.getCode());
			legalRepresentativeTO.setFlagIssuerOrParticipant(BooleanType.YES
					.getBooleanValue());

			legalRepresentativeHelperBean
					.setLegalRepresentativeTO(legalRepresentativeTO);
			lstLegalRepresentativeHistory.add(objLegalRepresentativeHistory);
			lstLegalRepresentativeHistoryOrigin
					.add(objLegalRepresentativeHistory);
			legalRepresentativeHelperBean
					.setLstLegalRepresentativeHistory(lstLegalRepresentativeHistory);

		}
	}

	// public void checkBalanceRestrictions(){
	// try{
	// IssuanceTO issuanceTO = new IssuanceTO();
	// issuanceTO.setIdIssuerPk(issuer.getIdIssuerPk());
	// List<Issuance> listIssuance =
	// issuanceSecuritiesServiceFacade.findIssuancesServiceFacade(issuanceTO);
	// if(listIssuance!=null && listIssuance.size()>0){
	// issuer.getHolder().getIdHolderPk();
	// boolean indBalance =
	// accountsFacade.validityHolderAccountBalanceByHolder(issuer.getHolder());
	//
	// if(indBalance){
	// indicatorModify = false;
	// }
	// else{
	// indicatorModify = true;
	// }
	// }
	// else{
	// indicatorModify = true;
	// }
	//
	// }
	// catch(Exception e){
	// e.printStackTrace();
	// }
	// }

	/**
	 * Validate id issuer.
	 */
	@LoggerAuditWeb
	public String validateIdIssuer(){				
		if(Validations.validateIsNotNullAndNotEmpty(issuer.getIdIssuerPk()))	
		{
			try {
			indFICPATEconomicActivity=false;
			Issuer issuerFilter = new Issuer();
			issuerFilter.setIdIssuerPk(issuer.getIdIssuerPk());
			issuerFilter.setStateIssuer(IssuerStateType.REGISTERED.getCode());		
			Issuer issuerResult = findIssuerInformation(issuerFilter);
			Issuer issuerResultOrigin = findIssuerInformation(issuerFilter);
			if(Validations.validateIsNotNull(issuerResult)){
				
				if(Validations.validateIsNotNull(issuerResult.getEconomicActivity())){
					buildInvestorTypeByEconAcSelected(issuerResult.getEconomicActivity());
				}
				
				issuer = issuerResult;	
				
				//checkBalanceRestrictions();				
				issuerOrigin = issuerResultOrigin;
				if(existsPreviousRequest())
				{
					initializeObjectsRegister();	
					indIssuerSelectedModif = Boolean.FALSE;
					return null;
				}
				
				if(issuer.getEconomicActivity().equals(EconomicActivityType.FONDOS_INVERSION_CERRADOS.getCode())){
					indFICPATEconomicActivity=true;
				}
				else if(issuerResult.getEconomicActivity().equals(EconomicActivityType.PATRIMONIO_AUTONOMO_TITULARIZACION.getCode())){
					indFICPATEconomicActivity=true;
				}
				
				setIndicatorValidateDetailIssuer();		
				lstIssuerFilesOrigin = new ArrayList<IssuerFile>();   
				lstIssuerFiles = issuerServiceFacade.getListIssuerFilesServiceFacade(issuerFilter);
				setValidateDetailIssuerFile(lstIssuerFiles);   	
	    		lstRepresentedEntity = issuerServiceFacade.getListRepresentedEntityByIssuerServiceFacade(issuerFilter);    		
	    		lstLegalRepresentativeHistory = new ArrayList<LegalRepresentativeHistory>(); 
	    		lstLegalRepresentativeHistoryOrigin = new ArrayList<LegalRepresentativeHistory>();   
	    		if(Validations.validateListIsNotNullAndNotEmpty(lstRepresentedEntity))    			
	    			setValidateLegalRepresentativeHistory(lstRepresentedEntity);   		
	    		representedEntity = new RepresentedEntity();	    		
	    		representedEntity.setLegalRepresentative(new LegalRepresentative());
	    		pepPerson = new PepPerson(); 
	    		
	    		if(Validations.validateIsNotNullAndPositive(issuer.getDocumentType()))
	    		
	    		documentTORegister=documentValidatorRegister.loadMaxLenghtType(issuer.getDocumentType());
	    		DocumentTO documentTO = documentValidatorRegister.validateEmissionRules(issuer.getDocumentType());
				documentTORegister.setIndicatorAlphanumericWithDash(documentTO.isIndicatorAlphanumericWithDash());
				documentTORegister.setIndicatorDocumentSource(documentTO.isIndicatorDocumentSource());
			
	    		
	    		idIdGeographicLocation = issuer.getGeographicLocation().getIdGeographicLocationPk();
	    			    		
			} else {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.ISSUER_ERROR_STATE_BLOCKED));
				JSFUtilities.showSimpleValidationDialog();	
				initializeObjectsRegister();
		    	indIssuerSelectedModif = Boolean.FALSE;
			}
			currencySelected= issuer.getCurrencySirtexNegotiate();
			personTypeSelected = issuer.getPersonTypeSirtexNegotiate();
			withCouponsSelected = issuer.getWithCouponsSirtexNegotiate();
			issuanceDateSelected = issuer.getIssuanceDateSirtexNegotiate();//==null?CommonsUtilities.currentDate():issuer.getIssuanceDateSirtexNegotiate();
			} catch (Exception e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
		}
		else {
			initializeObjectsRegister();
	    	indIssuerSelectedModif = Boolean.FALSE;
		}
		return null;
	}

	/**
	 * Load default combos.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	private void loadCombosIssuerManagement() throws ServiceException {
		loadCountries();
		loadIssuerLocationInfo();
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED
				.getCode());
		loadListOfferType(filterParameterTable);
		loadListEconomicSector(filterParameterTable);
		loadListSocietyType(filterParameterTable);
		loadListCurrency(filterParameterTable);
		changeSelectedEconomicSector();
	}

	/**
	 * Load cbo lst credit rating scales.
	 */
	public void loadCboLstCreditRatingScales() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.CREDIT_RATING_SCALES_ISSUER.getCode());
			parameterTableTO.setOrderbyParameterTableCd(GeneralConstants.ONE_VALUE_INTEGER.intValue());
			lstCboCreditRatingScales = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);

			for (ParameterTable param : lstCboCreditRatingScales) {
				getParametersTableMap().put(param.getParameterTablePk(), param.getParameterName());
			}

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Load combos for search.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	private void loadCombosForSearch() throws ServiceException {

		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED
				.getCode());
		loadListStateSearch(filterParameterTable);
		loadListDocumenTypeSearch(filterParameterTable);
		getLstDocumentSource();
	}

	/**
	 * List the State of issuers.
	 * 
	 * @param filterParameterTable
	 *            Object Filter
	 * @throws ServiceException
	 *             the service exception
	 */
	private void loadListStateSearch(ParameterTableTO filterParameterTable)
			throws ServiceException {
		filterParameterTable
				.setMasterTableFk(MasterTableType.ISSUER_BLOCK_REQUEST_STATUS_TYPE
						.getCode());
		lstIssuerRequestState = generalParametersFacade
				.getListParameterTableServiceBean(filterParameterTable);
	}

	/**
	 * List the document types of issuers.
	 * 
	 * @param filterParameterTable
	 *            Object Filter
	 * @throws ServiceException
	 *             the service exception
	 */
	private void loadListDocumenTypeSearch(ParameterTableTO filterParameterTable)
			throws ServiceException {
		filterParameterTable
				.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON
						.getCode());
		List<Integer> lstParameterTablePk = new ArrayList<Integer>();
		lstParameterTablePk.add(DocumentType.RUC.getCode());
		lstParameterTablePk.add(DocumentType.DIO.getCode());
		filterParameterTable.setLstParameterTablePk(lstParameterTablePk);
		lstIssuerDocumentTypeForSearch = generalParametersFacade
				.getListParameterTableServiceBean(filterParameterTable);
	}

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		try {
			loadCombosForSearch();
			listHolidays();
			sessionValidUserAccount();
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setMasterTableFk(MasterTableType.DOCUMENT_SOURCE
					.getCode());
			for (ParameterTable param : generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO)) {
				documentSourceDescription.put(param.getParameterTablePk(),	param.getParameterName());
			}

			loadCboLstCreditRatingScales();

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Validates user session objects to enable or disable.
	 */
	public void sessionValidUserAccount() {
		IssuerRequestTO issuerRequestTO = new IssuerRequestTO();
		issuerTO.setIssuerRequestTO(issuerRequestTO);
		issuerRequestTO.setInitiaRequestDate(CommonsUtilities.currentDate());
		issuerRequestTO.setEndRequestDate(CommonsUtilities.currentDate());
		if (userInfo.getUserAccountSession().isDepositaryInstitution()) {
			allowSearchAll = true;
		} else if ((userInfo.getUserAccountSession().isIssuerInstitucion() || userInfo
				.getUserAccountSession().isIssuerDpfInstitucion())
				&& Validations.validateIsNotNullAndNotEmpty(userInfo
						.getUserAccountSession().getIssuerCode())) {
			allowSearchAll = false;
			issuerTO.setIdIssuerPk(userInfo.getUserAccountSession()
					.getIssuerCode());
			Issuer objIssuer = null;
			try {
				objIssuer = issuerServiceFacade
						.findIssuerServiceFacade(issuerTO);
				issuerTO.setBusinessName(objIssuer.getBusinessName());
				issuerTO.setDocumentType(objIssuer.getDocumentType());
				issuerTO.setDocumentNumber(objIssuer.getDocumentNumber());
				issuerTO.setMnemonic(objIssuer.getMnemonic());
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * List holidays.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	private void listHolidays() throws ServiceException {
		Holiday holidayFilter = new Holiday();
		holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		GeographicLocation countryFilter = new GeographicLocation();
		countryFilter.setIdGeographicLocationPk(countryResidence);
		holidayFilter.setGeographicLocation(countryFilter);

		allHolidays = CommonsUtilities
				.convertListDateToString(generalParametersFacade
						.getListHolidayDateServiceFacade(holidayFilter));
	}

	/**
	 * Initialize Objects Change Selected Country.
	 */
	public void initializeObjectsChangeSelectedCountry() {
		issuer.setDocumentType(null);
		issuerFileNameDisplay = null;
		indIssuerDifferentiated = Boolean.FALSE;
	}

	/**
	 * Load Departments.
	 * 
	 * @param referenceFk
	 *            the reference fk
	 * @return List Departments
	 * @throws ServiceException
	 *             the service exception
	 */
	public List<GeographicLocation> getLstDepartments(Integer referenceFk)
			throws ServiceException {
		GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
		geographicLocationTO
				.setGeographicLocationType(GeographicLocationType.DEPARTMENT
						.getCode());
		geographicLocationTO.setState(GeographicLocationStateType.REGISTERED
				.getCode());
		geographicLocationTO.setIdLocationReferenceFk(referenceFk);
		return generalParametersFacade
				.getListGeographicLocationServiceFacade(geographicLocationTO);
	}

	/**
	 * Load Department Country.
	 */
	public void loadDepartmentCountry() {
		try {
			if (!Validations.validateIsNullOrNotPositive(issuer
					.getGeographicLocation().getIdGeographicLocationPk())) {
				lstDepartments = getLstDepartments(issuer
						.getGeographicLocation().getIdGeographicLocationPk());
				lstProvinces = null;
				lstDistricts = null;
			} else {
				lstDepartments = null;
				lstProvinces = null;
				lstDistricts = null;
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Country selection validate.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	public void validateSelectedCountry() throws ServiceException {
		// Validate if the country was no selected
		if (Validations.validateIsNullOrNotPositive(issuer
				.getGeographicLocation().getIdGeographicLocationPk())) {
			lstIssuerFiles = null;
			issuer.setDocumentType(null);
		} else {
			// Validate if the issuer is national or foreign in order to set the
			// document type and trade name
			if (isIndIssuerNational()) {
				loadIssuerDocumentTypeNational();
				loadIssuerAttachmentTypeNational();
			} else {
				loadIssuerDocumentTypeForeign();
				loadIssuerAttachmentTypeForeign();
			}
		}
	}

	/**
	 * Load Issuer Document Type National.
	 */
	public void loadIssuerDocumentTypeNational() {
		PersonTO person = new PersonTO();
		person.setFlagIssuerIndicator(true);
		person.setFlagNational(true);
		person.setPersonType(PersonType.JURIDIC.getCode());
		lstIssuerDocumentType = documentValidatorRegister
				.getDocumentTypeResult(person);
	}

	/**
	 * Load Issuer Document Type Foreign.
	 */
	public void loadIssuerDocumentTypeForeign() {
		PersonTO person = new PersonTO();
		person.setFlagIssuerIndicator(true);
		person.setFlagForeign(true);
		person.setPersonType(PersonType.JURIDIC.getCode());
		lstIssuerDocumentType = documentValidatorRegister
				.getDocumentTypeResult(person);
	}

	/**
	 * Load Issuer Attachment Type National.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	public void loadIssuerAttachmentTypeNational() throws ServiceException {
		ParameterTableTO paramFilter = new ParameterTableTO();
		paramFilter.setMasterTableFk(MasterTableType.ISSUER_FILE_NATIONAL_TYPE
				.getCode());
		paramFilter.setState(ParameterTableStateType.REGISTERED.getCode());
		setLstIssuerAttachmentType(generalParametersFacade
				.getListParameterTableServiceBean(paramFilter));
	}

	/**
	 * Load Issuer Attachment Type Foreign.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	public void loadIssuerAttachmentTypeForeign() throws ServiceException {
		ParameterTableTO paramFilter = new ParameterTableTO();
		paramFilter.setMasterTableFk(MasterTableType.ISSUER_FILE_FOREIGN_TYPE
				.getCode());
		paramFilter.setState(ParameterTableStateType.REGISTERED.getCode());
		setLstIssuerAttachmentType(generalParametersFacade
				.getListParameterTableServiceBean(paramFilter));
	}

	/**
	 * Clean Objects Change Selected Country.
	 */
	public void cleanObjectsChangeSelectedCountry() {
		issuer.setLegalAddress(null);
		issuer.setDocumentNumber(null);
		issuer.setBusinessName(null);
		setAttachIssuerFileSelected(null);
	}

	/**
	 * Change selected country.
	 */
	@LoggerAuditWeb
	public void changeSelectedCountry() {
		try {
			hideDialogsListener(null);
			if (isIndIssuerNational())
				issuer.getGeographicLocation().setIdGeographicLocationPk(
						idIdGeographicLocation);
			else {
				initializeObjectsChangeSelectedCountry();
				loadDepartmentCountry();
				validateSelectedCountry();
				cleanObjectsChangeSelectedCountry();
			}
			JSFUtilities
					.resetComponent(":frmIssuerRegister:tabViewIssuer:cboDocumentType");
			JSFUtilities
					.resetComponent(":frmIssuerRegister:tabViewIssuer:msgCboDocumentType");
			JSFUtilities
					.resetComponent(":frmIssuerRegister:tabViewIssuer:txtTradeName");
			JSFUtilities
					.resetComponent(":frmIssuerRegister:tabViewIssuer:msgTxtTradeName");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Change selected document type register.
	 */
	public void changeSelectedDocumentTypeRegister() {
		hideDialogsListener(null);
		issuer.setDocumentNumber(null);
		if (Validations.validateIsNotNullAndPositive(issuer.getDocumentType()))
			documentTORegister = documentValidatorRegister
					.loadMaxLenghtType(issuer.getDocumentType());

		DocumentTO documentTO = documentValidatorRegister
				.validateEmissionRules(issuer.getDocumentType());
		documentTORegister.setIndicatorAlphanumericWithDash(documentTO
				.isIndicatorAlphanumericWithDash());
		documentTORegister.setIndicatorDocumentSource(documentTO
				.isIndicatorDocumentSource());

		JSFUtilities
				.resetComponent(":frmIssuerRegister:tabViewIssuer:txtDocumentNumber");
		JSFUtilities
				.resetComponent(":frmIssuerRegister:tabViewIssuer:msgTxtDocumentNumber");
	}

	/**
	 * Change selected country from registered participant.
	 */
	@LoggerAuditWeb
	public void changeSelectedCountryFromRegisteredParticipant() {
		try {
			hideDialogsListener(null);

			GeographicLocationTO filter = new GeographicLocationTO();
			filter.setGeographicLocationType(GeographicLocationType.PROVINCE
					.getCode());
			filter.setState(GeographicLocationStateType.REGISTERED.getCode());
			filter.setIdLocationReferenceFk(issuer.getGeographicLocation()
					.getIdGeographicLocationPk());

			// List The provinces
			lstProvinces = generalParametersFacade
					.getListGeographicLocationServiceFacade(filter);
			issuer.setProvince(GeneralConstants.NEGATIVE_ONE_INTEGER);
			// Clean the list of sectors
			lstDistricts = null;
			issuer.setDistrict(GeneralConstants.NEGATIVE_ONE_INTEGER);

			InputText txtDocumentNumber = (InputText) JSFUtilities
					.findViewComponent("frmIssuerRegister:tabViewIssuer:txtDocumentNumber");
			if (Validations.validateIsNotNull(txtDocumentNumber)) {
				txtDocumentNumber.setDisabled(Boolean.FALSE);
			}

			// Validate if the participant is national or foreign in order to
			// set the document type and trade name
			if (countryResidence.equals(issuer.getGeographicLocation()
					.getIdGeographicLocationPk())) {
				// Max Length of document number to 9
				if (Validations.validateIsNotNull(txtDocumentNumber)) {
					txtDocumentNumber.setMaxlength(9);
				}
			} else {
				issuer.setDocumentType(IssuerDocumentType.DIO.getCode());
				// Max Length of document number to 20
				if (Validations.validateIsNotNull(txtDocumentNumber)) {
					txtDocumentNumber.setMaxlength(20);
				}
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Load participant location info.
	 */
	private void loadIssuerLocationInfo() {
		try {
			hideDialogsListener(null);

			GeographicLocationTO filter = new GeographicLocationTO();
			filter.setState(GeographicLocationStateType.REGISTERED.getCode());

			filter.setGeographicLocationType(GeographicLocationType.DEPARTMENT
					.getCode());
			filter.setIdLocationReferenceFk(issuer.getGeographicLocation()
					.getIdGeographicLocationPk());
			// List The departament
			lstDepartments = generalParametersFacade
					.getListGeographicLocationServiceFacade(filter);

			filter.setGeographicLocationType(GeographicLocationType.PROVINCE
					.getCode());
			filter.setIdLocationReferenceFk(issuer.getDepartment());
			// List The provinces
			lstProvinces = generalParametersFacade
					.getListGeographicLocationServiceFacade(filter);

			filter.setGeographicLocationType(GeographicLocationType.DISTRICT
					.getCode());
			filter.setIdLocationReferenceFk(issuer.getProvince());
			lstDistricts = generalParametersFacade
					.getListGeographicLocationServiceFacade(filter);

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Load Districts.
	 * 
	 * @param referenceFk
	 *            the reference fk
	 * @return List Districts
	 * @throws ServiceException
	 *             the service exception
	 */
	public List<GeographicLocation> getLstDistricts(Integer referenceFk)
			throws ServiceException {
		GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
		geographicLocationTO
				.setGeographicLocationType(GeographicLocationType.DISTRICT
						.getCode());
		geographicLocationTO.setState(GeographicLocationStateType.REGISTERED
				.getCode());
		geographicLocationTO.setIdLocationReferenceFk(referenceFk);
		return generalParametersFacade
				.getListGeographicLocationServiceFacade(geographicLocationTO);
	}

	/**
	 * Validate address format.
	 */
	public void validateAddressFormat() {
		hideDialogsListener(null);

		if (Validations.validateIsNotNullAndNotEmpty(issuer.getLegalAddress())) {
			if (!CommonsUtilities.matchAddress(issuer.getLegalAddress())) {
				showMessageOnDialog(
						PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities
								.getMessage(PropertiesConstants.ADM_ISSUER_VALIDATION_ADDRESS_FORMAT));
				JSFUtilities.showSimpleValidationDialog();
				issuer.setLegalAddress(null);
			}
		}
	}

	/**
	 * Load District Province.
	 */
	public void loadDistrictProvince() {
		hideDialogsListener(null);
		try {
			if (!Validations.validateIsNullOrNotPositive(issuer.getProvince()))
				lstDistricts = getLstDistricts(issuer.getProvince());
			else
				lstDistricts = null;
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Change selected province.
	 */
	@LoggerAuditWeb
	public void changeSelectedProvince() {
		try {
			hideDialogsListener(null);

			GeographicLocationTO filter = new GeographicLocationTO();
			filter.setGeographicLocationType(GeographicLocationType.DISTRICT
					.getCode());
			filter.setState(GeographicLocationStateType.REGISTERED.getCode());
			filter.setIdLocationReferenceFk(issuer.getProvince());

			lstDistricts = generalParametersFacade
					.getListGeographicLocationServiceFacade(filter);
			issuer.setDistrict(GeneralConstants.NEGATIVE_ONE_INTEGER);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Change cbo economic sector.
	 */
	public void changeCboEconomicSector() {
		try {
			issuer.setEconomicActivity(null);
			issuer.setSocietyType(null);
			changeSelectedEconomicSector();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Change selected economic sector.
	 */
	public void changeSelectedEconomicSector() {
		try {
			hideDialogsListener(null);
			lstEconomicActivity = generalParametersFacade
					.getListEconomicActivityBySector(issuer.getEconomicSector());
			lstSocietyType = generalParametersFacade
					.getListSocietyTypeBySector(issuer.getEconomicSector());
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Load Province Department.
	 */
	public void loadProvinceDepartment() {
		hideDialogsListener(null);
		try {
			if (!Validations
					.validateIsNullOrNotPositive(issuer.getDepartment())) {
				lstProvinces = getLstProvinces(issuer.getDepartment());
				lstDistricts = null;
			} else {
				lstProvinces = null;
				lstDistricts = null;
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Load Provinces.
	 * 
	 * @param referenceFk
	 *            the reference fk
	 * @return List Provinces
	 * @throws ServiceException
	 *             the service exception
	 */
	public List<GeographicLocation> getLstProvinces(Integer referenceFk)
			throws ServiceException {
		GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
		geographicLocationTO
				.setGeographicLocationType(GeographicLocationType.PROVINCE
						.getCode());
		geographicLocationTO.setState(GeographicLocationStateType.REGISTERED
				.getCode());
		geographicLocationTO.setIdLocationReferenceFk(referenceFk);
		return generalParametersFacade
				.getListGeographicLocationServiceFacade(geographicLocationTO);
	}

	/**
	 * Validate document issuer.
	 */
	public void validateNullEmptyDocumentNumber() {
		if (Validations.validateIsNullOrEmpty(issuer.getDocumentNumber())) {
			issuer.setBusinessName(null);
			return;
		}
	}

	/**
	 * View Message Issuer Is Already Registered.
	 * 
	 * @param issuerResult
	 *            Object Issuer
	 */
	public void viewMessageIssuerIsAlreadyRegistered(Issuer issuerResult) {
		StringBuilder sbBodyData = new StringBuilder();
		sbBodyData.append(IssuerDocumentType
				.get(issuerResult.getDocumentType()).getValue());
		sbBodyData.append(GeneralConstants.HYPHEN);
		sbBodyData.append(issuerResult.getDocumentNumber());
		Object[] arrBodyData = { sbBodyData.toString() };
		showMessageOnDialog(
				PropertiesUtilities
						.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities
						.getMessage(
								PropertiesConstants.ERROR_ADM_ISSUER_DOC_NUM_NATIONAL_REPEATED,
								arrBodyData));
		JSFUtilities.showSimpleValidationDialog();
	}

	/**
	 * View message other holder is already registered.
	 */
	public void viewMessageOtherHolderIsAlreadyRegistered() {
		Object[] arrBodyData = new Object[0];
		showMessageOnDialog(
				PropertiesUtilities
						.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities
						.getMessage(
								PropertiesConstants.ADM_ISSUER_OTHER_HOLDER_FOUND_REGISTERED,
								arrBodyData));
		JSFUtilities.showSimpleValidationDialog();
	}

	/**
	 * Find Institution Information().
	 * 
	 * @return Object Institution Information
	 * @throws ServiceException
	 *             Service Exception
	 */
	public InstitutionInformation findInstitutionInformation()
			throws ServiceException {
		InstitutionInformation cuiFilter = new InstitutionInformation();
		cuiFilter.setDocumentNumber(issuer.getDocumentNumber());
		cuiFilter.setDocumentType(Integer.valueOf(issuer.getDocumentType()));
		return issuerServiceFacade
				.findInstitutionInformationByIdServiceFacade(cuiFilter);
	}

	/**
	 * View Message Issuer Does Not Exist().
	 */
	public void viewMessageIssuerDoesNotExist() {
		StringBuilder sbBodyData = new StringBuilder();
		sbBodyData.append(IssuerDocumentType.get(issuer.getDocumentType())
				.getValue());
		sbBodyData.append(GeneralConstants.HYPHEN);
		sbBodyData.append(issuer.getDocumentNumber());
		Object[] arrBodyData = { sbBodyData.toString() };
		showMessageOnDialog(
				PropertiesUtilities
						.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities.getMessage(
						PropertiesConstants.ERROR_ADM_ISSUER_RNC_NOT_FOUND,
						arrBodyData));
		JSFUtilities.showSimpleValidationDialog();
	}

	/**
	 * View Message Foreign Issuer Does Not Exist.
	 * 
	 * @param issuerResult
	 *            Object Issuer
	 */
	public void viewMessageForeignIssuerDoesNotExist(Issuer issuerResult) {
		StringBuilder sbBodyData = new StringBuilder();
		sbBodyData.append(IssuerDocumentType
				.get(issuerResult.getDocumentType()).getValue());
		sbBodyData.append(GeneralConstants.HYPHEN);
		sbBodyData.append(issuerResult.getDocumentNumber());
		sbBodyData.append(GeneralConstants.HYPHEN);
		sbBodyData.append(issuerResult.getBusinessName());
		Object[] arrBodyData = { sbBodyData.toString() };
		showMessageOnDialog(
				PropertiesUtilities
						.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities
						.getMessage(
								PropertiesConstants.ERROR_ADM_ISSUER_DOC_NUM_FOREIGN_REPEATED,
								arrBodyData));
		JSFUtilities.showSimpleValidationDialog();
	}

	/**
	 * view Message Holder Is Not Been Registered.
	 * 
	 * @param holder
	 *            object Holder
	 */
	public void viewMessageHolderIsNotBeenRegistered(Holder holder) {
		StringBuilder sbMessageHolder = new StringBuilder();
		sbMessageHolder
				.append(DocumentType.get(holder.getDocumentType()).name())
				.append(GeneralConstants.BLANK_SPACE)
				.append(holder.getDocumentNumber());
		Object[] arrBodyData = { sbMessageHolder.toString() };
		showMessageOnDialog(
				PropertiesUtilities
						.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities
						.getMessage(
								PropertiesConstants.ADM_ISSUER_HOLDER_FOUND_NOT_REGISTERED,
								arrBodyData));
		JSFUtilities.showSimpleValidationDialog();
	}

	/**
	 * Clean Object For Document Issuer.
	 */
	public void cleanObjectForDocumentIssuer() {
		issuer.setDocumentNumber(null);
		issuer.setBusinessName(null);
		issuer.setDescription(null);
	}

	/**
	 * Clean the holder of Issuer.
	 */
	public void cleanHolderIssuer() {
		issuer.setHolder(new Holder());
		issuer.setIndHolderWillBeCreated(Boolean.TRUE);
	}

	/**
	 * Find issuer national or foreign.
	 * 
	 * @param holder
	 *            the holder
	 * @return the issuer
	 * @throws ServiceException
	 *             the service exception
	 */
	public Issuer findIssuerNationalOrForeign(Holder holder)
			throws ServiceException {
		Issuer issuerFilterValidations = new Issuer();
		issuerFilterValidations.setDocumentType(issuer.getDocumentType());
		issuerFilterValidations.setDocumentNumber(issuer.getDocumentNumber());
		if (IssuerDocumentType.DIO.getCode().equals(issuer.getDocumentType())
				|| IssuerDocumentType.CIE.getCode().equals(
						issuer.getDocumentType())
				|| IssuerDocumentType.PASAPORTE.getCode().equals(
						issuer.getDocumentType())) {
			issuerFilterValidations.setBusinessName(holder.getFullName());
		}
		return issuerServiceFacade
				.findIssuerByFiltersServiceFacade(issuerFilterValidations);
	}

	/**
	 * set Details Of The Holder.
	 * 
	 * @param holder
	 *            the new details of the holder
	 */
	public void setDetailsOfTheHolder(Holder holder) {
		issuer.setHolder(holder);
		issuer.setBusinessName(holder.getFullName());
		issuer.setDescription(holder.getFullName());
		issuer.setEconomicSector(holder.getEconomicSector());
		changeSelectedEconomicSector();
		issuer.setEconomicActivity(holder.getEconomicActivity());
		issuer.setIndHolderWillBeCreated(Boolean.FALSE);
	}

	/**
	 * view Message CUI Code Be Created
	 * ADM_ISSUER_REQUEST_VALIDATION_ISSUER_DOCUMENT_NUMBER.
	 */
	public void viewMessageCUICodeBeCreated() {
		showMessageOnDialog(
				PropertiesUtilities
						.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities
						.getMessage(PropertiesConstants.ADM_ISSUER_HOLDER_WILL_BE_CREATED));
		JSFUtilities.showSimpleValidationDialog();
	}

	/**
	 * View message request issuer document number.
	 */
	/*
	 * view Message ADM_ISSUER_REQUEST_VALIDATION_ISSUER_DOCUMENT_NUMBER
	 */
	public void viewMessageRequestIssuerDocumentNumber() {
		showMessageOnDialog(
				PropertiesUtilities
						.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities
						.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_ISSUER_DOCUMENT_NUMBER));
		JSFUtilities.showSimpleValidationDialog();
	}

	/**
	 * Find Issuer.
	 * 
	 * @return object Issuer
	 * @throws ServiceException
	 *             service exception
	 */
	public Issuer findIssuer() throws ServiceException {
		Issuer issuerFilterValidations = new Issuer();
		issuerFilterValidations.setDocumentType(issuer.getDocumentType());
		issuerFilterValidations.setDocumentNumber(issuer.getDocumentNumber());
		if (Validations.validateIsNotNullAndNotEmpty(issuer.getBusinessName()))
			issuerFilterValidations.setBusinessName(issuer.getBusinessName());
		return issuerServiceFacade
				.findIssuerByFiltersServiceFacade(issuerFilterValidations);
	}

	/**
	 * View Message Sender Found.
	 * 
	 * @param issuerResult
	 *            object Issuer
	 */
	public void viewMessageSenderFound(Issuer issuerResult) {
		StringBuilder sbBodyData = new StringBuilder();
		sbBodyData.append(IssuerDocumentType
				.get(issuerResult.getDocumentType()).getValue());
		sbBodyData.append(GeneralConstants.HYPHEN);
		sbBodyData.append(issuerResult.getDocumentNumber());
		sbBodyData.append(GeneralConstants.HYPHEN);
		sbBodyData.append(issuerResult.getBusinessName());
		Object[] arrBodyData = { sbBodyData.toString() };
		showMessageOnDialog(
				PropertiesUtilities
						.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities
						.getMessage(
								PropertiesConstants.ERROR_ADM_ISSUER_DOC_NUM_FOREIGN_REPEATED,
								arrBodyData));
		JSFUtilities.showSimpleValidationDialog();
	}

	/**
	 * Validate document and description issuer.
	 */
	public void validateDocumentAndDescriptionIssuer() {
		try {
			if (isIndIssuerForeign()
					&& Validations.validateIsNotNullAndNotEmpty(issuer
							.getBusinessName())) {
				if (Validations.validateIsNullOrEmpty(issuer
						.getDocumentNumber())) {
					hideDialogsListener(null);
					viewMessageRequestIssuerDocumentNumber();
					issuer.setBusinessName(null);
					cleanHolderIssuer();
					return;
				}
				Issuer issuerResult = findIssuer();
				if (Validations.validateIsNotNull(issuerResult)) {
					if (issuerResult.getIdIssuerPk().equals(
							issuer.getIdIssuerPk())
							&& issuerResult.getDocumentNumber().equals(
									issuer.getDocumentNumber()))
						;
					else {
						hideDialogsListener(null);
						viewMessageSenderFound(issuerResult);
						issuer.setDocumentNumber(null);
						issuer.setBusinessName(null);
						cleanHolderIssuer();
					}
				} else {
					List<OfacPerson> lstOfacPerson = findOfacPerson(issuer
							.getBusinessName());
					if (Validations.validateIsNullOrNotPositive(lstOfacPerson
							.size())) {
						List<PepPerson> lstPepPerson = findPepPerson(issuer
								.getBusinessName());
						if (Validations
								.validateIsNullOrNotPositive(lstPepPerson
										.size())) {
							List<PnaPerson> lstPnaPerson = findPnaPerson(issuer
									.getBusinessName());
							if (Validations
									.validateIsNullOrNotPositive(lstPnaPerson
											.size()))
								hideDialogsListener(null);
							else {
								hideDialogsListener(null);
								viewMessageThereIsNoCodeInTypeDocumentPna();
								cleanObjectForDocumentIssuer();
								cleanHolderIssuer();
							}
						} else {
							hideDialogsListener(null);
							viewMessageThereIsNoCodeInTypeDocumentPep();
							cleanObjectForDocumentIssuer();
							cleanHolderIssuer();
						}
					} else {
						hideDialogsListener(null);
						viewMessageThereIsNoCodeInTypeDocumentOfac();
						cleanObjectForDocumentIssuer();
						cleanHolderIssuer();
					}
				}
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Validate document participant.
	 */
	@LoggerAuditWeb
	public void validateDocumentIssuer() {

		try {
			hideDialogsListener(null);
			validateNullEmptyDocumentNumber();
			Issuer issuerFilterValidations = new Issuer();
			issuerFilterValidations.setDocumentType(issuer.getDocumentType());
			issuerFilterValidations.setDocumentNumber(issuer
					.getDocumentNumber());
			issuerFilterValidations.setDocumentSource(issuer
					.getDocumentSource());

			// Validate if exists issuer with entered document type and document
			// number
			Issuer issuerResult = issuerServiceFacade
					.findIssuerByFiltersServiceFacade(issuerFilterValidations);
			if (Validations.validateIsNotNull(issuerResult)) {
				if (issuerResult.getIdIssuerPk().equals(
						issuerOrigin.getIdIssuerPk())
						&& issuerResult.getDocumentNumber().equals(
								issuerOrigin.getDocumentNumber()))
					;
				else {
					viewMessageIssuerIsAlreadyRegistered(issuerResult);
					issuer.setDocumentNumber(null);
					return;
				}
			}

			Holder holderFilter = new Holder();
			holderFilter.setDocumentType(issuer.getDocumentType());
			holderFilter.setDocumentNumber(issuer.getDocumentNumber());
			holderFilter.setDocumentSource(issuer.getDocumentSource());
			holderFilter.setRelatedCui(issuer.getRelatedCui());

			Holder holder = issuerServiceFacade
					.getHolderByDocumentTypeAndDocumentNumberServiceFacade(holderFilter);
			if (holder != null
					&& !issuerOrigin.getHolder().getIdHolderPk()
							.equals(holder.getIdHolderPk())
					&& !issuerOrigin.getHolder().getRelatedCui()
							.equals(holder.getRelatedCui())) {
				viewMessageOtherHolderIsAlreadyRegistered();
				issuer.setDocumentNumber(null);
				return;
			}

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Clean Object For Document Issuer.
	 */
	public void cleanObjectForDocumentIssuerNotDocumentNumber() {
		issuer.setBusinessName(null);
		issuer.setDescription(null);
	}

	/**
	 * View Message There Is No Code In Type Document.
	 */
	public void viewMessageThereIsNoCodeInTypeDocument() {
		StringBuilder sbBodyData = new StringBuilder();
		sbBodyData.append(IssuerDocumentType.get(issuer.getDocumentType())
				.getValue());
		sbBodyData.append(GeneralConstants.HYPHEN);
		sbBodyData.append(issuer.getDocumentNumber());
		Object[] arrBodyData = { sbBodyData.toString() };
		showMessageOnDialog(
				PropertiesUtilities
						.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities.getMessage(
						PropertiesConstants.ERROR_ADM_ISSUER_RNC_NOT_FOUND,
						arrBodyData));
		JSFUtilities.showSimpleValidationDialog();
	}

	/**
	 * Find holder .
	 * 
	 * @return object holder
	 * @throws ServiceException
	 *             service exception
	 */
	public Holder findHolder() throws ServiceException {
		Holder holderFilter = new Holder();
		if (IssuerDocumentType.RUC.getCode().equals(issuer.getDocumentType()))
			holderFilter.setDocumentType(DocumentType.RUC.getCode());
		else {
			if (IssuerDocumentType.DIO.getCode().equals(
					issuer.getDocumentType()))
				holderFilter.setDocumentType(DocumentType.DIO.getCode());
			else {
				if (IssuerDocumentType.CI.getCode().equals(
						issuer.getDocumentType()))
					holderFilter.setDocumentType(DocumentType.CI.getCode());
				else {
					if (IssuerDocumentType.CIE.getCode().equals(
							issuer.getDocumentType()))
						holderFilter
								.setDocumentType(DocumentType.CIE.getCode());
					else {
						if (IssuerDocumentType.CDN.getCode().equals(
								issuer.getDocumentType()))
							holderFilter.setDocumentType(DocumentType.CDN
									.getCode());
						else
							holderFilter.setDocumentType(DocumentType.PAS
									.getCode());
					}
				}
			}
		}
		holderFilter.setDocumentNumber(issuer.getDocumentNumber());

		return issuerServiceFacade
				.getHolderByDocumentTypeAndDocumentNumberServiceFacade(holderFilter);
	}

	/**
	 * View Message There Is No Code In NIT.
	 */
	public void viewMessageThereIsNoCodeInTypeDocumentOfac() {
		StringBuilder sbBodyData = new StringBuilder();
		sbBodyData.append(IssuerDocumentType.get(issuer.getDocumentType())
				.getValue());
		sbBodyData.append(GeneralConstants.HYPHEN);
		sbBodyData.append(issuer.getDocumentNumber());
		Object[] arrBodyData = { sbBodyData.toString() };
		showMessageOnDialog(
				PropertiesUtilities
						.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities
						.getMessage(
								PropertiesConstants.ERROR_ADM_ISSUER_TYPE_DOCUMENT_OFAC,
								arrBodyData));
		JSFUtilities.showSimpleValidationDialog();
	}

	/**
	 * View Message There Is No Code In NIT.
	 */
	public void viewMessageThereIsNoCodeInTypeDocumentPep() {
		StringBuilder sbBodyData = new StringBuilder();
		sbBodyData.append(IssuerDocumentType.get(issuer.getDocumentType())
				.getValue());
		sbBodyData.append(GeneralConstants.HYPHEN);
		sbBodyData.append(issuer.getDocumentNumber());
		Object[] arrBodyData = { sbBodyData.toString() };
		showMessageOnDialog(
				PropertiesUtilities
						.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities.getMessage(
						PropertiesConstants.ERROR_ADM_ISSUER_TYPE_DOCUMENT_PEP,
						arrBodyData));
		JSFUtilities.showSimpleValidationDialog();
	}

	/**
	 * View Message There Is No Code In NIT.
	 */
	public void viewMessageThereIsNoCodeInTypeDocumentPna() {
		StringBuilder sbBodyData = new StringBuilder();
		sbBodyData.append(IssuerDocumentType.get(issuer.getDocumentType())
				.getValue());
		sbBodyData.append(GeneralConstants.HYPHEN);
		sbBodyData.append(issuer.getDocumentNumber());
		Object[] arrBodyData = { sbBodyData.toString() };
		showMessageOnDialog(
				PropertiesUtilities
						.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities.getMessage(
						PropertiesConstants.ERROR_ADM_ISSUER_TYPE_DOCUMENT_PNA,
						arrBodyData));
		JSFUtilities.showSimpleValidationDialog();
	}

	/**
	 * Find Ofac Person.
	 * 
	 * @param name
	 *            Trade or name
	 * @return List Ofac Person
	 * @throws ServiceException
	 *             Service Exception
	 */
	public List<OfacPerson> findOfacPerson(String name) throws ServiceException {
		return issuerServiceFacade.getOfacPersonList(name);
	}

	/**
	 * Find Pep Person.
	 * 
	 * @param name
	 *            name
	 * @return List pep person
	 * @throws ServiceException
	 *             Service Exception
	 */
	public List<PepPerson> findPepPerson(String name) throws ServiceException {
		return issuerServiceFacade.getPepPersonList(name);
	}

	/**
	 * Find Pna Person.
	 * 
	 * @param name
	 *            name
	 * @return List pna person
	 * @throws ServiceException
	 *             Service Exception
	 */
	public List<PnaPerson> findPnaPerson(String name) throws ServiceException {
		return issuerServiceFacade.getPnaPersonList(name);
	}

	/**
	 * View Message Used Mnemonic.
	 * 
	 * @param issuerResult
	 *            the issuer result
	 */
	public void viewMessageUsedMnemonic(Issuer issuerResult) {
		StringBuilder sbBodyData = new StringBuilder();
		sbBodyData.append(String.valueOf(issuerResult.getIdIssuerPk()));
		sbBodyData.append(GeneralConstants.HYPHEN);
		sbBodyData.append(issuerResult.getBusinessName());
		Object[] arrBodyData = { sbBodyData.toString() };
		showMessageOnDialog(
				PropertiesUtilities
						.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities.getMessage(
						PropertiesConstants.ERROR_ADM_ISSUER_MNEMONIC_REPEATED,
						arrBodyData));
		JSFUtilities.showSimpleValidationDialog();
	}

	/**
	 * Validate mnemonic participant.
	 */
	@LoggerAuditWeb
	public void validateMnemonicIssuer() {
		try {
			hideDialogsListener(null);
			if (Validations.validateIsNotNullAndNotEmpty(issuer.getMnemonic())) {
				Issuer issuerFilterValidations = new Issuer();
				issuerFilterValidations.setMnemonic(issuer.getMnemonic());
				Issuer issuerResult = issuerServiceFacade
						.findIssuerByFiltersServiceFacade(issuerFilterValidations);
				if (Validations.validateIsNotNull(issuerResult)) {
					if (issuerResult.getIdIssuerPk().equals(
							issuer.getIdIssuerPk())
							&& issuerResult.getMnemonic().equals(
									issuer.getMnemonic()))
						;
					else {
						viewMessageUsedMnemonic(issuerResult);
						issuer.setMnemonic(null);
					}
				}
			}

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Validate resolution number.
	 */
	@LoggerAuditWeb
	public void validateResolutionNumber() {
		try {
			hideDialogsListener(null);

			if (Validations.validateIsNotNullAndNotEmpty(issuer
					.getSuperintendentResolution())) {
				if (issuerServiceFacade
						.validateExistsSuperintendentResolutionSivServiceFacade(issuer)) {

					showMessageOnDialog(
							PropertiesUtilities
									.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities
									.getMessage(PropertiesConstants.ERROR_ADM_ISSUER_RESOLUTION_SIV_REPEATED));
					JSFUtilities.showSimpleValidationDialog();

					issuer.setSuperintendentResolution(null);
				}
			}

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Check inscription stock exchange.
	 */
	public void checkInscriptionStockExchange() {
		if (Validations.validateIsNotNull(issuer.getSuperResolutionDate())) {
			minInscriptionStockExchDate = CommonsUtilities.addDate(
					issuer.getSuperResolutionDate(), 1);
		} else {
			minInscriptionStockExchDate = null;
		}

		issuer.setEnrolledStockExchDate(null);
		JSFUtilities
				.resetComponent(":frmIssuerRegister:tabViewIssuer:calInscriptionStockExchDate");
	}

	/**
	 * Check issuer depositary contract.
	 */
	public void checkIssuerDepositaryContract() {
		if (Validations.validateIsNotNull(issuer.getSuperResolutionDate())) {
			minDepositaryContractDate = CommonsUtilities.addDate(
					issuer.getSuperResolutionDate(), 1);
		} else {
			minDepositaryContractDate = null;
		}

		issuer.setContractDate(null);
		issuer.setClosingServiceDate(null);
		JSFUtilities
				.resetComponent(":frmIssuerRegister:tabViewIssuer:calContractDate");
		JSFUtilities
				.resetComponent(":frmIssuerRegister:tabViewIssuer:calEffectiveDate");
	}

	public void checkIssuerSirtexNegotiate() {
		if (indIssuerSIRTEXnegociate) {
			issuer.setIndSirtexNegotiate(BooleanType.YES.getCode());
			if(issuanceDateSelected ==null)
				issuanceDateSelected = CommonsUtilities.currentDate();
		} else {
			issuer.setIndSirtexNegotiate(BooleanType.NO.getCode());
		}
		verifyCheck();
		JSFUtilities.updateComponent("fieldModifySecuritiesData");
		
	}

	/**
	 * Begin register listener.
	 * 
	 * @return the string
	 */
	@LoggerAuditWeb
	public void beginRegisterModificationRequest() {

		hideDialogsListener(null);

		int countValidationErrors = 0;

		if (Validations.validateIsNull(issuer.getGeographicLocation()
				.getIdGeographicLocationPk())
				|| !Validations.validateIsPositiveNumber(issuer
						.getGeographicLocation().getIdGeographicLocationPk())) {
			countValidationErrors++;
		} else {
			if (Validations.validateIsNull(issuer.getDocumentType())
					|| !Validations.validateIsPositiveNumber(issuer
							.getDocumentType())) {
				countValidationErrors++;
			}

			if (Validations.validateIsNullOrEmpty(issuer.getDocumentNumber())) {
				countValidationErrors++;
			}

			if (Validations.validateIsNullOrEmpty(issuer.getBusinessName())) {
				countValidationErrors++;
			}
		}

		if (Validations.validateIsNullOrEmpty(issuer.getMnemonic())) {
			countValidationErrors++;
		}

		if (Validations.validateIsNull(issuer.getEconomicSector())
				|| !Validations.validateIsPositiveNumber(issuer
						.getEconomicSector())) {
			countValidationErrors++;
		}

		if (Validations.validateIsNull(issuer.getOfferType())
				|| !Validations.validateIsPositiveNumber(issuer.getOfferType())) {
			countValidationErrors++;
		}

		if (Validations.validateIsNull(issuer.getEconomicActivity())
				|| !Validations.validateIsPositiveNumber(issuer
						.getEconomicActivity())) {
			countValidationErrors++;
		}

		if (Validations.validateIsNullOrEmpty(issuer.getSuperResolutionDate())) {

			countValidationErrors++;
		}

		if (Validations.validateIsNullOrEmpty(issuer
				.getSuperintendentResolution())) {

			countValidationErrors++;
		}

		if (indIssuerDepositaryContract) {
			if (Validations.validateIsNullOrEmpty(issuer.getContractDate())) {
				countValidationErrors++;
			}

			if (Validations.validateIsNullOrEmpty(issuer
					.getClosingServiceDate())) {
				countValidationErrors++;
			}
		}

		if (indInscriptionStockExchange) {
			if (Validations.validateIsNullOrEmpty(issuer
					.getEnrolledStockExchDate())) {
				countValidationErrors++;
			}
		}

		if (Validations.validateIsNull(issuer.getDepartment())
				|| !Validations
						.validateIsPositiveNumber(issuer.getDepartment())) {
			countValidationErrors++;
		}

		if (Validations.validateIsNull(issuer.getProvince())
				|| !Validations.validateIsPositiveNumber(issuer.getProvince())) {
			countValidationErrors++;
		}

		if (Validations.validateIsNullOrEmpty(issuer.getLegalAddress())) {
			countValidationErrors++;
		}

		if (Validations.validateIsNullOrEmpty(issuer.getCurrency()))
			;
		else {
			if (Validations.validateIsNotNullAndNotEmpty(issuer
					.getSocialCapital())
					&& Validations.validateIsNotNullAndPositive(issuer
							.getSocialCapital().longValue()))
				;
			else {
				countValidationErrors++;
			}
		}

		if (countValidationErrors > 0) {
			return;
		}

		// Validate if at least one Legal Representative
		if (Validations.validateIsNull(legalRepresentativeHelperBean
				.getLegalRepresentativeHistory())
				|| Validations
						.validateIsNullOrNotPositive(legalRepresentativeHelperBean
								.getLstLegalRepresentativeHistory().size())) {
			showMessageOnDialog(
					PropertiesUtilities
							.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities
							.getMessage(PropertiesConstants.ERROR_ADM_ISSUER_NO_LEGAL_REPRESENTATIVE));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}

		// Validtions of participant files

		if (Validations.validateListIsNullOrEmpty(lstIssuerFiles)
				|| Validations
						.validateListIsNullOrEmpty(lstIssuerAttachmentType)) {
			showMessageOnDialog(
					PropertiesUtilities
							.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities
							.getMessage(PropertiesConstants.ERROR_ADM_ISSUER_FILE_NO_COMPLETE));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		/*
		 * Issue 1231-Para esto, se definirA que solo sea necesario adjuntar un
		 * documento como obligatorio, pero que a su vez sea posible adjuntar
		 * mas de uno.
		 * 
		 * else { if(lstIssuerFiles.size()<lstIssuerAttachmentType.size()) {
		 * showMessageOnDialog
		 * (PropertiesUtilities.getGenericMessage(GeneralConstants
		 * .LBL_HEADER_ALERT),
		 * PropertiesUtilities.getMessage(PropertiesConstants
		 * .ERROR_ADM_ISSUER_FILE_NO_COMPLETE));
		 * JSFUtilities.showSimpleValidationDialog(); return; } }
		 */
		if (existsPreviousRequest()) {
			return;
		}

		// Verificamos si existen cambios en Representante Legal, Documentos
		// adjuntos y la Pantalla del emisor en general
		if (changeExistsLegalRepresentativeHistory() == false
				&& changeExistsFileIssuer() == false
				&& changeExistsIssuer() == false) {
			showMessageOnDialog(
					PropertiesUtilities
							.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities
							.getMessage(PropertiesConstants.ERROR_ISSUER_MSG_MODIFIED_SAVE));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}

		Object[] arrBodyData = { String.valueOf(issuer.getIdIssuerPk()) };

		showMessageOnDialog(PropertiesConstants.LBL_CONFIRM_HEADER_RECORD,null,PropertiesConstants.LBL_CONFIRM_ISSUER_MODIFICATION_REQUEST_REGISTER,arrBodyData);
		JSFUtilities.showComponent("cnfRegisterRequestIssuer");

	}
	
	/**
	 * Exists Previous Request.
	 * 
	 * @return validate exists Previous Request
	 */
	private boolean existsPreviousRequest() {
		IssuerRequestTO issuerRequestTO = new IssuerRequestTO();
		issuerRequestTO.setIdIssuerPk(issuer.getIdIssuerPk());
		issuerRequestTO
				.setRequestType(IssuerRequestType.MODIFICATION.getCode());
		issuerRequestTO.setStateIssuerRequest(IssuerRequestStateType.REGISTERED
				.getCode());
		// Validate if ISSUER has previous request
		IssuerRequest issuerRequestResult = issuerServiceFacade
				.validateExistsIssuerRequestServiceFacade(issuerRequestTO);
		if (Validations.validateIsNotNull(issuerRequestResult)) {
			// If exists previous participant request, show error and return
			Object[] arrBodyData = {
					issuerRequestResult.getRequestTypeDescription(),
					issuerRequestResult.getIssuer().getCodeMnemonic() };
			showMessageOnDialog(
					PropertiesUtilities
							.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities
							.getMessage(
									PropertiesConstants.ERROR_ADM_ISSUER_BLOCK_PREVIOUS_REQUEST,
									arrBodyData));
			JSFUtilities.showSimpleValidationDialog();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Change Exists Issuer.
	 * 
	 * @return flag Compare
	 */
	public boolean changeExistsIssuer() {
		boolean flagCompare = false;
		issuer.setCurrencySirtexNegotiate(currencySelected==null?-1:currencySelected);
		issuerOrigin.setCurrencySirtexNegotiate(issuerOrigin.getCurrencySirtexNegotiate()==null?-1:issuerOrigin.getCurrencySirtexNegotiate());
		
		issuer.setPersonTypeSirtexNegotiate(personTypeSelected==null?-1:personTypeSelected);
		issuerOrigin.setPersonTypeSirtexNegotiate(issuerOrigin.getPersonTypeSirtexNegotiate()==null?-1:issuerOrigin.getPersonTypeSirtexNegotiate());
		
		issuer.setWithCouponsSirtexNegotiate(withCouponsSelected==null?-1:withCouponsSelected);
		issuerOrigin.setWithCouponsSirtexNegotiate(issuerOrigin.getWithCouponsSirtexNegotiate()==null?-1:issuerOrigin.getWithCouponsSirtexNegotiate());
		
		issuer.setIssuanceDateSirtexNegotiate(issuanceDateSelected);
		
			if (!(issuer.getGeographicLocation().getIdGeographicLocationPk()
					.equals(issuerOrigin.getGeographicLocation()
							.getIdGeographicLocationPk()))
					|| !(issuer.getDocumentType().equals(issuerOrigin
							.getDocumentType()))
					|| !(issuer.getDocumentNumber().equals(issuerOrigin
							.getDocumentNumber()))
					|| !(issuer.getBusinessName().equals(issuerOrigin
							.getBusinessName()))
					|| !(issuer.getMnemonic().equals(issuerOrigin.getMnemonic()))
					|| !(issuer.getEconomicSector().equals(issuerOrigin
							.getEconomicSector()))
					|| !(issuer.getEconomicActivity().equals(issuerOrigin
							.getEconomicActivity()))
					|| !(issuer.getSocietyType().equals(issuerOrigin
							.getSocietyType()))
					|| !(issuer.getDepartment()
							.equals(issuerOrigin.getDepartment()))
					// || !(issuer.getProvince().equals(issuerOrigin.getProvince()))
					|| !(issuer.getLegalAddress().equals(issuerOrigin
							.getLegalAddress()))
					|| !(issuer.getSuperResolutionDate().equals(issuerOrigin
							.getSuperResolutionDate()))
					|| !(issuer.getSuperintendentResolution().equals(issuerOrigin
							.getSuperintendentResolution()))
					|| !(issuer.getIndSirtexNegotiate().equals(issuerOrigin
							.getIndSirtexNegotiate()))
				    /*Verificando cambios*/						
					|| !(issuer.getIndSirtexNegotiate().equals(BooleanType.YES.getCode())?issuer.getCurrencySirtexNegotiate().equals(issuerOrigin.getCurrencySirtexNegotiate()):true)
					|| !(issuer.getIndSirtexNegotiate().equals(BooleanType.YES.getCode())?issuer.getPersonTypeSirtexNegotiate().equals(issuerOrigin.getPersonTypeSirtexNegotiate()):true)
					|| !(issuer.getIndSirtexNegotiate().equals(BooleanType.YES.getCode())?issuer.getWithCouponsSirtexNegotiate().equals(issuerOrigin.getWithCouponsSirtexNegotiate()):true)
					|| !(issuer.getIndSirtexNegotiate().equals(BooleanType.YES.getCode())?issuer.getIssuanceDateSirtexNegotiate().equals(issuerOrigin.getIssuanceDateSirtexNegotiate()):true)
			)
				flagCompare = true;
			else {
				// issue 708
				if ((Validations
						.validateIsNullOrEmpty(issuer.getConstitutionDate()) && Validations
						.validateIsNullOrEmpty(issuerOrigin.getConstitutionDate())))
					;
				else {
					if ((Validations.validateIsNotNullAndNotEmpty(issuer
							.getConstitutionDate()) && Validations
							.validateIsNotNullAndNotEmpty(issuerOrigin
									.getConstitutionDate()))) {
						if (!(issuer.getConstitutionDate().equals(issuerOrigin
								.getConstitutionDate())))
							flagCompare = true;
					} else
						flagCompare = true;
				}

				if ((Validations.validateIsNullOrEmpty(issuer.getRmvRegister()) && Validations
						.validateIsNullOrEmpty(issuerOrigin.getRmvRegister())))
					;
				else {
					if ((Validations.validateIsNotNullAndNotEmpty(issuer
							.getRmvRegister()) && Validations
							.validateIsNotNullAndNotEmpty(issuerOrigin
									.getRmvRegister()))) {
						if (!(issuer.getRmvRegister().equals(issuerOrigin
								.getRmvRegister())))
							flagCompare = true;
					} else
						flagCompare = true;
				}

				if ((Validations.validateIsNullOrEmpty(issuer.getDistrict()) && Validations
						.validateIsNullOrEmpty(issuerOrigin.getDistrict())))
					;
				else {
					if ((Validations.validateIsNotNullAndNotEmpty(issuer
							.getDistrict()) && Validations
							.validateIsNotNullAndNotEmpty(issuerOrigin
									.getDistrict()))) {
						if (!(issuer.getDistrict().equals(issuerOrigin
								.getDistrict())))
							flagCompare = true;
					} else
						flagCompare = true;
				}

				if ((Validations.validateIsNullOrEmpty(issuer.getContractDate()) && Validations
						.validateIsNullOrEmpty(issuerOrigin.getContractDate())))
					;
				else {
					if ((Validations.validateIsNotNullAndNotEmpty(issuer
							.getContractDate()) && Validations
							.validateIsNotNullAndNotEmpty(issuerOrigin
									.getContractDate()))) {
						if (!(issuer.getContractDate().equals(issuerOrigin
								.getContractDate())))
							flagCompare = true;
					} else
						flagCompare = true;
				}

				if ((Validations.validateIsNullOrEmpty(issuer
						.getClosingServiceDate()) && Validations
						.validateIsNullOrEmpty(issuerOrigin.getClosingServiceDate())))
					;
				else {
					if ((Validations.validateIsNotNullAndNotEmpty(issuer
							.getClosingServiceDate()) && Validations
							.validateIsNotNullAndNotEmpty(issuerOrigin
									.getClosingServiceDate()))) {
						if (!(issuer.getClosingServiceDate().equals(issuerOrigin
								.getClosingServiceDate())))
							flagCompare = true;
					} else
						flagCompare = true;
				}

				if ((Validations.validateIsNullOrEmpty(issuer
						.getEnrolledStockExchDate()) && Validations
						.validateIsNullOrEmpty(issuerOrigin
								.getEnrolledStockExchDate())))
					;
				else {
					if ((Validations.validateIsNotNullAndNotEmpty(issuer
							.getEnrolledStockExchDate()) && Validations
							.validateIsNotNullAndNotEmpty(issuerOrigin
									.getEnrolledStockExchDate()))) {
						if (!(issuer.getEnrolledStockExchDate().equals(issuerOrigin
								.getEnrolledStockExchDate())))
							flagCompare = true;
					} else
						flagCompare = true;
				}

				if ((Validations.validateIsNullOrEmpty(issuer.getCurrency()) && Validations
						.validateIsNullOrEmpty(issuerOrigin.getCurrency())))
					;
				else {
					if ((Validations.validateIsNotNullAndNotEmpty(issuer
							.getCurrency()) && Validations
							.validateIsNotNullAndNotEmpty(issuerOrigin
									.getCurrency()))) {
						if (!(issuer.getCurrency().equals(issuerOrigin
								.getCurrency())))
							flagCompare = true;
					} else
						flagCompare = true;
				}

				if ((Validations.validateIsNullOrEmpty(issuer.getSocialCapital()) && Validations
						.validateIsNullOrEmpty(issuerOrigin.getSocialCapital())))
					;
				else {
					if ((Validations.validateIsNotNullAndNotEmpty(issuer
							.getSocialCapital()) && Validations
							.validateIsNotNullAndNotEmpty(issuerOrigin
									.getSocialCapital()))) {
						if (!(issuer.getSocialCapital().equals(issuerOrigin
								.getSocialCapital())))
							flagCompare = true;
					} else
						flagCompare = true;
				}

				if ((Validations.validateIsNullOrEmpty(issuer.getPhoneNumber()) && Validations
						.validateIsNullOrEmpty(issuerOrigin.getPhoneNumber())))
					;
				else {
					if ((Validations.validateIsNotNullAndNotEmpty(issuer
							.getPhoneNumber()) && Validations
							.validateIsNotNullAndNotEmpty(issuerOrigin
									.getPhoneNumber()))) {
						if (!(issuer.getPhoneNumber().equals(issuerOrigin
								.getPhoneNumber())))
							flagCompare = true;
					} else
						flagCompare = true;
				}

				if ((Validations.validateIsNullOrEmpty(issuer.getFaxNumber()) && Validations
						.validateIsNullOrEmpty(issuerOrigin.getFaxNumber())))
					;
				else {
					if ((Validations.validateIsNotNullAndNotEmpty(issuer
							.getFaxNumber()) && Validations
							.validateIsNotNullAndNotEmpty(issuerOrigin
									.getFaxNumber()))) {
						if (!(issuer.getFaxNumber().equals(issuerOrigin
								.getFaxNumber())))
							flagCompare = true;
					} else
						flagCompare = true;
				}

				if ((Validations.validateIsNullOrEmpty(issuer.getWebsite()) && Validations
						.validateIsNullOrEmpty(issuerOrigin.getWebsite())))
					;
				else {
					if ((Validations.validateIsNotNullAndNotEmpty(issuer
							.getWebsite()) && Validations
							.validateIsNotNullAndNotEmpty(issuerOrigin.getWebsite()))) {
						if (!(issuer.getWebsite().equals(issuerOrigin.getWebsite())))
							flagCompare = true;
					} else
						flagCompare = true;
				}

				if ((Validations.validateIsNullOrEmpty(issuer.getEmail()) && Validations
						.validateIsNullOrEmpty(issuerOrigin.getEmail())))
					;
				else {
					if ((Validations
							.validateIsNotNullAndNotEmpty(issuer.getEmail()) && Validations
							.validateIsNotNullAndNotEmpty(issuerOrigin.getEmail()))) {
						if (!(issuer.getEmail().equals(issuerOrigin.getEmail())))
							flagCompare = true;
					} else
						flagCompare = true;
				}

				if ((Validations.validateIsNullOrEmpty(issuer.getContactName()) && Validations
						.validateIsNullOrEmpty(issuerOrigin.getContactName())))
					;
				else {
					if ((Validations.validateIsNotNullAndNotEmpty(issuer
							.getContactName()) && Validations
							.validateIsNotNullAndNotEmpty(issuerOrigin
									.getContactName()))) {
						if (!(issuer.getContactName().equals(issuerOrigin
								.getContactName())))
							flagCompare = true;
					} else
						flagCompare = true;
				}

				if ((Validations.validateIsNullOrEmpty(issuer.getContactPhone()) && Validations
						.validateIsNullOrEmpty(issuerOrigin.getContactPhone())))
					;
				else {
					if ((Validations.validateIsNotNullAndNotEmpty(issuer
							.getContactPhone()) && Validations
							.validateIsNotNullAndNotEmpty(issuerOrigin
									.getContactPhone()))) {
						if (!(issuer.getContactPhone().equals(issuerOrigin
								.getContactPhone())))
							flagCompare = true;
					} else
						flagCompare = true;
				}

				if ((Validations.validateIsNullOrEmpty(issuer.getContactEmail()) && Validations
						.validateIsNullOrEmpty(issuerOrigin.getContactEmail())))
					;
				else {
					if ((Validations.validateIsNotNullAndNotEmpty(issuer
							.getContactEmail()) && Validations
							.validateIsNotNullAndNotEmpty(issuerOrigin
									.getContactEmail()))) {
						if (!(issuer.getContactEmail().equals(issuerOrigin
								.getContactEmail())))
							flagCompare = true;
					} else
						flagCompare = true;
				}

				if ((Validations.validateIsNullOrEmpty(issuer.getObservation()) && Validations
						.validateIsNullOrEmpty(issuerOrigin.getObservation())))
					;
				else {
					if ((Validations.validateIsNotNullAndNotEmpty(issuer
							.getObservation()) && Validations
							.validateIsNotNullAndNotEmpty(issuerOrigin
									.getObservation()))) {
						if (!(issuer.getObservation().equals(issuerOrigin
								.getObservation())))
							flagCompare = true;
					} else
						flagCompare = true;
				}

				if ((Validations.validateIsNullOrEmpty(issuer
						.getCreditRatingScales()) && Validations
						.validateIsNullOrEmpty(issuerOrigin.getCreditRatingScales())))
					;
				else {
					if ((Validations.validateIsNotNullAndNotEmpty(issuer
							.getCreditRatingScales()) && Validations
							.validateIsNotNullAndNotEmpty(issuerOrigin
									.getCreditRatingScales()))) {
						if (!(issuer.getCreditRatingScales().equals(issuerOrigin
								.getCreditRatingScales())))
							flagCompare = true;
					} else
						flagCompare = true;
				}
				if ((Validations.validateIsNullOrEmpty(issuer
						.getSirtexNegotiateDate()) && Validations
						.validateIsNullOrEmpty(issuerOrigin
								.getSirtexNegotiateDate())))
					;
				else {
					if ((Validations.validateIsNotNullAndNotEmpty(issuer
							.getSirtexNegotiateDate()) && Validations
							.validateIsNotNullAndNotEmpty(issuerOrigin
									.getSirtexNegotiateDate()))) {
						if (!(issuer.getSirtexNegotiateDate().equals(issuerOrigin
								.getSirtexNegotiateDate())))
							flagCompare = true;
					} else
						flagCompare = true;
				}
				if ((Validations.validateIsNullOrEmpty(issuer.getOfferType()) && Validations
						.validateIsNullOrEmpty(issuerOrigin.getOfferType())))
					;
				else {
					if ((Validations.validateIsNotNullAndNotEmpty(issuer
							.getOfferType()) && Validations
							.validateIsNotNullAndNotEmpty(issuerOrigin
									.getOfferType()))) {
						if (!(issuer.getOfferType().equals(issuerOrigin
								.getOfferType())))
							flagCompare = true;
					} else
						flagCompare = true;
				}
			}
			/*moneda*/
			if(issuer.getCurrencySirtexNegotiate()==-1)
				issuer.setCurrencySirtexNegotiate(null);
			
			if(issuerOrigin.getCurrencySirtexNegotiate()==-1)
				issuerOrigin.setCurrencySirtexNegotiate(null);
			
			/*tipo de persona*/
			if(issuer.getPersonTypeSirtexNegotiate()==-1)
				issuer.setPersonTypeSirtexNegotiate(null);
			
			if(issuerOrigin.getPersonTypeSirtexNegotiate()==-1)
				issuerOrigin.setPersonTypeSirtexNegotiate(null);
			
			/*con cupones y sin cupones*/
			if(issuer.getWithCouponsSirtexNegotiate()==-1)
				issuer.setWithCouponsSirtexNegotiate(null);
			
			if(issuerOrigin.getWithCouponsSirtexNegotiate()==-1)
				issuerOrigin.setWithCouponsSirtexNegotiate(null);
		return flagCompare;
	}

	/**
	 * Change Exists Issuer.
	 * 
	 * @return flag Compare
	 */
	public boolean changeExistsIssuerHistoryDetail() {
		boolean flagCompare = false;
		if (!(issuer.getGeographicLocation().getIdGeographicLocationPk()
				.equals(issuerHistory.getGeographicLocation()
						.getIdGeographicLocationPk()))
				|| !(issuer.getDocumentType().equals(issuerHistory
						.getDocumentType()))
				|| !(issuer.getDocumentNumber().equals(issuerHistory
						.getDocumentNumber()))
				|| !(issuer.getBusinessName().equals(issuerHistory
						.getBusinessName()))
				|| !(issuer.getMnemonic().equals(issuerHistory.getMnemonic()))
				|| !(issuer.getEconomicSector().equals(issuerHistory
						.getEconomicSector()))
				|| !(issuer.getEconomicActivity().equals(issuerHistory
						.getEconomicActivity()))
				|| !(issuer.getSocietyType().equals(issuerHistory
						.getSocietyType()))
				|| !(issuer.getDepartment().equals(issuerHistory
						.getDepartment()))
				|| !(issuer.getProvince().equals(issuerHistory.getProvince()))
				|| !(issuer.getLegalAddress().equals(issuerHistory
						.getLegalAddress()))
				|| !(issuer.getSuperResolutionDate().equals(issuerHistory
						.getSuperResolutionDate()))
				|| !(issuer.getSuperintendentResolution().equals(issuerHistory
						.getSuperintendentResolution())))
			flagCompare = true;
		else {
			// issue 708
			if ((Validations
					.validateIsNullOrEmpty(issuer.getConstitutionDate()) && Validations
					.validateIsNullOrEmpty(issuerHistory.getConstitutionDate())))
				;
			else {
				if ((Validations.validateIsNotNullAndNotEmpty(issuer
						.getConstitutionDate()) && Validations
						.validateIsNotNullAndNotEmpty(issuerHistory
								.getConstitutionDate()))) {
					if (!(issuer.getConstitutionDate().equals(issuerHistory
							.getConstitutionDate())))
						flagCompare = true;
				} else
					flagCompare = true;
			}

			if ((Validations.validateIsNullOrEmpty(issuer.getRmvRegister()) && Validations
					.validateIsNullOrEmpty(issuerHistory.getRmvRegister())))
				;
			else {
				if ((Validations.validateIsNotNullAndNotEmpty(issuer
						.getRmvRegister()) && Validations
						.validateIsNotNullAndNotEmpty(issuerHistory
								.getRmvRegister()))) {
					if (!(issuer.getRmvRegister().equals(issuerHistory
							.getRmvRegister())))
						flagCompare = true;
				} else
					flagCompare = true;
			}

			if ((Validations.validateIsNullOrEmpty(issuer.getDistrict()) && Validations
					.validateIsNullOrEmpty(issuerHistory.getDistrict())))
				;
			else {
				if ((Validations.validateIsNotNullAndNotEmpty(issuer
						.getDistrict()) && Validations
						.validateIsNotNullAndNotEmpty(issuerHistory
								.getDistrict()))) {
					if (!(issuer.getDistrict().equals(issuerHistory
							.getDistrict())))
						flagCompare = true;
				} else
					flagCompare = true;
			}

			if ((Validations.validateIsNullOrEmpty(issuer.getContractDate()) && Validations
					.validateIsNullOrEmpty(issuerHistory.getContractDate())))
				;
			else {
				if ((Validations.validateIsNotNullAndNotEmpty(issuer
						.getContractDate()) && Validations
						.validateIsNotNullAndNotEmpty(issuerHistory
								.getContractDate()))) {
					if (!(issuer.getContractDate().equals(issuerHistory
							.getContractDate())))
						flagCompare = true;
				} else
					flagCompare = true;
			}

			if ((Validations.validateIsNullOrEmpty(issuer
					.getClosingServiceDate()) && Validations
					.validateIsNullOrEmpty(issuerHistory
							.getClosingServiceDate())))
				;
			else {
				if ((Validations.validateIsNotNullAndNotEmpty(issuer
						.getClosingServiceDate()) && Validations
						.validateIsNotNullAndNotEmpty(issuerHistory
								.getClosingServiceDate()))) {
					if (!(issuer.getClosingServiceDate().equals(issuerHistory
							.getClosingServiceDate())))
						flagCompare = true;
				} else
					flagCompare = true;
			}

			if ((Validations.validateIsNullOrEmpty(issuer
					.getEnrolledStockExchDate()) && Validations
					.validateIsNullOrEmpty(issuerHistory
							.getEnrolledStockExchDate())))
				;
			else {
				if ((Validations.validateIsNotNullAndNotEmpty(issuer
						.getEnrolledStockExchDate()) && Validations
						.validateIsNotNullAndNotEmpty(issuerHistory
								.getEnrolledStockExchDate()))) {
					if (!(issuer.getEnrolledStockExchDate()
							.equals(issuerHistory.getEnrolledStockExchDate())))
						flagCompare = true;
				} else
					flagCompare = true;
			}

			if ((Validations.validateIsNullOrEmpty(issuer.getCurrency()) && Validations
					.validateIsNullOrEmpty(issuerHistory.getCurrency())))
				;
			else {
				if ((Validations.validateIsNotNullAndNotEmpty(issuer
						.getCurrency()) && Validations
						.validateIsNotNullAndNotEmpty(issuerHistory
								.getCurrency()))) {
					if (!(issuer.getCurrency().equals(issuerHistory
							.getCurrency())))
						flagCompare = true;
				} else
					flagCompare = true;
			}

			if ((Validations.validateIsNullOrEmpty(issuer.getSocialCapital()) && Validations
					.validateIsNullOrEmpty(issuerHistory.getSocialCapital())))
				;
			else {
				if ((Validations.validateIsNotNullAndNotEmpty(issuer
						.getSocialCapital()) && Validations
						.validateIsNotNullAndNotEmpty(issuerHistory
								.getSocialCapital()))) {
					if (!(issuer.getSocialCapital().equals(issuerHistory
							.getSocialCapital())))
						flagCompare = true;
				} else
					flagCompare = true;
			}

			if ((Validations.validateIsNullOrEmpty(issuer.getPhoneNumber()) && Validations
					.validateIsNullOrEmpty(issuerHistory.getPhoneNumber())))
				;
			else {
				if ((Validations.validateIsNotNullAndNotEmpty(issuer
						.getPhoneNumber()) && Validations
						.validateIsNotNullAndNotEmpty(issuerHistory
								.getPhoneNumber()))) {
					if (!(issuer.getPhoneNumber().equals(issuerHistory
							.getPhoneNumber())))
						flagCompare = true;
				} else
					flagCompare = true;
			}

			if ((Validations.validateIsNullOrEmpty(issuer.getFaxNumber()) && Validations
					.validateIsNullOrEmpty(issuerHistory.getFaxNumber())))
				;
			else {
				if ((Validations.validateIsNotNullAndNotEmpty(issuer
						.getFaxNumber()) && Validations
						.validateIsNotNullAndNotEmpty(issuerHistory
								.getFaxNumber()))) {
					if (!(issuer.getFaxNumber().equals(issuerHistory
							.getFaxNumber())))
						flagCompare = true;
				} else
					flagCompare = true;
			}

			if ((Validations.validateIsNullOrEmpty(issuer.getWebsite()) && Validations
					.validateIsNullOrEmpty(issuerHistory.getWebsite())))
				;
			else {
				if ((Validations.validateIsNotNullAndNotEmpty(issuer
						.getWebsite()) && Validations
						.validateIsNotNullAndNotEmpty(issuerHistory
								.getWebsite()))) {
					if (!(issuer.getWebsite()
							.equals(issuerHistory.getWebsite())))
						flagCompare = true;
				} else
					flagCompare = true;
			}

			if ((Validations.validateIsNullOrEmpty(issuer.getEmail()) && Validations
					.validateIsNullOrEmpty(issuerHistory.getEmail())))
				;
			else {
				if ((Validations
						.validateIsNotNullAndNotEmpty(issuer.getEmail()) && Validations
						.validateIsNotNullAndNotEmpty(issuerHistory.getEmail()))) {
					if (!(issuer.getEmail().equals(issuerHistory.getEmail())))
						flagCompare = true;
				} else
					flagCompare = true;
			}

			if ((Validations.validateIsNullOrEmpty(issuer.getContactName()) && Validations
					.validateIsNullOrEmpty(issuerHistory.getContactName())))
				;
			else {
				if ((Validations.validateIsNotNullAndNotEmpty(issuer
						.getContactName()) && Validations
						.validateIsNotNullAndNotEmpty(issuerHistory
								.getContactName()))) {
					if (!(issuer.getContactName().equals(issuerHistory
							.getContactName())))
						flagCompare = true;
				} else
					flagCompare = true;
			}

			if ((Validations.validateIsNullOrEmpty(issuer.getContactPhone()) && Validations
					.validateIsNullOrEmpty(issuerHistory.getContactPhone())))
				;
			else {
				if ((Validations.validateIsNotNullAndNotEmpty(issuer
						.getContactPhone()) && Validations
						.validateIsNotNullAndNotEmpty(issuerHistory
								.getContactPhone()))) {
					if (!(issuer.getContactPhone().equals(issuerHistory
							.getContactPhone())))
						flagCompare = true;
				} else
					flagCompare = true;
			}

			if ((Validations.validateIsNullOrEmpty(issuer.getContactEmail()) && Validations
					.validateIsNullOrEmpty(issuerHistory.getContactEmail())))
				;
			else {
				if ((Validations.validateIsNotNullAndNotEmpty(issuer
						.getContactEmail()) && Validations
						.validateIsNotNullAndNotEmpty(issuerHistory
								.getContactEmail()))) {
					if (!(issuer.getContactEmail().equals(issuerHistory
							.getContactEmail())))
						flagCompare = true;
				} else
					flagCompare = true;
			}

			if ((Validations.validateIsNullOrEmpty(issuer.getObservation()) && Validations
					.validateIsNullOrEmpty(issuerHistory.getObservation())))
				;
			else {
				if ((Validations.validateIsNotNullAndNotEmpty(issuer
						.getObservation()) && Validations
						.validateIsNotNullAndNotEmpty(issuerHistory
								.getObservation()))) {
					if (!(issuer.getObservation().equals(issuerHistory
							.getObservation())))
						flagCompare = true;
				} else
					flagCompare = true;
			}
		}
		return flagCompare;
	}

	/**
	 * change Exists File Issuer.
	 * 
	 * @return flag Compare
	 */
	public boolean changeExistsFileIssuer() {
		boolean flagCompare = false;
		if (Validations.validateListIsNotNullAndNotEmpty(lstIssuerFiles)
		// && Validations.validateListIsNotNullAndNotEmpty(lstIssuerFilesOrigin)
		) {
			if (lstIssuerFiles.size() == lstIssuerFilesOrigin.size()) {
				for (int i = 0; i < lstIssuerFilesOrigin.size(); i++) {
					if (Validations.validateIsNotNullAndNotEmpty(lstIssuerFiles
							.get(i).getIdIssuerFilePk())) {
						if (lstIssuerFilesOrigin
								.get(i)
								.getIdIssuerFilePk()
								.equals(lstIssuerFiles.get(i)
										.getIdIssuerFilePk()))
							;
						else {
							flagCompare = true;
							break;
						}
					} else {
						flagCompare = true;
						break;
					}

				}
			} else
				flagCompare = true;
		}
		return flagCompare;
	}

	/**
	 * change Exists Legal Representative History.
	 * 
	 * @return flag Compare
	 */
	public boolean changeExistsLegalRepresentativeHistory() {
		boolean flagCompare = false;
		if (Validations
				.validateListIsNotNullAndNotEmpty(legalRepresentativeHelperBean
						.getLstLegalRepresentativeHistory())
		// &&
		// Validations.validateListIsNotNullAndNotEmpty(lstLegalRepresentativeHistoryOrigin
		// )
		) {
			if (legalRepresentativeHelperBean
					.getLstLegalRepresentativeHistory().size() == lstLegalRepresentativeHistoryOrigin
					.size()) {
				for (int i = 0; i < lstLegalRepresentativeHistoryOrigin.size(); i++) {
					if (Validations
							.validateIsNotNullAndNotEmpty(legalRepresentativeHelperBean
									.getLstLegalRepresentativeHistory().get(i)
									.getIdRepresentativeHistoryPk())) {
						if (lstLegalRepresentativeHistoryOrigin
								.get(i)
								.getIdRepresentativeHistoryPk()
								.equals(legalRepresentativeHelperBean
										.getLstLegalRepresentativeHistory()
										.get(i).getIdRepresentativeHistoryPk()))
							;
						else {
							flagCompare = true;
							break;
						}
					} else {
						flagCompare = true;
						break;
					}

				}
			} else
				flagCompare = true;
		}
		return flagCompare;
	}

	/**
	 * Save register modification request listener.
	 */
	@LoggerAuditWeb
	public void saveRegisterModificationRequestListener() {

		try {

			JSFUtilities.executeJavascriptFunction("cnfwIssuerRegisterRequest.hide()");

			if (changeExistsFileIssuer())
				issuer.setIssuerFiles(lstIssuerFiles);
			else
				issuer.setIssuerFiles(null);

			issuer.setIndStockExchEnrolled(indInscriptionStockExchange ? BooleanType.YES.getCode() : BooleanType.NO.getCode());
			issuer.setIndContractDepository(indIssuerDepositaryContract ? BooleanType.YES.getCode() : BooleanType.NO.getCode());
			issuer.setIndDifferentiated(indIssuerDifferentiated ? BooleanType.YES.getCode() : BooleanType.NO.getCode());
			
			/**
			 * Campos a ser registrador para la modificacion de valores segun
			 * los datos
			 */
			issuer.setIndSirtexNegotiate(indIssuerSIRTEXnegociate ? BooleanType.YES.getCode() : BooleanType.NO.getCode());
			//issuer.setSecurityClassSirtexNegotiate(securityClassSelected!=null?securityClassSelected:null);
			issuer.setCurrencySirtexNegotiate(currencySelected!=null?currencySelected:null);
			issuer.setPersonTypeSirtexNegotiate(personTypeSelected!=null?personTypeSelected:null);
			issuer.setWithCouponsSirtexNegotiate(withCouponsSelected!=null?withCouponsSelected:null);
			issuer.setIssuanceDateSirtexNegotiate(issuanceDateSelected!=null?issuanceDateSelected:null);

			if (Validations.validateIsNullOrNotPositive(issuer.getCurrency())) {
				issuer.setCurrency(null);
				issuer.setSocialCapital(null);
			}

			if (existsPreviousRequest()) {
				return;
			}

			IssuerRequest issuerRequestResultTransaction = null;
			if (changeExistsLegalRepresentativeHistory()) {
				issuerRequestResultTransaction = issuerServiceFacade.registryIssuerModificacionRequestServiceFacade(issuer,legalRepresentativeHelperBean										.getLstLegalRepresentativeHistory());
			} else {
				issuerRequestResultTransaction = issuerServiceFacade.registryIssuerModificacionRequestServiceFacade(issuer,
								null);
			}

			if (Validations.validateIsNotNull(issuerRequestResultTransaction)) {

				BusinessProcess businessProcessNotification = new BusinessProcess();
				businessProcessNotification
						.setIdBusinessProcessPk(BusinessProcessType.ISSUER_MODIFICATION_REGISTER
								.getCode());
				notificationServiceFacade.sendNotification(userInfo
						.getUserAccountSession().getUserName(),
						businessProcessNotification, issuer.getIdIssuerPk(),
						null);

				Object[] arrBodyData = { String
						.valueOf(issuerRequestResultTransaction
								.getRequestNumber()) };
				showMessageOnDialog(
						PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						PropertiesUtilities
								.getMessage(
										PropertiesConstants.LBL_SUCCESS_ISSUER_MODIFICATION_REQUEST_REGISTER,
										arrBodyData));
				JSFUtilities.showComponent("cnfEndTransactionIssuer");

				cleanSearchModifRequest();
			}

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}

	}

	/**
	 * Clean search modif request.
	 */
	public void cleanSearchModifRequest() {
		hideDialogsListener(null);
		issuerTO = new IssuerTO();
		IssuerRequestTO issuerRequestTO = new IssuerRequestTO();
		issuerRequestTO.setInitiaRequestDate(CommonsUtilities.currentDate());
		issuerRequestTO.setEndRequestDate(CommonsUtilities.currentDate());

		issuerTO.setIssuerRequestTO(issuerRequestTO);
		issuerRequestDataModel = null;
	}

	/**
	 * Hide dialogs listener.
	 * 
	 * @param actionEvent
	 *            the action event
	 */
	public void hideDialogsListener(ActionEvent actionEvent) {
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		JSFUtilities.hideGeneralDialogues();

		JSFUtilities.hideComponent("cnfRegisterRequestIssuer");
		JSFUtilities.hideComponent("cnfEndTransactionParticipant");
		JSFUtilities.hideComponent("cnfMsgBusinessValidation");
		JSFUtilities.hideComponent("cnfRegisterIssuerRequest");
		JSFUtilities.hideComponent("cnfIssuerRequestConfirmation");
	}

	/**
	 * Find participant information.
	 * 
	 * @param filter
	 *            the filter
	 * @return the participant
	 * @throws ServiceException
	 *             the service exception
	 */
	private Issuer findIssuerInformation(Issuer filter) throws ServiceException {
		Issuer issuerResult = issuerServiceFacade
				.findIssuerByFiltersServiceFacade(filter);

		if (Validations.validateIsNotNull(issuerResult)) {

			issuer = issuerResult;

			ParameterTableTO filterParameterTable = new ParameterTableTO();
			filterParameterTable.setState(ParameterTableStateType.REGISTERED
					.getCode());
			filterParameterTable.setMasterTableFk(MasterTableType.ISSUER_STATE
					.getCode());
			lstIssuerState = generalParametersFacade
					.getListParameterTableServiceBean(filterParameterTable);

			loadCombosIssuerManagement();

			if (Validations.validateIsNotNullAndPositive(issuer
					.getGeographicLocation().getIdGeographicLocationPk())) {
				if (countryResidence.equals(issuer.getGeographicLocation()
						.getIdGeographicLocationPk())) {
					loadIssuerDocumentTypeNational();
					if (BooleanType.YES.getCode().equals(
							issuer.getIndDifferentiated()))
						loadIssuerFileTypes(MasterTableType.ISSUER_FILE_DIFFERENTIATED_TYPE
								.getCode());
					else
						listIssuerFileNationalTypes();
				} else {
					loadIssuerDocumentTypeForeign();
					listIssuerFileForeignTypes();
				}
			} else {
				lstIssuerAttachmentType = null;
			}

			return issuerResult;
		} else {
			return null;
		}
	}

	/**
	 * Change resolution ASFI date.
	 * 
	 * @param event
	 *            the event
	 */
	public void changeResolutionSivDate(SelectEvent event) {
		// Depositary contract date must be grater than super resolution ASFI
		// date and less or equals than current date
		minDepositaryContractDate = CommonsUtilities.addDate(
				issuer.getSuperResolutionDate(), 1);

		if (Validations.validateIsNotNull(issuer.getContractDate())
				&& !issuer.getContractDate().after(
						issuer.getSuperResolutionDate())) {
			issuer.setContractDate(null);
			issuer.setClosingServiceDate(null);
		}

		// Inscription Stock Exchange Date must be grater than super resolution
		// SIV date and less than current date
		minInscriptionStockExchDate = CommonsUtilities.addDate(
				issuer.getSuperResolutionDate(), 1);

		if (Validations.validateIsNotNull(issuer.getEnrolledStockExchDate())
				&& !issuer.getEnrolledStockExchDate().after(
						issuer.getSuperResolutionDate())) {
			issuer.setEnrolledStockExchDate(null);
		}
	}

	/**
	 * Change selected currency.
	 */
	public void changeSelectedCurrency() {
		issuer.setSocialCapital(null);
		JSFUtilities
				.resetComponent(":frmIssuerRegister:tabViewIssuer:txtQuantity");
		JSFUtilities
				.resetComponent(":frmIssuerRegister:tabViewIssuer:msgQuantity");
	}

	/**
	 * List issuer file national types.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	private void listIssuerFileNationalTypes() throws ServiceException {
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED
				.getCode());
		filterParameterTable
				.setMasterTableFk(MasterTableType.ISSUER_FILE_NATIONAL_TYPE
						.getCode());
		lstIssuerAttachmentType = generalParametersFacade
				.getListParameterTableServiceBean(filterParameterTable);
	}

	/**
	 * List participant file foreign types.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	private void listIssuerFileForeignTypes() throws ServiceException {
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED
				.getCode());
		filterParameterTable
				.setMasterTableFk(MasterTableType.ISSUER_FILE_FOREIGN_TYPE
						.getCode());
		lstIssuerAttachmentType = generalParametersFacade
				.getListParameterTableServiceBean(filterParameterTable);
	}

	/**
	 * Search issuer modification request by filters.
	 */
	@LoggerAuditWeb
	public void searchIssuerModificationRequestByFilters() {
		try {
			hideDialogsListener(null);
			if (userInfo.getUserAccountSession().isDepositaryInstitution())
				validateDateSearch();
			firsRegisterDataTable = 0;
			issuerTO.getIssuerRequestTO().setRequestType(
					IssuerRequestType.MODIFICATION.getCode());
			issuerRequestDataModel = new IssuerRequestDataModel(
					issuerServiceFacade
							.getListIssuerRequestsModificationByFiltersServiceFacade(issuerTO));
			if (issuerRequestDataModel.getRowCount() > 0)
				setTrueValidButtons();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Sets the true valid buttons.
	 */
	public void setTrueValidButtons() {
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		if (userPrivilege.getUserAcctions().isConfirm()) {
			privilegeComponent.setBtnConfirmView(true);
		}
		if (userPrivilege.getUserAcctions().isReject()) {
			privilegeComponent.setBtnRejectView(true);
		}
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}

	/**
	 * Validate Date Search.
	 */
	public void validateDateSearch() {
		// Validations of initial dare not greater than system date
		if (issuerTO.getIssuerRequestTO().getInitiaRequestDate()
				.after(CommonsUtilities.currentDate())) {
			showMessageOnDialog(
					PropertiesUtilities
							.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities
							.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_SYS_DATE));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}

		// Validations of end dare not greater than system date
		if (issuerTO.getIssuerRequestTO().getEndRequestDate()
				.after(CommonsUtilities.currentDate())) {
			showMessageOnDialog(
					PropertiesUtilities
							.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities
							.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_SEARCH_END_DATE_GREAT_SYS_DATE));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}

		// Validations of initial dare not greater than end date
		if (issuerTO.getIssuerRequestTO().getInitiaRequestDate()
				.after(issuerTO.getIssuerRequestTO().getEndRequestDate())) {
			showMessageOnDialog(
					PropertiesUtilities
							.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities
							.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_END_DATE));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}

		// Validations of end dare not less than initial date
		if (issuerTO.getIssuerRequestTO().getEndRequestDate()
				.before(issuerTO.getIssuerRequestTO().getInitiaRequestDate())) {
			showMessageOnDialog(
					PropertiesUtilities
							.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities
							.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_END_DATE));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
	}

	/**
	 * Detail confirm issuer modification.
	 * 
	 * @return the string
	 */
	@LoggerAuditWeb
	public String detailConfirmIssuerModification() {
		try {
			hideDialogsListener(null);

			if (Validations.validateIsNull(issuerRequest)) {
				showMessageOnDialog(
						PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities
								.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
				return null;
			}

			if (!IssuerRequestStateType.REGISTERED.getCode().equals(
					issuerRequest.getState())) {
				showMessageOnDialog(
						PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities
								.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_NOT_IN_REGISTERED_STATE));
				JSFUtilities.showSimpleValidationDialog();
				return null;
			}

			idIssuerRequestPk = issuerRequest.getIdIssuerRequestPk();

			detailIssuerModification();

			setViewOperationType(ViewOperationsType.CONFIRM.getCode());

			return "issuerModificationDetailRule";

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}

	/**
	 * Detail reject issuer modification.
	 * 
	 * @return the string
	 */
	@LoggerAuditWeb
	public String detailRejectIssuerModification() {
		try {
			if (Validations.validateIsNull(issuerRequest)) {
				showMessageOnDialog(
						PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities
								.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
				return null;
			}

			if (!IssuerRequestStateType.REGISTERED.getCode().equals(
					issuerRequest.getState())) {
				showMessageOnDialog(
						PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities
								.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_NOT_IN_REGISTERED_STATE));
				JSFUtilities.showSimpleValidationDialog();
				return null;
			}

			idIssuerRequestPk = issuerRequest.getIdIssuerRequestPk();

			detailIssuerModification();

			setViewOperationType(ViewOperationsType.REJECT.getCode());

			return "issuerModificationDetailRule";
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}

	/**
	 * Get Issuer Origin Of The Request.
	 * 
	 * @param objIssuerHistory
	 *            the obj issuer history
	 * @return object Issuer
	 */
	public Issuer getIssuerOriginOfTheRequest(IssuerHistory objIssuerHistory) {
		Issuer objIssuer = new Issuer();

		objIssuer.setMnemonic(objIssuerHistory.getMnemonic());
		objIssuer.setBusinessName(objIssuerHistory.getBusinessName());
		objIssuer.setDocumentType(objIssuerHistory.getDocumentType());
		objIssuer.setDocumentNumber(objIssuerHistory.getDocumentNumber());
		objIssuer.setOfferType(objIssuerHistory.getOfferType());
		objIssuer.setDocumentSource(objIssuerHistory.getDocumentSource());
		if (objIssuerHistory.getDocumentSource() != null) {
			objIssuer.setDocumentSourceDescription(documentSourceDescription
					.get(objIssuerHistory.getDocumentSource()));
		}
		objIssuer.setRelatedCui(objIssuerHistory.getRelatedCui());
		objIssuer.setFundAdministrator(objIssuerHistory.getFundAdministrator());
		objIssuer.setFundType(objIssuerHistory.getFundType());
		objIssuer.setTransferNumber(objIssuerHistory.getTransferNumber());
		objIssuer.setSocietyType(objIssuerHistory.getSocietyType());
		objIssuer.setContractNumber(objIssuerHistory.getContractNumber());
		objIssuer.setContractDate(objIssuerHistory.getContractDate());
		objIssuer.setSuperintendentResolution(objIssuerHistory
				.getSuperintendentResolution());
		objIssuer.setSocialCapital(objIssuerHistory.getSocialCapital());
		objIssuer.setGeographicLocation(objIssuerHistory
				.getGeographicLocation());
		objIssuer.setDepartment(objIssuerHistory.getDepartment());
		objIssuer.setProvince(objIssuerHistory.getProvince());
		objIssuer.setDistrict(objIssuerHistory.getDistrict());
		objIssuer.setLegalAddress(objIssuerHistory.getLegalAddress());
		objIssuer.setPhoneNumber(objIssuerHistory.getPhoneNumber());
		objIssuer.setFaxNumber(objIssuerHistory.getFaxNumber());
		objIssuer.setWebsite(objIssuerHistory.getWebsite());
		objIssuer.setEmail(objIssuerHistory.getEmail());
		objIssuer.setContactPhone(objIssuerHistory.getContactPhone());
		objIssuer.setContactName(objIssuerHistory.getContactName());
		objIssuer.setObservation(objIssuerHistory.getObservation());
		objIssuer.setStateIssuer(objIssuerHistory.getStateIssuer());
		objIssuer.setCreationDate(objIssuerHistory.getCreationDate());
		objIssuer.setEconomicSector(objIssuerHistory.getEconomicSector());
		objIssuer.setEconomicActivity(objIssuerHistory.getEconomicActivity());
		objIssuer.setContactPhone(objIssuerHistory.getContactPhone());
		objIssuer.setCurrency(objIssuerHistory.getCurrency());
		objIssuer.setContactEmail(objIssuerHistory.getContactEmail());
		objIssuer.setRegistryUser(objIssuerHistory.getRegistryUser());
		objIssuer.setIndStockExchEnrolled(objIssuerHistory
				.getIndStockExchEnrolled());
		objIssuer.setIndContractDepository(objIssuerHistory
				.getIndContractDepository());
		objIssuer.setRetirementDate(objIssuerHistory.getRetirementDate());
		objIssuer.setClosingServiceDate(objIssuerHistory
				.getClosingServiceDate());
		objIssuer.setEnrolledStockExchDate(objIssuerHistory
				.getEnrolledStockExchDate());
		objIssuer.setSuperResolutionDate(objIssuerHistory
				.getSuperResolutionDate());
		objIssuer.setIndDifferentiated(objIssuerHistory.getIndDifferentiated());
		objIssuer.setCreditRatingScales(objIssuerHistory
				.getCreditRatingScales());
		objIssuer.setRmvRegister(objIssuerHistory.getRmvRegister());
		objIssuer.setConstitutionDate(objIssuerHistory.getConstitutionDate());
		objIssuer.setInvestorType(objIssuerHistory.getInvestorType());

		return objIssuer;
	}
	
	/**
	 * Detail issuer modification.
	 * 
	 * @return the string
	 */
	public String detailIssuerModification() {

		try {
			IssuerRequestTO issuerRequestTO = new IssuerRequestTO();
			issuerRequestTO.setIdIssuerRequestPk(this.idIssuerRequestPk);
			issuerRequest = issuerServiceFacade.findIssuerModificationRequestByIdServiceFacade(issuerRequestTO);
			billParameterTableById(issuerRequest.getIssuer().getEconomicActivity());

			for (int i = 0; i < issuerRequest.getIssuerHistories().size(); i++) {
				IssuerHistory objIssuerHistory = issuerRequest.getIssuerHistories().get(i);

				if (objIssuerHistory.getRegistryType().equals(RegistryType.ORIGIN_OF_THE_REQUEST.getCode())) {
					GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
					GeographicLocation geographicLocationResult;
					
					issuer = getIssuerOriginOfTheRequest(issuerRequest.getIssuerHistories().get(i));
					
					if(Validations.validateIsNotNullAndPositive(issuer.getSocietyType())){
						try {
							ParameterTable pTable = generalParametersFacade
									.getParamDetailServiceFacade(issuer.getSocietyType());
							if(Validations.validateIsNotNull(pTable) && Validations.validateIsNotNullAndNotEmpty(pTable.getDescription())){
								descriptionSocietyTypeIssuer = pTable.getDescription();
							}
						} catch (Exception e) {
							System.out.println("::: no se pudo obtener tipo de sociedad "+e.getMessage());
						}
					}
					
					if(Validations.validateIsNotNull(issuer.getEconomicActivity())){
						buildInvestorTypeByEconAcSelected(issuer.getEconomicActivity());
						if(Validations.validateIsNotNull(issuer.getInvestorType())){
							for(InvestorTypeModelTO it:listInvestorType){
								if(it.getId().equals(issuer.getInvestorType())){
									issuer.setInvestorTypeDescription(it.getShortName());
									break;
								}
							}
						}
					}
					
					billParameterTableById(issuer.getEconomicActivity());
					// Find the Department of Issuer
					geographicLocationTO.setIdGeographicLocationPk(issuer.getDepartment());
					geographicLocationResult = generalParametersFacade.findGeographicLocationByIdServiceFacade(geographicLocationTO);

					// Set the Province description of Issuer
					if (Validations.validateIsNotNull(geographicLocationResult)) {
						issuer.setDepartmentDescription(geographicLocationResult.getName());
					}

					// Find the Province of Issuer
					geographicLocationTO.setIdGeographicLocationPk(issuer.getProvince());
					geographicLocationResult = generalParametersFacade.findGeographicLocationByIdServiceFacade(geographicLocationTO);

					// Set the Province description of Issuer
					if (Validations.validateIsNotNull(geographicLocationResult)) {
						issuer.setProvinceDescription(geographicLocationResult
								.getName());
					}

					// Find the District of Issuer
					if (Validations.validateIsNotNull(issuer.getDistrict())) {
						geographicLocationTO.setIdGeographicLocationPk(issuer
								.getDistrict());
						geographicLocationResult = generalParametersFacade
								.findGeographicLocationByIdServiceFacade(geographicLocationTO);
					}

					// Set the District description of Issuer
					if (Validations.validateIsNotNull(geographicLocationResult)) {
						issuer.setDistrictDescription(geographicLocationResult
								.getName());
					}
					/**Clase de valor*/
					ParameterTable parameterTable = null;
					
					if(objIssuerHistory.getIndSirtexNegotiate()!=null)
						if(objIssuerHistory.getIndSirtexNegotiate().equals(BooleanType.YES.getCode()))
							issuer.setIndSirtexNegotiateDesc(BooleanType.YES.getValue());
						else
							issuer.setIndSirtexNegotiateDesc(BooleanType.NO.getValue());
					/**Moneda*/
					if(objIssuerHistory.getCurrencySirtexNegotiate()!=null){
						parameterTable = generalParametersFacade.getParamDetailServiceFacade(objIssuerHistory.getCurrencySirtexNegotiate());
						issuer.setCurrencySirtexNegotiateDesc(parameterTable.getParameterName());
					}else
						issuer.setCurrencySirtexNegotiateDesc("<<Todos>>");
					/**Tipo de persona*/
					if(objIssuerHistory.getPersonTypeSirtexNegotiate()!=null){
						parameterTable = generalParametersFacade.getParamDetailServiceFacade(objIssuerHistory.getPersonTypeSirtexNegotiate());
						issuer.setPersonTypeSirtexNegotiateDesc(parameterTable.getParameterName());
					}else
						issuer.setPersonTypeSirtexNegotiateDesc("<<Ambos>>");
					/**Con cupones*/
					if(objIssuerHistory.getWithCouponsSirtexNegotiate()!=null){
						if(objIssuerHistory.getWithCouponsSirtexNegotiate()!=null&&objIssuerHistory.getWithCouponsSirtexNegotiate().equals(BooleanType.YES.getCode()))
							issuer.setWithCouponsSirtexNegotiateDesc(BooleanType.YES.getValue());
						else
							issuer.setWithCouponsSirtexNegotiateDesc(BooleanType.NO.getValue());
					}else
						issuer.setWithCouponsSirtexNegotiateDesc("<<Ambos>>");
					
					issuer.setIssuanceDateSirtexNegotiate(objIssuerHistory.getIssuanceDateSirtexNegotiate());
					
				} else {
					if (objIssuerHistory.getRegistryType().equals(
							RegistryType.HISTORICAL_COPY.getCode())) {
						GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
						GeographicLocation geographicLocationResult;
						
						issuerHistory = issuerRequest.getIssuerHistories().get(i);
						
						if(Validations.validateIsNotNullAndPositive(issuerHistory.getSocietyType())){
							try {
								ParameterTable pTable = generalParametersFacade
										.getParamDetailServiceFacade(issuerHistory.getSocietyType());
								if(Validations.validateIsNotNull(pTable) && Validations.validateIsNotNullAndNotEmpty(pTable.getDescription())){
									descriptionSocietyType = pTable.getDescription();
								}
							} catch (Exception e) {
								System.out.println("::: no se pudo obtener tipo de sociedad "+e.getMessage());
							}
						}
						
						issuerHistory.setInvestorType(objIssuerHistory.getInvestorType());
						if(Validations.validateIsNotNull(objIssuerHistory.getEconomicActivity())){
							buildInvestorTypeByEconAcSelected(objIssuerHistory.getEconomicActivity());
							for(InvestorTypeModelTO it:listInvestorType){
								if(it.getId().equals(objIssuerHistory.getInvestorType())){
									issuerHistory.setInvestorTypeDescription(it.getShortName());
									break;
								}
							}
						}
						
						billParameterTableById(issuerHistory.getEconomicActivity());
						// Find the Department of Issuer
						geographicLocationTO.setIdGeographicLocationPk(issuerHistory.getDepartment());
						geographicLocationResult = generalParametersFacade.findGeographicLocationByIdServiceFacade(geographicLocationTO);

						// Set the Province description of Issuer
						if (Validations.validateIsNotNull(geographicLocationResult)) {
							issuerHistory.setDepartmentDescription(geographicLocationResult.getName());
						}
						if (issuerHistory.getDocumentSource() != null) {
							issuerHistory.setDepartmentDescription(documentSourceDescription.get(issuerHistory.getDocumentSource()));
						}
						// Find the Province of Issuer History
						geographicLocationTO
								.setIdGeographicLocationPk(issuerHistory
										.getProvince());
						geographicLocationResult = generalParametersFacade
								.findGeographicLocationByIdServiceFacade(geographicLocationTO);

						// Set the Province description of Issuer History
						if (Validations
								.validateIsNotNull(geographicLocationResult)) {
							issuerHistory
									.setProvinceDescription(geographicLocationResult
											.getName());
						}

						// Find the Province of Issuer History
						if (Validations.validateIsNotNull(issuerHistory
								.getDistrict())) {
							geographicLocationTO
									.setIdGeographicLocationPk(issuerHistory
											.getDistrict());
							geographicLocationResult = generalParametersFacade
									.findGeographicLocationByIdServiceFacade(geographicLocationTO);
						}

						// Set the Province description of Issuer History
						if (Validations
								.validateIsNotNull(geographicLocationResult)) {
							issuerHistory
									.setDistrictDescription(geographicLocationResult
											.getName());
						}
						ParameterTable parameterTable = null;
						/**Clase de valor*/
						/*if(issuerHistory.getSecurityClassSirtexNegotiate()!=null){
							parameterTable = generalParametersFacade.getParamDetailServiceFacade(issuerHistory.getSecurityClassSirtexNegotiate());
							issuerHistory.setSecurityClassSirtexNegotiateDesc(parameterTable.getParameterName());
						}*/
						
						/*Indicador de SIRTEX*/
						if(objIssuerHistory.getIndSirtexNegotiate()!=null)
							if(objIssuerHistory.getIndSirtexNegotiate().equals(BooleanType.YES.getCode()))
								issuerHistory.setIndSirtexNegotiateDesc(BooleanType.YES.getValue());
							else
								issuerHistory.setIndSirtexNegotiateDesc(BooleanType.NO.getValue());
						/*Indicador de moneda sirtex*/
						if(objIssuerHistory.getCurrencySirtexNegotiate()!=null)
							if(objIssuerHistory.getIndSirtexNegotiate().equals(BooleanType.YES.getCode()))
								issuerHistory.setIndSirtexNegotiateDesc(BooleanType.YES.getValue());
							else
								issuerHistory.setIndSirtexNegotiateDesc(BooleanType.NO.getValue());
						/**Fecha de emision*/
						if(objIssuerHistory.getIssuanceDateSirtexNegotiate()!=null)
							issuerHistory.setIssuanceDateSirtexNegotiate(objIssuerHistory.getIssuanceDateSirtexNegotiate());
						/**Moneda*/
						if(objIssuerHistory.getCurrencySirtexNegotiate()!=null){
							parameterTable = generalParametersFacade.getParamDetailServiceFacade(objIssuerHistory.getCurrencySirtexNegotiate());
							issuerHistory.setCurrencySirtexNegotiateDesc(parameterTable.getParameterName());
						}else
							issuerHistory.setCurrencySirtexNegotiateDesc("<<Todos>>");
						/**Tipo de persona*/
						if(objIssuerHistory.getPersonTypeSirtexNegotiate()!=null){
							parameterTable = generalParametersFacade.getParamDetailServiceFacade(objIssuerHistory.getPersonTypeSirtexNegotiate());
							issuerHistory.setPersonTypeSirtexNegotiateDesc(parameterTable.getParameterName());
						}else
							issuerHistory.setPersonTypeSirtexNegotiateDesc("<<Ambos>>");
						/**Con cupones*/
						if(objIssuerHistory.getWithCouponsSirtexNegotiate()!=null){
							if(objIssuerHistory.getWithCouponsSirtexNegotiate()!=null&&objIssuerHistory.getWithCouponsSirtexNegotiate().equals(BooleanType.YES.getCode()))
								issuerHistory.setWithCouponsSirtexNegotiateDesc(BooleanType.YES.getValue());
							else
								issuerHistory.setWithCouponsSirtexNegotiateDesc(BooleanType.NO.getValue());
						}else
							issuerHistory.setWithCouponsSirtexNegotiateDesc("<<Ambos>>");
						
						
						issuerHistory.setIssuanceDateSirtexNegotiate(objIssuerHistory.getIssuanceDateSirtexNegotiate());
					}
				}
			}

			// Load Current Files of issuer
			lstIssuerFileHistories = issuerRequest.getIssuerFileHistories();

			if (Validations
					.validateListIsNotNullAndNotEmpty(lstIssuerFileHistories)) {
				InputStream inputStream;
				StreamedContent streamedContentFile;
				lstIssuerFiles = new ArrayList<IssuerFile>();
				for (IssuerFileHistory issuerFileHistory : lstIssuerFileHistories) {
					if (issuerFileHistory.getRegistryType().equals(
							RegistryType.HISTORICAL_COPY.getCode())) {
						/*
						 * set the transient attribute streamedContentFile in
						 * order to shown the content of this file on
						 * participant detail.
						 */
						inputStream = new ByteArrayInputStream(
								issuerFileHistory.getDocumentFile());
						streamedContentFile = new DefaultStreamedContent(
								inputStream, null,
								issuerFileHistory.getFilename());
						setStreamedContentFile(streamedContentFile);
						inputStream.close();
					} else {
						if (issuerFileHistory.getRegistryType().equals(
								RegistryType.ORIGIN_OF_THE_REQUEST.getCode())) {
							IssuerFile objIssuerFile = new IssuerFile();
							objIssuerFile.setIdIssuerFilePk(issuerFileHistory
									.getIdIssuerFileHisPk());
							objIssuerFile.setDocumentType(issuerFileHistory
									.getDocumentType());
							objIssuerFile.setDescription(issuerFileHistory
									.getDescription());
							objIssuerFile.setRequestFileType(issuerFileHistory
									.getRequestFileType());
							objIssuerFile.setFilename(issuerFileHistory
									.getFilename());
							objIssuerFile.setDocumentFile(issuerFileHistory
									.getDocumentFile());

							objIssuerFile.setRegistryUser(issuerFileHistory
									.getRegistryUser());
							objIssuerFile.setRegistryDate(issuerFileHistory
									.getRegistryDate());

							objIssuerFile.setRequestFileType(issuerFileHistory
									.getRequestFileType());
							objIssuerFile.setIssuerRequest(issuerFileHistory
									.getIssuerRequest());
							objIssuerFile.setIssuer(issuer);
							objIssuerFile.setStateFile(issuerFileHistory
									.getStateFile());

							lstIssuerFiles.add(objIssuerFile);
						}
					}
				}

				for (int j = 0; j < lstIssuerFiles.size(); j++) {
					IssuerFile issuerFile = lstIssuerFiles.get(j);
					for (int i = 0; i < lstIssuerFileHistories.size(); i++) {
						IssuerFileHistory issuerFileHistory = lstIssuerFileHistories
								.get(i);
						if (issuerFileHistory.getIdIssuerFileHisPk().equals(
								issuerFile.getIdIssuerFilePk()))
							lstIssuerFileHistories.remove(i);
					}
				}
			} else {
				issuer.setIdIssuerPk(issuerRequest.getIssuer().getIdIssuerPk());
				issuer.setStateIssuer(IssuerStateType.REGISTERED.getCode());
				lstIssuerFiles = issuerServiceFacade
						.getListIssuerFilesServiceFacade(issuer);
				if (Validations
						.validateListIsNotNullAndNotEmpty(lstIssuerFiles)) {
					InputStream inputStream;
					StreamedContent streamedContentFile;
					for (IssuerFile issuerFile : lstIssuerFiles) {
						/*
						 * set the transient attribute streamedContentFile in
						 * order to shown the content of this file on
						 * participant detail.
						 */
						inputStream = new ByteArrayInputStream(
								issuerFile.getDocumentFile());
						streamedContentFile = new DefaultStreamedContent(
								inputStream, null, issuerFile.getFilename());
						setStreamedContentFile(streamedContentFile);
						inputStream.close();
					}
				}
			}

			// Load Legal Representative Histories associated to IssuerRequest
			LegalRepresentativeTO legalRepresentativeTO = new LegalRepresentativeTO();
			legalRepresentativeTO
					.setViewOperationType(ViewOperationsType.DETAIL.getCode());

			legalRepresentativeHelperBean
					.setLegalRepresentativeTO(legalRepresentativeTO);

			List<LegalRepresentativeHistory> lstLegalRepresentativeHistory = issuerServiceFacade
					.getListLegalRepresentativeHistByIssuerRequestServiceFacade(issuerRequest);

			if (Validations
					.validateListIsNotNullAndNotEmpty(lstLegalRepresentativeHistory)) {
				lstLegalRepresentativeHistoryBefore = new ArrayList<LegalRepresentativeHistory>();
				lstLegalRepresentativeHistoryAfter = new ArrayList<LegalRepresentativeHistory>();
				for (LegalRepresentativeHistory objLegalRepresentativeHistory : lstLegalRepresentativeHistory) {
					LegalRepresentative legalRepresentative = new LegalRepresentative();
					legalRepresentative
							.setIdLegalRepresentativePk(objLegalRepresentativeHistory
									.getIdRepresentativeHistoryPk());
					List<RepresentativeFileHistory> representativeFiles = issuerServiceFacade
							.getListLegalRepresentativeFileHistoryServiceBean(legalRepresentative);
					lstRepresentativeFileHistory = new ArrayList<RepresentativeFileHistory>();

					RepresentativeFileHistory representativeFileHistory;
					for (RepresentativeFileHistory legalRepresentativeFile : representativeFiles) {
						representativeFileHistory = new RepresentativeFileHistory();
						representativeFileHistory
								.setDocumentType(legalRepresentativeFile
										.getDocumentType());
						representativeFileHistory
								.setFilename(legalRepresentativeFile
										.getFilename());
						representativeFileHistory
								.setDescription(legalRepresentativeFile
										.getDescription());
						representativeFileHistory
								.setFileRepresentative(legalRepresentativeFile
										.getFileRepresentative());
						representativeFileHistory
								.setLegalRepresentativeHistory(objLegalRepresentativeHistory);
						lstRepresentativeFileHistory
								.add(representativeFileHistory);
					}

					objLegalRepresentativeHistory
							.setRepresenteFileHistory(lstRepresentativeFileHistory);

					if (objLegalRepresentativeHistory.getRegistryType().equals(
							RegistryType.ORIGIN_OF_THE_REQUEST.getCode()))
						lstLegalRepresentativeHistoryBefore
								.add(objLegalRepresentativeHistory);

					if (objLegalRepresentativeHistory.getRegistryType().equals(
							RegistryType.HISTORICAL_COPY.getCode()))
						lstLegalRepresentativeHistoryAfter
								.add(objLegalRepresentativeHistory);

				}
				legalRepresentativeHelperBean
						.setLstLegalRepresentativeHistoryDetailBefore(lstLegalRepresentativeHistoryBefore);
				legalRepresentativeHelperBean
						.setLstLegalRepresentativeHistoryDetail(lstLegalRepresentativeHistoryAfter);
			} else {
				// load Represented Entities from Issuer
				issuer.setIdIssuerPk(issuerRequest.getIssuer().getIdIssuerPk());
				issuer.setStateIssuer(IssuerStateType.REGISTERED.getCode());
				lstRepresentedEntity = issuerServiceFacade
						.getListRepresentedEntityByIssuerServiceFacade(issuer);

				if (Validations
						.validateListIsNotNullAndNotEmpty(lstRepresentedEntity)) {
					lstLegalRepresentativeHistoryBefore = new ArrayList<LegalRepresentativeHistory>();
					LegalRepresentativeHistory objLegalRepresentativeHistory;
					for (RepresentedEntity objRepresentedEntity : lstRepresentedEntity) {
						objLegalRepresentativeHistory = new LegalRepresentativeHistory();
						objLegalRepresentativeHistory
								.setIdRepresentativeHistoryPk(objRepresentedEntity
										.getLegalRepresentative()
										.getIdLegalRepresentativePk());
						objLegalRepresentativeHistory
								.setRepresentativeClass(objRepresentedEntity
										.getRepresentativeClass());
						objLegalRepresentativeHistory
								.setBirthDate(objRepresentedEntity
										.getLegalRepresentative()
										.getBirthDate());
						objLegalRepresentativeHistory
								.setDocumentNumber(objRepresentedEntity
										.getLegalRepresentative()
										.getDocumentNumber());
						objLegalRepresentativeHistory
								.setDocumentType(objRepresentedEntity
										.getLegalRepresentative()
										.getDocumentType());
						objLegalRepresentativeHistory
								.setDocumentSource(objRepresentedEntity
										.getLegalRepresentative()
										.getDocumentSource());
						objLegalRepresentativeHistory
								.setDocumentIssuanceDate(objRepresentedEntity
										.getLegalRepresentative()
										.getDocumentIssuanceDate());
						objLegalRepresentativeHistory
								.setEconomicActivity(objRepresentedEntity
										.getLegalRepresentative()
										.getEconomicActivity());
						objLegalRepresentativeHistory
								.setEconomicSector(objRepresentedEntity
										.getLegalRepresentative()
										.getEconomicSector());
						objLegalRepresentativeHistory
								.setEmail(objRepresentedEntity
										.getLegalRepresentative().getEmail());
						objLegalRepresentativeHistory
								.setFaxNumber(objRepresentedEntity
										.getLegalRepresentative()
										.getFaxNumber());
						objLegalRepresentativeHistory
								.setFirstLastName(objRepresentedEntity
										.getLegalRepresentative()
										.getFirstLastName());
						objLegalRepresentativeHistory
								.setFullName(objRepresentedEntity
										.getLegalRepresentative().getFullName());
						objLegalRepresentativeHistory
								.setHomePhoneNumber(objRepresentedEntity
										.getLegalRepresentative()
										.getHomePhoneNumber());
						objLegalRepresentativeHistory
								.setIndResidence(objRepresentedEntity
										.getLegalRepresentative()
										.getIndResidence());
						objLegalRepresentativeHistory
								.setLegalAddress(objRepresentedEntity
										.getLegalRepresentative()
										.getLegalAddress());
						objLegalRepresentativeHistory
								.setLegalDistrict(objRepresentedEntity
										.getLegalRepresentative()
										.getLegalDistrict());
						objLegalRepresentativeHistory
								.setLegalProvince(objRepresentedEntity
										.getLegalRepresentative()
										.getLegalProvince());
						objLegalRepresentativeHistory
								.setLegalDepartment(objRepresentedEntity
										.getLegalRepresentative()
										.getLegalDepartment());
						objLegalRepresentativeHistory
								.setLegalResidenceCountry(objRepresentedEntity
										.getLegalRepresentative()
										.getLegalResidenceCountry());
						objLegalRepresentativeHistory
								.setMobileNumber(objRepresentedEntity
										.getLegalRepresentative()
										.getMobileNumber());
						objLegalRepresentativeHistory
								.setName(objRepresentedEntity
										.getLegalRepresentative().getName());
						objLegalRepresentativeHistory
								.setNationality(objRepresentedEntity
										.getLegalRepresentative()
										.getNationality());
						objLegalRepresentativeHistory
								.setOfficePhoneNumber(objRepresentedEntity
										.getLegalRepresentative()
										.getOfficePhoneNumber());
						objLegalRepresentativeHistory
								.setPersonType(objRepresentedEntity
										.getLegalRepresentative()
										.getPersonType());
						objLegalRepresentativeHistory
								.setPostalAddress(objRepresentedEntity
										.getLegalRepresentative()
										.getPostalAddress());
						objLegalRepresentativeHistory
								.setPostalDepartment(objRepresentedEntity
										.getLegalRepresentative()
										.getPostalDepartment());
						objLegalRepresentativeHistory
								.setPostalDistrict(objRepresentedEntity
										.getLegalRepresentative()
										.getPostalDistrict());
						objLegalRepresentativeHistory
								.setPostalProvince(objRepresentedEntity
										.getLegalRepresentative()
										.getPostalProvince());
						objLegalRepresentativeHistory
								.setPostalResidenceCountry(objRepresentedEntity
										.getLegalRepresentative()
										.getPostalResidenceCountry());
						objLegalRepresentativeHistory
								.setSecondDocumentNumber(objRepresentedEntity
										.getLegalRepresentative()
										.getSecondDocumentNumber());
						objLegalRepresentativeHistory
								.setSecondDocumentType(objRepresentedEntity
										.getLegalRepresentative()
										.getSecondDocumentType());
						objLegalRepresentativeHistory
								.setSecondLastName(objRepresentedEntity
										.getLegalRepresentative()
										.getSecondLastName());
						objLegalRepresentativeHistory
								.setSecondNationality(objRepresentedEntity
										.getLegalRepresentative()
										.getSecondNationality());
						objLegalRepresentativeHistory
								.setSex(objRepresentedEntity
										.getLegalRepresentative().getSex());
						objLegalRepresentativeHistory
								.setStateRepreHistory(LegalRepresentativeStateType.REGISTERED
										.getCode());

						PepPerson pepPersonRepresentative = issuerServiceFacade
								.findPepPersonByLegalRepresentativeServiceFacade(objRepresentedEntity
										.getLegalRepresentative());
						if (Validations
								.validateIsNotNull(pepPersonRepresentative)) {
							objLegalRepresentativeHistory
									.setIndPEP(BooleanType.YES.getCode());
							objLegalRepresentativeHistory
									.setCategory(pepPersonRepresentative
											.getCategory());
							objLegalRepresentativeHistory
									.setRole(pepPersonRepresentative.getRole());
							objLegalRepresentativeHistory
									.setBeginningPeriod(pepPersonRepresentative
											.getBeginingPeriod());
							objLegalRepresentativeHistory
									.setEndingPeriod(pepPersonRepresentative
											.getEndingPeriod());
						} else {
							objLegalRepresentativeHistory
									.setIndPEP(BooleanType.NO.getCode());
						}

						if (Validations
								.validateIsNotNullAndPositive(objLegalRepresentativeHistory
										.getIdRepresentativeHistoryPk())
								&& Validations
										.validateListIsNullOrEmpty(objLegalRepresentativeHistory
												.getRepresenteFileHistory())) {
							LegalRepresentative legalRepresentative = new LegalRepresentative();
							legalRepresentative
									.setIdLegalRepresentativePk(objLegalRepresentativeHistory
											.getIdRepresentativeHistoryPk());
							List<LegalRepresentativeFile> representativeFiles = issuerServiceFacade
									.getListLegalRepresentativeFileServiceFacade(legalRepresentative);
							lstRepresentativeFileHistory = new ArrayList<RepresentativeFileHistory>();

							RepresentativeFileHistory representativeFileHistory;
							for (LegalRepresentativeFile legalRepresentativeFile : representativeFiles) {
								representativeFileHistory = new RepresentativeFileHistory();
								representativeFileHistory
										.setDocumentType(legalRepresentativeFile
												.getDocumentType());
								representativeFileHistory
										.setFilename(legalRepresentativeFile
												.getFilename());
								representativeFileHistory
										.setDescription(legalRepresentativeFile
												.getDescription());
								representativeFileHistory
										.setFileRepresentative(legalRepresentativeFile
												.getFileLegalRepre());
								representativeFileHistory
										.setLegalRepresentativeHistory(objLegalRepresentativeHistory);
								lstRepresentativeFileHistory
										.add(representativeFileHistory);
							}

							objLegalRepresentativeHistory
									.setRepresenteFileHistory(lstRepresentativeFileHistory);

						} else {
							lstRepresentativeFileHistory = objLegalRepresentativeHistory
									.getRepresenteFileHistory();
						}

						LegalRepresentativeTO legalRepresentativeTOAux = new LegalRepresentativeTO();
						legalRepresentativeTOAux
								.setViewOperationType(ViewOperationsType.DETAIL
										.getCode());
						legalRepresentativeTOAux
								.setFlagIssuerOrParticipant(BooleanType.YES
										.getBooleanValue());
						legalRepresentativeHelperBean
								.setLegalRepresentativeTO(legalRepresentativeTOAux);
						lstLegalRepresentativeHistoryBefore
								.add(objLegalRepresentativeHistory);
					}
					legalRepresentativeHelperBean
							.setLstLegalRepresentativeHistoryDetailBefore(lstLegalRepresentativeHistoryBefore);
					legalRepresentativeHelperBean
							.setLstLegalRepresentativeHistoryDetail(null);
				}
			}

			if (changeExistsIssuerHistoryDetail())
				indExistsIssuerHistory = false;
			else
				indExistsIssuerHistory = true;

			setViewOperationType(ViewOperationsType.DETAIL.getCode());

			return "issuerModificationDetailRule";

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}

	}

	/**
	 * Begin reject issuer request.
	 */
	@LoggerAuditWeb
	public void beginRejectIssuerRequest() {
		try {
			hideDialogsListener(null);

			if (Validations.validateIsNull(issuerRequest)) {
				showMessageOnDialog(
						PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities
								.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}

			if (!IssuerRequestStateType.REGISTERED.getCode().equals(
					issuerRequest.getState())) {
				showMessageOnDialog(
						PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities
								.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_NOT_IN_REGISTERED_STATE));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}

			loadLstIssuerRequestRejectMotives();

			issuerRequest.setActionMotive(null);
			issuerRequest.setActionOtherMotive(null);

			indDisableAcceptMotiveReject = Boolean.TRUE;
			indSelectedOtherMotiveReject = Boolean.FALSE;

			JSFUtilities
					.executeJavascriptFunction("PF('cnfwMotiveRejectIssRequest').show()");

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}

	}

	/**
	 * Load lst issuer request reject motives.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	private void loadLstIssuerRequestRejectMotives() throws ServiceException {
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED
				.getCode());
		filterParameterTable
				.setMasterTableFk(MasterTableType.ISSUER_REQUEST_REJECT_MOTIVE_TYPE
						.getCode());
		lstIssuerRequestRejectMotives = generalParametersFacade
				.getListParameterTableServiceBean(filterParameterTable);
	}

	/**
	 * Change motive reject.
	 */
	@LoggerAuditWeb
	public void changeMotiveReject() {
		if (IssuerRequestRejectMotiveType.OTHER_MOTIVE_REJECT.getCode().equals(
				issuerRequest.getActionMotive())) {
			indSelectedOtherMotiveReject = Boolean.TRUE;
			indDisableAcceptMotiveReject = Boolean.TRUE;
		} else {
			indSelectedOtherMotiveReject = Boolean.FALSE;
			if (Validations.validateIsNotNullAndPositive(issuerRequest
					.getActionMotive())) {
				indDisableAcceptMotiveReject = Boolean.FALSE;
			} else {
				indDisableAcceptMotiveReject = Boolean.TRUE;
			}
		}
		issuerRequest.setActionOtherMotive(null);
	}

	/**
	 * Type other motive reject.
	 */
	public void typeOtherMotiveReject() {
		if (Validations.validateIsNotNullAndNotEmpty(issuerRequest
				.getActionOtherMotive())) {
			setIndDisableAcceptMotiveReject(Boolean.FALSE);
		} else {
			setIndDisableAcceptMotiveReject(Boolean.TRUE);
		}
	}

	/**
	 * Accept reject motive issuer request.
	 */
	public void acceptRejectMotiveIssuerRequest() {
		hideDialogsListener(null);

		JSFUtilities
				.executeJavascriptFunction("PF('cnfwIssuerRequestReject').show()");

		int countValidationRequiredErrors = 0;

		if (Validations.validateIsNullOrNotPositive(issuerRequest
				.getActionMotive())) {
			countValidationRequiredErrors++;
		}

		if (indSelectedOtherMotiveReject
				&& Validations.validateIsNullOrEmpty(issuerRequest
						.getActionOtherMotive())) {
			countValidationRequiredErrors++;
		}

		if (countValidationRequiredErrors > 0) {
			return;
		}

		Object[] arrBodyData = { String.valueOf(issuerRequest
				.getRequestNumber()) };
		showMessageOnDialog(PropertiesConstants.LBL_CONFIRM_HEADER_RECORD,
				null, PropertiesConstants.LBL_CONFIRM_ISSUER_REQUEST_REJECT,
				arrBodyData);
		JSFUtilities
				.executeJavascriptFunction("cnfwMotiveRejectIssRequest.hide()");

	}

	/**
	 * Save reject issuer request.
	 */
	@LoggerAuditWeb
	public void saveRejectIssuerRequest() {
		try {
			JSFUtilities
					.executeJavascriptFunction("cnfwIssuerRequestReject.hide()");

			issuerRequest.setActionDate(CommonsUtilities.currentDateTime());
			issuerRequest.setState(IssuerRequestStateType.REJECTED.getCode());

			IssuerRequest issuerRequestres = issuerServiceFacade
					.rejectIssuerModificationRequestServiceFacade(issuerRequest);

			BusinessProcess businessProcessNotification = new BusinessProcess();
			businessProcessNotification
					.setIdBusinessProcessPk(BusinessProcessType.ISSUER_MODIFICATION_REJECT
							.getCode());
			notificationServiceFacade.sendNotification(userInfo
					.getUserAccountSession().getUserName(),
					businessProcessNotification, issuerRequest.getIssuer()
							.getIdIssuerPk(), null);

			Object[] arrBodyData = {
					issuerRequestres.getRequestTypeDescription(),
					String.valueOf(issuerRequestres.getRequestNumber()) };

			showMessageOnDialog(
					PropertiesUtilities
							.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities
							.getMessage(
									PropertiesConstants.LBL_SUCCESS_ISSUER_REQUEST_REJECT,
									arrBodyData));

			searchIssuerModificationRequestByFilters();

			JSFUtilities.showComponent("cnfEndTransactionIssuer");

			setViewOperationType(ViewOperationsType.CONSULT.getCode());

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Begin confirm issuer request.
	 */
	@LoggerAuditWeb
	public void beginConfirmIssuerRequest() {
		try {
			hideDialogsListener(null);

			if (Validations.validateIsNull(issuerRequest)) {
				showMessageOnDialog(
						PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities
								.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}

			if (!IssuerRequestStateType.REGISTERED.getCode().equals(
					issuerRequest.getState())) {
				showMessageOnDialog(
						PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities
								.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_NOT_IN_REGISTERED_STATE));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}

			Object[] arrBodyData = { issuerRequest.getRequestTypeDescription(),
					String.valueOf(issuerRequest.getRequestNumber()) };

			showMessageOnDialog(
					PropertiesConstants.LBL_CONFIRM_HEADER_RECORD,
					null,
					PropertiesConstants.LBL_CONFIRM_ISSUER_REQUEST_CONFIRMATION,
					arrBodyData);
			JSFUtilities.showComponent("cnfIssuerRequestConfirmation");

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Save confirm issuer request.
	 */
	@LoggerAuditWeb
	public void saveConfirmIssuerRequest() {
		try {
			
			JSFUtilities.executeJavascriptFunction("cnfwIssuerRequestConfirmation.hide()");
			
			if(!issuerServiceFacade.verifiesHolderOfTheIssuer(issuerRequest)){
				String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
				String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_VALIDATION_HOLDER);
				JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
				JSFUtilities.putViewMap("headerMessageView", headerMessage);
				JSFUtilities.showSimpleValidationDialog();
				return;
			}

			boolean transactionResult = issuerServiceFacade
					.confirmIssuerModificationRequestServiceFacade(issuerRequest);

			if (transactionResult) {

				BusinessProcess businessProcessNotification = new BusinessProcess();
				businessProcessNotification
						.setIdBusinessProcessPk(BusinessProcessType.ISSUER_MODIFICATION_CONFIRM
								.getCode());
				notificationServiceFacade.sendNotification(userInfo
						.getUserAccountSession().getUserName(),
						businessProcessNotification, issuerRequest.getIssuer()
								.getIdIssuerPk(), null);

				// if(transactionResult) {
				Object[] arrBodyData = {
						issuerRequest.getRequestTypeDescription(),
						String.valueOf(issuerRequest.getRequestNumber()) };

				showMessageOnDialog(
						PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						PropertiesUtilities
								.getMessage(
										PropertiesConstants.LBL_SUCCESS_ISSUER_REQUEST_CONFINRMATION,
										arrBodyData));

				searchIssuerModificationRequestByFilters();

				JSFUtilities.showComponent("cnfEndTransactionIssuer");

				setViewOperationType(ViewOperationsType.CONSULT.getCode());
				// }
			}

			BusinessProcess sirtexNegotiateUpdate = new BusinessProcess();
			
			Map<String, Object> details = new HashMap<String, Object>();
			details.put("idIssuerPk", issuerRequest.getIssuer().getIdIssuerPk());
			
			IssuerTO issuerTO = new IssuerTO();
			issuerTO.setIdIssuerPk(issuerRequest.getIssuer().getIdIssuerPk());
			//Issuer issuer = issuerServiceFacade.findIssuerServiceFacade(issuerTO);
			
			IssuerHistory iissuerOld = issuerServiceFacade.findIssuerHistory(issuerHistory.getIssuerRequest().getIdIssuerRequestPk(),RegistryType.ORIGIN_OF_THE_REQUEST.getCode());
			IssuerHistory iissuerbatch = issuerServiceFacade.findIssuerHistory(issuerHistory.getIssuerRequest().getIdIssuerRequestPk(),RegistryType.HISTORICAL_COPY.getCode());
			
			if(iissuerbatch.getIndSirtexNegotiate()!=null){
				if(iissuerOld.getIndSirtexNegotiate()!=null&&!iissuerOld.getIndSirtexNegotiate().equals(issuerHistory.getIndSirtexNegotiate())){
					if (iissuerbatch.getIndSirtexNegotiate().equals(BooleanType.YES.getCode())) {
						sirtexNegotiateUpdate.setIdBusinessProcessPk(BusinessProcessType.SIRTEX_ENABLE_MECHANISM_ISSUER_DPF.getCode());
						//details.put("securityClass",issuer.getSecurityClassSirtexNegotiate());
						details.put("securityClass", SecurityClassType.DPF.getCode()); 
						if(iissuerbatch.getCurrencySirtexNegotiate()!=null)
							details.put("currency", iissuerbatch.getCurrencySirtexNegotiate());
						if(iissuerbatch.getPersonTypeSirtexNegotiate()!=null)
							details.put("personType", iissuerbatch.getPersonTypeSirtexNegotiate());
						if(iissuerbatch.getWithCouponsSirtexNegotiate()!=null)
							details.put("withCoupons",iissuerbatch.getWithCouponsSirtexNegotiate());
						details.put("issueanceDate", CommonsUtilities.convertDateToString(iissuerbatch.getIssuanceDateSirtexNegotiate(),"dd/MM/yyyy"));
					} else {
						sirtexNegotiateUpdate.setIdBusinessProcessPk(BusinessProcessType.SIRTEX_DISABLE_MECHANISM_ISSUER_DPF.getCode());
					}
					batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(),sirtexNegotiateUpdate, details);
				}
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Zero to validate string.
	 * 
	 * @param str
	 *            validate string
	 * @return string to validate
	 */
	public boolean validateNotZero(String str) {
		boolean charNotZero = false;
		for (char charStr : str.toCharArray()) {
			if (charStr != '0') {
				charNotZero = true;
				break;
			}
		}
		return charNotZero;
	}

	/**
	 * Validate contact phone number format.
	 */
	public void validateContactPhoneNumberFormat() {
		hideDialogsListener(null);
		if (Validations.validateIsNotNullAndNotEmpty(issuer.getContactPhone())) {
			if (!validateNotZero(issuer.getContactPhone())) {
				showMessageOnDialog(
						PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities
								.getMessage(PropertiesConstants.ADM_ISSUER_VALIDATION_CONTACT_PHONE_ONLY_ZERO));
				JSFUtilities.showSimpleValidationDialog();
				issuer.setContactPhone(null);
			}
		}
	}

	/**
	 * Validate contact email format.
	 */
	public void validateContactEmailFormat() {
		hideDialogsListener(null);
		if (Validations.validateIsNotNullAndNotEmpty(issuer.getContactEmail())) {
			if (!CommonsUtilities.matchEmail(issuer.getContactEmail())) {
				showMessageOnDialog(
						PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities
								.getMessage(PropertiesConstants.ADM_ISSUER_VALIDATION_CONTACT_EMAIL_FORMAT));
				JSFUtilities.showSimpleValidationDialog();
				issuer.setContactEmail(null);
			}
		}
	}

	/**
	 * Validate email format.
	 */
	public void validateEmailFormat() {
		hideDialogsListener(null);
		if (Validations.validateIsNotNullAndNotEmpty(issuer.getEmail())) {
			if (!CommonsUtilities.matchEmail(issuer.getEmail())) {
				showMessageOnDialog(
						PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities
								.getMessage(PropertiesConstants.ADM_ISSUER_VALIDATION_EMAIL_FORMAT));
				JSFUtilities.showSimpleValidationDialog();
				issuer.setEmail(null);
			}
		}
	}

	/**
	 * Validate web page format.
	 */
	public void validateWebPageFormat() {
		hideDialogsListener(null);
		if (Validations.validateIsNotNullAndNotEmpty(issuer.getWebsite())) {
			if (!CommonsUtilities.matchWebPage(issuer.getWebsite())) {
				showMessageOnDialog(
						PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities
								.getMessage(PropertiesConstants.ADM_ISSUER_VALIDATION_WEB_PAGE_FORMAT));
				JSFUtilities.showSimpleValidationDialog();
				issuer.setWebsite(null);
			}
		}
	}

	/**
	 * Validate fax number format.
	 */
	public void validateFaxNumberFormat() {
		hideDialogsListener(null);
		if (Validations.validateIsNotNullAndNotEmpty(issuer.getFaxNumber())) {
			if (!validateNotZero(issuer.getFaxNumber())) {
				showMessageOnDialog(
						PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities
								.getMessage(PropertiesConstants.ADM_ISSUER_VALIDATION_FAX_ONLY_ZERO));
				JSFUtilities.showSimpleValidationDialog();
				issuer.setFaxNumber(null);
			}
		}
	}

	/**
	 * Load countries.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	private void loadCountries() throws ServiceException {
		GeographicLocationTO filterCountry = new GeographicLocationTO();
		filterCountry.setGeographicLocationType(GeographicLocationType.COUNTRY
				.getCode());
		filterCountry
				.setState(GeographicLocationStateType.REGISTERED.getCode());
		lstCountries = generalParametersFacade
				.getListGeographicLocationServiceFacade(filterCountry);
	}

	/**
	 * List the Currency of issuers.
	 * 
	 * @param filterParameterTable
	 *            Object Filter
	 * @throws ServiceException
	 *             the service exception
	 */
	public void loadListCurrency(ParameterTableTO filterParameterTable)
			throws ServiceException {
		filterParameterTable.setMasterTableFk(MasterTableType.CURRENCY
				.getCode());
		lstCurrency = generalParametersFacade
				.getListParameterTableServiceBean(filterParameterTable);
	}

	/**
	 * List the Society Type of issuers.
	 * 
	 * @param filterParameterTable
	 *            Object Filter
	 * @throws ServiceException
	 *             the service exception
	 */
	public void loadListSocietyType(ParameterTableTO filterParameterTable)
			throws ServiceException {
		// filterParameterTable.setMasterTableFk(MasterTableType.SOCIETY_TYPE.getCode());
		lstSocietyType = generalParametersFacade
				.getListSocietyTypeBySector(issuer.getEconomicSector());
	}

	/**
	 * List the Economic Sector of issuers.
	 * 
	 * @param filterParameterTable
	 *            Object Filter
	 * @throws ServiceException
	 *             the service exception
	 */
	public void loadListEconomicSector(ParameterTableTO filterParameterTable)
			throws ServiceException {
		filterParameterTable.setMasterTableFk(MasterTableType.ECONOMIC_SECTOR
				.getCode());
		lstEconomicSector = generalParametersFacade
				.getListParameterTableServiceBean(filterParameterTable);
	}

	/**
	 * List the Offer Type of issuers.
	 * 
	 * @param filterParameterTable
	 *            Object Filter
	 * @throws ServiceException
	 *             the service exception
	 */
	public void loadListOfferType(ParameterTableTO filterParameterTable)
			throws ServiceException {
		filterParameterTable
				.setMasterTableFk(MasterTableType.MASTER_TABLE_OFFER_TYPE
						.getCode());
		lstOfferType = generalParametersFacade
				.getListParameterTableServiceBean(filterParameterTable);
	}

	/**
	 * Value change cmb document issuer.
	 */
	public void valueChangeCmbDocumentIssuer() {
		setIssuerFileNameDisplay(null);
	}

	/**
	 * Document attach issuer file.
	 * 
	 * @param event
	 *            the event
	 */
	public void documentAttachIssuerFile(FileUploadEvent event) {

		if (Validations.validateIsNotNullAndPositive(attachIssuerFileSelected)) {

			String fDisplayName = fUploadValidateFile(event.getFile(), null,
					fUploadFileDocumentsSize, getfUploadFileDocumentsTypes());

			if (fDisplayName != null) {
				issuerFileNameDisplay = fDisplayName;
			}

			for (int i = 0; i < lstIssuerAttachmentType.size(); i++) {
				if (lstIssuerAttachmentType.get(i).getParameterTablePk()
						.equals(attachIssuerFileSelected)) {
					IssuerFile issuerFile = new IssuerFile();
					issuerFile
							.setRequestFileType(RequestIssuerFileType.REGISTER
									.getCode());
					issuerFile.setDocumentType(lstIssuerAttachmentType.get(i)
							.getParameterTablePk());
					issuerFile.setDescription(lstIssuerAttachmentType.get(i)
							.getParameterName());

					issuerFile.setFilename(event.getFile().getFileName());
					issuerFile.setDocumentFile(event.getFile().getContents());
					issuerFile.setStateFile(IssuerFileStateType.REGISTERED
							.getCode());
					issuerFile.setIssuer(issuer);

					insertOrReplaceIssuerFileItem(issuerFile);
				}
			}
		}

	}

	/**
	 * Insert or replace issuer file item.
	 * 
	 * @param issuerFile
	 *            the issuer file
	 */
	public void insertOrReplaceIssuerFileItem(IssuerFile issuerFile) {

		boolean flag = false;

		int listSize = lstIssuerFiles.size();
		if (listSize > 0) {
			for (int i = 0; i < listSize; i++) {

				if (lstIssuerFiles.get(i).getDocumentType()
						.equals(issuerFile.getDocumentType())) {
					lstIssuerFiles.remove(i);
					lstIssuerFiles.add(i, issuerFile);
					attachIssuerFileSelected = GeneralConstants.NEGATIVE_ONE_INTEGER;

					flag = true;
				}
			}
			if (!flag) {
				lstIssuerFiles.add(issuerFile);
				attachIssuerFileSelected = GeneralConstants.NEGATIVE_ONE_INTEGER;
			}
		} else {
			lstIssuerFiles.add(issuerFile);
			attachIssuerFileSelected = GeneralConstants.NEGATIVE_ONE_INTEGER;
		}

	}

	/**
	 * Removes the issuer file.
	 * 
	 * @param issuerFile
	 *            the issuer file
	 */
	public void removeIssuerFile(IssuerFile issuerFile) {
		lstIssuerFiles.remove(issuerFile);
		issuerFileNameDisplay = null;
		attachIssuerFileSelected = GeneralConstants.NEGATIVE_ONE_INTEGER;
	}

	/**
	 * Check issuer differentiated.
	 */
	public void checkIssuerDifferentiated() {
		try {
			if (indIssuerDifferentiated) {
				if (Validations
						.validateListIsNotNullAndNotEmpty(legalRepresentativeHelperBean
								.getLstLegalRepresentativeHistory())) {
					for (LegalRepresentativeHistory objLegalRepresentativeHistory : legalRepresentativeHelperBean
							.getLstLegalRepresentativeHistory()) {
						if (BooleanType.NO.getCode()
								.equals(objLegalRepresentativeHistory
										.getIndResidence())) {

							showMessageOnDialog(
									PropertiesConstants.LBL_WARNING_ACTION,
									null,
									PropertiesConstants.WARNING_DELETE_REPRESENTATIVE_NO_RESIDENT,
									null);
							JSFUtilities
									.executeJavascriptFunction("cnfwDifferentiated.show();");
							return;
						}
					}
				}
				loadIssuerFileTypes(MasterTableType.ISSUER_FILE_DIFFERENTIATED_TYPE
						.getCode());
				resetIssuerFiles();
			} else {
				loadIssuerFileTypes(MasterTableType.ISSUER_FILE_NATIONAL_TYPE
						.getCode());
				resetIssuerFiles();
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Accept issuer differentiated.
	 */
	public void acceptIssuerDifferentiated() {
		try {
			hideDialogsListener(null);

			Iterator<LegalRepresentativeHistory> itrLegalRepresentativeHistory = legalRepresentativeHelperBean
					.getLstLegalRepresentativeHistory().iterator();
			LegalRepresentativeHistory objLegalRepresentativeHistory = null;
			while (itrLegalRepresentativeHistory.hasNext()) {
				objLegalRepresentativeHistory = itrLegalRepresentativeHistory
						.next();
				if (BooleanType.NO.getCode().equals(
						objLegalRepresentativeHistory.getIndResidence())) {
					itrLegalRepresentativeHistory.remove();
				}
			}

			indIssuerDifferentiated = Boolean.TRUE;
			loadIssuerFileTypes(MasterTableType.ISSUER_FILE_DIFFERENTIATED_TYPE
					.getCode());
			resetIssuerFiles();

			JSFUtilities
					.executeJavascriptFunction("cnfwDifferentiated.hide();");
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Reject issuer differentiated.
	 */
	public void rejectIssuerDifferentiated() {
		indIssuerDifferentiated = Boolean.FALSE;
		JSFUtilities.executeJavascriptFunction("cnfwDifferentiated.hide();");
	}
	
	public void changeEconomicActivity(){
		if(Validations.validateIsNotNull(issuer) && Validations.validateIsNotNull(issuer.getEconomicActivity())){
			buildInvestorTypeByEconAcSelected(issuer.getEconomicActivity());
		}
	}
	
	public void buildInvestorTypeByEconAcSelected(Integer economicActivity){
		// construyendo la lista de tipo de inversionista
		InvestorTypeModelTO it = new InvestorTypeModelTO();
		it.setEconomicActivity(economicActivity);
		try {
			listInvestorType = issuerServiceFacade.getLstInvestorTypeModelTO(it);
		} catch (ServiceException e) {
			e.printStackTrace();
			listInvestorType = new ArrayList<>();
		}
	}

	/**
	 * Load issuer file types.
	 * 
	 * @param issuerFileType
	 *            the issuer file type
	 * @throws ServiceException
	 *             the service exception
	 */
	private void loadIssuerFileTypes(Integer issuerFileType)
			throws ServiceException {
		ParameterTableTO paramFilter = new ParameterTableTO();
		paramFilter.setMasterTableFk(issuerFileType);
		paramFilter.setState(ParameterTableStateType.REGISTERED.getCode());
		setLstIssuerAttachmentType(generalParametersFacade
				.getListParameterTableServiceBean(paramFilter));
	}

	/**
	 * Reset issuer files.
	 */
	private void resetIssuerFiles() {
		lstIssuerFiles = new ArrayList<IssuerFile>();
		issuerFileNameDisplay = null;
		attachIssuerFileSelected = GeneralConstants.NEGATIVE_ONE_INTEGER;
	}

	/**
	 * Bill parameter table by id.
	 * 
	 * @param parameterPk
	 *            the parameter pk
	 * @throws ServiceException
	 *             the service exception
	 */
	public void billParameterTableById(Integer parameterPk)
			throws ServiceException {
		billParameterTableById(parameterPk, false);
	}

	/**
	 * Bill parameter table by id.
	 * 
	 * @param parameterPk
	 *            the parameter pk
	 * @param isObj
	 *            the is obj
	 * @throws ServiceException
	 *             the service exception
	 */
	public void billParameterTableById(Integer parameterPk, boolean isObj)
			throws ServiceException {
		ParameterTable pTable = generalParametersFacade
				.getParamDetailServiceFacade(parameterPk);
		if (pTable != null) {
			if (isObj) {
				super.getParametersTableMap().put(pTable.getParameterTablePk(),
						pTable);
			} else {
				super.getParametersTableMap().put(pTable.getParameterTablePk(),
						pTable.getParameterName());
			}
		}
	}

	/**
	 * Gets the lst countries.
	 * 
	 * @return the lst countries
	 */
	public List<GeographicLocation> getLstCountries() {
		return lstCountries;
	}

	/**
	 * Sets the lst countries.
	 * 
	 * @param lstCountries
	 *            the new lst countries
	 */
	public void setLstCountries(List<GeographicLocation> lstCountries) {
		this.lstCountries = lstCountries;
	}

	/**
	 * Sets the lst Departments.
	 * 
	 * @param lstDepartments
	 *            the new lst departments
	 */
	public void setLstDepartments(List<GeographicLocation> lstDepartments) {
		this.lstDepartments = lstDepartments;
	}

	/**
	 * Gets the lst Departments.
	 * 
	 * @return the lst Departments
	 */
	public List<GeographicLocation> getLstDepartments() {
		return lstDepartments;
	}

	/**
	 * Gets the lst provinces.
	 * 
	 * @return the lst provinces
	 */
	public List<GeographicLocation> getLstProvinces() {
		return lstProvinces;
	}

	/**
	 * Sets the lst provinces.
	 * 
	 * @param lstProvinces
	 *            the new lst provinces
	 */
	public void setLstProvinces(List<GeographicLocation> lstProvinces) {
		this.lstProvinces = lstProvinces;
	}

	/**
	 * Gets the lst districts.
	 * 
	 * @return the lst districts
	 */
	public List<GeographicLocation> getLstDistricts() {
		return lstDistricts;
	}

	/**
	 * Sets the lst districts.
	 * 
	 * @param lstDistricts
	 *            the new lst districts
	 */
	public void setLstDistricts(List<GeographicLocation> lstDistricts) {
		this.lstDistricts = lstDistricts;
	}

	/**
	 * Gets the lst economic sector.
	 * 
	 * @return the lst economic sector
	 */
	public List<ParameterTable> getLstEconomicSector() {
		return lstEconomicSector;
	}

	/**
	 * Sets the lst economic sector.
	 * 
	 * @param lstEconomicSector
	 *            the new lst economic sector
	 */
	public void setLstEconomicSector(List<ParameterTable> lstEconomicSector) {
		this.lstEconomicSector = lstEconomicSector;
	}

	/**
	 * Gets the lst economic activity.
	 * 
	 * @return the lst economic activity
	 */
	public List<ParameterTable> getLstEconomicActivity() {
		return lstEconomicActivity;
	}

	/**
	 * Sets the lst economic activity.
	 * 
	 * @param lstEconomicActivity
	 *            the new lst economic activity
	 */
	public void setLstEconomicActivity(List<ParameterTable> lstEconomicActivity) {
		this.lstEconomicActivity = lstEconomicActivity;
	}

	/**
	 * Checks if is ind participant national.
	 * 
	 * @return true, if is ind participant national
	 */
	public boolean isIndIssuerNational() {
		return Validations.validateIsNotNull(issuer.getGeographicLocation()
				.getIdGeographicLocationPk())
				&& countryResidence.equals(issuer.getGeographicLocation()
						.getIdGeographicLocationPk());
	}

	/**
	 * Checks if is ind participant foreign.
	 * 
	 * @return true, if is ind participant foreign
	 */
	public boolean isIndIssuerForeign() {
		return Validations.validateIsNotNull(issuer.getGeographicLocation()
				.getIdGeographicLocationPk())
				&& issuer.getGeographicLocation().getIdGeographicLocationPk() > 0
				&& !(countryResidence.equals(issuer.getGeographicLocation()
						.getIdGeographicLocationPk()));
	}

	/**
	 * Gets the lst represented entity.
	 * 
	 * @return the lst represented entity
	 */
	public List<RepresentedEntity> getLstRepresentedEntity() {
		return lstRepresentedEntity;
	}

	/**
	 * Sets the lst represented entity.
	 * 
	 * @param lstRepresentedEntity
	 *            the new lst represented entity
	 */
	public void setLstRepresentedEntity(
			List<RepresentedEntity> lstRepresentedEntity) {
		this.lstRepresentedEntity = lstRepresentedEntity;
	}

	/**
	 * Gets the lst legal representative.
	 * 
	 * @return the lst legal representative
	 */
	public List<LegalRepresentative> getLstLegalRepresentative() {
		return lstLegalRepresentative;
	}

	/**
	 * Sets the lst legal representative.
	 * 
	 * @param lstLegalRepresentative
	 *            the new lst legal representative
	 */
	public void setLstLegalRepresentative(
			List<LegalRepresentative> lstLegalRepresentative) {
		this.lstLegalRepresentative = lstLegalRepresentative;
	}

	/**
	 * Gets the legal representative history.
	 * 
	 * @return the legal representative history
	 */
	public LegalRepresentativeHistory getLegalRepresentativeHistory() {
		return legalRepresentativeHistory;
	}

	/**
	 * Sets the legal representative history.
	 * 
	 * @param legalRepresentativeHistory
	 *            the new legal representative history
	 */
	public void setLegalRepresentativeHistory(
			LegalRepresentativeHistory legalRepresentativeHistory) {
		this.legalRepresentativeHistory = legalRepresentativeHistory;
	}

	/**
	 * Gets the lst legal representative history.
	 * 
	 * @return the lst legal representative history
	 */
	public List<LegalRepresentativeHistory> getLstLegalRepresentativeHistory() {
		return lstLegalRepresentativeHistory;
	}

	/**
	 * Sets the lst legal representative history.
	 * 
	 * @param lstLegalRepresentativeHistory
	 *            the new lst legal representative history
	 */
	public void setLstLegalRepresentativeHistory(
			List<LegalRepresentativeHistory> lstLegalRepresentativeHistory) {
		this.lstLegalRepresentativeHistory = lstLegalRepresentativeHistory;
	}

	/**
	 * Gets the pep person.
	 * 
	 * @return the pep person
	 */
	public PepPerson getPepPerson() {
		return pepPerson;
	}

	/**
	 * Sets the pep person.
	 * 
	 * @param pepPerson
	 *            the new pep person
	 */
	public void setPepPerson(PepPerson pepPerson) {
		this.pepPerson = pepPerson;
	}

	/**
	 * Gets the lst representative class.
	 * 
	 * @return the lst representative class
	 */
	public List<ParameterTable> getLstRepresentativeClass() {
		return lstRepresentativeClass;
	}

	/**
	 * Sets the lst representative class.
	 * 
	 * @param lstRepresentativeClass
	 *            the new lst representative class
	 */
	public void setLstRepresentativeClass(
			List<ParameterTable> lstRepresentativeClass) {
		this.lstRepresentativeClass = lstRepresentativeClass;
	}

	/**
	 * Gets the lst document type representative.
	 * 
	 * @return the lst document type representative
	 */
	public List<ParameterTable> getLstDocumentTypeRepresentative() {
		return lstDocumentTypeRepresentative;
	}

	/**
	 * Sets the lst document type representative.
	 * 
	 * @param lstDocumentTypeRepresentative
	 *            the new lst document type representative
	 */
	public void setLstDocumentTypeRepresentative(
			List<ParameterTable> lstDocumentTypeRepresentative) {
		this.lstDocumentTypeRepresentative = lstDocumentTypeRepresentative;
	}

	/**
	 * Gets the lst economic activity representative.
	 * 
	 * @return the lst economic activity representative
	 */
	public List<ParameterTable> getLstEconomicActivityRepresentative() {
		return lstEconomicActivityRepresentative;
	}

	/**
	 * Sets the lst economic activity representative.
	 * 
	 * @param lstEconomicActivityRepresentative
	 *            the new lst economic activity representative
	 */
	public void setLstEconomicActivityRepresentative(
			List<ParameterTable> lstEconomicActivityRepresentative) {
		this.lstEconomicActivityRepresentative = lstEconomicActivityRepresentative;
	}

	/**
	 * Gets the lst sex.
	 * 
	 * @return the lst sex
	 */
	public List<ParameterTable> getLstSex() {
		return lstSex;
	}

	/**
	 * Sets the lst sex.
	 * 
	 * @param lstSex
	 *            the new lst sex
	 */
	public void setLstSex(List<ParameterTable> lstSex) {
		this.lstSex = lstSex;
	}

	/**
	 * Checks if is ind representative natural person.
	 * 
	 * @return true, if is ind representative natural person
	 */
	public boolean isIndRepresentativeNaturalPerson() {
		return indRepresentativeNaturalPerson;
	}

	/**
	 * Sets the ind representative natural person.
	 * 
	 * @param indRepresentativeNaturalPerson
	 *            the new ind representative natural person
	 */
	public void setIndRepresentativeNaturalPerson(
			boolean indRepresentativeNaturalPerson) {
		this.indRepresentativeNaturalPerson = indRepresentativeNaturalPerson;
	}

	/**
	 * Checks if is ind representative juridic person.
	 * 
	 * @return true, if is ind representative juridic person
	 */
	public boolean isIndRepresentativeJuridicPerson() {
		return indRepresentativeJuridicPerson;
	}

	/**
	 * Sets the ind representative juridic person.
	 * 
	 * @param indRepresentativeJuridicPerson
	 *            the new ind representative juridic person
	 */
	public void setIndRepresentativeJuridicPerson(
			boolean indRepresentativeJuridicPerson) {
		this.indRepresentativeJuridicPerson = indRepresentativeJuridicPerson;
	}

	/**
	 * Checks if is flag cmb representative class.
	 * 
	 * @return true, if is flag cmb representative class
	 */
	public boolean isFlagCmbRepresentativeClass() {
		return flagCmbRepresentativeClass;
	}

	/**
	 * Sets the flag cmb representative class.
	 * 
	 * @param flagCmbRepresentativeClass
	 *            the new flag cmb representative class
	 */
	public void setFlagCmbRepresentativeClass(boolean flagCmbRepresentativeClass) {
		this.flagCmbRepresentativeClass = flagCmbRepresentativeClass;
	}

	/**
	 * Checks if is ind representative resident.
	 * 
	 * @return true, if is ind representative resident
	 */
	public boolean isIndRepresentativeResident() {
		return indRepresentativeResident;
	}

	/**
	 * Sets the ind representative resident.
	 * 
	 * @param indRepresentativeResident
	 *            the new ind representative resident
	 */
	public void setIndRepresentativeResident(boolean indRepresentativeResident) {
		this.indRepresentativeResident = indRepresentativeResident;
	}

	/**
	 * Checks if is disabled country resident representative.
	 * 
	 * @return true, if is disabled country resident representative
	 */
	public boolean isDisabledCountryResidentRepresentative() {
		return disabledCountryResidentRepresentative;
	}

	/**
	 * Sets the disabled country resident representative.
	 * 
	 * @param disabledCountryResidentRepresentative
	 *            the new disabled country resident representative
	 */
	public void setDisabledCountryResidentRepresentative(
			boolean disabledCountryResidentRepresentative) {
		this.disabledCountryResidentRepresentative = disabledCountryResidentRepresentative;
	}

	/**
	 * Checks if is ind representative is pep.
	 * 
	 * @return true, if is ind representative is pep
	 */
	public boolean isIndRepresentativeIsPEP() {
		return indRepresentativeIsPEP;
	}

	/**
	 * Sets the ind representative is pep.
	 * 
	 * @param indRepresentativeIsPEP
	 *            the new ind representative is pep
	 */
	public void setIndRepresentativeIsPEP(boolean indRepresentativeIsPEP) {
		this.indRepresentativeIsPEP = indRepresentativeIsPEP;
	}

	/**
	 * Checks if is ind copy legal representative.
	 * 
	 * @return true, if is ind copy legal representative
	 */
	public boolean isIndCopyLegalRepresentative() {
		return indCopyLegalRepresentative;
	}

	/**
	 * Sets the ind copy legal representative.
	 * 
	 * @param indCopyLegalRepresentative
	 *            the new ind copy legal representative
	 */
	public void setIndCopyLegalRepresentative(boolean indCopyLegalRepresentative) {
		this.indCopyLegalRepresentative = indCopyLegalRepresentative;
	}

	/**
	 * Gets the lst person type.
	 * 
	 * @return the lst person type
	 */
	public List<ParameterTable> getLstPersonType() {
		return lstPersonType;
	}

	/**
	 * Sets the lst person type.
	 * 
	 * @param lstPersonType
	 *            the new lst person type
	 */
	public void setLstPersonType(List<ParameterTable> lstPersonType) {
		this.lstPersonType = lstPersonType;
	}

	/**
	 * Gets the lst boolean.
	 * 
	 * @return the lst boolean
	 */
	public List<BooleanType> getLstBoolean() {
		return lstBoolean;
	}

	/**
	 * Sets the lst boolean.
	 * 
	 * @param lstBoolean
	 *            the new lst boolean
	 */
	public void setLstBoolean(List<BooleanType> lstBoolean) {
		this.lstBoolean = lstBoolean;
	}

	/**
	 * Gets the lst postal provinces representative.
	 * 
	 * @return the lst postal provinces representative
	 */
	public List<GeographicLocation> getLstPostalProvincesRepresentative() {
		return lstPostalProvincesRepresentative;
	}

	/**
	 * Sets the lst postal provinces representative.
	 * 
	 * @param lstPostalProvincesRepresentative
	 *            the new lst postal provinces representative
	 */
	public void setLstPostalProvincesRepresentative(
			List<GeographicLocation> lstPostalProvincesRepresentative) {
		this.lstPostalProvincesRepresentative = lstPostalProvincesRepresentative;
	}

	/**
	 * Gets the lst postal districts representative.
	 * 
	 * @return the lst postal districts representative
	 */
	public List<GeographicLocation> getLstPostalDistrictsRepresentative() {
		return lstPostalDistrictsRepresentative;
	}

	/**
	 * Sets the lst postal districts representative.
	 * 
	 * @param lstPostalDistrictsRepresentative
	 *            the new lst postal districts representative
	 */
	public void setLstPostalDistrictsRepresentative(
			List<GeographicLocation> lstPostalDistrictsRepresentative) {
		this.lstPostalDistrictsRepresentative = lstPostalDistrictsRepresentative;
	}

	/**
	 * Gets the lst legal provinces representative.
	 * 
	 * @return the lst legal provinces representative
	 */
	public List<GeographicLocation> getLstLegalProvincesRepresentative() {
		return lstLegalProvincesRepresentative;
	}

	/**
	 * Sets the lst legal provinces representative.
	 * 
	 * @param lstLegalProvincesRepresentative
	 *            the new lst legal provinces representative
	 */
	public void setLstLegalProvincesRepresentative(
			List<GeographicLocation> lstLegalProvincesRepresentative) {
		this.lstLegalProvincesRepresentative = lstLegalProvincesRepresentative;
	}

	/**
	 * Gets the lst legal districts representative.
	 * 
	 * @return the lst legal districts representative
	 */
	public List<GeographicLocation> getLstLegalDistrictsRepresentative() {
		return lstLegalDistrictsRepresentative;
	}

	/**
	 * Sets the lst legal districts representative.
	 * 
	 * @param lstLegalDistrictsRepresentative
	 *            the new lst legal districts representative
	 */
	public void setLstLegalDistrictsRepresentative(
			List<GeographicLocation> lstLegalDistrictsRepresentative) {
		this.lstLegalDistrictsRepresentative = lstLegalDistrictsRepresentative;
	}

	/**
	 * Gets the issuer.
	 * 
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}

	/**
	 * Sets the issuer.
	 * 
	 * @param issuer
	 *            the new issuer
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	/**
	 * Gets the lst issuer document type.
	 * 
	 * @return the lst issuer document type
	 */
	public List<ParameterTable> getLstIssuerDocumentType() {
		return lstIssuerDocumentType;
	}

	/**
	 * Sets the lst issuer document type.
	 * 
	 * @param lstIssuerDocumentType
	 *            the new lst issuer document type
	 */
	public void setLstIssuerDocumentType(
			List<ParameterTable> lstIssuerDocumentType) {
		this.lstIssuerDocumentType = lstIssuerDocumentType;
	}

	/**
	 * Gets the lst issuer document type for search.
	 * 
	 * @return the lst issuer document type for search
	 */
	public List<ParameterTable> getLstIssuerDocumentTypeForSearch() {
		return lstIssuerDocumentTypeForSearch;
	}

	/**
	 * Sets the lst issuer document type for search.
	 * 
	 * @param lstIssuerDocumentTypeForSearch
	 *            the new lst issuer document type for search
	 */
	public void setLstIssuerDocumentTypeForSearch(
			List<ParameterTable> lstIssuerDocumentTypeForSearch) {
		this.lstIssuerDocumentTypeForSearch = lstIssuerDocumentTypeForSearch;
	}

	/**
	 * Gets the lst economic activity for search.
	 * 
	 * @return the lst economic activity for search
	 */
	public List<ParameterTable> getLstEconomicActivityForSearch() {
		return lstEconomicActivityForSearch;
	}

	/**
	 * Sets the lst economic activity for search.
	 * 
	 * @param lstEconomicActivityForSearch
	 *            the new lst economic activity for search
	 */
	public void setLstEconomicActivityForSearch(
			List<ParameterTable> lstEconomicActivityForSearch) {
		this.lstEconomicActivityForSearch = lstEconomicActivityForSearch;
	}

	/**
	 * Gets the lst issuer state.
	 * 
	 * @return the lst issuer state
	 */
	public List<ParameterTable> getLstIssuerState() {
		return lstIssuerState;
	}

	/**
	 * Sets the lst issuer state.
	 * 
	 * @param lstIssuerState
	 *            the new lst issuer state
	 */
	public void setLstIssuerState(List<ParameterTable> lstIssuerState) {
		this.lstIssuerState = lstIssuerState;
	}

	/**
	 * Gets the lst issuer state for search.
	 * 
	 * @return the lst issuer state for search
	 */
	public List<ParameterTable> getLstIssuerStateForSearch() {
		return lstIssuerStateForSearch;
	}

	/**
	 * Sets the lst issuer state for search.
	 * 
	 * @param lstIssuerStateForSearch
	 *            the new lst issuer state for search
	 */
	public void setLstIssuerStateForSearch(
			List<ParameterTable> lstIssuerStateForSearch) {
		this.lstIssuerStateForSearch = lstIssuerStateForSearch;
	}

	/**
	 * Gets the issuer data model.
	 * 
	 * @return the issuer data model
	 */
	public IssuerDataModel getIssuerDataModel() {
		return issuerDataModel;
	}

	/**
	 * Sets the issuer data model.
	 * 
	 * @param issuerDataModel
	 *            the new issuer data model
	 */
	public void setIssuerDataModel(IssuerDataModel issuerDataModel) {
		this.issuerDataModel = issuerDataModel;
	}

	/**
	 * Gets the issuer to.
	 * 
	 * @return the issuer to
	 */
	public IssuerTO getIssuerTO() {
		return issuerTO;
	}

	/**
	 * Sets the issuer to.
	 * 
	 * @param issuerTO
	 *            the new issuer to
	 */
	public void setIssuerTO(IssuerTO issuerTO) {
		this.issuerTO = issuerTO;
	}

	/**
	 * Gets the id issuer pk.
	 * 
	 * @return the id issuer pk
	 */
	public String getIdIssuerPk() {
		return idIssuerPk;
	}

	/**
	 * Sets the id issuer pk.
	 * 
	 * @param idIssuerPk
	 *            the new id issuer pk
	 */
	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}

	/**
	 * Gets the state issuer.
	 * 
	 * @return the state issuer
	 */
	public Integer getStateIssuer() {
		return stateIssuer;
	}

	/**
	 * Sets the state issuer.
	 * 
	 * @param stateIssuer
	 *            the new state issuer
	 */
	public void setStateIssuer(Integer stateIssuer) {
		this.stateIssuer = stateIssuer;
	}

	/**
	 * Gets the lst issuer files.
	 * 
	 * @return the lst issuer files
	 */
	public List<IssuerFile> getLstIssuerFiles() {
		return lstIssuerFiles;
	}

	/**
	 * Sets the lst issuer files Origin.
	 * 
	 * @param lstIssuerFilesOrigin
	 *            the new lst issuer files origin
	 */
	public void setLstIssuerFilesOrigin(List<IssuerFile> lstIssuerFilesOrigin) {
		this.lstIssuerFilesOrigin = lstIssuerFilesOrigin;
	}

	/**
	 * Gets the lst issuer files Origin.
	 * 
	 * @return the lst issuer files Origin
	 */
	public List<IssuerFile> getLstIssuerFilesOrigin() {
		return lstIssuerFilesOrigin;
	}

	/**
	 * Sets the lst issuer files.
	 * 
	 * @param lstIssuerFiles
	 *            the new lst issuer files
	 */
	public void setLstIssuerFiles(List<IssuerFile> lstIssuerFiles) {
		this.lstIssuerFiles = lstIssuerFiles;
	}

	/**
	 * Gets the id represented entity pk.
	 * 
	 * @return the id represented entity pk
	 */
	public Long getIdRepresentedEntityPk() {
		return idRepresentedEntityPk;
	}

	/**
	 * Sets the id represented entity pk.
	 * 
	 * @param idRepresentedEntityPk
	 *            the new id represented entity pk
	 */
	public void setIdRepresentedEntityPk(Long idRepresentedEntityPk) {
		this.idRepresentedEntityPk = idRepresentedEntityPk;
	}

	/**
	 * Gets the represented entity.
	 * 
	 * @return the represented entity
	 */
	public RepresentedEntity getRepresentedEntity() {
		return representedEntity;
	}

	/**
	 * Sets the represented entity.
	 * 
	 * @param representedEntity
	 *            the new represented entity
	 */
	public void setRepresentedEntity(RepresentedEntity representedEntity) {
		this.representedEntity = representedEntity;
	}

	/**
	 * Gets the lst economic sector for search.
	 * 
	 * @return the lst economic sector for search
	 */
	public List<ParameterTable> getLstEconomicSectorForSearch() {
		return lstEconomicSectorForSearch;
	}

	/**
	 * Sets the lst economic sector for search.
	 * 
	 * @param lstEconomicSectorForSearch
	 *            the new lst economic sector for search
	 */
	public void setLstEconomicSectorForSearch(
			List<ParameterTable> lstEconomicSectorForSearch) {
		this.lstEconomicSectorForSearch = lstEconomicSectorForSearch;
	}

	/**
	 * Gets the lst society type.
	 * 
	 * @return the lst society type
	 */
	public List<ParameterTable> getLstSocietyType() {
		return lstSocietyType;
	}

	/**
	 * Sets the lst society type.
	 * 
	 * @param lstSocietyType
	 *            the new lst society type
	 */
	public void setLstSocietyType(List<ParameterTable> lstSocietyType) {
		this.lstSocietyType = lstSocietyType;
	}

	/**
	 * Gets the lst currency.
	 * 
	 * @return the lst currency
	 */
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}

	/**
	 * Sets the lst currency.
	 * 
	 * @param lstCurrency
	 *            the new lst currency
	 */
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}

	/**
	 * Checks if is ind issuer depositary contract.
	 * 
	 * @return true, if is ind issuer depositary contract
	 */
	public boolean isIndIssuerDepositaryContract() {
		return indIssuerDepositaryContract;
	}

	/**
	 * Sets the ind issuer depositary contract.
	 * 
	 * @param indIssuerDepositaryContract
	 *            the new ind issuer depositary contract
	 */
	public void setIndIssuerDepositaryContract(
			boolean indIssuerDepositaryContract) {
		this.indIssuerDepositaryContract = indIssuerDepositaryContract;
	}

	/**
	 * Checks if is ind inscription stock exchange.
	 * 
	 * @return true, if is ind inscription stock exchange
	 */
	public boolean isIndInscriptionStockExchange() {
		return indInscriptionStockExchange;
	}

	/**
	 * Sets the ind inscription stock exchange.
	 * 
	 * @param indInscriptionStockExchange
	 *            the new ind inscription stock exchange
	 */
	public void setIndInscriptionStockExchange(
			boolean indInscriptionStockExchange) {
		this.indInscriptionStockExchange = indInscriptionStockExchange;
	}

	/**
	 * Gets the lst category pep.
	 * 
	 * @return the lst category pep
	 */
	public List<ParameterTable> getLstCategoryPep() {
		return lstCategoryPep;
	}

	/**
	 * Sets the lst category pep.
	 * 
	 * @param lstCategoryPep
	 *            the new lst category pep
	 */
	public void setLstCategoryPep(List<ParameterTable> lstCategoryPep) {
		this.lstCategoryPep = lstCategoryPep;
	}

	/**
	 * Gets the lst role pep.
	 * 
	 * @return the lst role pep
	 */
	public List<ParameterTable> getLstRolePep() {
		return lstRolePep;
	}

	/**
	 * Sets the lst role pep.
	 * 
	 * @param lstRolePep
	 *            the new lst role pep
	 */
	public void setLstRolePep(List<ParameterTable> lstRolePep) {
		this.lstRolePep = lstRolePep;
	}

	/**
	 * Checks if is ind issuer selected modif.
	 * 
	 * @return true, if is ind issuer selected modif
	 */
	public boolean isIndIssuerSelectedModif() {
		return indIssuerSelectedModif;
	}

	/**
	 * Sets the ind issuer selected modif.
	 * 
	 * @param indIssuerSelectedModif
	 *            the new ind issuer selected modif
	 */
	public void setIndIssuerSelectedModif(boolean indIssuerSelectedModif) {
		this.indIssuerSelectedModif = indIssuerSelectedModif;
	}

	/**
	 * Gets the lst issuer request state.
	 * 
	 * @return the lst issuer request state
	 */
	public List<ParameterTable> getLstIssuerRequestState() {
		return lstIssuerRequestState;
	}

	/**
	 * Sets the lst issuer request state.
	 * 
	 * @param lstIssuerRequestState
	 *            the new lst issuer request state
	 */
	public void setLstIssuerRequestState(
			List<ParameterTable> lstIssuerRequestState) {
		this.lstIssuerRequestState = lstIssuerRequestState;
	}

	/**
	 * Gets the lst issuer request.
	 * 
	 * @return the lst issuer request
	 */
	public List<IssuerRequest> getLstIssuerRequest() {
		return lstIssuerRequest;
	}

	/**
	 * Sets the lst issuer request.
	 * 
	 * @param lstIssuerRequest
	 *            the new lst issuer request
	 */
	public void setLstIssuerRequest(List<IssuerRequest> lstIssuerRequest) {
		this.lstIssuerRequest = lstIssuerRequest;
	}

	/**
	 * Gets the issuer request data model.
	 * 
	 * @return the issuer request data model
	 */
	public IssuerRequestDataModel getIssuerRequestDataModel() {
		return issuerRequestDataModel;
	}

	/**
	 * Sets the issuer request data model.
	 * 
	 * @param issuerRequestDataModel
	 *            the new issuer request data model
	 */
	public void setIssuerRequestDataModel(
			IssuerRequestDataModel issuerRequestDataModel) {
		this.issuerRequestDataModel = issuerRequestDataModel;
	}

	/**
	 * Gets the issuer request.
	 * 
	 * @return the issuer request
	 */
	public IssuerRequest getIssuerRequest() {
		return issuerRequest;
	}

	/**
	 * Sets the issuer request.
	 * 
	 * @param issuerRequest
	 *            the new issuer request
	 */
	public void setIssuerRequest(IssuerRequest issuerRequest) {
		this.issuerRequest = issuerRequest;
	}

	/**
	 * Gets the id issuer request pk.
	 * 
	 * @return the id issuer request pk
	 */
	public Long getIdIssuerRequestPk() {
		return idIssuerRequestPk;
	}

	/**
	 * Sets the id issuer request pk.
	 * 
	 * @param idIssuerRequestPk
	 *            the new id issuer request pk
	 */
	public void setIdIssuerRequestPk(Long idIssuerRequestPk) {
		this.idIssuerRequestPk = idIssuerRequestPk;
	}

	/**
	 * Gets the issuer history.
	 * 
	 * @return the issuer history
	 */
	public IssuerHistory getIssuerHistory() {
		return issuerHistory;
	}

	/**
	 * Sets the issuer history.
	 * 
	 * @param issuerHistory
	 *            the new issuer history
	 */
	public void setIssuerHistory(IssuerHistory issuerHistory) {
		this.issuerHistory = issuerHistory;
	}

	/**
	 * Gets the lst issuer file histories.
	 * 
	 * @return the lst issuer file histories
	 */
	public List<IssuerFileHistory> getLstIssuerFileHistories() {
		return lstIssuerFileHistories;
	}

	/**
	 * Sets the lst issuer file histories.
	 * 
	 * @param lstIssuerFileHistories
	 *            the new lst issuer file histories
	 */
	public void setLstIssuerFileHistories(
			List<IssuerFileHistory> lstIssuerFileHistories) {
		this.lstIssuerFileHistories = lstIssuerFileHistories;
	}

	/**
	 * Checks if is ind selected other motive reject.
	 * 
	 * @return true, if is ind selected other motive reject
	 */
	public boolean isIndSelectedOtherMotiveReject() {
		return indSelectedOtherMotiveReject;
	}

	/**
	 * Sets the ind selected other motive reject.
	 * 
	 * @param indSelectedOtherMotiveReject
	 *            the new ind selected other motive reject
	 */
	public void setIndSelectedOtherMotiveReject(
			boolean indSelectedOtherMotiveReject) {
		this.indSelectedOtherMotiveReject = indSelectedOtherMotiveReject;
	}

	/**
	 * Checks if is ind disable accept motive reject.
	 * 
	 * @return true, if is ind disable accept motive reject
	 */
	public boolean isIndDisableAcceptMotiveReject() {
		return indDisableAcceptMotiveReject;
	}

	/**
	 * Sets the ind disable accept motive reject.
	 * 
	 * @param indDisableAcceptMotiveReject
	 *            the new ind disable accept motive reject
	 */
	public void setIndDisableAcceptMotiveReject(
			boolean indDisableAcceptMotiveReject) {
		this.indDisableAcceptMotiveReject = indDisableAcceptMotiveReject;
	}

	/**
	 * Gets the lst issuer request reject motives.
	 * 
	 * @return the lst issuer request reject motives
	 */
	public List<ParameterTable> getLstIssuerRequestRejectMotives() {
		return lstIssuerRequestRejectMotives;
	}

	/**
	 * Sets the lst issuer request reject motives.
	 * 
	 * @param lstIssuerRequestRejectMotives
	 *            the new lst issuer request reject motives
	 */
	public void setLstIssuerRequestRejectMotives(
			List<ParameterTable> lstIssuerRequestRejectMotives) {
		this.lstIssuerRequestRejectMotives = lstIssuerRequestRejectMotives;
	}

	/**
	 * Gets the representative file name display.
	 * 
	 * @return the representative file name display
	 */
	public String getRepresentativeFileNameDisplay() {
		return representativeFileNameDisplay;
	}

	/**
	 * Sets the representative file name display.
	 * 
	 * @param representativeFileNameDisplay
	 *            the new representative file name display
	 */
	public void setRepresentativeFileNameDisplay(
			String representativeFileNameDisplay) {
		this.representativeFileNameDisplay = representativeFileNameDisplay;
	}

	/**
	 * Gets the lst representative file types.
	 * 
	 * @return the lst representative file types
	 */
	public List<ParameterTable> getLstRepresentativeFileTypes() {
		return lstRepresentativeFileTypes;
	}

	/**
	 * Sets the lst representative file types.
	 * 
	 * @param lstRepresentativeFileTypes
	 *            the new lst representative file types
	 */
	public void setLstRepresentativeFileTypes(
			List<ParameterTable> lstRepresentativeFileTypes) {
		this.lstRepresentativeFileTypes = lstRepresentativeFileTypes;
	}

	/**
	 * Gets the lst representative file history.
	 * 
	 * @return the lst representative file history
	 */
	public List<RepresentativeFileHistory> getLstRepresentativeFileHistory() {
		return lstRepresentativeFileHistory;
	}

	/**
	 * Sets the lst representative file history.
	 * 
	 * @param lstRepresentativeFileHistory
	 *            the new lst representative file history
	 */
	public void setLstRepresentativeFileHistory(
			List<RepresentativeFileHistory> lstRepresentativeFileHistory) {
		this.lstRepresentativeFileHistory = lstRepresentativeFileHistory;
	}

	/**
	 * Gets the representative file type.
	 * 
	 * @return the representative file type
	 */
	public Integer getRepresentativeFileType() {
		return representativeFileType;
	}

	/**
	 * Sets the representative file type.
	 * 
	 * @param representativeFileType
	 *            the new representative file type
	 */
	public void setRepresentativeFileType(Integer representativeFileType) {
		this.representativeFileType = representativeFileType;
	}

	/**
	 * Gets the max lenght document number legal.
	 * 
	 * @return the max lenght document number legal
	 */
	public int getMaxLenghtDocumentNumberLegal() {
		return maxLenghtDocumentNumberLegal;
	}

	/**
	 * Sets the max lenght document number legal.
	 * 
	 * @param maxLenghtDocumentNumberLegal
	 *            the new max lenght document number legal
	 */
	public void setMaxLenghtDocumentNumberLegal(int maxLenghtDocumentNumberLegal) {
		this.maxLenghtDocumentNumberLegal = maxLenghtDocumentNumberLegal;
	}

	/**
	 * Checks if is ind numeric type document legal.
	 * 
	 * @return true, if is ind numeric type document legal
	 */
	public boolean isIndNumericTypeDocumentLegal() {
		return indNumericTypeDocumentLegal;
	}

	/**
	 * Sets the ind numeric type document legal.
	 * 
	 * @param indNumericTypeDocumentLegal
	 *            the new ind numeric type document legal
	 */
	public void setIndNumericTypeDocumentLegal(
			boolean indNumericTypeDocumentLegal) {
		this.indNumericTypeDocumentLegal = indNumericTypeDocumentLegal;
	}

	/**
	 * Gets the lst document type representative result.
	 * 
	 * @return the lst document type representative result
	 */
	public List<ParameterTable> getLstDocumentTypeRepresentativeResult() {
		return lstDocumentTypeRepresentativeResult;
	}

	/**
	 * Sets the lst document type representative result.
	 * 
	 * @param lstDocumentTypeRepresentativeResult
	 *            the new lst document type representative result
	 */
	public void setLstDocumentTypeRepresentativeResult(
			List<ParameterTable> lstDocumentTypeRepresentativeResult) {
		this.lstDocumentTypeRepresentativeResult = lstDocumentTypeRepresentativeResult;
	}

	/**
	 * Checks if is enable select representative file.
	 * 
	 * @return true, if is enable select representative file
	 */
	public boolean isEnableSelectRepresentativeFile() {
		return Validations.validateIsNotNullAndPositive(representativeFileType);
	}

	/**
	 * Gets the lst issuer attachment type.
	 * 
	 * @return the lst issuer attachment type
	 */
	public List<ParameterTable> getLstIssuerAttachmentType() {
		return lstIssuerAttachmentType;
	}

	/**
	 * Sets the lst issuer attachment type.
	 * 
	 * @param lstIssuerAttachmentType
	 *            the new lst issuer attachment type
	 */
	public void setLstIssuerAttachmentType(
			List<ParameterTable> lstIssuerAttachmentType) {
		this.lstIssuerAttachmentType = lstIssuerAttachmentType;
	}

	/**
	 * Gets the issuer file name display.
	 * 
	 * @return the issuer file name display
	 */
	public String getIssuerFileNameDisplay() {
		return issuerFileNameDisplay;
	}

	/**
	 * Sets the issuer file name display.
	 * 
	 * @param issuerFileNameDisplay
	 *            the new issuer file name display
	 */
	public void setIssuerFileNameDisplay(String issuerFileNameDisplay) {
		this.issuerFileNameDisplay = issuerFileNameDisplay;
	}

	/**
	 * Gets the attach issuer file selected.
	 * 
	 * @return the attach issuer file selected
	 */
	public Integer getAttachIssuerFileSelected() {
		return attachIssuerFileSelected;
	}

	/**
	 * Sets the attach issuer file selected.
	 * 
	 * @param attachIssuerFileSelected
	 *            the new attach issuer file selected
	 */
	public void setAttachIssuerFileSelected(Integer attachIssuerFileSelected) {
		this.attachIssuerFileSelected = attachIssuerFileSelected;
	}

	/**
	 * Checks if is enable select issuer file.
	 * 
	 * @return true, if is enable select issuer file
	 */
	public boolean isEnableSelectIssuerFile() {
		return Validations
				.validateIsNotNullAndPositive(attachIssuerFileSelected);
	}

	/**
	 * Gets the lst legal representative history Origin.
	 * 
	 * @return the lst legal representative history Origin
	 */
	public List<LegalRepresentativeHistory> getLstLegalRepresentativeHistoryOrigin() {
		return lstLegalRepresentativeHistoryOrigin;
	}

	/**
	 * Sets the lst legal representative history Origin.
	 * 
	 * @param lstLegalRepresentativeHistoryOrigin
	 *            the new lst legal representative history origin
	 */
	public void setLstLegalRepresentativeHistoryOrigin(
			List<LegalRepresentativeHistory> lstLegalRepresentativeHistoryOrigin) {
		this.lstLegalRepresentativeHistoryOrigin = lstLegalRepresentativeHistoryOrigin;
	}

	/**
	 * Gets the lst legal representative history before.
	 * 
	 * @return the lst legal representative history before
	 */
	public List<LegalRepresentativeHistory> getLstLegalRepresentativeHistoryBefore() {
		return lstLegalRepresentativeHistoryBefore;
	}

	/**
	 * Sets the lst legal representative history before.
	 * 
	 * @param lstLegalRepresentativeHistoryBefore
	 *            the new lst legal representative history before
	 */
	public void setLstLegalRepresentativeHistoryBefore(
			List<LegalRepresentativeHistory> lstLegalRepresentativeHistoryBefore) {
		this.lstLegalRepresentativeHistoryBefore = lstLegalRepresentativeHistoryBefore;
	}

	/**
	 * Gets the lst legal representative history after.
	 * 
	 * @return the lst legal representative history after
	 */
	public List<LegalRepresentativeHistory> getLstLegalRepresentativeHistoryAfter() {
		return lstLegalRepresentativeHistoryAfter;
	}

	/**
	 * Sets the lst legal representative history after.
	 * 
	 * @param lstLegalRepresentativeHistoryAfter
	 *            the new lst legal representative history after
	 */
	public void setLstLegalRepresentativeHistoryAfter(
			List<LegalRepresentativeHistory> lstLegalRepresentativeHistoryAfter) {
		this.lstLegalRepresentativeHistoryAfter = lstLegalRepresentativeHistoryAfter;
	}

	/**
	 * Gets the email legal representative.
	 * 
	 * @return the email legal representative
	 */
	public String getEmailLegalRepresentative() {
		return emailLegalRepresentative;
	}

	/**
	 * Sets the email legal representative.
	 * 
	 * @param emailLegalRepresentative
	 *            the new email legal representative
	 */
	public void setEmailLegalRepresentative(String emailLegalRepresentative) {
		this.emailLegalRepresentative = emailLegalRepresentative;
	}

	/**
	 * Checks if is ind representative nationality national.
	 * 
	 * @return true, if is ind representative nationality national
	 */
	public boolean isIndRepresentativeNationalityNational() {
		return Validations.validateIsNotNull(legalRepresentativeHistory
				.getNationality())
				&& countryResidence.equals(legalRepresentativeHistory
						.getNationality());
	}

	/**
	 * Checks if is ind representative nationality foreign.
	 * 
	 * @return true, if is ind representative nationality foreign
	 */
	public boolean isIndRepresentativeNationalityForeign() {
		return Validations
				.validateIsNotNullAndPositive(legalRepresentativeHistory
						.getNationality())
				&& !(countryResidence.equals(legalRepresentativeHistory
						.getNationality()));
	}

	/**
	 * Gets the min depositary contract date.
	 * 
	 * @return the min depositary contract date
	 */
	public Date getMinDepositaryContractDate() {
		return minDepositaryContractDate;
	}

	/**
	 * Sets the min depositary contract date.
	 * 
	 * @param minDepositaryContractDate
	 *            the new min depositary contract date
	 */
	public void setMinDepositaryContractDate(Date minDepositaryContractDate) {
		this.minDepositaryContractDate = minDepositaryContractDate;
	}

	/**
	 * Gets the min inscription stock exch date.
	 * 
	 * @return the min inscription stock exch date
	 */
	public Date getMinInscriptionStockExchDate() {
		return minInscriptionStockExchDate;
	}

	/**
	 * Sets the min inscription stock exch date.
	 * 
	 * @param minInscriptionStockExchDate
	 *            the new min inscription stock exch date
	 */
	public void setMinInscriptionStockExchDate(Date minInscriptionStockExchDate) {
		this.minInscriptionStockExchDate = minInscriptionStockExchDate;
	}

	/**
	 * Checks if is ind representative modification.
	 * 
	 * @return true, if is ind representative modification
	 */
	public boolean isIndRepresentativeModification() {
		return indRepresentativeModification;
	}

	/**
	 * Sets the ind representative modification.
	 * 
	 * @param indRepresentativeModification
	 *            the new ind representative modification
	 */
	public void setIndRepresentativeModification(
			boolean indRepresentativeModification) {
		this.indRepresentativeModification = indRepresentativeModification;
	}

	/**
	 * Checks if is ind numeric second type document legal.
	 * 
	 * @return true, if is ind numeric second type document legal
	 */
	public boolean isIndNumericSecondTypeDocumentLegal() {
		return indNumericSecondTypeDocumentLegal;
	}

	/**
	 * Sets the ind numeric second type document legal.
	 * 
	 * @param indNumericSecondTypeDocumentLegal
	 *            the new ind numeric second type document legal
	 */
	public void setIndNumericSecondTypeDocumentLegal(
			boolean indNumericSecondTypeDocumentLegal) {
		this.indNumericSecondTypeDocumentLegal = indNumericSecondTypeDocumentLegal;
	}

	/**
	 * Gets the lst second document type representative.
	 * 
	 * @return the lst second document type representative
	 */
	public List<ParameterTable> getLstSecondDocumentTypeRepresentative() {
		return lstSecondDocumentTypeRepresentative;
	}

	/**
	 * Sets the lst second document type representative.
	 * 
	 * @param lstSecondDocumentTypeRepresentative
	 *            the new lst second document type representative
	 */
	public void setLstSecondDocumentTypeRepresentative(
			List<ParameterTable> lstSecondDocumentTypeRepresentative) {
		this.lstSecondDocumentTypeRepresentative = lstSecondDocumentTypeRepresentative;
	}

	/**
	 * Gets the lst second countries representative.
	 * 
	 * @return the lst second countries representative
	 */
	public List<GeographicLocation> getLstSecondCountriesRepresentative() {
		return lstSecondCountriesRepresentative;
	}

	/**
	 * Sets the lst second countries representative.
	 * 
	 * @param lstSecondCountriesRepresentative
	 *            the new lst second countries representative
	 */
	public void setLstSecondCountriesRepresentative(
			List<GeographicLocation> lstSecondCountriesRepresentative) {
		this.lstSecondCountriesRepresentative = lstSecondCountriesRepresentative;
	}

	/**
	 * Gets the max lenght second document number legal.
	 * 
	 * @return the max lenght second document number legal
	 */
	public int getMaxLenghtSecondDocumentNumberLegal() {
		return maxLenghtSecondDocumentNumberLegal;
	}

	/**
	 * Sets the max lenght second document number legal.
	 * 
	 * @param maxLenghtSecondDocumentNumberLegal
	 *            the new max lenght second document number legal
	 */
	public void setMaxLenghtSecondDocumentNumberLegal(
			int maxLenghtSecondDocumentNumberLegal) {
		this.maxLenghtSecondDocumentNumberLegal = maxLenghtSecondDocumentNumberLegal;
	}

	/**
	 * Checks if is ind issuer differentiated.
	 * 
	 * @return true, if is ind issuer differentiated
	 */
	public boolean isIndIssuerDifferentiated() {
		return indIssuerDifferentiated;
	}

	/**
	 * Sets the ind issuer differentiated.
	 * 
	 * @param indIssuerDifferentiated
	 *            the new ind issuer differentiated
	 */
	public void setIndIssuerDifferentiated(boolean indIssuerDifferentiated) {
		this.indIssuerDifferentiated = indIssuerDifferentiated;
	}

	/**
	 * Checks if is allow search all.
	 * 
	 * @return true, if is allow search all
	 */
	public boolean isAllowSearchAll() {
		return allowSearchAll;
	}

	/**
	 * Sets the allow search all.
	 * 
	 * @param allowSearchAll
	 *            the new allow search all
	 */
	public void setAllowSearchAll(boolean allowSearchAll) {
		this.allowSearchAll = allowSearchAll;
	}

	/**
	 * #########################################################################
	 * REFACTOR
	 * ####################################################################.
	 * 
	 * @return the user info
	 */
	/**
	 * get User Information
	 * 
	 * @return object User Information
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * set User Information.
	 * 
	 * @param userInfo
	 *            object User Information
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * Clean Data Model.
	 */
	public void cleanDataModel() {
		issuerRequestDataModel = null;
	}

	/**
	 * Gets the document Validator Register.
	 * 
	 * @return the document Validator Register
	 */
	public DocumentValidator getDocumentValidatorRegister() {
		return documentValidatorRegister;
	}

	/**
	 * Sets the document Validator Register.
	 * 
	 * @param documentValidatorRegister
	 *            the new document validator register
	 */
	public void setDocumentValidatorRegister(
			DocumentValidator documentValidatorRegister) {
		this.documentValidatorRegister = documentValidatorRegister;
	}

	/**
	 * Gets the document Register.
	 * 
	 * @return the document Register
	 */
	public DocumentTO getDocumentTORegister() {
		return documentTORegister;
	}

	/**
	 * Sets the document Register.
	 * 
	 * @param documentTORegister
	 *            the new document to register
	 */
	public void setDocumentTORegister(DocumentTO documentTORegister) {
		this.documentTORegister = documentTORegister;
	}

	/**
	 * Change selected document type search.
	 */
	public void changeSelectedDocumentTypeSearch() {
		issuerTO.setDocumentNumber(null);
		if (Validations
				.validateIsNotNullAndPositive(issuerTO.getDocumentType())) {
			documentTORegister = documentValidatorRegister
					.loadMaxLenghtType(issuerTO.getDocumentType());
			DocumentTO documentTO = documentValidatorRegister
					.validateEmissionRules(issuerTO.getDocumentType());
			documentTORegister.setIndicatorAlphanumericWithDash(documentTO
					.isIndicatorAlphanumericWithDash());
			documentTORegister.setIndicatorDocumentSource(documentTO
					.isIndicatorDocumentSource());

		}
		cleanDataModel();
	}

	/**
	 * Gets the streamed content file.
	 * 
	 * @return the streamed content file
	 */
	public StreamedContent getStreamedContentFile() {
		return streamedContentFile;
	}

	/**
	 * Sets the streamed content file.
	 * 
	 * @param streamedContentFile
	 *            the new streamed content file
	 */
	public void setStreamedContentFile(StreamedContent streamedContentFile) {
		this.streamedContentFile = streamedContentFile;
	}

	/**
	 * Gets the legal representative helper bean.
	 * 
	 * @return the legal representative helper bean
	 */
	public LegalRepresentativeHelperBean getLegalRepresentativeHelperBean() {
		return legalRepresentativeHelperBean;
	}

	/**
	 * Sets the legal representative helper bean.
	 * 
	 * @param legalRepresentativeHelperBean
	 *            the new legal representative helper bean
	 */
	public void setLegalRepresentativeHelperBean(
			LegalRepresentativeHelperBean legalRepresentativeHelperBean) {
		this.legalRepresentativeHelperBean = legalRepresentativeHelperBean;
	}

	/**
	 * Checks if is ind exists issuer history.
	 * 
	 * @return true, if is ind exists issuer history
	 */
	public boolean isIndExistsIssuerHistory() {
		return indExistsIssuerHistory;
	}

	/**
	 * Sets the ind exists issuer history.
	 * 
	 * @param indExistsIssuerHistory
	 *            the new ind exists issuer history
	 */
	public void setIndExistsIssuerHistory(boolean indExistsIssuerHistory) {
		this.indExistsIssuerHistory = indExistsIssuerHistory;
	}

	/**
	 * Gets the list document source.
	 * 
	 * @return the list document source
	 */
	public List<ParameterTable> getListDocumentSource() {
		return listDocumentSource;
	}

	/**
	 * Sets the list document source.
	 * 
	 * @param listDocumentSource
	 *            the new list document source
	 */
	public void setListDocumentSource(List<ParameterTable> listDocumentSource) {
		this.listDocumentSource = listDocumentSource;
	}

	/**
	 * Gets the lst cbo credit rating scales.
	 * 
	 * @return the lst cbo credit rating scales
	 */
	public List<ParameterTable> getLstCboCreditRatingScales() {
		return lstCboCreditRatingScales;
	}

	/**
	 * Sets the lst cbo credit rating scales.
	 * 
	 * @param lstCboCreditRatingScales
	 *            the new lst cbo credit rating scales
	 */
	public void setLstCboCreditRatingScales(
			List<ParameterTable> lstCboCreditRatingScales) {
		this.lstCboCreditRatingScales = lstCboCreditRatingScales;
	}

	/**
	 * Checks if is ind ficpat economic activity.
	 * 
	 * @return true, if is ind ficpat economic activity
	 */
	public boolean isIndFICPATEconomicActivity() {
		return indFICPATEconomicActivity;
	}

	/**
	 * Sets the ind ficpat economic activity.
	 * 
	 * @param indFICPATEconomicActivity
	 *            the new ind ficpat economic activity
	 */
	public void setIndFICPATEconomicActivity(boolean indFICPATEconomicActivity) {
		this.indFICPATEconomicActivity = indFICPATEconomicActivity;
	}

	/** SIRTEX negotiate */
	public boolean isIndIssuerSIRTEXnegociate() {
		return indIssuerSIRTEXnegociate;
	}

	public void setIndIssuerSIRTEXnegociate(boolean indIssuerSIRTEXnegociate) {
		this.indIssuerSIRTEXnegociate = indIssuerSIRTEXnegociate;
	}

	public List<ParameterTable> getLstOfferType() {
		return lstOfferType;
	}

	public void setLstOfferType(List<ParameterTable> lstOfferType) {
		this.lstOfferType = lstOfferType;
	}

	public String getfUploadFileDocumentsSize() {
		return fUploadFileDocumentsSize;
	}

	public void setfUploadFileDocumentsSize(String fUploadFileDocumentsSize) {
		this.fUploadFileDocumentsSize = fUploadFileDocumentsSize;
	}

	public String getfUploadFileSizeDocumentsDisplay() {
		return fUploadFileSizeDocumentsDisplay;
	}

	public void setfUploadFileSizeDocumentsDisplay(
			String fUploadFileSizeDocumentsDisplay) {
		this.fUploadFileSizeDocumentsDisplay = fUploadFileSizeDocumentsDisplay;
	}

	public List<ParameterTable> getListSecurityClass() {
		return listSecurityClass;
	}

	public void setListSecurityClass(List<ParameterTable> listSecurityClass) {
		this.listSecurityClass = listSecurityClass;
	}

	public List<ParameterTable> getListCurrency() {
		return listCurrency;
	}

	public void setListCurrency(List<ParameterTable> listCurrency) {
		this.listCurrency = listCurrency;
	}

	public List<ParameterTable> getListBooleanValues() {
		return listBooleanValues;
	}

	public void setListBooleanValues(List<ParameterTable> listBooleanValues) {
		this.listBooleanValues = listBooleanValues;
	}

	public List<ParameterTable> getListPersonType() {
		return listPersonType;
	}

	public void setListPersonType(List<ParameterTable> listPersonType) {
		this.listPersonType = listPersonType;
	}

	public List<InvestorTypeModelTO> getListInvestorType() {
		return listInvestorType;
	}

	public void setListInvestorType(List<InvestorTypeModelTO> listInvestorType) {
		this.listInvestorType = listInvestorType;
	}

	public String getDescriptionSocietyType() {
		return descriptionSocietyType;
	}

	public void setDescriptionSocietyType(String descriptionSocietyType) {
		this.descriptionSocietyType = descriptionSocietyType;
	}

	public String getDescriptionSocietyTypeIssuer() {
		return descriptionSocietyTypeIssuer;
	}

	public void setDescriptionSocietyTypeIssuer(String descriptionSocietyTypeIssuer) {
		this.descriptionSocietyTypeIssuer = descriptionSocietyTypeIssuer;
	}
	
}
