package com.pradera.securities.issuer.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.to.RepresentedEntityTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.InstitutionInformation;
import com.pradera.model.accounts.LegalRepresentative;
import com.pradera.model.accounts.LegalRepresentativeFile;
import com.pradera.model.accounts.LegalRepresentativeHistory;
import com.pradera.model.accounts.OfacPerson;
import com.pradera.model.accounts.PepPerson;
import com.pradera.model.accounts.PnaPerson;
import com.pradera.model.accounts.RepresentativeFileHistory;
import com.pradera.model.accounts.RepresentedEntity;
import com.pradera.model.accounts.type.HolderFileStateType;
import com.pradera.model.accounts.type.LegalRepresentativeStateType;
import com.pradera.model.accounts.type.RepresentativeEntityStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.IssuanceCertificate;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.IssuerFile;
import com.pradera.model.issuancesecuritie.IssuerFileHistory;
import com.pradera.model.issuancesecuritie.IssuerHistory;
import com.pradera.model.issuancesecuritie.IssuerHistoryState;
import com.pradera.model.issuancesecuritie.IssuerRequest;
import com.pradera.model.issuancesecuritie.type.IssuerFileStateType;
import com.pradera.securities.issuancesecuritie.to.IssuanceTO;
import com.pradera.securities.issuer.to.IssuerHistoryStateTO;
import com.pradera.securities.issuer.to.IssuerRequestTO;
import com.pradera.securities.issuer.to.IssuerTO;
import com.pradera.securities.issuer.view.InvestorTypeModelTO;

// TODO: Auto-generated Javadoc
/**
 * The Class IssuerServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class IssuerServiceBean extends CrudDaoServiceBean{
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	public void updateInvestorTypeHolderFromIssuer(Holder h) {
		StringBuilder sd = new StringBuilder();
		sd.append(" update Holder h set ");
		sd.append(" h.investorType = :investorType");
		sd.append(" where h.idHolderPk = :idHolderPk");
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("investorType", h.getInvestorType());
		parameters.put("idHolderPk", h.getIdHolderPk());
		updateByQuery(sd.toString(), parameters);
	}
	
	public List<InvestorTypeModelTO> getLstInvestorTypeModelTO(InvestorTypeModelTO filter) throws ServiceException{
		List<InvestorTypeModelTO> lst = new ArrayList<>();
		StringBuilder sd = new StringBuilder();
		sd.append(" select ");
		sd.append(" new com.pradera.securities.issuer.view.InvestorTypeModelTO(");
		sd.append(" p.parameterTablePk,");
		sd.append(" p.parameterName,");
		sd.append(" p.description,");
		sd.append(" pFK.parameterTablePk,");
		sd.append(" p.parameterState");
		sd.append(" ) ");
		sd.append(" from ParameterTable p");
		sd.append(" inner join p.masterTable mt");
		sd.append(" inner join p.relatedParameter pFK");
		sd.append(" where mt.masterTablePk = :idMasterTablePk");
		sd.append(" AND p.parameterState = :parameterState");
		if (Validations.validateIsNotNullAndPositive(filter.getEconomicActivity())){
			sd.append(" and pFK.parameterTablePk = :economicActivity");
		}
		sd.append(" order by p.parameterTablePk desc");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idMasterTablePk", MasterTableType.INVESTOR_TYPE_ECONOMIC_ACTIVITY.getCode());
		if (Validations.validateIsNotNullAndPositive(filter.getEconomicActivity())){
			parameters.put("economicActivity", filter.getEconomicActivity());
		}
		parameters.put("parameterState", GeneralConstants.ONE_VALUE_INTEGER);
		lst = findListByQueryString(sd.toString(), parameters);
		return lst;
	}
	
	public List<InvestorTypeModelTO> getDefaultLstInvestorTypeModelTO() throws ServiceException{
		List<InvestorTypeModelTO> lst = new ArrayList<>();
		StringBuilder sd = new StringBuilder();
		sd.append(" select ");
		sd.append(" new com.pradera.securities.issuer.view.InvestorTypeModelTO(");
		sd.append(" p.parameterTablePk,");
		sd.append(" p.parameterName,");
		sd.append(" p.description,");
		sd.append(" p.parameterState");
		sd.append(" ) ");
		sd.append(" from ParameterTable p");
		sd.append(" inner join p.masterTable mt");
		sd.append(" where mt.masterTablePk = :idMasterTablePk");
		sd.append(" AND p.parameterState = :parameterState");
		sd.append(" order by p.parameterTablePk desc");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idMasterTablePk", MasterTableType.INVESTOR_TYPE_ECONOMIC_ACTIVITY.getCode());
		parameters.put("parameterState", GeneralConstants.ONE_VALUE_INTEGER);
		
		lst = findListByQueryString(sd.toString(), parameters);
		return lst;
	}
	
	/**
	 * Metodo para Buscar un emisor.
	 *
	 * @param issuerTO the issuer to
	 * @return the issuer
	 */
	public Issuer findIssuerServiceBean(IssuerTO issuerTO) {

		Map<String, Object> parameters = new HashMap<String, Object>();
		
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT iss from Issuer iss ");
		stringBuffer.append(" Left outer join fetch iss.holder ");
		stringBuffer.append(" Left outer join fetch iss.geographicLocation ");
		stringBuffer.append(" where 1 = 1 ");
		
		if(Validations.validateIsNotNullAndNotEmpty(issuerTO.getIdIssuerPk())){
			stringBuffer.append(" and iss.idIssuerPk = :idIssuer ");
			parameters.put("idIssuer", issuerTO.getIdIssuerPk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(issuerTO.getIssuerState())){
			stringBuffer.append(" and iss.stateIssuer = :stateIssuer ");
			parameters.put("stateIssuer", issuerTO.getIssuerState());
		}
		if(Validations.validateIsNotNullAndNotEmpty(issuerTO.getMnemonic())){
			stringBuffer.append(" and iss.mnemonic = :mnemonic");
			parameters.put("mnemonic", issuerTO.getMnemonic());
		}
		
		try {
			return (Issuer) findObjectByQueryString(stringBuffer.toString(), parameters);
		} catch(NoResultException nex) {
			log.info("Issuer not found");
		}
		return null;
	}

	/**
	 * Find institution information by id service bean.
	 *
	 * @param institutionFilter the institution filter
	 * @return the institution information
	 * @throws ServiceException the service exception
	 */
	public InstitutionInformation findInstitutionInformationByIdServiceBean(InstitutionInformation institutionFilter) throws ServiceException {
		try{
			Query query = em.createNamedQuery(InstitutionInformation.INSTITUTION_INFORMATION_BY_ID);
			
			query.setParameter("docNum", institutionFilter.getDocumentNumber());
			query.setParameter("docTyp", institutionFilter.getDocumentType());
			
			return (InstitutionInformation) query.getSingleResult();
		} catch(NoResultException ex){
		   return null;
		} catch(NonUniqueResultException ex){
		   return null;
		}
	}
	
	/**
	 * Gets the ofac person list.
	 *
	 * @param filter the filter
	 * @return the ofac person list
	 * @throws ServiceException the service exception
	 */
	public List<OfacPerson> getOfacPersonList(String filter) throws ServiceException{
		List<OfacPerson> list;
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select p from OfacPerson p");
		sbQuery.append(" Where 1 = 1");
		
 		if(Validations.validateIsNotNullAndNotEmpty(filter)){
			sbQuery.append(" and p.fullName like :fName");
		}
 		
 		Query query = em.createQuery(sbQuery.toString());
		if(Validations.validateIsNotNullAndNotEmpty(filter)){
			query.setParameter("fName",filter);
		}
		
		list = (List<OfacPerson>)query.getResultList();

		return list;

	}
	
	/**
	 * Gets the pep person list.
	 *
	 * @param filter the filter
	 * @return the pep person list
	 * @throws ServiceException the service exception
	 */
	public List<PepPerson> getPepPersonList(String filter) throws ServiceException
	{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select p from PepPerson p");
		sbQuery.append(" where 1 = 1");		
		if(Validations.validateIsNotNull(filter)){
			if(!Validations.validateIsEmpty(filter)){
				sbQuery.append(" And (p.firstName || ' ' || p.lastName) like :fName");
			}
		}		
		//********* CREATE QUERY *************************
		Query query = em.createQuery(sbQuery.toString());			
		if(Validations.validateIsNotNull(filter)){
			if(!Validations.validateIsEmpty(filter)){
				query.setParameter("fName",filter.toUpperCase());
			}
		}		
		List<PepPerson> list = (List<PepPerson>)query.getResultList();
		return list;
	}
	/**
	 * Gets the pna person list.
	 *
	 * @param filter the filter
	 * @return the pna person list
	 * @throws ServiceException the service exception
	 */
	public List<PnaPerson> getPnaPersonList(String filter) throws ServiceException
	{
		List<PnaPerson> list;
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select p from PnaPerson p");
		sbQuery.append(" where 1 = 1");
		if(Validations.validateIsNotNull(filter)){
			if(!Validations.validateIsEmpty(filter)){
				sbQuery.append(" And (p.firstName || ' ' || p.lastName) like :fName");
			}
		}
		Query query = em.createQuery(sbQuery.toString());
		if(Validations.validateIsNotNull(filter)){
			if(!Validations.validateIsEmpty(filter)){
				query.setParameter("fName",filter.toUpperCase());
			}
		}
		list = (List<PnaPerson>)query.getResultList();
		return list;
	}

	/**
	 * Find issuances service bean.
	 *
	 * @param issuanceTO the issuance to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Issuance> findIssuancesServiceBean(IssuanceTO issuanceTO) throws ServiceException{
		CriteriaBuilder cb=em.getCriteriaBuilder();
		CriteriaQuery<Issuance> cq=cb.createQuery(Issuance.class);
		Root<Issuance> issuanceRoot=cq.from(Issuance.class);
		issuanceRoot.fetch("issuer");
		issuanceRoot.fetch("geographicLocation");

		List<Predicate> criteriaParameters=new ArrayList<Predicate>();
		cq.select(issuanceRoot);
		cq.distinct(true);
		
		if(Validations.validateIsNotNullAndNotEmpty( issuanceTO.getIdIssuerPk() )){
			criteriaParameters.add(cb.equal(issuanceRoot.<String>get("issuer").get("idIssuerPk"), issuanceTO.getIdIssuerPk()));
		}
		if(Validations.validateIsNotNullAndNotEmpty( issuanceTO.getInstrumentType() ) && !issuanceTO.getInstrumentType().equals(0)){
			criteriaParameters.add(cb.equal(issuanceRoot.<Integer>get("instrumentType"), issuanceTO.getInstrumentType()));
		}
		if(Validations.validateIsNotNullAndNotEmpty( issuanceTO.getSecurityType() ) && !issuanceTO.getSecurityType().equals(0)){
			criteriaParameters.add(cb.equal(issuanceRoot.<Integer>get("securityType"), issuanceTO.getSecurityType()));
		}
		if(Validations.validateIsNotNullAndNotEmpty( issuanceTO.getSecurityClass() ) && !issuanceTO.getSecurityClass().equals(0)){
			criteriaParameters.add(cb.equal(issuanceRoot.<Integer>get("securityClass"), issuanceTO.getSecurityClass()));
		}
		if(Validations.validateIsNotNullAndNotEmpty( issuanceTO.getIdIssuanceCodePk()  )){
			criteriaParameters.add(cb.like(issuanceRoot.<String>get("idIssuanceCodePk"), issuanceTO.getIdIssuanceCodePk()));
		}
		if(Validations.validateIsNotNullAndNotEmpty( issuanceTO.getDescription()  )){
			criteriaParameters.add(cb.like(issuanceRoot.<String>get("description"), issuanceTO.getDescription()));
		}
		if(Validations.validateIsNotNullAndNotEmpty( issuanceTO.getIssuanceState() ) && !issuanceTO.getIssuanceState().equals(0)){
			criteriaParameters.add(cb.equal(issuanceRoot.<Integer>get("stateIssuance"), issuanceTO.getIssuanceState()));
		}
		cq.where( cb.and( criteriaParameters.toArray(new Predicate[criteriaParameters.size()]) ) );
		
		TypedQuery<Issuance> issuanceCriteria=em.createQuery(cq);
		return (List<Issuance>) issuanceCriteria.getResultList();
	}
	
	/**
	 * Find issuance service bean.
	 *
	 * @param issuanceTO the issuance to
	 * @return the issuance
	 * @throws ServiceException the service exception
	 */
	public Issuance findIssuanceServiceBean(IssuanceTO issuanceTO) throws ServiceException{
		CriteriaBuilder cb=em.getCriteriaBuilder();
		CriteriaQuery<Issuance> cq=cb.createQuery(Issuance.class);
		Root<Issuance> issuanceRoot=cq.from(Issuance.class);
		issuanceRoot.fetch("issuer");
		issuanceRoot.fetch("geographicLocation");
		
		List<Predicate> criteriaParameters=new ArrayList<Predicate>();
		cq.select(issuanceRoot);
		
		//Some cases is necesary get Issuance with all securities
		if(issuanceTO.isNeedSecurities()){
			issuanceRoot.fetch("securities");
		}
		//END - Some cases is necesary get Issuance with all securities
		
		if(Validations.validateIsNotNullAndNotEmpty( issuanceTO.getIdIssuanceCodePk())){
			criteriaParameters.add(cb.equal(issuanceRoot.<String>get("idIssuanceCodePk"), issuanceTO.getIdIssuanceCodePk()));
		}

		cq.where( cb.and( criteriaParameters.toArray(new Predicate[criteriaParameters.size()]) ) );

		Issuance issuance=null;
		TypedQuery<Issuance> issuanceCriteria=em.createQuery(cq);
		try {
			issuance = issuanceCriteria.getSingleResult();
		} catch(NonUniqueResultException nuex){
			log.info("Issuance have more codes same!");
		} 
		catch(NoResultException nex) {
			log.info("Issuance not found");
		}
		
		return issuance;
	}
	
	/**
	 * Find issuance certificates service bean.
	 *
	 * @param idIssuanceCodePkPrm the id issuance code pk prm
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<IssuanceCertificate> findIssuanceCertificatesServiceBean(String idIssuanceCodePkPrm) throws ServiceException{
		String strQuery="Select I From IssuanceCertificate I where I.issuance.idIssuanceCodePk=:idIssuanceCodePkPrm";
		Query query=em.createQuery(strQuery);
		query.setParameter("idIssuanceCodePkPrm", idIssuanceCodePkPrm);
		return (List<IssuanceCertificate>)query.getResultList();
	}
	
	
							
	/**
	 * Gets the issuance sequence code service bean.
	 *
	 * @return the issuance sequence code service bean
	 * @throws ServiceException the service exception
	 */
	public String getIssuanceSequenceCodeServiceBean() throws ServiceException {
		Query query=em.createNativeQuery("select SQ_ID_FOREIGN_DEPOSITORY_PK.nextval from dual");	

		BigDecimal issuanceCode=(BigDecimal)query.getSingleResult();
		StringBuilder sbIssuanceCode=new StringBuilder();
		if(issuanceCode!=null){
			String strIssuanceCode =issuanceCode.toString();
			if(strIssuanceCode.length()==1){
				sbIssuanceCode.append("0").append( strIssuanceCode );
			}else{
				sbIssuanceCode.append( strIssuanceCode );
			}
			strIssuanceCode=sbIssuanceCode.toString();
		}
		return sbIssuanceCode.toString();
	}
	
	/**
	 * Validate issuance description service bean.
	 *
	 * @param issuance the issuance
	 * @return the int
	 */
	public int validateIssuanceDescriptionServiceBean(Issuance issuance){
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append("Select count(I) from Issuance i where i.description=:descriptionPrm");
		if(issuance.getIdIssuanceCodePk()!=null){
			sbQuery.append(" and i.idIssuanceCodePk != :idIssuanceCodePkPrm");
		}
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("descriptionPrm", issuance.getDescription());
		if(issuance.getIdIssuanceCodePk()!=null){
			query.setParameter("idIssuanceCodePkPrm", issuance.getIdIssuanceCodePk());
		}
		return Integer.parseInt(  query.getSingleResult().toString()  );
	}

	/**
	 * Find represented entity by id service bean.
	 *
	 * @param filter the filter
	 * @return the represented entity
	 * @throws ServiceException the service exception
	 */
	public RepresentedEntity findRepresentedEntityByIdServiceBean(
			RepresentedEntityTO filter) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder("Select distinct RP From RepresentedEntity RP ");
			sbQuery.append(" Join Fetch RP.legalRepresentative LegRep ");
			sbQuery.append(" Where LegRep.state = :stateLegalReprePrm ");
			sbQuery.append(" And RP.stateRepresented = :stateRepreEntityPrm ");
			sbQuery.append(" And RP.idRepresentedEntityPk = :idRepresentedEntityPrm ");
			
			Query query = em.createQuery(sbQuery.toString());		
			query.setParameter("stateLegalReprePrm",filter.getStateLegalRepresentative());
			query.setParameter("stateRepreEntityPrm", filter.getStateRepresentedEntity());
			query.setParameter("idRepresentedEntityPrm", filter.getIdRepresentedEntityPk());
			
			return (RepresentedEntity) query.getSingleResult();
		
			} catch(NoResultException ex){
				   return null;
			} catch(NonUniqueResultException ex){
				   return null;
			}
	}
	
	/**
	 * Find issuer by filters service bean.
	 *
	 * @param filter the filter
	 * @return the issuer
	 * @throws ServiceException the service exception
	 */
	public Issuer findIssuerByFiltersServiceBean(Issuer filter) throws ServiceException{
		try {
			CriteriaBuilder criteriaBuilder=em.getCriteriaBuilder();
			CriteriaQuery<Issuer> criteriaQuery=criteriaBuilder.createQuery(Issuer.class);
			Root<Issuer> issuerRoot=criteriaQuery.from(Issuer.class);
			List<Predicate> criteriaParameters=new ArrayList<Predicate>();
			
			criteriaQuery.select(issuerRoot);
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getIdIssuerPk())){
				ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"idPrm");
				criteriaParameters.add(criteriaBuilder.equal(issuerRoot.get("idIssuerPk"), param));			
			}
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getStateIssuer()) &&
					Validations.validateIsPositiveNumber(filter.getStateIssuer())){
				ParameterExpression<Integer> param = criteriaBuilder.parameter(Integer.class,"statePrm");
				criteriaParameters.add(criteriaBuilder.equal(issuerRoot.get("stateIssuer"), param));			
			}
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getDocumentType()) &&
					Validations.validateIsPositiveNumber(filter.getDocumentType())){
				ParameterExpression<Integer> param = criteriaBuilder.parameter(Integer.class,"documentTypePrm");
				criteriaParameters.add(criteriaBuilder.equal(issuerRoot.get("documentType"), param));	
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentSource()) && 
					Validations.validateIsPositiveNumber(filter.getDocumentSource())){
				ParameterExpression<Integer> param = criteriaBuilder.parameter(Integer.class,"documentSourcePrm");
				criteriaParameters.add(criteriaBuilder.equal(issuerRoot.get("documentSource"), param));			
			}
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getDocumentNumber() )){
				ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"documentNumberPrm");
				criteriaParameters.add(criteriaBuilder.equal(issuerRoot.get("documentNumber"), param));	
			}
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getBusinessName() )){
				ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"businessNamePrm");
				criteriaParameters.add(criteriaBuilder.equal(issuerRoot.get("businessName"), param));	
			}
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getMnemonic() )){
				ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"mnemonicPrm");
				criteriaParameters.add(criteriaBuilder.equal(issuerRoot.get("mnemonic"), param));	
			}
			
			if(!criteriaParameters.isEmpty()){
				if (criteriaParameters.size() == 1) {
					criteriaQuery.where(criteriaParameters.get(0));
			    } else {
			    	criteriaQuery.where(criteriaBuilder.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()])));
			    }
			}
			
			TypedQuery<Issuer> issuerRequestCriteria = em.createQuery(criteriaQuery);
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getIdIssuerPk())){
				issuerRequestCriteria.setParameter("idPrm", filter.getIdIssuerPk());
			}
			
			if(Validations.validateIsNotNullAndNotEmpty( filter.getStateIssuer()) &&
					Validations.validateIsPositiveNumber(filter.getStateIssuer())){
				issuerRequestCriteria.setParameter("statePrm", filter.getStateIssuer());
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentType()) &&
					Validations.validateIsPositiveNumber(filter.getDocumentType())){
				issuerRequestCriteria.setParameter("documentTypePrm", filter.getDocumentType());
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentSource()) &&
					Validations.validateIsPositiveNumber(filter.getDocumentSource())){
				issuerRequestCriteria.setParameter("documentSourcePrm", filter.getDocumentSource());
			}			
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentNumber())){
				issuerRequestCriteria.setParameter("documentNumberPrm", filter.getDocumentNumber());
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getBusinessName())){
				issuerRequestCriteria.setParameter("businessNamePrm", filter.getBusinessName());
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(filter.getMnemonic())){
				issuerRequestCriteria.setParameter("mnemonicPrm", filter.getMnemonic());
			}
			
			List<Issuer> list = (List<Issuer>) issuerRequestCriteria.getResultList();
			
			Issuer issuerResult = null;
			if(list!=null && list.size()>0){
				
				if(filter.getListSectorActivity()==null){
					issuerResult = list.get(0);
				}
				else{
					exit:
					for(Issuer iss : list){
					  for(Integer ea : filter.getListSectorActivity()){
						   if(iss.getEconomicActivity().equals(ea)){
							   issuerResult = iss;
							   break exit;
						   }
					  }
					
					}
				}
			}
			
			if(Validations.validateIsNotNull(issuerResult)){
				if(Validations.validateIsNotNull(issuerResult.getGeographicLocation())){
					issuerResult.getGeographicLocation().getName();
				}
				if(Validations.validateIsNotNull(issuerResult.getHolder())){
					issuerResult.getHolder().getFullName();
				}
			}
			
			return issuerResult;
		
		} catch(NoResultException ex){
			return null;
		} catch(NonUniqueResultException ex){
		   return null;
		}
	}
	
	/**
	 * Gets the list issuer by filters service bean.
	 *
	 * @param filter the filter
	 * @return the list issuer by filters service bean
	 * @throws ServiceException the service exception
	 */
	public List<Issuer> getListIssuerByFiltersServiceBean(IssuerTO filter) throws ServiceException{
		CriteriaBuilder criteriaBuilder=em.getCriteriaBuilder();
		CriteriaQuery<Issuer> criteriaQuery=criteriaBuilder.createQuery(Issuer.class);
		Root<Issuer> issuerRoot=criteriaQuery.from(Issuer.class);
		List<Predicate> criteriaParameters=new ArrayList<Predicate>();
		
		criteriaQuery.select(issuerRoot);
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdIssuerPk())){
			ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"idIssuerPkPrm");
			criteriaParameters.add(criteriaBuilder.equal(issuerRoot.get("idIssuerPk"), param));
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIssuerState()) && Validations.validateIsPositiveNumber(filter.getIssuerState())){
			ParameterExpression<Integer> param = criteriaBuilder.parameter(Integer.class,"statePrm");
			criteriaParameters.add(criteriaBuilder.equal(issuerRoot.get("stateIssuer"), param));
		}
		
		if(Validations.validateIsNotNull(filter.getLstIssuerStates()) && Validations.validateIsPositiveNumber(filter.getLstIssuerStates().size())){
			ParameterExpression<List> param = criteriaBuilder.parameter(List.class,"lstStatePrm");
			criteriaParameters.add(issuerRoot.get("stateIssuer").in(param));
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentType()) && Validations.validateIsPositiveNumber(filter.getDocumentType())){
			ParameterExpression<Integer> param = criteriaBuilder.parameter(Integer.class,"documentTypePrm");
			criteriaParameters.add(criteriaBuilder.equal(issuerRoot.get("documentType"), param));
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentNumber())){
			ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"documentNumberPrm");
			criteriaParameters.add(criteriaBuilder.like(issuerRoot.get("documentNumber"), param));
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getBusinessName())){
			ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"businessNamePrm");
			criteriaParameters.add(criteriaBuilder.like(issuerRoot.<String>get("businessName"), param));				
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getMnemonic())){
			ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"mnemonicPrm");
			criteriaParameters.add(criteriaBuilder.like(issuerRoot.<String>get("mnemonic"), param));
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getEconomicSector())) {
			ParameterExpression<Integer> param = criteriaBuilder.parameter(Integer.class,"economicSectorPrm");
			criteriaParameters.add(criteriaBuilder.equal(issuerRoot.get("economicSector"), param));
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getEconomicActivity())) {
			ParameterExpression<Integer> param = criteriaBuilder.parameter(Integer.class,"economicActivityPrm");
			criteriaParameters.add(criteriaBuilder.equal(issuerRoot.get("economicActivity"), param));
		}
		
		//Sort By
		if(Validations.validateIsNotNullAndNotEmpty(filter.getSortBy())){
			criteriaQuery.orderBy(criteriaBuilder.asc(issuerRoot.get(filter.getSortBy())));
		}
		
		if(!criteriaParameters.isEmpty()){
			if (criteriaParameters.size() == 1) {
				criteriaQuery.where(criteriaParameters.get(0));
		    } else {
		    	criteriaQuery.where(criteriaBuilder.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()])));
		    }
		}
		
		TypedQuery<Issuer> issuerRequestCriteria = em.createQuery(criteriaQuery);
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdIssuerPk())){
			issuerRequestCriteria.setParameter("idIssuerPkPrm", filter.getIdIssuerPk());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIssuerState()) && Validations.validateIsPositiveNumber(filter.getIssuerState())){
			issuerRequestCriteria.setParameter("statePrm", filter.getIssuerState());
		}
		
		if(Validations.validateIsNotNull(filter.getLstIssuerStates()) && Validations.validateIsPositiveNumber(filter.getLstIssuerStates().size())){
			issuerRequestCriteria.setParameter("lstStatePrm", filter.getLstIssuerStates());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentType()) && Validations.validateIsPositiveNumber(filter.getDocumentType())){
			issuerRequestCriteria.setParameter("documentTypePrm", filter.getDocumentType());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentNumber())){
			issuerRequestCriteria.setParameter("documentNumberPrm", "%"+filter.getDocumentNumber()+"%");
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getBusinessName())){
			if(filter.getBusinessName().indexOf(GeneralConstants.ASTERISK_STRING) != -1) {
				issuerRequestCriteria.setParameter("businessNamePrm", filter.getBusinessName().replace(GeneralConstants.ASTERISK_CHAR, GeneralConstants.PERCENTAGE_CHAR));
			} else {
				issuerRequestCriteria.setParameter("businessNamePrm", "%"+filter.getBusinessName()+"%");
			}
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getMnemonic())){
			if(filter.getMnemonic().indexOf(GeneralConstants.ASTERISK_STRING) != -1) {
				issuerRequestCriteria.setParameter("mnemonicPrm", filter.getMnemonic().replace(GeneralConstants.ASTERISK_CHAR, GeneralConstants.PERCENTAGE_CHAR));
			} else {
				issuerRequestCriteria.setParameter("mnemonicPrm", "%"+filter.getMnemonic()+"%");
			}
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getEconomicSector())) {
			issuerRequestCriteria.setParameter("economicSectorPrm", filter.getEconomicSector());
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getEconomicActivity())) {
			issuerRequestCriteria.setParameter("economicActivityPrm", filter.getEconomicActivity());
		}
		
		return (List<Issuer>)issuerRequestCriteria.getResultList();				
	}

	/**
	 * Find issuer request by id service bean.
	 *
	 * @param issuerRequestTO the issuer request to
	 * @return the issuer request
	 */
	public IssuerRequest findIssuerRequestByIdServiceBean(
			IssuerRequestTO issuerRequestTO) {
		try {
			
			StringBuilder sbQuery = new StringBuilder("Select ireq From IssuerRequest ireq ");
			sbQuery.append(" join fetch ireq.issuer iss ");
			sbQuery.append(" join fetch ireq.issuerFiles ifiles ");
			sbQuery.append(" where ireq.idIssuerRequestPk = :idIssuerRequestPrm ");
			sbQuery.append(" and ifiles.stateFile = :stateFilePrm ");
			
			Query query = em.createQuery(sbQuery.toString());		
			query.setParameter("idIssuerRequestPrm", issuerRequestTO.getIdIssuerRequestPk());
			query.setParameter("stateFilePrm", IssuerFileStateType.REGISTERED.getCode());
			
			return (IssuerRequest) query.getSingleResult();
			
		} catch(NoResultException ex){
		   return null;
		} catch(NonUniqueResultException ex){
		   return null;
		}
	}

	/**
	 * Gets the list issuer requests by filters service bean.
	 *
	 * @param filter the filter
	 * @return the list issuer requests by filters service bean
	 */
	public List<IssuerRequest> getListIssuerRequestsByFiltersServiceBean(
			IssuerTO filter) {
		CriteriaBuilder criteriaBuilder=em.getCriteriaBuilder();
		CriteriaQuery<IssuerRequest> criteriaQuery=criteriaBuilder.createQuery(IssuerRequest.class);
		Root<IssuerRequest> issuerRequestRoot=criteriaQuery.from(IssuerRequest.class);
		issuerRequestRoot.fetch("issuer");
		List<Predicate> criteriaParameters=new ArrayList<Predicate>();
		
		criteriaQuery.select(issuerRequestRoot);
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdIssuerPk())){
			ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"idIssuerPkPrm");
			criteriaParameters.add(criteriaBuilder.equal(issuerRequestRoot.get("issuer").get("idIssuerPk"), param));
		}

		if(Validations.validateIsNotNullAndNotEmpty(filter.getMnemonic())){
			ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"mnemonicPrm");
			criteriaParameters.add(criteriaBuilder.like(issuerRequestRoot.get("issuer").<String>get("mnemonic"), param));
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDescription())){
			ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"descriptionPrm");
			criteriaParameters.add(criteriaBuilder.like(issuerRequestRoot.get("issuer").<String>get("businessName"), param));	
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getDocumentType())){
			ParameterExpression<Integer> param = criteriaBuilder.parameter(Integer.class,"documentTypePrm");
			criteriaParameters.add(criteriaBuilder.equal(issuerRequestRoot.get("issuer").get("documentType"), param));	
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentNumber())){
			ParameterExpression<String> param = criteriaBuilder.parameter(String.class,"documentNumberPrm");
			criteriaParameters.add(criteriaBuilder.equal(issuerRequestRoot.get("issuer").get("documentNumber"), param));	
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(filter.getIssuerRequestTO().getLstIssuerRequestTypes())){
			ParameterExpression<List> param = criteriaBuilder.parameter(List.class,"lstIssuerRequestTypePrm");
			criteriaParameters.add(issuerRequestRoot.get("requestType").in(param));				
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getIssuerRequestTO().getRequestType())){
			ParameterExpression<Integer> param = criteriaBuilder.parameter(Integer.class,"requestTypePrm");
			criteriaParameters.add(criteriaBuilder.equal(issuerRequestRoot.get("requestType"), param));	
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIssuerRequestTO().getRequestNumber())){
			ParameterExpression<Long> param = criteriaBuilder.parameter(Long.class,"requestNumberPrm");
			criteriaParameters.add(criteriaBuilder.equal(issuerRequestRoot.get("requestNumber"), param));	
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(filter.getIssuerRequestTO().getLstIssuerRequestStates())){
			ParameterExpression<List> param = criteriaBuilder.parameter(List.class,"lstIssuerRequestStatePrm");
			criteriaParameters.add(issuerRequestRoot.get("state").in(param));				
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getIssuerRequestTO().getStateIssuerRequest())){
			ParameterExpression<Integer> param = criteriaBuilder.parameter(Integer.class,"stateIssuerRequestPrm");
			criteriaParameters.add(criteriaBuilder.equal(issuerRequestRoot.get("state"), param));	
		}				
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIssuerRequestTO().getInitiaRequestDate()) &&
				Validations.validateIsNotNullAndNotEmpty(filter.getIssuerRequestTO().getEndRequestDate())){
			ParameterExpression<Date> paramDate1 = criteriaBuilder.parameter(Date.class,"initReqDatePrm");
			ParameterExpression<Date> paramDate2 = criteriaBuilder.parameter(Date.class,"endReqDatePrm");
			criteriaParameters.add(criteriaBuilder.between(issuerRequestRoot.<Date>get("registryDate"), paramDate1 , paramDate2));
		}
		
		criteriaQuery.orderBy(criteriaBuilder.asc(issuerRequestRoot.get("registryDate")));
		
		if(!criteriaParameters.isEmpty()){
			if (criteriaParameters.size() == 1) {
				criteriaQuery.where(criteriaParameters.get(0));
		    } else {
		    	criteriaQuery.where(criteriaBuilder.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()])));
		    }
		}
		
		TypedQuery<IssuerRequest> issuerRequestCriteria = em.createQuery(criteriaQuery);
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdIssuerPk())){
			issuerRequestCriteria.setParameter("idIssuerPkPrm", filter.getIdIssuerPk());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getMnemonic())){
			if(filter.getMnemonic().indexOf(GeneralConstants.ASTERISK_STRING) != -1) {
				issuerRequestCriteria.setParameter("mnemonicPrm", filter.getMnemonic().replace(GeneralConstants.ASTERISK_CHAR, GeneralConstants.PERCENTAGE_CHAR));
			} else {
				issuerRequestCriteria.setParameter("mnemonicPrm", filter.getMnemonic());
			}			
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDescription())){
			if(filter.getDescription().indexOf(GeneralConstants.ASTERISK_STRING) != -1) {
				issuerRequestCriteria.setParameter("descriptionPrm", filter.getDescription().replace(GeneralConstants.ASTERISK_CHAR, GeneralConstants.PERCENTAGE_CHAR));
			} else {
				issuerRequestCriteria.setParameter("descriptionPrm", filter.getDescription());
			}
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getDocumentType())){
			issuerRequestCriteria.setParameter("documentTypePrm", filter.getDocumentType());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentNumber())){
			issuerRequestCriteria.setParameter("documentNumberPrm", filter.getDocumentNumber());
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(filter.getIssuerRequestTO().getLstIssuerRequestTypes())){
			issuerRequestCriteria.setParameter("lstIssuerRequestTypePrm", filter.getIssuerRequestTO().getLstIssuerRequestTypes());
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getIssuerRequestTO().getRequestType())){
			issuerRequestCriteria.setParameter("requestTypePrm", filter.getIssuerRequestTO().getRequestType());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIssuerRequestTO().getRequestNumber())){
			issuerRequestCriteria.setParameter("requestNumberPrm", filter.getIssuerRequestTO().getRequestNumber());
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(filter.getIssuerRequestTO().getLstIssuerRequestStates())){
			issuerRequestCriteria.setParameter("lstIssuerRequestStatePrm", filter.getIssuerRequestTO().getLstIssuerRequestStates());
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getIssuerRequestTO().getStateIssuerRequest())){
			issuerRequestCriteria.setParameter("stateIssuerRequestPrm", filter.getIssuerRequestTO().getStateIssuerRequest());
		}				
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIssuerRequestTO().getInitiaRequestDate()) &&
				Validations.validateIsNotNullAndNotEmpty(filter.getIssuerRequestTO().getEndRequestDate())) {
			issuerRequestCriteria.setParameter("initReqDatePrm", filter.getIssuerRequestTO().getInitiaRequestDate(), TemporalType.DATE);			
			issuerRequestCriteria.setParameter("endReqDatePrm", CommonsUtilities.addDate(filter.getIssuerRequestTO().getEndRequestDate(), 1) , TemporalType.DATE);
		}
		
		return (List<IssuerRequest>)issuerRequestCriteria.getResultList();	
	}

	/**
	 * Gets the list issuer history states service bean.
	 *
	 * @param issuerHistoryStateTO the issuer history state to
	 * @return the list issuer history states service bean
	 */
	public List<IssuerHistoryState> getListIssuerHistoryStatesServiceBean(
			IssuerHistoryStateTO issuerHistoryStateTO) {
		StringBuilder sbQuery = new StringBuilder("Select IHS From IssuerHistoryState IHS ");
		sbQuery.append(" Join Fetch IHS.issuer iss");
		sbQuery.append(" Where iss.idIssuerPk = :idIssuerPrm ");

		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idIssuerPrm", issuerHistoryStateTO.getIdIssuerPk());

		return (List<IssuerHistoryState>) query.getResultList();
	}
	
	/**
	 * Gets the issuer sequence code service bean.
	 *
	 * @return the issuer sequence code service bean
	 * @throws ServiceException the service exception
	 */
	public String getIssuerSequenceCodeServiceBean() throws ServiceException {
		Query query = em.createNativeQuery("select SQ_ID_ISSUER_PK.nextval from dual");					
		return String.format("%03d", Long.valueOf(query.getSingleResult().toString()));
	}
	
	/**
	 * Gets the holder by document type and document number service bean.
	 *
	 * @param holder the holder
	 * @return the holder by document type and document number service bean
	 * @throws ServiceException the service exception
	 */
	public Holder getHolderByDocumentTypeAndDocumentNumberServiceBean(Holder holder) throws ServiceException{
		try {
			  StringBuilder stringBuilderSql = new StringBuilder();
			  stringBuilderSql.append("SELECT h FROM Holder h WHERE h.documentType = :documentType ");	      
			  stringBuilderSql.append(" and h.documentNumber = :documentNumber");
			  if(holder.getDocumentSource()!=null){
				  stringBuilderSql.append(" and h.documentSource = :documentSource");
			  }
			  if(holder.getRelatedCui()!=null){
				  stringBuilderSql.append(" and h.relatedCui = :relatedCui");
			  }
			  
			  Query query = em.createQuery(stringBuilderSql.toString());
			  query.setParameter("documentType", holder.getDocumentType());	
			  query.setParameter("documentNumber", holder.getDocumentNumber());	
			  if(holder.getDocumentSource()!=null){
				  query.setParameter("documentSource", holder.getDocumentSource());	
			  }
			  if(holder.getRelatedCui()!=null){
				  query.setParameter("relatedCui", holder.getRelatedCui());
			  }
			  List<Holder> list =  (List<Holder>)query.getResultList();
			  Holder objHolder = new Holder();
			  if(list!=null && list.size()>0){
				 objHolder = list.get(0);
			  }
			  else{
				  objHolder = null;
			  }
		return objHolder;
		
		} catch(NoResultException ex){
		   return null;
		} catch(NonUniqueResultException ex){
		   return null;
		}
	}

	/**
	 * Validate exists issuer request service bean.
	 *
	 * @param issuerRequestTO the issuer request to
	 * @return the issuer request
	 */
	public IssuerRequest validateExistsIssuerRequestServiceBean(
			IssuerRequestTO issuerRequestTO) {
			
			StringBuilder sbQuery = new StringBuilder("Select IR From IssuerRequest IR ");
			sbQuery.append(" Where IR.state = :stateIssuerRequestPrm ");
			
			if(Validations.validateIsNotNullAndNotEmpty(issuerRequestTO.getIdIssuerPk())){
				sbQuery.append(" And IR.issuer.idIssuerPk = :idIssuerPrm ");
			}
			if(Validations.validateIsNotNullAndPositive(issuerRequestTO.getRequestType())){
				sbQuery.append(" And IR.requestType = :requestTypePrm ");
			}
			
			TypedQuery<IssuerRequest> query = em.createQuery(sbQuery.toString(), IssuerRequest.class);		
			query.setParameter("stateIssuerRequestPrm", issuerRequestTO.getStateIssuerRequest());
			
			if(Validations.validateIsNotNullAndNotEmpty(issuerRequestTO.getIdIssuerPk())){
				query.setParameter("idIssuerPrm", issuerRequestTO.getIdIssuerPk());
			}
			
			if(Validations.validateIsNotNullAndPositive(issuerRequestTO.getRequestType())){
				query.setParameter("requestTypePrm", issuerRequestTO.getRequestType());
			}
			
			List<IssuerRequest> lstIssuerRequest = query.getResultList();
			
			if(Validations.validateListIsNotNullAndNotEmpty(lstIssuerRequest)){
				if(Validations.validateIsNotNull(lstIssuerRequest.get(0).getIssuer())){
					lstIssuerRequest.get(0).getIssuer().getBusinessName();
				}
				return lstIssuerRequest.get(0);
			} else {
				return null;
			}
	
	}
	
	/**
	 * Gets the list issuer files service bean.
	 *
	 * @param issuerFilter the issuer filter
	 * @return the list issuer files service bean
	 * @throws ServiceException the service exception
	 */
	public List<IssuerFile> getListIssuerFilesServiceBean(Issuer issuerFilter) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder("Select IFile From IssuerFile IFile ");
		sbQuery.append(" Where IFile.issuer.idIssuerPk = :idIssuerPrm ");
		sbQuery.append(" And IFile.stateFile = :stateFilePrm ");	
		
		TypedQuery<IssuerFile> query = em.createQuery(sbQuery.toString(),IssuerFile.class);
		query.setParameter("idIssuerPrm", issuerFilter.getIdIssuerPk());
		query.setParameter("stateFilePrm", IssuerFileStateType.REGISTERED.getCode());		
		
		return (List<IssuerFile>) query.getResultList();
	}
	
	/**
	 * Gets the list represented entity by issuer service bean.
	 *
	 * @param issuerFilter the issuer filter
	 * @return the list represented entity by issuer service bean
	 * @throws ServiceException the service exception
	 */
	public List<RepresentedEntity> getListRepresentedEntityByIssuerServiceBean(
			Issuer issuerFilter) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder("Select distinct RP From RepresentedEntity RP ");
		sbQuery.append(" Join Fetch RP.legalRepresentative LegRep ");
		sbQuery.append(" Where LegRep.state = :stateLegalReprePrm ");
		sbQuery.append(" And RP.stateRepresented = :stateRepreEntityPrm ");
		sbQuery.append(" And RP.issuer.idIssuerPk = :idIssuerPrm ");
		
		TypedQuery<RepresentedEntity> query = em.createQuery(sbQuery.toString(),RepresentedEntity.class);		
		query.setParameter("stateLegalReprePrm", LegalRepresentativeStateType.REGISTERED.getCode());
		query.setParameter("stateRepreEntityPrm", RepresentativeEntityStateType.REGISTERED.getCode());
		query.setParameter("idIssuerPrm", issuerFilter.getIdIssuerPk());
		
		return (List<RepresentedEntity>) query.getResultList();
	}
	
	/**
	 * Find issuer modification request by id service bean.
	 *
	 * @param issuerRequestTO the issuer request to
	 * @return the issuer request
	 * @throws ServiceException the service exception
	 */
	public IssuerRequest findIssuerModificationRequestByIdServiceBean(
			IssuerRequestTO issuerRequestTO) throws ServiceException{
		try {
			
			StringBuilder sbQuery = new StringBuilder("Select ireq From IssuerRequest ireq ");
			sbQuery.append(" where ireq.idIssuerRequestPk = :idIssuerRequestPrm ");
			
			TypedQuery<IssuerRequest> query = em.createQuery(sbQuery.toString(), IssuerRequest.class);		
			query.setParameter("idIssuerRequestPrm", issuerRequestTO.getIdIssuerRequestPk());
			
			IssuerRequest issuerRequest = query.getSingleResult();
			if(Validations.validateIsNotNull(issuerRequest)) {
				if((Validations.validateIsNotNull(issuerRequest.getIssuer()))){
					issuerRequest.getIssuer().getBusinessName();
					if((Validations.validateIsNotNull(issuerRequest.getIssuer().getGeographicLocation()))){
						issuerRequest.getIssuer().getGeographicLocation().getName();
					}
				}
				if((Validations.validateIsNotNull(issuerRequest.getIssuerFileHistories()))){
					issuerRequest.getIssuerFileHistories().size();
				}
				if((Validations.validateIsNotNull(issuerRequest.getIssuerHistories()))){
					issuerRequest.getIssuerHistories().size();
					if(Validations.validateListIsNotNullAndNotEmpty(issuerRequest.getIssuerHistories())){
						IssuerHistory issuerHistory = issuerRequest.getIssuerHistories().get(0);
						if((Validations.validateIsNotNull(issuerHistory.getGeographicLocation()))){
							issuerHistory.getGeographicLocation().getIdGeographicLocationPk();
							issuerHistory.getGeographicLocation().getName();
						}
						IssuerHistory issuerHistoryAux = issuerRequest.getIssuerHistories().get(1);
						if((Validations.validateIsNotNull(issuerHistoryAux.getGeographicLocation()))){
							issuerHistoryAux.getGeographicLocation().getIdGeographicLocationPk();
							issuerHistoryAux.getGeographicLocation().getName();
						}
					}
					
				}
			}
			
			return issuerRequest;
		} catch(NoResultException ex){
		   return null;
		} catch(NonUniqueResultException ex){
		   return null;
		}
	}

	/**
	 * Busqueda de modificacion de emisor historico
	 * **/
	public IssuerHistory findIssuerHistory(Long issuerRequest,Integer registryType){
		 StringBuilder stringBuilderSql = new StringBuilder();
		 stringBuilderSql.append("Select ih from IssuerHistory ih ");
		 stringBuilderSql.append(" Where 1 = 1 ");
		 stringBuilderSql.append(" and ih.issuerRequest.idIssuerRequestPk = :idIssuerHistoryPk ");
		 stringBuilderSql.append(" and ih.registryType = :registryType "); 

		 Query query = em.createQuery(stringBuilderSql.toString());
		 query.setParameter("idIssuerHistoryPk",issuerRequest);
		 query.setParameter("registryType",registryType);
		 
		 IssuerHistory history = (IssuerHistory) query.getSingleResult();
		 
		 return history;
	}
	
	
	/**
	 * Gets the list issuer file histories service bean.
	 *
	 * @param issuerRequest the issuer request
	 * @return the list issuer file histories service bean
	 * @throws ServiceException the service exception
	 */
	public List<IssuerFileHistory> getListIssuerFileHistoriesServiceBean(IssuerRequest issuerRequest) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder("Select IFile From IssuerFileHistory IFile ");
		sbQuery.append(" Where IFile.issuerRequest.idIssuerRequestPk = :idIssuerRequestPrm ");
		
		TypedQuery<IssuerFileHistory> query = em.createQuery(sbQuery.toString(),IssuerFileHistory.class);
		query.setParameter("idIssuerRequestPrm", issuerRequest.getIdIssuerRequestPk());		
		
		return (List<IssuerFileHistory>) query.getResultList();
	}
	
	/**
	 * Gets the list legal representative hist by issuer request service bean.
	 *
	 * @param issuerRequest the issuer request
	 * @return the list legal representative hist by issuer request service bean
	 * @throws ServiceException the service exception
	 */
	public List<LegalRepresentativeHistory> getListLegalRepresentativeHistByIssuerRequestServiceBean(IssuerRequest issuerRequest) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder("Select distinct(LRH) From LegalRepresentativeHistory LRH ");
		sbQuery.append(" Where LRH.issuerRequest.idIssuerRequestPk = :idIssuerRequestPrm ");
		
		TypedQuery<LegalRepresentativeHistory> query = em.createQuery(sbQuery.toString(),LegalRepresentativeHistory.class);
		query.setParameter("idIssuerRequestPrm", issuerRequest.getIdIssuerRequestPk());		
		
		return (List<LegalRepresentativeHistory>) query.getResultList();
	}

	/**
	 * Find issuer request by id service bean.
	 *
	 * @param idIssuerRequestPk the id issuer request pk
	 * @return the issuer request
	 */
	public IssuerRequest findIssuerRequestByIdServiceBean(Long idIssuerRequestPk) {
		try {
			
			StringBuilder sbQuery = new StringBuilder("Select ireq From IssuerRequest ireq ");
			sbQuery.append(" join fetch ireq.issuer iss ");
			sbQuery.append(" where ireq.idIssuerRequestPk = :idIssuerRequestPrm ");
			
			Query query = em.createQuery(sbQuery.toString());		
			query.setParameter("idIssuerRequestPrm", idIssuerRequestPk);			
			
			return (IssuerRequest) query.getSingleResult();
			
		} catch(NoResultException ex){
		   return null;
		} catch(NonUniqueResultException ex){
		   return null;
		}
	}
	
	/**
	 * Disable issuer files state service bean.
	 *
	 * @param issuer the issuer
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int disableIssuerFilesStateServiceBean(Issuer issuer, LoggerUser loggerUser) throws ServiceException {
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update IssuerFile IFile ");	
		sbQuery.append(" Set IFile.stateFile = :statePrm, IFile.lastModifyUser = :lastModifyUserPrm, ");
		sbQuery.append(" IFile.lastModifyDate = :lastModifyDatePrm, IFile.lastModifyIp = :lastModifyIpPrm, ");
		sbQuery.append(" IFile.lastModifyApp = :lastModifyAppPrm ");
		sbQuery.append(" Where IFile.issuer.idIssuerPk = :idIssuerPrm And IFile.issuerRequest.idIssuerRequestPk is null ");
		
		Query query = em.createQuery(sbQuery.toString());
					
		query.setParameter("statePrm",IssuerFileStateType.DELETED.getCode());
		query.setParameter("lastModifyUserPrm",loggerUser.getUserName());
		query.setParameter("lastModifyDatePrm",loggerUser.getAuditTime());
		query.setParameter("lastModifyIpPrm",loggerUser.getIpAddress());
		query.setParameter("lastModifyAppPrm",loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("idIssuerPrm", issuer.getIdIssuerPk());

		return query.executeUpdate();
	}

	/**
	 * Disable represented entities state service bean.
	 *
	 * @param issuer the issuer
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int disableRepresentedEntitiesStateServiceBean(Issuer issuer, LoggerUser loggerUser) throws ServiceException{

		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update RepresentedEntity RE ");	
		sbQuery.append(" Set RE.stateRepresented = :statePrm, RE.lastModifyUser = :lastModifyUserPrm, ");
		sbQuery.append(" RE.lastModifyDate = :lastModifyDatePrm, RE.lastModifyIp = :lastModifyIpPrm, ");
		sbQuery.append(" RE.lastModifyApp = :lastModifyAppPrm ");
		sbQuery.append(" Where RE.issuer.idIssuerPk = :idIssuerPrm ");
		
		Query query = em.createQuery(sbQuery.toString());
					
		query.setParameter("statePrm",RepresentativeEntityStateType.DELETED.getCode());
		query.setParameter("lastModifyUserPrm",loggerUser.getUserName());
		query.setParameter("lastModifyDatePrm",loggerUser.getAuditTime());
		query.setParameter("lastModifyIpPrm",loggerUser.getIpAddress());
		query.setParameter("lastModifyAppPrm",loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("idIssuerPrm", issuer.getIdIssuerPk());

		return query.executeUpdate();
	}
	
	/**
	 * Gets the list legal representative file service bean.
	 *
	 * @param legalRepresentative the legal representative
	 * @return the list legal representative file service bean
	 * @throws ServiceException the service exception
	 */
	public List<LegalRepresentativeFile> getListLegalRepresentativeFileServiceBean(
			LegalRepresentative legalRepresentative) throws ServiceException{
		
		StringBuilder sbQuery = new StringBuilder("Select LRF From LegalRepresentativeFile LRF ");
		sbQuery.append(" Where LRF.legalRepresentative.idLegalRepresentativePk = :idLegalRepresentativePrm ");
		sbQuery.append(" And LRF.stateFile = :statePrm ");
		
		TypedQuery<LegalRepresentativeFile> query = em.createQuery(sbQuery.toString(),LegalRepresentativeFile.class);
		query.setParameter("idLegalRepresentativePrm", legalRepresentative.getIdLegalRepresentativePk());
		query.setParameter("statePrm", HolderFileStateType.REGISTERED.getCode());
		
		List<LegalRepresentativeFile> legalRepresentativeFiles = (List<LegalRepresentativeFile>) query.getResultList();
		
		if(Validations.validateListIsNotNullAndNotEmpty(legalRepresentativeFiles)){
			for (LegalRepresentativeFile legalRepresentativeFile : legalRepresentativeFiles) {
				legalRepresentativeFile.getFileLegalRepre();
			}
		}
				
		return legalRepresentativeFiles;
	}
	/**
	 * Gets the list legal representative file service bean.
	 *
	 * @param legalRepresentative the legal representative
	 * @return the list legal representative file service bean
	 * @throws ServiceException the service exception
	 */
	public List<RepresentativeFileHistory> getListLegalRepresentativeFileHistoryServiceBean(
			LegalRepresentative legalRepresentative) throws ServiceException{
		
		List<RepresentativeFileHistory> lstRepresentativeFileHistory = null;
		StringBuilder sbQuery = new StringBuilder("Select LRF From RepresentativeFileHistory LRF ");
		sbQuery.append(" Where LRF.legalRepresentativeHistory.idRepresentativeHistoryPk = :idLegalRepresentativePrm ");
		sbQuery.append(" And LRF.stateFile = :statePrm ");
		
		TypedQuery<RepresentativeFileHistory> query = em.createQuery(sbQuery.toString(),RepresentativeFileHistory.class);
		query.setParameter("idLegalRepresentativePrm", legalRepresentative.getIdLegalRepresentativePk());
		query.setParameter("statePrm", HolderFileStateType.REGISTERED.getCode());
		
		lstRepresentativeFileHistory = (List<RepresentativeFileHistory>)query.getResultList();
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstRepresentativeFileHistory)){
			for (RepresentativeFileHistory legalRepresentativeFile : lstRepresentativeFileHistory) {
				legalRepresentativeFile.getFileRepresentative();
			}
		}
				
		return lstRepresentativeFileHistory;
	}

	/**
	 * Gets the list legal representative file hist service bean.
	 *
	 * @param legalRepresentativeHistory the legal representative history
	 * @return the list legal representative file hist service bean
	 * @throws ServiceException the service exception
	 */
	public List<RepresentativeFileHistory> getListLegalRepresentativeFileHistServiceBean(
			LegalRepresentativeHistory legalRepresentativeHistory) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder("Select RFH From RepresentativeFileHistory RFH ");
		sbQuery.append(" Where RFH.legalRepresentativeHistory.idRepresentativeHistoryPk = :ididRepresentativeHistoryPrm ");
		
		TypedQuery<RepresentativeFileHistory> query = em.createQuery(sbQuery.toString(),RepresentativeFileHistory.class);
		query.setParameter("ididRepresentativeHistoryPrm", legalRepresentativeHistory.getIdRepresentativeHistoryPk());		
		
		List<RepresentativeFileHistory> representativeFileHistories = (List<RepresentativeFileHistory>) query.getResultList();
		
		if(Validations.validateListIsNotNullAndNotEmpty(representativeFileHistories)){
			for (RepresentativeFileHistory representativeFileHistory : representativeFileHistories) {
				representativeFileHistory.getFileRepresentative();
			}
		}
		return representativeFileHistories;
	}
	
	/**
	 * Update state issuer service bean.
	 *
	 * @param issuer the issuer
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int updateStateIssuerServiceBean(Issuer issuer, LoggerUser loggerUser) throws ServiceException {
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update Issuer Iss ");
		sbQuery.append(" Set Iss.stateIssuer = :statePrm, Iss.lastModifyUser = :lastModifyUserPrm, ");
		sbQuery.append(" Iss.lastModifyDate = :lastModifyDatePrm, Iss.lastModifyIp = :lastModifyIpPrm, ");
		sbQuery.append(" Iss.lastModifyApp = :lastModifyAppPrm ");
		sbQuery.append(" Where Iss.idIssuerPk = :idIssuerPrm ");
		
		Query query = em.createQuery(sbQuery.toString());
					
		query.setParameter("statePrm", issuer.getStateIssuer());
		query.setParameter("lastModifyUserPrm",loggerUser.getUserName());
		query.setParameter("lastModifyDatePrm",loggerUser.getAuditTime());
		query.setParameter("lastModifyIpPrm",loggerUser.getIpAddress());
		query.setParameter("lastModifyAppPrm",loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("idIssuerPrm", issuer.getIdIssuerPk());

		return query.executeUpdate();
	}
	
	/**
	 * Update state issuer request service bean.
	 *
	 * @param issuerRequest the issuer request
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int updateStateIssuerRequestServiceBean(IssuerRequest issuerRequest, LoggerUser loggerUser) throws ServiceException {
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update IssuerRequest IR ");
		sbQuery.append(" Set IR.state = :statePrm, IR.lastModifyUser = :lastModifyUserPrm, ");
		sbQuery.append(" IR.lastModifyDate = :lastModifyDatePrm, IR.lastModifyIp = :lastModifyIpPrm, ");
		sbQuery.append(" IR.lastModifyApp = :lastModifyAppPrm, ");
		sbQuery.append(" IR.actionDate = :actionDatePrm ");
		
		if(Validations.validateIsNotNullAndNotEmpty(issuerRequest.getActionMotive())){
			sbQuery.append(" , IR.actionMotive = :actionMotivePrm ");
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(issuerRequest.getActionOtherMotive())){
			sbQuery.append(" , IR.actionOtherMotive = :actionOtherMotivePrm ");
		}
		
		sbQuery.append(" Where IR.idIssuerRequestPk = :idIssuerRequestPrm ");
		
		Query query = em.createQuery(sbQuery.toString());
					
		query.setParameter("statePrm", issuerRequest.getState());
		query.setParameter("lastModifyUserPrm",loggerUser.getUserName());
		query.setParameter("lastModifyDatePrm",loggerUser.getAuditTime());
		query.setParameter("lastModifyIpPrm",loggerUser.getIpAddress());
		query.setParameter("lastModifyAppPrm",loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("idIssuerRequestPrm", issuerRequest.getIdIssuerRequestPk());
		query.setParameter("actionDatePrm",loggerUser.getAuditTime());
		
		if(Validations.validateIsNotNullAndNotEmpty(issuerRequest.getActionMotive())){
			query.setParameter("actionMotivePrm", issuerRequest.getActionMotive());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(issuerRequest.getActionOtherMotive())){
			query.setParameter("actionOtherMotivePrm", issuerRequest.getActionOtherMotive());
		}

		return query.executeUpdate();
	}
	
	/**
	 * Find pep person by legal representative service bean.
	 *
	 * @param legalRepresentative the legal representative
	 * @return the pep person
	 * @throws ServiceException the service exception
	 */
	public PepPerson findPepPersonByLegalRepresentativeServiceBean(LegalRepresentative legalRepresentative)
			throws ServiceException{
		try {
			
			StringBuilder sbQuery = new StringBuilder("Select P From PepPerson P ");			
			sbQuery.append(" Where P.legalRepresentative.idLegalRepresentativePk = :idLegalRepresentativePrm");
			
			TypedQuery<PepPerson> query = em.createQuery(sbQuery.toString() , PepPerson.class);		
			query.setParameter("idLegalRepresentativePrm", legalRepresentative.getIdLegalRepresentativePk());
			
			return (PepPerson) query.getSingleResult();
			
		} catch(NoResultException ex){
		   return null;
		} catch(NonUniqueResultException ex){
		   return null;
		}
	}
	
	/**
	 * Gets the legal representative by document type and document number service bean.
	 *
	 * @param legalRepresentative the legal representative
	 * @return the legal representative by document type and document number service bean
	 * @throws ServiceException the service exception
	 */
	public LegalRepresentative getLegalRepresentativeByDocumentTypeAndDocumentNumberServiceBean(LegalRepresentative legalRepresentative) throws ServiceException {
		
		  StringBuilder stringBuilderSql = new StringBuilder();			
		  stringBuilderSql.append("SELECT h FROM LegalRepresentative h WHERE h.documentType = :documentType ");	      
		  stringBuilderSql.append(" and h.documentNumber = :documentNumber");
		  
		  if(legalRepresentative.getDocumentSource()!=null){
			  stringBuilderSql.append(" and h.documentSource = :documentSource");
		  }
		  
		  TypedQuery<LegalRepresentative> query = em.createQuery(stringBuilderSql.toString(),LegalRepresentative.class);
		  query.setParameter("documentType", legalRepresentative.getDocumentType());	
		  query.setParameter("documentNumber", legalRepresentative.getDocumentNumber());
		  
		  if(legalRepresentative.getDocumentSource()!=null){
			  query.setParameter("documentSource", legalRepresentative.getDocumentSource());
		  }
		 
		  List<LegalRepresentative> lstresultRepresentative = (List<LegalRepresentative>) query.getResultList();
		  
		  if(Validations.validateListIsNotNullAndNotEmpty(lstresultRepresentative)) {
			  return lstresultRepresentative.get(0);
		  } else {
			  return null;
		  }
	}
	
	/**
	 * Gets the pep person by id holder service bean.
	 *
	 * @param idHolderPk the id holder pk
	 * @return the pep person by id holder service bean
	 */
	public PepPerson getPepPersonByIdHolderServiceBean(Long idHolderPk){
		PepPerson pepPerson;
		try{
		  StringBuilder stringBuilderSql = new StringBuilder();
		  stringBuilderSql.append("SELECT p FROM PepPerson p WHERE p.holder.idHolderPk = :idHolderPk");	      
		  Query query = em.createQuery(stringBuilderSql.toString());
		  query.setParameter("idHolderPk", idHolderPk);	 
		  pepPerson = (PepPerson) query.getSingleResult();
		}
		catch(Exception e){
			pepPerson = null;
		}
	
		return pepPerson;
		
	}
	
	/**
	 * Disable legal representative files service bean.
	 *
	 * @param objLegalRepresentative the obj legal representative
	 * @param loggerUser the logger user
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int disableLegalRepresentativeFilesServiceBean(LegalRepresentative objLegalRepresentative, LoggerUser loggerUser) 
									throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update LegalRepresentativeFile LFile ");
		sbQuery.append(" Set LFile.stateFile = :statePrm, LFile.lastModifyUser = :lastModifyUserPrm, ");
		sbQuery.append(" LFile.lastModifyDate = :lastModifyDatePrm, LFile.lastModifyIp = :lastModifyIpPrm, ");
		sbQuery.append(" LFile.lastModifyApp = :lastModifyAppPrm ");
		sbQuery.append(" Where LFile.legalRepresentative.idLegalRepresentativePk = :idLegaRepresentativePrm ");
		
		Query query = em.createQuery(sbQuery.toString());
					
		query.setParameter("statePrm",HolderFileStateType.DELETED.getCode());
		query.setParameter("lastModifyUserPrm",loggerUser.getUserName());
		query.setParameter("lastModifyDatePrm",loggerUser.getAuditTime());
		query.setParameter("lastModifyIpPrm",loggerUser.getIpAddress());
		query.setParameter("lastModifyAppPrm",loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("idLegaRepresentativePrm", objLegalRepresentative.getIdLegalRepresentativePk());

		return query.executeUpdate();		
	}
	
	/**
	 * Gets the pep person by id legal representative service bean.
	 *
	 * @param idLegalRepresentativePk the id legal representative pk
	 * @return the pep person by id legal representative service bean
	 */
	public PepPerson getPepPersonByIdLegalRepresentativeServiceBean(Long idLegalRepresentativePk){
		PepPerson pepPerson;
		try{
		  StringBuilder stringBuilderSql = new StringBuilder();
		  stringBuilderSql.append("SELECT p FROM PepPerson p WHERE p.legalRepresentative.idLegalRepresentativePk = :idLegalRepresentativePk");	      
		  Query query = em.createQuery(stringBuilderSql.toString());
		  query.setParameter("idLegalRepresentativePk", idLegalRepresentativePk);	 
	      pepPerson = (PepPerson) query.getSingleResult();
		}
		catch(Exception e){
			pepPerson = null;
		}
		return pepPerson;
	}
	
	/**
	 * Validate exists superintendent resolution siv service bean.
	 *
	 * @param issuer the issuer
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean validateExistsSuperintendentResolutionSivServiceBean(Issuer issuer) throws ServiceException{
		Integer resultCount;
		
		 StringBuilder stringBuilderSql = new StringBuilder();
		 stringBuilderSql.append("SELECT Count(Iss) FROM Issuer Iss ");
		 stringBuilderSql.append(" Where Iss.superintendentResolution = :superintendentResolutionPrm ");
		  
		  if(Validations.validateIsNotNullAndNotEmpty(issuer.getIdIssuerPk())){
			  stringBuilderSql.append(" And Iss.idIssuerPk != :idIssuerPrm ");
		  }
		  
		  Query query = em.createQuery(stringBuilderSql.toString());
		  query.setParameter("superintendentResolutionPrm", issuer.getSuperintendentResolution());
		  
		  if(Validations.validateIsNotNullAndNotEmpty(issuer.getIdIssuerPk())){
			  query.setParameter("idIssuerPrm", issuer.getIdIssuerPk());  
		  }
		  
		  resultCount = Integer.parseInt(query.getSingleResult().toString());
		
		return resultCount > 0;
	}

	/**
	 * Gets the holder state by id.
	 *
	 * @param idHolderPk the id holder pk
	 * @return the holder state by id
	 * @throws ServiceException the service exception
	 */
	public Integer getHolderStateById(Long idHolderPk) throws ServiceException {
		 StringBuilder stringBuilderSql = new StringBuilder();
		 stringBuilderSql.append("SELECT H.stateHolder from Holder H ");
		 stringBuilderSql.append(" Where H.idHolderPk = :idHolderPrm ");

		  TypedQuery<Integer> query = em.createQuery(stringBuilderSql.toString(), Integer.class);
		  query.setParameter("idHolderPrm", idHolderPk);
		  
		  return query.getSingleResult();
	}
	
	
	/**
	 * Creates the sequence dinamic.
	 *
	 * @param mnenomicIssuer the mnenomic issuer
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int createSequenceDinamic(String mnenomicIssuer) throws ServiceException {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" CREATE SEQUENCE SQ_ID_ISSUANCE_" + mnenomicIssuer);
		stringBuffer.append(" INCREMENT BY 1 ");
		stringBuffer.append(" START WITH 1  ");
		stringBuffer.append(" MINVALUE 1 ");
		stringBuffer.append(" MAXVALUE 99999999 ");				
		Query query= em.createNativeQuery(stringBuffer.toString());			
		return query.executeUpdate();		
	}
	
	public List<LegalRepresentative> getListLegalRepresentativeByIssuerServiceBean(Issuer issuer) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder("Select distinct(LR) From LegalRepresentative LR ");
		sbQuery.append(" Where LR.idLegalRepresentativePk in (select re.legalRepresentative.idLegalRepresentativePk from RepresentedEntity re where re.issuer.idIssuerPk = :idIssuerPrm) ");
		
		TypedQuery<LegalRepresentative> query = em.createQuery(sbQuery.toString(),LegalRepresentative.class);
		query.setParameter("idIssuerPrm", issuer.getIdIssuerPk());		
		
		return (List<LegalRepresentative>) query.getResultList();
	}

}
