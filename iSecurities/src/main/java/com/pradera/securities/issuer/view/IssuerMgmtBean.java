package com.pradera.securities.issuer.view;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.to.LegalRepresentativeTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.extension.view.LegalRepresentativeHelperBean;
import com.pradera.core.component.utils.DocumentTO;
import com.pradera.core.component.utils.DocumentValidator;
import com.pradera.core.component.utils.PersonTO;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.HolderFile;
import com.pradera.model.accounts.InstitutionInformation;
import com.pradera.model.accounts.LegalRepresentative;
import com.pradera.model.accounts.LegalRepresentativeFile;
import com.pradera.model.accounts.LegalRepresentativeHistory;
import com.pradera.model.accounts.OfacPerson;
import com.pradera.model.accounts.PepPerson;
import com.pradera.model.accounts.PnaPerson;
import com.pradera.model.accounts.RepresentativeFileHistory;
import com.pradera.model.accounts.RepresentedEntity;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.EconomicActivityType;
import com.pradera.model.accounts.type.EconomicSectorType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.LegalRepresentativeStateType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.GeographicLocationStateType;
import com.pradera.model.generalparameter.type.GeographicLocationType;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.IssuerFile;
import com.pradera.model.issuancesecuritie.type.IssuerDocumentType;
import com.pradera.model.issuancesecuritie.type.IssuerFileStateType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.RequestIssuerFileType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.securities.issuancesecuritie.util.PropertiesConstants;
import com.pradera.securities.issuer.facade.IssuerServiceFacade;
import com.pradera.securities.issuer.to.IssuerTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ParticipantMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 08/02/2013
 */
@DepositaryWebBean
@LoggerCreateBean
public class IssuerMgmtBean extends GenericBaseBean implements Serializable{
	
	/** The notification service facade. */
	@EJB NotificationServiceFacade notificationServiceFacade;
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5237749810753840177L;

	/** The ind numeric second type document legal. */
	private boolean indNumericSecondTypeDocumentLegal;		
	
	/** The lst second countries representative. */
	private List<GeographicLocation> lstSecondCountriesRepresentative;		

	/** The country residence. */
	@Inject @Configurable Integer countryResidence;
	
	
	/** The legal representative helper bean. */
	@Inject
    private LegalRepresentativeHelperBean legalRepresentativeHelperBean;
	
	
	/** The email legal representative. */
	private String emailLegalRepresentative;
	
	/** The lstt holder file types. */
	private List<ParameterTable> lsttHolderFileTypes;
	
	/** The lst holder files. */
	private List<HolderFile> lstHolderFiles;
	
	/** The representative file name display. */
	private String representativeFileNameDisplay;
	
	/** The lst representative file types. */
	private List<ParameterTable> lstRepresentativeFileTypes;
	
	/** The lst representative file history. */
	private List<RepresentativeFileHistory> lstRepresentativeFileHistory;
	
	/** The representative file type. */
	private Integer representativeFileType;
	
	/** The max lenght document number legal. */
	private int maxLenghtDocumentNumberLegal;
	
	/** The ind numeric type document legal. */
	private boolean indNumericTypeDocumentLegal;
	
	/** The lst document type representative result. */
	private List<ParameterTable> lstDocumentTypeRepresentativeResult;
	
	/** The lst represented entity. */
	private List<RepresentedEntity> lstRepresentedEntity;
	
	/** The lst legal representative. */
	private List<LegalRepresentative> lstLegalRepresentative;
	
	/** The legal representative history. */
	private LegalRepresentativeHistory legalRepresentativeHistory;
	
	/** The lst legal representative history. */
	private List<LegalRepresentativeHistory> lstLegalRepresentativeHistory;
	
	/** The pep person. */
	private PepPerson pepPerson;
	
	/** The lst representative class. */
	private List<ParameterTable> lstRepresentativeClass;
	
	/** The lst document type representative. */
	private List<ParameterTable> lstDocumentTypeRepresentative;
	
	/** The lst second document type representative. */
	private List<ParameterTable> lstSecondDocumentTypeRepresentative;

	/** The lst economic activity representative. */
	private List<ParameterTable> lstEconomicActivityRepresentative;
	
	/** The lst sex. */
	private List<ParameterTable> lstSex;	
	
	/** The ind representative natural person. */
	private boolean indRepresentativeNaturalPerson;
	
	/** The ind representative juridic person. */
	private boolean indRepresentativeJuridicPerson;
	
	/** The flag cmb representative class. */
	private boolean flagCmbRepresentativeClass;
	
	/** The ind representative resident. */
	private boolean indRepresentativeResident;
	
	/** The disabled country resident representative. */
	private boolean disabledCountryResidentRepresentative;
	
	/** The ind representative is pep. */
	private boolean indRepresentativeIsPEP;
	
	/** The ind copy legal representative. */
	private boolean indCopyLegalRepresentative;
	
	/** The lst person type. */
	private List<ParameterTable> lstPersonType;
	
	/** The lst boolean. */
	private List<BooleanType> lstBoolean;    
    
    /** The lst postal provinces representative. */
    private List<GeographicLocation> lstPostalProvincesRepresentative;
    
    /** The lst postal districts representative. */
    private List<GeographicLocation> lstPostalDistrictsRepresentative;    
    
    /** The lst legal provinces representative. */
    private List<GeographicLocation> lstLegalProvincesRepresentative;
    
    /** The lst legal districts representative. */
    private List<GeographicLocation> lstLegalDistrictsRepresentative;
	
	/** The issuer. */
	private Issuer issuer;       
	
	/** The issuer result. */
	private Issuer issuerResult;
	
	/** The holder result. */
	private Holder holderResult;
	
	/** The list document source. */
	private List<ParameterTable> listDocumentSource;
	
	/** The ind ficpat economic activity. */
	private boolean indFICPATEconomicActivity;
	
	private List<InvestorTypeModelTO> listInvestorType;
	
	
	/** The lst countries. */
    private List<GeographicLocation> lstCountries;
    
    /** The lst Department. */
    private List<GeographicLocation> lstDepartments;
    
    /** The lst provinces. */
    private List<GeographicLocation> lstProvinces;
    
    /** The lst districts. */
    private List<GeographicLocation> lstDistricts;
    
    /** The lst participant document type. */
    private List<ParameterTable> lstIssuerDocumentType;
    
    /** The lst participant document type for search. */
    private List<ParameterTable> lstIssuerDocumentTypeForSearch;
    
    /** The lst issuer attachment type. */
    private List<ParameterTable> lstIssuerAttachmentType;
    
    /** The lst economic sector. */
    private List<ParameterTable> lstEconomicSector;
    
    /** The lst economic sector for search. */
    private List<ParameterTable> lstEconomicSectorForSearch;
    
    /** The lst cbo credit rating scales. */
    private List<ParameterTable> lstCboCreditRatingScales;
    
    /** The lst economic activity. */
    private List<ParameterTable> lstEconomicActivity;
    
    /** The lst economic activity for search. */
    private List<ParameterTable> lstEconomicActivityForSearch;
    
    /** The lst participant state. */
    private List<ParameterTable> lstIssuerState;
    
    /** The lst participant state for search. */
    private List<ParameterTable> lstIssuerStateForSearch;
    
    /** The lst society type. */
    private List<ParameterTable> lstSocietyType;
    
    /** The lst currency. */
    private List<ParameterTable> lstCurrency;
    
    /** The lst category pep. */
    private List<ParameterTable> lstCategoryPep;

	/** The lst role pep. */
	private List<ParameterTable> lstRolePep;
	
	/** The lst Issuer Type */
	private List<ParameterTable> lstOfferType;
    
    /** The participant data model. */
    private IssuerDataModel issuerDataModel;
    
    /** The participant to. */
    private IssuerTO issuerTO;
    
    /** The id participant pk. */
    private String idIssuerPk;
    
    /** The state participant. */
    private Integer stateIssuer;    
    
    /** The participant files. */
    private List<IssuerFile> lstIssuerFiles;
    
    /** The id represented entity pk. */
    private Long idRepresentedEntityPk;
    
    /** The represented entity. */
    private RepresentedEntity representedEntity;
    
    /** Document Document Size. */
    private String fUploadFileDocumentsSize="16777216";//15MB en bytes;
    
    /** The size document display. */
	private String fUploadFileSizeDocumentsDisplay="15000";
	
    /** Document Validate. */
    @Inject
    private DocumentValidator documentValidatorRegister;  
    
    /** Document search. */
    private DocumentTO documentTORegister;
    
    /** The general parameters facade. */
    @EJB
    GeneralParametersFacade generalParametersFacade;
    
    /** The accounts facade. */
    @EJB
    AccountsFacade accountsFacade;
    
    /** The participant service facade. */
    @EJB
    IssuerServiceFacade issuerServiceFacade;
    
    /** The user info. */
    @Inject UserInfo userInfo;
    
    /** The user privilege. */
    @Inject UserPrivilege userPrivilege;
    
    /** The province default. */
	@Inject @Configurable Integer provinceDefault;
	
	/** The department default. */
	@Inject @Configurable Integer departmentDefault;
	
	/** The district default. */
	@Inject @Configurable Integer districtDefault;
	
        
    /** The ind issuer depositary contract. */
    private boolean indIssuerDepositaryContract;
    
    /** The ind inscription stock exchange. */
    private boolean indInscriptionStockExchange;        
    
	/** The ind issuer differentiated. */
    private boolean indIssuerDifferentiated;
    
    /** The ind issuer differentiated. */
    private boolean indIssuerSIRTEXnegociate;

	/** The max lenght document number holder. */
	private int maxLenghtDocumentNumberHolder;

	/** The max lenght second document number legal. */
	private int maxLenghtSecondDocumentNumberLegal;

	/** The issuer file name display. */
	private String issuerFileNameDisplay;
	
	/** The attach issuer file selected. */
	private Integer attachIssuerFileSelected;
	
	/** The min depositary contract date. */
	private Date minDepositaryContractDate;				
	
	/** The min inscription stock exch date. */
	private Date minInscriptionStockExchDate;
		  
	/**  Window registry issuer. */
	private String WINDOW_REGISTRY_ISSUER = "registerIssuerRule";
	
	/**  Window detail issuer. */
	private String WINDOW_DETAIL_ISSUER = "issuerDetailRule";
	
	/** The streamed content file. */
	private transient  StreamedContent streamedContentFile;
	/**
	 * Instantiates a new participant mgmt bean.
	 */
	public IssuerMgmtBean(){		  
		 
	}

	/**
	 * Clean search.
	 */
	public void cleanSearch(){		
		cleanDataModel();		
		if(!userInfo.getUserAccountSession().isIssuerInstitucion() && !userInfo.getUserAccountSession().isIssuerDpfInstitucion()) {
			validateUserAccountSession();
			JSFUtilities.resetViewRoot();
		}		
	}
	
	/**
	 * Validate User Account Session().
	 */
	public void validateUserAccountSession()
	{
		issuerTO = new IssuerTO();
		if((userInfo.getUserAccountSession().isIssuerInstitucion() || userInfo.getUserAccountSession().isIssuerDpfInstitucion()) && 
				Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getIssuerCode())) {			
			issuerTO.setIdIssuerPk(userInfo.getUserAccountSession().getIssuerCode());
		}
	}
	
	/**
	 * Load Issuer Document Type National.
	 */
	public void loadIssuerDocumentTypeNational()
	{
		PersonTO person=new PersonTO();
		person.setFlagIssuerIndicator(true);
		person.setFlagNational(true);
		person.setPersonType(PersonType.JURIDIC.getCode());
		lstIssuerDocumentType=documentValidatorRegister.getDocumentTypeResult(person);
	}
	
	/**
	 * Load Issuer Document Type Foreign.
	 */
	public void loadIssuerDocumentTypeForeign()
	{
		PersonTO person=new PersonTO();
		person.setFlagIssuerIndicator(true);
		person.setFlagForeign(true);
		person.setPersonType(PersonType.JURIDIC.getCode());
		lstIssuerDocumentType=documentValidatorRegister.getDocumentTypeResult(person);
	}
	
	/**
	 * Load Issuer Attachment Type National.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadIssuerAttachmentTypeNational()throws ServiceException
	{
		ParameterTableTO paramFilter = new ParameterTableTO();
		paramFilter.setMasterTableFk(MasterTableType.ISSUER_FILE_NATIONAL_TYPE.getCode());
		paramFilter.setState(ParameterTableStateType.REGISTERED.getCode());
		setLstIssuerAttachmentType(generalParametersFacade.getListParameterTableServiceBean(paramFilter));	
	}
	
	/**
	 * Load Issuer Attachment Type Foreign.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadIssuerAttachmentTypeForeign()throws ServiceException
	{
		ParameterTableTO paramFilter = new ParameterTableTO();
		paramFilter.setMasterTableFk(MasterTableType.ISSUER_FILE_FOREIGN_TYPE.getCode());
		paramFilter.setState(ParameterTableStateType.REGISTERED.getCode());
		setLstIssuerAttachmentType(generalParametersFacade.getListParameterTableServiceBean(paramFilter));
	}
	
	/**
	 * Load Issuer Attachment Type Differentiated.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadIssuerAttachmentTypeDifferentiated()throws ServiceException
	{
		ParameterTableTO paramFilter = new ParameterTableTO();
		paramFilter.setMasterTableFk(MasterTableType.ISSUER_FILE_DIFFERENTIATED_TYPE.getCode());
		paramFilter.setState(ParameterTableStateType.REGISTERED.getCode());
		setLstIssuerAttachmentType(generalParametersFacade.getListParameterTableServiceBean(paramFilter));
	}	
	
	/**
	 * Load issuer location info.
	 */
	private void loadIssuerLocationInfo(){
		try{
			hideDialogsListener(null);			
			
			if(issuer.getGeographicLocation()!=null){
				
				if(issuer.getGeographicLocation().getIdGeographicLocationPk()!=null){
			
				GeographicLocationTO filter = new GeographicLocationTO();
				filter.setState(GeographicLocationStateType.REGISTERED.getCode());
			
				filter.setGeographicLocationType(GeographicLocationType.DEPARTMENT.getCode());			
				filter.setIdLocationReferenceFk(issuer.getGeographicLocation().getIdGeographicLocationPk());			
				//List The departament
				lstDepartments = generalParametersFacade.getListGeographicLocationServiceFacade(filter);			
			
				filter.setGeographicLocationType(GeographicLocationType.PROVINCE.getCode());			
				filter.setIdLocationReferenceFk(issuer.getDepartment());			
				//List The provinces
				lstProvinces = generalParametersFacade.getListGeographicLocationServiceFacade(filter);			
			
				filter.setGeographicLocationType(GeographicLocationType.DISTRICT.getCode());			
				filter.setIdLocationReferenceFk(issuer.getProvince());			
				lstDistricts = generalParametersFacade.getListGeographicLocationServiceFacade(filter);
				}
			}
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change selected province.
	 */
	@LoggerAuditWeb
	public void changeSelectedProvince(){
		try{
			hideDialogsListener(null);
			GeographicLocationTO filter = new GeographicLocationTO();
			filter.setGeographicLocationType(GeographicLocationType.DISTRICT.getCode());
			filter.setState(GeographicLocationStateType.REGISTERED.getCode());
			filter.setIdLocationReferenceFk(issuer.getProvince());
			
			lstDistricts = generalParametersFacade.getListGeographicLocationServiceFacade(filter);
			issuer.setDistrict(GeneralConstants.NEGATIVE_ONE_INTEGER);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	/**
	 * Validate resolution number.
	 */
	@LoggerAuditWeb
	public void validateResolutionNumber(){
		try {
			hideDialogsListener(null);
			
			if(Validations.validateIsNotNullAndNotEmpty(issuer.getSuperintendentResolution())){
				if(issuerServiceFacade.validateExistsSuperintendentResolutionSivServiceFacade(issuer)){					
					
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_ISSUER_RESOLUTION_SIV_REPEATED));
					JSFUtilities.showSimpleValidationDialog();	
		
					issuer.setSuperintendentResolution(null);
				}
			}
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
     
    /**
     * Begin register listener.
     *
     * @return the string
     */
	@LoggerAuditWeb
    public void beginRegisterAction(){
    	
    	hideDialogsListener(null);
    	
    	int countValidationErrors = 0;
    	
    	if(Validations.validateIsNull(issuer.getGeographicLocation().getIdGeographicLocationPk()) ||
    			!Validations.validateIsPositiveNumber(issuer.getGeographicLocation().getIdGeographicLocationPk())){    	
    		countValidationErrors++;
    	} else {
    		
    		// issue 1063: el campo provincia esta removido para patrimonio, sin embargo en BBDD esta como notnull
        	// se coloca un valor por defcto al campo provicia
    		if (Validations.validateIsNotNull(issuer)
    				&& Validations.validateIsNotNull(issuer.getEconomicActivity()))
    			if (issuer.getEconomicActivity().equals(EconomicActivityType.PATRIMONIO_AUTONOMO_TITULARIZACION.getCode()))
    				issuer.setProvince(issuer.getGeographicLocation().getIdGeographicLocationPk());
    		
    		if(Validations.validateIsNull(issuer.getDocumentType()) ||
        			!Validations.validateIsPositiveNumber(issuer.getDocumentType())){        		
        		countValidationErrors++;
        	}
        	
        	if(Validations.validateIsNullOrEmpty(issuer.getDocumentNumber())){        		
        		countValidationErrors++;
        	}
        	
        	if(Validations.validateIsNullOrEmpty(issuer.getBusinessName())){        		
        		countValidationErrors++;
        	}
    	}    	    	    	    	
    	
    	if(Validations.validateIsNullOrEmpty(issuer.getOfferType())){    		
    		countValidationErrors++;
    	}
    	
    	if(Validations.validateIsNullOrEmpty(issuer.getMnemonic())){    		
    		countValidationErrors++;
    	}
    	
    	if(Validations.validateIsNull(issuer.getEconomicSector()) ||
    			!Validations.validateIsPositiveNumber(issuer.getEconomicSector())){    	
    		countValidationErrors++;
    	}
    	
    	if(Validations.validateIsNull(issuer.getEconomicActivity()) ||
    			!Validations.validateIsPositiveNumber(issuer.getEconomicActivity())){    		
    		countValidationErrors++;
    	}
    	
    	
    	if(Validations.validateIsNullOrNotPositive(issuer.getSocietyType()) && !indFICPATEconomicActivity){    		
    		countValidationErrors++;
    	}
    	
    	if(Validations.validateIsNullOrEmpty(issuer.getSuperResolutionDate())){    		
    	   		countValidationErrors++;
    	}
    	
    	if(Validations.validateIsNullOrEmpty(issuer.getSuperintendentResolution())){    		
    		
    		countValidationErrors++;
    	}
    	
    	if(indIssuerDepositaryContract){
    		if(Validations.validateIsNullOrEmpty(issuer.getContractDate())){        		
        		countValidationErrors++;
        	}
    		
    		if(Validations.validateIsNullOrEmpty(issuer.getClosingServiceDate())){        		
        		countValidationErrors++;
        	}
    	}
    	
    	if(indInscriptionStockExchange) {
    		if(Validations.validateIsNullOrEmpty(issuer.getEnrolledStockExchDate())){        	
        		countValidationErrors++;
        	}
    	}
    	
    	
    	if(Validations.validateIsNull(issuer.getDepartment()) ||
    			!Validations.validateIsPositiveNumber(issuer.getDepartment())){    		
    		countValidationErrors++;
    	}
    	
    	if(Validations.validateIsNull(issuer.getProvince()) ||
    			!Validations.validateIsPositiveNumber(issuer.getProvince())){    		
    		countValidationErrors++;
    	}
    	
    	if(Validations.validateIsNullOrEmpty(issuer.getLegalAddress())){    	
    		countValidationErrors++;
    	}
    	
    	if(Validations.validateIsNullOrEmpty(issuer.getCurrency()));
    	else
    	{
    		if(Validations.validateIsNotNullAndNotEmpty(issuer.getSocialCapital()) && Validations.validateIsNotNullAndPositive(issuer.getSocialCapital().longValue()));
          	else{        		
        		countValidationErrors++;
        	}
    	}      	
      	
    	if(countValidationErrors > 0) {    		
    		return;
    	}
    	
    	//Validate if at least one Legal Representative
    	if(Validations.validateIsNull(legalRepresentativeHelperBean.getLegalRepresentativeHistory()) || 
    			Validations.validateIsNullOrNotPositive(legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size())){  
    		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
    				PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_ISSUER_NO_LEGAL_REPRESENTATIVE));
    		JSFUtilities.showSimpleValidationDialog();
 
    		return;
    	}    	
    	
    	if(issuer.getIndSirtexNegotiate()==null)
    		issuer.setIndSirtexNegotiate(new Integer(0));
    	
    	//Validations of Issuer files

    	 
    	if(Validations.validateListIsNullOrEmpty(lstIssuerFiles)){
    		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
    				PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_ISSUER_FILE_NO_COMPLETE));
    		JSFUtilities.showSimpleValidationDialog();
    		return;
    	}
    	/*Issue 1231-Para esto, se definió que solo sea necesario adjuntar un documento como obligatorio, 
    	 * pero que a su vez sea posible adjuntar mas de uno.
    	 * 
    	*/
    	
		showMessageOnDialog(PropertiesConstants.LBL_CONFIRM_HEADER_RECORD,null,PropertiesConstants.LBL_CONFIRM_ISSUER_REGISTER,null);
		JSFUtilities.showComponent("cnfIssuerRegister");
		
    }
	
	/**
	 * Set List Object File InIssuer.
	 */
    public void setListObjectFileInIssuer()
    {
    	if(Validations.validateListIsNotNullAndNotEmpty(lstIssuerFiles)){
 	       issuer.setIssuerFiles(lstIssuerFiles);
	 	} else {
	 		issuer.setIssuerFiles(null);
	 	}
    }
    
    /**
     * Set Indicator Save Issuer.
     */
    public void setIndicatorSaveIssuer()
    {
    	issuer.setIndStockExchEnrolled(indInscriptionStockExchange ? BooleanType.YES.getCode() : BooleanType.NO.getCode());
    	issuer.setIndContractDepository(indIssuerDepositaryContract ? BooleanType.YES.getCode() : BooleanType.NO.getCode());
    	issuer.setIndDifferentiated(indIssuerDifferentiated ? BooleanType.YES.getCode() : BooleanType.NO.getCode());
    }
    
    /**
     * Validate Holder And Set File.
     */
    public void validateHolderAndSetFile()
    {
    	if(issuer.isIndHolderWillBeCreated()){
    		loadListHolderFileTypeParameter();
    		if(Validations.validateListIsNotNullAndNotEmpty(lstHolderFiles)){
    			issuer.getHolder().setHolderFile(lstHolderFiles);
    		}
    	}
    }
    
    /**
     * Validate Currency Issuer.
     */
    public void validateCurrencyIssuer()
    {
    	if(Validations.validateIsNullOrNotPositive(issuer.getCurrency())){
    		issuer.setCurrency(null);
    		issuer.setSocialCapital(null);
    	}
    }
    
    /**
     * Process Notification Save Issuer.
     */
    public void processNotificationSaveIssuer()
    {
    	//Sending notification
		BusinessProcess businessProcessNotification = new BusinessProcess();					
			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.ISSUER_REGISTRATION.getCode());
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
					businessProcessNotification,issuer.getIdIssuerPk(), null);
    }
    
    /**
     * View Message Issuer Register.
     */
    public void viewMessageIssuerRegister()
    {
    	if(issuer.isIndHolderWillBeCreated()) {
			Object[] arrBodyData = {String.valueOf(issuer.getMnemonic()), String.valueOf(issuer.getHolder().getIdHolderPk())};
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.LBL_SUCCESS_ISSUER_AND_HOLDER_REGISTER,arrBodyData));
		} else {
			Object[] arrBodyData = {String.valueOf(issuer.getMnemonic())};
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.LBL_SUCCESS_ISSUER_REGISTER,arrBodyData));
		}
    	JSFUtilities.showComponent("cnfEndTransactionIssuer");
    	cleanDataModel();		
		validateUserAccountSession();		
    }
    /**
     * Save register action.
     *
     */
	@LoggerAuditWeb
    public void saveRegisterListener(){    	
    	try {    		
    		JSFUtilities.executeJavascriptFunction("cnfwIssuerRegister.hide()");  
    		setListObjectFileInIssuer();
    		setIndicatorSaveIssuer();
    		validateHolderAndSetFile();
    		validateCurrencyIssuer();
    		
    		if(!Validations.validateIsNotNullAndNotEmpty(issuer.getIdIssuerPk())) {
    			//Registro de nuevo issuer
    			if(issuerServiceFacade.registryIssuerOnCascadeServiceFacade(issuer,legalRepresentativeHelperBean.getLstLegalRepresentativeHistory())){
        			//issuerServiceFacade.createSequenceDinamic(issuer.getMnemonic());
        			//processNotificationSaveIssuer();
        			issuer.setIndHolderWillBeCreated(false);
        			viewMessageIssuerRegister();    			
        			setViewOperationType(ViewOperationsType.CONSULT.getCode());
        		}    
        		else
        		{
        			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
     						, PropertiesUtilities.getMessage(PropertiesConstants.ISSUER_ERROR_YES_FOUND));	    			
        			 JSFUtilities.showComponent("cnfEndTransactionIssuer");
        		    	cleanDataModel();		
        				validateUserAccountSession();	
        		}
    		}else {
    			//Modificacion de issuer
    			issuer.setIndHolderWillBeCreated(true);
    			if(issuerServiceFacade.modifyIssuerOnCascadeServiceFacade(issuer,legalRepresentativeHelperBean.getLstLegalRepresentativeHistory())){
        			//issuerServiceFacade.createSequenceDinamic(issuer.getMnemonic());
        			//processNotificationSaveIssuer();
        			issuer.setIndHolderWillBeCreated(false);
        			
        			if(issuer.isIndHolderWillBeCreated()) {
        				Object[] arrBodyData = {String.valueOf(issuer.getMnemonic()), String.valueOf(issuer.getHolder().getIdHolderPk())};
        				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
        						PropertiesUtilities.getMessage(PropertiesConstants.LBL_SUCCESS_ISSUER_AND_HOLDER_MODIFY,arrBodyData));
        			} else {
        				Object[] arrBodyData = {String.valueOf(issuer.getMnemonic())};
        				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
        						PropertiesUtilities.getMessage(PropertiesConstants.LBL_SUCCESS_ISSUER_MODIFY,arrBodyData));
        			}
        	    	JSFUtilities.showComponent("cnfEndTransactionIssuer");
        	    	cleanDataModel();		
        			validateUserAccountSession();
        			
        			setViewOperationType(ViewOperationsType.CONSULT.getCode());
        		}    
        		else
        		{
        			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
     						, PropertiesUtilities.getMessage(PropertiesConstants.ISSUER_ERROR_YES_FOUND));	    			
        			 JSFUtilities.showComponent("cnfEndTransactionIssuer");
        		    	cleanDataModel();		
        				validateUserAccountSession();	
        		}
    			
    		}
    		
    		
		} catch (ServiceException e) {
			cleanSearch();
			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
			JSFUtilities.executeJavascriptFunction("cnfEndTransaction.show()");			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}    	
    }
	
	/**
	 * Load list holder file type parameter.
	 */
	private void loadListHolderFileTypeParameter(){
		try {

		Document pdfDocument;
		ByteArrayOutputStream byteArrayOutputStream;
		HolderFile holderFile;
		StringBuilder sbFileName;
		String fileNamePrefix = PropertiesUtilities.getMessage(PropertiesConstants.AMD_ISSUER_HOLDER_DOCUMENT_DEFAULT_NAME_PREFIX);
		String fileExtension = PropertiesUtilities.getMessage(PropertiesConstants.AMD_ISSUER_HOLDER_DOCUMENT_DEFAULT_EXTENSION);
		String contentFile = PropertiesUtilities.getMessage(PropertiesConstants.AMD_ISSUER_HOLDER_DOCUMENT_DEFAULT_CONTENT);
		
		loadHolderFileTypeParameter(MasterTableType.BOLIV_OR_FOREIGN_RESIDENT_DOC_HOLDER_JUR.getCode());
		
		lstHolderFiles = new ArrayList<HolderFile>();
		
		for (int i = 0 ; i<lsttHolderFileTypes.size(); i++) {
			pdfDocument = new Document();

			// Se crea el OutputStream para el fichero donde queremos dejar el pdf.
			byteArrayOutputStream = new ByteArrayOutputStream();
			
			PdfWriter.getInstance(pdfDocument, byteArrayOutputStream);

			pdfDocument.open();  
			pdfDocument.setPageSize(PageSize.A4);  
		  
			pdfDocument.add(new Paragraph(contentFile));		
			
			byteArrayOutputStream.flush();			
			byteArrayOutputStream.close();
			pdfDocument.close();
			
			sbFileName = new StringBuilder();
			sbFileName.append(fileNamePrefix);
			sbFileName.append(i+1);
			sbFileName.append(fileExtension);
			
			holderFile = new HolderFile();			
			holderFile.setFilename(sbFileName.toString());
			holderFile.setFileHolder(byteArrayOutputStream.toByteArray());			
			holderFile.setDocumentType(lsttHolderFileTypes.get(i).getParameterTablePk());
			holderFile.setDescription(lsttHolderFileTypes.get(i).getParameterName());			
			
			lstHolderFiles.add(holderFile);
			
		}
		
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		
	}
	
	/**
	 * Load holder file type parameter.
	 *
	 * @param masterTableTypeCode the master table type code
	 */
	private void loadHolderFileTypeParameter(Integer masterTableTypeCode){
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(masterTableTypeCode);
			
			lsttHolderFileTypes = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Load cbo lst credit rating scales.
	 */
	private void loadCboLstCreditRatingScales(){
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.CREDIT_RATING_SCALES_ISSUER.getCode());
			parameterTableTO.setOrderbyParameterTableCd(GeneralConstants.ONE_VALUE_INTEGER.intValue());
			lstCboCreditRatingScales = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);

			for (ParameterTable param : lstCboCreditRatingScales) {
				getParametersTableMap().put(param.getParameterTablePk(), param.getParameterName());
			}
			
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Value change cmb document issuer.
	 */
	public void valueChangeCmbDocumentIssuer(){
		setIssuerFileNameDisplay(null);
	}
	
	/**
	 * Document attach issuer file.
	 *
	 * @param event the event
	 */
	public void documentAttachIssuerFile(FileUploadEvent event){
		
		if(Validations.validateIsNotNullAndPositive(attachIssuerFileSelected)){		
			String fDisplayName=fUploadValidateFile(event.getFile(),null, getfUploadFileDocumentsSize(),getfUploadFileDocumentsTypes());
			 if(fDisplayName!=null){
				 issuerFileNameDisplay=fDisplayName;
			 }
			 for(int i=0;i<lstIssuerAttachmentType.size();i++){
				if(lstIssuerAttachmentType.get(i).getParameterTablePk().equals(attachIssuerFileSelected)){					
				    	IssuerFile issuerFile = new IssuerFile();
				    	issuerFile.setRequestFileType(RequestIssuerFileType.REGISTER.getCode());
				    	issuerFile.setDocumentType(lstIssuerAttachmentType.get(i).getParameterTablePk());
					    issuerFile.setDescription(lstIssuerAttachmentType.get(i).getParameterName());
				    	issuerFile.setFilename(event.getFile().getFileName());
				    	issuerFile.setDocumentFile(event.getFile().getContents());		    	
				    	issuerFile.setStateFile(IssuerFileStateType.REGISTERED.getCode());
				    	issuerFile.setIssuer(issuer);					
				    	insertOrReplaceIssuerFileItem(issuerFile);
				}
			}
		}		
	}
	
	/**
	 * Insert or replace issuer file item.
	 *
	 * @param issuerFile the issuer file
	 */
	public void insertOrReplaceIssuerFileItem(IssuerFile issuerFile){
		
		boolean flag = false;
		
		int listSize = lstIssuerFiles.size();
		if(listSize>0){
			for(int i=0;i<listSize;i++)
			{
			
				if(lstIssuerFiles.get(i).getDocumentType().equals(issuerFile.getDocumentType())){
					lstIssuerFiles.remove(i);
					lstIssuerFiles.add(i,issuerFile);
					attachIssuerFileSelected = GeneralConstants.NEGATIVE_ONE_INTEGER;
					
					flag = true;
				}
			}
			if(!flag){
				lstIssuerFiles.add(issuerFile);
				attachIssuerFileSelected = GeneralConstants.NEGATIVE_ONE_INTEGER;
			}
		}
		else{
			lstIssuerFiles.add(issuerFile);
			attachIssuerFileSelected = GeneralConstants.NEGATIVE_ONE_INTEGER;
		}
		
	}
    
    
    
    /**
     * Find issuer information.
     *
     * @param filter the filter
     * @return the participant
     * @throws ServiceException the service exception
     */
    private Issuer findIssuerInformation(Issuer filter) throws ServiceException{
    	Issuer issuerResult = issuerServiceFacade.findIssuerByFiltersServiceFacade(filter);    	
    	if(Validations.validateIsNotNull(issuerResult)){    		
    		issuer = issuerResult;    		
    		ParameterTableTO filterParameterTable = new ParameterTableTO();
    		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
    		filterParameterTable.setMasterTableFk(MasterTableType.ISSUER_STATE.getCode());
    		lstIssuerState = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);   		
    		loadCombosIssuerManagement();    		
    		loadIssuerLocationInfo();    	 		
    		changeSelectedEconomicSector();   		
    		return issuerResult;
    	} else {
    		return null;
    	}
    }   	
    
		/**
		 * Load Departments.
		 *
		 * @param referenceFk the reference fk
		 * @return List Departments
		 * @throws ServiceException the service exception
		 */
	public List<GeographicLocation> getLstDepartments(Integer referenceFk) throws ServiceException{		
		GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
		geographicLocationTO.setGeographicLocationType(GeographicLocationType.DEPARTMENT.getCode());
		geographicLocationTO.setState(GeographicLocationStateType.REGISTERED.getCode());
		geographicLocationTO.setIdLocationReferenceFk(referenceFk);
		return  generalParametersFacade.getListGeographicLocationServiceFacade(geographicLocationTO);	
	}
	
	/**
	 * Load Provinces.
	 *
	 * @param referenceFk the reference fk
	 * @return List Provinces
	 * @throws ServiceException the service exception
	 */
	public List<GeographicLocation> getLstProvinces(Integer referenceFk) throws ServiceException{		
		GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
		geographicLocationTO.setGeographicLocationType(GeographicLocationType.PROVINCE.getCode());
		geographicLocationTO.setState(GeographicLocationStateType.REGISTERED.getCode());
		geographicLocationTO.setIdLocationReferenceFk(referenceFk);
		return  generalParametersFacade.getListGeographicLocationServiceFacade(geographicLocationTO);	
	}
	
	/**
	 * Load Districts.
	 *
	 * @param referenceFk the reference fk
	 * @return List Districts
	 * @throws ServiceException the service exception
	 */
	public List<GeographicLocation> getLstDistricts(Integer referenceFk) throws ServiceException{		
		GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
		geographicLocationTO.setGeographicLocationType(GeographicLocationType.DISTRICT.getCode());
		geographicLocationTO.setState(GeographicLocationStateType.REGISTERED.getCode());
		geographicLocationTO.setIdLocationReferenceFk(referenceFk);
		return  generalParametersFacade.getListGeographicLocationServiceFacade(geographicLocationTO);	
	}
	
	/**
	 * Set Indicator Detail Issuer.
	 */
	public void setIndicatorDetailIssuer()
	{
		indInscriptionStockExchange = BooleanType.YES.getCode().equals(issuer.getIndStockExchEnrolled());
		indIssuerDepositaryContract = BooleanType.YES.getCode().equals(issuer.getIndContractDepository());
		indIssuerDifferentiated = BooleanType.YES.getCode().equals(issuer.getIndDifferentiated());
		indIssuerSIRTEXnegociate = BooleanType.YES.getCode().equals(issuer.getIndSirtexNegotiate());
	}
	
	/**
	 * Set Detail Issuer File.
	 *
	 * @param lstIssuerFiles List File
	 * @throws Exception Exception
	 */
	public void setDetailIssuerFile(List<IssuerFile> lstIssuerFiles)throws Exception
	{
		InputStream inputStream;
		StreamedContent streamedContentFile;
		for (IssuerFile issuerFile : lstIssuerFiles) {
			/*set the transient attribute streamedContentFile in order to shown 
			 * the content of this file on participant detail.
			 */
			inputStream = new ByteArrayInputStream(issuerFile.getDocumentFile());
			streamedContentFile = new DefaultStreamedContent(inputStream, null, issuerFile.getFilename());
			setStreamedContentFile(streamedContentFile);
			inputStream.close();
		}
	}
	
	/**
	 * Set Legal Representative History.
	 *
	 * @param lstRepresentedEntity List Represented Entity
	 * @throws ServiceException  Service Exception
	 */
	public void setLegalRepresentativeHistory(List<RepresentedEntity> lstRepresentedEntity) throws ServiceException
	{
		lstLegalRepresentativeHistory = new ArrayList<LegalRepresentativeHistory>();
		LegalRepresentativeHistory objLegalRepresentativeHistory;
		for (RepresentedEntity objRepresentedEntity : lstRepresentedEntity) {
			objLegalRepresentativeHistory = new LegalRepresentativeHistory();
			objLegalRepresentativeHistory.setIdRepresentativeHistoryPk(objRepresentedEntity.getLegalRepresentative().getIdLegalRepresentativePk());
			objLegalRepresentativeHistory.setRepresentativeClass(objRepresentedEntity.getRepresentativeClass());
			objLegalRepresentativeHistory.setBirthDate(objRepresentedEntity.getLegalRepresentative().getBirthDate());
			objLegalRepresentativeHistory.setDocumentNumber(objRepresentedEntity.getLegalRepresentative().getDocumentNumber());
			objLegalRepresentativeHistory.setDocumentType(objRepresentedEntity.getLegalRepresentative().getDocumentType());
			objLegalRepresentativeHistory.setDocumentSource(objRepresentedEntity.getLegalRepresentative().getDocumentSource());
			objLegalRepresentativeHistory.setEconomicActivity(objRepresentedEntity.getLegalRepresentative().getEconomicActivity());
			objLegalRepresentativeHistory.setEconomicSector(objRepresentedEntity.getLegalRepresentative().getEconomicSector());
			objLegalRepresentativeHistory.setEmail(objRepresentedEntity.getLegalRepresentative().getEmail());
			objLegalRepresentativeHistory.setFaxNumber(objRepresentedEntity.getLegalRepresentative().getFaxNumber());
			objLegalRepresentativeHistory.setFirstLastName(objRepresentedEntity.getLegalRepresentative().getFirstLastName());
			objLegalRepresentativeHistory.setFullName(objRepresentedEntity.getLegalRepresentative().getFullName());
			objLegalRepresentativeHistory.setHomePhoneNumber(objRepresentedEntity.getLegalRepresentative().getHomePhoneNumber());
			objLegalRepresentativeHistory.setIndResidence(objRepresentedEntity.getLegalRepresentative().getIndResidence());
			objLegalRepresentativeHistory.setLegalAddress(objRepresentedEntity.getLegalRepresentative().getLegalAddress());
			objLegalRepresentativeHistory.setLegalDistrict(objRepresentedEntity.getLegalRepresentative().getLegalDistrict());
			objLegalRepresentativeHistory.setLegalProvince(objRepresentedEntity.getLegalRepresentative().getLegalProvince());
			objLegalRepresentativeHistory.setLegalDepartment(objRepresentedEntity.getLegalRepresentative().getLegalDepartment());
			objLegalRepresentativeHistory.setLegalResidenceCountry(objRepresentedEntity.getLegalRepresentative().getLegalResidenceCountry());
			objLegalRepresentativeHistory.setMobileNumber(objRepresentedEntity.getLegalRepresentative().getMobileNumber());
			objLegalRepresentativeHistory.setName(objRepresentedEntity.getLegalRepresentative().getName());
			objLegalRepresentativeHistory.setNationality(objRepresentedEntity.getLegalRepresentative().getNationality());
			objLegalRepresentativeHistory.setOfficePhoneNumber(objRepresentedEntity.getLegalRepresentative().getOfficePhoneNumber());
			objLegalRepresentativeHistory.setPersonType(objRepresentedEntity.getLegalRepresentative().getPersonType());
			objLegalRepresentativeHistory.setPostalAddress(objRepresentedEntity.getLegalRepresentative().getPostalAddress());
			objLegalRepresentativeHistory.setPostalDepartment(objRepresentedEntity.getLegalRepresentative().getPostalDepartment());
			objLegalRepresentativeHistory.setPostalDistrict(objRepresentedEntity.getLegalRepresentative().getPostalDistrict());
			objLegalRepresentativeHistory.setPostalProvince(objRepresentedEntity.getLegalRepresentative().getPostalProvince());
			objLegalRepresentativeHistory.setPostalResidenceCountry(objRepresentedEntity.getLegalRepresentative().getPostalResidenceCountry());
			objLegalRepresentativeHistory.setSecondDocumentNumber(objRepresentedEntity.getLegalRepresentative().getSecondDocumentNumber());
			objLegalRepresentativeHistory.setSecondDocumentType(objRepresentedEntity.getLegalRepresentative().getSecondDocumentType());
			objLegalRepresentativeHistory.setSecondLastName(objRepresentedEntity.getLegalRepresentative().getSecondLastName());
			objLegalRepresentativeHistory.setSecondNationality(objRepresentedEntity.getLegalRepresentative().getSecondNationality());
			objLegalRepresentativeHistory.setSex(objRepresentedEntity.getLegalRepresentative().getSex());
			objLegalRepresentativeHistory.setStateRepreHistory(LegalRepresentativeStateType.REGISTERED.getCode());
			
			PepPerson pepPersonRepresentative = issuerServiceFacade.findPepPersonByLegalRepresentativeServiceFacade(objRepresentedEntity.getLegalRepresentative());
			if(Validations.validateIsNotNull(pepPersonRepresentative)) {
				objLegalRepresentativeHistory.setIndPEP(BooleanType.YES.getCode());
				objLegalRepresentativeHistory.setCategory(pepPersonRepresentative.getCategory());
				objLegalRepresentativeHistory.setRole(pepPersonRepresentative.getRole());
				objLegalRepresentativeHistory.setBeginningPeriod(pepPersonRepresentative.getBeginingPeriod());
				objLegalRepresentativeHistory.setEndingPeriod(pepPersonRepresentative.getEndingPeriod());
			} else {
				objLegalRepresentativeHistory.setIndPEP(BooleanType.NO.getCode());
			}			
			
			if(Validations.validateIsNotNullAndPositive(objLegalRepresentativeHistory.getIdRepresentativeHistoryPk())
					&& Validations.validateListIsNullOrEmpty(objLegalRepresentativeHistory.getRepresenteFileHistory())){
				LegalRepresentative legalRepresentative = new LegalRepresentative();
				legalRepresentative.setIdLegalRepresentativePk(objLegalRepresentativeHistory.getIdRepresentativeHistoryPk());
				List<LegalRepresentativeFile> representativeFiles = 
						issuerServiceFacade.getListLegalRepresentativeFileServiceFacade(legalRepresentative);
				lstRepresentativeFileHistory = new ArrayList<RepresentativeFileHistory>();
				
				RepresentativeFileHistory representativeFileHistory;
				for (LegalRepresentativeFile legalRepresentativeFile : representativeFiles) {
					representativeFileHistory = new RepresentativeFileHistory();
					representativeFileHistory.setDocumentType(legalRepresentativeFile.getDocumentType());
					representativeFileHistory.setFilename(legalRepresentativeFile.getFilename());
					representativeFileHistory.setFileRepresentative( legalRepresentativeFile.getFileLegalRepre() );
					representativeFileHistory.setDescription(legalRepresentativeFile.getDescription());
					representativeFileHistory.setFileRepresentative(legalRepresentativeFile.getFileLegalRepre());	
					representativeFileHistory.setLegalRepresentativeHistory(objLegalRepresentativeHistory);
					lstRepresentativeFileHistory.add(representativeFileHistory);
				}
				
				objLegalRepresentativeHistory.setRepresenteFileHistory(lstRepresentativeFileHistory);
				
			} else {
				lstRepresentativeFileHistory = objLegalRepresentativeHistory.getRepresenteFileHistory();
			}		
			
			LegalRepresentativeTO legalRepresentativeTO = new LegalRepresentativeTO();
			legalRepresentativeTO.setViewOperationType(ViewOperationsType.DETAIL.getCode());
			legalRepresentativeTO.setFlagIssuerOrParticipant(BooleanType.YES.getBooleanValue());
			legalRepresentativeHelperBean.setLegalRepresentativeTO(legalRepresentativeTO);
			lstLegalRepresentativeHistory.add(objLegalRepresentativeHistory);
			legalRepresentativeHelperBean.setLstLegalRepresentativeHistory(lstLegalRepresentativeHistory);
			
		}
	}
	/**
	 * Detail participant.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String detailIssuer(){
		try{
			setViewOperationType(ViewOperationsType.DETAIL.getCode());
			Issuer issuerFilter = new Issuer();
	    	issuerFilter.setIdIssuerPk(idIssuerPk);    
	    	Issuer issuerResult = findIssuerInformation(issuerFilter);			
	    	if(Validations.validateIsNotNull(issuerResult)) {
	    		if(Validations.validateIsNotNull(issuerResult.getEconomicActivity())){
	    			buildInvestorTypeByEconAcSelected(issuerResult.getEconomicActivity());
	    		}else{
	    			buildInvestorTypeByEconAcSelected(null);
	    		}
	    		setIndicatorDetailIssuer();				
	    		lstIssuerFiles = issuerServiceFacade.getListIssuerFilesServiceFacade(issuerFilter);	    			    		
	    		setDetailIssuerFile(lstIssuerFiles);	    			    		
	    		lstRepresentedEntity = issuerServiceFacade.getListRepresentedEntityByIssuerServiceFacade(issuerFilter);
	    		issuer = issuerResult;	    		
	    		if (Validations.validateListIsNotNullAndNotEmpty(lstRepresentedEntity)) 
	    			setLegalRepresentativeHistory(lstRepresentedEntity);					    		
	    		return WINDOW_DETAIL_ISSUER;
	    	} else {
	    		return null;
	    	}				
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
			return null;
		}
	}

	/**
	 * Change resolution ASFI date.
	 *
	 * @param event the event
	 */
	public void changeResolutionSivDate(SelectEvent event){
		//Depositary contract date must be grater than super resolution ASFI date and less or equals than current date 
		minDepositaryContractDate = CommonsUtilities.addDate(issuer.getSuperResolutionDate(), 1);			
		
		if(Validations.validateIsNotNull(issuer.getContractDate()) &&
				!issuer.getContractDate().after(issuer.getSuperResolutionDate())){
			issuer.setContractDate(null);
			issuer.setClosingServiceDate(null);
		}
		
		//Inscription Stock Exchange Date must be grater than super resolution SIV date and less than current date 
		minInscriptionStockExchDate =  CommonsUtilities.addDate(issuer.getSuperResolutionDate(), 1);
		
		if(Validations.validateIsNotNull(issuer.getEnrolledStockExchDate()) &&
				!issuer.getEnrolledStockExchDate().after(issuer.getSuperResolutionDate())){
			issuer.setEnrolledStockExchDate(null);
		}
	}
	
	/**
	 * Check issuer depositary contract.
	 */
	public void checkIssuerDepositaryContract(){
			if(Validations.validateIsNotNull(issuer.getSuperResolutionDate())){
				minDepositaryContractDate = CommonsUtilities.addDate(issuer.getSuperResolutionDate(), 1);
			} else {
				minDepositaryContractDate = null;
			}
			
			issuer.setContractDate(null);
			issuer.setClosingServiceDate(null);
			JSFUtilities.resetComponent(":frmIssuerRegister:tabViewIssuer:calContractDate");
			JSFUtilities.resetComponent(":frmIssuerRegister:tabViewIssuer:calEffectiveDate");
						
	}
	
	/**
	 * Check inscription stock exchange.
	 */
	public void checkInscriptionStockExchange(){		 
		if(Validations.validateIsNotNull(issuer.getSuperResolutionDate())){
			minInscriptionStockExchDate = CommonsUtilities.addDate(issuer.getSuperResolutionDate(), 1);
		} else {
			minInscriptionStockExchDate = null;
		}
		
		issuer.setEnrolledStockExchDate(null);
		JSFUtilities.resetComponent(":frmIssuerRegister:tabViewIssuer:calInscriptionStockExchDate");
	}
	
	/**
	 * Check inscription stock exchange.
	 */
	public void checkIssuerSIRTEXnegociate(){		 
		
		//TODO: SIRTEX
		
	}
	
	/**
	 * Change selected currency.
	 */
	public void changeSelectedCurrency(){
		issuer.setSocialCapital(null);
		JSFUtilities.resetComponent(":frmIssuerRegister:tabViewIssuer:txtQuantity");
		JSFUtilities.resetComponent(":frmIssuerRegister:tabViewIssuer:msgQuantity");
	}		
	/**
	 * Validate address format.
	 */
	public void validateAddressFormat(){
		hideDialogsListener(null);
    	if(Validations.validateIsNotNullAndNotEmpty(issuer.getLegalAddress())){    		
    		if(!CommonsUtilities.matchAddress(issuer.getLegalAddress())){    		
    			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
    					PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_VALIDATION_ADDRESS_FORMAT));
    			JSFUtilities.showSimpleValidationDialog();    					
    			issuer.setLegalAddress(null);
    		}
    	}
	}
			
	/**
	 * Gets the lst sector location.
	 *
	 * @param referenceFk the reference fk
	 * @return the lst sector location
	 * @throws ServiceException the service exception
	 */
	public List<GeographicLocation> getLstSectorLocation(Integer referenceFk) throws ServiceException{

			GeographicLocationTO geographicLocationTO = new GeographicLocationTO();

			geographicLocationTO
					.setGeographicLocationType(GeographicLocationType.DISTRICT
							.getCode());
			geographicLocationTO
					.setState(GeographicLocationStateType.REGISTERED.getCode());
			geographicLocationTO.setIdLocationReferenceFk(referenceFk);

			return generalParametersFacade.getListGeographicLocationServiceFacade(geographicLocationTO);

	}	
	
	/**
	 * Click tab view.
	 *
	 * @return the string
	 */
	public String clickTabView()
	{
		
		return "";
	}
	
	/**
	 * Accept issuer differentiated.
	 */
	public void acceptIssuerDifferentiated(){
		try {
			hideDialogsListener(null);
			
			Iterator<LegalRepresentativeHistory> itrLegalRepresentativeHistory = legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().iterator();
			LegalRepresentativeHistory objLegalRepresentativeHistory = null;
		      while(itrLegalRepresentativeHistory.hasNext()) {
		    	  objLegalRepresentativeHistory = itrLegalRepresentativeHistory.next();	    	  
		    	  if(BooleanType.NO.getCode().equals(objLegalRepresentativeHistory.getIndResidence())){
		    		  itrLegalRepresentativeHistory.remove();
		    	  }
		      }
		      
		    indIssuerDifferentiated = Boolean.TRUE;
		    loadIssuerAttachmentTypeDifferentiated();
		    resetIssuerFiles();
		    
		    JSFUtilities.executeJavascriptFunction("cnfwDifferentiated.hide();");
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Reject issuer differentiated.
	 */
	public void rejectIssuerDifferentiated(){
		indIssuerDifferentiated = Boolean.FALSE;
		JSFUtilities.executeJavascriptFunction("cnfwDifferentiated.hide();");
	}
	
	/**
	 * Clean issuer registry.
	 */
	public void cleanIssuerRegistry(){
		hideDialogsListener(null);		
		initializeObjectsRegister();		
		initializeIndicators();		
		JSFUtilities.resetViewRoot();			
	}
	
	/**
	 * Removes the issuer file.
	 *
	 * @param issuerFile the issuer file
	 */
	public void removeIssuerFile(IssuerFile issuerFile) {
		lstIssuerFiles.remove(issuerFile);
		issuerFileNameDisplay = null;
		attachIssuerFileSelected = GeneralConstants.NEGATIVE_ONE_INTEGER;		
	}
	      
	/**
	 * Gets the lst countries.
	 *
	 * @return the lst countries
	 */
	public List<GeographicLocation> getLstCountries() {
		return lstCountries;
	}

	/**
	 * Sets the lst countries.
	 *
	 * @param lstCountries the new lst countries
	 */
	public void setLstCountries(List<GeographicLocation> lstCountries) {
		this.lstCountries = lstCountries;
	}

	/**
	 * Gets the lst provinces.
	 *
	 * @return the lst provinces
	 */
	public List<GeographicLocation> getLstProvinces() {
		return lstProvinces;
	}

	/**
	 * Sets the lst Departments.
	 *
	 * @param lstDepartments the new lst departments
	 */
	public void setLstDepartments(List<GeographicLocation> lstDepartments) {
		this.lstDepartments = lstDepartments;
	}
	
	/**
	 * Gets the lst Departments.
	 *
	 * @return the lst Departments
	 */
	public List<GeographicLocation> getLstDepartments() {
		return lstDepartments;
	}

	/**
	 * Sets the lst provinces.
	 *
	 * @param lstProvinces the new lst provinces
	 */
	public void setLstProvinces(List<GeographicLocation> lstProvinces) {
		this.lstProvinces = lstProvinces;
	}

	/**
	 * Gets the lst districts.
	 *
	 * @return the lst districts
	 */
	public List<GeographicLocation> getLstDistricts() {
		return lstDistricts;
	}

	/**
	 * Sets the lst districts.
	 *
	 * @param lstDistricts the new lst districts
	 */
	public void setLstDistricts(List<GeographicLocation> lstDistricts) {
		this.lstDistricts = lstDistricts;
	}
	
	/**
	 * Gets the lst economic sector.
	 *
	 * @return the lst economic sector
	 */
	public List<ParameterTable> getLstEconomicSector() {
		return lstEconomicSector;
	}

	/**
	 * Sets the lst economic sector.
	 *
	 * @param lstEconomicSector the new lst economic sector
	 */
	public void setLstEconomicSector(List<ParameterTable> lstEconomicSector) {
		this.lstEconomicSector = lstEconomicSector;
	}

	/**
	 * Gets the lst economic activity.
	 *
	 * @return the lst economic activity
	 */
	public List<ParameterTable> getLstEconomicActivity() {
		return lstEconomicActivity;
	}

	/**
	 * Sets the lst economic activity.
	 *
	 * @param lstEconomicActivity the new lst economic activity
	 */
	public void setLstEconomicActivity(List<ParameterTable> lstEconomicActivity) {
		this.lstEconomicActivity = lstEconomicActivity;
	}

	/**
	 * Checks if is ind participant national.
	 *
	 * @return true, if is ind participant national
	 */
	public boolean isIndIssuerNational() {
		return Validations.validateIsNotNull(issuer.getGeographicLocation().getIdGeographicLocationPk()) && 
				countryResidence.equals(issuer.getGeographicLocation().getIdGeographicLocationPk());
	}
	
	/**
	 * Checks if is ind participant foreign.
	 *
	 * @return true, if is ind participant foreign
	 */
	public boolean isIndIssuerForeign() {
		return Validations.validateIsNotNull(issuer.getGeographicLocation().getIdGeographicLocationPk()) &&
				issuer.getGeographicLocation().getIdGeographicLocationPk() > 0 &&
				!(countryResidence.equals(issuer.getGeographicLocation().getIdGeographicLocationPk()));
	}



	/**
	 * Gets the lst represented entity.
	 *
	 * @return the lst represented entity
	 */
	public List<RepresentedEntity> getLstRepresentedEntity() {
		return lstRepresentedEntity;
	}

	/**
	 * Sets the lst represented entity.
	 *
	 * @param lstRepresentedEntity the new lst represented entity
	 */
	public void setLstRepresentedEntity(List<RepresentedEntity> lstRepresentedEntity) {
		this.lstRepresentedEntity = lstRepresentedEntity;
	}

	/**
	 * Gets the lst legal representative.
	 *
	 * @return the lst legal representative
	 */
	public List<LegalRepresentative> getLstLegalRepresentative() {
		return lstLegalRepresentative;
	}

	/**
	 * Sets the lst legal representative.
	 *
	 * @param lstLegalRepresentative the new lst legal representative
	 */
	public void setLstLegalRepresentative(
			List<LegalRepresentative> lstLegalRepresentative) {
		this.lstLegalRepresentative = lstLegalRepresentative;
	}

	/**
	 * Gets the legal representative history.
	 *
	 * @return the legal representative history
	 */
	public LegalRepresentativeHistory getLegalRepresentativeHistory() {
		return legalRepresentativeHistory;
	}

	/**
	 * Sets the legal representative history.
	 *
	 * @param legalRepresentativeHistory the new legal representative history
	 */
	public void setLegalRepresentativeHistory(
			LegalRepresentativeHistory legalRepresentativeHistory) {
		this.legalRepresentativeHistory = legalRepresentativeHistory;
	}

	/**
	 * Gets the lst legal representative history.
	 *
	 * @return the lst legal representative history
	 */
	public List<LegalRepresentativeHistory> getLstLegalRepresentativeHistory() {
		return lstLegalRepresentativeHistory;
	}

	/**
	 * Sets the lst legal representative history.
	 *
	 * @param lstLegalRepresentativeHistory the new lst legal representative history
	 */
	public void setLstLegalRepresentativeHistory(
			List<LegalRepresentativeHistory> lstLegalRepresentativeHistory) {
		this.lstLegalRepresentativeHistory = lstLegalRepresentativeHistory;
	}

	/**
	 * Gets the pep person.
	 *
	 * @return the pep person
	 */
	public PepPerson getPepPerson() {
		return pepPerson;
	}

	/**
	 * Sets the pep person.
	 *
	 * @param pepPerson the new pep person
	 */
	public void setPepPerson(PepPerson pepPerson) {
		this.pepPerson = pepPerson;
	}

	/**
	 * Gets the lst representative class.
	 *
	 * @return the lst representative class
	 */
	public List<ParameterTable> getLstRepresentativeClass() {
		return lstRepresentativeClass;
	}

	/**
	 * Sets the lst representative class.
	 *
	 * @param lstRepresentativeClass the new lst representative class
	 */
	public void setLstRepresentativeClass(
			List<ParameterTable> lstRepresentativeClass) {
		this.lstRepresentativeClass = lstRepresentativeClass;
	}

	/**
	 * Gets the lst document type representative.
	 *
	 * @return the lst document type representative
	 */
	public List<ParameterTable> getLstDocumentTypeRepresentative() {
		return lstDocumentTypeRepresentative;
	}

	/**
	 * Sets the lst document type representative.
	 *
	 * @param lstDocumentTypeRepresentative the new lst document type representative
	 */
	public void setLstDocumentTypeRepresentative(
			List<ParameterTable> lstDocumentTypeRepresentative) {
		this.lstDocumentTypeRepresentative = lstDocumentTypeRepresentative;
	}

	/**
	 * Gets the lst economic activity representative.
	 *
	 * @return the lst economic activity representative
	 */
	public List<ParameterTable> getLstEconomicActivityRepresentative() {
		return lstEconomicActivityRepresentative;
	}

	/**
	 * Sets the lst economic activity representative.
	 *
	 * @param lstEconomicActivityRepresentative the new lst economic activity representative
	 */
	public void setLstEconomicActivityRepresentative(
			List<ParameterTable> lstEconomicActivityRepresentative) {
		this.lstEconomicActivityRepresentative = lstEconomicActivityRepresentative;
	}

	/**
	 * Gets the lst sex.
	 *
	 * @return the lst sex
	 */
	public List<ParameterTable> getLstSex() {
		return lstSex;
	}

	/**
	 * Sets the lst sex.
	 *
	 * @param lstSex the new lst sex
	 */
	public void setLstSex(List<ParameterTable> lstSex) {
		this.lstSex = lstSex;
	}

	/**
	 * Checks if is ind representative natural person.
	 *
	 * @return true, if is ind representative natural person
	 */
	public boolean isIndRepresentativeNaturalPerson() {
		return indRepresentativeNaturalPerson;
	}

	/**
	 * Sets the ind representative natural person.
	 *
	 * @param indRepresentativeNaturalPerson the new ind representative natural person
	 */
	public void setIndRepresentativeNaturalPerson(
			boolean indRepresentativeNaturalPerson) {
		this.indRepresentativeNaturalPerson = indRepresentativeNaturalPerson;
	}

	/**
	 * Checks if is ind representative juridic person.
	 *
	 * @return true, if is ind representative juridic person
	 */
	public boolean isIndRepresentativeJuridicPerson() {
		return indRepresentativeJuridicPerson;
	}

	/**
	 * Sets the ind representative juridic person.
	 *
	 * @param indRepresentativeJuridicPerson the new ind representative juridic person
	 */
	public void setIndRepresentativeJuridicPerson(
			boolean indRepresentativeJuridicPerson) {
		this.indRepresentativeJuridicPerson = indRepresentativeJuridicPerson;
	}

	/**
	 * Checks if is flag cmb representative class.
	 *
	 * @return true, if is flag cmb representative class
	 */
	public boolean isFlagCmbRepresentativeClass() {
		return flagCmbRepresentativeClass;
	}

	/**
	 * Sets the flag cmb representative class.
	 *
	 * @param flagCmbRepresentativeClass the new flag cmb representative class
	 */
	public void setFlagCmbRepresentativeClass(boolean flagCmbRepresentativeClass) {
		this.flagCmbRepresentativeClass = flagCmbRepresentativeClass;
	}

	/**
	 * Checks if is ind representative resident.
	 *
	 * @return true, if is ind representative resident
	 */
	public boolean isIndRepresentativeResident() {
		return indRepresentativeResident;
	}

	/**
	 * Sets the ind representative resident.
	 *
	 * @param indRepresentativeResident the new ind representative resident
	 */
	public void setIndRepresentativeResident(boolean indRepresentativeResident) {
		this.indRepresentativeResident = indRepresentativeResident;
	}

	/**
	 * Checks if is disabled country resident representative.
	 *
	 * @return true, if is disabled country resident representative
	 */
	public boolean isDisabledCountryResidentRepresentative() {
		return disabledCountryResidentRepresentative;
	}

	/**
	 * Sets the disabled country resident representative.
	 *
	 * @param disabledCountryResidentRepresentative the new disabled country resident representative
	 */
	public void setDisabledCountryResidentRepresentative(
			boolean disabledCountryResidentRepresentative) {
		this.disabledCountryResidentRepresentative = disabledCountryResidentRepresentative;
	}

	/**
	 * Checks if is ind representative is pep.
	 *
	 * @return true, if is ind representative is pep
	 */
	public boolean isIndRepresentativeIsPEP() {
		return indRepresentativeIsPEP;
	}

	/**
	 * Sets the ind representative is pep.
	 *
	 * @param indRepresentativeIsPEP the new ind representative is pep
	 */
	public void setIndRepresentativeIsPEP(boolean indRepresentativeIsPEP) {
		this.indRepresentativeIsPEP = indRepresentativeIsPEP;
	}

	/**
	 * Checks if is ind copy legal representative.
	 *
	 * @return true, if is ind copy legal representative
	 */
	public boolean isIndCopyLegalRepresentative() {
		return indCopyLegalRepresentative;
	}

	/**
	 * Sets the ind copy legal representative.
	 *
	 * @param indCopyLegalRepresentative the new ind copy legal representative
	 */
	public void setIndCopyLegalRepresentative(boolean indCopyLegalRepresentative) {
		this.indCopyLegalRepresentative = indCopyLegalRepresentative;
	}

	/**
	 * Gets the lst person type.
	 *
	 * @return the lst person type
	 */
	public List<ParameterTable> getLstPersonType() {
		return lstPersonType;
	}

	/**
	 * Sets the lst person type.
	 *
	 * @param lstPersonType the new lst person type
	 */
	public void setLstPersonType(List<ParameterTable> lstPersonType) {
		this.lstPersonType = lstPersonType;
	}

	/**
	 * Gets the lst boolean.
	 *
	 * @return the lst boolean
	 */
	public List<BooleanType> getLstBoolean() {
		return lstBoolean;
	}

	/**
	 * Sets the lst boolean.
	 *
	 * @param lstBoolean the new lst boolean
	 */
	public void setLstBoolean(List<BooleanType> lstBoolean) {
		this.lstBoolean = lstBoolean;
	}

	/**
	 * Gets the lst postal provinces representative.
	 *
	 * @return the lst postal provinces representative
	 */
	public List<GeographicLocation> getLstPostalProvincesRepresentative() {
		return lstPostalProvincesRepresentative;
	}

	/**
	 * Sets the lst postal provinces representative.
	 *
	 * @param lstPostalProvincesRepresentative the new lst postal provinces representative
	 */
	public void setLstPostalProvincesRepresentative(
			List<GeographicLocation> lstPostalProvincesRepresentative) {
		this.lstPostalProvincesRepresentative = lstPostalProvincesRepresentative;
	}

	/**
	 * Gets the lst postal districts representative.
	 *
	 * @return the lst postal districts representative
	 */
	public List<GeographicLocation> getLstPostalDistrictsRepresentative() {
		return lstPostalDistrictsRepresentative;
	}

	/**
	 * Sets the lst postal districts representative.
	 *
	 * @param lstPostalDistrictsRepresentative the new lst postal districts representative
	 */
	public void setLstPostalDistrictsRepresentative(
			List<GeographicLocation> lstPostalDistrictsRepresentative) {
		this.lstPostalDistrictsRepresentative = lstPostalDistrictsRepresentative;
	}

	/**
	 * Gets the lst legal provinces representative.
	 *
	 * @return the lst legal provinces representative
	 */
	public List<GeographicLocation> getLstLegalProvincesRepresentative() {
		return lstLegalProvincesRepresentative;
	}

	/**
	 * Sets the lst legal provinces representative.
	 *
	 * @param lstLegalProvincesRepresentative the new lst legal provinces representative
	 */
	public void setLstLegalProvincesRepresentative(
			List<GeographicLocation> lstLegalProvincesRepresentative) {
		this.lstLegalProvincesRepresentative = lstLegalProvincesRepresentative;
	}

	/**
	 * Gets the lst legal districts representative.
	 *
	 * @return the lst legal districts representative
	 */
	public List<GeographicLocation> getLstLegalDistrictsRepresentative() {
		return lstLegalDistrictsRepresentative;
	}

	/**
	 * Sets the lst legal districts representative.
	 *
	 * @param lstLegalDistrictsRepresentative the new lst legal districts representative
	 */
	public void setLstLegalDistrictsRepresentative(
			List<GeographicLocation> lstLegalDistrictsRepresentative) {
		this.lstLegalDistrictsRepresentative = lstLegalDistrictsRepresentative;
	}

	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}

	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	/**
	 * Gets the lst issuer document type.
	 *
	 * @return the lst issuer document type
	 */
	public List<ParameterTable> getLstIssuerDocumentType() {
		return lstIssuerDocumentType;
	}

	/**
	 * Sets the lst issuer document type.
	 *
	 * @param lstIssuerDocumentType the new lst issuer document type
	 */
	public void setLstIssuerDocumentType(List<ParameterTable> lstIssuerDocumentType) {
		this.lstIssuerDocumentType = lstIssuerDocumentType;
	}

	/**
	 * Gets the lst issuer document type for search.
	 *
	 * @return the lst issuer document type for search
	 */
	public List<ParameterTable> getLstIssuerDocumentTypeForSearch() {
		return lstIssuerDocumentTypeForSearch;
	}

	/**
	 * Sets the lst issuer document type for search.
	 *
	 * @param lstIssuerDocumentTypeForSearch the new lst issuer document type for search
	 */
	public void setLstIssuerDocumentTypeForSearch(
			List<ParameterTable> lstIssuerDocumentTypeForSearch) {
		this.lstIssuerDocumentTypeForSearch = lstIssuerDocumentTypeForSearch;
	}

	/**
	 * Gets the lst economic activity for search.
	 *
	 * @return the lst economic activity for search
	 */
	public List<ParameterTable> getLstEconomicActivityForSearch() {
		return lstEconomicActivityForSearch;
	}

	/**
	 * Sets the lst economic activity for search.
	 *
	 * @param lstEconomicActivityForSearch the new lst economic activity for search
	 */
	public void setLstEconomicActivityForSearch(
			List<ParameterTable> lstEconomicActivityForSearch) {
		this.lstEconomicActivityForSearch = lstEconomicActivityForSearch;
	}

	/**
	 * Gets the lst issuer state.
	 *
	 * @return the lst issuer state
	 */
	public List<ParameterTable> getLstIssuerState() {
		return lstIssuerState;
	}

	/**
	 * Sets the lst issuer state.
	 *
	 * @param lstIssuerState the new lst issuer state
	 */
	public void setLstIssuerState(List<ParameterTable> lstIssuerState) {
		this.lstIssuerState = lstIssuerState;
	}

	/**
	 * Gets the lst issuer state for search.
	 *
	 * @return the lst issuer state for search
	 */
	public List<ParameterTable> getLstIssuerStateForSearch() {
		return lstIssuerStateForSearch;
	}

	/**
	 * Sets the lst issuer state for search.
	 *
	 * @param lstIssuerStateForSearch the new lst issuer state for search
	 */
	public void setLstIssuerStateForSearch(
			List<ParameterTable> lstIssuerStateForSearch) {
		this.lstIssuerStateForSearch = lstIssuerStateForSearch;
	}

	/**
	 * Gets the issuer data model.
	 *
	 * @return the issuer data model
	 */
	public IssuerDataModel getIssuerDataModel() {
		return issuerDataModel;
	}

	/**
	 * Sets the issuer data model.
	 *
	 * @param issuerDataModel the new issuer data model
	 */
	public void setIssuerDataModel(IssuerDataModel issuerDataModel) {
		this.issuerDataModel = issuerDataModel;
	}

	/**
	 * Gets the issuer to.
	 *
	 * @return the issuer to
	 */
	public IssuerTO getIssuerTO() {
		return issuerTO;
	}

	/**
	 * Sets the issuer to.
	 *
	 * @param issuerTO the new issuer to
	 */
	public void setIssuerTO(IssuerTO issuerTO) {
		this.issuerTO = issuerTO;
	}

	/**
	 * Gets the id issuer pk.
	 *
	 * @return the id issuer pk
	 */
	public String getIdIssuerPk() {
		return idIssuerPk;
	}

	/**
	 * Sets the id issuer pk.
	 *
	 * @param idIssuerPk the new id issuer pk
	 */
	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}

	/**
	 * Gets the state issuer.
	 *
	 * @return the state issuer
	 */
	public Integer getStateIssuer() {
		return stateIssuer;
	}

	/**
	 * Sets the state issuer.
	 *
	 * @param stateIssuer the new state issuer
	 */
	public void setStateIssuer(Integer stateIssuer) {
		this.stateIssuer = stateIssuer;
	}

	/**
	 * Gets the lst issuer files.
	 *
	 * @return the lst issuer files
	 */
	public List<IssuerFile> getLstIssuerFiles() {
		return lstIssuerFiles;
	}

	/**
	 * Sets the lst issuer files.
	 *
	 * @param lstIssuerFiles the new lst issuer files
	 */
	public void setLstIssuerFiles(List<IssuerFile> lstIssuerFiles) {
		this.lstIssuerFiles = lstIssuerFiles;
	}

	/**
	 * Gets the id represented entity pk.
	 *
	 * @return the id represented entity pk
	 */
	public Long getIdRepresentedEntityPk() {
		return idRepresentedEntityPk;
	}

	/**
	 * Sets the id represented entity pk.
	 *
	 * @param idRepresentedEntityPk the new id represented entity pk
	 */
	public void setIdRepresentedEntityPk(Long idRepresentedEntityPk) {
		this.idRepresentedEntityPk = idRepresentedEntityPk;
	}

	/**
	 * Gets the represented entity.
	 *
	 * @return the represented entity
	 */
	public RepresentedEntity getRepresentedEntity() {
		return representedEntity;
	}
	
	/**
	 * Sets the represented entity.
	 *
	 * @param representedEntity the new represented entity
	 */
	public void setRepresentedEntity(RepresentedEntity representedEntity) {
		this.representedEntity = representedEntity;
	}

	/**
	 * Gets the document Validator Register.
	 *
	 * @return the document Validator Register
	 */
	public DocumentValidator getDocumentValidatorRegister() {
		return documentValidatorRegister;
	}
	
	/**
	 * Sets the document Validator Register.
	 *
	 * @param documentValidatorRegister the new document validator register
	 */
	public void setDocumentValidatorRegister(DocumentValidator documentValidatorRegister) {
		this.documentValidatorRegister = documentValidatorRegister;
	}
	
	/**
	 * Gets the document Register.
	 *
	 * @return the document  Register
	 */
	public DocumentTO getDocumentTORegister() {
		return documentTORegister;
	}
	
	/**
	 * Sets the document  Register.
	 *
	 * @param documentTORegister the new document to register
	 */
	public void setDocumentTORegister(DocumentTO documentTORegister) {
		this.documentTORegister = documentTORegister;
	}
	/**
	 * Gets the lst economic sector for search.
	 *
	 * @return the lst economic sector for search
	 */
	public List<ParameterTable> getLstEconomicSectorForSearch() {
		return lstEconomicSectorForSearch;
	}

	/**
	 * Sets the lst economic sector for search.
	 *
	 * @param lstEconomicSectorForSearch the new lst economic sector for search
	 */
	public void setLstEconomicSectorForSearch(
			List<ParameterTable> lstEconomicSectorForSearch) {
		this.lstEconomicSectorForSearch = lstEconomicSectorForSearch;
	}

	/**
	 * Gets the lst society type.
	 *
	 * @return the lst society type
	 */
	public List<ParameterTable> getLstSocietyType() {
		return lstSocietyType;
	}

	/**
	 * Sets the lst society type.
	 *
	 * @param lstSocietyType the new lst society type
	 */
	public void setLstSocietyType(List<ParameterTable> lstSocietyType) {
		this.lstSocietyType = lstSocietyType;
	}

	/**
	 * Gets the lst currency.
	 *
	 * @return the lst currency
	 */
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}

	/**
	 * Sets the lst currency.
	 *
	 * @param lstCurrency the new lst currency
	 */
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}

	/**
	 * Checks if is ind issuer depositary contract.
	 *
	 * @return true, if is ind issuer depositary contract
	 */
	public boolean isIndIssuerDepositaryContract() {
		return indIssuerDepositaryContract;
	}

	/**
	 * Sets the ind issuer depositary contract.
	 *
	 * @param indIssuerDepositaryContract the new ind issuer depositary contract
	 */
	public void setIndIssuerDepositaryContract(boolean indIssuerDepositaryContract) {
		this.indIssuerDepositaryContract = indIssuerDepositaryContract;
	}

	/**
	 * Checks if is ind inscription stock exchange.
	 *
	 * @return true, if is ind inscription stock exchange
	 */
	public boolean isIndInscriptionStockExchange() {
		return indInscriptionStockExchange;
	}

	/**
	 * Sets the ind inscription stock exchange.
	 *
	 * @param indInscriptionStockExchange the new ind inscription stock exchange
	 */
	public void setIndInscriptionStockExchange(boolean indInscriptionStockExchange) {
		this.indInscriptionStockExchange = indInscriptionStockExchange;
	}
	
	/** SIRTEX negotiate */
	public boolean isIndIssuerSIRTEXnegociate() {
		return indIssuerSIRTEXnegociate;
	}

	public void setIndIssuerSIRTEXnegociate(boolean indIssuerSIRTEXnegociate) {
		this.indIssuerSIRTEXnegociate = indIssuerSIRTEXnegociate;
	}
	
	/**
	 * Gets the lst category pep.
	 *
	 * @return the lst category pep
	 */
	public List<ParameterTable> getLstCategoryPep() {
		return lstCategoryPep;
	}

	/**
	 * Sets the lst category pep.
	 *
	 * @param lstCategoryPep the new lst category pep
	 */
	public void setLstCategoryPep(List<ParameterTable> lstCategoryPep) {
		this.lstCategoryPep = lstCategoryPep;
	}

	/**
	 * Gets the lst role pep.
	 *
	 * @return the lst role pep
	 */
	public List<ParameterTable> getLstRolePep() {
		return lstRolePep;
	}

	/**
	 * Sets the lst role pep.
	 *
	 * @param lstRolePep the new lst role pep
	 */
	public void setLstRolePep(List<ParameterTable> lstRolePep) {
		this.lstRolePep = lstRolePep;
	}

	/**
	 * Gets the max lenght document number holder.
	 *
	 * @return the max lenght document number holder
	 */
	public int getMaxLenghtDocumentNumberHolder() {
		return maxLenghtDocumentNumberHolder;
	}

	/**
	 * Sets the max lenght document number holder.
	 *
	 * @param maxLenghtDocumentNumberHolder the new max lenght document number holder
	 */
	public void setMaxLenghtDocumentNumberHolder(
			int maxLenghtDocumentNumberHolder) {
		this.maxLenghtDocumentNumberHolder = maxLenghtDocumentNumberHolder;
	}
	
	
	/**
	 * Gets the lst second document type representative.
	 *
	 * @return the lst second document type representative
	 */
	public List<ParameterTable> getLstSecondDocumentTypeRepresentative() {
		return lstSecondDocumentTypeRepresentative;
	}

	/**
	 * Sets the lst second document type representative.
	 *
	 * @param lstSecondDocumentTypeRepresentative the new lst second document type representative
	 */
	public void setLstSecondDocumentTypeRepresentative(
			List<ParameterTable> lstSecondDocumentTypeRepresentative) {
		this.lstSecondDocumentTypeRepresentative = lstSecondDocumentTypeRepresentative;
	}

	/**
	 * Gets the max lenght second document number legal.
	 *
	 * @return the max lenght second document number legal
	 */
	public int getMaxLenghtSecondDocumentNumberLegal() {
		return maxLenghtSecondDocumentNumberLegal;
	}

	/**
	 * Sets the max lenght second document number legal.
	 *
	 * @param maxLenghtSecondDocumentNumberLegal the new max lenght second document number legal
	 */
	public void setMaxLenghtSecondDocumentNumberLegal(
			int maxLenghtSecondDocumentNumberLegal) {
		this.maxLenghtSecondDocumentNumberLegal = maxLenghtSecondDocumentNumberLegal;
	}

	/**
	 * Gets the lst issuer attachment type.
	 *
	 * @return the lst issuer attachment type
	 */
	public List<ParameterTable> getLstIssuerAttachmentType() {
		return lstIssuerAttachmentType;
	}

	/**
	 * Sets the lst issuer attachment type.
	 *
	 * @param lstIssuerAttachmentType the new lst issuer attachment type
	 */
	public void setLstIssuerAttachmentType(List<ParameterTable> lstIssuerAttachmentType) {
		this.lstIssuerAttachmentType = lstIssuerAttachmentType;
	}

	/**
	 * Gets the issuer file name display.
	 *
	 * @return the issuer file name display
	 */
	public String getIssuerFileNameDisplay() {
		return issuerFileNameDisplay;
	}

	/**
	 * Sets the issuer file name display.
	 *
	 * @param issuerFileNameDisplay the new issuer file name display
	 */
	public void setIssuerFileNameDisplay(String issuerFileNameDisplay) {
		this.issuerFileNameDisplay = issuerFileNameDisplay;
	}

	/**
	 * Gets the attach issuer file selected.
	 *
	 * @return the attach issuer file selected
	 */
	public Integer getAttachIssuerFileSelected() {
		return attachIssuerFileSelected;
	}

	/**
	 * Sets the attach issuer file selected.
	 *
	 * @param attachIssuerFileSelected the new attach issuer file selected
	 */
	public void setAttachIssuerFileSelected(Integer attachIssuerFileSelected) {
		this.attachIssuerFileSelected = attachIssuerFileSelected;
	}
	
	/**
	 * Checks if is enable select participant file.
	 *
	 * @return true, if is enable select participant file
	 */
	public boolean isEnableSelectIssuerFile() {
		return Validations.validateIsNotNullAndPositive(attachIssuerFileSelected);
	}

	/**
	 * Gets the representative file name display.
	 *
	 * @return the representative file name display
	 */
	public String getRepresentativeFileNameDisplay() {
		return representativeFileNameDisplay;
	}

	/**
	 * Sets the representative file name display.
	 *
	 * @param representativeFileNameDisplay the new representative file name display
	 */
	public void setRepresentativeFileNameDisplay(
			String representativeFileNameDisplay) {
		this.representativeFileNameDisplay = representativeFileNameDisplay;
	}

	/**
	 * Gets the lst representative file types.
	 *
	 * @return the lst representative file types
	 */
	public List<ParameterTable> getLstRepresentativeFileTypes() {
		return lstRepresentativeFileTypes;
	}

	/**
	 * Sets the lst representative file types.
	 *
	 * @param lstRepresentativeFileTypes the new lst representative file types
	 */
	public void setLstRepresentativeFileTypes(
			List<ParameterTable> lstRepresentativeFileTypes) {
		this.lstRepresentativeFileTypes = lstRepresentativeFileTypes;
	}

	/**
	 * Gets the lst representative file history.
	 *
	 * @return the lst representative file history
	 */
	public List<RepresentativeFileHistory> getLstRepresentativeFileHistory() {
		return lstRepresentativeFileHistory;
	}

	/**
	 * Sets the lst representative file history.
	 *
	 * @param lstRepresentativeFileHistory the new lst representative file history
	 */
	public void setLstRepresentativeFileHistory(
			List<RepresentativeFileHistory> lstRepresentativeFileHistory) {
		this.lstRepresentativeFileHistory = lstRepresentativeFileHistory;
	}

	/**
	 * Gets the representative file type.
	 *
	 * @return the representative file type
	 */
	public Integer getRepresentativeFileType() {
		return representativeFileType;
	}

	/**
	 * Sets the representative file type.
	 *
	 * @param representativeFileType the new representative file type
	 */
	public void setRepresentativeFileType(Integer representativeFileType) {
		this.representativeFileType = representativeFileType;
	}

	/**
	 * Gets the max lenght document number legal.
	 *
	 * @return the max lenght document number legal
	 */
	public int getMaxLenghtDocumentNumberLegal() {
		return maxLenghtDocumentNumberLegal;
	}

	/**
	 * Sets the max lenght document number legal.
	 *
	 * @param maxLenghtDocumentNumberLegal the new max lenght document number legal
	 */
	public void setMaxLenghtDocumentNumberLegal(int maxLenghtDocumentNumberLegal) {
		this.maxLenghtDocumentNumberLegal = maxLenghtDocumentNumberLegal;
	}

	/**
	 * Checks if is ind numeric type document legal.
	 *
	 * @return true, if is ind numeric type document legal
	 */
	public boolean isIndNumericTypeDocumentLegal() {
		return indNumericTypeDocumentLegal;
	}

	/**
	 * Sets the ind numeric type document legal.
	 *
	 * @param indNumericTypeDocumentLegal the new ind numeric type document legal
	 */
	public void setIndNumericTypeDocumentLegal(boolean indNumericTypeDocumentLegal) {
		this.indNumericTypeDocumentLegal = indNumericTypeDocumentLegal;
	}

	/**
	 * Gets the lst document type representative result.
	 *
	 * @return the lst document type representative result
	 */
	public List<ParameterTable> getLstDocumentTypeRepresentativeResult() {
		return lstDocumentTypeRepresentativeResult;
	}

	/**
	 * Sets the lst document type representative result.
	 *
	 * @param lstDocumentTypeRepresentativeResult the new lst document type representative result
	 */
	public void setLstDocumentTypeRepresentativeResult(
			List<ParameterTable> lstDocumentTypeRepresentativeResult) {
		this.lstDocumentTypeRepresentativeResult = lstDocumentTypeRepresentativeResult;
	}
	
	/**
	 * Checks if is enable select representative file.
	 *
	 * @return true, if is enable select representative file
	 */
	public boolean isEnableSelectRepresentativeFile() {
		return Validations.validateIsNotNullAndPositive(representativeFileType);
	}

	/**
	 * Gets the email legal representative.
	 *
	 * @return the email legal representative
	 */
	public String getEmailLegalRepresentative() {
		return emailLegalRepresentative;
	}

	/**
	 * Sets the email legal representative.
	 *
	 * @param emailLegalRepresentative the new email legal representative
	 */
	public void setEmailLegalRepresentative(String emailLegalRepresentative) {
		this.emailLegalRepresentative = emailLegalRepresentative;
	}

	/**
	 * Checks if is ind representative nationality national.
	 *
	 * @return true, if is ind representative nationality national
	 */
	public boolean isIndRepresentativeNationalityNational() {
		return Validations.validateIsNotNull(legalRepresentativeHistory.getNationality()) && 
				countryResidence.equals(legalRepresentativeHistory.getNationality());
	}
	
	/**
	 * Checks if is ind representative nationality foreign.
	 *
	 * @return true, if is ind representative nationality foreign
	 */
	public boolean isIndRepresentativeNationalityForeign() {
		return Validations.validateIsNotNullAndPositive(legalRepresentativeHistory.getNationality()) &&				
				!(countryResidence.equals(legalRepresentativeHistory.getNationality()));
	}

	/**
	 * Checks if is ind issuer differentiated.
	 *
	 * @return true, if is ind issuer differentiated
	 */
	public boolean isIndIssuerDifferentiated() {
		return indIssuerDifferentiated;
	}

	/**
	 * Sets the ind issuer differentiated.
	 *
	 * @param indIssuerDifferentiated the new ind issuer differentiated
	 */
	public void setIndIssuerDifferentiated(boolean indIssuerDifferentiated) {
		this.indIssuerDifferentiated = indIssuerDifferentiated;
	}	
	
	/**
	 * Gets the min depositary contract date.
	 *
	 * @return the min depositary contract date
	 */
	public Date getMinDepositaryContractDate() {
		return minDepositaryContractDate;
	}

	/**
	 * Sets the min depositary contract date.
	 *
	 * @param minDepositaryContractDate the new min depositary contract date
	 */
	public void setMinDepositaryContractDate(Date minDepositaryContractDate) {
		this.minDepositaryContractDate = minDepositaryContractDate;
	}

	/**
	 * Gets the min inscription stock exch date.
	 *
	 * @return the min inscription stock exch date
	 */
	public Date getMinInscriptionStockExchDate() {
		return minInscriptionStockExchDate;
	}

	/**
	 * Sets the min inscription stock exch date.
	 *
	 * @param minInscriptionStockExchDate the new min inscription stock exch date
	 */
	public void setMinInscriptionStockExchDate(Date minInscriptionStockExchDate) {
		this.minInscriptionStockExchDate = minInscriptionStockExchDate;
	}

	/**
	 * Checks if is ind numeric second type document legal.
	 *
	 * @return true, if is ind numeric second type document legal
	 */
	public boolean isIndNumericSecondTypeDocumentLegal() {
		return indNumericSecondTypeDocumentLegal;
	}

	/**
	 * Sets the ind numeric second type document legal.
	 *
	 * @param indNumericSecondTypeDocumentLegal the new ind numeric second type document legal
	 */
	public void setIndNumericSecondTypeDocumentLegal(
			boolean indNumericSecondTypeDocumentLegal) {
		this.indNumericSecondTypeDocumentLegal = indNumericSecondTypeDocumentLegal;
	}

	/**
	 * Gets the lst second countries representative.
	 *
	 * @return the lst second countries representative
	 */
	public List<GeographicLocation> getLstSecondCountriesRepresentative() {
		return lstSecondCountriesRepresentative;
	}

	/**
	 * Sets the lst second countries representative.
	 *
	 * @param lstSecondCountriesRepresentative the new lst second countries representative
	 */
	public void setLstSecondCountriesRepresentative(
			List<GeographicLocation> lstSecondCountriesRepresentative) {
		this.lstSecondCountriesRepresentative = lstSecondCountriesRepresentative;
	}

	public List<ParameterTable> getLstOfferType() {
		return lstOfferType;
	}

	public void setLstOfferType(List<ParameterTable> lstOfferType) {
		this.lstOfferType = lstOfferType;
	}

	/**
	 * #########################################################################REFACTOR####################################################################.
	 *
	 * @return the user info
	 */
	/**
	 * get User Information
	 * @return object User Information
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}
	
	/**
	 * set User Information.
	 *
	 * @param userInfo object User Information
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
			try { 
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
				createObjects();  
				loadCombosForSearch();	
				listHolidays();
				sessionValidUserAccount();	
				loadAllEconomicActivity();
				loadCboLstCreditRatingScales();
				getLstDocumentSource();
				
			} catch (Exception e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
	}
	
	/**
	 * Validates user session objects to enable or disable.
	 *
	 * @throws ServiceException the service exception
	 */
	public void sessionValidUserAccount() throws ServiceException
	{
		if(userInfo.getUserAccountSession().isDepositaryInstitution()) {
			issuerTO.setAllowSearchAll(false);
		} else if((userInfo.getUserAccountSession().isIssuerInstitucion() || userInfo.getUserAccountSession().isIssuerDpfInstitucion()) && 
				Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getIssuerCode())){
			issuerTO.setAllowSearchAll(true);
			issuerTO.setIdIssuerPk(userInfo.getUserAccountSession().getIssuerCode());
			Issuer objIssuer= issuerServiceFacade.findIssuerServiceFacade(issuerTO);
			issuerTO.setMnemonic(objIssuer.getMnemonic());
			issuerTO.setBusinessName(objIssuer.getBusinessName());
			issuerTO.setDocumentType(objIssuer.getDocumentType());
			issuerTO.setDocumentNumber(objIssuer.getDocumentNumber());
			issuerTO.setIssuerState(objIssuer.getStateIssuer());
			issuerTO.setEconomicSector(objIssuer.getEconomicSector());
			changeSelectedEconomicSectorForSearch();
			issuerTO.setEconomicActivity(objIssuer.getEconomicActivity());
		}
	}
	/**
	 * List holidays.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listHolidays() throws ServiceException{
		Holiday holidayFilter = new Holiday();
		holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		GeographicLocation countryFilter = new GeographicLocation();
		countryFilter.setIdGeographicLocationPk(countryResidence);
		holidayFilter.setGeographicLocation(countryFilter);
				
		allHolidays = CommonsUtilities.convertListDateToString(generalParametersFacade.getListHolidayDateServiceFacade(holidayFilter));
	}
	
	/**
	 * Create objects to store and obtain data relating to issuers.
	 */
	public void createObjects()
	{
		issuer = new Issuer();
		issuerTO = new IssuerTO();
		legalRepresentativeHistory = new LegalRepresentativeHistory();
		pepPerson = new PepPerson();	
	}
	/**
	 * Load combos for search.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCombosForSearch() throws ServiceException{		
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());		
		loadListStateSearch(filterParameterTable);	
		loadListEconomicSectorSearch(filterParameterTable);	
		loadListDocumenTypeSearch(filterParameterTable);
	}
	
	/**
	 * List the document types of issuers.
	 *
	 * @param filterParameterTable Object Filter
	 * @throws ServiceException the service exception
	 */
	private void loadListDocumenTypeSearch(ParameterTableTO filterParameterTable)throws ServiceException
	{
		filterParameterTable.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
		List<Integer> lstParameterTablePk = new ArrayList<Integer>();
		lstParameterTablePk.add(DocumentType.RUC.getCode());
		lstParameterTablePk.add(DocumentType.DIO.getCode());
		filterParameterTable.setLstParameterTablePk(lstParameterTablePk);
		lstIssuerDocumentTypeForSearch = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
	}
	
	/**
	 * List the State of issuers.
	 *
	 * @param filterParameterTable Object Filter
	 * @throws ServiceException the service exception
	 */
	private void loadListStateSearch(ParameterTableTO filterParameterTable)throws ServiceException
	{
		filterParameterTable.setMasterTableFk(MasterTableType.ISSUER_STATE.getCode());
		lstIssuerStateForSearch = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
	}
	
	/**
	 * List the Economic Sector of issuers.
	 *
	 * @param filterParameterTable Object Filter
	 * @throws ServiceException the service exception
	 */
	private void loadListEconomicSectorSearch(ParameterTableTO filterParameterTable)throws ServiceException
	{
		filterParameterTable.setMasterTableFk(MasterTableType.ECONOMIC_SECTOR.getCode());
		lstEconomicSectorForSearch = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
	}
	
	/**
	 * List the Economic Activity of issuers.
	 *
	 * @throws ServiceException the service exception
	 */
//	private void loadListEconomicActivitySearch(ParameterTableTO filterParameterTable)throws ServiceException
//	{
//		filterParameterTable.setMasterTableFk(MasterTableType.ECONOMIC_ACTIVITY.getCode());
//		lstEconomicActivityForSearch = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
//	}
//	
	public void loadAllEconomicActivity() throws ServiceException{
		ParameterTableTO pTableTo=new ParameterTableTO();
		pTableTo.setMasterTableFk( MasterTableType.ECONOMIC_ACTIVITY.getCode() );
		List<ParameterTable> lstParameter=generalParametersFacade.getListParameterTableServiceBean( pTableTo);
		billParametersTableMap(lstParameter);
	}
	

	/**
	 * Load register participant action.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String loadRegisterIssuerAction(){
		try {
			setViewOperationType(ViewOperationsType.REGISTER.getCode());
			initializeObjectsRegister();
			initializeIndicators();			
			loadCombosIssuerManagement();			
			issuer.getGeographicLocation().setIdGeographicLocationPk(countryResidence);
			changeSelectedCountry();
			changeSelectedDocumentTypeRegister();
			changeSelectedEconomicSector();
			issuer.setDepartment(departmentDefault);			
			loadProvinceDepartment();
			issuer.setProvince(provinceDefault);
			loadDistrictProvince();
			issuer.setDistrict(districtDefault);
			issuer.setDocumentType(DocumentType.RUC.getCode());
			issuer.setEconomicSector(EconomicSectorType.FINANCIAL_PUBLIC_SECTOR.getCode());
			issuer.setConstitutionDate(CommonsUtilities.currentDate()); //issue 708
			changeSelectedEconomicSector();
			buildInvestorTypeByEconAcSelected(null);
			return WINDOW_REGISTRY_ISSUER;	
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}
	
	/**
	 * Initialize Objects Register.
	 */
	public void initializeObjectsRegister()
	{
		issuer = new Issuer();
		issuer.setHolder(new Holder());
		issuer.setGeographicLocation(new GeographicLocation());		
		issuer.setConstitutionDate(CommonsUtilities.currentDate());
		legalRepresentativeHelperBean.setLstLegalRepresentativeHistory(new  ArrayList<LegalRepresentativeHistory>());		
		lstIssuerFiles = new ArrayList<IssuerFile>();			
		issuerFileNameDisplay = null;
		documentTORegister = new DocumentTO();
	}
	
	/**
	 * Initialize Indicators.
	 */
	public void initializeIndicators()
	{		
		indIssuerDepositaryContract = Boolean.FALSE;
		indInscriptionStockExchange = Boolean.FALSE;
		indIssuerDifferentiated = Boolean.FALSE;
		indFICPATEconomicActivity=Boolean.FALSE;
		indIssuerSIRTEXnegociate=Boolean.FALSE;
	}
	
	/**
	 * Load default combos.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCombosIssuerManagement() throws ServiceException{	
		loadCountries();	
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		loadListOfferType(filterParameterTable);
		loadListEconomicSector(filterParameterTable);		
	//	loadListSocietyType(filterParameterTable);	
		loadListCurrency(filterParameterTable);	
		loadListDocumenType(filterParameterTable);	
	}
	
	/**
	 * Load countries.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCountries() throws ServiceException{
		GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
		geographicLocationTO.setGeographicLocationType(GeographicLocationType.COUNTRY.getCode());
		geographicLocationTO.setState(GeographicLocationStateType.REGISTERED.getCode());
		lstCountries = generalParametersFacade.getListGeographicLocationServiceFacade(geographicLocationTO);
	}
	
	/**
	 * List the document types of issuers.
	 *
	 * @param filterParameterTable Object Filter
	 * @throws ServiceException the service exception
	 */
	public void loadListDocumenType(ParameterTableTO filterParameterTable)throws ServiceException
	{
		filterParameterTable.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
		List<Integer> lstParameterTablePk = new ArrayList<Integer>();
		lstParameterTablePk.add(DocumentType.RUC.getCode());
		lstParameterTablePk.add(DocumentType.DIO.getCode());
		filterParameterTable.setLstParameterTablePk(lstParameterTablePk);
		lstIssuerDocumentType = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
	}
	
	/**
	 * List the Currency of issuers.
	 *
	 * @param filterParameterTable Object Filter
	 * @throws ServiceException the service exception
	 */	
	public void loadListCurrency(ParameterTableTO filterParameterTable)throws ServiceException
	{
		filterParameterTable.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		lstCurrency = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable); 
	}
	
	/**
	 * List the Society Type of issuers.
	 *
	 * @param filterParameterTable Object Filter
	 * @throws ServiceException the service exception
	 */	
	public void loadListSocietyType(ParameterTableTO filterParameterTable)throws ServiceException
	{
		filterParameterTable.setMasterTableFk(MasterTableType.SOCIETY_TYPE.getCode());
		lstSocietyType = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
	}
	
	/**
	 * List the Economic Sector of issuers.
	 *
	 * @param filterParameterTable Object Filter
	 * @throws ServiceException the service exception
	 */
	public void loadListEconomicSector(ParameterTableTO filterParameterTable)throws ServiceException
	{
		filterParameterTable.setMasterTableFk(MasterTableType.ECONOMIC_SECTOR.getCode());
		lstEconomicSector = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
	}
	
	/**
	 * List the type of issuers.
	 *
	 * @param filterParameterTable Object Filter
	 * @throws ServiceException the service exception
	 */
	public void loadListOfferType(ParameterTableTO filterParameterTable)throws ServiceException
	{
		filterParameterTable.setMasterTableFk(MasterTableType.MASTER_TABLE_OFFER_TYPE.getCode());
		lstOfferType = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
	}
	
	/**
	 * Clean Data Model.
	 */
	public void cleanDataModel()
	{
		issuerDataModel = null;
		issuer=null;
	}
	/**
	 * Change selected document type search.
	 */
	public void changeSelectedDocumentTypeSearch(){
		issuerTO.setDocumentNumber(null);		
		if(Validations.validateIsNotNullAndPositive(issuerTO.getDocumentType()))
			documentTORegister=documentValidatorRegister.loadMaxLenghtType(issuerTO.getDocumentType());				
		cleanDataModel();
	}
	/**
	 * Change selected economic sector for search.
	 */
	@LoggerAuditWeb
	public void changeSelectedEconomicSectorForSearch(){
		try{
			hideDialogsListener(null);			
			ParameterTableTO filterParameterTable = new ParameterTableTO();
			filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());			
			if(Validations.validateIsNotNullAndPositive(issuerTO.getEconomicSector())){
				filterParameterTable.setIdRelatedParameterFk(issuerTO.getEconomicSector());
			//	loadListEconomicActivitySearch(filterParameterTable);
				issuerTO.setEconomicActivity(null);
				lstEconomicActivityForSearch=generalParametersFacade.getListEconomicActivityBySector(issuerTO.getEconomicSector());
			}	
			else lstEconomicActivityForSearch=null;
			cleanDataModel();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	/**
	 * Search issuer by filters.
	 */
	@LoggerAuditWeb
	public void searchIssuerByFilters(){
		try{
		firsRegisterDataTable = 0;
		issuerTO.setSortBy("idIssuerPk");	
		issuerDataModel = new IssuerDataModel(issuerServiceFacade.getListIssuerByFiltersServiceFacade(issuerTO));
		if (issuerDataModel.getRowCount() > 0)
			setTrueValidButtons();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	/**
	 * Change selected country.
	 */
	@LoggerAuditWeb
	public void changeSelectedCountry(){
		try{
			hideDialogsListener(null);									
			createObjectsChangeSelectedCountry();  
			initializeObjectsChangeSelectedCountry();	
			loadDepartmentCountry();
			validateSelectedCountry();
			cleanObjectsChangeSelectedCountry();
			if(issuer.getGeographicLocation().getIdGeographicLocationPk().equals(countryResidence)){ 
				issuer.setDocumentType(DocumentType.RUC.getCode()); 
				changeSelectedDocumentTypeRegister();
			}
			JSFUtilities.resetComponent(":frmIssuerRegister:tabViewIssuer:cboDocumentType");
			JSFUtilities.resetComponent(":frmIssuerRegister:tabViewIssuer:msgCboDocumentType");
			JSFUtilities.resetComponent(":frmIssuerRegister:tabViewIssuer:txtTradeName");
			JSFUtilities.resetComponent(":frmIssuerRegister:tabViewIssuer:msgTxtTradeName");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	/**
     * Hide dialogs listener.
     *
     * @param actionEvent the action event
     */
    public void hideDialogsListener(ActionEvent actionEvent){
		JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
		JSFUtilities.hideGeneralDialogues();		
		JSFUtilities.hideComponent("cnfIssuerRegister");
		JSFUtilities.hideComponent("cnfEndTransactionIssuer");
		

		/*JSFUtilities.hideComponent("cnfRegisterRequestIssuer");
		JSFUtilities.hideComponent("cnfEndTransactionParticipant");
		JSFUtilities.hideComponent("cnfMsgBusinessValidation");
		JSFUtilities.hideComponent("cnfRegisterIssuerRequest");
		JSFUtilities.hideComponent("cnfIssuerRequestConfirmation");*/
	}
    
    /**
     * Create Objects Change Selected Country.
     */
	public void createObjectsChangeSelectedCountry()
	{
		issuer.setHolder(new Holder());	
		lstIssuerFiles = new ArrayList<IssuerFile>();	
	}
	
	/**
	 * Initialize Objects Change Selected Country.
	 */
	public void initializeObjectsChangeSelectedCountry()
	{
		issuer.setDocumentType(null);
		issuerFileNameDisplay = null;
		indIssuerDifferentiated = Boolean.FALSE;
	}
	
	/**
	 * Load Department Country.
	 */
	public void loadDepartmentCountry()
	{
		try{
			if(!Validations.validateIsNullOrNotPositive(issuer.getGeographicLocation().getIdGeographicLocationPk()))
			{
				lstDepartments=getLstDepartments(issuer.getGeographicLocation().getIdGeographicLocationPk());
				lstProvinces = null;				
				lstDistricts = null;
			}				
			else
			{				
				lstDepartments = null;				
				lstProvinces = null;				
				lstDistricts = null;
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Country selection validate.
	 *
	 * @throws ServiceException the service exception
	 */
	public void validateSelectedCountry()throws ServiceException
	{
		//Validate if the country was no selected
		if(Validations.validateIsNullOrNotPositive(issuer.getGeographicLocation().getIdGeographicLocationPk()))
		{
			lstIssuerFiles = null;
			issuer.setDocumentType(null);
		}			
		else {				
			//Validate if the issuer is national or foreign in order to set the document type and trade name
			if(isIndIssuerNational()){
				loadIssuerDocumentTypeNational();
				loadIssuerAttachmentTypeNational();			
			} else {
				loadIssuerDocumentTypeForeign();
				loadIssuerAttachmentTypeForeign();			
			}
		}
	}
	
	/**
	 * Clean Objects Change Selected Country.
	 */
	public void cleanObjectsChangeSelectedCountry()
	{
		issuer.setLegalAddress(null);
		issuer.setDocumentNumber(null);
		issuer.setBusinessName(null);
	}	
	/**
	 * Change selected document type register.
	 */
	public void changeSelectedDocumentTypeRegister() {
		hideDialogsListener(null);	
		issuer.setDocumentNumber(null);
		if(Validations.validateIsNotNullAndPositive(issuer.getDocumentType())){
			documentTORegister=documentValidatorRegister.loadMaxLenghtType(issuer.getDocumentType());
			DocumentTO documentTO = documentValidatorRegister.validateEmissionRules(issuer.getDocumentType());
			documentTORegister.setIndicatorAlphanumericWithDash(documentTO.isIndicatorAlphanumericWithDash());
			documentTORegister.setIndicatorDocumentSource(documentTO.isIndicatorDocumentSource());
		}
		JSFUtilities.resetComponent(":frmIssuerRegister:tabViewIssuer:txtDocumentNumber");
		JSFUtilities.resetComponent(":frmIssuerRegister:tabViewIssuer:msgTxtDocumentNumber");
	}
	/**
	 * Validate document issuer.
	 */
	@LoggerAuditWeb
	public void validateDocumentIssuer(){			
		try{
			if(Validations.validateIsNullOrEmpty(issuer.getDocumentNumber())){
				hideDialogsListener(null);
				issuer.setBusinessName(null);
				cleanHolderIssuer();				
				return;
			}
			
			if(!documentValidatorRegister.validateFormatDocumentNumber(issuer.getDocumentType(),issuer.getDocumentNumber(),"frmIssuerRegister:tabViewIssuer:txtDocumentNumber")){
				issuer.setDocumentNumber(null);
				return;
			}
			if(validateIssuerInvestmentFundsClosedAndLegacy(issuer.getDocumentNumber(),issuer.getDocumentSource(),issuer.getDocumentType(),issuer.getGeographicLocation().getIdGeographicLocationPk())){
				return;
			}
			    
			//CUI valid code
			Holder holder = findHolder();			
			if(Validations.validateIsNotNull(holder)){				
				if(!(HolderStateType.BLOCKED.getCode().equals(holder.getStateHolder())) &&
				   !(HolderStateType.REGISTERED.getCode().equals(holder.getStateHolder()))){
					hideDialogsListener(null);
					viewMessageHolderIsNotBeenRegistered(holder);					
					cleanObjectForDocumentIssuer();
					cleanHolderIssuer();
				} else {					
					Issuer issuerResult = findIssuerNationalOrForeign(holder);					
					if(Validations.validateIsNotNull(issuerResult)){
						hideDialogsListener(null);
						viewMessageIssuerIsAlreadyRegistered(issuerResult);	    		
			    		cleanObjectForDocumentIssuer();						
						cleanHolderIssuer();
					} else 
					{
						hideDialogsListener(null);
						setDetailsOfTheHolder(holder);	
					}										
				}
			} else {		
				//valid listing NIT
//				if(IssuerDocumentType.RUC.getCode().equals(issuer.getDocumentType()) || IssuerDocumentType.CI.getCode().equals(issuer.getDocumentType())
//						|| IssuerDocumentType.CDN.getCode().equals(issuer.getDocumentType())){				
//					try {											
//						InstitutionInformation institutionResultFind = findInstitutionInformation();						
//						if(Validations.validateIsNotNull(institutionResultFind)){						
//						//Validate if exists issuer with entered  document type and document number
//							Issuer issuerResult = findIssuer();											
//							if(Validations.validateIsNotNull(issuerResult)){
//								hideDialogsListener(null);
//								viewMessageIssuerIsAlreadyRegistered(issuerResult);
//					    		issuer.setDocumentNumber(null);
//					    		cleanHolderIssuer();							
//							} else {
//								List<OfacPerson> lstOfacPerson = findOfacPerson(institutionResultFind.getCorporateName());
//								if(Validations.validateIsNullOrNotPositive(lstOfacPerson.size()))
//								{
//									List<PepPerson> lstPepPerson= findPepPerson(institutionResultFind.getCorporateName());
//									if(Validations.validateIsNullOrNotPositive(lstPepPerson.size()))
//									{
//										List<PnaPerson> lstPnaPerson = findPnaPerson(institutionResultFind.getCorporateName());
//										if(Validations.validateIsNullOrNotPositive(lstPnaPerson.size()))
//										{
//											hideDialogsListener(null);
//											issuer.setBusinessName(institutionResultFind.getCorporateName());
//											issuer.setDescription(institutionResultFind.getCorporateName());							
//											cleanHolderIssuer();											
//											viewMessageCUICodeBeCreated();
//										}else{
//											hideDialogsListener(null);
//											viewMessageThereIsNoCodeInTypeDocumentPna();
//											cleanObjectForDocumentIssuer();						
//											cleanHolderIssuer();
//										}
//									}
//									else {
//										hideDialogsListener(null);
//										viewMessageThereIsNoCodeInTypeDocumentPep();
//										cleanObjectForDocumentIssuer();						
//										cleanHolderIssuer();
//									}
//								}
//								else {
//									hideDialogsListener(null);
//									viewMessageThereIsNoCodeInTypeDocumentOfac();	
//									cleanObjectForDocumentIssuer();						
//									cleanHolderIssuer();
//								}
//							}						
//						} else {
//							//there is no code in NIT list
//							hideDialogsListener(null);
//							viewMessageThereIsNoCodeInTypeDocument();			    		
//				    		cleanObjectForDocumentIssuer();						
//							cleanHolderIssuer();
//						}						
//					} catch (NumberFormatException e) {
//						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
//								PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_ISSUER_INVALID_RNC_FORMAT));
//						JSFUtilities.showSimpleValidationDialog();						
//			    		cleanObjectForDocumentIssuer();						
//					}					
//				} else {
					hideDialogsListener(null);
					cleanObjectForDocumentIssuerNotDocumentNumber();
					viewMessageCUICodeBeCreated();									
					cleanHolderIssuer();					
//				}				
			}		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Find Ofac Person.
	 *
	 * @param name Trade or name
	 * @return List  Ofac Person
	 * @throws ServiceException Service Exception
	 */
	public List<OfacPerson> findOfacPerson(String name)throws ServiceException
	{		
		return  issuerServiceFacade.getOfacPersonList(name);
	}
	
	/**
	 * Find Pep Person.
	 *
	 * @param name name
	 * @return List pep person
	 * @throws ServiceException Service Exception
	 */
	public List<PepPerson> findPepPerson(String name)throws ServiceException
	{
		return  issuerServiceFacade.getPepPersonList(name);
	}
	
	/**
	 * Find Pna Person.
	 *
	 * @param name name
	 * @return List pna person
	 * @throws ServiceException Service Exception
	 */
	public List<PnaPerson> findPnaPerson(String name)throws ServiceException
	{
		return  issuerServiceFacade.getPnaPersonList(name);		
	}
	
	/**
	 * View Message There Is No Code In Type Document.
	 */
	public void viewMessageThereIsNoCodeInTypeDocument()
	{
		StringBuilder sbBodyData = new StringBuilder();
		sbBodyData.append(IssuerDocumentType.get(issuer.getDocumentType()).getValue());
		sbBodyData.append(GeneralConstants.HYPHEN);
		sbBodyData.append(issuer.getDocumentNumber());							
		Object[] arrBodyData = {sbBodyData.toString()};		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_ISSUER_RNC_NOT_FOUND,arrBodyData));
		JSFUtilities.showSimpleValidationDialog();	
	}
	
	/**
	 * View Message There Is No Code In NIT.
	 */
	public void viewMessageThereIsNoCodeInTypeDocumentOfac()
	{
		StringBuilder sbBodyData = new StringBuilder();
		sbBodyData.append(IssuerDocumentType.get(issuer.getDocumentType()).getValue());
		sbBodyData.append(GeneralConstants.HYPHEN);
		sbBodyData.append(issuer.getDocumentNumber());							
		Object[] arrBodyData = {sbBodyData.toString()};	
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_ISSUER_TYPE_DOCUMENT_OFAC,arrBodyData));
		JSFUtilities.showSimpleValidationDialog();	
	}	
	
	/**
	 * View Message There Is No Code In NIT.
	 */
	public void viewMessageThereIsNoCodeInTypeDocumentPep()
	{
		StringBuilder sbBodyData = new StringBuilder();
		sbBodyData.append(IssuerDocumentType.get(issuer.getDocumentType()).getValue());
		sbBodyData.append(GeneralConstants.HYPHEN);
		sbBodyData.append(issuer.getDocumentNumber());							
		Object[] arrBodyData = {sbBodyData.toString()};			
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_ISSUER_TYPE_DOCUMENT_PEP,arrBodyData));
		JSFUtilities.showSimpleValidationDialog();	
	}	
	
	/**
	 * View Message There Is No Code In NIT.
	 */
	public void viewMessageThereIsNoCodeInTypeDocumentPna()
	{
		StringBuilder sbBodyData = new StringBuilder();
		sbBodyData.append(IssuerDocumentType.get(issuer.getDocumentType()).getValue());
		sbBodyData.append(GeneralConstants.HYPHEN);
		sbBodyData.append(issuer.getDocumentNumber());							
		Object[] arrBodyData = {sbBodyData.toString()};		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_ISSUER_TYPE_DOCUMENT_PNA,arrBodyData));
		JSFUtilities.showSimpleValidationDialog();	
	}	
	
	/**
	 * return object Institution Information.
	 *
	 * @return object Institution Information
	 * @throws ServiceException service exception
	 */
	public InstitutionInformation findInstitutionInformation()throws ServiceException
	{
		InstitutionInformation institutionFilter = new InstitutionInformation();
		institutionFilter.setDocumentNumber(issuer.getDocumentNumber());	
		institutionFilter.setDocumentType(Integer.valueOf(issuer.getDocumentType()));
		return  issuerServiceFacade.findInstitutionInformationByIdServiceFacade(institutionFilter);
	}
	
	/**
	 * Clean the holder of Issuer.
	 */
	public void cleanHolderIssuer()
	{
		issuer.setHolder(new Holder());
		issuer.setIndHolderWillBeCreated(Boolean.TRUE);
	}

	/**
	 * Clean Object For Document Issuer.
	 */
	public void cleanObjectForDocumentIssuer()
	{
		issuer.setDocumentNumber(null);
		issuer.setBusinessName(null);		
		issuer.setDescription(null);
	}
	
	/**
	 * set Details Of The Holder.
	 *
	 * @param holder the new details of the holder
	 */
	public void setDetailsOfTheHolder(Holder holder)
	{
		issuer.setHolder(holder);
		issuer.setBusinessName(holder.getFullName());
		issuer.setDescription(holder.getFullName());
		issuer.setEconomicSector(holder.getEconomicSector());
		changeSelectedEconomicSector();
		issuer.setEconomicActivity(holder.getEconomicActivity());	
		buildInvestorTypeByEconAcSelected(holder.getEconomicActivity());
		issuer.setInvestorType(holder.getInvestorType());
		issuer.setIndHolderWillBeCreated(Boolean.FALSE);	
	}
		
		/**
		 * Clean Object For Document Issuer.
		 */
	public void cleanObjectForDocumentIssuerNotDocumentNumber()
	{
		issuer.setBusinessName(null);
		issuer.setDescription(null);
	}
	
	/**
	 * view Message CUI Code Be Created ADM_ISSUER_REQUEST_VALIDATION_ISSUER_DOCUMENT_NUMBER.
	 */
	public void viewMessageCUICodeBeCreated()
	{
	    showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_WILL_BE_CREATED));
		JSFUtilities.showSimpleValidationDialog();		
	}
	
	/**
	 * View message request issuer document number.
	 */
	/*
	 * view Message ADM_ISSUER_REQUEST_VALIDATION_ISSUER_DOCUMENT_NUMBER
	 */
	public void viewMessageRequestIssuerDocumentNumber()
	{
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_ISSUER_DOCUMENT_NUMBER));
		JSFUtilities.showSimpleValidationDialog();			
	}
	
	/**
	 * Find holder .
	 *
	 * @return object holder
	 * @throws ServiceException service exception
	 */
	public Holder findHolder() throws ServiceException
	{
		Holder holderFilter = new Holder();			
		if(IssuerDocumentType.RUC.getCode().equals(issuer.getDocumentType()))
			holderFilter.setDocumentType(DocumentType.RUC.getCode());
		else {
			if(IssuerDocumentType.DIO.getCode().equals(issuer.getDocumentType()))
				holderFilter.setDocumentType(DocumentType.DIO.getCode());
			else
			{
				if(IssuerDocumentType.CI.getCode().equals(issuer.getDocumentType()))
					holderFilter.setDocumentType(DocumentType.CI.getCode());
				else
				{
					if(IssuerDocumentType.CIE.getCode().equals(issuer.getDocumentType()))
						holderFilter.setDocumentType(DocumentType.CIE.getCode());
					else
					{
						if(IssuerDocumentType.CDN.getCode().equals(issuer.getDocumentType()))
							holderFilter.setDocumentType(DocumentType.CDN.getCode());
						else
							holderFilter.setDocumentType(DocumentType.PAS.getCode());
					}
				}
			}
		}
		holderFilter.setDocumentNumber(issuer.getDocumentNumber());				
		holderFilter.setDocumentSource(issuer.getDocumentSource());
		
		return issuerServiceFacade.getHolderByDocumentTypeAndDocumentNumberServiceFacade(holderFilter);
	}
	
	/**
	 * Find Issuer.
	 *
	 * @return object Issuer
	 * @throws ServiceException service exception
	 */
	public Issuer findIssuer() throws ServiceException
	{
		Issuer issuerFilterValidations = new Issuer();
		issuerFilterValidations.setDocumentType(issuer.getDocumentType());
		issuerFilterValidations.setDocumentNumber(issuer.getDocumentNumber());			
		if(Validations.validateIsNotNullAndNotEmpty(issuer.getBusinessName()))
			issuerFilterValidations.setBusinessName(issuer.getBusinessName());				
		return issuerServiceFacade.findIssuerByFiltersServiceFacade(issuerFilterValidations);
	}
	
	/**
	 * Find issuer national or foreign.
	 *
	 * @param holder the holder
	 * @return the issuer
	 * @throws ServiceException the service exception
	 */
	public Issuer findIssuerNationalOrForeign(Holder holder)throws ServiceException
	{
		Issuer issuerFilterValidations = new Issuer();
		issuerFilterValidations.setDocumentType(issuer.getDocumentType());
		issuerFilterValidations.setDocumentNumber(issuer.getDocumentNumber());
		if(IssuerDocumentType.DIO.getCode().equals(issuer.getDocumentType())||IssuerDocumentType.CIE.getCode().equals(issuer.getDocumentType())
				||IssuerDocumentType.PASAPORTE.getCode().equals(issuer.getDocumentType())){
			issuerFilterValidations.setBusinessName(holder.getFullName());
		}
		return	issuerServiceFacade.findIssuerByFiltersServiceFacade(issuerFilterValidations);
	}
	
	/**
	 * Validate document and description issuer.
	 */
	@LoggerAuditWeb
	public void validateDocumentAndDescriptionIssuer(){		
		try{
			if(isIndIssuerForeign() && Validations.validateIsNotNullAndNotEmpty(issuer.getBusinessName())){
				if(Validations.validateIsNullOrEmpty(issuer.getDocumentNumber())){
					hideDialogsListener(null);
					viewMessageRequestIssuerDocumentNumber();
					issuer.setBusinessName(null);
					cleanHolderIssuer();				
					return;
				}								
				Issuer issuerResult = findIssuer();				
				if(Validations.validateIsNotNull(issuerResult)){
					hideDialogsListener(null);
					viewMessageSenderFound(issuerResult);
		    		issuer.setDocumentNumber(null);
		    		issuer.setBusinessName(null);
		    		cleanHolderIssuer();
				}
				else
				{
					List<OfacPerson> lstOfacPerson = findOfacPerson(issuer.getBusinessName());
					if(Validations.validateIsNullOrNotPositive(lstOfacPerson.size()))
					{
						List<PepPerson> lstPepPerson= findPepPerson(issuer.getBusinessName());
						if(Validations.validateIsNullOrNotPositive(lstPepPerson.size()))
						{
							List<PnaPerson> lstPnaPerson = findPnaPerson(issuer.getBusinessName());
							if(Validations.validateIsNullOrNotPositive(lstPnaPerson.size()))
								hideDialogsListener(null);				
							else{
								hideDialogsListener(null);
								viewMessageThereIsNoCodeInTypeDocumentPna();
								cleanObjectForDocumentIssuer();						
								cleanHolderIssuer();
							}
						}
						else {
							hideDialogsListener(null);
							viewMessageThereIsNoCodeInTypeDocumentPep();
							cleanObjectForDocumentIssuer();						
							cleanHolderIssuer();
						}
					}
					else {
						hideDialogsListener(null);
						viewMessageThereIsNoCodeInTypeDocumentOfac();	
						cleanObjectForDocumentIssuer();						
						cleanHolderIssuer();
					}
				}
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * view Message Holder Is Not Been Registered.
	 *
	 * @param holder object Holder
	 */
	public void viewMessageHolderIsNotBeenRegistered(Holder holder)
	{
		StringBuilder sbMessageHolder = new StringBuilder();
		sbMessageHolder.append(DocumentType.get(holder.getDocumentType()).name()).
		append(GeneralConstants.BLANK_SPACE).
		append(holder.getDocumentNumber());
		Object[] arrBodyData = {sbMessageHolder.toString()};	
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_HOLDER_FOUND_NOT_REGISTERED,arrBodyData));
		JSFUtilities.showSimpleValidationDialog();		
	}
	
	/**
	 * View Message Sender Found.
	 *
	 * @param issuerResult object Issuer
	 */
	public void viewMessageSenderFound(Issuer issuerResult)
	{
		StringBuilder sbBodyData = new StringBuilder();
		sbBodyData.append(IssuerDocumentType.get(issuerResult.getDocumentType()).getValue());
		sbBodyData.append(GeneralConstants.HYPHEN);
		sbBodyData.append(issuerResult.getDocumentNumber());
		sbBodyData.append(GeneralConstants.HYPHEN);
		sbBodyData.append(issuerResult.getBusinessName());									
		Object[] arrBodyData = {sbBodyData.toString()};
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_ISSUER_DOC_NUM_FOREIGN_REPEATED,arrBodyData));
		JSFUtilities.showSimpleValidationDialog();		
	}
	
	/**
	 * View message issuer is already registered.
	 *
	 * @param issuerResult the issuer result
	 */
	public void viewMessageIssuerIsAlreadyRegistered(Issuer issuerResult)
	{
		StringBuilder sbBodyData = new StringBuilder();
		sbBodyData.append(IssuerDocumentType.get(issuerResult.getDocumentType()).getValue());
		sbBodyData.append(GeneralConstants.HYPHEN);
		sbBodyData.append(issuerResult.getDocumentNumber());
		if(IssuerDocumentType.DIO.getCode().equals(issuer.getDocumentType())||IssuerDocumentType.CIE.getCode().equals(issuer.getDocumentType())
				||IssuerDocumentType.PASAPORTE.getCode().equals(issuer.getDocumentType())){
			sbBodyData.append(GeneralConstants.HYPHEN);
			sbBodyData.append(issuerResult.getBusinessName());
		}											
		Object[] arrBodyData = {sbBodyData.toString()};						
		if(IssuerDocumentType.DIO.getCode().equals(issuer.getDocumentType())||IssuerDocumentType.CIE.getCode().equals(issuer.getDocumentType())
				||IssuerDocumentType.PASAPORTE.getCode().equals(issuer.getDocumentType())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_ISSUER_DOC_NUM_FOREIGN_REPEATED,arrBodyData));
		} else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_ISSUER_DOC_NUM_NATIONAL_REPEATED,arrBodyData));
		}						
		JSFUtilities.showSimpleValidationDialog();	
	}
	/**
	 * Validate mnemonic participant.
	 */
	@LoggerAuditWeb
	public void validateMnemonicIssuer(){
		try{			
			if(Validations.validateIsNotNullAndNotEmpty(issuer.getMnemonic())){		
				Issuer issuerFilterValidations = new Issuer();
				issuerFilterValidations.setMnemonic(issuer.getMnemonic());				
				Issuer issuerResult =issuerServiceFacade.findIssuerByFiltersServiceFacade(issuerFilterValidations);				
				if(Validations.validateIsNotNull(issuerResult)){
					hideDialogsListener(null);
					viewMessageUsedMnemonic(issuerResult);
					issuer.setMnemonic(null);
				}
				if(indFICPATEconomicActivity){
					issuer.setFundType(issuer.getMnemonic());
				}
			}
			else hideDialogsListener(null);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * View Message Used Mnemonic.
	 *
	 * @param issuerResult Object issuer
	 */
	public void viewMessageUsedMnemonic(Issuer issuerResult)
	{
		StringBuilder sbBodyData = new StringBuilder();
		sbBodyData.append(String.valueOf(issuerResult.getIdIssuerPk()));
		if(issuerResult.getBusinessName()!=null){
			sbBodyData.append(GeneralConstants.HYPHEN);
			sbBodyData.append(issuerResult.getBusinessName());
		}										
		Object[] arrBodyData = {sbBodyData.toString()};		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities.getMessage(PropertiesConstants.ERROR_ADM_ISSUER_MNEMONIC_REPEATED,arrBodyData));
		JSFUtilities.showSimpleValidationDialog();			
	}
	/**
	 * Change selected economic sector.
	 */
	@LoggerAuditWeb
	public void changeSelectedEconomicSector(){
		try{
			hideDialogsListener(null);
//			ParameterTableTO filterParameterTable = new ParameterTableTO();
//			filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
//			filterParameterTable.setMasterTableFk(MasterTableType.ECONOMIC_ACTIVITY.getCode());
//			if(!EconomicSectorType.MIXTO.getCode().equals( issuer.getEconomicSector() )){
	//			filterParameterTable.setIdRelatedParameterFk(issuer.getEconomicSector());
			//}
			lstEconomicActivity=new ArrayList<ParameterTable>();
			lstSocietyType=new ArrayList<ParameterTable>();
			List<ParameterTable> lstEconomicActivityAux = new ArrayList<ParameterTable>();
			lstEconomicActivityAux = generalParametersFacade.getListEconomicActivityBySector(issuer.getEconomicSector());
			if(!indFICPATEconomicActivity){
				for(ParameterTable param : lstEconomicActivityAux){
					if(!param.getParameterTablePk().equals(EconomicActivityType.FONDOS_INVERSION_CERRADOS.getCode())
							|| !param.getParameterTablePk().equals(EconomicActivityType.PATRIMONIO_AUTONOMO_TITULARIZACION.getCode())){
						lstEconomicActivity.add(param);
					}
					
				}
			}
			else{
				lstEconomicActivity = lstEconomicActivityAux;
			}
			
			//	lstEconomicActivity=generalParametersFacade.GETl
			lstSocietyType = generalParametersFacade.getListSocietyTypeBySector(issuer.getEconomicSector());
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Load Province Department.
	 */
	public void loadProvinceDepartment()
	{
		hideDialogsListener(null);	
		try{
			if(!Validations.validateIsNullOrNotPositive(issuer.getDepartment()))
			{
				lstProvinces=getLstProvinces(issuer.getDepartment());
				lstDistricts = null;
			}
			else
			{								
				lstProvinces = null;				
				lstDistricts = null;
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Load District Province.
	 */
	public void loadDistrictProvince()
	{
		hideDialogsListener(null);	
		try{
			if(!Validations.validateIsNullOrNotPositive(issuer.getProvince()))
				lstDistricts=getLstDistricts(issuer.getProvince());
			else						
				lstDistricts = null;			
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Zero to validate string.
	 *
	 * @param str  validate string
	 * @return string to validate
	 */
	public boolean validateNotZero(String str)
	{
		boolean charNotZero = false;
		for (char charStr : str.toCharArray()) {
			if(charStr!='0'){
				charNotZero = true;
				break;
			}
		} 
		return charNotZero;
	}
	/**
	 * Validate phone number format.
	 */
	public void validatePhoneNumberFormat(){
		hideDialogsListener(null);
    	if(Validations.validateIsNotNullAndNotEmpty(issuer.getPhoneNumber())){      			
    		if(!validateNotZero(issuer.getPhoneNumber())){    		
    			/*showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
    					PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_VALIDATION_PHONE_ONLY_ZERO));
    			JSFUtilities.showSimpleValidationDialog();*/			
    			issuer.setPhoneNumber(null);
    		}
    	}
	}
	/**
	 * Validate fax number format.
	 */
	public void validateFaxNumberFormat(){
		hideDialogsListener(null);
    	if(Validations.validateIsNotNullAndNotEmpty(issuer.getFaxNumber())){    			
    		if(!validateNotZero(issuer.getFaxNumber())){    			
    			/*showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
    					PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_VALIDATION_FAX_ONLY_ZERO));
    			JSFUtilities.showSimpleValidationDialog();*/				
    			issuer.setFaxNumber(null);
    		}
    	}
	}
	/**
	 * Validate web page format.
	 */
	public void validateWebPageFormat(){
		hideDialogsListener(null);
    	if(Validations.validateIsNotNullAndNotEmpty(issuer.getWebsite())){
    		if(!CommonsUtilities.matchWebPage(issuer.getWebsite())){
    			/*showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
    					PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_VALIDATION_WEB_PAGE_FORMAT));
    			JSFUtilities.showSimpleValidationDialog();*/		
    			//Clean the web site
    			issuer.setWebsite(null);
    		}
    	}
	}
	/**
	 * Validate email format.
	 */
	public void validateEmailFormat(){
		hideDialogsListener(null);
    	if(Validations.validateIsNotNullAndNotEmpty(issuer.getEmail())){    		
    		if(!CommonsUtilities.matchEmail(issuer.getEmail())){    		
    			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
    					PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_VALIDATION_EMAIL_FORMAT));
    			JSFUtilities.showSimpleValidationDialog();				
    			issuer.setEmail(null);
    		}
    	}
	}
	/**
	 * Validate contact phone number format.
	 */
	public void validateContactPhoneNumberFormat(){
		hideDialogsListener(null);
    	if(Validations.validateIsNotNullAndNotEmpty(issuer.getContactPhone())){    		
    		if(!validateNotZero(issuer.getContactPhone())){    			
    			/*showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
    					PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_VALIDATION_CONTACT_PHONE_ONLY_ZERO));
    			JSFUtilities.showSimpleValidationDialog();*/    					
    			issuer.setContactPhone(null);
    		}
    	}
	}
	
	
	
	/**
	 * Validate contact email format.
	 */
	public void validateContactEmailFormat(){	
		hideDialogsListener(null);
    	if(Validations.validateIsNotNullAndNotEmpty(issuer.getContactEmail())){    		
    		if(!CommonsUtilities.matchEmail(issuer.getContactEmail())){
    			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
    					PropertiesUtilities.getMessage(PropertiesConstants.ADM_ISSUER_VALIDATION_CONTACT_EMAIL_FORMAT));
    			JSFUtilities.showSimpleValidationDialog();    	
    			issuer.setContactEmail(null);
    		}
    	}
	}
	
	/**
	 * Issuer Differentiated.
	 */
	public void checkIssuerDifferentiated(){
		try {
			if(indIssuerDifferentiated) {
				if(Validations.validateListIsNotNullAndNotEmpty(legalRepresentativeHelperBean.getLstLegalRepresentativeHistory())){
					for (LegalRepresentativeHistory objLegalRepresentativeHistory : legalRepresentativeHelperBean.getLstLegalRepresentativeHistory()) {
						if(BooleanType.NO.getCode().equals(objLegalRepresentativeHistory.getIndResidence())){
							legalRepresentativeHelperBean.setLstLegalRepresentativeHistory(new ArrayList<LegalRepresentativeHistory>());
							showMessageOnDialog(PropertiesConstants.LBL_WARNING_ACTION,null,PropertiesConstants.WARNING_DELETE_REPRESENTATIVE_NO_RESIDENT,null);							
							JSFUtilities.executeJavascriptFunction("cnfwDifferentiated.show();");
							return;
						}
					}
				}
				loadIssuerAttachmentTypeDifferentiated();
				resetIssuerFiles();
			} else {
				loadIssuerAttachmentTypeNational();
				resetIssuerFiles();
			}
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Reset Issuer Files.
	 */
	private void resetIssuerFiles(){
		lstIssuerFiles = new ArrayList<IssuerFile>();
		issuerFileNameDisplay = null;
		setAttachIssuerFileSelected(null);
	}
	
	/**
	 * Action do investment funds closed and legacy.
	 *
	 * @param option the option
	 */
	public void actionDoInvestmentFundsClosedAndLegacy(boolean option){
		try{
		
		if(option){
			loadCombosIssuerManagement();
			indFICPATEconomicActivity=true;
			ParameterTableTO filterParameterTable = new ParameterTableTO();
			filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());		
			loadListStateSearch(filterParameterTable);	
			loadListEconomicSectorSearch(filterParameterTable);
			if(issuerResult!=null){
				issuer = issuerResult;
				issuer.setMnemonic(null);
				issuer.setRelatedCui(issuerResult.getHolder().getIdHolderPk());
				issuer.setSocialCapital(null);
				issuer.setCurrency(null);
				issuer.setHolder(new Holder());
				issuer.setIndHolderWillBeCreated(Boolean.TRUE);
				issuer.setSocietyType(null);
				changeSelectedEconomicSector();
				
				if(issuerResult.getEconomicActivity().equals(EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode())){
					issuer.setEconomicActivity(EconomicActivityType.FONDOS_INVERSION_CERRADOS.getCode());
				}
				if(issuerResult.getEconomicActivity().equals(EconomicActivityType.SOCIEDADES_TITULARIZACION.getCode())){
					issuer.setEconomicActivity(EconomicActivityType.PATRIMONIO_AUTONOMO_TITULARIZACION.getCode());
				}
				buildInvestorTypeByEconAcSelected(issuer.getEconomicActivity());
				
				GeographicLocation geo = new GeographicLocation();
				geo  = issuerResult.getGeographicLocation();
				issuer.setGeographicLocation(geo);
				
				lstDepartments = getLstDepartments(issuerResult.getGeographicLocation().getIdGeographicLocationPk());
				lstProvinces = getLstProvinces(issuerResult.getDepartment());
				lstDistricts = getLstDistricts(issuerResult.getProvince());
				
				if(issuerResult.getContractDate()!=null || issuerResult.getClosingServiceDate()!=null){
					indIssuerDepositaryContract = true;
				}else{
					indIssuerDepositaryContract = false;
				}
				
				if(issuerResult.getEnrolledStockExchDate()!=null || issuerResult.getRetirementDate()!=null){
					indInscriptionStockExchange = true;
				}
				else{
					indInscriptionStockExchange = false;
				}
					
				lstIssuerFiles = issuerServiceFacade.getListIssuerFilesServiceFacade(issuer);	    			    		
		    	//setDetailIssuerFile(lstIssuerFiles);
		    	
		    	if(lstIssuerFiles!=null && lstIssuerFiles.size()>0){
		    		for(IssuerFile isf:lstIssuerFiles){
		    			isf.setIdIssuerFilePk(null);
		    			isf.setIssuer(issuer);
		    		}
		    	}
		    	
		    	legalRepresentativeHelperBean.initializeLstLegalRepresentativeHistory();
				lstRepresentedEntity = issuerServiceFacade.getListRepresentedEntityByIssuerServiceFacade(issuer);
		    	if (Validations.validateListIsNotNullAndNotEmpty(lstRepresentedEntity)){ 
		    			setLegalRepresentativeHistory(lstRepresentedEntity);	
				}
		    	if(legalRepresentativeHelperBean.getLstLegalRepresentativeHistory()!=null && 
		    			legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size()>0){
		    	   for(LegalRepresentativeHistory objLRH : legalRepresentativeHelperBean.getLstLegalRepresentativeHistory()){
		    		   objLRH.setIdRepresentativeHistoryPk(null);
		    	   }
		    	}
		    	LegalRepresentativeTO legalRepresentativeTO = new LegalRepresentativeTO();
				legalRepresentativeTO.setViewOperationType(ViewOperationsType.REGISTER.getCode());
				legalRepresentativeTO.setFlagIssuerOrParticipant(BooleanType.YES.getBooleanValue());
				issuer.setIdIssuerPk(null);
				indIssuerDifferentiated = BooleanType.YES.getCode().equals(issuer.getIndDifferentiated());
				if(indIssuerDifferentiated){
				   loadIssuerAttachmentTypeDifferentiated();
				}
			}
			else if(holderResult!=null){
				indIssuerDepositaryContract = false;
				indInscriptionStockExchange = false;
				holderResult = accountsFacade.getHolderServiceFacade(holderResult.getIdHolderPk());
				issuer = new Issuer();
				issuer.setMnemonic(null);
				issuer.setSocialCapital(null);
				issuer.setSocietyType(null);
				issuer.setCurrency(null);
				issuer.setHolder(new Holder());
				issuer.setEconomicSector(holderResult.getEconomicSector());
				changeSelectedEconomicSector();
				issuer.setDocumentType(holderResult.getDocumentType());
				issuer.setDocumentNumber(holderResult.getDocumentNumber());
				issuer.setRelatedCui(holderResult.getIdHolderPk());;
				issuer.setIndHolderWillBeCreated(Boolean.TRUE);
				if(holderResult.getEconomicActivity().equals(EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode())){
					issuer.setEconomicActivity(EconomicActivityType.FONDOS_INVERSION_CERRADOS.getCode());
				}
				if(holderResult.getEconomicActivity().equals(EconomicActivityType.SOCIEDADES_TITULARIZACION.getCode())){
					issuer.setEconomicActivity(EconomicActivityType.PATRIMONIO_AUTONOMO_TITULARIZACION.getCode());
				}
				buildInvestorTypeByEconAcSelected(issuer.getEconomicActivity());
				
				issuer.setBusinessName(null);
				issuer.setMnemonic(null);
				issuer.setFundAdministrator(holderResult.getMnemonic());
				issuer.setDocumentNumber(holderResult.getDocumentNumber());
				issuer.setDocumentSource(holderResult.getDocumentSource());
				issuer.setDepartment(holderResult.getLegalDepartment());
				issuer.setProvince(holderResult.getLegalProvince());
				issuer.setDistrict(holderResult.getLegalDistrict());
				
				lstDepartments = getLstDepartments(holderResult.getNationality());
				lstProvinces = getLstProvinces(holderResult.getLegalDepartment());
				lstDistricts = getLstDistricts(holderResult.getLegalProvince());
				
				GeographicLocation geo = new GeographicLocation();
				geo.setIdGeographicLocationPk(holderResult.getNationality());
				issuer.setGeographicLocation(geo);
				
				
				issuer.setPhoneNumber(holderResult.getHomePhoneNumber());
				issuer.setFaxNumber(holderResult.getFaxNumber());
				issuer.setEmail(holderResult.getEmail());
				issuer.setContactEmail(holderResult.getEmail());
				
				legalRepresentativeHelperBean.initializeLstLegalRepresentativeHistory();
				
				if (Validations.validateListIsNotNullAndNotEmpty(holderResult.getRepresentedEntity())){ 
		    			setLegalRepresentativeHistory(holderResult.getRepresentedEntity());	
				}
				if(legalRepresentativeHelperBean.getLstLegalRepresentativeHistory()!=null && 
		    			legalRepresentativeHelperBean.getLstLegalRepresentativeHistory().size()>0){
		    	   for(LegalRepresentativeHistory objLRH : legalRepresentativeHelperBean.getLstLegalRepresentativeHistory()){
		    		   objLRH.setIdRepresentativeHistoryPk(null);
		    	   }
		    	}
				LegalRepresentativeTO legalRepresentativeTO = new LegalRepresentativeTO();
				legalRepresentativeTO.setViewOperationType(ViewOperationsType.REGISTER.getCode());
				legalRepresentativeTO.setFlagIssuerOrParticipant(BooleanType.YES.getBooleanValue());
				issuer.setIdIssuerPk(null);
				indIssuerDifferentiated = BooleanType.YES.getCode().equals(issuer.getIndDifferentiated());
				if(indIssuerDifferentiated){
				   loadIssuerAttachmentTypeDifferentiated();
				}
			}
			
			JSFUtilities.resetViewRoot();
			JSFUtilities.executeJavascriptFunction("cnfwIssuerInvestmentFundsClosedAndLegacy.hide()");
			
		}
		else{
			cleanIssuerRegistry();
			indFICPATEconomicActivity=false;
			JSFUtilities.resetViewRoot();
			JSFUtilities.executeJavascriptFunction("cnfwIssuerInvestmentFundsClosedAndLegacy.hide()");
			
		}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Action back fund dialog.
	 */
	public void actionBackFundDialog(){
		JSFUtilities.resetViewRoot();
		JSFUtilities.executeJavascriptFunction("wdlgFundInformation.hide()");
		validateIssuerInvestmentFundsClosedAndLegacy(issuer.getDocumentNumber(),issuer.getDocumentSource(),issuer.getDocumentType(),issuer.getGeographicLocation().getIdGeographicLocationPk());
	}
	
	/**
	 * Validate issuer investment funds closed and legacy.
	 *
	 * @param documentNumber the document number
	 * @param documentSource the document source
	 * @param documentType the document type
	 * @param nationality the nationality
	 * @return true, if successful
	 */
	public boolean validateIssuerInvestmentFundsClosedAndLegacy(String documentNumber,Integer documentSource,Integer documentType,Integer nationality){
		
		boolean flagExists=false;
		Object[] bodyData;
		indFICPATEconomicActivity=false;
	    
		try {
	    
	    if(nationality!=null && nationality.equals(countryResidence)
				&& documentType!=null && documentType.equals(DocumentType.RUC.getCode())){
			Issuer filter = new Issuer();
			filter.setDocumentType(documentType);
			filter.setDocumentNumber(documentNumber);
			filter.setDocumentSource(documentSource);
			List<Integer> listEAI = new ArrayList<Integer>();
			listEAI.add(EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode());
			listEAI.add(EconomicActivityType.SOCIEDADES_TITULARIZACION.getCode());
			filter.setListSectorActivity(listEAI);
			issuerResult = issuerServiceFacade.findIssuerByFiltersServiceFacade(filter);    	
	    	
			if(Validations.validateIsNotNull(issuerResult)){    		
			   holderResult=null;
			if(Validations.validateIsNotNull(issuerResult.getEconomicActivity()) && 
					(issuerResult.getEconomicActivity().equals(EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode())
		    		|| issuerResult.getEconomicActivity().equals(EconomicActivityType.SOCIEDADES_TITULARIZACION.getCode()))){
				
				bodyData = new Object[0];
				flagExists=true;
				
				if(issuerResult.getEconomicActivity().equals(EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode())){
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.ISSUER_INVESTMENT_FUNDS_CLOSED,bodyData));
					JSFUtilities.executeJavascriptFunction("cnfwIssuerInvestmentFundsClosedAndLegacy.show()");
					
				}
				else if(issuerResult.getEconomicActivity().equals(EconomicActivityType.SOCIEDADES_TITULARIZACION.getCode())){
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.ISSUER_SOCIETY_SECURITIZATION,bodyData));
					JSFUtilities.executeJavascriptFunction("cnfwIssuerInvestmentFundsClosedAndLegacy.show()");
					
				}				
		    }
		}else{
			
				Holder holder = new Holder();
				holder.setDocumentType(documentType);
				holder.setDocumentNumber(documentNumber);
				holder.setDocumentSource(documentSource);
				List<Integer> listEAH = new ArrayList<Integer>();
				listEAH.add(EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode());
				listEAH.add(EconomicActivityType.SOCIEDADES_TITULARIZACION.getCode());
				holder.setListSectorActivity(listEAH);
				
				holderResult = accountsFacade.getHolderByDocumentTypeAndDocumentNumberServiceFacade(holder);
				
				if(Validations.validateIsNotNull(holderResult)){    		
					issuerResult=null;
					if(Validations.validateIsNotNull(holderResult.getEconomicActivity()) &&
						(holderResult.getEconomicActivity().equals(EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode())
				    	|| holderResult.getEconomicActivity().equals(EconomicActivityType.SOCIEDADES_TITULARIZACION.getCode()))){
						flagExists=true; 
						bodyData = new Object[0];
						
						JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
								PropertiesUtilities.getMessage(PropertiesConstants.ISSUER_ONLY_CREATE,bodyData));
						JSFUtilities.executeJavascriptFunction("cnfwIssuerInvestmentFundsClosedAndLegacyCreateIssuerOnly.show()");
					 
				    } 

				}
			}
		
	    }
	    
	}
	catch(Exception e){
		e.printStackTrace();
	}
	
	    return flagExists;
		
	}
	
	/**
	 * Action do about create issuer only.
	 *
	 * @param option the option
	 */
	public void actionDoAboutCreateIssuerOnly(boolean option){
		try{
		Object[] bodyData = new Object[0];
		JSFUtilities.resetViewRoot();
		JSFUtilities.executeJavascriptFunction("cnfwIssuerInvestmentFundsClosedAndLegacyCreateIssuerOnly.hide()");
		
		if(!option){
			if(holderResult.getEconomicActivity().equals(EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode())){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ISSUER_INVESTMENT_FUNDS_CLOSED,bodyData));
				JSFUtilities.executeJavascriptFunction("cnfwIssuerInvestmentFundsClosedAndLegacy.show()");
			}
			else if(holderResult.getEconomicActivity().equals(EconomicActivityType.SOCIEDADES_TITULARIZACION.getCode())){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ISSUER_SOCIETY_SECURITIZATION,bodyData));
				JSFUtilities.executeJavascriptFunction("cnfwIssuerInvestmentFundsClosedAndLegacy.show()");
			}	
		}
		else{
			//CUI valid code
			Holder holder = findHolder();			
			if(Validations.validateIsNotNull(holder)){				
				if(!(HolderStateType.BLOCKED.getCode().equals(holder.getStateHolder())) &&
				   !(HolderStateType.REGISTERED.getCode().equals(holder.getStateHolder()))){
					hideDialogsListener(null);
					viewMessageHolderIsNotBeenRegistered(holder);					
					cleanObjectForDocumentIssuer();
					cleanHolderIssuer();
				} else {					
						hideDialogsListener(null);
						setDetailsOfTheHolder(holder);	
						
						//asignando por defecto la ctividad economica STI Sociedad de Titularizacion
						//issuerMgmtBean.issuer.economicActivity
						issuer.setEconomicSector(EconomicSectorType.NON_FINANCIAL_PUBLIC_SECTOR.getCode());
						changeSelectedEconomicSector();
						issuer.setEconomicActivity(EconomicActivityType.SOCIEDADES_TITULARIZACION.getCode());
				}
			}
		}
		}
		catch(Exception e){
		   e.printStackTrace();
		}
	}
	
	public void changeEconomicActivity(){
		if(Validations.validateIsNotNull(issuer) && Validations.validateIsNotNull(issuer.getEconomicActivity())){
			buildInvestorTypeByEconAcSelected(issuer.getEconomicActivity());
		}
	}
	
	public void buildInvestorTypeByEconAcSelected(Integer economicActivity){
		// construyendo la lista de tipo de inversionista
		InvestorTypeModelTO it = new InvestorTypeModelTO();
		it.setEconomicActivity(economicActivity);
		try {
			listInvestorType = issuerServiceFacade.getLstInvestorTypeModelTO(it);
		} catch (ServiceException e) {
			e.printStackTrace();
			listInvestorType = new ArrayList<>();
		}
	}
	
	/**
	 * Gets the lst document source.
	 *
	 * @return the lst document source
	 */
	public void getLstDocumentSource() {

		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(MasterTableType.DOCUMENT_SOURCE.getCode());
			
			
			listDocumentSource = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}
	
	
	/**
	 * Bill parameters table map.
	 *
	 * @param lstParameterTable the lst parameter table
	 */
	public void billParametersTableMap(List<ParameterTable> lstParameterTable){
		billParametersTableMap(lstParameterTable,false);
	}
	
	/**
	 * Bill parameters table map.
	 *
	 * @param lstParameterTable the lst parameter table
	 * @param isObj the is obj
	 */
	public void billParametersTableMap(List<ParameterTable> lstParameterTable, boolean isObj){
		if(super.getParametersTableMap()==null){
			super.setParametersTableMap(new HashMap<Integer,Object>());
		}
		for(ParameterTable pTable : lstParameterTable){
			if(isObj){
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable);
			}else{
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable.getParameterName());
			}
		}
	}
	
	/**
	 * Detail confirm issuer register.
	 * 
	 * @return the string
	 */
	@LoggerAuditWeb
	public String detailConfirmIssuerRegister() {
		try {
			hideDialogsListener(null);

			if (Validations.validateIsNull(issuer)) {
				showMessageOnDialog(
						PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities
								.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
				return null;
			}

			if (!IssuerStateType.PENDING.getCode().equals(
					issuer.getStateIssuer())) {
				showMessageOnDialog(
						PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities
								.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_NOT_IN_PENDIG_STATE));
				JSFUtilities.showSimpleValidationDialog();
				return null;
			}
			
			idIssuerPk = issuer.getIdIssuerPk();
			setViewOperationType(ViewOperationsType.CONFIRM.getCode());
			Issuer issuerFilter = new Issuer();
	    	issuerFilter.setIdIssuerPk(idIssuerPk);    
	    	Issuer issuerResult = findIssuerInformation(issuerFilter);			
	    	if(Validations.validateIsNotNull(issuerResult)) {
	    		if(Validations.validateIsNotNull(issuerResult.getEconomicActivity())){
	    			buildInvestorTypeByEconAcSelected(issuerResult.getEconomicActivity());
	    		}else{
	    			buildInvestorTypeByEconAcSelected(null);
	    		}
	    		setIndicatorDetailIssuer();				
	    		lstIssuerFiles = issuerServiceFacade.getListIssuerFilesServiceFacade(issuerFilter);	    			    		
	    		setDetailIssuerFile(lstIssuerFiles);	    			    		
	    		lstRepresentedEntity = issuerServiceFacade.getListRepresentedEntityByIssuerServiceFacade(issuerFilter);
	    		issuer = issuerResult;	    		
	    		if (Validations.validateListIsNotNullAndNotEmpty(lstRepresentedEntity)) 
	    			setLegalRepresentativeHistory(lstRepresentedEntity);					    		
	    		return WINDOW_DETAIL_ISSUER;
	    	} else {
	    		return null;
	    	}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}
	
	/**
	 * Modify issuer register.
	 * 
	 * @return the string
	 */
	@LoggerAuditWeb
	public String modifyIssuerRegister() {
		try {
			hideDialogsListener(null);

			if (Validations.validateIsNull(issuer)) {
				showMessageOnDialog(
						PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities
								.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
				return null;
			}

			if (!IssuerStateType.PENDING.getCode().equals(
					issuer.getStateIssuer())) {
				showMessageOnDialog(
						PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities
								.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_NOT_IN_PENDIG_STATE));
				JSFUtilities.showSimpleValidationDialog();
				return null;
			}
			idIssuerPk = issuer.getIdIssuerPk();
			setViewOperationType(ViewOperationsType.MODIFY.getCode());
			Issuer issuerFilter = new Issuer();
	    	issuerFilter.setIdIssuerPk(idIssuerPk);    
	    	Issuer issuerResult = findIssuerInformation(issuerFilter);			
	    	if(Validations.validateIsNotNull(issuerResult)) {
	    		buildInvestorTypeByEconAcSelected(null);
	    		setIndicatorDetailIssuer();
	    		validateSelectedCountry();
	    		lstIssuerFiles = issuerServiceFacade.getListIssuerFilesServiceFacade(issuerFilter);	    			    		
	    		setDetailIssuerFile(lstIssuerFiles);	    			    		
	    		lstRepresentedEntity = issuerServiceFacade.getListRepresentedEntityByIssuerServiceFacade(issuerFilter);
	    		issuer = issuerResult;	    		
	    		if (Validations.validateListIsNotNullAndNotEmpty(lstRepresentedEntity)) 
	    			setLegalRepresentativeHistory(lstRepresentedEntity);					    		
	    		return "registerIssuerRule";
	    	} else {
	    		return null;
	    	}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}
		
	/**
	 * Begin confirm issuer request.
	 */
	@LoggerAuditWeb
	public void beginConfirmIssuerRequest() {
		try {
			hideDialogsListener(null);

			if (Validations.validateIsNull(issuer)) {
				showMessageOnDialog(
						PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities
								.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}

			if (!IssuerStateType.PENDING.getCode().equals(
					issuer.getStateIssuer())) {
				showMessageOnDialog(
						PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities
								.getMessage(PropertiesConstants.ADM_ISSUER_REQUEST_VALIDATION_NOT_IN_PENDIG_STATE));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}

			Object[] arrBodyData = { issuer.getMnemonic(),// getTypeDescription(),
					String.valueOf(issuer.getDocumentNumber())};// getNumber()) };

			showMessageOnDialog(
					PropertiesConstants.LBL_CONFIRM_HEADER_RECORD,
					null,
					PropertiesConstants.LBL_CONFIRM_ISSUER_ONFIRMATION,
					arrBodyData);
			JSFUtilities.showComponent("cnfIssuerRequestConfirmation");

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Save confirm issuer request.
	 */
	@LoggerAuditWeb
	public void saveConfirmIssuerRequest() {
		try {
			
			JSFUtilities.executeJavascriptFunction("cnfwIssuerRequestConfirmation.hide()");
			if(Validations.validateIsNotNullAndNotEmpty(issuer.getHolder()) && Validations.validateIsNotNullAndNotEmpty(issuer.getHolder().getIdHolderPk())) {
				issuer.setIndHolderWillBeCreated(false);
			}else {
				issuer.setIndHolderWillBeCreated(true);
				issuer.setHolder(new Holder());
			}
    		validateHolderAndSetFile();
			issuerServiceFacade.confirmIssuerServiceFacade(issuer, lstRepresentedEntity);
			processNotificationSaveIssuer();
			
			//Mensaje exito
			Object[] arrBodyData = { String.valueOf(issuer.getMnemonic()), String.valueOf(issuer.getHolder().getIdHolderPk()) };
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), PropertiesUtilities.getMessage(PropertiesConstants.LBL_SUCCESS_ISSUER_AND_HOLDER_CONFIRM, arrBodyData));
			JSFUtilities.showComponent("cnfEndTransactionIssuer");
			cleanDataModel();
			validateUserAccountSession();

			setViewOperationType(ViewOperationsType.CONSULT.getCode());
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Sets the true valid buttons.
	 */
	public void setTrueValidButtons() {
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		if (userPrivilege.getUserAcctions().isConfirm()) {
			privilegeComponent.setBtnConfirmView(true);
		}
		if (userPrivilege.getUserAcctions().isModify()) {
			privilegeComponent.setBtnModifyView(true);
		}
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}
	
	/**
	 * Gets the legal representative helper bean.
	 *
	 * @return the legal representative helper bean
	 */
	public LegalRepresentativeHelperBean getLegalRepresentativeHelperBean() {
		return legalRepresentativeHelperBean;
	}

	/**
	 * Sets the legal representative helper bean.
	 *
	 * @param legalRepresentativeHelperBean the new legal representative helper bean
	 */
	public void setLegalRepresentativeHelperBean(
			LegalRepresentativeHelperBean legalRepresentativeHelperBean) {
		this.legalRepresentativeHelperBean = legalRepresentativeHelperBean;
	}

	/**
	 * Gets the streamed content file.
	 *
	 * @return the streamed content file
	 */
	public StreamedContent getStreamedContentFile() {
		return streamedContentFile;
	}

	/**
	 * Sets the streamed content file.
	 *
	 * @param streamedContentFile the new streamed content file
	 */
	public void setStreamedContentFile(StreamedContent streamedContentFile) {
		this.streamedContentFile = streamedContentFile;
	}

	/**
	 * Gets the list document source.
	 *
	 * @return the list document source
	 */
	public List<ParameterTable> getListDocumentSource() {
		return listDocumentSource;
	}

	/**
	 * Sets the list document source.
	 *
	 * @param listDocumentSource the new list document source
	 */
	public void setListDocumentSource(List<ParameterTable> listDocumentSource) {
		this.listDocumentSource = listDocumentSource;
	}

	/**
	 * Checks if is ind ficpat economic activity.
	 *
	 * @return true, if is ind ficpat economic activity
	 */
	public boolean isIndFICPATEconomicActivity() {
		return indFICPATEconomicActivity;
	}

	/**
	 * Sets the ind ficpat economic activity.
	 *
	 * @param indFICPATEconomicActivity the new ind ficpat economic activity
	 */
	public void setIndFICPATEconomicActivity(boolean indFICPATEconomicActivity) {
		this.indFICPATEconomicActivity = indFICPATEconomicActivity;
	}

	/**
	 * Gets the lst cbo credit rating scales.
	 *
	 * @return the lst cbo credit rating scales
	 */
	public List<ParameterTable> getLstCboCreditRatingScales() {
		return lstCboCreditRatingScales;
	}

	/**
	 * Sets the lst cbo credit rating scales.
	 *
	 * @param lstCboCreditRatingScales the new lst cbo credit rating scales
	 */
	public void setLstCboCreditRatingScales(
			List<ParameterTable> lstCboCreditRatingScales) {
		this.lstCboCreditRatingScales = lstCboCreditRatingScales;
	}

	public String getfUploadFileDocumentsSize() {
		return fUploadFileDocumentsSize;
	}

	public void setfUploadFileDocumentsSize(String fUploadFileDocumentsSize) {
		this.fUploadFileDocumentsSize = fUploadFileDocumentsSize;
	}

	public String getfUploadFileSizeDocumentsDisplay() {
		return fUploadFileSizeDocumentsDisplay;
	}

	public void setfUploadFileSizeDocumentsDisplay(
			String fUploadFileSizeDocumentsDisplay) {
		this.fUploadFileSizeDocumentsDisplay = fUploadFileSizeDocumentsDisplay;
	}

	public List<InvestorTypeModelTO> getListInvestorType() {
		return listInvestorType;
	}

	public void setListInvestorType(List<InvestorTypeModelTO> listInvestorType) {
		this.listInvestorType = listInvestorType;
	}
}
