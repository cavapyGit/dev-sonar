package com.pradera.securities.issuer.to;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ParticipantHistoryStateTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/03/2013
 */
public class IssuerHistoryStateTO implements Serializable{
	
	/** The id participant pk. */
	private String idIssuerPk;
	
	/** The id participant request pk. */
	private Long idIssuerRequestPk;
	
	/**
	 * Instantiates a new participant history state to.
	 */
	public IssuerHistoryStateTO() {
		// TODO Auto-generated constructor stub
	}


	/**
	 * Gets the id issuer request pk.
	 *
	 * @return the id issuer request pk
	 */
	public Long getIdIssuerRequestPk() {
		return idIssuerRequestPk;
	}

	/**
	 * Sets the id issuer request pk.
	 *
	 * @param idIssuerRequestPk the new id issuer request pk
	 */
	public void setIdIssuerRequestPk(Long idIssuerRequestPk) {
		this.idIssuerRequestPk = idIssuerRequestPk;
	}


	/**
	 * Gets the id issuer pk.
	 *
	 * @return the id issuer pk
	 */
	public String getIdIssuerPk() {
		return idIssuerPk;
	}


	/**
	 * Sets the id issuer pk.
	 *
	 * @param idIssuerPk the new id issuer pk
	 */
	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}


}
