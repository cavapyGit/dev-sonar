package com.pradera.securities.issuer.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.integration.common.validation.Validations;

// TODO: Auto-generated Javadoc
/**
 * The Class IssuerTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 03/04/2013
 */
public class IssuerTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7816665012760669996L;

	/** The id issuer pk. */
	private String idIssuerPk;
	
	/** The issuer state. */
	private Integer issuerState;
	
	/** The sort by. */
	private String sortBy;
	
	/** The description. */
	private String description;
	
	/** The business name. */
	private String businessName;
	
	/** The document type. */
	private Integer documentType;
	
	/** The document number. */
	private String documentNumber;
	
	/** The mnemonic. */
	private String mnemonic;
	
	/** The economic sector. */
	private Integer economicSector;
	
	/** The economic activity. */
	private Integer economicActivity;
	
	/** The issuer to instance. */
	private static IssuerTO issuerTOInstance;
	
	/** The lst participant states. */
	private List<Integer> lstIssuerStates;
	
	/** The issuer request to. */
	private IssuerRequestTO issuerRequestTO;
	
	/** The allow search all. */
	private boolean allowSearchAll;
	
	/**
	 * Gets the id issuer pk.
	 *
	 * @return the id issuer pk
	 */
	public String getIdIssuerPk() {
		return idIssuerPk;
	}
	
	/**
	 * Sets the id issuer pk.
	 *
	 * @param idIssuerPk the new id issuer pk
	 */
	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}
	
	/**
	 * Gets the issuer state.
	 *
	 * @return the issuer state
	 */
	public Integer  getIssuerState() {
		return issuerState;
	}
	
	/**
	 * Sets the issuer state.
	 *
	 * @param issuerState the new issuer state
	 */
	public void setIssuerState(Integer issuerState) {
		this.issuerState = issuerState;
	}
	
	/**
	 * Gets the instance from issuer pk.
	 *
	 * @param idIssuerPk the id issuer pk
	 * @return the instance from issuer pk
	 */
	public static IssuerTO getInstanceFromIssuerPK(String idIssuerPk){
		if(Validations.validateIsNull(IssuerTO.issuerTOInstance)){
			IssuerTO.issuerTOInstance = new IssuerTO();
		}
		IssuerTO.issuerTOInstance.setIdIssuerPk(idIssuerPk);
			return IssuerTO.issuerTOInstance;
	}

	/**
	 * Gets the sort by.
	 *
	 * @return the sort by
	 */
	public String getSortBy() {
		return sortBy;
	}

	/**
	 * Sets the sort by.
	 *
	 * @param sortBy the new sort by
	 */
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the document type.
	 *
	 * @return the document type
	 */
	public Integer getDocumentType() {
		return documentType;
	}

	/**
	 * Sets the document type.
	 *
	 * @param documentType the new document type
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	/**
	 * Gets the document number.
	 *
	 * @return the document number
	 */
	public String getDocumentNumber() {
		return documentNumber;
	}

	/**
	 * Sets the document number.
	 *
	 * @param documentNumber the new document number
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}

	/**
	 * Sets the mnemonic.
	 *
	 * @param mnemonic the new mnemonic
	 */
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	/**
	 * Gets the economic sector.
	 *
	 * @return the economic sector
	 */
	public Integer getEconomicSector() {
		return economicSector;
	}

	/**
	 * Sets the economic sector.
	 *
	 * @param economicSector the new economic sector
	 */
	public void setEconomicSector(Integer economicSector) {
		this.economicSector = economicSector;
	}

	/**
	 * Gets the economic activity.
	 *
	 * @return the economic activity
	 */
	public Integer getEconomicActivity() {
		return economicActivity;
	}

	/**
	 * Sets the economic activity.
	 *
	 * @param economicActivity the new economic activity
	 */
	public void setEconomicActivity(Integer economicActivity) {
		this.economicActivity = economicActivity;
	}

	/**
	 * Gets the lst issuer states.
	 *
	 * @return the lst issuer states
	 */
	public List<Integer> getLstIssuerStates() {
		return lstIssuerStates;
	}

	/**
	 * Sets the lst issuer states.
	 *
	 * @param lstIssuerStates the new lst issuer states
	 */
	public void setLstIssuerStates(List<Integer> lstIssuerStates) {
		this.lstIssuerStates = lstIssuerStates;
	}

	/**
	 * Gets the issuer request to.
	 *
	 * @return the issuer request to
	 */
	public IssuerRequestTO getIssuerRequestTO() {
		return issuerRequestTO;
	}

	/**
	 * Sets the issuer request to.
	 *
	 * @param issuerRequestTO the new issuer request to
	 */
	public void setIssuerRequestTO(IssuerRequestTO issuerRequestTO) {
		this.issuerRequestTO = issuerRequestTO;
	}

	/**
	 * Gets the business name.
	 *
	 * @return the business name
	 */
	public String getBusinessName() {
		return businessName;
	}

	/**
	 * Sets the business name.
	 *
	 * @param businessName the new business name
	 */
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	
	/**
	 * Checks if is allow search all.
	 *
	 * @return true, if is allow search all
	 */
	public boolean isAllowSearchAll() {
		return allowSearchAll;
	}

	/**
	 * Sets the allow search all.
	 *
	 * @param allowSearchAll the new allow search all
	 */
	public void setAllowSearchAll(boolean allowSearchAll) {
		this.allowSearchAll = allowSearchAll;
	}	
	
}