package com.pradera.securities.administrator.events.corporatives;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;

import com.pradera.commons.view.GenericBaseBean;

// TODO: Auto-generated Javadoc
/**
 * The Class ProcessCalculateStock.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19-ago-2015
 */
@ManagedBean
public class ProcessCalculateStock extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	/** The calculate stock filter. */
	CalculateStockFilter calculateStockFilter;
	
	/**
	 * Instantiates a new process calculate stock.
	 */
	public ProcessCalculateStock()  {
		calculateStockFilter = new CalculateStockFilter();
	}

	/**
	 * Gets the calculate stock filter.
	 *
	 * @return the calculate stock filter
	 */
	public CalculateStockFilter getCalculateStockFilter() {
		return calculateStockFilter;
	}

	/**
	 * Sets the calculate stock filter.
	 *
	 * @param calculateStockFilter the new calculate stock filter
	 */
	public void setCalculateStockFilter(CalculateStockFilter calculateStockFilter) {
		this.calculateStockFilter = calculateStockFilter;
	}
}