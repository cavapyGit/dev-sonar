package com.pradera.securities.administrator.events.corporatives;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class CalculateStockFilter.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19-ago-2015
 */
public class CalculateStockFilter implements Serializable {
	
	/** The type stock selected. */
	Integer typeStockSelected;
	
	/** The code securities. */
	String  codeSecurities;

	/**
	 * Gets the code securities.
	 *
	 * @return the code securities
	 */
	public String getCodeSecurities() {
		return codeSecurities;
	}

	/**
	 * Sets the code securities.
	 *
	 * @param codeSecurities the new code securities
	 */
	public void setCodeSecurities(String codeSecurities) {
		this.codeSecurities = codeSecurities;
	}

	/**
	 * Gets the type stock selected.
	 *
	 * @return the type stock selected
	 */
	public Integer getTypeStockSelected() {
		return typeStockSelected;
	}

	/**
	 * Sets the type stock selected.
	 *
	 * @param typeStockSelected the new type stock selected
	 */
	public void setTypeStockSelected(Integer typeStockSelected) {
		this.typeStockSelected = typeStockSelected;
	}
	
}
