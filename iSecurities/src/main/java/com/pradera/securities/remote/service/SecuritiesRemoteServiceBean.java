package com.pradera.securities.remote.service;

import java.math.BigDecimal;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.AsynchronousLoggerInterceptor;
import com.pradera.commons.contextholder.interceptor.RemoteLoggerUserInterceptor;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.securities.service.SecuritiesRemoteService;
import com.pradera.integration.component.securities.to.IssuanceRegisterTO;
import com.pradera.integration.component.securities.to.SecurityObjectTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.valuatorprocess.SecurityClassValuator;
import com.pradera.securities.issuancesecuritie.service.SecurityServiceBean;
import com.pradera.securities.issuancesecuritie.util.SecuritiesUtils;
import com.pradera.securities.placementsegment.facade.PlacementSegmentServiceFacade;
import com.pradera.securities.valuator.classes.facade.SecuritiesClassValuatorFacade;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SecuritiesRemoteServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@Stateless
public class SecuritiesRemoteServiceBean extends CrudDaoServiceBean implements SecuritiesRemoteService {

	/** The securities utils. */
	@Inject 
	SecuritiesUtils securitiesUtils;
	
	/** The securitiy service bean. */
	@EJB 
	private SecurityServiceBean securitiyServiceBean;
	
	/** The valuator class. */
	@EJB 
	private SecuritiesClassValuatorFacade valuatorClass;
	
	/** The transaction registry. */
	@Resource 
	TransactionSynchronizationRegistry transactionRegistry;		
	
	/** The interface component service bean. */
	@Inject
    private Instance<InterfaceComponentService> interfaceComponentServiceBean;
	
	/** The placement facade. */
	@EJB private PlacementSegmentServiceFacade placementFacade;

	
	/* (non-Javadoc)
	 * @see com.pradera.integration.component.securities.service.SecuritiesRemoteService#createSecuritiesRemoteService(com.pradera.integration.component.securities.to.SecurityObjectTO)
	 */
	@Override
	@Interceptors(AsynchronousLoggerInterceptor.class)
	public Object createSecuritiesRemoteService(LoggerUser loggerUser, SecurityObjectTO securityObjectTO) throws ServiceException {			
		return securitiyServiceBean.validateAndSaveSecurity(securityObjectTO, loggerUser);		
	}
	
	/**
	 * *
	 * Method to get valuatorClass Id from security.
	 *
	 * @param securityCode the security code
	 * @return the automatic valuator class pk
	 * @throws ServiceException the service exception
	 */
	public String getAutomaticValuatorClassPk(String securityCode) throws ServiceException{
		/**Find security Object*/
		Security security = find(Security.class, securityCode);
		/**associated securityClass with valuatorClass*/
		SecurityClassValuator sec=new SecurityClassValuator();
		sec.setIdSecurityClassFk(security.getSecurityClass());
		/**Get list of valuatorClass according with securityClass*/
		List<SecurityClassValuator>  lstCboSecurityClassValuator=valuatorClass.getValuatorClassesBySecClassFacade(sec);
		/**If securityClass is one, then only this could be returned*/
		if(lstCboSecurityClassValuator!=null && lstCboSecurityClassValuator.size()==ComponentConstant.ONE){
			return lstCboSecurityClassValuator.get(0).getValuatorProcessClass().getIdValuatorClassPk();
		}
		/**Get valuatorProcessClass from security and list of valuatorClass*/
		return valuatorClass.getValuatorProcessClass(security,lstCboSecurityClassValuator).getIdValuatorClassPk();
	}
	 
	
	/* (non-Javadoc)
	 * @see com.pradera.integration.component.securities.service.SecuritiesRemoteService#deleteSecuritiesRemoteService(com.pradera.integration.component.securities.to.SecurityObjectTO)
	 */
	@Override
	public List<RecordValidationType> deleteSecuritiesRemoteService(SecurityObjectTO securityObjectTO) throws ServiceException {
		
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.component.securities.service.SecuritiesRemoteService#createIssuanceSecurityTx(com.pradera.integration.component.securities.to.IssuanceRegisterTO, com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public RecordValidationType createIssuanceSecurityTx(IssuanceRegisterTO issuanceRegisterTO, LoggerUser loggerUser) throws ServiceException {		
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
        try {					
        	RecordValidationType recordValidationType = securitiyServiceBean.validateAndSaveIssuanceTx(issuanceRegisterTO, loggerUser);  
        	interfaceComponentServiceBean.get().saveInterfaceTransactionTx(issuanceRegisterTO, BooleanType.YES.getCode(), loggerUser);		
        	return recordValidationType;
		} catch (ServiceException e) {			
			interfaceComponentServiceBean.get().saveInterfaceTransactionTx(issuanceRegisterTO, BooleanType.NO.getCode(), loggerUser);		
			throw e;
		}			
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.component.securities.service.SecuritiesRemoteService#addOperationPlacement(java.lang.Long, java.lang.String, java.lang.Long, java.math.BigDecimal)
	 */
	@Override
	@Interceptors(RemoteLoggerUserInterceptor.class)
	public void addOperationPlacement(Long participantCode,String securityCode, Long operationPk, BigDecimal stockQuantity)throws ServiceException {
		// TODO Auto-generated method stub
		placementFacade.addOperationPlacement(participantCode, securityCode, operationPk, stockQuantity);
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.component.securities.service.SecuritiesRemoteService#substractOperationPlacement(java.lang.Long, java.lang.String, java.lang.Long, java.math.BigDecimal)
	 */
	@Override
	@Interceptors(RemoteLoggerUserInterceptor.class)
	public void substractOperationPlacement(Long participantCode,String securityCode, Long operationPk, BigDecimal stockQuantity)throws ServiceException {
		// TODO Auto-generated method stub
		placementFacade.substractOperationPlacement(participantCode, securityCode, operationPk, stockQuantity);
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.component.securities.service.SecuritiesRemoteService#addAnnotationPlacement(java.lang.Long, java.lang.String, java.lang.Long, java.math.BigDecimal)
	 */
	@Override
	@Interceptors(RemoteLoggerUserInterceptor.class)
	public void addAnnotationPlacement(Long participantCode,String securityCode, Long operationPk, BigDecimal stockQuantity)throws ServiceException {
		// TODO Auto-generated method stub
		placementFacade.addAnnotationPlacement(participantCode, securityCode, operationPk, stockQuantity);
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.component.securities.service.SecuritiesRemoteService#substractAnnotationPlacement(java.lang.Long, java.lang.String, java.lang.Long, java.math.BigDecimal)
	 */
	@Override
	@Interceptors(RemoteLoggerUserInterceptor.class)
	public void substractAnnotationPlacement(Long participantCode,String securityCode, Long operationPk, BigDecimal stockQuantity)throws ServiceException {
		// TODO Auto-generated method stub
		placementFacade.substractAnnotationPlacement(participantCode, securityCode, operationPk, stockQuantity);
	}
}