package com.pradera.securities.externalinterface.batch;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;

import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
import com.pradera.securities.issuancesecuritie.facade.SecurityServiceFacade;

/**
 * 
 * @author rchiara
 *
 */

@BatchProcess(name="SirtexNegotiateUpdateDisableBatch")
@RequestScoped
public class SirtexNegotiateUpdateDisableBatch implements Serializable,JobExecution {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The security service facade. */
	@EJB
	SecurityServiceFacade securityServiceFacade;
	
	@Override
	public void startJob(ProcessLogger processLogger) {
	    String idIssuerPk = null;   
        for(ProcessLoggerDetail detail : processLogger.getProcessLoggerDetails()) {
             if(detail.getParameterName().equals("idIssuerPk")){
            	 idIssuerPk = detail.getParameterValue();
            	 continue;
             }
        }     
        
        List<Object> security = securityServiceFacade.findSirtexLiveOperations(idIssuerPk);
		
		try {			
			securityServiceFacade.updateSIRTEXnegotiationDisable(security);
		} catch (ServiceException e) {			
			e.printStackTrace();
		}
	}

	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}

}
