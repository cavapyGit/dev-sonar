package com.pradera.securities.externalinterface.batch;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;

import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
import com.pradera.securities.issuancesecuritie.facade.SecurityServiceFacade;

/**
 * 
 * @author rchiara
 *
 */

@BatchProcess(name="SendNotificationExpirationCfcBatch")
@RequestScoped
public class SendNotificationExpirationCfcBatch implements Serializable,JobExecution {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The security service facade. */
	@EJB
	SecurityServiceFacade securityServiceFacade;
	
	@EJB
	NotificationServiceFacade notificationServiceFacade;
	
	@Override
	public void startJob(ProcessLogger processLogger) {
		
		try {
			System.out.println("::: Iniciando Batchero 40873 - SendNotificationExpirationCfcBatch");
//			for(ProcessLoggerDetail detail : processLogger.getProcessLoggerDetails()) {
//	        	System.out.println("::: "+detail.getParameterName());
//	             if(detail.getParameterName().equals("idIssuerPk")){
//	            	 idIssuerPk = detail.getParameterValue();
//	            	 continue;
//	             }
//	        }
			
			List<String> lstCfcSecuritiesToExpire=new ArrayList<>();
			StringBuilder cfcSecurities;
			boolean firstLoop=true;
			Integer[] lstPeriods={15,30};
			Date currentDate = CommonsUtilities.currentDate();
			Date dateTarget = CommonsUtilities.currentDate();
			for(Integer countDay:lstPeriods){
				dateTarget = CommonsUtilities.addDate(currentDate, countDay);
				dateTarget = CommonsUtilities.truncateDateTime(dateTarget);
				lstCfcSecuritiesToExpire = securityServiceFacade.getLstCfcSecuritiesToExpire(dateTarget);
				cfcSecurities=new StringBuilder();
				if(Validations.validateListIsNotNullAndNotEmpty(lstCfcSecuritiesToExpire)){
					firstLoop=true;
					for(String cfcSecurity:lstCfcSecuritiesToExpire){
						if(firstLoop){
							cfcSecurities.append(cfcSecurity);
							firstLoop=false;
						}else{
							cfcSecurities.append(", ").append(cfcSecurity);
						}
					}
					
					Object[] parameters = { cfcSecurities.toString(), String.valueOf(countDay)};
					// envio de notificacion que se realizo un registro de solicitud de apertura de canal
					BusinessProcess businessProcessNotification = new BusinessProcess();
					businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.NOTIFY_EXPIRATION_CFC.getCode());
					notificationServiceFacade.sendNotification("ADMIN",
					businessProcessNotification,
					20, parameters);
				}else{
					System.out.println("::: No se encontro CFC para vencer en "+countDay+" dias");
				}
			}
		} catch (ServiceException e) {	
			e.printStackTrace();
			System.out.println("::: error ServiceException "+e.getMessage());
		}catch (Exception e) {
			e.printStackTrace();
			System.out.println("::: error Exception "+e.getMessage());
		}
		System.out.println("::: FIN Batchero 40873 - SendNotificationExpirationCfcBatch");
	}

	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}

}
