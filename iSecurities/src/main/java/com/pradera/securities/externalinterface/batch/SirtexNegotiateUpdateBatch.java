package com.pradera.securities.externalinterface.batch;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
import com.pradera.securities.issuancesecuritie.facade.SecurityServiceFacade;

/**
 * 
 * @author rchiara
 *
 */

@BatchProcess(name="SirtexNegotiateUpdateBatch")
@RequestScoped
public class SirtexNegotiateUpdateBatch implements Serializable,JobExecution {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The security service facade. */
	@EJB
	SecurityServiceFacade securityServiceFacade;
	
	@Override
	public void startJob(ProcessLogger processLogger) {
	    String idIssuerPk = null;   
	    Integer securityClass = null;
	    Integer currency = null;
	    Integer personType = null;
	    String withCoupons = null;
	    String issueanceDate = null;
        for(ProcessLoggerDetail detail : processLogger.getProcessLoggerDetails()){
             if(detail.getParameterName().equals("idIssuerPk")&&detail.getParameterValue()!=null)
            	 idIssuerPk = detail.getParameterValue();
             if(detail.getParameterName().equals("securityClass")&&detail.getParameterValue()!=null)
            	 securityClass = Integer.parseInt(detail.getParameterValue());
             if(detail.getParameterName().equals("currency")&&detail.getParameterValue()!=null)
            	 currency = Integer.parseInt(detail.getParameterValue());
             if(detail.getParameterName().equals("personType")&&detail.getParameterValue()!=null)
            	 personType = Integer.parseInt(detail.getParameterValue());
             if(detail.getParameterName().equals("withCoupons")&&detail.getParameterValue()!=null)
            	 withCoupons = detail.getParameterValue();
             if(detail.getParameterName().equals("issueanceDate")&&detail.getParameterValue()!=null)
            	 issueanceDate =  detail.getParameterValue();
        }
        //idIssuerPk = "BO011";
	    //securityClass= 420;
	    //currency = 127;
	    issueanceDate = issueanceDate!=null?issueanceDate:CommonsUtilities.convertDateToString(CommonsUtilities.currentDate(),"dd/MM/yyyy");
        List<Object> security = securityServiceFacade.findSirtexNegotiation(idIssuerPk, securityClass, currency, personType, withCoupons, issueanceDate);
		try {			
			securityServiceFacade.updateSIRTEXnegotiation(security);
		} catch (ServiceException e) {			
			e.printStackTrace();
		}
	}

	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}

}
