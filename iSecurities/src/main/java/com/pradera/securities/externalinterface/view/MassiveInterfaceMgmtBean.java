package com.pradera.securities.externalinterface.view;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.beanio.BeanIOConfigurationException;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.GenericPropertiesConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.common.validation.to.ValidationProcessTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.BusinessProcess;
import com.pradera.securities.issuancesecuritie.util.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MassiveInterfaceMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class MassiveInterfaceMgmtBean extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The upload file types. */
	private String fUploadFileTypes = "xml";
	
	/** The upload file types pattern. */
	private Pattern fUploadFileTypesPattern=Pattern.compile("([^\\s]+(\\.(?i)("+fUploadFileTypes+"))$)");
	
	/** The upload file types display. */
	private String fUploadFileTypesDisplay="*.xml";

	/** The process files data model. */
	private GenericDataModel<ProcessFileTO> processFilesDataModel;
	
	/** The errors data model. */
	private GenericDataModel<RecordValidationType> errorsDataModel;
	
	/** The process file filter. */
	private ProcessFileTO processFileFilter;  // for searches
	
	/** The process file. */
	private ProcessFileTO processFile; // bean sent to Registration Batch 
	
	/** The tmp process file. */
	private ProcessFileTO tmpProcessFile; // temporal bean on upload
	
	/** The back action. */
	private String backAction;
	
	/** The title label. */
	private String titleLabel;
	
	/** The massive interface facade. */
	@EJB
	private GeneralParametersFacade massiveInterfaceFacade;
	
	/** The batch process service facade. */
	@EJB
    private BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/**
	 * Metodo de inicializacion de la clase.
	 */
	@PostConstruct
	public void init() {
		processFileFilter = new ProcessFileTO();
		processFileFilter.setProcessDate(CommonsUtilities.currentDate());
		processFile = null;
	}
	
	/**
	 * Cargamos parametros de interfaces.
	 *
	 * @param interfaceName the interface name
	 * @param backAction the back action
	 */
	public void loadInterfaceParams(String interfaceName, String backAction){
		if(interfaceName!=null){
			
			processFileFilter.setInterfaceName(interfaceName);
			
			this.tmpProcessFile = massiveInterfaceFacade.getExternalInterfaceInformation(interfaceName);
			
			if(ComponentConstant.INTERFACE_SECURITIES_REGISTER.equals(interfaceName)){
				this.titleLabel = PropertiesUtilities.getGenericMessage(PropertiesConstants.SECURITY_LBL_MASSIVE);
			}
			
		}
		this.backAction = backAction;
		
	}

	/**
	 * Metodo cuando se cambia de fecha.
	 */
	public void onChangeProcessDate() {
		processFilesDataModel = null;
		processFile = null;
	}
	
	/**
	 * Metodo para limpiar los todos los datos.
	 */
	public void cleanAll() {
		JSFUtilities.hideGeneralDialogues();
		JSFUtilities.setValidViewComponentAndChildrens(":frmMassiveMCN");
		String interfaceName = processFileFilter.getInterfaceName();
		processFileFilter = new ProcessFileTO();
		processFileFilter.setProcessDate(CommonsUtilities.currentDate());
		processFileFilter.setInterfaceName(interfaceName);
		
		processFile = null;
		processFilesDataModel = null;
	}

	/**
	 * Metodo para buscarlos archivos procesados.
	 */
	public void searchProcessedFiles() {
		List<ProcessFileTO> mcnProcessFiles = massiveInterfaceFacade.getProcessedFilesInformation(processFileFilter.getProcessDate(),processFileFilter.getInterfaceName());
		processFilesDataModel = new GenericDataModel<ProcessFileTO>(mcnProcessFiles);
	}

	/**
	 * Metodo para manejar la carga de archivos.
	 *
	 * @param event the event
	 */
	public void fileUploadHandler(FileUploadEvent event) {
		try {
			String fDisplayName = fUploadValidateFile(event.getFile(), null, null,fUploadFileTypes);
			if (fDisplayName != null) {
				tmpProcessFile.setFileName(fDisplayName);
				tmpProcessFile.setProcessFile(event.getFile().getContents());
				tmpProcessFile.setTempProcessFile(File.createTempFile("tempMcnfile", ".tmp"));
				FileUtils.writeByteArrayToFile(tmpProcessFile.getTempProcessFile(), tmpProcessFile.getProcessFile());
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Obtenemos los datos del archivo MCN.
	 *
	 * @param mcnFile the mcn file
	 * @return the file stream content
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public StreamedContent getFileStreamContent(ProcessFileTO mcnFile) throws IOException{
		if(mcnFile!=null){
			
			InputStream inputStream = new ByteArrayInputStream(mcnFile.getProcessFile());
			StreamedContent streamedContentFile = new DefaultStreamedContent(inputStream,null, mcnFile.getFileName());
			inputStream.close();

			return streamedContentFile;
		}
		return null;
	}

	/**
	 * Metodo para remover el archivo cargado.
	 *
	 * @param actionEvent the action event
	 */
	public void removefileHandler(ActionEvent actionEvent) {
		tmpProcessFile.setProcessFile(null);
		tmpProcessFile.setFileName(null);
		if(tmpProcessFile.getTempProcessFile()!=null){
			tmpProcessFile.getTempProcessFile().delete();
			tmpProcessFile.setTempProcessFile(null);
		}
	}
	
	/**
	 * Metodo para procesar el contenido del archivo.
	 *
	 * @param mcnfile the mcnfile
	 * @return the processed file content
	 */
	public StreamedContent getProcessedFileContent(ProcessFileTO mcnfile) {
		try {
			
			byte[] file = massiveInterfaceFacade.getInterfaceUploadedFileContent(mcnfile.getIdProcessFilePk());
			
			return getStreamedContentFromFile(file, null,mcnfile.getFileName());
		} catch (Exception e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getGenericMessage(GenericPropertiesConstants.EXTERNAL_INTERFACE_ERROR_DOWNLOADING_FILE));
    		JSFUtilities.showComponent("cnfMsgRequiredValidation");
		}
		return null;
	}
	
	/**
	 * Metodo para obtener la respues del archivo.
	 *
	 * @param mcnfile the mcnfile
	 * @return the response file content
	 */
	public StreamedContent getResponseFileContent(ProcessFileTO mcnfile) {
		try {
			
			byte[] file = massiveInterfaceFacade.getInterfaceResponseFileContent(mcnfile.getIdProcessFilePk());
			
			return getStreamedContentFromFile(file, null,mcnfile.getFileName());
		} catch (Exception e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getGenericMessage(GenericPropertiesConstants.EXTERNAL_INTERFACE_ERROR_DOWNLOADING_FILE));
    		JSFUtilities.showComponent("cnfMsgRequiredValidation");
		}
		return null;
	}
	

	/**
	 * Metodo para validar la interface.
	 *
	 * @throws BeanIOConfigurationException the bean io configuration exception
	 * @throws IllegalArgumentException the illegal argument exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ServiceException the service exception
	 * @throws ParserConfigurationException the parser configuration exception
	 */
	public void validateInterfaceFile() 
			throws BeanIOConfigurationException, IllegalArgumentException, IOException, ServiceException, ParserConfigurationException {
		JSFUtilities.setValidViewComponentAndChildrens(":frmMassiveMCN");
		int countValidationErrors = 0;
				
		if(Validations.validateIsNull(tmpProcessFile.getTempProcessFile())){
    		JSFUtilities.addContextMessage("frmMassiveMCN:fuplMcnFile", FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getGenericMessage(GenericPropertiesConstants.EXTERNAL_INTERFACE_FILE_IS_NULL),
    				PropertiesUtilities.getGenericMessage(GenericPropertiesConstants.EXTERNAL_INTERFACE_FILE_IS_NULL) );
			countValidationErrors++;
    	}
		
		if(countValidationErrors > 0) {
    		showMessageOnDialog(null,null,PropertiesConstants.MESSAGE_DATA_REQUIRED,null);
    		JSFUtilities.showComponent("cnfMsgRequiredValidation");
    		return;
    	}
		
		CommonsUtilities.validateFileOperation(tmpProcessFile);
		 
		ValidationProcessTO mcnValidationTO = tmpProcessFile.getValidationProcessTO();
		
		errorsDataModel = new GenericDataModel<RecordValidationType>(mcnValidationTO==null?new ArrayList<RecordValidationType>():mcnValidationTO.getLstValidations());
		processFile = tmpProcessFile;
	}
	
	/**
	 * Metodo para antes de procesar la interface.
	 */
	public void beforeProcessInterfaceFile(){	
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
				PropertiesUtilities.getGenericMessage(GenericPropertiesConstants.EXTERNAL_INTERFACE_UPLOAD_CONFIRM));
		JSFUtilities.executeJavascriptFunction("cnfwProcessMcnFile.show();");
	}
	
	/**
	 * Metodo para procesar la interface.
	 */
	@LoggerAuditWeb
	public void processInterfaceFile(){	
		
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(processFile.getIdBusinessProcess());
		
		try {
			Map<String, Object> details = new HashMap<String, Object>();
			details.put(GeneralConstants.PARAMETER_FILE, processFile.getTempProcessFile().getAbsolutePath());
			details.put(GeneralConstants.PARAMETER_STREAM, processFile.getStreamFileDir());
			details.put(GeneralConstants.PARAMETER_STREAM_RESPONSE, processFile.getStreamResponseFileDir());
			details.put(GeneralConstants.PARAMETER_LOCALE, "es"); //JSFUtilities.getCurrentLocale().toString());
			details.put(GeneralConstants.PARAMETER_FILE_NAME, processFile.getFileName());
			details.put(GeneralConstants.PARAMETER_ROOT_TAG, processFile.getRootTag());
			
			batchProcessServiceFacade.registerBatch(
					userInfo.getUserAccountSession().getUserName(),businessProcess,details);
			
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
				PropertiesUtilities.getGenericMessage(GenericPropertiesConstants.EXTERNAL_INTERFACE_UPLOAD_SUCCESS));
		JSFUtilities.showSimpleValidationDialog();
		cleanAll();
	}
	
	/**
	 * Metodo para verificar la accion hacia atras.
	 *
	 * @return the string
	 */
	public String verifyOnBackAction(){
		if(isModifiedForm()){
			JSFUtilities.putViewMap("headerMessageView", PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING));
			JSFUtilities.executeJavascriptFunction("transferOwnershipBackCnfWidget.show();");
			return null;
		}else{
			return "searchMCNOperations";
		}
	}

	/**
	 * Metodo para verificar la Accion.
	 */
	public void verifyOnCleanAction(){
		if(isModifiedForm()){
			JSFUtilities.putViewMap("headerMessageView", PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING));
			JSFUtilities.executeJavascriptFunction("transferOwnershipClearCnfWidget.show();");
		}else{
			cleanAll();
		}
	}
	
	
	/**
	 * Metodo para verificar si el formulario fue modificado.
	 *
	 * @return true, if is modified form
	 */
	public boolean isModifiedForm(){
		if(Validations.validateIsNotNullAndPositive(processFileFilter.getIdNegotiationMechanismPk()) ||
				Validations.validateIsNotNullAndPositive(processFile.getIdNegotiationMechanismPk()) ||
				Validations.validateIsNotNull(processFile.getTempProcessFile())){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Gets the process files data model.
	 *
	 * @return the mcnProcessFilesDataModel
	 */
	public GenericDataModel<ProcessFileTO> getProcessFilesDataModel() {
		return processFilesDataModel;
	}

	/**
	 * Sets the process files data model.
	 *
	 * @param processFilesDataModel the new process files data model
	 */
	public void setProcessFilesDataModel(
			GenericDataModel<ProcessFileTO> processFilesDataModel) {
		this.processFilesDataModel = processFilesDataModel;
	}

	/**
	 * Gets the process file filter.
	 *
	 * @return the mcnProcessFileFilter
	 */
	public ProcessFileTO getProcessFileFilter() {
		return processFileFilter;
	}

	/**
	 * Sets the process file filter.
	 *
	 * @param mcnProcessFileFilter the mcnProcessFileFilter to set
	 */
	public void setProcessFileFilter(ProcessFileTO mcnProcessFileFilter) {
		this.processFileFilter = mcnProcessFileFilter;
	}

	/**
	 * Gets the process file.
	 *
	 * @return the mcnProcessFile
	 */
	public ProcessFileTO getProcessFile() {
		return processFile;
	}

	/**
	 * Sets the process file.
	 *
	 * @param mcnProcessFile the mcnProcessFile to set
	 */
	public void setProcessFile(ProcessFileTO mcnProcessFile) {
		this.processFile = mcnProcessFile;
	}

	/**
	 * Gets the errors data model.
	 *
	 * @return the errorsDataModel
	 */
	public GenericDataModel<RecordValidationType> getErrorsDataModel() {
		return errorsDataModel;
	}

	/**
	 * Sets the errors data model.
	 *
	 * @param errorsDataModel            the errorsDataModel to set
	 */
	public void setErrorsDataModel(GenericDataModel<RecordValidationType> errorsDataModel) {
		this.errorsDataModel = errorsDataModel;
	}
	
	/**
	 * Gets the f upload file types.
	 *
	 * @return the fUploadFileTypes
	 */
	public String getfUploadFileTypes() {
		return fUploadFileTypes;
	}

	/**
	 * Sets the f upload file types.
	 *
	 * @param fUploadFileTypes the fUploadFileTypes to set
	 */
	public void setfUploadFileTypes(String fUploadFileTypes) {
		this.fUploadFileTypes = fUploadFileTypes;
	}

	/**
	 * Gets the f upload file types display.
	 *
	 * @return the fUploadFileTypesDisplay
	 */
	public String getfUploadFileTypesDisplay() {
		return fUploadFileTypesDisplay;
	}

	/**
	 * Sets the f upload file types display.
	 *
	 * @param fUploadFileTypesDisplay the fUploadFileTypesDisplay to set
	 */
	public void setfUploadFileTypesDisplay(String fUploadFileTypesDisplay) {
		this.fUploadFileTypesDisplay = fUploadFileTypesDisplay;
	}
	
	/**
	 * Gets the f upload file types pattern.
	 *
	 * @return the fUploadFileTypesPattern
	 */
	public Pattern getfUploadFileTypesPattern() {
		return fUploadFileTypesPattern;
	}

	/**
	 * Sets the f upload file types pattern.
	 *
	 * @param fUploadFileTypesPattern the fUploadFileTypesPattern to set
	 */
	public void setfUploadFileTypesPattern(Pattern fUploadFileTypesPattern) {
		this.fUploadFileTypesPattern = fUploadFileTypesPattern;
	}
	
	/**
	 * Gets the tmp process file.
	 *
	 * @return the tmpMcnProcessFile
	 */
	public ProcessFileTO getTmpProcessFile() {
		return tmpProcessFile;
	}

	/**
	 * Sets the tmp process file.
	 *
	 * @param tmpMcnProcessFile the tmpMcnProcessFile to set
	 */
	public void setTmpProcessFile(ProcessFileTO tmpMcnProcessFile) {
		this.tmpProcessFile = tmpMcnProcessFile;
	}

	/**
	 * Back action.
	 *
	 * @return the string
	 */
	public String backAction() {
		return backAction;
	}

	/**
	 * Gets the title label.
	 *
	 * @return the title label
	 */
	public String getTitleLabel() {
		return titleLabel;
	}

	/**
	 * Sets the title label.
	 *
	 * @param titleLabel the new title label
	 */
	public void setTitleLabel(String titleLabel) {
		this.titleLabel = titleLabel;
	}

}
