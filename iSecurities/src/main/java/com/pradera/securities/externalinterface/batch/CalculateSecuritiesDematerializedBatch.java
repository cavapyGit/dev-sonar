package com.pradera.securities.externalinterface.batch;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.extension.transaction.Transactional;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.custody.transfersecurities.TotalSecurityClassBalance;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.type.ReportEDV1SetupType;
import com.pradera.model.process.ProcessLogger;
import com.pradera.securities.issuancesecuritie.facade.IssuanceSecuritiesServiceFacade;
import com.pradera.securities.issuancesecuritie.service.SecurityServiceBean;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CalculateSecuritiesDematerializedBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19-ago-2015
 */
@BatchProcess(name="CalculateSecuritiesDematerializedBatch")
@RequestScoped
public class CalculateSecuritiesDematerializedBatch implements JobExecution,Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The security service bean. */
	@EJB
	private SecurityServiceBean securityServiceBean;
	
	/** The issuance securities service facade. */
	@EJB
	private IssuanceSecuritiesServiceFacade issuanceSecuritiesServiceFacade;

	@EJB
	private ParameterServiceBean parameterServiceBean;
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/* (non-Javadoc)
	 * Batchero que calcula los movimientos diarios de valores desmaterializados.
	 */
	@Override
	@Transactional
	public void startJob(ProcessLogger processLogger){
		
		log.info("::::::::::::: DATO PARAMETRICO DE FECHA TOPE:::::::::::::");
		
		ParameterTable parameterTable=null;
		Date topDate=null;
		/**
		 * Indicador de Recalculo   1 : Reprocesa Si o si a la fecha de tope
		 * 							0 : Reprocesa Solo si no se ejecuto la fecha anterior
		 */
		Integer indRecalculate=null;
		List<TotalSecurityClassBalance> lstTotalSecurity;
		
		try {
			parameterTable = parameterServiceBean.getParameterTableById(ReportEDV1SetupType.DATE_TOP.getCode());
		} catch (ServiceException e1) {
			e1.printStackTrace();
		}
		
		if(parameterTable!=null){
			topDate=CommonsUtilities.convertStringtoDate(parameterTable.getText1());
			indRecalculate=parameterTable.getShortInteger();
			
		}
		
		log.info("::::::::::::: CARGAMOS LA FECHA DE HOY PARA ENVIAR COMO PARAMETRO :::::::::::::");
		Date sendDate =  CommonsUtilities.addDaysToDate(CommonsUtilities.currentDate(),0);
		Date sendDateTotal = CommonsUtilities.addDaysToDate(CommonsUtilities.currentDate(),-1);
		
		/**
		 * Si No hay procesos en Sysdate-1 (SendDateTotal) entonces Empezar a procesar desde Fecha tope +1
		 * 		Procesar desde Fecha TOPE + 1, hasta Sysdate (SendDate)
		 * 			
		 * Caso contrario
		 * 		Procesar el Sysdate (SendDate)
		 *  
		 */
		lstTotalSecurity=securityServiceBean.searchAmountsPrevius(sendDateTotal);
		if(Validations.validateListIsNotNullAndNotEmpty(lstTotalSecurity)&&
				BooleanType.NO.getCode().equals(indRecalculate)){

			issuanceSecuritiesServiceFacade.calculateSecuritiesByDate(sendDate, sendDateTotal);

		}else{
			/**
			 * topDate: Fecha Parametrica del ultimo proceso 
			 * sendDate<==TopDate
			 * Empezara desde sendDate(topDate) hasta sendDateTmp
			 */
			Date sendDateTmp =  CommonsUtilities.addDaysToDate(CommonsUtilities.currentDate(),1);
			sendDate =  CommonsUtilities.addDaysToDate(topDate,1);
			sendDateTotal = topDate;
			
			while(!CommonsUtilities.isEqualDate(sendDate, sendDateTmp)){
				log.info("::::::::PROCESANDO PARA FECHA "+sendDate+"::::::::::::::");
				issuanceSecuritiesServiceFacade.calculateSecuritiesByDate(sendDate, sendDateTotal);
				sendDate =  CommonsUtilities.addDaysToDate(sendDate,1);
				sendDateTotal = CommonsUtilities.addDaysToDate(sendDateTotal,1);

			}

		}
		
		/**
		 * Update last Date The Parameter Top
		 */
		issuanceSecuritiesServiceFacade.updateParameterTop(CommonsUtilities.addDaysToDate(sendDateTotal,-1), ReportEDV1SetupType.DATE_TOP.getCode());

	}
	
	

	/* (non-Javadoc)
	 * Metodo implementado para obtener los parametros de notificaciones
	 */
	@Override
	public Object[] getParametersNotification() {
		return null;
	}

	/* (non-Javadoc)
	 * Metodo implementado para obtener a los destinatarios
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		return null;
	}

	/* (non-Javadoc)
	 * Metodo Implementado para enviar las notificaciones
	 */
	@Override
	public boolean sendNotification() {
		return true;
	}

}
