package com.pradera.securities.externalinterface.batch;

import java.io.File;
import java.io.Serializable;
import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.apache.commons.io.FileUtils;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
import com.pradera.securities.issuancesecuritie.facade.SecurityServiceFacade;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MassiveIssuanceRegisterBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19-ago-2015
 */
@BatchProcess(name="MassiveIssuanceRegisterBatch")
@RequestScoped
public class MassiveIssuanceRegisterBatch implements Serializable,JobExecution {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;
	
	/** The security service facade. */
	@EJB
	SecurityServiceFacade securityServiceFacade;

	/* (non-Javadoc)
	 * Batchero para registrar masivamente la emisiones del BCB y TGN
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		
		System.out.println("************************************* BATCH SERVICE WAS CALLED ******************************************************");
		
		String mappingFile=null;
		String mappingResponseFile=null;
		String filePath=null;
		String locale=null;
		String fileName=null;
		String rootTag=null;
		
		for(ProcessLoggerDetail detail : processLogger.getProcessLoggerDetails()){
			if(detail.getParameterName().equals(GeneralConstants.PARAMETER_FILE)){
				filePath = detail.getParameterValue();
			}else if(detail.getParameterName().equals(GeneralConstants.PARAMETER_STREAM)){
				mappingFile = detail.getParameterValue();
			}else if(detail.getParameterName().equals(GeneralConstants.PARAMETER_STREAM_RESPONSE)){
				mappingResponseFile = detail.getParameterValue();
			}else if(detail.getParameterName().equals(GeneralConstants.PARAMETER_LOCALE)){
				locale = detail.getParameterValue();
			}else if(detail.getParameterName().equals(GeneralConstants.PARAMETER_FILE_NAME)){
				fileName = detail.getParameterValue();
			}else if(detail.getParameterName().equals(GeneralConstants.PARAMETER_ROOT_TAG)){
				rootTag = detail.getParameterValue();
			}
		}
		
		try {
			
			ProcessFileTO processFileTO = new ProcessFileTO();
			processFileTO.setStreamFileDir(mappingFile);
			processFileTO.setStreamResponseFileDir(mappingResponseFile);
			processFileTO.setTempProcessFile(new File(filePath));
			processFileTO.setProcessFile(FileUtils.readFileToByteArray(processFileTO.getTempProcessFile()));
			processFileTO.setLocale(new Locale(locale));
			processFileTO.setFileName(fileName);
			processFileTO.setIndAutomaticProcess(GeneralConstants.TWO_VALUE_INTEGER);
			processFileTO.setInterfaceName(ComponentConstant.INTERFACE_SECURITIES_REGISTER);
			processFileTO.setRootTag(rootTag);
			
			CommonsUtilities.validateFileOperationStruct(processFileTO);
			
			securityServiceFacade.registryMassiveSecurities(processFileTO);

				
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/* (non-Javadoc)
	 * Metodo implementado para obtener los parametros de notificaciones
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * Metodo implementado para obtener a los destinatarios
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * Metodo Implementado para enviar las notificaciones
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}
