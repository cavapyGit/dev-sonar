package com.pradera.securities.administration.issues.securities.view.filters;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class IssueFilter.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19-ago-2015
 */
public class IssueFilter implements Serializable  {

	/** The issuer selected. */
	Integer issuerSelected;
	
	/** The instrument type selected. */
	Integer instrumentTypeSelected;
	
	/** The securities type selected. */
	Integer securitiesTypeSelected;
	
	/** The securities class selected. */
	Integer securitiesClassSelected;
	
	/** The issue code. */
	String  issueCode;
	
	/** The issue description. */
	String  issueDescription;
	
	/** The issue status selected. */
	Integer issueStatusSelected;
	
	
	/**
	 * Instantiates a new issue filter.
	 */
	public IssueFilter() {
		// TODO Auto-generated constructor stub
	}


	/**
	 * Gets the issuer selected.
	 *
	 * @return the issuer selected
	 */
	public Integer getIssuerSelected() {
		return issuerSelected;
	}


	/**
	 * Sets the issuer selected.
	 *
	 * @param issuerSelected the new issuer selected
	 */
	public void setIssuerSelected(Integer issuerSelected) {
		this.issuerSelected = issuerSelected;
	}


	/**
	 * Gets the instrument type selected.
	 *
	 * @return the instrument type selected
	 */
	public Integer getInstrumentTypeSelected() {
		return instrumentTypeSelected;
	}


	/**
	 * Sets the instrument type selected.
	 *
	 * @param instrumentTypeSelected the new instrument type selected
	 */
	public void setInstrumentTypeSelected(Integer instrumentTypeSelected) {
		this.instrumentTypeSelected = instrumentTypeSelected;
	}


	/**
	 * Gets the securities type selected.
	 *
	 * @return the securities type selected
	 */
	public Integer getSecuritiesTypeSelected() {
		return securitiesTypeSelected;
	}


	/**
	 * Sets the securities type selected.
	 *
	 * @param securitiesTypeSelected the new securities type selected
	 */
	public void setSecuritiesTypeSelected(Integer securitiesTypeSelected) {
		this.securitiesTypeSelected = securitiesTypeSelected;
	}


	/**
	 * Gets the securities class selected.
	 *
	 * @return the securities class selected
	 */
	public Integer getSecuritiesClassSelected() {
		return securitiesClassSelected;
	}


	/**
	 * Sets the securities class selected.
	 *
	 * @param securitiesClassSelected the new securities class selected
	 */
	public void setSecuritiesClassSelected(Integer securitiesClassSelected) {
		this.securitiesClassSelected = securitiesClassSelected;
	}


	/**
	 * Gets the issue code.
	 *
	 * @return the issue code
	 */
	public String getIssueCode() {
		return issueCode;
	}


	/**
	 * Sets the issue code.
	 *
	 * @param issueCode the new issue code
	 */
	public void setIssueCode(String issueCode) {
		this.issueCode = issueCode;
	}


	/**
	 * Gets the issue description.
	 *
	 * @return the issue description
	 */
	public String getIssueDescription() {
		return issueDescription;
	}


	/**
	 * Sets the issue description.
	 *
	 * @param issueDescription the new issue description
	 */
	public void setIssueDescription(String issueDescription) {
		this.issueDescription = issueDescription;
	}


	/**
	 * Gets the issue status selected.
	 *
	 * @return the issue status selected
	 */
	public Integer getIssueStatusSelected() {
		return issueStatusSelected;
	}


	/**
	 * Sets the issue status selected.
	 *
	 * @param issueStatusSelected the new issue status selected
	 */
	public void setIssueStatusSelected(Integer issueStatusSelected) {
		this.issueStatusSelected = issueStatusSelected;
	}

}
