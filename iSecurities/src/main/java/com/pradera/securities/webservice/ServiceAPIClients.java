package com.pradera.securities.webservice;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.apache.log4j.Logger;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.services.remote.security.to.InstitutionTO;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.type.ModuleWarType;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;

@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ServiceAPIClients {

	@Inject
	ClientRestService clientRestService;
	
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
    
	Logger log = Logger.getLogger(ServiceAPIClients.class);
	
	public void callWsCreateInstitutionSecurity(String name, String mnemonic, Integer type, String issuerCode, Long participantCode) throws ServiceException {
		InstitutionTO req = new InstitutionTO(name, mnemonic, type, issuerCode, participantCode);
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		log.info("loggerUser: " + loggerUser);
		if(loggerUser.getUserAction() != null)
			log.info("loggerUser.getUserAction(): " + loggerUser.getUserAction());
		else
			log.info("loggerUser.getUserAction(): NULL");
		if(loggerUser.getUserAction().getIdPrivilegeExecute() != null)
			log.info("loggerUser.getUserAction().getIdPrivilegeExecute(): " + loggerUser.getUserAction().getIdPrivilegeExecute());
		else 
			log.info("loggerUser.getUserAction().getIdPrivilegeExecute(): NULL");
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
	    req.setLoggerUser(loggerUser);
	    
		this.clientRestService.callLocalWebService(ModuleWarType.SECURITY.getValue(), EndPointConstants.CREATE_INSTITUTION_SECURITY, req);
	}
	
	public void callWsUpdateInstitutionSecurity(String name, String mnemonic, Integer type, String issuerCode, Long participantCode, Integer state) throws ServiceException {
		InstitutionTO req = new InstitutionTO(name, mnemonic, type, issuerCode, participantCode, state);
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		log.info("loggerUser: " + loggerUser);
		if(loggerUser.getUserAction() != null)
			log.info("loggerUser.getUserAction(): " + loggerUser.getUserAction());
		else
			log.info("loggerUser.getUserAction(): NULL");
		if(loggerUser.getUserAction().getIdPrivilegeExecute() != null)
			log.info("loggerUser.getUserAction().getIdPrivilegeExecute(): " + loggerUser.getUserAction().getIdPrivilegeExecute());
		else 
			log.info("loggerUser.getUserAction().getIdPrivilegeExecute(): NULL");
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
	    req.setLoggerUser(loggerUser);
	    
		this.clientRestService.callLocalWebService(ModuleWarType.SECURITY.getValue(), EndPointConstants.UPDATE_INSTITUTION_SECURITY, req);
	}
}
