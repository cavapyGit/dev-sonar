package com.pradera.securities.webservice;

public class EndPointConstants {
	public static final String CREATE_INSTITUTION_SECURITY = "/resources/institution/create";

	public static final String UPDATE_INSTITUTION_SECURITY = "/resources/institution/update";
}
