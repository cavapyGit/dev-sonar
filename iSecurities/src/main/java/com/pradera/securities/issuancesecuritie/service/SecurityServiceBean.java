package com.pradera.securities.issuancesecuritie.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.exception.SQLGrammarException;
import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericPropertiesConstants;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.component.accounts.service.HolderBalanceMovementsServiceBean;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.accounts.to.InternationalDepositoryTO;
import com.pradera.core.component.accounts.to.NegotiationMechanismTO;
import com.pradera.core.component.business.to.ExpirationSecurityTO;
import com.pradera.core.component.business.to.SecurityConsultTO;
import com.pradera.core.component.business.to.SecurityOverdueTO;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.service.AccountQueryServiceBean;
import com.pradera.core.component.securities.service.SecuritiesServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.common.validation.to.ValidationOperationType;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.service.SecuritiesComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.component.business.to.SecuritiesComponentTO;
import com.pradera.integration.component.securities.to.AmortizationObjectTO;
import com.pradera.integration.component.securities.to.CouponObjectTO;
import com.pradera.integration.component.securities.to.IssuanceObjectTO;
import com.pradera.integration.component.securities.to.IssuanceRegisterTO;
import com.pradera.integration.component.securities.to.SecurityObjectTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.MechanismModalityStateType;
import com.pradera.model.accounts.type.NegotiationMechanismStateType;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.component.SecuritiesOperation;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.custody.securitiesretirement.RetirementBlockDetail;
import com.pradera.model.custody.securitiesretirement.RetirementDetail;
import com.pradera.model.custody.securitiesretirement.RetirementMarketfact;
import com.pradera.model.custody.securitiesretirement.RetirementOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.transfersecurities.TotalSecurityClassBalance;
import com.pradera.model.custody.type.RetirementOperationMotiveType;
import com.pradera.model.custody.type.RetirementOperationRedemptionType;
import com.pradera.model.custody.type.RetirementOperationStateType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.externalinterface.InterfaceTransaction;
import com.pradera.model.generalparameter.type.GeographicLocationType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.AmortizationPaymentSchedule;
import com.pradera.model.issuancesecuritie.CfiAttribute;
import com.pradera.model.issuancesecuritie.CfiCategory;
import com.pradera.model.issuancesecuritie.CfiGroup;
import com.pradera.model.issuancesecuritie.InterestPaymentSchedule;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.ProgramAmortizationCoupon;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.SecurityCodeFormat;
import com.pradera.model.issuancesecuritie.SecurityForeignDepository;
import com.pradera.model.issuancesecuritie.SecurityHistoryState;
import com.pradera.model.issuancesecuritie.SecurityInvestor;
import com.pradera.model.issuancesecuritie.SecurityNegotiationMechanism;
import com.pradera.model.issuancesecuritie.SecurityRemoved;
import com.pradera.model.issuancesecuritie.SecuritySerialRange;
import com.pradera.model.issuancesecuritie.type.AmortizationOnType;
import com.pradera.model.issuancesecuritie.type.AmortizationType;
import com.pradera.model.issuancesecuritie.type.CalendarDayType;
import com.pradera.model.issuancesecuritie.type.CalendarMonthType;
import com.pradera.model.issuancesecuritie.type.CalendarType;
import com.pradera.model.issuancesecuritie.type.CapitalPaymentModalityType;
import com.pradera.model.issuancesecuritie.type.EncoderAgentType;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.InterestClassType;
import com.pradera.model.issuancesecuritie.type.InterestPaymentModalityType;
import com.pradera.model.issuancesecuritie.type.InterestPeriodicityType;
import com.pradera.model.issuancesecuritie.type.InterestType;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleSituationType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityPlacementType;
import com.pradera.model.issuancesecuritie.type.SecuritySourceType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.issuancesecuritie.type.SecurityTermType;
import com.pradera.model.issuancesecuritie.type.SecurityType;
import com.pradera.model.negotiation.MechanismModality;
import com.pradera.model.negotiation.MechanismModalityPK;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.type.NegotiationModalityStateType;
import com.pradera.model.negotiation.type.NegotiationModalityType;
import com.pradera.model.operations.RequestCorrelativeType;
import com.pradera.securities.issuancesecuritie.to.SecurityRemovedModelTO;
import com.pradera.securities.issuancesecuritie.util.PropertiesConstants;
import com.pradera.securities.issuancesecuritie.util.SecuritiesUtils;
import com.pradera.securities.issuer.service.IssuerServiceBean;
import com.pradera.securities.issuer.to.IssuerTO;
import com.pradera.securities.query.to.SecurityBalanceMovTO;
import com.pradera.securities.query.to.SecurityMovementsResultTO;
import com.pradera.securities.valuator.classes.service.SecuritiesClassValuatorService;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SecurityServiceBean. 
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class SecurityServiceBean extends CrudDaoServiceBean {

	
	/** The issuance service bean. */
	@EJB
	private IssuanceServiceBean issuanceServiceBean;
	
	/** The issuer service bean. */
	@EJB
	private IssuerServiceBean issuerServiceBean;
	
	/** The securities service bean. */
	@EJB
	private SecuritiesServiceBean securitiesServiceBean;
	
	/** The Holder Balance Movement Service Bean*/
	@EJB
	private HolderBalanceMovementsServiceBean holderBalanceMovementsServiceBean;
	
	/** The securities class valuator service. */
	@EJB 
	private SecuritiesClassValuatorService securitiesClassValuatorService;
	
	/** The general parameter facade. */
	@Inject 
	private GeneralParametersFacade generalParameterFacade;
	
	/** The participant service bean. */
	@EJB
	private ParticipantServiceBean participantServiceBean;
	
	/** The parameter service bean. */
	@EJB
	private ParameterServiceBean parameterServiceBean;
	
	@EJB
	private AccountQueryServiceBean accountQueryServiceBean;
	
	/** The securities utils. */
	@Inject SecuritiesUtils securitiesUtils;
		
	/** The log. */
	@Inject private PraderaLogger log;
	
	/** The country residence. */
	@Inject @Configurable Integer countryResidence;
	
	/** The geographic location. */
	@Inject @Configurable String geographicLocation;
	
	/** The id participant bc. */
	@Inject @Configurable Long idParticipantBC;
	
	/** The id participant vun. */
	@Inject @Configurable Long idParticipantVUN;

	/** The executor component service bean. */
	@Inject
    private Instance<AccountsComponentService> accountsComponentService;
	
	/** The securities component service. */
	@Inject
    private Instance<SecuritiesComponentService> securitiesComponentService;

	/** The idepositary setup. */
	@Inject
	@DepositarySetup
    IdepositarySetup idepositarySetup;
	
	
	private static final String CUT_DATE_SIRTEX_OPERATION = "31/08/2019";
	
	/** issue 891: registro de solictud de eliminacion de valor no autorizado
	 * 
	 * @param entity
	 * @return */
	public SecurityRemoved regiterSecurityRemoved(SecurityRemoved entity) throws ServiceException {
		return createWithFlush(entity);
	}
	
	/** issue 891: cambio de estado de una solictud de eliminacion de valor no autorizado
	 * 
	 * @param entity
	 * @return 
	 * @throws ServiceException */
	public SecurityRemoved changeStateSecurityRemoved(SecurityRemoved entity) throws ServiceException {
		return update(entity);
	} 
	
	public void executeRemovedSecurityQueryNative(String queryNative, Map<String, Object> parameters){
		Set<Entry<String, Object>> rawParameters = parameters.entrySet();
		Query query = em.createNativeQuery(queryNative);
		for (Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		query.executeUpdate();
	}
	
	/**
	 * lista los CFC que venceran (se toma como referencia el campo EXPIRATION_FONDO_DATE de la tabla SECURITY)
	 * @param dateTarget
	 * @return
	 * @throws ServiceException
	 */
	public List<String> getLstCfcSecuritiesToExpire(Date dateTarget) throws ServiceException {
		StringBuilder sd = new StringBuilder();
		sd.append(" SELECT S.ID_SECURITY_CODE_PK");
		sd.append(" FROM SECURITY S");
		sd.append(" WHERE S.SECURITY_CLASS = :securityClass");
		sd.append(" AND trunc(S.EXPIRATION_FONDO_DATE) = :dateTarget");
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("securityClass", SecurityClassType.CFC.getCode());
		parameters.put("dateTarget", dateTarget);
		List<String> lst=findByNativeQuery(sd.toString(), parameters);
		return lst;
	}
	
	/** issue 605: obtiene las solictudes de eliminacion de valor segun los filtros enviados
	 * 
	 * @param filter
	 * @return
	 * @throws ServiceException */
	public List<SecurityRemovedModelTO> getLstSecurityRemovedTO(SecurityRemovedModelTO filter) throws ServiceException {
		List<SecurityRemovedModelTO> lst = new ArrayList<>();
		StringBuilder sd = new StringBuilder();
		sd.append("select");
		sd.append(" new com.pradera.securities.issuancesecuritie.to.SecurityRemovedModelTO(");
		sd.append(" sr.idSecurityRemovedPk,");
		sd.append(" sr.registryDate,");
		sd.append(" sr.idSecurityCodePk,");
		sd.append(" sr.issuanceDate,");
		sd.append(" sr.expirationDate,");
		sd.append(" sr.currency,");
		sd.append(" sr.nominalValue,");
		sd.append(" sr.stateSecurityRemoved");
		sd.append(" ) ");
		sd.append(" from SecurityRemoved sr ");
		sd.append(" where 0=0");
		if (Validations.validateIsNotNull(filter.getInitialDate()) && Validations.validateIsNotNull(filter.getFinalDate())) {
			sd.append(" and trunc(sr.registryDate) between :initialDate and :finalDate");
		}
		if (Validations.validateIsNotNull(filter.getIdSecurityRemovedPk())) {
			sd.append(" and sr.idSecurityRemovedPk = :numberRequest");
		}
		if (Validations.validateIsNotNull(filter.getIdSecurityCodePk())) {
			sd.append(" and sr.idSecurityCodePk = :idSecurityCode");
		}
		if (Validations.validateIsNotNull(filter.getStateSecurityRemoved())) {
			sd.append(" and sr.stateSecurityRemoved = :stateSecurityRemoved");
		}
		if (Validations.validateIsNotNull(filter.getCurrency())) {
			sd.append(" and sr.currency = :currency");
		}
		sd.append(" order by sr.idSecurityRemovedPk asc");

		Map<String, Object> parameters = new HashMap<String, Object>();
		if (Validations.validateIsNotNull(filter.getInitialDate()) && Validations.validateIsNotNull(filter.getFinalDate())) {
			parameters.put("initialDate", filter.getInitialDate());
			parameters.put("finalDate", filter.getFinalDate());
		}
		
		if (Validations.validateIsNotNull(filter.getIdSecurityRemovedPk())) {
			parameters.put("numberRequest", filter.getIdSecurityRemovedPk());
		}
		if (Validations.validateIsNotNull(filter.getIdSecurityCodePk())) {
			parameters.put("idSecurityCode", filter.getIdSecurityCodePk());
		}
		if (Validations.validateIsNotNull(filter.getStateSecurityRemoved())) {
			parameters.put("stateSecurityRemoved", filter.getStateSecurityRemoved());
		}
		if (Validations.validateIsNotNull(filter.getCurrency())) {
			parameters.put("currency", filter.getCurrency());
		}

		lst = findListByQueryString(sd.toString(), parameters);
		return lst;
	}
	
	/**
	 * Metodo para Obtener los Grupos CFI
	 *
	 * @param idCategoryPk the id category pk
	 * @return the cFI group service bean
	 * @throws ServiceException the service exception
	 */
	public List<CfiGroup> getCFIGroupServiceBean(String idCategoryPk) throws ServiceException{
		CriteriaBuilder cb=em.getCriteriaBuilder();
		CriteriaQuery<CfiGroup> cq=cb.createQuery(CfiGroup.class);
		Root<CfiGroup> CfiGroupRoot=cq.from(CfiGroup.class);
		List<Predicate> criteriaParameters=new ArrayList<Predicate>();
		cq.select(CfiGroupRoot);
		
		criteriaParameters.add(cb.equal(CfiGroupRoot.<String>get("id").get("idCategoryPk"), idCategoryPk));
		
		if(criteriaParameters.size()==0){
			throw new RuntimeException("no criteria");
		}else if(criteriaParameters.size() == 1){
			cq.where( criteriaParameters.get(0) );
		}else {
			cq.where( cb.and( criteriaParameters.toArray(new Predicate[criteriaParameters.size()])  ) );
		}
		
		TypedQuery<CfiGroup> issuerCriteria = em.createQuery(cq);
		return issuerCriteria.getResultList();
	}
	
	
	/**
	 * Gets the cFI categories.
	 *
	 * @return the cFI categories
	 * @throws ServiceException the service exception
	 */
	public List<CfiCategory> getCFICategoriesServiceBean() throws ServiceException{
		CriteriaBuilder cb=em.getCriteriaBuilder();
		CriteriaQuery<CfiCategory> cq=cb.createQuery(CfiCategory.class);
		Root<CfiCategory> cfiCategoryRoot=cq.from(CfiCategory.class);
		List<Predicate> criteriaParameters=new ArrayList<Predicate>();
		cq.select(cfiCategoryRoot);
		
		if(!criteriaParameters.isEmpty()){
			if (criteriaParameters.size() == 1) {
		        cq.where(criteriaParameters.get(0));
		    } else {
		        cq.where(cb.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()])));
		    }
		}
		TypedQuery<CfiCategory> cfiCategoryCriteria= em.createQuery(cq);
		
		
		return (List<CfiCategory>) cfiCategoryCriteria.getResultList();
	}
	
	/**
	 * Metodo para Obtener los categorias CFI
	 *
	 * @param idCategoryPk the id category pk
	 * @param idGroupPk the id group pk
	 * @param attributeLevel the attribute level
	 * @return the cFI attributes service bean
	 * @throws ServiceException the service exception
	 */
	public List<CfiAttribute> getCFIAttributesServiceBean(String idCategoryPk, String idGroupPk, String attributeLevel) throws ServiceException{
		CriteriaBuilder criteriaBuilder=em.getCriteriaBuilder();
		CriteriaQuery<CfiAttribute> criteriaQuery=criteriaBuilder.createQuery(CfiAttribute.class);
		Root<CfiAttribute> cfiAttributeRoot=criteriaQuery.from(CfiAttribute.class);
		List<Predicate> criteriaParameters=new ArrayList<Predicate>();
		criteriaQuery.select( cfiAttributeRoot );
		
		criteriaParameters.add(criteriaBuilder.equal(cfiAttributeRoot.<String>get("id").get("idCategoryPk"), idCategoryPk));
		criteriaParameters.add(criteriaBuilder.equal(cfiAttributeRoot.<String>get("id").get("idGroupPk"), idGroupPk));
		criteriaParameters.add(criteriaBuilder.equal(cfiAttributeRoot.<String>get("attributeLevel"), attributeLevel));
		
		if(!criteriaParameters.isEmpty()){
			if(criteriaParameters.size()==1){
				criteriaQuery.where( criteriaParameters.get(0) );
			}else {
				criteriaQuery.where( criteriaBuilder.and( criteriaParameters.toArray(new Predicate[criteriaParameters.size()]) ) );
			}
		}
		TypedQuery<CfiAttribute> cfiAttribute=em.createQuery(criteriaQuery);
		return (List<CfiAttribute>) cfiAttribute.getResultList();
	}
	
	/**
	 * Metodo para Obtener una Lista de Depositarias Internacionales.
	 *
	 * @param filter the filter
	 * @return the list international depository service bean
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<InternationalDepository> getListInternationalDepositoryServiceBean(InternationalDepositoryTO filter) throws ServiceException{		
		Map<String,Object> parameters = new HashMap<String, Object>();
	 	parameters.put("stateIntDepository", filter.getStateIntDepository());
	 	    
    	return (List<InternationalDepository>) findWithNamedQuery(InternationalDepository.INTERNATIONAL_DEPOSITORIES_BY_STATE, parameters);
	}
	
	/**
	 * Metodo para obtener una lista de negociaciones por mecanismo
	 *
	 * @param filter the filter
	 * @return the list negotiation mechanism service bean
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<NegotiationMechanism> getListNegotiationMechanismServiceBean(NegotiationMechanismTO filter) throws ServiceException	{
		
		Map<String,Object> parameters = new HashMap<String, Object>();
	 	parameters.put("stateMechanismPrm", filter.getNegotiationMechanismState());
	 	parameters.put("stateMechanismModalityPrm", filter.getMechanismModalityState());
	 	
	 	List<NegotiationMechanism> lstNegotiationMechanism = (List<NegotiationMechanism>)findWithNamedQuery(NegotiationMechanism.NEGOTIATION_MECHANISM_BY_STATE, parameters);
     	
    	for (NegotiationMechanism negotiationMechanism : lstNegotiationMechanism) {
			List<MechanismModality> mechanismModalities = negotiationMechanism.getMechanismModalities();
			for (MechanismModality mechanismModality : mechanismModalities) {
				if(Validations.validateIsNotNull(mechanismModality.getNegotiationModality())){
					mechanismModality.getNegotiationModality().getModalityName();
				}
			}
		}
    	return lstNegotiationMechanism;
	}
	
	/**
	 * Metodo para Obtener una lista de mecanismos.
	 *
	 * @param state the state
	 * @return the list mechanism service bean
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<NegotiationMechanism> getListMechanismServiceBean(Integer state) throws ServiceException	{
		
		Map<String,Object> parameters = new HashMap<String, Object>();
	 	parameters.put("statePrm", state);
	 	
	 	List<NegotiationMechanism> lstNegotiationMechanism = (List<NegotiationMechanism>)findWithNamedQuery(NegotiationMechanism.MECHANISM_BY_STATE, parameters);
     	
    	return lstNegotiationMechanism;
	}
	
	/**
	 * Gets the securities count service bean.
	 *
	 * @param idIssuerPkPrm the id issuer pk prm
	 * @return the securities count service bean
	 * @throws ServiceException the service exception
	 */
	public int getSecuritiesCountServiceBean(String idIssuerPkPrm) throws ServiceException{
		CriteriaBuilder cb=em.getCriteriaBuilder();
		CriteriaQuery<Long> cq=cb.createQuery(Long.class);
		Root<Security> securityRoot=cq.from(Security.class);
		List<Predicate> criteriaParameters=new ArrayList<Predicate>();
		Expression<Long> count = cb.count(securityRoot);
		cq.select(count);

		criteriaParameters.add(cb.equal(securityRoot.<String>get("issuer").get("idIssuerPk"), idIssuerPkPrm));
		
		if(criteriaParameters.size()==0){
			throw new RuntimeException("no criteria");
		}else if(criteriaParameters.size() == 1){
			cq.where( criteriaParameters.get(0) );
		}else {
			cq.where( cb.and( criteriaParameters.toArray(new Predicate[criteriaParameters.size()])  ) );
		}
		
		Long countCriteria = em.createQuery(cq).getSingleResult();
		return countCriteria.intValue();
	}
	
	/**
	 * Find securities service bean.
	 *
	 * @param securityTO the security to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Security> findSecuritiesServiceBean(SecurityTO securityTO) throws ServiceException{
		CriteriaBuilder cb=em.getCriteriaBuilder();
		CriteriaQuery<Security> cq=cb.createQuery(Security.class);
		Root<Security> securityRoot=cq.from( Security.class );
		
		List<Predicate> criteriaParameters=new ArrayList<Predicate>();
		cq.select(securityRoot);
		
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuerCodePk()  )){
			criteriaParameters.add( cb.equal(securityRoot.<String>get("issuer").get("idIssuerPk"), securityTO.getIdIssuerCodePk()) );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuanceCodePk() )){
			criteriaParameters.add(cb.equal(securityRoot.<String>get("issuance").get("idIssuanceCodePk"), securityTO.getIdIssuanceCodePk() ));
		}
		
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdSecurityCodePk() )){
			criteriaParameters.add(cb.equal(securityRoot.<String>get("idSecurityCodePk"), securityTO.getIdSecurityCodePk() ));
		}
		
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIsinCodePk() )){
			criteriaParameters.add(cb.equal(securityRoot.<String>get("idIsinCodePk"), securityTO.getIdIsinCodePk() ));
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdCfiCodePk() )){
			criteriaParameters.add(cb.equal(securityRoot.<String>get("cfiCode"), securityTO.getIdCfiCodePk() ));
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getAlternativeCode() )){
			criteriaParameters.add(cb.equal(securityRoot.<String>get("alternativeCode"), securityTO.getAlternativeCode() ));
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getDescriptionSecurity() )){
			criteriaParameters.add(cb.like(securityRoot.<String>get("description"),
																new StringBuilder()
																.append(GeneralConstants.PERCENTAGE_CHAR)
																.append(securityTO.getDescriptionSecurity())
																.append(GeneralConstants.PERCENTAGE_CHAR).toString())    );
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getSecurityState())){
			criteriaParameters.add(cb.equal(securityRoot.<String>get("stateSecurity"), securityTO.getSecurityState() ));
		}
		
		if(!criteriaParameters.isEmpty()){
			if (criteriaParameters.size() == 1) {
		        cq.where(criteriaParameters.get(0));
		    } else {
		        cq.where(cb.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()])));
		    }
		}
		
		TypedQuery<Security> security=em.createQuery(cq);
		return (List<Security>) security.getResultList();
	}
	
	/**
	 * Find securities lite service bean.
	 *
	 * @param securityTO the security to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Security> findSecuritiesLiteServiceBean(SecurityTO securityTO) throws ServiceException{
		

		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" select  new Security( se.idSecurityCodePk, ").
				append("   se.idIsinCode, ").
				append("   se.alternativeCode, ").
				append("   se.cfiCode, ").
				append("   se.description, ").
				append("   se.mnemonic, ").
				append("   se.securityType, ").
				append("   se.securityClass, ").
				append("   se.stateSecurity, ").
				append("   se.initialNominalValue, ").
				append("   se.currentNominalValue, ").
				append("   se.interestRate, ").
				append("   se.issuanceDate, ").
				append("   se.securityDaysTerm, ").
				append("   se.currency, ").
				append("   se.desmaterializedBalance, ").
				append("   se.expirationDate, ").
				append("   se.numberCoupons, ").
				append("   se.registryUser, ").
				append("   se.indAuthorized ) "). //issue 891
				append("  From Security se   ").
				append("  ");//19

		sbQuery.append("  WHERE   1=1 ");
		
		
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuerCodePk()  )){
			sbQuery.append(" and se.issuer.idIssuerPk= :idIssuerCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuanceCodePk() )){
			sbQuery.append(" and se.issuance.idIssuanceCodePk= :idIssuanceCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdSecurityCodePk() )){
			sbQuery.append(" and se.idSecurityCodePk like :idSecurityCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIsinCodePk() )){
			sbQuery.append(" and se.idIsinCode= :idIsinCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityCurrency() )){
			sbQuery.append(" and se.currency= :securityCurrency ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityType() )){
			sbQuery.append(" and se.securityType= :securityType ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityClass())){
			sbQuery.append(" and se.securityClass= :securityClass ");
		}else if(Validations.validateListIsNotNullAndNotEmpty(securityTO.getLstSecurityClass())){
			sbQuery.append(" and se.securityClass in( :lstSecurityClass) ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIssuanceDate() )){
			sbQuery.append(" and se.issuanceDate>= :issuanceDate ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIssuanceDateMax())) {
			sbQuery.append(" and se.issuanceDate<= :issuanceDateMax ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getExpirationDate() )){
			sbQuery.append(" and se.expirationDate= :expirationDate ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdCfiCodePk() )){
			sbQuery.append(" and se.cfiCode = :cfiCode");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getAlternativeCode() )){
			sbQuery.append(" and se.alternativeCode= :alternativeCode ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getDescriptionSecurity() )){
			sbQuery.append(" and se.description like :description ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getSecurityState())){
			sbQuery.append(" and se.stateSecurity= :securityState ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIndIsCoupon() )){
			sbQuery.append(" and se.indIsCoupon= :indIsCoupon ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIndIsDetached() )){
			sbQuery.append(" and se.indIsDetached= :indIsDetached ");
		}
		
		sbQuery.append(" order by se.idSecurityCodePk asc ");
		
    	Query query = em.createQuery(sbQuery.toString());
    	

    	if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuerCodePk()  )){
    		query.setParameter("idIssuerCodePk",  securityTO.getIdIssuerCodePk()  );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuanceCodePk() )){
			query.setParameter("idIssuanceCodePk",  securityTO.getIdIssuanceCodePk() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdSecurityCodePk() )){
			query.setParameter("idSecurityCodePk",  GeneralConstants.PERCENTAGE_STRING +  securityTO.getIdSecurityCodePk()+ GeneralConstants.PERCENTAGE_STRING  );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIsinCodePk() )){
			query.setParameter("idIsinCodePk",  securityTO.getIdIsinCodePk() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityCurrency() )){
			query.setParameter("securityCurrency",  securityTO.getSecurityCurrency() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityType() )){
			query.setParameter("securityType",  securityTO.getSecurityType() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityClass() )){
			query.setParameter("securityClass",  securityTO.getSecurityClass() );
		}else if(Validations.validateListIsNotNullAndNotEmpty(securityTO.getLstSecurityClass())){
			query.setParameter("lstSecurityClass",  securityTO.getLstSecurityClass() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIssuanceDate() )){
			query.setParameter("issuanceDate",  securityTO.getIssuanceDate() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIssuanceDateMax() )){
			query.setParameter("issuanceDateMax",  securityTO.getIssuanceDateMax() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getExpirationDate() )){
			query.setParameter("expirationDate",  securityTO.getExpirationDate() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdCfiCodePk() )){
			query.setParameter("cfiCode",   securityTO.getIdCfiCodePk()  );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getAlternativeCode() )){
			query.setParameter("alternativeCode",  securityTO.getAlternativeCode() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getDescriptionSecurity() )){
			query.setParameter("description",  new StringBuilder()
												.append(GeneralConstants.PERCENTAGE_CHAR)
												.append(securityTO.getDescriptionSecurity())
												.append(GeneralConstants.PERCENTAGE_CHAR).toString())  ;
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getSecurityState())){
			query.setParameter("securityState", Integer.parseInt(securityTO.getSecurityState()));
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIndIsCoupon() )){
			query.setParameter("indIsCoupon",  securityTO.getIndIsCoupon() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIndIsDetached() )){
			query.setParameter("indIsDetached",  securityTO.getIndIsDetached() );
		}
		
    	return  ((List<Security>)query.getResultList());

	}
	
	/**
	 * Find securities list for participant investor.
	 *
	 * @param securityTO the security to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Security> findSecuritiesListForParticipantInvestor(SecurityTO securityTO) throws ServiceException{
		

		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" select distinct se ").
				append("  From HolderAccountBalance hab , Security se   ").
				append("  WHERE   1=1 and hab.totalBalance > 0");

		sbQuery.append("   and hab.security.idSecurityCodePk = se.idSecurityCodePk");		
		
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuerCodePk()  )){
			sbQuery.append(" and se.issuer.idIssuerPk= :idIssuerCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuanceCodePk() )){
			sbQuery.append(" and se.issuance.idIssuanceCodePk= :idIssuanceCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdSecurityCodePk() )){
			sbQuery.append(" and se.idSecurityCodePk= :idSecurityCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIsinCodePk() )){
			sbQuery.append(" and se.idIsinCode= :idIsinCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityCurrency() )){
			sbQuery.append(" and se.currency= :securityCurrency ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityType() )){
			sbQuery.append(" and se.securityType= :securityType ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityClass())){
			sbQuery.append(" and se.securityClass= :securityClass ");
		}else if(Validations.validateListIsNotNullAndNotEmpty(securityTO.getLstSecurityClass())){
			sbQuery.append(" and se.securityClass in( :lstSecurityClass) ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIssuanceDate() )){
			sbQuery.append(" and se.issuanceDate= :issuanceDate ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getExpirationDate() )){
			sbQuery.append(" and se.expirationDate= :expirationDate ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdCfiCodePk() )){
			sbQuery.append(" and se.cfiCode = :cfiCode");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getAlternativeCode() )){
			sbQuery.append(" and se.alternativeCode= :alternativeCode ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getDescriptionSecurity() )){
			sbQuery.append(" and se.description like :description ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getSecurityState())){
			sbQuery.append(" and se.stateSecurity= :securityState ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIndIsCoupon() )){
			sbQuery.append(" and se.indIsCoupon= :indIsCoupon ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIndIsDetached() )){
			sbQuery.append(" and se.indIsDetached= :indIsDetached ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdParticipant())){
			sbQuery.append(" and hab.participant.idParticipantPk = :idParticipantPk ");
		}
		if(Validations.validateListIsNotNullAndNotEmpty(securityTO.getLstHolderAccountPk())){
			sbQuery.append(" and hab.holderAccount.idHolderAccountPk in (:idHolderAccountPk) ");
		}
    	Query query = em.createQuery(sbQuery.toString());
    	

    	if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuerCodePk()  )){
    		query.setParameter("idIssuerCodePk",  securityTO.getIdIssuerCodePk()  );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuanceCodePk() )){
			query.setParameter("idIssuanceCodePk",  securityTO.getIdIssuanceCodePk() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdSecurityCodePk() )){
			query.setParameter("idSecurityCodePk",  securityTO.getIdSecurityCodePk() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIsinCodePk() )){
			query.setParameter("idIsinCodePk",  securityTO.getIdIsinCodePk() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityCurrency() )){
			query.setParameter("securityCurrency",  securityTO.getSecurityCurrency() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityType() )){
			query.setParameter("securityType",  securityTO.getSecurityType() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityClass() )){
			query.setParameter("securityClass",  securityTO.getSecurityClass() );
		}else if(Validations.validateListIsNotNullAndNotEmpty(securityTO.getLstSecurityClass())){
			query.setParameter("lstSecurityClass",  securityTO.getLstSecurityClass() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIssuanceDate() )){
			query.setParameter("issuanceDate",  securityTO.getIssuanceDate() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getExpirationDate() )){
			query.setParameter("expirationDate",  securityTO.getExpirationDate() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdCfiCodePk() )){
			query.setParameter("cfiCode",   securityTO.getIdCfiCodePk()  );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getAlternativeCode() )){
			query.setParameter("alternativeCode",  securityTO.getAlternativeCode() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getDescriptionSecurity() )){
			query.setParameter("description",  new StringBuilder()
												.append(GeneralConstants.PERCENTAGE_CHAR)
												.append(securityTO.getDescriptionSecurity())
												.append(GeneralConstants.PERCENTAGE_CHAR).toString())  ;
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getSecurityState())){
			query.setParameter("securityState", Integer.parseInt(securityTO.getSecurityState()));
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIndIsCoupon() )){
			query.setParameter("indIsCoupon",  securityTO.getIndIsCoupon() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIndIsDetached() )){
			query.setParameter("indIsDetached",  securityTO.getIndIsDetached() );
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdParticipant())){
			query.setParameter("idParticipantPk",  securityTO.getIdParticipant() );
		}
		if(Validations.validateListIsNotNullAndNotEmpty(securityTO.getLstHolderAccountPk())){
			query.setParameter("idHolderAccountPk",  securityTO.getLstHolderAccountPk() );
		}
    	return  ((List<Security>)query.getResultList());

	}
	
	/**
	 * Metodo para cambiar la situacion de un valor a Autorizado
	 */
	public void securityAuthorize(Security security, LoggerUser loggerUser){
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update Security sec ");
		sbQuery.append(" Set sec.indAuthorized=:indAuthorized, ");
		sbQuery.append(" sec.authorizedDate=:lastModifyDatePrm, ");
		sbQuery.append(" sec.authorizedUser=:lastModifyUserPrm, ");
		sbQuery.append(" sec.lastModifyDate=:lastModifyDatePrm, ");
		sbQuery.append(" sec.lastModifyUser=:lastModifyUserPrm, ");
		sbQuery.append(" sec.lastModifyIp=:lastModifyIpPrm, ");
		sbQuery.append(" sec.lastModifyApp=:lastModifyAppPrm ");
		sbQuery.append(" where sec.idSecurityCodePk=:idSecurityCodePk ");
		
		Query query = em.createQuery(sbQuery.toString());
		
		query.setParameter("indAuthorized",GeneralConstants.ONE_VALUE_INTEGER);
		query.setParameter("lastModifyDatePrm",loggerUser.getAuditTime());
		query.setParameter("lastModifyUserPrm",loggerUser.getUserName());
		query.setParameter("lastModifyIpPrm",loggerUser.getIpAddress());
		query.setParameter("lastModifyAppPrm",loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("idSecurityCodePk",security.getIdSecurityCodePk());
	    query.executeUpdate();
		
		
	}
	
	/**
	 * Find security service bean.
	 *
	 * @param securityTO the security to
	 * @return the security
	 * @throws ServiceException the service exception
	 */
	public Security findSecurityServiceBean(SecurityTO securityTO) throws ServiceException{
		CriteriaBuilder cb=em.getCriteriaBuilder();
		CriteriaQuery<Security> cq=cb.createQuery(Security.class);
		Root<Security> securityRoot=cq.from( Security.class );
		securityRoot.fetch("issuer");		
		securityRoot.fetch("issuance");
		securityRoot.fetch("security", JoinType.LEFT);
		securityRoot.fetch("securityOrigin", JoinType.LEFT);
		securityRoot.fetch("idSubproductRefFk", JoinType.LEFT);
		securityRoot.fetch("issuanceCountry");
		securityRoot.fetch("valuatorProcessClass", JoinType.LEFT);
//		securityRoot.fetch("securityInvestor", JoinType.LEFT);
//		securityRoot.fetch("interestPaymentSchedule", JoinType.LEFT);
//		securityRoot.fetch("amortizationPaymentSchedule", JoinType.LEFT);

		List<Predicate> criteriaParameters=new ArrayList<Predicate>();
		cq.select(securityRoot);
		 
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdSecurityCodePk() )){
			criteriaParameters.add(cb.equal(securityRoot.<String>get("idSecurityCodePk"), securityTO.getIdSecurityCodePk() ));
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIsinCodePk() )){
			criteriaParameters.add(cb.equal(securityRoot.<String>get("idIsinCode"), securityTO.getIdIsinCodePk() ));
		}

		if(!criteriaParameters.isEmpty()){
			if (criteriaParameters.size() == 1) {
		        cq.where(criteriaParameters.get(0));
		    } else {
		        cq.where(cb.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()])));
		    }
		}
		
		TypedQuery<Security> security=em.createQuery(cq);
		Security securityRes=null;
		try {
			securityRes=security.getSingleResult();
			
			Map<String,Object> params = new HashMap<String, Object>();
			params.put("idSecurityCodePrm", securityRes.getIdSecurityCodePk());
			SecurityInvestor securityInvestor = findObjectByNamedQuery(SecurityInvestor.SEARCH_BY_SECURITY, params);
			
			securityRes.setSecurityInvestor(securityInvestor);
			if(securityRes.getIssuer().getHolder()!=null){
				securityRes.getIssuer().getHolder().toString();
			}
		} catch(NonUniqueResultException nuex){
			securityRes=null;
			log.info("Security have more codes same!");
		} 
				
		catch(NoResultException nex) {
			securityRes=null;
			log.info("Security not found");
		}
		return securityRes;
	}
	
	public List<SecuritySerialRange> findSecuritySerialRange(String idSecurityCodePk) {
		StringBuilder sd = new StringBuilder();
		sd.append(" SELECT s ");
		sd.append(" FROM SecuritySerialRange s ");
		sd.append(" WHERE s.security.idSecurityCodePk = :idSecurityCodePk AND s.stateRange = :stateRange ");
		
		Query q = em.createQuery(sd.toString());
		q.setParameter("idSecurityCodePk", idSecurityCodePk);
		q.setParameter("stateRange", 1);
		
		return q.getResultList();
	}
	
	/**
	 * Find security negotiation mechanisms service bean.
	 *
	 * @param parameters the parameters
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<SecurityNegotiationMechanism> findSecurityNegotiationMechanismsServiceBean(Map<String,Object> parameters){
		return (List<SecurityNegotiationMechanism>)findWithNamedQuery(SecurityNegotiationMechanism.SEC_NEGOTIATION_MECHANISM_BY_PARAMS, parameters);
	}
	
	
	/**
	 * Update security state service bean.
	 *
	 * @param security the security
	 * @param secHistoryState the sec history state
	 * @throws ServiceException the service exception
	 */
	public void updateSecurityStateServiceBean(Security security,SecurityHistoryState secHistoryState) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update Security s ");
		sbQuery.append(" Set s.stateSecurity=:statePrm, ");
		sbQuery.append(" s.lastModifyDate=:lastModifyDatePrm, ");
		sbQuery.append(" s.lastModifyUser=:lastModifyUserPrm, ");
		sbQuery.append(" s.lastModifyIp=:lastModifyIpPrm, ");
		sbQuery.append(" s.lastModifyApp=:lastModifyAppPrm ");
		sbQuery.append(" where s.idSecurityCodePk=:idSecurityCodePrm");
		
		Query query = em.createQuery(sbQuery.toString());
		
		query.setParameter("statePrm",security.getStateSecurity());
		query.setParameter("lastModifyDatePrm",secHistoryState.getLastModifyDate());
		query.setParameter("lastModifyUserPrm",secHistoryState.getLastModifyUser());
		query.setParameter("lastModifyIpPrm",secHistoryState.getLastModifyIp());
		query.setParameter("lastModifyAppPrm",secHistoryState.getLastModifyApp());
		query.setParameter("idSecurityCodePrm",security.getIdSecurityCodePk());
	    query.executeUpdate();
	    
	    create(secHistoryState);
	}
	
	/**
	 * Validate security menominc service bean.
	 *
	 * @param security the security
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int validateSecurityMenomincServiceBean(Security security) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append("Select count(I) from Security i where i.mnemonic=:mnemonicPrm");
		if(StringUtils.isNotBlank(security.getIdSecurityCodePk())){
			sbQuery.append(" and i.idSecurityCodePk != :idSecurityCodePkPrm");
		}
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("mnemonicPrm", security.getMnemonic());
		if(StringUtils.isNotBlank(security.getIdSecurityCodePk())){
			query.setParameter("idSecurityCodePkPrm", security.getIdSecurityCodePk());
		}
		return Integer.parseInt(  query.getSingleResult().toString()  );
	}
	
	/**
	 * Validate security code service bean.
	 *
	 * @param security the security
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int validateSecurityCodeServiceBean(Security security) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append("Select count(I) from Security i where i.idSecurityCodePk=:idSecurityCodePkPrm");
		
		Query query = em.createQuery(sbQuery.toString());

		query.setParameter("idSecurityCodePkPrm", security.getIdSecurityCodePk());
		
		return Integer.parseInt(  query.getSingleResult().toString()  );		
	}

	/**
	 * Metodo para buscar un valor por formato de codigo.
	 *
	 * @param security the security
	 * @return the security code format
	 * @throws ServiceException the service exception
	 */
	public SecurityCodeFormat findSecurityCodeFormat(Security security) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append("Select s from SecurityCodeFormat s where s.securityClass=:securityClassPrm and s.instrumentType=:instrumentTypePrm");
		SecurityCodeFormat securityCodeFormat=null;
		try {

			Query query = em.createQuery(sbQuery.toString());

			query.setParameter("securityClassPrm", security.getSecurityClass());
			query.setParameter("instrumentTypePrm", security.getInstrumentType());
			
			securityCodeFormat = (SecurityCodeFormat) query.getSingleResult();
			
		} catch(NonUniqueResultException nuex){
			securityCodeFormat=null;
			log.info("SecurityCodeFormat have more codes same!");
		} 
				
		catch(NoResultException nex) {
			securityCodeFormat=null;
			log.info("SecurityCodeFormat not found");
		}
		
		return securityCodeFormat;		
	}
	
	/**
	 * Metodo para validar el codigo isin por parametro de valor
	 *
	 * @param security the security
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int validateSecurityISINServiceBean(Security security) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append("Select count(I) from Security i where i.idSecurityCodePk=:idSecurityCodePkPrm");
		
		Query query = em.createQuery(sbQuery.toString());
        query.setParameter("idSecurityCodePkPrm", security.getIdSecurityCodePk());

		return Integer.parseInt(  query.getSingleResult().toString()  );
	}
	
	/**
	 * Gets the count securities by issuance service bean.
	 *
	 * @param idIssuanceCode the id issuance code
	 * @return the count securities by issuance service bean
	 * @throws ServiceException the service exception
	 */
	public int getCountSecuritiesByIssuanceServiceBean(String idIssuanceCode) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append("Select count(I) from Security i where i.issuance.idIssuanceCodePk=:idIssuanceCodePrm");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idIssuanceCodePrm", idIssuanceCode);

		return Integer.parseInt(  query.getSingleResult().toString()  );
	}
	
	
	/**
	 * Gets the list negotiation mechanism.
	 *
	 * @return the list negotiation mechanism
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<NegotiationMechanism> getListNegotiationMechanism() throws ServiceException{
		String strQuery = " Select nm" +
							" From NegotiationMechanism nm" +
							" Where nm.stateMechanism = :state";
		Query query = em.createQuery(strQuery);
		query.setParameter("state", NegotiationMechanismStateType.ACTIVE.getCode());
		return (List<NegotiationMechanism>)query.getResultList();
	}
	
	/**
	 * Gets the list negotiation modalities service bean.
	 *
	 * @param params the params
	 * @return the list negotiation modalities service bean
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<NegotiationModality> getListNegotiationModalitiesServiceBean(Map<String, Object> params) throws ServiceException{
		StringBuilder sb=new StringBuilder("Select distinct nm from	NegotiationModality nm");
		sb.append(" join Fetch nm.mechanismModalities mm");
		sb.append(" Where nm.modalityState = :modalityStatePrm And mm.stateMechanismModality = :stateMechanismModalityPrm");
		sb.append(" and nm.instrumentType=:instrumentTypePrm");
		sb.append(" Order By nm.modalityCode");
		
		Query query=em.createQuery( sb.toString() );
		query.setParameter("modalityStatePrm", params.get("negotiationModalityState"));
		query.setParameter("stateMechanismModalityPrm", params.get("mechanismModalityState"));
		query.setParameter("instrumentTypePrm", params.get("instrumentType"));
		
		return (List<NegotiationModality>) query.getResultList();
	}
	
	
	/**
	 * Validate security serie service bean.
	 *
	 * @param security the security
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int validateSecuritySerieServiceBean(Security security) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append( "Select count(v) from Security v where v.securitySerial=:securitySerialPrm and v.issuance.idIssuanceCodePk=:idIssuanceCodePkPrm" );
		if(Validations.validateIsNotNullAndNotEmpty(security.getIdSecurityCodePk())){
			sbQuery.append(" and v.idSecurityCodePk != :idSecurityCodePrm ");
		}
		Query query = em.createQuery(sbQuery.toString());
		
		query.setParameter( "securitySerialPrm" , security.getSecuritySerial());
		query.setParameter( "idIssuanceCodePkPrm" , security.getIssuance().getIdIssuanceCodePk() );
		if(Validations.validateIsNotNullAndNotEmpty(security.getIdSecurityCodePk())){
			query.setParameter( "idSecurityCodePrm" , security.getIdSecurityCodePk() );
		}
		
		return Integer.parseInt( query.getSingleResult().toString() );
	}
	
	/**
	 * Update security int pay sche field.
	 *
	 * @param security the security
	 * @throws ServiceException the service exception
	 */
	public void updateSecurityIntPayScheField(Security security) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update Security s ");
		sbQuery.append(" Set s.interestType=:interestTypePrm, ");
		sbQuery.append(" s.interestRate=:interestRatePrm, ");
		sbQuery.append(" s.periodicity=:periodicityPrm, ");
		sbQuery.append(" s.calendarMonth=:calendarMonthPrm, ");
		sbQuery.append(" s.calendarType=:calendarTypePrm, ");
		sbQuery.append(" s.calendarDays=:calendarDaysPrm, ");
		sbQuery.append(" s.rateType=:rateTypePrm, ");
		sbQuery.append(" s.rateValue=:rateValuePrm, ");
		sbQuery.append(" s.operationType=:operationTypePrm, ");
		sbQuery.append(" s.spread=:spreadPrm, ");
		sbQuery.append(" s.minimumRate=:minimumRatePrm, ");
		sbQuery.append(" s.maximumRate=:maximumRatePrm, ");
		
		sbQuery.append(" s.lastModifyDate=:lastModifyDatePrm, ");
		sbQuery.append(" s.lastModifyUser=:lastModifyUserPrm, ");
		sbQuery.append(" s.lastModifyIp=:lastModifyIpPrm, ");
		sbQuery.append(" s.lastModifyApp=:lastModifyAppPrm ");
		sbQuery.append(" where s.idSecurityCodePk=:idSecurityCodePkPrm");
		
		Query query = em.createQuery(sbQuery.toString());
		
		query.setParameter("interestTypePrm",security.getInterestType());
		query.setParameter("interestRatePrm",security.getInterestRate());
		query.setParameter("periodicityPrm",security.getPeriodicity());
		query.setParameter("calendarMonthPrm",security.getCalendarMonth());
		query.setParameter("calendarTypePrm",security.getCalendarType());
		query.setParameter("calendarDaysPrm",security.getCalendarDays());
		query.setParameter("rateTypePrm",security.getRateType());
		query.setParameter("rateValuePrm",security.getRateValue());
		query.setParameter("operationTypePrm",security.getOperationType());
		query.setParameter("spreadPrm",security.getSpread());
		query.setParameter("minimumRatePrm",security.getMinimumRate());
		query.setParameter("maximumRatePrm",security.getMaximumRate());
		
		query.setParameter("lastModifyDatePrm",security.getLastModifyDate());
		query.setParameter("lastModifyUserPrm",security.getLastModifyUser());
		query.setParameter("lastModifyIpPrm",security.getLastModifyIp());
		query.setParameter("lastModifyAppPrm",security.getLastModifyApp());
		query.setParameter("idSecurityCodePkPrm",security.getIdSecurityCodePk());
	    query.executeUpdate();
	}
	
	/**
	 * Find security foreign depositories.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @param state the state
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SecurityForeignDepository> findSecurityForeignDepositories(String idSecurityCodePk, Integer state) throws ServiceException{
		Map<String,Object> params=new HashMap<String, Object>();
		params.put("idSecurityCodePrm", idSecurityCodePk);
		params.put("statePrm", state);
	
		return findWithNamedQuery(SecurityForeignDepository.SEARCH_BY_SECURITY_AND_STATE, params);
	}


	/**
	 * List security movement result.
	 *
	 * @param securityBalanceMovTO the security balance mov to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SecurityMovementsResultTO> listSecurityMovementResult(
			SecurityBalanceMovTO securityBalanceMovTO) throws ServiceException{
		
		List<SecurityMovementsResultTO> lstMovements = new ArrayList<SecurityMovementsResultTO>();
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select ");
		sbQuery.append(" SM.idSecuritiesMovementPk, ");
		sbQuery.append(" SM.movementDate, ");
		sbQuery.append(" SOP.operationDate, ");
		sbQuery.append(" SOP.corporativeOperation.idCorporativeOperationPk, ");
		sbQuery.append(" MT.idMovementTypePk, ");
		sbQuery.append(" MT.movementName, ");
		sbQuery.append(" SM.movementQuantity, ");
		sbQuery.append(" SM.nominalValue, ");
		sbQuery.append(" SOP.marketPrice, ");
		sbQuery.append(" SOP.marketRate, ");
		sbQuery.append(" SOP.marketDate, ");
		sbQuery.append(" CO.operationNumber, "); //11
		sbQuery.append(" TO.idTradeOperationPk "); //12
		sbQuery.append(" From ");
		sbQuery.append(" SecuritiesMovement SM ");
		sbQuery.append(" Join SM.securitiesOperation SOP ");
		sbQuery.append(" Join SM.movementType MT ");
		sbQuery.append(" left Join SOP.custodyOperation CO ");
		sbQuery.append(" left Join SOP.tradeOperation TO ");
		sbQuery.append(" Where SM.securities.idSecurityCodePk = :securityCodePkPrm ");
		sbQuery.append(" And trunc(SM.movementDate) Between trunc(:issuanceDatePrm) and trunc(:finalDatePrm) ");
		sbQuery.append(" Order by SM.movementDate desc ");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("securityCodePkPrm", securityBalanceMovTO.getSecurity().getIdSecurityCodePk());
		query.setParameter("issuanceDatePrm", securityBalanceMovTO.getIssuanceDate(), TemporalType.DATE);
		query.setParameter("finalDatePrm", securityBalanceMovTO.getFinalDate(), TemporalType.DATE);
		
		List<Object[]> lstResult = (List<Object[]>)query.getResultList();
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
			SecurityMovementsResultTO objSecurityMovementsResultTO;
			for (Object[] objMov : lstResult) {
				objSecurityMovementsResultTO = new SecurityMovementsResultTO();
				objSecurityMovementsResultTO.setIdSecuritiesMovementPk((Long)objMov[0]);
				objSecurityMovementsResultTO.setMovementDate((Date)objMov[1]);
				objSecurityMovementsResultTO.setOperationDate((Date)objMov[2]);
				objSecurityMovementsResultTO.setIdCorporativeOperationPk((Long)objMov[3]);
				objSecurityMovementsResultTO.setMovementType((Long)objMov[4]);
				objSecurityMovementsResultTO.setMovementTypeDescription((String)objMov[5]);
				objSecurityMovementsResultTO.setMovementQuantity((BigDecimal)objMov[6]);
				objSecurityMovementsResultTO.setNominalValue((BigDecimal)objMov[7]);
				objSecurityMovementsResultTO.setMarketPrice((BigDecimal)objMov[8]);
				objSecurityMovementsResultTO.setMarketRate((BigDecimal)objMov[9]);
				objSecurityMovementsResultTO.setMarketDate((Date)objMov[10]);	
				if(objMov[3]!=null){
					objSecurityMovementsResultTO.setOperationNumber( (Long)objMov[3] );
				}else if(objMov[11]!=null){
					objSecurityMovementsResultTO.setOperationNumber( (Long)objMov[11] );
				}else if(objMov[12]!=null){
					objSecurityMovementsResultTO.setOperationNumber( (Long)objMov[12] );
				}
				lstMovements.add(objSecurityMovementsResultTO);
			}
		}
		
		return lstMovements;
	}
	
	/**
	 * Validate and save issuance tx.
	 *
	 * @param issuanceRegisterTO the issuance register to
	 * @param loggerUser the logger user
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW) 
	@Interceptors(ContextHolderInterceptor.class)
	public RecordValidationType validateAndSaveIssuanceTx(IssuanceRegisterTO issuanceRegisterTO, LoggerUser loggerUser) throws ServiceException{
		
		Object objResult =  validateAndSaveIssuance(issuanceRegisterTO, loggerUser);
		
		if(objResult instanceof RecordValidationType){
			throw new ServiceException((RecordValidationType) objResult);
		}else{
			return CommonsUtilities.populateRecordCodeValidation(issuanceRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
		}
	} 
	
	
	/**
	 * Validate and save issuance.
	 *
	 * @param issuanceRegisterTO the issuance register to
	 * @param loggerUser the logger user
	 * @return the object
	 * @throws ServiceException the service exception
	 */
	public Object validateAndSaveIssuance(IssuanceRegisterTO issuanceRegisterTO, LoggerUser loggerUser) 
			throws ServiceException {
		
		//CHECK OPERATION TYPE

		if(!ComponentConstant.INTERFACE_SECURITIES_REGISTER.equals(issuanceRegisterTO.getOperationCode())){			
			return CommonsUtilities.populateRecordCodeValidation(issuanceRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION, null);
		}
		
		//CHECK OPERATION DATE IS TODAY
		// se verifica si la fecha de operacion es valida
		Date operationDate = CommonsUtilities.truncateDateTime(issuanceRegisterTO.getOperationDate());
		if(operationDate.compareTo(CommonsUtilities.currentDate()) != 0){
			return CommonsUtilities.populateRecordCodeValidation(issuanceRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SEND_DATE, null);
		}
		
		//VALIDATE PARTICIPANT OperationInterfaceTO
		// se verifica si la entidad financiera, emisor. participante es valido
		Participant objParticipant = new Participant();
		objParticipant.setMnemonic(issuanceRegisterTO.getEntityNemonic());
		objParticipant = participantServiceBean.findParticipantByFiltersServiceBean(objParticipant);		
		if(objParticipant == null  
			
				// issue 1248: se quita esta restriccion porque a fututo se tiene pensado que este metodo sea para todos los participantes.
				/*	|| ( 	!objParticipant.getIdParticipantPk().equals(idParticipantBC) 
						&& !objParticipant.getIdParticipantPk().equals(idParticipantVUN)
						)
				 */ 
			){				
			return CommonsUtilities.populateRecordCodeValidation(issuanceRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_ENTITY_CODE, null);
		} else {
			issuanceRegisterTO.setIdParticipant(objParticipant.getIdParticipantPk());
		}
		
		//CHECK OPERATION NUMBER EXITS
		// verificando si el nro de operacion ya fue procesado anteriormente con exito
		List<InterfaceTransaction> lstInterfaceTransaction = parameterServiceBean.getListInterfaceTransaction(
						objParticipant.getIdParticipantPk(), issuanceRegisterTO.getOperationNumber(), 
						// issue 1239: se adiona una L por que en la BBDD esta con RVAL (para diferenciar del otro RVA)
						ComponentConstant.INTERFACE_ISUANCE_SECURITIES);
		if(Validations.validateListIsNotNullAndNotEmpty(lstInterfaceTransaction)){
			return CommonsUtilities.populateRecordCodeValidation(issuanceRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_DUPLICATE_OPERATION_TAG_OPERATION, null);
		}
		
		// validadndo datos de la emision
		Issuance issuance = new Issuance();
		RecordValidationType issuanceValidation = validateIssuanceCreation(issuanceRegisterTO.getIssuanceObjectTO(),issuance);
		if(issuanceValidation != null){
			issuanceValidation.setOperationRef(issuanceRegisterTO);
			return issuanceValidation;
		}
		
		// issue 1239: Si la clase es BBX la emsion debe tener las siguientes caracterisiticas
		if(Validations.validateIsNotNull(issuance)
				&& Validations.validateIsNotNull(issuance.getSecurityClass())
				&& issuance.getSecurityClass().equals(SecurityClassType.BBX.getCode()) ){
			// colocacion primaria SI
			issuance.setIndPrimaryPlacement(BooleanType.YES.getCode());
			// tipo de colocacion FIJA
			issuance.setPlacementType(SecurityPlacementType.FIXED.getCode());
			// tramo de colocacion vacio
			issuance.setNumberTotalTranches(null);
			issuance.setNumberPlacementTranches(null);
			
			// A requerimiento de usuario, para valores del BCB la Calificacion siempre es AAA, ParameterTable = 764 Mapear en un Enum
			issuance.setCreditRatingScales(764);
		}
		
		//ISSUE 1248: Si se trata de un PGE la calificacion ya no es: AAA, tiene que estar vacio
		if(Validations.validateIsNotNull(issuance)
				&& Validations.validateIsNotNull(issuance.getSecurityClass())
				&& issuance.getSecurityClass().equals(SecurityClassType.PGE.getCode()) ){
			
			
			if(Validations.validateIsNotNull(issuance.getIssuanceDate())
					&& Validations.validateIsNotNull(issuance.getExpirationDate())){
				
				Date expirationDate = CommonsUtilities.truncateDateTime(issuance.getExpirationDate());
				Date issuanceDate = CommonsUtilities.truncateDateTime(issuance.getIssuanceDate());
				
				int differenceDays=CommonsUtilities.getDaysBetween(issuanceDate,expirationDate);
				if(differenceDays > 360){
					// ParameterTable 2322 = NO APLICA (mapear en un enum)
					issuance.setCreditRatingScales(2322);
				}else{
					// ParameterTable 2321 = NO APLICA (mapear en un enum)
					issuance.setCreditRatingScales(2321);
				}
				
			}
			
			// issue 1248:  Identificando la forma de emsion para PGE
			if(Validations.validateIsNotNull(issuance.getSecurityType())
					&& issuance.getSecurityType().equals(SecurityType.PRIVATE.getCode())){
				
				for(SecurityObjectTO securityObjectTO : issuanceRegisterTO.getLstSecurityObjectTO()){
					if(Validations.validateListIsNotNullAndNotEmpty(securityObjectTO.getLstCouponObjectTOs())
						 	&& securityObjectTO.getLstCouponObjectTOs().size() > 1 ){
						issuance.setIssuanceType(IssuanceType.MIXED.getCode());
					}else{
						issuance.setIssuanceType(IssuanceType.PHYSICAL.getCode());
					}
					break;
				}
				
			}
		}
		
		issuanceServiceBean.registryIssuanceServiceBean(issuance, loggerUser);
		
		String idSecurityCode = null;
		for(SecurityObjectTO securityObjectTO : issuanceRegisterTO.getLstSecurityObjectTO()){
			
			issuanceRegisterTO.setSecurityObjectRef(securityObjectTO);
			securityObjectTO.setSecurityClass(issuanceRegisterTO.getIssuanceObjectTO().getSecurityClass());
			securityObjectTO.setSecurityClassDesc(issuanceRegisterTO.getIssuanceObjectTO().getSecurityClassDesc());
			
			RecordValidationType securityObjectValidation = validateSecurityObject(securityObjectTO);
			if(securityObjectValidation != null){
				return securityObjectValidation;
			}
			
			Security security=new Security();
			security.setIssuance(issuance);
			RecordValidationType securityValidation = validateSecurityCreation(securityObjectTO,security, loggerUser);
			if(securityValidation != null){
				securityValidation.setOperationRef(issuanceRegisterTO);
				return securityValidation;
			}
			
			// si es BTX O BBX protesto en NO
			if(Validations.validateIsNotNull(securityObjectTO.getSecurityClass())
					&& (securityObjectTO.getSecurityClass().equals(SecurityClassType.BBX.getCode()) 
							|| securityObjectTO.getSecurityClass().equals(SecurityClassType.BTX.getCode()))){
				security.setPaymentAgent(objParticipant.getMnemonic());
				security.setIndTraderSecondary(BooleanType.NO.getCode());
				security.setIndIssuanceProtest(BooleanType.NO.getCode());
				security.setMinimumInvesment(new BigDecimal("1000"));
				security.setMaximumInvesment(BigDecimal.ZERO);
				if(securityObjectTO.getSecurityClass().equals(SecurityClassType.BBX.getCode())){
					security.setCouponFirstExpirationDate(security.getExpirationDate());
					security.setPaymentFirstExpirationDate(security.getExpirationDate());
					security.setAmortizationOn(AmortizationOnType.CAPITAL.getCode());
					security.setAmortizationType(AmortizationType.NO_PROPORTIONAL.getCode());
					security.getAmortizationPaymentSchedule().setAmortizationType(AmortizationType.NO_PROPORTIONAL.getCode());
				}
			}
			
			// issue 1248: Si se trata de un PGE se guarda en estado NO autorizado y el protestoEmision en: NO
			if(Validations.validateIsNotNull(security)
					&& Validations.validateIsNotNull(security.getSecurityClass())
					&& security.getSecurityClass().equals(SecurityClassType.PGE.getCode()) ){
				security.setIndAuthorized(BooleanType.NO.getCode());
				security.setIndIssuanceProtest(BooleanType.NO.getCode());
				
				// el agente pagador deberia ser el mismo emisor solo caso PGE
				if(Validations.validateIsNotNull(security.getIssuer())
						&& Validations.validateIsNotNull(security.getIssuer().getMnemonic())){
					security.setPaymentAgent(security.getIssuer().getMnemonic());
				}

				if(Validations.validateIsNotNull(securityObjectTO.getPeriodicity())){
					security.setPeriodicityDays(securityObjectTO.getPeriodicity());
				}
				if(Validations.validateIsNotNull(securityObjectTO.getCouponFirstExpirationDate())){
					security.setCouponFirstExpirationDate(securityObjectTO.getCouponFirstExpirationDate());
				}
				security.setPeriodicity(InterestPeriodicityType.BY_DAYS.getCode());
				
				// esto es para que entre en custodia fisica
				security.setPhysicalBalance(BigDecimal.ONE);
				security.setPhysicalDepositBalance(BigDecimal.ZERO);
				security.setCirculationBalance(BigDecimal.ONE);
				security.setShareBalance(BigDecimal.ONE);
				
				// si es de oferta privada se setea el VN en CirculationAmount y ShareCapital
				if(Validations.validateIsNotNull(issuance.getSecurityType())
						&& issuance.getSecurityType().equals(SecurityType.PRIVATE.getCode())
						&& Validations.validateIsNotNull(security.getInitialNominalValue())){
					security.setCirculationAmount(security.getInitialNominalValue());
					security.setShareCapital(security.getInitialNominalValue());
				}
			}
			
			securitiesServiceBean.registrySecurityServiceBean(security, loggerUser, null, null, null, null);
			idSecurityCode =security.getIdSecurityCodePk();
		}
		
		issuanceRegisterTO.setTransactionState(BooleanType.YES.getCode());
		issuanceRegisterTO.setIdSecurityCode(idSecurityCode);
		return issuanceRegisterTO;
	}
	
	/**
	 * Validate and save security.
	 *
	 * @param securityObjectTO the security object to
	 * @param loggerUser the logger user
	 * @return the object
	 */
	public Object validateAndSaveSecurity(SecurityObjectTO securityObjectTO, LoggerUser loggerUser) {
		
		List<NegotiationModality> lsNegotiationModality = null;
		
		RecordValidationType securityObjectValidation = validateSecurityObject(securityObjectTO);
		if(securityObjectValidation != null){
			return securityObjectValidation;
		}
		
		Issuance issuance = new Issuance();
		RecordValidationType issuanceValidation = validateIssuanceCreationBySecurity(securityObjectTO,issuance);
		if(issuanceValidation != null){
			return issuanceValidation;
		}
		
		try {
			
			issuanceServiceBean.registryIssuanceServiceBean(issuance, loggerUser);
			Issuance objIssuance = find(Issuance.class, issuance.getIdIssuanceCodePk());
			Security security=new Security();
			security.setIssuance(objIssuance);
			RecordValidationType securityValidation = validateSecurityCreation(securityObjectTO,security, loggerUser);
			if(securityValidation != null){
				return securityValidation;
			}
			if(securityObjectTO.isDpfInterface()){	
				if(Validations.validateIsNotNullAndNotEmpty(security.getNotTraded()) && 
						GeneralConstants.ZERO_VALUE_INTEGER.equals(security.getNotTraded())){
					lsNegotiationModality = getLstNegotiationModalityDpf(security.getInstrumentType());
				}				
			}			
			
			securitiesServiceBean.registrySecurityServiceBean(security, loggerUser, null, lsNegotiationModality,null, null);
			securityObjectTO.setIdSecurityCode(security.getIdSecurityCodePk());
			
		} catch (ServiceException e) {
			RecordValidationType recordError = CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_UNDEFINED_ERROR, null);
			recordError.setErrorDescription(PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_EXCEPTIONS,"es", e.getMessage()));
			return recordError;
		}
		
		return securityObjectTO;
	}
	

	/**
	 * Validate security object.
	 *
	 * @param securityObjectTO the security object to
	 * @return the record validation type
	 */
	private RecordValidationType validateSecurityObject(SecurityObjectTO securityObjectTO) {

		ParameterTableTO objParameterTableTO = new ParameterTableTO();
		
		if(securityObjectTO.getCurrencyMnemonic()!=null){
			objParameterTableTO = new ParameterTableTO();
			objParameterTableTO.setText1(securityObjectTO.getCurrencyMnemonic());		
			objParameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			List<ParameterTable> lstCurrency = parameterServiceBean.getListParameterTableServiceBean(objParameterTableTO);			
			if(Validations.validateListIsNotNullAndNotEmpty(lstCurrency)){
				ParameterTable objParameterTable = lstCurrency.get(0);				
				if(Validations.validateIsNotNullAndNotEmpty(securityObjectTO.getCurrencySource())){
					if(!objParameterTable.getParameterTablePk().equals(securityObjectTO.getCurrencySource())){
						return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CURRENCY_SOURCE, null);
					}
				}				
				securityObjectTO.setCurrency(objParameterTable.getParameterTablePk());				
			} else {							
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CURRENCY, null);
			}
		}
		
		if (securityObjectTO.isDpfInterface()){			
			
			Integer calendarDays = new Integer(CommonsUtilities.getDaysBetween(securityObjectTO.getIssuanceDate(), securityObjectTO.getExpirationDate()));
			if(!calendarDays.equals(securityObjectTO.getCalendarDays())){
				return CommonsUtilities.populateRecordCodeValidation(null, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_EXPIRATION_DAYS, null);
			}
			
			securityObjectTO.setCapitalPaymentModality(CapitalPaymentModalityType.AT_MATURITY.getCode());
			securityObjectTO.setSecurityType(SecurityType.PUBLIC.getCode());
			securityObjectTO.setInstrumentType(InstrumentType.FIXED_INCOME.getCode());
			
			if (SecurityClassType.DPF.getText1().equalsIgnoreCase(securityObjectTO.getSecurityClassDesc())) {
				securityObjectTO.setSecurityClass(SecurityClassType.DPF.getCode());
				securityObjectTO.setSecurityClassDesc(SecurityClassType.DPF.getText1());
			}
			securityObjectTO.setStockQuantity(BigDecimal.ONE);
			
			//DPA - issue 158
			if(securityObjectTO.getInstrumentTypeMnemonic() != null){
				if (!securityObjectTO.getInstrumentTypeMnemonic().equals(SecurityClassType.DPA.getText1())){
					objParameterTableTO = new ParameterTableTO();	
					objParameterTableTO.setMasterTableFk(MasterTableType.INTEREST_TYPE.getCode());
					objParameterTableTO.setIndicator6(securityObjectTO.getInterestType());
					List<ParameterTable> lstInterestType = parameterServiceBean.getListParameterTableServiceBean(objParameterTableTO);			
					if(Validations.validateListIsNotNullAndNotEmpty(lstInterestType)){	
						ParameterTable objParameterTable = lstInterestType.get(0);				
						securityObjectTO.setInterestType(objParameterTable.getParameterTablePk());		
					} else {							
						return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_INTEREST_TYPE, null);
					}
				}else{
					securityObjectTO.setInterestType(InterestType.CERO.getCode());	
				}
			}else{
				objParameterTableTO = new ParameterTableTO();	
				objParameterTableTO.setMasterTableFk(MasterTableType.INTEREST_TYPE.getCode());
				objParameterTableTO.setIndicator6(securityObjectTO.getInterestType());
				List<ParameterTable> lstInterestType = parameterServiceBean.getListParameterTableServiceBean(objParameterTableTO);			
				if(Validations.validateListIsNotNullAndNotEmpty(lstInterestType)){	
					ParameterTable objParameterTable = lstInterestType.get(0);				
					securityObjectTO.setInterestType(objParameterTable.getParameterTablePk());		
				} else {							
					return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_INTEREST_TYPE, null);
				}
			}
			
			objParameterTableTO = new ParameterTableTO();
			objParameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			objParameterTableTO.setIndicator6(securityObjectTO.getIndexed());
			List<ParameterTable> lstCurrencyIndexed = parameterServiceBean.getListParameterTableServiceBean(objParameterTableTO);			
			if(Validations.validateListIsNotNullAndNotEmpty(lstCurrencyIndexed)){	
				ParameterTable objParameterTable = lstCurrencyIndexed.get(0);				
				securityObjectTO.setIndexed(objParameterTable.getParameterTablePk());								
			} else {							
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_INDEXED_CURRENCY, null);
			}
			
			if(securityObjectTO.getPeriodicity()!=null){
				objParameterTableTO = new ParameterTableTO();	
				objParameterTableTO.setMasterTableFk(MasterTableType.INTEREST_PERIODICITY.getCode());
				objParameterTableTO.setParameterTableCd(securityObjectTO.getPeriodicity().toString());
				List<ParameterTable> lstInterestPeriodicity = parameterServiceBean.getListParameterTableServiceBean(objParameterTableTO);			
				if(Validations.validateListIsNotNullAndNotEmpty(lstInterestPeriodicity)){	
					ParameterTable objParameterTable = lstInterestPeriodicity.get(0);	
					securityObjectTO.setPeriodicity(objParameterTable.getParameterTablePk());
				} else {							
					return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_INTEREST_PAYMENT_PERIODICITY, null);
				}
			}
			//DPA - issue 158
			objParameterTableTO = new ParameterTableTO();	
			objParameterTableTO.setMasterTableFk(MasterTableType.INTEREST_PAYMENT_MODALITY.getCode());
			
			if(securityObjectTO.getInstrumentTypeMnemonic() != null){
				if (securityObjectTO.getInstrumentTypeMnemonic().equals(SecurityClassType.DPA.getText1())){
					if(securityObjectTO.getInterestPaymentModality().equals(GeneralConstants.THREE_VALUE_INTEGER)){
						securityObjectTO.setInterestPaymentModality(GeneralConstants.TWO_VALUE_INTEGER);
					}else{
						return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_INTEREST_PAYMENT_MODALITY, null);
					}
				}
			}

			objParameterTableTO.setIndicator6(securityObjectTO.getInterestPaymentModality());
			List<ParameterTable> lstInterestPaymentMod = parameterServiceBean.getListParameterTableServiceBean(objParameterTableTO);			
			if(Validations.validateListIsNotNullAndNotEmpty(lstInterestPaymentMod)){	
				ParameterTable objParameterTable = lstInterestPaymentMod.get(0);	
				securityObjectTO.setInterestPaymentModality(objParameterTable.getParameterTablePk());
			} else {							
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_INTEREST_PAYMENT_MODALITY, null);
			}
			
			objParameterTableTO = new ParameterTableTO();	
			objParameterTableTO.setMasterTableFk(MasterTableType.SECURITY_SOURCE.getCode());
			objParameterTableTO.setIndicator6(securityObjectTO.getSecuritySource());
			List<ParameterTable> lstSecuritySource = parameterServiceBean.getListParameterTableServiceBean(objParameterTableTO);			
			if(Validations.validateListIsNotNullAndNotEmpty(lstSecuritySource)){	
				ParameterTable objParameterTable = lstSecuritySource.get(0);	
				securityObjectTO.setSecuritySource(objParameterTable.getParameterTablePk());
			} else {							
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_ISSUANCE_SOURCE, null);
			}
			
		}else{
			// validadndo la clase de valor
			objParameterTableTO = new ParameterTableTO();	
			objParameterTableTO.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			objParameterTableTO.setParameterTablePk(securityObjectTO.getSecurityClass());
			List<ParameterTable> lstSecClass = parameterServiceBean.getListParameterTableServiceBean(objParameterTableTO);			
			if(Validations.validateListIsNotNullAndNotEmpty(lstSecClass)){	
				ParameterTable objParameterTable = lstSecClass.get(0);				
				securityObjectTO.setSecurityClass(objParameterTable.getParameterTablePk());
				securityObjectTO.setSecurityClassDesc(objParameterTable.getText1());		
			} else {							
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CLASS, null);
			}
		}
		
		return null;
	}


	/**
	 * Validate issuance creation.
	 *
	 * @param issuanceObjectTO the issuance object to
	 * @param issuance the issuance
	 * @return the record validation type
	 */
	public RecordValidationType validateIssuanceCreation(IssuanceObjectTO issuanceObjectTO, Issuance issuance){
		
		Map<String,Object> params;
		List<ParameterTable> lstParameter = null;
		
		IssuerTO issuerTO=new IssuerTO();
		issuerTO.setMnemonic(issuanceObjectTO.getIssuerMnemonic());
		Issuer issuer = issuerServiceBean.findIssuerServiceBean(issuerTO);
		
		if(issuer == null){
			return CommonsUtilities.populateRecordCodeValidation(null,null,PropertiesConstants.ISSUER_ERROR_NOT_FOUND);
		}
		
		params=new HashMap<String, Object>();
		params.put("economicSectorPrm",issuer.getEconomicSector());
		params.put("offerType",issuanceObjectTO.getSecurityType());
		params.put("instrumentType",issuanceObjectTO.getInstrumentType());
		params.put("securityClassPrm",issuanceObjectTO.getSecurityClass());
		lstParameter=securitiesServiceBean.getListSecuritiesClassSetup(params);
		if(Validations.validateListIsNullOrEmpty(lstParameter)){
			return CommonsUtilities.populateRecordCodeValidation(null,null,PropertiesConstants.ISSUANCE_ERROR_SECURITY_CLASS_SETUP);
		}
		
		//check country
		params=new HashMap<String, Object>();
		if(issuanceObjectTO.getCodeCountryLocation()!=null){
			params.put("codeGeographicLocPrm", issuanceObjectTO.getCodeCountryLocation());
		}else{
			params.put("codeGeographicLocPrm", geographicLocation);
		}
		params.put("typeGeographicLocPrm", GeographicLocationType.COUNTRY.getCode());
		GeographicLocation geographicLocation = findObjectByNamedQuery(GeographicLocation.FIND_BY_CODE_GEO_LOC, params);
		if(geographicLocation.getIdGeographicLocationPk().compareTo(countryResidence)!=0){
			return CommonsUtilities.populateRecordCodeValidation(null,null,PropertiesConstants.ISSUANCE_ERROR_COUNTRY_INCORRECT);
		}else{
			issuance.setGeographicLocation(geographicLocation);
		}
		
		//Dates
		if(CommonsUtilities.isLessOrEqualDate(issuanceObjectTO.getExpirationDate(), issuanceObjectTO.getIssuanceDate(), true)){
			return CommonsUtilities.populateRecordCodeValidation(null,null,PropertiesConstants.ISSUANCE_ERROR_EXPIRATION_DATE_LESS);
		}
		
		issuance.setSecurityClass( issuanceObjectTO.getSecurityClass() );
		issuance.setIssuer(issuer);
		issuance.setDescription( issuanceObjectTO.getIssuanceDescription() );
		issuance.setInstrumentType( issuanceObjectTO.getInstrumentType() );
		issuance.setSecurityType( issuanceObjectTO.getSecurityType()  );
		issuance.setSecurityClass( issuanceObjectTO.getSecurityClass() );
		issuance.setCurrency( issuanceObjectTO.getCurrency() );		
		issuance.setIssuanceDate( issuanceObjectTO.getIssuanceDate() );
		issuance.setExpirationDate( issuanceObjectTO.getExpirationDate() );
		issuance.setIndPrimaryPlacement(issuanceObjectTO.getIndPrimaryPlacement());
		
		//Dpf Dpa Dates		
		if(issuance.isDpfDpaSecurityClass()){
			Integer issuanceDaysTerm = CommonsUtilities.getDaysBetween(issuance.getIssuanceDate(), issuance.getExpirationDate());
			if(issuanceDaysTerm.compareTo(GeneralConstants.DPA_DPF_MIN_DAYS_TERM)<GeneralConstants.ZERO_VALUE_INT){
				return CommonsUtilities.populateRecordCodeValidation(null,null,PropertiesConstants.ISSUANCE_ERROR_DPA_DPF_MIN_DAYS_TERM);
			}
		}
		
		issuance.setIndPrimaryPlacement(BooleanType.NO.getCode());
		issuance.setPlacementType(null);
		issuance.setNumberTotalTranches(null);
		issuance.setIssuanceType(IssuanceType.DEMATERIALIZED.getCode());		
		issuance.setResolutionNumber( issuanceObjectTO.getResolutionNumber() );
		issuance.setResolutionDate( issuanceObjectTO.getResolutionDate() );
		issuance.setIndRegulatorReport(BooleanType.YES.getCode());
		issuance.setRegistryDate(new Date());
		
		issuance.setIssuanceAmount(issuanceObjectTO.getIssuanceAmount());
		issuance.setPlacedAmount( BigDecimal.ZERO );
		issuance.setAmortizationAmount( BigDecimal.ZERO );
		issuance.setCreditRatingScales( getCreditRatingScales(issuanceObjectTO, issuance) );
		return null;
	}
	

	/**
	 * Validate issuance creation by security.
	 *
	 * @param securityObjectTO the security object to
	 * @param issuance the issuance
	 * @return the record validation type
	 */
	public RecordValidationType validateIssuanceCreationBySecurity(SecurityObjectTO securityObjectTO, 
																Issuance issuance){
		
		IssuanceObjectTO issuanceObjectTO = new IssuanceObjectTO();
		issuanceObjectTO.setDpfInterface(securityObjectTO.isDpfInterface());
		issuanceObjectTO.setIssuerMnemonic(securityObjectTO.getIssuerMnemonic());
		issuanceObjectTO.setSecurityType(securityObjectTO.getSecurityType());
		//issuanceObjectTO.setSecurityTypeDesc(securityObjectTO.getSecurityTypeDesc());
		issuanceObjectTO.setInstrumentType(securityObjectTO.getInstrumentType());
		//issuanceObjectTO.setInstrumentTypeDesc(securityObjectTO.getInstrumentTypeDesc());
		issuanceObjectTO.setSecurityClass(securityObjectTO.getSecurityClass());
		issuanceObjectTO.setSecurityClassDesc(securityObjectTO.getSecurityClassDesc());
		issuanceObjectTO.setCodeGeographicLocation(securityObjectTO.getCodeGeographicLocation());
		issuanceObjectTO.setIssuanceDate(securityObjectTO.getIssuanceDate());
		issuanceObjectTO.setExpirationDate(securityObjectTO.getExpirationDate());
		if(securityObjectTO.getIssuanceDescription() != null){
			issuanceObjectTO.setIssuanceDescription( securityObjectTO.getIssuanceDescription() );
		} else {
			issuanceObjectTO.setIssuanceDescription( securityObjectTO.getAlternativeCode());
		}
		if(securityObjectTO.isDpfInterface()){
			issuanceObjectTO.setCurrency( securityObjectTO.getCurrency() );	
		} else {
			issuanceObjectTO.setCurrency( issuanceObjectTO.getCurrency() );	
		}
			
		issuanceObjectTO.setResolutionNumber( securityObjectTO.getIssuanceResolutionNumber() );
		issuanceObjectTO.setResolutionDate( securityObjectTO.getIssuanceResolutionDate() );
		BigDecimal bgNominalValue = CommonsUtilities.truncateDecimal(securityObjectTO.getNominalValue(), GeneralConstants.TWO_VALUE_INTEGER);
		issuanceObjectTO.setIssuanceAmount(CommonsUtilities.roundAmount(bgNominalValue.multiply(securityObjectTO.getStockQuantity(),MathContext.UNLIMITED) ));
		
		return validateIssuanceCreation(issuanceObjectTO, issuance);
	}
	
	/**
	 * Validate security creation.
	 *
	 * @param securityObjectTO the security object to
	 * @param security the security
	 * @param loggerUser the logger user
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	public RecordValidationType validateSecurityCreation(SecurityObjectTO securityObjectTO, Security security, LoggerUser loggerUser) throws ServiceException{
		
		security.setCashNominal(InterestClassType.EFFECTIVE.getCode());
		security.setInstrumentType(security.getIssuance().getInstrumentType());
		security.setIssuanceForm(security.getIssuance().getIssuanceType());
		security.setSecurityType(security.getIssuance().getSecurityType());
		security.setCurrency(security.getIssuance().getCurrency());
		security.setSecurityClass(security.getIssuance().getSecurityClass());
		security.setEconomicActivity(security.getIssuance().getIssuer().getEconomicActivity());
		security.setIssuanceCountry(security.getIssuance().getGeographicLocation());
		if(security.getIssuance().getGeographicLocation().getIdGeographicLocationPk().equals( countryResidence )){
			security.setSecuritySource( SecuritySourceType.NATIONAL.getCode() );
		} else {
			security.setSecuritySource( SecuritySourceType.FOREIGN.getCode() );
		}
		security.setIssuer(security.getIssuance().getIssuer());
		
		Security securityAux = new Security();
		StringBuilder code = new StringBuilder();
		code.append(securityObjectTO.getSecurityClassDesc());
		code.append("-");
		code.append(securityObjectTO.getSecuritySerial());
		securityAux.setIdSecurityCodePk(code.toString());
		try {
			int quantityRegistred = validateSecurityCodeServiceBean(securityAux);
			if(quantityRegistred > 0){
//				return CommonsUtilities.populateRecordCodeValidation(null, 
//						GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CODE_REPEATED, null);
				Object parameter[] = {securityObjectTO.getSecuritySerial()};
				return CommonsUtilities.populateRecordCodeValidation(null, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CODE_REPEATED_MASSIVE, null, parameter);
			}
			SecurityCodeFormat objSecurityCodeFormat = new SecurityCodeFormat();
			objSecurityCodeFormat = findSecurityCodeFormat(security);
			
			if(Validations.validateIsNull(objSecurityCodeFormat)){
				Object parameter[] = {securityObjectTO.getSecuritySerial()};
				return CommonsUtilities.populateRecordCodeValidation(null, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CODE_INCORRECT_INSTRUMENT_CLASS_MASSIVE, null, parameter);
			} else {
				Object value = securityObjectTO.getSecuritySerial();
				try{
					securitiesUtils.processValidateSecurityCodeFormat(security, objSecurityCodeFormat, value);
				} catch (ServiceException e) {
					ValidationOperationType validationOperationType = new ValidationOperationType();
					validationOperationType.setLocale(new Locale("es"));					
					RecordValidationType recordError = CommonsUtilities.populateRecordCodeValidation(validationOperationType, GenericPropertiesConstants.MSG_INTERFACE_UNDEFINED_ERROR, null);
					recordError.setErrorDescription(PropertiesUtilities.getMessage(
							GeneralConstants.PROPERTY_FILE_EXCEPTIONS, validationOperationType.getLocale(), e.getErrorService().getMessage()));
					return recordError;
				}
				
			}
			
		} catch (ServiceException e) {
			RecordValidationType recordError = CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_UNDEFINED_ERROR, null);
			recordError.setErrorDescription(PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_EXCEPTIONS,"es", e.getMessage()));
			return recordError;
		}
		
		try {
			security.defaultValues();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(securityObjectTO.getExpirationDate()!=null){
			if(securityObjectTO.getExpirationDate().compareTo(security.getIssuance().getExpirationDate()) > 0){
				Object parameter[] = {securityObjectTO.getSecuritySerial()};
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_EXPIRATION_DATE, null, parameter);
			}
			security.setExpirationDate(securityObjectTO.getExpirationDate());
		}else{
			security.setExpirationDate(security.getIssuance().getExpirationDate());
		}
		
		// si es BTX O BBX Se valida que el la fecha de emision y vencimiento del valor
		// estE dentro del rango de la fecha de emision y vencimiento de la emision
		if(Validations.validateIsNotNull(securityObjectTO.getSecurityClass())
				&& Validations.validateIsNotNull(securityObjectTO.getIssuanceDate())){
			if(securityObjectTO.getSecurityClass().equals(SecurityClassType.BBX.getCode())
					|| securityObjectTO.getSecurityClass().equals(SecurityClassType.BTX.getCode())){
				if(securityObjectTO.getIssuanceDate().compareTo(security.getIssuance().getIssuanceDate()) < 0){
					Object parameter[] = {securityObjectTO.getSecuritySerial()};
					return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_ISSUANCE_DATE_SECURITY, null, parameter);
				}
			}
		}
		
		security.setAlternativeCode(securityObjectTO.getAlternativeCode());
		security.setIdSecurityCodeOnly(securityObjectTO.getSecuritySerial());
		security.setDescription(securityObjectTO.getSecuritySerial());
		security.setIndPrepaid(BooleanType.NO.getCode() );
		
		if(securityObjectTO.getIndFCI()!=null){
			security.getSecurityInvestor().setIndCfi( securityObjectTO.getIndFCI() );
		}
		
		//null for DPF,DPA,BBX,BTX
		security.setMnemonic(null);
		security.setSecuritySerial(null);
		
		security.setInitialNominalValue(CommonsUtilities.truncateDecimal(securityObjectTO.getNominalValue(), GeneralConstants.TWO_VALUE_INTEGER));
		security.setCurrentNominalValue(CommonsUtilities.truncateDecimal(securityObjectTO.getNominalValue(), GeneralConstants.TWO_VALUE_INTEGER) );		
		security.setMinimumInvesment( securityObjectTO.getMinimumInvesment() );
		security.setMaximumInvesment( securityObjectTO.getMaximumInvesment() );	
		if(Validations.validateIsNotNull(securityObjectTO.getIssuanceDate())
				&& Validations.validateIsNotNull(security.getIssuance().getIssuanceDate()) 
				&& Validations.validateIsNotNull(security.getIssuance().getExpirationDate())
				// validando que la fecha de emision del XML sea mayor o igual a la fecha de emision de la emision y menor o igual a la fehca de vencimiento de de la emision
				&& CommonsUtilities.truncateDateTime(securityObjectTO.getIssuanceDate()).compareTo(CommonsUtilities.truncateDateTime(security.getIssuance().getIssuanceDate())) >= 0
				&& CommonsUtilities.truncateDateTime(securityObjectTO.getIssuanceDate()).compareTo(CommonsUtilities.truncateDateTime(security.getIssuance().getExpirationDate())) <= 0
				){
			security.setIssuanceDate( securityObjectTO.getIssuanceDate() );
		}else{
			security.setIssuanceDate( security.getIssuance().getIssuanceDate() );
		}
		security.setSecurityMonthsTerm(CommonsUtilities.getMonthsBetween(security.getIssuanceDate(), security.getExpirationDate()));
		security.setSecurityDaysTerm(CommonsUtilities.getDaysBetween(security.getIssuanceDate(), security.getExpirationDate()));
		security.setSecurityTerm( SecurityTermType.getByDays(security.getSecurityDaysTerm()).getCode() );
		if(securityObjectTO.getIndExtendedTerm()!=null){
			security.setIndExtendedTerm( securityObjectTO.getIndExtendedTerm() );
		}
		
		security.setYield( securityObjectTO.getYield() );
		security.setInterestPaymentModality( securityObjectTO.getInterestPaymentModality() );
		security.setInterestRate( CommonsUtilities.truncateDecimal(securityObjectTO.getInterestRate(), GeneralConstants.FOUR_VALUE_INTEGER) );
		security.setPassiveInterestRate( CommonsUtilities.truncateDecimal(securityObjectTO.getPassiveInterestRate(), GeneralConstants.FOUR_VALUE_INTEGER) );
		
		security.setPeriodicity( securityObjectTO.getPeriodicity() );
		security.setPeriodicityDays( securityObjectTO.getPeriodicityDays() );
		
		security.setCalendarMonth( CalendarMonthType.ACTUAL.getCode() );
		security.setCalendarType( CalendarType.COMMERCIAL.getCode() );
		security.setCalendarDays( CalendarDayType._360.getCode() );
		//security.setCouponFirstExpirationDate( securityObjectTO.getLstCouponObjectTOs().get(0).getExpirationDate() );
		
		//DPA - issue 158
		if(securityObjectTO.getInstrumentTypeMnemonic() != null){
			if (securityObjectTO.getInstrumentTypeMnemonic().equals(SecurityClassType.DPA.getText1())){
				if(!securityObjectTO.getNumberCoupons().equals(GeneralConstants.ZERO_VALUE_INT) ){
					return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_NUMBER, null);
				}
			}
		}
		
		security.setNumberCoupons( securityObjectTO.getNumberCoupons() );
		
		security.setYieldRate( securityObjectTO.getYieldRate() );
		security.setDiscountRate( securityObjectTO.getDiscountRate() );
		
		if(security.isBbxSecurityClass() || security.isBtxSecurityClass()){
			security.setIndSplitCoupon( securityObjectTO.getIndSplitCoupon() );
		}
		
		if(securityObjectTO.getIndEarlyRedemption() != null){
			security.setIndEarlyRedemption( securityObjectTO.getIndEarlyRedemption());
		} else {
			if(securityObjectTO.isDpfInterface()){
				security.setIndEarlyRedemption( BooleanType.YES.getCode() );
			} else {
				security.setIndEarlyRedemption( BooleanType.NO.getCode() );
			}			
		}
		
		if(securityObjectTO.getIndSubordinated() != null){
			security.setIndSubordinated( securityObjectTO.getIndSubordinated());
		} else {
			security.setIndSubordinated( BooleanType.NO.getCode() );
		}
		
		if(securityObjectTO.getIndTaxExempt() !=null ){
			security.setIndTaxExempt( securityObjectTO.getIndTaxExempt());
		}else{
			security.setIndTaxExempt(BooleanType.NO.getCode());
		}
		
		if(security.isDpfSecurityClass() || security.isDpaSecurityClass()){
			security.setBolCfiCode(false);
			
			security.setShareCapital(BigDecimal.ZERO);
			security.setCirculationAmount(BigDecimal.ZERO);
			
			security.setShareBalance(BigDecimal.ZERO);
			security.setCirculationBalance(BigDecimal.ZERO);
			
			security.setPhysicalBalance(BigDecimal.ZERO);
			security.setDesmaterializedBalance(BigDecimal.ZERO);
		}else{
			security.setBolCfiCode(true);
		}
		
		security.setCorporativeProcessDays(Integer.valueOf(0));
		security.setInterestType( securityObjectTO.getInterestType() );
		// start interest payment schedule data
		
		security.setInterestPaymentSchedule(new InterestPaymentSchedule());
		security.getInterestPaymentSchedule().setDataFromSecurity(security);
		security.getInterestPaymentSchedule().setProgramInterestCoupons(new ArrayList<ProgramInterestCoupon>());
		if(Validations.validateIsNotNull(security.getSecurityClass()) && security.getSecurityClass().equals(SecurityClassType.PGE.getCode()) ){
			security.getInterestPaymentSchedule().setPeriodicity(InterestPeriodicityType.BY_DAYS.getCode());
		}
		if(!security.isCeroInterestType()){
			
			boolean checkPaymentVsExpiration = false;	
			
			// si la modalidad de pago de interes es AL VENCIMIENTO entonces la cantidad de cupones debe ser 1
			if(security.getInterestPaymentModality().equals(InterestPaymentModalityType.AT_MATURITY.getCode())){
				// if it is at maturity, and list of coupons is not empty, there must exists only one coupon
				if(Validations.validateListIsNotNullAndNotEmpty(securityObjectTO.getLstCouponObjectTOs()) && 
						securityObjectTO.getLstCouponObjectTOs().size() != ComponentConstant.ONE){
//					return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_ELEMENT, null);
					Object parameter[] = {securityObjectTO.getSecuritySerial()};
					return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECTION_COUPON_MASSIVE, null, parameter);
				}
				
				// issue 1239: como es al VENCIMIENTO debemos validar que los tag NRO_CUPONES, FECHA_VCTO_PRIMER_CUPON, PERIODICIDAD, TIPO_PAGO, PERIODICIDAD_PAGO
				// por el momento esto solo aplica a BBX y BTX
				if(Validations.validateIsNotNull(securityObjectTO.getSecurityClass())
						&& ( securityObjectTO.getSecurityClass().equals(SecurityClassType.BBX.getCode()) 
								||securityObjectTO.getSecurityClass().equals(SecurityClassType.BTX.getCode()) ) ){
					
					Object parameter[] = {securityObjectTO.getSecuritySerial()};
					if(Validations.validateIsNotNull(securityObjectTO.getNumberCoupons())){
						return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_MUST_HAVE_NULL_TAG_NROCUPONES, null, parameter);
					}
					if(Validations.validateIsNotNull(securityObjectTO.getCouponFirstExpirationDate())){
						return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_MUST_HAVE_NULLFECHAVCTOPRIMERUPON, null, parameter);
					}
					if(Validations.validateIsNotNull(securityObjectTO.getPeriodicity())){
						return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_MUST_HAVE_NULL_PERIODICIDAD, null, parameter);
					}
					if(Validations.validateIsNotNull(securityObjectTO.getAmortizationType())){
						return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_MUST_HAVE_NULL_TIPOPAGO, null, parameter);
					}
					if(Validations.validateIsNotNull(securityObjectTO.getAmortizationPeriodicity())){
						return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_MUST_HAVE_NULL_PERIODICIDADPAGO, null, parameter);
					}
				}
			}else{
				//if it is by coupon, a not empty list of coupons must exist
				if(Validations.validateListIsNullOrEmpty(securityObjectTO.getLstCouponObjectTOs())){
					//return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_ELEMENT, null);
					Object parameter[] = {securityObjectTO.getSecuritySerial()};
					return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECTION_COUPON_MASSIVE, null, parameter);
				}
			}
			
			// validando la cantidad de cupones con el numero de cupones definido en el valor
			if(Validations.validateIsNotNull(securityObjectTO.getNumberCoupons())){
				// if number of coupons property exists, it must match the size of coupon objects
				if(Validations.validateListIsNotNullAndNotEmpty(securityObjectTO.getLstCouponObjectTOs()) && 
						!securityObjectTO.getNumberCoupons().equals(securityObjectTO.getLstCouponObjectTOs().size())){
					Object parameter[] = {securityObjectTO.getSecuritySerial()};
					//return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_NUMBER, null);
					return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_NUMBER_MASSIVE, null, parameter);
				}
				// check if Numbers of coupons property is one 
				if(securityObjectTO.getNumberCoupons().equals(ComponentConstant.ONE)){
					checkPaymentVsExpiration = true; 
				}
			}else{
				// check if size of list of coupons is one
				if(Validations.validateListIsNotNullAndNotEmpty(securityObjectTO.getLstCouponObjectTOs()) && 
						securityObjectTO.getLstCouponObjectTOs().size() == ComponentConstant.ONE){
					checkPaymentVsExpiration = true; 
				}
			}
			
			
			Date previousExpirationDate=null;
			if (Validations.validateListIsNotNullAndNotEmpty(securityObjectTO.getLstCouponObjectTOs())) {
				
				if(securityObjectTO.getPeriodicity() == null){
					if(securityObjectTO.isDpfInterface()){
						securityObjectTO.setPeriodicity(GeneralConstants.ZERO_VALUE_INTEGER);
					} else {
						Object parameter[] = {securityObjectTO.getSecuritySerial()};
						return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_INTEREST_PAYMENT_PERIODICITY_MASSIVE, null, parameter);
					}
					//return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_INTEREST_PAYMENT_PERIODICITY, null);					
				}
				security.setNumberCoupons(securityObjectTO.getLstCouponObjectTOs().size());
				
				// Validar que la fecha del primer cupon sea mayor a la fecha de emision
				// y que la fecha del utlimo cupon coincida con la fecha de vencimiento del valor
				
				CouponObjectTO objCouponObjectTOFirst = securityObjectTO.getLstCouponObjectTOs().get(0);
				CouponObjectTO objCouponObjectTOLast = securityObjectTO.getLstCouponObjectTOs().get(security.getNumberCoupons()-1);
				if(objCouponObjectTOFirst != null && objCouponObjectTOLast != null){
					Object parameter[] = {securityObjectTO.getSecuritySerial()};
					if(objCouponObjectTOFirst.getExpirationDate().compareTo(security.getIssuanceDate()) < 0){
//						return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_EXPIRATION_DATE_ISSUANCE_DATE, null);
						return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_EXPIRATION_DATE_ISSUANCE_DATE_MASSIVE, null, parameter);
					}
					if(objCouponObjectTOLast.getExpirationDate().compareTo(security.getExpirationDate()) != 0){
//						return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_EXPIRATION_DATE_EXPIRATION_DATE, null);
						return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_EXPIRATION_DATE_EXPIRATION_DATE_MASSIVE, null, parameter);
					}
				}								
				
				Integer couponNumber=0;		
				Date dtExpirationCoupon = new Date();
				for(CouponObjectTO couponTO : securityObjectTO.getLstCouponObjectTOs()){
					couponNumber += 1;
					
					//check if payment day is equal to expiration security, only when there is an unique coupon
					if(checkPaymentVsExpiration){
						if(couponTO.getExpirationDate().compareTo(security.getExpirationDate()) != 0){
//							return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_EXPIRATION_DATE, null);
							Object parameter[] = {securityObjectTO.getSecuritySerial()};
							return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_EXPIRATION_DATE_MASSIVE, null, parameter);
						}
					}
					
					// VALIDAR QUE LA FECHA DE EXPIRACION DEL CUPON CORRESPONDA A LA VIGENCIA DEL VALOR					
					Object parameter[] = {securityObjectTO.getSecuritySerial()};
					if(couponNumber.equals(GeneralConstants.ONE_VALUE_INTEGER)){
						if(couponTO.getExpirationDate().compareTo(security.getIssuanceDate()) < 0 
								|| couponTO.getExpirationDate().compareTo(security.getExpirationDate()) > 0){
							return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_EXPIRATION_DATE_MASSIVE, null, parameter);
						}
						dtExpirationCoupon = couponTO.getExpirationDate();
					} else {						
						if(couponTO.getExpirationDate().compareTo(dtExpirationCoupon) <= 0 
								|| couponTO.getExpirationDate().compareTo(security.getExpirationDate()) > 0){
							return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_EXPIRATION_DATE_MASSIVE, null,parameter);
						}
						dtExpirationCoupon = couponTO.getExpirationDate();
					}
					
					//we create the interest coupon
					ProgramInterestCoupon programInterestCoupon = createProgramInterestCoupon(security, couponNumber, previousExpirationDate, couponTO.getExpirationDate(), loggerUser);
					previousExpirationDate = couponTO.getExpirationDate();
					security.getInterestPaymentSchedule().getProgramInterestCoupons().add(programInterestCoupon);
				}
			} else {
				//by default it will have just one interest coupon 
				Integer couponNumber = 1;
				
				// issue 1239: si es al vencimiento nro de cupones debeser 0 (requerimiento para BBX Y BTX)
				if(Validations.validateIsNotNull(securityObjectTO.getSecurityClass())
						&& (securityObjectTO.getSecurityClass().equals(SecurityClassType.BBX.getCode())
							||securityObjectTO.getSecurityClass().equals(SecurityClassType.BTX.getCode())) 
						&& security.getInterestPaymentModality().equals(InterestPaymentModalityType.AT_MATURITY.getCode()) ){
					security.setNumberCoupons(0);
				}else{
					security.setNumberCoupons(couponNumber);
				}
				security.setPeriodicity(InterestPeriodicityType.INDISTINCT.getCode());
				// como solo serA 1 cupon entonces la fecha de vencimineto del priemrr cupon es la misma fecha de vencimiento del valor
				security.setCouponFirstExpirationDate(security.getExpirationDate());
				ProgramInterestCoupon programInterestCoupon= createProgramInterestCoupon(security, couponNumber, null, security.getExpirationDate(), loggerUser);
				security.getInterestPaymentSchedule().getProgramInterestCoupons().add(programInterestCoupon);
			}
			
		}else{
			if(!security.getInterestPaymentModality().equals(InterestPaymentModalityType.AT_MATURITY.getCode())){
//				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_INTEREST_PAYMENT_MODALITY, null);
				Object parameter[] = {securityObjectTO.getSecuritySerial()};
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_INTEREST_PAYMENT_MODALITY_MASSIVE, null, parameter);
			}
		}
		
		// start amortization payment data
		
		// capital payment modality cannot be null

		security.setCapitalPaymentModality( securityObjectTO.getCapitalPaymentModality() );
		security.setAmortizationPaymentSchedule(new AmortizationPaymentSchedule());
		
		if(securityObjectTO.getCapitalPaymentModality().equals(CapitalPaymentModalityType.PARTIAL.getCode())){
			Object parameter[] = {securityObjectTO.getSecuritySerial()};
			if(Validations.validateIsNull(securityObjectTO.getAmortizationType())){
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_AMORTIZATION_TYPE_MASSIVE, null, parameter);
			}
			if(Validations.validateIsNull(securityObjectTO.getAmortizationPeriodicity())){
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_AMORTIZATION_PERIODICITY_MASSIVE, null, parameter);
			}
			if(securityObjectTO.getAmortizationType().equals(AmortizationType.PROPORTIONAL.getCode())){
				if(Validations.validateIsNull(securityObjectTO.getAmortizationFactor())){
					return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_AMORTIZATION_PAYMENT_FACTOR_MASSIVE, null, parameter);
				}
				security.setAmortizationFactor(securityObjectTO.getAmortizationFactor());
			}
			
			if(Validations.validateListIsNullOrEmpty(securityObjectTO.getLstAmortizationObjectTOs())){
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECTION_SECCPAGOS_MASSIVE, null, parameter);
			}
			
			security.setAmortizationType(securityObjectTO.getAmortizationType());
			security.setAmortizationPeriodicity( securityObjectTO.getAmortizationPeriodicity());
			security.setAmortizationPeriodicityDays( securityObjectTO.getAmortizationPeriodicityDays());
			
			security.getAmortizationPaymentSchedule().setNumberPayments(securityObjectTO.getLstAmortizationObjectTOs().size());
			security.getAmortizationPaymentSchedule().setProgramAmortizationCoupons(new ArrayList<ProgramAmortizationCoupon>());

			Date previousExpirationDate=null;
			Integer paymentNumber=0;
			Date paymentFirstExpirationDate=null;
			BigDecimal firsAmortizationFactor=null;
			
			for(AmortizationObjectTO  paymentTO: securityObjectTO.getLstAmortizationObjectTOs()){
				paymentNumber=paymentNumber+1;
				
//				Date expirationDate = generalParameterFacade.workingDateCalculateServiceFacade(
//						paymentTO.getPaymentDate(), -1);
				
				Date expirationDate = paymentTO.getPaymentDate();
				//calculando la fecha de pago
				Date paymentDate = generalParameterFacade.workingDateCalculateServiceFacade(CommonsUtilities.truncateDateTime(paymentTO.getPaymentDate()), 
						GeneralConstants.ONE_VALUE_INTEGER);
				
				ProgramAmortizationCoupon programAmortizationCoupon = createProgramAmortizationCoupon(security, previousExpirationDate, 
						paymentDate, expirationDate, paymentTO.getPaymentFactor(), null, loggerUser, paymentNumber);
				
				// si es la 1ra amortizacion capturamos su fecha de vencimiento para setear en AmortizationPaymentSchedule
				if(paymentNumber.intValue() == 1){
					paymentFirstExpirationDate = expirationDate;
					firsAmortizationFactor = programAmortizationCoupon.getAmortizationFactor();
					security.setAmortizationPeriodicityDays(CommonsUtilities.getDaysBetween(programAmortizationCoupon.getBeginingDate(), programAmortizationCoupon.getExpirationDate()));
				}
				
				if(programAmortizationCoupon.getCouponNumber().compareTo(security.getAmortizationPaymentSchedule().getNumberPayments() )==0){
					programAmortizationCoupon.setLastPayment(true);
				}
				security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().add( programAmortizationCoupon );
				//previousExpirationDate=CommonsUtilities.addDaysToDate(programAmortizationCoupon.getExpirationDate(), 1);
				previousExpirationDate=CommonsUtilities.addDaysToDate(programAmortizationCoupon.getExpirationDate(),0);
				
			}
			security.getAmortizationPaymentSchedule().calculateTotalFactorAndAmount();
			
			// issue 1304: Adecuaciones para que el registro masivo tambien tome en cuenta las amortizaciones parciales
			if(Validations.validateIsNull(security.getAmortizationFactor())){
				if(Validations.validateIsNotNull(securityObjectTO.getAmortizationFactor())){
					security.setAmortizationFactor(securityObjectTO.getAmortizationFactor());
				}else{
					security.setAmortizationFactor(firsAmortizationFactor);
				}
			}
			security.getAmortizationPaymentSchedule().setDataSecurity(security);
			security.getAmortizationPaymentSchedule().setPaymentFirstExpirationDate(paymentFirstExpirationDate);
			
		}else if(securityObjectTO.getCapitalPaymentModality().equals(CapitalPaymentModalityType.AT_MATURITY.getCode())){
			
			security.setAmortizationType( AmortizationType.PROPORTIONAL.getCode());
			security.setAmortizationFactor(BigDecimal.valueOf(100));
			
			//it has only one amortization coupon
			security.getAmortizationPaymentSchedule().setDataSecurity(security);
			security.getAmortizationPaymentSchedule().setRegistryDays(security.getInterestPaymentSchedule().getRegistryDays());
			security.getAmortizationPaymentSchedule().setCorporativeDays(security.getCorporativeProcessDays());
			security.getAmortizationPaymentSchedule().setPeriodicity(security.getPeriodicity());
			security.getAmortizationPaymentSchedule().setPeriodicityDays(security.getInterestPaymentSchedule().getPeriodicityDays());
			security.getAmortizationPaymentSchedule().setPaymentModality(CapitalPaymentModalityType.AT_MATURITY.getCode());
			
			Date paymentDate = generalParameterFacade.workingDateCalculateServiceFacade(security.getExpirationDate(), 1);
			
			ProgramAmortizationCoupon programAmortizationCoupon = createProgramAmortizationCoupon(security, 
					security.getIssuanceDate(), paymentDate, security.getExpirationDate(), security.getAmortizationFactor() ,
					security.getInitialNominalValue(), loggerUser, 1);
			
			programAmortizationCoupon.setLastPayment(true);
			
			security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().add(programAmortizationCoupon);
		}
		
		//issue 1304: recalculando los montos de interes correctamente, con el VN descontado en cada amortizacion
		if(		Validations.validateIsNotNullAndPositive(security.getInitialNominalValue())
				&& Validations.validateIsNotNull(security.getInterestPaymentSchedule())
				&& Validations.validateListIsNotNullAndNotEmpty(security.getInterestPaymentSchedule().getProgramInterestCoupons())
				&& Validations.validateIsNotNull(security.getAmortizationPaymentSchedule())
				&& Validations.validateListIsNotNullAndNotEmpty(security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons()) ){
			securitiesUtils.calculateCouponAmountProgramInterest(security);
		}
		
		// issue 1361: redondeando los decimales de las tasas a 8 decimales antes de persistir el la BBDD
		if( Validations.validateIsNotNull(security.getInterestPaymentSchedule())
			&& Validations.validateListIsNotNullAndNotEmpty(security.getInterestPaymentSchedule().getProgramInterestCoupons()) ){
			BigDecimal interesRateReal;
			for(ProgramInterestCoupon pic:security.getInterestPaymentSchedule().getProgramInterestCoupons()){
				interesRateReal = pic.getInterestRate();
				pic.setInterestRate( CommonsUtilities.roundRateFactor(interesRateReal));
			}
		}
		
		security.setValuatorProcessClass(securitiesClassValuatorService.getValuatorProcessClass(security, null));
		if(security.getValuatorProcessClass().getIdValuatorClassPk() == null){
			security.setValuatorProcessClass(null);
		}
		if(securityObjectTO.getSecuritySerialPrevious() != null){
			security.setIdSourceSecurityCodePk(securityObjectTO.getSecuritySerialPrevious());
		}	
		if(securityObjectTO.getRestrictions() != null){
			security.setObservations(securityObjectTO.getRestrictions());
		}		
		
		if(Validations.validateIsNotNullAndNotEmpty(securityObjectTO.getNotTraded())){
			if(GeneralConstants.VALUE_TRADED_STRING.equalsIgnoreCase(securityObjectTO.getNotTraded().toUpperCase())){
				security.setNotTraded(GeneralConstants.ONE_VALUE_INTEGER);
			} else {
				security.setNotTraded(GeneralConstants.ZERO_VALUE_INTEGER);
			}
		} else {
			security.setNotTraded(GeneralConstants.ZERO_VALUE_INTEGER);
		}
		
		// ISSUE 633
		// verificando si el valor a crear es resultaddo de un fraccionamiento
		if(securityObjectTO != null && securityObjectTO.getFractionableSecurity() != null && securityObjectTO.getFractionableSecurity().length() > 0){
			security.setIdFractionSecurityCodePk("DPF-" + securityObjectTO.getFractionableSecurity());			
		}
		
		if(loggerUser != null){
			log.info("SECURITY validateSecurityCreation ");
			log.info("WSERROR Privilege: " + loggerUser.getIdPrivilegeOfSystem());
			log.info("WSERROR user : " + loggerUser.getUserName());
			log.info("WSERROR address : " + loggerUser.getIpAddress());
		}		
		security.setAudit(loggerUser);
		return null;
	}
	
	/**
	 * Creates the program amortization coupon.
	 *
	 * @param security the security
	 * @param previousExpirationDate the previous expiration date
	 * @param paymentDate the payment date
	 * @param expirationDate the expiration date
	 * @param paymentFactor the payment factor
	 * @param couponAmount the coupon amount
	 * @param loggerUser the logger user
	 * @param paymentNumber the payment number
	 * @return the program amortization coupon
	 * @throws ServiceException the service exception
	 */
	public ProgramAmortizationCoupon createProgramAmortizationCoupon(Security security, Date previousExpirationDate,   
			Date paymentDate, Date expirationDate, BigDecimal paymentFactor, BigDecimal couponAmount, LoggerUser loggerUser, Integer paymentNumber ) throws ServiceException{
		ProgramAmortizationCoupon programAmortizationCoupon=new ProgramAmortizationCoupon();
		if(previousExpirationDate==null){
			programAmortizationCoupon.setBeginingDate(security.getIssuanceDate());	
		}else{
			programAmortizationCoupon.setBeginingDate(previousExpirationDate);
		}
		programAmortizationCoupon.setPaymentDate(paymentDate);
		programAmortizationCoupon.setExpirationDate(expirationDate);
		programAmortizationCoupon.setAmortizationFactor(paymentFactor);
		if(couponAmount == null){
			programAmortizationCoupon.setCouponAmount(security.getInitialNominalValue().
					multiply(programAmortizationCoupon.getAmortizationFactor()).divide(BigDecimal.valueOf(100), MathContext.DECIMAL128));
			programAmortizationCoupon.setAmortizationAmount(couponAmount);
		}else{
			programAmortizationCoupon.setCouponAmount(couponAmount);
			programAmortizationCoupon.setAmortizationAmount(couponAmount);
		}
		programAmortizationCoupon.setCurrency(security.getCurrency());
		Date registryDate= CommonsUtilities.addDaysToDate(programAmortizationCoupon.getExpirationDate(), 
															security.getStockRegistryDays()*-1);
		programAmortizationCoupon.setRegistryDate(generalParameterFacade.workingDateCalculateServiceFacade(registryDate, -1));
		programAmortizationCoupon.setCutoffDate(registryDate); 
		programAmortizationCoupon.setIndRounding(BooleanType.NO.getCode());
		programAmortizationCoupon.setStateProgramAmortization(ProgramScheduleStateType.PENDING.getCode());
		programAmortizationCoupon.setSituationProgramAmortization(ProgramScheduleSituationType.PENDING.getCode());
		
		// basado como esta en el registro manual de valor
		if(security.isHavePaymentScheduleNoDesm() && security.isPartialAmortPaymentModality() 
				//&& security.isProportionalAmortizationType()
				){
			if(CommonsUtilities.isLessOrEqualDate(programAmortizationCoupon.getExpirationDate(), CommonsUtilities.currentDate(), true) ){
				programAmortizationCoupon.setStateProgramAmortization(ProgramScheduleStateType.EXPIRED.getCode());
			}
		}
		
		programAmortizationCoupon.setAmortizationPaymentSchedule(security.getAmortizationPaymentSchedule());
		programAmortizationCoupon.setPayedAmount(BigDecimal.ZERO);
		
		programAmortizationCoupon.setRegistryUser(loggerUser.getUserName());
		programAmortizationCoupon.setRegisterDate(loggerUser.getAuditTime());
		programAmortizationCoupon.setCouponNumber(paymentNumber);
		programAmortizationCoupon.setCorporativeDate(CommonsUtilities.currentDate());
		programAmortizationCoupon.setAudit(loggerUser);
		return programAmortizationCoupon;
	}

	/**
	 * Creates the program interest coupon.
	 *
	 * @param security the security
	 * @param couponNumber the coupon number
	 * @param previousExpirationDate the previous expiration date
	 * @param expirationDate the expiration date
	 * @param loggerUser the logger user
	 * @return the program interest coupon
	 * @throws ServiceException the service exception
	 */
	public ProgramInterestCoupon createProgramInterestCoupon(Security security, Integer couponNumber, Date previousExpirationDate, 
			Date expirationDate, LoggerUser loggerUser) throws ServiceException {
		ProgramInterestCoupon programInterestCoupon=new ProgramInterestCoupon();
		programInterestCoupon.setCouponNumber( couponNumber );
		programInterestCoupon.setCurrency( security.getCurrency() );
		//Begin Date
		if(previousExpirationDate==null){
			programInterestCoupon.setBeginingDate(security.getIssuanceDate() );	
		}else{
			programInterestCoupon.setBeginingDate( previousExpirationDate );
		}
		//Expiration Date
		programInterestCoupon.setExperitationDate(expirationDate);
		//payment date
		programInterestCoupon.setPaymentDate(generalParameterFacade.workingDateCalculateServiceFacade(programInterestCoupon.getExperitationDate(), 1));
		
		Integer paymentDays=null;
		if(security.isExtendedTerm()){
			paymentDays=CommonsUtilities.getDaysBetween(programInterestCoupon.getBeginingDate(),programInterestCoupon.getPaymentDate());
		}else{
			paymentDays=CommonsUtilities.getDaysBetween(programInterestCoupon.getBeginingDate(),programInterestCoupon.getExperitationDate());
		}
		programInterestCoupon.setPaymentDays(paymentDays);
		programInterestCoupon.setCorporativeDate( CommonsUtilities.currentDate() );
		//Registry Date
		Date registerDate= CommonsUtilities.addDaysToDate(programInterestCoupon.getExperitationDate(), 
														security.getStockRegistryDays()*-1);
		programInterestCoupon.setRegistryDate(generalParameterFacade.workingDateCalculateServiceFacade(registerDate, -1));
					
		//Cut Off Date
		programInterestCoupon.setCutoffDate(registerDate);
		programInterestCoupon.setInterestPaymentCronogram( security.getInterestPaymentSchedule() );
		programInterestCoupon.setInterestFactor(security.getInterestRate());
		
		programInterestCoupon.setStateProgramInterest( ProgramScheduleStateType.PENDING.getCode() );
		programInterestCoupon.setSituationProgramInterest( ProgramScheduleSituationType.PENDING.getCode() );
		
		// esta basado como en el registro de valor manual
		if(security.isHavePaymentScheduleNoDesm()){
			if(CommonsUtilities.isLessOrEqualDate(programInterestCoupon.getExperitationDate(), CommonsUtilities.currentDate(), true)){
				programInterestCoupon.setStateProgramInterest( ProgramScheduleStateType.EXPIRED.getCode() );
			}
		}
		
		programInterestCoupon.setIndRounding(BooleanType.NO.getCode()); 
		programInterestCoupon.setPayedAmount(BigDecimal.ZERO);
		
		// capturando interes calculado sin redondeo
		BigDecimal interestRateCalculateTemp = securitiesUtils.interestRateCalculate(security, 
                security.getPeriodicity(), 
                security.getInterestRate(), 
                programInterestCoupon.getBeginingDate(), 
                programInterestCoupon.getPaymentDays(),
                programInterestCoupon.getCouponNumber(),
                null,
                null);
			
//		programInterestCoupon.setCouponAmount(security.getInitialNominalValue().multiply(
//					programInterestCoupon.getInterestRate()).divide(BigDecimal.valueOf(100), MathContext.DECIMAL128));
		
		programInterestCoupon.setCouponAmount(CommonsUtilities.roundAmountCoupon(security.getInitialNominalValue().multiply(
				interestRateCalculateTemp, MathContext.UNLIMITED).divide(BigDecimal.valueOf(100), MathContext.UNLIMITED)));
		
		// redondeando a 8 decimales
		// issue 1361: ahora se redondea mas adelante
		//interestRateCalculateTemp=CommonsUtilities.roundRateFactor(interestRateCalculateTemp);
		// seteando tasa de interes con redondeo de 8 decimales
		programInterestCoupon.setInterestRate(interestRateCalculateTemp);
		
		if(programInterestCoupon.getCouponNumber().compareTo(security.getNumberCoupons())==0){
			programInterestCoupon.setLastCoupon(true);
		}
		
		programInterestCoupon.setAudit(loggerUser);
		
		return programInterestCoupon;
	}
	
	/**
	 * Gets the lst negotiation modality dpf.
	 *
	 * @param instrumentType the instrument type
	 * @return the lst negotiation modality dpf
	 */
	public List<NegotiationModality> getLstNegotiationModalityDpf(Integer instrumentType) {
		List<NegotiationModality> lsNegotiationModality = null;
		try {				
			Map<String, Object> filters = new HashMap<String, Object>();
			filters.put("negotiationModalityState", NegotiationModalityStateType.ACTIVE.getCode());
			filters.put("mechanismModalityState", MechanismModalityStateType.ACTIVE.getCode());
			filters.put("instrumentType", instrumentType);
			lsNegotiationModality = getListNegotiationModalitiesServiceBean(filters);			
			List<NegotiationMechanism> lstNegotiationMechanism = getListMechanismServiceBean(NegotiationMechanismStateType.ACTIVE.getCode());			
			if(Validations.validateListIsNotNullAndNotEmpty(lsNegotiationModality)){
				for(NegotiationModality objNegotiationModality : lsNegotiationModality){						
					List<NegotiationMechanism> lstNegMec = new ArrayList<NegotiationMechanism>();
					if(Validations.validateListIsNotNullAndNotEmpty(objNegotiationModality.getMechanismModalities())){
						for(MechanismModality objMechanismModality : objNegotiationModality.getMechanismModalities()){
							if(Validations.validateListIsNotNullAndNotEmpty(lstNegotiationMechanism)){
								for(NegotiationMechanism negMechanismType : lstNegotiationMechanism){
									if(negMechanismType.getMechanismCode().compareTo(GeneralConstants.VALUE_STRING_MECHANISM_CODE) != 0 
											&& !NegotiationModalityType.SALE_FIXED_INCOME.getCode().equals(objNegotiationModality.getIdNegotiationModalityPk())
											&& objMechanismModality.getId().getIdNegotiationMechanismPk().equals(negMechanismType.getIdNegotiationMechanismPk())){
										lstNegMec.add(new NegotiationMechanism(negMechanismType.getIdNegotiationMechanismPk(),negMechanismType.getMechanismName(),false,true));
									}								
								}
							}
						}
					}						
					objNegotiationModality.setNegotiationMechanisms(lstNegMec);						
					for(NegotiationMechanism objNegotiationMechanism : objNegotiationModality.getNegotiationMechanisms()){
						objNegotiationMechanism.setSelected(true);
					}
				}
			}			
		} catch (ServiceException e) {
			return null;
		}
		return lsNegotiationModality;
	}
	
	/**
	 * Find securities trcr.
	 *
	 * @param listIdSecurityPk the list id security pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object> findSecuritiesTRCR(List<String> listIdSecurityPk) throws ServiceException{


		StringBuilder stringBuilderSqlBase = new StringBuilder();
		stringBuilderSqlBase.append(" SELECT  ").
		 		 append("	SE.ID_SECURITY_CODE_PK, ").
				 append("	(SELECT PA.TEXT1 FROM PARAMETER_TABLE PA ").
				 append("		WHERE PA.PARAMETER_TABLE_PK=SE.SECURITY_CLASS) AS codinst,").
				 append("	SE.id_security_code_only AS serie, ").
				 append("	ISS.MNEMONIC AS emisor, ").
				 append("	TO_CHAR(SE.REGISTRY_DATE,'YYYY-MM-DD') AS fecha_emision,").
				 append("	TO_CHAR(SE.EXPIRATION_DATE,'YYYY-MM-DD') AS fecha_venci, ").
				 append("	(SELECT PA.TEXT1 FROM PARAMETER_TABLE PA ").
				 append("		WHERE PA.PARAMETER_TABLE_PK=SE.CURRENCY) AS moneda,").
				 append("	SE.CIRCULATION_BALANCE AS valor_emision,").
				 append("	SE.CURRENT_NOMINAL_VALUE AS vn,").
				 append("	NVL(SE.YIELD_RATE,0) AS tasa_rend, ").
				 append("	'AAA' AS VALUACION, ").  //VALUACION
				 append("	SE.NUMBER_COUPONS AS nro_cupones,").
				 append("	SE.AMORTIZATION_PERIODICITY_DAYS AS plazo_amortiza,").
				 append("	CASE WHEN SE.INSTRUMENT_TYPE =124 AND SE.SECURITY_CLASS<>1962").
				 append("		AND IND_EARLY_REDEMPTION=1").
				 append("			THEN (DECODE ( SE.IND_PREPAID,1,'Q','P'))").
				 append("			ELSE 'N'").
				 append("		END AS prepago,").
				 append("	DECODE(IND_SUBORDINATED,1,'T',0,'0') AS subordinado,").
				 append("	DECODE (SE.IND_CONVERTIBLE_STOCK, 1,").
				 append("		DECODE(SE.CONVERTIBLE_STOCK_TYPE, ").
				 append("		2199, DECODE(SE.ID_SECURITY_CLASS_TRG, 406,'OO',1962,'OP'), "). //OBLIGATORIO
				 append("		2200, DECODE(SE.ID_SECURITY_CLASS_TRG, 406,'PO',1962,'PP')"). //OPCIONALMENTE
				 append("		)").
				 append("		,'NN') as tipo_bono,").
				 append("	DECODE(SE.INTEREST_TYPE, 148,'F',534,'V','A') as tipo_tasa, ").
				 append("	'A' as tasa_dif,").
				 append("	'A' as neg_fci,").
				 append("	CASE WHEN IND_IS_DETACHED=1 AND IND_IS_COUPON =0").
				 append("		 THEN 'T'").
				 append("		 ELSE 'F' ").
				 append("		 END as subproducto,").
				 append("	999.99 as tasa_pen").
				 append("	FROM SECURITY SE ").
				 append("		INNER JOIN ISSUER ISS ON ISS.ID_ISSUER_PK=SE.ID_ISSUER_FK ").
				 append("	WHERE SE.ID_SECURITY_CODE_PK IN(:listSecuritiesPk) ").
				 append("	ORDER BY SE.ID_SECURITY_CODE_PK ").
				 append("").
				 append("").
				 append("");

	
			
		
		Query query=null;
		List<Object> listObject = null;
		try {

			query = em.createNativeQuery(stringBuilderSqlBase.toString());
			query.setParameter("listSecuritiesPk", listIdSecurityPk);
			
			listObject=query.getResultList();

		} catch (Exception ex) {
			if(ex.getCause() instanceof SQLGrammarException){
				
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}
			
		}
		
		return listObject;
	}
	
	/**
	 * Find securities coupon detail trcr.
	 *
	 * @param idSecurityPk the id security pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object> findSecuritiesCouponDetailTRCR(String idSecurityPk) throws ServiceException{


		StringBuilder stringBuilderSqlBase = new StringBuilder();
		stringBuilderSqlBase.append(" WITH  PROGRAM_AMORTIZATION  AS  ").
		 		 append("	(  ").
				 append("	SELECT ").
				 append("		APS.ID_SECURITY_CODE_FK,   ").
				 append("	 	PAC.COUPON_AMOUNT,   ").
				 append("		PAC.EXPRITATION_DATE    ").
				 append("	FROM").
				 append("	PROGRAM_AMORTIZATION_COUPON PAC  ").
				 append("	INNER JOIN     ").
				 append("		AMORTIZATION_PAYMENT_SCHEDULE APS ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK=APS.ID_AMO_PAYMENT_SCHEDULE_PK   ").
				 append("	WHERE   ").
				 append("	STATE_PROGRAM_AMORTIZATON=1350  ").
				 append("	)").
				 append("	SELECT").
				 append("		PIC.COUPON_NUMBER AS nro_cupon,    ").
				 append("		TO_CHAR(PIC.EXPERITATION_DATE,'yyyy-MM-dd') AS fecha_venci, ").
				 append("		NVL(ROUND(PAC.COUPON_AMOUNT,2),0) AS amortiza_k,").
				 append("		0 as tasa_cupon_dif").
				 append("	FROM").
				 append("	SECURITY SE    ").
				 append("	INNER JOIN ").
				 append("		INTEREST_PAYMENT_SCHEDULE IPS on SE.ID_SECURITY_CODE_PK=IPS.ID_SECURITY_CODE_FK   ").
				 append("	INNER JOIN ").
				 append("		PROGRAM_INTEREST_COUPON PIC on PIC.ID_INT_PAYMENT_SCHEDULE_FK=IPS.ID_INT_PAYMENT_SCHEDULE_PK    ").
				 append("	LEFT JOIN   ").
				 append("		AMORTIZATION_PAYMENT_SCHEDULE APS on SE.ID_SECURITY_CODE_PK=APS.ID_SECURITY_CODE_FK   ").
				 append("	LEFT JOIN ").
				 append("		PROGRAM_AMORTIZATION PAC on trunc(PIC.EXPERITATION_DATE)=trunc(PAC.EXPRITATION_DATE)").
				 append("		AND PAC.ID_SECURITY_CODE_FK=IPS.ID_SECURITY_CODE_FK").
				 append("	WHERE PIC.STATE_PROGRAM_INTEREST=1350").
				 append("		AND SE.INTEREST_PAYMENT_MODALITY<>1527 ").
				 append("		AND SE.IND_IS_COUPON=0  AND SE.IND_IS_DETACHED=0 ").
				 append("		AND SE.ID_SECURITY_CODE_PK= :idSecuritiesPk").
				 append("	ORDER BY SE.ID_SECURITY_CODE_PK, SE.SECURITY_CLASS, PIC.COUPON_NUMBER").
				 
				 append("");

	
			
		
		Query query=null;
		List<Object> listObject = null;
		try {

			query = em.createNativeQuery(stringBuilderSqlBase.toString());
			query.setParameter("idSecuritiesPk", idSecurityPk);
			
			listObject=query.getResultList();

		} catch (Exception ex) {
			if(ex.getCause() instanceof SQLGrammarException){
				
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}
			
		}
		
		return listObject;
	}
	
	private BigDecimal getMaxInterfaceTransaction(Long idParticipant, String interfaceCode) {
		StringBuilder sd= new StringBuilder();
		sd.append(" SELECT max(IT.operationNumber) ");
		sd.append(" FROM InterfaceTransaction IT ");
		sd.append(" WHERE IT.participant.idParticipantPk = :idParticipant ");
		sd.append(" and IT.interfaceProcess.idExternalInterfaceFk.interfaceName = :interfaceCode ");
		sd.append(" and IT.transactionState = :transactionState ");
		
		Query query = em.createQuery(sd.toString());
		query.setParameter("idParticipant", idParticipant);
		query.setParameter("interfaceCode", interfaceCode);
		query.setParameter("transactionState", BooleanType.YES.getCode());
		
		List<Object> lst = query.getResultList();
		if(Validations.validateListIsNotNullAndNotEmpty(lst)){
			BigDecimal numero = new BigDecimal(lst.get(0).toString());
			return numero;
		}
		return BigDecimal.ZERO;
	}
	
	
	/**
	 * Gets the credit rating scales.
	 *
	 * @param issuanceObjectTO the issuance object to
	 * @param issuance the issuance
	 * @return the credit rating scales
	 */
	public Integer getCreditRatingScales(IssuanceObjectTO issuanceObjectTO, Issuance issuance) {
		Integer intCreditRating = null;
		Integer idMasterTable = null;		
		List<ParameterTable> lstCboCreditRatingScales = null;
		Integer issuanceDaysTerm = CommonsUtilities.getDaysBetween(issuance.getIssuanceDate(), issuance.getExpirationDate());
		if(Validations.validateIsNotNullAndPositive(issuance.getInstrumentType())){
			if(issuance.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode())){
				if(Validations.validateIsNotNullAndPositive(issuance.getSecurityClass())){
					if(!issuance.getSecurityClass().equals(SecurityClassType.ACC_RF.getCode())){
						if(Validations.validateIsNotNullAndPositive(issuanceDaysTerm)){
							if(issuanceDaysTerm.compareTo(ComponentConstant.ONE) >= 0 
									&& issuanceDaysTerm.compareTo(CalendarDayType._360.getIntegerValue()) <= 0 ){
								idMasterTable = MasterTableType.CREDIT_RATING_SCALES_SHORT_TERM.getCode();
							}else if (issuanceDaysTerm.compareTo(CalendarDayType._360.getIntegerValue()) > 0){
								idMasterTable = MasterTableType.CREDIT_RATING_SCALES_LONG_TERM.getCode();
							}
						}
					}else{
						idMasterTable = MasterTableType.CREDIT_RATING_SCALES_PREFERED_STOCK.getCode();
					}
				}
			}else if(issuance.getInstrumentType().equals(InstrumentType.MIXED_INCOME.getCode())){
				if(Validations.validateIsNotNullAndPositive(issuanceDaysTerm)){
					if(issuanceDaysTerm.compareTo(ComponentConstant.ONE) >= 0 && issuanceDaysTerm.compareTo(CalendarDayType._360.getIntegerValue()) <= 0 ){
						idMasterTable = MasterTableType.CREDIT_RATING_SCALES_SHORT_TERM.getCode();
					}else if (issuanceDaysTerm.compareTo(CalendarDayType._360.getIntegerValue()) > 0){
						idMasterTable = MasterTableType.CREDIT_RATING_SCALES_LONG_TERM.getCode();
					}
				}
			}else if(issuance.getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode())){
				if(Validations.validateIsNotNullAndPositive(issuance.getSecurityClass())){
					if(!issuance.getSecurityClass().equals(SecurityClassType.CFC.getCode())){
						idMasterTable = MasterTableType.CREDIT_RATING_SCALES_EQUITIES.getCode();
					}else{
						idMasterTable = MasterTableType.CREDIT_RATING_SCALES_LONG_TERM.getCode();					
					}
				}
				
			}			
			if(idMasterTable!=null){
				ParameterTableTO parameterTableTO = new ParameterTableTO();
				parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
				parameterTableTO.setMasterTableFk(idMasterTable);
				parameterTableTO.setOrderByText1(ComponentConstant.ONE);				
				if(issuanceObjectTO.isDpfInterface()){
					parameterTableTO.setText2(ComponentConstant.NOT_ACTIVE);
				}				
				try { 
					lstCboCreditRatingScales = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
				} catch (ServiceException e) {}
			}			
			if(Validations.validateListIsNotNullAndNotEmpty(lstCboCreditRatingScales)){
				intCreditRating = lstCboCreditRatingScales.get(0).getParameterTablePk();
			} 						
		}	
		return intCreditRating;
	}
	
	/**
	 * Checks if is placement segments.
	 *
	 * @param idSecurityPk the id security pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object> isPlacementSegments(String idSecurityPk) throws ServiceException{ 
		
		StringBuilder stringBuilderSql = new StringBuilder();
		stringBuilderSql.append(" SELECT  ");
		stringBuilderSql.append("	psd.security.idSecurityCodePk ");
		stringBuilderSql.append("	FROM PlacementSegmentDetail psd ");
		stringBuilderSql.append("		 WHERE psd.security.idSecurityCodePk = :idSecuritiesPk "  );
		stringBuilderSql.append("	 ");
		Query query=null;
		List<Object> listObject = new ArrayList<Object>();
		try {
			query = em.createQuery(stringBuilderSql.toString());
			query.setParameter("idSecuritiesPk", idSecurityPk);
			listObject=query.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
			if(ex.getCause() instanceof SQLGrammarException){
				
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}
		}
		return listObject;
	}

	/**
	 * Calculate desmaterialized.
	 *
	 * @param currentDate the current date
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> calculateDesmaterialized (Date currentDate){
		StringBuilder sb = new StringBuilder();
		sb.append("	SELECT																							");
		sb.append("	SUM(DECODE(mb.ID_BEHAVIOR,1,																	");
		sb.append("		FN_NOMINAL_VALUE_CORP_HIST(SE.id_security_code_pk,trunc(sm.MOVEMENT_DATE))*sm.MOVEMENT_QUANTITY ,				");
		sb.append("		FN_NOMINAL_VALUE_CORP_HIST(SE.id_security_code_pk,trunc(sm.MOVEMENT_DATE))*(-1)*sm.MOVEMENT_QUANTITY				");
		sb.append("	  )) NOMINAL_DIARIO,  																			");
		sb.append("	SUM(DECODE(mb.ID_BEHAVIOR,1,sm.MOVEMENT_QUANTITY,sm.MOVEMENT_QUANTITY*(-1))) CANTIDAD_DIARIA,	");
		sb.append("	SE.CURRENCY,																					");
		sb.append(" SE.SECURITY_CLASS																				");
		sb.append("	FROM securities_movement sm, movement_type mt, MOVEMENT_BEHAVIOR mb, 							");
		sb.append("	BALANCE_TYPE bt, SECURITY SE																	");
		sb.append("	where sm.ID_MOVEMENT_TYPE_FK=mt.ID_MOVEMENT_TYPE_PK												");
		sb.append("	and mb.ID_MOVEMENT_TYPE_FK=mt.ID_MOVEMENT_TYPE_PK												");
		sb.append("	and bt.ID_BALANCE_TYPE_PK=mb.ID_BALANCE_TYPE_FK													");
		sb.append("	AND SE.ID_SECURITY_CODE_PK=sm.ID_SECURITY_CODE_FK												");
		sb.append("	and bt.ID_BALANCE_TYPE_PK=2005																	");
		sb.append("	and mb.ID_BEHAVIOR=1 and mt.ID_MOVEMENT_TYPE_PK not in (200085,200081,200034,200052)			"); //Solo ingresos - issue 180
		sb.append("	and trunc(sm.MOVEMENT_DATE)= to_date(:date,'dd/mm/yyyy')										");
		sb.append(" GROUP BY SE.SECURITY_CLASS, SE.CURRENCY															");
		Query query=null;
		List<Object[]> lstDailySecurity = null;
		try {
			query = em.createNativeQuery(sb.toString());
			query.setParameter("date",CommonsUtilities.convertDatetoStringForTemplate(currentDate, CommonsUtilities.DATE_PATTERN));
			lstDailySecurity=query.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return lstDailySecurity;
	}
	
	/**
	 * Search amounts previus.
	 * Verifica si se ejecuto el proceso en la Fecha T-1
	 * @param currentDate the current date
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<TotalSecurityClassBalance> searchAmountsPrevius (Date currentDate){
		StringBuilder sb = new StringBuilder();
		sb.append("	SELECT	tscb																					");
		sb.append("	FROM TotalSecurityClassBalance tscb									 							");
		sb.append("	where trunc(tscb.processDate)= to_date(:date,'dd/mm/yyyy')										");
		Query query=null;
		List<TotalSecurityClassBalance> lstDailySecurity = null;
		try {
			query = em.createQuery(sb.toString());
			query.setParameter("date",CommonsUtilities.convertDatetoStringForTemplate(currentDate, CommonsUtilities.DATE_PATTERN));
			lstDailySecurity=query.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return lstDailySecurity;
	}
	

	/**
	 * 
	 * @param securityTO
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	public List<SecurityOverdueTO> findSecuritiesForExpiration(SecurityTO securityTO) throws ServiceException{
		
		List<SecurityOverdueTO> listSecurityOverdueTO=new ArrayList<SecurityOverdueTO>(); 
		StringBuilder sbQuery = new StringBuilder();


		sbQuery.append(" SELECT DISTINCT    ");
		sbQuery.append("  ISS.mnemonic, ");//0
		sbQuery.append("  PA.idParticipantPk, ");//1
		sbQuery.append("  PA.mnemonic, ");//2
		sbQuery.append("  SE.idSecurityCodePk, ");//3
		sbQuery.append("  SE.currentNominalValue, ");//4
		sbQuery.append("  HA.idHolderAccountPk, ");//5
		sbQuery.append("  HA.alternateCode, ");//6
		sbQuery.append("  HAB.totalBalance, ");//7
		sbQuery.append("  HAB.availableBalance, ");//8
		sbQuery.append("  HAB.banBalance, ");//9
		sbQuery.append("  HAB.pawnBalance, ");//10
		sbQuery.append("  HMB, ");//11
		sbQuery.append("  HAB.id ");//12
		
		sbQuery.append(" FROM HolderAccountBalance HAB  ");
		sbQuery.append(" INNER JOIN  HAB.holderAccount HA ");
		sbQuery.append(" INNER JOIN  HAB.security SE ");
		sbQuery.append(" INNER JOIN  HAB.participant PA ");
		sbQuery.append(" INNER JOIN  HA.holderAccountDetails HAD ");
		sbQuery.append(" INNER JOIN  HAD.holder HO ");
		sbQuery.append(" INNER JOIN  SE.issuer ISS ");
		sbQuery.append(" INNER JOIN  HAB.holderMarketfactBalance HMB ");
				
		sbQuery.append("  WHERE   1=1 and hab.totalBalance > 0");
		sbQuery.append("  and HMB.indActive = "+BooleanType.YES.getCode());
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuerCodePk()  )){
			sbQuery.append(" and se.issuer.idIssuerPk= :idIssuerCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuanceCodePk() )){
			sbQuery.append(" and se.issuance.idIssuanceCodePk= :idIssuanceCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdSecurityCodePk() )){
			sbQuery.append(" and se.idSecurityCodePk= :idSecurityCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIsinCodePk() )){
			sbQuery.append(" and se.idIsinCode= :idIsinCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityCurrency() )){
			sbQuery.append(" and se.currency= :securityCurrency ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityType() )){
			sbQuery.append(" and se.securityType= :securityType ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityClass())){
			sbQuery.append(" and se.securityClass= :securityClass ");
		}else if(Validations.validateListIsNotNullAndNotEmpty(securityTO.getLstSecurityClass())){
			sbQuery.append(" and se.securityClass in( :lstSecurityClass) ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIssuanceDate() )){
			sbQuery.append(" and se.issuanceDate= :issuanceDate ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getExpirationDate() )){
			sbQuery.append(" and se.expirationDate= :expirationDate ");
		}
		if(Validations.validateIsNotNull(securityTO.getInitialDate())){
			sbQuery.append(" and trunc(se.expirationDate) >= :initialDate ");
		}
		if(Validations.validateIsNotNull(securityTO.getFinalDate())){
			sbQuery.append(" and trunc(se.expirationDate) <= :finalDate ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdCfiCodePk() )){
			sbQuery.append(" and se.cfiCode = :cfiCode");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getAlternativeCode() )){
			sbQuery.append(" and se.alternativeCode= :alternativeCode ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getDescriptionSecurity() )){
			sbQuery.append(" and se.description like :description ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getSecurityState())){
			sbQuery.append(" and se.stateSecurity= :securityState ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIndIsCoupon() )){
			sbQuery.append(" and se.indIsCoupon= :indIsCoupon ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIndIsDetached() )){
			sbQuery.append(" and se.indIsDetached= :indIsDetached ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdParticipant())){
			sbQuery.append(" and hab.participant.idParticipantPk = :idParticipantPk ");
		}
		if(Validations.validateListIsNotNullAndNotEmpty(securityTO.getLstHolderAccountPk())){
			sbQuery.append(" and hab.holderAccount.idHolderAccountPk in (:listIdHolderAccountPk) ");
		}
		if(Validations.validateIsNotNullAndPositive(securityTO.getIdHolderAccountPk())){
			sbQuery.append(" and HA.idHolderAccountPk = :idHolderAccountPk ");
		}
		
		//Excluded Security Code
		if(Validations.validateListIsNotNullAndNotEmpty(securityTO.getListExcludedSecurityCode())){
			sbQuery.append(" and not se.idSecurityCodePk in :listSecCodeExcluded ");
		}
		
		//Excluded Security Class
		if(Validations.validateListIsNotNullAndNotEmpty(securityTO.getListExcludedSecurityClass())){
			sbQuery.append(" and not se.securityClass in :listSecClassExcluded ");
		}
		
		sbQuery.append(" Order By SE.idSecurityCodePk, HA.alternateCode ");
    	Query query = em.createQuery(sbQuery.toString());

    	if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuerCodePk()  )){
    		query.setParameter("idIssuerCodePk",  securityTO.getIdIssuerCodePk()  );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuanceCodePk() )){
			query.setParameter("idIssuanceCodePk",  securityTO.getIdIssuanceCodePk() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdSecurityCodePk() )){
			query.setParameter("idSecurityCodePk",  securityTO.getIdSecurityCodePk() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIsinCodePk() )){
			query.setParameter("idIsinCodePk",  securityTO.getIdIsinCodePk() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityCurrency() )){
			query.setParameter("securityCurrency",  securityTO.getSecurityCurrency() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityType() )){
			query.setParameter("securityType",  securityTO.getSecurityType() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityClass() )){
			query.setParameter("securityClass",  securityTO.getSecurityClass() );
		}else if(Validations.validateListIsNotNullAndNotEmpty(securityTO.getLstSecurityClass())){
			query.setParameter("lstSecurityClass",  securityTO.getLstSecurityClass() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIssuanceDate() )){
			query.setParameter("issuanceDate",  securityTO.getIssuanceDate() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getExpirationDate() )){
			query.setParameter("expirationDate",  securityTO.getExpirationDate() );
		}
		
		if(Validations.validateIsNotNull(securityTO.getInitialDate())){
			query.setParameter("initialDate",  securityTO.getInitialDate());
		}
		if(Validations.validateIsNotNull(securityTO.getFinalDate())){
			query.setParameter("finalDate",  securityTO.getFinalDate() );
		}
		
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdCfiCodePk() )){
			query.setParameter("cfiCode",   securityTO.getIdCfiCodePk()  );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getAlternativeCode() )){
			query.setParameter("alternativeCode",  securityTO.getAlternativeCode() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getDescriptionSecurity() )){
			query.setParameter("description",  new StringBuilder()
												.append(GeneralConstants.PERCENTAGE_CHAR)
												.append(securityTO.getDescriptionSecurity())
												.append(GeneralConstants.PERCENTAGE_CHAR).toString())  ;
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getSecurityState())){
			query.setParameter("securityState", Integer.parseInt(securityTO.getSecurityState()));
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIndIsCoupon() )){
			query.setParameter("indIsCoupon",  securityTO.getIndIsCoupon() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIndIsDetached() )){
			query.setParameter("indIsDetached",  securityTO.getIndIsDetached() );
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdParticipant())){
			query.setParameter("idParticipantPk",  securityTO.getIdParticipant() );
		}
		if(Validations.validateListIsNotNullAndNotEmpty(securityTO.getLstHolderAccountPk())){
			query.setParameter("listIdHolderAccountPk",  securityTO.getLstHolderAccountPk() );
		}
		if(Validations.validateIsNotNullAndPositive(securityTO.getIdHolderAccountPk())){
			query.setParameter("idHolderAccountPk",  securityTO.getIdHolderAccountPk() );
		}
		
		//Excluded Security Code
		if(Validations.validateListIsNotNullAndNotEmpty(securityTO.getListExcludedSecurityCode())){
			query.setParameter("listSecCodeExcluded",  securityTO.getListExcludedSecurityCode() );
		}
		
		//Excluded Security Class
		if(Validations.validateListIsNotNullAndNotEmpty(securityTO.getListExcludedSecurityClass())){
			query.setParameter("listSecClassExcluded",  securityTO.getListExcludedSecurityClass() );
		}

		List<Object[]> lstResult = (List<Object[]>)query.getResultList();

		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
			SecurityOverdueTO securityOverdueTO;
			for (Object[] objMov : lstResult) {

				securityOverdueTO = new SecurityOverdueTO();
				if(objMov[11] instanceof ArrayList){

					List<HolderMarketFactBalance> holderMarketFactBalance= (List<HolderMarketFactBalance>)objMov[11];
					securityOverdueTO.setListMarketFactAccountTO(getListMarketFactAccountTO(holderMarketFactBalance));
					
				}else if(objMov[11] instanceof HolderMarketFactBalance){
					HolderMarketFactBalance holderMarketFactBalance=(HolderMarketFactBalance)objMov[11];
					MarketFactAccountTO marketFactAccountTO=new MarketFactAccountTO();
					marketFactAccountTO.setMarketDate(holderMarketFactBalance.getMarketDate());
					marketFactAccountTO.setMarketPrice(holderMarketFactBalance.getMarketPrice());
					marketFactAccountTO.setMarketRate(holderMarketFactBalance.getMarketRate());
					marketFactAccountTO.setQuantity(holderMarketFactBalance.getAvailableBalance());
					securityOverdueTO.setListMarketFactAccountTO(Arrays.asList(marketFactAccountTO));
				}
				
				Integer blockBalance=Integer.parseInt(objMov[9].toString())+Integer.parseInt(objMov[10].toString());
				securityOverdueTO.setKeyHolderAccountBalance(objMov[12].toString());
				securityOverdueTO.setMnemonicIssuer(objMov[0].toString());
				securityOverdueTO.setIdParticipantPK((Long) objMov[1]);
				securityOverdueTO.setMnemonicParticipant(objMov[2].toString());
				securityOverdueTO.setIdSecurityCodePk(objMov[3].toString());
				securityOverdueTO.setNominalValue((BigDecimal) objMov[4]);
				securityOverdueTO.setIdHolderAccountPk((Long) objMov[5]);
				securityOverdueTO.setAlternateCode(objMov[6].toString());
				securityOverdueTO.setDescriptionHolder(accountQueryServiceBean.getFullNameHolderAccount((Long) objMov[5]));
				securityOverdueTO.setTotalBalance(Integer.parseInt(objMov[7].toString()));
				securityOverdueTO.setAvailableBalance(Integer.parseInt(objMov[8].toString()));
				securityOverdueTO.setBanBalance(Integer.parseInt(objMov[9].toString()));
				securityOverdueTO.setPawnBalance(Integer.parseInt(objMov[10].toString()));
				securityOverdueTO.setBlockBalance(blockBalance);
				
				listSecurityOverdueTO.add(securityOverdueTO);
			}
		}
		return listSecurityOverdueTO;
		

	}
	
	/**
	 * Fill List Market Fact Account To Since List HolderMarketFactBalance 
	 * @param listHolderMarketFactBalance  List HolderMarketFactBalance
	 * @return
	 */
	public List<MarketFactAccountTO> getListMarketFactAccountTO(List<HolderMarketFactBalance> listHolderMarketFactBalance){
		List<MarketFactAccountTO> listMarketFactAccountTO=new ArrayList<MarketFactAccountTO>(); 
		
		for (HolderMarketFactBalance holderMarketFactBalance : listHolderMarketFactBalance) {
			MarketFactAccountTO marketFactAccountTO=new MarketFactAccountTO();
			marketFactAccountTO.setMarketDate(holderMarketFactBalance.getMarketDate());
			marketFactAccountTO.setMarketPrice(holderMarketFactBalance.getMarketPrice());
			marketFactAccountTO.setMarketRate(holderMarketFactBalance.getMarketRate());
			marketFactAccountTO.setQuantity(holderMarketFactBalance.getAvailableBalance());
			listMarketFactAccountTO.add(marketFactAccountTO);
		}

		return listMarketFactAccountTO;
	}
	

	/**
	 * Confirm Withdraw expired Securities 
	 * @param listSecurityOverdueTO
	 * @throws ServiceException
	 */
	public void confirmWithdrawExpiredSecurities(List<SecurityOverdueTO> listSecurityOverdueTO, LoggerUser loggerUser) throws ServiceException{
		
		List<HolderAccountBalanceTO> lstSourceAccounts=new ArrayList<HolderAccountBalanceTO>();
		for (SecurityOverdueTO securityOverdueTO : listSecurityOverdueTO) {
			HolderAccountBalanceTO holderAccountBalanceTO=new HolderAccountBalanceTO();
			holderAccountBalanceTO.setIdHolderAccount(securityOverdueTO.getIdHolderAccountPk());
			holderAccountBalanceTO.setIdParticipant(securityOverdueTO.getIdParticipantPK());
			holderAccountBalanceTO.setIdSecurityCode(securityOverdueTO.getIdSecurityCodePk());
			holderAccountBalanceTO.setStockQuantity(new BigDecimal(securityOverdueTO.getAvailableBalance()));
			holderAccountBalanceTO.setLstMarketFactAccounts(securityOverdueTO.getListMarketFactAccountTO());
			lstSourceAccounts.add(holderAccountBalanceTO);
		}
		AccountsComponentTO objAccountComponent = new AccountsComponentTO();		
		objAccountComponent.setIdBusinessProcess(BusinessProcessType.SECURITY_RETIREMENT_CONFIRM.getCode());
		objAccountComponent.setLstSourceAccounts(lstSourceAccounts);
		objAccountComponent.setLstTargetAccounts(new ArrayList<HolderAccountBalanceTO>());
		objAccountComponent.setIdOperationType(ParameterOperationType.WITHDRAWAL_OF_SECURITIES_EXPIRED.getCode());
		objAccountComponent.setIndMarketFact(idepositarySetup.getIndMarketFact());
		
		accountsComponentService.get().executeAccountsComponent(objAccountComponent);
		
		
		for (SecurityOverdueTO securityOverdueTO : listSecurityOverdueTO) {
			SecuritiesComponentTO securitiesComponentTO = new SecuritiesComponentTO();
			SecuritiesOperation securitiesOperation= registerSecuritiesOperation(securityOverdueTO, loggerUser);
			securitiesComponentTO.setIdBusinessProcess(BusinessProcessType.SECURITY_RETIREMENT_CONFIRM.getCode());
			securitiesComponentTO.setIdOperationType(ParameterOperationType.WITHDRAWAL_OF_SECURITIES_EXPIRED.getCode());
			securitiesComponentTO.setIdSecuritiesOperation(securitiesOperation.getIdSecuritiesOperationPk());
			securitiesComponentTO.setIdSecurityCode(securityOverdueTO.getIdSecurityCodePk());		
			securitiesComponentTO.setDematerializedBalance(new BigDecimal(securityOverdueTO.getAvailableBalance()));
			securitiesComponentTO.setShareBalance(new BigDecimal(securityOverdueTO.getAvailableBalance()));
			securitiesComponentTO.setCirculationBalance(new BigDecimal(securityOverdueTO.getAvailableBalance()));
			securitiesComponentTO.setSecuritiesSuspended(Boolean.TRUE);
			
			securitiesComponentService.get().executeSecuritiesComponent(securitiesComponentTO);
		}
		
		
		/**
		 * Actualizar el estado del Valor si ya no tiene saldos en las cuentas
		 */
		changeStateForSecurity(listSecurityOverdueTO, loggerUser);

	}

	
	/**
	 * Register securities operation.
	 * @param securityOverdueTO 
	 * @param loggerUser
	 * @return
	 * @throws ServiceException
	 */
	private SecuritiesOperation registerSecuritiesOperation(SecurityOverdueTO securityOverdueTO, LoggerUser loggerUser)throws ServiceException{

		BigDecimal cashAmount = null;
		SecuritiesOperation securitiesOperation=null;
		
		if(Validations.validateIsNotNullAndNotEmpty(securityOverdueTO.getIdSecurityCodePk())&&
				Validations.validateIsNotNullAndPositive(securityOverdueTO.getIdParticipantPK())&&
						Validations.validateIsNotNullAndPositive(securityOverdueTO.getIdHolderAccountPk())){
			Security security= new Security(securityOverdueTO.getIdSecurityCodePk());
			Participant participant= new Participant(securityOverdueTO.getIdParticipantPK());
			HolderAccount holderAccount= new HolderAccount(securityOverdueTO.getIdHolderAccountPk());
			
			/**
			 * Create Retirement Operation
			 */
			RetirementOperation retirementOperation = new RetirementOperation();
			retirementOperation.setMotive(RetirementOperationMotiveType.REDEMPTION_FIXED_INCOME.getCode());
			retirementOperation.setRetirementType(RetirementOperationRedemptionType.TOTAL.getCode());
			retirementOperation.setIndRetirementExpiration(BooleanType.YES.getCode());
			retirementOperation.setRetirementDetails(new ArrayList<RetirementDetail>());
			retirementOperation.setSecurity(security);
			
			/**
			 * Create Retirement Detail
			 */
			RetirementDetail retirementDetail = new RetirementDetail();
			retirementOperation.setTotalBalance(new BigDecimal(securityOverdueTO.getAvailableBalance()));
			retirementDetail.setAvailableBalance(new BigDecimal(securityOverdueTO.getAvailableBalance()));
			retirementDetail.setHolderAccount(holderAccount);
			retirementDetail.setParticipant(participant);
			retirementDetail.setRetirementOperation(retirementOperation);
			retirementDetail.setRetirementMarketfacts(new ArrayList<RetirementMarketfact>());
			
			/**
			 * Create Retirement MarketFact
			 */
			for (MarketFactAccountTO marketFactAccount : securityOverdueTO.getListMarketFactAccountTO()) {
				RetirementMarketfact retirementMarketfact = new RetirementMarketfact();
				retirementMarketfact.setRetirementDetail(retirementDetail);
				retirementMarketfact.setMarketRate(marketFactAccount.getMarketRate());
				retirementMarketfact.setMarketPrice(marketFactAccount.getMarketPrice());
				retirementMarketfact.setMarketDate(marketFactAccount.getMarketDate());
				retirementMarketfact.setOperationQuantity(new BigDecimal(securityOverdueTO.getAvailableBalance()));
				retirementDetail.getRetirementMarketfacts().add(retirementMarketfact);
			}
			
			retirementOperation.getRetirementDetails().add(retirementDetail);
			retirementOperation.setState(RetirementOperationStateType.APPROVED.getCode());
			retirementOperation.setCustodyOperation(new CustodyOperation());
			
			/**
			 * Create Securities Operation
			 */
			securitiesOperation = new SecuritiesOperation();
			securitiesOperation.setSecurities(new Security(securityOverdueTO.getIdSecurityCodePk()));
			securitiesOperation.setOperationDate(CommonsUtilities.currentDate());
			//* Cash Amount
			if(Validations.validateIsNotNull(securityOverdueTO.getAvailableBalance())){
				if(securityOverdueTO.getAvailableBalance()>=0){
					cashAmount = CommonsUtilities.multiplyDecimalInteger(securityOverdueTO.getNominalValue(), securityOverdueTO.getAvailableBalance(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
				}
			}
			securitiesOperation.setCashAmount(cashAmount);
			securitiesOperation.setStockQuantity(new BigDecimal(securityOverdueTO.getAvailableBalance()));
			securitiesOperation.setOperationType(ParameterOperationType.WITHDRAWAL_OF_SECURITIES_EXPIRED.getCode());
			
			saveRetirementOperation(retirementOperation, securityOverdueTO, loggerUser, false);
			changeStateRetirementOperation(retirementOperation, loggerUser, RetirementOperationStateType.CONFIRMED.getCode());
		}
		
		return create(securitiesOperation);
	}
	
	
	
	/**
	 * Save retirement operation.
	 *
	 * @param retirementOperation the retirement operation
	 * @param loggerUser the logger user
	 * @param totalRemoval the total removal
	 * @throws ServiceException the service exception
	 */
	public void saveRetirementOperation(RetirementOperation retirementOperation, SecurityOverdueTO securityOverdueTO, LoggerUser loggerUser, boolean totalRemoval) throws ServiceException {
		Map<String, Object> params = new HashMap<String, Object>();
		HolderAccount holderAccount=null;
		
		for(RetirementDetail retirementDet : retirementOperation.getRetirementDetails()){
			params.put("idHolderAccountPkParam", retirementDet.getHolderAccount().getIdHolderAccountPk());
			holderAccount=(HolderAccount) findObjectByNamedQuery(HolderAccount.HOLDER_ACCOUNT_STATE,params);
			
			if(!HolderAccountStatusType.ACTIVE.getCode().equals( holderAccount.getStateAccount())){
				String code=holderAccount.getAccountNumber().toString();
				throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_NOT_REGISTERED,new Object[]{code});
			}
		}
        
        CustodyOperation custodyOperation = retirementOperation.getCustodyOperation();
		if(retirementOperation.getMotive().equals(RetirementOperationMotiveType.REDEMPTION_VARIABLE_INCOME.getCode())){
			custodyOperation.setOperationType(ParameterOperationType.SECURITIES_REDEMPTION_EQUITIES.getCode());
		}else if(retirementOperation.getMotive().equals(RetirementOperationMotiveType.REVERSION.getCode())){
			custodyOperation.setOperationType(ParameterOperationType.SECURITIES_REVERSION.getCode());
		}else if(RetirementOperationMotiveType.REDEMPTION_FIXED_INCOME.getCode().equals(retirementOperation.getMotive())){
			custodyOperation.setOperationType(ParameterOperationType.SECURITIES_REDEMPTION_FIXED_INCOME.getCode());
		}
		
		custodyOperation.setOperationDate(CommonsUtilities.currentDateTime());		
		custodyOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		custodyOperation.setRegistryUser(loggerUser.getUserName());
		custodyOperation.setState(retirementOperation.getState());
		
		retirementOperation.setState(custodyOperation.getState());
        
		Long operationNumber = getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_SECURITIES_WITHDRAWAL);
        custodyOperation.setOperationNumber(operationNumber);
				
		create(retirementOperation);

		for (RetirementDetail retirementDetail : retirementOperation.getRetirementDetails()) {
			
			if(totalRemoval){
				retirementDetail.setRetirementMarketfacts(new ArrayList<RetirementMarketfact>());
				
				BigDecimal totalBalanceByMktFact = BigDecimal.ZERO; 
				for (MarketFactAccountTO marketFactTO : securityOverdueTO.getListMarketFactAccountTO()) {
					RetirementMarketfact retirementMarketfact = new RetirementMarketfact();
					retirementMarketfact.setMarketDate(marketFactTO.getMarketDate());
					retirementMarketfact.setMarketRate(marketFactTO.getMarketRate());
					retirementMarketfact.setMarketPrice(marketFactTO.getMarketPrice());
					retirementMarketfact.setOperationQuantity(marketFactTO.getQuantity());
					retirementMarketfact.setRetirementDetail(retirementDetail);
					retirementDetail.getRetirementMarketfacts().add(retirementMarketfact);
					totalBalanceByMktFact = totalBalanceByMktFact.add(retirementMarketfact.getOperationQuantity());
				}
				
				if(totalBalanceByMktFact.compareTo(retirementDetail.getAvailableBalance()) != 0){
					throw new ServiceException(ErrorServiceType.CONCURRENCY_LOCK_UPDATE);
				}
			}
			create(retirementDetail);
			saveAll(retirementDetail.getRetirementMarketfacts());
			
			if(Validations.validateListIsNotNullAndNotEmpty(retirementDetail.getRetirementBlockDetails())){
				for (RetirementBlockDetail retirementBlockDetail : retirementDetail.getRetirementBlockDetails()) {
					create(retirementBlockDetail);
					saveAll(retirementBlockDetail.getRetirementMarketfacts());
				}
			}
			
		}

		
	}
	
	/**
	 * Change state retirement operation.
	 *
	 * @param objRetirementOperation the obj retirement operation
	 * @param loggerUser the logger user
	 * @param newState the new state
	 * @throws ServiceException the service exception
	 */
	public void changeStateRetirementOperation(RetirementOperation objRetirementOperation, LoggerUser loggerUser, Integer newState) throws ServiceException {
		RetirementOperation retirementOperation = find(RetirementOperation.class, objRetirementOperation.getIdRetirementOperationPk());
		
		if(RetirementOperationStateType.APPROVED.getCode().equals(newState)){
			retirementOperation.getCustodyOperation().setApprovalUser(loggerUser.getUserName());
			retirementOperation.getCustodyOperation().setApprovalDate(CommonsUtilities.currentDateTime());
			retirementOperation.setState(RetirementOperationStateType.APPROVED.getCode());
		} else if(RetirementOperationStateType.ANNULLED.getCode().equals(newState)){
			retirementOperation.getCustodyOperation().setRejectUser(loggerUser.getUserName());
			retirementOperation.getCustodyOperation().setRejectDate(CommonsUtilities.currentDateTime());
			retirementOperation.setState(RetirementOperationStateType.ANNULLED.getCode());
			retirementOperation.getCustodyOperation().setRejectMotive(objRetirementOperation.getRejectMotive());
			retirementOperation.getCustodyOperation().setRejectMotiveOther(objRetirementOperation.getRejectMotiveOther());
		} else if(RetirementOperationStateType.CONFIRMED.getCode().equals(newState)){
			retirementOperation.getCustodyOperation().setConfirmUser(loggerUser.getUserName());
			retirementOperation.getCustodyOperation().setConfirmDate(CommonsUtilities.currentDateTime());
			retirementOperation.setState(RetirementOperationStateType.CONFIRMED.getCode());
		} else if(RetirementOperationStateType.REJECTED.getCode().equals(newState)){
			retirementOperation.getCustodyOperation().setRejectUser(loggerUser.getUserName());
			retirementOperation.getCustodyOperation().setRejectDate(CommonsUtilities.currentDateTime());
			retirementOperation.setState(RetirementOperationStateType.REJECTED.getCode());
			retirementOperation.getCustodyOperation().setRejectMotive(objRetirementOperation.getRejectMotive());
			retirementOperation.getCustodyOperation().setRejectMotiveOther(objRetirementOperation.getRejectMotiveOther());
		} else if (RetirementOperationStateType.REVERSED.getCode().equals(newState)){
			retirementOperation.setState(RetirementOperationStateType.REVERSED.getCode());
			retirementOperation.getCustodyOperation().setOperationType(ParameterOperationType.REVERSION_SECURITIES_REDEMPTION_FIXED_INCOME.getCode());
		}
		retirementOperation.getCustodyOperation().setState(retirementOperation.getState());
		
		update(retirementOperation);		
	}
	
	/**
	 * 	 * Find Security For Expiration By Issuer
	 * @param securityTO
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	public List<ExpirationSecurityTO> findSecForExpirationByIssuer(SecurityTO securityTO) throws ServiceException{
		List<ExpirationSecurityTO> listExpirationSecurityTO = new ArrayList<ExpirationSecurityTO>(); 
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" SELECT 																																	 	 ");
		sbQuery.append("     CO.ID_CORPORATIVE_OPERATION_PK,																										 	 ");
		sbQuery.append("     CET.description,																															 ");
		sbQuery.append("     'DISPONIBLE' DESCRIPTION_SALDO,                                                                                                             ");
		sbQuery.append("     CPR.ID_CORPORATIVE_PROCESS_RESULT,																											 ");
		sbQuery.append("     HA.ACCOUNT_NUMBER,		                                                                                                                     ");
		sbQuery.append("     (SELECT LISTAGG(H.ID_HOLDER_PK,'<br>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)                                                             ");
		sbQuery.append("      FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK                                                    ");
		sbQuery.append("      WHERE HAD.ID_HOLDER_ACCOUNT_FK = CPR.ID_HOLDER_ACCOUNT_FK) CUI,																			 ");
		sbQuery.append("     (SELECT LISTAGG(H.FULL_NAME,'<br>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)                                                                ");
		sbQuery.append("      FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK                                                    ");
		sbQuery.append("      WHERE HAD.ID_HOLDER_ACCOUNT_FK = CPR.ID_HOLDER_ACCOUNT_FK) NAME_HOLDER,                                                                    ");
		sbQuery.append("     CO.ID_ORIGIN_SECURITY_CODE_FK,                                                                                                              ");
		sbQuery.append("     (CASE CET.ID_CORPORATIVE_EVENT_TYPE_PK                                                                                                      ");
		sbQuery.append("         WHEN 14 THEN PIC.BEGINING_DATE                                                                                                          ");
		sbQuery.append("         ELSE PAC.BEGINING_DATE                                                                                                          ");
		sbQuery.append("      END) BEGINING_DATE,																														 ");
		sbQuery.append("     (CASE CET.ID_CORPORATIVE_EVENT_TYPE_PK                                                                                                      ");
		sbQuery.append("         WHEN 14 THEN PIC.EXPERITATION_DATE                                                                                                      ");
		sbQuery.append("         ELSE PAC.EXPRITATION_DATE                                                                                                       ");
		sbQuery.append("      END) EXPIRATION_DATE,                                                                                                                      ");
		sbQuery.append("     SCB.AVAILABLE_BALANCE SALDO,                                                                                                                ");
		sbQuery.append("     (CASE CET.ID_CORPORATIVE_EVENT_TYPE_PK                                                                                                      ");
		sbQuery.append("         WHEN 14 THEN PIC.COUPON_AMOUNT                                                                                                          ");
		sbQuery.append("         ELSE PAC.COUPON_AMOUNT                                                                                                          ");
		sbQuery.append("      END) VALOR_FINAL,                                                                                                                          ");
		sbQuery.append("     (CASE                                                                                                                                       ");
		sbQuery.append("         WHEN  CO.CURRENCY IN (1304,1734,1853) THEN (SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = CO.CURRENCY_PAYMENT)     ");
		sbQuery.append("         ELSE (SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = CO.CURRENCY)                                                   ");
		sbQuery.append("     END) CURRENCY,                                                                                                                              ");
		sbQuery.append("     CPR.AVAILABLE_BALANCE MONTO_TOTAL,                                                                                                          ");
		sbQuery.append("     CPR.IND_AVAILABLE INDICADOR                                                                                                                ");
		sbQuery.append(" FROM                                                                                                                                            ");
		sbQuery.append("     corporative_operation CO																													 ");
		sbQuery.append("     inner join CORPORATIVE_PROCESS_RESULT CPR   on  CPR.ID_CORPORATIVE_OPERATION_FK = CO.ID_CORPORATIVE_OPERATION_PK                            ");
		sbQuery.append("     inner join corporative_event_type CET       on  CO.CORPORATIVE_EVENT_TYPE = CET.ID_CORPORATIVE_EVENT_TYPE_PK                                ");
		sbQuery.append("     inner join stock_calculation_process SCP    on  SCP.id_stock_calculation_pk = CO.id_stock_calculation                                       ");
		sbQuery.append("     inner join stock_calculation_balance SCB    on  SCB.id_stock_calculation_fk = SCP.id_stock_calculation_pk AND                               ");
		sbQuery.append("                                                     SCB.ID_PARTICIPANT_FK = CPR.ID_PARTICIPANT_FK AND                                           ");
		sbQuery.append("                                                     SCB.ID_HOLDER_ACCOUNT_FK = CPR.ID_HOLDER_ACCOUNT_FK AND                                     ");
		sbQuery.append("                                                     SCB.ID_SECURITY_CODE_FK = CPR.ID_SECURITY_CODE_FK                                           ");
		sbQuery.append("     inner join issuer iss                       on  iss.id_issuer_pk = co.id_issuer_fk                                                          ");
		sbQuery.append("     inner join holder_account ha                on  ha.id_holder_account_pk = CPR.ID_HOLDER_ACCOUNT_FK                                          ");
		sbQuery.append("     inner join security sec                     on  sec.id_security_code_pk = CO.ID_ORIGIN_SECURITY_CODE_FK                                     ");
		sbQuery.append("     left join PROGRAM_INTEREST_COUPON PIC       on  CO.ID_PROGRAM_INTEREST_FK = PIC.ID_PROGRAM_INTEREST_PK                                      ");
		sbQuery.append("     left join program_amortization_coupon PAC   on  CO.ID_PROGRAM_AMORTIZATION_FK = PAC.ID_PROGRAM_AMORTIZATION_PK                              ");
		sbQuery.append(" WHERE 1 = 1 AND                                                                                                                                 ");
		sbQuery.append("     ((CET.ID_CORPORATIVE_EVENT_TYPE_PK = 14 and TRUNC(PIC.EXPERITATION_DATE) = :expirationDate) OR												 ");
		sbQuery.append("         (CET.ID_CORPORATIVE_EVENT_TYPE_PK = 15 and CPR.IND_ORIGIN_TARGET = 2 and TRUNC(PAC.EXPRITATION_DATE) = :expirationDate)) AND			 ");
		sbQuery.append("     SCB.AVAILABLE_BALANCE > 0																													 ");
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdIssuerCodePk())){
			sbQuery.append(" and iss.id_issuer_pk = :idIssuerCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdHolderPk())){
			sbQuery.append(" AND (EXISTS																																");
			sbQuery.append(" 	(SELECT had.ID_HOLDER_FK																												");
			sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL had																											");
			sbQuery.append(" 	WHERE had.ID_HOLDER_FK = :cui_code																										");
			sbQuery.append(" 		AND had.ID_HOLDER_ACCOUNT_FK = CPR.ID_HOLDER_ACCOUNT_FK))																			");
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getSecurityCurrency())){
			sbQuery.append(" and CO.CURRENCY= :currency 																												");
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getSecurityClass())){
			sbQuery.append(" and sec.security_class= :securityClass 																									");
		}
		sbQuery.append("                                                                                                                                                 ");
		sbQuery.append(" UNION                                                                                                                                           ");
		sbQuery.append("                                                                                                                                                 ");
		
		sbQuery.append(" SELECT                                                                                                                                          ");
		sbQuery.append("     CO.ID_CORPORATIVE_OPERATION_PK,                                                                                                             ");
		sbQuery.append("     CET.description,                                                                                                                            ");
		sbQuery.append("     'EMBARGO' DESCRIPTION_SALDO,                                                                                                                ");
		sbQuery.append("     CPR.ID_CORPORATIVE_PROCESS_RESULT,                                                                                                          ");
		sbQuery.append("     HA.ACCOUNT_NUMBER,                                                                                                                   		 ");
		sbQuery.append("     (SELECT LISTAGG(H.ID_HOLDER_PK,'<br>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)                                                             ");
		sbQuery.append("      FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK                                                    ");
		sbQuery.append("      WHERE HAD.ID_HOLDER_ACCOUNT_FK = CPR.ID_HOLDER_ACCOUNT_FK) CUI,                                                                            ");
		sbQuery.append("     (SELECT LISTAGG(H.FULL_NAME,'<br>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)                                                                ");
		sbQuery.append("      FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK                                                    ");
		sbQuery.append("      WHERE HAD.ID_HOLDER_ACCOUNT_FK = CPR.ID_HOLDER_ACCOUNT_FK) NAME_HOLDER,                                                                    ");
		sbQuery.append("     CO.ID_ORIGIN_SECURITY_CODE_FK,                                                                                                              ");
		sbQuery.append("     (CASE CET.ID_CORPORATIVE_EVENT_TYPE_PK                                                                                                      ");
		sbQuery.append("         WHEN 14 THEN PIC.BEGINING_DATE                                                                                                          ");
		sbQuery.append("         ELSE PAC.BEGINING_DATE                                                                                                          ");
		sbQuery.append("      END) BEGINING_DATE,                                                                                                                        ");
		sbQuery.append("     (CASE CET.ID_CORPORATIVE_EVENT_TYPE_PK                                                                                                      ");
		sbQuery.append("         WHEN 14 THEN PIC.EXPERITATION_DATE                                                                                                      ");
		sbQuery.append("         ELSE PAC.EXPRITATION_DATE                                                                                                       ");
		sbQuery.append("      END) EXPIRATION_DATE,                                                                                                                      ");
		sbQuery.append("     SCB.BAN_BALANCE SALDO,                                                                                                                      ");
		sbQuery.append("     (CASE CET.ID_CORPORATIVE_EVENT_TYPE_PK                                                                                                      ");
		sbQuery.append("         WHEN 14 THEN PIC.COUPON_AMOUNT                                                                                                          ");
		sbQuery.append("         ELSE PAC.COUPON_AMOUNT                                                                                                          ");
		sbQuery.append("      END) VALOR_FINAL,                                                                                                                          ");
		sbQuery.append("     (CASE                                                                                                                                       ");
		sbQuery.append("         WHEN  CO.CURRENCY IN (1304,1734,1853) THEN (SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = CO.CURRENCY_PAYMENT)     ");
		sbQuery.append("         ELSE (SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = CO.CURRENCY)                                                   ");
		sbQuery.append("     END) CURRENCY,                                                                                                                              ");
		sbQuery.append("     CPR.BAN_BALANCE MONTO_TOTAL,                                                                                                                ");
		sbQuery.append("     CPR.IND_BAN_BALANCE INDICADOR                                                                                                              ");
		sbQuery.append(" FROM                                                                                                                                            ");
		sbQuery.append("     corporative_operation CO                                                                                                                    ");
		sbQuery.append("     inner join CORPORATIVE_PROCESS_RESULT CPR   on  CPR.ID_CORPORATIVE_OPERATION_FK = CO.ID_CORPORATIVE_OPERATION_PK                            ");
		sbQuery.append("     inner join corporative_event_type CET       on  CO.CORPORATIVE_EVENT_TYPE = CET.ID_CORPORATIVE_EVENT_TYPE_PK                                ");
		sbQuery.append("     inner join stock_calculation_process SCP    on  SCP.id_stock_calculation_pk = CO.id_stock_calculation                                       ");
		sbQuery.append("     inner join stock_calculation_balance SCB    on  SCB.id_stock_calculation_fk = SCP.id_stock_calculation_pk AND                               ");
		sbQuery.append("                                                     SCB.ID_PARTICIPANT_FK = CPR.ID_PARTICIPANT_FK AND                                           ");
		sbQuery.append("                                                     SCB.ID_HOLDER_ACCOUNT_FK = CPR.ID_HOLDER_ACCOUNT_FK AND                                     ");
		sbQuery.append("                                                     SCB.ID_SECURITY_CODE_FK = CPR.ID_SECURITY_CODE_FK                                           ");
		sbQuery.append("     inner join issuer iss                       on  iss.id_issuer_pk = co.id_issuer_fk															 ");
		sbQuery.append("     inner join holder_account ha                on  ha.id_holder_account_pk = CPR.ID_HOLDER_ACCOUNT_FK                                          ");
		sbQuery.append("     inner join security sec                     on  sec.id_security_code_pk = CO.ID_ORIGIN_SECURITY_CODE_FK                                     ");
		sbQuery.append("     left join PROGRAM_INTEREST_COUPON PIC       on  CO.ID_PROGRAM_INTEREST_FK = PIC.ID_PROGRAM_INTEREST_PK                                      ");
		sbQuery.append("     left join program_amortization_coupon PAC   on  CO.ID_PROGRAM_AMORTIZATION_FK = PAC.ID_PROGRAM_AMORTIZATION_PK                              ");
		sbQuery.append(" WHERE 1 = 1 AND                                                                                                                                 ");
		sbQuery.append("     ((CET.ID_CORPORATIVE_EVENT_TYPE_PK = 14 and TRUNC(PIC.EXPERITATION_DATE) = :expirationDate) OR												 ");
		sbQuery.append("         (CET.ID_CORPORATIVE_EVENT_TYPE_PK = 15 and CPR.IND_ORIGIN_TARGET = 2 and TRUNC(PAC.EXPRITATION_DATE) = :expirationDate)) AND			 ");
		sbQuery.append("     SCB.BAN_BALANCE > 0                                                                                                                         ");
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdIssuerCodePk())){
			sbQuery.append(" and iss.id_issuer_pk = :idIssuerCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdHolderPk())){
			sbQuery.append(" AND (EXISTS																																");
			sbQuery.append(" 	(SELECT had.ID_HOLDER_FK																												");
			sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL had																											");
			sbQuery.append(" 	WHERE had.ID_HOLDER_FK = :cui_code																										");
			sbQuery.append(" 		AND had.ID_HOLDER_ACCOUNT_FK = CPR.ID_HOLDER_ACCOUNT_FK))																			");
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getSecurityCurrency())){
			sbQuery.append(" and CO.CURRENCY= :currency 																												");
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getSecurityClass())){
			sbQuery.append(" and sec.security_class= :securityClass 																									");
		}
		sbQuery.append("                                                                                                                                                 ");
		sbQuery.append(" UNION                                                                                                                                           ");
		sbQuery.append("                                                                                                                                                 ");
		
		sbQuery.append(" SELECT                                                                                                                                          ");
		sbQuery.append("     CO.ID_CORPORATIVE_OPERATION_PK,                                                                                                             ");
		sbQuery.append("     CET.description,                                                                                                                            ");
		sbQuery.append("     'PRENDA' DESCRIPTION_SALDO,                                                                                                                 ");
		sbQuery.append("     CPR.ID_CORPORATIVE_PROCESS_RESULT,                                                                                                          ");
		sbQuery.append("     HA.ACCOUNT_NUMBER,		                                                                                                                     ");
		sbQuery.append("     (SELECT LISTAGG(H.ID_HOLDER_PK,'<br>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)                                                             ");
		sbQuery.append("      FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK                                                    ");
		sbQuery.append("      WHERE HAD.ID_HOLDER_ACCOUNT_FK = CPR.ID_HOLDER_ACCOUNT_FK) CUI,                                                                            ");
		sbQuery.append("     (SELECT LISTAGG(H.FULL_NAME,'<br>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)                                                                ");
		sbQuery.append("      FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK                                                    ");
		sbQuery.append("      WHERE HAD.ID_HOLDER_ACCOUNT_FK = CPR.ID_HOLDER_ACCOUNT_FK) NAME_HOLDER,                                                                    ");
		sbQuery.append("     CO.ID_ORIGIN_SECURITY_CODE_FK,                                                                                                              ");
		sbQuery.append("     (CASE CET.ID_CORPORATIVE_EVENT_TYPE_PK                                                                                                      ");
		sbQuery.append("         WHEN 14 THEN PIC.BEGINING_DATE                                                                                                          ");
		sbQuery.append("         ELSE PAC.BEGINING_DATE                                                                                                          ");
		sbQuery.append("      END) BEGINING_DATE,                                                                                                                        ");
		sbQuery.append("     (CASE CET.ID_CORPORATIVE_EVENT_TYPE_PK                                                                                                      ");
		sbQuery.append("         WHEN 14 THEN PIC.EXPERITATION_DATE                                                                                                      ");
		sbQuery.append("         ELSE PAC.EXPRITATION_DATE                                                                                                       ");
		sbQuery.append("      END) EXPIRATION_DATE,                                                                                                                      ");
		sbQuery.append("     SCB.PAWN_BALANCE SALDO,                                                                                                                     ");
		sbQuery.append("     (CASE CET.ID_CORPORATIVE_EVENT_TYPE_PK                                                                                                      ");
		sbQuery.append("         WHEN 14 THEN PIC.COUPON_AMOUNT                                                                                                          ");
		sbQuery.append("         ELSE PAC.COUPON_AMOUNT                                                                                                          ");
		sbQuery.append("      END) VALOR_FINAL,                                                                                                                          ");
		sbQuery.append("     (CASE                                                                                                                                       ");
		sbQuery.append("         WHEN  CO.CURRENCY IN (1304,1734,1853) THEN (SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = CO.CURRENCY_PAYMENT)	 ");
		sbQuery.append("         ELSE (SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = CO.CURRENCY)                                                   ");
		sbQuery.append("     END) CURRENCY,                                                                                                                              ");
		sbQuery.append("     CPR.PAWN_BALANCE MONTO_TOTAL,                                                                                                               ");
		sbQuery.append("     CPR.IND_PAWN_BALANCE INDICADOR                                                                                                              ");
		sbQuery.append(" FROM                                                                                                                                            ");
		sbQuery.append("     corporative_operation CO                                                                                                                    ");
		sbQuery.append("     inner join CORPORATIVE_PROCESS_RESULT CPR   on  CPR.ID_CORPORATIVE_OPERATION_FK = CO.ID_CORPORATIVE_OPERATION_PK                            ");
		sbQuery.append("     inner join corporative_event_type CET       on  CO.CORPORATIVE_EVENT_TYPE = CET.ID_CORPORATIVE_EVENT_TYPE_PK                                ");
		sbQuery.append("     inner join stock_calculation_process SCP    on  SCP.id_stock_calculation_pk = CO.id_stock_calculation                                       ");
		sbQuery.append("     inner join stock_calculation_balance SCB    on  SCB.id_stock_calculation_fk = SCP.id_stock_calculation_pk AND                               ");
		sbQuery.append("                                                     SCB.ID_PARTICIPANT_FK = CPR.ID_PARTICIPANT_FK AND                                           ");
		sbQuery.append("                                                     SCB.ID_HOLDER_ACCOUNT_FK = CPR.ID_HOLDER_ACCOUNT_FK AND                                     ");
		sbQuery.append("                                                     SCB.ID_SECURITY_CODE_FK = CPR.ID_SECURITY_CODE_FK                                           ");
		sbQuery.append("     inner join issuer iss                       on  iss.id_issuer_pk = co.id_issuer_fk                                                          ");
		sbQuery.append("     inner join holder_account ha                on  ha.id_holder_account_pk = CPR.ID_HOLDER_ACCOUNT_FK                                          ");
		sbQuery.append("     inner join security sec                     on  sec.id_security_code_pk = CO.ID_ORIGIN_SECURITY_CODE_FK                                     ");
		sbQuery.append("     left join PROGRAM_INTEREST_COUPON PIC       on  CO.ID_PROGRAM_INTEREST_FK = PIC.ID_PROGRAM_INTEREST_PK                                      ");
		sbQuery.append("     left join program_amortization_coupon PAC   on  CO.ID_PROGRAM_AMORTIZATION_FK = PAC.ID_PROGRAM_AMORTIZATION_PK                              ");
		sbQuery.append(" WHERE 1 = 1 AND                                                                                                                                 ");
		sbQuery.append("     ((CET.ID_CORPORATIVE_EVENT_TYPE_PK = 14 and TRUNC(PIC.EXPERITATION_DATE) = :expirationDate) OR												 ");
		sbQuery.append("         (CET.ID_CORPORATIVE_EVENT_TYPE_PK = 15 and CPR.IND_ORIGIN_TARGET = 2 and TRUNC(PAC.EXPRITATION_DATE) = :expirationDate)) AND			 ");
		sbQuery.append("     SCB.PAWN_BALANCE > 0                                                                                                                        ");
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdIssuerCodePk())){
			sbQuery.append(" and iss.id_issuer_pk = :idIssuerCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdHolderPk())){
			sbQuery.append(" AND (EXISTS																																");
			sbQuery.append(" 	(SELECT had.ID_HOLDER_FK																												");
			sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL had																											");
			sbQuery.append(" 	WHERE had.ID_HOLDER_FK = :cui_code																										");
			sbQuery.append(" 		AND had.ID_HOLDER_ACCOUNT_FK = CPR.ID_HOLDER_ACCOUNT_FK))																			");
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getSecurityCurrency())){
			sbQuery.append(" and CO.CURRENCY= :currency 																												");
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getSecurityClass())){
			sbQuery.append(" and sec.security_class= :securityClass 																									");
		}
		sbQuery.append("                                                                                                                                                 ");
		sbQuery.append(" UNION                                                                                                                                           ");
		sbQuery.append("                                                                                                                                                 ");
		
		sbQuery.append(" SELECT                                                                                                                                          ");
		sbQuery.append("     CO.ID_CORPORATIVE_OPERATION_PK,                                                                                                             ");
		sbQuery.append("     CET.description,                                                                                                                            ");
		sbQuery.append("     'OTROS' DESCRIPTION_SALDO,                                                                                                                  ");
		sbQuery.append("     CPR.ID_CORPORATIVE_PROCESS_RESULT,                                                                                                          ");
		sbQuery.append("     HA.ACCOUNT_NUMBER,		                                                                                                                     ");
		sbQuery.append("     (SELECT LISTAGG(H.ID_HOLDER_PK,'<br>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)                                                             ");
		sbQuery.append("      FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK                                                    ");
		sbQuery.append("      WHERE HAD.ID_HOLDER_ACCOUNT_FK = CPR.ID_HOLDER_ACCOUNT_FK) CUI,                                                                            ");
		sbQuery.append("     (SELECT LISTAGG(H.FULL_NAME,'<br>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)                                                                ");
		sbQuery.append("      FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK                                                    ");
		sbQuery.append("      WHERE HAD.ID_HOLDER_ACCOUNT_FK = CPR.ID_HOLDER_ACCOUNT_FK) NAME_HOLDER,                                                                    ");
		sbQuery.append("     CO.ID_ORIGIN_SECURITY_CODE_FK,                                                                                                              ");
		sbQuery.append("     (CASE CET.ID_CORPORATIVE_EVENT_TYPE_PK                                                                                                      ");
		sbQuery.append("         WHEN 14 THEN PIC.BEGINING_DATE																											 ");
		sbQuery.append("         ELSE PAC.BEGINING_DATE                                                                                                          ");
		sbQuery.append("      END) BEGINING_DATE,                                                                                                                        ");
		sbQuery.append("     (CASE CET.ID_CORPORATIVE_EVENT_TYPE_PK                                                                                                      ");
		sbQuery.append("         WHEN 14 THEN PIC.EXPERITATION_DATE                                                                                                      ");
		sbQuery.append("         ELSE PAC.EXPRITATION_DATE                                                                                                       ");
		sbQuery.append("      END) EXPIRATION_DATE,                                                                                                                      ");
		sbQuery.append("     SCB.OTHER_BLOCK_BALANCE SALDO,                                                                                                              ");
		sbQuery.append("     (CASE CET.ID_CORPORATIVE_EVENT_TYPE_PK                                                                                                      ");
		sbQuery.append("         WHEN 14 THEN PIC.COUPON_AMOUNT                                                                                                          ");
		sbQuery.append("         ELSE PAC.COUPON_AMOUNT                                                                                                          ");
		sbQuery.append("      END) VALOR_FINAL,                                                                                                                          ");
		sbQuery.append("     (CASE                                                                                                                                       ");
		sbQuery.append("         WHEN  CO.CURRENCY IN (1304,1734,1853) THEN (SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = CO.CURRENCY_PAYMENT)     ");
		sbQuery.append("         ELSE (SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = CO.CURRENCY)                                                   ");
		sbQuery.append("     END) CURRENCY,                                                                                                                              ");
		sbQuery.append("     CPR.OTHER_BLOCK_BALANCE MONTO_TOTAL,                                                                                                        ");
		sbQuery.append("     CPR.IND_OTHER_BLOCK_BALANCE INDICADOR                                                                                                      ");
		sbQuery.append(" FROM                                                                                                                                            ");
		sbQuery.append("     corporative_operation CO                                                                                                                    ");
		sbQuery.append("     inner join CORPORATIVE_PROCESS_RESULT CPR   on  CPR.ID_CORPORATIVE_OPERATION_FK = CO.ID_CORPORATIVE_OPERATION_PK                            ");
		sbQuery.append("     inner join corporative_event_type CET       on  CO.CORPORATIVE_EVENT_TYPE = CET.ID_CORPORATIVE_EVENT_TYPE_PK                                ");
		sbQuery.append("     inner join stock_calculation_process SCP    on  SCP.id_stock_calculation_pk = CO.id_stock_calculation                                       ");
		sbQuery.append("     inner join stock_calculation_balance SCB    on  SCB.id_stock_calculation_fk = SCP.id_stock_calculation_pk AND                               ");
		sbQuery.append("                                                     SCB.ID_PARTICIPANT_FK = CPR.ID_PARTICIPANT_FK AND                                           ");
		sbQuery.append("                                                     SCB.ID_HOLDER_ACCOUNT_FK = CPR.ID_HOLDER_ACCOUNT_FK AND                                     ");
		sbQuery.append("                                                     SCB.ID_SECURITY_CODE_FK = CPR.ID_SECURITY_CODE_FK                                           ");
		sbQuery.append("     inner join issuer iss                       on  iss.id_issuer_pk = co.id_issuer_fk                                                          ");
		sbQuery.append("     inner join holder_account ha                on  ha.id_holder_account_pk = CPR.ID_HOLDER_ACCOUNT_FK                                          ");
		sbQuery.append("     inner join security sec                     on  sec.id_security_code_pk = CO.ID_ORIGIN_SECURITY_CODE_FK                                     ");
		sbQuery.append("     left join PROGRAM_INTEREST_COUPON PIC       on  CO.ID_PROGRAM_INTEREST_FK = PIC.ID_PROGRAM_INTEREST_PK                                      ");
		sbQuery.append("     left join program_amortization_coupon PAC   on  CO.ID_PROGRAM_AMORTIZATION_FK = PAC.ID_PROGRAM_AMORTIZATION_PK                              ");
		sbQuery.append(" WHERE 1 = 1 AND                                                                                                                                 ");
		sbQuery.append("     ((CET.ID_CORPORATIVE_EVENT_TYPE_PK = 14 and TRUNC(PIC.EXPERITATION_DATE) = :expirationDate) OR												 ");
		sbQuery.append("         (CET.ID_CORPORATIVE_EVENT_TYPE_PK = 15 and CPR.IND_ORIGIN_TARGET = 2 and TRUNC(PAC.EXPRITATION_DATE) = :expirationDate)) AND			 ");
		sbQuery.append("     SCB.OTHER_BLOCK_BALANCE > 0                                                                                                                 ");
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdIssuerCodePk())){
			sbQuery.append(" and iss.id_issuer_pk = :idIssuerCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdHolderPk())){
			sbQuery.append(" AND (EXISTS																																");
			sbQuery.append(" 	(SELECT had.ID_HOLDER_FK																												");
			sbQuery.append(" 	FROM HOLDER_ACCOUNT_DETAIL had																											");
			sbQuery.append(" 	WHERE had.ID_HOLDER_FK = :cui_code																										");
			sbQuery.append(" 		AND had.ID_HOLDER_ACCOUNT_FK = CPR.ID_HOLDER_ACCOUNT_FK))																			");
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getSecurityCurrency())){
			sbQuery.append(" and CO.CURRENCY= :currency 																												");
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getSecurityClass())){
			sbQuery.append(" and sec.security_class= :securityClass 																									");
		}
		sbQuery.append("                                                                                                                                                ");
		sbQuery.append(" ORDER BY 1,2,3,5,8                                                                                                                             ");

		Query query = em.createNativeQuery(sbQuery.toString());

		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdIssuerCodePk())){
    		query.setParameter("idIssuerCodePk",  securityTO.getIdIssuerCodePk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdHolderPk())){
			query.setParameter("cui_code",  securityTO.getIdHolderPk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getSecurityCurrency())){
			query.setParameter("currency",  securityTO.getSecurityCurrency());
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getSecurityClass())){
			query.setParameter("securityClass",  securityTO.getSecurityClass());
		}
		query.setParameter("expirationDate",  securityTO.getExpirationDate());

		List<Object> objects =(List<Object>) query.getResultList();
		
		if(Validations.validateListIsNotNullAndNotEmpty(objects)){
			if(objects.size() <= 100){
				ExpirationSecurityTO expirationSecurityTO;
				for(int i=0;i<objects.size();++i){
					Object [] object = (Object[]) objects.get(i);
					expirationSecurityTO = new ExpirationSecurityTO();
					expirationSecurityTO.setIdCorporativeProcessResult(Long.valueOf(((BigDecimal) object[3]).longValue()));
					expirationSecurityTO.setDescription((String)object[1]);
					expirationSecurityTO.setAccountNumber(Long.valueOf(((BigDecimal) object[4]).longValue()));
					expirationSecurityTO.setHolder((String)object[5]);
					expirationSecurityTO.setHolderName((String)object[6]);
					expirationSecurityTO.setIdSecurityCodePk((String)object[7]);
					expirationSecurityTO.setIssuanceDate((Date)object[8]);
					expirationSecurityTO.setExpirationDate((Date)object[9]);
					expirationSecurityTO.setBalance((String)object[2]);
					expirationSecurityTO.setQuantity((BigDecimal)object[10]);
					expirationSecurityTO.setFinalSecurity((BigDecimal)object[11]);
					expirationSecurityTO.setCurrencyDescription((String)object[12]);
					expirationSecurityTO.setTotalAmount((BigDecimal)object[13]);
					expirationSecurityTO.setIndicator(Integer.valueOf(((BigDecimal) object[14]).intValue()));
					listExpirationSecurityTO.add(expirationSecurityTO);
				}
			}else{
				throw new ServiceException(ErrorServiceType.ERROR_VALIDATE_MAX_RECORDS);
			}
		}
		return listExpirationSecurityTO;
	}
	
	/**
	 * 	 * Update Security For Expiration By Issuer
	 * @param loggerUser
	 * @param expirationSecTO
	 * @throws ServiceException
	 */
	public void updateSecurityExpiration(LoggerUser loggerUser,ExpirationSecurityTO expirationSecTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update CorporativeProcessResult cpr ");
		if(expirationSecTO.getIndicatorType().equals(GeneralConstants.ONE_VALUE_INTEGER)){
			sbQuery.append(" Set cpr.indAvailable=:statePrm, ");
		}else if(expirationSecTO.getIndicatorType().equals(GeneralConstants.TWO_VALUE_INTEGER)){
			sbQuery.append(" Set cpr.indBanBalance=:statePrm, ");
		}else if(expirationSecTO.getIndicatorType().equals(GeneralConstants.THREE_VALUE_INTEGER)){
			sbQuery.append(" Set cpr.indPawnBalance=:statePrm, ");
		}else if(expirationSecTO.getIndicatorType().equals(GeneralConstants.FOUR_VALUE_INTEGER)){
			sbQuery.append(" Set cpr.indOtherBlockBalance=:statePrm, ");
		}
		sbQuery.append(" cpr.lastModifyDate=:lastModifyDatePrm, ");
		sbQuery.append(" cpr.lastModifyUser=:lastModifyUserPrm, ");
		sbQuery.append(" cpr.lastModifyIp=:lastModifyIpPrm, ");
		sbQuery.append(" cpr.lastModifyApp=:lastModifyAppPrm ");
		sbQuery.append(" where cpr.idCorporativeProcessResult=:idCorporativeProcessResultPrm");
		
		Query query = em.createQuery(sbQuery.toString());
		
		query.setParameter("statePrm",GeneralConstants.ONE_VALUE_INTEGER);
		query.setParameter("lastModifyDatePrm",loggerUser.getAuditTime());
		query.setParameter("lastModifyUserPrm",loggerUser.getUserName());
		query.setParameter("lastModifyIpPrm",loggerUser.getIpAddress());
		query.setParameter("lastModifyAppPrm",loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("idCorporativeProcessResultPrm",expirationSecTO.getIdCorporativeProcessResult());
		
	    query.executeUpdate();
	}
	
	/**
	 * Cambiar de estado al Valor solo si No tiene mas cuentas Matriz
	 * @param listSecurityOverdueTO
	 * @param loggerUser
	 * @throws ServiceException
	 */
	public void changeStateForSecurity(List<SecurityOverdueTO> listSecurityOverdueTO,  LoggerUser loggerUser) throws ServiceException{

		for(SecurityOverdueTO securityOverdue: listSecurityOverdueTO ){
			Long numberAccounts = holderBalanceMovementsServiceBean.getBalanceHolderFromSecurity(securityOverdue.getIdSecurityCodePk(),true);
			if(numberAccounts!=null && numberAccounts.compareTo(GeneralConstants.ZERO_VALUE_LONG)==0 ){
				Security security= find(securityOverdue.getIdSecurityCodePk(), Security.class);
				security.setStateSecurity(SecurityStateType.EXPIRED.getCode());
				em.detach(security);
				securitiesServiceBean.updateStateSecurity(security, loggerUser);
			}
		}
	}
	
	
	/**
	 * 
	 * @param securityTO
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	public List<SecurityConsultTO> findSecuritiesForConsult(SecurityTO securityTO) throws ServiceException{
		
		List<SecurityConsultTO> listSecurityConsultTO=new ArrayList<SecurityConsultTO>(); 
		StringBuilder sbQuery = new StringBuilder();


		sbQuery.append(" SELECT DISTINCT    ");
		sbQuery.append("  SE.idSecurityCodePk, ");//0
		sbQuery.append("  SE.securityDaysTerm, ");//1
		sbQuery.append("  SE.interestRate, ");//2
		sbQuery.append("  SE.currency, ");//3
		sbQuery.append("  SE.currentNominalValue, ");//4
		sbQuery.append("  HA.idHolderAccountPk, ");//5
		sbQuery.append("  SE.securityClass, ");//6
		sbQuery.append("  HAB.totalBalance, ");//7
		sbQuery.append("  HAB.availableBalance ");//8
		
		sbQuery.append(" FROM HolderAccountBalance HAB  ");
		sbQuery.append(" INNER JOIN  HAB.holderAccount HA ");
		sbQuery.append(" INNER JOIN  HAB.security SE ");
		sbQuery.append(" INNER JOIN  HA.holderAccountDetails HAD ");
		sbQuery.append(" INNER JOIN  HAD.holder HO ");
		sbQuery.append(" INNER JOIN  SE.issuer ISS ");
				
		sbQuery.append("  WHERE   1=1  ");

		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdHolderPk() )){
			sbQuery.append(" and HO.idHolderPk= :idHolderPk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuerCodePk()  )){
			sbQuery.append(" and se.issuer.idIssuerPk= :idIssuerCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuanceCodePk() )){
			sbQuery.append(" and se.issuance.idIssuanceCodePk= :idIssuanceCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdSecurityCodePk() )){
			sbQuery.append(" and se.idSecurityCodePk= :idSecurityCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIsinCodePk() )){
			sbQuery.append(" and se.idIsinCode= :idIsinCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityCurrency() )){
			sbQuery.append(" and se.currency= :securityCurrency ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityType() )){
			sbQuery.append(" and se.securityType= :securityType ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityClass())){
			sbQuery.append(" and se.securityClass= :securityClass ");
		}else if(Validations.validateListIsNotNullAndNotEmpty(securityTO.getLstSecurityClass())){
			sbQuery.append(" and se.securityClass in( :lstSecurityClass) ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIssuanceDate() )){
			sbQuery.append(" and se.issuanceDate= :issuanceDate ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getExpirationDate() )){
			sbQuery.append(" and se.expirationDate= :expirationDate ");
		}
		if(Validations.validateIsNotNull(securityTO.getInitialDate())&&Validations.validateIsNotNull(securityTO.getFinalDate())){
			sbQuery.append(" and trunc(se.expirationDate) >= :initialDate ");
			sbQuery.append(" and trunc(se.expirationDate) <= :finalDate ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getAlternativeCode() )){
			sbQuery.append(" and se.alternativeCode= :alternativeCode ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getDescriptionSecurity() )){
			sbQuery.append(" and se.description like :description ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getSecurityState())){
			sbQuery.append(" and se.stateSecurity= :securityState ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdParticipant())){
			sbQuery.append(" and hab.participant.idParticipantPk = :idParticipantPk ");
		}

		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getAmount())){
			sbQuery.append(" and SE.currentNominalValue = :amount ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getRate())){
			sbQuery.append(" and SE.interestRate = :rateSecurity ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getTerm())){
			sbQuery.append(" and SE.securityDaysTerm = :termSecurity ");
		}
		
    	Query query = em.createQuery(sbQuery.toString());

		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdHolderPk() )){
			query.setParameter("idHolderPk",  securityTO.getIdHolderPk()  );
		}
    	if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuerCodePk()  )){
    		query.setParameter("idIssuerCodePk",  securityTO.getIdIssuerCodePk()  );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuanceCodePk() )){
			query.setParameter("idIssuanceCodePk",  securityTO.getIdIssuanceCodePk() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdSecurityCodePk() )){
			query.setParameter("idSecurityCodePk",  securityTO.getIdSecurityCodePk() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIsinCodePk() )){
			query.setParameter("idIsinCodePk",  securityTO.getIdIsinCodePk() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityCurrency() )){
			query.setParameter("securityCurrency",  securityTO.getSecurityCurrency() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityType() )){
			query.setParameter("securityType",  securityTO.getSecurityType() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityClass() )){
			query.setParameter("securityClass",  securityTO.getSecurityClass() );
		}else if(Validations.validateListIsNotNullAndNotEmpty(securityTO.getLstSecurityClass())){
			query.setParameter("lstSecurityClass",  securityTO.getLstSecurityClass() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIssuanceDate() )){
			query.setParameter("issuanceDate",  securityTO.getIssuanceDate() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getExpirationDate() )){
			query.setParameter("expirationDate",  securityTO.getExpirationDate() );
		}
		if(Validations.validateIsNotNull(securityTO.getInitialDate())&&Validations.validateIsNotNull(securityTO.getFinalDate())){
			query.setParameter("initialDate",  securityTO.getInitialDate());
			query.setParameter("finalDate",  securityTO.getFinalDate() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getAlternativeCode() )){
			query.setParameter("alternativeCode",  securityTO.getAlternativeCode() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getDescriptionSecurity() )){
			query.setParameter("description",  new StringBuilder()
												.append(GeneralConstants.PERCENTAGE_CHAR)
												.append(securityTO.getDescriptionSecurity())
												.append(GeneralConstants.PERCENTAGE_CHAR).toString())  ;
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getSecurityState())){
			query.setParameter("securityState", Integer.parseInt(securityTO.getSecurityState()));
		}

		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdParticipant())){
			query.setParameter("idParticipantPk",  securityTO.getIdParticipant() );
		}
		
		/////////////////////////////////////////////////////////////////////////////////////////////////
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getAmount())){
			query.setParameter("amount",  securityTO.getAmount() );
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getRate())){
			query.setParameter("rateSecurity",  securityTO.getRate() );
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getTerm())){
			query.setParameter("termSecurity",  securityTO.getTerm() );
		}


		List<Object[]> lstResult = (List<Object[]>)query.getResultList();


		
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
			SecurityConsultTO securityConsultTO;
			for (Object[] objMov : lstResult) {

				securityConsultTO = new SecurityConsultTO();

				securityConsultTO.setIdSecurityCodePk(objMov[0].toString());
				securityConsultTO.setSecurityDaysTerm((Integer) objMov[1]);
				securityConsultTO.setInterestRate((BigDecimal) objMov[2]);
				securityConsultTO.setCurrency((Integer) objMov[3]);
				securityConsultTO.setNominalValue((BigDecimal) objMov[4]);
				securityConsultTO.setSecurityClass((Integer) objMov[6]);
//				securityConsultTO.setAlternateCode(objMov[5].toString());
//				securityConsultTO.setDescriptionHolder(accountQueryServiceBean.getHolderAccountDetailDescription((Long) objMov[4]));
				securityConsultTO.setAvailableBalance(Integer.parseInt(objMov[7].toString()));

				
				listSecurityConsultTO.add(securityConsultTO);
			}
		}
		return listSecurityConsultTO;
		

	}
	
	/**
	 * Metodo para saber el monto de la emision issue 578
	 * @param idIssuancePk
	 * @return
	 */
	public BigDecimal getIssuanceAmount(String idIssuancePk){
		
		StringBuilder querySql = new StringBuilder();
		
		querySql.append("  SELECT ISS.ISSUANCE_AMOUNT                       ");
		querySql.append("  FROM ISSUANCE ISS                                ");
		querySql.append("  WHERE ISS.ID_ISSUANCE_CODE_PK = :idIssuancePk    "); //REGISTRADO
		
		Query query = em.createNativeQuery(querySql.toString());
		query.setParameter("idIssuancePk", idIssuancePk);
		return (BigDecimal)query.getSingleResult();
	}
	
	/**
	 * @throws ServiceException 
	 * 
	 */	
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=180)
	public void updateSIRTEXnegotiation(LoggerUser loggerUser, Security security, String enable) throws ServiceException {
		SecurityNegotiationMechanism secNegotiationMechanism = null;
		List <SecurityNegotiationMechanism> secNegotiationMechanismList = null;
		if (enable == "1") {
			//habilitar
			for (int i=1; i<=3; i++) {
				secNegotiationMechanism = new SecurityNegotiationMechanism();
				secNegotiationMechanism.setSecurity(security);
				secNegotiationMechanism.setSecurityNegotationState(BooleanType.YES.getCode());
				switch (i) {
				case 1: //compra venta RF
//					secNegotiationMechanism.setMechanismModality(new MechanismModality(new MechanismModalityPK(GeneralConstants.FIVE_VALUE_LONG ,GeneralConstants.ONE_VALUE_LONG )));
//					security.getSecurityNegotiationMechanisms().add(secNegotiationMechanism);
					break;
				case 2: //reporto RF
					secNegotiationMechanism.setMechanismModality(new MechanismModality(new MechanismModalityPK(GeneralConstants.FIVE_VALUE_LONG ,Long.parseLong("3"))));
					security.getSecurityNegotiationMechanisms().add(secNegotiationMechanism);
					break;
				case 3: //reporto reverso RF
//					secNegotiationMechanism.setMechanismModality(new MechanismModality(new MechanismModalityPK(GeneralConstants.FIVE_VALUE_LONG ,Long.parseLong("22"))));
//					security.getSecurityNegotiationMechanisms().add(secNegotiationMechanism);
					break;
				default:
					break;
				}								
			}
		} else {
			//deshabilitar	
			secNegotiationMechanismList = security.getSecurityNegotiationMechanisms();
			for (SecurityNegotiationMechanism secNegotiationMech : secNegotiationMechanismList) {
				if(secNegotiationMech.getMechanismModality().getIdConverted().equals("5-1")) {
//					secNegotiationMech.setSecurityNegotationState(BooleanType.NO.getCode()); //compra venta RF
				}else
				if(secNegotiationMech.getMechanismModality().getIdConverted().equals("5-3")) {
					secNegotiationMech.setSecurityNegotationState(BooleanType.NO.getCode()); //reporto RF
				}else
				if(secNegotiationMech.getMechanismModality().getIdConverted().equals("5-22")) {
//					secNegotiationMech.setSecurityNegotationState(BooleanType.NO.getCode()); //reporto reverso RF
				}
			}			
		}
		
		if (security.isVariableInstrumentTypeAcc() || (security.isFixedInstrumentType() && security.isPhysicalIssuanceForm())) {
			security.setAmortizationPaymentSchedule(null);
			security.setInterestPaymentSchedule(null);
		}
		if(security.isCeroInterestType()){
			security.setInterestPaymentSchedule(null);
		}
		
		//actualizacion valor
		security = update(security);	
	}	
	
	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	public List<Object> findSirtexNegotiation(String idIssuerPk,Integer securityClass,Integer currency,Integer personType,String withCoupons, String issueanceDate) {
		
		StringBuilder querySql = new StringBuilder();
		
		querySql.append(" SELECT DISTINCT SEC.ID_SECURITY_CODE_PK ");		
		querySql.append(" FROM HOLDER_ACCOUNT_BALANCE HMB ");
		querySql.append(" INNER JOIN SECURITY SEC ON SEC.ID_SECURITY_CODE_PK = HMB.ID_SECURITY_CODE_PK ");
		querySql.append(" INNER JOIN SECURITY_NEGOTIATION_MECHANISM SNM ON SNM.ID_SECURITY_CODE_FK = SEC.ID_SECURITY_CODE_PK ");
		querySql.append(" INNER JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK = HMB.ID_HOLDER_ACCOUNT_PK ");
		querySql.append(" INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ");
		querySql.append(" INNER JOIN HOLDER HO ON HO.ID_HOLDER_PK = HAD.ID_HOLDER_FK ");
		querySql.append(" WHERE  1 = 1  ");		
		querySql.append(" AND SEC.NOT_TRADED = 0  					");//negociable
		querySql.append(" AND HMB.TOTAL_BALANCE > 0 				");//con saldo
		querySql.append(" AND SNM.ID_NEGOTIATION_MECHANISM_FK <> 5	");//sirtex deshabilitado
		querySql.append(" AND SEC.STATE_SECURITY = 131 				");//estado registrado	
		querySql.append(" AND HA.ACCOUNT_GROUP = 462 				");//inversionista
		
		/**Filtros de modificacion*/
		querySql.append(" AND SEC.ID_ISSUER_FK = :idIssuerPk ");
		/**clase de valor*/
		if(securityClass!=null)
			querySql.append(" AND SEC.SECURITY_CLASS = :securityClass ");
		/**moneda*/
		if(currency!=null)
			querySql.append(" AND SEC.CURRENCY = :currency ");
		/**tipo de persona*/
		if(personType!=null)
			querySql.append(" AND HO.HOLDER_TYPE = :personType ");
		/**Con cupones y sin cupones*/
		if(withCoupons!=null){
			Integer with = Integer.parseInt(withCoupons);
			if(with.equals(BooleanType.YES.getCode()))
				querySql.append(" AND SEC.NUMBER_COUPONS > 1 ");
			else
				querySql.append(" AND SEC.NUMBER_COUPONS = 1 ");
		}
		
		/*fecha de emision*/
		querySql.append("  AND TRUNC(SEC.ISSUANCE_DATE) <= TO_DATE('"+issueanceDate+"','dd/MM/yyyy') ");
		
		Query query = em.createNativeQuery(querySql.toString());
		query.setParameter("idIssuerPk", idIssuerPk);
		if(securityClass!=null) query.setParameter("securityClass", securityClass);
		if(currency!=null) query.setParameter("currency", currency);
		if(personType!=null) query.setParameter("personType", personType);
		
		return  ((List<Object>)query.getResultList());
	}	
	
	public Security findSecurityFromSerie(String securitySerial){
        StringBuilder sbQuery = new StringBuilder();
        sbQuery.append(" Select sec from Security sec where sec.idSecurityCodePk = :idSecurityCode ");        
        Query query = em.createQuery(sbQuery.toString());
        query.setParameter("idSecurityCode", securitySerial);       
        try {
               return (Security) query.getSingleResult();
        } catch (NoResultException e) {
               return null;
        }
	}

	@SuppressWarnings("unchecked")
	public List<Object> findSirtexLiveOperations(String idIssuerPk) {		
		StringBuilder sbQuery = new StringBuilder();
		//operaciones vivas sirtex
		sbQuery.append(" SELECT DISTINCT SEC.ID_SECURITY_CODE_PK       	");
		sbQuery.append(" FROM HOLDER_MARKETFACT_BALANCE HMB				");
		sbQuery.append(" INNER JOIN SECURITY SEC ON SEC.ID_SECURITY_CODE_PK = HMB.ID_SECURITY_CODE_FK 	");	
		sbQuery.append(" INNER JOIN SECURITY_NEGOTIATION_MECHANISM SNM ON SNM.ID_SECURITY_CODE_FK = SEC.ID_SECURITY_CODE_PK 	");	
		sbQuery.append(" WHERE  1=1 									");
		sbQuery.append(" 	AND HMB.IND_ACTIVE = 1 						");//negociable
		sbQuery.append(" 	AND HMB.TOTAL_BALANCE > 0 					");//con saldo
		sbQuery.append(" 	AND SNM.ID_NEGOTIATION_MECHANISM_FK = 5		");//sirtex habilitado
		sbQuery.append(" 	AND SNM.ID_NEGOTIATION_MODALITY_FK in (1,3,22)	");//compra-venta/reporto/reporto reverso
		sbQuery.append(" 	AND SEC.SECURITY_CLASS = 420			 	");//solo DPFs
		sbQuery.append(" 	AND SEC.ID_ISSUER_FK = :idIssuerPk		 	");
		Query query = em.createNativeQuery(sbQuery.toString());	
		query.setParameter("idIssuerPk", idIssuerPk);		
		
		return  ((List<Object>)query.getResultList());
	}
	
	/**
	 * Find securities lite service bean.
	 *
	 * @param securityTO the security to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Security> findSecuritiesForShare(SecurityTO securityTO, String subClass) throws ServiceException{
		

		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" select  se ").
				append("  From Security se   ").
				append("  ");//19

		sbQuery.append("  WHERE   1=1 ");
		
		
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuerCodePk()  )){
			sbQuery.append(" and se.issuer.idIssuerPk= :idIssuerCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuanceCodePk() )){
			sbQuery.append(" and se.issuance.idIssuanceCodePk= :idIssuanceCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdSecurityCodePk() )){
			sbQuery.append(" and se.idSecurityCodePk like :idSecurityCodePk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityCurrency() )){
			sbQuery.append(" and se.currency= :securityCurrency ");
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getInstrumentType() )){
			sbQuery.append(" and se.instrumentType= :instrumentType ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getSecurityState())){
			sbQuery.append(" and se.stateSecurity= :securityState ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(subClass)){
			sbQuery.append(" and se.subClass= :subClass ");
		}
		
		sbQuery.append(" order by se.idSecurityCodePk asc ");
		
    	Query query = em.createQuery(sbQuery.toString());
    	

    	if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuerCodePk()  )){
    		query.setParameter("idIssuerCodePk",  securityTO.getIdIssuerCodePk()  );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuanceCodePk() )){
			query.setParameter("idIssuanceCodePk",  securityTO.getIdIssuanceCodePk() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdSecurityCodePk() )){
			query.setParameter("idSecurityCodePk",  GeneralConstants.PERCENTAGE_STRING +  securityTO.getIdSecurityCodePk()+ GeneralConstants.PERCENTAGE_STRING  );
		}
		
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getSecurityCurrency() )){
			query.setParameter("securityCurrency",  securityTO.getSecurityCurrency() );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getInstrumentType())){
			query.setParameter("instrumentType",  securityTO.getInstrumentType() );
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getSecurityState())){
			query.setParameter("securityState", Integer.parseInt(securityTO.getSecurityState()));
		}
		if(Validations.validateIsNotNullAndNotEmpty(subClass)){
			query.setParameter("subClass", subClass);
		}
		
		
    	return  ((List<Security>)query.getResultList());

	}
	
	public Boolean validateCountryInIsin(Security sec) {
		if(Validations.validateIsNotNullAndNotEmpty(sec.getIdSecurityCodeOnly())) {
			if(Validations.validateIsNotNull(sec.getEncoderAgent()) && !sec.getEncoderAgent().equals(EncoderAgentType.CAVAPY.getCode())) {
				if(Validations.validateIsNotNull(sec.getIssuance())) {
					Map<String, Object> params=new HashMap<String, Object>();
					params.put("codeGeographicLocPrm", sec.getIdSecurityCodeOnly().substring(0,2));					
					params.put("typeGeographicLocPrm", GeographicLocationType.COUNTRY.getCode());
					GeographicLocation geographicLocation = findObjectByNamedQuery(GeographicLocation.FIND_BY_CODE_GEO_LOC, params);
					if(Validations.validateIsNull(geographicLocation)) {
						return Boolean.FALSE;
					} else if(!geographicLocation.getIdGeographicLocationPk().equals(sec.getIssuance().getGeographicLocation().getIdGeographicLocationPk())){
						return Boolean.FALSE;			
					}
				}
			}
		}
		
		return Boolean.TRUE;
	}
	
}