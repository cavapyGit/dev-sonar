package com.pradera.securities.issuancesecuritie.view;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Enum SecurityMgmtType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public enum SecurityMgmtType {
	
	/** The frm motive. */
	FRM_MOTIVE("frmMotive",null),
	
	/** The txt security code. */
	TXT_SECURITY_CODE("frmSecurityMgmt:tviewGeneral:txtSecurityCode",null),
	
	/** The txt amortization factor. */
	TXT_AMORTIZATION_FACTOR("frmSecurityMgmt:tviewGeneral:txtAmortizationFactor",null),
	
	/** The txt months terms. */
	TXT_MONTHS_TERMS("frmSecurityMgmt:tviewGeneral:txtMonthsTerms",null),
	
	/** The txt issue expiration date. */
	TXT_ISSUE_EXPIRATION_DATE("frmIssueMgmt:calIssueExpirationDate",null),
	
	/** The txt issuance term. */
	TXT_ISSUANCE_TERM("frmIssueMgmt:txtIssuanceTerm",null),
	
	/** The txt days terms. */
	TXT_DAYS_TERMS("frmSecurityMgmt:tviewGeneral:txtDaysTerms",null),
	
	/** The txt payments number. */
	TXT_PAYMENTS_NUMBER("frmSecurityMgmt:tviewGeneral:txtPaymentsNumber",null),
	
	/** The txt amortization factor amort. */
	TXT_AMORTIZATION_FACTOR_AMORT("frmSecurityMgmt:tviewGeneral:txtAmortizationFactorAmort",null),
	
	/** The txt minium rate. */
	TXT_MINIUM_RATE("frmSecurityMgmt:tviewGeneral:txtMiniumRate",null),
	
	/** The txt maxium rate. */
	TXT_MAXIUM_RATE("frmSecurityMgmt:tviewGeneral:txtMaxiumRate",null),
	
	/** The txt interest rate. */
	TXT_INTEREST_RATE("frmSecurityMgmt:tviewGeneral:txtInterestRate",null),
	
	/** The txt periodicity days. */
	TXT_PERIODICITY_DAYS("frmSecurityMgmt:tviewGeneral:txtPeriodicityDays",null),
	
	/** The txt amort periodicity days. */
	TXT_AMORT_PERIODICITY_DAYS("frmSecurityMgmt:tviewGeneral:txtAmortPeriodicityDays",null),
	
	/** The txt nro coupons. */
	TXT_NRO_COUPONS("frmSecurityMgmt:tviewGeneral:txtNroCoupons",null),
	
	/** The txt spread escalon. */
	TXT_SPREAD_ESCALON("frmSecurityMgmt:tviewGeneral:txtSpreadEscalon",null),
	
	/** The cbo amortization type. */
	CBO_AMORTIZATION_TYPE("frmSecurityMgmt:tviewGeneral:cboAmortizationType",null),
	
	/** The cbo per amortization. */
	CBO_PER_AMORTIZATION("frmSecurityMgmt:tviewGeneral:cboPerAmortization",null),
	
	/** The cbo amortization on. */
	CBO_AMORTIZATION_ON("frmSecurityMgmt:tviewGeneral:cboAmortizationOn",null),
	
	/** The cal expired date. */
	CAL_EXPIRED_DATE("frmSecurityMgmt:tviewGeneral:calExpiredDate",null),
	
	/** The cal first pay coupon. */
	CAL_FIRST_PAY_COUPON("frmSecurityMgmt:tviewGeneral:calFirstPayCoupon",null),
	
	/** The cal first exp amo coupon. */
	CAL_FIRST_EXP_AMO_COUPON("frmSecurityMgmt:tviewGeneral:calFirstExpAmoCoupon",null),
	
	/** The cbo calendar day. */
	CBO_CALENDAR_DAY("frmSecurityMgmt:tviewGeneral:cboCalendarDay",null),
	
	/** The cbo calendar type. */
	CBO_CALENDAR_TYPE("frmSecurityMgmt:tviewGeneral:cboCalendarType",null),
	
	/** The cbo rate type. */
	CBO_RATE_TYPE("frmSecurityMgmt:tviewGeneral:cboRateType",null),
	
	/** The cbo security term. */
	CBO_SECURITY_TERM("frmSecurityMgmt:tviewGeneral:cboSecurityTerm",null),
	
	/** The cbo int payment mode. */
	CBO_INT_PAYMENT_MODE("frmSecurityMgmt:tviewGeneral:cboIntPaymentMode",null),
	
	/** The cbo interest periodicity. */
	CBO_INTEREST_PERIODICITY("frmSecurityMgmt:tviewGeneral:cboInterestPayPeriodicity",null),
	
	/** The cbo calendar month. */
	CBO_CALENDAR_MONTH("frmSecurityMgmt:tviewGeneral:cboCalendarMonth",null),
	
	/** The cbo amort payment mode. */
	CBO_AMORT_PAYMENT_MODE("frmSecurityMgmt:tviewGeneral:cboAmortPaymentMode",null),
	
	/** The tab valuator history. */
	TAB_VALUATOR_HISTORY("frmSecurityMgmt:tviewGeneral:tabValuatorHistory",null);
	
	/** The value. */
	private String value;
	
	/** The label. */
	private String label;

	/**
	 * Instantiates a new security mgmt type.
	 *
	 * @param value the value
	 * @param label the label
	 */
	private SecurityMgmtType(String value, String label){
		this.value=value;
		this.label=label;
	}
	
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue(){
		return this.value;
	}
	
	/**
	 * Gets the label.
	 *
	 * @return the label
	 */
	public String getLabel(){
		return this.label;
	}
}
