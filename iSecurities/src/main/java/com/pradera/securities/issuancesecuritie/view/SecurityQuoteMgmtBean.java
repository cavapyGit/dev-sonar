package com.pradera.securities.issuancesecuritie.view;


import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.IssuerSearcherTO;
import com.pradera.core.component.helperui.view.IssuerHelperBean;
import com.pradera.core.component.helperui.view.SecuritiesHelperBean;
import com.pradera.core.component.helperui.view.SecurityHelpBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.SecurityQuote;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.SecurityQuoteStateType;
import com.pradera.securities.issuancesecuritie.facade.IssuanceSecuritiesServiceFacade;
import com.pradera.securities.issuancesecuritie.facade.SecurityQuoteServiceFacade;
import com.pradera.securities.issuancesecuritie.to.SecurityQuoteTO;
import com.pradera.securities.issuancesecuritie.util.PropertiesConstants;
import com.pradera.securities.issuer.to.IssuerTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SecurityQuoteMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class SecurityQuoteMgmtBean extends GenericBaseBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The security quote session. */
	private SecurityQuote securityQuoteSession;
	
	/** The country residence. */
	@Inject @Configurable Integer countryResidence;
	
	/** The general parameter facade. */
	@EJB GeneralParametersFacade generalParameterFacade;
	
	/** The security quote facade. */
	@EJB SecurityQuoteServiceFacade securityQuoteFacade;

	/** The security quote states. */
	private List<ParameterTable> securityQuoteStates;

	/** The currency description. */
	private Map<Integer,String> currencyDescription = new HashMap<Integer,String>();
	
	/** The request type description. */
	private Map<Integer,String> requestTypeDescription = new HashMap<Integer,String>();
	
	/** The lst securities. */
	private List<Security> lstSecurities = new ArrayList<Security>();
	
	/** The lst all securities. */
	private List<Security> lstAllSecurities = new ArrayList<Security>();
	
	/** The securitie description. */
	private Map<String,String> securitieDescription = new HashMap<String,String>();
	
	/** The lst tipo solicitud. */
	private List<ParameterTable> lstTipoSolicitud;
	
	/** The security quote to. */
	private SecurityQuoteTO securityQuoteTO;
	
	/** The security quote to selected. */
	private SecurityQuoteTO securityQuoteTOSelected;
	
	/** The issu securities facade. */
	@EJB private IssuanceSecuritiesServiceFacade issuSecuritiesFacade;
	
	/** The user info. */
	@Inject private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject UserPrivilege userPrivilege;	
	
	/** The helper component facade. */
	@EJB HelperComponentFacade helperComponentFacade;
	
	/** The min date register. */
	private Date minDateRegister;
	
	/** The security quote list. */
	private GenericDataModel<SecurityQuoteTO> securityQuoteList;
	
	/** The security help bean. */
	@Inject SecurityHelpBean securityHelpBean;
	
	/** The issuer helper bean. */
	@Inject IssuerHelperBean issuerHelperBean;
	
	/** The securities helper bean. */
	@Inject SecuritiesHelperBean securitiesHelperBean;
	
	/** The new share capital. */
	private BigDecimal newShareCapital;

	/** The variation share capital. */
	private BigDecimal variationShareCapital;
	
	/** The radio selected. */
	private Integer radioSelected=1;
	
	/** The actualizar. */
	private Integer actualizar=1671;
	
	/** The modificar. */
	private Integer modificar=1672;
	
	/** The flag search. */
	/*security*/
	private boolean flagSearch = false;
	
	/** The flag register. */
	private boolean flagRegister = false;
	
	/** The is issuer. */
	private boolean isIssuer = false;
	
	/** The is cevaldom. */
	private boolean isCevaldom = false;
	
	/** The issuer login. */
	private com.pradera.core.component.helperui.to.IssuerTO issuerLogin = new com.pradera.core.component.helperui.to.IssuerTO();
	
	/** The hora inicio. */
	private String horaInicio="09:00:00";
	
	/** The hora fin. */
	private String horaFin="21:30:00";
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		try {
			loadSecurityQuoteState();
			loadSecurityQuoteTO();
			loadUserPrivileges();
			securityEvents();
			loadHolidays();
			validateHours();
			loadCurrencyDescription();
			loadTipoSolicitud();
			loadSecurities();
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Radio select event.
	 */
	public void radioSelectEvent(){

		securityQuoteTO = new SecurityQuoteTO();
		securityQuoteTO.setIssuer(new Issuer());
		securityQuoteTO.setSecurity(new Security());
		securityQuoteTO.setFinalDate(CommonsUtilities.currentDate());
		securityQuoteTO.setInitialDate(CommonsUtilities.currentDate());
		securityEvents();
	}
	
	
	/**
	 * Current time in24.
	 *
	 * @return the string
	 */
	public String currentTimeIn24(){
		String currentTime = StringUtils.EMPTY;
		Calendar calendario = new GregorianCalendar();
		SimpleDateFormat dt = new SimpleDateFormat("HH:mm:ss");
		currentTime = dt.format(calendario.getTime());
		
		return currentTime;
	}
	
	/**
	 * Validate hours.
	 *
	 * @throws ServiceException the service exception
	 */
	private void validateHours() throws ServiceException{
		try {
		
			Date currentDate = CommonsUtilities.currentDate();
			Date currentDateTime = null;
			Date initDateTime = null;
			Date finDateTime = null;
	
			String strCurrentTime = currentTimeIn24();
			String strCurrentDate = CommonsUtilities.convertDateToString(currentDate,"yyyy-mm-dd"); 
			SimpleDateFormat dt = new SimpleDateFormat("yyyy-mm-dd HH:mm");
			
			currentDateTime = dt.parse(strCurrentDate+" "+strCurrentTime);
			initDateTime = dt.parse(strCurrentDate+" "+horaInicio);
			finDateTime = dt.parse(strCurrentDate+" "+horaFin);
			
			if( finDateTime.compareTo(currentDateTime) < 0 ||  
				currentDateTime.compareTo(initDateTime) < 0 ){
				
				throw new ServiceException(ErrorServiceType.HOURS_NOT_VALID,new Object[]{horaInicio,horaFin});
				
			}
		
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Load securities.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadSecurities() throws ServiceException{
		//lstSecurities = 
		lstSecurities = securityQuoteFacade.getSecurityListByParameters(securityQuoteTO);
		
		
		for (int i = 0; i < lstSecurities.size(); i++) {
			lstSecurities.get(i).setCurrencyName(currencyDescription.get(lstSecurities.get(i).getCurrency()));
			lstAllSecurities.add(lstSecurities.get(i));
			securitieDescription.put(lstSecurities.get(i).getIdIsinCode(), lstSecurities.get(i).getDescription());
		}
	}
	
	/**
	 * Security search selected event.
	 */
	public void securitySearchSelectedEvent(){
		
		for(Security sec : lstSecurities){
			if( sec.getIdIsinCode().equals(securityQuoteTO.getSecurity().getIdIsinCode()) ){
				securityQuoteTO.setSecurity(sec);
			}
		}
		
	}
	
	/**
	 * Security events.
	 */
	private void securityEvents(){
		try {
			
			if(userInfo.getUserAcctions().isSearch())
				flagSearch=true;
	
			if(userInfo.getUserAcctions().isRegister())
				flagRegister=true;
			
			if(userInfo.getUserAccountSession().isIssuerInstitucion() ){
				
				//filtros para la busqueda
				IssuerSearcherTO issuerSearch = new IssuerSearcherTO();
				issuerSearch.setIssuerCode(userInfo.getUserAccountSession().getIssuerCode());
				
				//ejecuto la busqueda del helper
				issuerLogin = helperComponentFacade.findIssuerByCode(issuerSearch);
				
				//setteo datos a mi issuer temporal
				Issuer issuerTmp = new Issuer();
				issuerTmp.setIdIssuerPk(issuerLogin.getIssuerCode());
				issuerTmp.setMnemonic(issuerLogin.getMnemonic());
				issuerTmp.setDescription(issuerLogin.getIssuerDesc());
				issuerTmp.setBusinessName(issuerLogin.getIssuerBusinessName());
				issuerTmp.setStateIssuer(issuerLogin.getIssuerState());
				
				//setteo datos al helper (Vista)
//				issuerHelperBean.setIssuerMnemonic(issuerLogin.getMnemonic());
//				issuerHelperBean.setIssuerDescription(issuerLogin.getIssuerDesc());
//				issuerHelperBean.setIssuerBusinessName(issuerLogin.getIssuerBusinessName());
				
				//setteo los datos
				securityQuoteTO.setIssuer(issuerTmp);
				securityQuoteSession.setIssuer(issuerTmp);
				
				if( issuerLogin.getIssuerState().equals(IssuerStateType.BLOCKED.getCode()) ){
					flagRegister=false;
					disabledUserPrivileges();
				}
				
				isIssuer = true;
			}else{
				isCevaldom = true;
			}
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Load holidays.
	 */
	private void loadHolidays() {
		// TODO Auto-generated method stub
		Holiday holidayFilter = new Holiday();
		holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		GeographicLocation countryFilter = new GeographicLocation();
		countryFilter.setIdGeographicLocationPk(countryResidence);
		holidayFilter.setGeographicLocation(countryFilter);
		try {
			allHolidays = CommonsUtilities.convertListDateToString(generalParameterFacade.getListHolidayDateServiceFacade(holidayFilter));
		}catch (ServiceException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Load user privileges.
	 */
	private void loadUserPrivileges() {
		// TODO Auto-generated method stub
		if(userInfo.getUserAccountSession().isIssuerInstitucion()){
			minDateRegister = CommonsUtilities.currentDate();
		}else if(userInfo.getUserAccountSession().isDepositaryInstitution()){
			
		}
		PrivilegeComponent component = new PrivilegeComponent();
		component.setBtnRegisterView(Boolean.TRUE);
		component.setBtnConfirmView(Boolean.TRUE);
		component.setBtnAnnularView(Boolean.TRUE);
		//component.setBtnModifyView(Boolean.TRUE);
		userPrivilege.setPrivilegeComponent(component);
	}
	
	/**
	 * Disabled user privileges.
	 */
	private void disabledUserPrivileges() {
		// TODO Auto-generated method stub
		if(userInfo.getUserAccountSession().isIssuerInstitucion()){
			minDateRegister = CommonsUtilities.currentDate();
		}else if(userInfo.getUserAccountSession().isDepositaryInstitution()){
			
		}
		PrivilegeComponent component = new PrivilegeComponent();
		component.setBtnRegisterView(Boolean.FALSE);
		component.setBtnConfirmView(Boolean.FALSE);
		component.setBtnAnnularView(Boolean.FALSE);
		userPrivilege.setPrivilegeComponent(component);
	}

	/**
	 * Load security quote state.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadSecurityQuoteState()  throws ServiceException{
		// TODO Auto-generated method stub
		ParameterTableTO parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.SECURITY_QUOTE_STATES.getCode());
		securityQuoteStates = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}
	
	/**
	 * Load tipo solicitud.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadTipoSolicitud()  throws ServiceException{
		// TODO Auto-generated method stub
		ParameterTableTO parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.TIPO_SOLICITUD_SERCURITY_QUOTE.getCode());
		lstTipoSolicitud = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		
		for (ParameterTable parameter: lstTipoSolicitud) {
			requestTypeDescription.put(parameter.getParameterTablePk(), parameter.getParameterName());
		}
		
	}
	
	
	/**
	 * Load currency description.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCurrencyDescription()  throws ServiceException{
		// TODO Auto-generated method stub
		ParameterTableTO parameterTableTO=new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		List<ParameterTable> lstCurrency = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		
		for (ParameterTable parameter: lstCurrency) {
			currencyDescription.put(parameter.getParameterTablePk(), parameter.getParameterName());
		}
		
	}

	/**
	 * Load security quote to.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadSecurityQuoteTO() throws ServiceException{
		// TODO Auto-generated method stub
		securityQuoteTO = new SecurityQuoteTO();
		securityQuoteTO.setIssuer(new Issuer());
		securityQuoteTO.setSecurity(new Security());
		securityQuoteTO.setInitialDate(CommonsUtilities.currentDate());
		securityQuoteTO.setFinalDate(CommonsUtilities.currentDate());
		
		securityQuoteSession = new SecurityQuote();
		securityQuoteSession.setRegistryDate(CommonsUtilities.currentDate());
	}
	
	/**
	 * Validate issuer search.
	 */
	public void validateIssuerSearch(){
		if(securityQuoteTO.getIssuer()!=null && securityQuoteTO.getIssuer().getIdIssuerPk()!=null && !securityQuoteTO.getIssuer().getIdIssuerPk().isEmpty()){
			
			//Que solo aparesca en el combo los valores del emisor seleccionado
			lstSecurities = new ArrayList<Security>();
			for(Security sec : lstAllSecurities){
				if( sec.getIssuer().getIdIssuerPk().equals(securityQuoteTO.getIssuer().getIdIssuerPk()) ){
					lstSecurities.add(sec);
					securityQuoteTO.setSecurity(new Security());
				}
			}
			
			if(lstSecurities.size()<=0){
				Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();	
				String headerMessageConfirmation = PropertiesUtilities.getMessage(locale, PropertiesConstants.LBL_HEADER_ALERT);
				String bodyMessageConfirmation = PropertiesUtilities.getMessage(locale, "security.quote.issuer.security",
																				new Object[]{securityQuoteTO.getIssuer().getIdIssuerPk()});
				showMessageOnDialog(headerMessageConfirmation,bodyMessageConfirmation);
				//No tiene valores el emisor, enviar alerta
				alert(headerMessageConfirmation,bodyMessageConfirmation);
				
				//Que solo aparesca en el combo los valores del emisor seleccionado
				lstSecurities = new ArrayList<Security>();
				for(Security sec : lstAllSecurities){
					lstSecurities.add(sec);
				}
				securityQuoteTO.setSecurity(new Security());
				securityQuoteTO.setIssuer(new Issuer());
			}
			
		}else{
			lstSecurities = new ArrayList<Security>();
			for(Security securiti : lstAllSecurities){
				lstSecurities.add(securiti);
			}
		}
	}
	
	/**
	 * Validate security search.
	 */
	public void validateSecuritySearch(){
		SecurityQuote securityQuote = new SecurityQuote();
			try {
				if(securityQuoteTO.getSecurity()!=null && securityQuoteTO.getSecurity().getIdIsinCode()!=null && !securityQuoteTO.getSecurity().getIdIsinCode().isEmpty()){
					securityQuoteTO.setSecurity(issuSecuritiesFacade.getSecurityFromIsinCode(securityQuoteTO.getSecurity().getIdIsinCode()));
					securityQuote.setSecurity(securityQuoteTO.getSecurity());
					securityQuoteFacade.validateSecurityFacade(securityQuote);
				}
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				if(e.getErrorService()!=null){
					showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getMessage());
					JSFUtilities.executeJavascriptFunction("cnfwMsgCustomValidationSec.show()");
					securityQuoteTO.setSecurity(new Security());
				}
				e.printStackTrace();
			}
	}
	
	/**
	 * Validate issuer handler.
	 */
	public void validateIssuerHandler(){
		try {
			removeAlertClear();
			if(Validations.validateIsNotNullAndNotEmpty(securityQuoteSession.getIssuer().getIdIssuerPk()) ){
				IssuerTO issuerTO = new IssuerTO();
				issuerTO.setIdIssuerPk(securityQuoteSession.getIssuer().getIdIssuerPk());
				securityQuoteSession.setIssuer(issuSecuritiesFacade.findIssuerServiceFacade(issuerTO));
				
				//Que solo aparesca en el combo los valores del emisor seleccionado
				lstSecurities = new ArrayList<Security>();
				for(Security sec : lstAllSecurities){
					if( sec.getIssuer().getIdIssuerPk().equals(securityQuoteSession.getIssuer().getIdIssuerPk()) ){
						lstSecurities.add(sec);
						securityQuoteSession.setSecurity(new Security());
					}
				}

				securityQuoteFacade.validateIssuerFacade(securityQuoteSession);
				
				if(lstSecurities.size()<=0){
					Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();	
					String headerMessageConfirmation = PropertiesUtilities.getMessage(locale, PropertiesConstants.LBL_HEADER_ALERT);
					String bodyMessageConfirmation = PropertiesUtilities.getMessage(locale, "security.quote.issuer.security",
																					new Object[]{securityQuoteSession.getIssuer().getIdIssuerPk()});
					showMessageOnDialog(headerMessageConfirmation,bodyMessageConfirmation);
					//No tiene valores el emisor, enviar alerta
					alert(headerMessageConfirmation,bodyMessageConfirmation);
					
					//Que solo aparesca en el combo los valores del emisor seleccionado
					lstSecurities = new ArrayList<Security>();
					for(Security sec : lstAllSecurities){
						lstSecurities.add(sec);
					}
					securityQuoteSession.setSecurity(new Security());
					securityQuoteSession.setIssuer(new Issuer());
//					issuerHelperBean.setIssuerMnemonic(null);
//					issuerHelperBean.setIssuerDescription(null);
				}
				
			}else{
				lstSecurities = new ArrayList<Security>();
				for(Security securiti : lstAllSecurities){
					lstSecurities.add(securiti);
				}
			}
			
		} catch (ServiceException e) {
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getMessage());
				JSFUtilities.executeJavascriptFunction("cnfwMsgCustomValidationSec.show()");
				securityQuoteSession.setIssuer(new Issuer());
				//securityQuoteSession.setIssuance(new Issuance());
				securityQuoteSession.setSecurity(new Security());
			}
			e.printStackTrace();
		}	
	}
	
	/**
	 * Validate issuance handler.
	 */
	public void validateIssuanceHandler(){
		try {
			securityQuoteFacade.validateIssuanceFacade(securityQuoteSession);
		} catch (ServiceException e) {
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getMessage());
				JSFUtilities.executeJavascriptFunction("cnfwMsgCustomValidationSec.show()");
				securityQuoteSession.setSecurity(new Security());
			}
			e.printStackTrace();
		}
	}
	
	/**
	 * Validate security handler.
	 */
	public void validateSecurityHandler(){
		try {
			if(securityQuoteSession.getSecurity()!=null && !securityQuoteSession.getSecurity().getIdIsinCode().isEmpty()){
				securityQuoteSession.setSecurity(issuSecuritiesFacade.getSecurityFromIsinCode(securityQuoteSession.getSecurity().getIdIsinCode()));
				securityQuoteFacade.validateSecurityFacade(securityQuoteSession);
			}
		} catch (ServiceException e) {
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getMessage());
				JSFUtilities.executeJavascriptFunction("cnfwMsgCustomValidationSec.show()");
				securityQuoteSession.setSecurity(new Security());
			}
			e.printStackTrace();
		}
	}
	
	/**
	 * Register security quote acction.
	 *
	 * @return the string
	 */
	public String registerSecurityQuoteAcction(){
		
		try {
			validateHours();
		} catch (ServiceException e1) {
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
				  PropertiesUtilities.getExceptionMessage(e1.getErrorService().getMessage(),e1.getParams()) );

			return null;
		}
		
		securityQuoteSession = new SecurityQuote();
		securityQuoteSession.setIssuer(new Issuer());
		securityQuoteSession.setSecurity(new Security());
		securityQuoteSession.setRegistryDate(CommonsUtilities.currentDate());
		securityQuoteSession.setQuoteDate(CommonsUtilities.currentDate());
		securityQuoteSession.setRequestType(actualizar);
		setViewOperationType(ViewOperationsType.REGISTER.getCode());
		
		securitiesHelperBean.setSecurityDescription(null);

		if(isIssuer)
			securityEvents();
		
		return "admSecurityQuoteRule";
	}
	
	/**
	 * Register update security quote acction.
	 *
	 * @return the string
	 */
	public String registerUpdateSecurityQuoteAcction(){
		
		try {
			validateHours();
		} catch (ServiceException e1) {
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
				  PropertiesUtilities.getExceptionMessage(e1.getErrorService().getMessage(),e1.getParams()) );

			return null;
		}
		
		securityQuoteSession = new SecurityQuote();
		securityQuoteSession.setIssuer(new Issuer());
		securityQuoteSession.setSecurity(new Security());
		securityQuoteSession.setQuoteDate(CommonsUtilities.currentDate());
		setViewOperationType(ViewOperationsType.REGISTER.getCode());
		securityQuoteSession.setRequestType(modificar);
		
		securitiesHelperBean.setSecurityDescription(null);
		
		if(isIssuer)
			securityEvents();
		
		return "admSecurityQuoteRule";
	}
	

	
	/**
	 * Securityselected event.
	 */
	public void securityselectedEvent(){
		
		for(Security sec : lstSecurities){
			if( sec.getIdIsinCode().equals(securityQuoteSession.getSecurity().getIdIsinCode()) ){
				securityQuoteSession.setSecurity(sec);
			}
		}
		
		if(Validations.validateIsNotNull(securityQuoteSession.getQuoteValuePrice())){
			BigDecimal tmpQuoteValuePrice = securityQuoteSession.getQuoteValuePrice();
			BigDecimal tmpShareBalance = securityQuoteSession.getSecurity().getShareBalance();
			BigDecimal tmpShareCapital = securityQuoteSession.getSecurity().getShareCapital();
			
			newShareCapital = tmpShareBalance.multiply(tmpQuoteValuePrice);
			
			variationShareCapital = newShareCapital.subtract(tmpShareCapital);
			
			if(variationShareCapital.compareTo(BigDecimal.ZERO) < 0 ){
				variationShareCapital = tmpShareCapital.subtract(newShareCapital);
			}
		}else{
			newShareCapital = new BigDecimal(0);
			variationShareCapital = new BigDecimal(0);
		}
	}
	
	/**
	 * Before save security quote.
	 */
	public void beforeSaveSecurityQuote(){
		
			
		securityQuoteSession.setShareBalance(securityQuoteSession.getSecurity().getShareBalance());
		securityQuoteSession.setShareCapital(securityQuoteSession.getSecurity().getShareCapital());
		try {
			validateHours();
		} catch (ServiceException e1) {
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
				  PropertiesUtilities.getExceptionMessage(e1.getErrorService().getMessage(),e1.getParams()) );

			return;
		}
		
		try {
			securityQuoteFacade.validateQuoteDate(securityQuoteSession);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getMessage());
				JSFUtilities.executeJavascriptFunction("cnfwMsgCustomValidationSec.show()");
				return;
			}
		}
		
		if(securityQuoteSession.getRequestType().equals(modificar)){
			showMessageOnDialogOnlyKey(PropertiesConstants.LBL_CONFIRM_HEADER_REGISTER, "security.quote.registry.message.modify");
		}else{//actualizar
			showMessageOnDialogOnlyKey(PropertiesConstants.LBL_CONFIRM_HEADER_REGISTER, "security.quote.registry.message.update");
		}
		
		JSFUtilities.executeJavascriptFunction("cnfwFormMgmtRegister.show()");
	}
	
	/**
	 * Before confirm security quote.
	 */
	public void beforeConfirmSecurityQuote(){
		try {
			validateHours();
		} catch (ServiceException e1) {
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
				  PropertiesUtilities.getExceptionMessage(e1.getErrorService().getMessage(),e1.getParams()) );

			return;
		}
		
		showMessageOnDialogOnlyKey(PropertiesConstants.LBL_CONFIRM_HEADER_REGISTER, "security.quote.confirm.message");
		JSFUtilities.executeJavascriptFunction("cnfwFormMgmtRegister.show()");
	}
	
	/**
	 * Before reject security quote.
	 */
	public void beforeRejectSecurityQuote(){
		try {
			validateHours();
		} catch (ServiceException e1) {
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
				  PropertiesUtilities.getExceptionMessage(e1.getErrorService().getMessage(),e1.getParams()) );

			return;
		}
		showMessageOnDialogOnlyKey(PropertiesConstants.LBL_CONFIRM_HEADER_REGISTER, "security.quote.reject.message");
		JSFUtilities.executeJavascriptFunction("cnfwFormMgmtRegister.show()");
	}
	/**
	 * Save security quote.
	 */
	@LoggerAuditWeb
	public void saveSecurityQuote(){
		try {
			switch(ViewOperationsType.get(getViewOperationType())){
				case REGISTER: executeRegistryQuote();break;
				case CONFIRM: executeConfirmQuote();break;
				case REJECT: executeRejectQuote();break;
			}
		} catch (ServiceException e) {
			showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getMessage());
			JSFUtilities.executeJavascriptFunction("formWMgmtOk.show()");
		}
	}
	/**
	 * Execute registry quote.
	 *
	 * @throws ServiceException the service exception
	 */
	public void executeRegistryQuote() throws ServiceException{
		securityQuoteFacade.validateQuoteDate(securityQuoteSession);
		securityQuoteFacade.saveSecurityQuoteFacade(securityQuoteSession);
		
		Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();	
		String headerMessageConfirmation = PropertiesUtilities.getMessage(locale, PropertiesConstants.LBL_HEADER_ALERT);
		String bodyMessageConfirmation = "";
		if(securityQuoteSession.getRequestType().equals(modificar)){
			
			bodyMessageConfirmation = PropertiesUtilities.getMessage(locale, "security.quote.registry.message.modify.ok",new Object[]{securityQuoteSession.getIdSecurityQuotePk().toString()});
			showMessageOnDialog(headerMessageConfirmation,bodyMessageConfirmation);
		}else{
			bodyMessageConfirmation = PropertiesUtilities.getMessage(locale, "security.quote.registry.message.update.ok",new Object[]{securityQuoteSession.getIdSecurityQuotePk().toString()});
			showMessageOnDialog(headerMessageConfirmation,bodyMessageConfirmation);
			
		}
		cleanAfterSave();
		JSFUtilities.executeJavascriptFunction("formWMgmtOk.show()");
	}
	
	/**
	 * Clean after save.
	 *
	 * @throws ServiceException the service exception
	 */
	public void cleanAfterSave() throws ServiceException{
		securitiesHelperBean.setSecurityDescription(null);
		lstSecurities = new ArrayList<Security>();
		for(Security securiti : lstAllSecurities){
			lstSecurities.add(securiti);
		}
		loadSecurityQuoteTO();
		securityQuoteSession = new SecurityQuote();
		
		if(!isIssuer){
			securityQuoteSession.setIssuer(new Issuer());
		}
		securityQuoteSession.setSecurity(new Security());
		securityQuoteSession.setRegistryDate(CommonsUtilities.currentDate());
		securityQuoteSession.setQuoteDate(CommonsUtilities.currentDate());
		securityEvents();
	}
	
	/**
	 * Alert clear fields.
	 */
	public void alertClearFields(){
		Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		String message = PropertiesUtilities.getMessage(locale, "lbl.clear.screen");
		String validationMessage=PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES,
																locale,GeneralConstants.LBL_HEADER_ALERT_WARNING);
		
		showMessageOnDialog(validationMessage,message); 
		JSFUtilities.showComponent("formPopUp:confirmClearAlert"); 
	}
	
	/**
	 * Removes the alert clear.
	 */
	public void removeAlertClear(){
		JSFUtilities.hideComponent("formPopUp:confirmClearAlert");
	}
	
	/**
	 * Clean screen quote mgmt.
	 */
	public void cleanScreenQuoteMgmt(){
		boolean flagExecuteAlert = false;
		if(!isIssuer && Validations.validateIsNotNullAndNotEmpty(securityQuoteSession.getIssuer())){
			if(Validations.validateIsNotNull(securityQuoteSession.getIssuer().getIdIssuerPk())){
				flagExecuteAlert = true;
			}
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(securityQuoteSession.getSecurity())){
			if(Validations.validateIsNotNull(securityQuoteSession.getSecurity().getIdIsinCode())){
				flagExecuteAlert = true;
			}
		}
		
		if(Validations.validateIsNotNull(securityQuoteSession.getQuoteValuePrice())){
			if(Validations.validateIsNotNullAndPositive(securityQuoteSession.getQuoteValuePrice().intValue())){
					flagExecuteAlert = true;
			}
		}
		
		if(flagExecuteAlert){
			alertClearFields();
		}
		
	}
	
	/**
	 * Clear quote mgmt.
	 */
	public void clearQuoteMgmt(){

		Integer requeType = securityQuoteSession.getRequestType();
		
		securityQuoteSession = new SecurityQuote();
		securityQuoteSession.setIssuer(new Issuer());
		securityQuoteSession.setSecurity(new Security());
		securityQuoteSession.setRegistryDate(CommonsUtilities.currentDate());
		securityQuoteSession.setQuoteDate(CommonsUtilities.currentDate());
		securityQuoteSession.setRequestType(requeType);
		variationShareCapital = null;
		newShareCapital = null;

		if(!isIssuer){
			lstSecurities = new ArrayList<Security>();
			for(Security securiti : lstAllSecurities){
				lstSecurities.add(securiti);
			}
		}
		
		if(isIssuer){
			securityEvents();
		}
	}
	
	/**
	 * Execute reject quote.
	 *
	 * @throws ServiceException the service exception
	 */
	public void executeRejectQuote() throws ServiceException{
		securityQuoteFacade.rejectSecurityQuoteFacade(securityQuoteSession);
		searchSecurityQuoteList();
		Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();	
		String headerMessageConfirmation = PropertiesUtilities.getMessage(locale, PropertiesConstants.LBL_HEADER_ALERT);
		String bodyMessageConfirmation = PropertiesUtilities.getMessage(locale, "security.quote.reject.message.ok",new Object[]{securityQuoteSession.getIdSecurityQuotePk().toString()});
		showMessageOnDialog(headerMessageConfirmation,bodyMessageConfirmation);
		JSFUtilities.executeJavascriptFunction("formWMgmtOk.show()");
	}
	
	/**
	 * Execute confirm quote.
	 *
	 * @throws ServiceException the service exception
	 */
	public void executeConfirmQuote() throws ServiceException{
		securityQuoteFacade.confirmSecurityQuoteFacade(securityQuoteSession);
		searchSecurityQuoteList();
		Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();	
		String headerMessageConfirmation = PropertiesUtilities.getMessage(locale, PropertiesConstants.LBL_HEADER_ALERT);
		String bodyMessageConfirmation = "";
		JSFUtilities.executeJavascriptFunction("formWMgmtOk.show()");
	}
	
	/**
	 * Search security quote list.
	 */
	public void searchSecurityQuoteList(){
		
		try {
			validateHours();
		} catch (ServiceException e1) {
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
				  PropertiesUtilities.getExceptionMessage(e1.getErrorService().getMessage(),e1.getParams()) );
			return;
		}
		
		List<SecurityQuoteTO> securityQuoteList = null;
		SecurityQuoteTO quoteTO = new SecurityQuoteTO();
		
		if(Validations.validateIsNotNull(securityQuoteTO.getIdSecurityQuotePk())){
			quoteTO.setIdSecurityQuotePk(securityQuoteTO.getIdSecurityQuotePk());

			if(Validations.validateIsNotNull(securityQuoteTO.getIdSecurityQuotePk())){
				quoteTO.setIssuer(securityQuoteTO.getIssuer());
			}
		}else{
			quoteTO.setIssuer(securityQuoteTO.getIssuer());
			quoteTO.setRequestType(securityQuoteTO.getRequestType());
			quoteTO.setSecurity(securityQuoteTO.getSecurity());
			quoteTO.setInitialDate(securityQuoteTO.getInitialDate());
			quoteTO.setFinalDate(securityQuoteTO.getFinalDate());
			quoteTO.setQuoteState(securityQuoteTO.getQuoteState());
		}
		
		try {
			securityQuoteList = securityQuoteFacade.getSecurityQuoteListFacade(quoteTO);
			for(SecurityQuoteTO secQuoTo : securityQuoteList){
				secQuoTo.getSecurity().setDescription(securitieDescription.get(secQuoTo.getSecurity().getIdIsinCode()));
				secQuoTo.setQuoteStateDescription(SecurityQuoteStateType.get(secQuoTo.getQuoteState()).getValue());
				secQuoTo.setRequestTypeDescription(requestTypeDescription.get(secQuoTo.getRequestType()));
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		this.securityQuoteList = new GenericDataModel<SecurityQuoteTO>(securityQuoteList);
	}
	
	/**
	 * Before confirm action.
	 *
	 * @return the string
	 */
	public String beforeConfirmAction(){
		return getSecurityQuoteSelectedAction(ViewOperationsType.CONFIRM);
	}
	
	/**
	 * Before modify action.
	 *
	 * @return the string
	 */
	public String beforeModifyAction(){
		return getSecurityQuoteSelectedAction(ViewOperationsType.MODIFY);
	}
	
	/**
	 * Before reject action.
	 *
	 * @return the string
	 */
	public String beforeRejectAction(){
		return getSecurityQuoteSelectedAction(ViewOperationsType.REJECT);
	}
	
	/**
	 * Alert.
	 *
	 * @param header the header
	 * @param message the message
	 */
	public void alert(String header,String message){		
		showMessageOnDialog(header,message); 
		JSFUtilities.showComponent("formPopUp:alterValidation"); 
	}
	
	/**
	 * Removes the alert.
	 */
	public void removeAlert(){
		JSFUtilities.hideComponent("formPopUp:alterValidation"); 
	}
	

	/**
	 * Gets the security quote selected action.
	 *
	 * @param viewOperationsType the view operations type
	 * @return the security quote selected action
	 */
	public String getSecurityQuoteSelectedAction(ViewOperationsType viewOperationsType){

		try {
			validateHours();
		} catch (ServiceException e1) {
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
				  PropertiesUtilities.getExceptionMessage(e1.getErrorService().getMessage(),e1.getParams()) );
			return null;
		}
		
		SecurityQuoteStateType securityQuotState = null;
		Boolean canExecuteAction = Boolean.FALSE;
		
		try {
			
			SecurityQuoteTO securityQuoteTO = securityQuoteTOSelected;
			securityQuoteSession = securityQuoteFacade.getSecurityQuoteFromPkFacade(securityQuoteTO.getIdSecurityQuotePk());
			securityQuoteSession.getSecurity().setCurrencyName(currencyDescription.get(securityQuoteSession.getSecurity().getCurrency()));
			
			securityQuotState = SecurityQuoteStateType.get(securityQuoteTO.getQuoteState());
			securityQuoteTO.getSecurity().setCurrencyName(currencyDescription.get(securityQuoteTO.getSecurity().getCurrency()));
			
			if(! securityQuoteSession.getQuoteState().equals(SecurityQuoteStateType.CONFIRMED.getCode()) ){
				
				BigDecimal tmpQuoteValuePrice = securityQuoteSession.getQuoteValuePrice();
				BigDecimal tmpShareBalance = securityQuoteSession.getShareBalance();
				BigDecimal tmpShareCapital = securityQuoteSession.getShareCapital();
				
				newShareCapital = tmpShareBalance.multiply(tmpQuoteValuePrice);
				
				variationShareCapital = newShareCapital.subtract(tmpShareCapital);
				
				if(variationShareCapital.compareTo(BigDecimal.ZERO) < 0 ){
					variationShareCapital = tmpShareCapital.subtract(newShareCapital);
				}
				
			}else{
				
				BigDecimal tmpShareBalance = securityQuoteSession.getShareBalance();			//cantidad de valores (*)
				BigDecimal tmpQuoteValuePrice = securityQuoteSession.getQuoteValuePrice();		//valor cuota
				BigDecimal tmpQuoteValueBefore = securityQuoteSession.getQuoteValueBefore();	//valor cuota anterior
				BigDecimal tmpShareCapital;// = securityQuoteSession.getShareCapital();			//nuevo share capital
				
				newShareCapital = tmpShareBalance.multiply(tmpQuoteValuePrice);;//tmpQuoteValuePrice.multiply(tmpShareBalance);
				tmpShareCapital = tmpShareBalance.multiply(tmpQuoteValueBefore);
				variationShareCapital = newShareCapital.subtract(tmpShareCapital);
				
				if(variationShareCapital.compareTo(BigDecimal.ZERO) < 0 ){
					variationShareCapital = tmpShareCapital.subtract(newShareCapital);
				}
			}
		
		
			IssuerSearcherTO issuerSearch = new IssuerSearcherTO();
			issuerSearch.setIssuerCode(securityQuoteTO.getIssuer().getIdIssuerPk());
			com.pradera.core.component.helperui.to.IssuerTO issuerLoad = new com.pradera.core.component.helperui.to.IssuerTO();
			issuerLoad = helperComponentFacade.findIssuerByCode(issuerSearch);
				
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			switch(viewOperationsType){
			case MODIFY:
					if(securityQuotState.equals(SecurityQuoteStateType.CONFIRMED)){
						canExecuteAction = Boolean.TRUE;
					}break;
			case CONFIRM: case REJECT:
					if(securityQuotState.equals(SecurityQuoteStateType.REGISTERED)){
						canExecuteAction = Boolean.TRUE;
					}break;
			}
		if(canExecuteAction){
			setViewOperationType(viewOperationsType.getCode());
			try {
				securityQuoteSession = securityQuoteFacade.getSecurityQuoteFromPkFacade(securityQuoteTO.getIdSecurityQuotePk());
				securityQuoteSession.getSecurity().setCurrencyName(currencyDescription.get(securityQuoteSession.getSecurity().getCurrency()));
			} catch (ServiceException e) {
				e.printStackTrace();
			}
			return "admSecurityQuoteRule";
		}else{
			if(securityQuoteTO!=null){
				executeJsFunctionAndShowMessage(PropertiesConstants.LBL_HEADER_ALERT, "security.quote.validation.incorrect","cnfwMsgCustomValidationSec.show()");
			}else{
				executeJsFunctionAndShowMessage(PropertiesConstants.LBL_HEADER_ALERT,PropertiesConstants.ERROR_RECORD_REQUIRED,"cnfwMsgCustomValidationSec.show()");
			}
			return null;
		}
	}
	
	/**
	 * Gets the security quote view.
	 *
	 * @param securityQuoteTO the security quote to
	 * @return the security quote view
	 */
	public String getSecurityQuoteView(SecurityQuoteTO securityQuoteTO){
		
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		try {
			securityQuoteSession = securityQuoteFacade.getSecurityQuoteFromPkFacade(securityQuoteTO.getIdSecurityQuotePk());
			securityQuoteSession.getSecurity().setCurrencyName(currencyDescription.get(securityQuoteSession.getSecurity().getCurrency()));
			
			if(! securityQuoteSession.getQuoteState().equals(SecurityQuoteStateType.CONFIRMED.getCode()) ){
				
				BigDecimal tmpQuoteValuePrice = securityQuoteSession.getQuoteValuePrice();
				BigDecimal tmpShareBalance = securityQuoteSession.getShareBalance();
				BigDecimal tmpShareCapital = securityQuoteSession.getShareCapital();
				
				newShareCapital = tmpShareBalance.multiply(tmpQuoteValuePrice);
				
				variationShareCapital = newShareCapital.subtract(tmpShareCapital);
				
				if(variationShareCapital.compareTo(BigDecimal.ZERO) < 0 ){
					variationShareCapital = tmpShareCapital.subtract(newShareCapital);
				}
				
			}else{
				
				BigDecimal tmpShareBalance = securityQuoteSession.getShareBalance();			//cantidad de valores (*)
				BigDecimal tmpQuoteValuePrice = securityQuoteSession.getQuoteValuePrice();		//valor cuota
				BigDecimal tmpQuoteValueBefore = securityQuoteSession.getQuoteValueBefore();	//valor cuota anterior
				BigDecimal tmpShareCapital;// = securityQuoteSession.getShareCapital();			//nuevo share capital
				
				newShareCapital = tmpShareBalance.multiply(tmpQuoteValuePrice);;//tmpQuoteValuePrice.multiply(tmpShareBalance);
				tmpShareCapital = tmpShareBalance.multiply(tmpQuoteValueBefore);
				variationShareCapital = newShareCapital.subtract(tmpShareCapital);
				
				if(variationShareCapital.compareTo(BigDecimal.ZERO) < 0 ){
					variationShareCapital = tmpShareCapital.subtract(newShareCapital);
				}
			}
			
			IssuerSearcherTO issuerSearch = new IssuerSearcherTO();
			issuerSearch.setIssuerCode(securityQuoteSession.getSecurity().getIssuance().getIssuer().getIdIssuerPk());
			com.pradera.core.component.helperui.to.IssuerTO issuerLoad = new com.pradera.core.component.helperui.to.IssuerTO();
			issuerLoad = helperComponentFacade.findIssuerByCode(issuerSearch);
			
			
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return "admSecurityQuoteRule";
	}

	/**
	 * Gets the security quote session.
	 *
	 * @return the security quote session
	 */
	public SecurityQuote getSecurityQuoteSession() {
		return securityQuoteSession;
	}

	/**
	 * Sets the security quote session.
	 *
	 * @param securityQuoteSession the new security quote session
	 */
	public void setSecurityQuoteSession(SecurityQuote securityQuoteSession) {
		this.securityQuoteSession = securityQuoteSession;
	}
	/**
	 * Gets the security quote states.
	 *
	 * @return the security quote states
	 */
	public List<ParameterTable> getSecurityQuoteStates() {
		return securityQuoteStates;
	}
	/**
	 * Sets the security quote states.
	 *
	 * @param securityQuoteStates the new security quote states
	 */
	public void setSecurityQuoteStates(List<ParameterTable> securityQuoteStates) {
		this.securityQuoteStates = securityQuoteStates;
	}
	/**
	 * Gets the security quote to.
	 *
	 * @return the security quote to
	 */
	public SecurityQuoteTO getSecurityQuoteTO() {
		return securityQuoteTO;
	}
	/**
	 * Sets the security quote to.
	 *
	 * @param securityQuoteTO the new security quote to
	 */
	public void setSecurityQuoteTO(SecurityQuoteTO securityQuoteTO) {
		this.securityQuoteTO = securityQuoteTO;
	}
	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}
	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * Gets the min date register.
	 *
	 * @return the min date register
	 */
	public Date getMinDateRegister() {
		return minDateRegister;
	}
	
	/**
	 * Sets the min date register.
	 *
	 * @param minDateRegister the new min date register
	 */
	public void setMinDateRegister(Date minDateRegister) {
		this.minDateRegister = minDateRegister;
	}

	/**
	 * Gets the security quote list.
	 *
	 * @return the security quote list
	 */
	public GenericDataModel<SecurityQuoteTO> getSecurityQuoteList() {
		return securityQuoteList;
	}

	/**
	 * Sets the security quote list.
	 *
	 * @param securityQuoteList the new security quote list
	 */
	public void setSecurityQuoteList(
			GenericDataModel<SecurityQuoteTO> securityQuoteList) {
		this.securityQuoteList = securityQuoteList;
	}

	/**
	 * Gets the security quote to selected.
	 *
	 * @return the security quote to selected
	 */
	public SecurityQuoteTO getSecurityQuoteTOSelected() {
		return securityQuoteTOSelected;
	}

	/**
	 * Sets the security quote to selected.
	 *
	 * @param securityQuoteTOSelected the new security quote to selected
	 */
	public void setSecurityQuoteTOSelected(SecurityQuoteTO securityQuoteTOSelected) {
		this.securityQuoteTOSelected = securityQuoteTOSelected;
	}
	
	/**
	 * Show isin detail.
	 *
	 * @param isinCode the isin code
	 */
	public void showIsinDetail(String isinCode){
		securityHelpBean.setSecurityCode(isinCode);
		securityHelpBean.setName("securityHelp");
		securityHelpBean.searchSecurityHandler();
	}
	
	/**
	 * Clean security quote search.
	 */
	public void cleanSecurityQuoteSearch(){
		try {
			loadSecurityQuoteTO();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		securityEvents();
		if(!isIssuer){
			
			lstSecurities = new ArrayList<Security>();
			for(Security securiti : lstAllSecurities){
				lstSecurities.add(securiti);
			}
		}
		securityQuoteList = null;
	}
	
	/**
	 * Back to search page handler.
	 */
	public void backToSearchPageHandler(){
		securitiesHelperBean.setSecurityDescription(securityQuoteTO.getSecurity().getDescription());
		newShareCapital = null;
		variationShareCapital = null;
	}

	/**
	 * Gets the flag search.
	 *
	 * @return the flag search
	 */
	public boolean getFlagSearch() {
		return flagSearch;
	}

	/**
	 * Sets the flag search.
	 *
	 * @param flagSearch the new flag search
	 */
	public void setFlagSearch(boolean flagSearch) {
		this.flagSearch = flagSearch;
	}

	/**
	 * Checks if is flag register.
	 *
	 * @return true, if is flag register
	 */
	public boolean isFlagRegister() {
		return flagRegister;
	}

	/**
	 * Sets the flag register.
	 *
	 * @param flagRegister the new flag register
	 */
	public void setFlagRegister(boolean flagRegister) {
		this.flagRegister = flagRegister;
	}

	/**
	 * Gets the checks if is issuer.
	 *
	 * @return the checks if is issuer
	 */
	public boolean getIsIssuer() {
		return isIssuer;
	}

	/**
	 * Sets the checks if is issuer.
	 *
	 * @param isIssuer the new checks if is issuer
	 */
	public void setIsIssuer(boolean isIssuer) {
		this.isIssuer = isIssuer;
	}

	/**
	 * Gets the checks if is cevaldom.
	 *
	 * @return the checks if is cevaldom
	 */
	public boolean getIsCevaldom() {
		return isCevaldom;
	}

	/**
	 * Sets the checks if is cevaldom.
	 *
	 * @param isCevaldom the new checks if is cevaldom
	 */
	public void setIsCevaldom(boolean isCevaldom) {
		this.isCevaldom = isCevaldom;
	}

	/**
	 * Gets the issuer login.
	 *
	 * @return the issuer login
	 */
	public com.pradera.core.component.helperui.to.IssuerTO getIssuerLogin() {
		return issuerLogin;
	}

	/**
	 * Sets the issuer login.
	 *
	 * @param issuerLogin the new issuer login
	 */
	public void setIssuerLogin(
			com.pradera.core.component.helperui.to.IssuerTO issuerLogin) {
		this.issuerLogin = issuerLogin;
	}

	/**
	 * Gets the lst securities.
	 *
	 * @return the lst securities
	 */
	public List<Security> getLstSecurities() {
		return lstSecurities;
	}

	/**
	 * Sets the lst securities.
	 *
	 * @param lstSecurities the new lst securities
	 */
	public void setLstSecurities(List<Security> lstSecurities) {
		this.lstSecurities = lstSecurities;
	}

	/**
	 * Gets the new share capital.
	 *
	 * @return the new share capital
	 */
	public BigDecimal getNewShareCapital() {
		return newShareCapital;
	}

	/**
	 * Sets the new share capital.
	 *
	 * @param newShareCapital the new new share capital
	 */
	public void setNewShareCapital(BigDecimal newShareCapital) {
		this.newShareCapital = newShareCapital;
	}

	/**
	 * Gets the variation share capital.
	 *
	 * @return the variation share capital
	 */
	public BigDecimal getVariationShareCapital() {
		return variationShareCapital;
	}

	/**
	 * Sets the variation share capital.
	 *
	 * @param variationShareCapital the new variation share capital
	 */
	public void setVariationShareCapital(BigDecimal variationShareCapital) {
		this.variationShareCapital = variationShareCapital;
	}

	/**
	 * Gets the radio selected.
	 *
	 * @return the radio selected
	 */
	public Integer getRadioSelected() {
		return radioSelected;
	}

	/**
	 * Sets the radio selected.
	 *
	 * @param radioSelected the new radio selected
	 */
	public void setRadioSelected(Integer radioSelected) {
		this.radioSelected = radioSelected;
	}

	/**
	 * Gets the lst tipo solicitud.
	 *
	 * @return the lst tipo solicitud
	 */
	public List<ParameterTable> getLstTipoSolicitud() {
		return lstTipoSolicitud;
	}

	/**
	 * Sets the lst tipo solicitud.
	 *
	 * @param lstTipoSolicitud the new lst tipo solicitud
	 */
	public void setLstTipoSolicitud(List<ParameterTable> lstTipoSolicitud) {
		this.lstTipoSolicitud = lstTipoSolicitud;
	}

	/**
	 * Gets the actualizar.
	 *
	 * @return the actualizar
	 */
	public Integer getActualizar() {
		return actualizar;
	}

	/**
	 * Sets the actualizar.
	 *
	 * @param actualizar the new actualizar
	 */
	public void setActualizar(Integer actualizar) {
		this.actualizar = actualizar;
	}

	/**
	 * Gets the modificar.
	 *
	 * @return the modificar
	 */
	public Integer getModificar() {
		return modificar;
	}

	/**
	 * Sets the modificar.
	 *
	 * @param modificar the new modificar
	 */
	public void setModificar(Integer modificar) {
		this.modificar = modificar;
	}

	/**
	 * Gets the request type description.
	 *
	 * @return the request type description
	 */
	public Map<Integer, String> getRequestTypeDescription() {
		return requestTypeDescription;
	}

	/**
	 * Sets the request type description.
	 *
	 * @param requestTypeDescription the request type description
	 */
	public void setRequestTypeDescription(
			Map<Integer, String> requestTypeDescription) {
		this.requestTypeDescription = requestTypeDescription;
	}
	
}