package com.pradera.securities.issuancesecuritie.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Security;

public class SecurityRemovedModelTO implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long idSecurityRemovedPk;

	private String idSecurityCodePk;

	private Security security;

	private Date issuanceDate;

	private Date expirationDate;

	private Integer currency;
	private String currencyText;

	private BigDecimal nominalValue;

	private BigDecimal interestRateNominal;

	private Integer couponNumber;

	private String motiveRemoved;

	private Date registryDate;

	private Integer stateSecurityRemoved;
	private String stateSecurityRemovedText;

	private List<ParameterTable> lstState = new ArrayList<>();
	private List<ParameterTable> lstCurrency = new ArrayList<>();

	private Date initialDate;
	private Date finalDate;

	// para las pistas de auditoria manuales
	private String auditUserName;
	private String auditIpAddress;
	private Long auditIdPrivilege;

	// fisrt constructor
	public SecurityRemovedModelTO() {
		security = new Security();
	}

	// constructor para la lista JPQL del metodo
	public SecurityRemovedModelTO(Long idSecurityRemovedPk, Date registryDate,
			String idSecurityCodePk, Date issuanceDate, Date expirationDate,
			Integer currency, BigDecimal nominalValue,
			Integer stateSecurityRemoved) {
		this.idSecurityRemovedPk = idSecurityRemovedPk;
		this.registryDate = registryDate;
		this.idSecurityCodePk = idSecurityCodePk;
		this.issuanceDate = issuanceDate;
		this.expirationDate = expirationDate;
		this.currency = currency;
		this.nominalValue = nominalValue;
		this.stateSecurityRemoved = stateSecurityRemoved;
	}

	/**
	 * para la clonacion de objetos, asi evitamos la referencia
	 */
	@Override
	public Object clone() {
		Object obj = null;
		try {
			obj = super.clone();
		} catch (CloneNotSupportedException ex) {
			System.out.println("No se puede clonar Objeto");
		}
		return obj;
	}

	public Long getIdSecurityRemovedPk() {
		return idSecurityRemovedPk;
	}

	public void setIdSecurityRemovedPk(Long idSecurityRemovedPk) {
		this.idSecurityRemovedPk = idSecurityRemovedPk;
	}

	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public Date getIssuanceDate() {
		return issuanceDate;
	}

	public void setIssuanceDate(Date issuanceDate) {
		this.issuanceDate = issuanceDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public String getCurrencyText() {
		return currencyText;
	}

	public void setCurrencyText(String currencyText) {
		this.currencyText = currencyText;
	}

	public BigDecimal getNominalValue() {
		return nominalValue;
	}

	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}

	public BigDecimal getInterestRateNominal() {
		return interestRateNominal;
	}

	public void setInterestRateNominal(BigDecimal interestRateNominal) {
		this.interestRateNominal = interestRateNominal;
	}

	public Integer getCouponNumber() {
		return couponNumber;
	}

	public void setCouponNumber(Integer couponNumber) {
		this.couponNumber = couponNumber;
	}

	public String getMotiveRemoved() {
		return motiveRemoved;
	}

	public void setMotiveRemoved(String motiveRemoved) {
		this.motiveRemoved = motiveRemoved;
	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public Integer getStateSecurityRemoved() {
		return stateSecurityRemoved;
	}

	public void setStateSecurityRemoved(Integer stateSecurityRemoved) {
		this.stateSecurityRemoved = stateSecurityRemoved;
	}

	public String getStateSecurityRemovedText() {
		return stateSecurityRemovedText;
	}

	public void setStateSecurityRemovedText(String stateSecurityRemovedText) {
		this.stateSecurityRemovedText = stateSecurityRemovedText;
	}

	public List<ParameterTable> getLstState() {
		return lstState;
	}

	public void setLstState(List<ParameterTable> lstState) {
		this.lstState = lstState;
	}

	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}

	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	public String getAuditUserName() {
		return auditUserName;
	}

	public void setAuditUserName(String auditUserName) {
		this.auditUserName = auditUserName;
	}

	public String getAuditIpAddress() {
		return auditIpAddress;
	}

	public void setAuditIpAddress(String auditIpAddress) {
		this.auditIpAddress = auditIpAddress;
	}

	public Long getAuditIdPrivilege() {
		return auditIdPrivilege;
	}

	public void setAuditIdPrivilege(Long auditIdPrivilege) {
		this.auditIdPrivilege = auditIdPrivilege;
	}

}
