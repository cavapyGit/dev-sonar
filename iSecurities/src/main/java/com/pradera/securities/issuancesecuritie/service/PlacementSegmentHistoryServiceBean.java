package com.pradera.securities.issuancesecuritie.service;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;

/**
 * The Class PlacementSegmentHistoryServiceBean.
 * 
 * @author jquino.
 * @version 1.0 , 06/07/2020
 */
@Stateless
public class PlacementSegmentHistoryServiceBean extends CrudDaoServiceBean {

	@Inject
	private PraderaLogger praderaLogger;
	
	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;

	/**
	 * Funcion para obtener y registrar historicos de valores no colocados
	 * 
	 * @param today
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	public void generatePlacementSegmentHistory (Date fecha) throws ServiceException {

		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		StringBuilder builder = new StringBuilder();
		
		builder.append(" INSERT INTO PLACEMENT_SEGMENT_HISTORY (                                                                                                                                ");
		builder.append(" 	 ID_PLACEMENT_SEGMENT_HISTORY_PK                                                                                                                                    ");
		builder.append(" 	,NOMINAL_VALUE                                                                                                                                                      ");
		builder.append(" 	,MOVEMENT_QUANTITY                                                                                                                                                  ");
		builder.append(" 	,NOT_PLACED                                                                                                                                                         ");
		builder.append(" 	,ID_ISSUANCE_CODE_FK                                                                                                                                                ");
		builder.append(" 	,ID_PARTICIPANT_FK                                                                                                                                                  ");
		builder.append(" 	,ID_SECURITY_CODE_FK                                                                                                                                                ");
		builder.append(" 	,MOVEMENT_TYPE                                                                                                                                                      ");
		builder.append(" 	,ID_HOLDER_FK                                                                                                                                                       ");
		builder.append(" 	,INSTRUMENT_TYPE                                                                                                                                                    ");
		builder.append(" 	,CURRENCY_TYPE                                                                                                                                                      ");
		builder.append(" 	,PROCESS_DATE                                                                                                                                                       ");
		builder.append(" 	,LAST_MODIFY_APP                                                                                                                                                    ");
		builder.append(" 	,LAST_MODIFY_USER                                                                                                                                                   ");
		builder.append(" 	,LAST_MODIFY_DATE                                                                                                                                                   ");
		builder.append(" 	,LAST_MODIFY_IP)                                                                                                                                                    ");
		builder.append(" SELECT SQ_ID_PLAC_SEGMENT_HIST_PK.NEXTVAL ,T.*                                                                                                                         ");
		builder.append(" FROM(                                                                                                                                                                  ");
		builder.append("   SELECT DISTINCT                                                                                                                                                      ");
		builder.append("      NOMINAL_VALUE                                                                                                                                                     ");
		builder.append("     ,MOVEMENT_QUANTITY                                                                                                                                                 ");
		builder.append("     ,NOT_PLACED                                                                                                                                                        ");
		builder.append("     ,ISSUANCE_CODE                                                                                                                                                     ");
		builder.append("     ,PARTICIPANT                                                                                                                                                       ");
		builder.append("     ,SECURITY_CODE                                                                                                                                                     ");
		builder.append("     ,MOVEMENT_TYPE                                                                                                                                                     ");
		builder.append("     ,ID_HOLDER_PK                                                                                                                                                      ");
		builder.append("     ,INSTRUMENT_TYPE                                                                                                                                                   ");
		builder.append("     ,CURRENCY                                                                                                                                                          ");
		builder.append("     ,to_date('" + CommonsUtilities.convertDateToString(fecha,"dd/MM/yyyy") + "','dd/MM/yyyy')                                                                          ");
		builder.append("     ,1                                                                                                                                                                 ");
		builder.append("     ,'ADMIN'                                                                                                                                                           ");
		builder.append("     ,SYSDATE                                                                                                                                                           ");
		builder.append("     ,'" + loggerUser.getIpAddress()+"'");
		builder.append("   FROM(                                                                                                                                                                ");
		builder.append("     SELECT DISTINCT                                                                                                                                                    ");
		builder.append("       DECODE(se.security_class,426,                                                                                                                                    ");
		builder.append("       ROUND( decode(SE.STATE_SECURITY,131,SE.current_nominal_value,                                                                                                    ");
		builder.append("       FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , to_date('" + CommonsUtilities.convertDateToString(fecha,"dd/MM/yyyy") + "','dd/MM/yyyy'))), 4),                    ");
		builder.append("       ROUND( decode(SE.STATE_SECURITY,131,se.current_nominal_value,                                                                                                    ");
		builder.append("       FN_NOMINAL_PORTFOLIO(SE.id_security_code_pk , to_date('" + CommonsUtilities.convertDateToString(fecha,"dd/MM/yyyy") + "','dd/MM/yyyy'))), 2)) AS NOMINAL_VALUE, 	");
		builder.append("       ROUND((PSD.PLACED_AMOUNT - NVL(PSD.REQUEST_AMOUNT,0))/                                                                                                           ");
		builder.append("       DECODE(SE.CURRENT_NOMINAL_VALUE,0,1,SE.CURRENT_NOMINAL_VALUE),4) AS MOVEMENT_QUANTITY ,                                                                          ");
		builder.append("       PSD.PLACED_AMOUNT AS NOT_PLACED,                                                                                                                                 ");
		builder.append("       ISS.ID_ISSUANCE_CODE_PK AS ISSUANCE_CODE,                                                                                                                        ");
		builder.append("       PSS.ID_PARTICIPANT_FK AS PARTICIPANT,                                                                                                                            ");
		builder.append("       PSD.ID_SECURITY_CODE_FK AS SECURITY_CODE,                                                                                                                        ");
		builder.append("       300842 as MOVEMENT_TYPE,ISS.INSTRUMENT_TYPE,ISS.CURRENCY,                                                                                                        ");
		builder.append("       H.ID_HOLDER_PK                                                                                                                                                   ");
		builder.append("     FROM ISSUANCE ISS                                                                                                                                                  ");
		builder.append("       INNER JOIN PLACEMENT_SEGMENT PS ON ISS.ID_ISSUANCE_CODE_PK=PS.ID_ISSUANCE_CODE_FK                                                                                ");
		builder.append("       INNER JOIN PLACEMENT_SEGMENT_DETAIL PSD ON PS.ID_PLACEMENT_SEGMENT_PK=PSD.ID_PLACEMENT_SEGMENT_FK                                                                ");
		builder.append("       INNER JOIN PLACEMENT_SEG_PARTICIPA_STRUCT PSS ON PS.ID_PLACEMENT_SEGMENT_PK=PSS.ID_PLACEMENT_SEGMENT_FK                                                          ");
		builder.append("       INNER JOIN SECURITY SE ON SE.ID_SECURITY_CODE_PK=PSD.ID_SECURITY_CODE_FK                                                                                         ");
		builder.append("       INNER JOIN PARTICIPANT PA on PA.ID_PARTICIPANT_PK=PSS.ID_PARTICIPANT_FK                                                                                          ");
		builder.append("       INNER JOIN HOLDER H ON H.ID_HOLDER_PK=PA.ID_HOLDER_FK                                                                                                            ");
		builder.append("       INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAD.ID_HOLDER_FK=H.ID_HOLDER_PK                                                                                          ");
		builder.append("       INNER JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK=HAD.ID_HOLDER_ACCOUNT_FK AND HA.ID_PARTICIPANT_FK=PA.ID_PARTICIPANT_PK 	                                ");
		builder.append("     WHERE PSD.PENDING_ANNOTATION=0                                                                                                                                     ");
		builder.append("    )                                                                                                                                                                   ");
		builder.append("   WHERE (NOMINAL_VALUE*MOVEMENT_QUANTITY)!=0                                                                                                                           ");
		builder.append("   ORDER BY PARTICIPANT                                                                                                                                                 ");
		builder.append(" ) T                                                                                                                                                                    ");
		
		try{
			
			em.createNativeQuery(builder.toString()).executeUpdate();
			
		}catch (Exception e){
			praderaLogger.error(e.getMessage());
		} 

	}
	
	/**
	 * MEtodo que obtiene Historicos de no colocados segun la fecha
	 * @param fecha
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getPlacementSegmentHistoryByDate (Date fecha){
		
		StringBuilder builder = new StringBuilder();
		
		builder.append(" SELECT * ");
		builder.append(" FROM PLACEMENT_SEGMENT_HISTORY ");
		builder.append(" WHERE trunc(process_date) = trunc(to_date(:fecha,'dd/MM/yyyy')) ");
		
		Query queryString = em.createNativeQuery(builder.toString());
		queryString.setParameter("fecha", CommonsUtilities.convertDateToString(fecha,"dd/MM/yyyy"));
		
		return queryString.getResultList();
	}

}
