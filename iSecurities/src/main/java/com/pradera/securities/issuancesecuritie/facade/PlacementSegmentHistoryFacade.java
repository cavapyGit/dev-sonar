package com.pradera.securities.issuancesecuritie.facade;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.exception.ServiceException;
import com.pradera.securities.issuancesecuritie.service.PlacementSegmentHistoryServiceBean;


/**
 * 
 * The Class generate Placement Segment
 *
 * @author jquino
 * @version 1.0 , 06/07/2020
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class PlacementSegmentHistoryFacade {
	
	@Inject
    private PraderaLogger praderaLogger;
	
	@EJB
	private PlacementSegmentHistoryServiceBean placementSegmentHistoryServiceBean;
	
	
	
	@ProcessAuditLogger(process=BusinessProcessType.GENERATE_PLACEMENT_SEGMENT_HISTORY)
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=20)
	public void generatePlacementSegmentHistory (Date fecha) throws ServiceException{
		
		praderaLogger.info(":::: INICIO REGISTRO DE HISTORICOS TRAMO DE COLOCACION ::::");
		
		//verificando que no se tengan registros en la fecha enviada
		if(placementSegmentHistoryServiceBean.getPlacementSegmentHistoryByDate(fecha).isEmpty()){
			placementSegmentHistoryServiceBean.generatePlacementSegmentHistory(fecha);
		}
		
		praderaLogger.info(":::: REGISTRO DE HISTORICOS TRAMO DE COLOCACION COMPLETADO ::::");
	}
	
}
