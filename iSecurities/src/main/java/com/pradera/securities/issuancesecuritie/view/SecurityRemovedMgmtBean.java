package com.pradera.securities.issuancesecuritie.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.event.Event;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.SecurityRemoved;
import com.pradera.model.issuancesecuritie.type.SecurityRemovedStateType;
import com.pradera.securities.issuancesecuritie.facade.SecurityServiceFacade;
import com.pradera.securities.issuancesecuritie.to.SecurityRemovedModelTO;
import com.pradera.securities.issuancesecuritie.util.PropertiesConstants;

/**
 * 
 * @author rlarico
 *
 */
@LoggerCreateBean
@DepositaryWebBean
public class SecurityRemovedMgmtBean extends GenericBaseBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// issue 891: navegacion de solicitud para la eliminacion de valor
	public static final String PAGE_REGISTER="newSecurityRemoved";
	public static final String PAGE_SEARCH="searchSecurityRemoved";
	public static final String PAGE_VIEW="viewSecurityRemoved";
	
	
	
	@Inject
	private UserInfo userInfo;
	
	
	@Inject
	private UserPrivilege userPrivilege;
	
	
	@Inject
	private transient PraderaLogger log;
	
	
	@EJB
	NotificationServiceFacade notificationServiceFacade;
	
	
	@EJB
	private AccountsFacade accountsFacade;
	
	
	@EJB
	private GeneralParametersFacade generalParametersFacade;
	
	
	@Inject
	protected Event<ExceptionToCatchEvent> excepcion;
	
	
	@EJB
	private SecurityServiceFacade ejbSecurityRemoved;
	
	private SecurityRemovedModelTO modelTOReg;

	private SecurityRemovedModelTO modelTOSearch;

	private SecurityRemovedModelTO modelTOView;
	
	private SecurityRemovedDataModel searchSecurityRemovedDataModel = new SecurityRemovedDataModel();
	private List<SecurityRemovedModelTO> lstSecurityRemovedTO;
	
	private boolean privilegeRegister;
	private boolean privilegeSearch;
	
	private boolean operationApprove;
	private boolean operationAnulate;
	private boolean operationConfirm;
	private boolean operationReject;
	
	@PostConstruct
	public void init() {
		loadSearch();
	}
	
	/** redirige a la pagina de registro
	 * 
	 * @return */
	public String goPageRegister() {
		loadRegiter();
		return PAGE_REGISTER;
	}
	
	/** redireige a la pnatalla de administracion
	 * 
	 * @return */
	public String goPageSearch() {
		loadSearch();
		return PAGE_SEARCH;
	}
	
	/** cargando la pantalla de registro */
	public void loadRegiter() {
		modelTOReg = new SecurityRemovedModelTO();
		modelTOReg.setLstCurrency(getLstCurrency());
	}
	
	/** limpiando la pantalla de registro */
	public void btnClearRegister() {
		loadRegiter();
		JSFUtilities.resetViewRoot();
	}
	
	/**
	 * metodo que se activa al ingresar un valor
	 * @throws ServiceException
	 */
	public void validateSecurity() throws ServiceException{
		if(Validations.validateIsNotNull(modelTOReg.getSecurity())){
			
			// validar si el valor esta en estado NO autorizado
			if (Validations.validateIsNull(modelTOReg.getSecurity().getIndAuthorized())
					|| modelTOReg.getSecurity().getIndAuthorized().equals(BooleanType.YES.getCode())) {
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.MSG_VALIDATION_REQUIRED_SECURITY_AUTHORIZED,null);
				JSFUtilities.showComponent("validationLocal");
				loadRegiter();
				return;
			}
			
			// validar que la fecha de registro del valor debe ser hoy
			if(Validations.validateIsNotNull(modelTOReg.getSecurity().getRegistryDate())){
				Date registryDateTrunc = CommonsUtilities.truncateDateTime(modelTOReg.getSecurity().getRegistryDate());
				Date date = CommonsUtilities.currentDate();
				if(registryDateTrunc.compareTo(date)!=0){
					showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MSG_VALIDATION_REQUIRED_REGISTRYDATE_EQUALS_CURRENTDATE, null);
					JSFUtilities.showComponent("validationLocal");
					loadRegiter();
					return;
				}
			}
			
			// si pasa las anteriores validaciones, extraer datos relevantes para el regitro de la validacion
			modelTOReg.setIdSecurityCodePk(modelTOReg.getSecurity().getIdSecurityCodePk());
			modelTOReg.setIssuanceDate(modelTOReg.getSecurity().getIssuanceDate());
			modelTOReg.setExpirationDate(modelTOReg.getSecurity().getExpirationDate());
			modelTOReg.setCurrency(modelTOReg.getSecurity().getCurrency());
			modelTOReg.setNominalValue(modelTOReg.getSecurity().getInitialNominalValue());
			modelTOReg.setInterestRateNominal(modelTOReg.getSecurity().getInterestRate());
			modelTOReg.setCouponNumber(modelTOReg.getSecurity().getNumberCoupons());
		}else{
			loadRegiter();
		}
		
		JSFUtilities.hideComponent("validationLocal");
		JSFUtilities.updateComponent("opnlDialogs");
	}


	
	/** antes de guardar registro de la solicitud */
	public void beforeSaveRegister() {
		// enviando el valor al mensaje de confirmacion de registro
		Object[] arrBodyData = { modelTOReg.getIdSecurityCodePk() };
		
		showMessageOnDialog(PropertiesConstants.MESSAGES_CONFIRM, null, PropertiesConstants.MSG_CONFIRM_REGISTER_SECURITYREMOVED, arrBodyData);
		JSFUtilities.showComponent("cnfRegiterrr");
	}
	
	/** guardando en BBDD la solicutd de eliminacion de valor */
	public void saveRegisterSecurityRemoved() {

		try {
			hideDialogsListener(null);

			// opteniendo las pistas de auditoria manualmente por si el processLogger esta en null en el facade
			modelTOReg.setAuditUserName(userInfo.getUserAccountSession().getUserName());
			modelTOReg.setAuditIpAddress(userInfo.getUserAccountSession().getIpAddress());
			modelTOReg.setAuditIdPrivilege(userPrivilege.getUserAcctions().getIdPrivilegeRegister().longValue());

			SecurityRemoved securityRemoved = ejbSecurityRemoved.regiterSecurityRemoved(modelTOReg);

			// notificando al usuario correspondiente
//			String descriptionParticipat="";
//			for(Participant p:modelTOReg.getLstParticipant()){
//				if(p.getIdParticipantPk().equals(modelTOReg.getIdParticipantSelected())){
//					descriptionParticipat = p.getDescription();
//					break;
//				}
//			}
//			Object[] parameters = { holderAnnulation.getIdAnnulationHolderPk().toString(), descriptionParticipat};
//			BusinessProcess businessProcessNotification = new BusinessProcess();
//			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_ANNULATION_CUI_REGISTER.getCode());
//			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
//			businessProcessNotification,
//			modelTOReg.getIdParticipantSelected(), parameters);
						
			btnSearch();

			// mensaje de exito
			Object[] arrBodyData = { String.valueOf(securityRemoved.getIdSecurityRemovedPk()) };
			showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, PropertiesConstants.MSG_SUCCESFUL_REGISTER_SECURITYREMOVED, arrBodyData);
			JSFUtilities.showComponent("cnfEndTransactionLocal");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/** metodo para cargar la pantalla de busqueda(administracion) */
	public void loadSearch() {

		modelTOSearch = new SecurityRemovedModelTO();
		if (userPrivilege.getUserAcctions().isRegister()) {
			log.info("::: tiene privilegios de registro");
			privilegeRegister = true;
		}
		if (userPrivilege.getUserAcctions().isSearch()) {
			log.info("::: tiene privilegios de busqueda");
			privilegeSearch = true;
		}

		modelTOSearch.setLstState(getLstStateSecurityRemoved());
		modelTOSearch.setLstCurrency(getLstCurrency());

		modelTOSearch.setInitialDate(new Date());
		modelTOSearch.setFinalDate(new Date());
		
		lstSecurityRemovedTO=new ArrayList<>();
		searchSecurityRemovedDataModel = new SecurityRemovedDataModel();
	}
	
	/** realizando la busqueda segun los criterios seleccionados */
	public void btnSearch() {
		try {
			lstSecurityRemovedTO = ejbSecurityRemoved. getLstSecurityRemovedModelTO(modelTOSearch);
			searchSecurityRemovedDataModel = new SecurityRemovedDataModel(lstSecurityRemovedTO);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/** limpiando la pantalla de search */
	public void btnClearSearch() {
		loadSearch();
		JSFUtilities.resetViewRoot();
	}
	
	/** lleva al detalle (pantalla de vista) un registro mediante su link
	 * 
	 * @param channelOp
	 * @return */
	public String linkView(SecurityRemovedModelTO securityRemove) {
		modelTOView = (SecurityRemovedModelTO) securityRemove.clone();
		modelTOView.setLstState(getLstStateSecurityRemoved());
		modelTOView.setLstCurrency(getLstCurrency());

		// oteniendo los demas datos para la pantalla de vista
		try {
			SecurityRemoved sr=ejbSecurityRemoved.findSecurityRemoved(modelTOView.getIdSecurityRemovedPk());
			modelTOView.setInterestRateNominal(sr.getInterestRateNominal());
			modelTOView.setCouponNumber(sr.getCouponNumber());
			modelTOView.setMotiveRemoved(sr.getMotiveRemoved());
		} catch (ServiceException e) {
			e.printStackTrace();
			log.info("::: se ha producido un error en el metodo SecurityServiceFacade.findSecurityRemoved " + e.getMessage());
			modelTOView.setInterestRateNominal(null);
			modelTOView.setCouponNumber(null);
			modelTOView.setMotiveRemoved("---");
		}

		renderFalseAllOperation();
		return PAGE_VIEW;
	}
	
	/** regresando a la pantalla de search desde la pantalla view
	 * 
	 * @return */
	public String backToSearchFromView() {
		btnSearch();
		return PAGE_SEARCH;
	}
	
	/** cargando datos para la pantalla de aprobacion
	 * 
	 * @return */
	public String loadApproved() {

		if (Validations.validateIsNotNull(modelTOView) && Validations.validateIsNotNull(modelTOView.getIdSecurityRemovedPk())) {
			// validando que el estado sea registrado
			if (modelTOView.getStateSecurityRemoved().equals(SecurityRemovedStateType.REGISTRADO.getCode())) {
				
				modelTOView.setLstState(getLstStateSecurityRemoved());
				modelTOView.setLstCurrency(getLstCurrency());
				// oteniendo los demas datos para la pantalla de vista
				try {
					SecurityRemoved sr=ejbSecurityRemoved.findSecurityRemoved(modelTOView.getIdSecurityRemovedPk());
					modelTOView.setInterestRateNominal(sr.getInterestRateNominal());
					modelTOView.setCouponNumber(sr.getCouponNumber());
					modelTOView.setMotiveRemoved(sr.getMotiveRemoved());
				} catch (ServiceException e) {
					e.printStackTrace();
					log.info("::: se ha producido un error en el metodo SecurityServiceFacade.findSecurityRemoved " + e.getMessage());
					modelTOView.setInterestRateNominal(null);
					modelTOView.setCouponNumber(null);
					modelTOView.setMotiveRemoved("---");
				}
				renderBtnApprove();
				return PAGE_VIEW;
			} else {
				// lanzando mensaje de que tiene q estar en estado registrado
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_SECURITYREMOVED_REQUIRED_STATEREGISTERED, null);
				JSFUtilities.updateComponent("opnlDialogs");
				JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
				return null;
			}
		} else {
			// lanzar mensaje de requerimiento de seleccion
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_SECURITYREMOVED_REQUIRED_SELECTED, null);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
			return null;
		}
	}
	
	/** antes de aprobar */
	public void beforeApprove() {
		String numberRequest = modelTOView.getIdSecurityRemovedPk().toString();

		Object[] arrBodyData = { numberRequest };
		showMessageOnDialog(PropertiesConstants.MESSAGES_CONFIRM, null, PropertiesConstants.LBL_MSG_CONFIRM_APPROVED_SECURITYREMOVED, arrBodyData);
		JSFUtilities.showComponent("cnfRegiterrr");
	}
	
	/** guardando en la BBDD la aprobacion de una apertura de canal
	 * 
	 * @throws Exception */
	public void saveApproveSecurityRemoved() throws Exception {

		try {
			hideDialogsListener(null);
			// opteniendo las pistas de auditoria manualmente por si el processLogger esta en null en el facade
			modelTOView.setAuditUserName(userInfo.getUserAccountSession().getUserName());
			modelTOView.setAuditIpAddress(userInfo.getUserAccountSession().getIpAddress());
			modelTOView.setAuditIdPrivilege(userPrivilege.getUserAcctions().getIdPrivilegeApprove().longValue());
			
			modelTOView.setStateSecurityRemoved(SecurityRemovedStateType.APROBADO.getCode());
			SecurityRemoved sr = ejbSecurityRemoved.changeStateSecurityRemoved(modelTOView);

			// envio de notificacion que se realizo unA APROBACIONS de solicitud de apertura de canal
//			String descriptionParticipat="";
//			for(Participant p:modelTOView.getLstParticipant()){
//				if(p.getIdParticipantPk().equals(modelTOView.getIdParticipantSelected())){
//					descriptionParticipat = p.getDescription();
//					break;
//				}
//			}
//			Object[] parameters = { String.valueOf(channelOpening.getIdChannelOpeningPk()), descriptionParticipat};
//			BusinessProcess businessProcessNotification = new BusinessProcess();
//			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_CHANNEL_OPENING_APPROVAL.getCode());
//			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
//			businessProcessNotification,
//			modelTOView.getIdParticipantSelected(), parameters);

			btnSearch();

			// mensaje de exito
			Object[] arrBodyData = { String.valueOf(sr.getIdSecurityRemovedPk()) };
			showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, PropertiesConstants.MSG_SUCCESFUL_APPROVED_SECURITYREMOVED, arrBodyData);
			JSFUtilities.showComponent("cnfEndTransactionLocal");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			//throw new Exception();
		}
	}
	
	/** cargando datos para la pantalla de anulacion
	 * 
	 * @return */
	public String loadAnulate() {

		if (Validations.validateIsNotNull(modelTOView) && Validations.validateIsNotNull(modelTOView.getIdSecurityRemovedPk())) {
			// validando que el estado sea registrado
			if (modelTOView.getStateSecurityRemoved().equals(SecurityRemovedStateType.REGISTRADO.getCode())) {
				
				modelTOView.setLstState(getLstStateSecurityRemoved());
				modelTOView.setLstCurrency(getLstCurrency());
				// oteniendo los demas datos para la pantalla de vista
				try {
					SecurityRemoved sr=ejbSecurityRemoved.findSecurityRemoved(modelTOView.getIdSecurityRemovedPk());
					modelTOView.setInterestRateNominal(sr.getInterestRateNominal());
					modelTOView.setCouponNumber(sr.getCouponNumber());
					modelTOView.setMotiveRemoved(sr.getMotiveRemoved());
				} catch (ServiceException e) {
					e.printStackTrace();
					log.info("::: se ha producido un error en el metodo SecurityServiceFacade.findSecurityRemoved " + e.getMessage());
					modelTOView.setInterestRateNominal(null);
					modelTOView.setCouponNumber(null);
					modelTOView.setMotiveRemoved("---");
				}
				renderBtnAnulate();
				return PAGE_VIEW;
			} else {
				// lanzando mensaje de que tiene q estar en estado registrado
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_SECURITYREMOVED_REQUIRED_STATEREGISTERED, null);
				JSFUtilities.updateComponent("opnlDialogs");
				JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
				return null;
			}
		} else {
			// lanzar mensaje de requerimiento de seleccion
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_SECURITYREMOVED_REQUIRED_SELECTED, null);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
			return null;
		}
	}
	
	/** antes de anular */
	public void beforeAnulate() {
		String numberRequest = modelTOView.getIdSecurityRemovedPk().toString();

		Object[] arrBodyData = { numberRequest };
		showMessageOnDialog(PropertiesConstants.MESSAGES_CONFIRM, null, PropertiesConstants.LBL_MSG_CONFIRM_ANNULATE_SECURITYREMOVED, arrBodyData);
		JSFUtilities.showComponent("cnfRegiterrr");
	}
	
	/** guardando en la BBDD la anulacion de eliminacion de valor
	 * 
	 * @throws Exception */
	public void saveAnnulateSecurityRemoved() throws Exception {

		try {
			hideDialogsListener(null);
			// opteniendo las pistas de auditoria manualmente por si el processLogger esta en null en el facade
			modelTOView.setAuditUserName(userInfo.getUserAccountSession().getUserName());
			modelTOView.setAuditIpAddress(userInfo.getUserAccountSession().getIpAddress());
			modelTOView.setAuditIdPrivilege(userPrivilege.getUserAcctions().getIdPrivilegeApprove().longValue());
			
			modelTOView.setStateSecurityRemoved(SecurityRemovedStateType.ANULADO.getCode());
			SecurityRemoved sr = ejbSecurityRemoved.changeStateSecurityRemoved(modelTOView);

			// envio de notificacion que se realizo unA APROBACIONS de solicitud de apertura de canal
//			String descriptionParticipat="";
//			for(Participant p:modelTOView.getLstParticipant()){
//				if(p.getIdParticipantPk().equals(modelTOView.getIdParticipantSelected())){
//					descriptionParticipat = p.getDescription();
//					break;
//				}
//			}
//			Object[] parameters = { String.valueOf(channelOpening.getIdChannelOpeningPk()), descriptionParticipat};
//			BusinessProcess businessProcessNotification = new BusinessProcess();
//			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_CHANNEL_OPENING_APPROVAL.getCode());
//			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
//			businessProcessNotification,
//			modelTOView.getIdParticipantSelected(), parameters);

			btnSearch();

			// mensaje de exito
			Object[] arrBodyData = { String.valueOf(sr.getIdSecurityRemovedPk()) };
			showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, PropertiesConstants.MSG_SUCCESFUL_ANNULATE_SECURITYREMOVED, arrBodyData);
			JSFUtilities.showComponent("cnfEndTransactionLocal");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/** cargando datos para la pantalla de rechazo
	 * 
	 * @return */
	public String loadReject() {

		if (Validations.validateIsNotNull(modelTOView) && Validations.validateIsNotNull(modelTOView.getIdSecurityRemovedPk())) {
			// validando que el estado sea aprobado
			if (modelTOView.getStateSecurityRemoved().equals(SecurityRemovedStateType.APROBADO.getCode())) {
				
				modelTOView.setLstState(getLstStateSecurityRemoved());
				modelTOView.setLstCurrency(getLstCurrency());
				// oteniendo los demas datos para la pantalla de vista
				try {
					SecurityRemoved sr=ejbSecurityRemoved.findSecurityRemoved(modelTOView.getIdSecurityRemovedPk());
					modelTOView.setInterestRateNominal(sr.getInterestRateNominal());
					modelTOView.setCouponNumber(sr.getCouponNumber());
					modelTOView.setMotiveRemoved(sr.getMotiveRemoved());
				} catch (ServiceException e) {
					e.printStackTrace();
					log.info("::: se ha producido un error en el metodo SecurityServiceFacade.findSecurityRemoved " + e.getMessage());
					modelTOView.setInterestRateNominal(null);
					modelTOView.setCouponNumber(null);
					modelTOView.setMotiveRemoved("---");
				}
				renderBtnReject();
				return PAGE_VIEW;
			} else {
				// lanzando mensaje de que tiene q estar en estado aprobado
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_SECURITYREMOVED_REQUIRED_STATEAPPROVED, null);
				JSFUtilities.updateComponent("opnlDialogs");
				JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
				return null;
			}
		} else {
			// lanzar mensaje de requerimiento de seleccion
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_SECURITYREMOVED_REQUIRED_SELECTED, null);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
			return null;
		}
	}
	
	public void beforeReject() {
		String numberRequest = modelTOView.getIdSecurityRemovedPk().toString();

		Object[] arrBodyData = { numberRequest };
		showMessageOnDialog(PropertiesConstants.MESSAGES_CONFIRM, null, PropertiesConstants.LBL_MSG_CONFIRM_REJECT_SECURITYREMOVED, arrBodyData);
		JSFUtilities.showComponent("cnfRegiterrr");
	}
	
	/** guardando en la BBDD la aprobacion de una apertura de canal
	 * 
	 * @throws Exception */
	public void saveRejectSecurityRemoved() throws Exception {

		try {
			hideDialogsListener(null);
			// opteniendo las pistas de auditoria manualmente por si el processLogger esta en null en el facade
			modelTOView.setAuditUserName(userInfo.getUserAccountSession().getUserName());
			modelTOView.setAuditIpAddress(userInfo.getUserAccountSession().getIpAddress());
			modelTOView.setAuditIdPrivilege(userPrivilege.getUserAcctions().getIdPrivilegeApprove().longValue());
			
			modelTOView.setStateSecurityRemoved(SecurityRemovedStateType.RECHAZADO.getCode());
			SecurityRemoved sr = ejbSecurityRemoved.changeStateSecurityRemoved(modelTOView);

			// envio de notificacion que se realizo unA APROBACIONS de solicitud de apertura de canal
//			String descriptionParticipat="";
//			for(Participant p:modelTOView.getLstParticipant()){
//				if(p.getIdParticipantPk().equals(modelTOView.getIdParticipantSelected())){
//					descriptionParticipat = p.getDescription();
//					break;
//				}
//			}
//			Object[] parameters = { String.valueOf(channelOpening.getIdChannelOpeningPk()), descriptionParticipat};
//			BusinessProcess businessProcessNotification = new BusinessProcess();
//			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_CHANNEL_OPENING_APPROVAL.getCode());
//			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
//			businessProcessNotification,
//			modelTOView.getIdParticipantSelected(), parameters);

			btnSearch();

			// mensaje de exito
			Object[] arrBodyData = { String.valueOf(sr.getIdSecurityRemovedPk()) };
			showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, PropertiesConstants.MSG_SUCCESFUL_REJECT_SECURITYREMOVED, arrBodyData);
			JSFUtilities.showComponent("cnfEndTransactionLocal");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/** cargando datos para la pantalla de Confirmacion
	 * 
	 * @return */
	public String loadConfirm() {

		if (Validations.validateIsNotNull(modelTOView) && Validations.validateIsNotNull(modelTOView.getIdSecurityRemovedPk())) {
			// validando que el estado sea aprobado
			if (modelTOView.getStateSecurityRemoved().equals(SecurityRemovedStateType.APROBADO.getCode())) {
				
				modelTOView.setLstState(getLstStateSecurityRemoved());
				modelTOView.setLstCurrency(getLstCurrency());
				// oteniendo los demas datos para la pantalla de vista
				try {
					SecurityRemoved sr=ejbSecurityRemoved.findSecurityRemoved(modelTOView.getIdSecurityRemovedPk());
					modelTOView.setInterestRateNominal(sr.getInterestRateNominal());
					modelTOView.setCouponNumber(sr.getCouponNumber());
					modelTOView.setMotiveRemoved(sr.getMotiveRemoved());
					
					// volviendo a validar la fecha y la NO autorizacion del valor antes de confirmar
					Security s=ejbSecurityRemoved.findSecurityOnly(modelTOView.getIdSecurityCodePk());
					if(Validations.validateIsNotNull(s)){
						if (Validations.validateIsNull(s.getIndAuthorized())
								|| s.getIndAuthorized().equals(BooleanType.YES.getCode())) {
							showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT,null,PropertiesConstants.MSG_VALIDATION_REQUIRED_SECURITY_AUTHORIZED,null);
							JSFUtilities.updateComponent("opnlDialogs");
							JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
							return null;
						}
						// validar que la fecha de registro del valor debe ser hoy
						if(Validations.validateIsNotNull(s.getRegistryDate())){
							Date registryDateTrunc = CommonsUtilities.truncateDateTime(s.getRegistryDate());
							Date date = CommonsUtilities.currentDate();
							if(registryDateTrunc.compareTo(date)!=0){
								showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MSG_VALIDATION_REQUIRED_REGISTRYDATE_EQUALS_CURRENTDATE, null);
								JSFUtilities.updateComponent("opnlDialogs");
								JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
								return null;
							}
						}
					}
				} catch (ServiceException e) {
					e.printStackTrace();
					log.info("::: se ha producido un error en el metodo SecurityServiceFacade.findSecurityRemoved " + e.getMessage());
					modelTOView.setInterestRateNominal(null);
					modelTOView.setCouponNumber(null);
					modelTOView.setMotiveRemoved("---");
				}
				renderBtnConfirm();
				return PAGE_VIEW;
			} else {
				// lanzando mensaje de que tiene q estar en estado APROBADO
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_SECURITYREMOVED_REQUIRED_STATEAPPROVED, null);
				JSFUtilities.updateComponent("opnlDialogs");
				JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
				return null;
			}
		} else {
			// lanzar mensaje de requerimiento de seleccion
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_SECURITYREMOVED_REQUIRED_SELECTED, null);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
			return null;
		}
	}
	
	public void beforeConfirm() {
		String numberRequest = modelTOView.getIdSecurityRemovedPk().toString();

		Object[] arrBodyData = { numberRequest };
		showMessageOnDialog(PropertiesConstants.MESSAGES_CONFIRM, null, PropertiesConstants.LBL_MSG_CONFIRM_CONFIRMED_SECURITYREMOVED, arrBodyData);
		JSFUtilities.showComponent("cnfRegiterrr");
	}
	
	/** guardando en la BBDD CONFIRMACION de eliminacion de valor
	 * 
	 * @throws Exception */
	public void saveConfirmSecurityRemoved() throws Exception {

		try {
			hideDialogsListener(null);
			// opteniendo las pistas de auditoria manualmente por si el processLogger esta en null en el facade
			modelTOView.setAuditUserName(userInfo.getUserAccountSession().getUserName());
			modelTOView.setAuditIpAddress(userInfo.getUserAccountSession().getIpAddress());
			modelTOView.setAuditIdPrivilege(userPrivilege.getUserAcctions().getIdPrivilegeApprove().longValue());
			
			modelTOView.setStateSecurityRemoved(SecurityRemovedStateType.CONFIRMADO.getCode());
			SecurityRemoved sr = ejbSecurityRemoved.changeStateSecurityRemoved(modelTOView);

			// envio de notificacion que se realizo unA APROBACIONS de solicitud de apertura de canal
//			String descriptionParticipat="";
//			for(Participant p:modelTOView.getLstParticipant()){
//				if(p.getIdParticipantPk().equals(modelTOView.getIdParticipantSelected())){
//					descriptionParticipat = p.getDescription();
//					break;
//				}
//			}
//			Object[] parameters = { String.valueOf(channelOpening.getIdChannelOpeningPk()), descriptionParticipat};
//			BusinessProcess businessProcessNotification = new BusinessProcess();
//			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_CHANNEL_OPENING_APPROVAL.getCode());
//			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
//			businessProcessNotification,
//			modelTOView.getIdParticipantSelected(), parameters);

			btnSearch();

			// mensaje de exito
			Object[] arrBodyData = { String.valueOf(sr.getIdSecurityRemovedPk()) };
			showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, PropertiesConstants.MSG_SUCCESFUL_CONFIRMED_SECURITYREMOVED, arrBodyData);
			JSFUtilities.showComponent("cnfEndTransactionLocal");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	public void hideDialogsListener(ActionEvent actionEvent) {
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		JSFUtilities.hideGeneralDialogues();

		JSFUtilities.hideComponent("cnfRegiterrr");
		JSFUtilities.hideComponent("validationLocal");
		// JSFUtilities.hideComponent("detailRepresentativeDialog");
	}
	
	public void saveOperationChangeState() throws Exception {
		if (operationApprove) {
			saveApproveSecurityRemoved();
			return;
		}
		if (operationAnulate) {
			saveAnnulateSecurityRemoved();
			return;
		}
		if (operationConfirm) {
			saveConfirmSecurityRemoved();
			return;
		}
		if (operationReject) {
			saveRejectSecurityRemoved();
			return;
		}
	}
	
	// TODO metodos genericos para cargar las listas(combobox) necesarios para cualquier pantalla (registro, vista, search)
	private List<ParameterTable> getLstStateSecurityRemoved() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.STATUS_SECURITY_REMOVED.getCode());
			return generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
			return new ArrayList<>();
		}
	}
	
	private List<ParameterTable> getLstCurrency() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			return generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
			return new ArrayList<>();
		}
	}
	
	// TODO metodos para el renderizados de los botones de las operaciones (aprobado, anulado, confirmado, rechzado)
	public void renderBtnApprove() {
		renderFalseAllOperation();
		operationApprove = true;
	}

	public void renderBtnAnulate() {
		renderFalseAllOperation();
		operationAnulate = true;
	}

	public void renderBtnConfirm() {
		renderFalseAllOperation();
		operationConfirm = true;
	}

	public void renderBtnReject() {
		renderFalseAllOperation();
		operationReject = true;
	}
	
	private void renderFalseAllOperation() {
		operationApprove = false;
		operationAnulate = false;
		operationConfirm = false;
		operationReject = false;
	}

	public SecurityRemovedModelTO getModelTOReg() {
		return modelTOReg;
	}

	public void setModelTOReg(SecurityRemovedModelTO modelTOReg) {
		this.modelTOReg = modelTOReg;
	}

	public SecurityRemovedModelTO getModelTOSearch() {
		return modelTOSearch;
	}

	public void setModelTOSearch(SecurityRemovedModelTO modelTOSearch) {
		this.modelTOSearch = modelTOSearch;
	}

	public SecurityRemovedModelTO getModelTOView() {
		return modelTOView;
	}

	public void setModelTOView(SecurityRemovedModelTO modelTOView) {
		this.modelTOView = modelTOView;
	}

	public SecurityRemovedDataModel getSearchSecurityRemovedDataModel() {
		return searchSecurityRemovedDataModel;
	}

	public void setSearchSecurityRemovedDataModel(SecurityRemovedDataModel searchSecurityRemovedDataModel) {
		this.searchSecurityRemovedDataModel = searchSecurityRemovedDataModel;
	}

	public List<SecurityRemovedModelTO> getLstSecurityRemovedTO() {
		return lstSecurityRemovedTO;
	}

	public void setLstSecurityRemovedTO(List<SecurityRemovedModelTO> lstSecurityRemovedTO) {
		this.lstSecurityRemovedTO = lstSecurityRemovedTO;
	}

	public boolean isPrivilegeRegister() {
		return privilegeRegister;
	}

	public void setPrivilegeRegister(boolean privilegeRegister) {
		this.privilegeRegister = privilegeRegister;
	}

	public boolean isPrivilegeSearch() {
		return privilegeSearch;
	}

	public void setPrivilegeSearch(boolean privilegeSearch) {
		this.privilegeSearch = privilegeSearch;
	}
}