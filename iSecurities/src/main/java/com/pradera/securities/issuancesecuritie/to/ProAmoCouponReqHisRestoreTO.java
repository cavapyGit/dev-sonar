package com.pradera.securities.issuancesecuritie.to;

import java.math.BigDecimal;

/** clase TO para guardar parcialmente los montos de los cupones que seran modificados en la cuponera de amortizaciones
 * 
 * @author rlarico */
public class ProAmoCouponReqHisRestoreTO {

	private Integer couponNumberGuard;
	private BigDecimal amortizationAmountGuard;
	private BigDecimal amortizationFactorGuard;

	/** custom constructor
	 * 
	 * @param couponNumberGuard
	 * @param amortizationAmountGuard
	 * @param amortizationFactorGuard */
	public ProAmoCouponReqHisRestoreTO(Integer couponNumberGuard, BigDecimal amortizationAmountGuard, BigDecimal amortizationFactorGuard) {
		this.couponNumberGuard = couponNumberGuard;
		this.amortizationAmountGuard = amortizationAmountGuard;
		this.amortizationFactorGuard = amortizationFactorGuard;
	}

	// TODO methods getter and setter
	public Integer getCouponNumberGuard() {
		return couponNumberGuard;
	}

	public void setCouponNumberGuard(Integer couponNumberGuard) {
		this.couponNumberGuard = couponNumberGuard;
	}

	public BigDecimal getAmortizationAmountGuard() {
		return amortizationAmountGuard;
	}

	public void setAmortizationAmountGuard(BigDecimal amortizationAmountGuard) {
		this.amortizationAmountGuard = amortizationAmountGuard;
	}

	public BigDecimal getAmortizationFactorGuard() {
		return amortizationFactorGuard;
	}

	public void setAmortizationFactorGuard(BigDecimal amortizationFactorGuard) {
		this.amortizationFactorGuard = amortizationFactorGuard;
	}

	@Override
	public String toString() {
		return "ProAmoCouponReqHisRestoreTO [couponNumberGuard=" + couponNumberGuard + ", amortizationAmountGuard=" + amortizationAmountGuard
				+ ", amortizationFactorGuard=" + amortizationFactorGuard + "]";
	}
}