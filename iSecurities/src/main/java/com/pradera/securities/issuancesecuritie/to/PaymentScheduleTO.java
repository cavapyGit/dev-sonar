package com.pradera.securities.issuancesecuritie.to;

import java.io.Serializable;
import java.util.Date;

import com.pradera.model.issuancesecuritie.Issuance;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PaymentScheduleTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class PaymentScheduleTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The rdo filter issuer. */
	public String rdoFilterIssuer="I";
	
	/** The rdo filter security class code. */
	public String rdoFilterSecurityClassCode="S";
	
	/** The rdo filter request number. */
	public String rdoFilterRequestNumber="R";
	
	/** The id payment schedule req hi pk. */
	private Long idPaymentScheduleReqHiPk;
	
	/** The id issuer pk. */
	private String idIssuerPk;
	
	/** The issuance. */
	private Issuance issuance;
	
	/** The id isin code. */
	private String idIsinCode;
	
	/** The id security code pk. */
	private String idSecurityCodePk;
	
	/** The security description. */
	private String securityDescription;
	
	/** The payment schedule type. */
	private Integer paymentScheduleType;
	
	/** The registry type. */
	private Integer registryType;
	
	/** The request state. */
	private Integer requestState;
	
	/** The begin date. */
	private Date beginDate;
	
	/** The end date. */
	private Date endDate;
	
	/** The rdo filter. */
	private String rdoFilter;
	
	/** The security class. */
	private Integer securityClass;
	
	/**
	 * Instantiates a new payment schedule to.
	 */
	public PaymentScheduleTO() {
		issuance=new Issuance();
		rdoFilter=rdoFilterRequestNumber;
	}
	
	/**
	 * Change rdo filter payment schedule mgmt.
	 *
	 * @throws Exception the exception
	 */
	public void changeRdoFilterPaymentScheduleMgmt() throws Exception{
			this.setIdIssuerPk(null);
			this.setIssuance(new Issuance());
			this.setIdPaymentScheduleReqHiPk(null);
			this.setSecurityDescription(null);
			this.setIdSecurityCodePk(null);
			this.setSecurityClass(null);
//		}
	}
	
	/**
	 * Gets the id issuer pk.
	 *
	 * @return the id issuer pk
	 */
	public String getIdIssuerPk() {
		return idIssuerPk;
	}
	
	/**
	 * Sets the id issuer pk.
	 *
	 * @param idIssuerPk the new id issuer pk
	 */
	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}
	
	/**
	 * Gets the issuance.
	 *
	 * @return the issuance
	 */
	public Issuance getIssuance() {
		return issuance;
	}

	/**
	 * Sets the issuance.
	 *
	 * @param issuance the new issuance
	 */
	public void setIssuance(Issuance issuance) {
		this.issuance = issuance;
	}

	/**
	 * Gets the id isin code.
	 *
	 * @return the id isin code
	 */
	public String getIdIsinCode() {
		return idIsinCode;
	}
	
	/**
	 * Sets the id isin code.
	 *
	 * @param idIsinCode the new id isin code
	 */
	public void setIdIsinCode(String idIsinCode) {
		this.idIsinCode = idIsinCode;
	}
	
	/**
	 * Gets the security description.
	 *
	 * @return the security description
	 */
	public String getSecurityDescription() {
		return securityDescription;
	}
	
	/**
	 * Sets the security description.
	 *
	 * @param securityDescription the new security description
	 */
	public void setSecurityDescription(String securityDescription) {
		this.securityDescription = securityDescription;
	}
	
	/**
	 * Gets the payment schedule type.
	 *
	 * @return the payment schedule type
	 */
	public Integer getPaymentScheduleType() {
		return paymentScheduleType;
	}
	
	/**
	 * Sets the payment schedule type.
	 *
	 * @param paymentScheduleType the new payment schedule type
	 */
	public void setPaymentScheduleType(Integer paymentScheduleType) {
		this.paymentScheduleType = paymentScheduleType;
	}

	/**
	 * Gets the id payment schedule req hi pk.
	 *
	 * @return the id payment schedule req hi pk
	 */
	public Long getIdPaymentScheduleReqHiPk() {
		return idPaymentScheduleReqHiPk;
	}

	/**
	 * Sets the id payment schedule req hi pk.
	 *
	 * @param idPaymentScheduleReqHiPk the new id payment schedule req hi pk
	 */
	public void setIdPaymentScheduleReqHiPk(Long idPaymentScheduleReqHiPk) {
		this.idPaymentScheduleReqHiPk = idPaymentScheduleReqHiPk;
	}

	/**
	 * Gets the registry type.
	 *
	 * @return the registry type
	 */
	public Integer getRegistryType() {
		return registryType;
	}

	/**
	 * Sets the registry type.
	 *
	 * @param registryType the new registry type
	 */
	public void setRegistryType(Integer registryType) {
		this.registryType = registryType;
	}

	/**
	 * Checks if is interest payment schedule.
	 *
	 * @return true, if is interest payment schedule
	 */
	public boolean isInterestPaymentSchedule(){
		return paymentScheduleType.equals(Integer.valueOf(1));
	}
	
	/**
	 * Checks if is amortization payment schedule.
	 *
	 * @return true, if is amortization payment schedule
	 */
	public boolean isAmortizationPaymentSchedule(){
		return paymentScheduleType.equals(Integer.valueOf(2));
	}

	/**
	 * Gets the request state.
	 *
	 * @return the request state
	 */
	public Integer getRequestState() {
		return requestState;
	}

	/**
	 * Sets the request state.
	 *
	 * @param requestState the new request state
	 */
	public void setRequestState(Integer requestState) {
		this.requestState = requestState;
	}

	/**
	 * Gets the begin date.
	 *
	 * @return the begin date
	 */
	public Date getBeginDate() {
		return beginDate;
	}

	/**
	 * Sets the begin date.
	 *
	 * @param beginDate the new begin date
	 */
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * Gets the id security code pk.
	 *
	 * @return the id security code pk
	 */
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	/**
	 * Sets the id security code pk.
	 *
	 * @param idSecurityCodePk the new id security code pk
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
	
	/**
	 * Gets the rdo filter issuer.
	 *
	 * @return the rdo filter issuer
	 */
	public String getRdoFilterIssuer() {
		return rdoFilterIssuer;
	}

	/**
	 * Sets the rdo filter issuer.
	 *
	 * @param rdoFilterIssuer the new rdo filter issuer
	 */
	public void setRdoFilterIssuer(String rdoFilterIssuer) {
		this.rdoFilterIssuer = rdoFilterIssuer;
	}

	/**
	 * Gets the rdo filter security class code.
	 *
	 * @return the rdo filter security class code
	 */
	public String getRdoFilterSecurityClassCode() {
		return rdoFilterSecurityClassCode;
	}

	/**
	 * Sets the rdo filter security class code.
	 *
	 * @param rdoFilterSecurityClassCode the new rdo filter security class code
	 */
	public void setRdoFilterSecurityClassCode(String rdoFilterSecurityClassCode) {
		this.rdoFilterSecurityClassCode = rdoFilterSecurityClassCode;
	}

	/**
	 * Gets the rdo filter.
	 *
	 * @return the rdo filter
	 */
	public String getRdoFilter() {
		return rdoFilter;
	}

	/**
	 * Sets the rdo filter.
	 *
	 * @param rdoFilter the new rdo filter
	 */
	public void setRdoFilter(String rdoFilter) {
		this.rdoFilter = rdoFilter;
	}

	
	
	/**
	 * Gets the security class.
	 *
	 * @return the security class
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}

	/**
	 * Sets the security class.
	 *
	 * @param securityClass the new security class
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	/**
	 * Checks if is rdo filter issuer sel.
	 *
	 * @return true, if is rdo filter issuer sel
	 */
	public boolean isRdoFilterIssuerSel(){
		if(rdoFilterIssuer.equals(rdoFilter)){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is rdo filter security class code sel.
	 *
	 * @return true, if is rdo filter security class code sel
	 */
	public boolean isRdoFilterSecurityClassCodeSel(){
		if(rdoFilterSecurityClassCode.equals(rdoFilter)){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is rdo filter request number sel.
	 *
	 * @return true, if is rdo filter request number sel
	 */
	public boolean isRdoFilterRequestNumberSel(){
		if(rdoFilterRequestNumber.equals(rdoFilter)){
			return true;
		}
		return false;	
	}

	/**
	 * Gets the rdo filter request number.
	 *
	 * @return the rdo filter request number
	 */
	public String getRdoFilterRequestNumber() {
		return rdoFilterRequestNumber;
	}

	/**
	 * Sets the rdo filter request number.
	 *
	 * @param rdoFilterRequestNumber the new rdo filter request number
	 */
	public void setRdoFilterRequestNumber(String rdoFilterRequestNumber) {
		this.rdoFilterRequestNumber = rdoFilterRequestNumber;
	}
	
	
	
}