package com.pradera.securities.issuancesecuritie.view;

import java.io.Serializable;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;
import com.pradera.model.issuancesecuritie.IssuanceAux;

public class IssuanceAuxDataModel extends ListDataModel<IssuanceAux> implements SelectableDataModel<IssuanceAux>, Serializable{

	/**
	 * Instantiates a new issuance data model.
	 */
	public IssuanceAuxDataModel(){
		super();
	}
	
	/**
	 * Instantiates a new issuance data model.
	 *
	 * @param data the data
	 */
	public IssuanceAuxDataModel(List<IssuanceAux> data){
		super(data);
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(IssuanceAux issuane) {
		// TODO Auto-generated method stub
		return issuane.getIdIssuanceCodePk();
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public IssuanceAux getRowData(String rowKey) {
		// TODO Auto-generated method stub
		List<IssuanceAux> lstIssuance=(List<IssuanceAux>)getWrappedData();
		for(IssuanceAux issuance:lstIssuance){
			if(issuance.getIdIssuanceCodePk().equals( rowKey ))
				return issuance;
		}
		return null;
	}
	
	


}
