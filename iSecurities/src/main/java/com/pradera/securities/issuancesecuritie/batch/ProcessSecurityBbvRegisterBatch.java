package com.pradera.securities.issuancesecuritie.batch;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.process.ProcessLogger;
import com.pradera.securities.issuancesecuritie.facade.ProcessSecurityBbvFacade;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MassiveIssuanceRegisterBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19-ago-2015
 */
@BatchProcess(name="ProcessSecurityBbvRegisterBatch")
@RequestScoped
public class ProcessSecurityBbvRegisterBatch implements Serializable,JobExecution {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;
	
	/** The security service facade. */
	@EJB
	ProcessSecurityBbvFacade processSecurityBbvFacade;

	/* (non-Javadoc)
	 * Batchero para registrar masivamente la emisiones a la BBV
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void startJob(ProcessLogger processLogger) {
		
		System.out.println("************************************* BATCH ENVIO DE DPF A LA BBV ******************************************************");
		
		try {
			Date fechaActual = CommonsUtilities.currentDate();
			
			Object[] result = processSecurityBbvFacade.listSecurityAvailable(fechaActual);
			
			List<Security> listSecurity =(List<Security>)result[0]; 
			
			// DPFs revertidos
//			List<SecuritySendBbv> listSecurityRedention = (List<SecuritySendBbv>)result[1];
			processSecurityBbvFacade.serachAndProcess(listSecurity,fechaActual);
			
			System.out.println("************************************* FIN BATCH ENVIO DE DPF A LA BBV ******************************************************");
				
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/* (non-Javadoc)
	 * Metodo implementado para obtener los parametros de notificaciones
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * Metodo implementado para obtener a los destinatarios
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * Metodo Implementado para enviar las notificaciones
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}
