package com.pradera.securities.issuancesecuritie.to;

import java.io.Serializable;
import java.util.Date;

public class ResumeSecuritySendBbvTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer numberProcess;
	private Integer sended;
	private Integer registered;
	private Integer errorReception;
	private Date dateProcess;
	
	public Integer getNumberProcess() {
		return numberProcess;
	}
	public void setNumberProcess(Integer numberProcess) {
		this.numberProcess = numberProcess;
	}
	public Integer getSended() {
		return sended;
	}
	public void setSended(Integer sended) {
		this.sended = sended;
	}
	public Integer getRegistered() {
		return registered;
	}
	public void setRegistered(Integer registered) {
		this.registered = registered;
	}
	public Integer getErrorReception() {
		return errorReception;
	}
	public void setErrorReception(Integer errorReception) {
		this.errorReception = errorReception;
	}
	public Date getDateProcess() {
		return dateProcess;
	}
	public void setDateProcess(Date dateProcess) {
		this.dateProcess = dateProcess;
	}
	
	
	
}
