package com.pradera.securities.issuancesecuritie.view;


import java.io.Serializable;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.apache.myfaces.extensions.cdi.core.api.scope.conversation.ConversationGroup;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryGroupConversation;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.securities.issuancesecuritie.facade.IssuanceSecuritiesServiceFacade;
import com.pradera.securities.issuancesecuritie.facade.SecurityServiceFacade;
import com.pradera.securities.issuancesecuritie.to.CouponInformationTO;
import com.pradera.securities.issuancesecuritie.util.PropertiesConstants;
import com.pradera.securities.issuer.to.IssuerTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SecurityAndPaymentCronoQueryBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class SecurityAndPaymentCronoQueryBean extends GenericBaseBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7696311037770317274L;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The event model. */
	private ScheduleModel eventModel;
	
	/** The month year selected. */
	private String monthYearSelected;
	
	/** The lst coupon information to. */
	private List<CouponInformationTO> lstCouponInformationTO;
	
	/** The security service facade. */
	@EJB
	private SecurityServiceFacade securityServiceFacade;
	
	/** The issuance securities service facade. */
	@EJB
	private IssuanceSecuritiesServiceFacade issuanceSecuritiesServiceFacade;
	
	/** The security mgmt bean. */
	@Inject @ConversationGroup(DepositaryGroupConversation.class)
	SecurityMgmtBean securityMgmtBean;
	
	/** The initial month date schedule. */
	private Date initialMonthDateSchedule;	
	
	/** The lst coupons date. */
	private List<Date> lstCouponsDate;
	
	/** The security helper search. */
	private Security securityHelperSearch;
	
	/** The issuer helper search. */
	private Issuer issuerHelperSearch;
	
	/** The security to. */
	private SecurityTO securityTO;
	
//	private TimeZone timeZone;
	
	 private String timeZone = ZoneId.systemDefault().toString();
	

	/**
	 * Instantiates a new security and payment crono query bean.
	 */
	public SecurityAndPaymentCronoQueryBean() {
	}
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		try {
			securityTO=new SecurityTO();
			securityHelperSearch=new Security();
			issuerHelperSearch=new Issuer();
			securityTO.setIssuanceDate(new Date());
			securityTO.setExpirationDate(new Date());
			initializateMinDateToday();			
			listCouponsDate();
			listCouponsSchedule();
			if (userInfo.getUserAccountSession().isIssuerInstitucion()) {
				IssuerTO issuerTO= new IssuerTO();
				issuerTO.setIdIssuerPk(userInfo.getUserAccountSession().getIssuerCode());
				issuerHelperSearch= issuanceSecuritiesServiceFacade.findIssuerServiceFacade(issuerTO);
			}
		
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));		
		}
	}
	
	/**
	 * List coupons schedule.
	 */
	private void listCouponsSchedule(){
		eventModel = new DefaultScheduleModel();
		if(Validations.validateListIsNotNullAndNotEmpty(lstCouponsDate)) {
			
			DefaultScheduleEvent scheduleEvent = null;
			
			for (Date couponDate : lstCouponsDate) {
				scheduleEvent = new DefaultScheduleEvent(
						PropertiesUtilities.getMessage(PropertiesConstants.LABEL_VIEW), couponDate, couponDate);
				scheduleEvent.setAllDay(true);
				scheduleEvent.setData(couponDate);
				
				eventModel.addEvent(scheduleEvent);
			}
		}
	}
	
	/**
	 * List coupons date.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listCouponsDate() throws ServiceException{
		Calendar calInitialDateYear = Calendar.getInstance();
		calInitialDateYear.setTime(CommonsUtilities.copyDate(initialMonthDateSchedule));
		calInitialDateYear.set(Calendar.MONTH, Calendar.JANUARY);
		calInitialDateYear.set(Calendar.DAY_OF_MONTH, calInitialDateYear.getActualMinimum(Calendar.DAY_OF_MONTH));		
		
		Calendar calFinalDateYear = Calendar.getInstance();
		calFinalDateYear.setTime(CommonsUtilities.copyDate(initialMonthDateSchedule));
		calFinalDateYear.set(Calendar.MONTH, Calendar.DECEMBER);
		calFinalDateYear.set(Calendar.DAY_OF_MONTH, calFinalDateYear.getActualMaximum(Calendar.DAY_OF_MONTH));
		
		
		Date initialDateYear = calInitialDateYear.getTime();
		Date finalDateYear = calFinalDateYear.getTime();
		
		lstCouponsDate = securityServiceFacade.
				findDatesProgramAmortizationAndInterestCoupons(initialDateYear, finalDateYear);
	}
	
	/**
	 * Initializate min date today.
	 */
	private void initializateMinDateToday(){
		Calendar calCurrentDate = Calendar.getInstance();	    
	    calCurrentDate.set(Calendar.DAY_OF_MONTH, 1);
	    
	    initialMonthDateSchedule = calCurrentDate.getTime();
	}
	
	/**
	 * Select date listener.
	 *
	 * @param selectEvent the select event
	 */
	public void selectDateListener(SelectEvent selectEvent) {
		try {
			
			hideDialogs();
			
			Date selectedDate = (Date) selectEvent.getObject();
			securityTO=new SecurityTO();
			securityTO.setSelectDate(selectedDate);
			lstCouponInformationTO = securityServiceFacade.findCouponInformationByDate(securityTO);
			
			lstCouponInformationTO = lstCouponInformationTO.stream()
					.filter(security -> !security.getStateSecurity().equals(SecurityStateType.RETIREMENT.getCode()))
					.collect(Collectors.toList());
			
			if(Validations.validateListIsNotNullAndNotEmpty(lstCouponInformationTO)){
				FacesContext.getCurrentInstance().getExternalContext().redirect("securitiesAndPayCronoDetail.xhtml");
			} else {
				JSFUtilities.showMessageOnDialog(
						PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.LABEL_NO_COUPONS_PAYMENT_CRON_QUERY, null);
				JSFUtilities.showValidationDialog();
			}
		
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));		
		}
	}
	
	/**
	 * Select event listener.
	 *
	 * @param selectEvent the select event
	 */
	public void selectEventListener(SelectEvent selectEvent) {	
		
		try {
		hideDialogs();
		
		ScheduleEvent event = (ScheduleEvent) selectEvent.getObject();
		
		Date selectedDate = (Date) event.getData();
		securityTO=new SecurityTO();
		securityTO.setSelectDate(selectedDate);
		lstCouponInformationTO = securityServiceFacade.findCouponInformationByDate(securityTO);
		
		lstCouponInformationTO = lstCouponInformationTO.stream()
				.filter(security -> !security.getStateSecurity().equals(SecurityStateType.RETIREMENT.getCode()))
				.collect(Collectors.toList());
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstCouponInformationTO)){
			FacesContext.getCurrentInstance().getExternalContext().redirect("securitiesAndPayCronoDetail.xhtml");
		} else {
			JSFUtilities.showMessageOnDialog(
					PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.LABEL_NO_COUPONS_PAYMENT_CRON_QUERY, null);
			JSFUtilities.showValidationDialog();
		}
		
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));		
		}
	}
	
	/**
	 * Search payment schedules.
	 */
	public void searchPaymentSchedules(){
		try {
			securityTO.setSelectDate(null);
			securityTO.setIdIssuerCodePk(issuerHelperSearch.getIdIssuerPk());
			securityTO.setIdSecurityCodePk( securityHelperSearch.getIdSecurityCodePk() );
			lstCouponInformationTO = securityServiceFacade.findCouponInformationByDate(securityTO);
			
			lstCouponInformationTO = lstCouponInformationTO.stream().filter(security -> !security.getStateSecurity().equals(SecurityStateType.RETIREMENT.getCode())).collect(Collectors.toList());
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clean search.
	 */
	public void cleanSearch(){
		if (!userInfo.getUserAccountSession().isIssuerInstitucion()) {
			issuerHelperSearch = new Issuer();
		}			
		securityHelperSearch=new Security();
		securityTO=new SecurityTO();
		securityTO.setIssuanceDate(new Date());
		securityTO.setExpirationDate(new Date());
		lstCouponInformationTO=new ArrayList<CouponInformationTO>();
	}
	
	/**
	 * Detail operation handler.
	 *
	 * @param event the event
	 */
	public void detailOperationHandler(ActionEvent event) {
		try {
			String idSecurityCode = (String)event.getComponent().getAttributes().get("idSecurityCodePrm");
			
			Security security = new Security(idSecurityCode);
			
			securityMgmtBean.setSecurity(security);
			securityMgmtBean.modificationOperationHandler();
			securityMgmtBean.setRedirectFromPaymentCronoQuery(Boolean.TRUE);
			securityMgmtBean.setViewOperationType(ViewOperationsType.DETAIL.getCode());
						
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Detail operation adv handler.
	 *
	 * @param event the event
	 */
	public void detailOperationAdvHandler(ActionEvent event) {
		try {
			String idSecurityCode = (String)event.getComponent().getAttributes().get("idSecurityCodePrm");
			
			Security security = new Security(idSecurityCode);
			
			securityMgmtBean.setSecurity(security);
			securityMgmtBean.modificationOperationHandler();
			securityMgmtBean.setRedirectFromPaymentCronoAdvQuery(Boolean.TRUE);
			securityMgmtBean.setViewOperationType(ViewOperationsType.DETAIL.getCode());
						
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change final date.
	 */
	public void changeFinalDate(){
		securityTO.setIssuanceDate(null);
	}
	
	/**
	 * Change month.
	 *
	 * @param month the month
	 */
	public void changeMonth(String month) {	      	      	      
	      
	      Calendar calSelectedMonth = Calendar.getInstance();
	      Calendar calCurrentDate = Calendar.getInstance();
	      calSelectedMonth.setTime(initialMonthDateSchedule);
	      
	      if( month.equals("previous") ) {
	         calSelectedMonth.add(Calendar.MONTH, -1);
	      }
	      else if ( month.equals("next") ) {
	         calSelectedMonth.add(Calendar.MONTH, 1);
	            
	      }
	      else {
	         calSelectedMonth = calCurrentDate;
	      }
	      
	      initialMonthDateSchedule = calSelectedMonth.getTime();	      
	      
	   }
	
	/**
	 * Hide dialogs.
	 */
	public void hideDialogs(){
		executeAction();
		JSFUtilities.hideGeneralDialogues();
	}

	/**
	 * Gets the event model.
	 *
	 * @return the event model
	 */
	public ScheduleModel getEventModel() {
		return eventModel;
	}

	/**
	 * Sets the event model.
	 *
	 * @param eventModel the new event model
	 */
	public void setEventModel(ScheduleModel eventModel) {
		this.eventModel = eventModel;
	}

	/**
	 * Gets the month year selected.
	 *
	 * @return the month year selected
	 */
	public String getMonthYearSelected() {
		return monthYearSelected;
	}

	/**
	 * Sets the month year selected.
	 *
	 * @param monthYearSelected the new month year selected
	 */
	public void setMonthYearSelected(String monthYearSelected) {
		this.monthYearSelected = monthYearSelected;
	}

	/**
	 * Gets the lst coupon information to.
	 *
	 * @return the lst coupon information to
	 */
	public List<CouponInformationTO> getLstCouponInformationTO() {
		return lstCouponInformationTO;
	}

	/**
	 * Sets the lst coupon information to.
	 *
	 * @param lstCouponInformationTO the new lst coupon information to
	 */
	public void setLstCouponInformationTO(
			List<CouponInformationTO> lstCouponInformationTO) {
		this.lstCouponInformationTO = lstCouponInformationTO;
	}

	/**
	 * Gets the initial month date schedule.
	 *
	 * @return the initial month date schedule
	 */
	public Date getInitialMonthDateSchedule() {
		return initialMonthDateSchedule;
	}

	/**
	 * Sets the initial month date schedule.
	 *
	 * @param initialMonthDateSchedule the new initial month date schedule
	 */
	public void setInitialMonthDateSchedule(Date initialMonthDateSchedule) {
		this.initialMonthDateSchedule = initialMonthDateSchedule;
	}

	/**
	 * Gets the security helper search.
	 *
	 * @return the security helper search
	 */
	public Security getSecurityHelperSearch() {
		return securityHelperSearch;
	}

	/**
	 * Sets the security helper search.
	 *
	 * @param securityHelperSearch the new security helper search
	 */
	public void setSecurityHelperSearch(Security securityHelperSearch) {
		this.securityHelperSearch = securityHelperSearch;
	}

	/**
	 * Gets the issuer helper search.
	 *
	 * @return the issuer helper search
	 */
	public Issuer getIssuerHelperSearch() {
		return issuerHelperSearch;
	}

	/**
	 * Sets the issuer helper search.
	 *
	 * @param issuerHelperSearch the new issuer helper search
	 */
	public void setIssuerHelperSearch(Issuer issuerHelperSearch) {
		this.issuerHelperSearch = issuerHelperSearch;
	}

	/**
	 * Gets the security to.
	 *
	 * @return the security to
	 */
	public SecurityTO getSecurityTO() {
		return securityTO;
	}

	/**
	 * Sets the security to.
	 *
	 * @param securityTO the new security to
	 */
	public void setSecurityTO(SecurityTO securityTO) {
		this.securityTO = securityTO;
	}
	
	
	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

}
