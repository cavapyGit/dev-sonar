package com.pradera.securities.issuancesecuritie.batch;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.apache.commons.collections4.map.HashedMap;
import org.hibernate.sql.Update;

import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.component.business.service.SecuritiesComponentService;
import com.pradera.integration.component.business.service.VaultControlService;
import com.pradera.integration.component.business.to.SecuritiesComponentTO;
import com.pradera.integration.component.business.to.VaultControlTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.VaultControlComponent;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.custody.payroll.PayrollDetail;
import com.pradera.model.custody.payroll.PayrollHeader;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationCupon;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.type.DematerializationCertificateMotiveType;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.ProgramAmortizationCoupon;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.IssuanceStateType;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
import com.pradera.securities.issuancesecuritie.facade.IssuanceSecuritiesServiceFacade;
import com.pradera.securities.issuancesecuritie.service.IssuanceServiceBean;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ExecIssuanceSecurityBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19-ago-2015
 */
@BatchProcess(name="ExecIssuanceSecurityBatch")
@RequestScoped
public class ExecIssuanceSecurityBatch implements Serializable,JobExecution {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	/** The security service facade. */
	@EJB
	IssuanceServiceBean issuanceServiceBean;//IssuanceServiceBean 	

	@Inject
	Instance<VaultControlService> vaultControlService;
	List<VaultControlTO> lstVaultControlTOs = new ArrayList<VaultControlTO>();
	
	/* (non-Javadoc)
	 * Batchero para registrar masivamente la emisiones del BCB y TGN
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {

		String tmpParameterPk= "";
		
		List<String> lstParameterPayrollDetail = null;
		List<String> lstParameterPayrollHeader = null;
		String userName=null;
		
		for(ProcessLoggerDetail detail : processLogger.getProcessLoggerDetails()){
			if(detail.getParameterName().equals(GeneralConstants.PAYROLL_DETAIL_PK)){
				tmpParameterPk = detail.getParameterValue();
				lstParameterPayrollDetail = new ArrayList<String>();
				lstParameterPayrollDetail.add(tmpParameterPk);
				
			}else if(detail.getParameterName().equals(GeneralConstants.PAYROLL_HEADER_PK)){
				tmpParameterPk = detail.getParameterValue();
				lstParameterPayrollHeader = new ArrayList<String>();
				lstParameterPayrollHeader.add(tmpParameterPk);
				
			}if(detail.getParameterName().equals(GeneralConstants.LIST_PAYROLL_DETAIL_PK)){
				tmpParameterPk = detail.getParameterValue();
				String[] valuesDetail = detail.getParameterValue().split(",");
				lstParameterPayrollDetail = Arrays.asList(valuesDetail);
				
			}else if(detail.getParameterName().equals(GeneralConstants.LIST_PAYROLL_HEADER_PK)){
				tmpParameterPk = detail.getParameterValue();
				String[] valuesHeader = detail.getParameterValue().split(",");
				lstParameterPayrollHeader = Arrays.asList(valuesHeader);
				
			}else if(detail.getParameterName().equals(GeneralConstants.USER_NAME)){
				userName = detail.getParameterValue();
			}
		}
		
		try {
			
			/*
			 
			 -------------------------------------------------------
			 -------------------------------------------------------
			 -- estos pasos se dan al confirmar el ingreso a boveda
			 -------------------------------------------------------
			 -------------------------------------------------------
			  
			 
			 -- EMISION: 
			 -----------
			 
			 1- buscar si existe una emision con el pivot:
			 	- EMISOR
			 	- MONEDA
			 	- FORMA DE EMISION: MIXTO
			 	- CLASE DE VALOR : DPF-CDA
			 	- TIPO DE OFERTA: PRIVADA
			 	- TIPO DE RENTA: RENTA FIJA
			 	- FECHA DE EMISION
			 	
			
			1.1 CONDICION: si existe, hacer una modificacion de emision:
				- aumentar la cantidad de la emision el p*q del CDA a ingresar
			
			1.2 CONDICION: si no existe una emision: 
				- crear una con los datos de la pantalla de deposito de CDA
			
			-- VALOR:
			---------
			2- buscar si no se creo una nueva emision y buscar un valor con el siguiente pivot: 
				- EMISOR 
			    - EMISION
			    - SERIE 
			    - F.VENCIMIENTO 
			    - VALOR NOMINAL 
			    - TASA 
			    - F. EMISION 
			    - PERIDIOCIDAD 
			    
			
			2.1 CONDICION: si existe el valor:
		    	- usar ese valor y darle saldos
		    	- si no tiene cupones crearlos (cronograma de interests), de la misma forma con la amortizacion
		    
		    2.2	CONDICION: si el valor no existe
		    	- crear el valor padre
		    	- crear sus cupones (cronograma de intereses), la amortizacion es al vencimiento
		    	
		    2.3 SIEMPRE:
		    	al crear los cupones (crongorama de interes) y amortizacion:
		    	- debe de crearse los corporativos correspondientes
				
			
			-------------------------------------------------
			-------------------------------------------------
			-- estos pasos se dan al confirmar el deposito::
			-------------------------------------------------
			-------------------------------------------------
			
			-- CERTIFICADOS FISICOS:
			------------------------
			3- Buscar las anotaciones en cuentras registrados ACCOUNT_ANNOTATION_OPERATION (padre) Y ACCOUNT_ANNOTATION_CUPON (cupones/cronograma)
			
			3.1 CONDICION: si en la anotacion en cuenta se marco que los hijos son electronicos:
				- crear el certificado fisico para el papa
				
			3.2 CONDICION: si en la anotacion en cuenta se marco que los hijos NO son electronicos (osea son fisicos):
				- crear el certificado fisico para el papa
				- crear el certificado fisico para los cupones
				
				
			-- ADICIONAL:
			-------------
				- agregar un campo en la tabla physical_certifica donde asociare que program_interest se a creado
				- agregar campo valor de referencia en el physical_certificate (futuro codigo isin) 
				
			
			 */
			
			
			LoggerUser loggerUser = new LoggerUser();
			loggerUser.setUserName(userName);
			loggerUser.setIpAddress("127.0.0.1");
			loggerUser.setAuditTime(CommonsUtilities.currentDate());
			loggerUser.setIdPrivilegeOfSystem(1);
			
			
			List<AccountAnnotationOperation> lstAccountAnnotationOperation = getAccountAnnotationOperationsWithParameters(lstParameterPayrollHeader, lstParameterPayrollDetail);
			
			if(lstAccountAnnotationOperation!=null && lstAccountAnnotationOperation.size()>0) {
			
				Map<Long, Issuance> mapAlldIssuanceGroupedAccountAnnotation = getIssuancesAndCreateIfNotExistForAccountAnnotationOperation(lstAccountAnnotationOperation, loggerUser);
				
				putIssuanceInAccountAnnotationOperation(lstAccountAnnotationOperation, mapAlldIssuanceGroupedAccountAnnotation);
				
				Map<Long, Security> mapSecurityGroupedAccountAnnotation = getSecuritysAndCreateIfNotExistForAccountAnnotationOperation(lstAccountAnnotationOperation, loggerUser);
				
				putSecurityInAccountAnnotationOperation(lstAccountAnnotationOperation, mapSecurityGroupedAccountAnnotation);
				
				
				
				Map<Long, List<PhysicalCertificate>> mapListPhysicalCertificateGroupedAccountAnnotation = createPhysicalCertificateWithAccountAnnotationOperation(lstAccountAnnotationOperation);
				
				putPhysicalCertificateInAccountAnnotationOperation(lstAccountAnnotationOperation, mapListPhysicalCertificateGroupedAccountAnnotation);
				
				
				for(AccountAnnotationOperation accountOperation : lstAccountAnnotationOperation) {
					List<PayrollDetail> lstUpdateDetail = findListPayrollDetailWithAnnotationOperation(accountOperation.getIdAnnotationOperationPk());
					for(PayrollDetail prdUpdate : lstUpdateDetail) {
						prdUpdate.setIndSecurityCreated(1);
						issuanceServiceBean.update(prdUpdate);
					}
				}

				

				
				//VaultControlTO current = vaultControlService.get().getBestPosition(lstVaultControlTOs.size(), CommonsUtilities.currentDate());
				if(lstVaultControlTOs!=null && lstVaultControlTOs.size()>0) {

					for(VaultControlTO current: lstVaultControlTOs ) {
						issuanceServiceBean.updateIssuanceAmountWithSecurity(current.getIdSecurityCodeFk());
					}
					
					vaultControlService.get().insertCertificates(lstVaultControlTOs.size(), CommonsUtilities.currentDate(),lstVaultControlTOs, loggerUser);
					
				}
				
				//
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public Map<Long, List<PhysicalCertificate>> createPhysicalCertificateWithAccountAnnotationOperation(List<AccountAnnotationOperation> lstAccountAnnotationOperation) throws ServiceException {
		Map<Long, List<PhysicalCertificate>> mapListPhysicalCertificateGroupedAccountAnnotation = new HashedMap<Long, List<PhysicalCertificate>>();
		List<PhysicalCertificate> lstPhysicalCertificate = null;
		for( AccountAnnotationOperation accountAnnotationOperation: lstAccountAnnotationOperation ) {
			lstPhysicalCertificate =issuanceServiceBean.registerNewPhysicalCertificateWithAccountAnnotationOperation(accountAnnotationOperation);
			mapListPhysicalCertificateGroupedAccountAnnotation.put(accountAnnotationOperation.getIdAnnotationOperationPk(), lstPhysicalCertificate);
			
			//solo para cuando son ingresos serializados (acciones)
			if(accountAnnotationOperation.getIndSerializable().equals(1)) {
				VaultControlTO currentValue = new VaultControlTO();
				currentValue.setIdSecurityCodeFk(accountAnnotationOperation.getSecurity().getIdSecurityCodePk());
				currentValue.setIdPhysicalCertificateFk(lstPhysicalCertificate.get(0).getIdPhysicalCertificatePk());
				lstVaultControlTOs.add(currentValue);
			}
			
		}
		
		return mapListPhysicalCertificateGroupedAccountAnnotation;
	}
	

	public List<PayrollDetail> findListPayrollDetailWithAnnotationOperation(Long idAnnotationOperationPk) throws ServiceException{
		return issuanceServiceBean.findListPayrollDetailWithAnnotationOperation(idAnnotationOperationPk);
	}
	
	
	public List<AccountAnnotationOperation> getAccountAnnotationOperationsWithParameters(List<String> lstParameterPayrollHeader, List<String> lstParameterPayrollDetail) throws NumberFormatException, ServiceException{
		
		List<AccountAnnotationOperation> lstAccountAnnotationOperation = null;
		if(lstParameterPayrollHeader!=null) {
			lstAccountAnnotationOperation = new ArrayList<AccountAnnotationOperation>();
			for (String payrollHeaderPk : lstParameterPayrollHeader) {
				PayrollHeader payrollHeader = issuanceServiceBean.findPayrollHeader(Long.valueOf(payrollHeaderPk));
				for(PayrollDetail payrollDetail: payrollHeader.getLstPayrollDetail()) {
					if(payrollDetail!=null) {
						lstAccountAnnotationOperation.add(payrollDetail.getAccountAnnotationOperation());
					}
				}
			}
		}
		
		if(lstParameterPayrollDetail!=null) {
			lstAccountAnnotationOperation = new ArrayList<AccountAnnotationOperation>();
			for (String payrollDetailPk : lstParameterPayrollDetail) {
				PayrollDetail payrollDetail = issuanceServiceBean.findPayrollDetailPendingCreate(Long.valueOf(payrollDetailPk));
				if(payrollDetail!=null && payrollDetail.getIndCupon()== 0) {
					lstAccountAnnotationOperation.add(payrollDetail.getAccountAnnotationOperation());
				}
			}
		}
		
		return lstAccountAnnotationOperation;
	}
	
	public void updateInterestAmortizationAndCorporative(String idSecurityCodePk,Long idAnnotationOperationPk ) throws ServiceException {
		List<AccountAnnotationCupon> lstAccountAnnotationCupons = issuanceServiceBean.accountAnnotationCupons(idAnnotationOperationPk);
		for(AccountAnnotationCupon annotationCupon: lstAccountAnnotationCupons) {
			CorporativeOperation corporativeAndInterest = issuanceServiceBean.CorporativeInterest(annotationCupon.getExpirationDate(), idSecurityCodePk);
			CorporativeOperation corporativeAndAmortization = issuanceServiceBean.CorporativeAmortization(annotationCupon.getExpirationDate(), idSecurityCodePk);
			if(corporativeAndInterest!=null) {
				corporativeAndInterest.setState(CorporateProcessStateType.REGISTERED.getCode());
				ProgramInterestCoupon pic = corporativeAndInterest.getProgramInterestCoupon();
				pic.setStateProgramInterest(ProgramScheduleStateType.PENDING.getCode());

				issuanceServiceBean.update(pic);
				issuanceServiceBean.update(corporativeAndInterest);
			}
			if(corporativeAndAmortization!=null) {
				corporativeAndAmortization.setState(CorporateProcessStateType.REGISTERED.getCode());
				ProgramAmortizationCoupon pac = corporativeAndAmortization.getProgramAmortizationCoupon();
				pac.setStateProgramAmortization(ProgramScheduleStateType.PENDING.getCode());

				issuanceServiceBean.update(pac);
				issuanceServiceBean.update(corporativeAndAmortization);
			}
		}
	}
	

	public Map<Long, Security> getSecuritysAndCreateIfNotExistForAccountAnnotationOperation(List<AccountAnnotationOperation> lstAccountAnnotationOperation, LoggerUser loggerUser) throws ServiceException {
		/*
		 1- buscar si el valor existe
		 2- crear los valores que no existen
		 	- crear sus cronogramas y procesos corporativos
		 */

		Map<Long, Security> mapAllSecurityGroupedAccountAnnotation = new HashedMap<Long, Security>();

		Security security = null;
		List<AccountAnnotationOperation> lstAccountAnnotationOperationWithoutSecurity = new ArrayList<AccountAnnotationOperation>();

		List<AccountAnnotationOperation> lstAccountAnnotationOperationWithSecurity = new ArrayList<AccountAnnotationOperation>();
		
		for(AccountAnnotationOperation accountAnnotationOperation: lstAccountAnnotationOperation) {
			if(accountAnnotationOperation.getIndSerializable().equals(1)) {
				lstAccountAnnotationOperationWithSecurity.add(accountAnnotationOperation);
				mapAllSecurityGroupedAccountAnnotation.put(accountAnnotationOperation.getIdAnnotationOperationPk(), accountAnnotationOperation.getSecurity());
			}else {
				security = issuanceServiceBean.findSecurityWithAccountAnnotation(accountAnnotationOperation);
								
				mapAllSecurityGroupedAccountAnnotation.put(accountAnnotationOperation.getIdAnnotationOperationPk(), security);
				if(security == null) {//si se ingresa por primera vez al sistema
					lstAccountAnnotationOperationWithoutSecurity.add(accountAnnotationOperation);
				}else {//si es un reingreso
					accountAnnotationOperation.setSecurity(security);
					
					if( accountAnnotationOperation.getMotive().equals(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode())  ){
						security.setStateSecurity(SecurityStateType.GUARDA_EXCLUSIVE.getCode());
						security.setIndPaymentBenefit(0);
					}else{
			            security.setStateSecurity(SecurityStateType.REGISTERED.getCode());
			            security.setIndPaymentBenefit(1);
			        }
					security.setReferencePartCode(accountAnnotationOperation.getReferencePartCode());
					
					if(accountAnnotationOperation.getIndSerializable()!=null && accountAnnotationOperation.getIndSerializable().equals(0)) {
						security.setPhysicalBalance(accountAnnotationOperation.getTotalBalance());
					}

					/*
					if(security.getMotiveAnnotation()!= null && security.getMotiveAnnotation().equals(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode())) {
						security.setStateSecurity(SecurityStateType.GUARDA_EXCLUSIVE.getCode());
					}else{
			            security.setStateSecurity(SecurityStateType.REGISTERED.getCode());
			        }
			        */
					
					issuanceServiceBean.update(security);
					issuanceServiceBean.update(accountAnnotationOperation);
					if(accountAnnotationOperation.getIndSerializable().equals(0)) {//la annotacion ya es otra
						updateInterestAmortizationAndCorporative(security.getIdSecurityCodePk(), accountAnnotationOperation.getIdAnnotationOperationPk());
					}
					lstAccountAnnotationOperationWithSecurity.add(accountAnnotationOperation);

					VaultControlTO currentValue = null;
					currentValue = new VaultControlTO();
					currentValue.setIdSecurityCodeFk(security.getIdSecurityCodePk());
					lstVaultControlTOs.add(currentValue);
				}
				
			}
			
		}
		

		VaultControlTO currentValue = null;
		Security newSecurity = null;
		for ( AccountAnnotationOperation accountAnnotationOperation: lstAccountAnnotationOperationWithoutSecurity ) {
			newSecurity = issuanceServiceBean.registerNewSecurityWithAccountAnnotationOperation(accountAnnotationOperation, loggerUser);
			
			mapAllSecurityGroupedAccountAnnotation.put(accountAnnotationOperation.getIdAnnotationOperationPk(), newSecurity);
			
			currentValue = new VaultControlTO();
			currentValue.setIdSecurityCodeFk(newSecurity.getIdSecurityCodePk());
			lstVaultControlTOs.add(currentValue);
		}
		
		
		//Serializados - si ya se ingreso el valor, no volver a ingresar mas titulos
		/*for ( AccountAnnotationOperation accountAnnotationOperation: lstAccountAnnotationOperationWithSecurity ) {
			//solo a los que no tienen valores
			currentValue = new VaultControlTO();

			if(accountAnnotationOperation.getIndSerializable().equals(1)) {
				currentValue.setIdSecurityCodeFk(accountAnnotationOperation.getSecurity().getIdSecurityCodePk());
				lstVaultControlTOs.add(currentValue);
			}else {
				VaultControlComponent vaultControlComponent = issuanceServiceBean.getVaultControlComponentWithSecurity(accountAnnotationOperation.getSecurity().getIdSecurityCodePk());
				if(vaultControlComponent == null) {
					currentValue.setIdSecurityCodeFk(accountAnnotationOperation.getSecurity().getIdSecurityCodePk());
					lstVaultControlTOs.add(currentValue);
				}
			}
			
		}*/
		
		return mapAllSecurityGroupedAccountAnnotation;
	}
	
	public Map<Long, Issuance> getIssuancesAndCreateIfNotExistForAccountAnnotationOperation(List<AccountAnnotationOperation> lstAccountAnnotationOperation, LoggerUser loggerUser) throws ServiceException {

		Issuance issuance = null;
		Map<Long, Issuance> mapAlldIssuanceGroupedAccountAnnotation = new HashedMap<Long, Issuance>();
		
		for(AccountAnnotationOperation accountAnnotationOperation: lstAccountAnnotationOperation) {
			if(accountAnnotationOperation.getIndSerializable() == 0) {
				issuance = returnIssuanceWithPreferences(issuanceServiceBean.findIssuanceWithAccountAnnotation(accountAnnotationOperation));
				if(issuance == null) {
					issuance = issuanceServiceBean.registerNewIssuanceWithAccountAnnotationOperation( accountAnnotationOperation , loggerUser);
				}else {
					//la emision ya existe
					BigDecimal montoValor= accountAnnotationOperation.getTotalBalance().multiply(accountAnnotationOperation.getSecurityNominalValue());
					issuance.setIssuanceAmount(issuance.getIssuanceAmount().add(montoValor));
					issuance.setStateIssuance(IssuanceStateType.AUTHORIZED.getCode());
					if(issuance.getExpirationDate()!=null) {
						Date expirationIssuance = issuance.getExpirationDate();
						Date expirationAnnotation = accountAnnotationOperation.getSecurityExpirationDate();
						if(expirationAnnotation.after(expirationIssuance)) {
							issuance.setExpirationDate(expirationAnnotation);
						}
					}
					issuance = issuanceServiceBean.update(issuance);
				}
			}else {
				issuance = null;
			}
			mapAlldIssuanceGroupedAccountAnnotation.put(accountAnnotationOperation.getIdAnnotationOperationPk(), issuance);
		}
		
		
		
		
		/*
		Map<Long, Issuance> mapAlldIssuanceGroupedAccountAnnotation = new HashedMap<Long, Issuance>();
		Map<String, List<AccountAnnotationOperation>> mapAccountAnnotationGroupedIssuer = new HashedMap<String, List<AccountAnnotationOperation>>();
		Map<String, List<Issuance>> mapNewIssuanceGroupedIssuer = new HashedMap<String, List<Issuance>>();
		
		
		Issuance issuance = null;
		List<AccountAnnotationOperation> lstAccountAnnotationOperationWithoutIssuance = new ArrayList<AccountAnnotationOperation>();
		
		for(AccountAnnotationOperation accountAnnotationOperation: lstAccountAnnotationOperation) {
			if(accountAnnotationOperation.getIndSerializable() == 0) {
				issuance = returnIssuanceWithPreferences(issuanceServiceBean.findIssuanceWithAccountAnnotation(accountAnnotationOperation));
				if(issuance == null) {
					lstAccountAnnotationOperationWithoutIssuance.add(accountAnnotationOperation);
				}else {
					//la emision ya existe
					BigDecimal montoValor= accountAnnotationOperation.getTotalBalance().multiply(accountAnnotationOperation.getSecurityNominalValue());
					issuance.setIssuanceAmount(issuance.getIssuanceAmount().add(montoValor));
					issuance.setStateIssuance(IssuanceStateType.AUTHORIZED.getCode());
					if(issuance.getExpirationDate()!=null) {
						Date expirationIssuance = issuance.getExpirationDate();
						Date expirationAnnotation = accountAnnotationOperation.getSecurityExpirationDate();
						if(expirationAnnotation.after(expirationIssuance)) {
							issuance.setExpirationDate(expirationAnnotation);
						}
					}
					issuance = issuanceServiceBean.update(issuance);
					
				}
				mapAlldIssuanceGroupedAccountAnnotation.put(accountAnnotationOperation.getIdAnnotationOperationPk(), issuance);
			}
		}

		//anotaciones en cuenta que no tienen emisiones
		for(AccountAnnotationOperation accountAnnotationOperationWithoutIssuance: lstAccountAnnotationOperationWithoutIssuance) {
			accountAnnotationOperationWithoutIssuance.getIssuer().getIdIssuerPk();
			if( mapAccountAnnotationGroupedIssuer.get(accountAnnotationOperationWithoutIssuance.getIssuer().getIdIssuerPk()) == null ) {
				mapAccountAnnotationGroupedIssuer.put(accountAnnotationOperationWithoutIssuance.getIssuer().getIdIssuerPk(), new ArrayList<AccountAnnotationOperation>());
			}
			
			List<AccountAnnotationOperation> lstPutMap = mapAccountAnnotationGroupedIssuer.get(accountAnnotationOperationWithoutIssuance.getIssuer().getIdIssuerPk());
			lstPutMap.add(accountAnnotationOperationWithoutIssuance);
			mapAccountAnnotationGroupedIssuer.put(accountAnnotationOperationWithoutIssuance.getIssuer().getIdIssuerPk(), lstPutMap);
		}
		
		//emisores de las anotaciones en cuenta que no tienen emision
		for (Map.Entry<String, List<AccountAnnotationOperation>> entry : mapAccountAnnotationGroupedIssuer.entrySet()) {
			String IdIssuer = entry.getKey();
			List<AccountAnnotationOperation> lstAccountAnnotationOperationInMap = entry.getValue();

			
			List<Issuance> lstIssuance = mapNewIssuanceGroupedIssuer.get(IdIssuer);
			if( !(lstIssuance != null && lstIssuance.size()>0) ) {
				lstIssuance = new ArrayList<Issuance>();
			}
			
			Issuance newIssuance = issuanceServiceBean.registerNewIssuanceWithAccountAnnotationOperation( lstAccountAnnotationOperationInMap.get(0) , loggerUser);
			Boolean existIssuanceCurrency = false;
			for(Issuance issua: lstIssuance) {
				if(issua.getCurrency().equals(newIssuance.getCurrency())) {
					existIssuanceCurrency = true;
				}
			}
			
			if(!existIssuanceCurrency) {
				lstIssuance.add(newIssuance);
			}
			mapNewIssuanceGroupedIssuer.put(IdIssuer, lstIssuance);	
		}
		
		//agrego las emisiones a las anotaciones en cuenta
		for(AccountAnnotationOperation accountAnnotationOperationWithoutIssuance: lstAccountAnnotationOperationWithoutIssuance) {
			String idIssuerPk = accountAnnotationOperationWithoutIssuance.getIssuer().getIdIssuerPk();
			
			List<Issuance> lstIssuance = mapNewIssuanceGroupedIssuer.get(idIssuerPk);
			
			Issuance issuanceCreated = null;
			for(Issuance issua: lstIssuance) {
				issuanceCreated = null;
				if(issua.getCurrency().equals(accountAnnotationOperationWithoutIssuance.getSecurityCurrency())) {
					issuanceCreated = issua;
					break;
				}
			}
			
			mapAlldIssuanceGroupedAccountAnnotation.put(accountAnnotationOperationWithoutIssuance.getIdAnnotationOperationPk(), issuanceCreated);
		}*/
		
		return mapAlldIssuanceGroupedAccountAnnotation;
	}


	/*
	 1- separ las anotaciones en cuenta que tienen emisiones de las que no lo tienen
	 2- de las emisiones pendientes de crear, clasificarlas por emisor
	 3- crear solo 1 emision por emisor y asignarselas a todas las anotaciones en cuenta
	 */
	/*
	 public Map<Long, Issuance> getIssuancesAndCreateIfNotExistForAccountAnnotationOperation(List<AccountAnnotationOperation> lstAccountAnnotationOperation, LoggerUser loggerUser) throws ServiceException {

		
		
		Map<Long, Issuance> mapAlldIssuanceGroupedAccountAnnotation = new HashedMap<Long, Issuance>();
		Map<String, List<AccountAnnotationOperation>> mapAccountAnnotationGroupedIssuer = new HashedMap<String, List<AccountAnnotationOperation>>();
		Map<String, List<Issuance>> mapNewIssuanceGroupedIssuer = new HashedMap<String, List<Issuance>>();
		
		
		Issuance issuance = null;
		List<AccountAnnotationOperation> lstAccountAnnotationOperationWithoutIssuance = new ArrayList<AccountAnnotationOperation>();
		
		for(AccountAnnotationOperation accountAnnotationOperation: lstAccountAnnotationOperation) {
			if(accountAnnotationOperation.getIndSerializable() == 0) {
				issuance = returnIssuanceWithPreferences(issuanceServiceBean.findIssuanceWithAccountAnnotation(accountAnnotationOperation));
				if(issuance == null) {
					lstAccountAnnotationOperationWithoutIssuance.add(accountAnnotationOperation);
				}else {
					//la emision ya existe
					BigDecimal montoValor= accountAnnotationOperation.getTotalBalance().multiply(accountAnnotationOperation.getSecurityNominalValue());
					issuance.setIssuanceAmount(issuance.getIssuanceAmount().add(montoValor));
					issuance.setStateIssuance(IssuanceStateType.AUTHORIZED.getCode());
					if(issuance.getExpirationDate()!=null) {
						Date expirationIssuance = issuance.getExpirationDate();
						Date expirationAnnotation = accountAnnotationOperation.getSecurityExpirationDate();
						if(expirationAnnotation.after(expirationIssuance)) {
							issuance.setExpirationDate(expirationAnnotation);
						}
					}
					issuance = issuanceServiceBean.update(issuance);
				}
				mapAlldIssuanceGroupedAccountAnnotation.put(accountAnnotationOperation.getIdAnnotationOperationPk(), issuance);
			}
		}

		//anotaciones en cuenta que no tienen emisiones
		for(AccountAnnotationOperation accountAnnotationOperationWithoutIssuance: lstAccountAnnotationOperationWithoutIssuance) {
			accountAnnotationOperationWithoutIssuance.getIssuer().getIdIssuerPk();
			if( mapAccountAnnotationGroupedIssuer.get(accountAnnotationOperationWithoutIssuance.getIssuer().getIdIssuerPk()) == null ) {
				mapAccountAnnotationGroupedIssuer.put(accountAnnotationOperationWithoutIssuance.getIssuer().getIdIssuerPk(), new ArrayList<AccountAnnotationOperation>());
			}
			
			List<AccountAnnotationOperation> lstPutMap = mapAccountAnnotationGroupedIssuer.get(accountAnnotationOperationWithoutIssuance.getIssuer().getIdIssuerPk());
			lstPutMap.add(accountAnnotationOperationWithoutIssuance);
			mapAccountAnnotationGroupedIssuer.put(accountAnnotationOperationWithoutIssuance.getIssuer().getIdIssuerPk(), lstPutMap);
		}
		
		//emisores de las anotaciones en cuenta que no tienen emision
		for (Map.Entry<String, List<AccountAnnotationOperation>> entry : mapAccountAnnotationGroupedIssuer.entrySet()) {
			String IdIssuer = entry.getKey();
			List<AccountAnnotationOperation> lstAccountAnnotationOperationInMap = entry.getValue();

			
			List<Issuance> lstIssuance = mapNewIssuanceGroupedIssuer.get(IdIssuer);
			if( !(lstIssuance != null && lstIssuance.size()>0) ) {
				lstIssuance = new ArrayList<Issuance>();
			}
			
			Issuance newIssuance = issuanceServiceBean.registerNewIssuanceWithAccountAnnotationOperation( lstAccountAnnotationOperationInMap.get(0) , loggerUser);
			Boolean existIssuanceCurrency = false;
			for(Issuance issua: lstIssuance) {
				if(issua.getCurrency().equals(newIssuance.getCurrency())) {
					existIssuanceCurrency = true;
				}
			}
			
			if(!existIssuanceCurrency) {
				lstIssuance.add(newIssuance);
			}
			mapNewIssuanceGroupedIssuer.put(IdIssuer, lstIssuance);	
		}
		
		//agrego las emisiones a las anotaciones en cuenta
		for(AccountAnnotationOperation accountAnnotationOperationWithoutIssuance: lstAccountAnnotationOperationWithoutIssuance) {
			String idIssuerPk = accountAnnotationOperationWithoutIssuance.getIssuer().getIdIssuerPk();
			
			List<Issuance> lstIssuance = mapNewIssuanceGroupedIssuer.get(idIssuerPk);
			
			Issuance issuanceCreated = null;
			for(Issuance issua: lstIssuance) {
				issuanceCreated = null;
				if(issua.getCurrency().equals(accountAnnotationOperationWithoutIssuance.getSecurityCurrency())) {
					issuanceCreated = issua;
					break;
				}
			}
			
			mapAlldIssuanceGroupedAccountAnnotation.put(accountAnnotationOperationWithoutIssuance.getIdAnnotationOperationPk(), issuanceCreated);
		}
		
		return mapAlldIssuanceGroupedAccountAnnotation;
	}
	 */
	
	
	private Issuance returnIssuanceWithPreferences(List<Issuance> lstReturnIssuance) {
		Issuance issuancePreference =  null;
		if(lstReturnIssuance!=null && lstReturnIssuance.size()>0) {
			issuancePreference =  lstReturnIssuance.get(0);
			/*if(lstReturnIssuance.size()>1) {
				for (Issuance issuance : lstReturnIssuance) {
					if( issuance.getIssuanceType().equals(IssuanceType.PHYSICAL.getCode()) && issuance.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode()) ){
						return issuance;
					}else{
						issuancePreference = issuance;
					}
				}
			}*/
		}
		
		return issuancePreference;
	}
	
	public void putIssuanceInAccountAnnotationOperation(List<AccountAnnotationOperation> lstAccountAnnotationOperation, Map<Long, Issuance> mapAlldIssuanceGroupedAccountAnnotation) {
		for (AccountAnnotationOperation accountAnnotationOperation : lstAccountAnnotationOperation) {
			Issuance issuance = mapAlldIssuanceGroupedAccountAnnotation.get(accountAnnotationOperation.getIdAnnotationOperationPk());
			accountAnnotationOperation.setIssuance(issuance);
		}
	}

	
	public void putSecurityInAccountAnnotationOperation(List<AccountAnnotationOperation> lstAccountAnnotationOperation, Map<Long, Security> mapAlldIssuanceGroupedAccountAnnotation) {
		
		try {
			
			for (AccountAnnotationOperation accountAnnotationOperation : lstAccountAnnotationOperation) {
				if(accountAnnotationOperation.getIndSerializable().equals(0)) {
					Security security = mapAlldIssuanceGroupedAccountAnnotation.get(accountAnnotationOperation.getIdAnnotationOperationPk());
					accountAnnotationOperation.setSecurity(security);
					
					issuanceServiceBean.update(accountAnnotationOperation);
	
					for(ProgramInterestCoupon securityInterestCupon: security.getInterestPaymentSchedule().getProgramInterestCoupons()) {
						for(AccountAnnotationCupon annotationCupon: accountAnnotationOperation.getAccountAnnotationCupons()) {
							if(annotationCupon.getCuponNumber().equals(securityInterestCupon.getCouponNumber())) {
								annotationCupon.setProgramInterestCoupon(securityInterestCupon);
								issuanceServiceBean.update(annotationCupon);
							}
						}
					}
				}				
			}
			
		}catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	public void putPhysicalCertificateInAccountAnnotationOperation(List<AccountAnnotationOperation> lstAccountAnnotationOperation, Map<Long, List<PhysicalCertificate>> mapListPhysicalCertificateGroupedAccountAnnotation) {

		try {
			
			for (AccountAnnotationOperation accountAnnotationOperation : lstAccountAnnotationOperation) {
				List<PhysicalCertificate> lstPhysicalCertificate = mapListPhysicalCertificateGroupedAccountAnnotation.get(accountAnnotationOperation.getIdAnnotationOperationPk());
				accountAnnotationOperation.setPhysicalCertificate(lstPhysicalCertificate.get(0));
				
				issuanceServiceBean.update(accountAnnotationOperation);
			}
			
		}catch (Exception e) {
			// TODO: handle exception
		}
		
	}

	/* (non-Javadoc)
	 * Metodo implementado para obtener los parametros de notificaciones
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * Metodo implementado para obtener a los destinatarios
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * Metodo Implementado para enviar las notificaciones
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}
