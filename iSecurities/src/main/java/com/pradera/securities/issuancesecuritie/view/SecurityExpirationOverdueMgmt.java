package com.pradera.securities.issuancesecuritie.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.business.to.SecurityOverdueTO;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.AmortizationPaymentSchedule;
import com.pradera.model.issuancesecuritie.InterestPaymentSchedule;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.securities.issuancesecuritie.facade.SecurityServiceFacade;
import com.pradera.securities.issuancesecuritie.util.PropertiesConstants;
import com.pradera.securities.issuer.facade.IssuerServiceFacade;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2015.</li>
 * </ul>
 * 
 * The Class InterfaceSendMgmt.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18-set-2015
 */
@DepositaryWebBean
public class SecurityExpirationOverdueMgmt extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The user info. */
	@Inject
	private UserInfo userInfo;



	/** The issuer service facade. */
	@EJB
	private IssuerServiceFacade issuerServiceFacade;
	
	/** The secuity service facade. */
	@EJB
	private SecurityServiceFacade securityServiceFacade;

	/** The general parameter facade. */
	@EJB private GeneralParametersFacade generalParameterFacade;
	
	/** The list participant. */
	private List<Participant> listParticipant;
	
	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;

	

	/** The batch process service facade. */
	@EJB
	BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The country residence. */
	@Inject 
	@Configurable 
	Integer countryResidence;
	
	/** The logged issuer. */
	private Issuer loggedIssuer;
	
	/** The issuer helper search. */
	private Issuer issuerHelperSearch;
	
	/** The issuance helper search. */
	private Issuance issuanceHelperSearch;
	
	/** The security t osearch. */
	private SecurityTO securityTOsearch;
	
	/** The lst cbo filter currency. */
	private List<ParameterTable> lstCboFilterCurrency;
	
	/** The lst cbo security state. */
	private List<ParameterTable> lstCboSecurityState;
	
	/** The lst cbo filter securitie class. */
	private List<ParameterTable> lstCboFilterSecuritieClass;
	
	/** The security data model. */
	private GenericDataModel<SecurityOverdueTO> securityDataModel;	
		
	/** The security. */
	private Security security;
	
/** The list security selected. */
	private SecurityOverdueTO[] listSecuritySelected;
	
	
	/** The parameter table to. */
	private ParameterTableTO parameterTableTO;
	
	/** The lst cbo instrument type. */
	private List<ParameterTable> lstCboInstrumentType;	
		
	/** The execution type parameter list. */
	private List<ParameterTable> sendTypeParameterList;
	/** The participant list. */
	private List<Participant> participantList;
	
	private Map<String,Integer> mapSecurityClass;
		
	/** The initial state search form. */
	String initialStateSearchForm;
	

	/** The source holder. */
	private Holder holder;
	
	/** The source holder accounts. */
	private List<HolderAccount> holderAccounts;

	/** The account sale. */
	private HolderAccount accountSend;
	
	/** The map parameters. */
	private HashMap<Integer, ParameterTable> mapParameters = new HashMap<Integer,ParameterTable>();
	
	/** The max date. */
	private Date maxDate= getYesterdaySystemDate();
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		try {
			super.setParametersTableMap(new HashMap<Integer,Object>());
			 
			security=new Security(); 
			holder=new Holder();			
			securityTOsearch=new SecurityTO();
			mapSecurityClass= new HashMap<String, Integer>();

			security.setIssuer( new Issuer() );
			security.setInterestPaymentSchedule(new InterestPaymentSchedule());
			security.setAmortizationPaymentSchedule(new AmortizationPaymentSchedule());
			initialStateSearchForm=securityTOsearch.toString();
			
			issuerHelperSearch=new Issuer();
			issuanceHelperSearch=new Issuance();
			
			
			allHolidays=generalParametersFacade.getAllHolidaysByCountry(countryResidence) ;
			Participant participant=new Participant();
			listParticipant=helperComponentFacade.findParticipantNativeQueryFacade(participant);
			loadCboInstrumentType();
			loadCboSecuritiesState();
			loadCboFilterSecuritieClass();
			loadCboFilterCurency();
			loadCboSecuritieClassTarget();
			
			if((userInfo.getUserAccountSession().isIssuerInstitucion()|| userInfo.getUserAccountSession().isIssuerDpfInstitucion()) && 
					Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getIssuerCode())){
				Issuer issuerFilter=new Issuer();
				issuerFilter.setIdIssuerPk(userInfo.getUserAccountSession().getIssuerCode());
				loggedIssuer=new Issuer();				
				issuerHelperSearch=loggedIssuer;
				if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
					List<ParameterTable> lstSecuritieClass = new ArrayList<ParameterTable>();
					List<Integer>lstSecurityClass = new ArrayList<Integer>();
					for(ParameterTable objParameterTable : lstCboFilterSecuritieClass){
						if(objParameterTable.getParameterTablePk().equals(SecurityClassType.DPA.getCode()) || 
								objParameterTable.getParameterTablePk().equals(SecurityClassType.DPF.getCode())){
							lstSecuritieClass.add(objParameterTable);
							lstSecurityClass.add(objParameterTable.getParameterTablePk());
						}
					}
					lstCboFilterSecuritieClass = lstSecuritieClass;
					securityTOsearch.setLstSecurityClass(lstSecurityClass);
				}
			}
			setViewOperationType(ViewOperationsType.CONSULT.getCode());
			securityTOsearch.setSecurityState(SecurityStateType.SUSPENDED.getCode().toString());
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire( new ExceptionToCatchEvent(e) );
		}
	}
	
	/**
	 * Load cbo instrument type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCboInstrumentType() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
		lstCboInstrumentType=generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
	}

	/**
	 * Load cbo filter securitie class.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboFilterSecuritieClass() throws ServiceException {
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.SECURITIES_CLASS.getCode() );
		lstCboFilterSecuritieClass=generalParametersFacade.getListParameterTableServiceBean( parameterTableTO );
		fillMapSecurityClass(lstCboFilterSecuritieClass);
		billParametersTableMap(lstCboFilterSecuritieClass,true);
	}
	
	/**
	 * Load cbo securitie class target.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboSecuritieClassTarget() throws ServiceException {
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		
		parameterTableTO.setLstParameterTablePk( new ArrayList<Integer>() );
		parameterTableTO.getLstParameterTablePk().add( SecurityClassType.ACC.getCode() );
		parameterTableTO.getLstParameterTablePk().add( SecurityClassType.ACC_RF.getCode());
		
		
	}	
	/**
	 * Load cbo filter curency.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboFilterCurency() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.CURRENCY.getCode() );
		lstCboFilterCurrency=generalParametersFacade.getListParameterTableServiceBean( parameterTableTO );
		billParametersTableMap(lstCboFilterCurrency, true);
	}
	/**
	 * Load cbo securities state.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboSecuritiesState() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.SECURITIES_STATE.getCode() );
		lstCboSecurityState=generalParametersFacade.getListParameterTableServiceBean( parameterTableTO );
		
		billParametersTableMap(lstCboSecurityState);
	}

	/**
	 * Load user session privileges.
	 */
	private void loadUserSessionPrivileges(){
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnRegisterView(Boolean.TRUE);
		privilegeComponent.setBtnModifyView(Boolean.TRUE);
		privilegeComponent.setBtnConfirmView(Boolean.TRUE);
		privilegeComponent.setBtnAnnularView(Boolean.TRUE);
		privilegeComponent.setBtnBlockView(Boolean.TRUE);
		privilegeComponent.setBtnUnblockView(Boolean.TRUE);
		userInfo.setPrivilegeComponent(privilegeComponent);
	}
		
	
	
	

	
	/**
	 * Change filter field.
	 */
	public void changeFilterField(){

	}
	
	
	/**
	 * Validate not empty fields.
	 *
	 * @param securityTOsearch the security t osearch
	 * @return true, if successful
	 */
	private boolean validateNotEmptyFields(SecurityTO securityTOsearch) {
		if(Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getIdIssuanceCodePk())
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getIdIssuerCodePk())
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getIdSecurityCodePk())
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getSecurityCurrency())
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getAlternativeCode())				
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getDescriptionSecurity())
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getIssuanceDate())
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getExpirationDate())
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getSecurityClass())
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getSecurityState())
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getIdIsinCodePk())){
			return true;
		}
		return false;
	}
	/**
	 * Search securities handler.
	 *
	 * @param event the event
	 */
	public void searchSecuritiesHandler(ActionEvent event){
		JSFUtilities.hideGeneralDialogues();
		try {

			List<String> listSecCodeExcluded=null;
			List<Integer> listSecClassExcluded=null;
			StringBuilder secClassFail=new StringBuilder();
			
			/**
			 * Text Area With Security Code Excluded
			 */
			if(Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getExcludedSecurityCode())){
				List<String> listSecurities = new ArrayList<String>(Arrays.asList(securityTOsearch.getExcludedSecurityCode().split(",")));
				listSecCodeExcluded = new ArrayList<>();
				for (String securityCode : listSecurities ){
					listSecCodeExcluded.add(securityCode.trim());
				}
			}
			
			/**
			 * Text Area With Security Class Excluded
			 */
			if(Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getExcludedSecurityClass())){
				List<String> listSecurities = new ArrayList<String>(Arrays.asList(securityTOsearch.getExcludedSecurityClass().split(",")));
				listSecClassExcluded = new ArrayList<>();
				for (String securityClass : listSecurities ){
					Integer secClass=mapSecurityClass.get(securityClass.trim());
					if(secClass==null){
						secClassFail.append(securityClass.trim()).append(GeneralConstants.STR_COMMA);
					}
					listSecClassExcluded.add(secClass);
				}
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(secClassFail.toString())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ERROR_SECURITY_CLASS_FAILED,new Object[] {secClassFail}));
				JSFUtilities.showValidationDialog();
				return;
			}
			
			securityTOsearch.setListExcludedSecurityCode(listSecCodeExcluded);
			securityTOsearch.setListExcludedSecurityClass(listSecClassExcluded);
			
			
			securityTOsearch.setIdIssuerCodePk( issuerHelperSearch.getIdIssuerPk() );
			securityTOsearch.setIdIssuanceCodePk( issuanceHelperSearch.getIdIssuanceCodePk() );
			securityTOsearch.setStateSecurity(SecurityStateType.SUSPENDED.getCode());
			if(Validations.validateIsNotNull(accountSend)){
				securityTOsearch.setIdHolderAccountPk(accountSend.getIdHolderAccountPk());
			}
			
			
			if(validateNotEmptyFields(securityTOsearch)){
				List<SecurityOverdueTO> lstSecurity = null;
				lstSecurity=securityServiceFacade.findSecuritiesForExpiration(securityTOsearch);
				this.securityDataModel=new GenericDataModel(lstSecurity);

			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ALT_NEED_ONE_FIELD));
				JSFUtilities.showValidationDialog();
			}		
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	
	/**
	 * Clean security search handler.
	 */
	public void cleanSecuritySearchHandler(ActionEvent event){
		try { 
			securityDataModel=new GenericDataModel<>();
			listSecuritySelected=null;
			securityTOsearch=new SecurityTO();
			security=null;
			holder=new Holder();
			holderAccounts=new ArrayList<HolderAccount>();
			issuanceHelperSearch=new Issuance();
			securityTOsearch.setSecurityState(SecurityStateType.SUSPENDED.getCode().toString());
			
			if((userInfo.getUserAccountSession().isIssuerInstitucion()|| userInfo.getUserAccountSession().isIssuerDpfInstitucion()) &&
					Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getIssuerCode())){
				Issuer issuerFilter=new Issuer();
				issuerFilter.setIdIssuerPk( userInfo.getUserAccountSession().getIssuerCode() );
				loggedIssuer=new Issuer();
				loggedIssuer=issuerServiceFacade.findIssuerByFiltersServiceFacade(issuerFilter);
				
			} else {
				issuerHelperSearch = new Issuer();
			}
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
 
	
	/**
	 * Bill parameters table map.
	 *
	 * @param lstParameterTable the lst parameter table
	 * @param isObj the is obj
	 */
	public void billParametersTableMap(List<ParameterTable> lstParameterTable, boolean isObj){
		for(ParameterTable pTable : lstParameterTable){
			if(isObj){
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable);
			}else{
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable.getParameterName());
			}
		}
	}
	
	/**
	 * Fill Map Security Class
	 * Key: Mnemonic Security Class
	 * Value: Pk parameter table
	 * @param lstParameterTable
	 */
	public void fillMapSecurityClass(List<ParameterTable> lstParameterTable){
		for(ParameterTable pTable : lstParameterTable){
			mapSecurityClass.put(pTable.getText1()	, pTable.getParameterTablePk());
		}
	}
	/**
	 * Bill parameters table map.
	 *
	 * @param lstParameterTable the lst parameter table
	 */
	public void billParametersTableMap(List<ParameterTable> lstParameterTable){
		billParametersTableMap(lstParameterTable,false);
	}

	/**
	 * Before operation handler.
	 */
	public void beforeOperationHandler(){
		
		List<String> listRejected=new ArrayList<String>();
		StringBuilder sbSecurity=new StringBuilder();
		
		for (SecurityOverdueTO security : listSecuritySelected){
			if(security.getAvailableBalance()!=null && security.getAvailableBalance()<=0){
				listRejected.add(security.getIdSecurityCodePk().concat(GeneralConstants.BLANK_SPACE).concat(security.getAlternateCode()));
				sbSecurity.append(security.getIdSecurityCodePk().concat(GeneralConstants.BLANK_SPACE).concat(security.getAlternateCode()));
			}
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(listRejected)){
			
			JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_WARNING_ACTION, null, 
								 PropertiesConstants.CONFIRM_EXPIRATION_BALANCES_NOT_FOUND, 
								 new Object[] {sbSecurity});
			JSFUtilities.showSimpleValidationDialog();
			/**Limpiar la seleccion*/
			listSecuritySelected=null;
			return;
		}
		
		/**
		 * Validar si en la lista existen valores vencidos y no dejar
		 */
		showMessageOnDialog(
				PropertiesUtilities.getGenericMessage(PropertiesConstants.DIALOG_HEADER_CONFIRM),
				PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_EXPIRATION_SECURITY
								));
		JSFUtilities.executeJavascriptFunction("cnfwConfirmOperation.show();");
		
		
	}
	
	/**
	 * Process operation handler.
	 */
	@LoggerAuditWeb
	public void processOperationHandler(){
		
		List<SecurityOverdueTO> listIdSecurity=new ArrayList<>();
		/**
		 * Fill Security Send 
		 * */
		for (SecurityOverdueTO security : listSecuritySelected){
			listIdSecurity.add(security);
		}
		
		try {
			securityServiceFacade.confirmWithdrawExpiredSecurities(listIdSecurity);
			
			JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_WARNING_ACTION, null, 
					 PropertiesConstants.CONFIRM_EXPIRATION_WITHDRAW,null );
			JSFUtilities.showSimpleValidationDialog();
			
			cleanSecuritySearchHandler(null);
			
		} catch (ServiceException e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Validate seller holder account.
	 *
	 * @throws ServiceException the service exception
	 */
	public void validateSellerHolderAccount() throws ServiceException {		
		if(accountSend != null && accountSend.getIdHolderAccountPk() != null){
			if(HolderAccountStatusType.CLOSED.getCode().equals(accountSend.getStateAccount())){				
				String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
				String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_EXPIRATION_SECURITY);
				showMessageOnDialog(headerMessage, bodyMessage);
				JSFUtilities.showSimpleValidationDialog();	
				accountSend=null;
				return;
			}
		}
	}
	/**
	 * Validate source holder.
	 *
	 * @throws ServiceException the service exception
	 */
	public void validateSellerHolder() throws ServiceException{
		holderAccounts=null;
		accountSend=new HolderAccount();

		if(holder != null && holder .getIdHolderPk() != null){
			// verify if is registered or blocked
			if(!holder .getStateHolder().equals(HolderStateType.REGISTERED.getCode()) && 
					!holder .getStateHolder().equals(HolderStateType.BLOCKED.getCode()) ){
				String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
				String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_HOLDER_NOT_REGISTER);
				showMessageOnDialog(headerMessage, bodyMessage);
				JSFUtilities.showSimpleValidationDialog();				
				holder =new Holder();
				return;
			}
			
			HolderAccountTO holderAccountTO = new HolderAccountTO();
			holderAccountTO.setParticipantTO(securityTOsearch.getIdParticipant());
			holderAccountTO.setIdHolderPk(holder.getIdHolderPk());
			holderAccountTO.setNeedHolder(Boolean.TRUE);
			holderAccountTO.setNeedParticipant(Boolean.TRUE);
			holderAccountTO.setNeedBanks(Boolean.FALSE);			
			List<HolderAccount> accountList = securityServiceFacade.getHolderAccounts(holderAccountTO);
			if(accountList.size() > 0 ){				
				holderAccounts=accountList;
				//check true when accountList return only one account
				if(holderAccounts.size() == ComponentConstant.ONE){
					holderAccounts.get(0).setSelected(true);
					HolderAccount ha = holderAccounts.get(0);
					ha.setSelected(true);
					accountSend=ha;
					//call validation of holder accounts
					validateSellerHolderAccount();
				}
			}else{
				String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
				String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_EXPIRATION_SECURITY);
				showMessageOnDialog(headerMessage, bodyMessage);
				JSFUtilities.showSimpleValidationDialog();
				holder=new Holder();
				return;
			}
			
		}
	}
	/**
	 * Methods to load parameters.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCboParemetersAccount() throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		List<Integer> masterPks = new ArrayList<Integer>(); 
		masterPks.add(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
		masterPks.add(MasterTableType.ACCOUNTS_TYPE.getCode());
		parameterTableTO.setLstMasterTableFk(masterPks);
		List<ParameterTable> lst = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		for(ParameterTable pt : lst ){
			mapParameters.put(pt.getParameterTablePk(), pt);
		}
	}
	
	/**
	 * Change participant.
	 */
	public void changeParticipant(){
		holder=new Holder();
	}
	
	/**
	 * Gets the list security selected.
	 *
	 * @return the listSecuritySelected
	 */
	public SecurityOverdueTO[] getListSecuritySelected() {
		return listSecuritySelected;
	}

	/**
	 * Sets the list security selected.
	 *
	 * @param listSecuritySelected the listSecuritySelected to set
	 */
	public void setListSecuritySelected(SecurityOverdueTO[] listSecuritySelected) {
		this.listSecuritySelected = listSecuritySelected;
	}

	/**
	 * Gets the security service facade.
	 *
	 * @return the securityServiceFacade
	 */
	public SecurityServiceFacade getSecurityServiceFacade() {
		return securityServiceFacade;
	}

	/**
	 * Sets the security service facade.
	 *
	 * @param securityServiceFacade the securityServiceFacade to set
	 */
	public void setSecurityServiceFacade(SecurityServiceFacade securityServiceFacade) {
		this.securityServiceFacade = securityServiceFacade;
	}

	/**
	 * Gets the security t osearch.
	 *
	 * @return the securityTOsearch
	 */
	public SecurityTO getSecurityTOsearch() {
		return securityTOsearch;
	}

	/**
	 * Sets the security t osearch.
	 *
	 * @param securityTOsearch the securityTOsearch to set
	 */
	public void setSecurityTOsearch(SecurityTO securityTOsearch) {
		this.securityTOsearch = securityTOsearch;
	}

	/**
	 * Gets the lst cbo filter currency.
	 *
	 * @return the lstCboFilterCurrency
	 */
	public List<ParameterTable> getLstCboFilterCurrency() {
		return lstCboFilterCurrency;
	}

	/**
	 * Sets the lst cbo filter currency.
	 *
	 * @param lstCboFilterCurrency the lstCboFilterCurrency to set
	 */
	public void setLstCboFilterCurrency(List<ParameterTable> lstCboFilterCurrency) {
		this.lstCboFilterCurrency = lstCboFilterCurrency;
	}

	/**
	 * Gets the lst cbo security state.
	 *
	 * @return the lstCboSecurityState
	 */
	public List<ParameterTable> getLstCboSecurityState() {
		return lstCboSecurityState;
	}

	/**
	 * Sets the lst cbo security state.
	 *
	 * @param lstCboSecurityState the lstCboSecurityState to set
	 */
	public void setLstCboSecurityState(List<ParameterTable> lstCboSecurityState) {
		this.lstCboSecurityState = lstCboSecurityState;
	}

	/**
	 * Gets the lst cbo filter securitie class.
	 *
	 * @return the lstCboFilterSecuritieClass
	 */
	public List<ParameterTable> getLstCboFilterSecuritieClass() {
		return lstCboFilterSecuritieClass;
	}

	/**
	 * Sets the lst cbo filter securitie class.
	 *
	 * @param lstCboFilterSecuritieClass the lstCboFilterSecuritieClass to set
	 */
	public void setLstCboFilterSecuritieClass(
			List<ParameterTable> lstCboFilterSecuritieClass) {
		this.lstCboFilterSecuritieClass = lstCboFilterSecuritieClass;
	}

	/**
	 * Gets the security data model.
	 *
	 * @return the securityDataModel
	 */
	public GenericDataModel<SecurityOverdueTO> getSecurityDataModel() {
		return securityDataModel;
	}

	/**
	 * Sets the security data model.
	 *
	 * @param securityDataModel the securityDataModel to set
	 */
	public void setSecurityDataModel(GenericDataModel<SecurityOverdueTO> securityDataModel) {
		this.securityDataModel = securityDataModel;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the security to set
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets the parameter table to.
	 *
	 * @return the parameterTableTO
	 */
	public ParameterTableTO getParameterTableTO() {
		return parameterTableTO;
	}

	/**
	 * Sets the parameter table to.
	 *
	 * @param parameterTableTO the parameterTableTO to set
	 */
	public void setParameterTableTO(ParameterTableTO parameterTableTO) {
		this.parameterTableTO = parameterTableTO;
	}

	/**
	 * Gets the lst cbo instrument type.
	 *
	 * @return the lstCboInstrumentType
	 */
	public List<ParameterTable> getLstCboInstrumentType() {
		return lstCboInstrumentType;
	}

	/**
	 * Sets the lst cbo instrument type.
	 *
	 * @param lstCboInstrumentType the lstCboInstrumentType to set
	 */
	public void setLstCboInstrumentType(List<ParameterTable> lstCboInstrumentType) {
		this.lstCboInstrumentType = lstCboInstrumentType;
	}

	/**
	 * Gets the send type parameter list.
	 *
	 * @return the sendTypeParameterList
	 */
	public List<ParameterTable> getSendTypeParameterList() {
		return sendTypeParameterList;
	}

	/**
	 * Sets the send type parameter list.
	 *
	 * @param sendTypeParameterList the sendTypeParameterList to set
	 */
	public void setSendTypeParameterList(List<ParameterTable> sendTypeParameterList) {
		this.sendTypeParameterList = sendTypeParameterList;
	}

	/**
	 * Gets the participant list.
	 *
	 * @return the participantList
	 */
	public List<Participant> getParticipantList() {
		return participantList;
	}

	/**
	 * Sets the participant list.
	 *
	 * @param participantList the participantList to set
	 */
	public void setParticipantList(List<Participant> participantList) {
		this.participantList = participantList;
	}

	/**
	 * Gets the issuer helper search.
	 *
	 * @return the issuerHelperSearch
	 */
	public Issuer getIssuerHelperSearch() {
		return issuerHelperSearch;
	}

	/**
	 * Sets the issuer helper search.
	 *
	 * @param issuerHelperSearch the issuerHelperSearch to set
	 */
	public void setIssuerHelperSearch(Issuer issuerHelperSearch) {
		this.issuerHelperSearch = issuerHelperSearch;
	}

	/**
	 * Gets the list participant.
	 *
	 * @return the listParticipant
	 */
	public List<Participant> getListParticipant() {
		return listParticipant;
	}

	/**
	 * Sets the list participant.
	 *
	 * @param listParticipant the listParticipant to set
	 */
	public void setListParticipant(List<Participant> listParticipant) {
		this.listParticipant = listParticipant;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the holder to set
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Gets the holder accounts.
	 *
	 * @return the holderAccounts
	 */
	public List<HolderAccount> getHolderAccounts() {
		return holderAccounts;
	}

	/**
	 * Sets the holder accounts.
	 *
	 * @param holderAccounts the holderAccounts to set
	 */
	public void setHolderAccounts(List<HolderAccount> holderAccounts) {
		this.holderAccounts = holderAccounts;
	}

	/**
	 * Gets the account send.
	 *
	 * @return the accountSend
	 */
	public HolderAccount getAccountSend() {
		return accountSend;
	}

	/**
	 * Sets the account send.
	 *
	 * @param accountSend the accountSend to set
	 */
	public void setAccountSend(HolderAccount accountSend) {
		this.accountSend = accountSend;
	}

	/**
	 * Gets the map parameters.
	 *
	 * @return the mapParameters
	 */
	public HashMap<Integer, ParameterTable> getMapParameters() {
		return mapParameters;
	}

	/**
	 * Sets the map parameters.
	 *
	 * @param mapParameters the mapParameters to set
	 */
	public void setMapParameters(HashMap<Integer, ParameterTable> mapParameters) {
		this.mapParameters = mapParameters;
	}

	/**
	 * Gets the max date.
	 *
	 * @return the maxDate
	 */
	public Date getMaxDate() {
		return maxDate;
	}

	/**
	 * Sets the max date.
	 *
	 * @param maxDate the maxDate to set
	 */
	public void setMaxDate(Date maxDate) {
		this.maxDate = maxDate;
	}
	
	
}