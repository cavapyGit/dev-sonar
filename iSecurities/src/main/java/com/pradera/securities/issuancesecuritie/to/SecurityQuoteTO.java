package com.pradera.securities.issuancesecuritie.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SecurityQuoteTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class SecurityQuoteTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	
	/** The id security quote pk. */
	private Long idSecurityQuotePk;
	
	/** The issuer. */
	private Issuer issuer;
	
	/** The security. */
	private Security security;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/** The quote state. */
	private Integer quoteState;
	
	/** The quote state description. */
	private String quoteStateDescription;
	
	/** The price current. */
	private BigDecimal priceCurrent;
	
	/** The price before. */
	private BigDecimal priceBefore;
	
	/** The quote date. */
	private Date quoteDate;

	/** The request type. */
	private Integer requestType;
	
	/** The request type description. */
	private String requestTypeDescription;
	
	/** The share capital. */
	private BigDecimal shareCapital;
	
	/** The share balance. */
	private BigDecimal shareBalance;
	
	/** The last modify user. */
	private String lastModifyUser;
	
	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}

	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	/**
	 * Gets the quote state.
	 *
	 * @return the quote state
	 */
	public Integer getQuoteState() {
		return quoteState;
	}

	/**
	 * Sets the quote state.
	 *
	 * @param quoteState the new quote state
	 */
	public void setQuoteState(Integer quoteState) {
		this.quoteState = quoteState;
	}

	/**
	 * Instantiates a new security quote to.
	 */
	public SecurityQuoteTO() {
		super();
		issuer = new Issuer();
		security = new Security();
	}

	/**
	 * Gets the id security quote pk.
	 *
	 * @return the id security quote pk
	 */
	public Long getIdSecurityQuotePk() {
		return idSecurityQuotePk;
	}
	/**
	 * Sets the id security quote pk.
	 *
	 * @param idSecurityQuotePk the new id security quote pk
	 */
	public void setIdSecurityQuotePk(Long idSecurityQuotePk) {
		this.idSecurityQuotePk = idSecurityQuotePk;
	}

	/**
	 * Instantiates a new security quote to.
	 *
	 * @param idSecurityQuotePk the id security quote pk
	 * @param idIssuerPk the id issuer pk
	 * @param valueCurrent the value current
	 * @param valueBefore the value before
	 * @param quoteDate the quote date
	 * @param security the security
	 * @param quoteState the quote state
	 * @param requestType the request type
	 * @param shareBalance the share balance
	 * @param shareCapital the share capital
	 * @param lastModifyUser the last modify user
	 */
	public SecurityQuoteTO(Long idSecurityQuotePk,String idIssuerPk, 
			BigDecimal valueCurrent, BigDecimal valueBefore, Date quoteDate, String security, Integer quoteState, Integer requestType,
			BigDecimal shareBalance, BigDecimal shareCapital, String lastModifyUser) {
		super();
		Issuer iss = new Issuer();
		iss.setIdIssuerPk(idIssuerPk);
		
		this.issuer = iss;
		this.idSecurityQuotePk = idSecurityQuotePk;
		this.security = new Security(security);
		this.quoteDate = quoteDate;
		this.quoteState = quoteState;
		this.priceCurrent = valueCurrent;
		this.priceBefore = valueBefore;
		this.requestType = requestType;
		this.shareBalance=shareBalance;
		this.shareCapital=shareCapital;
		this.lastModifyUser=lastModifyUser;
	}

	/**
	 * Gets the price current.
	 *
	 * @return the price current
	 */
	public BigDecimal getPriceCurrent() {
		return priceCurrent;
	}

	/**
	 * Sets the price current.
	 *
	 * @param priceCurrent the new price current
	 */
	public void setPriceCurrent(BigDecimal priceCurrent) {
		this.priceCurrent = priceCurrent;
	}

	/**
	 * Gets the price before.
	 *
	 * @return the price before
	 */
	public BigDecimal getPriceBefore() {
		return priceBefore;
	}

	/**
	 * Sets the price before.
	 *
	 * @param priceBefore the new price before
	 */
	public void setPriceBefore(BigDecimal priceBefore) {
		this.priceBefore = priceBefore;
	}

	/**
	 * Gets the quote date.
	 *
	 * @return the quote date
	 */
	public Date getQuoteDate() {
		return quoteDate;
	}

	/**
	 * Sets the quote date.
	 *
	 * @param quoteDate the new quote date
	 */
	public void setQuoteDate(Date quoteDate) {
		this.quoteDate = quoteDate;
	}

	/**
	 * Gets the quote state description.
	 *
	 * @return the quote state description
	 */
	public String getQuoteStateDescription() {
		return quoteStateDescription;
	}

	/**
	 * Sets the quote state description.
	 *
	 * @param quoteStateDescription the new quote state description
	 */
	public void setQuoteStateDescription(String quoteStateDescription) {
		this.quoteStateDescription = quoteStateDescription;
	}

	/**
	 * Gets the request type.
	 *
	 * @return the request type
	 */
	public Integer getRequestType() {
		return requestType;
	}

	/**
	 * Sets the request type.
	 *
	 * @param requestType the new request type
	 */
	public void setRequestType(Integer requestType) {
		this.requestType = requestType;
	}

	/**
	 * Gets the request type description.
	 *
	 * @return the request type description
	 */
	public String getRequestTypeDescription() {
		return requestTypeDescription;
	}

	/**
	 * Sets the request type description.
	 *
	 * @param requestTypeDescription the new request type description
	 */
	public void setRequestTypeDescription(String requestTypeDescription) {
		this.requestTypeDescription = requestTypeDescription;
	}

	/**
	 * Gets the share capital.
	 *
	 * @return the share capital
	 */
	public BigDecimal getShareCapital() {
		return shareCapital;
	}

	/**
	 * Sets the share capital.
	 *
	 * @param shareCapital the new share capital
	 */
	public void setShareCapital(BigDecimal shareCapital) {
		this.shareCapital = shareCapital;
	}

	/**
	 * Gets the share balance.
	 *
	 * @return the share balance
	 */
	public BigDecimal getShareBalance() {
		return shareBalance;
	}

	/**
	 * Sets the share balance.
	 *
	 * @param shareBalance the new share balance
	 */
	public void setShareBalance(BigDecimal shareBalance) {
		this.shareBalance = shareBalance;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

}