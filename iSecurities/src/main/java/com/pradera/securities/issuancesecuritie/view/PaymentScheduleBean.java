package com.pradera.securities.issuancesecuritie.view;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.AESKeyType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.AESEncryptUtils;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericColumnModel;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.to.InternationalDepositoryTO;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.view.IssuanceHelperBean;
import com.pradera.core.component.helperui.view.SecuritiesHelperBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.accounts.type.InternationalDepositoryStateType;
import com.pradera.model.accounts.type.MechanismModalityStateType;
import com.pradera.model.accounts.type.NegotiationMechanismStateType;
import com.pradera.model.generalparameter.FinancialIndicator;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.FinancialIndicatorStateType;
import com.pradera.model.generalparameter.type.GeographicLocationStateType;
import com.pradera.model.generalparameter.type.GeographicLocationType;
import com.pradera.model.generalparameter.type.IndicatorType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.AmoPaymentScheduleReqHi;
import com.pradera.model.issuancesecuritie.AmortizationPaymentSchedule;
import com.pradera.model.issuancesecuritie.CfiAttribute;
import com.pradera.model.issuancesecuritie.CfiCategory;
import com.pradera.model.issuancesecuritie.CfiGroup;
import com.pradera.model.issuancesecuritie.IntPaymentScheduleReqHi;
import com.pradera.model.issuancesecuritie.InterestPaymentSchedule;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.ProgramAmoCouponReqHi;
import com.pradera.model.issuancesecuritie.ProgramAmortizationCoupon;
import com.pradera.model.issuancesecuritie.ProgramIntCouponReqHi;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.SecurityForeignDepository;
import com.pradera.model.issuancesecuritie.SecurityInvestor;
import com.pradera.model.issuancesecuritie.SecurityNegotiationMechanism;
import com.pradera.model.issuancesecuritie.type.AmortizationOnType;
import com.pradera.model.issuancesecuritie.type.AmortizationType;
import com.pradera.model.issuancesecuritie.type.CalendarDayType;
import com.pradera.model.issuancesecuritie.type.CalendarMonthType;
import com.pradera.model.issuancesecuritie.type.CalendarType;
import com.pradera.model.issuancesecuritie.type.CapitalPaymentModalityType;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.InterestPeriodicityType;
import com.pradera.model.issuancesecuritie.type.InterestType;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.OperationType;
import com.pradera.model.issuancesecuritie.type.PaymentScheduleReqRegisterType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;
import com.pradera.model.issuancesecuritie.type.RequestStateType;
import com.pradera.model.issuancesecuritie.type.SecurityForeignDepositoryStateType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.negotiation.MechanismModality;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.type.NegotiationModalityStateType;
import com.pradera.securities.issuancesecuritie.facade.PaymentCronogramServiceFacade;
import com.pradera.securities.issuancesecuritie.facade.SecurityServiceFacade;
import com.pradera.securities.issuancesecuritie.to.ExceDataTO;
import com.pradera.securities.issuancesecuritie.to.IssuanceTO;
import com.pradera.securities.issuancesecuritie.to.PaymentScheduleResultTO;
import com.pradera.securities.issuancesecuritie.to.PaymentScheduleTO;
import com.pradera.securities.issuancesecuritie.util.PropertiesConstants;
import com.pradera.securities.issuancesecuritie.util.SecuritiesUtils;

// TODO: Auto-generated Javadoc
/** <ul>
 * <li>
 * Project iDepositary. Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PaymentScheduleBean.
 * 
 * 
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015 */
@DepositaryWebBean
@LoggerCreateBean
// @ManagedBean(name="paymentScheduleBean")
public class PaymentScheduleBean extends GenericBaseBean implements Serializable {

	private static final Integer REQUEST_CONFIRM = Integer.valueOf(1347);
	private static final Integer REQUEST_REJECTED = Integer.valueOf(1348);
	private static final Integer REQUEST_REGISTERED = Integer.valueOf(1349);

	private static final int READ_SUCCESS = 0;
	private static final int REQUIRED_NUMERIC = 1;
	private static final int REQUIRED_FORMULA = 2;
	private static final int REQUIRED_VALUE = 3;
	private static final int POSITION_FIRSTCOUPON = 4;
	private static final int REQUIRED_FORMULA_OR_DATE = 5;
	private static final int REQUIRED_FORMULA_OR_NUMERIC = 6;
	private static final int ERROR_EXCEPTION = 7;
	private static final int UNBALANCE = 8;
	private static final int UNBALANCE_NOMINAL_VALUE = 9;
	private static final int PERCENTAGE_OUT_OF_RANGE = 10;
	private static final int INVALID_EXPIRATION_DATE = 11;
	private static final int INVALID_ROWS_NUMBER = 12;
	private static final String PLEASE_VERIFY = ". Por favor Verifique.";

	@Inject
	private transient PraderaLogger log;

	/** The country residence. */
	@Inject
	@Configurable
	Integer countryResidence;

	/** The security service facade. */
	@EJB
	private SecurityServiceFacade securityServiceFacade;
	// @EJB
	// private IssuanceSecuritiesServiceFacade issuanceSecuritiesServiceFacade;
	/** The payment cronogram service facade. */
	@EJB
	private PaymentCronogramServiceFacade paymentCronogramServiceFacade;

	/** The general parameter facade. */
	@EJB
	private GeneralParametersFacade generalParameterFacade;

	/** The issuer helper bean. */
	// @Inject
	// private IssuerHelperBean issuerHelperBean;

	/** The issuance helper bean. */
	@Inject
	private IssuanceHelperBean issuanceHelperBean;

	/** The securities helper bean. */
	@Inject
	private SecuritiesHelperBean securitiesHelperBean;

	/** The securities utils. */
	@Inject
	private SecuritiesUtils securitiesUtils;

	// @Inject
	// private SecuritiesHelperBean securitiesHelperBean;
	/** The user info. */
	@Inject
	UserInfo userInfo;

	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;

	private boolean selectAllCoupons;
	private boolean selectAllCouponsInterest;

	/** The interest payment schedule type. */
	private Integer INTEREST_PAYMENT_SCHEDULE_TYPE = Integer.valueOf(1);

	/** The amortization payment schedule type. */
	private Integer AMORTIZATION_PAYMENT_SCHEDULE_TYPE = Integer.valueOf(2);

	/** The security. */
	private Security security;

	/** The is Exchange Rate. */
	private boolean isExchangeRate;

	/** The int payment schedule req hi. */
	private IntPaymentScheduleReqHi intPaymentScheduleReqHi;

	/** The amo payment schedule req hi. */
	private AmoPaymentScheduleReqHi amoPaymentScheduleReqHi;

	/** The program int coupon req hi aux. */
	private ProgramIntCouponReqHi programIntCouponReqHiAux;

	/** The program amo coupon req hi aux. */
	private ProgramAmoCouponReqHi programAmoCouponReqHiAux;

	/** The program amortization coupon. */
	private ProgramAmortizationCoupon programAmortizationCoupon;

	/** The program amo coupon req hi. */
	private ProgramAmoCouponReqHi programAmoCouponReqHi;

	/** The security helper mgmt out. */
	private Security securityHelperMgmtOut;

	/** The payment schedule to. */
	private PaymentScheduleTO paymentScheduleTO;

	/** The payment schedule result to. */
	private PaymentScheduleResultTO paymentScheduleResultTO;

	/** The issuance t omgmt. */
	private IssuanceTO issuanceTOmgmt;

	/** The Security to. */
	private SecurityTO SecurityTO;

	/** The parameter table to. */
	private ParameterTableTO parameterTableTO;

	/** The issuance data model. */
	private IssuanceDataModel issuanceDataModel;

	/** The security data model. */
	private SecurityDataModel securityDataModel;

	/** The initial int payment schedule req hi. */
	private String initialIntPaymentScheduleReqHi;

	/** The initial amo payment schedule req hi. */
	private String initialAmoPaymentScheduleReqHi;

	/** The view operation int pay she modification. */
	private boolean viewOperationIntPaySheModification;

	/** The view operation amo pay she modification. */
	private boolean viewOperationAmoPaySheModification;

	/** The amortization payment entered. */
	private boolean amortizationPaymentEntered;

	/** The payment schedule data model. */
	private GenericDataModel<PaymentScheduleResultTO> paymentScheduleDataModel;

	// User only in mb
	/** The lst dtb pro int coupon req his. */
	private List<ProgramIntCouponReqHi> lstDtbProIntCouponReqHis;
	
	private List<ProgramIntCouponReqHi> lstDtbProIntCouponReqHisPreview=new ArrayList<>();

	/** The lst dtb pro amo coupon req his. */
	private List<ProgramAmoCouponReqHi> lstDtbProAmoCouponReqHis;
	
	private List<ProgramAmoCouponReqHi> lstDtbProAmoCouponReqHisPreview=new ArrayList<>();

	/** The lst selected inversionists. */
	@NotNull
	private List<String> lstSelectedInversionists;

	/** The lst cbo cfi categories. */
	private List<CfiCategory> lstCboCFICategories;

	/** The lst cbo cfi groups. */
	private List<CfiGroup> lstCboCFIGroups;

	/** The lst cbo cfi attribute1. */
	private List<CfiAttribute> lstCboCFIAttribute1;

	/** The lst cbo cfi attribute2. */
	private List<CfiAttribute> lstCboCFIAttribute2;

	/** The lst cbo cfi attribute3. */
	private List<CfiAttribute> lstCboCFIAttribute3;

	/** The lst cbo cfi attribute4. */
	private List<CfiAttribute> lstCboCFIAttribute4;

	/** The lst cbo instrument type. */
	private List<ParameterTable> lstCboInstrumentType;

	/** The lst cbo securitie type. */
	private List<ParameterTable> lstCboSecuritieType;

	/** The lst cbo security term. */
	private List<ParameterTable> lstCboSecurityTerm;

	/** The lst cbo securitie class. */
	private List<ParameterTable> lstCboSecuritieClass;

	/** The lst cbo issuance state. */
	private List<ParameterTable> lstCboIssuanceState;

	/** The lst cbo geographic location. */
	private List<GeographicLocation> lstCboGeographicLocation;

	/** The lst cbo calendar month. */
	private List<ParameterTable> lstCboCalendarMonth;

	/** The lst cbo currency. */
	private List<ParameterTable> lstCboCurrency;

	/** The lst cbo issuance type. */
	private List<ParameterTable> lstCboIssuanceType;

	/** The lst cbo security state. */
	private List<ParameterTable> lstCboSecurityState;

	/** The lst cbo boolean type. */
	private List<BooleanType> lstCboBooleanType;

	/** The lst cbo interest type. */
	private List<ParameterTable> lstCboInterestType;

	/** The lst cbo calendar type. */
	private List<ParameterTable> lstCboCalendarType;

	/** The lst cbo calendar days. */
	private List<ParameterTable> lstCboCalendarDays;

	/** The lst cbo payment modality. */
	private List<ParameterTable> lstCboPaymentModality;

	/** The lst cbo rate type. */
	private List<ParameterTable> lstCboRateType;

	/** The lst cbo operation type. */
	private List<ParameterTable> lstCboOperationType;

	/** The lst cbo interest periodicity. */
	private List<ParameterTable> lstCboInterestPeriodicity;

	/** The lst cbo yield. */
	private List<ParameterTable> lstCboYield;

	/** The lst cbo cash nominal. */
	private List<ParameterTable> lstCboCashNominal;

	/** The lst cbo amortization type. */
	private List<ParameterTable> lstCboAmortizationType;

	/** The lst cbo amortization on. */
	private List<ParameterTable> lstCboAmortizationOn;

	/** The lst cbo indexed to. */
	private List<ParameterTable> lstCboIndexedTo;

	/** The lst cbo investor type. */
	private List<ParameterTable> lstCboInvestorType;

	/** The lst cbo financial index. */
	private List<ParameterTable> lstCboFinancialIndex;

	/** The lst cbo security block motive. */
	private List<ParameterTable> lstCboSecurityBlockMotive;

	/** The lst cbo security unblock motive. */
	private List<ParameterTable> lstCboSecurityUnblockMotive;

	/** The lst cbo security motive aux. */
	private List<ParameterTable> lstCboSecurityMotiveAux; // Auxiliar

	/** The lst cbo credit rating scales. */
	private List<ParameterTable> lstCboCreditRatingScales;

	/** The lst cbo economic activity. */
	private List<ParameterTable> lstCboEconomicActivity;

	/** The lst cbo economic sector. */
	private List<ParameterTable> lstCboEconomicSector;

	/** The lst cbo request state. */
	private List<ParameterTable> lstCboRequestState;

	/** The lst cbo interest payment modality. */
	private List<ParameterTable> lstCboInterestPaymentModality;

	/** The lst cbo financial periodicity. */
	private List<ParameterTable> lstCboFinancialPeriodicity;

	/** The lst dtb international depository. */
	private List<InternationalDepository> lstDtbInternationalDepository;

	/** The lst dtb negotiation modalities. */
	private List<NegotiationModality> lstDtbNegotiationModalities;

	/** The mechanism columns model. */
	private List<GenericColumnModel> mechanismColumnsModel;

	/** The lst cbo reject motive. */
	private List<ParameterTable> lstCboRejectMotive;

	/** The bl short term. */
	private boolean blLongTerm, blShortTerm;

	/** The payment schedule modification page. */
	private boolean paymentScheduleModificationPage;

	/** The payment schedule restructuration page. */
	private boolean paymentScheduleRestructurationPage;

	/** The payment schedule type selected. */
	private Integer paymentScheduleTypeSelected;

	/** The amortization total factor. */
	private BigDecimal amortizationTotalFactor;

	/** The interes rate selected. */
	private BigDecimal interesRateSelected;

	/** The _360_ calendar day. */
	private Integer _360_CalendarDay = CalendarDayType._360.getCode();

	/** The issuer helper search. */
	private Issuer issuerHelperSearch;

	/** The index tab. */
	private Integer indexTab;

	private List<ExceDataTO> lDatosExcel;
	
	private List<ProgramIntCouponReqHi> lstDtbProIntCouponReqHisCopy = new ArrayList<ProgramIntCouponReqHi>();


	/** Inits the. */
	@PostConstruct
	public void init() {
		try {
			super.setParametersTableMap(new HashMap<Integer, Object>());

			allHolidays = generalParameterFacade.getAllHolidaysByCountry(countryResidence);
			setViewOperationType(ViewOperationsType.CONSULT.getCode());

			issuerHelperSearch = new Issuer();

			paymentScheduleTO = new PaymentScheduleTO();
			securityHelperMgmtOut = new Security();
			loadCboRequestState();
			loadCboSecurieClass();
			loadCboCurrency();
			loadCboRejectMotive();

			paymentScheduleTO.setBeginDate(new Date());
			paymentScheduleTO.setEndDate(new Date());

			programAmortizationCoupon = new ProgramAmortizationCoupon();
			programAmoCouponReqHi = new ProgramAmoCouponReqHi();

			lDatosExcel = new ArrayList<ExceDataTO>();

			// por defecto se ninguncupon estA seleccionado
			selectAllCoupons = false;
			selectAllCouponsInterest = false;
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	// method to select or deselect all amortization coupons according to whether or not the global box is checked
	/** metodo para seleccionar o deseleccionar todos los cupones de amortizacion segun se marque o no la casilla global */
	public void selectAndUnselectAllCouponsAmortization() {
		if (selectAllCoupons) {
			// seleccionando todos los cupones vigentes
			for (ProgramAmoCouponReqHi programAmoCouponReqHi : lstDtbProAmoCouponReqHis) {
				if (programAmoCouponReqHi.getStateProgramAmortizaton().equals(ProgramScheduleStateType.PENDING.getCode())) {
					programAmoCouponReqHi.setSelected(true);
					programAmoCouponReqHi.setDisabled(false);
				}
			}
		} else {
			// deseleccionando todos los cupones vigentes
			for (ProgramAmoCouponReqHi programAmoCouponReqHi : lstDtbProAmoCouponReqHis) {
				if (programAmoCouponReqHi.getStateProgramAmortizaton().equals(ProgramScheduleStateType.PENDING.getCode())) {
					programAmoCouponReqHi.setSelected(false);
					programAmoCouponReqHi.setDisabled(true);
				}
			}
		}
	}

	// method to select or deselect all interest coupons according to whether or not the global box is checked
	/** metodo para seleccionar o deseleccionar todos los cupones de interEs segun se marque o no la casilla global */
	public void selectAndUnselectAllCouponsInterest() {
		if (selectAllCouponsInterest) {
			// seleccionando todos los cupones vigentes
			for (ProgramIntCouponReqHi programIntCouponReqHi : lstDtbProIntCouponReqHis) {
				if (programIntCouponReqHi.getStateProgramInterest().equals(ProgramScheduleStateType.PENDING.getCode())) {
					programIntCouponReqHi.setSelected(true);
					programIntCouponReqHi.setDisabled(false);
				}
			}
		} else {
			// deseleccionando todos los cupones vigentes
			for (ProgramIntCouponReqHi programIntCouponReqHi : lstDtbProIntCouponReqHis) {
				if (programIntCouponReqHi.getStateProgramInterest().equals(ProgramScheduleStateType.PENDING.getCode())) {
					programIntCouponReqHi.setSelected(false);
					programIntCouponReqHi.setDisabled(true);
				}
			}
		}
	}

	/** metodo para obtener el mensaje segun el idSalida
	 * 
	 * @param idSalida
	 * @return */
	public String getSalidaMensaje(int idSalida) {
		String msg = "";
		switch (idSalida) {
		case READ_SUCCESS:
			msg = PropertiesUtilities.getMessage("msg.read.success");
			break;
		case REQUIRED_NUMERIC:
			msg = PropertiesUtilities.getMessage("msg.required.numeric");
			break;
		case REQUIRED_FORMULA:
			msg = PropertiesUtilities.getMessage("msg.required.formula");
			break;
		case REQUIRED_VALUE:
			msg = PropertiesUtilities.getMessage("msg.required.value");
			break;
		case POSITION_FIRSTCOUPON:
			msg = PropertiesUtilities.getMessage("msg.position.firstcoupon");
			break;
		case REQUIRED_FORMULA_OR_DATE:
			msg = PropertiesUtilities.getMessage("msg.required.formula.or.date");
			break;
		case REQUIRED_FORMULA_OR_NUMERIC:
			msg = PropertiesUtilities.getMessage("msg.required.formula.or.numeric");
			break;
		case ERROR_EXCEPTION:
			msg = PropertiesUtilities.getMessage("msg.error.exception");
			break;
		case UNBALANCE:
			msg = PropertiesUtilities.getMessage("msg.unbalance");
			break;
		case UNBALANCE_NOMINAL_VALUE:
			msg = PropertiesUtilities.getMessage("msg.unbalance.nominal.value");
			break;
		case PERCENTAGE_OUT_OF_RANGE:
			msg = PropertiesUtilities.getMessage("msg.percentage.out.of.range");
			break;
		case INVALID_EXPIRATION_DATE:
			msg = PropertiesUtilities.getMessage("msg.error.invalid.expirationDate.lastCupon");
			break;
		case INVALID_ROWS_NUMBER:
			msg = PropertiesUtilities.getMessage("msg.invalid.rows.number");
			break;
		}
		return msg;
	}

	/** metodo para leer los datos del archivo Excel para las amortizaciones
	 * 
	 * @param entryFile
	 * @return */
	public String[] readInterestAndAmortizationCoupons(InputStream entryFile) {

		String[] infoSalida = new String[2];
		infoSalida[0] = "" + READ_SUCCESS;
		infoSalida[1] = getSalidaMensaje(READ_SUCCESS);

		try {

			XSSFWorkbook libro = new XSSFWorkbook(entryFile);

			XSSFSheet hoja;
			XSSFRow fila;
			XSSFCell celda;

			hoja = libro.getSheetAt(0);

			lDatosExcel = new ArrayList<ExceDataTO>();
			ExceDataTO datosExcel;

			// posicion de las columnas de las que se extraeran los datos
			int colExpiredDateExtracIndex = 3;
			int colAmortizationFactorExtracIndex = 5;
			int colAmortizationExtracIndex = 6;
			int colInterestRateExtracIndex = 7;
			int colInterestExtracIndex = 8;
			int colInitialDateIndex = 2;
			int colPaymentDateIndex = 4;

			// hallando la cantidad max de cupones
			int rowFirstCoupon = 1;
			int colCoupon = 1;
			int maxCoupon = 0;
			int counter = 0;

			int cantfilas = hoja.getLastRowNum();

			boolean firstLoop = true;
			for (int filaExtrac = rowFirstCoupon; filaExtrac <= cantfilas; filaExtrac++) {
				fila = hoja.getRow(filaExtrac);
				if (fila != null) {
					
					
					celda = fila.getCell(colCoupon);
					if (celda != null) {
						counter +=1;	
						if (celda.getCellType() == XSSFCell.CELL_TYPE_NUMERIC) {
							maxCoupon = (int) (celda.getNumericCellValue());
						} else if (celda.getCellType() == XSSFCell.CELL_TYPE_FORMULA) {
							if (celda.getCachedFormulaResultType() == Cell.CELL_TYPE_NUMERIC) {
								maxCoupon = (int) (celda.getNumericCellValue());
							}
						} else {
							log.info("::: El ultimo cupon es: " + maxCoupon + "En la celda: " + celda.getReference());
							break;
						}

						if (firstLoop) {
							firstLoop = false;
							if (maxCoupon != 1) {
								log.info("::: se encontro otro valor distinto de 1 en la celda B10");
								infoSalida[0] = "" + POSITION_FIRSTCOUPON;
								infoSalida[1] = getSalidaMensaje(POSITION_FIRSTCOUPON) + PLEASE_VERIFY;
								entryFile.close();
								return infoSalida;
							}
						}
					}
				} else {
					if (firstLoop) {
						infoSalida[0] = "" + POSITION_FIRSTCOUPON;
						infoSalida[1] = getSalidaMensaje(POSITION_FIRSTCOUPON) + PLEASE_VERIFY;
						entryFile.close();
						return infoSalida;
					} else {
						firstLoop = false;
					}
				}
			}
			
			//INTERES
			if(paymentScheduleTypeSelected.equals(1)) {
				if(counter != lstDtbProIntCouponReqHis.size()) {
					infoSalida[0] = "" + INVALID_ROWS_NUMBER;
					infoSalida[1] = getSalidaMensaje(INVALID_ROWS_NUMBER);
					entryFile.close();
					return infoSalida;
				}
			} 
			//AMORTIZACION
			else if (paymentScheduleTypeSelected.equals(2)) {
				if(counter != lstDtbProAmoCouponReqHis.size()) {
					infoSalida[0] = "" + INVALID_ROWS_NUMBER;
					infoSalida[1] = getSalidaMensaje(INVALID_ROWS_NUMBER);
					entryFile.close();
					return infoSalida;
				}
			}

			int lastRowToRead = rowFirstCoupon + maxCoupon;
			int nroCoupon = 1;

			BigDecimal valorPorcentaje = BigDecimal.ZERO;
			BigDecimal minPorceje = BigDecimal.ZERO;
			BigDecimal maxPorcentaje = BigDecimal.TEN.multiply(BigDecimal.TEN);

			// Extrayendo los los datos
			for (int filaExtrac = rowFirstCoupon; filaExtrac < lastRowToRead; filaExtrac++) {
				fila = hoja.getRow(filaExtrac);
				if (fila != null) {
					datosExcel = new ExceDataTO();
					// numero de cupon
					datosExcel.setNroCoupon(nroCoupon++);
					
					//cargando FECHA DE INICIO
					/*celda = fila.getCell(colInitialDateIndex);
					if(celda != null) {
						if (celda.getCellType() == XSSFCell.CELL_TYPE_NUMERIC
								|| celda.getCellType() == XSSFCell.CELL_TYPE_FORMULA) {
							datosExcel.setInitialDate(celda.getDateCellValue());
						}
					}*/

					// cargando FECHA DE VENCIMIENTO
					celda = fila.getCell(colExpiredDateExtracIndex);
					if (celda != null) {
						if (celda.getCellType() == XSSFCell.CELL_TYPE_NUMERIC
								|| celda.getCellType() == XSSFCell.CELL_TYPE_FORMULA) {
							datosExcel.setExpirationDate(celda.getDateCellValue());
							
							//si es el ultimo cupon, validar que la fecha de vencimiento sea la misma fecha de Vencimiento del Valor
							if(filaExtrac == (lastRowToRead-1)){
								if(Validations.validateIsNotNull(security)
										&& Validations.validateIsNotNull(security.getExpirationDate())){
									// comparando las fechas de vencimiento del valor y del ultimo cupon del excel
									if(CommonsUtilities.truncateDateTime(security.getExpirationDate()).compareTo(CommonsUtilities.truncateDateTime(datosExcel.getExpirationDate())) != 0 ){
										//private static final int ERROR_EXCEPTION = 7;
										infoSalida[0] = "" + INVALID_EXPIRATION_DATE;
										infoSalida[1] = getSalidaMensaje(INVALID_EXPIRATION_DATE);
										entryFile.close();
										return infoSalida;
									}
								}
							}
							
						} else {
							infoSalida[0] = "" + REQUIRED_FORMULA_OR_DATE;
							infoSalida[1] = getSalidaMensaje(REQUIRED_FORMULA_OR_DATE)+ celda.getReference() + PLEASE_VERIFY;
							entryFile.close();
							return infoSalida;
						}
					}
					
					//cargando FECHA DE PAGO
					/*celda = fila.getCell(colPaymentDateIndex);
					if(celda != null) {
						if (celda.getCellType() == XSSFCell.CELL_TYPE_NUMERIC
								|| celda.getCellType() == XSSFCell.CELL_TYPE_FORMULA) {
							datosExcel.setPaymentDate(celda.getDateCellValue());
						}
					}*/

					// cargando FACTOR DE AMORTIZACION
					/*celda = fila.getCell(colAmortizationFactorExtracIndex);
					if (celda != null) {
						if (celda.getCellType() == XSSFCell.CELL_TYPE_NUMERIC || celda.getCellType() == XSSFCell.CELL_TYPE_FORMULA) {

							if (celda.getCellType() == XSSFCell.CELL_TYPE_NUMERIC) {

								valorPorcentaje = BigDecimal.valueOf(celda.getNumericCellValue()).multiply(BigDecimal.TEN).multiply(BigDecimal.TEN);
								// validando si el porcentaje esta dentro del rango [0 - 100]
								if (valorPorcentaje.compareTo(minPorceje) >= 0 && valorPorcentaje.compareTo(maxPorcentaje) <= 0) {
									datosExcel.setAmortizationFactor(valorPorcentaje);
								} else {
									// como es fuera de rango lanzar error de fuera de rango
									log.info("::: se encontro valor porcentual fuera de rango en la celda: " + celda.getReference());
									infoSalida[0] = "" + PERCENTAGE_OUT_OF_RANGE;
									infoSalida[1] = getSalidaMensaje(PERCENTAGE_OUT_OF_RANGE) + celda.getReference() + PLEASE_VERIFY;
									entryFile.close();
									return infoSalida;
								}
							} else if (celda.getCachedFormulaResultType() == Cell.CELL_TYPE_NUMERIC) {

								valorPorcentaje = BigDecimal.valueOf(celda.getNumericCellValue()).multiply(BigDecimal.TEN).multiply(BigDecimal.TEN);
								// validando si el porcentaje esta dentro del rango [0 - 100]
								if (valorPorcentaje.compareTo(minPorceje) >= 0 && valorPorcentaje.compareTo(maxPorcentaje) <= 0) {
									datosExcel.setAmortizationFactor(valorPorcentaje);
								} else {
									// como es fuera de rango lanzar error de fuera de rango
									log.info("::: se encontro valor porcentual fuera de rango en la celda: " + celda.getReference());
									infoSalida[0] = "" + PERCENTAGE_OUT_OF_RANGE;
									infoSalida[1] = getSalidaMensaje(PERCENTAGE_OUT_OF_RANGE) + celda.getReference() + PLEASE_VERIFY;
									entryFile.close();
									return infoSalida;
								}
							} else {
								log.info("No se encontrO valor numerico en la celda: " + celda.getReference());
								datosExcel.setAmortizationFactor(BigDecimal.ZERO);
							}
						} else {
							infoSalida[0] = "" + REQUIRED_FORMULA_OR_NUMERIC;
							infoSalida[1] = getSalidaMensaje(REQUIRED_FORMULA_OR_NUMERIC) + celda.getReference() + PLEASE_VERIFY;
							entryFile.close();
							return infoSalida;
						}
					}*/

					// cargando la AMORTIZACION
					/*celda = fila.getCell(colAmortizationExtracIndex);
					if (celda != null) {
						if (celda.getCellType() == XSSFCell.CELL_TYPE_NUMERIC || celda.getCellType() == XSSFCell.CELL_TYPE_FORMULA) {
							if (celda.getCellType() == XSSFCell.CELL_TYPE_NUMERIC) {
								datosExcel.setAmortization(BigDecimal.valueOf(celda.getNumericCellValue()));
							} else if (celda.getCachedFormulaResultType() == Cell.CELL_TYPE_NUMERIC) {
								datosExcel.setAmortization(BigDecimal.valueOf(celda.getNumericCellValue()));
							} else {
								log.info("No se encontrO valor numerico en la celda: " + celda.getReference());
								datosExcel.setAmortization(BigDecimal.ZERO);
							}
						} else {
							infoSalida[0] = "" + REQUIRED_FORMULA_OR_NUMERIC;
							infoSalida[1] = getSalidaMensaje(REQUIRED_FORMULA_OR_NUMERIC) + celda.getReference() + PLEASE_VERIFY;
							entryFile.close();
							return infoSalida;
						}
					}*/

					// cargando TASA DE INTERES
					/*celda = fila.getCell(colInterestRateExtracIndex);
					if (celda != null) {
						if (celda.getCellType() == XSSFCell.CELL_TYPE_NUMERIC || celda.getCellType() == XSSFCell.CELL_TYPE_FORMULA) {

							if (celda.getCellType() == XSSFCell.CELL_TYPE_NUMERIC) {

								valorPorcentaje = BigDecimal.valueOf(celda.getNumericCellValue()).multiply(BigDecimal.TEN).multiply(BigDecimal.TEN);
								// validando si el porcentaje esta dentro del rango [0 - 100]
								if (valorPorcentaje.compareTo(minPorceje) >= 0 && valorPorcentaje.compareTo(maxPorcentaje) <= 0) {
									datosExcel.setInterestRate(valorPorcentaje);
								} else {
									// como es fuera de rango lanzar error de fuera de rango
									log.info("::: se encontro valor porcentual fuera de rango en la celda: " + celda.getReference());
									infoSalida[0] = "" + PERCENTAGE_OUT_OF_RANGE;
									infoSalida[1] = getSalidaMensaje(PERCENTAGE_OUT_OF_RANGE) + celda.getReference() + PLEASE_VERIFY;
									entryFile.close();
									return infoSalida;
								}
							} else if (celda.getCachedFormulaResultType() == Cell.CELL_TYPE_NUMERIC) {

								valorPorcentaje = BigDecimal.valueOf(celda.getNumericCellValue()).multiply(BigDecimal.TEN).multiply(BigDecimal.TEN);
								// validando si el porcentaje esta dentro del rango [0 - 100]
								if (valorPorcentaje.compareTo(minPorceje) >= 0 && valorPorcentaje.compareTo(maxPorcentaje) <= 0) {
									datosExcel.setInterestRate(valorPorcentaje);
								} else {
									// como es fuera de rango lanzar error de fuera de rango
									log.info("::: se encontro valor porcentual fuera de rango en la celda: " + celda.getReference());
									infoSalida[0] = "" + PERCENTAGE_OUT_OF_RANGE;
									infoSalida[1] = getSalidaMensaje(PERCENTAGE_OUT_OF_RANGE) + celda.getReference() + PLEASE_VERIFY;
									entryFile.close();
									return infoSalida;
								}
							} else {
								log.info("No se encontrO valor numerico en la celda: " + celda.getReference());
								datosExcel.setInterestRate(BigDecimal.ZERO);
							}
						} else {
							infoSalida[0] = "" + REQUIRED_FORMULA_OR_NUMERIC;
							infoSalida[1] = getSalidaMensaje(REQUIRED_FORMULA_OR_NUMERIC) + celda.getReference() + PLEASE_VERIFY;
							entryFile.close();
							return infoSalida;
						}
					}*/

					// cargando INTERES
					/*celda = fila.getCell(colInterestExtracIndex);
					if (celda != null) {
						if (celda.getCellType() == XSSFCell.CELL_TYPE_NUMERIC || celda.getCellType() == XSSFCell.CELL_TYPE_FORMULA) {
							if (celda.getCellType() == XSSFCell.CELL_TYPE_NUMERIC) {
								datosExcel.setInterest(BigDecimal.valueOf(celda.getNumericCellValue()));
							} else if (celda.getCachedFormulaResultType() == Cell.CELL_TYPE_NUMERIC) {
								datosExcel.setInterest(BigDecimal.valueOf(celda.getNumericCellValue()));
							} else {
								log.info("No se encontrO valor numerico en la celda: " + celda.getReference());
								datosExcel.setInterest(BigDecimal.ZERO);
							}
						} else {
							infoSalida[0] = "" + REQUIRED_FORMULA_OR_NUMERIC;
							infoSalida[1] = getSalidaMensaje(REQUIRED_FORMULA_OR_NUMERIC) + celda.getReference() + PLEASE_VERIFY;
							entryFile.close();
							return infoSalida;
						}
					}*/

					// cargando cupon extraido del Archivo Excel
					lDatosExcel.add(datosExcel);
				}
			}

//			BigDecimal sumaValorNominal = BigDecimal.ZERO;
//			BigDecimal sumaFactorAmortizacion = BigDecimal.ZERO;
//			log.info("::: Mostrando los cupones extraidos");
//			for (ExceDataTO cupon : lDatosExcel) {
//				log.info(cupon.toString());
//				sumaFactorAmortizacion = sumaFactorAmortizacion.add(cupon.getAmortizationFactor());
//				sumaFactorAmortizacion = sumaFactorAmortizacion.setScale(8, RoundingMode.HALF_UP);
//
//				sumaValorNominal = sumaValorNominal.add(cupon.getAmortization());
//				sumaValorNominal = sumaValorNominal.setScale(8, RoundingMode.HALF_UP);
//			}
//			log.info("Sumatoria total factor amortizaciOn: " + sumaFactorAmortizacion + " %");
//			log.info("Sumatoria total amortizaciOn es: " + sumaValorNominal + " (Valor Nominal)");

			// verificando si la sumatoria del factor de amortizacion es igual al 100%
//			if (sumaFactorAmortizacion.compareTo(BigDecimal.TEN.multiply(BigDecimal.TEN)) != 0) {
//				infoSalida[0] = "" + UNBALANCE;
//				infoSalida[1] = getSalidaMensaje(UNBALANCE) + PLEASE_VERIFY;
//				entryFile.close();
//				return infoSalida;
//			}

			// verificando si la sumatoria amortizacion es igual al valor Nominal
//			if (sumaValorNominal.compareTo(security.getInitialNominalValue()) != 0) {
//				infoSalida[0] = "" + UNBALANCE_NOMINAL_VALUE;
//				infoSalida[1] = getSalidaMensaje(UNBALANCE_NOMINAL_VALUE) + "(" + security.getInitialNominalValue() + ")" + PLEASE_VERIFY;
//				entryFile.close();
//				return infoSalida;
//			}

			log.info("$%& mostrando los datos de archivo Excel");
			for (ExceDataTO cupon : lDatosExcel) {
				log.info(cupon.toString());
			}

			entryFile.close();

		} catch (FileNotFoundException e) {
			// e.printStackTrace();
			log.error("::: No se pudo encontrar archivo: " + e.getMessage());
			infoSalida[0] = "" + ERROR_EXCEPTION;
			infoSalida[1] = getSalidaMensaje(ERROR_EXCEPTION) + PLEASE_VERIFY;
		} catch (IOException e) {
			// e.printStackTrace();
			log.error("::: No se pudo leer archivo Excel: " + e.getMessage());
			infoSalida[0] = "" + ERROR_EXCEPTION;
			infoSalida[1] = getSalidaMensaje(ERROR_EXCEPTION) + PLEASE_VERIFY;
		} catch (Exception e) {
			// e.printStackTrace();
			log.error("::: Archivo invalido " + e.getMessage());

			infoSalida[0] = "" + ERROR_EXCEPTION;
			infoSalida[1] = getSalidaMensaje(ERROR_EXCEPTION) + PLEASE_VERIFY;
		}

		return infoSalida;
	}

	/** metodo para leer el archivo Excel para modificar los datos de la cuponera y amortizaciones
	 * 
	 * @param event */
	public void leerArchivoExcel(FileUploadEvent event) {

		UploadedFile archivoExcel = event.getFile();
		String cuerpoMensaje = "";
		String[] infoSalida;

		if (archivoExcel != null) {
			try {
				infoSalida = readInterestAndAmortizationCoupons(archivoExcel.getInputstream());

				int idSalidaNumeric = Integer.parseInt(infoSalida[0]);
				cuerpoMensaje = infoSalida[1];

				switch (idSalidaNumeric) {
				case READ_SUCCESS:
					log.info("::: (Datos extraidos del archivo Excel)Se validarA la cantidad de cupones");

					// verificando el nuemro de cupones del archivo Excel debe ser menor o igual al nuemro de cupones en el
					// SAN 1 es intereses, 2 Amortizaciones
					if (paymentScheduleTypeSelected.equals(INTEREST_PAYMENT_SCHEDULE_TYPE)) {
						// intreses
						lstDtbProIntCouponReqHisPreview = new ArrayList<>();
						// deseleccionando todos los cupones vigentes (esto es asi despues de leer el archivo excel)
						for (ProgramIntCouponReqHi programIntCouponReqHi : lstDtbProIntCouponReqHis) {
							if (programIntCouponReqHi.getStateProgramInterest().equals(ProgramScheduleStateType.PENDING.getCode())) {
								programIntCouponReqHi.setSelected(false);
								programIntCouponReqHi.setDisabled(true);
							}
						}
						
						// issue 1232: Nueva implementacion para el PILAT (Aumento o reduccion de cupones)
						Date previousExpirationDate=null;
						ProgramIntCouponReqHi proInt;
						
						IntPaymentScheduleReqHi ciHistory=null;
						for (ProgramIntCouponReqHi proIntAux : lstDtbProIntCouponReqHis) {
							System.out.println("::: "+proIntAux.toString());
							if(Validations.validateIsNotNull(proIntAux)
									&& Validations.validateIsNotNull(proIntAux.getIntPaymentScheduleReqHi())){
								ciHistory = proIntAux.getIntPaymentScheduleReqHi();
								break;
							}
						}
						
						if(Validations.validateIsNotNull(ciHistory)){
							int counter = 0;
							for (ExceDataTO dataExcel : lDatosExcel) {
								
								counter++;

								proInt=new ProgramIntCouponReqHi();
								proInt.setIntPaymentScheduleReqHi(ciHistory);
								
								proInt.setSelected(false);
								proInt.setDisabled(true);
								
								// calculando el nuevo cupon desde el excel
								// este calculo es el mismo que se usa en la generacion de cuponera SecurityMgmtBean -> generatePaymentCronogram()
								proInt.setCouponNumber(dataExcel.getNroCoupon());
								proInt.setCurrency(security.getCurrency());
								if(previousExpirationDate==null){
									proInt.setBeginingDate(security.getIssuanceDate() );	
								}else{
									proInt.setBeginingDate( previousExpirationDate );
								}
								if(counter == lDatosExcel.size()){
									proInt.setExpirationDate(security.getExpirationDate());
								}else{
									proInt.setExpirationDate(CommonsUtilities.truncateDateTime(dataExcel.getExpirationDate()));
								}
								previousExpirationDate=proInt.getExpirationDate();
								
								try {
									proInt.setPaymentDate( generalParameterFacade.workingDateCalculateServiceFacade(CommonsUtilities.truncateDateTime(proInt.getExpirationDate()), 
											GeneralConstants.ONE_VALUE_INTEGER) );
								} catch (ServiceException e) {
									log.error("::: Error calcular el paymentDate de un PROGRAM_INT_COUPON_REQ_HIS: "+e.getMessage());
									e.printStackTrace();
								}
								
								Integer paymentDays=null;
								if(security.isExtendedTerm()){
									paymentDays=CommonsUtilities.getDaysBetween(proInt.getBeginingDate(),proInt.getPaymentDate());
								}else{
									paymentDays=CommonsUtilities.getDaysBetween(proInt.getBeginingDate(),proInt.getExpirationDate());
								}
								proInt.setPaymentDays(paymentDays);
								
								if(proInt.getCurrency().equals(CurrencyType.UFV.getCode()) 
										|| proInt.getCurrency().equals(CurrencyType.DMV.getCode())){
									proInt.setExchangeDateRate(proInt.getPaymentDate());
									proInt.setCurrencyPayment(CurrencyType.PYG.getCode());
								}
								proInt.setCorporativeDate(CommonsUtilities.currentDate());
								
								proInt.setRegistryDate(CommonsUtilities.addDaysToDate(proInt.getExpirationDate(), security.getStockRegistryDays()*-1));
								try {
									proInt.setRegistryDate(generalParameterFacade.workingDateCalculateServiceFacade(proInt.getRegistryDate(), -1));
								} catch (ServiceException e) {
									log.error("::: Error calcular el RegistryDate de un PROGRAM_INT_COUPON_REQ_HIS: "+e.getMessage());
									e.printStackTrace();
								}
								proInt.setCutoffDate( proInt.getRegistryDate() );
								
								proInt.setInterestFactor(dataExcel.getInterestRate());
								proInt.setStateProgramInterest( ProgramScheduleStateType.PENDING.getCode() );
								
								Date securityRegistryDate=isViewOperationRegister() ? CommonsUtilities.currentDate() : security.getRegistryDate();
								if(security.isHavePaymentScheduleNoDesm()){
									if(CommonsUtilities.isLessOrEqualDate(proInt.getExpirationDate(), securityRegistryDate, true)){
										proInt.setStateProgramInterest( ProgramScheduleStateType.EXPIRED.getCode() );
										proInt.setInterestRate(BigDecimal.ZERO);
									}
								}
								
								proInt.setInterestRate(securitiesUtils.interestRateCalculate(security, 
										security.getPeriodicity(), 
										security.getInterestRate(), 
										proInt.getBeginingDate(), 
										proInt.getPaymentDays(),
										proInt.getCouponNumber(),
										null,
										null));
								
								proInt.setIndRounding(BooleanType.NO.getCode()); 
								proInt.setPayedAmount(BigDecimal.ZERO);
								
								proInt.setCouponAmount(dataExcel.getInterest());
								
								if(security.isIndistinctAmortizationPeriodicity()){
									proInt.setCouponAmount(null);
								}
								proInt.setLastModifyApp( Integer.valueOf(1) );
								proInt.setLastModifyDate( CommonsUtilities.currentDateTime() );
								proInt.setLastModifyIp( userInfo.getUserAccountSession().getIpAddress()  );
								proInt.setLastModifyUser(userInfo.getUserAccountSession().getUserName() );
								
								lstDtbProIntCouponReqHisPreview.add(proInt);
								previousExpirationDate=proInt.getExpirationDate();
							}
						}
						
						JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
						mostrarMensaje("Mensaje Informativo", cuerpoMensaje);
						JSFUtilities.showSimpleValidationDialog();
					} else {
						// amortizaciones
						lstDtbProAmoCouponReqHisPreview = new ArrayList<>();

						// deseleccionando todos los cupones vigentes (esto es asi despues de leer el archivo excel)
						for (ProgramAmoCouponReqHi programAmoCouponReqHi : lstDtbProAmoCouponReqHis) {
							if (programAmoCouponReqHi.getStateProgramAmortizaton().equals(ProgramScheduleStateType.PENDING.getCode())) {
								programAmoCouponReqHi.setSelected(false);
								programAmoCouponReqHi.setDisabled(true);
							}
						}
						
						// issue 1232: Nueva implementacion para PILAT (Aumento de amortizaciones)
						// capurando cronogramaAmortizacionHistorico
						AmoPaymentScheduleReqHi caHistory=null;
						for (ProgramAmoCouponReqHi proAmo : lstDtbProAmoCouponReqHis) {
							if(Validations.validateIsNotNull(proAmo)
									&& Validations.validateIsNotNull(proAmo.getAmoPaymentScheduleReqHi())){
								caHistory = proAmo.getAmoPaymentScheduleReqHi();
								break;
							}
						}
						
						int counter=0;
						ProgramAmoCouponReqHi proAmo;
						Date previousExpirationDate=null;
						if(Validations.validateIsNotNull(caHistory)){
							for (ExceDataTO dataExcel : lDatosExcel) {
								counter++;
								
								// construyendola amortizacion a partir de los datos del excel
								// issue 1232: este calculo esta basado en el mismo que se usa en la generacion de cuponera SecurityMgmtBean -> generatePaymentCronogram()
								proAmo = new ProgramAmoCouponReqHi();
								proAmo.setAmoPaymentScheduleReqHi(caHistory);
								proAmo.setSelected(false);
								proAmo.setDisabled(true);
								
								proAmo.setCouponNumber( dataExcel.getNroCoupon() );
								if(Validations.validateIsNullOrEmpty(previousExpirationDate))
									proAmo.setBeginingDate(security.getIssuanceDate());
								else{
									proAmo.setBeginingDate(previousExpirationDate);
								}	
								
								// fecha de expiracion
								if(counter == lDatosExcel.size()){
									proAmo.setExpirationDate(security.getExpirationDate());
								}else{
									proAmo.setExpirationDate(CommonsUtilities.truncateDateTime(dataExcel.getExpirationDate()));
								}
								previousExpirationDate = proAmo.getExpirationDate();
								proAmo.setAmortizationFactor(dataExcel.getAmortizationFactor());
								proAmo.setCouponAmount(dataExcel.getAmortization());
								try {
									proAmo.setPaymentDate( generalParameterFacade.workingDateCalculateServiceFacade(proAmo.getExpirationDate(),GeneralConstants.ONE_VALUE_INTEGER) );
								} catch (ServiceException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								proAmo.setCorporativeDate(CommonsUtilities.currentDate());
								proAmo.setCurrency( security.getCurrency() );
								if(proAmo.getCurrency().equals(CurrencyType.UFV.getCode()) 
										|| proAmo.getCurrency().equals(CurrencyType.DMV.getCode())){
									proAmo.setExchangeDateRate(proAmo.getPaymentDate());
									proAmo.setCurrencyPayment(CurrencyType.PYG.getCode());
								}
								proAmo.setRegistryDate( CommonsUtilities.addDaysToDate(proAmo.getExpirationDate(), security.getStockRegistryDays()*-1) );
								try {
									proAmo.setRegistryDate(generalParameterFacade.workingDateCalculateServiceFacade(proAmo.getRegistryDate(), -1));
								} catch (ServiceException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								proAmo.setCutoffDate( proAmo.getRegistryDate() ); 
								proAmo.setIndRounding(BooleanType.NO.getCode());
								
								if(security.getAmortizationOn().equals( AmortizationOnType.CAPITAL.getCode() )){
									proAmo.setAmortizationAmount(security.getCurrentNominalValue().multiply( security.getAmortizationPaymentSchedule().getAmortizationFactor() ).divide(BigDecimal.valueOf(100.0)) );
								}else if(security.getAmortizationOn().equals( AmortizationOnType.NOMINAL_VALUE.getCode() )){
									proAmo.setAmortizationAmount(security.getInitialNominalValue().multiply( security.getAmortizationPaymentSchedule().getAmortizationFactor() ).divide(BigDecimal.valueOf(100.0)) );
								}
								proAmo.setStateProgramAmortizaton(ProgramScheduleStateType.PENDING.getCode());
								
								Date securityRegistryDate=isViewOperationRegister() ? CommonsUtilities.currentDate() : security.getRegistryDate();
								//if(security.isHavePaymentScheduleNoDesm() && security.isPartialAmortPaymentModality() && security.isProportionalAmortizationType()){
									if(CommonsUtilities.isLessOrEqualDate(proAmo.getExpirationDate(), securityRegistryDate, true)){
										proAmo.setStateProgramAmortizaton(ProgramScheduleStateType.EXPIRED.getCode());
									}
								//}
								
								proAmo.setPayedAmount(BigDecimal.ZERO);
								proAmo.setRegistryUser(userInfo.getUserAccountSession().getUserName());
								proAmo.setRegisterDate(CommonsUtilities.currentDateTime());
								proAmo.setIndRounding(Integer.valueOf(2));
								proAmo.setLastModifyApp( Integer.valueOf(1) );
								proAmo.setLastModifyDate( CommonsUtilities.currentDateTime() );
								proAmo.setLastModifyIp( userInfo.getUserAccountSession().getIpAddress()  );
								proAmo.setLastModifyUser(userInfo.getUserAccountSession().getUserName() );
								
								lstDtbProAmoCouponReqHisPreview.add(proAmo);
							}
						}

						JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
						mostrarMensaje("Mensaje Informativo", cuerpoMensaje);
						JSFUtilities.showSimpleValidationDialog();
					}
					break;
				default:
					JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
					mostrarMensaje("Error", cuerpoMensaje);
					JSFUtilities.showSimpleValidationDialog();

					// si se produce el error, limpiar la lista
					lDatosExcel = new ArrayList<ExceDataTO>();
					break;
				}
			} catch (IOException e) {
				e.printStackTrace();
				cuerpoMensaje = PropertiesUtilities.getMessage("msg.invalid.file");

				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				mostrarMensaje("Error", cuerpoMensaje);
				JSFUtilities.showSimpleValidationDialog();

				// si se produce el error, limpiar la lista
				lDatosExcel = new ArrayList<ExceDataTO>();
			}
		} else {
			cuerpoMensaje = PropertiesUtilities.getMessage("msg.null.or.damaged.file");

			JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
			mostrarMensaje("Error", cuerpoMensaje);
			JSFUtilities.showSimpleValidationDialog();

			// si se produce el error, limpiar la lista
			lDatosExcel = new ArrayList<ExceDataTO>();
		}
	}

	/** metodo para mostrar un mensaje por pantalla
	 * 
	 * @param cabeceraMensaje
	 * @param cuerpoMensaje */
	private static void mostrarMensaje(String cabeceraMensaje, String cuerpoMensaje) {
		JSFUtilities.putViewMap("bodyMessageView", cuerpoMensaje);
		JSFUtilities.putViewMap("headerMessageView", cabeceraMensaje);
	}

	/** Register operation handler.
	 * 
	 * @return the string */
	public String registerOperationHandler() {
		try {
			setViewOperationType(ViewOperationsType.REGISTER.getCode());

			security = new Security();
			intPaymentScheduleReqHi = new IntPaymentScheduleReqHi();
			amoPaymentScheduleReqHi = new AmoPaymentScheduleReqHi();
			lstDtbProIntCouponReqHis = new ArrayList<ProgramIntCouponReqHi>();
			lstDtbProAmoCouponReqHis = new ArrayList<ProgramAmoCouponReqHi>();
			issuanceTOmgmt = new IssuanceTO();

			// clean security helper
			cleanSecurityHelper();
			paymentScheduleTypeSelected = null;
			loadGeneralsCombos();
			setIndexTab(0);
			
			return "paymentScheduleMgmtRule";
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}

	/** Clean payment schedule mod search handler.
	 * 
	 * @param event the event */
	public void cleanPaymentScheduleModSearchHandler(ActionEvent event) {
		try {
			paymentScheduleDataModel = null;
			paymentScheduleTO = new PaymentScheduleTO();
			paymentScheduleResultTO = new PaymentScheduleResultTO();
			cleanIssuerHelper();
			cleanSecurityHelper();
			cleanIssuanceHelper();
			JSFUtilities.resetComponent("frmPaymentScheSearch");

			paymentScheduleTO.setBeginDate(new Date());
			paymentScheduleTO.setEndDate(new Date());
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Clean issuer helper. */
	public void cleanIssuerHelper() {
		issuerHelperSearch = new Issuer();
	}

	/** Clean security helper.
	 * 
	 * @throws Exception the exception */
	public void cleanSecurityHelper() throws Exception {
		securityHelperMgmtOut = new Security();
//		securitiesHelperBean.clean();
	}

	/** Clean issuance helper.
	 * 
	 * @throws Exception the exception */
	public void cleanIssuanceHelper() throws Exception {
		// issuanceHelperBean.clear();

	}

	/** Clean payment schedule mod handler. */
	public void cleanPaymentScheduleModHandler() {
		try {
			paymentScheduleDataModel = new GenericDataModel<PaymentScheduleResultTO>();
			paymentScheduleTO = new PaymentScheduleTO();
			JSFUtilities.resetComponent("frmPaymentScheSearch");
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Register operation restructuring handler.
	 * 
	 * @return the string */
	public String registerOperationRestructuringHandler() {
		try {
			registerOperationHandler();
			return "paymentScheduleRestMgmttRule";
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}

	/** Detail operation handler.
	 * 
	 * @param event the event */
	public void detailOperationHandler(ActionEvent event) {
		try {
			setViewOperationType(ViewOperationsType.DETAIL.getCode());

			if (event != null) {
				paymentScheduleResultTO = (PaymentScheduleResultTO) event.getComponent().getAttributes().get("payScheSelected");
			}
			SecurityTO securityTO = new SecurityTO();
			securityTO.setIdSecurityCodePk(paymentScheduleResultTO.getIdSecurityCodePk());
			security = securityServiceFacade.findSecurityServiceFacade(securityTO);
			billParameterTableById(security.getSecurityType());
			billParameterTableById(security.getSecurityClass(), true);

			if (security.getSecurityInvestor() == null) {
				security.setSecurityInvestor(new SecurityInvestor());
			}

			intPaymentScheduleReqHi = new IntPaymentScheduleReqHi();
			amoPaymentScheduleReqHi = new AmoPaymentScheduleReqHi();
			lstDtbProIntCouponReqHis = new ArrayList<ProgramIntCouponReqHi>();
			lstDtbProAmoCouponReqHis = new ArrayList<ProgramAmoCouponReqHi>();
			issuanceTOmgmt = new IssuanceTO();

			loadGeneralsCombos();
			loadDtbNegotiationModality(security.getInstrumentType());
			loadCboEconomicActivity(security.getIssuer().getEconomicSector());
			loadCboSecurityTypeByInstrumentType(InstrumentType.FIXED_INCOME.getCode());

			List<SecurityForeignDepository> lstAssignedInternationalDep = securityServiceFacade.findSecurityForeignDepositoriesServiceFacade(security.getIdIsinCode(),
					SecurityForeignDepositoryStateType.REGISTERED.getCode());
			security.setSecurityForeignDepositories(lstAssignedInternationalDep);
			selectCheckboxIntDepository();

			List<SecurityNegotiationMechanism> lstAssignedSecNegotiationMechanism = securityServiceFacade.findSecurityNegotiationMechanismsServiceFacade(
					security.getIdSecurityCodePk(), BooleanType.YES.getCode());
			security.setSecurityNegotiationMechanisms(lstAssignedSecNegotiationMechanism);

			PaymentScheduleTO paymentScheduleToFilter = null;
			if (paymentScheduleTO.getPaymentScheduleType().equals(INTEREST_PAYMENT_SCHEDULE_TYPE)) {
				setPaymentScheduleTypeSelected(Integer.valueOf(1));
				viewOperationIntPaySheModification = true;
				viewOperationAmoPaySheModification = false;
				paymentScheduleToFilter = new PaymentScheduleTO();
				paymentScheduleToFilter.setIdPaymentScheduleReqHiPk(paymentScheduleResultTO.getIdPaymentScheduleReqHi());

				intPaymentScheduleReqHi = paymentCronogramServiceFacade.finIntPaymentScheduleReqHisServiceFacade(paymentScheduleToFilter);
				lstDtbProIntCouponReqHis = intPaymentScheduleReqHi.getProgramIntCouponReqHis();
				List<ProgramInterestCoupon> lstProgramInterestCoupon = securityServiceFacade.findProgramInterestCouponsServiceFacade(security
						.getInterestPaymentSchedule().getIdIntPaymentSchedulePk());
				intPaymentScheduleReqHi.setAllCouponsPendingPayment(Boolean.TRUE);
				for (ProgramIntCouponReqHi programInt : intPaymentScheduleReqHi.getProgramIntCouponReqHis()) {
					if (programInt.isPaidState()) {
						intPaymentScheduleReqHi.setAllCouponsPendingPayment(Boolean.FALSE);
						break;
					}
				}
				
				// issue 1232: ahora siempre estara en modo: solo lectura
				for(ProgramIntCouponReqHi proInt:lstDtbProIntCouponReqHis){
					proInt.setSelected(false);
					proInt.setDisabled(true);
				}
				
				security.getInterestPaymentSchedule().setProgramInterestCoupons(lstProgramInterestCoupon);
				security.setAmortizationPaymentSchedule(new AmortizationPaymentSchedule());
			} else if (paymentScheduleTO.getPaymentScheduleType().equals(AMORTIZATION_PAYMENT_SCHEDULE_TYPE)) {
				setPaymentScheduleTypeSelected(Integer.valueOf(2));
				viewOperationIntPaySheModification = false;
				viewOperationAmoPaySheModification = true;
				paymentScheduleToFilter = new PaymentScheduleTO();
				paymentScheduleToFilter.setIdPaymentScheduleReqHiPk(paymentScheduleResultTO.getIdPaymentScheduleReqHi());

				amoPaymentScheduleReqHi = paymentCronogramServiceFacade.finAmoPaymentScheduleReqHisServiceFacade(paymentScheduleToFilter);
				lstDtbProAmoCouponReqHis = amoPaymentScheduleReqHi.getProgramAmoCouponReqHis();
				
				// issue 1232: ahora siempre estara en modo: solo lectura
				for(ProgramAmoCouponReqHi proAmo:lstDtbProAmoCouponReqHis){
					proAmo.setSelected(false);
					proAmo.setDisabled(true);
				}

				List<ProgramAmortizationCoupon> lstProgramAmortizationCoupon = securityServiceFacade.findProgramAmortizationCouponsServiceFacade(security
						.getAmortizationPaymentSchedule().getIdAmoPaymentSchedulePk());
				security.getAmortizationPaymentSchedule().setProgramAmortizationCoupons(lstProgramAmortizationCoupon);
				security.setInterestPaymentSchedule(new InterestPaymentSchedule());
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Approve operation handler.
	 * 
	 * @return the string */
	public String approveOperationHandler() {
		try {
			if (paymentScheduleResultTO == null) {
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.ERROR_RECORD_REQUIRED, null);
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationSec').show()");
				return null;
			} else if (!paymentScheduleResultTO.getRequestState().equals(RequestStateType.REGISTERED.getCode())) {
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.PAYMENT_CRON_ERROR_APPROVE_STATE_NO_PENDING, null);
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationSec').show()");
				return null;
			}
			detailOperationHandler(null);
			setViewOperationType(ViewOperationsType.APPROVE.getCode());
			paymentScheduleTypeSelected = paymentScheduleTO.getPaymentScheduleType();

			if (validateRequestTransaction()) {
				JSFUtilities.showSimpleValidationDialog();
				security = new Security();
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
				return "";
			}

			return "paymentScheduleMgmtRule";
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}

	/** Approve operation restructuring handler.
	 * 
	 * @return the string */
	public String approveOperationRestructuringHandler() {
		try {
			if (paymentScheduleResultTO == null) {
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.ERROR_RECORD_REQUIRED, null);
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationSec').show()");
				return null;
			} else if (!paymentScheduleResultTO.getRequestState().equals(RequestStateType.REGISTERED.getCode())) {
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.PAYMENT_CRON_ERROR_APPROVE_STATE_NO_PENDING, null);
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationSec').show()");
				return null;
			}
			detailOperationHandler(null);
			setViewOperationType(ViewOperationsType.APPROVE.getCode());
			return "paymentScheduleRestMgmttRule";
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}

	/** Annulate operation handler.
	 * 
	 * @return the string */
	public String annulateOperationHandler() {
		try {
			if (paymentScheduleResultTO == null) {
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.ERROR_RECORD_REQUIRED, null);
				JSFUtilities.executeJavascriptFunction("cnfwMsgCustomValidationSec.show()");
				return null;
			} else if (!paymentScheduleResultTO.getRequestState().equals(RequestStateType.REGISTERED.getCode())) {
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.PAYMENT_CRON_ERROR_ANNUL_STATE_NO_PENDING, null);
				JSFUtilities.executeJavascriptFunction("cnfwMsgCustomValidationSec.show()");
				return null;
			}

			detailOperationHandler(null);
			setViewOperationType(ViewOperationsType.ANULATE.getCode());
			paymentScheduleTypeSelected = paymentScheduleTO.getPaymentScheduleType();

			return "paymentScheduleMgmtRule";
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}

	/** Annulate operation restructuring handler.
	 * 
	 * @return the string */
	public String annulateOperationRestructuringHandler() {
		try {
			if (paymentScheduleResultTO == null) {
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.ERROR_RECORD_REQUIRED, null);
				JSFUtilities.executeJavascriptFunction("cnfwMsgCustomValidationSec.show()");
				return null;
			} else if (!paymentScheduleResultTO.getRequestState().equals(RequestStateType.REGISTERED.getCode())) {
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.PAYMENT_CRON_ERROR_ANNUL_STATE_NO_PENDING, null);
				JSFUtilities.executeJavascriptFunction("cnfwMsgCustomValidationSec.show()");
				return null;
			}

			detailOperationHandler(null);
			setViewOperationType(ViewOperationsType.ANULATE.getCode());
			return "paymentScheduleRestMgmttRule";
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}

	/** Confirm operation handler.
	 * 
	 * @return the string */
	public String confirmOperationHandler() {
		log.info("Entrando al metodo de Confirmacion de la Solicitud de cuponera");
		try {
			if (paymentScheduleResultTO == null) {
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.ERROR_RECORD_REQUIRED, null);
				JSFUtilities.showSimpleValidationDialog();
				return null;
			} else {
				// si su estado es diferente de REGISTRADO mostrar mensaje de error
				if (!paymentScheduleResultTO.getRequestState().equals(REQUEST_REGISTERED)) {
					String cuerpoMensaje = PropertiesUtilities.getMessage("msg.confirm.only.request.registered");
					JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
					mostrarMensaje("Error", cuerpoMensaje);
					JSFUtilities.showSimpleValidationDialog();

					// showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null,
					// PropertiesConstants.ERROR_RECORD_REQUIRED, null);
					// JSFUtilities.showSimpleValidationDialog();
					return null;
				}
			}
			detailOperationHandler(null);
			setViewOperationType(ViewOperationsType.CONFIRM.getCode());
			paymentScheduleTypeSelected = paymentScheduleTO.getPaymentScheduleType();

			if (validateRequestTransaction()) {
				JSFUtilities.showSimpleValidationDialog();
				security = new Security();
				setViewOperationType(ViewOperationsType.CONSULT.getCode());
				return "";
			}

			return "paymentScheduleMgmtRule";
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}

	/** Confirm restructuring operation handler.
	 * 
	 * @return the string */
	public String confirmRestructuringOperationHandler() {
		try {
			if (paymentScheduleResultTO == null) {
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.ERROR_RECORD_REQUIRED, null);
				JSFUtilities.executeJavascriptFunction("cnfwMsgCustomValidationSec.show()");
				return null;
			}
			detailOperationHandler(null);
			setViewOperationType(ViewOperationsType.CONFIRM.getCode());
			return "paymentScheduleRestMgmttRule";
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}

	/** Reject operation handler.
	 * 
	 * @return the string */
	public String rejectOperationHandler() {
		try {
			if (paymentScheduleResultTO == null) {
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.ERROR_RECORD_REQUIRED, null);
				JSFUtilities.executeJavascriptFunction("cnfwMsgCustomValidationSec.show()");
				return null;
			} else {
				// si su estado es diferente de REGISTRADO mostrar mensaje de error
				if (!paymentScheduleResultTO.getRequestState().equals(REQUEST_REGISTERED)) {
					String cuerpoMensaje = PropertiesUtilities.getMessage("msg.reject.only.request.registered");
					JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
					mostrarMensaje("Error", cuerpoMensaje);
					JSFUtilities.showSimpleValidationDialog();

					// showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null,
					// PropertiesConstants.ERROR_RECORD_REQUIRED, null);
					// JSFUtilities.showSimpleValidationDialog();
					return null;
				}
			}
			detailOperationHandler(null);
			setViewOperationType(ViewOperationsType.REJECT.getCode());
			paymentScheduleTypeSelected = paymentScheduleTO.getPaymentScheduleType();

			return "paymentScheduleMgmtRule";
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}

	/** Reject restructuring operation handler.
	 * 
	 * @return the string */
	public String rejectRestructuringOperationHandler() {
		try {
			if (paymentScheduleResultTO == null) {
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.ERROR_RECORD_REQUIRED, null);
				JSFUtilities.executeJavascriptFunction("cnfwMsgCustomValidationSec.show()");
				return null;
			}
			detailOperationHandler(null);
			setViewOperationType(ViewOperationsType.REJECT.getCode());
			return "paymentScheduleRestMgmttRule";
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}

	/** Search payment sche modification handler. */
	public void searchPaymentScheModificationHandler() {
		try {
			paymentScheduleResultTO = new PaymentScheduleResultTO();
			paymentScheduleTO.setRegistryType(PaymentScheduleReqRegisterType.REQUEST_MODIFICATION.getCode());

			searchPaymentSchedule();
			loadUserValidation();
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Change rdo filter. */
	public void changeRdoFilter() {
		try {
			issuerHelperSearch = new Issuer();
			this.getPaymentScheduleTO().changeRdoFilterPaymentScheduleMgmt();
			JSFUtilities.resetComponent(PaymentScheduleType.TXT_SEARCH_REQUEST_NUMBER.getValue());
			JSFUtilities.resetComponent(PaymentScheduleType.TXT_SEARCH_ISIN_CODE.getValue());
			JSFUtilities.resetComponent(PaymentScheduleType.TXT_SEARCH_CBO_SECURITY_CLASS.getValue());

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Search payment sche restructuring handler. */
	public void searchPaymentScheRestructuringHandler() {
		try {
			paymentScheduleResultTO = new PaymentScheduleResultTO();
			paymentScheduleTO.setRegistryType(PaymentScheduleReqRegisterType.REQUEST_RESTRUCTURING.getCode());

			searchPaymentSchedule();

			loadUserValidation();
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Load user validation. */
	private void loadUserValidation() {
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnConfirmView(Boolean.TRUE);
		privilegeComponent.setBtnRejectView(Boolean.TRUE);
		this.userPrivilege.setPrivilegeComponent(privilegeComponent);
	}

	/** Search payment schedule.
	 * 
	 * @throws ServiceException the service exception */
	public void searchPaymentSchedule() throws ServiceException {
		List<PaymentScheduleResultTO> lstPaymentScheduleResult = null;
		if (paymentScheduleTO.getPaymentScheduleType() != null) {
			if (paymentScheduleTO.getPaymentScheduleType().equals(INTEREST_PAYMENT_SCHEDULE_TYPE)) {
				lstPaymentScheduleResult = paymentCronogramServiceFacade.finIntPaymentScheduleReqHisLiteServiceFacade(paymentScheduleTO);
				paymentScheduleDataModel = new GenericDataModel<PaymentScheduleResultTO>(lstPaymentScheduleResult);
			} else if (paymentScheduleTO.getPaymentScheduleType().equals(AMORTIZATION_PAYMENT_SCHEDULE_TYPE)) {
				lstPaymentScheduleResult = paymentCronogramServiceFacade.finAmoPaymentScheduleReqHisLiteServiceFacade(paymentScheduleTO);
				paymentScheduleDataModel = new GenericDataModel<PaymentScheduleResultTO>(lstPaymentScheduleResult);
			}
		}
	}

	/** Search security helper handler. */
	public void searchSecurityHelperHandler() {

		// validando que se haya ingresado el valor antes de realizar busqueda
		if (!Validations.validateIsNotNullAndNotEmpty(securityHelperMgmtOut.getIdSecurityCodePk())) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesUtilities.getMessage("program.security.required"));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}

		try {
			JSFUtilities.resetComponent("frmSecurityMgmt");

			SecurityTO securityTO = new SecurityTO();
			securityTO.setIdSecurityCodePk(securityHelperMgmtOut.getIdSecurityCodePk());
			Object[] argObj = new Object[3];
			security = securityServiceFacade.findSecurityServiceFacade(securityTO);

			if (security == null) {

				JSFUtilities.showMessageOnDialog(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES, PropertiesConstants.LBL_HEADER_ALERT_ERROR, null,
						GeneralConstants.PROPERTY_FILE_MESSAGES, PropertiesConstants.SECURITY_ERROR_CODE_NOT_FOUND, null);
				JSFUtilities.showSimpleValidationDialog();

				security = new Security();
				securityHelperMgmtOut.setIdSecurityCodePk(null);
				JSFUtilities.resetComponent("frmSecurityMgmt");
			} else {

				if (security.getCurrency().equals(CurrencyType.UFV.getCode()) || security.getCurrency().equals(CurrencyType.DMV.getCode())) {
					this.setExchangeRate(true);
				} else {
					this.setExchangeRate(false);
				}

				argObj[0] = security.getIdSecurityCodePk();
				
				// issue 1232: validando, si el valor esta en estado bloqueado no permitira realizar la modificacion
				if(security.getStateSecurity().equals(SecurityStateType.BLOCKED.getCode())){
					JSFUtilities.showMessageOnDialog(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES, PropertiesConstants.LBL_HEADER_ALERT_ERROR, null,
							GeneralConstants.PROPERTY_FILE_MESSAGES, PropertiesConstants.PAYMENT_CRONOGRAM_ERROR_STATE_SECURITY_INVALID, null);
					JSFUtilities.showSimpleValidationDialog();

					security = new Security();
					cleanSecurityHelper();
					paymentScheduleTypeSelected = null;
					return;
				}
				
				if(Boolean.TRUE.equals(paymentCronogramServiceFacade.verifyDefinitiveStateCorporativeOperation(security))) {
					JSFUtilities.showMessageOnDialog("Mensaje de alerta", "No se puede realizar la modificacion del cronograma debido a que "
							+ "existe un corporativo en estado definitivo ");
					JSFUtilities.showSimpleValidationDialog();

					security = new Security();
					cleanSecurityHelper();
					paymentScheduleTypeSelected = null;
					return;
				}
				
				if (security.isVariableInstrumentType()) {

					JSFUtilities.showMessageOnDialog(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES, PropertiesConstants.LBL_HEADER_ALERT_ERROR, null,
							GeneralConstants.PROPERTY_FILE_MESSAGES, PropertiesConstants.PAYMENT_CRON_ERROR_VARIABLE_INSTRUMENT, null);
					JSFUtilities.showSimpleValidationDialog();

					security = new Security();
					cleanSecurityHelper();
					paymentScheduleTypeSelected = null;
				} else if (IssuerStateType.BLOCKED.getCode().equals(security.getIssuer().getStateIssuer())) {
					JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
					JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PAYMENT_CRON_ERROR_ISSUER_BLOKED, null);
					JSFUtilities.showSimpleValidationDialog();
					security = new Security();
					cleanSecurityHelper();
					paymentScheduleTypeSelected = null;
				} else if (security.isFixedInstrumentType() && security.isPhysicalIssuanceForm()) {

					if (isPaymentScheduleModificationPage()) {
						JSFUtilities.showMessageOnDialog(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES, PropertiesConstants.LBL_HEADER_ALERT_ERROR, null,
								GeneralConstants.PROPERTY_FILE_MESSAGES, PropertiesConstants.PAYMENT_CRON_ERROR_MOD_NO_DESMATERIALIZE, null);
						JSFUtilities.showSimpleValidationDialog();
					} else if (isPaymentScheduleRestructurationPage()) {
						JSFUtilities.showMessageOnDialog(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES, PropertiesConstants.LBL_HEADER_ALERT_ERROR, null,
								GeneralConstants.PROPERTY_FILE_MESSAGES, PropertiesConstants.PAYMENT_CRON_ERROR_RES_NO_DESMATERIALIZE, null);
					}
					JSFUtilities.showSimpleValidationDialog();
					security = new Security();
					cleanSecurityHelper();
					paymentScheduleTypeSelected = null;
				} else if (!SecurityStateType.REGISTERED.getCode().equals(security.getStateSecurity())) {

					if (isPaymentScheduleModificationPage()) {
						JSFUtilities.showMessageOnDialog(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES, PropertiesConstants.LBL_HEADER_ALERT_ERROR, null,
								GeneralConstants.PROPERTY_FILE_MESSAGES, PropertiesConstants.PAYMENT_CRON_ERROR_MOD_SECURITY_STATE_NO_REGISTERED, null);
					} else if (isPaymentScheduleRestructurationPage()) {
						JSFUtilities.showMessageOnDialog(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES, PropertiesConstants.LBL_HEADER_ALERT_ERROR, null,
								GeneralConstants.PROPERTY_FILE_MESSAGES, PropertiesConstants.PAYMENT_CRON_ERROR_RES_SECURITY_STATE_NO_REGISTERED, null);
					}
					JSFUtilities.showSimpleValidationDialog();
					security = new Security();
					cleanSecurityHelper();
					paymentScheduleTypeSelected = null;
				} else {
					try {
						if (security.isHasSplitSecurities()) {

							JSFUtilities.showMessageOnDialog(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES, PropertiesConstants.LBL_HEADER_ALERT_ERROR, null,
									GeneralConstants.PROPERTY_FILE_MESSAGES, PropertiesConstants.PAYMENT_CRON_ERROR_HAS_SPLIT_SECURITIES, null);
							JSFUtilities.showSimpleValidationDialog();

							security = new Security();
							cleanSecurityHelper();
							paymentScheduleTypeSelected = null;
							return;
						}
						if (security.isCoupon()) {

							JSFUtilities.showMessageOnDialog(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES, PropertiesConstants.LBL_HEADER_ALERT_ERROR, null,
									GeneralConstants.PROPERTY_FILE_MESSAGES, PropertiesConstants.PAYMENT_CRON_IS_COUPON, null);
							JSFUtilities.showSimpleValidationDialog();

							security = new Security();
							cleanSecurityHelper();
							paymentScheduleTypeSelected = null;
							return;
						}
						if (security.isDetached()) {
							JSFUtilities.showMessageOnDialog(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES, PropertiesConstants.LBL_HEADER_ALERT_ERROR, null,
									GeneralConstants.PROPERTY_FILE_MESSAGES, PropertiesConstants.PAYMENT_CRON_IS_DETACHED, null);
							JSFUtilities.showSimpleValidationDialog();

							security = new Security();
							cleanSecurityHelper();
							paymentScheduleTypeSelected = null;
							return;
						}
						billParameterTableById(security.getSecurityType());
						billParameterTableById(security.getSecurityClass(), true);
						if (isInterestPaymentScheduleSelected()) {
							paymentCronogramServiceFacade.validateIntPaymentScheRequestAlreadyRegisteredServiceFacade(intPaymentScheduleReqHi, security);
						} else if (isAmortizationPaymentScheduleSelected()) {
							paymentCronogramServiceFacade.validateAmoPaymentScheRequestAlreadyRegisteredServiceFacade(amoPaymentScheduleReqHi, security);
						}
					} catch (ServiceException se) {
						security = new Security();
						cleanSecurityHelper();
						paymentScheduleTypeSelected = null;
						if (se.getMessage() != null) {
							JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
							showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, null, se.getErrorService().getMessage(), se.getParams());
							JSFUtilities.showSimpleValidationDialog();
						}
						return;
					}

					loadDtbNegotiationModality(security.getInstrumentType());
					loadCboEconomicActivity(security.getIssuer().getEconomicSector());

					List<SecurityForeignDepository> lstAssignedInternationalDep = securityServiceFacade.findSecurityForeignDepositoriesServiceFacade(
							security.getIdIsinCode(), SecurityForeignDepositoryStateType.REGISTERED.getCode());
					security.setSecurityForeignDepositories(lstAssignedInternationalDep);
					selectCheckboxIntDepository();

					List<SecurityNegotiationMechanism> lstAssignedSecNegotiationMechanism = securityServiceFacade.findSecurityNegotiationMechanismsServiceFacade(
							security.getIdSecurityCodePk(), BooleanType.YES.getCode());
					security.setSecurityNegotiationMechanisms(lstAssignedSecNegotiationMechanism);

					blLongTerm = false;
					blShortTerm = false;
					programAmoCouponReqHi = new ProgramAmoCouponReqHi();
					amortizationPaymentEntered = false;
					/** SI ES LARGO PLAZO */
					if (security.isLongSecurityTerm() && IssuanceType.DEMATERIALIZED.getCode().equals(security.getIssuance().getIssuanceType())) {
						blLongTerm = true;
					}

					/** SI ES CORTO PLAZO */
					if (security.isShortSecurityTerm() && IssuanceType.DEMATERIALIZED.getCode().equals(security.getIssuance().getIssuanceType())) {
						blShortTerm = true;
					}
					if (isFixedInstrumentType()) {
						loadCboSecurityTypeByInstrumentType(InstrumentType.FIXED_INCOME.getCode());

						if (isInterestPaymentScheduleSelected()) {
							List<ProgramInterestCoupon> lstProgramInterestCoupon = securityServiceFacade.findProgramInterestCouponsServiceFacade(security
									.getInterestPaymentSchedule().getIdIntPaymentSchedulePk());
							security.getInterestPaymentSchedule().setProgramInterestCoupons(lstProgramInterestCoupon);
							security.getInterestPaymentSchedule().setAllCouponsPendingPayment(Boolean.TRUE);

							List<ProgramAmortizationCoupon> lstProgramAmortizationCoupon = securityServiceFacade.findProgramAmortizationCouponsServiceFacade(security
									.getAmortizationPaymentSchedule().getIdAmoPaymentSchedulePk());
							security.getAmortizationPaymentSchedule().setProgramAmortizationCoupons(lstProgramAmortizationCoupon);

							for (ProgramInterestCoupon proInt : security.getInterestPaymentSchedule().getProgramInterestCoupons()) {
								if (proInt.isPaidState()) {
									security.getInterestPaymentSchedule().setAllCouponsPendingPayment(Boolean.TRUE);
									break;
								}
							}

							billIntPaymentScheduleReqHi();
						} else if (isAmortizationPaymentScheduleSelected()) {
							List<ProgramAmortizationCoupon> lstProgramAmortizationCoupon = securityServiceFacade.findProgramAmortizationCouponsServiceFacade(security
									.getAmortizationPaymentSchedule().getIdAmoPaymentSchedulePk());
							security.getAmortizationPaymentSchedule().setProgramAmortizationCoupons(lstProgramAmortizationCoupon);
							security.getAmortizationPaymentSchedule().setAllCouponsPendingPayment(Boolean.TRUE);

							for (ProgramAmortizationCoupon proAmo : lstProgramAmortizationCoupon) {
								if (proAmo.isPaidState()) {
									security.getAmortizationPaymentSchedule().setAllCouponsPendingPayment(Boolean.FALSE);
									break;
								}
							}

							billAmoPaymentScheduleReqHi();
							amortizationTotalFactorCalculate();
						}
					}

				}

			}

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** After security helper handler. */
	public void afterSecurityHelperHandler() {
		if (StringUtils.isBlank(securityHelperMgmtOut.getIdSecurityCodePk())) {
			security = new Security();
			JSFUtilities.showSimpleValidationDialog();
			JSFUtilities.resetComponent("frmSecurityMgmt");
		}
	}

	/** Change cbo payment schedule type. */
	public void changeCboPaymentScheduleType() {
		try {
			security = new Security();
			cleanSecurityHelper();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Search payment schedule handler. */
	public void searchPaymentScheduleHandler() {
		try {
			SecurityTO securityTO = new SecurityTO();
			securityTO.setIdIsinCodePk(securityHelperMgmtOut.getIdIsinCode());

			if (StringUtils.isBlank(securityHelperMgmtOut.getIdIsinCode())) {
				security = new Security();

				JSFUtilities.showSimpleValidationDialog();
				JSFUtilities.resetComponent("frmSecurityMgmt");
				return;
			}
			security = securityServiceFacade.findSecurityServiceFacade(securityTO);
			loadDtbNegotiationModality(security.getInstrumentType());
			loadCboEconomicActivity(security.getIssuer().getEconomicSector());

			if (security == null) {
				security = new Security();
				JSFUtilities.resetComponent("frmSecurityMgmt");
			} else if (security.isVariableInstrumentType()) {

				JSFUtilities.showMessageOnDialog(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES, PropertiesConstants.LBL_HEADER_ALERT_ERROR, null,
						GeneralConstants.PROPERTY_FILE_MESSAGES, PropertiesConstants.PAYMENT_CRON_ERROR_VARIABLE_INSTRUMENT, null);
				JSFUtilities.showSimpleValidationDialog();

				security = new Security();
			} else if (security.isFixedInstrumentType() && !security.isDesmaterializedIssuanceForm()) {

				JSFUtilities.showMessageOnDialog(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES, PropertiesConstants.LBL_HEADER_ALERT_ERROR, null,
						GeneralConstants.PROPERTY_FILE_MESSAGES, PropertiesConstants.PAYMENT_CRON_ERROR_NO_INSTRUMENT_DESMATERIALIZE, null);
				JSFUtilities.showSimpleValidationDialog();
				security = new Security();

			} else if (!SecurityStateType.REGISTERED.getCode().equals(security.getStateSecurity())) {

				if (isPaymentScheduleModificationPage()) {
					JSFUtilities.showMessageOnDialog(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES, PropertiesConstants.LBL_HEADER_ALERT_ERROR, null,
							GeneralConstants.PROPERTY_FILE_MESSAGES, PropertiesConstants.PAYMENT_CRON_ERROR_MOD_SECURITY_STATE_NO_REGISTERED, null);
				} else if (isPaymentScheduleRestructurationPage()) {
					JSFUtilities.showMessageOnDialog(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES, PropertiesConstants.LBL_HEADER_ALERT_ERROR, null,
							GeneralConstants.PROPERTY_FILE_MESSAGES, PropertiesConstants.PAYMENT_CRON_ERROR_RES_SECURITY_STATE_NO_REGISTERED, null);
				}
				JSFUtilities.showSimpleValidationDialog();
				security = new Security();
			} else {

				List<SecurityForeignDepository> lstAssignedInternationalDep = securityServiceFacade.findSecurityForeignDepositoriesServiceFacade(
						security.getIdIsinCode(), SecurityForeignDepositoryStateType.REGISTERED.getCode());
				security.setSecurityForeignDepositories(lstAssignedInternationalDep);
				selectCheckboxIntDepository();

				List<SecurityNegotiationMechanism> lstAssignedSecNegotiationMechanism = securityServiceFacade.findSecurityNegotiationMechanismsServiceFacade(
						security.getIdSecurityCodePk(), BooleanType.YES.getCode());
				security.setSecurityNegotiationMechanisms(lstAssignedSecNegotiationMechanism);

				blLongTerm = false;
				blShortTerm = false;
				/** SI ES LARGO PLAZO */
				if (security.isLongSecurityTerm() && IssuanceType.DEMATERIALIZED.getCode().equals(security.getIssuance().getIssuanceType())) {
					blLongTerm = true;
				}

				/** SI ES CORTO PLAZO */
				if (security.isShortSecurityTerm() && IssuanceType.DEMATERIALIZED.getCode().equals(security.getIssuance().getIssuanceType())) {
					blShortTerm = true;
				}
				if (isFixedInstrumentType()) {
					List<ProgramInterestCoupon> lstProgramInterestCoupon = securityServiceFacade.findProgramInterestCouponsServiceFacade(security
							.getInterestPaymentSchedule().getIdIntPaymentSchedulePk());
					List<ProgramAmortizationCoupon> lstProgramAmortizationCoupon = securityServiceFacade.findProgramAmortizationCouponsServiceFacade(security
							.getAmortizationPaymentSchedule().getIdAmoPaymentSchedulePk());

					security.getInterestPaymentSchedule().setProgramInterestCoupons(lstProgramInterestCoupon);
					security.getAmortizationPaymentSchedule().setProgramAmortizationCoupons(lstProgramAmortizationCoupon);

					for (ProgramAmortizationCoupon proAmortCoupons : security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons()) {

						if (security.getAmortizationOn().equals(AmortizationOnType.CAPITAL.getCode()))
							proAmortCoupons.setAmortizationAmount(security.getCurrentNominalValue().multiply(proAmortCoupons.getAmortizationFactor())
									.divide(BigDecimal.valueOf(100.0)));
						else if (security.getAmortizationOn().equals(AmortizationOnType.NOMINAL_VALUE.getCode()))
							proAmortCoupons.setAmortizationAmount(security.getInitialNominalValue().multiply(proAmortCoupons.getAmortizationFactor())
									.divide(BigDecimal.valueOf(100.0)));
					}
					if (isInterestPaymentScheduleSelected()) {
						billIntPaymentScheduleReqHi();
					} else if (isAmortizationPaymentScheduleSelected()) {
						billAmoPaymentScheduleReqHi();
					}

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Bill int payment schedule req hi.
	 * 
	 * @throws Exception the exception */
	public void billIntPaymentScheduleReqHi() throws Exception {
		intPaymentScheduleReqHi = new IntPaymentScheduleReqHi();
		lstDtbProIntCouponReqHis = new ArrayList<ProgramIntCouponReqHi>();

		intPaymentScheduleReqHi.setAllCouponsPendingPayment(Boolean.TRUE);
		intPaymentScheduleReqHi.setInterestPaymentSchedule(security.getInterestPaymentSchedule());
		intPaymentScheduleReqHi.setSecurity(security);
		intPaymentScheduleReqHi.setPeriodicity(security.getInterestPaymentSchedule().getPeriodicity());
		intPaymentScheduleReqHi.setExpirationDate(security.getInterestPaymentSchedule().getExpirationDate());
		intPaymentScheduleReqHi.setCalendarType(security.getInterestPaymentSchedule().getCalendarType());
		intPaymentScheduleReqHi.setInterestPeriodicity(security.getInterestPaymentSchedule().getInterestPeriodicity());
		intPaymentScheduleReqHi.setInterestType(security.getInterestPaymentSchedule().getInterestType());
		intPaymentScheduleReqHi.setInterestFactor(security.getInterestPaymentSchedule().getInterestFactor());
		intPaymentScheduleReqHi.setFinancialIndex(security.getInterestPaymentSchedule().getFinancialIndex());
		intPaymentScheduleReqHi.setTaxFactor(security.getInterestPaymentSchedule().getTaxFactor());
		intPaymentScheduleReqHi.setRegistryDays(security.getInterestPaymentSchedule().getRegistryDays());
		intPaymentScheduleReqHi.setYield(security.getInterestPaymentSchedule().getYield());
		intPaymentScheduleReqHi.setCashNominal(security.getInterestPaymentSchedule().getCashNominal());
		intPaymentScheduleReqHi.setSpread(security.getInterestPaymentSchedule().getSpread());
		intPaymentScheduleReqHi.setScheduleState(security.getInterestPaymentSchedule().getScheduleState());
		intPaymentScheduleReqHi.setCalendarMonth(security.getInterestPaymentSchedule().getCalendarMonth());
		intPaymentScheduleReqHi.setCalendarDays(security.getInterestPaymentSchedule().getCalendarDays());
		intPaymentScheduleReqHi.setRateType(security.getInterestPaymentSchedule().getRateType());
		intPaymentScheduleReqHi.setRateValue(security.getInterestPaymentSchedule().getRateValue());
		intPaymentScheduleReqHi.setOperationType(security.getInterestPaymentSchedule().getOperationType());
		intPaymentScheduleReqHi.setMinimumRate(security.getInterestPaymentSchedule().getMinimumRate());
		intPaymentScheduleReqHi.setMaximumRate(security.getInterestPaymentSchedule().getMaximumRate());

		ProgramIntCouponReqHi programIntCouponReqHi = null;

		boolean firstPending = true;
		int currentIndex = 0;
		for (ProgramInterestCoupon programInterestCoupon : security.getInterestPaymentSchedule().getProgramInterestCoupons()) {
			programIntCouponReqHi = new ProgramIntCouponReqHi();
			programIntCouponReqHi.setDisabled(Boolean.TRUE);
			if (ProgramScheduleStateType.PENDING.getCode().equals(programInterestCoupon.getStateProgramInterest()) && firstPending) {
				// programIntCouponReqHi.setDisabled(Boolean.FALSE);
				// programIntCouponReqHi.setSelected(Boolean.TRUE);
				// firstPending=false;

			} else if (ProgramScheduleStateType.EXPIRED.getCode().equals(programInterestCoupon.getStateProgramInterest())) {
				intPaymentScheduleReqHi.setAllCouponsPendingPayment(Boolean.FALSE);
			}

			programIntCouponReqHi.setIntPaymentScheduleReqHi(intPaymentScheduleReqHi);
			programIntCouponReqHi.setBeginingDate(programInterestCoupon.getBeginingDate());
			programIntCouponReqHi.setExpirationDate(programInterestCoupon.getExperitationDate());
			programIntCouponReqHi.setCouponNumber(programInterestCoupon.getCouponNumber());
			programIntCouponReqHi.setRegistryDate(programInterestCoupon.getRegistryDate());
			programIntCouponReqHi.setCutoffDate(programInterestCoupon.getCutoffDate());
			programIntCouponReqHi.setCorporativeDate(programInterestCoupon.getCorporativeDate());
			programIntCouponReqHi.setPaymentDate(programInterestCoupon.getPaymentDate());
			programIntCouponReqHi.setPaymentDays(programInterestCoupon.getPaymentDays());
			programIntCouponReqHi.setCurrency(programInterestCoupon.getCurrency());
			programIntCouponReqHi.setFinantialIndexRate(programInterestCoupon.getFinantialIndexRate());
			programIntCouponReqHi.setInterestFactor(programInterestCoupon.getInterestFactor());
			programIntCouponReqHi.setInterestRate(programInterestCoupon.getInterestRate());
			programIntCouponReqHi.setIndRounding(programInterestCoupon.getIndRounding());
			programIntCouponReqHi.setStateProgramInterest(programInterestCoupon.getStateProgramInterest());
			programIntCouponReqHi.setPayedAmount(programInterestCoupon.getPayedAmount());
			programIntCouponReqHi.setCouponAmount(programInterestCoupon.getCouponAmount());
			programIntCouponReqHi.setExchangeDateRate(programInterestCoupon.getPaymentDate());
			programIntCouponReqHi.setCurrencyPayment(CurrencyType.PYG.getCode());

			generateMinMaxDateInterestCouponMod(programIntCouponReqHi);
			generateMinMaxDatesInterestCouponMod(programIntCouponReqHi, currentIndex);
			lstDtbProIntCouponReqHis.add(programIntCouponReqHi);
			currentIndex++;
		}
		initialIntPaymentScheduleReqHi = intPaymentScheduleReqHi.toString();
		//lstDtbProIntCouponReqHisCopy = lstDtbProIntCouponReqHis.clone();
		
		for(ProgramIntCouponReqHi i : lstDtbProIntCouponReqHis) {
			lstDtbProIntCouponReqHisCopy.add(i.clone());
		}
	}

	// @LoggerAuditWeb
	public void selectProgramInterestCoupon(ProgramIntCouponReqHi programIntReqHi) {
		if (programIntReqHi.isSelected()) {
			programIntReqHi.setDisabled(Boolean.FALSE);
		} else {
			programIntReqHi.setDisabled(Boolean.TRUE);
		}
	}

	public void selectProgramAmortizationCoupon(ProgramAmoCouponReqHi programAmoReqHi) {
		if (programAmoReqHi.isSelected()) {
			programAmoReqHi.setDisabled(Boolean.FALSE);
		} else {
			programAmoReqHi.setDisabled(Boolean.TRUE);
		}
	}

	// revertido salida a produccion issues: 100,148,143
	// public void selectProgramAmortizationCoupon(ProgramAmoCouponReqHi programAmoReqHi) {
	// if(programAmoReqHi.isSelected()){
	// programAmoReqHi.setDisabled(Boolean.FALSE);
	// }else{
	// programAmoReqHi.setDisabled(Boolean.TRUE);
	// }
	// }

	/** Bill amo payment schedule req hi.
	 * 
	 * @throws Exception the exception */
	public void billAmoPaymentScheduleReqHi() throws Exception {
		amoPaymentScheduleReqHi = new AmoPaymentScheduleReqHi();
		lstDtbProAmoCouponReqHis = new ArrayList<ProgramAmoCouponReqHi>();

		amoPaymentScheduleReqHi.setAmortizationPaymentSchedule(security.getAmortizationPaymentSchedule());
		amoPaymentScheduleReqHi.setSecurity(security);
		amoPaymentScheduleReqHi.setAmortizationType(security.getAmortizationPaymentSchedule().getAmortizationType());
		amoPaymentScheduleReqHi.setAmortizationFactor(security.getAmortizationPaymentSchedule().getAmortizationFactor());
		amoPaymentScheduleReqHi.setTaxFactor(security.getAmortizationPaymentSchedule().getTaxFactor());
		amoPaymentScheduleReqHi.setCalendarDays(security.getAmortizationPaymentSchedule().getCalendarType());
		amoPaymentScheduleReqHi.setPeriodicity(security.getAmortizationPaymentSchedule().getPeriodicity());
		amoPaymentScheduleReqHi.setRegistryDays(security.getAmortizationPaymentSchedule().getRegistryDays());
		amoPaymentScheduleReqHi.setCorporativeDays(security.getAmortizationPaymentSchedule().getCorporativeDays());
		amoPaymentScheduleReqHi.setScheduleState(security.getAmortizationPaymentSchedule().getScheduleState());

		amoPaymentScheduleReqHi.setPeriodicityDays(security.getAmortizationPaymentSchedule().getPeriodicityDays());
		amoPaymentScheduleReqHi.setPaymentFirstExpirationDate(security.getAmortizationPaymentSchedule().getPaymentFirstExpirationDate());
		amoPaymentScheduleReqHi.setPaymentModality(security.getAmortizationPaymentSchedule().getPaymentModality());
		amoPaymentScheduleReqHi.setNumberPayments(security.getAmortizationPaymentSchedule().getNumberPayments());

		ProgramAmoCouponReqHi programAmoCouponReqHi = null;
		boolean firstPending = true;
		int currentIndex = 0;
		for (ProgramAmortizationCoupon programAmortizationCoupon : security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons()) {
			programAmoCouponReqHi = new ProgramAmoCouponReqHi();

			programAmoCouponReqHi.setDisabled(Boolean.TRUE);
			// if(ProgramScheduleStateType.PENDING.getCode().equals( programAmortizationCoupon.getStateProgramAmortization() )
			// && firstPending){
			// programAmoCouponReqHi.setDisabled(Boolean.FALSE);
			// firstPending=false;
			// }
			programAmoCouponReqHi.setAmoPaymentScheduleReqHi(amoPaymentScheduleReqHi);
			programAmoCouponReqHi.setCouponNumber(programAmortizationCoupon.getCouponNumber());
			programAmoCouponReqHi.setCurrency(programAmortizationCoupon.getCurrency());
			programAmoCouponReqHi.setAmortizationFactor(programAmortizationCoupon.getAmortizationFactor());
			programAmoCouponReqHi.setPaymentDate(programAmortizationCoupon.getPaymentDate());
			programAmoCouponReqHi.setRegistryDate(programAmortizationCoupon.getRegistryDate());
			programAmoCouponReqHi.setCutoffDate(programAmortizationCoupon.getCutoffDate());
			programAmoCouponReqHi.setCorporativeDate(programAmortizationCoupon.getCorporativeDate());
			programAmoCouponReqHi.setBeginingDate(programAmortizationCoupon.getBeginingDate());
			programAmoCouponReqHi.setExpirationDate(programAmortizationCoupon.getExpirationDate());
			programAmoCouponReqHi.setIndRounding(programAmortizationCoupon.getIndRounding());
			programAmoCouponReqHi.setStateProgramAmortizaton(programAmortizationCoupon.getStateProgramAmortization());
			programAmoCouponReqHi.setAmortizationAmount(programAmortizationCoupon.getAmortizationAmount());
			programAmoCouponReqHi.setPayedAmount(programAmortizationCoupon.getPayedAmount());
			programAmoCouponReqHi.setCouponAmount(programAmortizationCoupon.getCouponAmount());
			programAmoCouponReqHi.setExchangeDateRate(programAmortizationCoupon.getPaymentDate());
			programAmoCouponReqHi.setCurrencyPayment(CurrencyType.PYG.getCode());
			generateMinMaxPaymentDateAmortizationCouponMod(programAmoCouponReqHi);
			// System.out.println("+++++++"+security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().size()+"-------"+currentIndex+"---------"+currentIndex+1);
			// if(security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().size() != currentIndex + 1)
			generateMinMaxDatesAmortizationCouponMod(programAmoCouponReqHi, currentIndex);

			currentIndex++;

			lstDtbProAmoCouponReqHis.add(programAmoCouponReqHi);
		}
		initialAmoPaymentScheduleReqHi = amoPaymentScheduleReqHi.toString() + "," + lstDtbProAmoCouponReqHis.toString();
	}

	/** Generate min max dates interest coupon mod.
	 * 
	 * @param programIntCouponReqHi the program int coupon req hi
	 * @param currentRowIndex the current row index
	 * @throws ServiceException the service exception */
	public void generateMinMaxDatesInterestCouponMod(ProgramIntCouponReqHi programIntCouponReqHi, Integer currentRowIndex) throws ServiceException {
		ProgramInterestCoupon previousProgram = null;
		ProgramInterestCoupon nextProgram = null;
		if (security.getInterestPaymentSchedule().getProgramInterestCoupons().size() == GeneralConstants.ONE_VALUE_INTEGER) {
			programIntCouponReqHi.setMinBeginDate(security.getIssuanceDate());
			programIntCouponReqHi.setMaxBeginDate(security.getExpirationDate());
			programIntCouponReqHi.setMaxExpirationDate(security.getExpirationDate());
		} else {
			if (programIntCouponReqHi.getCouponNumber().equals(GeneralConstants.ONE_VALUE_INTEGER)) {
				nextProgram = security.getInterestPaymentSchedule().getProgramInterestCoupons().get(currentRowIndex.intValue() + 1);

				programIntCouponReqHi.setMinBeginDate(security.getIssuanceDate());
				programIntCouponReqHi.setMaxBeginDate(nextProgram.getBeginingDate());

				programIntCouponReqHi.setMaxExpirationDate(nextProgram.getBeginingDate());

			} else if (security.getInterestPaymentSchedule().getProgramInterestCoupons().size() == programIntCouponReqHi.getCouponNumber().intValue()) {
				previousProgram = security.getInterestPaymentSchedule().getProgramInterestCoupons().get(currentRowIndex.intValue() - 1);

				programIntCouponReqHi.setMinBeginDate(previousProgram.getExperitationDate());
				programIntCouponReqHi.setMaxBeginDate(security.getExpirationDate());

				programIntCouponReqHi.setMaxExpirationDate(security.getExpirationDate());
			} else {
				previousProgram = security.getInterestPaymentSchedule().getProgramInterestCoupons().get(currentRowIndex.intValue() - 1);
				nextProgram = security.getInterestPaymentSchedule().getProgramInterestCoupons().get(currentRowIndex.intValue() + 1);

				programIntCouponReqHi.setMinBeginDate(previousProgram.getExperitationDate());
				programIntCouponReqHi.setMaxBeginDate(nextProgram.getBeginingDate());

				programIntCouponReqHi.setMaxExpirationDate(nextProgram.getBeginingDate());
			}
		}
	}

	/** Generate min max dates amortization coupon mod.
	 * 
	 * @param programAmoCouponReqHi the program amo coupon req hi
	 * @param currentRowIndex the current row index
	 * @throws ServiceException the service exception */
	public void generateMinMaxDatesAmortizationCouponMod(ProgramAmoCouponReqHi programAmoCouponReqHi, Integer currentRowIndex) throws ServiceException {
		ProgramAmortizationCoupon previousProgram = null;
		ProgramAmortizationCoupon nextProgram = null;
		if (security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().size() == GeneralConstants.ONE_VALUE_INTEGER) {
			programAmoCouponReqHi.setMinBeginDate(security.getIssuanceDate());
			programAmoCouponReqHi.setMaxBeginDate(security.getExpirationDate());
			programAmoCouponReqHi.setMaxExpirationDate(security.getExpirationDate());
		} else {
			if (programAmoCouponReqHi.getCouponNumber().compareTo(GeneralConstants.ONE_VALUE_INTEGER) == 0) {
				nextProgram = security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().get(currentRowIndex.intValue() + 1);

				programAmoCouponReqHi.setMinBeginDate(security.getIssuanceDate());
				programAmoCouponReqHi.setMaxBeginDate(nextProgram.getBeginingDate());

				programAmoCouponReqHi.setMaxExpirationDate(nextProgram.getBeginingDate());

			} else if (security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().size() == programAmoCouponReqHi.getCouponNumber().intValue()) {
				previousProgram = security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().get(currentRowIndex.intValue() - 1);

				programAmoCouponReqHi.setMinBeginDate(previousProgram.getExpirationDate());
				programAmoCouponReqHi.setMaxBeginDate(security.getExpirationDate());

				programAmoCouponReqHi.setMaxExpirationDate(security.getExpirationDate());
			} else {
				previousProgram = security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().get(currentRowIndex.intValue() - 1);

				nextProgram = security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().get(currentRowIndex.intValue() + 1);

				programAmoCouponReqHi.setMinBeginDate(previousProgram.getExpirationDate());
				programAmoCouponReqHi.setMaxBeginDate(nextProgram.getBeginingDate());

				programAmoCouponReqHi.setMaxExpirationDate(nextProgram.getBeginingDate());
			}
		}
	}

	/** Generate min max date interest coupon mod.
	 * 
	 * @param programIntCouponReqHi the program int coupon req hi
	 * @throws ServiceException the service exception */
	public void generateMinMaxDateInterestCouponMod(ProgramIntCouponReqHi programIntCouponReqHi) throws ServiceException {

		if (generalParameterFacade.isUtilDateServiceFacade(programIntCouponReqHi.getExpirationDate())) {
			programIntCouponReqHi
					.setPaymentDateMin(CommonsUtilities.addDaysToDate(programIntCouponReqHi.getPaymentDate(), GeneralConstants.ONE_VALUE_INTEGER.intValue()));
			programIntCouponReqHi.setPaymentDateMin(generalParameterFacade.workingDateCalculateServiceFacade(programIntCouponReqHi.getPaymentDateMin(),
					GeneralConstants.ONE_VALUE_INTEGER));
		} else {
			programIntCouponReqHi
					.setPaymentDateMin(CommonsUtilities.addDaysToDate(programIntCouponReqHi.getPaymentDate(), -GeneralConstants.ONE_VALUE_INTEGER.intValue()));
			programIntCouponReqHi.setPaymentDateMin(generalParameterFacade.workingDateCalculateServiceFacade(programIntCouponReqHi.getPaymentDateMin(),
					-GeneralConstants.ONE_VALUE_INTEGER));
		}

		Integer intNonWorkingDays = generalParameterFacade.getNonWorkingDaysBetweenDatesServiceFacade(programIntCouponReqHi.getPaymentDate(),
				CommonsUtilities.addDaysToDate(programIntCouponReqHi.getPaymentDateMin(), GeneralConstants.NINE_VALUE_INTEGER), countryResidence);

		programIntCouponReqHi.setPaymentDateMax(CommonsUtilities.addDaysToDate(programIntCouponReqHi.getPaymentDateMin(), GeneralConstants.NINE_VALUE_INTEGER.intValue()
				+ intNonWorkingDays.intValue()));
	}

	/** Generate min max payment date amortization coupon mod.
	 * 
	 * @param programAmoCouponReqHi the program amo coupon req hi
	 * @throws ServiceException the service exception */
	public void generateMinMaxPaymentDateAmortizationCouponMod(ProgramAmoCouponReqHi programAmoCouponReqHi) throws ServiceException {

		if (generalParameterFacade.isUtilDateServiceFacade(programAmoCouponReqHi.getExpirationDate())) {
			programAmoCouponReqHi
					.setPaymentDateMin(CommonsUtilities.addDaysToDate(programAmoCouponReqHi.getPaymentDate(), GeneralConstants.ONE_VALUE_INTEGER.intValue()));
			programAmoCouponReqHi.setPaymentDateMin(generalParameterFacade.workingDateCalculateServiceFacade(programAmoCouponReqHi.getPaymentDateMin(),
					GeneralConstants.ONE_VALUE_INTEGER));
		} else {
			programAmoCouponReqHi
					.setPaymentDateMin(CommonsUtilities.addDaysToDate(programAmoCouponReqHi.getPaymentDate(), -GeneralConstants.ONE_VALUE_INTEGER.intValue()));
			programAmoCouponReqHi.setPaymentDateMin(generalParameterFacade.workingDateCalculateServiceFacade(programAmoCouponReqHi.getPaymentDateMin(),
					-GeneralConstants.ONE_VALUE_INTEGER));
		}

		Integer intNonWorkingDays = generalParameterFacade.getNonWorkingDaysBetweenDatesServiceFacade(programAmoCouponReqHi.getPaymentDate(),
				CommonsUtilities.addDaysToDate(programAmoCouponReqHi.getPaymentDateMin(), GeneralConstants.NINE_VALUE_INTEGER), countryResidence);

		programAmoCouponReqHi.setPaymentDateMax(CommonsUtilities.addDaysToDate(programAmoCouponReqHi.getPaymentDateMin(), GeneralConstants.NINE_VALUE_INTEGER.intValue()
				+ intNonWorkingDays.intValue()));
	}

	/** Change program int begin date.
	 * 
	 * @param programIntReqHi the program int req hi */
	public void changeProgramIntBeginDate(ProgramIntCouponReqHi programIntReqHi) {
		try {
			Integer paymentDays = null;
			if (security.isExtendedTerm()) {
				paymentDays = CommonsUtilities.getDaysBetween(programIntReqHi.getBeginingDate(), programIntReqHi.getPaymentDate());
			} else {
				paymentDays = CommonsUtilities.getDaysBetween(programIntReqHi.getBeginingDate(), programIntReqHi.getExpirationDate());
			}
			programIntReqHi.setPaymentDays(paymentDays);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** mwtoso para obtener la cantidad de dias entre una fecha y otra
	 * 
	 * @param beginDate
	 * @param endDate
	 * @return */
	public Integer getPaymentDaysToAmortization(Date beginDate, Date endDate) {
		Integer paymentDays = null;
		paymentDays = CommonsUtilities.getDaysBetween(beginDate, beginDate);
		return paymentDays;
	}

	/** Change program int expiration date.
	 * 
	 * @param programIntReqHi the program int req hi */
	public void changeProgramIntExpirationDate(ProgramIntCouponReqHi programIntReqHi) {
		try {
			// Payment Date
			programIntReqHi.setPaymentDate(generalParameterFacade.workingDateCalculateServiceFacade(programIntReqHi.getExpirationDate(), Integer.valueOf(1)));
			// programIntReqHi.setPaymentDate( CommonsUtilities.workingDateCalculate(programIntReqHi.getExpirationDate(),
			// Integer.valueOf(1)) );
			// Registry Date
			programIntReqHi.setRegistryDate(CommonsUtilities.addDaysToDate(programIntReqHi.getExpirationDate(), security.getStockRegistryDays() * -1));
			programIntReqHi.setRegistryDate(generalParameterFacade.workingDateCalculateServiceFacade(programIntReqHi.getRegistryDate(), -1));
			// Cut Off Date
			programIntReqHi.setCutoffDate(programIntReqHi.getRegistryDate());

			programIntReqHi.setExchangeDateRate(programIntReqHi.getPaymentDate());

			Integer paymentDays = null;
			if (security.isExtendedTerm()) {
				paymentDays = CommonsUtilities.getDaysBetween(programIntReqHi.getBeginingDate(), programIntReqHi.getPaymentDate());
			} else {
				paymentDays = CommonsUtilities.getDaysBetween(programIntReqHi.getBeginingDate(), programIntReqHi.getExpirationDate());
			}
			programIntReqHi.setPaymentDays(paymentDays);

			generateMinMaxDateInterestCouponMod(programIntReqHi);
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Change program int payment date.
	 * 
	 * @param programIntReqHi the program int req hi */
	public void changeProgramIntPaymentDate(ProgramIntCouponReqHi programIntReqHi) {
		try {
			// Registry Date
			if (CommonsUtilities.truncateDateTime(programIntReqHi.getPaymentDate()).equals(CommonsUtilities.truncateDateTime(programIntReqHi.getRegistryDate()))) {
				programIntReqHi.setRegistryDate(CommonsUtilities.addDaysToDate(programIntReqHi.getPaymentDate(), security.getStockRegistryDays() * -1));
				programIntReqHi.setRegistryDate(generalParameterFacade.workingDateCalculateServiceFacade(programIntReqHi.getRegistryDate(), -1));
			} else {
				programIntReqHi.setRegistryDate(CommonsUtilities.addDaysToDate(programIntReqHi.getExpirationDate(), security.getStockRegistryDays() * -1));
				programIntReqHi.setRegistryDate(generalParameterFacade.workingDateCalculateServiceFacade(programIntReqHi.getRegistryDate(), -1));
			}

			// Cut Off Date
			programIntReqHi.setCutoffDate(programIntReqHi.getRegistryDate());

			programIntReqHi.setExchangeDateRate(programIntReqHi.getPaymentDate());

			Integer paymentDays = null;
			if (security.isExtendedTerm()) {
				paymentDays = CommonsUtilities.getDaysBetween(programIntReqHi.getBeginingDate(), programIntReqHi.getPaymentDate());
			} else {
				paymentDays = CommonsUtilities.getDaysBetween(programIntReqHi.getBeginingDate(), programIntReqHi.getExpirationDate());
			}
			programIntReqHi.setPaymentDays(paymentDays);

		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Change program int coupon amount.
	 * 
	 * @param programIntReqHi the program int req hi */
	public void changeProgramIntCouponAmount(ProgramIntCouponReqHi programIntReqHi) {
		Date nextYear = null;
		Date septemberLeap = null;
		programIntReqHi.setInterestRate(securitiesUtils.interestRateCalculate(security, security.getPeriodicity(), programIntReqHi.getInterestFactor(),
				programIntReqHi.getBeginingDate(), programIntReqHi.getPaymentDays(), programIntReqHi.getCouponNumber(), septemberLeap, nextYear));

		BigDecimal newCouponAmount = securitiesUtils.calculateModifyCouponAmountProgramInterest(security, programIntReqHi.getIntPaymentScheduleReqHi()
				.getInterestPaymentSchedule(), programIntReqHi.getInterestRate(), programIntReqHi.getCouponNumber());

		programIntReqHi.setCouponAmount(newCouponAmount);
	}

	// revertido salida a produccion issues: 100,148,143
	// public void changeProgAmoCouponAmount(ProgramAmoCouponReqHi programAmoReqHi) {
	// List<ProgramAmortizationCoupon> listProgramAmortizationCoupon =
	// programAmoReqHi.getAmoPaymentScheduleReqHi().getAmortizationPaymentSchedule().getProgramAmortizationCoupons();
	// List<ProgramAmoCouponReqHi> listProgAmoCouponTmp = this.getLstDtbProAmoCouponReqHis();
	// BigDecimal newAmortizationFactor = securitiesUtils.amortizationFactorCalculate(security,
	// programAmoReqHi.getCouponAmount());
	// programAmoReqHi.setAmortizationFactor( newAmortizationFactor );
	//
	// ProgramAmoCouponReqHi programAmoReqHiAux = new ProgramAmoCouponReqHi();
	// BigDecimal totalAmount = new BigDecimal(BigInteger.ZERO);
	// BigDecimal initNomValue = security.getInitialNominalValue();
	// for(int i=0; i<listProgramAmortizationCoupon.size(); i++) {
	// if(i != listProgramAmortizationCoupon.size()-1) {
	// totalAmount = totalAmount.add( listProgAmoCouponTmp.get(i).getCouponAmount() );
	// }
	// }
	// totalAmount = initNomValue.subtract(totalAmount);
	// for(int i=0; i<listProgramAmortizationCoupon.size(); i++) {
	// if (i == listProgramAmortizationCoupon.size()-1) {
	// programAmoReqHiAux = lstDtbProAmoCouponReqHis.get(i);
	// programAmoReqHiAux.setCouponAmount(totalAmount);
	// newAmortizationFactor = securitiesUtils.amortizationFactorCalculate(security, programAmoReqHiAux.getCouponAmount());
	// programAmoReqHiAux.setAmortizationFactor( newAmortizationFactor );
	// lstDtbProAmoCouponReqHis.set(i, programAmoReqHiAux);
	// }
	// }
	// }

	/** Change program amo coupon amount.
	 * 
	 * @param programAmoReqHi the program amo req hi */
	public void changeProgramAmoCouponAmount(ProgramAmoCouponReqHi programAmoReqHi) {
		List<ProgramAmoCouponReqHi> listProgAmoCouponTmp = null;
		List<ProgramAmortizationCoupon> listProgAmorCoupon = programAmoReqHi.getAmoPaymentScheduleReqHi().getAmortizationPaymentSchedule()
				.getProgramAmortizationCoupons();

		listProgAmoCouponTmp = securitiesUtils.calculateModifyCouponAmountProgramAmo(security, this.getLstDtbProAmoCouponReqHis(), listProgAmorCoupon,
				programAmoReqHi.getAmortizationFactor(), programAmoReqHi.getCouponNumber());

		/** Validations */
		if (securitiesUtils.validateCouponAmortizations(listProgAmoCouponTmp)) {
			lstDtbProAmoCouponReqHis = listProgAmoCouponTmp;
		} else {
			/** Show Message Security out Range */

			for (ProgramAmortizationCoupon programAmoCouponReqHi : listProgAmorCoupon) {
				if (programAmoReqHi.getCouponNumber().equals(programAmoCouponReqHi.getCouponNumber())) {
					programAmoReqHi.setAmortizationAmount(programAmoCouponReqHi.getAmortizationAmount());
					programAmoReqHi.setAmortizationFactor(programAmoCouponReqHi.getAmortizationFactor());
					break;

				}
			}
		}
	}

	/** Change program amo expiration date.
	 * 
	 * @param programAmoReqHi the program amo req hi */
	public void changeProgramAmoExpirationDate(ProgramAmoCouponReqHi programAmoReqHi) {
		try {
			// Payment Date
			programAmoReqHi.setPaymentDate(generalParameterFacade.workingDateCalculateServiceFacade(programAmoReqHi.getExpirationDate(), Integer.valueOf(1)));
			// Registry Date
			programAmoReqHi.setRegistryDate(CommonsUtilities.addDaysToDate(programAmoReqHi.getExpirationDate(), security.getStockRegistryDays() * -1));
			programAmoReqHi.setRegistryDate(generalParameterFacade.workingDateCalculateServiceFacade(programAmoReqHi.getRegistryDate(), -1));
			// CutOffDate
			programAmoReqHi.setCutoffDate(programAmoReqHi.getRegistryDate());

			generateMinMaxPaymentDateAmortizationCouponMod(programAmoReqHi);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Change program amo payment date.
	 * 
	 * @param programAmoReqHi the program amo req hi */
	public void changeProgramAmoPaymentDate(ProgramAmoCouponReqHi programAmoReqHi) {
		try {
			// Registry Date
			if (CommonsUtilities.truncateDateTime(programAmoReqHi.getPaymentDate()).equals(CommonsUtilities.truncateDateTime(programAmoReqHi.getRegistryDate()))) {
				programAmoReqHi.setRegistryDate(CommonsUtilities.addDaysToDate(programAmoReqHi.getPaymentDate(), security.getStockRegistryDays() * -1));
				programAmoReqHi.setRegistryDate(generalParameterFacade.workingDateCalculateServiceFacade(programAmoReqHi.getRegistryDate(), -1));
			} else {
				programAmoReqHi.setRegistryDate(CommonsUtilities.addDaysToDate(programAmoReqHi.getExpirationDate(), security.getStockRegistryDays() * -1));
				programAmoReqHi.setRegistryDate(generalParameterFacade.workingDateCalculateServiceFacade(programAmoReqHi.getRegistryDate(), -1));
			}

			// Cut Off Date
			programAmoReqHi.setCutoffDate(programAmoReqHi.getRegistryDate());
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Insert interest rate. */
	public void insertInterestRate() {
		try {
			intPaymentScheduleReqHi.setMinimumRate(null);
			intPaymentScheduleReqHi.setMaximumRate(null);

			JSFUtilities.resetComponent(PaymentScheduleType.TXT_CRON_MINIUM_RATE.getValue());
			JSFUtilities.resetComponent(PaymentScheduleType.TXT_CRON_MAXIUM_RATE.getValue());
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Calculate interest rate handler.
	 * 
	 * @throws Exception the exception */
	public void calculateInterestRateHandler() throws Exception {
		try {
			if (intPaymentScheduleReqHi.isMixedInterestType()) {
				if (intPaymentScheduleReqHi.getRateType() != null && intPaymentScheduleReqHi.getOperationType() != null && intPaymentScheduleReqHi.getSpread() != null) {
					if (intPaymentScheduleReqHi.getOperationType().equals(OperationType.SUM.getCode())) {
						intPaymentScheduleReqHi.setInterestFactor(intPaymentScheduleReqHi.getRateValue().add(intPaymentScheduleReqHi.getSpread()));
					} else if (intPaymentScheduleReqHi.getOperationType().equals(OperationType.SUBTRACTION.getCode())) {
						intPaymentScheduleReqHi.setInterestFactor(intPaymentScheduleReqHi.getRateValue().subtract(intPaymentScheduleReqHi.getSpread()));
					}
				} else {
					intPaymentScheduleReqHi.setInterestFactor(null);
				}
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Adds the program amortization coupon.
	 * 
	 * @param event the event */
	public void addProgramAmortizationCoupon(ActionEvent event) {
		try {
			if (lstDtbProAmoCouponReqHis == null) {
				lstDtbProAmoCouponReqHis = new ArrayList<ProgramAmoCouponReqHi>();
			}

			programAmoCouponReqHi.setAmoPaymentScheduleReqHi(amoPaymentScheduleReqHi);
			programAmoCouponReqHi.setCouponNumber(Integer.valueOf(lstDtbProAmoCouponReqHis.size() + 1));
			programAmoCouponReqHi.setCurrency(security.getCurrency());

			programAmoCouponReqHi.setIndRounding(BooleanType.NO.getCode());
			programAmoCouponReqHi.setStateProgramAmortizaton(ProgramScheduleStateType.PENDING.getCode());

			// Payment Date
			programAmoCouponReqHi.setPaymentDate(generalParameterFacade.workingDateCalculateServiceFacade(programAmoCouponReqHi.getExpirationDate(), Integer.valueOf(1)));

			// Registry Date
			programAmoCouponReqHi.setRegistryDate(CommonsUtilities.addDaysToDate(programAmoCouponReqHi.getExpirationDate(), security.getStockRegistryDays() * -1));
			programAmoCouponReqHi.setRegistryDate(generalParameterFacade.workingDateCalculateServiceFacade(programAmoCouponReqHi.getRegistryDate(), -1));
			// CutOffDate
			programAmoCouponReqHi.setCutoffDate(programAmoCouponReqHi.getRegistryDate());
			// Corporative Date
			programAmoCouponReqHi.setCorporativeDate(CommonsUtilities.currentDate());

			//
			if (security.getAmortizationOn().equals(AmortizationOnType.CAPITAL.getCode())) {
				programAmoCouponReqHi.setAmortizationAmount(security.getCurrentNominalValue().multiply(programAmoCouponReqHi.getAmortizationFactor())
						.divide(BigDecimal.valueOf(100.0)));

			} else if (security.getAmortizationOn().equals(AmortizationOnType.NOMINAL_VALUE.getCode())) {
				programAmoCouponReqHi.setAmortizationAmount(security.getInitialNominalValue().multiply(programAmoCouponReqHi.getAmortizationFactor())
						.divide(BigDecimal.valueOf(100.0)));
			}

			programAmoCouponReqHi.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			programAmoCouponReqHi.setLastModifyApp(Integer.valueOf(1));
			programAmoCouponReqHi.setLastModifyDate(CommonsUtilities.currentDateTime());
			programAmoCouponReqHi.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
			programAmoCouponReqHi.setLastModifyUser(userInfo.getUserAccountSession().getUserName());

			BigDecimal amortTotalFactorTemp = amortizationTotalFactor.add(programAmoCouponReqHi.getAmortizationFactor());
			if (amortTotalFactorTemp.compareTo(BigDecimal.valueOf(100)) == 1) {
				Object[] parameters = { PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_LBL_AMORTIZATION_FACTOR_TOTAL), "100" };
				String strMsg = PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_VALIDATION_MESSAGES, JSFUtilities.getCurrentLocale(),
						PropertiesConstants.ERROR_LESS_THAN_OR_EQUAL_TO, parameters);
				JSFUtilities.addContextMessage("frmSecurityMgmt:tviewGeneral:dtbAmortizationCoupons", FacesMessage.SEVERITY_ERROR, strMsg, strMsg);
				return;
			}

			lstDtbProAmoCouponReqHis.add(programAmoCouponReqHi);
			programAmoCouponReqHi = new ProgramAmoCouponReqHi();
			amortizationPaymentEntered = true;
			amortizationTotalFactorCalculate();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Amortization total factor calculate. */
	public void amortizationTotalFactorCalculate() {
		amortizationTotalFactor = BigDecimal.ZERO;
		if (lstDtbProAmoCouponReqHis != null && !lstDtbProAmoCouponReqHis.isEmpty()) {
			for (ProgramAmoCouponReqHi proAmoCoupon : lstDtbProAmoCouponReqHis) {
				amortizationTotalFactor = amortizationTotalFactor.add(proAmoCoupon.getAmortizationFactor());
			}
		}
	}

	/** Before save int payment shedule. */
	public void beforeSaveIntPaymentShedule() {
		try {
			viewOperationIntPaySheModification = true;
			viewOperationAmoPaySheModification = false;

			Object[] argObj = new Object[3];
			argObj[0] = security.getIdSecurityCodePk();
			intPaymentScheduleReqHi.setRegistryType(PaymentScheduleReqRegisterType.REQUEST_MODIFICATION.getCode());
			intPaymentScheduleReqHi.setRequestState(RequestStateType.REGISTERED.getCode());
			
			Date previousExpirationDate=null;
			ProgramIntCouponReqHi proInt;

			if (!lDatosExcel.isEmpty()) {
				
				// issue 1232: Nueva implementacion para el PILAT (Aumento o reduccion de cupones)

				
				IntPaymentScheduleReqHi ciHistory=null;
				for (ProgramIntCouponReqHi proIntAux : lstDtbProIntCouponReqHis) {
					System.out.println("::: "+proIntAux.toString());
					if(Validations.validateIsNotNull(proIntAux)
							&& Validations.validateIsNotNull(proIntAux.getIntPaymentScheduleReqHi())){
						ciHistory = proIntAux.getIntPaymentScheduleReqHi();
						break;
					}
				}
				
				if(Validations.validateIsNotNull(ciHistory)){
					int counter = 0;
					for (ExceDataTO dataExcel : lDatosExcel) {
						
						counter++;

						proInt=new ProgramIntCouponReqHi();
						proInt.setIntPaymentScheduleReqHi(ciHistory);
						
						int i = lDatosExcel.indexOf(dataExcel);
						
						//si el cupon esta vencido no toma los datos del excel
						if(lstDtbProIntCouponReqHis.get(i).isPaidState()) {
							dataExcel.setExpirationDate(lstDtbProIntCouponReqHis.get(i).getExpirationDate());
						}
						
						// calculando el nuevo cupon desde el excel
						// este calculo es el mismo que se usa en la generacion de cuponera SecurityMgmtBean -> generatePaymentCronogram()
						proInt.setCouponNumber(dataExcel.getNroCoupon());
						proInt.setCurrency(security.getCurrency());
						if(previousExpirationDate==null){
							proInt.setBeginingDate(security.getIssuanceDate() );	
						}else{
							proInt.setBeginingDate( previousExpirationDate );
						}
						if(counter == lDatosExcel.size()){
							proInt.setExpirationDate(security.getExpirationDate());
						}else{
							proInt.setExpirationDate(CommonsUtilities.truncateDateTime(dataExcel.getExpirationDate()));
						}
						previousExpirationDate=proInt.getExpirationDate();
						
						proInt.setPaymentDate( generalParameterFacade.workingDateCalculateServiceFacade(CommonsUtilities.truncateDateTime(proInt.getExpirationDate()), 
								GeneralConstants.ONE_VALUE_INTEGER) );
						
						Integer paymentDays=null;
						if(security.isExtendedTerm()){
							paymentDays=CommonsUtilities.getDaysBetween(proInt.getBeginingDate(),proInt.getPaymentDate());
						}else{
							paymentDays=CommonsUtilities.getDaysBetween(proInt.getBeginingDate(),proInt.getExpirationDate());
						}
						proInt.setPaymentDays(paymentDays);
						
						if(proInt.getCurrency().equals(CurrencyType.UFV.getCode()) 
								|| proInt.getCurrency().equals(CurrencyType.DMV.getCode())){
							proInt.setExchangeDateRate(proInt.getPaymentDate());
							proInt.setCurrencyPayment(CurrencyType.PYG.getCode());
						}
						proInt.setCorporativeDate(CommonsUtilities.currentDate());
						
						proInt.setRegistryDate(CommonsUtilities.addDaysToDate(proInt.getExpirationDate(), security.getStockRegistryDays()*-1));
						proInt.setRegistryDate(generalParameterFacade.workingDateCalculateServiceFacade(proInt.getRegistryDate(), -1));
						proInt.setCutoffDate( proInt.getRegistryDate() );
						
						proInt.setInterestFactor(lstDtbProIntCouponReqHis.get(i).getInterestFactor());
						proInt.setStateProgramInterest( ProgramScheduleStateType.PENDING.getCode() );
						
						Date securityRegistryDate=isViewOperationRegister() ? CommonsUtilities.currentDate() : security.getRegistryDate();
//						if(security.isHavePaymentScheduleNoDesm()){
							if(CommonsUtilities.isLessOrEqualDate(proInt.getExpirationDate(), securityRegistryDate, true)){
								proInt.setStateProgramInterest( ProgramScheduleStateType.EXPIRED.getCode() );
								proInt.setInterestRate(BigDecimal.ZERO);
							}
//						}
						
						proInt.setInterestRate(securitiesUtils.interestRateCalculate(security, 
								security.getPeriodicity(), 
								security.getInterestRate(), 
								proInt.getBeginingDate(), 
								proInt.getPaymentDays(),
								proInt.getCouponNumber(),
								null,
								null));
						
						proInt.setIndRounding(BooleanType.NO.getCode()); 
						proInt.setPayedAmount(BigDecimal.ZERO);
						
						proInt.setCouponAmount(lstDtbProIntCouponReqHis.get(i).getCouponAmount());
						
						if(security.isIndistinctAmortizationPeriodicity()){
							proInt.setCouponAmount(null);
						}
						proInt.setLastModifyApp( Integer.valueOf(1) );
						proInt.setLastModifyDate( CommonsUtilities.currentDateTime() );
						proInt.setLastModifyIp( userInfo.getUserAccountSession().getIpAddress()  );
						proInt.setLastModifyUser(userInfo.getUserAccountSession().getUserName() );
						
						intPaymentScheduleReqHi.getProgramIntCouponReqHis().add(proInt);
						previousExpirationDate=proInt.getExpirationDate();
					}
				} else {
					previousExpirationDate= new Date();
					proInt = new ProgramIntCouponReqHi();
					
					//IntPaymentScheduleReqHi ciHistory=null;
					
					for (ProgramIntCouponReqHi proIntAux : lstDtbProIntCouponReqHis) {
						System.out.println("::: "+proIntAux.toString());
						if(Validations.validateIsNotNull(proIntAux)
								&& Validations.validateIsNotNull(proIntAux.getIntPaymentScheduleReqHi())){
							ciHistory = proIntAux.getIntPaymentScheduleReqHi();
							break;
						}
					}
					
					
				}
				

			} else {
				
				if(!compareCouponsDates()) {
					JSFUtilities.addContextMessage("frmSecurityMgmt:tviewGeneral:dtbInterestPaymentCron", FacesMessage.SEVERITY_ERROR,
							PropertiesUtilities.getMessage(PropertiesConstants.PAYMENT_CRON_ERROR_INT_PRO_MOD_REQUIRED),
							PropertiesUtilities.getMessage(PropertiesConstants.PAYMENT_CRON_ERROR_INT_PRO_MOD_REQUIRED));
					return;
				}
				
				IntPaymentScheduleReqHi ciHistory=null;
				for (ProgramIntCouponReqHi proIntAux : lstDtbProIntCouponReqHis) {
					System.out.println("::: "+proIntAux.toString());
					/*if(Validations.validateIsNotNull(proIntAux)
							&& Validations.validateIsNotNull(proIntAux.getIntPaymentScheduleReqHi())){
						ciHistory = proIntAux.getIntPaymentScheduleReqHi();
						break;
					}*/
					
					intPaymentScheduleReqHi.getProgramIntCouponReqHis().add(proIntAux);
				}		
				
			}
			
			if (intPaymentScheduleReqHi.getProgramIntCouponReqHis().isEmpty()) {
				JSFUtilities.addContextMessage("frmSecurityMgmt:tviewGeneral:dtbInterestPaymentCron", FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities.getMessage(PropertiesConstants.PAYMENT_CRON_ERROR_INT_PRO_MOD_REQUIRED),
						PropertiesUtilities.getMessage(PropertiesConstants.PAYMENT_CRON_ERROR_INT_PRO_MOD_REQUIRED));
				return;
			}

			paymentCronogramServiceFacade.validateIntPaymentScheRequestAlreadyRegisteredServiceFacade(intPaymentScheduleReqHi, security);

			showMessageOnDialog(PropertiesConstants.LBL_CONFIRM_HEADER_REGISTER, null, PropertiesConstants.PAYMENT_CRON_CONF_REGISTER_MOD_INT, argObj);
			
			JSFUtilities.executeJavascriptFunction("PF('cnfwPaymentShceMgmt').show()");

		} catch (ServiceException se) {
			if (se.getMessage() != null) {
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, null, se.getErrorService().getMessage(), se.getParams());
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}
	
	private Boolean compareCouponsDates() {
		
		Boolean isModificated = false;
		
		for(int i = 0; i < lstDtbProIntCouponReqHis.size(); i++) {
			
			ProgramIntCouponReqHi iteration = lstDtbProIntCouponReqHis.get(i);
			ProgramIntCouponReqHi iterationCopyList = lstDtbProIntCouponReqHisCopy.get(i);
			
			if(!iteration.getBeginingDate().equals(iterationCopyList.getBeginingDate())
					|| !iteration.getExpirationDate().equals(iterationCopyList.getExpirationDate())
					|| !iteration.getPaymentDate().equals(iterationCopyList.getPaymentDate())) {
				isModificated = true;
				break;
			}
			
		}
		
		return isModificated;
		
	}

	/** Before amo payment shedule. */
	public void beforeAmoPaymentShedule() {
		try {
			viewOperationAmoPaySheModification = true;
			viewOperationIntPaySheModification = false;

			Object[] argObj = new Object[3];
			argObj[0] = security.getIdSecurityCodePk();

			amoPaymentScheduleReqHi.setRegistryType(PaymentScheduleReqRegisterType.REQUEST_MODIFICATION.getCode());
			amoPaymentScheduleReqHi.setRequestState(RequestStateType.REGISTERED.getCode());

			if (security.isProportionalAmortizationType()) {
				// validar datos distintos para modificar Amortizaciones
				boolean existeModificacionDatos = true;
				if (!lDatosExcel.isEmpty()) {
					
					// issue 1232: Nueva implementacion para PILAT (Aumento de amortizaciones)
					// capurando cronogramaAmortizacionHistorico
					AmoPaymentScheduleReqHi caHistory=null;
					for (ProgramAmoCouponReqHi proAmo : lstDtbProAmoCouponReqHis) {
						if(Validations.validateIsNotNull(proAmo)
								&& Validations.validateIsNotNull(proAmo.getAmoPaymentScheduleReqHi())){
							caHistory = proAmo.getAmoPaymentScheduleReqHi();
							break;
						}
					}
					
					int counter=0;
					ProgramAmoCouponReqHi proAmo;
					Date previousExpirationDate=null;
					amoPaymentScheduleReqHi.getProgramAmoCouponReqHis().clear();

					if(Validations.validateIsNotNull(caHistory)){
						for (ExceDataTO dataExcel : lDatosExcel) {
							counter++;
							
							Integer i = lDatosExcel.indexOf(dataExcel);
							
							// construyendola amortizacion a partir de los datos del excel
							// issue 1232: este calculo esta basado en el mismo que se usa en la generacion de cuponera SecurityMgmtBean -> generatePaymentCronogram()
							proAmo = new ProgramAmoCouponReqHi();
							proAmo.setAmoPaymentScheduleReqHi(caHistory);
							
							proAmo.setCouponNumber( dataExcel.getNroCoupon() );
							if(Validations.validateIsNullOrEmpty(previousExpirationDate))
								proAmo.setBeginingDate(security.getIssuanceDate());
							else{
								proAmo.setBeginingDate(previousExpirationDate);
							}	
							
							// fecha de expiracion
							if(counter == lDatosExcel.size()){
								proAmo.setExpirationDate(security.getExpirationDate());
							}else{
								proAmo.setExpirationDate(CommonsUtilities.truncateDateTime(dataExcel.getExpirationDate()));
							}
							previousExpirationDate = proAmo.getExpirationDate();
							proAmo.setAmortizationFactor(lstDtbProAmoCouponReqHis.get(i).getAmortizationFactor());
							proAmo.setCouponAmount(lstDtbProAmoCouponReqHis.get(i).getCouponAmount());
							proAmo.setPaymentDate( generalParameterFacade.workingDateCalculateServiceFacade(proAmo.getExpirationDate(),GeneralConstants.ONE_VALUE_INTEGER) );
							proAmo.setCorporativeDate(CommonsUtilities.currentDate());
							proAmo.setCurrency( security.getCurrency() );
							if(proAmo.getCurrency().equals(CurrencyType.UFV.getCode()) 
									|| proAmo.getCurrency().equals(CurrencyType.DMV.getCode())){
								proAmo.setExchangeDateRate(proAmo.getPaymentDate());
								proAmo.setCurrencyPayment(CurrencyType.PYG.getCode());
							}
							proAmo.setRegistryDate( CommonsUtilities.addDaysToDate(proAmo.getExpirationDate(), security.getStockRegistryDays()*-1) );
							proAmo.setRegistryDate(generalParameterFacade.workingDateCalculateServiceFacade(proAmo.getRegistryDate(), -1));
							proAmo.setCutoffDate( proAmo.getRegistryDate() ); 
							proAmo.setIndRounding(BooleanType.NO.getCode());
							
							if(security.getAmortizationOn().equals( AmortizationOnType.CAPITAL.getCode() )){
								proAmo.setAmortizationAmount(security.getCurrentNominalValue().multiply( security.getAmortizationPaymentSchedule().getAmortizationFactor() ).divide(BigDecimal.valueOf(100.0)) );
							}else if(security.getAmortizationOn().equals( AmortizationOnType.NOMINAL_VALUE.getCode() )){
								proAmo.setAmortizationAmount(security.getInitialNominalValue().multiply( security.getAmortizationPaymentSchedule().getAmortizationFactor() ).divide(BigDecimal.valueOf(100.0)) );
							}
							proAmo.setStateProgramAmortizaton(ProgramScheduleStateType.PENDING.getCode());
							
							Date securityRegistryDate=isViewOperationRegister() ? CommonsUtilities.currentDate() : security.getRegistryDate();
//							if(security.isHavePaymentScheduleNoDesm() && security.isPartialAmortPaymentModality() && security.isProportionalAmortizationType()){
								if(CommonsUtilities.isLessOrEqualDate(proAmo.getExpirationDate(), securityRegistryDate, true)){
									proAmo.setStateProgramAmortizaton(ProgramScheduleStateType.EXPIRED.getCode());
								}
//							}
							
							proAmo.setPayedAmount(BigDecimal.ZERO);
							proAmo.setRegistryUser(userInfo.getUserAccountSession().getUserName());
							proAmo.setRegisterDate(CommonsUtilities.currentDateTime());
							proAmo.setIndRounding(Integer.valueOf(2));
							proAmo.setLastModifyApp( Integer.valueOf(1) );
							proAmo.setLastModifyDate( CommonsUtilities.currentDateTime() );
							proAmo.setLastModifyIp( userInfo.getUserAccountSession().getIpAddress()  );
							proAmo.setLastModifyUser(userInfo.getUserAccountSession().getUserName() );
							
							amoPaymentScheduleReqHi.getProgramAmoCouponReqHis().add(proAmo);
						}
					}
				} else {
					existeModificacionDatos = true;
				}

				String cuerpoMensaje = "";
				if (existeModificacionDatos) {
					// validando que los montos seteados desde Excel no afecte en la sumatoria de amortizaciones que tiene que
					// ser igual a valor nominal inicial
					BigDecimal sumaValorNominal = BigDecimal.ZERO;
					BigDecimal sumaFactorAmortizacion = BigDecimal.ZERO;
					log.info("::: Mostrando los cupones extraidos");
					for (ProgramAmoCouponReqHi proAmo : amoPaymentScheduleReqHi.getProgramAmoCouponReqHis()) {

						sumaFactorAmortizacion = sumaFactorAmortizacion.add(proAmo.getAmortizationFactor());
						sumaFactorAmortizacion = sumaFactorAmortizacion.setScale(8, RoundingMode.HALF_UP);

						sumaValorNominal = sumaValorNominal.add(proAmo.getCouponAmount());
						sumaValorNominal = sumaValorNominal.setScale(8, RoundingMode.HALF_UP);
					}
					log.info("Sumatoria total factor amortizaciOn: " + sumaFactorAmortizacion + " %");
					log.info("Sumatoria total amortizaciOn es: " + sumaValorNominal + " (Valor Nominal)");

					// verificando si la sumatoria del factor de amortizacion es igual al 100%
					if (sumaFactorAmortizacion.compareTo(BigDecimal.TEN.multiply(BigDecimal.TEN)) != 0) {

						String sumatoriaFactorAmort = PropertiesUtilities.getMessage("msg.info.summation.amortization.factor") + sumaFactorAmortizacion + "%";
						String requiereSumatoriaCienPorciento = PropertiesUtilities.getMessage("msg.requires.hundred.porcient.amortization.factor");
						cuerpoMensaje = sumatoriaFactorAmort + ". " + requiereSumatoriaCienPorciento;
						log.info("::: " + cuerpoMensaje);

						JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
						mostrarMensaje("Error", cuerpoMensaje);
						JSFUtilities.showSimpleValidationDialog();
						return;
					}

					// verificando si la sumatoria amortizacion es igual al valor Nominal
					if (sumaValorNominal.compareTo(security.getInitialNominalValue()) != 0) {
						log.info("::: Montos y Factores de Cuponera de Amortizacion restaurados");

						String sumatoriaMontoAmort = PropertiesUtilities.getMessage("msg.info.summation.amortization.amount") + sumaValorNominal;
						String requiereSumatorialIgualValorNominal = PropertiesUtilities.getMessage("msg.requires.amount.equal.initial.nominal.value") + "("
								+ security.getInitialNominalValue() + "). " + PLEASE_VERIFY;
						cuerpoMensaje = sumatoriaMontoAmort + ". " + requiereSumatorialIgualValorNominal;

						JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
						mostrarMensaje("Error", cuerpoMensaje);
						JSFUtilities.showSimpleValidationDialog();
						return;
					}
				} else {
					// deseleccionando todos los cupones seleccionados para evitar el registro de modificacion
					for (ProgramAmoCouponReqHi proAmo : lstDtbProAmoCouponReqHis) {
						proAmo.setSelected(false);
						proAmo.setDisabled(true);
					}
					log.info("::: Cuponera de AmortizaciOn restaurada con exito (Se deseleccionO los cupones Seleccionados)");

					cuerpoMensaje = PropertiesUtilities.getMessage("msg.no.modified.data");
					JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
					mostrarMensaje("Error", cuerpoMensaje);
					JSFUtilities.showSimpleValidationDialog();
					return;
				}

				// revertido salida a produccion issues: 100,148,143
				// amoPaymentScheduleReqHi.getProgramAmoCouponReqHis().add(
				// lstDtbProAmoCouponReqHis.get(lstDtbProAmoCouponReqHis.size()-1) );
			} else if (!security.isProportionalAmortizationType()) {
				if (!lstDtbProAmoCouponReqHis.isEmpty()) {
					amoPaymentScheduleReqHi.getProgramAmoCouponReqHis().add(lstDtbProAmoCouponReqHis.get(lstDtbProAmoCouponReqHis.size() - 1));
				}
				// revertido salida a produccion issues: 100,148,143
				/* for(ProgramAmoCouponReqHi modAmo : lstDtbProAmoCouponReqHis){ if(modAmo.isPendingState()){
				 * amoPaymentScheduleReqHi.getProgramAmoCouponReqHis().add( modAmo ); } } */
				// revertido salida a produccion issues: 100,148,143
				// se esta cambiando la logica en la modificacion para que solo haga solo update y no recreacion
				// for(ProgramAmoCouponReqHi proAmo : lstDtbProAmoCouponReqHis){
				// if(!proAmo.isDisabled()){
				// amoPaymentScheduleReqHi.getProgramAmoCouponReqHis().add( proAmo );
				// }
				// }
				// amoPaymentScheduleReqHi.getProgramAmoCouponReqHis().add(
				// lstDtbProAmoCouponReqHis.get(lstDtbProAmoCouponReqHis.size()-1) );
			}

			if (amoPaymentScheduleReqHi.getProgramAmoCouponReqHis().isEmpty()) {
				JSFUtilities.addContextMessage("frmSecurityMgmt:tviewGeneral:dtbAmortizationCoupons", FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities.getMessage(PropertiesConstants.PAYMENT_CRON_ERROR_AMO_PRO_MOD_REQUIRED),
						PropertiesUtilities.getMessage(PropertiesConstants.PAYMENT_CRON_ERROR_AMO_PRO_MOD_REQUIRED));
				return;
			}

			paymentCronogramServiceFacade.validateIntPaymentScheRequestAlreadyRegisteredServiceFacade(intPaymentScheduleReqHi, security);
			// paymentCronogramServiceFacade.validateAmoPaymentScheRequestAlreadyRegisteredServiceFacade(amoPaymentScheduleReqHi,
			// security);

			showMessageOnDialog(PropertiesConstants.LBL_CONFIRM_HEADER_REGISTER, null, PropertiesConstants.PAYMENT_CRON_CONF_REGISTER_MOD_AMO, argObj);

			JSFUtilities.executeJavascriptFunction("PF('cnfwPaymentShceMgmt').show()");

		} catch (ServiceException se) {
			if (se.getMessage() != null) {
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, null, se.getErrorService().getMessage(), se.getParams());
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}

	/** Save transaction. */
	@LoggerAuditWeb
	public void saveTransaction() {
		try {
			Object[] argObj = new Object[2];
			argObj[0] = security.getIdSecurityCodePk();
			String viewPage = JSFUtilities.getRequestParameterMap("viewPage");
			// bill in detailOperationHandler
			if (isInterestPaymentScheduleSelected()) {
				argObj[1] = intPaymentScheduleReqHi.getIdIntScheduleReqHisPk() != null ? intPaymentScheduleReqHi.getIdIntScheduleReqHisPk().toString() : null;
			} else if (isAmortizationPaymentScheduleSelected()) {
				argObj[1] = amoPaymentScheduleReqHi.getIdAmoScheduleReqHisPk() != null ? amoPaymentScheduleReqHi.getIdAmoScheduleReqHisPk().toString() : null;
			}

			if (isViewOperationRegister()) {
				if (isInterestPaymentScheduleSelected()) {

					paymentCronogramServiceFacade.registerIntPaymentScheduleReqHiServiceFacade(intPaymentScheduleReqHi, security);
					argObj[1] = intPaymentScheduleReqHi.getIdIntScheduleReqHisPk();
					if (isPaymentScheduleModificationPage()) {
						showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_SUCCESS, null, PropertiesConstants.PAYMENT_CRON_REGISTER_MOD_INT_SUCCESS, argObj);
						JSFUtilities.showSimpleValidationDialog();
					} else if (isPaymentScheduleRestructurationPage()) {
						showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_SUCCESS, null, PropertiesConstants.PAYMENT_CRON_REGISTER_REST_INT_SUCCESS, argObj);
						JSFUtilities.showSimpleValidationDialog();
					}

				} else if (isAmortizationPaymentScheduleSelected()) {

					paymentCronogramServiceFacade.registerAmoPaymentScheduleReqHiServiceFacade(amoPaymentScheduleReqHi, security);
					argObj[1] = amoPaymentScheduleReqHi.getIdAmoScheduleReqHisPk();
					if (isPaymentScheduleModificationPage()) {
						showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_SUCCESS, null, PropertiesConstants.PAYMENT_CRON_REGISTER_MOD_AMO_SUCCESS, argObj);
						JSFUtilities.showSimpleValidationDialog();
					} else if (isPaymentScheduleRestructurationPage()) {
						showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_SUCCESS, null, PropertiesConstants.PAYMENT_CRON_REGISTER_REST_AMO_SUCCESS, argObj);
						JSFUtilities.showSimpleValidationDialog();
					}
				}
			} else if (isViewOperationApprove()) {
				if (viewOperationIntPaySheModification) {
					intPaymentScheduleReqHi.setRequestState(RequestStateType.APPROVED.getCode());
					paymentCronogramServiceFacade.updateIntPayScheReqHiReqStateServiceFacade(intPaymentScheduleReqHi, ViewOperationsType.APPROVE);

				} else if (viewOperationAmoPaySheModification) {
					amoPaymentScheduleReqHi.setRequestState(RequestStateType.APPROVED.getCode());
					paymentCronogramServiceFacade.updateAmoPayScheReqHiReqStateServiceFacade(amoPaymentScheduleReqHi, ViewOperationsType.APPROVE);
				}

				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_SUCCESS, null, PropertiesConstants.PAYMENT_CRON_LBL_APPROVE_REQ_SUCCESS, argObj);
				JSFUtilities.showSimpleValidationDialog();
			} else if (isViewOperationAnnul()) {
				if (viewOperationIntPaySheModification) {
					intPaymentScheduleReqHi.setRequestState(RequestStateType.ANNULED.getCode());
					paymentCronogramServiceFacade.updateIntPayScheReqHiReqStateServiceFacade(intPaymentScheduleReqHi, ViewOperationsType.ANULATE);

				} else if (viewOperationAmoPaySheModification) {
					amoPaymentScheduleReqHi.setRequestState(RequestStateType.ANNULED.getCode());
					paymentCronogramServiceFacade.updateAmoPayScheReqHiReqStateServiceFacade(amoPaymentScheduleReqHi, ViewOperationsType.ANULATE);
				}

				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_SUCCESS, null, PropertiesConstants.PAYMENT_CRON_LBL_ANNULATION_REQ_SUCCESS, argObj);
				JSFUtilities.showSimpleValidationDialog();
			} else if (isViewOperationReject()) {
				if (viewOperationIntPaySheModification) {
					intPaymentScheduleReqHi.setRequestState(RequestStateType.REJECTED.getCode());
					paymentCronogramServiceFacade.updateIntPayScheReqHiReqStateServiceFacade(intPaymentScheduleReqHi, ViewOperationsType.REJECT);

				} else if (viewOperationAmoPaySheModification) {
					amoPaymentScheduleReqHi.setRequestState(RequestStateType.REJECTED.getCode());
					paymentCronogramServiceFacade.updateAmoPayScheReqHiReqStateServiceFacade(amoPaymentScheduleReqHi, ViewOperationsType.REJECT);
				}

				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_SUCCESS, null, PropertiesConstants.PAYMENT_CRON_LBL_REJECT_REQ_SUCCESS, argObj);
				JSFUtilities.showSimpleValidationDialog();

			} else if (isViewOperationConfirm()) {

				if (viewPage.equals(GeneralConstants.PAY_SHEDULE_MODIFICATION_PAGE)) {

					if (viewOperationIntPaySheModification) {

						intPaymentScheduleReqHi.setProgramIntCouponReqHis(lstDtbProIntCouponReqHis);
						paymentCronogramServiceFacade.confirmIntPaymentScheduleModificationServiceFacade(intPaymentScheduleReqHi, security.getInterestPaymentSchedule());
					} else if (viewOperationAmoPaySheModification) {
						amoPaymentScheduleReqHi.setProgramAmoCouponReqHis(lstDtbProAmoCouponReqHis);
						paymentCronogramServiceFacade.confirmAmoPaymentScheduleModificationServiceFacade(amoPaymentScheduleReqHi,
								security.getAmortizationPaymentSchedule());
						// TODO metodo UPDATE revertido salida a produccion issues: 100,148,143
						// paymentCronogramServiceFacade.confirmAmoPaymentScheduleModificationUpdateServiceFacade(amoPaymentScheduleReqHi,
						// security.getAmortizationPaymentSchedule());
					}

				} else if (viewPage.equals(GeneralConstants.PAY_SHEDULE_RESTRUCTURATION_PAGE)) {
					if (viewOperationIntPaySheModification) {
						intPaymentScheduleReqHi.setProgramIntCouponReqHis(lstDtbProIntCouponReqHis);
						paymentCronogramServiceFacade.confirmIntPaymentScheduleRestructuringServiceFacade(intPaymentScheduleReqHi, security.getInterestPaymentSchedule());
					} else if (viewOperationAmoPaySheModification) {
						amoPaymentScheduleReqHi.setProgramAmoCouponReqHis(lstDtbProAmoCouponReqHis);
						paymentCronogramServiceFacade.confirmAmoPaymentScheduleRestructuringServiceFacade(amoPaymentScheduleReqHi,
								security.getAmortizationPaymentSchedule());
					}
				}
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_SUCCESS, null, PropertiesConstants.PAYMENT_CRON_LBL_CONFIRM_REQ_SUCCESS, argObj);
				JSFUtilities.showSimpleValidationDialog();
			}
			if (viewPage.equals("M")) {
				searchPaymentScheModificationHandler();
				JSFUtilities.showSimpleEndTransactionDialog("searchScheduleModification");
			} else if (viewPage.equals("R")) {
				searchPaymentScheRestructuringHandler();
				JSFUtilities.showSimpleEndTransactionDialog("searchScheduleRestructuring");
			}

		} catch (ServiceException se) {
			if (se.getMessage() != null) {
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, null, se.getErrorService().getMessage(), se.getParams());
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}

	/** Confirm modificacion request.
	 * 
	 * @throws ServiceException the service exception */
	public void confirmModificacionRequest() throws ServiceException {
		if (viewOperationIntPaySheModification) {
			intPaymentScheduleReqHi.setRequestState(RequestStateType.CONFIRMED.getCode());
			paymentCronogramServiceFacade.updateIntPayScheReqHiReqStateServiceFacade(intPaymentScheduleReqHi, ViewOperationsType.CONFIRM);

			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_SUCCESS, null, PropertiesConstants.PAYMENT_CRON_LBL_CONFIRM_REQ_SUCCESS, null);
		} else if (viewOperationAmoPaySheModification) {
			amoPaymentScheduleReqHi.setRequestState(RequestStateType.CONFIRMED.getCode());
			paymentCronogramServiceFacade.updateAmoPayScheReqHiReqStateServiceFacade(amoPaymentScheduleReqHi, ViewOperationsType.CONFIRM);

			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_SUCCESS, null, PropertiesConstants.PAYMENT_CRON_LBL_CONFIRM_REQ_SUCCESS, null);
		}
	}

	/** Validate request transaction.
	 * 
	 * @return true, if successful
	 * @throws ServiceException the service exception */
	public boolean validateRequestTransaction() throws ServiceException {
		String message = "";

		if (IssuerStateType.BLOCKED.getCode().equals(security.getIssuer().getStateIssuer())) {
			switch (ViewOperationsType.get(getViewOperationType())) {
			case APPROVE:
				message = PropertiesConstants.PAYMENT_CRON_ERROR_APPROVE_ISSUER_BLOCKED;
				break;
			case CONFIRM:
				message = PropertiesConstants.PAYMENT_CRON_ERROR_CONFIRM_ISSUER_BLOCKED;
				break;
			}
			JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, message, null);
			JSFUtilities.showSimpleValidationDialog();
			return true;
		}

		if (IssuerStateType.BLOCKED.getCode().equals(security.getIssuer().getStateIssuer())) {
			switch (ViewOperationsType.get(getViewOperationType())) {
			case APPROVE:
				message = PropertiesConstants.PAYMENT_CRON_ERROR_APPROVE_SECURITY_BLOCKED;
				break;
			case CONFIRM:
				message = PropertiesConstants.PAYMENT_CRON_ERROR_CONFIRM_SECURITY_BLOCKED;
				break;
			}
			JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, message, null);
			JSFUtilities.showSimpleValidationDialog();
			return true;
		}

		return false;
	}

	/** Before approve modification request. */
	public void beforeApproveModificationRequest() {
		try {
			Object[] argObj = new Object[1];

			if (viewOperationIntPaySheModification) {
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_APPROVE, null, PropertiesConstants.PAYMENT_CRON_LBL_APPROVE_REQ_QUESTION, argObj);

			} else if (viewOperationAmoPaySheModification) {
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_APPROVE, null, PropertiesConstants.PAYMENT_CRON_LBL_APPROVE_REQ_QUESTION, argObj);
			}

			JSFUtilities.executeJavascriptFunction("PF('cnfwPaymentShceMgmt').show()");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Before annulate modification request. */
	public void beforeAnnulateModificationRequest() {
		try {
			Object[] argObj = new Object[1];

			if (viewOperationIntPaySheModification) {
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ANNUL, null, PropertiesConstants.PAYMENT_CRON_LBL_ANNULATION_REQ_QUESTION, argObj);

			} else if (viewOperationAmoPaySheModification) {

				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ANNUL, null, PropertiesConstants.PAYMENT_CRON_LBL_ANNULATION_REQ_QUESTION, argObj);

			}
			JSFUtilities.executeJavascriptFunction("PF('cnfwPaymentShceMgmt').show()");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Before reject modification request. */
	public void beforeRejectModificationRequest() {
		try {
			Object[] argObj = new Object[1];

			intPaymentScheduleReqHi.setRejectMotive(null);
			intPaymentScheduleReqHi.setRejectOtherMotive(null);

			amoPaymentScheduleReqHi.setRejectMotive(null);
			amoPaymentScheduleReqHi.setRejectOtherMotive(null);

			if (viewOperationIntPaySheModification) {

				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_REJECT, null, PropertiesConstants.PAYMENT_CRON_LBL_REJECT_REQ_QUESTION, argObj);

			} else if (viewOperationAmoPaySheModification) {

				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_REJECT, null, PropertiesConstants.PAYMENT_CRON_LBL_REJECT_REQ_QUESTION, argObj);

			}
			JSFUtilities.executeJavascriptFunction("PF('dlgwMotive').show()");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Before confirm modification. */
	public void beforeConfirmModification() {
		Object[] argObj = new Object[1];

		intPaymentScheduleReqHi.setRejectMotive(null);
		intPaymentScheduleReqHi.setRejectOtherMotive(null);

		amoPaymentScheduleReqHi.setRejectMotive(null);
		amoPaymentScheduleReqHi.setRejectOtherMotive(null);

		if (viewOperationIntPaySheModification) {
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_CONFIRM, null, PropertiesConstants.PAYMENT_CRON_LBL_CONFIRM_REQ_QUESTION, argObj);

		} else if (viewOperationAmoPaySheModification) {

			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_CONFIRM, null, PropertiesConstants.PAYMENT_CRON_LBL_CONFIRM_REQ_QUESTION, argObj);

		}
		JSFUtilities.executeJavascriptFunction("PF('cnfwPaymentShceMgmt').show()");
	}

	/** Process reject handler. */
	public void processRejectHandler() {
		try {
			JSFUtilities.executeJavascriptFunction("PF('dlgwMotive').hide()");
			JSFUtilities.executeJavascriptFunction("PF('cnfwPaymentShceMgmt').show()");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** After issuer helper handler. */
	public void afterIssuerHelperHandler() {
		try {
			if (issuerHelperSearch != null && StringUtils.isNotBlank(issuerHelperSearch.getIdIssuerPk())) {

				paymentScheduleTO.setIdIssuerPk(issuerHelperSearch.getIdIssuerPk());
				// }
			} else {
				paymentScheduleTO.setIdIssuerPk(null);
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Start Restructuring. */

	public void beforeSaveAmoPayRestructuringHandler() {
		try {
			viewOperationAmoPaySheModification = true;
			viewOperationIntPaySheModification = false;

			Object[] argObj = new Object[1];
			argObj[0] = security.getIdSecurityCodePk();

			amoPaymentScheduleReqHi.setRegistryType(PaymentScheduleReqRegisterType.REQUEST_RESTRUCTURING.getCode());
			amoPaymentScheduleReqHi.setRequestState(RequestStateType.REGISTERED.getCode());

			/** If Amortization Payment Schedule is Type NO PROPORTIONAL, then no Ask Restructuring */
			if (AmortizationType.NO_PROPORTIONAL.getCode().equals(amoPaymentScheduleReqHi.getAmortizationType())) {
				BigDecimal amortizationFactor = BigDecimal.ZERO;
				for (ProgramAmoCouponReqHi proAmoCoupon : lstDtbProAmoCouponReqHis) {
					amortizationFactor = CommonsUtilities.addTwoDecimal(amortizationFactor, proAmoCoupon.getAmortizationFactor(),
							GeneralConstants.ROUNDING_DIGIT_NUMBER_FOUR);
				}
				if (amortizationFactor.compareTo(new BigDecimal(100)) != 0) {

					showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.SECURITY_ERROR_PAYMENT_INCORRECT_TOT_FACTOR, null);
					JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationSec').show()");
					return;
				}
			} else if (amoPaymentScheduleReqHi.getProgramAmoCouponReqHis().isEmpty()) {
				JSFUtilities.addContextMessage("frmSecurityMgmt:tviewGeneral:dtbAmortizationCoupons", FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities.getMessage(PropertiesConstants.PAYMENT_CRON_ERROR_AMO_PRO_REST_REQUIRED),
						PropertiesUtilities.getMessage(PropertiesConstants.PAYMENT_CRON_ERROR_AMO_PRO_REST_REQUIRED));
				return;
			}

			boolean blProgramAmo = false;
			for (ProgramAmoCouponReqHi proAmoCoupon : lstDtbProAmoCouponReqHis) {
				if (ProgramScheduleStateType.PENDING.getCode().equals(proAmoCoupon.getStateProgramAmortizaton())) {
					blProgramAmo = true;
					break;
				}
			}

			if (!blProgramAmo) {
				JSFUtilities.addContextMessage("frmSecurityMgmt:tviewGeneral:dtbAmortizationCoupons", FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities.getMessage(PropertiesConstants.PAYMENT_CRON_ERROR_AMO_PRO_REST_REQUIRED),
						PropertiesUtilities.getMessage(PropertiesConstants.PAYMENT_CRON_ERROR_AMO_PRO_REST_REQUIRED));
				return;
			}

			paymentCronogramServiceFacade.validateAmoPaymentScheRequestAlreadyRegisteredServiceFacade(amoPaymentScheduleReqHi, security);

			showMessageOnDialog(PropertiesConstants.LBL_CONFIRM_HEADER_REGISTER, null, PropertiesConstants.PAYMENT_CRON_CONF_REGISTER_REST_AMO, argObj);

			JSFUtilities.executeJavascriptFunction("PF('cnfwPaymentShceMgmt').show()");

		} catch (ServiceException se) {
			if (se.getMessage() != null) {
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, null, se.getErrorService().getMessage(), se.getParams());
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}

	/** Before save int payment restructuring handler. */
	public void beforeSaveIntPaymentRestructuringHandler() {
		try {
			viewOperationIntPaySheModification = true;
			viewOperationAmoPaySheModification = false;

			Object[] argObj = new Object[1];
			argObj[0] = security.getIdSecurityCodePk();
			intPaymentScheduleReqHi.setRegistryType(PaymentScheduleReqRegisterType.REQUEST_RESTRUCTURING.getCode());
			intPaymentScheduleReqHi.setRequestState(RequestStateType.REGISTERED.getCode());

			if (!initialIntPaymentScheduleReqHi.equals(intPaymentScheduleReqHi.toString())) {
				JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.PAYMENT_CRON_ERROR_RESTRUCTURE_AGAIN, null);
				JSFUtilities.showSimpleValidationDialog();
				return;
			}

			if (intPaymentScheduleReqHi.getProgramIntCouponReqHis().isEmpty()) {
				JSFUtilities.addContextMessage("frmSecurityMgmt:tviewGeneral:dtbInterestPaymentCron", FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities.getMessage(PropertiesConstants.PAYMENT_CRON_ERROR_INT_PRO_REST_REQUIRED),
						PropertiesUtilities.getMessage(PropertiesConstants.PAYMENT_CRON_ERROR_INT_PRO_REST_REQUIRED));
				return;
			}

			boolean blProgramInt = false;
			for (ProgramIntCouponReqHi proIntCoupon : lstDtbProIntCouponReqHis) {
				if (ProgramScheduleStateType.PENDING.getCode().equals(proIntCoupon.getStateProgramInterest())) {
					blProgramInt = true;
					break;
				}
			}

			if (!blProgramInt) {
				JSFUtilities.addContextMessage("frmSecurityMgmt:tviewGeneral:dtbInterestPaymentCron", FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities.getMessage(PropertiesConstants.PAYMENT_CRON_ERROR_INT_PRO_REST_REQUIRED),
						PropertiesUtilities.getMessage(PropertiesConstants.PAYMENT_CRON_ERROR_INT_PRO_REST_REQUIRED));
				return;
			}

			paymentCronogramServiceFacade.validateIntPaymentScheRequestAlreadyRegisteredServiceFacade(intPaymentScheduleReqHi, security);

			showMessageOnDialog(PropertiesConstants.LBL_CONFIRM_HEADER_REGISTER, null, PropertiesConstants.PAYMENT_CRON_CONF_REGISTER_REST_INT, argObj);

			JSFUtilities.executeJavascriptFunction("PF('cnfwPaymentShceMgmt').show()");

			// JSFUtilities.showSimpleValidationDialog();
		} catch (ServiceException se) {
			if (se.getMessage() != null) {
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, null, se.getErrorService().getMessage(), se.getParams());
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}

	/** Generate interest restructuring handler. */
	public void generateInterestRestructuringHandler() {
		try {
			Date nextYear = null;
			Date septemberLeap = null;
			Security securityAux = new Security();

			Integer indexFirstPendingCoupon = 0;
			Date firstPendingCouponDate = null;
			Date expirationDate = security.getExpirationDate();
			Integer nroCoupons = null;
			Integer monthsTerm = null;
			Integer daysTerm = null;
			Date previousExpirationDate = null;
			List<ProgramIntCouponReqHi> lstDtbProIntCouponReqHisAux = new ArrayList<ProgramIntCouponReqHi>();

			securityAux.setInterestPaymentModality(security.getInterestPaymentModality());
			securityAux.setCalendarMonth(intPaymentScheduleReqHi.getCalendarMonth());
			securityAux.setCalendarType(intPaymentScheduleReqHi.getCalendarType());
			securityAux.setCalendarDays(intPaymentScheduleReqHi.getCalendarDays());

			// iterate the list for search the first pending coupon and the last payed coupon number
			for (ProgramIntCouponReqHi proIntCoupon : lstDtbProIntCouponReqHis) {
				// added the payed coupons to the list
				if (proIntCoupon.getStateProgramInterest().equals(ProgramScheduleStateType.EXPIRED.getCode())
						|| proIntCoupon.getStateProgramInterest().equals(ProgramScheduleStateType.REDEEMED.getCode())) {
					lstDtbProIntCouponReqHisAux.add(proIntCoupon);
				}
				if (proIntCoupon.getStateProgramInterest().equals(ProgramScheduleStateType.PENDING.getCode())) {
					firstPendingCouponDate = proIntCoupon.getBeginingDate();
					break;
				}
				indexFirstPendingCoupon = indexFirstPendingCoupon + 1;
			}

			monthsTerm = CommonsUtilities.getMonthsBetween(firstPendingCouponDate, expirationDate);
			daysTerm = CommonsUtilities.getDaysBetween(firstPendingCouponDate, expirationDate);

			nroCoupons = generateCouponsNumber(intPaymentScheduleReqHi.getPeriodicity(), intPaymentScheduleReqHi.getPeriodicityDays(), monthsTerm, daysTerm,
					"frmSecurityMgmt:tviewGeneral:cboCronInterestPayPeriodicity");

			if (nroCoupons == null) {
				return;
			}
			ProgramIntCouponReqHi programIntCouponReqHi = null;

			for (int i = 1; i <= nroCoupons; i++) {
				programIntCouponReqHi = new ProgramIntCouponReqHi();
				programIntCouponReqHi.setCouponNumber(indexFirstPendingCoupon + Integer.valueOf(i));

				// Begin Date
				if (Validations.validateIsNullOrEmpty(previousExpirationDate)) {
					programIntCouponReqHi.setBeginingDate(firstPendingCouponDate);
				} else {
					programIntCouponReqHi.setBeginingDate(previousExpirationDate);
				}
				// Expiration Date
				if (i == nroCoupons) {
					programIntCouponReqHi.setExpirationDate(security.getExpirationDate());
				} else {
					if (intPaymentScheduleReqHi.isByDaysPeriodicity()) {
						programIntCouponReqHi.setExpirationDate(CommonsUtilities.addDaysToDate(programIntCouponReqHi.getBeginingDate(),
								intPaymentScheduleReqHi.getPeriodicityDays()));
					} else {
						programIntCouponReqHi.setExpirationDate(CommonsUtilities.addMonthsToDate(programIntCouponReqHi.getBeginingDate(),
								InterestPeriodicityType.get(intPaymentScheduleReqHi.getPeriodicity()).getIndicator1()));
					}
				}
				previousExpirationDate = programIntCouponReqHi.getExpirationDate();

				// Payment Date
				programIntCouponReqHi.setPaymentDate(generalParameterFacade.workingDateCalculateServiceFacade(programIntCouponReqHi.getExpirationDate(),
						GeneralConstants.ONE_VALUE_INTEGER));

				Integer paymentDays = null;
				if (security.isExtendedTerm()) {
					paymentDays = CommonsUtilities.getDaysBetween(programIntCouponReqHi.getBeginingDate(), programIntCouponReqHi.getPaymentDate());
				} else {
					paymentDays = CommonsUtilities.getDaysBetween(programIntCouponReqHi.getBeginingDate(), programIntCouponReqHi.getExpirationDate());
				}
				programIntCouponReqHi.setPaymentDays(paymentDays);

				programIntCouponReqHi.setCorporativeDate(CommonsUtilities.currentDate());

				if (security.getCurrency().equals(CurrencyType.UFV.getCode()) || security.getCurrency().equals(CurrencyType.DMV.getCode())) {
					programIntCouponReqHi.setExchangeDateRate(programIntCouponReqHi.getPaymentDate());
					programIntCouponReqHi.setCurrencyPayment(CurrencyType.PYG.getCode());
				}

				// Registry Date
				programIntCouponReqHi.setRegistryDate(CommonsUtilities.addDaysToDate(programIntCouponReqHi.getExpirationDate(), security.getStockRegistryDays() * -1));
				programIntCouponReqHi.setRegistryDate(generalParameterFacade.workingDateCalculateServiceFacade(programIntCouponReqHi.getRegistryDate(), -1));
				// Cut Off
				programIntCouponReqHi.setCutoffDate(programIntCouponReqHi.getRegistryDate());
				// Interest Rate
				programIntCouponReqHi.setInterestRate(securitiesUtils.interestRateCalculate(securityAux, intPaymentScheduleReqHi.getPeriodicity(),
						intPaymentScheduleReqHi.getInterestFactor(), programIntCouponReqHi.getBeginingDate(), programIntCouponReqHi.getPaymentDays(),
						programIntCouponReqHi.getCouponNumber(), septemberLeap, nextYear));

				programIntCouponReqHi.setIntPaymentScheduleReqHi(intPaymentScheduleReqHi);
				programIntCouponReqHi.setStateProgramInterest(ProgramScheduleStateType.PENDING.getCode());
				programIntCouponReqHi.setIndRounding(BooleanType.NO.getCode());
				programIntCouponReqHi.setPayedAmount(BigDecimal.ZERO);
				programIntCouponReqHi.setCurrency(security.getCurrency());
				programIntCouponReqHi.setLastModifyApp(Integer.valueOf(1));
				programIntCouponReqHi.setLastModifyDate(CommonsUtilities.currentDateTime());
				programIntCouponReqHi.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
				programIntCouponReqHi.setLastModifyUser(userInfo.getUserAccountSession().getUserName());

				lstDtbProIntCouponReqHisAux.add(programIntCouponReqHi);
			}
			intPaymentScheduleReqHi.setProgramIntCouponReqHis(lstDtbProIntCouponReqHisAux);
			lstDtbProIntCouponReqHis = lstDtbProIntCouponReqHisAux;

			securitiesUtils.calculateCouponAmountProgramInterestHistory(security, intPaymentScheduleReqHi);

			initialIntPaymentScheduleReqHi = intPaymentScheduleReqHi.toString();

		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Chage cbo amort payment modality handler. */
	public void chageCboAmortPaymentModalityHandler() {
		try {
			amoPaymentScheduleReqHi.setAmortizationType(null);
			changeCboAmortizationTypeHandler();
			if (amoPaymentScheduleReqHi.isAtMaturityPaymentModality()) {
				amoPaymentScheduleReqHi.setPaymentFirstExpirationDate(security.getExpirationDate());
				amoPaymentScheduleReqHi.setNumberPayments(GeneralConstants.ONE_VALUE_INTEGER);
				amoPaymentScheduleReqHi.setAmortizationFactor(new BigDecimal(100));
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Change cbo amortization type handler. */
	public void changeCboAmortizationTypeHandler() {
		try {
			JSFUtilities.resetComponent(PaymentScheduleType.CBO_CRON_PERIODICITY_AMORT.getValue());
			JSFUtilities.resetComponent(PaymentScheduleType.CBO_CRON_PERIODICITY_DAYS.getValue());

			JSFUtilities.resetComponent(PaymentScheduleType.CAL_FIRST_EXP_PAYMENT.getValue());
			JSFUtilities.resetComponent(PaymentScheduleType.TXT_CRON_PAYMENTS_NUMBER.getValue());
			JSFUtilities.resetComponent(PaymentScheduleType.TXT_CRON_AMORTIZATION_FACTOR.getValue());

			amoPaymentScheduleReqHi.setPeriodicity(null);
			amoPaymentScheduleReqHi.setPeriodicityDays(null);
			amoPaymentScheduleReqHi.setPaymentFirstExpirationDate(null);
			amoPaymentScheduleReqHi.setNumberPayments(null);
			amoPaymentScheduleReqHi.setAmortizationFactor(null);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}

	}

	/** Change cbo per cron amortization. */
	public void changeCboPerCronAmortization() {
		try {
			amoPaymentScheduleReqHi.setPeriodicityDays(null);
			amoPaymentScheduleReqHi.setPaymentFirstExpirationDate(null);
			amoPaymentScheduleReqHi.setNumberPayments(null);
			amoPaymentScheduleReqHi.setAmortizationFactor(null);

			calculateAmortFactorAndNumberCoupons();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Insert amort periodicity days. */
	public void insertAmortPeriodicityDays() {
		if (amoPaymentScheduleReqHi.getPeriodicity() != null) {
			if (amoPaymentScheduleReqHi.getPeriodicityDays().compareTo(security.getSecurityDaysTerm()) == 1) {
				amoPaymentScheduleReqHi.setPeriodicityDays(null);
				String message = PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_ERROR_PERIOD_DAYS_EXCEEDS);
				JSFUtilities.addContextMessage(PaymentScheduleType.TXT_CRON_PERIODICITY_DAYS.getValue(), FacesMessage.SEVERITY_ERROR, message, message);
				return;
			}
			calculateAmortFactorAndNumberCoupons();
		}
	}

	/** Calculate amort factor and number coupons. */
	public void calculateAmortFactorAndNumberCoupons() {
		try {
			if (amoPaymentScheduleReqHi.isAtMaturityPaymentModality()) {
				amoPaymentScheduleReqHi.setPeriodicityDays(GeneralConstants.ONE_VALUE_INTEGER);
			} else if (amoPaymentScheduleReqHi.isPartialPaymentModality()) {
				if (amoPaymentScheduleReqHi.isByDaysPeriodicity()) {
					if (amoPaymentScheduleReqHi.getPeriodicityDays() != null && amoPaymentScheduleReqHi.getPeriodicityDays() > 0) {
						amoPaymentScheduleReqHi.setPaymentFirstExpirationDate(CommonsUtilities.addDaysToDate(security.getIssuanceDate(),
								amoPaymentScheduleReqHi.getPeriodicityDays()));
					}
				} else {
					if (amoPaymentScheduleReqHi.getPeriodicity() != null) {
						amoPaymentScheduleReqHi.setPaymentFirstExpirationDate(CommonsUtilities.addMonthsToDate(security.getIssuanceDate(),
								InterestPeriodicityType.get(amoPaymentScheduleReqHi.getPeriodicity()).getIndicator1()));
					}
				}
			}

			Date beginDateSecondCoupon = amoPaymentScheduleReqHi.getPaymentFirstExpirationDate();
			int remaindsDaysTerm = CommonsUtilities.getDaysBetween(beginDateSecondCoupon, security.getExpirationDate());
			int remaindsMonthsTerm = CommonsUtilities.getMonthsBetween(beginDateSecondCoupon, security.getExpirationDate());

			BigDecimal coupons = getNroCouponsForAmortization(remaindsMonthsTerm, remaindsDaysTerm);

			if (coupons != null) {
				coupons = coupons.add(BigDecimal.ONE);
				amoPaymentScheduleReqHi.setNumberPayments(coupons.intValue());
				BigDecimal facAmort = null;
				BigDecimal actualPercent = new BigDecimal("100");

				facAmort = actualPercent.divide(coupons, MathContext.DECIMAL128);
				facAmort = CommonsUtilities.roundRateFactor(facAmort);
				amoPaymentScheduleReqHi.setAmortizationFactor(facAmort);
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Gets the nro coupons for amortization.
	 * 
	 * @param securityMonths the security months
	 * @param securityDays the security days
	 * @return the nro coupons for amortization
	 * @throws Exception the exception */
	private BigDecimal getNroCouponsForAmortization(Integer securityMonths, Integer securityDays) throws Exception {
		if (Validations.validateIsNotNullAndNotEmpty(securityMonths) && Validations.validateIsNotNullAndNotEmpty(securityDays)
				&& Validations.validateIsNotNullAndNotEmpty(amoPaymentScheduleReqHi.getPeriodicity())) {
			BigDecimal coupons = null;
			BigDecimal monthsTerm = new BigDecimal(securityMonths);
			BigDecimal daysTerm = new BigDecimal(securityDays);
			BigDecimal periodicity = null;

			if (amoPaymentScheduleReqHi.isByDaysPeriodicity()) {
				if (amoPaymentScheduleReqHi.getPeriodicityDays() == null || amoPaymentScheduleReqHi.getPeriodicityDays() <= 0) {
					amoPaymentScheduleReqHi.setNumberPayments(null);
					return null;
				}
				periodicity = BigDecimal.valueOf(amoPaymentScheduleReqHi.getPeriodicityDays());
				coupons = daysTerm.divide(periodicity, RoundingMode.DOWN);
				return coupons;
			} else {
				periodicity = new BigDecimal(InterestPeriodicityType.get(amoPaymentScheduleReqHi.getPeriodicity()).getIndicator1());
				coupons = monthsTerm.divide(periodicity, RoundingMode.DOWN);
				return coupons;
			}
		}
		return null;
	}

	/** Gets the interest rate pro int coupon req his.
	 * 
	 * @param coupon the coupon
	 * @return the interest rate pro int coupon req his */
	private BigDecimal getInterestRateProIntCouponReqHis(Integer coupon) {
		for (ProgramIntCouponReqHi proInt : lstDtbProIntCouponReqHis) {
			if (proInt.getCouponNumber().equals(coupon)) {
				return proInt.getInterestRate();
			}
		}
		return BigDecimal.ZERO;
	}

	/** Change cbo per cron interest. */
	public void changeCboPerCronInterest() {
		try {
			if (intPaymentScheduleReqHi.isByDaysPeriodicity()) {
				intPaymentScheduleReqHi.setCalendarMonth(CalendarMonthType.ACTUAL.getCode());
				intPaymentScheduleReqHi.setCalendarType(CalendarType.CALENDAR.getCode());
				intPaymentScheduleReqHi.setCalendarDays(CalendarDayType.ACTUAL.getCode());
			} else {
				intPaymentScheduleReqHi.setPeriodicityDays(null);
				JSFUtilities.resetComponent("frmSecurityMgmt:tviewGeneral:txtCronPeriodicityDays");
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Change cbo calendar month. */
	public void changeCboCalendarMonth() {
		try {
			intPaymentScheduleReqHi.setCalendarType(null);
			JSFUtilities.resetComponent(PaymentScheduleType.CBO_CRON_CALENDAR_TYPE.getValue());

			if (intPaymentScheduleReqHi.is_30CalendarMonth()) {
				intPaymentScheduleReqHi.setCalendarType(CalendarType.COMMERCIAL.getCode());
			}
			changeCboCalendarType();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Change cbo calendar type. */
	public void changeCboCalendarType() {
		try {
			intPaymentScheduleReqHi.setCalendarDays(null);
			JSFUtilities.resetComponent(PaymentScheduleType.CBO_CRON_MAXIUM_RATE.getValue());
			if (intPaymentScheduleReqHi.isCommercialCalendarType()) {
				intPaymentScheduleReqHi.setCalendarDays(CalendarDayType._360.getCode());
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Change cbo interest type handler. */
	public void changeCboInterestTypeHandler() {
		try {

			intPaymentScheduleReqHi.setInterestFactor(null);

			intPaymentScheduleReqHi.setRateType(null);
			intPaymentScheduleReqHi.setSpread(null);
			intPaymentScheduleReqHi.setOperationType(null);
			intPaymentScheduleReqHi.setRateValue(null);

			intPaymentScheduleReqHi.setYield(null);
			intPaymentScheduleReqHi.setMinimumRate(null);
			intPaymentScheduleReqHi.setMaximumRate(null);

			JSFUtilities.resetComponent(PaymentScheduleType.TXT_CRON_INTEREST_RATE.getValue());

			JSFUtilities.resetComponent(PaymentScheduleType.CBO_CRON_RATE_TYPE.getValue());
			JSFUtilities.resetComponent(PaymentScheduleType.TXT_CRON_INT_SPREAD_ESCALON.getValue());
			JSFUtilities.resetComponent(PaymentScheduleType.CBO_CRON_OPERATION_TYPE.getValue());
			JSFUtilities.resetComponent(PaymentScheduleType.TXT_CRON_RATE_VALUE.getValue());

			JSFUtilities.resetComponent(PaymentScheduleType.TXT_CRON_MINIUM_RATE.getValue());
			JSFUtilities.resetComponent(PaymentScheduleType.TXT_CRON_MAXIUM_RATE.getValue());

			changeCboRateTypeHandler();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Change cbo rate type handler. */
	public void changeCboRateTypeHandler() {
		try {
			if (intPaymentScheduleReqHi.getRateType() != null) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("indicatorType", IndicatorType.TASA.getCode());
				map.put("indicatorCode", intPaymentScheduleReqHi.getRateType());
				map.put("status", FinancialIndicatorStateType.VIGENTE.getCode());
				map.put("currentDate", CommonsUtilities.currentDate());
				FinancialIndicator financialIndicator = null;
				try {
					financialIndicator = generalParameterFacade.findFinancialIndicatorServiceFacade(map);

					intPaymentScheduleReqHi.setRateValue(financialIndicator.getRateValue());
				} catch (ServiceException se) {
					intPaymentScheduleReqHi.setRateType(null);
					intPaymentScheduleReqHi.setRateValue(null);
					if (se.getErrorService() != null) {
						String message = PropertiesUtilities.getExceptionMessage(se.getErrorService().getMessage());
						JSFUtilities.addContextMessage(PaymentScheduleType.CBO_CRON_RATE_TYPE.getValue(), FacesMessage.SEVERITY_ERROR, message, message);
					}
				}
			} else {
				intPaymentScheduleReqHi.setRateValue(null);
			}
			calculateInterestRateHandler();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Gets the amortization factor.
	 * 
	 * @return the amortization factor */
	public void getAmortizationFactor() {
		try {
			if (isProportionalAmortizationType()) {
				Date expirationDate = security.getExpirationDate();
				BigDecimal facAmort = null;
				BigDecimal actualPercent = new BigDecimal("100");
				Integer monthsTerm = CommonsUtilities.getMonthsBetween(getRealInitialAmortizationDate(), expirationDate);
				Integer daysTerm = CommonsUtilities.getDaysBetween(getRealInitialAmortizationDate(), expirationDate);
				Integer nroCoupons = generateCouponsNumber(amoPaymentScheduleReqHi.getPeriodicity(), amoPaymentScheduleReqHi.getPeriodicityDays(), monthsTerm, daysTerm,
						null);

				if (Validations.validateIsNotNullAndNotEmpty(nroCoupons)) {
					BigDecimal coupons = new BigDecimal(nroCoupons.toString());
					actualPercent = actualPercent.subtract(getRealPercentPaid());
					try {
						facAmort = actualPercent.divide(coupons);
					} catch (ArithmeticException e) {
						facAmort = null;
					}
					amoPaymentScheduleReqHi.setAmortizationFactor(facAmort);
				} else
					amoPaymentScheduleReqHi.setAmortizationFactor(null);
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Evaluate payment date.
	 * 
	 * @param amortCoupon the amort coupon
	 * @throws ServiceException the service exception */
	public void evaluatePaymentDate(ProgramAmoCouponReqHi amortCoupon) throws ServiceException {
		Date expirationDate = amortCoupon.getExpirationDate();
		Date nextWorkDay = generalParameterFacade.workingDateCalculateServiceFacade(expirationDate, GeneralConstants.ONE_VALUE_INTEGER);
		amortCoupon.setPaymentDate(nextWorkDay);
		int indx = lstDtbProAmoCouponReqHis.indexOf(amortCoupon);
		lstDtbProAmoCouponReqHis.get(indx + 1).setBeginingDate(expirationDate);
	}

	/** Evaluate amortization factor.
	 * 
	 * @param amortCoupon the amort coupon
	 * @throws ServiceException the service exception */
	public void evaluateAmortizationFactor(ProgramAmoCouponReqHi amortCoupon) throws ServiceException {
		BigDecimal amortFactor = amortCoupon.getAmortizationFactor();
		BigDecimal couponAmount = CommonsUtilities.getPercentOnDecimals(amortFactor.multiply(security.getInitialNominalValue()));
		amortCoupon.setCouponAmount(couponAmount);
	}

	/** Generate amortization restructuring handler. */
	public void generateAmortizationRestructuringHandler() {
		try {
			Integer indexFirstPendingCoupon = 0;
			Date firstPendingCouponDate = null;
			Date expirationDate = security.getExpirationDate();

			Integer nroCoupons = null;
			Integer monthsTerms = null;
			Integer daysTerms = null;
			Date previousExpirationDate = null;

			BigDecimal realAmortizationPercent = getRealActualAmortizationPercent();

			List<ProgramAmoCouponReqHi> lstDtbProAmoCouponReqHisAux = new ArrayList<ProgramAmoCouponReqHi>();

			// iterate the list for search the first pending coupon and the last payed coupon number
			for (ProgramAmoCouponReqHi proAmoCoupon : lstDtbProAmoCouponReqHis) {
				// added the payed coupons to the list
				if (proAmoCoupon.getStateProgramAmortizaton().equals(ProgramScheduleStateType.EXPIRED.getCode())) {
					lstDtbProAmoCouponReqHisAux.add(proAmoCoupon);
				}
				if (proAmoCoupon.getStateProgramAmortizaton().equals(ProgramScheduleStateType.PENDING.getCode())) {
					firstPendingCouponDate = proAmoCoupon.getBeginingDate();
					break;
				}
				indexFirstPendingCoupon = indexFirstPendingCoupon + 1;
			}

			monthsTerms = CommonsUtilities.getMonthsBetween(firstPendingCouponDate, expirationDate);
			daysTerms = CommonsUtilities.getDaysBetween(firstPendingCouponDate, expirationDate);

			if (amoPaymentScheduleReqHi.isAtMaturityPaymentModality()) {
				nroCoupons = amoPaymentScheduleReqHi.getNumberPayments();
			} else {
				nroCoupons = generateCouponsNumber(amoPaymentScheduleReqHi.getPeriodicity(), amoPaymentScheduleReqHi.getPeriodicityDays(), monthsTerms, daysTerms,
						"frmSecurityMgmt:tviewGeneral:cboCronPerAmortization");
			}

			if (nroCoupons == null) {
				return;
			}

			ProgramAmoCouponReqHi programAmoCouponReqHi = null;
			BigDecimal amortFactorTotal = BigDecimal.ZERO;

			for (int i = 1; i <= nroCoupons; i++) {
				programAmoCouponReqHi = new ProgramAmoCouponReqHi();
				programAmoCouponReqHi.setCouponNumber(indexFirstPendingCoupon + Integer.valueOf(i));
				programAmoCouponReqHi.setCurrency(security.getCurrency());

				if (i == nroCoupons) { // the last coupon
					if (amortFactorTotal.compareTo(realAmortizationPercent) == -1) {
						BigDecimal sustrac = realAmortizationPercent.subtract(amortFactorTotal);
						sustrac = CommonsUtilities.roundRateFactor(sustrac);
						programAmoCouponReqHi.setAmortizationFactor(sustrac);
					}
				} else {
					programAmoCouponReqHi.setAmortizationFactor(amoPaymentScheduleReqHi.getAmortizationFactor());
					amortFactorTotal = amortFactorTotal.add(amoPaymentScheduleReqHi.getAmortizationFactor());
				}
				programAmoCouponReqHi.setCouponAmount(programAmoCouponReqHi.getAmortizationFactor().multiply(security.getInitialNominalValue())
						.divide(BigDecimal.valueOf(100), MathContext.DECIMAL128));
				// Begin Date
				if (Validations.validateIsNullOrEmpty(previousExpirationDate)) {
					programAmoCouponReqHi.setBeginingDate(firstPendingCouponDate);
				} else {
					programAmoCouponReqHi.setBeginingDate(previousExpirationDate);
				}
				// Expiration Date
				if (amoPaymentScheduleReqHi.isAtMaturityPaymentModality() || i == nroCoupons) {
					programAmoCouponReqHi.setExpirationDate(security.getExpirationDate());
				} else {
					if (amoPaymentScheduleReqHi.isByDaysPeriodicity()) {
						programAmoCouponReqHi.setExpirationDate(CommonsUtilities.addDaysToDate(programAmoCouponReqHi.getBeginingDate(),
								amoPaymentScheduleReqHi.getPeriodicityDays()));
					} else {
						programAmoCouponReqHi.setExpirationDate(CommonsUtilities.addMonthsToDate(programAmoCouponReqHi.getBeginingDate(),
								InterestPeriodicityType.get(amoPaymentScheduleReqHi.getPeriodicity()).getIndicator1()));
					}
				}
				previousExpirationDate = programAmoCouponReqHi.getExpirationDate();

				// Payment Date
				programAmoCouponReqHi.setPaymentDate(generalParameterFacade.workingDateCalculateServiceFacade(programAmoCouponReqHi.getExpirationDate(),
						GeneralConstants.ONE_VALUE_INTEGER));
				programAmoCouponReqHi.setCorporativeDate(CommonsUtilities.currentDate());

				if (security.getCurrency().equals(CurrencyType.UFV.getCode()) || security.getCurrency().equals(CurrencyType.DMV.getCode())) {
					programAmoCouponReqHi.setExchangeDateRate(programAmoCouponReqHi.getPaymentDate());
					programAmoCouponReqHi.setCurrencyPayment(CurrencyType.PYG.getCode());
				}

				// Registry Date
				Date registryDate = CommonsUtilities.addDaysToDate(programAmoCouponReqHi.getExpirationDate(), security.getStockRegistryDays() * -1);
				programAmoCouponReqHi.setRegistryDate(generalParameterFacade.workingDateCalculateServiceFacade(registryDate, -1));
				// Cut Off
				programAmoCouponReqHi.setCutoffDate(programAmoCouponReqHi.getRegistryDate());

				programAmoCouponReqHi.setAmoPaymentScheduleReqHi(amoPaymentScheduleReqHi);
				programAmoCouponReqHi.setStateProgramAmortizaton(ProgramScheduleStateType.PENDING.getCode());

				programAmoCouponReqHi.setIndRounding(BooleanType.NO.getCode());
				programAmoCouponReqHi.setPayedAmount(BigDecimal.ZERO);

				if (amoPaymentScheduleReqHi.getAmortizationType().equals(AmortizationType.NO_PROPORTIONAL.getCode())) {
					// programAmoCouponReqHi.setSelected(Boolean.TRUE);
				}

				lstDtbProAmoCouponReqHisAux.add(programAmoCouponReqHi);

			}

			amoPaymentScheduleReqHi.setProgramAmoCouponReqHis(lstDtbProAmoCouponReqHisAux);
			lstDtbProAmoCouponReqHis = lstDtbProAmoCouponReqHisAux;

		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** Generate coupons number.
	 * 
	 * @param periodicity the periodicity
	 * @param periodicityDays the periodicity days
	 * @param securityMonthsTerm the security months term
	 * @param securityDaysTerm the security days term
	 * @param idControl the id control
	 * @return the integer
	 * @throws Exception the exception */
	public Integer generateCouponsNumber(Integer periodicity, Integer periodicityDays, Integer securityMonthsTerm, Integer securityDaysTerm, String idControl)
			throws Exception {
		if (periodicity != null && securityDaysTerm != null && securityMonthsTerm != null) {

			BigDecimal periodityValue = null;
			Integer coupons = null;
			BigDecimal daysTerm = BigDecimal.valueOf(securityDaysTerm);
			BigDecimal monthsTerm = BigDecimal.valueOf(securityMonthsTerm);

			if (InterestPeriodicityType.BY_DAYS.getCode().equals(periodicity)) {
				if (periodicityDays != null) {
					periodityValue = BigDecimal.valueOf(periodicityDays);
					if (periodityValue.compareTo(daysTerm) == 1) {
						String strMsg = PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_ERROR_PERIOD_DAYS_EXCEEDS);
						JSFUtilities.addContextMessage(idControl, FacesMessage.SEVERITY_ERROR, strMsg, strMsg);
						return null;
					}
					coupons = daysTerm.divide(periodityValue, RoundingMode.DOWN).intValue();
				}
			} else {
				periodityValue = BigDecimal.valueOf(InterestPeriodicityType.get(periodicity).getIndicator1());
				if (periodityValue.compareTo(monthsTerm) == 1) {
					String strMsg = PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_ERROR_PERIOD_EXEEDS_MONTHS_TERM);
					JSFUtilities.addContextMessage(idControl, FacesMessage.SEVERITY_ERROR, strMsg, strMsg);
					return null;
				}
				coupons = monthsTerm.divide(periodityValue, RoundingMode.DOWN).intValue();
			}

			return coupons;
		}
		return null;
	}

	/** Generate nro coupons.
	 * 
	 * @param periodicity the periodicity
	 * @param monthsTerm the months term
	 * @return the integer
	 * @throws Exception the exception */
	public Integer generateNroCoupons(Integer periodicity, Integer monthsTerm) throws Exception {
		if (CapitalPaymentModalityType.AT_MATURITY.getCode().equals(security.getCapitalPaymentModality()))
			return 1;
		Integer periodicityMonth = InterestPeriodicityType.get(periodicity).getIndicator1();
		Object[] argObj = new Object[2];
		if (periodicityMonth.intValue() > monthsTerm.intValue()) {
			argObj[0] = PropertiesUtilities.getMessage("security.lbl.interest.payment.periodicity");
			argObj[1] = PropertiesUtilities.getMessage("security.lbl.months.terms.value");

			String strMsg = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_LESS_THAN_OR_EQUAL_TO, argObj);
			JSFUtilities.addContextMessage("frmSecurityMgmt:tviewGeneral:dtbAmortizationCoupons", FacesMessage.SEVERITY_ERROR, strMsg, strMsg);
			return null;
		}
		// Validamos que el tiempo de vida del valor entre la periodicidad de un valor entero
		BigDecimal perMoth = new BigDecimal(periodicityMonth.toString());
		BigDecimal termMoth = new BigDecimal(monthsTerm.toString());
		BigDecimal nroCoupons = null;
		try {
			nroCoupons = termMoth.divide(perMoth);
		} catch (ArithmeticException e) {
			String strMsg = "La periocidad no se puede aplicar. Por favor verifique.";
			JSFUtilities.addContextMessage("frmSecurityMgmt:tviewGeneral:dtbAmortizationCoupons", FacesMessage.SEVERITY_ERROR, strMsg, strMsg);
			return null;
		}
		BigDecimal decimal = nroCoupons.remainder(BigDecimal.ONE);

		if (decimal.compareTo(BigDecimal.ZERO) == 1) {// si es mayor a 0
			String strMsg = "La periocidad no se puede aplicar. Por favor verifique.";
			JSFUtilities.addContextMessage("frmSecurityMgmt:tviewGeneral:dtbAmortizationCoupons", FacesMessage.SEVERITY_ERROR, strMsg, strMsg);
		} else
			return Integer.parseInt(nroCoupons.toString());
		return null;
	}

	/** End Restructuring. This Method is used in the view (web page)
	 * 
	 * @param date the date
	 * @param days the days
	 * @return the date
	 * @throws Exception the exception */

	public Date addDaysToDate(Date date, Integer days) throws Exception {
		return CommonsUtilities.addDaysToDate(date, days);
	}

	/** Clean cronos data. */
	public void cleanCronosData() {
		try {
			if (blLongTerm) {
				security.setSecurityMonthsTerm(null);
				security.setExpirationDate(null);
				security.setCouponFirstExpirationDate(null);
				security.setNumberCoupons(null);
				lstDtbProAmoCouponReqHis = new ArrayList<ProgramAmoCouponReqHi>();
				calculateNroCouponsAndFirstPayment();
			} else {
				CommonsUtilities.getMonthsBetween(security.getIssuanceDate(), security.getExpirationDate());
				calculateNroCouponsAndFirstPayment();
			}
		} catch (Exception e) {
		}
	}

	/** Validate term. */
	public void validateTerm() {
		try {
			if (Validations.validateIsNotNullAndNotEmpty(security.getSecurityMonthsTerm())) {
				Integer monthTerm = security.getSecurityMonthsTerm();
				if (monthTerm < 12) {
					security.setSecurityMonthsTerm(null);
					security.setExpirationDate(null);
				} else {
					BigDecimal month = new BigDecimal(monthTerm.toString());
					BigDecimal year = new BigDecimal("12");
					BigDecimal result = null;
					try {
						result = month.divide(year);
					} catch (ArithmeticException e) {
						security.setSecurityMonthsTerm(null);
						security.setExpirationDate(null);
						return;
					}
					BigDecimal decimal = result.remainder(BigDecimal.ONE);
					if (decimal.compareTo(BigDecimal.ZERO) == 1) {// si es mayor a 0
						security.setSecurityMonthsTerm(null);
						security.setExpirationDate(null);
						return;
					} else {
						if (Validations.validateIsNotNullAndNotEmpty(security.getIssuanceDate())) {
							Date expirationDate = CommonsUtilities.addMonthsToDate(security.getIssuanceDate(), monthTerm);
							if (expirationDate.after(security.getIssuance().getExpirationDate())) {
								security.setSecurityMonthsTerm(null);
								security.setExpirationDate(null);
							} else {
								security.setExpirationDate(expirationDate);
								getAmortizationFactor();
								calculateNroCouponsAndFirstPayment();
							}
						} else {
							security.setSecurityMonthsTerm(null);
						}
					}
				}
			}
		} catch (Exception e) {
		}
	}

	/** Genereta corporative event remote redirect link.
	 * 
	 * @param importanceEventId the importance event id
	 * @return the string
	 * @throws Exception the exception */
	public String generetaCorporativeEventRemoteRedirectLink(Long importanceEventId) throws Exception {

		StringBuilder urlFinal = new StringBuilder(serverPath);
		urlFinal.append(GeneralConstants.IMPORTANCE_EVENT_DETAIL_PAGE);

		if (urlFinal.toString().contains("?")) {
			urlFinal.append("&");
		} else {
			urlFinal.append("?");
		}

		urlFinal.append("tikectId=" + AESEncryptUtils.encrypt(AESKeyType.PRADERAKEY.getValue(), userInfo.getUserAccountSession().getTicketSession()));

		urlFinal.append("&" + GeneralConstants.RESET_BEAN_MENU).append("=").append(GeneralConstants.RESET_BEAN_MENU);
		urlFinal.append("&").append(GeneralConstants.IMPORTANCE_EVENT_QUERY_STRING).append("=");
		urlFinal.append(importanceEventId);

		return urlFinal.toString();
	}

	/** Calculate nro coupons and first payment. */
	public void calculateNroCouponsAndFirstPayment() {
		try {
			if (CapitalPaymentModalityType.AT_MATURITY.getCode().equals(security.getInterestPaymentModality())) {
				security.setNumberCoupons(1);
				security.setCouponFirstExpirationDate(security.getExpirationDate());
			} else {
				BigDecimal coupons = getNroCouponsForInterest();
				if (Validations.validateIsNotNullAndNotEmpty(coupons)) {
					if (Validations.validateIsNotNullAndNotEmpty(security.getIssuance()) && Validations.validateIsNotNullAndNotEmpty(security.getPeriodicity())) {
						security.setNumberCoupons(new Integer(coupons.toString()));
						Date expirationDate = CommonsUtilities.addMonthsToDate(security.getIssuanceDate(), InterestPeriodicityType.get(security.getPeriodicity())
								.getIndicator1());

						security.setCouponFirstExpirationDate(CommonsUtilities.workingDateCalculate(expirationDate, GeneralConstants.ONE_VALUE_INTEGER));
					} else {
						security.setNumberCoupons(null);
						security.setCouponFirstExpirationDate(null);
					}
				} else {
					security.setNumberCoupons(null);
					security.setCouponFirstExpirationDate(null);
				}
			}
		} catch (Exception e) {
		}
	}

	/** Bill parameters table map.
	 * 
	 * @param lstParameterTable the lst parameter table */
	public void billParametersTableMap(List<ParameterTable> lstParameterTable) {
		billParametersTableMap(lstParameterTable, false);
	}

	/** Bill parameters table map.
	 * 
	 * @param lstParameterTable the lst parameter table
	 * @param isObj the is obj */
	public void billParametersTableMap(List<ParameterTable> lstParameterTable, boolean isObj) {
		for (ParameterTable pTable : lstParameterTable) {
			if (isObj) {
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable);
			} else {
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable.getParameterName());
			}
		}
	}

	/** Bill parameter table by id.
	 * 
	 * @param parameterPk the parameter pk
	 * @throws ServiceException the service exception */
	public void billParameterTableById(Integer parameterPk) throws ServiceException {
		billParameterTableById(parameterPk, false);
	}

	/** Bill parameter table by id.
	 * 
	 * @param parameterPk the parameter pk
	 * @param isObj the is obj
	 * @throws ServiceException the service exception */
	public void billParameterTableById(Integer parameterPk, boolean isObj) throws ServiceException {
		ParameterTable pTable = generalParameterFacade.getParamDetailServiceFacade(parameterPk);
		if (pTable != null) {
			if (isObj) {
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable);
			} else {
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable.getParameterName());
			}
		}
	}

	/** Gets the nro coupons for amortization.
	 * 
	 * @return the nro coupons for amortization */
	private BigDecimal getNroCouponsForAmortization() {
		if (Validations.validateIsNotNullAndNotEmpty(security.getSecurityMonthsTerm()) && Validations.validateIsNotNullAndNotEmpty(security.getAmortizationPeriodicity())) {
			BigDecimal coupons = null;
			BigDecimal monthsTerm = new BigDecimal(security.getSecurityMonthsTerm());
			BigDecimal periodicity = new BigDecimal(InterestPeriodicityType.get(security.getAmortizationPeriodicity().intValue()).getIndicator1());
			try {
				coupons = monthsTerm.divide(periodicity);
			} catch (ArithmeticException e) {// Decimal periodo
				String strMsg = "La periocidad no se puede aplicar. Por favor verifique.";
				JSFUtilities.addContextMessage("frmSecurityMgmt:tviewGeneral:dtbAmortizationCoupons", FacesMessage.SEVERITY_ERROR, strMsg, strMsg);
				return null;
			}
			BigDecimal decimal = coupons.remainder(BigDecimal.ONE);// Se extrae el dato decimal
			if (decimal.compareTo(BigDecimal.ZERO) == 1) {// si es mayor a 0
				String strMsg = "La periocidad no se puede aplicar. Por favor verifique.";
				JSFUtilities.addContextMessage("frmSecurityMgmt:tviewGeneral:dtbAmortizationCoupons", FacesMessage.SEVERITY_ERROR, strMsg, strMsg);
				return null;
			}
			return coupons;
		}
		return null;
	}

	/** Gets the nro coupons for interest.
	 * 
	 * @return the nro coupons for interest */
	private BigDecimal getNroCouponsForInterest() {
		if (Validations.validateIsNotNullAndNotEmpty(security.getSecurityMonthsTerm()) && Validations.validateIsNotNullAndNotEmpty(security.getPeriodicity())) {
			BigDecimal coupons = null;
			BigDecimal monthsTerm = new BigDecimal(security.getSecurityMonthsTerm());
			BigDecimal periodicity = new BigDecimal(InterestPeriodicityType.get(security.getPeriodicity().intValue()).getIndicator1());
			try {
				coupons = monthsTerm.divide(periodicity);
			} catch (ArithmeticException e) {// Decimal periodo
				String strMsg = "La periocidad no se puede aplicar. Por favor verifique.";
				JSFUtilities.addContextMessage("frmSecurityMgmt:tviewGeneral:dtbInterestPaymentCron", FacesMessage.SEVERITY_ERROR, strMsg, strMsg);
				return null;
			}
			BigDecimal decimal = coupons.remainder(BigDecimal.ONE);// Se extrae el dato decimal
			if (decimal.compareTo(BigDecimal.ZERO) == 1) {// si es mayor a 0
				String strMsg = "La periocidad no se puede aplicar. Por favor verifique.";
				JSFUtilities.addContextMessage("frmSecurityMgmt:tviewGeneral:dtbInterestPaymentCron", FacesMessage.SEVERITY_ERROR, strMsg, strMsg);
				return null;
			}
			return coupons;
		}
		return null;
	}

	/** Gets the min begin date.
	 * 
	 * @return the min begin date */
	public Date getMinBeginDate() {
		Date minBeginDate = null;
		ProgramAmoCouponReqHi programAmoCouponReqHi = null;
		if (lstDtbProAmoCouponReqHis.isEmpty()) {
			if (security.getIssuanceDate() != null) {
				minBeginDate = CommonsUtilities.copyDate(security.getIssuanceDate());
			}
		} else {
			programAmoCouponReqHi = lstDtbProAmoCouponReqHis.get(lstDtbProAmoCouponReqHis.size() - 1);
			minBeginDate = programAmoCouponReqHi.getExpirationDate();
		}
		return minBeginDate;
	}

	/** Load generals combos.
	 * 
	 * @throws ServiceException the service exception */
	public void loadGeneralsCombos() throws ServiceException {
		loadCboIssuanceStateType();
		loadCboBooleanType();
		loadCboInterestType();
		loadCboPaymentModality();
		loadCboCalendarType();
		loadCboCalendarDays();
		loadCboRateType();
		loadCboOperationType();
		loadCboInterestPeriodicity();
		loadCboYield();
		loadCboCashNominal();
		loadCboAmortizationType();
		loadCboAmortizationOn();
		loadCboIndexedTo();
		loadCboInvestorType();
		loadCboFinancialIndex();
		loadDtbInternationalDepository();
		loadCboGeographicLocation();
		loadCboCurrency();
		loadCboIssuanceType();
		loadCboSecuritiesState();
		loadCboCreditRatingScales();
		loadCboInstrumentType();
		loadCboInterestPaymentModality();
		loadCboMonthType();
		loadCboFinancialPeriodicity();
		loadCboSecurityTerm();
	}

	/** Prints the security class.
	 * 
	 * @param securityClass the security class
	 * @return the string */
	public String printSecurityClass(Integer securityClass) {
		ParameterTable pTable = (ParameterTable) super.getParametersTableMap().get(securityClass);
		return pTable.getText1() + " - " + pTable.getParameterName();
	}

	/** Select checkbox int depository.
	 * 
	 * @throws Exception the exception */
	public void selectCheckboxIntDepository() throws Exception {
		for (SecurityForeignDepository sec : security.getSecurityForeignDepositories()) {
			for (InternationalDepository idep : lstDtbInternationalDepository) {
				if (idep.getIdInternationalDepositoryPk().equals(sec.getId().getIdInternationalDepositoryPk())) {
					idep.setSelected(Boolean.TRUE);
					break;
				}
			}
		}
	}

	/** Checks if is assigned sec neg mechanism.
	 * 
	 * @param mechModality the mech modality
	 * @return true, if is assigned sec neg mechanism */
	public boolean isAssignedSecNegMechanism(MechanismModality mechModality) {
		for (SecurityNegotiationMechanism secNeg : security.getSecurityNegotiationMechanisms()) {
			if (mechModality.getIdConverted().equals(secNeg.getMechanismModality().getIdConverted())) {
				return true;
			}
		}
		return false;
	}

	/** Load cbo cfi categories.
	 * 
	 * @throws ServiceException the service exception */
	public void loadCboCFICategories() throws ServiceException {
		lstCboCFICategories = securityServiceFacade.getCFICategoriesServiceFacade();
	}

	/** Load cbo cfi groups by category.
	 * 
	 * @throws ServiceException the service exception */

	/** Load cbo instrument type.
	 * 
	 * @throws ServiceException the service exception */
	private void loadCboInstrumentType() throws ServiceException {
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
		if (lstCboInstrumentType == null || lstCboInstrumentType.isEmpty()) {
			lstCboInstrumentType = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		}
	}

	/** Load cbo issuance state type.
	 * 
	 * @throws ServiceException the service exception */
	private void loadCboIssuanceStateType() throws ServiceException {
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.ISSUANCE_STATE.getCode());
		lstCboIssuanceState = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}

	/** Load cbo security term.
	 * 
	 * @throws ServiceException the service exception */
	public void loadCboSecurityTerm() throws ServiceException {
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.SECURITIE_TERM.getCode());
		lstCboSecurityTerm = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}

	public void loadCboSecurieClass() {
		try {
			parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			lstCboSecuritieClass = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
			billParametersTableMap(lstCboSecuritieClass, true);
		} catch (Exception e) {
		}
	}

	/** Load cbo geographic location.
	 * 
	 * @throws ServiceException the service exception */
	public void loadCboGeographicLocation() throws ServiceException {
		GeographicLocationTO geoLocation = new GeographicLocationTO();
		geoLocation.setGeographicLocationType(GeographicLocationType.COUNTRY.getCode());
		geoLocation.setState(GeographicLocationStateType.REGISTERED.getCode());
		lstCboGeographicLocation = generalParameterFacade.getListGeographicLocationServiceFacade(geoLocation);
	}

	/** Load cbo currency.
	 * 
	 * @throws ServiceException the service exception */
	public void loadCboCurrency() throws ServiceException {
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());

		lstCboCurrency = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		billParametersTableMap(lstCboCurrency);
	}

	/** Load cbo interest type.
	 * 
	 * @throws ServiceException the service exception */
	public void loadCboInterestType() throws ServiceException {
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.INTEREST_TYPE.getCode());

		lstCboInterestType = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}

	/** Load cbo issue type.
	 * 
	 * @throws ServiceException the service exception */
	private void loadCboIssuanceType() throws ServiceException {
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.ISSUANCE_TYPE.getCode());
		lstCboIssuanceType = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}

	/** Load cbo security block motive.
	 * 
	 * @throws ServiceException the service exception */
	public void loadCboSecurityBlockMotive() throws ServiceException {
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.SECURITY_BLOCK_MOTIVE.getCode());
		lstCboSecurityBlockMotive = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}

	/** Load cbo security unblock motive.
	 * 
	 * @throws ServiceException the service exception */
	public void loadCboSecurityUnblockMotive() throws ServiceException {
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.SECURITY_UNBLOCK_MOTIVE.getCode());
		lstCboSecurityUnblockMotive = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}

	/** Load cbo boolean type.
	 * 
	 * @throws ServiceException the service exception */
	public void loadCboBooleanType() throws ServiceException {
		lstCboBooleanType = BooleanType.list;
	}

	/** Load cbo rate type.
	 * 
	 * @throws ServiceException the service exception */
	public void loadCboRateType() throws ServiceException {
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.RATE_TYPE.getCode());
		lstCboRateType = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}

	/** Load cbo interest periodicity.
	 * 
	 * @throws ServiceException the service exception */
	public void loadCboInterestPeriodicity() throws ServiceException {
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.INTEREST_PERIODICITY.getCode());
		lstCboInterestPeriodicity = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}

	/** Load cbo payment modality.
	 * 
	 * @throws ServiceException the service exception */
	public void loadCboPaymentModality() throws ServiceException {
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.PAYMENT_MODALITY.getCode());
		lstCboPaymentModality = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}

	/** Load cbo operation type.
	 * 
	 * @throws ServiceException the service exception */
	public void loadCboOperationType() throws ServiceException {
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.OPERATION_TYPE.getCode());
		lstCboOperationType = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}

	/** Load cbo calendar type.
	 * 
	 * @throws ServiceException the service exception */
	public void loadCboCalendarType() throws ServiceException {
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.CALENDAR_TYPE.getCode());
		lstCboCalendarType = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}

	/** Load cbo calendar days.
	 * 
	 * @throws ServiceException the service exception */
	public void loadCboCalendarDays() throws ServiceException {
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.CALENDAR_DAYS.getCode());
		lstCboCalendarDays = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}

	/** Load cbo yield.
	 * 
	 * @throws ServiceException the service exception */
	public void loadCboYield() throws ServiceException {
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.SECURITY_YIELD_TYPE.getCode());
		lstCboYield = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}

	/** Load cbo cash nominal.
	 * 
	 * @throws ServiceException the service exception */
	public void loadCboCashNominal() throws ServiceException {
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.CASH_NOMINAL.getCode());
		lstCboCashNominal = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}

	/** Load cbo amortization type.
	 * 
	 * @throws ServiceException the service exception */
	public void loadCboAmortizationType() throws ServiceException {
		parameterTableTO = new ParameterTableTO();
		// parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk(MasterTableType.AMORTIZATION_TYPE.getCode());
		lstCboAmortizationType = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}

	/** Load cbo amortization on.
	 * 
	 * @throws ServiceException the service exception */
	public void loadCboAmortizationOn() throws ServiceException {
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.AMORTIZATION_ON.getCode());
		lstCboAmortizationOn = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}

	/** Load cbo indexed to.
	 * 
	 * @throws ServiceException the service exception */
	public void loadCboIndexedTo() throws ServiceException {
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		lstCboIndexedTo = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}

	/** Load cbo month type.
	 * 
	 * @throws ServiceException the service exception */
	public void loadCboMonthType() throws ServiceException {
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.MONTH_TYPE.getCode());
		lstCboCalendarMonth = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}

	/** Load cbo security type by instrument type.
	 * 
	 * @param instrument the instrument
	 * @throws ServiceException the service exception */
	private void loadCboSecurityTypeByInstrumentType(Integer instrument) throws ServiceException {
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.SECURITIES_TYPE.getCode());
		parameterTableTO.setIdRelatedParameterFk(instrument);
		lstCboSecuritieType = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		billParametersTableMap(lstCboSecuritieType);
	}

	/** Load cbo investor type.
	 * 
	 * @throws ServiceException the service exception */
	public void loadCboInvestorType() throws ServiceException {
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.INVESTOR_TYPE.getCode());
		lstCboInvestorType = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}

	/** Load cbo financial index.
	 * 
	 * @throws ServiceException the service exception */
	public void loadCboFinancialIndex() throws ServiceException {
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.FINANCIAL_INDEX.getCode());
		lstCboFinancialIndex = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}

	/** Load dtb international depository.
	 * 
	 * @throws ServiceException the service exception */
	public void loadDtbInternationalDepository() throws ServiceException {
		InternationalDepositoryTO filterInternationalDepository = new InternationalDepositoryTO();
		filterInternationalDepository.setStateIntDepository(Integer.valueOf(InternationalDepositoryStateType.ACTIVE.getCode()));
		lstDtbInternationalDepository = securityServiceFacade.getListInternationalDepositoryServiceFacade(filterInternationalDepository);
	}

	/** Load cbo securities state.
	 * 
	 * @throws ServiceException the service exception */
	public void loadCboSecuritiesState() throws ServiceException {
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.SECURITIES_STATE.getCode());
		lstCboSecurityState = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}

	/** Load cbo credit rating scales.
	 * 
	 * @throws ServiceException the service exception */
	public void loadCboCreditRatingScales() throws ServiceException {
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.CREDIT_RATING_SCALES_LONG_TERM.getCode());

		lstCboCreditRatingScales = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}

	/** Load cbo economic activity.
	 * 
	 * @param economicaActivity the economica activity
	 * @throws ServiceException the service exception */
	public void loadCboEconomicActivity(Integer economicaActivity) throws ServiceException {
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.ECONOMIC_ACTIVITY.getCode());
		parameterTableTO.setIdRelatedParameterFk(economicaActivity);
		lstCboEconomicActivity = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}

	/** Load cbo interest payment modality.
	 * 
	 * @throws ServiceException the service exception */
	public void loadCboInterestPaymentModality() throws ServiceException {
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.INTEREST_PAYMENT_MODALITY.getCode());
		lstCboInterestPaymentModality = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}

	/** Load cbo financial periodicity.
	 * 
	 * @throws ServiceException the service exception */
	public void loadCboFinancialPeriodicity() throws ServiceException {
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.FINANCIAL_PERIOD.getCode());
		lstCboFinancialPeriodicity = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}

	/** Load cbo request state.
	 * 
	 * @throws ServiceException the service exception */
	public void loadCboRequestState() throws ServiceException {
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.REQUEST_STATE.getCode());
		lstCboRequestState = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		billParametersTableMap(lstCboRequestState);
	}

	/** Load cbo reject motive.
	 * 
	 * @throws ServiceException the service exception */
	public void loadCboRejectMotive() throws ServiceException {
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.REST_MOD_PAYMENT_SCHEDULE_REJECTION_MOTIVE.getCode());
		parameterTableTO.setOrderbyParameterTableCd(BooleanType.YES.getCode());
		lstCboRejectMotive = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}

	/** Load dtb negotiation modality.
	 * 
	 * @param instrumentType the instrument type
	 * @throws ServiceException the service exception */
	public void loadDtbNegotiationModality(Integer instrumentType) throws ServiceException {
		lstDtbNegotiationModalities = securityServiceFacade.getListNegotiationModalitiesServiceFacade(NegotiationModalityStateType.ACTIVE.getCode(),
				MechanismModalityStateType.ACTIVE.getCode(), instrumentType);

		List<NegotiationMechanism> lstNegotiationMechanismData = securityServiceFacade.getListMechanismServiceFacade(NegotiationMechanismStateType.ACTIVE.getCode());
		createMechanismDynamicColumns(lstNegotiationMechanismData);

		for (NegotiationModality mod : lstDtbNegotiationModalities) {
			List<NegotiationMechanism> lstNegotiationMechanism = new ArrayList<NegotiationMechanism>();
			for (NegotiationMechanism negMechanismType : lstNegotiationMechanismData) {
				lstNegotiationMechanism.add(new NegotiationMechanism(negMechanismType.getIdNegotiationMechanismPk(), negMechanismType.getMechanismName(), false, true));
			}
			mod.setNegotiationMechanisms(lstNegotiationMechanism);

			for (NegotiationMechanism negMec : mod.getNegotiationMechanisms()) {
				for (MechanismModality mecModality : mod.getMechanismModalities()) {
					if (negMec.getIdNegotiationMechanismPk().equals(mecModality.getId().getIdNegotiationMechanismPk())) {
						negMec.setDisabled(false);
					}
				}
			}
		}

		StringBuilder sbIdConverted = new StringBuilder();
		for (NegotiationModality negModality : lstDtbNegotiationModalities) {
			for (NegotiationMechanism negMec : negModality.getNegotiationMechanisms()) {
				sbIdConverted.delete(0, sbIdConverted.length());
				sbIdConverted.append(negMec.getIdNegotiationMechanismPk()).append("-").append(negModality.getIdNegotiationModalityPk());
				if (isAssignedSecNegMechanism(sbIdConverted.toString())) {
					negMec.setSelected(true);
				}
			}
		}
	}

	/** Creates the mechanism dynamic columns.
	 * 
	 * @param lstNegotiationMechanism the lst negotiation mechanism */
	public void createMechanismDynamicColumns(List<NegotiationMechanism> lstNegotiationMechanism) {
		mechanismColumnsModel = new ArrayList<GenericColumnModel>();
		for (NegotiationMechanism negMec : lstNegotiationMechanism) {
			mechanismColumnsModel.add(new GenericColumnModel(negMec.getDescription(), null, "chk" + negMec.getMechanismName().trim()));
		}

	}

	/** Checks if is assigned sec neg mechanism.
	 * 
	 * @param idConverted the id converted
	 * @return true, if is assigned sec neg mechanism */
	public boolean isAssignedSecNegMechanism(String idConverted) {
		for (SecurityNegotiationMechanism secNeg : security.getSecurityNegotiationMechanisms()) {
			if (idConverted.equals(secNeg.getMechanismModality().getIdConverted())) {
				return true;
			}
		}
		return false;
	}

	/** Checks if is fixed instrument type.
	 * 
	 * @return true, if is fixed instrument type */
	public boolean isFixedInstrumentType() {
		if (security.getInstrumentType() != null && security.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode())) {
			return true;
		}
		return false;
	}

	/** Checks if is variable instrument type.
	 * 
	 * @return true, if is variable instrument type */
	public boolean isVariableInstrumentType() {
		if (security.getInstrumentType() != null && security.getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode())) {
			return true;
		}
		return false;
	}

	/** Checks if is variable interest type.
	 * 
	 * @return true, if is variable interest type */
	public boolean isVariableInterestType() {
		if (security.getInterestType() != null && security.getInterestType().equals(InterestType.VARIABLE.getCode())) {
			return true;
		}
		return false;
	}

	/** Checks if is fixed interest type.
	 * 
	 * @return true, if is fixed interest type */
	public boolean isFixedInterestType() {
		if (security.getInterestType() != null && security.getInterestType().equals(InterestType.FIXED.getCode())) {
			return true;
		}
		return false;
	}

	/** Checks if is mixed interest type.
	 * 
	 * @return true, if is mixed interest type */
	public boolean isMixedInterestType() {
		if (security.getInterestType() != null && security.getInterestType().equals(InterestType.MIXED.getCode())) {
			return true;
		}
		return false;
	}

	/** Checks if is proportional amortization type.
	 * 
	 * @return true, if is proportional amortization type */
	public boolean isProportionalAmortizationType() {
		if (security.getAmortizationType() != null && security.getAmortizationType().equals(AmortizationType.PROPORTIONAL.getCode())) {
			return true;
		}
		return false;
	}

	/** Checks if is interest payment schedule selected.
	 * 
	 * @return true, if is interest payment schedule selected */
	public boolean isInterestPaymentScheduleSelected() {
		if (paymentScheduleTypeSelected != null && paymentScheduleTypeSelected.equals(Integer.valueOf(1))) {
			return true;
		}
		return false;
	}

	/** Checks if is amortization payment schedule selected.
	 * 
	 * @return true, if is amortization payment schedule selected */
	public boolean isAmortizationPaymentScheduleSelected() {
		if (paymentScheduleTypeSelected != null && paymentScheduleTypeSelected.equals(Integer.valueOf(2))) {
			return true;
		}
		return false;
	}

	/** Checks if is amortization payment schedule.
	 * 
	 * @return true, if is amortization payment schedule */
	public boolean isAmortizationPaymentSchedule() {
		if (paymentScheduleTO.getPaymentScheduleType() != null && paymentScheduleTO.getPaymentScheduleType().equals(Integer.valueOf(2))) {
			return true;
		}
		return false;
	}

	/** Checks if is interest payment schedule.
	 * 
	 * @return true, if is interest payment schedule */
	public boolean isInterestPaymentSchedule() {
		if (paymentScheduleTO.getPaymentScheduleType() != null && paymentScheduleTO.getPaymentScheduleType().equals(Integer.valueOf(2))) {
			return true;
		}
		return false;
	}

	/** Gets the real initial date.
	 * 
	 * @return the real initial date */
	private Date getRealInitialDate() {
		boolean blOnePaid = false;
		Date realInitialDate = null;
		for (ProgramAmortizationCoupon proAmoCoupon : security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons()) {
			if (ProgramScheduleStateType.EXPIRED.getCode().equals(proAmoCoupon.getStateProgramAmortization())) {// Si hay un
																												// pagado debe
																												// capturar su
																												// fecha de
																												// expiracion
				blOnePaid = true;
				realInitialDate = proAmoCoupon.getExpirationDate();
			}
		}
		if (!blOnePaid)// Si no hay un pagado entonces debe ser la fecha de emision del valor
			realInitialDate = security.getIssuanceDate();
		return realInitialDate;
	}

	/** Gets the real initial amortization date.
	 * 
	 * @return the real initial amortization date */
	private Date getRealInitialAmortizationDate() {
		boolean blOnePaid = false;
		Date realInitialDate = null;
		for (ProgramAmortizationCoupon proAmoCoupon : security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons()) {
			if (ProgramScheduleStateType.EXPIRED.getCode().equals(proAmoCoupon.getStateProgramAmortization())) {// Si hay un
																												// pagado debe
																												// capturar su
																												// fecha de
																												// expiracion
				blOnePaid = true;
				realInitialDate = proAmoCoupon.getExpirationDate();
			}
		}
		if (!blOnePaid)// Si no hay un pagado entonces debe ser la fecha de emision del valor
			realInitialDate = security.getIssuanceDate();
		return realInitialDate;
	}

	/** Gets the real actual amortization percent.
	 * 
	 * @return the real actual amortization percent */
	public BigDecimal getRealActualAmortizationPercent() {
		BigDecimal actualPercent = GeneralConstants.HUNDRED_VALUE_BIGDECIMAL;
		for (ProgramAmortizationCoupon proAmoCoupon : security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons()) {
			if (ProgramScheduleStateType.EXPIRED.getCode().equals(proAmoCoupon.getStateProgramAmortization())) {
				actualPercent = actualPercent.subtract(proAmoCoupon.getAmortizationFactor());
			}
		}
		return actualPercent;
	}

	/** Gets the real percent paid.
	 * 
	 * @return the real percent paid */
	private BigDecimal getRealPercentPaid() {
		BigDecimal percentPaid = BigDecimal.ZERO;
		for (ProgramAmortizationCoupon proAmoCoupon : security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons()) {
			if (ProgramScheduleStateType.EXPIRED.getCode().equals(proAmoCoupon.getStateProgramAmortization())) {// Se suma los
																												// porcentajes
																												// pagados
				percentPaid = percentPaid.add(proAmoCoupon.getAmortizationFactor());
			}
		}
		return percentPaid;
	}

	/** Gets the security.
	 * 
	 * @return the security */
	public Security getSecurity() {
		return security;
	}

	/** Sets the security.
	 * 
	 * @param security the new security */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/** Gets the lst cbo cfi categories.
	 * 
	 * @return the lst cbo cfi categories */
	public List<CfiCategory> getLstCboCFICategories() {
		return lstCboCFICategories;
	}

	/** Sets the lst cbo cfi categories.
	 * 
	 * @param lstCboCFICategories the new lst cbo cfi categories */
	public void setLstCboCFICategories(List<CfiCategory> lstCboCFICategories) {
		this.lstCboCFICategories = lstCboCFICategories;
	}

	/** Gets the lst cbo cfi groups.
	 * 
	 * @return the lst cbo cfi groups */
	public List<CfiGroup> getLstCboCFIGroups() {
		return lstCboCFIGroups;
	}

	/** Sets the lst cbo cfi groups.
	 * 
	 * @param lstCboCFIGroups the new lst cbo cfi groups */
	public void setLstCboCFIGroups(List<CfiGroup> lstCboCFIGroups) {
		this.lstCboCFIGroups = lstCboCFIGroups;
	}

	/** Gets the lst cbo cfi attribute1.
	 * 
	 * @return the lst cbo cfi attribute1 */
	public List<CfiAttribute> getLstCboCFIAttribute1() {
		return lstCboCFIAttribute1;
	}

	/** Sets the lst cbo cfi attribute1.
	 * 
	 * @param lstCboCFIAttribute1 the new lst cbo cfi attribute1 */
	public void setLstCboCFIAttribute1(List<CfiAttribute> lstCboCFIAttribute1) {
		this.lstCboCFIAttribute1 = lstCboCFIAttribute1;
	}

	/** Gets the lst cbo cfi attribute2.
	 * 
	 * @return the lst cbo cfi attribute2 */
	public List<CfiAttribute> getLstCboCFIAttribute2() {
		return lstCboCFIAttribute2;
	}

	/** Sets the lst cbo cfi attribute2.
	 * 
	 * @param lstCboCFIAttribute2 the new lst cbo cfi attribute2 */
	public void setLstCboCFIAttribute2(List<CfiAttribute> lstCboCFIAttribute2) {
		this.lstCboCFIAttribute2 = lstCboCFIAttribute2;
	}

	/** Gets the lst cbo cfi attribute3.
	 * 
	 * @return the lst cbo cfi attribute3 */
	public List<CfiAttribute> getLstCboCFIAttribute3() {
		return lstCboCFIAttribute3;
	}

	/** Sets the lst cbo cfi attribute3.
	 * 
	 * @param lstCboCFIAttribute3 the new lst cbo cfi attribute3 */
	public void setLstCboCFIAttribute3(List<CfiAttribute> lstCboCFIAttribute3) {
		this.lstCboCFIAttribute3 = lstCboCFIAttribute3;
	}

	/** Gets the lst cbo cfi attribute4.
	 * 
	 * @return the lst cbo cfi attribute4 */
	public List<CfiAttribute> getLstCboCFIAttribute4() {
		return lstCboCFIAttribute4;
	}

	/** Sets the lst cbo cfi attribute4.
	 * 
	 * @param lstCboCFIAttribute4 the new lst cbo cfi attribute4 */
	public void setLstCboCFIAttribute4(List<CfiAttribute> lstCboCFIAttribute4) {
		this.lstCboCFIAttribute4 = lstCboCFIAttribute4;
	}

	/** Gets the lst cbo instrument type.
	 * 
	 * @return the lst cbo instrument type */
	public List<ParameterTable> getLstCboInstrumentType() {
		return lstCboInstrumentType;
	}

	/** Sets the lst cbo instrument type.
	 * 
	 * @param lstCboInstrumentType the new lst cbo instrument type */
	public void setLstCboInstrumentType(List<ParameterTable> lstCboInstrumentType) {
		this.lstCboInstrumentType = lstCboInstrumentType;
	}

	/** Gets the lst cbo securitie type.
	 * 
	 * @return the lst cbo securitie type */
	public List<ParameterTable> getLstCboSecuritieType() {
		return lstCboSecuritieType;
	}

	/** Sets the lst cbo securitie type.
	 * 
	 * @param lstCboSecuritieType the new lst cbo securitie type */
	public void setLstCboSecuritieType(List<ParameterTable> lstCboSecuritieType) {
		this.lstCboSecuritieType = lstCboSecuritieType;
	}

	/** Gets the lst cbo securitie class.
	 * 
	 * @return the lst cbo securitie class */
	public List<ParameterTable> getLstCboSecuritieClass() {
		return lstCboSecuritieClass;
	}

	/** Sets the lst cbo securitie class.
	 * 
	 * @param lstCboSecuritieClass the new lst cbo securitie class */
	public void setLstCboSecuritieClass(List<ParameterTable> lstCboSecuritieClass) {
		this.lstCboSecuritieClass = lstCboSecuritieClass;
	}

	/** Gets the lst cbo issuance state.
	 * 
	 * @return the lst cbo issuance state */
	public List<ParameterTable> getLstCboIssuanceState() {
		return lstCboIssuanceState;
	}

	/** Sets the lst cbo issuance state.
	 * 
	 * @param lstCboIssuanceState the new lst cbo issuance state */
	public void setLstCboIssuanceState(List<ParameterTable> lstCboIssuanceState) {
		this.lstCboIssuanceState = lstCboIssuanceState;
	}

	/** Gets the lst cbo geographic location.
	 * 
	 * @return the lst cbo geographic location */
	public List<GeographicLocation> getLstCboGeographicLocation() {
		return lstCboGeographicLocation;
	}

	/** Sets the lst cbo geographic location.
	 * 
	 * @param lstCboGeographicLocation the new lst cbo geographic location */
	public void setLstCboGeographicLocation(List<GeographicLocation> lstCboGeographicLocation) {
		this.lstCboGeographicLocation = lstCboGeographicLocation;
	}

	/** Gets the lst cbo currency.
	 * 
	 * @return the lst cbo currency */
	public List<ParameterTable> getLstCboCurrency() {
		return lstCboCurrency;
	}

	/** Sets the lst cbo currency.
	 * 
	 * @param lstCboCurrency the new lst cbo currency */
	public void setLstCboCurrency(List<ParameterTable> lstCboCurrency) {
		this.lstCboCurrency = lstCboCurrency;
	}

	/** Gets the lst cbo issuance type.
	 * 
	 * @return the lst cbo issuance type */
	public List<ParameterTable> getLstCboIssuanceType() {
		return lstCboIssuanceType;
	}

	/** Sets the lst cbo issuance type.
	 * 
	 * @param lstCboIssuanceType the new lst cbo issuance type */
	public void setLstCboIssuanceType(List<ParameterTable> lstCboIssuanceType) {
		this.lstCboIssuanceType = lstCboIssuanceType;
	}

	/** Gets the lst cbo security state.
	 * 
	 * @return the lst cbo security state */
	public List<ParameterTable> getLstCboSecurityState() {
		return lstCboSecurityState;
	}

	/** Sets the lst cbo security state.
	 * 
	 * @param lstCboSecurityState the new lst cbo security state */
	public void setLstCboSecurityState(List<ParameterTable> lstCboSecurityState) {
		this.lstCboSecurityState = lstCboSecurityState;
	}

	/** Gets the lst cbo boolean type.
	 * 
	 * @return the lst cbo boolean type */
	public List<BooleanType> getLstCboBooleanType() {
		return lstCboBooleanType;
	}

	/** Sets the lst cbo boolean type.
	 * 
	 * @param lstCboBooleanType the new lst cbo boolean type */
	public void setLstCboBooleanType(List<BooleanType> lstCboBooleanType) {
		this.lstCboBooleanType = lstCboBooleanType;
	}

	/** Gets the lst cbo interest type.
	 * 
	 * @return the lst cbo interest type */
	public List<ParameterTable> getLstCboInterestType() {
		return lstCboInterestType;
	}

	/** Sets the lst cbo interest type.
	 * 
	 * @param lstCboInterestType the new lst cbo interest type */
	public void setLstCboInterestType(List<ParameterTable> lstCboInterestType) {
		this.lstCboInterestType = lstCboInterestType;
	}

	/** Gets the lst cbo calendar type.
	 * 
	 * @return the lst cbo calendar type */
	public List<ParameterTable> getLstCboCalendarType() {
		return lstCboCalendarType;
	}

	/** Sets the lst cbo calendar type.
	 * 
	 * @param lstCboCalendarType the new lst cbo calendar type */
	public void setLstCboCalendarType(List<ParameterTable> lstCboCalendarType) {
		this.lstCboCalendarType = lstCboCalendarType;
	}

	/** Gets the lst cbo calendar days.
	 * 
	 * @return the lst cbo calendar days */
	public List<ParameterTable> getLstCboCalendarDays() {
		return lstCboCalendarDays;
	}

	/** Sets the lst cbo calendar days.
	 * 
	 * @param lstCboCalendarDays the new lst cbo calendar days */
	public void setLstCboCalendarDays(List<ParameterTable> lstCboCalendarDays) {
		this.lstCboCalendarDays = lstCboCalendarDays;
	}

	/** Gets the lst cbo payment modality.
	 * 
	 * @return the lst cbo payment modality */
	public List<ParameterTable> getLstCboPaymentModality() {
		return lstCboPaymentModality;
	}

	/** Sets the lst cbo payment modality.
	 * 
	 * @param lstCboPaymentModality the new lst cbo payment modality */
	public void setLstCboPaymentModality(List<ParameterTable> lstCboPaymentModality) {
		this.lstCboPaymentModality = lstCboPaymentModality;
	}

	/** Gets the lst cbo rate type.
	 * 
	 * @return the lst cbo rate type */
	public List<ParameterTable> getLstCboRateType() {
		return lstCboRateType;
	}

	/** Sets the lst cbo rate type.
	 * 
	 * @param lstCboRateType the new lst cbo rate type */
	public void setLstCboRateType(List<ParameterTable> lstCboRateType) {
		this.lstCboRateType = lstCboRateType;
	}

	/** Gets the lst cbo operation type.
	 * 
	 * @return the lst cbo operation type */
	public List<ParameterTable> getLstCboOperationType() {
		return lstCboOperationType;
	}

	/** Sets the lst cbo operation type.
	 * 
	 * @param lstCboOperationType the new lst cbo operation type */
	public void setLstCboOperationType(List<ParameterTable> lstCboOperationType) {
		this.lstCboOperationType = lstCboOperationType;
	}

	/** Gets the lst cbo interest periodicity.
	 * 
	 * @return the lst cbo interest periodicity */
	public List<ParameterTable> getLstCboInterestPeriodicity() {
		return lstCboInterestPeriodicity;
	}

	/** Sets the lst cbo interest periodicity.
	 * 
	 * @param lstCboInterestPeriodicity the new lst cbo interest periodicity */
	public void setLstCboInterestPeriodicity(List<ParameterTable> lstCboInterestPeriodicity) {
		this.lstCboInterestPeriodicity = lstCboInterestPeriodicity;
	}

	/** Gets the lst cbo yield.
	 * 
	 * @return the lst cbo yield */
	public List<ParameterTable> getLstCboYield() {
		return lstCboYield;
	}

	/** Sets the lst cbo yield.
	 * 
	 * @param lstCboYield the new lst cbo yield */
	public void setLstCboYield(List<ParameterTable> lstCboYield) {
		this.lstCboYield = lstCboYield;
	}

	/** Gets the lst cbo cash nominal.
	 * 
	 * @return the lst cbo cash nominal */
	public List<ParameterTable> getLstCboCashNominal() {
		return lstCboCashNominal;
	}

	/** Sets the lst cbo cash nominal.
	 * 
	 * @param lstCboCashNominal the new lst cbo cash nominal */
	public void setLstCboCashNominal(List<ParameterTable> lstCboCashNominal) {
		this.lstCboCashNominal = lstCboCashNominal;
	}

	/** Gets the lst cbo amortization type.
	 * 
	 * @return the lst cbo amortization type */
	public List<ParameterTable> getLstCboAmortizationType() {
		return lstCboAmortizationType;
	}

	/** Sets the lst cbo amortization type.
	 * 
	 * @param lstCboAmortizationType the new lst cbo amortization type */
	public void setLstCboAmortizationType(List<ParameterTable> lstCboAmortizationType) {
		this.lstCboAmortizationType = lstCboAmortizationType;
	}

	/** Gets the lst cbo amortization on.
	 * 
	 * @return the lst cbo amortization on */
	public List<ParameterTable> getLstCboAmortizationOn() {
		return lstCboAmortizationOn;
	}

	/** Sets the lst cbo amortization on.
	 * 
	 * @param lstCboAmortizationOn the new lst cbo amortization on */
	public void setLstCboAmortizationOn(List<ParameterTable> lstCboAmortizationOn) {
		this.lstCboAmortizationOn = lstCboAmortizationOn;
	}

	/** Gets the lst cbo indexed to.
	 * 
	 * @return the lst cbo indexed to */
	public List<ParameterTable> getLstCboIndexedTo() {
		return lstCboIndexedTo;
	}

	/** Sets the lst cbo indexed to.
	 * 
	 * @param lstCboIndexedTo the new lst cbo indexed to */
	public void setLstCboIndexedTo(List<ParameterTable> lstCboIndexedTo) {
		this.lstCboIndexedTo = lstCboIndexedTo;
	}

	/** Gets the lst cbo investor type.
	 * 
	 * @return the lst cbo investor type */
	public List<ParameterTable> getLstCboInvestorType() {
		return lstCboInvestorType;
	}

	/** Sets the lst cbo investor type.
	 * 
	 * @param lstCboInvestorType the new lst cbo investor type */
	public void setLstCboInvestorType(List<ParameterTable> lstCboInvestorType) {
		this.lstCboInvestorType = lstCboInvestorType;
	}

	/** Gets the lst cbo financial index.
	 * 
	 * @return the lst cbo financial index */
	public List<ParameterTable> getLstCboFinancialIndex() {
		return lstCboFinancialIndex;
	}

	/** Sets the lst cbo financial index.
	 * 
	 * @param lstCboFinancialIndex the new lst cbo financial index */
	public void setLstCboFinancialIndex(List<ParameterTable> lstCboFinancialIndex) {
		this.lstCboFinancialIndex = lstCboFinancialIndex;
	}

	/** Gets the lst cbo security block motive.
	 * 
	 * @return the lst cbo security block motive */
	public List<ParameterTable> getLstCboSecurityBlockMotive() {
		return lstCboSecurityBlockMotive;
	}

	/** Sets the lst cbo security block motive.
	 * 
	 * @param lstCboSecurityBlockMotive the new lst cbo security block motive */
	public void setLstCboSecurityBlockMotive(List<ParameterTable> lstCboSecurityBlockMotive) {
		this.lstCboSecurityBlockMotive = lstCboSecurityBlockMotive;
	}

	/** Gets the lst cbo security unblock motive.
	 * 
	 * @return the lst cbo security unblock motive */
	public List<ParameterTable> getLstCboSecurityUnblockMotive() {
		return lstCboSecurityUnblockMotive;
	}

	/** Sets the lst cbo security unblock motive.
	 * 
	 * @param lstCboSecurityUnblockMotive the new lst cbo security unblock motive */
	public void setLstCboSecurityUnblockMotive(List<ParameterTable> lstCboSecurityUnblockMotive) {
		this.lstCboSecurityUnblockMotive = lstCboSecurityUnblockMotive;
	}

	/** Gets the lst cbo security motive aux.
	 * 
	 * @return the lst cbo security motive aux */
	public List<ParameterTable> getLstCboSecurityMotiveAux() {
		return lstCboSecurityMotiveAux;
	}

	/** Sets the lst cbo security motive aux.
	 * 
	 * @param lstCboSecurityMotiveAux the new lst cbo security motive aux */
	public void setLstCboSecurityMotiveAux(List<ParameterTable> lstCboSecurityMotiveAux) {
		this.lstCboSecurityMotiveAux = lstCboSecurityMotiveAux;
	}

	/** Gets the lst cbo credit rating scales.
	 * 
	 * @return the lst cbo credit rating scales */
	public List<ParameterTable> getLstCboCreditRatingScales() {
		return lstCboCreditRatingScales;
	}

	/** Sets the lst cbo credit rating scales.
	 * 
	 * @param lstCboCreditRatingScales the new lst cbo credit rating scales */
	public void setLstCboCreditRatingScales(List<ParameterTable> lstCboCreditRatingScales) {
		this.lstCboCreditRatingScales = lstCboCreditRatingScales;
	}

	/** Gets the parameter table to.
	 * 
	 * @return the parameter table to */
	public ParameterTableTO getParameterTableTO() {
		return parameterTableTO;
	}

	/** Sets the parameter table to.
	 * 
	 * @param parameterTableTO the new parameter table to */
	public void setParameterTableTO(ParameterTableTO parameterTableTO) {
		this.parameterTableTO = parameterTableTO;
	}

	/** Gets the lst dtb international depository.
	 * 
	 * @return the lst dtb international depository */
	public List<InternationalDepository> getLstDtbInternationalDepository() {
		return lstDtbInternationalDepository;
	}

	/** Sets the lst dtb international depository.
	 * 
	 * @param lstDtbInternationalDepository the new lst dtb international depository */
	public void setLstDtbInternationalDepository(List<InternationalDepository> lstDtbInternationalDepository) {
		this.lstDtbInternationalDepository = lstDtbInternationalDepository;
	}

	/** Gets the lst dtb negotiation modalities.
	 * 
	 * @return the lst dtb negotiation modalities */
	public List<NegotiationModality> getLstDtbNegotiationModalities() {
		return lstDtbNegotiationModalities;
	}

	/** Sets the lst dtb negotiation modalities.
	 * 
	 * @param lstDtbNegotiationModalities the new lst dtb negotiation modalities */
	public void setLstDtbNegotiationModalities(List<NegotiationModality> lstDtbNegotiationModalities) {
		this.lstDtbNegotiationModalities = lstDtbNegotiationModalities;
	}

	/** Gets the issuance t omgmt.
	 * 
	 * @return the issuance t omgmt */
	public IssuanceTO getIssuanceTOmgmt() {
		return issuanceTOmgmt;
	}

	/** Sets the issuance t omgmt.
	 * 
	 * @param issuanceTOmgmt the new issuance t omgmt */
	public void setIssuanceTOmgmt(IssuanceTO issuanceTOmgmt) {
		this.issuanceTOmgmt = issuanceTOmgmt;
	}

	/** Gets the lst selected inversionists.
	 * 
	 * @return the lst selected inversionists */
	public List<String> getLstSelectedInversionists() {
		return lstSelectedInversionists;
	}

	/** Sets the lst selected inversionists.
	 * 
	 * @param lstSelectedInversionists the new lst selected inversionists */
	public void setLstSelectedInversionists(List<String> lstSelectedInversionists) {
		this.lstSelectedInversionists = lstSelectedInversionists;
	}

	/** Gets the issuance data model.
	 * 
	 * @return the issuance data model */
	public IssuanceDataModel getIssuanceDataModel() {
		return issuanceDataModel;
	}

	/** Sets the issuance data model.
	 * 
	 * @param issuanceDataModel the new issuance data model */
	public void setIssuanceDataModel(IssuanceDataModel issuanceDataModel) {
		this.issuanceDataModel = issuanceDataModel;
	}

	/** Gets the security data model.
	 * 
	 * @return the security data model */
	public SecurityDataModel getSecurityDataModel() {
		return securityDataModel;
	}

	/** Sets the security data model.
	 * 
	 * @param securityDataModel the new security data model */
	public void setSecurityDataModel(SecurityDataModel securityDataModel) {
		this.securityDataModel = securityDataModel;
	}

	/** Gets the int payment schedule req hi.
	 * 
	 * @return the int payment schedule req hi */
	public IntPaymentScheduleReqHi getIntPaymentScheduleReqHi() {
		return intPaymentScheduleReqHi;
	}

	/** Sets the int payment schedule req hi.
	 * 
	 * @param intPaymentScheduleReqHi the new int payment schedule req hi */
	public void setIntPaymentScheduleReqHi(IntPaymentScheduleReqHi intPaymentScheduleReqHi) {
		this.intPaymentScheduleReqHi = intPaymentScheduleReqHi;
	}

	/** Gets the amo payment schedule req hi.
	 * 
	 * @return the amo payment schedule req hi */
	public AmoPaymentScheduleReqHi getAmoPaymentScheduleReqHi() {
		return amoPaymentScheduleReqHi;
	}

	/** Sets the amo payment schedule req hi.
	 * 
	 * @param amoPaymentScheduleReqHi the new amo payment schedule req hi */
	public void setAmoPaymentScheduleReqHi(AmoPaymentScheduleReqHi amoPaymentScheduleReqHi) {
		this.amoPaymentScheduleReqHi = amoPaymentScheduleReqHi;
	}

	/** Checks if is view operation int pay she modification.
	 * 
	 * @return true, if is view operation int pay she modification */
	public boolean isViewOperationIntPaySheModification() {
		return viewOperationIntPaySheModification;
	}

	/** Sets the view operation int pay she modification.
	 * 
	 * @param viewOperationIntPaySheModification the new view operation int pay she modification */
	public void setViewOperationIntPaySheModification(boolean viewOperationIntPaySheModification) {
		this.viewOperationIntPaySheModification = viewOperationIntPaySheModification;
	}

	/** Checks if is view operation amo pay she modification.
	 * 
	 * @return true, if is view operation amo pay she modification */
	public boolean isViewOperationAmoPaySheModification() {
		return viewOperationAmoPaySheModification;
	}

	/** Sets the view operation amo pay she modification.
	 * 
	 * @param viewOperationAmoPaySheModification the new view operation amo pay she modification */
	public void setViewOperationAmoPaySheModification(boolean viewOperationAmoPaySheModification) {
		this.viewOperationAmoPaySheModification = viewOperationAmoPaySheModification;
	}

	/** Gets the payment schedule to.
	 * 
	 * @return the payment schedule to */
	public PaymentScheduleTO getPaymentScheduleTO() {
		return paymentScheduleTO;
	}

	/** Sets the payment schedule to.
	 * 
	 * @param paymentScheduleTO the new payment schedule to */
	public void setPaymentScheduleTO(PaymentScheduleTO paymentScheduleTO) {
		this.paymentScheduleTO = paymentScheduleTO;
	}

	/** Gets the payment schedule data model.
	 * 
	 * @return the payment schedule data model */
	public GenericDataModel<PaymentScheduleResultTO> getPaymentScheduleDataModel() {
		return paymentScheduleDataModel;
	}

	/** Sets the payment schedule result to data model.
	 * 
	 * @param paymentScheduleDataModel the new payment schedule result to data model */
	public void setPaymentScheduleResultTODataModel(GenericDataModel<PaymentScheduleResultTO> paymentScheduleDataModel) {
		this.paymentScheduleDataModel = paymentScheduleDataModel;
	}

	/** Gets the lst dtb pro int coupon req his.
	 * 
	 * @return the lst dtb pro int coupon req his */
	public List<ProgramIntCouponReqHi> getLstDtbProIntCouponReqHis() {
		return lstDtbProIntCouponReqHis;
	}

	/** Sets the lst dtb pro int coupon req his.
	 * 
	 * @param lstDtbProIntCouponReqHis the new lst dtb pro int coupon req his */
	public void setLstDtbProIntCouponReqHis(List<ProgramIntCouponReqHi> lstDtbProIntCouponReqHis) {
		this.lstDtbProIntCouponReqHis = lstDtbProIntCouponReqHis;
	}

	/** Gets the lst dtb pro amo coupon req his.
	 * 
	 * @return the lst dtb pro amo coupon req his */
	public List<ProgramAmoCouponReqHi> getLstDtbProAmoCouponReqHis() {
		return lstDtbProAmoCouponReqHis;
	}

	/** Sets the lst dtb pro amo coupon req his.
	 * 
	 * @param lstDtbProAmoCouponReqHis the new lst dtb pro amo coupon req his */
	public void setLstDtbProAmoCouponReqHis(List<ProgramAmoCouponReqHi> lstDtbProAmoCouponReqHis) {
		this.lstDtbProAmoCouponReqHis = lstDtbProAmoCouponReqHis;
	}

	/** Gets the payment schedule result to.
	 * 
	 * @return the payment schedule result to */
	public PaymentScheduleResultTO getPaymentScheduleResultTO() {
		return paymentScheduleResultTO;
	}

	/** Sets the payment schedule result to.
	 * 
	 * @param paymentScheduleResultTO the new payment schedule result to */
	public void setPaymentScheduleResultTO(PaymentScheduleResultTO paymentScheduleResultTO) {
		this.paymentScheduleResultTO = paymentScheduleResultTO;
	}

	/** Gets the security helper mgmt out.
	 * 
	 * @return the security helper mgmt out */
	public Security getSecurityHelperMgmtOut() {
		return securityHelperMgmtOut;
	}

	/** Sets the security helper mgmt out.
	 * 
	 * @param securityHelperMgmtOut the new security helper mgmt out */
	public void setSecurityHelperMgmtOut(Security securityHelperMgmtOut) {
		this.securityHelperMgmtOut = securityHelperMgmtOut;
	}

	/** Checks if is bl long term.
	 * 
	 * @return true, if is bl long term */
	public boolean isBlLongTerm() {
		return blLongTerm;
	}

	/** Sets the bl long term.
	 * 
	 * @param blLongTerm the new bl long term */
	public void setBlLongTerm(boolean blLongTerm) {
		this.blLongTerm = blLongTerm;
	}

	/** Checks if is bl short term.
	 * 
	 * @return true, if is bl short term */
	public boolean isBlShortTerm() {
		return blShortTerm;
	}

	/** Sets the bl short term.
	 * 
	 * @param blShortTerm the new bl short term */
	public void setBlShortTerm(boolean blShortTerm) {
		this.blShortTerm = blShortTerm;
	}

	/** Gets the lst cbo economic activity.
	 * 
	 * @return the lst cbo economic activity */
	public List<ParameterTable> getLstCboEconomicActivity() {
		return lstCboEconomicActivity;
	}

	/** Sets the lst cbo economic activity.
	 * 
	 * @param lstCboEconomicActivity the new lst cbo economic activity */
	public void setLstCboEconomicActivity(List<ParameterTable> lstCboEconomicActivity) {
		this.lstCboEconomicActivity = lstCboEconomicActivity;
	}

	/** Gets the lst cbo economic sector.
	 * 
	 * @return the lst cbo economic sector */
	public List<ParameterTable> getLstCboEconomicSector() {
		return lstCboEconomicSector;
	}

	/** Sets the lst cbo economic sector.
	 * 
	 * @param lstCboEconomicSector the new lst cbo economic sector */
	public void setLstCboEconomicSector(List<ParameterTable> lstCboEconomicSector) {
		this.lstCboEconomicSector = lstCboEconomicSector;
	}

	/** Checks if is payment schedule modification page.
	 * 
	 * @return true, if is payment schedule modification page */
	public boolean isPaymentScheduleModificationPage() {
		return paymentScheduleModificationPage;
	}

	/** Sets the payment schedule modification page.
	 * 
	 * @param paymentScheduleModificationPage the new payment schedule modification page */
	public void setPaymentScheduleModificationPage(boolean paymentScheduleModificationPage) {
		this.paymentScheduleModificationPage = paymentScheduleModificationPage;
	}

	/** Checks if is payment schedule restructuration page.
	 * 
	 * @return true, if is payment schedule restructuration page */
	public boolean isPaymentScheduleRestructurationPage() {
		return paymentScheduleRestructurationPage;
	}

	/** Sets the payment schedule restructuration page.
	 * 
	 * @param paymentScheduleRestructurationPage the new payment schedule restructuration page */
	public void setPaymentScheduleRestructurationPage(boolean paymentScheduleRestructurationPage) {
		this.paymentScheduleRestructurationPage = paymentScheduleRestructurationPage;
	}

	/** Gets the payment schedule type selected.
	 * 
	 * @return the payment schedule type selected */
	public Integer getPaymentScheduleTypeSelected() {
		return paymentScheduleTypeSelected;
	}

	/** Sets the payment schedule type selected.
	 * 
	 * @param paymentScheduleTypeSelected the new payment schedule type selected */
	public void setPaymentScheduleTypeSelected(Integer paymentScheduleTypeSelected) {
		this.paymentScheduleTypeSelected = paymentScheduleTypeSelected;
	}

	/** Gets the lst cbo request state.
	 * 
	 * @return the lst cbo request state */
	public List<ParameterTable> getLstCboRequestState() {
		return lstCboRequestState;
	}

	/** Sets the lst cbo request state.
	 * 
	 * @param lstCboRequestState the new lst cbo request state */
	public void setLstCboRequestState(List<ParameterTable> lstCboRequestState) {
		this.lstCboRequestState = lstCboRequestState;
	}

	/** Gets the program amortization coupon.
	 * 
	 * @return the program amortization coupon */
	public ProgramAmortizationCoupon getProgramAmortizationCoupon() {
		return programAmortizationCoupon;
	}

	/** Sets the program amortization coupon.
	 * 
	 * @param programAmortizationCoupon the new program amortization coupon */
	public void setProgramAmortizationCoupon(ProgramAmortizationCoupon programAmortizationCoupon) {
		this.programAmortizationCoupon = programAmortizationCoupon;
	}

	/** Gets the program amo coupon req hi.
	 * 
	 * @return the program amo coupon req hi */
	public ProgramAmoCouponReqHi getProgramAmoCouponReqHi() {
		return programAmoCouponReqHi;
	}

	/** Sets the program amo coupon req hi.
	 * 
	 * @param programAmoCouponReqHi the new program amo coupon req hi */
	public void setProgramAmoCouponReqHi(ProgramAmoCouponReqHi programAmoCouponReqHi) {
		this.programAmoCouponReqHi = programAmoCouponReqHi;
	}

	/** Gets the amortization total factor.
	 * 
	 * @return the amortization total factor */
	public BigDecimal getAmortizationTotalFactor() {
		return amortizationTotalFactor;
	}

	/** Sets the amortization total factor.
	 * 
	 * @param amortizationTotalFactor the new amortization total factor */
	public void setAmortizationTotalFactor(BigDecimal amortizationTotalFactor) {
		this.amortizationTotalFactor = amortizationTotalFactor;
	}

	/** Checks if is amortization payment entered.
	 * 
	 * @return true, if is amortization payment entered */
	public boolean isAmortizationPaymentEntered() {
		return amortizationPaymentEntered;
	}

	/** Sets the amortization payment entered.
	 * 
	 * @param amortizationPaymentEntered the new amortization payment entered */
	public void setAmortizationPaymentEntered(boolean amortizationPaymentEntered) {
		this.amortizationPaymentEntered = amortizationPaymentEntered;
	}

	/** Gets the lst cbo interest payment modality.
	 * 
	 * @return the lst cbo interest payment modality */
	public List<ParameterTable> getLstCboInterestPaymentModality() {
		return lstCboInterestPaymentModality;
	}

	/** Sets the lst cbo interest payment modality.
	 * 
	 * @param lstCboInterestPaymentModality the new lst cbo interest payment modality */
	public void setLstCboInterestPaymentModality(List<ParameterTable> lstCboInterestPaymentModality) {
		this.lstCboInterestPaymentModality = lstCboInterestPaymentModality;
	}

	/** Gets the lst cbo financial periodicity.
	 * 
	 * @return the lst cbo financial periodicity */
	public List<ParameterTable> getLstCboFinancialPeriodicity() {
		return lstCboFinancialPeriodicity;
	}

	/** Sets the lst cbo financial periodicity.
	 * 
	 * @param lstCboFinancialPeriodicity the new lst cbo financial periodicity */
	public void setLstCboFinancialPeriodicity(List<ParameterTable> lstCboFinancialPeriodicity) {
		this.lstCboFinancialPeriodicity = lstCboFinancialPeriodicity;
	}

	/** Gets the lst cbo calendar month.
	 * 
	 * @return the lst cbo calendar month */
	public List<ParameterTable> getLstCboCalendarMonth() {
		return lstCboCalendarMonth;
	}

	/** Sets the lst cbo calendar month.
	 * 
	 * @param lstCboCalendarMonth the new lst cbo calendar month */
	public void setLstCboCalendarMonth(List<ParameterTable> lstCboCalendarMonth) {
		this.lstCboCalendarMonth = lstCboCalendarMonth;
	}

	/* /** Gets the payment number amortization.
	 * 
	 * @return the payment number amortization */
	// public Integer getPaymentNumberAmortization() {
	// return paymentNumberAmortization;
	// }

	/** Sets the payment number amortization.
	 * 
	 * @return the _360_ calendar day */
	// public void setPaymentNumberAmortization(Integer paymentNumberAmortization) {
	// this.paymentNumberAmortization = paymentNumberAmortization;
	// }

	/** Gets the _360_ calendar day.
	 * 
	 * @return the _360_ calendar day */
	public Integer get_360_CalendarDay() {
		return _360_CalendarDay;
	}

	/** Sets the _360_ calendar day.
	 * 
	 * @param _360_CalendarDay the new _360_ calendar day */
	public void set_360_CalendarDay(Integer _360_CalendarDay) {
		this._360_CalendarDay = _360_CalendarDay;
	}

	/** Gets the index tab.
	 * 
	 * @return the index tab */
	public Integer getIndexTab() {
		return indexTab;
	}

	/** Sets the index tab.
	 * 
	 * @param indexTab the new index tab */
	public void setIndexTab(Integer indexTab) {
		this.indexTab = indexTab;
	}

	/** Gets the lst cbo security term.
	 * 
	 * @return the lst cbo security term */
	public List<ParameterTable> getLstCboSecurityTerm() {
		return lstCboSecurityTerm;
	}

	/** Sets the lst cbo security term.
	 * 
	 * @param lstCboSecurityTerm the new lst cbo security term */
	public void setLstCboSecurityTerm(List<ParameterTable> lstCboSecurityTerm) {
		this.lstCboSecurityTerm = lstCboSecurityTerm;
	}

	/** Gets the issuer helper search.
	 * 
	 * @return the issuer helper search */
	public Issuer getIssuerHelperSearch() {
		return issuerHelperSearch;
	}

	/** Sets the issuer helper search.
	 * 
	 * @param issuerHelperSearch the new issuer helper search */
	public void setIssuerHelperSearch(Issuer issuerHelperSearch) {
		this.issuerHelperSearch = issuerHelperSearch;
	}

	/** Gets the mechanism columns model.
	 * 
	 * @return the mechanism columns model */
	public List<GenericColumnModel> getMechanismColumnsModel() {
		return mechanismColumnsModel;
	}

	/** Sets the mechanism columns model.
	 * 
	 * @param mechanismColumnsModel the new mechanism columns model */
	public void setMechanismColumnsModel(List<GenericColumnModel> mechanismColumnsModel) {
		this.mechanismColumnsModel = mechanismColumnsModel;
	}

	/** Gets the lst cbo reject motive.
	 * 
	 * @return the lst cbo reject motive */
	public List<ParameterTable> getLstCboRejectMotive() {
		return lstCboRejectMotive;
	}

	/** Sets the lst cbo reject motive.
	 * 
	 * @param lstCboRejectMotive the new lst cbo reject motive */
	public void setLstCboRejectMotive(List<ParameterTable> lstCboRejectMotive) {
		this.lstCboRejectMotive = lstCboRejectMotive;
	}

	/** Gets the interes rate selected.
	 * 
	 * @return the interesRateSelected */
	public BigDecimal getInteresRateSelected() {
		return interesRateSelected;
	}

	/** Sets the interes rate selected.
	 * 
	 * @param interesRateSelected the interesRateSelected to set */
	public void setInteresRateSelected(BigDecimal interesRateSelected) {
		this.interesRateSelected = interesRateSelected;
	}

	/** Checks if is exchange rate.
	 * 
	 * @return true, if is exchange rate */
	public boolean isExchangeRate() {
		return isExchangeRate;
	}

	/** Sets the exchange rate.
	 * 
	 * @param isExchangeRate the new exchange rate */
	public void setExchangeRate(boolean isExchangeRate) {
		this.isExchangeRate = isExchangeRate;
	}

	public boolean isSelectAllCoupons() {
		return selectAllCoupons;
	}

	public void setSelectAllCoupons(boolean selectAllCoupons) {
		this.selectAllCoupons = selectAllCoupons;
	}

	public boolean isSelectAllCouponsInterest() {
		return selectAllCouponsInterest;
	}

	public void setSelectAllCouponsInterest(boolean selectAllCouponsInterest) {
		this.selectAllCouponsInterest = selectAllCouponsInterest;
	}

	public List<ProgramIntCouponReqHi> getLstDtbProIntCouponReqHisPreview() {
		return lstDtbProIntCouponReqHisPreview;
	}

	public void setLstDtbProIntCouponReqHisPreview(
			List<ProgramIntCouponReqHi> lstDtbProIntCouponReqHisPreview) {
		this.lstDtbProIntCouponReqHisPreview = lstDtbProIntCouponReqHisPreview;
	}

	public List<ProgramAmoCouponReqHi> getLstDtbProAmoCouponReqHisPreview() {
		return lstDtbProAmoCouponReqHisPreview;
	}

	public void setLstDtbProAmoCouponReqHisPreview(
			List<ProgramAmoCouponReqHi> lstDtbProAmoCouponReqHisPreview) {
		this.lstDtbProAmoCouponReqHisPreview = lstDtbProAmoCouponReqHisPreview;
	}
	
	public void insertInitialNominalValueHandler() {
		
	}
	
	
	public void selectIssuanceHandler() {
		
	}


}
