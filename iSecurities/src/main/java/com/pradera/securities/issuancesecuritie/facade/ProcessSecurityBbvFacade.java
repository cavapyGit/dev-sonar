package com.pradera.securities.issuancesecuritie.facade;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.jboss.ejb3.annotation.TransactionTimeout;
import org.w3c.dom.Document;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.SecuritySendBbv;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.securities.issuancesecuritie.service.ProcessSecurityBbvServiceBean;
import com.pradera.securities.issuancesecuritie.to.CuponTmpEdvTO;
import com.pradera.securities.issuancesecuritie.to.ResumeSecuritySendBbvTO;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class send security to bbv
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/06/2014
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ProcessSecurityBbvFacade {
	
	@Inject
    private PraderaLogger praderaLogger;
	
	@EJB
	private ProcessSecurityBbvServiceBean processSecurityBbvServiceBean;
	
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	public List<ResumeSecuritySendBbvTO> getListSecurityProcesed(Date fecha) throws ServiceException{
		
		List<ResumeSecuritySendBbvTO> listSecuritySendBbvTO = new ArrayList<>();
		
		ResumeSecuritySendBbvTO securitySendBbvTO;
		List<Integer> listNumberProcess = processSecurityBbvServiceBean.getListNumberProcess(fecha);
		for (Integer number :listNumberProcess){
			securitySendBbvTO = new ResumeSecuritySendBbvTO();
			
			securitySendBbvTO.setNumberProcess(number);
			
			List<SecuritySendBbv> listSecuritySended = processSecurityBbvServiceBean.getSecurityAvilableSended(fecha,null,number);
			securitySendBbvTO.setSended(listSecuritySended.size());
			
			List<SecuritySendBbv> listSecuritySend = processSecurityBbvServiceBean.getSecurityAvilableSended(fecha,"1",number);
			securitySendBbvTO.setRegistered(listSecuritySend.size());
			
			List<SecuritySendBbv> listSecuritySendError = processSecurityBbvServiceBean.getSecurityAvilableSended(fecha,"0",number);
			securitySendBbvTO.setErrorReception(listSecuritySendError.size());
			
			listSecuritySendBbvTO.add(securitySendBbvTO);
		}
		
		return listSecuritySendBbvTO;
	}
	
	@ProcessAuditLogger(process=BusinessProcessType.SECURITY_ISSUER_MANAGEMENT)
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=20)
	public Integer serachAndProcess(List<Security> listSecurity,Date fecha) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

		Integer numberProcess=processSecurityBbvServiceBean.getNumberProcess(fecha);
		
		SecuritySendBbv securitySendBbv;
		
		Long inicio = System.currentTimeMillis();
		
		for (Security security: listSecurity){
			
			securitySendBbv= new SecuritySendBbv();
			securitySendBbv.setNumberProcess(numberProcess);
			securitySendBbv.setRegistryDate(fecha);
			securitySendBbv.setSecurity(security.getIdSecurityCodePk());
			securitySendBbv.setSecurityEmition(security.getIssuanceDate());
			securitySendBbv.setSecurityExpiration(security.getExpirationDate());
			securitySendBbv.setNumberCuopon(security.getNumberCoupons());
			
			try {
				Object[] securitySearch = processSecurityBbvServiceBean.getSecurityToSend(security.getIdSecurityCodePk());
				List<Object[]> listCupon = processSecurityBbvServiceBean.getCuponToSend(security.getIdSecurityCodePk());
				processSecurityBbvServiceBean.sendSecuritiesToBBV(securitySearch, listCupon);
				securitySendBbv.setSendState("1");
				securitySendBbv.setSendDescription("Enviado");
				processSecurityBbvServiceBean.create(securitySendBbv);
				praderaLogger.debug("::::: VALOR enviado a la BBV :::: "+security);
			} catch (EJBException e) {
				securitySendBbv.setSendState("0");
				securitySendBbv.setSendDescription("Error de comunicacion con la BD");
				praderaLogger.error(e.getMessage());
				praderaLogger.info(":::: ERROR AL ENVIAR DPF A LA BBV ::::");	
				processSecurityBbvServiceBean.create(securitySendBbv);
			} catch (Exception e) {
				securitySendBbv.setSendState("0");
				securitySendBbv.setSendDescription("Error Desconocido");
				praderaLogger.error(e.getMessage());
				praderaLogger.info(":::: ERROR DESCONOCIDO ::::");	
				processSecurityBbvServiceBean.create(securitySendBbv);
			} 
		}
		Long fin = System.currentTimeMillis();
		praderaLogger.info("::: TIEMPO DE PROCESO BBV:::"+(fin-inicio));
		praderaLogger.info(":::: BBV ENVIO COMPLETADO :::: NUMERO PROCESO "+numberProcess);
		
//		processSecurityBbvServiceBean.saveListSecurity(listSecuritySendBb);
		return numberProcess;
	}
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=20)
	public void saveCuponsBBV(List<CuponTmpEdvTO> cuponTmpEdvs){
		for (CuponTmpEdvTO cuponTmpEdv : cuponTmpEdvs) 
			processSecurityBbvServiceBean.create(cuponTmpEdv);
	}
	public Object[] listSecurityAvailable(Date today) throws ServiceException{
		List<Security> listSecurityAvailable = new ArrayList<>();
		List<SecuritySendBbv> listSecurityRevertion= new ArrayList<>();
		
		List<Security> listSecuritySan = new ArrayList<>();
		List<SecuritySendBbv> listSecuritySended = new ArrayList<>();
		
		listSecuritySan = processSecurityBbvServiceBean.getSecurityAvilable(today);
		listSecuritySended = processSecurityBbvServiceBean.getSecurityAvilableSended(today,"1",null);
		
		Object[] result = new Object[2];
		
		boolean indicator;
		
		//valores disponibles
		if (listSecuritySan.size()>0){
			for (Security security :listSecuritySan){
				if (listSecuritySended.size()>0){
					
					indicator=false;
					for (SecuritySendBbv listSend :listSecuritySended){
						if (security.getIdSecurityCodePk().equals(listSend.getSecurity())){
							indicator=true;
						}
					}
					if (!indicator){
						listSecurityAvailable.add(security);
					}
				}else{
					listSecurityAvailable.add(security);
				}
			}
		}
		result[0]=listSecurityAvailable;
		
		//valores revertidos
		if (listSecuritySended.size()>0){
			for (SecuritySendBbv listSend :listSecuritySended){
				if (listSecuritySan.size()>0){
					indicator = false;
					for (Security security :listSecuritySan){
						if (security.getIdSecurityCodePk().equals(listSend.getSecurity())){
							indicator=true;
						}
						if (!indicator){
							listSecurityRevertion.add(listSend);
						}
					}
				}else{
					listSecurityRevertion.add(listSend);
				}
			}
		}else{
			result[1]=listSecurityRevertion;
		}
		
		return result;
	}

	public List<SecuritySendBbv> getListSecurityProcesedByNumber(Integer numberProcess,Date registryDate) throws ServiceException{
		
		List<SecuritySendBbv> listSecuritySended = processSecurityBbvServiceBean.getSecuritySended(numberProcess,registryDate);
		return listSecuritySended;
	}
	public byte[] getDocumentXmlSecurity(Integer pro,Date fechaCalendar) throws TransformerConfigurationException, ParseException{
		Document document=processSecurityBbvServiceBean.getDocumentXmlSecurity(pro,fechaCalendar);
		DOMSource source = new DOMSource(document);
		ByteArrayOutputStream bos=new ByteArrayOutputStream();
		StreamResult result=new StreamResult(bos);
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer;
		try {
			transformer = transformerFactory.newTransformer();
			transformer.transform(source, result);
		} catch (TransformerException e) {
			e.printStackTrace();
		}
		byte []array=bos.toByteArray();
		return array;
	}
	public byte[] getDocumentXmlCoupon(Integer pro,Date fechaCalendar) throws TransformerConfigurationException,ParseException{
		Document document=processSecurityBbvServiceBean.getDocumentXmlCoupon(pro,fechaCalendar);
		DOMSource source = new DOMSource(document);
		ByteArrayOutputStream bos=new ByteArrayOutputStream();
		StreamResult result=new StreamResult(bos);
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer;
		try {
			transformer = transformerFactory.newTransformer();
			transformer.transform(source, result);
		} catch (TransformerException e) {
			e.printStackTrace();
		}
		 byte []array=bos.toByteArray();
		 return array;
	}
}
