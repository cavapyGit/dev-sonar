package com.pradera.securities.issuancesecuritie.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.business.to.ExpirationSecurityTO;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.securities.issuancesecuritie.facade.SecurityServiceFacade;
import com.pradera.securities.issuancesecuritie.util.PropertiesConstants;

@DepositaryWebBean
public class ExpirationOverdueSecByIssuerMgmt extends GenericBaseBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private GeneralParametersFacade generalParametersFacade;
	
	@EJB
	private HelperComponentFacade helperComponentFacade;
	
	@EJB
	private SecurityServiceFacade securityServiceFacade;

	private Issuer issuerHelperSearch;
	
	private List<ParameterTable> lstCboCurrrency;
	
	private List<ParameterTable> lstCboSecuritieClass;
	
	private List<ExpirationSecurityTO> lstExpirationSecurityTO;
	
	private GenericDataModel<ExpirationSecurityTO> expirationSecurityDataModel;	
	
	private Security security;
	
	private ParameterTableTO parameterTableTO;
	
	private Holder holder;
	
	private SecurityTO securityTO;
	
	private HashMap<Integer, ParameterTable> mapParameters = new HashMap<Integer,ParameterTable>();
	
	private Date currentDate = getCurrentSystemDate();
	
	private static String INDICATOR_AVAILABLE = "DISPONIBLE";
	
	private static String INDICATOR_BAN_BALANCE = "EMBARGO";
	
	private static String INDICATOR_PAWN_BALANCE = "PRENDA";
	
	private static String INDICATOR_OTHER_BLOCK_BALANCE = "OTROS";
	
	@PostConstruct
	public void init(){
		try {
			super.setParametersTableMap(new HashMap<Integer,Object>());
			holder = new Holder();
			securityTO = new SecurityTO();
			parameterTableTO = new ParameterTableTO();
			security = new Security();
			expirationSecurityDataModel = new GenericDataModel<ExpirationSecurityTO>();
			lstCboSecuritieClass = new ArrayList<ParameterTable>();
			lstCboCurrrency = new ArrayList<ParameterTable>();
			issuerHelperSearch = new Issuer();
			
			securityTO.setExpirationDate(currentDate);
			loadCboFilterSecuritieClass();
			loadCboFilterCurrency();
			
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire( new ExceptionToCatchEvent(e) );
		}
	}
	
	public void loadCboFilterSecuritieClass() throws ServiceException {
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk( MasterTableType.SECURITIES_CLASS.getCode());
		lstCboSecuritieClass = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
	}
	
	public void loadCboFilterCurrency() throws ServiceException {
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		List<Integer> lstCurrency = new ArrayList<Integer>();
		lstCurrency.add(CurrencyType.PYG.getCode());
		lstCurrency.add(CurrencyType.USD.getCode());
		parameterTableTO.setLstParameterTablePk(lstCurrency);
		lstCboCurrrency = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
	}
	
	@LoggerAuditWeb
	public void searchExpirationSecurities() throws ServiceException{
		JSFUtilities.hideGeneralDialogues();
		try {
			if(Validations.validateIsNotNullAndNotEmpty(issuerHelperSearch)){
				securityTO.setIdIssuerCodePk(issuerHelperSearch.getIdIssuerPk());
			}
			if(Validations.validateIsNotNullAndNotEmpty(holder)){
				securityTO.setIdHolderPk(holder.getIdHolderPk());
			}
			List<ExpirationSecurityTO> lstExpirationSec = null;
			lstExpirationSec = securityServiceFacade.findSecForExpirationByIssuer(securityTO);
			this.expirationSecurityDataModel = new GenericDataModel(lstExpirationSec);
		} catch (ServiceException e) {
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)						
						, PropertiesUtilities.getExceptionMessage(e.getMessage(), e.getParams()));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	public void cleanExpirationSecurities(){
		securityTO = new SecurityTO();
		parameterTableTO = new ParameterTableTO();
		security = new Security();
		holder = new Holder();
		issuerHelperSearch = new Issuer();
		expirationSecurityDataModel = new GenericDataModel<ExpirationSecurityTO>();
		securityTO.setExpirationDate(currentDate);
	}
	
	public void beforeSave(){
		if(Validations.validateListIsNotNullAndNotEmpty(lstExpirationSecurityTO)){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_EXPIRATION_SECURITY_BY_ISSUER));			
			JSFUtilities.executeJavascriptFunction("cnfDlgSaveWidget.show();");
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),						
					PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_EXPIRATION_SECURITY_BY_ISSUER_NOT_FOUND));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
	}
	
	@LoggerAuditWeb
	public void saveExpirationSecurities() throws ServiceException{
		JSFUtilities.hideGeneralDialogues();
		try {
			settingIndicatorType(lstExpirationSecurityTO);
			securityServiceFacade.updateSecForExpirationByIssuer(lstExpirationSecurityTO);
			List<ExpirationSecurityTO> lstExpirationSec = securityServiceFacade.findSecForExpirationByIssuer(securityTO);
			this.expirationSecurityDataModel = new GenericDataModel(lstExpirationSec);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),						
					PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_EXPIRATION_SECURITY_BY_ISSUER_SUCESS_SAVE));
			JSFUtilities.showSimpleValidationDialog();
			return;
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	private void settingIndicatorType(
			List<ExpirationSecurityTO> lstExpirationSecurityTO2) {
		// TODO Auto-generated method stub
		for(ExpirationSecurityTO obj : lstExpirationSecurityTO){
			if(obj.getBalance().equals(INDICATOR_AVAILABLE)){
				obj.setIndicatorType(GeneralConstants.ONE_VALUE_INTEGER);
			}else if(obj.getBalance().equals(INDICATOR_BAN_BALANCE)){
				obj.setIndicatorType(GeneralConstants.TWO_VALUE_INTEGER);
			}else if(obj.getBalance().equals(INDICATOR_PAWN_BALANCE)){
				obj.setIndicatorType(GeneralConstants.THREE_VALUE_INTEGER);
			}else if(obj.getBalance().equals(INDICATOR_OTHER_BLOCK_BALANCE)){
				obj.setIndicatorType(GeneralConstants.FOUR_VALUE_INTEGER);
			}
		}
	}

	public String setDescriptionIndicator(Integer valor){
		String description = GeneralConstants.EMPTY_STRING; 
		if(valor.equals(GeneralConstants.ONE_VALUE_INTEGER)){
			description = "SI";
		}else{
			description = "NO";
		}
		return description; 
	}
	
	public boolean disabledSelection(Integer valor){
		boolean disabled = false; 
		if(valor.equals(GeneralConstants.ONE_VALUE_INTEGER)){
			disabled = true;
		}
		return disabled; 
	}
	
	/**
	 * Change filter field.
	 */
	public void changeFilterField(){
		lstExpirationSecurityTO = new ArrayList<ExpirationSecurityTO>();
		expirationSecurityDataModel = new GenericDataModel<ExpirationSecurityTO>();
	}
	

	/**
	 * @return the generalParametersFacade
	 */
	public GeneralParametersFacade getGeneralParametersFacade() {
		return generalParametersFacade;
	}

	/**
	 * @param generalParametersFacade the generalParametersFacade to set
	 */
	public void setGeneralParametersFacade(
			GeneralParametersFacade generalParametersFacade) {
		this.generalParametersFacade = generalParametersFacade;
	}

	/**
	 * @return the helperComponentFacade
	 */
	public HelperComponentFacade getHelperComponentFacade() {
		return helperComponentFacade;
	}

	/**
	 * @param helperComponentFacade the helperComponentFacade to set
	 */
	public void setHelperComponentFacade(HelperComponentFacade helperComponentFacade) {
		this.helperComponentFacade = helperComponentFacade;
	}

	/**
	 * @return the issuerHelperSearch
	 */
	public Issuer getIssuerHelperSearch() {
		return issuerHelperSearch;
	}

	/**
	 * @param issuerHelperSearch the issuerHelperSearch to set
	 */
	public void setIssuerHelperSearch(Issuer issuerHelperSearch) {
		this.issuerHelperSearch = issuerHelperSearch;
	}

	/**
	 * @return the lstCboCurrrency
	 */
	public List<ParameterTable> getLstCboCurrrency() {
		return lstCboCurrrency;
	}

	/**
	 * @param lstCboCurrrency the lstCboCurrrency to set
	 */
	public void setLstCboCurrrency(List<ParameterTable> lstCboCurrrency) {
		this.lstCboCurrrency = lstCboCurrrency;
	}

	/**
	 * @return the lstCboSecuritieClass
	 */
	public List<ParameterTable> getLstCboSecuritieClass() {
		return lstCboSecuritieClass;
	}

	/**
	 * @param lstCboSecuritieClass the lstCboSecuritieClass to set
	 */
	public void setLstCboSecuritieClass(List<ParameterTable> lstCboSecuritieClass) {
		this.lstCboSecuritieClass = lstCboSecuritieClass;
	}

	/**
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * @param security the security to set
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * @return the parameterTableTO
	 */
	public ParameterTableTO getParameterTableTO() {
		return parameterTableTO;
	}

	/**
	 * @param parameterTableTO the parameterTableTO to set
	 */
	public void setParameterTableTO(ParameterTableTO parameterTableTO) {
		this.parameterTableTO = parameterTableTO;
	}

	/**
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * @param holder the holder to set
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * @return the mapParameters
	 */
	public HashMap<Integer, ParameterTable> getMapParameters() {
		return mapParameters;
	}

	/**
	 * @param mapParameters the mapParameters to set
	 */
	public void setMapParameters(HashMap<Integer, ParameterTable> mapParameters) {
		this.mapParameters = mapParameters;
	}

	/**
	 * @return the currentDate
	 */
	public Date getCurrentDate() {
		return currentDate;
	}

	/**
	 * @param currentDate the currentDate to set
	 */
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}

	/**
	 * @return the expirationSecurityDataModel
	 */
	public GenericDataModel<ExpirationSecurityTO> getExpirationSecurityDataModel() {
		return expirationSecurityDataModel;
	}

	/**
	 * @param expirationSecurityDataModel the expirationSecurityDataModel to set
	 */
	public void setExpirationSecurityDataModel(
			GenericDataModel<ExpirationSecurityTO> expirationSecurityDataModel) {
		this.expirationSecurityDataModel = expirationSecurityDataModel;
	}

	/**
	 * @return the securityServiceFacade
	 */
	public SecurityServiceFacade getSecurityServiceFacade() {
		return securityServiceFacade;
	}

	/**
	 * @param securityServiceFacade the securityServiceFacade to set
	 */
	public void setSecurityServiceFacade(SecurityServiceFacade securityServiceFacade) {
		this.securityServiceFacade = securityServiceFacade;
	}

	/**
	 * @return the securityTO
	 */
	public SecurityTO getSecurityTO() {
		return securityTO;
	}

	/**
	 * @param securityTO the securityTO to set
	 */
	public void setSecurityTO(SecurityTO securityTO) {
		this.securityTO = securityTO;
	}

	/**
	 * @return the lstExpirationSecurityTO
	 */
	public List<ExpirationSecurityTO> getLstExpirationSecurityTO() {
		return lstExpirationSecurityTO;
	}

	/**
	 * @param lstExpirationSecurityTO the lstExpirationSecurityTO to set
	 */
	public void setLstExpirationSecurityTO(
			List<ExpirationSecurityTO> lstExpirationSecurityTO) {
		this.lstExpirationSecurityTO = lstExpirationSecurityTO;
	}
}