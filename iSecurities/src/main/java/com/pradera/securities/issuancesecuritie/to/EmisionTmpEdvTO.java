package com.pradera.securities.issuancesecuritie.to;

import java.math.BigDecimal;
import java.util.Date;

public class EmisionTmpEdvTO {

    private String emnSerie;
    private String emnCodinst;
    private String emnCodem;
    private String emnCodemision;
    private String emnMonedaGen;
    private String emnMoneda;
    private String emnMetodoValuacion;
    private Date emnFechaEmision;
    private String emnPlazo;
    private Date emnFechaVenci;
    private BigDecimal emnTrem;
    private String emnTasaTxt;
    private BigDecimal emnTdesc;
    private String emnPlazoAmortiza;
    private Integer emnNroCupones;
    private BigDecimal emnValorEmision;
    private BigDecimal emnMontoAjuste;
    private BigDecimal emnVn;
    private String emnAgenteRegistrador;
    private String emnTipoaccion;
    private String emnStatus;
    private Date emnFechaBv;
    private String emnDsc;
    private String emnDsm;
    private Short emnDiasredimidos;
    private String emnTiporegistroMesa;
    private Short emnTipogenericovalor;
    private String emnLcupNegociable;
    private String emnLsubproducto;
    private String emnEdv;
    private String emnLfciNeg;
    private String emnPrepago;
    private String emnLsubor;
    private String emnLtasafija;
    private String emnTipobono;
    private String emnSerieacc;
    private String emnLtasaDif;
    private BigDecimal emnTir;
    private String emnLhabilitado;
    private Date emnFeajusteBcb;
    private String emnEstado;
    private Date emnFechaEnvio;
    private String emnNegociable;
  

    public EmisionTmpEdvTO() {
    }

    public EmisionTmpEdvTO(String emnSerie) {
        this.emnSerie = emnSerie;
    }

    public String getEmnSerie() {
        return emnSerie;
    }

    public void setEmnSerie(String emnSerie) {
        this.emnSerie = emnSerie;
    }

    public String getEmnCodinst() {
        return emnCodinst;
    }

    public void setEmnCodinst(String emnCodinst) {
        this.emnCodinst = emnCodinst;
    }

    public String getEmnCodem() {
        return emnCodem;
    }

    public void setEmnCodem(String emnCodem) {
        this.emnCodem = emnCodem;
    }

    public String getEmnCodemision() {
        return emnCodemision;
    }

    public void setEmnCodemision(String emnCodemision) {
        this.emnCodemision = emnCodemision;
    }

    public String getEmnMonedaGen() {
        return emnMonedaGen;
    }

    public void setEmnMonedaGen(String emnMonedaGen) {
        this.emnMonedaGen = emnMonedaGen;
    }

    public String getEmnMoneda() {
        return emnMoneda;
    }

    public void setEmnMoneda(String emnMoneda) {
        this.emnMoneda = emnMoneda;
    }

    public String getEmnMetodoValuacion() {
        return emnMetodoValuacion;
    }

    public void setEmnMetodoValuacion(String emnMetodoValuacion) {
        this.emnMetodoValuacion = emnMetodoValuacion;
    }

    public Date getEmnFechaEmision() {
        return emnFechaEmision;
    }

    public void setEmnFechaEmision(Date emnFechaEmision) {
        this.emnFechaEmision = emnFechaEmision;
    }

    public String getEmnPlazo() {
        return emnPlazo;
    }

    public void setEmnPlazo(String emnPlazo) {
        this.emnPlazo = emnPlazo;
    }

    public Date getEmnFechaVenci() {
        return emnFechaVenci;
    }

    public void setEmnFechaVenci(Date emnFechaVenci) {
        this.emnFechaVenci = emnFechaVenci;
    }

    public BigDecimal getEmnTrem() {
        return emnTrem;
    }

    public void setEmnTrem(BigDecimal emnTrem) {
        this.emnTrem = emnTrem;
    }

    public String getEmnTasaTxt() {
        return emnTasaTxt;
    }

    public void setEmnTasaTxt(String emnTasaTxt) {
        this.emnTasaTxt = emnTasaTxt;
    }

    public BigDecimal getEmnTdesc() {
        return emnTdesc;
    }

    public void setEmnTdesc(BigDecimal emnTdesc) {
        this.emnTdesc = emnTdesc;
    }

    public String getEmnPlazoAmortiza() {
        return emnPlazoAmortiza;
    }

    public void setEmnPlazoAmortiza(String emnPlazoAmortiza) {
        this.emnPlazoAmortiza = emnPlazoAmortiza;
    }

    public Integer getEmnNroCupones() {
        return emnNroCupones;
    }

    public void setEmnNroCupones(Integer emnNroCupones) {
        this.emnNroCupones = emnNroCupones;
    }

    public BigDecimal getEmnValorEmision() {
        return emnValorEmision;
    }

    public void setEmnValorEmision(BigDecimal emnValorEmision) {
        this.emnValorEmision = emnValorEmision;
    }

    public BigDecimal getEmnMontoAjuste() {
        return emnMontoAjuste;
    }

    public void setEmnMontoAjuste(BigDecimal emnMontoAjuste) {
        this.emnMontoAjuste = emnMontoAjuste;
    }

    public BigDecimal getEmnVn() {
        return emnVn;
    }

    public void setEmnVn(BigDecimal emnVn) {
        this.emnVn = emnVn;
    }

    public String getEmnAgenteRegistrador() {
        return emnAgenteRegistrador;
    }

    public void setEmnAgenteRegistrador(String emnAgenteRegistrador) {
        this.emnAgenteRegistrador = emnAgenteRegistrador;
    }

    public String getEmnTipoaccion() {
        return emnTipoaccion;
    }

    public void setEmnTipoaccion(String emnTipoaccion) {
        this.emnTipoaccion = emnTipoaccion;
    }

    public String getEmnStatus() {
        return emnStatus;
    }

    public void setEmnStatus(String emnStatus) {
        this.emnStatus = emnStatus;
    }

    public Date getEmnFechaBv() {
        return emnFechaBv;
    }

    public void setEmnFechaBv(Date emnFechaBv) {
        this.emnFechaBv = emnFechaBv;
    }

    public String getEmnDsc() {
        return emnDsc;
    }

    public void setEmnDsc(String emnDsc) {
        this.emnDsc = emnDsc;
    }

    public String getEmnDsm() {
        return emnDsm;
    }

    public void setEmnDsm(String emnDsm) {
        this.emnDsm = emnDsm;
    }

    public Short getEmnDiasredimidos() {
        return emnDiasredimidos;
    }

    public void setEmnDiasredimidos(Short emnDiasredimidos) {
        this.emnDiasredimidos = emnDiasredimidos;
    }

    public String getEmnTiporegistroMesa() {
        return emnTiporegistroMesa;
    }

    public void setEmnTiporegistroMesa(String emnTiporegistroMesa) {
        this.emnTiporegistroMesa = emnTiporegistroMesa;
    }

    public Short getEmnTipogenericovalor() {
        return emnTipogenericovalor;
    }

    public void setEmnTipogenericovalor(Short emnTipogenericovalor) {
        this.emnTipogenericovalor = emnTipogenericovalor;
    }

    public String getEmnLcupNegociable() {
        return emnLcupNegociable;
    }

    public void setEmnLcupNegociable(String emnLcupNegociable) {
        this.emnLcupNegociable = emnLcupNegociable;
    }

    public String getEmnLsubproducto() {
        return emnLsubproducto;
    }

    public void setEmnLsubproducto(String emnLsubproducto) {
        this.emnLsubproducto = emnLsubproducto;
    }

    public String getEmnEdv() {
        return emnEdv;
    }

    public void setEmnEdv(String emnEdv) {
        this.emnEdv = emnEdv;
    }

    public String getEmnLfciNeg() {
        return emnLfciNeg;
    }

    public void setEmnLfciNeg(String emnLfciNeg) {
        this.emnLfciNeg = emnLfciNeg;
    }

    public String getEmnPrepago() {
        return emnPrepago;
    }

    public void setEmnPrepago(String emnPrepago) {
        this.emnPrepago = emnPrepago;
    }

    public String getEmnLsubor() {
        return emnLsubor;
    }

    public void setEmnLsubor(String emnLsubor) {
        this.emnLsubor = emnLsubor;
    }

    public String getEmnLtasafija() {
        return emnLtasafija;
    }

    public void setEmnLtasafija(String emnLtasafija) {
        this.emnLtasafija = emnLtasafija;
    }

    public String getEmnTipobono() {
        return emnTipobono;
    }

    public void setEmnTipobono(String emnTipobono) {
        this.emnTipobono = emnTipobono;
    }

    public String getEmnSerieacc() {
        return emnSerieacc;
    }

    public void setEmnSerieacc(String emnSerieacc) {
        this.emnSerieacc = emnSerieacc;
    }

    public String getEmnLtasaDif() {
        return emnLtasaDif;
    }

    public void setEmnLtasaDif(String emnLtasaDif) {
        this.emnLtasaDif = emnLtasaDif;
    }

    public BigDecimal getEmnTir() {
        return emnTir;
    }

    public void setEmnTir(BigDecimal emnTir) {
        this.emnTir = emnTir;
    }

    public String getEmnLhabilitado() {
        return emnLhabilitado;
    }

    public void setEmnLhabilitado(String emnLhabilitado) {
        this.emnLhabilitado = emnLhabilitado;
    }

    public Date getEmnFeajusteBcb() {
        return emnFeajusteBcb;
    }

    public void setEmnFeajusteBcb(Date emnFeajusteBcb) {
        this.emnFeajusteBcb = emnFeajusteBcb;
    }

    public String getEmnEstado() {
        return emnEstado;
    }

    public void setEmnEstado(String emnEstado) {
        this.emnEstado = emnEstado;
    }

    public Date getEmnFechaEnvio() {
        return emnFechaEnvio;
    }

    public void setEmnFechaEnvio(Date emnFechaEnvio) {
        this.emnFechaEnvio = emnFechaEnvio;
    }

    public String getEmnNegociable() {
        return emnNegociable;
    }

    public void setEmnNegociable(String emnNegociable) {
        this.emnNegociable = emnNegociable;
    }
}
