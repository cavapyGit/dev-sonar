package com.pradera.securities.issuancesecuritie.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ExceDataTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Date expirationDate;
	private BigDecimal amortizationFactor;
	private BigDecimal amortization;
	private BigDecimal interestRate;
	private BigDecimal interest;
	private Date initialDate;
	private Date paymentDate;

	private Integer nroCoupon;

	/** class constructor */
	public ExceDataTO() {
	}

	// TODO methos getter and setter
	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Integer getNroCoupon() {
		return nroCoupon;
	}

	public void setNroCoupon(Integer nroCoupon) {
		this.nroCoupon = nroCoupon;
	}

	public BigDecimal getAmortizationFactor() {
		return amortizationFactor;
	}

	public void setAmortizationFactor(BigDecimal amortizationFactor) {
		this.amortizationFactor = amortizationFactor;
	}

	public BigDecimal getAmortization() {
		return amortization;
	}

	public void setAmortization(BigDecimal amortization) {
		this.amortization = amortization;
	}

	public BigDecimal getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}

	public BigDecimal getInterest() {
		return interest;
	}

	public void setInterest(BigDecimal interest) {
		this.interest = interest;
	}
	

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	@Override
	public String toString() {
		return "ExceDataTO [expirationDate=" + expirationDate + ", amortizationFactor=" + amortizationFactor
				+ ", amortization=" + amortization + ", interestRate=" + interestRate + ", interest=" + interest
				+ ", initialDate=" + initialDate + ", paymentDate=" + paymentDate + ", nroCoupon=" + nroCoupon + "]";
	}

}
