/*
 * 
 */
package com.pradera.securities.issuancesecuritie.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.jboss.ejb3.annotation.TransactionTimeout;
import org.jfree.util.Log;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.to.NegotiationMechanismTO;
import com.pradera.core.component.corporateevents.service.CorporateEventComponentServiceBean;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.CorrelativeServiceBean;
import com.pradera.core.component.helperui.service.SecuritiesQueryServiceBean;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.VaultControlComponent;
import com.pradera.model.custody.payroll.PayrollDetail;
import com.pradera.model.custody.payroll.PayrollHeader;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationCupon;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.transfersecurities.DailySecurityClassBalance;
import com.pradera.model.custody.transfersecurities.TotalSecurityClassBalance;
import com.pradera.model.issuancesecuritie.AmortizationPaymentSchedule;
import com.pradera.model.issuancesecuritie.InterestPaymentSchedule;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.IssuanceAux;
import com.pradera.model.issuancesecuritie.IssuanceCertificate;
import com.pradera.model.issuancesecuritie.IssuanceCertificateAux;
import com.pradera.model.issuancesecuritie.IssuanceFile;
import com.pradera.model.issuancesecuritie.IssuanceFileAux;
import com.pradera.model.issuancesecuritie.IssuanceObservation;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.securities.issuancesecuritie.service.IssuanceServiceBean;
import com.pradera.securities.issuancesecuritie.service.PaymentCronogramServiceBean;
import com.pradera.securities.issuancesecuritie.service.SecurityServiceBean;
import com.pradera.securities.issuancesecuritie.to.IssuanceTO;
import com.pradera.securities.issuer.service.IssuerServiceBean;
import com.pradera.securities.issuer.to.IssuerTO;
import com.pradera.securities.issuer.to.PhysicalCertificateTO;
import com.pradera.securities.query.to.IssuanceBalanceMovDetailResultTO;
import com.pradera.securities.query.to.IssuanceBalanceMovResultTO;
import com.pradera.securities.query.to.IssuanceBalanceMovTO;

// TODO: Auto-generated Javadoc
/**
 * Clase de Negocio IssuanceSecuritiesServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class IssuanceSecuritiesServiceFacade {

	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	/** The crud dao service bean. */
	@EJB
	CrudDaoServiceBean crudDaoServiceBean;
	
	/** The correlative service bean. */
	@EJB
	CorrelativeServiceBean correlativeServiceBean;
	
	/** The payment cronogram service bean. */
	@EJB
	PaymentCronogramServiceBean paymentCronogramServiceBean;
	
	/** The issuer service bean. */
	@EJB
	IssuerServiceBean issuerServiceBean;
	
	/** The issuance service bean. */
	@EJB
	IssuanceServiceBean issuanceServiceBean;
	
	/** The security service bean. */
	@EJB
	SecurityServiceBean securityServiceBean;
	
	/** The coporate event component service bean. */
	@EJB
	private CorporateEventComponentServiceBean coporateEventComponentServiceBean;
	
	/** The securities query service bean. */
	@EJB 
	SecuritiesQueryServiceBean securitiesQueryServiceBean;
	
	
	public PayrollDetail update(PayrollDetail entity) {
		return update(entity);
	}
	public AccountAnnotationCupon update(AccountAnnotationCupon entity) {
		return update(entity);
	}
	public AccountAnnotationOperation update(AccountAnnotationOperation entity) {
		return update(entity);
	}
	public Security update(Security entity) {
		return update(entity);
	}
	
	public List<PhysicalCertificate> registerNewPhysicalCertificateWithAccountAnnotationOperation(AccountAnnotationOperation accountAnnotationOperation) throws ServiceException {
		return issuanceServiceBean.registerNewPhysicalCertificateWithAccountAnnotationOperation(accountAnnotationOperation);
	}
	
	public List<PayrollDetail> findListPayrollDetailWithAnnotationOperation(Long idAnnotationOperationPk) throws ServiceException{
		return issuanceServiceBean.findListPayrollDetailWithAnnotationOperation(idAnnotationOperationPk);
	}
	public List<Issuance> findIssuanceWithAccountAnnotation(AccountAnnotationOperation accountAnnotation) throws ServiceException {
		return issuanceServiceBean.findIssuanceWithAccountAnnotation(accountAnnotation);
	}
	public PayrollHeader findPayrollHeader(Long idPayrollHeaderPk) throws ServiceException{
		return issuanceServiceBean.findPayrollHeader(idPayrollHeaderPk);
	}
	public PayrollDetail findPayrollDetailPendingCreate(Long idPayrollDetailPk) throws ServiceException{
		return issuanceServiceBean.findPayrollDetailPendingCreate(idPayrollDetailPk);
	}
	public Security findSecurityWithAccountAnnotation(AccountAnnotationOperation accountAnnotation) throws ServiceException {
		return issuanceServiceBean.findSecurityWithAccountAnnotation(accountAnnotation);
	}
	public Security registerNewSecurityWithAccountAnnotationOperation(AccountAnnotationOperation accountAnnotationOperation,LoggerUser loggerUser) throws ServiceException {
		return issuanceServiceBean.registerNewSecurityWithAccountAnnotationOperation(accountAnnotationOperation,loggerUser);
	}
	public Issuance registerNewIssuanceWithAccountAnnotationOperation(AccountAnnotationOperation accountAnnotationOperation,LoggerUser loggerUser) throws ServiceException {
		return issuanceServiceBean.registerNewIssuanceWithAccountAnnotationOperation(accountAnnotationOperation,loggerUser);
	}
	
	
	/**
	 * Metodo para el registro de una emision.
	 *
	 * @param issuance the issuance
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.ISSUANCE_DATA_REGISTER)
	public boolean registryIssuanceServiceFacade(Issuance issuance) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());       
        return issuanceServiceBean.registryIssuanceServiceBean(issuance, loggerUser);
	}
	
	/**
	 * Metodo para el registro de una emision aux
	 *
	 * @param issuance the issuance
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.ISSUANCE_DATA_REGISTER)
	public boolean registryIssuanceAuxServiceFacade(IssuanceAux issuance) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());       
        return issuanceServiceBean.registryIssuanceAuxServiceBean(issuance, loggerUser);
	}
	
	/**
	 * 	Metodo para guardar los totales diarios por moneda y clase.
	 *
	 * @param lstDailySecurity the lst daily security
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean saveDailySecClass(List<Object[]> lstDailySecurity, Date sendDate) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());  
		List<DailySecurityClassBalance> lstDailySecClass=new ArrayList<>();
		if(Validations.validateIsNotNullAndNotEmpty(lstDailySecurity)){
			for(Object[] dailySec : lstDailySecurity){
				DailySecurityClassBalance dailySecClassBalance=new DailySecurityClassBalance();
				dailySecClassBalance.setProcessDate(sendDate);
				dailySecClassBalance.setNominalAmount(new BigDecimal(dailySec[0].toString()));
				dailySecClassBalance.setStockQuantity(new BigDecimal(dailySec[1].toString()));
				dailySecClassBalance.setCurrency(Integer.valueOf(dailySec[2].toString()));
				dailySecClassBalance.setSecurityClass(Integer.valueOf(dailySec[3].toString()));
				lstDailySecClass.add(dailySecClassBalance);
			}
		}
        return issuanceServiceBean.saveDailySecClass(lstDailySecClass, loggerUser);
	}
	
	/**
	 * Metodo para guardar los totales Diarios acumulados de los valores agrupados por moneda y clase
	 *
	 * @param lstDailySecurity the lst daily security
	 * @param lstTotalSecurity the lst total security
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean saveTotalSecClass(List<Object[]> lstDailySecurity,List<TotalSecurityClassBalance> lstTotalSecurity, Date sendDate) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());  
		List<TotalSecurityClassBalance> lstTotalSecClass=new ArrayList<>();
		Map<String, Object> mapTSCB = new HashMap<String, Object>();
		//LLENAMOS LOS TOTALES DEL DIA ANTERIOR EN UN MAP 
		for(TotalSecurityClassBalance tscb:lstTotalSecurity ){
			String dia=CommonsUtilities.convertDateToString(sendDate,"DD");
			String mes=CommonsUtilities.convertDateToString(sendDate,"MM");
			//SI ES PRIMER DIA DEL ANIO VUELVES LAS SUMATORIAS DEL ANIO A CERO
			if(dia.equals("01")&& mes.equals("01")){
				tscb.setYearTotalAmount(new BigDecimal(0));
				tscb.setYearTotalBalance(new BigDecimal(0));
			}
			mapTSCB.put(tscb.getCurrency().toString().concat(tscb.getSecurityClass().toString()),tscb);
		}
		//VALIDAMOS LISTA
		if(Validations.validateIsNotNullAndNotEmpty(lstDailySecurity)){
			for(Object[] dailySec : lstDailySecurity){
				String clave=dailySec[2].toString().concat(dailySec[3].toString());
				TotalSecurityClassBalance tscb=new TotalSecurityClassBalance();
				if(mapTSCB.containsKey(clave)){//Si ya existe la Moneda-Clase
					tscb=(TotalSecurityClassBalance)mapTSCB.get(clave);
					BigDecimal totalAmount =new BigDecimal(0);
					BigDecimal totalBalance =new BigDecimal(0);
					BigDecimal totalAmountYear =new BigDecimal(0);
					BigDecimal totalBalanceYear =new BigDecimal(0);
					
					totalAmount=CommonsUtilities.addTwoDecimal(tscb.getTotalAmount(), new BigDecimal(dailySec[0].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
					totalBalance=CommonsUtilities.addTwoDecimal(tscb.getTotalBalance(), new BigDecimal(dailySec[1].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
					totalAmountYear=CommonsUtilities.addTwoDecimal(tscb.getYearTotalAmount(), new BigDecimal(dailySec[0].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
					totalBalanceYear=CommonsUtilities.addTwoDecimal(tscb.getYearTotalBalance(), new BigDecimal(dailySec[1].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
										
					tscb.setTotalAmount(totalAmount);
					tscb.setTotalBalance(totalBalance);
					tscb.setYearTotalAmount(totalAmountYear);
					tscb.setYearTotalBalance(totalBalanceYear);
					tscb.setProcessDate(sendDate);
					
					mapTSCB.put(clave, tscb);
				}else{
					tscb.setCurrency(Integer.parseInt(dailySec[2].toString()));
					tscb.setSecurityClass(Integer.parseInt(dailySec[3].toString()));
					tscb.setTotalAmount(new BigDecimal(dailySec[0].toString()));
					tscb.setTotalBalance(new BigDecimal(dailySec[1].toString()));
					tscb.setYearTotalAmount(new BigDecimal(dailySec[0].toString()));
					tscb.setYearTotalBalance(new BigDecimal(dailySec[1].toString()));
					tscb.setProcessDate(sendDate);
					mapTSCB.put(tscb.getCurrency().toString().concat(tscb.getSecurityClass().toString()),tscb);
				}
			}
		}
		Iterator it = mapTSCB.keySet().iterator();
		while(it.hasNext()){
			String key =(String) it.next();
			lstTotalSecClass.add((TotalSecurityClassBalance)mapTSCB.get(key));
		}
        return issuanceServiceBean.saveTotalSecClass(lstTotalSecClass, sendDate, loggerUser);
	}
	
	/**
	 * save issuance observations maker checker
	 */
	public boolean saveIssuanceObservations(IssuanceObservation obs) {
		return issuanceServiceBean.saveIssuanceObservations(obs);
	}
	
	/**
	 * Delete Last Process Daily Security Class 
	 * @param date
	 */
	public void deleteProcessDailySecClass(Date date){
		
		issuanceServiceBean.deleteProcessDailySecClass(date);
		
	}
	
	/**
	 * Delete Last Process Total Security Class 
	 * 
	 * @param date
	 */
	public void deleteProcessTotalSecClass(Date date){
		
		issuanceServiceBean.deleteProcessTotalSecClass(date);
		
	}
	/**
	 * Metodo para actualizar la emision.
	 *
	 * @param issuance the issuance
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.ISSUANCE_DATA_MODIFICATION)
	public boolean updateIssuanceServiceFacade(Issuance issuance) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);		
		Integer priv = loggerUser.getUserAction().getIdPrivilegeModify() ==null ? Integer.valueOf(1) : loggerUser.getUserAction().getIdPrivilegeModify();
		loggerUser.setIdPrivilegeOfSystem(priv);		
        crudDaoServiceBean.update(issuance);
		return true;
	}	
	
	@ProcessAuditLogger (process = BusinessProcessType.ISSUANCE_DATA_MODIFICATION)
	public boolean updateIssuanceServiceFacade(IssuanceAux issuance) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);		
		Integer priv = loggerUser.getUserAction().getIdPrivilegeModify() ==null ? Integer.valueOf(1) : loggerUser.getUserAction().getIdPrivilegeModify();
		loggerUser.setIdPrivilegeOfSystem(priv);		
        crudDaoServiceBean.update(issuance);
		return true;
	}
	/**
	 * Metodo para Buscar un emisor.
	 *
	 * @param issuerTO the issuer to
	 * @return the issuer
	 * @throws ServiceException the service exception
	 */
	public Issuer findIssuerServiceFacade(IssuerTO issuerTO) throws ServiceException{
		return issuerServiceBean.findIssuerServiceBean(issuerTO);
	}
	
	/**
	 * Metodo para Buscar una lista de emisiones.
	 *
	 * @param issuanceTO the issuance to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Issuance> findIssuancesServiceFacade(IssuanceTO issuanceTO) throws ServiceException{
		return issuanceServiceBean.findIssuancesServiceBean(issuanceTO);
	}
	public List<IssuanceAux> findIssuancesAuxServiceFacade(IssuanceTO issuanceTO) throws ServiceException{
		return issuanceServiceBean.findIssuancesAuxServiceBean(issuanceTO);
	}
	/**
	 * Metodo para Buscar un emision.
	 *
	 * @param issuanceTO the issuance to
	 * @return the issuance
	 * @throws ServiceException the service exception
	 */
	public Issuance findIssuanceServiceFacade(IssuanceTO issuanceTO) throws ServiceException{
		return issuanceServiceBean.findIssuanceServiceBean(issuanceTO);
	}
	
	public IssuanceAux findIssuanceAuxServiceFacade(IssuanceTO issuanceTO) throws ServiceException{
		return issuanceServiceBean.findIssuanceAuxServiceBean(issuanceTO);
	}
	
	public List<IssuanceObservation> findIssuanceObsServiceFacade (String idIssuanceCodePk) throws ServiceException {
		return issuanceServiceBean.findIssuanceObsService(idIssuanceCodePk);
	}
	
	public List<PhysicalCertificateTO> searchPhysicalCerticate(String to) throws ServiceException{
		
		return issuanceServiceBean.searchPhysicalCerticate(to);
	}
	
	/**
	 * Metodo para Buscar una lista de certificados de emision
	 *
	 * @param idIssuanceCodePkPrm the id issuance code pk prm
	 * @param issuanceCertificateState the issuance certificate state
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<IssuanceCertificate> findIssuanceCertificatesServiceFacade(String idIssuanceCodePkPrm,Integer issuanceCertificateState) throws ServiceException{
		List<IssuanceCertificate> lstIssuanceCertificates = issuanceServiceBean.findIssuanceCertificatesServiceBean(idIssuanceCodePkPrm,issuanceCertificateState);
		for(IssuanceCertificate issuanceCert : lstIssuanceCertificates){
			issuanceCert.getCertificateFile();
		}
		return lstIssuanceCertificates;
	}
	
	/**
	 * Metodo para Buscar una lista de archivos de emision
	 *
	 * @param idIssuanceCodePkPrm the id issuance code pk prm
	 * @param issuanceCertificateState the issuance certificate state
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<IssuanceFile> findIssuanceFilesServiceFacade(String idIssuanceCodePkPrm,Integer issuanceCertificateState) throws ServiceException{
		List<IssuanceFile> lstIssuanceFiles = issuanceServiceBean.findIssuanceFilesServiceBean(idIssuanceCodePkPrm,issuanceCertificateState);
		if (Validations.validateListIsNotNullAndNotEmpty(lstIssuanceFiles)) {
			for(IssuanceFile objiIssuanceFile : lstIssuanceFiles){
				objiIssuanceFile.getDocumentFile();
			}
		}
		return lstIssuanceFiles;
	}
	public List<IssuanceFileAux> findIssuanceFilesAuxServiceFacade(String idIssuanceCodePkPrm,Integer issuanceCertificateState) throws ServiceException{
		List<IssuanceFileAux> lstIssuanceFiles = issuanceServiceBean.findIssuanceFilesAuxServiceBean(idIssuanceCodePkPrm,issuanceCertificateState);
		if (Validations.validateListIsNotNullAndNotEmpty(lstIssuanceFiles)) {
			for(IssuanceFileAux objiIssuanceFile : lstIssuanceFiles){
				objiIssuanceFile.getDocumentFile();
			}
		}
		return lstIssuanceFiles;
	}
	
	/**
	 * Metodo para obtener el numero de emisiones por idIssuerPk
	 *
	 * @param idIssuerPk the id issuer pk
	 * @return the issuance sequence code service bean
	 * @throws ServiceException the service exception
	 */
	public String getIssuanceSequenceCodeServiceBean(String idIssuerPk) throws ServiceException{
		return issuanceServiceBean.getIssuanceSequenceCodeServiceBean(idIssuerPk);
	}
	
	/**
	 * Metodo para obtener el numero de emisiones por descripcion de la emision
	 *
	 * @param issuance the issuance
	 * @return true, if successful
	 */
	public boolean validateIssuanceDescriptionServiceFacade(Issuance issuance){
		return issuanceServiceBean.validateIssuanceDescriptionServiceBean(issuance) >0;
	}
	
	/**
	 * Metodo para obtener una lista de negociaciones por mecanismo
	 *
	 * @param filter the filter
	 * @return the list negotiation mechanism service facade
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationMechanism> getListNegotiationMechanismServiceFacade(NegotiationMechanismTO filter) throws ServiceException {
    	return securityServiceBean.getListNegotiationMechanismServiceBean(filter);
	}
	
	/**
	 * Gets the securities count service facade.
	 *
	 * @param idIssuerPkPrm the id issuer pk prm
	 * @return the securities count service facade
	 * @throws ServiceException the service exception
	 */
	public int getSecuritiesCountServiceFacade(String idIssuerPkPrm) throws ServiceException{
		return securityServiceBean.getSecuritiesCountServiceBean(idIssuerPkPrm);
	}
	
	/**
	 * Metodo para eliminar un Program Interest Coupon por Parent Service.
	 *
	 * @param intPaySchedule the int pay schedule
	 * @throws ServiceException the service exception
	 */
	public void deleteProgramInterestCouponByParentServiceFacade(InterestPaymentSchedule intPaySchedule) throws ServiceException{
		paymentCronogramServiceBean.deleteProgramInterestCouponByParentServiceBean(intPaySchedule,null);
	}
	
	/**
	 * Metodo para eliminar un Program Amortization Coupon por Parent Service.
	 *
	 * @param amoPaySchedule the amo pay schedule
	 * @throws ServiceException the service exception
	 */
	public void deleteProgramAmortizationCouponByParentServiceFacade(AmortizationPaymentSchedule amoPaySchedule) throws ServiceException{
		paymentCronogramServiceBean.deleteProgramAmortizationCouponByParentServiceBean(amoPaySchedule,null);
	}
	
	/**
	 * Metodo para obtener el una Valor por Codigo Isin.
	 *
	 * @param isinCode the isin code
	 * @return the security from isin code
	 * @throws ServiceException the service exception
	 */
	public Security getSecurityFromIsinCode(String isinCode) throws ServiceException{
		return securitiesQueryServiceBean.getSecurityHelpServiceBean(isinCode);
	}
	
	/**
	 * Metodo para Obtener issuance balance movement.
	 *
	 * @param issuanceBalanceMovTO the issuance balance mov to
	 * @return the issuance balance movement
	 * @throws ServiceException the service exception
	 */
	public List<IssuanceBalanceMovResultTO> getIssuanceBalanceMovement(IssuanceBalanceMovTO issuanceBalanceMovTO) throws ServiceException{
		return issuanceServiceBean.getIssuanceBalanceMovement(issuanceBalanceMovTO);
	}
	
	/**
	 * Metodo para Obtener los detalles issuance balance movement.
	 *
	 * @param issuanceBalanceMovTO the issuance balance mov to
	 * @return the issuance balance mov det
	 * @throws ServiceException the service exception
	 */
	public List<IssuanceBalanceMovDetailResultTO> getIssuanceBalanceMovDet(IssuanceBalanceMovTO issuanceBalanceMovTO, List<IssuanceBalanceMovResultTO> issuanceBalMovSessionList) throws ServiceException{
		return issuanceServiceBean.getIssuanceBalanceMovDetService(issuanceBalanceMovTO, issuanceBalMovSessionList);
	}
	
	/***
	 * 
	 * @param sendDate
	 * @param sendDateTotal
	 */
	@TransactionTimeout(unit=TimeUnit.HOURS,value=3)
	@Performance
	public void calculateSecuritiesByDate(Date sendDate, Date sendDateTotal){
		

		List <Object[]> lstDailySecurity;
		List<TotalSecurityClassBalance> lstTotalSecurity;
		
		try {
			Log.info("::::::::::::: CALCULAMOS LOS MONTOS DIARIOS DE VALORES DESMATERIALIZADOS DEL DIA:::::::::::::");
			lstDailySecurity=securityServiceBean.calculateDesmaterialized(sendDate);
			Log.info("::::::::::::: ELIMINAMOS LOS DATOS DEL ANTERIOR PORCESO GENERADO DE DAILY_SECURITY_CLASS_BALANCE :::::::::::::");
			deleteProcessDailySecClass(sendDate);
			Log.info("::::::::::::: GUARDAMOS LOS DATOS EN LA TABLA DAILY_SECURITY_CLASS_BALANCE :::::::::::::");
			saveDailySecClass(lstDailySecurity,sendDate);
			Log.info("::::::::::::: OBTENEMOS LOS MONTOS TOTALES DEL DIA ANTERIOR:::::::::::::");
			lstTotalSecurity=securityServiceBean.searchAmountsPrevius(sendDateTotal);
			Log.info("::::::::::::: ELIMINAMOS LOS DATOS DEL ANTERIOR PORCESO GENERADO DE TOTAL_SECURITY_CLASS_BALANCE :::::::::::::");
			deleteProcessTotalSecClass(sendDate);
			Log.info("::::::::::::: GUARDAMOS LOS DATOS EN LA TABLA TOTAL_SECURITY_CLASS_BALANCE :::::::::::::");
			saveTotalSecClass(lstDailySecurity,lstTotalSecurity,sendDate);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * UPDATE ISSUANCE_AUX STATE */
	public void updateIssuanceAuxState(String idIssuanceCodePk, Integer state){
		issuanceServiceBean.updateIssuanceAuxState(idIssuanceCodePk, state);
	}
	
	/**
	 * Method Update last Date The Parameter Top
	 * @param dateSend
	 * @param parameterDateTop
	 */
	public void updateParameterTop(Date dateSend, Integer parameterDateTop){
		issuanceServiceBean.updateParameterTop(dateSend,parameterDateTop);
	}
	
	/**
	 * Method Get Issuer Details
	 * @param idSecurityCode
	 * @return
	 */
	public Object[] getIssuerDetails(String idSecurityCode){
		return issuanceServiceBean.getIssuerDetails(idSecurityCode);
	}
	
	/**
	 * 
	 * @param idSecurityCode
	 * @return
	 */
	public BigDecimal getSumAmountSecBcb(String isIssuancePk){
		return issuanceServiceBean.getSumAmountSecBcb(isIssuancePk);
	}
	
	/**
	 * Metodo para el registro de una emision.
	 *
	 * @param issuance the issuance
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.ISSUANCE_DATA_REGISTER)
	public boolean confirmIssuance(IssuanceAux issuanceAux) throws ServiceException{
		Issuance issuance = new Issuance();
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());       
        
        issuance = fillIssuanceByIssuanceAux(issuanceAux);
        return this.registryIssuanceServiceFacade(issuance);
	}

	 public Issuance fillIssuanceByIssuanceAux(IssuanceAux issuanceAux){
		 Issuance issuance = new Issuance();
		 issuance.setAmortizationAmount(issuanceAux.getAmortizationAmount());
		 issuance.setAmountConfirmedSegments(issuanceAux.getAmountConfirmedSegments());
		 issuance.setAmountOpenedSegments(issuanceAux.getAmountOpenedSegments());
		 issuance.setCirculationAmount(issuanceAux.getCirculationAmount());
		 issuance.setIssuanceAmount(issuanceAux.getIssuanceAmount());
		 issuance.setSecurityType(issuanceAux.getSecurityType());
		 issuance.setSecurityClass(issuanceAux.getSecurityClass());
		 issuance.setCreditRatingScales(issuanceAux.getCreditRatingScales());
		 issuance.setCurrency(issuanceAux.getCurrency());
		 issuance.setCurrentTranche(issuanceAux.getCurrentTranche());
		 issuance.setDescription(issuanceAux.getDescription());
		 issuance.setEndorsement(issuanceAux.getEndorsement());
		 issuance.setExpirationDate(issuanceAux.getExpirationDate());
		 issuance.setGeographicLocation(issuanceAux.getGeographicLocation());
		 issuance.setIdIssuanceCodePk(issuanceAux.getIdIssuanceCodePk());
		 issuance.setIndHaveCertificate(issuanceAux.getIndHaveCertificate());
		 issuance.setIndPrimaryPlacement(issuanceAux.getIndPrimaryPlacement());
		 issuance.setIndRegulatorReport(issuanceAux.getIndRegulatorReport());
		 issuance.setInstrumentType(issuanceAux.getInstrumentType());
		 issuance.setIssuanceAmount(issuanceAux.getIssuanceAmount());
		// issuance.setIssuanceCertificates(issuanceAux.getIssuanceCertificates());
		 issuance.setIssuanceDate(issuanceAux.getIssuanceDate());
//		 issuance.setIssuanceFiles(null);
		 issuance.setIssuanceType(issuanceAux.getIssuanceType());
		 issuance.setIssuer(issuanceAux.getIssuer());
		 issuance.setNegotiationFactor(issuanceAux.getNegotiationFactor());
		 issuance.setNumberPlacementTranches(issuanceAux.getNumberPlacementTranches());
		 issuance.setNumberTotalTranches(issuanceAux.getNumberTotalTranches());
		 issuance.setPlacedAmount(issuanceAux.getPlacedAmount());
		 issuance.setStateIssuance(issuanceAux.getStateIssuance());
		 issuance.setPlacementType(issuanceAux.getPlacementType());
		 issuance.setResolutionDate(issuanceAux.getResolutionDate());
		 issuance.setResolutionNumber(issuanceAux.getResolutionNumber());
		 issuance.setTestimonyNumber(issuanceAux.getTestimonyNumber());
		 issuance.setTestimonyDate(issuanceAux.getTestimonyDate());
		 issuance.setIdIssuanceCodePk(issuanceAux.getIdIssuanceCodePk());
		 issuance.setPlacementExpirationDate(issuanceAux.getPlacementExpirationDate());
		 issuance.setPlacementExpirationDays(issuanceAux.getPlacementExpirationDays());
//		 List<IssuanceCertificate> lstIssCer= new ArrayList<>();
//		 IssuanceCertificate issCer= new IssuanceCertificate();
		 
//		 for(IssuanceCertificateAux obj:issuanceAux.getIssuanceCertificates()) {
//			 issCer= new IssuanceCertificate();
//			 issCer.setCertificateFile(obj.getCertificateFile());
//			 issCer.setCertificateNumber(null);
//			 issCer.setCertificateType(obj.getCertificateType());
//			 issCer.setFileName(obj.getFileName());
//			 issCer.setHolder(obj.getHolder());
//			 issCer.setIssuance(issuance);
//			 issCer.setIssuanceAmount(obj.getIssuanceAmount());
//			 issCer.setIssuanceBalance(obj.getIssuanceBalance());
//			 issCer.setIssuanceDate(obj.getIssuanceDate());
//			 lstIssCer.add(issCer);
//		 }
//		 issuance.setIssuanceCertificates(lstIssCer);
		 
		 List<IssuanceFile> lstIssFile= new ArrayList<>();
		 IssuanceFile issFile= new IssuanceFile();
		 for(IssuanceFileAux objIssFile:issuanceAux.getIssuanceFiles()) {
			 issFile = new IssuanceFile();
			 issFile.setDocumentDate(objIssFile.getDocumentDate());
			 issFile.setDocumentFile(objIssFile.getDocumentFile());
			 issFile.setDocumentNumber(objIssFile.getDocumentNumber());
			 issFile.setDocumentState(objIssFile.getDocumentState());
			 issFile.setFileName(objIssFile.getFileName());
			 issFile.setIssuance(issuance);
			 issFile.setMaxDocumentDate(objIssFile.getMaxDocumentDate());
			 lstIssFile.add(issFile);
		 }
		 issuance.setIssuanceFiles(lstIssFile);
		
		 return issuance;
     }
}
