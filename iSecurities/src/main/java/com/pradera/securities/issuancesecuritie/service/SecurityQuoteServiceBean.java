package com.pradera.securities.issuancesecuritie.service;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.TransactionSynchronizationRegistry;

import org.hibernate.exception.SQLGrammarException;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.SecurityQuote;
import com.pradera.model.issuancesecuritie.type.SecurityQuoteStateType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.securities.issuancesecuritie.to.SecurityQuoteTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SecurityQuoteServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class SecurityQuoteServiceBean extends CrudDaoServiceBean {
	
	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;

	/**
	 * Metodo para Guardar la Cotizacion de un Valor.
	 *
	 * @param securityQuote the security quote
	 * @return the security quote
	 * @throws ServiceException the service exception
	 */
	public SecurityQuote saveSecurityQuoteService(SecurityQuote securityQuote) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		
		securityQuote.setQuoteState(SecurityQuoteStateType.REGISTERED.getCode());
		securityQuote.setQuoteValueBefore(BigDecimal.ZERO);
		securityQuote.setRegistryUser(loggerUser.getUserName());
		securityQuote.setRegistryDate(CommonsUtilities.currentDate());
		
		create(securityQuote);
		return securityQuote;
	}
	
	/**
	 * Metodo para Rechazar una cotizacion de un valor.
	 *
	 * @param securityQuoteParam the security quote param
	 * @return the security quote
	 * @throws ServiceException the service exception
	 */
	public SecurityQuote rejectSecurityQuoteService(SecurityQuote securityQuoteParam) throws ServiceException{
		SecurityQuote securityQuote = em.find(SecurityQuote.class, securityQuoteParam.getIdSecurityQuotePk());
		securityQuote.setQuoteState(SecurityQuoteStateType.ANNULATED.getCode());
		update(securityQuote);
		return securityQuote;
	}
	
	/**
	 * Confirm security quote.
	 *
	 * @param securityQuoteParam the security quote param
	 * @return the security quote
	 * @throws ServiceException the service exception
	 */
	public SecurityQuote confirmSecurityQuote(SecurityQuote securityQuoteParam) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		
		SecurityQuote securityQuote = em.find(SecurityQuote.class, securityQuoteParam.getIdSecurityQuotePk());
		securityQuote.setQuoteState(SecurityQuoteStateType.CONFIRMED.getCode());
	
		if(securityQuote.getQuoteDate().equals(CommonsUtilities.currentDate())){
			Security security = securityQuote.getSecurity();
			securityQuote.setQuoteValueBefore(security.getCurrentNominalValue());
			modifyCurrentNominalValue(securityQuote, loggerUser.getUserAction().getIdPrivilegeConfirm());
		}
		update(securityQuote);
		return securityQuote;
	}
	
	
	/**
	 * Modify current nominal value.
	 *
	 * @param quote the quote
	 * @param privilegeApp the privilege app
	 */
	public void modifyCurrentNominalValue(SecurityQuote quote, Integer privilegeApp){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Update Security SET ");
		sbQuery.append("   currentNominalValue = :currentNominalValue ");
		sbQuery.append(" , lastModifyApp = :lastModifyApp ");
		sbQuery.append(" , lastModifyDate = :lastModifyDate ");
		sbQuery.append(" , lastModifyIp = :lastModifyIp ");
		sbQuery.append(" , lastModifyUser = :lastModifyUser ");
		sbQuery.append(" Where idIsinCodePk = :isinCodeParam ");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("currentNominalValue", quote.getQuoteValuePrice());
		query.setParameter("lastModifyApp", privilegeApp);
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		query.setParameter("isinCodeParam", quote.getSecurity().getIdIsinCode());
		
		query.executeUpdate();
	}
	
	/**
	 * Gets the security quote by isin.
	 *
	 * @param isinCode the isin code
	 * @param date the date
	 * @return the security quote by isin
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SecurityQuote> getSecurityQuoteByIsin(String isinCode, Date date) throws ServiceException{
		String queryExecute = "Select new com.pradera.model.issuancesecuritie.SecurityQuote(sq.idSecurityQuotePk, sq.quoteState) " +
				"			   From SecurityQuote sq " +
				"			   Where sq.security.idIsinCodePk = :idIsinParam AND" +
				"				     trunc(sq.quoteDate) = trunc(:quoteDateParam)";
		
		Query query = em.createQuery(queryExecute);
		query.setParameter("idIsinParam", isinCode);
		query.setParameter("quoteDateParam", date);
		
		try{
			return query.getResultList();
		}catch(NoResultException ex){
			return new ArrayList<SecurityQuote>();
		}
	}
	
	/**
	 * Metodo para obtener una lista de cotizaciones de un valor
	 *
	 * @param securityQuoteTO the security quote to
	 * @return the security quote list
	 * @throws ServiceException the service exception
	 */
	public List<SecurityQuoteTO> getSecurityQuoteList(SecurityQuoteTO securityQuoteTO) throws ServiceException{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<SecurityQuoteTO> criteriaQuery = cb.createQuery(SecurityQuoteTO.class);
		Root<SecurityQuote> securityQuoteRoot = criteriaQuery.from(SecurityQuote.class);
		List<Predicate> params = new ArrayList<Predicate>();
		
		criteriaQuery.distinct(true);
		criteriaQuery.multiselect(
				securityQuoteRoot.get("idSecurityQuotePk"),
				securityQuoteRoot.get("security").get("issuer").get("idIssuerPk"),
				securityQuoteRoot.get("quoteValuePrice"),
				securityQuoteRoot.get("quoteValueBefore"),
				securityQuoteRoot.get("quoteDate"),
				securityQuoteRoot.get("security").get("idSecurityCodePk"),
				securityQuoteRoot.get("quoteState"),
				securityQuoteRoot.get("requestType"), 
				securityQuoteRoot.get("shareBalance"),
				securityQuoteRoot.get("shareCapital"), 
				securityQuoteRoot.get("lastModifyUser")
		);
		
		if(Validations.validateIsNotNullAndPositive(securityQuoteTO.getIdSecurityQuotePk() )){
			params.add(cb.equal(securityQuoteRoot.get("idSecurityQuotePk"), securityQuoteTO.getIdSecurityQuotePk() ));

			if(Validations.validateIsNotNull(securityQuoteTO.getIssuer()) && 
					Validations.validateIsNotNull(securityQuoteTO.getIssuer().getIdIssuerPk())	
			){
				params.add(cb.equal(securityQuoteRoot.get("security").get("issuer").get("idIssuerPk"), securityQuoteTO.getIssuer().getIdIssuerPk()));
			}
			
		}else{
			if( Validations.validateIsNotNullAndPositive(securityQuoteTO.getRequestType()) )
				params.add(cb.equal(securityQuoteRoot.get("requestType"), securityQuoteTO.getRequestType()));
			if(Validations.validateIsNotNullAndNotEmpty( securityQuoteTO.getIssuer().getIdIssuerPk()) )
				params.add(cb.equal(securityQuoteRoot.get("security").get("issuer").get("idIssuerPk"), securityQuoteTO.getIssuer().getIdIssuerPk()));
			if(Validations.validateIsNotNullAndNotEmpty(  securityQuoteTO.getSecurity().getIdIsinCode()) )
				params.add(cb.equal(securityQuoteRoot.get("security").get("idSecurityCodePk"), securityQuoteTO.getSecurity().getIdSecurityCodePk()));
			if(Validations.validateIsNotNullAndNotEmpty( securityQuoteTO.getQuoteState() ))
				params.add(cb.equal(securityQuoteRoot.get("quoteState"), securityQuoteTO.getQuoteState()));
			if(Validations.validateIsNotNull( securityQuoteTO.getInitialDate() ))
				params.add(cb.greaterThanOrEqualTo(securityQuoteRoot.<Date>get("quoteDate"),securityQuoteTO.getInitialDate()));
			if(Validations.validateIsNotNullAndNotEmpty( securityQuoteTO.getFinalDate() ))
				params.add(cb.lessThanOrEqualTo(securityQuoteRoot.<Date>get("quoteDate"),securityQuoteTO.getFinalDate()));
		}
		criteriaQuery.where(cb.and(params.toArray(new Predicate[params.size()])));
		TypedQuery<SecurityQuoteTO> quoteCriteria = em.createQuery(criteriaQuery);
		
		try{
			return quoteCriteria.getResultList();
		}catch(NoResultException ex){
			return new ArrayList<SecurityQuoteTO>();
		}
	}
	
	/**
	 * Gets the security quote from pk.
	 *
	 * @param idSecurityQuotePk the id security quote pk
	 * @return the security quote from pk
	 * @throws ServiceException the service exception
	 */
	public SecurityQuote getSecurityQuoteFromPk(Long idSecurityQuotePk) throws ServiceException{
		SecurityQuote securityQuote = em.find(SecurityQuote.class, idSecurityQuotePk);
		securityQuote.getSecurity().getIdIsinCode();
		securityQuote.getSecurity().getIssuance().getIdIssuanceCodePk();
		securityQuote.getSecurity().getIssuance().getIssuer().getIdIssuerPk();
		//securityQuote.setIssuance(securityQuote.getSecurity().getIssuance());
		securityQuote.setIssuer(securityQuote.getSecurity().getIssuance().getIssuer());
		return securityQuote;
	}
	

	/**
	 * Validate state.
	 *
	 * @param idSecurityQuote the id security quote
	 * @param statesValidate the states validate
	 * @throws ServiceException the service exception
	 */
	public void validateState(Long idSecurityQuote, List<Integer> statesValidate) throws ServiceException{
		Map<String, Object> params = new HashMap<String, Object>();
		
		params.put("idSecQuoteParam", idSecurityQuote);
		SecurityQuote quoteValidate = findObjectByNamedQuery(SecurityQuote.SECURITY_QUOTE, params);
		if(!statesValidate.contains(quoteValidate.getQuoteState())){
			throw new ServiceException(ErrorServiceType.SECURITY_QUOTE_STATE);
		}
	}
	
	/**
	 * Gets the security list by parameters.
	 *
	 * @param securitiesTO the securities to
	 * @return the security list by parameters
	 * @throws ServiceException the service exception
	 */
	public List<Security> getSecurityListByParameters(SecurityQuoteTO securitiesTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" SELECT s.idIsinCodePk, " +//0
					   " s.description, " +//1
					   " s.currency, " +//2
					   " s.shareBalance, " +//3
					   " s.shareCapital," +//4
					   " s.issuer.idIssuerPk ");//5
		sbQuery.append(" FROM	");
		sbQuery.append("  	Security s ");
		sbQuery.append(" WHERE  ");

		
		sbQuery.append(" s.securityType = :securityType ");
		
		if (Validations.validateIsNotNullAndNotEmpty(securitiesTO.getIssuer().getIdIssuerPk())) {
			sbQuery.append(" AND s.issuance.idIssuanceCodePk = :idIssuerPk ");
		}
		
		sbQuery.append(" ORDER BY ");
		sbQuery.append("  	s.idIsinCodePk	");

		Query typedQuery= em.createQuery(sbQuery.toString()); 

		typedQuery.setParameter("securityType", new Integer(404));
		
		if (Validations.validateIsNotNullAndNotEmpty(securitiesTO.getIssuer().getIdIssuerPk())) {
			typedQuery.setParameter("idIssuerPk", securitiesTO.getIssuer().getIdIssuerPk());
		}
		
		List<Security> lstSecurity = new ArrayList<Security>();
		Security security = null;
		
		for (int i = 0; i < typedQuery.getResultList().size(); i++) {
			Object obj[] = (Object[])typedQuery.getResultList().get(i);
			
			security = new Security();
			
			if(Validations.validateIsNotNull(obj[0])){
				security.setIdIsinCode((String)obj[0]);
			}
			if(Validations.validateIsNotNull(obj[1])){
				security.setDescription((String)obj[1]);
			}
			if(Validations.validateIsNotNull(obj[2])){
				security.setCurrency((Integer)obj[2]);
			}
			if(Validations.validateIsNotNull(obj[3])){
				security.setShareBalance((BigDecimal)obj[3]);
			}
			if(Validations.validateIsNotNull(obj[4])){
				security.setShareCapital((BigDecimal)obj[4]);
			}
			if(Validations.validateIsNotNull(obj[5])){
				Issuer issuer = new Issuer();
				issuer.setIdIssuerPk((String)obj[5]);
				security.setIssuer(issuer);
			}
			lstSecurity.add(security);
		}
		
		return lstSecurity;
	}
	
	
}