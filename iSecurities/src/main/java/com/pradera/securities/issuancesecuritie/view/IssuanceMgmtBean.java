package com.pradera.securities.issuancesecuritie.view;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.security.model.type.LogoutMotiveType;
import com.pradera.commons.sessionuser.OperationUserTO;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.view.IssuerHelperBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.type.EconomicActivityType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.GeographicLocationStateType;
import com.pradera.model.generalparameter.type.GeographicLocationType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.IssuanceAux;
import com.pradera.model.issuancesecuritie.IssuanceCertificate;
import com.pradera.model.issuancesecuritie.IssuanceCertificateAux;
import com.pradera.model.issuancesecuritie.IssuanceFile;
import com.pradera.model.issuancesecuritie.IssuanceFileAux;
import com.pradera.model.issuancesecuritie.IssuanceHistoryBalance;
import com.pradera.model.issuancesecuritie.IssuanceHistoryBalanceAux;
import com.pradera.model.issuancesecuritie.IssuanceObservation;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.CalendarDayType;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.IssuanceAuxStateType;
import com.pradera.model.issuancesecuritie.type.IssuanceCertificateStateType;
import com.pradera.model.issuancesecuritie.type.IssuanceStateType;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityPlacementType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.issuancesecuritie.type.SecurityType;
import com.pradera.securities.issuancesecuritie.facade.IssuanceSecuritiesServiceFacade;
import com.pradera.securities.issuancesecuritie.facade.SecurityServiceFacade;
import com.pradera.securities.issuancesecuritie.to.IssuanceAuxTO;
import com.pradera.securities.issuancesecuritie.to.IssuanceTO;
import com.pradera.securities.issuancesecuritie.util.PropertiesConstants;
import com.pradera.securities.issuer.facade.IssuerServiceFacade;
import com.pradera.securities.issuer.to.IssuerTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class IssuanceMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class IssuanceMgmtBean extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	private String sessionID = "org.jboss.weld.context.http.HttpSessionContext#org.jboss.weld.bean-iSecurities.war/WEB-INF/classes-ManagedBean-class com.pradera.securities.issuancesecuritie.view.IssuanceModificationMgmtBean";

	/** The country residence. */
	@Inject 
	@Configurable 
	Integer countryResidence;
	
	/** The id issuer bc. */
	@Inject 
	@Configurable
	String idIssuerBC;
	
	/** The issuer helper bean. */
	@Inject
	IssuerHelperBean issuerHelperBean;
	
	/** The user info. */
	@Inject UserInfo userInfo;
	
	/** The user privilege. */
	@Inject UserPrivilege userPrivilege;
	
	/** The general parameter facade. */
	@EJB
	private GeneralParametersFacade generalParameterFacade;

	/** The issuance securities service facade. */
	@EJB
	private IssuanceSecuritiesServiceFacade issuanceSecuritiesServiceFacade;
	
	/** The issuer service facade. */
	@EJB
	private IssuerServiceFacade issuerServiceFacade;
	
	/** The security service facade. */
	@EJB
	private SecurityServiceFacade securityServiceFacade;

	/** The issuance. */
	private Issuance issuance;
	private IssuanceAux issuanceAux;
	private IssuanceObservation issuanceObs;
	private List<IssuanceObservation> issuanceObsList;
	
	/** The issuance history balance. */
	private IssuanceHistoryBalance issuanceHistoryBalance;
	private IssuanceHistoryBalanceAux issuanceHistoryBalanceAux;

	/** The issuance certificate. */
	private IssuanceCertificate issuanceCertificate;
	private IssuanceCertificateAux issuanceCertificateAux;
	
	/** The issuance file. */
	private IssuanceFile issuanceFile;
	private IssuanceFileAux issuanceFileAux;
	
	/** The issuance certificate to delete. */
	private IssuanceCertificate issuanceCertificateToDelete;
	private IssuanceCertificateAux issuanceCertificateToDeleteAux;

	/** The issuance file to delete. */
	private IssuanceFile issuanceFileToDelete;
	private IssuanceFileAux issuanceFileToDeleteAux;
	
	/** The issuer. */
	private Issuer issuer;
	
	/** The issuer. */
	private Issuer issuerSearch;

	/** The geographic location. */
	private GeographicLocation geographicLocation;

	/** The issuer to. */
	private IssuerTO issuerTO;

	/** The issuance to. */
	private IssuanceTO issuanceTO;
	
	/** The issuance to. */
	private IssuanceAuxTO issuanceAuxTO;

	/** The parameter table to. */
	private ParameterTableTO parameterTableTO;

	// informative fields in page
	/** The issued securities. */
	private BigDecimal availableAmount;

	/** The lst cbo instrument type. */
	private List<ParameterTable> lstCboInstrumentType;

	/** The lst cbo securitie type. */
	private List<ParameterTable> lstCboSecuritieType;

	/** The lst cbo securitie class. */
	private List<ParameterTable> lstCboSecuritieClass;

	/** The lst cbo issue type. */
	private List<ParameterTable> lstCboIssueType;

	/** The lst cbo currency type. */
	private List<ParameterTable> lstCboCurrencyType;

	/** The lst cbo geographic location. */
	private List<GeographicLocation> lstCboGeographicLocation;

	/** The lst cbo search securitie type. */
	private List<ParameterTable> lstCboSearchSecuritieType;
	
	/** The lst cbo search securitie class. */
	private List<ParameterTable> lstCboSearchSecuritieClass;
	
	/** The lst cbo credit rating scales. */
	private List<ParameterTable> lstCboCreditRatingScales;

	/** The lst cbo issuance state. */
	private List<ParameterTable> lstCboIssuanceState;
	
	/** The lst cbo security placement type. */
	private List<SecurityPlacementType> lstCboSecurityPlacementType;
	
	/** The lst offer type. */
	private List<ParameterTable> lstOfferType;

	// private List<IssuanceCertificate> lstDtbIssuanceCertificate;
	/** The lst dtb issuances. */
	private List<Issuance> lstDtbIssuances;
	private List<IssuanceAux> lstDtbIssuancesAux;
	
	/** The lst cbo boolean type. */
	private List<BooleanType> lstCboBooleanType;

	/** The issuance data model. */
	private IssuanceDataModel issuanceDataModel;
	private IssuanceAuxDataModel issuanceAuxDataModel;

	// Informative info
	/** The circulation balance. */

	/** The placed balance. */
	private Double placedBalance;
	
	/** The issuance certificate total amount. */
	private BigDecimal issuanceCertificateTotalAmount;
	
	/** The number total tranches. */
	private Integer numberTotalTranches;

	/** The issuance cert file name display. */
	private String issuanceCertFileNameDisplay;
	
	/** The window registry issuance. */
	private String WINDOW_REGISTRY_ISSUANCE="issueMgmtRule";
	
	/** The issuance term. variable utilizada para el plazo de la emision (NO tiempo de Vida de la Emision) */
	private Integer issuanceTerm;
	
	private Integer issuanceFileCertificate=0;
	
	/** The closed invesment fund. */
	private boolean closedInvesmentFund;
	
	private String optionIssuanceAux;
	
	/** Lista de Estados de los objetos IssuanceAux */
	private List<ParameterTable> lstCboIssuanceStateAux;
	
	/** issue 866 referencia issuer */
	private Date minimumDate = CommonsUtilities.currentDate();
	
	/**
	 * Instantiates a new issuance mgmt bean.
	 */
	public IssuanceMgmtBean() {

	}
	/**
	 * List holidays.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listHolidays() throws ServiceException{
		allHolidays=generalParameterFacade.getAllHolidaysByCountry(countryResidence);
	}
	
	/**
	 * Validates user session objects to enable or disable.
	 *
	 * @throws ServiceException the service exception
	 */
	public void sessionValidUserAccount() throws ServiceException
	{
		if((userInfo.getUserAccountSession().isIssuerInstitucion() || userInfo.getUserAccountSession().isIssuerDpfInstitucion()) && 
				Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getIssuerCode())){
			Issuer issuerFilter=new Issuer();
			issuerFilter.setIdIssuerPk( userInfo.getUserAccountSession().getIssuerCode() );
			issuerSearch=new Issuer();
			issuerSearch=issuerServiceFacade.findIssuerByFiltersServiceFacade(issuerFilter);
			loadLoggerIssuerData();
			if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
				issuanceTO.setInstrumentType(InstrumentType.FIXED_INCOME.getCode());
				issuanceAuxTO.setInstrumentType(InstrumentType.FIXED_INCOME.getCode());
				loadCboSecuritieTypeByInstrumentHandler();
				issuanceTO.setSecurityType(SecurityType.PUBLIC.getCode());
				issuanceAuxTO.setSecurityType(SecurityType.PUBLIC.getCode());
				changeCboSecurityTypeSearch();
				changeCboSecurityTypeSearchAux();
				List<ParameterTable> lstSecuritieClass = new ArrayList<ParameterTable>();
				if(lstCboSearchSecuritieClass!=null && lstCboSearchSecuritieClass.size()>0){
					for(ParameterTable objParameterTable : lstCboSearchSecuritieClass){
						if(objParameterTable.getParameterTablePk().equals(SecurityClassType.DPA.getCode()) || 
								objParameterTable.getParameterTablePk().equals(SecurityClassType.DPF.getCode())){
							lstSecuritieClass.add(objParameterTable);
						}
					}
					lstCboSearchSecuritieClass = lstSecuritieClass;
				}
				issuanceTO.setLstSecurityClass(new ArrayList<Integer>());
				issuanceTO.getLstSecurityClass().add(SecurityClassType.DPA.getCode());
				issuanceTO.getLstSecurityClass().add(SecurityClassType.DPF.getCode());
				issuanceAuxTO.setLstSecurityClass(new ArrayList<Integer>());
				issuanceAuxTO.getLstSecurityClass().add(SecurityClassType.DPA.getCode());
				issuanceAuxTO.getLstSecurityClass().add(SecurityClassType.DPF.getCode());
			}
		}
	}
	
	/**
	 * 
	 */
	@PostConstruct
	public void init() {
		try {
			setViewOperationType(ViewOperationsType.CONSULT.getCode());

			listHolidays();
			loadCboBooleanType();
			beginConversation();
			loadCboInstrumentType();
			loadCboCurrencyType();
			loadCboGeographicLocation();
			loadCboIssuanceStateType();
			loadCboSecurityPlacementType();
			//loadCboCreditRatingScales();
			createObjects();
			sessionValidUserAccount();
			loadCboCreditRatingScales();
			
			loadAllSecuritieClass();
			loadAllSecuritieType();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Create Objects.
	 */
	public void createObjects()
	{
		issuanceTO = new IssuanceTO();	
		
		issuanceTO.setIssuanceDateInitial(new Date());
		issuanceTO.setIssuanceDateFinal(new Date());
		
		issuanceAuxTO = new IssuanceAuxTO();
		issuance = new Issuance();
		//issue 669 macker checker
		issuanceAux = new IssuanceAux();
		issuanceObs = new IssuanceObservation();
		issuerSearch = new Issuer();
	}
	
	/**
	 * Load logger issuer data.
	 */
	public void loadLoggerIssuerData() {
		issuanceTO.setIdIssuerPk( issuerSearch.getIdIssuerPk() );	
		issuanceAuxTO.setIdIssuerPk( issuerSearch.getIdIssuerPk() );
	}
	
	/**
	 * Initialize Objects Register.
	 */
	public void initializeObjectsRegister()
	{
//		issue 669 macker checker
//		issuance = new Issuance();
//		issuanceCertificate = new IssuanceCertificate();
		issuanceCertFileNameDisplay=null;
		issuer = new Issuer();
		geographicLocation = new GeographicLocation();
		lstCboSecuritieType=new ArrayList<ParameterTable>();
		lstCboSecuritieClass=new ArrayList<ParameterTable>();
		setIssuanceTerm(null);
		
		issuanceAux = new IssuanceAux();
		issuanceObs = new IssuanceObservation();
		issuanceCertificateAux = new IssuanceCertificateAux();
		issuanceAux.setRegistryDate(new Date());
		issuanceAux.setExpirationDate(new Date());
		issuanceAux.setIndRegulatorReport( BooleanType.YES.getCode() );
		issuanceAux.setNumberPlacementTranches(ComponentConstant.ZERO);
		issuanceAux.setPlacedAmount(BigDecimal.ZERO);
		issuanceAux.setAmortizationAmount(BigDecimal.ZERO);
		issuanceAux.setGeographicLocation(geographicLocation);
		issuanceAux.setIssuer(issuer);	
		issuanceAux.setIndHaveCertificate(ComponentConstant.ZERO);
		issuanceAux.setStateChk(IssuanceAuxStateType.REGISTERED.getCode());//issue 669
	}
	
	/**
	 * Registration operation hanlder.
	 * 
	 * @return the string
	 */
	public String registrationOperationHanlder() {
		try {
			issuance = new Issuance();
			setViewOperationType(ViewOperationsType.REGISTER.getCode());
			initializeObjectsRegister();
			issuanceAux.setInstrumentType(InstrumentType.FIXED_INCOME.getCode());
			issuanceAux.setIndExchangeSecurities(BooleanType.NO.getCode());
			loadCboSecuritieTypeByInstrumentHandlerAux();
			issuanceAux.getGeographicLocation().setIdGeographicLocationPk(countryResidence);
			issuanceAux.setIssuanceDate(issuanceAux.getRegistryDate());
			changeDateToCalculateTerm();
			issuanceFileCertificate = 0;
			if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
				Issuer issuerFilter=new Issuer();
				issuerFilter.setIdIssuerPk( userInfo.getUserAccountSession().getIssuerCode());
				Issuer objIssuer = issuerServiceFacade.findIssuerByFiltersServiceFacade(issuerFilter);
				issuanceAux.setIssuer(objIssuer);
				loadCboSecuritieTypeByInstrumentHandler();
				issuanceAux.setSecurityType(SecurityType.PUBLIC.getCode());
				loadCboSecuritieClassBySecuritieTypeHandlerAux();
				List<ParameterTable> lstSecuritieClass = new ArrayList<ParameterTable>();
				if(lstCboSecuritieClass!=null && lstCboSecuritieClass.size()>0){
					for(ParameterTable objParameterTable : lstCboSecuritieClass){
						if(objParameterTable.getParameterTablePk().equals(SecurityClassType.DPA.getCode()) || 
								objParameterTable.getParameterTablePk().equals(SecurityClassType.DPF.getCode())){
							lstSecuritieClass.add(objParameterTable);
						}
					}
					lstCboSecuritieClass = lstSecuritieClass;
				}
			}
			loadCboIssueType();
			loadCboSecuritieType();
			return WINDOW_REGISTRY_ISSUANCE;
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}

	/**
	 * Validate is valid user.
	 *
	 * @param issuance the issuance
	 * @return the list
	 */	
	private List<String> validateIsValidUser(IssuanceAux issuance) {	
		List<OperationUserTO> lstOperationUserParam = new ArrayList<OperationUserTO>();
		List<String> lstOperationUserResult;	
		OperationUserTO operationUserTO = new OperationUserTO();  
		operationUserTO.setOperNumber(issuance.getIdIssuanceCodePk());
		operationUserTO.setUserName(issuance.getRegistryUser());
		lstOperationUserParam.add(operationUserTO);
		lstOperationUserResult = getIsSubsidiary(userInfo,lstOperationUserParam);
		return lstOperationUserResult;
	}
	
	/**
	 * Modification Issuance operation handler. 
	 * @return the string
	 */
	public String modificationOperationHandler() {
		try {		
			
			//Mensaje para evitar la modificacion de una emision suspendida o vencida
			if(issuanceAux.getStateChk().equals(IssuanceAuxStateType.EXPIRED.getCode())
					|| issuanceAux.getStateChk().equals(IssuanceAuxStateType.SUSPENDED.getCode())){
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.ISSUANCE_REQUEST_INCORRECT_STATE, null);
				JSFUtilities.showSimpleValidationDialog();
				return null;
			}
			
			if (issuanceAux == null) {
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null,PropertiesConstants.ERROR_RECORD_REQUIRED, null);
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationSec').show()");
				return null;
			}
			
			List<String> lstOperationUser = validateIsValidUser(issuanceAux);
			if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
			    String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
				String bodyMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_INVALID_USER_MODIFY,new Object[]{StringUtils.join(lstOperationUser,",")});
				showMessageOnDialog(headerMessage, bodyMessage);
				JSFUtilities.showSimpleValidationDialog();
				return "";
			}
			
			if(IssuanceAuxStateType.REJECTED.getCode().equals(issuanceAux.getStateChk())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_REQUEST_REJECT_TO_REJECT));
				JSFUtilities.showSimpleValidationDialog();				
	    		return null;
			}
			
			setViewOperationType(ViewOperationsType.MODIFY.getCode());
			issuanceCertificateAux = new IssuanceCertificateAux();
			issuanceFileAux = new IssuanceFileAux();
			issuanceCertFileNameDisplay = null;
			IssuanceTO issuanceToForSelect = new IssuanceTO();
			issuanceToForSelect.setIdIssuanceCodePk(issuanceAux.getIdIssuanceCodePk());
			issuanceAux = issuanceSecuritiesServiceFacade.findIssuanceAuxServiceFacade(issuanceToForSelect);
			issuance = issuanceSecuritiesServiceFacade.findIssuanceServiceFacade(issuanceToForSelect);			
			if(Validations.validateIsNotNull(issuance)){
				HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
				HttpSession session = request.getSession();
				session.setAttribute("issuancePK", issuance.getIdIssuanceCodePk());
				session.removeAttribute(sessionID);
				return "issueModMgmtRule";
			} else {
				List<IssuanceFileAux> lstIssuanceFiles = issuanceSecuritiesServiceFacade
						.findIssuanceFilesAuxServiceFacade(issuanceAux.getIdIssuanceCodePk(),IssuanceCertificateStateType.REGISTERED.getCode());
				if (Validations.validateListIsNotNullAndNotEmpty(lstIssuanceFiles)) {
					issuanceAux.setIssuanceFiles(lstIssuanceFiles);
					issuanceFileCertificate = 1;
				} else {
					issuanceAux.setIssuanceFiles(new ArrayList<IssuanceFileAux>());
					issuanceFileCertificate = 0;
				}
				Date expirationDate=issuanceAux.getExpirationDate();
				loadCboSecuritieTypeByInstrumentHandlerAux();
				loadCboSecuritieClassBySecuritieTypeHandlerAux();
				issuanceAux.setExpirationDate(expirationDate);
				setIssuanceTerm(null);
				if(Validations.validateIsNotNull(issuanceAux.getExpirationDate())){
					calculateIssuanceTerm();
				}
				numberTotalTranches = issuanceAux.getNumberTotalTranches();
//				loadCboCreditRatingScalesAux();
				
				return "issueMgmtRule";
			}				
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}
	/**
	 * change status maker - checker 
	 */
	
	public void updateIssuanceAuxState(String idIssuanceCodePk, Integer state){
		try	{
			issuanceSecuritiesServiceFacade.updateIssuanceAuxState(idIssuanceCodePk, state);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
//	@LoggerAuditWeb
//	public void confirmIssuance(IssuanceAux issuanceAux){
//		try	{
//			
//		} catch (Exception e) {
//			excepcion.fire(new ExceptionToCatchEvent(e));
//		}
//	}
	/**
	 * 
	 * @return
	 */
	public String viewDetailStateIssuance(Integer viewState){
		try {
			setViewOperationType(viewState);
			IssuanceTO issuanceToForSelect = new IssuanceTO();
			issuanceToForSelect.setIdIssuanceCodePk(issuanceAux.getIdIssuanceCodePk());
			issuanceFileAux = new IssuanceFileAux();
			issuanceAux = issuanceSecuritiesServiceFacade.findIssuanceAuxServiceFacade(issuanceToForSelect);
			List<IssuanceFileAux> lstIssuanceFiles = issuanceSecuritiesServiceFacade.findIssuanceFilesAuxServiceFacade(issuanceAux.getIdIssuanceCodePk(),IssuanceCertificateStateType.REGISTERED.getCode());
			issuanceObsList = new ArrayList<IssuanceObservation>();
			issuanceObsList = issuanceSecuritiesServiceFacade.findIssuanceObsServiceFacade(issuanceAux.getIdIssuanceCodePk());				
			if (Validations.validateListIsNotNullAndNotEmpty(lstIssuanceFiles)) {
				issuanceAux.setIssuanceFiles(lstIssuanceFiles);
			} else {
				issuanceAux.setIssuanceFiles(new ArrayList<IssuanceFileAux>());
			}
			Date expirationDate = issuanceAux.getExpirationDate();
			loadCboSecuritieTypeByInstrumentHandlerAux();
			loadCboSecuritieClassBySecuritieTypeHandlerAux();
			issuanceAux.setExpirationDate( expirationDate );
			setIssuanceTerm(null);
			if(Validations.validateIsNotNull(issuanceAux.getExpirationDate())){
				calculateIssuanceTerm();
			}
//			loadCboCreditRatingScalesAux();
			
			return "issuanceAuxDetailRule";
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}
		
	/**
	 * Metodo para mostrar el mensaje segun el privilegio
	 * @param optionOperation
	 * @return
	 */
	public String beforeStations(String optionOperation){

		Object[] argObj = new Object[1];
		
		if(issuanceAux != null && issuanceAux.getIdIssuanceCodePk() != null){
			argObj[0] = issuanceAux.getIdIssuanceCodePk();
		} else {
			argObj[0] = "";
		}		
		
		// asignando operacion seleccionada
		optionIssuanceAux = optionOperation;
		
		
		switch (optionOperation) {
		case "review":
			showMessageOnDialog(
					PropertiesConstants.LBL_HEADER_ALERT_REVIEW,
					null, PropertiesConstants.ISSUANCE_CONFIRM_REVIEW, argObj);
			
			break;
		case "approve":
			showMessageOnDialog(
					PropertiesConstants.LBL_HEADER_ALERT_APPROVE,
					null, PropertiesConstants.ISSUANCE_CONFIRM_APPROVE, argObj);
			
			break;
		case "confirm":
			showMessageOnDialog(
					PropertiesConstants.LBL_HEADER_ALERT_CONFIRM,
					null, PropertiesConstants.ISSUANCE_CONFIRM_CONFIRM, argObj);
			
			break;
		case "reject":
			showMessageOnDialog(
					PropertiesConstants.LBL_HEADER_ALERT_REJECT,
					null, PropertiesConstants.ISSUANCE_CONFIRM_REJECT, argObj);
			
			break;
			
		case "modify":
			showMessageOnDialog(
					PropertiesConstants.LBL_HEADER_ALERT_REJECT,
					null, PropertiesConstants.ISSUANCE_CONFIRM_CONFIRM, argObj);
			break;

		}
		
		JSFUtilities.executeJavascriptFunction("PF('cnfwIssuanceMgmt').show()");
		
		return null;
	}
	
	
	/**
	 * Metodo que analiza la accion a realizar sobre la solicitud 
	 * @param optionOperation
	 * @return
	 */
	public String beforeOperation(String optionOperation){
		
		//Verificamos que se seleccione un registro
		if (issuanceAux == null) {
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.ERROR_RECORD_REQUIRED, null);
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}
		
		Object[] argObj = new Object[1];
		
		if(issuanceAux != null && issuanceAux.getIdIssuanceCodePk() != null){
			argObj[0] = issuanceAux.getIdIssuanceCodePk();
			
			// issue 1359 se cargan las listas de los combobox 
			// para que se pueda visualizar los campos "tipo de oferta", "clase de valor", "forma de emision" y "tipo de colocacion"
			try {
				loadCboSecuritieType();
				loadCboSecuritieClass();
				loadCboIssueType();
				loadCboSecurityPlacementType();
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			argObj[0] = "";
		}		
		
		// asignando operacion seleccionada
		optionIssuanceAux = optionOperation;
		Integer codeStateAux = null;
		
		if(optionIssuanceAux.equals("reject")){
			codeStateAux = IssuanceAuxStateType.REJECTED.getCode();
		}else if(optionIssuanceAux.equals("review")){
			codeStateAux = IssuanceAuxStateType.REVIEWED.getCode();
		}else if(optionIssuanceAux.equals("approve")){
			codeStateAux = IssuanceAuxStateType.APPROBED.getCode();
		}else if(optionIssuanceAux.equals("confirm")){	
			codeStateAux = IssuanceAuxStateType.CONFIRMED.getCode();
		}
		
		
		if(issuanceAux.getStateChk().equals(IssuanceAuxStateType.REGISTERED.getCode())){
				
			if(!codeStateAux.equals(IssuanceAuxStateType.APPROBED.getCode())
					&& !codeStateAux.equals(IssuanceAuxStateType.REJECTED.getCode())){
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.ISSUANCE_REQUEST_INCORRECT_STATE, null);
				JSFUtilities.showSimpleValidationDialog();
				return null;
			}
			
		}else if(issuanceAux.getStateChk().equals(IssuanceAuxStateType.REVIEWED.getCode())){
			
			if(!codeStateAux.equals(IssuanceAuxStateType.CONFIRMED.getCode())
				    && !codeStateAux.equals(IssuanceAuxStateType.REJECTED.getCode())){
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.ISSUANCE_REQUEST_INCORRECT_STATE, null);
				JSFUtilities.showSimpleValidationDialog();
				return null;
			}
			
		}else if(issuanceAux.getStateChk().equals(IssuanceAuxStateType.APPROBED.getCode())){
			
			if(!codeStateAux.equals(IssuanceAuxStateType.REVIEWED.getCode())
					&& !codeStateAux.equals(IssuanceAuxStateType.REJECTED.getCode())){
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.ISSUANCE_REQUEST_INCORRECT_STATE, null);
				JSFUtilities.showSimpleValidationDialog();
				return null;
			}
			
		}else if(issuanceAux.getStateChk().equals(IssuanceAuxStateType.CONFIRMED.getCode())){
			
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.ISSUANCE_REQUEST_INCORRECT_STATE, null);
				JSFUtilities.showSimpleValidationDialog();
				return null;
				
		}else if(issuanceAux.getStateChk().equals(IssuanceAuxStateType.REJECTED.getCode())){
			
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.ISSUANCE_REQUEST_INCORRECT_STATE, null);
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}else if(issuanceAux.getStateChk().equals(IssuanceAuxStateType.SUSPENDED.getCode())
					||issuanceAux.getStateChk().equals(IssuanceAuxStateType.EXPIRED.getCode())){
			
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.ISSUANCE_REQUEST_INCORRECT_STATE, null);
			JSFUtilities.showSimpleValidationDialog();
			return null;
	}
	
		
		Integer operationView = null;
		
		if(optionIssuanceAux.equals("reject")){
			operationView = ViewOperationsType.REJECT.getCode();
		}else if(optionIssuanceAux.equals("review")){
			operationView = ViewOperationsType.REVIEW.getCode();
		}else if(optionIssuanceAux.equals("approve")){
			operationView = ViewOperationsType.APPROVE.getCode();
		}else if(optionIssuanceAux.equals("confirm")){	
			operationView = ViewOperationsType.CONFIRM.getCode();
		}
		
		return viewDetailStateIssuance(operationView);
	}
	
	/**
	 * Metodo que contiene validaciones cuando ejecutamos el privilegio rechazar
	 * @param optionOperation
	 * @return
	 */
	public String beforeOperationReject(String optionOperation){
		
		//Verificamos que se seleccione un registro
		if (issuanceAux == null) {
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.ERROR_RECORD_REQUIRED, null);
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}
		
		//Si es rechazado o confirmado, enviamos mensaje de estado incorrecto.
		if(issuanceAux.getStateChk().equals(IssuanceAuxStateType.REJECTED.getCode())
				|| issuanceAux.getStateChk().equals(IssuanceAuxStateType.CONFIRMED.getCode())){
			
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.ISSUANCE_REQUEST_INCORRECT_STATE, null);
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}
		
		optionIssuanceAux = optionOperation;
		
		if(optionIssuanceAux != null)
			
			switch (optionIssuanceAux) {
			
			case "reject":
				if(IssuanceAuxStateType.REJECTED.getCode().equals(issuanceAux.getStateChk())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_REQUEST_REJECT_TO_REJECT));
					JSFUtilities.showSimpleValidationDialog();				
		    		break;
				}
				if(IssuanceAuxStateType.EXPIRED.getCode().equals(issuanceAux.getStateChk())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_REQUEST_EXPIRED_TO_REJECT));
					JSFUtilities.showSimpleValidationDialog();				
		    		break;
				}
				if(IssuanceAuxStateType.SUSPENDED.getCode().equals(issuanceAux.getStateChk())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_REQUEST_SUSPENDED_TO_REJECT));
					JSFUtilities.showSimpleValidationDialog();				
		    		break;
				}
				JSFUtilities.executeJavascriptFunction("PF('sendDetailVar').show()");
				
				break;
			case "modify":

//				issuanceObsRegister();
				break;
			}
		
		return null;
	}
	
	/**
	 * Metodo que responde al YES para confirmar la accion despues del paso previo
	 * @return
	 */
	public String  optionOperation(){
		
		if(optionIssuanceAux != null)
			
			switch (optionIssuanceAux) {
			
			case "review":
				reviewOperation();
				
				break;
			case "approve":
				approveOperation();
				
				break;
			case "confirm":
				confirmOperation();
				
				break;
			case "reject":
				if(IssuanceAuxStateType.REJECTED.getCode().equals(issuanceAux.getStateChk())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_REQUEST_REJECT_TO_REJECT));
					JSFUtilities.showSimpleValidationDialog();				
		    		break;
				}
				if(IssuanceAuxStateType.EXPIRED.getCode().equals(issuanceAux.getStateChk())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_REQUEST_EXPIRED_TO_REJECT));
					JSFUtilities.showSimpleValidationDialog();				
		    		break;
				}
				if(IssuanceAuxStateType.SUSPENDED.getCode().equals(issuanceAux.getStateChk())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_REQUEST_SUSPENDED_TO_REJECT));
					JSFUtilities.showSimpleValidationDialog();				
		    		break;
				}
				JSFUtilities.executeJavascriptFunction("PF('sendDetailVar').show()");
				
				break;
			case "modify":

//				issuanceObsRegister();
				break;
			}
		
		return null;
	}
	
	/**
	 * Metodo para cambiar de estado a revisado de la emision
	 * @return
	 */
	@LoggerAuditWeb
	public String reviewOperation() {
		try{
			hideDialogsListener(null);			
			if(Validations.validateIsNull(issuanceAux)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();  			
	    		return null;
			}
			if(!IssuanceAuxStateType.APPROBED.getCode().equals(issuanceAux.getStateChk())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_SECURITY_REQUEST_VALIDATION_NOT_IN_APPROBED_STATE));
				JSFUtilities.showSimpleValidationDialog();				
				return null;
			}
//			if(!IssuanceAuxStateType.REGISTERED.getCode().equals(issuanceAux.getStateChk())){
//				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
//						PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_SECURITY_REQUEST_VALIDATION_NOT_IN_REGISTERED_STATE));
//				JSFUtilities.showSimpleValidationDialog();				
//	    		return null;
//			}
			String idIssuanceCodePk = issuanceAux.getIdIssuanceCodePk();			
			updateIssuanceAuxState(idIssuanceCodePk, IssuanceAuxStateType.REVIEWED.getCode());
			reloadPageChangeStatus();
			
			JSFUtilities.executeJavascriptFunction("PF('cnfwIssuanceMgmt').hide()");
			
			showMessageOnDialog(
					PropertiesConstants.LBL_HEADER_ALERT, null,
					PropertiesConstants.ISSUANCE_REQUEST_REVIEWED_STATE_CORRECT, null);
			JSFUtilities.executeJavascriptFunction("PF('cnfIssuanceMgmtOk').show()");
			
			
    		return null;
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}
	
	/**
	 * Metodo para cambiar de estado a aprobado de la emision
	 * @return
	 */
	@LoggerAuditWeb
	public String approveOperation() {
		try{
			hideDialogsListener(null);			
			if(Validations.validateIsNull(issuanceAux)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();  			
	    		return null;
			}
			if(!IssuanceAuxStateType.REGISTERED.getCode().equals(issuanceAux.getStateChk())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_SECURITY_REQUEST_VALIDATION_NOT_IN_REGISTERED_STATE));
				JSFUtilities.showSimpleValidationDialog();				
	    		return null;
			}
//			if(!IssuanceAuxStateType.REVIEWED.getCode().equals(issuanceAux.getStateChk())){
//				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
//						PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_SECURITY_REQUEST_VALIDATION_NOT_IN_REVIEWED_STATE));
//				JSFUtilities.showSimpleValidationDialog();				
//	    		return null;
//			}
			String idIssuanceCodePk = issuanceAux.getIdIssuanceCodePk();			
			updateIssuanceAuxState(idIssuanceCodePk, IssuanceAuxStateType.APPROBED.getCode());	
			reloadPageChangeStatus();
			
			showMessageOnDialog(
					PropertiesConstants.LBL_HEADER_ALERT, null,
					PropertiesConstants.ISSUANCE_REQUEST_APPROBED_STATE_CORRECT, null);
			JSFUtilities.executeJavascriptFunction("PF('cnfIssuanceMgmtOk').show()");
			
    		return null;
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}
	
	
	/**
	 * Metodo para cambiar de estado a confirmado de la emision
	 * @return
	 */
	@LoggerAuditWeb
	public String confirmOperation() {
		try{
			hideDialogsListener(null);			
			if(Validations.validateIsNull(issuanceAux)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();  			
	    		return null;
			}
			if(!IssuanceAuxStateType.REVIEWED.getCode().equals(issuanceAux.getStateChk())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_SECURITY_REQUEST_VALIDATION_NOT_IN_REVIEWED_STATE));
				JSFUtilities.showSimpleValidationDialog();				
	    		return null;
			}
//			if(!IssuanceAuxStateType.APPROBED.getCode().equals(issuanceAux.getStateChk())){
//				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
//						PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_SECURITY_REQUEST_VALIDATION_NOT_IN_APPROBED_STATE));
//				JSFUtilities.showSimpleValidationDialog();				
//				return null;
//			}
			String idIssuanceCodePk = issuanceAux.getIdIssuanceCodePk();			
			issuanceSecuritiesServiceFacade.confirmIssuance(issuanceAux);
			updateIssuanceAuxState(idIssuanceCodePk, IssuanceAuxStateType.CONFIRMED.getCode());	
			
			reloadPageChangeStatus();
			
			/*showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_REQUEST_CONFIRMED_STATE_CORRECT));
			JSFUtilities.showSimpleValidationDialog();*/
			
			showMessageOnDialog(
					PropertiesConstants.LBL_HEADER_ALERT, null,
					PropertiesConstants.ISSUANCE_REQUEST_CONFIRMED_STATE_CORRECT, null);
			JSFUtilities.executeJavascriptFunction("PF('cnfIssuanceMgmtOk').show()");
			
    		return null;
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}	
	
	
	/**
	 * Metodo para cambiar de estado a rechazado de la emision
	 * @return
	 */
	@LoggerAuditWeb
	public String rejectOperation() {
		
		//Validacion para el campo de observaciones, debe ser obligatorio
		if(!Validations.validateIsNotNullAndNotEmpty(issuanceObs.getObsText())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.OBSERVATION_DATA_REQUIRED));
			JSFUtilities.showSimpleValidationDialog();					
			return null;
		}
		
		try{
			if(Validations.validateIsNull(issuanceAux)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();					
				return null;
			}			
			if(IssuanceAuxStateType.CONFIRMED.getCode().equals(issuanceAux.getStateChk())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_REQUEST_CONFIRMED_TO_REJECT));
				JSFUtilities.showSimpleValidationDialog();				
	    		return null;
			}
			if(IssuanceAuxStateType.REJECTED.getCode().equals(issuanceAux.getStateChk())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_REQUEST_REJECT_TO_REJECT));
				JSFUtilities.showSimpleValidationDialog();				
	    		return null;
			}
			if(IssuanceAuxStateType.EXPIRED.getCode().equals(issuanceAux.getStateChk())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_REQUEST_EXPIRED_TO_REJECT));
				JSFUtilities.showSimpleValidationDialog();				
	    		return null;
			}
			if(IssuanceAuxStateType.SUSPENDED.getCode().equals(issuanceAux.getStateChk())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_REQUEST_SUSPENDED_TO_REJECT));
				JSFUtilities.showSimpleValidationDialog();				
	    		return null;
			}
			
			// REgistrando observacion
			issuanceObs.setIssuance(issuanceAux);		
			issuanceObs.setObsDate(CommonsUtilities.currentDate());
			issuanceObs.setObsState(0);
			issuanceObs.setLastModifyApp(userPrivilege.getUserAcctions().getIdPrivilegeReject());
			issuanceSecuritiesServiceFacade.saveIssuanceObservations(issuanceObs);
			
			issuanceObs = new IssuanceObservation();
			
			// modificando estado
			String idIssuanceCodePk = issuanceAux.getIdIssuanceCodePk();			
			updateIssuanceAuxState(idIssuanceCodePk, IssuanceAuxStateType.REJECTED.getCode());
			
			reloadPageChangeStatus();
			
			JSFUtilities.executeJavascriptFunction("PF('cnfIssuanceRejMgmtOk').show()");
			
    		return null;
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}
	
	/**
	 * Back Search Issuance.
	 *
	 * @return windows search
	 */
	public String backSearchIssuance()
	{
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		return "searchIssuanceMgmt";
	}
	
	/**
	 * Selecte issuance detail handler.
	 * 
	 * @return the string
	 */
	public String selecteIssuanceDetailHandler() {
		try {
			setViewOperationType(ViewOperationsType.DETAIL.getCode());
			issuanceFile = new IssuanceFile();
			IssuanceTO issuanceToForSelect = new IssuanceTO();
			issuanceToForSelect.setIdIssuanceCodePk(issuance.getIdIssuanceCodePk());
			issuance = issuanceSecuritiesServiceFacade.findIssuanceServiceFacade(issuanceToForSelect);
			List<IssuanceFile> lstIssuanceFiles = issuanceSecuritiesServiceFacade.findIssuanceFilesServiceFacade(issuance.getIdIssuanceCodePk(),IssuanceCertificateStateType.REGISTERED.getCode());
			if (Validations.validateListIsNotNullAndNotEmpty(lstIssuanceFiles)) {
				issuance.setIssuanceFiles(lstIssuanceFiles);
			} else {
				issuance.setIssuanceFiles(new ArrayList<IssuanceFile>());
			}
			Date expirationDate=issuance.getExpirationDate();
			loadCboSecuritieTypeByInstrumentHandler();
			loadCboSecuritieClassBySecuritieTypeHandler();
			issuance.setExpirationDate( expirationDate );
			setIssuanceTerm(null);
			if(Validations.validateIsNotNull(issuance.getExpirationDate())){
				calculateIssuanceTerm();
			}
//			loadCboCreditRatingScales();
			return "issuanceDetailRule";
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	} 
	
	/**
	 * Metodo para ver el detalle en la pantalla de administracion
	 * @return
	 */
//	public String selecteIssuanceAuxDetailHandler() {
//		try {
//			setViewOperationType(ViewOperationsType.DETAIL.getCode());
//			IssuanceTO issuanceToForSelect = new IssuanceTO();
//			issuanceToForSelect.setIdIssuanceCodePk(issuanceAux.getIdIssuanceCodePk());
//			if(!IssuanceAuxStateType.CONFIRMED.getCode().equals(issuanceAux.getStateChk())
//					&& !IssuanceAuxStateType.SUSPENDED.getCode().equals(issuanceAux.getStateChk())
//					&& !IssuanceAuxStateType.EXPIRED.getCode().equals(issuanceAux.getStateChk())) {
//				issuanceFileAux = new IssuanceFileAux();
//				issuanceAux = issuanceSecuritiesServiceFacade.findIssuanceAuxServiceFacade(issuanceToForSelect);
//				List<IssuanceFileAux> lstIssuanceFiles = issuanceSecuritiesServiceFacade.findIssuanceFilesAuxServiceFacade(issuanceAux.getIdIssuanceCodePk(),IssuanceCertificateStateType.REGISTERED.getCode());
//				issuanceObsList = new ArrayList<IssuanceObservation>();
//				issuanceObsList = issuanceSecuritiesServiceFacade.findIssuanceObsServiceFacade(issuanceAux.getIdIssuanceCodePk());				
//				if (Validations.validateListIsNotNullAndNotEmpty(lstIssuanceFiles)) {
//					issuanceAux.setIssuanceFiles(lstIssuanceFiles);
//					issuanceAux.setIndHaveCertificate(ComponentConstant.ONE);
//				} else {
//					issuanceAux.setIssuanceFiles(new ArrayList<IssuanceFileAux>());
//					issuanceAux.setIndHaveCertificate(ComponentConstant.ZERO);
//				}
//				Date expirationDate = issuanceAux.getExpirationDate();
//				loadCboSecuritieTypeByInstrumentHandlerAux();
//				loadCboSecuritieClassBySecuritieTypeHandlerAux();
//				issuanceAux.setExpirationDate( expirationDate );
//				setIssuanceTerm(null);
//				if(Validations.validateIsNotNull(issuanceAux.getExpirationDate())){
//					calculateIssuanceTermAux();
//				}
//				loadCboCreditRatingScalesAux();
//				return "issuanceAuxDetailRule";
//			} else {
//				issuanceAux = issuanceSecuritiesServiceFacade.findIssuanceAuxServiceFacade(issuanceToForSelect);
//				issuanceFile = new IssuanceFile();
//				issuance = issuanceSecuritiesServiceFacade.findIssuanceServiceFacade(issuanceToForSelect);
//				List<IssuanceFile> lstIssuanceFiles = issuanceSecuritiesServiceFacade.findIssuanceFilesServiceFacade(issuance.getIdIssuanceCodePk(),IssuanceCertificateStateType.REGISTERED.getCode());
//				if (Validations.validateListIsNotNullAndNotEmpty(lstIssuanceFiles)) {
//					issuance.setIssuanceFiles(lstIssuanceFiles);
//					issuance.setIndHaveCertificate(ComponentConstant.ONE);
//				} else {
//					issuance.setIssuanceFiles(new ArrayList<IssuanceFile>());
//					issuance.setIndHaveCertificate(ComponentConstant.ZERO);
//				}
//				Date expirationDate=issuance.getExpirationDate();
//				loadCboSecuritieTypeByInstrumentHandler();
//				loadCboSecuritieClassBySecuritieTypeHandler();
//				issuance.setExpirationDate( expirationDate );
//				//seteando fechas para realizar el calculo correcto de plazo, vida, tiempo trancurrido de la emision en estado confirmado
//				issuanceAux.setIssuanceDate(issuance.getIssuanceDate());
//				issuanceAux.setExpirationDate(issuance.getExpirationDate());
//				setIssuanceTerm(null);
//				if(Validations.validateIsNotNull(issuance.getExpirationDate())){
//					calculateIssuanceTerm();
//				}
//				loadCboCreditRatingScales();
//				return "issuanceDetailRule";
//			}			
//		} catch (Exception e) {
//			excepcion.fire(new ExceptionToCatchEvent(e));
//			return null;
//		}
//	} 
	public String selecteIssuanceAuxDetailHandler() {
		try {
			setViewOperationType(ViewOperationsType.DETAIL.getCode());
			IssuanceTO issuanceToForSelect = new IssuanceTO();
			issuanceToForSelect.setIdIssuanceCodePk(issuanceAux.getIdIssuanceCodePk());
			if(!IssuanceAuxStateType.CONFIRMED.getCode().equals(issuanceAux.getStateChk())
					&& !IssuanceAuxStateType.SUSPENDED.getCode().equals(issuanceAux.getStateChk())
					&& !IssuanceAuxStateType.EXPIRED.getCode().equals(issuanceAux.getStateChk())) {
				issuanceFileAux = new IssuanceFileAux();
				issuanceAux = issuanceSecuritiesServiceFacade.findIssuanceAuxServiceFacade(issuanceToForSelect);
				List<IssuanceFileAux> lstIssuanceFiles = issuanceSecuritiesServiceFacade.findIssuanceFilesAuxServiceFacade(issuanceAux.getIdIssuanceCodePk(),IssuanceCertificateStateType.REGISTERED.getCode());
				issuanceObsList = new ArrayList<IssuanceObservation>();
				issuanceObsList = issuanceSecuritiesServiceFacade.findIssuanceObsServiceFacade(issuanceAux.getIdIssuanceCodePk());				
				if (Validations.validateListIsNotNullAndNotEmpty(lstIssuanceFiles)) {
					issuanceAux.setIssuanceFiles(lstIssuanceFiles);
				} else {
					issuanceAux.setIssuanceFiles(new ArrayList<IssuanceFileAux>());
				}
				Date expirationDate = issuanceAux.getExpirationDate();
				loadCboSecuritieTypeByInstrumentHandlerAux();
				loadCboSecuritieClassBySecuritieTypeHandlerAux();
				issuanceAux.setExpirationDate( expirationDate );
				setIssuanceTerm(null);
				if(Validations.validateIsNotNull(issuanceAux.getExpirationDate())){
					calculateIssuanceTermAux();
				}
//				loadCboCreditRatingScalesAux();
				return "issuanceAuxDetailRule";
			} else {
				issuanceFile = new IssuanceFile();
				issuance = issuanceSecuritiesServiceFacade.findIssuanceServiceFacade(issuanceToForSelect);
				List<IssuanceFile> lstIssuanceFiles = issuanceSecuritiesServiceFacade.findIssuanceFilesServiceFacade(issuance.getIdIssuanceCodePk(),IssuanceCertificateStateType.REGISTERED.getCode());
				if (Validations.validateListIsNotNullAndNotEmpty(lstIssuanceFiles)) {
					issuance.setIssuanceFiles(lstIssuanceFiles);
				} else {
					issuance.setIssuanceFiles(new ArrayList<IssuanceFile>());
				}
				Date expirationDate=issuance.getExpirationDate();
				loadCboSecuritieTypeByInstrumentHandler();
				loadCboSecuritieClassBySecuritieTypeHandler();
				issuance.setExpirationDate( expirationDate );
				setIssuanceTerm(null);
				if(Validations.validateIsNotNull(issuance.getExpirationDate())){
					calculateIssuanceTerm();
				}
//				loadCboCreditRatingScales();
				return "issuanceDetailRule";
			}			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	} 
	
	/**
	 * Before save issue handler.
	 */
	public void beforeSaveIssueHandler() {
		try {
			if(issuanceAux.getIssuanceAmount().compareTo(BigDecimal.ZERO)==1 );
			else
			{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_LBL_AMOUNT_ZERO));
				JSFUtilities.showSimpleValidationDialog();   
				return;
			}
			if(isIndHaveCertificateAux()){
				if(Validations.validateListIsNullOrEmpty(issuanceAux.getIssuanceFiles())){
					
					JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
					JSFUtilities.showMessageOnDialog(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES,PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
							 GeneralConstants.PROPERTY_FILE_MESSAGES,PropertiesConstants.ISSUANCE_ERROR_ISSUANCE_CERTIFICATE_REQUIRED, null);
					JSFUtilities.addContextMessage("frmIssueMgmt:fldIssuanceCert", 
												   FacesMessage.SEVERITY_ERROR,
												   PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_ERROR_ISSUANCE_CERTIFICATE_REQUIRED),
												   PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_ERROR_ISSUANCE_CERTIFICATE_REQUIRED));
					JSFUtilities.showRequiredValidationDialog();
					return;
					
				}
			}

			Object[] argObj=new Object[1];
			if (isViewOperationRegister()) {
				try {
					IssuerTO issuerTO = new IssuerTO();
					issuerTO.setIdIssuerPk(issuanceAux.getIssuer().getIdIssuerPk());
					issuerTO.setIssuerState(IssuerStateType.REGISTERED.getCode());
					issuer = issuanceSecuritiesServiceFacade.findIssuerServiceFacade(issuerTO);
				} catch (Exception e) {
					issuer = null;
					JSFUtilities.showMessageOnDialog(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES,PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
							 GeneralConstants.PROPERTY_FILE_MESSAGES,PropertiesConstants.ISSUER_ERROR_NOT_FOUND, null);
					JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationSec').show()");
					
					JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
					JSFUtilities
							.addContextMessage(
									"frmIssueMgmt:txtIssuer",
									FacesMessage.SEVERITY_ERROR,
									PropertiesUtilities
											.getMessage(PropertiesConstants.ISSUER_ERROR_NOT_FOUND),
									PropertiesUtilities
											.getMessage(PropertiesConstants.ISSUER_ERROR_NOT_FOUND));
					e.printStackTrace();
					return;
				}
				if (issuer.getStateIssuer().equals(
						IssuerStateType.BLOCKED.getCode())) {
					
					
					showExceptionMessage(PropertiesConstants.MESSAGES_ALERT,ErrorServiceType.ISSUER_BLOCK.getMessage());
					
					JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
					JSFUtilities.showSimpleValidationDialog();
					
					return;
				}
				issuanceAux.setIssuer(issuer);				
				
				showMessageOnDialog(
						PropertiesConstants.LBL_HEADER_ALERT_REGISTER, null,
						PropertiesConstants.ISSUANCE_CONFIRM_REGISTER, null);
			} else if (isViewOperationModify()) {		
				/**
				 * Issue 2433
				 * If Issue Type is Mixed
				 */
				if(isMixedIssueTypeAux()&&
						(SecurityClassType.ACC.getCode().equals(issuanceAux.getSecurityClass())||
						SecurityClassType.ACP.getCode().equals(issuanceAux.getSecurityClass())||
						SecurityClassType.CFC.getCode().equals(issuanceAux.getSecurityClass()))
						){
					if(numberTotalTranches>issuanceAux.getNumberTotalTranches()){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
								PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_LBL_TRANCHES_BELOW_CURRENT));
						JSFUtilities.showSimpleValidationDialog();   
						return;
					}
				}	
							
				argObj[0]=issuanceAux.getIdIssuanceCodePk();
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_MODIFY,null, PropertiesConstants.ISSUANCE_CONFIRM_MODIFY, argObj);
			}
			JSFUtilities.executeJavascriptFunction("PF('cnfwIssuanceMgmt').show()");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Search issuance handler.
	 * 
	 * @param event
	 *            the event
	 */
	@LoggerAuditWeb
	public void searchIssuanceHandler(ActionEvent event) {		
		JSFUtilities.hideGeneralDialogues();
		try {
			if(Validations.validateIsNotNullAndNotEmpty(issuerSearch.getIdIssuerPk()))
				issuanceTO.setIdIssuerPk(issuerSearch.getIdIssuerPk());
			    issuanceAuxTO.setIdIssuerPk(issuerSearch.getIdIssuerPk());
			    
			    // 	Cargando datos de la tabla de emisiones
			    loadTableIssuance();
				
			if(issuanceAuxDataModel.getRowCount()>0)	
				loadUserValidation();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}	
	}

	/**
	 * Metodo que realiza la carga de los datos de la tabla de Emisiones
	 * 
	 */
	public void loadTableIssuance(){
		
		try {
			// Lista de emisiones que se encuentran en estado Registrado en la BBDD (Issuance)
		    List<Issuance> listIssuance = issuanceSecuritiesServiceFacade.findIssuancesServiceFacade(issuanceTO);
		    // lista de emisiones que se NO se encuentran en tablas temporales (IssuanceAux)
		    List<IssuanceAux> listAuxIssuance = issuanceSecuritiesServiceFacade.findIssuancesAuxServiceFacade(issuanceTO);
		    
		    if(listAuxIssuance == null)
		    	listAuxIssuance = new ArrayList<>();
		    
		    if(listIssuance != null){
		    	IssuanceAux issuanceAux = null;
			    for(Issuance issuance : listIssuance){
			    	
			    	issuanceAux = new IssuanceAux();
			    	issuanceAux.setIssuer(new Issuer());
			    		
			    	issuanceAux.setIssuanceDate(issuance.getIssuanceDate());
			    	issuanceAux.setDescription(issuance.getDescription());
			    	issuanceAux.setRegistryDate(issuance.getRegistryDate());
			    		
			    	issuanceAux.getIssuer().setMnemonic(issuance.getIssuer().getMnemonic());
			    		
			    	issuanceAux.setInstrumentType(issuance.getInstrumentType());
			    	issuanceAux.setSecurityType(issuance.getSecurityType());
			    	
			    	issuanceAux.setIdIssuanceCodePk(issuance.getIdIssuanceCodePk());
			    	issuanceAux.setSecurityClass(issuance.getSecurityClass());
			    	 
			    	if(issuance.getStateIssuance().equals(IssuanceStateType.EXPIRED.getCode())){
			    		issuanceAux.setStateChk(IssuanceAuxStateType.EXPIRED.getCode());
			    	} else if(issuance.getStateIssuance().equals(IssuanceStateType.SUSPENDED.getCode())){			    		
			    		issuanceAux.setStateChk(IssuanceAuxStateType.SUSPENDED.getCode());			    		
			    	} else{
			    		issuanceAux.setStateChk(IssuanceAuxStateType.CONFIRMED.getCode());
			    	}
			    		
			    	// add new object
			    	listAuxIssuance.add(issuanceAux);
			    		
			    }
		    }
			
			issuanceAuxDataModel = new IssuanceAuxDataModel(listAuxIssuance);	
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		
	}
	
	public void reloadPageChangeStatus() {
		try {
			if(Validations.validateIsNotNullAndNotEmpty(issuerSearch.getIdIssuerPk()))
				issuanceTO.setIdIssuerPk(issuerSearch.getIdIssuerPk());
			    issuanceAuxTO.setIdIssuerPk(issuerSearch.getIdIssuerPk());
			    
			    // 
			    loadTableIssuance();
				
				
			if(issuanceAuxDataModel.getRowCount()>0)	
				loadUserValidation();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	public IssuanceAuxDataModel getIssuanceAuxDataModel() {
		return issuanceAuxDataModel;
	}
	public void setIssuanceAuxDataModel(IssuanceAuxDataModel issuanceAuxDataModel) {
		this.issuanceAuxDataModel = issuanceAuxDataModel;
	}
	/**
	 * Load user validation.
	 */
	private void loadUserValidation() {
		// TODO Auto-generated method stub
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		if(userPrivilege.getUserAcctions().isModify()){
			privilegeComponent.setBtnModifyView(true);
		}
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}	
	
	/**
	 * Session Valid User Account for Clean.
	 */
	public void sessionValidUserAccountClean()
	{		
		issuanceTO = new IssuanceTO();
		issuanceAuxTO = new IssuanceAuxTO();
		issuance = null;		
		issuanceAux = null;
		issuanceTO.setIssuanceDateInitial(new Date());
		issuanceTO.setIssuanceDateFinal(new Date());
		if((userInfo.getUserAccountSession().isIssuerInstitucion() || userInfo.getUserAccountSession().isIssuerDpfInstitucion()) &&
				Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getIssuerCode())){
			loadLoggerIssuerData();	
			if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
				issuanceTO.setInstrumentType(InstrumentType.FIXED_INCOME.getCode());
				issuanceAuxTO.setInstrumentType(InstrumentType.FIXED_INCOME.getCode());
				loadCboSecuritieTypeByInstrumentHandler();
				issuanceTO.setSecurityType(SecurityType.PUBLIC.getCode());
				issuanceAuxTO.setSecurityType(SecurityType.PUBLIC.getCode());
				changeCboSecurityTypeSearch();
				changeCboSecurityTypeSearchAux();
				List<ParameterTable> lstSecuritieClass = new ArrayList<ParameterTable>();
				if(lstCboSearchSecuritieClass!=null && lstCboSearchSecuritieClass.size()>0){
					for(ParameterTable objParameterTable : lstCboSearchSecuritieClass){
						if(objParameterTable.getParameterTablePk().equals(SecurityClassType.DPA.getCode()) || 
								objParameterTable.getParameterTablePk().equals(SecurityClassType.DPF.getCode())){
							lstSecuritieClass.add(objParameterTable);
						}
					}
					lstCboSearchSecuritieClass = lstSecuritieClass;
				}
				issuanceTO.setLstSecurityClass(new ArrayList<Integer>());
				issuanceTO.getLstSecurityClass().add(SecurityClassType.DPA.getCode());
				issuanceTO.getLstSecurityClass().add(SecurityClassType.DPF.getCode());
				issuanceAuxTO.setLstSecurityClass(new ArrayList<Integer>());
				issuanceAuxTO.getLstSecurityClass().add(SecurityClassType.DPA.getCode());
				issuanceAuxTO.getLstSecurityClass().add(SecurityClassType.DPF.getCode());
			}
		}				
	}
	
	/**
	 * Clean Data Model.
	 */
	public void cleanDataModel()
	{
		issuanceDataModel = null;
		issuanceAuxDataModel = null;
	}
	
	/**
	 * Validate Issuer Search.
	 */
	public void validateIssuerSearch()
	{
		if(Validations.validateIsNotNullAndNotEmpty(issuerSearch.getIdIssuerPk()));			
		else {
			issuanceTO.setIdIssuerPk(null); 
			issuanceAuxTO.setIdIssuerPk(null);
		}
		cleanDataModel();	
	}
	/**
	 * Clean issuance search handler.
	 * 
	 * @param event
	 *            the event
	 */
	public void cleanIssuanceSearchHandler(ActionEvent event) {
		try {
			cleanDataModel();	
			createObjectsSearch();
			sessionValidUserAccountClean();			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change cbo has certificate handler.
	 */
	public void changeCboHasCertificateHandler(){
		try {
			JSFUtilities.resetComponent("frmIssueMgmt:fldIssuanceCert");
			issuanceAux.setIssuanceFiles(new ArrayList<IssuanceFileAux>());
//			issuance.setIssuanceFiles(new ArrayList<IssuanceFile>());
			removeUploadedFileHandler(null);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change cbo ind placement tranche.
	 */
	public void changeCboIndPlacementTranche(){
		try {
			JSFUtilities.resetComponent("frmIssueMgmt:opnlCertificateIssue");
			if(isIndPrimaryPlacement()){
				issuance.setPlacementType(null);
			}else{
				issuance.setPlacementType(Integer.valueOf(0));
				issuance.setExpirationDate(null);
			}
			issuance.setCirculationAmount(BigDecimal.ZERO);
			setAvailableAmount(BigDecimal.ZERO);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	//mod tramo de colocacion 
	public void changeCboIndPlacementTrancheAux(){
		try {
			JSFUtilities.resetComponent("frmIssueMgmt:opnlCertificateIssue");
			if(isIndPrimaryPlacementAux()){
				issuanceAux.setPlacementType(null);
			}else{
				issuanceAux.setPlacementType(Integer.valueOf(0));
				issuanceAux.setExpirationDate(null);
			}
			issuanceAux.setCirculationAmount(BigDecimal.ZERO);
			setAvailableAmount(BigDecimal.ZERO);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Certificate file handler.
	 * 
	 * @param event
	 *            the event
	 */
	public void certificateFileHandler(FileUploadEvent event) {
		try {
			 String fDisplayName=event.getFile().getFileName();
			 
			 if(fDisplayName!=null){
				 issuanceCertFileNameDisplay=fDisplayName;
				 issuanceFileAux.setDocumentFile(event.getFile().getContents());

				 //si el nombre del archivo es demasiado grande se limita el tamanio
				 String fileName=event.getFile().getFileName();
				 if(fileName.length()<50){
					 issuanceFileAux.setFileName(fileName);	 
				 }else{
					 issuanceFileAux.setFileName(fileName.substring(0,48));
				 }
			 }

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Removes the uploaded file handler.
	 *
	 * @param event the event
	 */
	public void removeUploadedFileHandler(ActionEvent event){
		try {
			issuanceCertFileNameDisplay=null;
			issuanceFileAux = new IssuanceFileAux();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Gets the download certificate file.
	 *
	 * @return the download certificate file
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public StreamedContent getDownloadCertificateFile() throws IOException {

		InputStream inputStream = new ByteArrayInputStream(issuanceFileAux.getDocumentFile());
		StreamedContent streamedContentFile = new DefaultStreamedContent(inputStream, null, issuanceFileAux.getFileName());
		inputStream.close();

		return streamedContentFile;
	}
	
	public void clearAfterSave(){
		issuanceAux = new IssuanceAux();
		issuanceAux.setIssuer(new Issuer());
		issuanceAux.setGeographicLocation(new GeographicLocation());
		issuanceTO = new IssuanceTO();
		issuanceTO.setIssuanceDateInitial(new Date());
		issuanceTO.setIssuanceDateFinal(new Date());
		issuanceAuxTO = new IssuanceAuxTO();
		issuanceDataModel = null;
		issuanceAuxDataModel = null;
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
	}

	/**
	 * Save issuance handler.
	 */
	@LoggerAuditWeb
	public void saveIssuanceHandler() {
		try {
			JSFUtilities.executeJavascriptFunction("PF('cnfwIssuanceMgmt').hide()");
			
			if (isViewOperationRegister()) {
				registryAux();
			} else if (isViewOperationModify()) {
				modifyAux();			
			}
			//issuance = new Issuance();
			// issue 874: se comenta esto porque ya esta en el metodo clearAfterSave()
//			issuanceAux = new IssuanceAux();
//			issuanceAux.setIssuer(new Issuer());
//			issuanceAux.setGeographicLocation(new GeographicLocation());
//			issuanceTO = new IssuanceTO();
//			issuanceTO.setIssuanceDateInitial(new Date());
//			issuanceTO.setIssuanceDateFinal(new Date());
//			issuanceAuxTO = new IssuanceAuxTO();
//			issuanceDataModel = null;
//			issuanceAuxDataModel = null;
			
//			setViewOperationType(ViewOperationsType.CONSULT.getCode());
		} catch (ServiceException se) {
			if(se.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, se.getErrorService().getMessage());
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}
	
	/**
	 * Before issuer helper handler.
	 */ 
	public void beforeIssuerHelperHandler() {		
		if( Validations.validateIsNotNullAndNotEmpty( issuanceAux.getIssuer().getIdIssuerPk() )){
			hideDialogsListener(null);
				try {					
					if(issuanceAux.getIssuer().getStateIssuer().equals( IssuerStateType.BLOCKED.getCode() )){
							
						issuanceAux.setIssuer( new Issuer() );
						issuanceAux.setGeographicLocation( new GeographicLocation() );
							this.closedInvesmentFund= false;
							cleanObjectsByInstrument();
							Object[] objArg=new Object[1];
							objArg[0]=issuanceAux.getIssuer().getIdIssuerPk();
							JSFUtilities.showMessageOnDialog(GeneralConstants.LBL_HEADER_ALERT, null, PropertiesConstants.ISSUANCE_ERROR_REGISTER_BLOCK_ISSUER, 
									objArg);
							JSFUtilities.showComponent("cnfGeneralMsgBusinessValidation");
							return;
					}
					if (EconomicActivityType.FONDOS_INVERSION_CERRADOS.getCode().equals(issuanceAux.getIssuer().getEconomicActivity())) {
						this.closedInvesmentFund= true;
						issuanceAux.setInstrumentType(InstrumentType.VARIABLE_INCOME.getCode());
						loadCboSecuritieTypeByInstrumentHandler();
						loadCboSecuritieTypeByInstrumentHandlerAux();
						issuanceAux.setSecurityType(SecurityType.PUBLIC.getCode());
						loadCboSecuritieClassBySecuritieTypeHandler();
						loadCboSecuritieClassBySecuritieTypeHandlerAux();
						issuanceAux.setSecurityClass(SecurityClassType.CFC.getCode());
						changeCboSecurityClass();
					} else {
						this.closedInvesmentFund= false;
						cleanObjectsByInstrument();
					}
					issuanceAux.setCreditRatingScales(issuanceAux.getIssuer().getCreditRatingScales());
				} catch (Exception e) {
					excepcion.fire(new ExceptionToCatchEvent(e));
			}
		} else {
			this.closedInvesmentFund= false;
			cleanObjectsByInstrument();
		}
	}	
	
	/**
	 * Hide dialogs listener.
	 *
	 * @param actionEvent the action event
	 */
	public void hideDialogsListener(ActionEvent actionEvent){
		executeAction();
		JSFUtilities.hideGeneralDialogues();
		JSFUtilities.hideComponent("cnfIssuanceMgmt");
		JSFUtilities.hideComponent("IssuanceMgmtOk");
	}
	
	/**
	 * Registry.
	 * 
	 * @return the string
	 * @throws ServiceException
	 *             the service exception
	 */
	@LoggerAuditWeb
	public void registryAux() throws ServiceException {
		Object[] argObj = new Object[1];
		issuanceSecuritiesServiceFacade.registryIssuanceAuxServiceFacade(issuanceAux);
		argObj[0]=issuanceAux.getIdIssuanceCodePk();		
		showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_SUCCESS,null,PropertiesConstants.ISSUANCE_MSG_REGISTER_SUCCESS,argObj);
		JSFUtilities.executeJavascriptFunction("PF('cnfEndTransaction').show()");
	}
	
	
	
	/**
	 * Modify.
	 * 
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void modifyAux() throws ServiceException {
		Object[] argObj = new Object[]{issuanceAux.getIdIssuanceCodePk()};
		
		if (Validations.validateListIsNotNullAndNotEmpty(issuanceAux.getIssuanceFiles())) {
			issuanceAux.setIndHaveCertificate(BooleanType.YES.getCode());
			int countDeleted=0;
			for (IssuanceFileAux objIssuanceFile: issuanceAux.getIssuanceFiles()) {
				objIssuanceFile.setIssuance(issuanceAux);
				objIssuanceFile.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
				objIssuanceFile.setLastModifyDate(CommonsUtilities.currentDateTime());
				objIssuanceFile.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
				objIssuanceFile.setLastModifyApp(userInfo.getUserAcctions().getIdPrivilegeAdd());
				if (objIssuanceFile.getDocumentState().equals(IssuanceCertificateStateType.TO_REGISTERED.getCode())) {
					objIssuanceFile.setDocumentState(IssuanceCertificateStateType.REGISTERED.getCode());
				}
				if(objIssuanceFile.getDocumentState().equals(IssuanceCertificateStateType.TO_DELETED.getCode())){
					objIssuanceFile.setDocumentState(IssuanceCertificateStateType.DELETED.getCode());
					countDeleted++;
				}
			}
			// si la cantidad de archivo a eliminar el igual al tamano de la lista el indicador de archivo cambia a NO
			if(issuanceAux.getIssuanceFiles().size() == countDeleted){
				issuanceAux.setIndHaveCertificate(BooleanType.NO.getCode());	
			}
		}else{
			issuanceAux.setIndHaveCertificate(BooleanType.NO.getCode());
		}
		
		issuanceSecuritiesServiceFacade.updateIssuanceServiceFacade(issuanceAux);
		
		showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_SUCCESS, null,PropertiesConstants.ISSUANCE_MSG_MODIFY_SUCCESS, argObj);
		JSFUtilities.executeJavascriptFunction("PF('cnfEndTransaction').show()");
	}

	/** CertificateAux
	 * Adds the certificate issue handler.
	 */
	public void addCertificateIssueHandler() {
		try {
			if (Validations.validateIsNullOrEmpty(issuanceFileAux.getDocumentNumber())) {
				JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null,"issuance.msg.error.document.number", null);
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			if (Validations.validateIsNull(issuanceFileAux.getDocumentDate())) {
				JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null,"issuance.msg.error.document.date", null);
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			if (Validations.validateIsNull(issuanceFileAux.getDocumentFile())) {
				JSFUtilities.addContextMessage(
								"frmIssueMgmt:fuplCertificateIssuance",
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities.getMessage("issuance.msg.error.certificate.file"),
								PropertiesUtilities.getMessage("issuance.msg.error.certificate.file"));
				JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null,"issuance.msg.error.certificate.file", null);
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			if (issuanceAux.getIssuanceFiles() == null) {
				issuanceAux.setIssuanceFiles(new ArrayList<IssuanceFileAux>());
			}
			if (!isContainDtbIssuanceCertificate(issuanceFileAux)) {
				issuanceFileAux.setIssuance(issuanceAux);
				issuanceFileAux.setDocumentState(IssuanceCertificateStateType.TO_REGISTERED.getCode());
				issuanceAux.getIssuanceFiles().add(issuanceFileAux);
				issuanceFileAux = new IssuanceFileAux();
				issuanceCertFileNameDisplay=null;
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Beofre remove issuance certivicate handler.
	 *
	 * @param event the event
	 */
	public void beofreRemoveIssuanceCertivicateHandler(ActionEvent event){
		try {
			issuanceFileToDeleteAux = (IssuanceFileAux) event.getComponent().getAttributes().get("cIssuanceSelected");
			JSFUtilities.executeJavascriptFunction("PF('cnfwRemIsssuanceCert').show();");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Certificate issuance remove handler.
	 * 
	 * @param event the event
	 */
	public void certificateIssuanceRemoveHandler(ActionEvent event) {
		try {
			if (isViewOperationRegister()) {
				issuanceAux.getIssuanceFiles().remove(issuanceFileToDeleteAux);
			} else if (isViewOperationModify()) {				
				issuanceFileToDeleteAux.setDocumentState(IssuanceCertificateStateType.TO_DELETED.getCode());
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Issuance certificate total amount calculate.
	 */
	public void issuanceCertificateTotalAmountCalculate(){
		issuanceCertificateTotalAmount=BigDecimal.ZERO;
		if(issuanceAux.getIssuanceCertificates()!=null && !issuanceAux.getIssuanceCertificates().isEmpty()){
			for(IssuanceCertificateAux issuanceCert : issuanceAux.getIssuanceCertificates()){
				issuanceCertificateTotalAmount=issuanceCertificateTotalAmount.add(issuanceCert.getIssuanceAmount());
			}
		}
		issuanceAux.setIssuanceAmount(issuanceCertificateTotalAmount);
	}
    /**CERTIFICATE */
	
	/**
	 * Insert id issuer code handler.
	 */
	public void insertIdIssuerCodeHandler() {
		try {
			issuer = new Issuer();
			issuerTO = new IssuerTO();

			if (isViewOperationRegister() || isViewOperationModify()) {
				issuerTO.setIdIssuerPk(issuanceAux.getIssuer().getIdIssuerPk());
				issuerTO.setIssuerState(Integer.valueOf(1));
			} else if (isViewOperationConsult()) {
				issuerTO.setIdIssuerPk(issuanceAuxTO.getIdIssuerPk());
				issuerTO.setIssuerState(Integer.valueOf(1));
			}
			try {
				issuer = issuanceSecuritiesServiceFacade.findIssuerServiceFacade(issuerTO);
			} catch (Exception e) {
				issuer = null;
				if (isViewOperationRegister() || isViewOperationModify()) {
					JSFUtilities
							.addContextMessage(
									"frmIssueMgmt:txtIssuer",
									FacesMessage.SEVERITY_ERROR,
									PropertiesUtilities
											.getMessage(PropertiesConstants.ISSUER_ERROR_NOT_FOUND),
									PropertiesUtilities
											.getMessage(PropertiesConstants.ISSUER_ERROR_NOT_FOUND));
				} else if (isViewOperationConsult()) {
					JSFUtilities
							.addContextMessage(
									"frmSearchIssuance:txtIssuer",
									FacesMessage.SEVERITY_ERROR,
									PropertiesUtilities
											.getMessage(PropertiesConstants.ISSUER_ERROR_NOT_FOUND),
									PropertiesUtilities
											.getMessage(PropertiesConstants.ISSUER_ERROR_NOT_FOUND));
				}
				JSFUtilities.showMessageOnDialog(null, null,PropertiesConstants.ISSUER_ERROR_NOT_FOUND, null);
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationSec').show()");
				issuanceAux.setIssuer(new Issuer());
				issuanceAuxTO.setIssuerDescription(null);
				return;
			}

			if (isViewOperationRegister() || isViewOperationModify()) {
				issuanceAux.setGeographicLocation(issuer.getGeographicLocation());
				issuanceAux.setIssuer(issuer);
			} else if (isViewOperationConsult()) {
				issuanceAuxTO.setIssuerDescription(issuer.getMnemonic());
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change security class.
	 */
	public void changeSecurityClass(){
		try {
			if(issuanceAux.isAccRfSecurityClass()){
				issuanceAux.setExpirationDate(null);
				setIssuance(null);
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change date to calculate term.
	 */
	public void changeDateToCalculateTerm(){
		try {
			if(issuanceAux.getIssuanceDate()!=null && issuanceAux.getExpirationDate()!=null){	
				if(isViewOperationModify()){
					if(issuanceAux.isSecuritiesRegisted()){
						SecurityTO securityTO=new SecurityTO();
						securityTO.setIdIssuanceCodePk( issuanceAux.getIdIssuanceCodePk() );
						securityTO.setSecurityState( SecurityStateType.REGISTERED.getCode().toString() );
						List<Security> lstSecurities=securityServiceFacade.findSecuritiesLiteServiceFacade(securityTO);
						
						for(Security security : lstSecurities){
							if(CommonsUtilities.isLessOrEqualDate(issuanceAux.getExpirationDate(), security.getExpirationDate(), true)){
								String message=PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_ERROR_EXP_DATE_LESS_SEC_EXP_DATE);
								JSFUtilities.addContextMessage("frmIssueMgmt:calIssueExpirationDate", 
																FacesMessage.SEVERITY_ERROR, 
																message, message);
								break;
							}
						}
					}
				}				
				calculateIssuanceTerm();
//				loadCboCreditRatingScalesAux();
			}else{
				setIssuanceTerm(null);
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/** retornar una fecha con HH:mm:ss 00:00:00
	 * 
	 * @param dateParameter
	 * @return */
	@SuppressWarnings("deprecation")
	private static Date getDateTrunc(Date dateParameter) {
		if(dateParameter==null){
			dateParameter=new Date();
		}
		dateParameter.setHours(0);
		dateParameter.setMinutes(0);
		dateParameter.setSeconds(0);
		return dateParameter;
	}
	
	/**
	 * metodo para retornar el tiempo de vida que le queda a una emision expresado en dias
	 * @param dateBefore
	 * @param dateAffter
	 * @return
	 */
	public int getDaysLifeTimeIssuance(Date dateBefore, Date dateAffter){
		int result = 0;
		Date dateNow = new Date();
		dateNow = getDateTrunc(dateNow);
		dateBefore = getDateTrunc(dateBefore);
		dateAffter = getDateTrunc(dateAffter);
		if (dateNow.before(dateBefore)) {
			result = CommonsUtilities.getDaysBetween(dateBefore, dateAffter);
		} else if (dateNow.after(dateAffter)) {
			result = 0;
		} else {
			result = CommonsUtilities.getDaysBetween(dateNow, dateAffter);
		}
		return result;
	}
	
	/** metodo para retornar los dias transcurridos de una emision hasta su vencimiento
	 * 
	 * @param dateBefore
	 * @param dateAffter
	 * @return */
	public int getDaysPassedIssuance(Date dateBefore, Date dateAffter) {
		int result = 0;
		Date dateNow = new Date();
		dateNow = getDateTrunc(dateNow);
		dateBefore = getDateTrunc(dateBefore);
		dateAffter = getDateTrunc(dateAffter);
		if (dateNow.before(dateBefore)) {
			result = 0;
		} else if (dateNow.after(dateAffter)) {
			result = CommonsUtilities.getDaysBetween(dateBefore, dateAffter);
		} else {
			result = CommonsUtilities.getDaysBetween(dateBefore, dateNow);
		}
		return result;
	}
	
	/**
	 * Calculate issuance term.
	 * Metodo para calcular el plazo de vida restante en pantalla de detalle
	 * @throws ServiceException the service exception
	 */
	public void calculateIssuanceTermAux() throws ServiceException{
		Integer issuanceDaysTerm=
				CommonsUtilities.getDaysBetween(issuanceAux.getIssuanceDate(), issuanceAux.getExpirationDate());
		if(issuanceAux.isDpfDpaSecurityClass()){
			if(issuanceDaysTerm.compareTo(GeneralConstants.DPA_DPF_MIN_DAYS_TERM)<GeneralConstants.ZERO_VALUE_INT){
				String message=PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_ERROR_DPA_DPF_MIN_DAYS_TERM);
				issuanceAux.setExpirationDate(null);
				setIssuanceTerm(null);
				JSFUtilities.resetComponent(SecurityMgmtType.TXT_ISSUE_EXPIRATION_DATE.getValue());
				JSFUtilities.resetComponent(SecurityMgmtType.TXT_ISSUANCE_TERM.getValue());
				JSFUtilities.addContextMessage(SecurityMgmtType.TXT_ISSUE_EXPIRATION_DATE.getValue(), 
												FacesMessage.SEVERITY_ERROR, message, message);
				return;
			}
		}
		setIssuanceTerm(issuanceDaysTerm);	
	}
	
	/**
	 * Metodo para calcular el plazo de vida restante en pantalla de detalle
	 * @throws ServiceException
	 */
	public void calculateIssuanceTerm() throws ServiceException{
		
		if (Validations.validateIsNotNullAndNotEmpty(issuance)
				&& Validations.validateIsNotNull(issuance.getIssuanceDate())
				&& Validations.validateIsNotNull(issuance.getExpirationDate())){
			Integer issuanceDaysTerm=
					CommonsUtilities.getDaysBetween(issuance.getIssuanceDate(), issuance.getExpirationDate());
			if(issuance.isDpfDpaSecurityClass()){
				if(issuanceDaysTerm.compareTo(GeneralConstants.DPA_DPF_MIN_DAYS_TERM)<GeneralConstants.ZERO_VALUE_INT){
					String message=PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_ERROR_DPA_DPF_MIN_DAYS_TERM);
					issuance.setExpirationDate(null);
					setIssuanceTerm(null);
					JSFUtilities.resetComponent(SecurityMgmtType.TXT_ISSUE_EXPIRATION_DATE.getValue());
					JSFUtilities.resetComponent(SecurityMgmtType.TXT_ISSUANCE_TERM.getValue());
					JSFUtilities.addContextMessage(SecurityMgmtType.TXT_ISSUE_EXPIRATION_DATE.getValue(), 
													FacesMessage.SEVERITY_ERROR, message, message);
					return;
				}
			}
			setIssuanceTerm(issuanceDaysTerm);	
		}else{
			Integer issuanceDaysTerm=
					CommonsUtilities.getDaysBetween(issuanceAux.getIssuanceDate(), issuanceAux.getExpirationDate());
			if(issuanceAux.isDpfDpaSecurityClass()){
				if(issuanceDaysTerm.compareTo(GeneralConstants.DPA_DPF_MIN_DAYS_TERM)<GeneralConstants.ZERO_VALUE_INT){
					String message=PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_ERROR_DPA_DPF_MIN_DAYS_TERM);
					issuance.setExpirationDate(null);
					setIssuanceTerm(null);
					JSFUtilities.resetComponent(SecurityMgmtType.TXT_ISSUE_EXPIRATION_DATE.getValue());
					JSFUtilities.resetComponent(SecurityMgmtType.TXT_ISSUANCE_TERM.getValue());
					JSFUtilities.addContextMessage(SecurityMgmtType.TXT_ISSUE_EXPIRATION_DATE.getValue(), 
													FacesMessage.SEVERITY_ERROR, message, message);
					return;
				}
			}
			setIssuanceTerm(issuanceDaysTerm);	
		}
	}
	
	/**
	 * Insert issuance term.
	 */
	public void insertIssuanceTerm(){
		try {
			if(issuance.getIssuanceDate()==null){
				setIssuanceTerm(null);
			}else{
				if(getIssuanceTerm()!=null){
					if(getIssuanceTerm().compareTo(GeneralConstants.DPA_DPF_MIN_DAYS_TERM)<GeneralConstants.ZERO_VALUE_INT){
						String message=PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_ERROR_DPA_DPF_MIN_DAYS_TERM);
						issuance.setExpirationDate(null);
						setIssuanceTerm(null);
						JSFUtilities.resetComponent(SecurityMgmtType.TXT_ISSUANCE_TERM.getValue());
						JSFUtilities.resetComponent(SecurityMgmtType.TXT_ISSUE_EXPIRATION_DATE.getValue());
						JSFUtilities.addContextMessage(SecurityMgmtType.TXT_ISSUANCE_TERM.getValue(), 
														FacesMessage.SEVERITY_ERROR, message, message);
						return;
					}
					Date newExpirationDate=CommonsUtilities.addDaysToDate(issuance.getIssuanceDate(), getIssuanceTerm());
					issuance.setExpirationDate( newExpirationDate  );
//					loadCboCreditRatingScales();
				}

			}
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	public void insertIssuanceAuxTerm(){
		try {
			if(issuanceAux.getIssuanceDate()==null){
				setIssuanceTerm(null);
			}else{
				if(getIssuanceTerm()!=null){
					if(getIssuanceTerm().compareTo(GeneralConstants.DPA_DPF_MIN_DAYS_TERM)<GeneralConstants.ZERO_VALUE_INT){
						String message=PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_ERROR_DPA_DPF_MIN_DAYS_TERM);
						issuanceAux.setExpirationDate(null);
						setIssuanceTerm(null);
						JSFUtilities.resetComponent(SecurityMgmtType.TXT_ISSUANCE_TERM.getValue());
						JSFUtilities.resetComponent(SecurityMgmtType.TXT_ISSUE_EXPIRATION_DATE.getValue());
						JSFUtilities.addContextMessage(SecurityMgmtType.TXT_ISSUANCE_TERM.getValue(), 
														FacesMessage.SEVERITY_ERROR, message, message);
						return;
					}
					Date newExpirationDate=CommonsUtilities.addDaysToDate(issuanceAux.getIssuanceDate(), getIssuanceTerm());
					issuanceAux.setExpirationDate( newExpirationDate  );
//					loadCboCreditRatingScalesAux();
				}

			}
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	public void changePlacementExpirationDays() {
		issuanceAux.setPlacementExpirationDate(null);
		if(issuanceAux.getIssuanceDate()==null){
			issuanceAux.setPlacementExpirationDays(null);
		}else{
			if(Validations.validateIsNotNullAndNotEmpty(issuanceAux.getPlacementExpirationDays())) {
				issuanceAux.setPlacementExpirationDate(CommonsUtilities.addDaysToDate(issuanceAux.getIssuanceDate(), issuanceAux.getPlacementExpirationDays()));
			}
		}
	}
	
	public void changePlacementExpirationDate() {
		issuanceAux.setPlacementExpirationDays(null);
		if(issuanceAux.getIssuanceDate()==null){
			issuanceAux.setPlacementExpirationDate(null);
		}else{
			if(Validations.validateIsNotNullAndNotEmpty(issuanceAux.getPlacementExpirationDate())) {
				Integer issuanceDaysTerm = CommonsUtilities.getDaysBetween(issuanceAux.getIssuanceDate(), issuanceAux.getPlacementExpirationDate());
				issuanceAux.setPlacementExpirationDays(issuanceDaysTerm);
			}
		}
	}
	
	
	
	
	/**
	 * Back to search page handler.
	 * @return windows search
	 * @throws ServiceException Service Exception
	 */
	public String backToSearchPageHandler() throws ServiceException{
			setViewOperationType(ViewOperationsType.CONSULT.getCode());		
			return "searchIssuanceMgmt";
	}

	
	/**
	 * Checks if is contain dtb issuance certificate.
	 *
	 * @param issuanceFile the issuance file
	 * @return true, if is contain dtb issuance certificate
	 * @throws Exception             the exception
	 */
	//issue 669 macker y checker
	@SuppressWarnings("unused")
	private boolean isContainDtbIssuanceCertificate(IssuanceFile issuanceFile) throws Exception {
		boolean contain = false;
		for (IssuanceFile objIssuanceFile : issuance.getIssuanceFiles()) {
			if (objIssuanceFile.getDocumentNumber().equals(issuanceFile.getDocumentNumber())) {
				JSFUtilities
						.addContextMessage(
								"frmIssueMgmt:txtCertificateIssueNumber",
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities
										.getMessage(PropertiesConstants.ISSUANCE_CERTIFICATE_NUMBER_REPEATED),
								PropertiesUtilities
										.getMessage(PropertiesConstants.ISSUANCE_CERTIFICATE_NUMBER_REPEATED));
				contain = true;
			}
		}
		return contain;
	}
	private boolean isContainDtbIssuanceCertificate(IssuanceFileAux issuanceFile) throws Exception {
		boolean contain = false;
		for (IssuanceFileAux objIssuanceFile : issuanceAux.getIssuanceFiles()) {
			if (objIssuanceFile.getDocumentNumber().equals(issuanceFile.getDocumentNumber())) {
				JSFUtilities
						.addContextMessage(
								"frmIssueMgmt:txtCertificateIssueNumber",
								FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities
										.getMessage(PropertiesConstants.ISSUANCE_CERTIFICATE_NUMBER_REPEATED),
								PropertiesUtilities
										.getMessage(PropertiesConstants.ISSUANCE_CERTIFICATE_NUMBER_REPEATED));
				contain = true;
			}
		}
		return contain;
	}

	/**
	 * Insert issued date handler.
	 */
	public void insertIssuedDateHandler() {
		try {
			issuanceAux.setExpirationDate(null);
			issuanceAux.setRegistryDate(null);
			issuanceAux.setResolutionDate(null);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Clean issuance mgmt.
	 */
	public void cleanIssuanceMgmt() {
		try {
			JSFUtilities.resetViewRoot();
				String issuerDes=issuanceAuxTO.getIssuerDescription();
				issuanceAux = new IssuanceAux();
				issuanceCertificateAux = new IssuanceCertificateAux();
				issuanceCertFileNameDisplay = null;
				issuanceAux.setRegistryDate(new Date());
				issuanceAux.setIndRegulatorReport( BooleanType.YES.getCode() );
				issuanceAux.setNumberPlacementTranches(Integer.valueOf(0));

				issuer = new Issuer();
				geographicLocation = new GeographicLocation();

				issuanceAux.setPlacedAmount(BigDecimal.valueOf(0.0));

				issuanceAux.setGeographicLocation(geographicLocation);
				issuanceAux.setIssuer(issuer);
				
				createObjectsByInstrument();
				
				issuanceAuxTO.setIssuerDescription(issuerDes);
				setIssuanceTerm(null);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Sum total share capital for issuance.
	 *
	 * @return the big decimal
	 * @throws ServiceException the service exception
	 */
	public BigDecimal sumTotalShareCapitalForIssuance() throws ServiceException{
		
		BigDecimal amount= new BigDecimal(0.00);
		
		SecurityTO securityTO = new SecurityTO();
		
		securityTO.setIdIssuanceCodePk(issuanceAux.getIdIssuanceCodePk());
		
		List<Security> lstSecurities = securityServiceFacade.findSecurities(securityTO);
		
		for (Security auxSecurities : lstSecurities) {
			if(!auxSecurities.isCoupon()){
				amount = auxSecurities.getShareCapital().add(amount);
			}
		}
		
 		return amount;
	}
	
	/**
	 * Load cbo boolean type.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboBooleanType() throws ServiceException{
		lstCboBooleanType=BooleanType.list;
	}
	
	/**
	 * Load cbo issuance state type.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	private void loadCboIssuanceStateType() throws ServiceException {
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.ISSUANCE_STATE
				.getCode());
		lstCboIssuanceState = generalParameterFacade
				.getListParameterTableServiceBean(parameterTableTO);
		
		// TODO: Codigo solo por la premura
//		if(lstCboIssuanceState != null && lstCboIssuanceState.size() > 0){
//			//for(ParameterTable parameterTable : lstCboIssuanceState){
//			for(int i = 0 ; i < lstCboIssuanceState.size() ; i++){
//				if("REGISTRADO".equals(lstCboIssuanceState.get(i).getParameterName()))
//					lstCboIssuanceState.get(i).setParameterName("CONFIRMADO");
//			}
//		}
		
		// cargando estados de la tabla auxiliar
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.ISSUANCE_STATE_AUX
				.getCode());
		lstCboIssuanceStateAux = generalParameterFacade
				.getListParameterTableServiceBean(parameterTableTO);
		
		// adicionando los estados del objeto Issuance Aux
		lstCboIssuanceState.addAll(lstCboIssuanceStateAux);
		
	}

	/**
	 * Load cbo instrument type.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	private void loadCboInstrumentType() throws ServiceException {
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
		lstCboInstrumentType = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		billParametersTableMap(lstCboInstrumentType);
	}

	/**
	 * Load cbo issue type.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	private void loadCboIssueType() throws ServiceException {
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.ISSUANCE_TYPE.getCode());
		lstCboIssueType = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}
	
	/**
	 * Clean Objects By Instrument.
	 */
	public void cleanObjectsByInstrument()
	{
		issuanceAux.setSecurityType(null);
		issuanceAux.setSecurityClass(null);										
		issuanceAux.setIssuanceType(null);		
//		issuance.setSecurityType(null);
//		issuance.setSecurityClass(null);										
//		issuance.setIssuanceType(null);		
	}
	
	/**
	 * Create Objects By Instrument.
	 */
	public void createObjectsByInstrument()
	{
		lstCboSecuritieType = new ArrayList<ParameterTable>();
		lstCboSecuritieClass = new ArrayList<ParameterTable>();
		lstCboIssueType=new ArrayList<ParameterTable>();
	}
	
	/**
	 * Load List Securities Type.
	 *
	 * @throws ServiceException Service Exception
	 */
	public void loadCboSecuritieType() throws ServiceException 
	{
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.SECURITIES_TYPE.getCode());
		lstCboSecuritieType = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}
	
	/**
	 * Load all securitie type.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadAllSecuritieType() throws ServiceException{
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.SECURITIES_TYPE.getCode());
		List<ParameterTable> lstCboSecuritieType = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);	
		billParametersTableMap(lstCboSecuritieType);
	}
	
	/**
	 * Remove Issue Type Physical.
	 */
	public void removeIssueTypePhysical()
	{
		for(ParameterTable pTable : lstCboIssueType){
			if(pTable.getParameterTablePk().equals( IssuanceType.PHYSICAL.getCode() )){
				lstCboIssueType.remove(pTable);
				break;
			}
		}
	}
	
	/**
	 * Load List Securities Class.
	 *
	 * @throws ServiceException Service Exception
	 */
	public void loadCboSecuritieClass() throws ServiceException 
	{
		lstCboSecuritieClass=generalParameterFacade.getListSecuritiesClassSetup(issuanceAux.getInstrumentType(), 
				issuanceAux.getIssuer().getEconomicSector(),
				issuanceAux.getSecurityType());
	}
	
	/**
	 * Load all securitie class.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadAllSecuritieClass() throws ServiceException{
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		List<ParameterTable> lstSecuritiesClass = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		billParametersTableMap(lstSecuritiesClass, true);
	}
	
	/**
	 * Get the mnemonic + name security class.
	 *
	 * @param classSecurities the class securities
	 * @return the mnemonic security class name
	 */
	public String getMnemonicSecurityClassName(Integer classSecurities){
		StringBuilder strSecuritiesClass = new StringBuilder();
		if(super.getParametersTableMap().get(classSecurities)!=null){
			ParameterTable pTable = (ParameterTable) super.getParametersTableMap().get(classSecurities);
			strSecuritiesClass.append(pTable.getText1());
			strSecuritiesClass.append(" - ");
			strSecuritiesClass.append(pTable.getParameterName());
		}
		return strSecuritiesClass.toString();
	}
	
	
	/**
	 * Load cbo securitie type by instrument handler.
	 */
	public void loadCboSecuritieTypeByInstrumentHandler() {
		try {
			parameterTableTO = new ParameterTableTO();

			if (isViewOperationModify() || isViewOperationRegister() || isViewOperationDetail()) {

				if(isViewOperationRegister()){
					cleanObjectsByInstrument();				
					changeCboIssueTypeHandler();
					changeCboHasCertificateHandler();
				}			
				
				createObjectsByInstrument();			
				
				if(issuance.getInstrumentType()==null)
					return;
				
				loadCboIssueType();	
				loadCboSecuritieType();				
					
				issuance.setExpirationDate(null);
				setIssuanceTerm(null);
				
				availableAmount = BigDecimal.ZERO;
				
			} else 	if (isViewOperationConsult()){
				cleanDataModel();
				if(Validations.validateIsNotNullAndNotEmpty(issuanceTO.getInstrumentType()))
				{
					initializeObjectsSearch();
					loadCboSecuritieTypeSearch();							
				}
				cleanDataModel();
				
				issuanceTO.setSecurityClass(null);
				lstCboSearchSecuritieClass=new ArrayList<ParameterTable>();
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	public void loadCboSecuritieTypeByInstrumentHandlerAux() {
		try {
			parameterTableTO = new ParameterTableTO();

			if (isViewOperationModify() || isViewOperationRegister() || isViewOperationDetail()) {

				if(isViewOperationRegister()){
					cleanObjectsByInstrument();				
					changeCboIssueTypeHandlerAux();
					changeCboHasCertificateHandler();
				}			
				
				createObjectsByInstrument();			
				
				if(issuanceAux.getInstrumentType()==null)
					return;
				
				loadCboIssueType();	
				loadCboSecuritieType();				
					
				issuanceAux.setExpirationDate(null);
				setIssuanceTerm(null);
				
				availableAmount = BigDecimal.ZERO;
				
			} else 	if (isViewOperationConsult()){
				cleanDataModel();
				if(Validations.validateIsNotNullAndNotEmpty(issuanceAuxTO.getInstrumentType()))
				{
					initializeObjectsSearch();
					loadCboSecuritieTypeSearch();							
				}
				cleanDataModel();
				
				issuanceAuxTO.setSecurityClass(null);
				lstCboSearchSecuritieClass=new ArrayList<ParameterTable>();
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	/**
	 * Change cbo security type search.
	 */
	public void changeCboSecurityTypeSearch(){
		try {
			cleanDataModel();
			issuanceTO.setSecurityClass(null);
			if(issuanceTO.getSecurityType()==null){
				lstCboSearchSecuritieClass=new ArrayList<ParameterTable>();
				issuanceTO.setSecurityClass(null);
			}else{
				parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
				parameterTableTO.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
				parameterTableTO.setIdRelatedParameterFk(issuanceTO.getSecurityType());
				lstCboSearchSecuritieClass = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);	
				
				lstCboSearchSecuritieClass=generalParameterFacade.getListSecuritiesClassSetup(issuanceTO.getInstrumentType(), 
						   null,
						   issuanceTO.getSecurityType());
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	public void changeCboSecurityTypeSearchAux(){
		try {
			cleanDataModel();
			issuanceAuxTO.setSecurityClass(null);
			if(issuanceAuxTO.getSecurityType()==null){
				lstCboSearchSecuritieClass=new ArrayList<ParameterTable>();
				issuanceAuxTO.setSecurityClass(null);
			}else{
				parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
				parameterTableTO.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
				parameterTableTO.setIdRelatedParameterFk(issuanceAuxTO.getSecurityType());
				lstCboSearchSecuritieClass = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);	
				
				lstCboSearchSecuritieClass=generalParameterFacade.getListSecuritiesClassSetup(issuanceAuxTO.getInstrumentType(), 
						   null,
						   issuanceAuxTO.getSecurityType());
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	/**
	 * Load List Securitie Type for Search.
	 *
	 * @throws ServiceException Service Exception
	 */
	public void loadCboSecuritieTypeSearch() throws ServiceException 
	{		
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.SECURITIES_TYPE.getCode());
		lstCboSearchSecuritieType = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);		
	}
	
	/**
	 * Create Objects for Search.
	 */
	public void createObjectsSearch()
	{
		lstCboSearchSecuritieType = new ArrayList<ParameterTable>();
		if(!userInfo.getUserAccountSession().isIssuerInstitucion() && !userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
			issuerSearch = new Issuer();
			
		}		
	}
	
	/**
	 * Initialize Objects for Search.
	 */
	public void initializeObjectsSearch()
	{
		issuanceTO.setSecurityType(null);
		issuanceAuxTO.setSecurityType(null);
	}
	/**
	 * Load cbo securitie class by securitie type handler.
	 */
	public void loadCboSecuritieClassBySecuritieTypeHandler() {
		try {
			parameterTableTO = new ParameterTableTO();
			if (isViewOperationRegister() || isViewOperationModify() || isViewOperationDetail()) {
				if(isViewOperationRegister()){
					issuance.setSecurityClass(null);
				}				
				if(issuance.getSecurityType()==null){
					lstCboSecuritieClass=new ArrayList<ParameterTable>();
					return;
				}
				loadCboSecuritieClass();
				issuance.setExpirationDate(null);
				setIssuanceTerm(null);
			} 
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	public void loadCboSecuritieClassBySecuritieTypeHandlerAux() {
		try {
			parameterTableTO = new ParameterTableTO();
			if (isViewOperationRegister() || isViewOperationModify() || isViewOperationDetail()) {
				if(isViewOperationRegister()){
					issuanceAux.setSecurityClass(null);
				}				
				if(issuanceAux.getSecurityType()==null){
					lstCboSecuritieClass=new ArrayList<ParameterTable>();
					return;
				}
				loadCboSecuritieClass();
				issuanceAux.setExpirationDate(null);
				setIssuanceTerm(null);
			} 
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	/**
	 * Change cbo security class.
	 */
	public void changeCboSecurityClass() {
		try{
			if(issuanceAux.getSecurityClass()!=null){
				if (isViewOperationRegister()){
//					loadCboCreditRatingScales();
//					loadCboCreditRatingScalesAux();
				}
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Load cbo currency type.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	public void loadCboCurrencyType() throws ServiceException {
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());

		lstCboCurrencyType = generalParameterFacade
				.getListParameterTableServiceBean(parameterTableTO);
	}

	/**
	 * Change cbo issue type handler.
	 */
	public void changeCboIssueTypeHandler() {
		try {			
			issuance.setIndPrimaryPlacement(null);
			issuance.setNumberTotalTranches(null);
			if(issuance.getInstrumentType()==null 
					|| issuance.getIssuanceType()==null){
				return;
			}
			if(isMixedIssueType()){
				/**Issue 2433*/
				if(SecurityClassType.ACC.getCode().equals(issuance.getSecurityClass())||
						SecurityClassType.ACP.getCode().equals(issuance.getSecurityClass())||
						//Acciones Preferentes
						SecurityClassType.ACC_RF.getCode().equals(issuance.getSecurityClass())||
						SecurityClassType.CFC.getCode().equals(issuance.getSecurityClass())
						){
					issuance.setIndPrimaryPlacement(BooleanType.YES.getCode());
					issuance.setNumberTotalTranches(1);	
				}else{
					issuance.setIndPrimaryPlacement(BooleanType.NO.getCode());
				}
			}else if(isPhysicalIssueType()){
				issuance.setIndPrimaryPlacement(BooleanType.NO.getCode());
			}else if(isDesmaterializedIssueType()){
				issuance.setIndPrimaryPlacement(BooleanType.YES.getCode());
				issuance.setNumberTotalTranches(1);
			}
			changeCboIndPlacementTranche();
			if(isDesmaterializedIssueType()){
				issuance.setPlacementType(SecurityPlacementType.FIXED.getCode());
			}else if(isMixedIssueType()&&
					(SecurityClassType.ACC.getCode().equals(issuance.getSecurityClass())||
						SecurityClassType.ACP.getCode().equals(issuance.getSecurityClass())||
						SecurityClassType.CFC.getCode().equals(issuance.getSecurityClass())
						)){
				issuance.setPlacementType(SecurityPlacementType.FIXED.getCode());
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	public void changeCboIssueTypeHandlerAux() {
		try {			
			issuanceAux.setIndPrimaryPlacement(null);
			issuanceAux.setNumberTotalTranches(null);
			if(issuanceAux.getInstrumentType()==null 
					|| issuanceAux.getIssuanceType()==null){
				return;
			}
			if(isMixedIssueTypeAux()){
				/**Issue 2433*/
				if(SecurityClassType.ACC.getCode().equals(issuanceAux.getSecurityClass())||
						SecurityClassType.ACP.getCode().equals(issuanceAux.getSecurityClass())||
						//Acciones Preferentes
						SecurityClassType.ACC_RF.getCode().equals(issuanceAux.getSecurityClass())||
						SecurityClassType.CFC.getCode().equals(issuanceAux.getSecurityClass())
						){
					issuanceAux.setIndPrimaryPlacement(BooleanType.YES.getCode());
					issuanceAux.setNumberTotalTranches(1);	
				}else{
					issuanceAux.setIndPrimaryPlacement(BooleanType.NO.getCode());
				}
			}else if(isPhysicalIssueTypeAux()){
				issuanceAux.setIndPrimaryPlacement(BooleanType.NO.getCode());
			}else if(isDesmaterializedIssueTypeAux()){
				issuanceAux.setIndPrimaryPlacement(BooleanType.YES.getCode());
				issuanceAux.setNumberTotalTranches(1);
			}
			changeCboIndPlacementTrancheAux();
			if(isDesmaterializedIssueTypeAux()){
				issuanceAux.setPlacementType(SecurityPlacementType.FIXED.getCode());
			}else if(isMixedIssueTypeAux()&&
					(SecurityClassType.ACC.getCode().equals(issuanceAux.getSecurityClass())||
						SecurityClassType.ACP.getCode().equals(issuanceAux.getSecurityClass())||
						SecurityClassType.CFC.getCode().equals(issuanceAux.getSecurityClass())
						)){
				issuanceAux.setPlacementType(SecurityPlacementType.FIXED.getCode());
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Bill parameters table map.
	 *
	 * @param lstParameterTable the lst parameter table
	 */
	public void billParametersTableMap(List<ParameterTable> lstParameterTable){
		billParametersTableMap(lstParameterTable,false);
	}
	
	/**
	 * Bill parameters table map.
	 *
	 * @param lstParameterTable the lst parameter table
	 * @param isObj the is obj
	 */
	public void billParametersTableMap(List<ParameterTable> lstParameterTable, boolean isObj){
		if(super.getParametersTableMap()==null){
			super.setParametersTableMap(new HashMap<Integer, Object>());
		}
		for(ParameterTable pTable : lstParameterTable){
			if(isObj){
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable);
			}else{
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable.getParameterName());
			}
		}
	}
	
	/**
	 * Bill parameter table by id.
	 *
	 * @param parameterPk the parameter pk
	 * @throws ServiceException the service exception
	 */
	public void billParameterTableById(Integer parameterPk) throws ServiceException{
		billParameterTableById(parameterPk, false);
	}
	
	/**
	 * Bill parameter table by id.
	 *
	 * @param parameterPk the parameter pk
	 * @param isObj the is obj
	 * @throws ServiceException the service exception
	 */
	public void billParameterTableById(Integer parameterPk, boolean isObj) throws ServiceException{
		ParameterTable pTable = generalParameterFacade.getParamDetailServiceFacade(parameterPk);
		if(pTable!=null){
			if(isObj){
				super.getParametersTableMap().put(pTable.getParameterTablePk()  , pTable);
			}else{
				super.getParametersTableMap().put(pTable.getParameterTablePk()  , pTable.getParameterName());
			}
		}
	}
	
	/**
	 * Load cbo geographic location.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	public void loadCboGeographicLocation() throws ServiceException {
		GeographicLocationTO geoLocation = new GeographicLocationTO();
		geoLocation.setGeographicLocationType(GeographicLocationType.COUNTRY
				.getCode());
		geoLocation.setState(GeographicLocationStateType.REGISTERED.getCode());
		lstCboGeographicLocation = generalParameterFacade
				.getListGeographicLocationServiceFacade(geoLocation);
	}

	/**
	 * Load cbo credit rating scales.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	public void loadCboCreditRatingScales() throws ServiceException {
//		Integer idMasterTable = null;
//		Integer discriminator = null;
		lstCboCreditRatingScales = null;
		
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.CREDIT_RATING_SCALES_ISSUER.getCode());
		
		lstCboCreditRatingScales = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);

		for (ParameterTable param : lstCboCreditRatingScales) {
			getParametersTableMap().put(param.getParameterTablePk(), param.getParameterName());
		}
//		
//		
//		if(Validations.validateIsNotNullAndPositive(issuance.getInstrumentType())){
//			if(issuance.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode())){
//				if(Validations.validateIsNotNullAndPositive(issuance.getSecurityClass())){
//					if(!issuance.getSecurityClass().equals(SecurityClassType.ACC_RF.getCode())){
//						if(Validations.validateIsNotNullAndPositive(issuanceTerm)){
//							if(issuanceTerm.compareTo(ComponentConstant.ONE) >= 0 && issuanceTerm.compareTo(CalendarDayType._360.getIntegerValue()) <= 0 ){
//								idMasterTable = MasterTableType.CREDIT_RATING_SCALES_SHORT_TERM.getCode();
//							}else if (issuanceTerm.compareTo(CalendarDayType._360.getIntegerValue()) > 0){
//								idMasterTable = MasterTableType.CREDIT_RATING_SCALES_LONG_TERM.getCode();
//							}
//						}
//					}else{
//						idMasterTable = MasterTableType.CREDIT_RATING_SCALES_PREFERED_STOCK.getCode();
//					}
//				}
//			}else if(issuance.getInstrumentType().equals(InstrumentType.MIXED_INCOME.getCode())){
//				if(Validations.validateIsNotNullAndPositive(issuanceTerm)){
//					if(issuanceTerm.compareTo(ComponentConstant.ONE) >= 0 && issuanceTerm.compareTo(CalendarDayType._360.getIntegerValue()) <= 0 ){
//						idMasterTable = MasterTableType.CREDIT_RATING_SCALES_SHORT_TERM.getCode();
//					}else if (issuanceTerm.compareTo(CalendarDayType._360.getIntegerValue()) > 0){
//						idMasterTable = MasterTableType.CREDIT_RATING_SCALES_LONG_TERM.getCode();
//					}
//				}
//			}else if(issuance.getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode())){
//				if(Validations.validateIsNotNullAndPositive(issuance.getSecurityClass())){
//					if(!issuance.getSecurityClass().equals(SecurityClassType.CFC.getCode())){
//						idMasterTable = MasterTableType.CREDIT_RATING_SCALES_EQUITIES.getCode();
//					}else{
//						idMasterTable = MasterTableType.CREDIT_RATING_SCALES_LONG_TERM.getCode();
//					}
//				}
//				
//			}
//			
//			if(idMasterTable!=null){
//				ParameterTableTO parameterTableTO = new ParameterTableTO();
//				parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
//				parameterTableTO.setMasterTableFk(idMasterTable);
//				parameterTableTO.setOrderByText1(ComponentConstant.ONE);
//				parameterTableTO.setIndicator5(discriminator);
//				lstCboCreditRatingScales = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
//			}
//		}
//		
//		// issue 1239: a requerimeinto de usuario si es un BBX, BTX su calificacion siempre es AAA
//		if(Validations.validateIsNotNull(issuance.getSecurityClass())
//			 && ( issuance.getSecurityClass().equals(SecurityClassType.BBX.getCode()) 
//				  || issuance.getSecurityClass().equals(SecurityClassType.BTX.getCode()) )
//			){
//			ParameterTableTO parameterTableTO = new ParameterTableTO();
//			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
//			parameterTableTO.setMasterTableFk(MasterTableType.CREDIT_RATING_SCALES_LONG_TERM.getCode());
//			parameterTableTO.setOrderByText1(ComponentConstant.ONE);
//			parameterTableTO.setIndicator5(discriminator);
//			lstCboCreditRatingScales = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
//		}
//		
//		if(isViewOperationDetail() || isViewOperationModify()){
//			if(Validations.validateListIsNotNullAndNotEmpty(lstCboCreditRatingScales)){
//				for (ParameterTable param : lstCboCreditRatingScales) {
//					getParametersTableMap().put(param.getParameterTablePk(), param.getParameterName());
//				}
//			}
//		}else{
//			issuance.setCreditRatingScales(null);
//		}
//		
	}
//	public void loadCboCreditRatingScalesAux() throws ServiceException {
//		Integer idMasterTable = null;
//		Integer discriminator = null;
//		lstCboCreditRatingScales = null;
//		if(Validations.validateIsNotNullAndPositive(issuanceAux.getInstrumentType())){
//			if(issuanceAux.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode())){
//				if(Validations.validateIsNotNullAndPositive(issuanceAux.getSecurityClass())){
//					if(!issuanceAux.getSecurityClass().equals(SecurityClassType.ACC_RF.getCode())){
//						if(Validations.validateIsNotNullAndPositive(issuanceTerm)){
//							if(issuanceTerm.compareTo(ComponentConstant.ONE) >= 0 && issuanceTerm.compareTo(CalendarDayType._360.getIntegerValue()) <= 0 ){
//								idMasterTable = MasterTableType.CREDIT_RATING_SCALES_SHORT_TERM.getCode();
//							}else if (issuanceTerm.compareTo(CalendarDayType._360.getIntegerValue()) > 0){
//								idMasterTable = MasterTableType.CREDIT_RATING_SCALES_LONG_TERM.getCode();
//							}
//						}
//					}else{
//						idMasterTable = MasterTableType.CREDIT_RATING_SCALES_PREFERED_STOCK.getCode();
//					}
//				}
//			}else if(issuanceAux.getInstrumentType().equals(InstrumentType.MIXED_INCOME.getCode())){
//				if(Validations.validateIsNotNullAndPositive(issuanceTerm)){
//					if(issuanceTerm.compareTo(ComponentConstant.ONE) >= 0 && issuanceTerm.compareTo(CalendarDayType._360.getIntegerValue()) <= 0 ){
//						idMasterTable = MasterTableType.CREDIT_RATING_SCALES_SHORT_TERM.getCode();
//					}else if (issuanceTerm.compareTo(CalendarDayType._360.getIntegerValue()) > 0){
//						idMasterTable = MasterTableType.CREDIT_RATING_SCALES_LONG_TERM.getCode();
//					}
//				}
//			}else if(issuanceAux.getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode())){
//				if(Validations.validateIsNotNullAndPositive(issuanceAux.getSecurityClass())){
//					if(!issuanceAux.getSecurityClass().equals(SecurityClassType.CFC.getCode())){
//						idMasterTable = MasterTableType.CREDIT_RATING_SCALES_EQUITIES.getCode();
//					}else{
//						idMasterTable = MasterTableType.CREDIT_RATING_SCALES_LONG_TERM.getCode();
//					}
//				}
//				
//			}
//			
//			if(idMasterTable!=null){
//				ParameterTableTO parameterTableTO = new ParameterTableTO();
//				parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
//				parameterTableTO.setMasterTableFk(idMasterTable);
//				parameterTableTO.setOrderByText1(ComponentConstant.ONE);
//				parameterTableTO.setIndicator5(discriminator);
//				lstCboCreditRatingScales = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
//			}
//		}
//		
//		// issue 1239: a requerimeinto de usuario si es un BBX, BTX su calificacion siempre es AAA
//		if(Validations.validateIsNotNull(issuanceAux.getSecurityClass())
//			 && ( issuanceAux.getSecurityClass().equals(SecurityClassType.BBX.getCode()) 
//				  || issuanceAux.getSecurityClass().equals(SecurityClassType.BTX.getCode()) )
//			){
//			ParameterTableTO parameterTableTO = new ParameterTableTO();
//			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
//			parameterTableTO.setMasterTableFk(MasterTableType.CREDIT_RATING_SCALES_LONG_TERM.getCode());
//			parameterTableTO.setOrderByText1(ComponentConstant.ONE);
//			parameterTableTO.setIndicator5(discriminator);
//			lstCboCreditRatingScales = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
//		}
//		
//		if(isViewOperationDetail() || isViewOperationModify()
//				|| isViewOperationReview() ||isViewOperationApprove() || isViewOperationReject() || isViewOperationConfirm()
//				){
//			if(Validations.validateListIsNotNullAndNotEmpty(lstCboCreditRatingScales)){
//				for (ParameterTable param : lstCboCreditRatingScales) {
//					getParametersTableMap().put(param.getParameterTablePk(), param.getParameterName());
//				}
//			}
//		}else{
//			issuanceAux.setCreditRatingScales(null);
//		}
//		
//	}
	/**
	 * Metodo para validar que el monto de la emision del BCB no sea mayor a la sumatoria del monto de sus valores , excepto BBX o BTX issue 578
	 * @return
	 */
	public void isIssuanceClassBCB() {
		
		if(Validations.validateIsNotNullAndNotEmpty(issuanceAux.getIssuanceAmount())){
			
			if(SecurityClassType.BTS.getCode().equals(issuanceAux.getSecurityClass())
					|| SecurityClassType.LTS.getCode().equals(issuanceAux.getSecurityClass())
					|| SecurityClassType.BBS.getCode().equals(issuanceAux.getSecurityClass())
					|| SecurityClassType.LBS.getCode().equals(issuanceAux.getSecurityClass())
					|| SecurityClassType.BRS.getCode().equals(issuanceAux.getSecurityClass())
					|| SecurityClassType.LRS.getCode().equals(issuanceAux.getSecurityClass())
					){
				
				BigDecimal sumSecIssuBcb = issuanceSecuritiesServiceFacade.getSumAmountSecBcb(issuanceAux.getIdIssuanceCodePk());
				if(sumSecIssuBcb.compareTo(issuanceAux.getIssuanceAmount())>0){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage("issuance.lbl.issuance.sec.bcb.maximum"));
							JSFUtilities.showSimpleValidationDialog();
							issuanceAux.setIssuanceAmount(null);
					JSFUtilities.resetComponent("frmIssueMgmt:txtIssuedAmount");
					return;
				}
			}
		}
	}
	
	
	
	
	/**
	 * Load cbo offer type.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboOfferType() throws ServiceException {
		
	}

	/**
	 * Load cbo security placement type.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboSecurityPlacementType() throws ServiceException {
		lstCboSecurityPlacementType = SecurityPlacementType
				.listIssuancePlacementType();
	}

	// Cargar Combos Fin

	/**
	 * Hide dialogs.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public void hideDialogs() throws Exception {
		JSFUtilities.hideGeneralDialogues();
	}

	/**
	 * Hide required dialog listener.
	 */
	public void hideRequiredDialogListener() {
		try {
			JSFUtilities.hideComponent("cnfMsgRequiredValidation");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	// Start -- Variables used for dynamic combobox in the view page Start

	/**
	 * Checks if is issuer bc.
	 *
	 * @return true, if is issuer bc
	 */
	public boolean isIssuerBC(){
		if(idIssuerBC.equals( issuanceAux.getIssuer().getIdIssuerPk() )){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is physical issue type.
	 * 
	 * @return true, if is physical issue type
	 */
//	TODO: reemplazar metodos issuanceAux
	public boolean isPhysicalIssueType() {
		if (issuance.getIssuanceType() != null
				&& issuance.getIssuanceType().equals(
						IssuanceType.PHYSICAL.getCode())) {
			return true;
		}
		return false;
	}
	public boolean isPhysicalIssueTypeAux() {
		if (issuanceAux.getIssuanceType() != null
				&& issuanceAux.getIssuanceType().equals(
						IssuanceType.PHYSICAL.getCode())) {
			return true;
		}
		return false;
	}
	/**
	 * Checks if is mixed issue type.
	 * 
	 * @return true, if is mixed issue type
	 */
	public boolean isMixedIssueType() {
		if (issuance.getIssuanceType() != null
				&& issuance.getIssuanceType().equals(
						IssuanceType.MIXED.getCode())) {
			return true;
		}
		return false;
	}
	public boolean isMixedIssueTypeAux() {
		if (issuanceAux.getIssuanceType() != null
				&& issuanceAux.getIssuanceType().equals(
						IssuanceType.MIXED.getCode())) {
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is desmaterialized issue type.
	 *
	 * @return true, if is desmaterialized issue type
	 */
	public boolean isDesmaterializedIssueType(){
		if(issuance.getIssuanceType()!=null
				&& issuance.getIssuanceType().equals(  IssuanceType.DEMATERIALIZED.getCode() )){
			return true;
		}
		return false;
	}
	public boolean isDesmaterializedIssueTypeAux(){
		if(issuanceAux.getIssuanceType()!=null
				&& issuanceAux.getIssuanceType().equals(  IssuanceType.DEMATERIALIZED.getCode() )){
			return true;
		}
		return false;
	}
	/**
	 * Checks if is ind have certificate.
	 *
	 * @return true, if is ind have certificate
	 */
	public boolean isIndHaveCertificate(){
		if(issuance.getIndHaveCertificate()!=null && 
				issuance.getIndHaveCertificate().equals( BooleanType.YES.getCode() )){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	public boolean isIndHaveCertificateAux(){
//		if(issuanceAux.getIndHaveCertificate()!=null && 
//				issuanceAux.getIndHaveCertificate().equals( BooleanType.YES.getCode() )){
//			return Boolean.TRUE;
//		}
		if (issuanceFileCertificate == 1) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	/**
	 * Checks if is ind primary placement.
	 *
	 * @return true, if is ind primary placement
	 */
	public boolean isIndPrimaryPlacement(){
		if(issuance.getIndPrimaryPlacement()!=null && 
				issuance.getIndPrimaryPlacement().equals( BooleanType.YES.getCode() )){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	public boolean isIndPrimaryPlacementAux(){
		if(issuanceAux.getIndPrimaryPlacement()!=null && 
				issuanceAux.getIndPrimaryPlacement().equals( BooleanType.YES.getCode() )){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	/**
	 * Checks if is fixed income instrument type.
	 * 
	 * @return true, if is fixed income instrument type
	 */
	public boolean isFixedIncomeInstrumentType() {
		if (issuance != null) {
			if ((issuance.getInstrumentType() != null && issuance.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode()))) {
				return true;
			}
		}
		return false;
	}

	public boolean isFixedIncomeInstrumentTypeAux() {
		if (issuanceAux != null) {
			if ((issuanceAux.getInstrumentType() != null && issuanceAux.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode()))) {
				return true;
			}
		}
		return false;
	}
	/**
	 * Checks if is fixed income instrument type for search.
	 * 
	 * @return true, if is fixed income instrument type
	 */
	public boolean isFixedIncomeInstrumentTypeTo() {
		if (issuanceTO.getInstrumentType() != null
				&& issuanceTO.getInstrumentType().equals(
						InstrumentType.FIXED_INCOME.getCode())) {
			return true;
		}
		return false;
	}
	public boolean isFixedIncomeInstrumentTypeToAux() {
		if (issuanceAuxTO.getInstrumentType() != null
				&& issuanceAuxTO.getInstrumentType().equals(
						InstrumentType.FIXED_INCOME.getCode())) {
			return true;
		}
		return false;
	}
	/**
	 * Checks if is mixed income instrument type.
	 * 
	 * @return true, if is mixed income instrument type
	 *         isMixedIncomeInstrumentType
	 */
	public boolean isVariableIncomeInstrumentType() {
		if(issuance!=null){
			if (issuance.getInstrumentType() != null
					&& issuance.getInstrumentType().equals(
							InstrumentType.VARIABLE_INCOME.getCode())) {
				return true;
			}
		}
		return false;
	}
	public boolean isVariableIncomeInstrumentTypeAux() {
		if (issuanceAux.getInstrumentType() != null
				&& issuanceAux.getInstrumentType().equals(
						InstrumentType.VARIABLE_INCOME.getCode())) {
			return true;
		}
		return false;
	}
	/**
	 * Checks if is issue type participation installments.
	 *
	 * @return true, if is issue type participation installments
	 */
	public boolean isIssueTypeParticipationInstallments(){
		if(issuance.getSecurityType()!=null
				&& issuance.getSecurityType().equals( 
					  SecurityType.PAR_QUO.getCode()	)){
			return true;
		}
		return false;
	}
	public boolean isIssueTypeParticipationInstallmentsAux(){
		if(issuanceAux.getSecurityType()!=null
				&& issuanceAux.getSecurityType().equals( 
					  SecurityType.PAR_QUO.getCode()	)){
			return true;
		}
		return false;
	}
	/**
	 * Method for logout the user session on idle time of the application.
	 */
	public void sessionLogout(){
		try{
			JSFUtilities.setHttpSessionAttribute(GeneralConstants.LOGOUT_MOTIVE, LogoutMotiveType.EXPIREDSESSION);
			JSFUtilities.killSession(); 
			JSFUtilities.showMessageOnDialog(null, null,PropertiesConstants.IDLE_TIME_EXPIRED, null);
			JSFUtilities.showComponent(":idCnfDialogKillSession");
		} catch (Exception ex) {
				excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	// End -- Variables used for dynamic combobox in the view page

	/**
	 * Gets the issuance.
	 * 
	 * @return the issuance
	 */
	public Issuance getIssuance() {
		return issuance;
	}
	public IssuanceAux getIssuanceAux() {
		return issuanceAux;
	}

	/**
	 * Sets the issuance.
	 * 
	 * @param issuance
	 *            the new issuance
	 */
	public void setIssuance(Issuance issuance) {
		this.issuance = issuance;
	}
	public void setIssuanceAux(IssuanceAux issuance) {
		this.issuanceAux = issuance;
	}

	/**
	 * Gets the issuance certificate.
	 * 
	 * @return the issuance certificate
	 */
	public IssuanceCertificate getIssuanceCertificate() {
		return issuanceCertificate;
	}
	public IssuanceCertificateAux getIssuanceCertificateAux() {
		return issuanceCertificateAux;
	}
	/**
	 * Sets the issuance certificate.
	 * 
	 * @param issuanceCertificate
	 *            the new issuance certificate
	 */
	public void setIssuanceCertificate(IssuanceCertificate issuanceCertificate) {
		this.issuanceCertificate = issuanceCertificate;
	}
	public void setIssuanceCertificateAux(IssuanceCertificateAux issuanceCertificate) {
		this.issuanceCertificateAux = issuanceCertificate;
	}
	/**
	 * Gets the lst cbo instrument type.
	 * 
	 * @return the lst cbo instrument type
	 */
	public List<ParameterTable> getLstCboInstrumentType() {
		return lstCboInstrumentType;
	}

	/**
	 * Sets the lst cbo instrument type.
	 * 
	 * @param lstCboInstrumentType
	 *            the new lst cbo instrument type
	 */
	public void setLstCboInstrumentType(
			List<ParameterTable> lstCboInstrumentType) {
		this.lstCboInstrumentType = lstCboInstrumentType;
	}

	/**
	 * Gets the lst cbo securitie type.
	 * 
	 * @return the lst cbo securitie type
	 */
	public List<ParameterTable> getLstCboSecuritieType() {
		return lstCboSecuritieType;
	}

	/**
	 * Sets the lst cbo securitie type.
	 * 
	 * @param lstCboSecuritieType
	 *            the new lst cbo securitie type
	 */
	public void setLstCboSecuritieType(List<ParameterTable> lstCboSecuritieType) {
		this.lstCboSecuritieType = lstCboSecuritieType;
	}

	/**
	 * Gets the lst cbo securitie class.
	 * 
	 * @return the lst cbo securitie class
	 */
	public List<ParameterTable> getLstCboSecuritieClass() {
		return lstCboSecuritieClass;
	}

	/**
	 * Sets the lst cbo securitie class.
	 * 
	 * @param lstCboSecuritieClass
	 *            the new lst cbo securitie class
	 */
	public void setLstCboSecuritieClass(
			List<ParameterTable> lstCboSecuritieClass) {
		this.lstCboSecuritieClass = lstCboSecuritieClass;
	}

	/**
	 * Gets the lst cbo issue type.
	 * 
	 * @return the lst cbo issue type
	 */
	public List<ParameterTable> getLstCboIssueType() {
		return lstCboIssueType;
	}

	/**
	 * Sets the lst cbo issue type.
	 * 
	 * @param lstCboIssueType
	 *            the new lst cbo issue type
	 */
	public void setLstCboIssueType(List<ParameterTable> lstCboIssueType) {
		this.lstCboIssueType = lstCboIssueType;
	}


	/**
	 * Gets the lst cbo currency type.
	 * 
	 * @return the lst cbo currency type
	 */
	public List<ParameterTable> getLstCboCurrencyType() {
		return lstCboCurrencyType;
	}

	/**
	 * Sets the lst cbo currency type.
	 * 
	 * @param lstCboCurrencyType
	 *            the new lst cbo currency type
	 */
	public void setLstCboCurrencyType(List<ParameterTable> lstCboCurrencyType) {
		this.lstCboCurrencyType = lstCboCurrencyType;
	}

	/**
	 * Gets the lst cbo geographic location.
	 * 
	 * @return the lst cbo geographic location
	 */
	public List<GeographicLocation> getLstCboGeographicLocation() {
		return lstCboGeographicLocation;
	}

	/**
	 * Sets the lst cbo geographic location.
	 * 
	 * @param lstCboGeographicLocation
	 *            the new lst cbo geographic location
	 */
	public void setLstCboGeographicLocation(
			List<GeographicLocation> lstCboGeographicLocation) {
		this.lstCboGeographicLocation = lstCboGeographicLocation;
	}


	/**
	 * Gets the available amount.
	 *
	 * @return the available amount
	 */
	public BigDecimal getAvailableAmount() {
		return availableAmount;
	}

	/**
	 * Sets the available amount.
	 *
	 * @param availableAmount the new available amount
	 */
	public void setAvailableAmount(BigDecimal availableAmount) {
		this.availableAmount = availableAmount;
	}

	/**
	 * Gets the issuance to.
	 * 
	 * @return the issuance to
	 */
	public IssuanceTO getIssuanceTO() {
		return issuanceTO;
	}
	public IssuanceAuxTO getIssuanceAuxTO() {
		return issuanceAuxTO;
	}
	/**
	 * Sets the issuance to.
	 * 
	 * @param issuanceTO
	 *            the new issuance to
	 */
	public void setIssuanceTO(IssuanceTO issuanceTO) {
		this.issuanceTO = issuanceTO;
	}
	public void setIssuanceAuxTO(IssuanceAuxTO issuanceTO) {
		this.issuanceAuxTO = issuanceTO;
	}
	/**
	 * Gets the issuance data model.
	 * 
	 * @return the issuance data model
	 */
	public IssuanceDataModel getIssuanceDataModel() {
		return issuanceDataModel;
	}	
	/**
	 * Sets the issuance data model.
	 * 
	 * @param issuanceDataModel
	 *            the new issuance data model
	 */
	public void setIssuanceDataModel(IssuanceDataModel issuanceDataModel) {
		this.issuanceDataModel = issuanceDataModel;
	}
	/**
	 * Gets the lst cbo search securitie type.
	 * 
	 * @return the lst cbo search securitie type
	 */
	public List<ParameterTable> getLstCboSearchSecuritieType() {
		return lstCboSearchSecuritieType;
	}

	/**
	 * Sets the lst cbo search securitie type.
	 * 
	 * @param lstCboSearchSecuritieType
	 *            the new lst cbo search securitie type
	 */
	public void setLstCboSearchSecuritieType(
			List<ParameterTable> lstCboSearchSecuritieType) {
		this.lstCboSearchSecuritieType = lstCboSearchSecuritieType;
	}

	/**
	 * Gets the lst dtb issuances.
	 * 
	 * @return the lst dtb issuances
	 */
	public List<Issuance> getLstDtbIssuances() {
		return lstDtbIssuances;
	}
	public List<IssuanceAux> getLstDtbIssuancesAux() {
		return lstDtbIssuancesAux;
	}

	/**
	 * Sets the lst dtb issuances.
	 * 
	 * @param lstDtbIssuances
	 *            the new lst dtb issuances
	 */
	public void setLstsDtbIssuances(List<Issuance> lstDtbIssuances) {
		this.lstDtbIssuances = lstDtbIssuances;
	}
	public void setLstDtbIssuances(List<IssuanceAux> lstDtbIssuances) {
		this.lstDtbIssuancesAux = lstDtbIssuances;
	}
	/**
	 * Gets the parameter table to.
	 * 
	 * @return the parameter table to
	 */
	public ParameterTableTO getParameterTableTO() {
		return parameterTableTO;
	}

	/**
	 * Sets the parameter table to.
	 * 
	 * @param parameterTableTO
	 *            the new parameter table to
	 */
	public void setParameterTableTO(ParameterTableTO parameterTableTO) {
		this.parameterTableTO = parameterTableTO;
	}

	/**
	 * Gets the lst cbo issuance state.
	 * 
	 * @return the lst cbo issuance state
	 */
	public List<ParameterTable> getLstCboIssuanceState() {
		return lstCboIssuanceState;
	}

	/**
	 * Sets the lst cbo issuance state.
	 * 
	 * @param lstCboIssuanceState
	 *            the new lst cbo issuance state
	 */
	public void setLstCboIssuanceState(List<ParameterTable> lstCboIssuanceState) {
		this.lstCboIssuanceState = lstCboIssuanceState;
	}

	/**
	 * Gets the placed balance.
	 * 
	 * @return the placed balance
	 */
	public Double getPlacedBalance() {
		return placedBalance;
	}

	/**
	 * Sets the placed balance.
	 * 
	 * @param placedBalance
	 *            the new placed balance
	 */
	public void setPlacedBalance(Double placedBalance) {
		this.placedBalance = placedBalance;
	}

	/**
	 * Gets the lst cbo security placement type.
	 *
	 * @return the lst cbo security placement type
	 */
	public List<SecurityPlacementType> getLstCboSecurityPlacementType() {
		return lstCboSecurityPlacementType;
	}

	/**
	 * Sets the lst cbo security placement type.
	 *
	 * @param lstCboSecurityPlacementType the new lst cbo security placement type
	 */
	public void setLstCboSecurityPlacementType(
			List<SecurityPlacementType> lstCboSecurityPlacementType) {
		this.lstCboSecurityPlacementType = lstCboSecurityPlacementType;
	}

	/**
	 * Gets the lst cbo credit rating scales.
	 *
	 * @return the lst cbo credit rating scales
	 */
	public List<ParameterTable> getLstCboCreditRatingScales() {
		return lstCboCreditRatingScales;
	}

	
	
	/**
	 * Gets the lst offer type.
	 *
	 * @return the lst offer type
	 */
	public List<ParameterTable> getLstOfferType() {
		return lstOfferType;
	}
	
	/**
	 * Sets the lst offer type.
	 *
	 * @param lstOfferType the new lst offer type
	 */
	public void setLstOfferType(List<ParameterTable> lstOfferType) {
		this.lstOfferType = lstOfferType;
	}
	/**
	 * Sets the lst cbo credit rating scales.
	 *
	 * @param lstCboCreditRatingScales the new lst cbo credit rating scales
	 */
	public void setLstCboCreditRatingScales(
			List<ParameterTable> lstCboCreditRatingScales) {
		this.lstCboCreditRatingScales = lstCboCreditRatingScales;
	}

	/**
	 * Gets the issuance certificate to delete.
	 *
	 * @return the issuance certificate to delete
	 */
	public IssuanceCertificate getIssuanceCertificateToDelete() {
		return issuanceCertificateToDelete;
	}
	public IssuanceCertificateAux getIssuanceAuxCertificateToDelete() {
		return issuanceCertificateToDeleteAux;
	}
	/**
	 * Sets the issuance certificate to delete.
	 *
	 * @param issuanceCertificateToDelete the new issuance certificate to delete
	 */
	public void setIssuanceCertificateToDelete(
			IssuanceCertificate issuanceCertificateToDelete) {
		this.issuanceCertificateToDelete = issuanceCertificateToDelete;
	}
	public void setIssuanceCertificateToDelete(
			IssuanceCertificateAux issuanceCertificateToDelete) {
		this.issuanceCertificateToDeleteAux = issuanceCertificateToDelete;
	}
	/**
	 * Gets the issuance history balance.
	 *
	 * @return the issuance history balance
	 */
	public IssuanceHistoryBalance getIssuanceHistoryBalance() {
		return issuanceHistoryBalance;
	}
	public IssuanceHistoryBalanceAux getIssuanceAuxHistoryBalance() {
		return issuanceHistoryBalanceAux;
	}
	/**
	 * Sets the issuance history balance.
	 *
	 * @param issuanceHistoryBalance the new issuance history balance
	 */
	public void setIssuanceHistoryBalance(
			IssuanceHistoryBalance issuanceHistoryBalance) {
		this.issuanceHistoryBalance = issuanceHistoryBalance;
	}
	public void setIssuanceHistoryBalance(
			IssuanceHistoryBalanceAux issuanceHistoryBalance) {
		this.issuanceHistoryBalanceAux = issuanceHistoryBalance;
	}
	/**
	 * Gets the issuance cert file name display.
	 *
	 * @return the issuance cert file name display
	 */
	public String getIssuanceCertFileNameDisplay() {
		return issuanceCertFileNameDisplay;
	}

	/**
	 * Sets the issuance cert file name display.
	 *
	 * @param issuanceCertFileNameDisplay the new issuance cert file name display
	 */
	public void setIssuanceCertFileNameDisplay(String issuanceCertFileNameDisplay) {
		this.issuanceCertFileNameDisplay = issuanceCertFileNameDisplay;
	}

	/**
	 * Gets the lst cbo boolean type.
	 *
	 * @return the lst cbo boolean type
	 */
	public List<BooleanType> getLstCboBooleanType() {
		return lstCboBooleanType;
	}

	/**
	 * Sets the lst cbo boolean type.
	 *
	 * @param lstCboBooleanType the new lst cbo boolean type
	 */
	public void setLstCboBooleanType(List<BooleanType> lstCboBooleanType) {
		this.lstCboBooleanType = lstCboBooleanType;
	}

	/**
	 * Gets the issuance certificate total amount.
	 *
	 * @return the issuance certificate total amount
	 */
	public BigDecimal getIssuanceCertificateTotalAmount() {
		return issuanceCertificateTotalAmount;
	}

	
	/**
	 * get User Information.
	 *
	 * @return object User Information
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}
	
	/**
	 * set User Information.
	 *
	 * @param userInfo object User Information
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	
	/**
	 * Limit min date expiration date.
	 *
	 * @return the date
	 */
	public Date limitMinDateExpirationDate()
	{
		Date result=null;
		if(issuanceAux.getIssuanceDate()!=null)
			result = CommonsUtilities.addDate(issuanceAux.getIssuanceDate(), 1)  ;
		return result;
	}
	
	/**
	 * Limit max date issuance date.
	 *
	 * @return the date
	 */
	public Date limitMaxDateIssuanceDate()
	{
		Date result=null;
		if(issuanceAux.getExpirationDate()!=null)
			result= CommonsUtilities.addDate(issuanceAux.getExpirationDate(), -1)  ;
		return result;
	}
	
	/**
	 * Get Issuer Search.
	 *
	 * @return object Issuer
	 */
	public Issuer getIssuerSearch() {
		return issuerSearch;
	}
	
	/**
	 * Set Issuer Search.
	 *
	 * @param issuerSearch object Issuer
	 */
	public void setIssuerSearch(Issuer issuerSearch) {
		this.issuerSearch = issuerSearch;
	}
	
	/**
	 * Gets the issuance term.
	 *
	 * @return the issuance term
	 */
	public Integer getIssuanceTerm() {
		return issuanceTerm;
	}
	
	/**
	 * Sets the issuance term.
	 *
	 * @param issuanceTerm the new issuance term
	 */
	public void setIssuanceTerm(Integer issuanceTerm) {
		this.issuanceTerm = issuanceTerm;
	}
	
	/**
	 * Gets the lst cbo search securitie class.
	 *
	 * @return the lst cbo search securitie class
	 */
	public List<ParameterTable> getLstCboSearchSecuritieClass() {
		return lstCboSearchSecuritieClass;
	}
	
	/**
	 * Sets the lst cbo search securitie class.
	 *
	 * @param lstCboSearchSecuritieClass the new lst cbo search securitie class
	 */
	public void setLstCboSearchSecuritieClass(
			List<ParameterTable> lstCboSearchSecuritieClass) {
		this.lstCboSearchSecuritieClass = lstCboSearchSecuritieClass;
	}
	
	/**
	 * Gets the user privilege.
	 *
	 * @return the user privilege
	 */
	public UserPrivilege getUserPrivilege() {
		return userPrivilege;
	}
	
	/**
	 * Sets the user privilege.
	 *
	 * @param userPrivilege the new user privilege
	 */
	public void setUserPrivilege(UserPrivilege userPrivilege) {
		this.userPrivilege = userPrivilege;
	}
	
	/**
	 * Gets the issuance file.
	 *
	 * @return the issuance file
	 */
	public IssuanceFile getIssuanceFile() {
		return issuanceFile;
	}
	public IssuanceFileAux getIssuanceFileAux() {
		return issuanceFileAux;
	}
	/**
	 * Sets the issuance file.
	 *
	 * @param issuanceFile the new issuance file
	 */
	public void setIssuanceFile(IssuanceFile issuanceFile) {
		this.issuanceFile = issuanceFile;
	}
	public void setIssuanceFile(IssuanceFileAux issuanceFile) {
		this.issuanceFileAux = issuanceFile;
	}
	/**
	 * Gets the issuance file to delete.
	 *
	 * @return the issuance file to delete
	 */
	public IssuanceFile getIssuanceFileToDelete() {
		return issuanceFileToDelete;
	}
	public IssuanceFileAux getIssuanceFileToDeleteAux() {
		return issuanceFileToDeleteAux;
	}
	/**
	 * Sets the issuance file to delete.
	 *
	 * @param issuanceFileToDelete the new issuance file to delete
	 */
	public void setIssuanceFileToDelete(IssuanceFile issuanceFileToDelete) {
		this.issuanceFileToDelete = issuanceFileToDelete;
	}
	public void setIssuanceFileToDelete(IssuanceFileAux issuanceFileToDelete) {
		this.issuanceFileToDeleteAux = issuanceFileToDelete;
	}
	/**
	 * Checks if is closed invesment fund.
	 *
	 * @return true, if is closed invesment fund
	 */
	public boolean isClosedInvesmentFund() {
		return closedInvesmentFund;
	}
	
	/**
	 * Sets the closed invesment fund.
	 *
	 * @param closedInvesmentFund the new closed invesment fund
	 */
	public void setClosedInvesmentFund(boolean closedInvesmentFund) {
		this.closedInvesmentFund = closedInvesmentFund;
	}
	
	/**
	 * Gets the number total tranches.
	 *
	 * @return the numberTotalTranches
	 */
	public Integer getNumberTotalTranches() {
		return numberTotalTranches;
	}
	
	/**
	 * Sets the number total tranches.
	 *
	 * @param numberTotalTranches the numberTotalTranches to set
	 */
	public void setNumberTotalTranches(Integer numberTotalTranches) {
		this.numberTotalTranches = numberTotalTranches;
	}
	
	/** registro de observaciones */
	public IssuanceObservation getIssuanceObs() {
		return issuanceObs;
	}
	public void setIssuanceObs(IssuanceObservation issuanceObs) {
		this.issuanceObs = issuanceObs;
	}
	
	/** lista de observaciones */
	public List<IssuanceObservation> getIssuanceObsList() {
		return issuanceObsList;
	}
	public void setIssuanceObsList(List<IssuanceObservation> issuanceObsList) {
		this.issuanceObsList = issuanceObsList;
	}

	public Integer getIssuanceFileCertificate() {
		return issuanceFileCertificate;
	}
	public void setIssuanceFileCertificate(Integer issuanceFileCertificate) {
		this.issuanceFileCertificate = issuanceFileCertificate;
	}
	
	/** issue 866 referencia issuer */
	public Date getMinimumDate() {
		return minimumDate;
	}
	public void setMinimumDate(Date minimumDate) {
		this.minimumDate = minimumDate;
	}
	
}
