package com.pradera.securities.issuancesecuritie.util;

public class SecurityUtilXml {
	final public String FILE_NAME="emisiones";
	
	final public String NAME_ROOT_XML="EMISIONES";
	final public String NAME_NODE_ELEMENT="EMISION";
	
	final public String EMN_SERIE="EMN_SERIE";
    final public String EMN_CODINST="EMN_CODINST";
    final public String EMN_CODEM="EMN_CODEM";
    final public String EMN_CODEMISION="EMN_CODEMISION";
    final public String EMN_MONEDA_GEN="EMN_MONEDA_GEN";
    final public String EMN_MONEDA="EMN_MONEDA";
    final public String EMN_METODO_VALUACION="EMN_METODO_VALUACION";
    final public String EMN_FECHA_EMISION="EMN_FECHA_EMISION";
    final public String EMN_PLAZO="EMN_PLAZO";
    final public String EMN_FECHA_VENCI="EMN_FECHA_VENCI";
    final public String EMN_TREM="EMN_TREM";
    final public String EMN_TASA_TXT="EMN_TASA_TXT";
    final public String EMN_TDESC="EMN_TDESC";
    final public String EMN_PLAZO_AMORTIZA="EMN_PLAZO_AMORTIZA";
    final public String EMN_NRO_CUPONES="EMN_NRO_CUPONES";
    final public String EMN_VALOR_EMISION="EMN_VALOR_EMISION";
    final public String EMN_MONTO_AJUSTE="EMN_MONTO_AJUSTE";
    final public String EMN_VN="EMN_VN";
    final public String EMN_AGENTE_REGISTRADOR="EMN_AGENTE_REGISTRADOR";
    final public String EMN_TIPOACCION="EMN_TIPOACCION";
    final public String EMN_STATUS="EMN_STATUS";
    final public String EMN_FECHA_BV="EMN_FECHA_BV";
    final public String EMN_DSC="EMN_DSC";
    final public String EMN_DSM="EMN_DSM";
    final public String EMN_DIASREDIMIDOS="EMN_DIASREDIMIDOS";
    final public String EMN_TIPOREGISTRO_MESA="EMN_TIPOREGISTRO_MESA";
    final public String EMN_TIPOGENERICOVALOR="EMN_TIPOGENERICOVALOR";
    final public String EMN_LCUP_NEGOCIABLE="EMN_LCUP_NEGOCIABLE";
    final public String EMN_LSUBPRODUCTO="EMN_LSUBPRODUCTO";
    final public String EMN_EDV="EMN_EDV";
    final public String EMN_LFCI_NEG="EMN_LFCI_NEG";
    final public String EMN_PREPAGO="EMN_PREPAGO";
    final public String EMN_LSUBOR="EMN_LSUBOR";
    final public String EMN_LTASAFIJA="EMN_LTASAFIJA";
    final public String EMN_TIPOBONO="EMN_TIPOBONO";
    final public String EMN_SERIEACC="EMN_SERIEACC";
    final public String EMN_LTASA_DIF="EMN_LTASA_DIF";
    final public String EMN_TIR="EMN_TIR";
    final public String EMN_LHABILITADO="EMN_LHABILITADO";
    final public String EMN_FEAJUSTE_BCB="EMN_FEAJUSTE_BCB";
    final public String EMN_ESTADO="EMN_ESTADO";
    final public String EMN_FECHA_ENVIO="EMN_FECHA_ENVIO";
    final public String EMN_NEGOCIABLE="EMN_NEGOCIABLE";
}
