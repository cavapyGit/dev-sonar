package com.pradera.securities.issuancesecuritie.batch;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.BusinessProcess;

@Singleton
@Performance
@Path("/IssuanceSecurityResource")
@Interceptors(ContextHolderInterceptor.class)
public class IssuanceSecurityController {

	/** The transaction registry. */
	@Resource TransactionSynchronizationRegistry transactionRegistry;
	
	/** The batch process service facade. */
	@EJB private BatchProcessServiceFacade batchProcessServiceFacade;
	
	@GET
	@Path("/createIssuanceAndSecurityWithEntryToVault/{userParam}/{ipParam}/{listPayRollDetailPk}")
	//@Path("/createIssuanceAndSecurityWithEntryToVault/{userParam}/{listPayRollDetailPk}")
	public void testBatch(@PathParam("userParam")String userNameParam, @PathParam("ipParam")String ipParam, @PathParam("listPayRollDetailPk")String listPayRollDetailPk){
    	
    	LoggerUser loggerUser = CommonsUtilities.getLoggerUser(userNameParam, ipParam);
        ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
        
		/*
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.OPERATIVE_DAILY_PROCESS.getCode());
		Map<String,Object> parameter = new HashMap<>();
		parameter.put(OperativeDailyConstants.PROCESS_DATE_PARAMETER, CommonsUtilities.convertDateToString(new Date(), CommonsUtilities.DATETIME_PATTERN));
		try {
			batchProcessServiceFacade.registerBatch(userNameParam,businessProcess,parameter);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/

		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.CREATE_ISSUANCE_SECURITY_WITH_VALUT_ENTRY.getCode());
		try {
			Map<String, Object> details = new HashMap<String, Object>();
			details.put(GeneralConstants.LIST_PAYROLL_DETAIL_PK, listPayRollDetailPk);
			details.put(GeneralConstants.USER_NAME, userNameParam);
			batchProcessServiceFacade.registerBatch(userNameParam,businessProcess,details);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
	}
	
}