package com.pradera.securities.issuancesecuritie.batch;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.model.process.ProcessLogger;
import com.pradera.securities.issuancesecuritie.facade.PlacementSegmentHistoryFacade;

/**
 * The Class GeneratorPlacementSegmentHistoryBatch.
 *
 * @author jquino.
 * @version 1.0 , 05-07-2020
 */
@BatchProcess(name="GeneratorPlacementSegmentHistoryBatch")
@RequestScoped
public class GeneratorPlacementSegmentHistoryBatch implements JobExecution,Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	
	/** The placement segment history facade. */
	@EJB
    PlacementSegmentHistoryFacade placementSegmentHistoryFacade;
	
	/** The crud dao service bean. */
	@EJB
	private CrudDaoServiceBean crudDaoServiceBean;
	
	/** The log. */
	@Inject
	transient PraderaLogger log;
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		
			
		try {
			Date fechaActual = CommonsUtilities.currentDate();
			
			
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(fechaActual);
			
			// obteniendo el ultimo dia del mes
			Date ultimoDiaMes = CommonsUtilities.getMaxDateByMonthAndYear(calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));
			ultimoDiaMes = CommonsUtilities.truncateDateTime(ultimoDiaMes);
			
			if(ultimoDiaMes.compareTo(CommonsUtilities.truncateDateTime(fechaActual)) == 0){
				log.info("********************** PROCESS GENERATION PLACEMENT SEGMENT HISTORY BATCH **********************");
				
				placementSegmentHistoryFacade.generatePlacementSegmentHistory(fechaActual);
				
				log.info("********************** END PROCESS GENERATION PLACEMENT SEGMENT HISTORY BATCH **********************");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}

}
