package com.pradera.securities.issuancesecuritie.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.pradera.model.issuancesecuritie.type.RequestStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PaymentScheduleResultTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class PaymentScheduleResultTO implements Serializable{


	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id payment schedule req hi. */
	private Long idPaymentScheduleReqHi;
	
	/** The id security code pk. */
	private String idSecurityCodePk;
	
	/** The id isin code pk. */
	private String idIsinCodePk;
	
	/** The security description. */
	private String securityDescription;
	
	/** The id issuance code pk. */
	private String idIssuanceCodePk;
	
	/** The issuance date. */
	private Date issuanceDate;
	
	/** The expiration date. */
	private Date expirationDate;
	
	/** The coupons count. */
	private Long couponsCount;
	
	/** The request state. */
	private Integer requestState;
	
	/** The registry date. */
	private Date registryDate;
	
	/** The security class. */
	private Integer securityClass;
	
	/** The currency. */
	private Integer currency;
	
	/** The current nominal value. */
	private BigDecimal currentNominalValue;

	/**
	 * Instantiates a new payment schedule result to.
	 */
	public PaymentScheduleResultTO(){
		
	}

	/**
	 * Instantiates a new payment schedule result to.
	 *
	 * @param idPaymentScheduleReqHi the id payment schedule req hi
	 * @param idSecurityCodePk the id security code pk
	 * @param idIsinCodePk the id isin code pk
	 * @param securityDescription the security description
	 * @param idIssuanceCodePk the id issuance code pk
	 * @param issuanceDate the issuance date
	 * @param expirationDate the expiration date
	 * @param couponsCount the coupons count
	 * @param requestState the request state
	 * @param registryDate the registry date
	 * @param securityClass the security class
	 * @param currency the currency
	 * @param currentNominalValue the current nominal value
	 */
	public PaymentScheduleResultTO(Long idPaymentScheduleReqHi, String idSecurityCodePk ,String idIsinCodePk, String securityDescription,
			String idIssuanceCodePk, Date issuanceDate, Date expirationDate,
			Long couponsCount, Integer requestState,Date registryDate,
			Integer securityClass, Integer currency, BigDecimal currentNominalValue) {
		super();
		this.idPaymentScheduleReqHi=idPaymentScheduleReqHi;
		this.idSecurityCodePk=idSecurityCodePk;
		this.idIsinCodePk = idIsinCodePk;
		this.securityDescription = securityDescription;
		this.idIssuanceCodePk = idIssuanceCodePk;
		this.issuanceDate = issuanceDate;
		this.expirationDate = expirationDate;
		this.couponsCount = couponsCount;
		this.requestState=requestState;
		this.registryDate=registryDate;
		
		this.securityClass=securityClass;
		this.currency=currency;
		this.currentNominalValue=currentNominalValue;
	}
	
	/**
	 * Gets the id isin code pk.
	 *
	 * @return the id isin code pk
	 */
	public String getIdIsinCodePk() {
		return idIsinCodePk;
	}
	
	/**
	 * Sets the id isin code pk.
	 *
	 * @param idIsinCodePk the new id isin code pk
	 */
	public void setIdIsinCodePk(String idIsinCodePk) {
		this.idIsinCodePk = idIsinCodePk;
	}
	
	/**
	 * Gets the security description.
	 *
	 * @return the security description
	 */
	public String getSecurityDescription() {
		return securityDescription;
	}
	
	/**
	 * Sets the security description.
	 *
	 * @param securityDescription the new security description
	 */
	public void setSecurityDescription(String securityDescription) {
		this.securityDescription = securityDescription;
	}
	
	/**
	 * Gets the id issuance code pk.
	 *
	 * @return the id issuance code pk
	 */
	public String getIdIssuanceCodePk() {
		return idIssuanceCodePk;
	}
	
	/**
	 * Sets the id issuance code pk.
	 *
	 * @param idIssuanceCodePk the new id issuance code pk
	 */
	public void setIdIssuanceCodePk(String idIssuanceCodePk) {
		this.idIssuanceCodePk = idIssuanceCodePk;
	}
	
	/**
	 * Gets the issuance date.
	 *
	 * @return the issuance date
	 */
	public Date getIssuanceDate() {
		return issuanceDate;
	}
	
	/**
	 * Sets the issuance date.
	 *
	 * @param issuanceDate the new issuance date
	 */
	public void setIssuanceDate(Date issuanceDate) {
		this.issuanceDate = issuanceDate;
	}
	
	/**
	 * Gets the expiration date.
	 *
	 * @return the expiration date
	 */
	public Date getExpirationDate() {
		return expirationDate;
	}
	
	/**
	 * Sets the expiration date.
	 *
	 * @param expirationDate the new expiration date
	 */
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	
	/**
	 * Gets the coupons count.
	 *
	 * @return the coupons count
	 */
	public Long getCouponsCount() {
		return couponsCount;
	}
	
	/**
	 * Sets the coupons count.
	 *
	 * @param couponsCount the new coupons count
	 */
	public void setCouponsCount(Long couponsCount) {
		this.couponsCount = couponsCount;
	}
	
	/**
	 * Gets the id payment schedule req hi.
	 *
	 * @return the id payment schedule req hi
	 */
	public Long getIdPaymentScheduleReqHi() {
		return idPaymentScheduleReqHi;
	}
	
	/**
	 * Sets the id payment schedule req hi.
	 *
	 * @param idPaymentScheduleReqHi the new id payment schedule req hi
	 */
	public void setIdPaymentScheduleReqHi(Long idPaymentScheduleReqHi) {
		this.idPaymentScheduleReqHi = idPaymentScheduleReqHi;
	}
	
	
	/**
	 * Gets the request state.
	 *
	 * @return the request state
	 */
	public Integer getRequestState() {
		return requestState;
	}
	
	/**
	 * Sets the request state.
	 *
	 * @param requestState the new request state
	 */
	public void setRequestState(Integer requestState) {
		this.requestState = requestState;
	}
	
	/**
	 * Gets the request state description.
	 *
	 * @return the request state description
	 */
	public String getRequestStateDescription(){
		if(RequestStateType.get( requestState )!=null){
			return RequestStateType.get( requestState ).getValue();
		}
		return "";
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the id security code pk.
	 *
	 * @return the id security code pk
	 */
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	/**
	 * Sets the id security code pk.
	 *
	 * @param idSecurityCodePk the new id security code pk
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * Gets the current nominal value.
	 *
	 * @return the current nominal value
	 */
	public BigDecimal getCurrentNominalValue() {
		return currentNominalValue;
	}

	/**
	 * Sets the current nominal value.
	 *
	 * @param currentNominalValue the new current nominal value
	 */
	public void setCurrentNominalValue(BigDecimal currentNominalValue) {
		this.currentNominalValue = currentNominalValue;
	}

	/**
	 * Gets the security class.
	 *
	 * @return the security class
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}

	/**
	 * Sets the security class.
	 *
	 * @param securityClass the new security class
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	
}