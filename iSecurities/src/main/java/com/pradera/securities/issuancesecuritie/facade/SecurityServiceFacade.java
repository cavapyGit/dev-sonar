/*
 * 
 */
package com.pradera.securities.issuancesecuritie.facade;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.report.ReportFile;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.services.remote.reports.ReportGenerationService;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.XMLUtils;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.accounts.to.InternationalDepositoryTO;
import com.pradera.core.component.accounts.to.NegotiationMechanismTO;
import com.pradera.core.component.business.to.ExpirationSecurityTO;
import com.pradera.core.component.business.to.SecurityConsultTO;
import com.pradera.core.component.business.to.SecurityOverdueTO;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.securities.service.SecuritiesServiceBean;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.common.validation.to.ValidationOperationType;
import com.pradera.integration.common.validation.to.ValidationProcessTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.securities.to.GenerationFileDetailCouponTRCRTO;
import com.pradera.integration.component.securities.to.GenerationFileSecurityTRCRTO;
import com.pradera.integration.component.securities.to.IssuanceRegisterTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionStateType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionType;
import com.pradera.model.issuancesecuritie.AmortizationPaymentSchedule;
import com.pradera.model.issuancesecuritie.CfiAttribute;
import com.pradera.model.issuancesecuritie.CfiCategory;
import com.pradera.model.issuancesecuritie.CfiCodeGenerator;
import com.pradera.model.issuancesecuritie.CfiGroup;
import com.pradera.model.issuancesecuritie.InterestPaymentSchedule;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.ProgramAmortizationCoupon;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.SecurityCodeFormat;
import com.pradera.model.issuancesecuritie.SecurityForeignDepository;
import com.pradera.model.issuancesecuritie.SecurityHistoryState;
import com.pradera.model.issuancesecuritie.SecurityNegotiationMechanism;
import com.pradera.model.issuancesecuritie.SecurityRemoved;
import com.pradera.model.issuancesecuritie.SecurityRemovedHistory;
import com.pradera.model.issuancesecuritie.SecuritySerialRange;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.SecurityForeignDepositoryStateType;
import com.pradera.model.issuancesecuritie.type.SecurityRemovedStateType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.securities.issuancesecuritie.service.PaymentCronogramServiceBean;
import com.pradera.securities.issuancesecuritie.service.SecurityServiceBean;
import com.pradera.securities.issuancesecuritie.to.CouponInformationTO;
import com.pradera.securities.issuancesecuritie.to.SecurityRemovedModelTO;
import com.pradera.securities.query.to.SecurityBalanceMovTO;
import com.pradera.securities.query.to.SecurityMovementsResultTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SecurityServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class SecurityServiceFacade {

	/** The interface component service bean. */
	@Inject
    private Instance<InterfaceComponentService> interfaceComponentServiceBean;
	
	/** The security service bean. */
	@EJB
	SecurityServiceBean securityServiceBean;
	
	/** The securities service bean. */
	@EJB
	SecuritiesServiceBean securitiesServiceBean;
	
	/** The crud dao service bean. */
	@EJB
	CrudDaoServiceBean crudDaoServiceBean;
	
	/** The payment cronogram service bean. */
	@EJB
	PaymentCronogramServiceBean paymentCronogramServiceBean;

	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	/** The parameter service bean. */
	@EJB
	ParameterServiceBean parameterServiceBean;
	
	/** The report generation service. */
	@Inject
	Instance<ReportGenerationService> reportGenerationService;
	
	/** The holder account component service bean. */
	@EJB
	HolderAccountComponentServiceBean holderAccountComponentServiceBean;
	
	/**
	 * lista los CFC que venceran (se toma como referencia el campo EXPIRATION_FONDO_DATE de la tabla SECURITY)
	 * @param dateTarget
	 * @return
	 * @throws ServiceException
	 */
	public List<String> getLstCfcSecuritiesToExpire(Date dateTarget)throws ServiceException{
		return securityServiceBean.getLstCfcSecuritiesToExpire(dateTarget);
	}
	
	/**
 	 * issue 891: obtiene las solictudes de eliminacion de valor segun los filtros enviados
 	 * @param filter
 	 * @return
 	 * @throws ServiceException
 	 */
 	public List<SecurityRemovedModelTO> getLstSecurityRemovedModelTO(SecurityRemovedModelTO filter)throws ServiceException{
 		List<SecurityRemovedModelTO> lst=securityServiceBean.getLstSecurityRemovedTO(filter);
 		
 		// aca se captura los estados y las monedas en formato texto
		for (SecurityRemovedModelTO securityRemove : lst) {
			// capturando el texto de estado
			for (ParameterTable state : filter.getLstState())
				if (state.getParameterTablePk().equals(securityRemove.getStateSecurityRemoved())) {
					securityRemove.setStateSecurityRemovedText(state.getParameterName());
					break;
				}
			
			// capturando el texto de la moneda
			for (ParameterTable currency : filter.getLstCurrency())
				if (currency.getParameterTablePk().equals(securityRemove.getCurrency())) {
					securityRemove.setCurrencyText(currency.getParameterName());
					break;
				}
		}
 		
 		return lst;
 	}
 	
 	public Security findSecurityOnly(String idSecurityCodePk){
 		try {
 			return securityServiceBean.find(Security.class, idSecurityCodePk);
		} catch (Exception e) {
			return null;
		}
 	}
 	
 	public SecurityRemoved findSecurityRemoved(Long idSecurityRemovedPk) throws ServiceException{
 		return securityServiceBean.find(SecurityRemoved.class, idSecurityRemovedPk);
 	}
	
 	/**
 	 * issue 891: registro de solicitud parala elimiancion de un valor NO autorizado
 	 * @param modelTO
 	 * @return
 	 * @throws ServiceException
 	 */
 	public SecurityRemoved regiterSecurityRemoved(SecurityRemovedModelTO modelTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		try {
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());	
		} catch (Exception e) {
			System.out.println("::: no se pudo obtener el privilegio en el metodo SecurityServiceFacade.regiterSecurityRemoved. Se procedera a setear las pistas de auditoria manualemnte");
		}
		
		SecurityRemoved securityRemoved=new SecurityRemoved();
		securityRemoved.setIdSecurityCodePk(modelTO.getIdSecurityCodePk());
		securityRemoved.setIssuanceDate(modelTO.getIssuanceDate());
		securityRemoved.setExpirationDate(modelTO.getExpirationDate());
		securityRemoved.setCurrency(modelTO.getCurrency());
		securityRemoved.setNominalValue(modelTO.getNominalValue());
		securityRemoved.setInterestRateNominal(modelTO.getInterestRateNominal());
		securityRemoved.setCouponNumber(modelTO.getCouponNumber());
		securityRemoved.setMotiveRemoved(modelTO.getMotiveRemoved());
		securityRemoved.setRegistryDate(new Date());
		securityRemoved.setStateSecurityRemoved(SecurityRemovedStateType.REGISTRADO.getCode());
		//pistas de auditoria
		securityRemoved.setLastModifyUser(modelTO.getAuditUserName());
		securityRemoved.setLastModifyDate(new Date());
		securityRemoved.setLastModifyIp(modelTO.getAuditIpAddress());
		securityRemoved.setLastModifyApp(modelTO.getAuditIdPrivilege());
		
		// preparando el historico
		SecurityRemovedHistory securityRemovedHistory=new SecurityRemovedHistory();
		securityRemovedHistory.setIdSecurityCodePk(modelTO.getIdSecurityCodePk());
		securityRemovedHistory.setIssuanceDate(modelTO.getIssuanceDate());
		securityRemovedHistory.setExpirationDate(modelTO.getExpirationDate());
		securityRemovedHistory.setCurrency(modelTO.getCurrency());
		securityRemovedHistory.setNominalValue(modelTO.getNominalValue());
		securityRemovedHistory.setInterestRateNominal(modelTO.getInterestRateNominal());
		securityRemovedHistory.setCouponNumber(modelTO.getCouponNumber());
		securityRemovedHistory.setMotiveRemoved(modelTO.getMotiveRemoved());
		securityRemovedHistory.setRegistryDate(securityRemoved.getRegistryDate());
		securityRemovedHistory.setStateSecurityRemoved(securityRemoved.getStateSecurityRemoved());
		securityRemovedHistory.setLastModifyUser(securityRemoved.getLastModifyUser());
		securityRemovedHistory.setLastModifyDate(securityRemoved.getLastModifyDate());
		securityRemovedHistory.setLastModifyIp(securityRemoved.getLastModifyIp());
		securityRemovedHistory.setLastModifyApp(securityRemoved.getLastModifyApp());
		securityRemovedHistory.setIdSecurityRemovedFk(securityRemoved);
		// para el registro en cascada
		List<SecurityRemovedHistory> lst=new ArrayList<>();
		lst.add(securityRemovedHistory);
		securityRemoved.setSecurityRemovedHistoryList(lst);
		
		return securityServiceBean.regiterSecurityRemoved(securityRemoved);
 	}
 	
 	/**
 	 * issue 891: cambio de estado de una solictud de Eliminacion de valor NO autorizado
 	 * @param modelTO
 	 * @return
 	 * @throws ServiceException
 	 */
 	public SecurityRemoved changeStateSecurityRemoved(SecurityRemovedModelTO modelTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		try {
			if(modelTO.getStateSecurityRemoved().equals(SecurityRemovedStateType.APROBADO.getCode())){
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
			} else if(modelTO.getStateSecurityRemoved().equals(SecurityRemovedStateType.ANULADO.getCode())){
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
			} else if(modelTO.getStateSecurityRemoved().equals(SecurityRemovedStateType.CONFIRMADO.getCode())){
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
			} else if(modelTO.getStateSecurityRemoved().equals(SecurityRemovedStateType.RECHAZADO.getCode())){
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
			}
		} catch (Exception e) {
			System.out.println("::: no se pudo obtener el privilegio en el metodo SecurityServiceFacade.changeStateSecurityRemoved. Se procedera a setear las pistas de auditoria manualemnte");
		}
		
		// si se cambia a estado CONFIRMADO entonces realizar eliminacion fisica del valor
		if(modelTO.getStateSecurityRemoved().equals(SecurityRemovedStateType.CONFIRMADO.getCode())){
			// se realizan DELETEs a las tablas asociadas al valor
			List<String> lst=new ArrayList<>();
			lst.add("delete from SECURITY_HISTORY_BALANCE where ID_SECURITY_CODE_FK = :idSecurityCode");
			lst.add("delete from CORPORATIVE_OPERATION where ID_ORIGIN_SECURITY_CODE_FK = :idSecurityCode");
			lst.add("delete from SECURITY_INVESTOR where ID_SECURITY_CODE_FK = :idSecurityCode");
			lst.add("delete from PROGRAM_INT_COUPON_REQ_HIS where ID_INT_SCHEDULE_REQ_HIS_FK in (select ID_INT_SCHEDULE_REQ_HIS_PK from INT_PAYMENT_SCHEDULE_REQ_HIS where ID_SECURITY_CODE_FK = :idSecurityCode)");
			lst.add("delete from INT_PAYMENT_SCHEDULE_REQ_HIS where ID_SECURITY_CODE_FK = :idSecurityCode");
			lst.add("delete from PROGRAM_INTEREST_COUPON where ID_INT_PAYMENT_SCHEDULE_FK in (select ID_INT_PAYMENT_SCHEDULE_PK from INTEREST_PAYMENT_SCHEDULE where ID_SECURITY_CODE_FK = :idSecurityCode)");
			lst.add("delete from INTEREST_PAYMENT_SCHEDULE where ID_SECURITY_CODE_FK =  :idSecurityCode");
			lst.add("delete from PROGRAM_AMO_COUPON_REQ_HIS where ID_AMO_SCHEDULE_REQ_HIS_FK in (select ID_AMO_SCHEDULE_REQ_HIS_PK from AMO_PAYMENT_SCHEDULE_REQ_HIS where ID_SECURITY_CODE_FK = :idSecurityCode)");
			lst.add("delete from AMO_PAYMENT_SCHEDULE_REQ_HIS where ID_SECURITY_CODE_FK = :idSecurityCode");
			lst.add("delete from PROGRAM_AMORTIZATION_COUPON where ID_AMO_PAYMENT_SCHEDULE_FK in (select ID_AMO_PAYMENT_SCHEDULE_PK from AMORTIZATION_PAYMENT_SCHEDULE where ID_SECURITY_CODE_FK = :idSecurityCode)");
			lst.add("delete from AMORTIZATION_PAYMENT_SCHEDULE where ID_SECURITY_CODE_FK = :idSecurityCode ");
			lst.add("delete from SECURITY_NEGOTIATION_MECHANISM where ID_SECURITY_CODE_FK = :idSecurityCode ");
			lst.add("delete from SECURITY where ID_SECURITY_CODE_PK = :idSecurityCode ");
			
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("idSecurityCode", modelTO.getIdSecurityCodePk());
			for(String query:lst){
				securityServiceBean.executeRemovedSecurityQueryNative(query, parameters);
			}
		}
		
		SecurityRemoved securityRemoved = securityServiceBean.find(SecurityRemoved.class, modelTO.getIdSecurityRemovedPk());
		securityRemoved.setStateSecurityRemoved(modelTO.getStateSecurityRemoved());
		
		//pistas de auditoria
		securityRemoved.setLastModifyUser(modelTO.getAuditUserName());
		securityRemoved.setLastModifyDate(new Date());
		securityRemoved.setLastModifyIp(modelTO.getAuditIpAddress());
		securityRemoved.setLastModifyApp(modelTO.getAuditIdPrivilege());
		
		// preparando el historico
		SecurityRemovedHistory securityRemovedHistory=new SecurityRemovedHistory();
		securityRemovedHistory.setIdSecurityCodePk(modelTO.getIdSecurityCodePk());
		securityRemovedHistory.setIssuanceDate(modelTO.getIssuanceDate());
		securityRemovedHistory.setExpirationDate(modelTO.getExpirationDate());
		securityRemovedHistory.setCurrency(modelTO.getCurrency());
		securityRemovedHistory.setNominalValue(modelTO.getNominalValue());
		securityRemovedHistory.setInterestRateNominal(modelTO.getInterestRateNominal());
		securityRemovedHistory.setCouponNumber(modelTO.getCouponNumber());
		securityRemovedHistory.setMotiveRemoved(modelTO.getMotiveRemoved());
		securityRemovedHistory.setStateSecurityRemoved(modelTO.getStateSecurityRemoved());
		securityRemovedHistory.setRegistryDate(securityRemoved.getRegistryDate());
		securityRemovedHistory.setLastModifyUser(securityRemoved.getLastModifyUser());
		securityRemovedHistory.setLastModifyDate(securityRemoved.getLastModifyDate());
		securityRemovedHistory.setLastModifyIp(securityRemoved.getLastModifyIp());
		securityRemovedHistory.setLastModifyApp(securityRemoved.getLastModifyApp());
		securityRemovedHistory.setIdSecurityRemovedFk(securityRemoved);
		// para el registro en cascada
		List<SecurityRemovedHistory> lst=new ArrayList<>();
		lst.add(securityRemovedHistory);
		securityRemoved.setSecurityRemovedHistoryList(lst);
		
		return securityServiceBean.changeStateSecurityRemoved(securityRemoved);
 	}
	
	/**
	 * Metodo para Obtener los Grupos CFI
	 *
	 * @param idCategoryPk the id category pk
	 * @return the cFI group service facade
	 * @throws ServiceException the service exception
	 */
	public List<CfiGroup> getCFIGroupServiceFacade(String idCategoryPk) throws ServiceException{
		return securityServiceBean.getCFIGroupServiceBean(idCategoryPk);
	}
	
	/**
	 * Metodo para Obtener los categorias CFI
	 *
	 * @return the cFI categories service facade
	 * @throws ServiceException the service exception
	 */
	public List<CfiCategory> getCFICategoriesServiceFacade() throws ServiceException{
		return securityServiceBean.getCFICategoriesServiceBean();
	}
	
	/**
	 * Metodo para Generar los Codigos CFI
	 *
	 * @param cfiCodeGen the cfi code gen
	 * @return the cfi code generator service facade
	 * @throws ServiceException the service exception
	 */
	public String getCfiCodeGeneratorServiceFacade(CfiCodeGenerator cfiCodeGen) throws ServiceException{
		return securitiesServiceBean.getCfiCodeGeneratorServiceBean(cfiCodeGen);
	}
	
	/**
	 * Metodo para obtener los atributos CFI
	 *
	 * @param idCategoryPk the id category pk
	 * @param idGroupPk the id group pk
	 * @param attributeLevel the attribute level
	 * @return the cFI attributes service facade
	 * @throws ServiceException the service exception
	 */
	public List<CfiAttribute> getCFIAttributesServiceFacade(String idCategoryPk, String idGroupPk, String attributeLevel) throws ServiceException{
		return securityServiceBean.getCFIAttributesServiceBean(idCategoryPk, idGroupPk, attributeLevel);
	}
	
	/**
	 * Metodo para Obtener una Lista de Depositarias Internacionales 
	 *
	 * @param filter the filter
	 * @return the list international depository service facade
	 * @throws ServiceException the service exception
	 */
	public List<InternationalDepository> getListInternationalDepositoryServiceFacade(InternationalDepositoryTO filter) throws ServiceException{
		return securityServiceBean.getListInternationalDepositoryServiceBean(filter);
	}
	
	/**
	 * Metodo para Obtener una lista de Valores
	 *
	 * @param securityTO the security to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Security> findSecuritiesServiceFacade(SecurityTO securityTO) throws ServiceException{
		return securityServiceBean.findSecuritiesLiteServiceBean(securityTO);
	}

	
	
	/***
	 * 
	 * @param securityTO
	 * @return
	 * @throws ServiceException
	 */
	public List<SecurityOverdueTO> findSecuritiesForExpiration(SecurityTO securityTO) throws ServiceException{
		return securityServiceBean.findSecuritiesForExpiration(securityTO);
	}
	
	/**
	 * Metodo para Obtener una lista de Valores
	 *
	 * @param securityTO the security to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Security> findSecuritiesListForParticipantInvestor(SecurityTO securityTO) throws ServiceException{
		return securityServiceBean.findSecuritiesListForParticipantInvestor(securityTO);
	}
	
	/**
	 * Metodo para Obtener una lista de Valores.
	 *
	 * @param securityTO the security to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Security> findSecuritiesLiteServiceFacade(SecurityTO securityTO) throws ServiceException{
		return securityServiceBean.findSecuritiesLiteServiceBean(securityTO);
	}
	
	/**
	 * Metodo para obtener el correlativo
	 *
	 * @param idIssuerPkPrm the id issuer pk prm
	 * @return the securities correlative by issuer service facade
	 * @throws ServiceException the service exception
	 */
	public String getSecuritiesCorrelativeByIssuerServiceFacade(String idIssuerPkPrm) throws ServiceException{
		int count= securityServiceBean.getSecuritiesCountServiceBean(idIssuerPkPrm);
		StringBuilder sbCorrelativeResult=new StringBuilder();
		count++;
		
		sbCorrelativeResult.append(count);
		if(sbCorrelativeResult.length()==1){
			sbCorrelativeResult.insert(0, "00");
		}else if(sbCorrelativeResult.length()==2){
			sbCorrelativeResult.insert(0, "0");
		}
		return sbCorrelativeResult.toString();
	}
	
	/**
	 * Metodo para obtener una lista de mecanismos de negociacion.
	 *
	 * @param filter the filter
	 * @return the list negotiation mechanism service facade
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationMechanism> getListNegotiationMechanismServiceFacade(NegotiationMechanismTO filter) throws ServiceException	{
		return securityServiceBean.getListNegotiationMechanismServiceBean(filter);
	}
	
	/**
	 * Metodo para Obtener una lista de mecanismos.
	 *
	 * @param state the state
	 * @return the list mechanism service facade
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationMechanism> getListMechanismServiceFacade(Integer state) throws ServiceException	{
		return securityServiceBean.getListMechanismServiceBean(state);
	}
	
	/**
	 * Metodo para Obtener un Valor.
	 *
	 * @param securityTO the security to
	 * @return the security
	 * @throws ServiceException the service exception
	 */
	public Security findSecurityLiteServiceFacade(SecurityTO securityTO) throws ServiceException{
		Security security = securityServiceBean.findSecurityServiceBean( securityTO );
		return security;
	}
	
	/**
	 * Metodo para cambiar la situacion de un valor a Autorizado
	 */
	@LoggerAuditWeb
	public void securityAuthorize(Security security) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAuthorize());
        security.setAudit(loggerUser);
        if(InstrumentType.FIXED_INCOME.getCode().equals(security.getInstrumentType()))
        	updateExpirationIssuanceDate(security);
		securityServiceBean.securityAuthorize(security,loggerUser);
	}
	
	
	public void updateExpirationIssuanceDate(Security security)  throws ServiceException{
		Issuance issuance = securityServiceBean.find(Issuance.class, security.getIssuance().getIdIssuanceCodePk());
		if(issuance.getExpirationDate().before(security.getExpirationDate())) {
			issuance.setExpirationDate(security.getExpirationDate());
			securityServiceBean.update(issuance);
		}
	}
	/**
	 * Metodo para obtener un Valor
	 *
	 * @param securityTO the security to
	 * @return the security
	 * @throws ServiceException the service exception
	 */
	public Security findSecurityServiceFacade(SecurityTO securityTO) throws ServiceException{
		Security security = securityServiceBean.findSecurityServiceBean( securityTO );
		
		if(security!=null){
			boolean inNegotiations=securityInNegotiations(securityTO);
			security.setInNegotiations(inNegotiations);
			
			//GET INTERNATIONAL DEPOSITORIES
			List<SecurityForeignDepository> lstAssignedInternationalDep=findSecurityForeignDepositoriesServiceFacade(security.getIdSecurityCodePk(), 
					SecurityForeignDepositoryStateType.REGISTERED.getCode());
			security.setSecurityForeignDepositories( lstAssignedInternationalDep );
			//GET NEGOTIATION MECHANISMS
			List<SecurityNegotiationMechanism> lstAssignedSecNegotiationMechanism=findSecurityNegotiationMechanismsServiceFacade(security.getIdSecurityCodePk(), 
					BooleanType.YES.getCode());
			security.setSecurityNegotiationMechanisms( lstAssignedSecNegotiationMechanism );
		}
		return security;
	}
	
	public List<SecuritySerialRange> findSecuritySerialRange(String idSecurityCodePk) {
		return securityServiceBean.findSecuritySerialRange(idSecurityCodePk);
	}
	
	/**
	 * Metodo para Obtener un valor de una Depositaria Foranea
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @param state the state
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<SecurityForeignDepository> findSecurityForeignDepositoriesServiceFacade(String idSecurityCodePk, Integer state) throws ServiceException{
		return securityServiceBean.findSecurityForeignDepositories(idSecurityCodePk, state);
	}
	    
	/**
	 * Metodo para Obtener un mecanismo de negociacion por clave de valor y estado
	 *
	 * @param idSecurityCode the id security code
	 * @param state the state
	 * @return the list
	 */
	public List<SecurityNegotiationMechanism> findSecurityNegotiationMechanismsServiceFacade(String idSecurityCode, Integer state){
		Map<String,Object> parameters=new HashMap<String, Object>();
		parameters.put("idSecurityCodePkPrm", idSecurityCode);
		parameters.put("statePrm", state);
		return securityServiceBean.findSecurityNegotiationMechanismsServiceBean(parameters);
	}
	
	/**
	 * Metodo para actualizar Valor.
	 *
	 * @param security the security
	 * @param lstDtbInternationalDepository the lst dtb international depository
	 * @param lstDtbNegotiationModalities the lst dtb negotiation modalities
	 * @param isReGenerated the is re generated
	 * @throws ServiceException the service exception
	 */
	public void updateSecurityServiceFacade(Security security, List<InternationalDepository> lstDtbInternationalDepository,
											List<NegotiationModality> lstDtbNegotiationModalities,
											boolean isReGenerated) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
        security.setAudit(loggerUser);
        
         securitiesServiceBean.updateSecurityServiceBean(security, loggerUser , lstDtbInternationalDepository, lstDtbNegotiationModalities, isReGenerated);
	}
	
	/**
	 * Metodo para registrar un Valor
	 *
	 * @param security the security
	 * @param lstDtbInternationalDepository the lst dtb international depository
	 * @param lstDtbNegotiationModalities the lst dtb negotiation modalities
	 * @throws ServiceException the service exception
	 */
	public void registrySecurityServiceFacade(Security security,List<InternationalDepository> lstDtbInternationalDepository,List<NegotiationModality> lstDtbNegotiationModalities, Integer indAutomatic, AccountAnnotationOperation accountAnnotationOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        security.setAudit(loggerUser);
        
        securitiesServiceBean.registrySecurityServiceBean(security, loggerUser, lstDtbInternationalDepository, lstDtbNegotiationModalities, indAutomatic, accountAnnotationOperation);
	}
	
	public String securityIsinGenerate(Security security) throws ServiceException{
        return securitiesServiceBean.securityIsinGenerate(security);
	}
	
	public void registrySecurityNegotiationMechanism(SecurityNegotiationMechanism securityNegotiationMechanism) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        securityNegotiationMechanism.setAudit(loggerUser);
        securitiesServiceBean.create(securityNegotiationMechanism);
	}
	
	
	public List<PhysicalCertificate> registryPhysicalCertificateAndCupons(List<PhysicalCertificate> lstPhysicalCertificate) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        
        int i=0;
        PhysicalCertificate physicalCertificatePrincipal = null;
        for(PhysicalCertificate physicalCertificate: lstPhysicalCertificate) {
        	i++;
        	physicalCertificate.setAudit(loggerUser);
    		physicalCertificate = securitiesServiceBean.create(physicalCertificate);
        	if(i==1) {
        		physicalCertificatePrincipal = physicalCertificate;
        	}else {
            	physicalCertificate.setPhysicalCertificateReference(physicalCertificatePrincipal.getIdPhysicalCertificatePk());
        	}
        }
        return lstPhysicalCertificate;
        
	}
	
	
	/**
	 * Metodo para actualizar el Estado de un Valor
	 *
	 * @param security the security
	 * @param viewOperationType the view operation type
	 * @param secHistoryState the sec history state
	 * @throws ServiceException the service exception
	 */
	public void updateSecurityStateServiceFacade(Security security, Integer viewOperationType, SecurityHistoryState secHistoryState) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        if(loggerUser!=null){
        	if(viewOperationType.equals( ViewOperationsType.BLOCK.getCode() )){
        		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeBlock());
        	}else if(viewOperationType.equals( ViewOperationsType.UNBLOCK.getCode() )){
        		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeUnblock());
        	}
        	secHistoryState.setUpdateStateDate(loggerUser.getAuditTime());
        	secHistoryState.setRegistryUser( loggerUser.getUserName() );
        	secHistoryState.setRegistryDate( loggerUser.getAuditTime() );
        }
        secHistoryState.setAudit(loggerUser);
//        security.setAudit(loggerUser);
		securityServiceBean.updateSecurityStateServiceBean(security,secHistoryState);
	}
	
	/**
	 * Metodo para validar un valor segun nemonicos
	 *
	 * @param security the security
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean validateSecurityMenomincServiceFacade(Security security) throws ServiceException{
		return securityServiceBean.validateSecurityMenomincServiceBean(security) >0;
	}
	
	/**
	 * Metodo para validar el codigo de un valor.
	 *
	 * @param security the security
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean validateSecurityCodeServiceFacade(Security security) throws ServiceException{
		return securityServiceBean.validateSecurityCodeServiceBean(security) >0;
	}
	
	public boolean validateCountryInIsin(Security security) throws ServiceException{
		return securityServiceBean.validateCountryInIsin(security);
	}

	/**
	 * Metodo para buscar un valor por formato de codigo.
	 *
	 * @param security the security
	 * @return the security code format
	 * @throws ServiceException the service exception
	 */
	public SecurityCodeFormat validateSecurityCodeFormat(Security security) throws ServiceException{
		return securityServiceBean.findSecurityCodeFormat(security);
	}
	
	/**
	 * Metodo para validar el codigo isin por parametro de valor
	 *
	 * @param security the security
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean validateSecurityISINServiceFacade(Security security) throws ServiceException{
		return securityServiceBean.validateSecurityISINServiceBean(security) >0;
	}
	
	/**
	 * Metodo para buscar un InterestPaymentSchedule por codigo de valor y estado.
	 *
	 * @param idSecurityCode the id security code
	 * @return the interest payment schedule
	 * @throws ServiceException the service exception
	 */
	public InterestPaymentSchedule findActiveInterestPaymentScheduleBySecurity(String idSecurityCode) throws ServiceException{
		return paymentCronogramServiceBean.findActiveInterestPaymentScheduleBySecurity(idSecurityCode);
	}
	
	/**
	 * Metodo para buscar un AmortizationPaymentSchedule por codigo de valor y estado.
	 *
	 * @param idSecurityCode the id security code
	 * @return the amortization payment schedule
	 * @throws ServiceException the service exception
	 */
	public AmortizationPaymentSchedule findActiveAmortizationPaymentSchedule(String idSecurityCode) throws ServiceException{
		return paymentCronogramServiceBean.findActiveAmortizationPaymentSchedule(idSecurityCode);
	}
	
	/**
	 * Metodo para buscar un ProgramInterestCoupon por id de cronograma.
	 *
	 * @param idIntPayCronPkPrm the id int pay cron pk prm
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ProgramInterestCoupon> findProgramInterestCouponsServiceFacade(Long idIntPayCronPkPrm) throws ServiceException{
		return paymentCronogramServiceBean.findProgramInterestCouponsServiceBean(idIntPayCronPkPrm);
	}
	
	/**
	 * Metodo para buscar un ProgramAmortizationCoupons por id de cronograma.
	 *
	 * @param idAmortCronPkPrm the id amort cron pk prm
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ProgramAmortizationCoupon> findProgramAmortizationCouponsServiceFacade(Long idAmortCronPkPrm) throws ServiceException{
		return paymentCronogramServiceBean.findProgramAmortizationCouponsServiceBean(idAmortCronPkPrm);
	}
	
	/**
	 * Find coupon information by date.
	 *
	 * @param securityTO the security to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<CouponInformationTO> findCouponInformationByDate(SecurityTO securityTO) throws ServiceException{
		return paymentCronogramServiceBean.findProgramAmortizationAndInterestCouponsByDate(securityTO);
	}
	
	/**
	 * Gets the count securities by issuance service facade.
	 *
	 * @param idIssuanceCode the id issuance code
	 * @return the count securities by issuance service facade
	 * @throws ServiceException the service exception
	 */
	public int getCountSecuritiesByIssuanceServiceFacade(String idIssuanceCode) throws ServiceException{
		return securityServiceBean.getCountSecuritiesByIssuanceServiceBean(idIssuanceCode);
	}
	
	/**
	 * Gets the lst negotiation mechanism.
	 *
	 * @return the lst negotiation mechanism
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationMechanism> getLstNegotiationMechanism() throws ServiceException{
		return securityServiceBean.getListNegotiationMechanism();		
	}
	
//	public List<MechanismModality> getLstMechanismModality(Long negotiationMechanism,Integer instrumentType) throws ServiceException{
//		return securityServiceBean.getListMechanismModality(negotiationMechanism,instrumentType);
//	}
	
	/**
 * Validate security serie service facade.
 *
 * @param security the security
 * @return true, if successful
 * @throws ServiceException the service exception
 */
public boolean validateSecuritySerieServiceFacade(Security security) throws ServiceException{
		return securityServiceBean.validateSecuritySerieServiceBean(security)>0;
	}
	
	/**
	 * Gets the list negotiation modalities service facade.
	 *
	 * @param negotiationModalityState the negotiation modality state
	 * @param mechanismModalityState the mechanism modality state
	 * @param instrumentType the instrument type
	 * @return the list negotiation modalities service facade
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationModality> getListNegotiationModalitiesServiceFacade(Integer negotiationModalityState, 
																			   Integer mechanismModalityState,
																			   Integer instrumentType) throws ServiceException{
		Map<String, Object> filters=new HashMap<String, Object>();
		filters.put("negotiationModalityState", negotiationModalityState);
		filters.put("mechanismModalityState", mechanismModalityState);
		filters.put("instrumentType", instrumentType);
		return securityServiceBean.getListNegotiationModalitiesServiceBean(filters);
	}
	
	
	
	/**
	 * Find dates program amortization and interest coupons.
	 *
	 * @param initialDate the initial date
	 * @param endDate the end date
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Date> findDatesProgramAmortizationAndInterestCoupons(Date initialDate, Date endDate) throws ServiceException{
		return paymentCronogramServiceBean.findDatesProgramAmortizationAndInterestCoupons(initialDate, endDate);
	}
	
	/**
	 * Security in negotiations.
	 *
	 * @param securityTO the security to
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean securityInNegotiations(SecurityTO securityTO) throws ServiceException{
		return securitiesServiceBean.findSecurityNegotiationParticipant(securityTO).size()>0;
	}
	
	/**
	 * List security movement result.
	 *
	 * @param securityBalanceMovTO the security balance mov to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<SecurityMovementsResultTO> listSecurityMovementResult(
			SecurityBalanceMovTO securityBalanceMovTO) throws ServiceException{
		return securityServiceBean.listSecurityMovementResult(securityBalanceMovTO);
	}
	
	/**
	 * Count security by issuance.
	 *
	 * @param securityTO the security to
	 * @return the integer
	 */
	public Integer countSecurityByIssuance(SecurityTO securityTO){
		return securitiesServiceBean.countSecurityByIssuance(securityTO);
	}
	
	/**
	 * Find securities.
	 *
	 * @param securityTO the security to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Security> findSecurities(SecurityTO securityTO) throws ServiceException{
		return securitiesServiceBean.findSecuritiesServiceBean(securityTO);
	}
	


	/**
	 * Registry massive securities.
	 *
	 * @param processFileTO the process file to
	 */
	public void registryMassiveSecurities(ProcessFileTO processFileTO) {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
        ValidationProcessTO validationTO = processFileTO.getValidationProcessTO();
        validationTO.setLstValidations(new ArrayList<RecordValidationType>());
        
        // #1 separate transaction: save file
        Long idInterfaceProcess = interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(processFileTO, 
        																ExternalInterfaceReceptionType.MANUAL.getCode(), loggerUser);
        
        // #2 separate transaction: save operations independently
		for(ValidationOperationType operation : validationTO.getLstOperations()){
			IssuanceRegisterTO issuanceRegisterTO = (IssuanceRegisterTO) operation;
			issuanceRegisterTO.setIdInterfaceProcess(idInterfaceProcess);
			
			try {
				
				validationTO.getLstValidations().add(securityServiceBean.validateAndSaveIssuanceTx(issuanceRegisterTO, loggerUser));
				
				interfaceComponentServiceBean.get().saveInterfaceTransactionTx(issuanceRegisterTO, BooleanType.YES.getCode(), loggerUser);
				
				
			} catch (Exception e) {
				validationTO.getLstValidations().addAll(parameterServiceBean.generateErrorValidationProcess(issuanceRegisterTO, e).getLstValidations());				
				
				interfaceComponentServiceBean.get().saveInterfaceTransactionTx(issuanceRegisterTO, BooleanType.NO.getCode(), loggerUser);
								
			} 
		}
		//#3 separate transaction: update file independently
		Integer processState= ExternalInterfaceReceptionStateType.CORRECT.getCode();
		interfaceComponentServiceBean.get().saveExternalInterfaceResponseFileTx(
				processFileTO.getIdProcessFilePk(), validationTO.getLstValidations(),
				processFileTO.getStreamResponseFileDir(), 
				ComponentConstant.INTERFACE_TAG_ISSUANCE_BCB, 
				ComponentConstant.INTERFACE_RECORD_ISSUANCE_BCB, 
				processState, loggerUser);
	}
	
	/**
	 * Find securities trcr.
	 *
	 * @param listSecurity the list security
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Object> findSecuritiesTRCR(List<Security> listSecurity) throws ServiceException{
		
		List<String> listIdSecurity=new ArrayList<>();
		for (Security security : listSecurity) {
			listIdSecurity.add(security.getIdSecurityCodePk());
		}
		
		return securityServiceBean.findSecuritiesTRCR(listIdSecurity);
	}
	
	/**
	 * Find securities coupon detail trcr.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<GenerationFileDetailCouponTRCRTO> findSecuritiesCouponDetailTRCR(String idSecurityCodePk) throws ServiceException{
		
		List<Object> dataList=securityServiceBean.findSecuritiesCouponDetailTRCR(idSecurityCodePk);

		Class entityTOClass=GenerationFileDetailCouponTRCRTO.class;
		List<String> listOfAlias=Arrays.asList("nroCupon","fechaVenci","amortizaK","tasaCuponDif");
		List<Object> listClass=new ArrayList<>();

		List<GenerationFileDetailCouponTRCRTO> listDetailCouponTRCR=new ArrayList<>();
				
		listClass=CommonsUtilities.filledBean(listOfAlias, entityTOClass, dataList);
		for (Object object : listClass) {
			listDetailCouponTRCR.add((GenerationFileDetailCouponTRCRTO)object);
		}
		
		return listDetailCouponTRCR;
	}
	
	/**
	 * Gets the ting mnemonic participant.
	 *
	 * @param idParticipantPk the id participant pk
	 * @return the ting mnemonic participant
	 */
	public String gettingMnemonicParticipant(Long idParticipantPk){
		
		Participant participant=securityServiceBean.find(Participant.class, idParticipantPk);
		
		return participant.getMnemonic();
	}
	
	/**
	 * Creates the bean file tr cr.
	 *
	 * @param listSecurity the list security
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Object> createBeanFileTrCr(List<Security> listSecurity) throws ServiceException{
		Class entityTOClass=GenerationFileSecurityTRCRTO.class;
		List<String> listOfAlias=null;
		List<Object> dataList=null;
		List<Object> listClass=new ArrayList<>();
		dataList=findSecuritiesTRCR(listSecurity);
		listOfAlias=Arrays.asList("idSecurityCodePk","instrumentType","serie","emisor","registryDate",
				"expirationDate","currency","valorEmision","valorNominal","tasaRend",
				"valuacion","nroCupones","plazoAmortiza","prepago","subordinado",
				"tipoBono","tipoTasa","tasaDif","negFci","subProducto","tasaPen");
				
		listClass=CommonsUtilities.filledBean(listOfAlias, entityTOClass, dataList);
		return listClass;
	}
	
	
	/**
	 * Send report.
	 *
	 * @param institutionType the institution type
	 * @param file the file
	 * @param participantId the participant id
	 * @param nameFile the name file
	 * @param reportId the report id
	 * @throws ServiceException the service exception
	 */
	public void sendReport(InstitutionType institutionType,List<File> file, Long participantId, String nameFile, Long reportId) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		//Map to put detail of send
		Map<String,String> parametersMap = new HashMap<>();
		
		//get ReportUser to send remote Method of Reports
		ReportUser reportUser = new ReportUser();
		reportUser.setIndExtInterface(BooleanType.YES.getCode());
		reportUser.setInstitutionType(institutionType.getCode());
		reportUser.setShowNotification(BooleanType.YES.getCode());
		
		if(institutionType.equals(InstitutionType.PARTICIPANT) || institutionType.equals(InstitutionType.AFP)){
			reportUser.setPartcipantCode(participantId);
		}

		try{
			//Get all report generated by External Interface
			List<ReportFile> reportFileList = new ArrayList<ReportFile>();
			for(File fileContent: file){
				ReportFile reportFile = new ReportFile();
				reportFile.setFile(XMLUtils.getBytesFromFile(fileContent));
				reportFile.setPhysicalName(fileContent.getName().substring(0,fileContent.getName().lastIndexOf(".")));
				reportFile.setReportType(ReportFormatType.XML.getCode());
				reportFileList.add(reportFile);
			}
	
			//send files on reports bitacore
			reportGenerationService.get().saveReportFile(reportId, reportUser, parametersMap, null, loggerUser, reportFileList);
		} catch (ServiceException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}catch(NullPointerException ex){
			throw new ServiceException();
		}
	}
	
	/**
	 * Checks if is placement segments.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @return true, if is placement segments
	 * @throws ServiceException the service exception
	 */
	public boolean isPlacementSegments(String idSecurityCodePk) throws ServiceException{
		
		boolean isPlacementSegments =false;
		List<Object> listObject = null;
		listObject=securityServiceBean.isPlacementSegments(idSecurityCodePk);
		if (listObject.size()>0)
		{
			isPlacementSegments=true;
		}
		return isPlacementSegments;
	}
	
	/**
	 * Gets the holder accounts.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder accounts
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> getHolderAccounts(HolderAccountTO holderAccountTO) throws ServiceException {
		return holderAccountComponentServiceBean.getHolderAccountListComponentServiceBean(holderAccountTO );
	}
	
	/**
	 * Confirm Withdraw expired Securities
	 * @param listSecurityOverdueTO
	 * @throws ServiceException
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SIRTEX_EARLY_PAYMENT_REJECT)
	public void confirmWithdrawExpiredSecurities(List<SecurityOverdueTO> listSecurityOverdueTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        
        securityServiceBean.confirmWithdrawExpiredSecurities(listSecurityOverdueTO,loggerUser);
		
	}
	
	/***
	 * Find Security For Expiration By Issuer
	 * @param securityTO
	 * @return
	 * @throws ServiceException
	 */
	public List<ExpirationSecurityTO> findSecForExpirationByIssuer(SecurityTO securityTO) throws ServiceException{
		return securityServiceBean.findSecForExpirationByIssuer(securityTO);
	}
	
	
	/**
	 * Update Security For Expiration By Issuer
	 * @param lstExpirationSecurityTO
	 * @throws ServiceException
	 */
	public void updateSecForExpirationByIssuer(List<ExpirationSecurityTO> lstExpirationSecurityTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		for(ExpirationSecurityTO objExpirationSecurityTO : lstExpirationSecurityTO){
			securityServiceBean.updateSecurityExpiration(loggerUser,objExpirationSecurityTO);
		}
	}
	
	/**
	 * Find Securities for Consult
	 */
	public List<SecurityConsultTO> findSecuritiesForConsult(SecurityTO securityTO) throws ServiceException{
		return securityServiceBean.findSecuritiesForConsult(securityTO);
	}
	
	/**
	 * Find Securities for Consult
	 */
	public BigDecimal getIssuanceAmount(String idIssuancePk) throws ServiceException{
		return securityServiceBean.getIssuanceAmount(idIssuancePk);
	}
	
	/**
	 * 
	 */
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=180)
	public void updateSIRTEXnegotiation (List<Object> listSecurity) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		for(Object securityId : listSecurity) {
			Security security = securityServiceBean.findSecurityFromSerie(securityId.toString());
			securityServiceBean.updateSIRTEXnegotiation(loggerUser, security, "1"); 
		}		
	}	
	/**Tiempo para habilitar valores a sirtex*/
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=180)
	public void updateSIRTEXnegotiationDisable (List<Object> listSecurity) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		for(Object securityId : listSecurity) {
			Security security = securityServiceBean.findSecurityFromSerie(securityId.toString());
			securityServiceBean.updateSIRTEXnegotiation(loggerUser, security, "0"); 
		}
	}		
	
	/**
	 * 
	 * @return
	 */
	public List<Object> findSirtexNegotiation (String idIssuerPk,Integer securityClass,Integer currency,Integer personType,String withCoupons, String issueanceDate) {
		return securityServiceBean.findSirtexNegotiation(idIssuerPk, securityClass, currency, personType, withCoupons, issueanceDate);
	}
	
	public List<Object> findSirtexLiveOperations (String idIssuerPk) {
		return securityServiceBean.findSirtexLiveOperations(idIssuerPk);
	}
	
	public boolean validateRangeNumberShare (Security newSecurity) throws ServiceException{
		SecurityTO securityTO = new SecurityTO();
		securityTO.setIdIssuanceCodePk(newSecurity.getIssuance().getIdIssuanceCodePk());
		securityTO.setInstrumentType(InstrumentType.VARIABLE_INCOME.getCode());
		securityTO.setStateSecurity(SecurityStateType.REGISTERED.getCode());
		List<Security> lstSecurity = securityServiceBean.findSecuritiesForShare(securityTO,newSecurity.getSubClass());
		for (Security objSecurity : lstSecurity) {
			if(Validations.validateIsNotNullAndPositive(objSecurity.getNumberShareInitial())&&
					Validations.validateIsNotNullAndPositive(objSecurity.getNumberShareFinal()) && 
					!objSecurity.getIdSecurityCodePk().equals(newSecurity.getIdSecurityCodePk())) {
				if(newSecurity.getNumberShareFinal()>=(objSecurity.getNumberShareInitial()) &&
						newSecurity.getNumberShareFinal()<=(objSecurity.getNumberShareFinal()) )
					return false;
				if(newSecurity.getNumberShareInitial()>=(objSecurity.getNumberShareInitial()) &&
						newSecurity.getNumberShareInitial()<=(objSecurity.getNumberShareFinal()) )
					return false;
			}
		}
		return true;
	}
}
