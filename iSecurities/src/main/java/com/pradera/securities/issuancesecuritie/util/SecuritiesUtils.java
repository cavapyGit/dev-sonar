package com.pradera.securities.issuancesecuritie.util;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.AmortizationPaymentSchedule;
import com.pradera.model.issuancesecuritie.IntPaymentScheduleReqHi;
import com.pradera.model.issuancesecuritie.InterestPaymentSchedule;
import com.pradera.model.issuancesecuritie.ProgramAmoCouponReqHi;
import com.pradera.model.issuancesecuritie.ProgramAmortizationCoupon;
import com.pradera.model.issuancesecuritie.ProgramIntCouponReqHi;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.SecurityCodeFormat;
import com.pradera.model.issuancesecuritie.type.CalendarDayType;
import com.pradera.model.issuancesecuritie.type.CalendarMonthType;
import com.pradera.model.issuancesecuritie.type.InterestPeriodicityType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SecuritiesUtils.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@ApplicationScoped
public class SecuritiesUtils {
	
	/** The last calendar day type. */
private CalendarDayType lastCalendarDayType=null;

/** The logger. */
private @Inject PraderaLogger logger;

/** The min years tax pay. */
private static Integer MIN_YEARS_TAX_PAY=Integer.valueOf(3);

/** The general parameter facade. */
@EJB
private GeneralParametersFacade generalParameterFacade;
	
	/**
	 * Interest rate calculate.
	 *
	 * @param security the security
	 * @param periodicity the periodicity
	 * @param interestRate the interest rate
	 * @param couponBeginDate the coupon begin date
	 * @param couponDays the coupon days
	 * @param couponNumber the coupon number
	 * @param septemberLeap the september leap
	 * @param nextYear the next year
	 * @return the big decimal
	 */
	public BigDecimal interestRateCalculate(Security security, Integer periodicity, BigDecimal interestRate, Date couponBeginDate, 
												    Integer couponDays, Integer couponNumber,
												    Date septemberLeap, Date nextYear) {
		if(security.isByCouponIntPaymentModality()){
			BigDecimal rate=interestRate.divide(BigDecimal.valueOf(100),MathContext.DECIMAL128);
			BigDecimal factor=null;
			
			Date couponExpirationDate=CommonsUtilities.addDaysToDate(couponBeginDate, couponDays);
			
			Integer intervalDays=couponDays;
			
			if(security.is_30CalendarMonth() && security.is_360CalendarDay()){
				
				factor=rate.divide( new BigDecimal(CalendarDayType._360.getIntegerValue()),MathContext.DECIMAL128 );
				factor=factor.multiply( BigDecimal.valueOf(InterestPeriodicityType.get( periodicity ).getIndicator1() * 
														   CalendarMonthType._30.getIntegerValue()) );
				factor=factor.multiply(GeneralConstants.HUNDRED_VALUE_BIGDECIMAL);

				
			}else if(security.isActualCalendarMonth() && security.is_360CalendarDay()){
			
				factor=rate.divide( new BigDecimal(CalendarDayType._360.getIntegerValue()),MathContext.DECIMAL128 );
				factor=factor.multiply( BigDecimal.valueOf(intervalDays) );
				factor=factor.multiply(GeneralConstants.HUNDRED_VALUE_BIGDECIMAL);
				
			}else if(security.isActualCalendarMonth() && security.is_365CalendarDay()){

				factor=rate.divide( new BigDecimal(CalendarDayType._365.getIntegerValue()),MathContext.DECIMAL128 );
				factor=factor.multiply( BigDecimal.valueOf(intervalDays) );
				factor=factor.multiply(GeneralConstants.HUNDRED_VALUE_BIGDECIMAL);
				
			}else if(security.isActualCalendarMonth() && security.isActualCalendarDay()){
				logger.debug("NUMERO DE CUPON " +couponNumber);
				
				if((CommonsUtilities.isLeapYearDate(couponBeginDate) || CommonsUtilities.isLeapYearDate(couponExpirationDate)) && 
				    nextYear==null && !CalendarDayType.ACTUAL.equals(lastCalendarDayType)){
					
					Calendar cal=Calendar.getInstance();
					
					if(CommonsUtilities.isLeapYearDate(couponBeginDate))
						cal.setTime(couponExpirationDate);
					else
						cal.setTime(couponExpirationDate);
					
					cal.set(Calendar.MONTH, 1);
					cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
					septemberLeap=cal.getTime();
 					if(CommonsUtilities.isBetweenDate(septemberLeap, couponBeginDate, couponExpirationDate)){
						nextYear=CommonsUtilities.addYearsToDate(couponBeginDate, GeneralConstants.ONE_VALUE_INTEGER);
					}
				}
				if(nextYear==null){
					logger.debug("Cupon "+couponNumber + " cantidad de dias "+CalendarDayType._365.getIntegerValue());
					
					factor=rate.divide( new BigDecimal(CalendarDayType._365.getIntegerValue()),MathContext.DECIMAL128 );
					factor=factor.multiply( BigDecimal.valueOf(intervalDays) );
					factor=factor.multiply(GeneralConstants.HUNDRED_VALUE_BIGDECIMAL);
					lastCalendarDayType=CalendarDayType._365;
				}else{
					logger.debug( "Cupon "+couponNumber + " cantidad de dias "+CalendarDayType.ACTUAL.getIntegerValue() );
					
					factor=rate.divide( new BigDecimal(CalendarDayType.ACTUAL.getIntegerValue()),MathContext.DECIMAL128 );
					factor=factor.multiply( BigDecimal.valueOf(intervalDays) );
					factor=factor.multiply(GeneralConstants.HUNDRED_VALUE_BIGDECIMAL);
					lastCalendarDayType=CalendarDayType.ACTUAL;
					if(nextYear.equals(CommonsUtilities.truncateDateTime(couponExpirationDate))){
						nextYear=null;
					}
				}
			}
			// quitando el redondeo
//			factor=CommonsUtilities.roundRateFactor(factor);
			return factor;
		}else if(security.isAtMaturityIntPaymentModality()){
			Integer calendarDays = CalendarDayType.get(security.getCalendarDays()).getIntegerValue();
			Integer periocityDays= couponDays;
			if(interestRate!=null && calendarDays!=null && periocityDays!=null){
				BigDecimal rate=interestRate.divide(BigDecimal.valueOf(100),MathContext.DECIMAL128);
				BigDecimal factor = rate.divide(new BigDecimal(calendarDays),MathContext.DECIMAL128);
						   factor = factor.multiply(new BigDecimal(periocityDays));
						   factor = factor.multiply(GeneralConstants.HUNDRED_VALUE_BIGDECIMAL);
				return factor;
			}else{
				return interestRate;
			}
		}
		return interestRate;
	}
	
//	revertido salida a produccion issues: 100,148,143
//	public BigDecimal amortizationFactorCalculate(Security security, BigDecimal couponAmount) {		
//		BigDecimal factor = null;
//		BigDecimal initNomVal = security.getInitialNominalValue();
//		couponAmount = couponAmount.multiply(GeneralConstants.HUNDRED_VALUE_BIGDECIMAL);
//		factor = couponAmount.divide(initNomVal, MathContext.DECIMAL128);
//		
//		return factor;
//	}
	
	/**
	 * Check digit generate.
	 *
	 * @param strbIsinCode the strb isin code
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String checkDigitGenerate(String strbIsinCode) throws ServiceException{
		StringBuilder isinCode=new StringBuilder(strbIsinCode);
		BigDecimal isinCodeDecimal=null;
		BigDecimal sum=new BigDecimal(BigInteger.ZERO);
		BigDecimal factor=new BigDecimal(2);
		BigDecimal checkDigit=new BigDecimal(BigInteger.ZERO);
		
		for(int i=0;i<isinCode.length();i++){
			char character=isinCode.charAt(i);
			if(Character.isLetter(character)){
				int asiiCode=(int)character;
				asiiCode=asiiCode-55;
				isinCode.replace(i, i+1, String.valueOf(asiiCode));
			}
		}
		isinCodeDecimal=new BigDecimal(isinCode.toString());
		
		while(isinCodeDecimal.compareTo(BigDecimal.ZERO)!=0){
			BigDecimal currentDigit=isinCodeDecimal.remainder(BigDecimal.TEN);
			isinCodeDecimal=isinCodeDecimal.divide(BigDecimal.TEN, BigDecimal.ROUND_FLOOR);
			if(currentDigit.multiply(factor).compareTo(BigDecimal.TEN)==-1){
				sum=sum.add( currentDigit.multiply(factor) );
			}else{
				BigDecimal multiplyCurrentDigit=currentDigit.multiply(factor);
				while(multiplyCurrentDigit.compareTo(BigDecimal.ZERO)!=0){
					BigDecimal individualDigit=multiplyCurrentDigit.remainder(BigDecimal.TEN);
					multiplyCurrentDigit=multiplyCurrentDigit.divide(BigDecimal.TEN, BigDecimal.ROUND_FLOOR);
					sum=sum.add( individualDigit );
				}
			}
			if(factor.compareTo(BigDecimal.valueOf(2))==0)
				factor=new BigDecimal(1);
			else
				factor=new BigDecimal(2);
		}
		checkDigit=sum.remainder(BigDecimal.TEN);
		if(checkDigit.compareTo(BigDecimal.ZERO)==0){
			return "0";
		}
		checkDigit=BigDecimal.TEN.subtract( checkDigit );
		return checkDigit.toString();
	}

	/**
	 * Alpha numeric sequnce issuance code.
	 *
	 * @param currentCode the current code
	 * @return the string
	 */
	public String alphaNumericSequnceIssuanceCode(String currentCode){
        Integer numbers = Integer.valueOf(currentCode.substring(1));  
        Integer letters = Character.toUpperCase(currentCode.charAt(0)) - 'A';  

        numbers += 1;  
        if (numbers >= 10000000) {  
        	letters += numbers/10000000;  
        	numbers %= 10000000;               
        }  
        
        return processAutoincremental(numbers,letters);
	}
    
    /**
     * Process autoincremental.
     *
     * @param number the number
     * @param letter the letter
     * @return the string
     */
    public String processAutoincremental(Integer number,Integer letter) {   
        char[] letters = new char[1];  
        int n = letter;  
        for (int i = 0; i < 1; i++) {  
                letters[0] = (char)('A' + (n%26));  
                n /= 26;  
        }  
       String strLetter=new String(letters);
       if(strLetter.equals(":"))
    	   strLetter="A";
       if(number==0)
    	   number++;
        return strLetter + String.format("%05d", number);  
    }  
    
    
    /**
     * Process validate security code format.
     *
     * @param security the security
     * @param securityCodeFormat the security code format
     * @param value the value
     * @return the boolean
     * @throws ServiceException the service exception
     */
    // Aqui hago las validaciones segun el excel
    public Boolean processValidateSecurityCodeFormat(Security security, SecurityCodeFormat securityCodeFormat, Object value) throws ServiceException{

		ParameterTable pTable = null;
    	
    	if(value.toString().length() < securityCodeFormat.getMinLegth().intValue() || value.toString().length() > securityCodeFormat.getMaxLength().intValue()){
    		throw new ServiceException(ErrorServiceType.SECURITY_CODE_FORMAT_LENGTH);
    	}
    	
    	if(securityCodeFormat.getIndDash().compareTo( GeneralConstants.ZERO_VALUE_INTEGER )==0){
    		String[] dash = value.toString().split("-");
    		if(dash.length > 1){
    			throw new ServiceException(ErrorServiceType.SECURITY_CODE_FORMAT_DASH);
    		}
    	}
    	if(securityCodeFormat.getIndAlphanumeric().compareTo( GeneralConstants.ZERO_VALUE_INTEGER )==0){
    		try {
        		Integer formatString = Integer.parseInt(value.toString());
			} catch (NumberFormatException e) {
				throw new ServiceException(ErrorServiceType.SECURITY_CODE_FORMAT_ALPHANUMERIC);
			}
    	}
//    	if(!securityCodeFormat.getPositionMnemonic().equalsIgnoreCase("0")){
//    		
//    		String[] rango = securityCodeFormat.getPositionMnemonic().toString().split("-");
//    		if(rango.length > 1){
//	    		if(!value.toString().substring(Integer.parseInt(rango[0])-1, Integer.parseInt(rango[1])).equalsIgnoreCase(security.getIssuer().getMnemonic().toString())){
//	        		throw new ServiceException(ErrorServiceType.SECURITY_CODE_FORMAT_ISSUER);
//	    		}
//    		}else{
//    			if(!value.toString().substring(Integer.parseInt(rango[0])-1, Integer.parseInt(rango[0])).equalsIgnoreCase(security.getIssuer().getMnemonic().toString())){
//	        		throw new ServiceException(ErrorServiceType.SECURITY_CODE_FORMAT_ISSUER);
//	    		}
//    		}
//    	}
    	if(!securityCodeFormat.getPositionCurrency().equalsIgnoreCase("0")){
			pTable = generalParameterFacade.getParamDetailServiceFacade(security.getCurrency());
    		if(!value.toString().substring(Integer.parseInt(securityCodeFormat.getPositionCurrency())-1,Integer.parseInt(securityCodeFormat.getPositionCurrency())).equalsIgnoreCase(pTable.getIndicator3().toString())){
        		throw new ServiceException(ErrorServiceType.SECURITY_CODE_FORMAT_CURRENCY);
    		}
    	}
    	if(securityCodeFormat.getPositionIssueCheckDigit() != 0){
    		//In blank for the moment
    		
    	}
    	if(!securityCodeFormat.getPositionNumericalData().equalsIgnoreCase("0")){

    		String[] rango = securityCodeFormat.getPositionNumericalData().toString().split("-");
    		Integer num = 0;
    		try {
        		if(rango.length > 1){
        			if(value.toString().length() < Integer.parseInt(rango[1])){
        				throw new ServiceException(ErrorServiceType.SECURITY_CODE_FORMAT_NUMERIC);
        			}
        			num = Integer.parseInt(value.toString().substring(Integer.parseInt(rango[0])-1, Integer.parseInt(rango[1])));
        		}else{
        			if(value.toString().length() < Integer.parseInt(rango[0])){
        				throw new ServiceException(ErrorServiceType.SECURITY_CODE_FORMAT_NUMERIC);
        			}
        			num = Integer.parseInt(value.toString().substring(Integer.parseInt(rango[0])-1, Integer.parseInt(rango[0])));
        		}
			} catch (NumberFormatException e) {
				throw new ServiceException(ErrorServiceType.SECURITY_CODE_FORMAT_NUMERIC);
			}
    		
    	}
    	if(!securityCodeFormat.getPositionSecurityTerm().equalsIgnoreCase("0")){
    		if(Validations.validateIsNotNull(security.getSecurityDaysTerm())){
	    		String[] rango = securityCodeFormat.getPositionSecurityTerm().toString().split("-");
	    		if(rango.length > 1){
		    		if(!value.toString().substring(Integer.parseInt(rango[0])-1, Integer.parseInt(rango[1])).equalsIgnoreCase(security.getSecurityDaysTerm().toString())){
		        		throw new ServiceException(ErrorServiceType.SECURITY_CODE_FORMAT_TERM);
		    		}
	    		}else{
		    		if(!value.toString().substring(Integer.parseInt(rango[0])-1, Integer.parseInt(rango[0])).equalsIgnoreCase(security.getSecurityDaysTerm().toString())){
		        		throw new ServiceException(ErrorServiceType.SECURITY_CODE_FORMAT_TERM);
		    		}
	    		}
    		} else throw new ServiceException(ErrorServiceType.SECURITY_CODE_FORMAT_TERM);
    	}
    	if(!securityCodeFormat.getPositionIssuanceYear().equalsIgnoreCase("0")){

    		String[] rango = securityCodeFormat.getPositionIssuanceYear().toString().split("-");
    		Calendar calendar = Calendar.getInstance();
    		calendar.setTime(security.getIssuance().getIssuanceDate());
			if(rango.length <= 1){
				throw new ServiceException(ErrorServiceType.SECURITY_CODE_FORMAT_YEAR);
			}
			if(value.toString().length() < Integer.parseInt(rango[1])){
				throw new ServiceException(ErrorServiceType.SECURITY_CODE_FORMAT_YEAR);
			}
    		if(!value.toString().substring(Integer.parseInt(rango[0])-1, Integer.parseInt(rango[1])).equalsIgnoreCase(Integer.toString(calendar.get(Calendar.YEAR)).substring(2, 4))){
        		throw new ServiceException(ErrorServiceType.SECURITY_CODE_FORMAT_YEAR);
    		}
    		
    	}
    	if(!securityCodeFormat.getPositionWeek().equalsIgnoreCase("0")){

    		String[] rango = securityCodeFormat.getPositionWeek().toString().split("-");
    		Calendar calendar = Calendar.getInstance();
    		calendar.setTime(security.getIssuance().getIssuanceDate());
			if(rango.length <= 1){
				throw new ServiceException(ErrorServiceType.SECURITY_CODE_FORMAT_WEEK);
			}
    		if(!value.toString().substring(Integer.parseInt(rango[0])-1, Integer.parseInt(rango[1])).equalsIgnoreCase(Integer.toString(calendar.get(Calendar.WEEK_OF_YEAR)))){
        		throw new ServiceException(ErrorServiceType.SECURITY_CODE_FORMAT_WEEK);
    		}
    		
    		
    	}
		
    	return true;
    }
    
	/**
	 * Cast payment schedule file.
	 *
	 * @param inputStream the input stream
	 * @return the security
	 * @throws InvalidFormatException the invalid format exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws InstantiationException the instantiation exception
	 * @throws IllegalAccessException the illegal access exception
	 * @throws ServiceException the service exception
	 */
	public Security castPaymentScheduleFile(InputStream inputStream ) throws InvalidFormatException, IOException, InstantiationException, IllegalAccessException, ServiceException {
		
	 	
		  Workbook wb = WorkbookFactory.create(inputStream);
		
		  Sheet mySheet = wb.getSheetAt(0);
		 
		 Iterator rowIterator = mySheet.rowIterator();
		 HSSFRow hssRow=null;
		 XSSFRow xssRow=null;
			 
		 Iterator iterator=null;
		 HSSFCell hssfCell = null;
		 XSSFCell xssCell=null;
		 DataFormatter dataFormatter = new DataFormatter();
		 
		 Security securityUploaded=new Security();

		 
		  for(int sheetNum = 0; sheetNum < wb.getNumberOfSheets(); sheetNum++) {
		      Sheet sheet = wb.getSheetAt(sheetNum);
		      for(Row r : sheet) {
		          for(Cell c : r) {
		              if(c.getCellType() == Cell.CELL_TYPE_FORMULA) {
		            	  throw new ServiceException(ErrorServiceType.SECURITY_EXCEL_WITH_FORMULE);
		              }
		          }
		      }
		  }
		 
		 while (rowIterator.hasNext()){
			 
			 ProgramInterestCoupon programInterestCoupon=null;
			 ProgramAmortizationCoupon programAmortizationCoupon=null;
			 Integer registerType=null;
			 
			 Object rowIterObj=rowIterator.next();
			 if(rowIterObj instanceof HSSFRow ){
				 hssRow = (HSSFRow) rowIterObj;
				 iterator=hssRow.cellIterator();
			 }else if(rowIterObj instanceof XSSFRow){
				 xssRow = (XSSFRow) rowIterObj;
				 iterator=xssRow.cellIterator();
			 }
			 
			 int ind=0;

			 
			 hssfCell = null;
			 xssCell=null;
			 
			 while (iterator.hasNext()){
				 Object cellIterObj=iterator.next();
				if(cellIterObj.toString().isEmpty()){
					break;
				}
				if(ind==0){

					try {
						Double regType=Double.parseDouble(cellIterObj.toString());
						registerType=Integer.valueOf(regType.intValue());
					} catch (NumberFormatException ne) {
						ne.printStackTrace();
						break;
					}
					if(PaymentScheduleFileRegisterType.SECURITY_REGISTER.getRegisterType().equals( registerType )){
						securityUploaded=new Security();
					}else if(PaymentScheduleFileRegisterType.PROGRAM_INTEREST.getRegisterType().equals( registerType )){
						programInterestCoupon=new ProgramInterestCoupon();
					}else if(PaymentScheduleFileRegisterType.PROGRAM_AMORTIZATION.getRegisterType().equals( registerType )){
						programAmortizationCoupon=new ProgramAmortizationCoupon();
					}	
				}
				if(ind>0){
					String theClass=null;
					String strValue=null;
					Date dateValue=null;
					BigDecimal bigDecValue=null;
					Integer intValue=null;
					
  				switch (ind) {
					case 1:
						theClass = PaymentScheduleFileRegisterType.get(registerType).getSecondColDataType();
						break;
					case 2:
						theClass = PaymentScheduleFileRegisterType.get(registerType).getThreeColDataType();
						break;
					case 3:
						theClass = PaymentScheduleFileRegisterType.get(registerType).getFourColDataType();
						break;
					case 4:
						theClass = PaymentScheduleFileRegisterType.get(registerType).getFiveColDataType();
						break;
					}

					if(theClass!=null){

							if(theClass.equals( String.class.getName() )){
								strValue=cellIterObj.toString();
							}else if(theClass.equals( Integer.class.getName() )){
								Double dblVal=Double.parseDouble(cellIterObj.toString());
								intValue=Integer.valueOf(dblVal.intValue());
							}else if(theClass.equals( Date.class.getName() )){
								
								
								dateValue = CommonsUtilities.convertStringtoDate(cellIterObj.toString(),
										GeneralConstants.DATE_FORMAT_PATTERN_ALT_01);

								if (!(dateValue instanceof Date)) {
									String message = "Error en la fecha de vencimiento en el archivo excel. Verifique";
									JSFUtilities.showMessageOnDialog(PropertiesUtilities
											.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), message);
									JSFUtilities.showSimpleValidationDialog();
									return null;
								}
																
							}else if(theClass.equals( BigDecimal.class.getName() )){
								bigDecValue=new BigDecimal(cellIterObj.toString());
								bigDecValue = bigDecValue.setScale(12, BigDecimal.ROUND_HALF_UP);
							}
						
	    				switch (ind) {
						case 1:
							if(PaymentScheduleFileRegisterType.SECURITY_REGISTER.getRegisterType().equals( registerType )){
								securityUploaded.setIdSecurityCodeOnly( strValue );
							}else if(PaymentScheduleFileRegisterType.PROGRAM_INTEREST.getRegisterType().equals( registerType )){
								programInterestCoupon.setCouponNumber(intValue);
							}else if(PaymentScheduleFileRegisterType.PROGRAM_AMORTIZATION.getRegisterType().equals( registerType )){
								programAmortizationCoupon.setCouponNumber(intValue);
							}	
							break;
						case 2:
							if(PaymentScheduleFileRegisterType.SECURITY_REGISTER.getRegisterType().equals( registerType )){
								securityUploaded.setIssuanceDate(dateValue );
							}else if(PaymentScheduleFileRegisterType.PROGRAM_INTEREST.getRegisterType().equals( registerType )){
								programInterestCoupon.setExperitationDate(dateValue);
							}else if(PaymentScheduleFileRegisterType.PROGRAM_AMORTIZATION.getRegisterType().equals( registerType )){
								programAmortizationCoupon.setExpirationDate(dateValue);
							}	
							break;
						case 3:
							if(PaymentScheduleFileRegisterType.SECURITY_REGISTER.getRegisterType().equals( registerType )){
								securityUploaded.setExpirationDate(dateValue);
							}else if(PaymentScheduleFileRegisterType.PROGRAM_INTEREST.getRegisterType().equals( registerType )){
								programInterestCoupon.setInterestRate( bigDecValue );
							}else if(PaymentScheduleFileRegisterType.PROGRAM_AMORTIZATION.getRegisterType().equals( registerType )){
								programAmortizationCoupon.setAmortizationFactor( bigDecValue );
							}	
							break;
						case 4:
							if(PaymentScheduleFileRegisterType.PROGRAM_INTEREST.getRegisterType().equals( registerType )){
								programInterestCoupon.setCouponAmount( bigDecValue );
							}
							break;
						}
					}
					
					
				}
				ind++;
			 }
			 
			if(PaymentScheduleFileRegisterType.PROGRAM_INTEREST.getRegisterType().equals( registerType )){
				if(programInterestCoupon!=null){
					securityUploaded.getInterestPaymentSchedule().getProgramInterestCoupons().add( programInterestCoupon );
				}
			}else if(PaymentScheduleFileRegisterType.PROGRAM_AMORTIZATION.getRegisterType().equals( registerType )){
				if(programAmortizationCoupon!=null){
				securityUploaded.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().add( programAmortizationCoupon );
				}
			}	
		 }
		 return securityUploaded;
	}
	
    /**
     * Checks if is different security file.
     *
     * @param securityRegistry the security registry
     * @param securityFile the security file
     * @return the string
     */
    public String isDifferentSecurityFile(Security securityRegistry, Security securityFile){
    	StringBuilder strResult=new StringBuilder();
    	
    	if(!securityRegistry.getIdSecurityCodeOnly().trim().equals(securityFile.getIdSecurityCodeOnly().trim())){
    		strResult.append("<br/>");
    		strResult.append( PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_ERROR_FILE_DIFF_SCREEN_FILE, 
    				new Object[]{PropertiesUtilities.getGenericMessage(GeneralConstants.SECURITY_LBL_CODE_ONLY),
    							 securityRegistry.getIdSecurityCodeOnly(),
    							 securityFile.getIdSecurityCodeOnly()}) );
    	}
    	if(!CommonsUtilities.truncateDateTime( securityRegistry.getIssuanceDate() ).equals( 
    			CommonsUtilities.truncateDateTime( securityFile.getIssuanceDate()))){
    		strResult.append("<br/>");
    		strResult.append( PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_ERROR_FILE_DIFF_SCREEN_FILE, 
    				new Object[]{PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_LBL_ISSUANCE_DATE),
    							CommonsUtilities.convertDatetoString( securityRegistry.getIssuanceDate()),
    							CommonsUtilities.convertDatetoString( securityFile.getIssuanceDate())}) );
    	}
    	if(!CommonsUtilities.truncateDateTime(securityRegistry.getExpirationDate()).equals( 
    			CommonsUtilities.truncateDateTime( securityFile.getExpirationDate() ))){
    		strResult.append("<br/>");
    		strResult.append( PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_ERROR_FILE_DIFF_SCREEN_FILE, 
    				new Object[]{PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_LBL_EXPIRATION_DATE),
    							CommonsUtilities.convertDatetoString( securityRegistry.getExpirationDate()),
    							CommonsUtilities.convertDatetoString( securityFile.getExpirationDate())}) );
    	}
    	if(strResult.length()>0){
    		strResult.insert(0, PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_ERROR_FILE_DIFF_FOUNDS));
    	}
    	return strResult.toString();
    }
    
    /**
     * Checks if is different proram int file.
     *
     * @param intPaymentScheduleReg the int payment schedule reg
     * @param intPaymentScheduleFile the int payment schedule file
     * @return the list
     */
    public List<Map<String,Object>> isDifferentProramIntFile(InterestPaymentSchedule intPaymentScheduleReg, 
    										InterestPaymentSchedule  intPaymentScheduleFile){
    	
    	List<Map<String,Object>> lstResul=new ArrayList<Map<String,Object>>();
    	Map<String, Object> map=new HashMap<String, Object>();
    	boolean difference=false;
    	
    	int cantRows=intPaymentScheduleReg.getProgramInterestCoupons().size();
    	ProgramInterestCoupon progSys=null;
    	ProgramInterestCoupon progFile=null;
    	
    	if(intPaymentScheduleReg.getProgramInterestCoupons().size()!=intPaymentScheduleFile.getProgramInterestCoupons().size()){
    		difference=true;
    		if(intPaymentScheduleFile.getProgramInterestCoupons().size()>intPaymentScheduleReg.getProgramInterestCoupons().size()){
    			cantRows=intPaymentScheduleFile.getProgramInterestCoupons().size();
    		}
    	}

    	if(difference==false){
	    	for(int index=0;index<cantRows;index++){
	    		progSys=intPaymentScheduleReg.getProgramInterestCoupons().get(index);
	    		progFile=intPaymentScheduleFile.getProgramInterestCoupons().get(index);
	    		if(progSys.getCouponNumber().compareTo( progFile.getCouponNumber() )!=0){
	    			difference=true;
	    			break;
	    		}
	    		if(!CommonsUtilities.truncateDateTime(progSys.getExperitationDate()).equals( 
	    				CommonsUtilities.truncateDateTime( progFile.getExperitationDate() ))){
	    			difference=true;
	    			break;
	    		}
	    		if(progSys.getInterestRate().compareTo( progFile.getInterestRate() )!=0){
	    			difference=true;
	    			break;
	    		}
	    	}
    	}
    	if(difference){
    		for(int index=0;index<cantRows;index++){
    			progSys=null;
    			progFile=null;
    			
    			if(index<intPaymentScheduleReg.getProgramInterestCoupons().size()){
    				progSys=intPaymentScheduleReg.getProgramInterestCoupons().get(index);
    			} 
    			if(index<intPaymentScheduleFile.getProgramInterestCoupons().size()){
    				progFile=intPaymentScheduleFile.getProgramInterestCoupons().get(index);
    			}
    			if((intPaymentScheduleReg.getProgramInterestCoupons().size()!=intPaymentScheduleFile.getProgramInterestCoupons().size())
    					|| !CommonsUtilities.truncateDateTime(progSys.getExperitationDate()).equals( 
    		    				CommonsUtilities.truncateDateTime( progFile.getExperitationDate() ))
    		    		|| progSys.getInterestRate().compareTo( progFile.getInterestRate() )!=0
    		    		|| progSys.getCouponAmount().compareTo( progFile.getCouponAmount() )!=0){
        			map=new HashMap<String, Object>();
    	    		map.put("field", PropertiesUtilities.getMessage(PropertiesConstants.PAYMENT_CRON_LBL_NRO_COUPON));
    	    		map.put("system", progSys !=null ? progSys.getCouponNumber() : null);
    	    		map.put("file", progFile !=null ? progFile.getCouponNumber() : null);
    	    		lstResul.add( map );
    			}
    			if((intPaymentScheduleReg.getProgramInterestCoupons().size()!=intPaymentScheduleFile.getProgramInterestCoupons().size())
    					|| !CommonsUtilities.truncateDateTime(progSys.getExperitationDate()).equals( 
    		    				CommonsUtilities.truncateDateTime( progFile.getExperitationDate() ))){
        			map=new HashMap<String, Object>();
    	    		map.put("field", PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_LBL_EXPIRATION_DATE));
    	    		map.put("system", progSys !=null ? CommonsUtilities.convertDatetoString(progSys.getExperitationDate()) : null);
    	    		map.put("file", progFile !=null ? CommonsUtilities.convertDatetoString(progFile.getExperitationDate()) : null);
    	    		lstResul.add( map );
    			}
    			if((intPaymentScheduleReg.getProgramInterestCoupons().size()!=intPaymentScheduleFile.getProgramInterestCoupons().size())
    					|| progSys.getInterestRate().compareTo( progFile.getInterestRate() )!=0){
        			map=new HashMap<String, Object>();
    	    		map.put("field", PropertiesUtilities.getMessage(PropertiesConstants.PAYMENT_CRON_LBL_RATE));
    	    		map.put("system", progSys !=null ? progSys.getInterestRate() : null);
    	    		map.put("file", progFile !=null ? progFile.getInterestRate() : null);
    	    		lstResul.add( map );
    			}
    			if((intPaymentScheduleReg.getProgramInterestCoupons().size()!=intPaymentScheduleFile.getProgramInterestCoupons().size())
    					|| progSys.getCouponAmount().compareTo( progFile.getCouponAmount() )!=0){
        			map=new HashMap<String, Object>();
    	    		map.put("field", PropertiesUtilities.getMessage(PropertiesConstants.LBL_COUPON_AMOUNT));
    	    		map.put("system", progSys !=null ? progSys.getCouponAmount() : null);
    	    		map.put("file", progFile !=null ? progFile.getCouponAmount() : null);
    	    		lstResul.add( map );
    			}
    		}
    	}
    	return lstResul;
    }
    
    /**
     * Checks if is different proram amo file.
     *
     * @param amoPaymentScheduleReg the amo payment schedule reg
     * @param amoPaymentScheduleFile the amo payment schedule file
     * @return the list
     */
    public List<Map<String,Object>> isDifferentProramAmoFile(AmortizationPaymentSchedule amoPaymentScheduleReg, 
    		AmortizationPaymentSchedule  amoPaymentScheduleFile) {

    	List<Map<String,Object>> lstResul=new ArrayList<Map<String,Object>>();
    	Map<String, Object> map=new HashMap<String, Object>();
    	boolean difference=false;
    	
    	int cantRows=amoPaymentScheduleReg.getProgramAmortizationCoupons().size();
    	ProgramAmortizationCoupon progSys=null;
    	ProgramAmortizationCoupon progFile=null;
    	
    	if(amoPaymentScheduleReg.getProgramAmortizationCoupons().size()!=amoPaymentScheduleFile.getProgramAmortizationCoupons().size()){
    		difference=true;
    		if(amoPaymentScheduleFile.getProgramAmortizationCoupons().size()>amoPaymentScheduleReg.getProgramAmortizationCoupons().size()){
    			cantRows=amoPaymentScheduleFile.getProgramAmortizationCoupons().size();
    		}
    	}

    	if(difference==false){
	    	for(int index=0;index<cantRows;index++){
	    		progSys=amoPaymentScheduleReg.getProgramAmortizationCoupons().get(index);
	    		progFile=amoPaymentScheduleFile.getProgramAmortizationCoupons().get(index);
	    		if(progSys.getCouponNumber().compareTo( progFile.getCouponNumber() )!=0){
	    			difference=true;
	    			break;
	    		}
	    		if(!CommonsUtilities.truncateDateTime(progSys.getExpirationDate()).equals( 
	    				CommonsUtilities.truncateDateTime( progFile.getExpirationDate() ))){
	    			difference=true;
	    			break;
	    		}
	    		if(progSys.getAmortizationFactor().compareTo( progFile.getAmortizationFactor() )!=0){
	    			difference=true;
	    			break;
	    		}
	    	}
    	}
    	if(difference){
    		for(int index=0;index<cantRows;index++){
    			progSys=null;
    			progFile=null;
    			
    			if(index<amoPaymentScheduleReg.getProgramAmortizationCoupons().size()){
    				progSys=amoPaymentScheduleReg.getProgramAmortizationCoupons().get(index);
    			} 
    			if(index<amoPaymentScheduleFile.getProgramAmortizationCoupons().size()){
    				progFile=amoPaymentScheduleFile.getProgramAmortizationCoupons().get(index);
    			}
    			
    			if((amoPaymentScheduleReg.getProgramAmortizationCoupons().size()!=amoPaymentScheduleFile.getProgramAmortizationCoupons().size())
    					|| progSys.getCouponNumber().compareTo( progFile.getCouponNumber() )!=0){
        			map=new HashMap<String, Object>();
    	    		map.put("field", PropertiesUtilities.getMessage(PropertiesConstants.PAYMENT_CRON_LBL_NRO_COUPON));
    	    		map.put("system", progSys !=null ? progSys.getCouponNumber() : null);
    	    		map.put("file", progFile !=null ? progFile.getCouponNumber() : null);
    	    		lstResul.add( map );
    			}
    			
				if((amoPaymentScheduleReg.getProgramAmortizationCoupons().size()!=amoPaymentScheduleFile.getProgramAmortizationCoupons().size())
    					|| !CommonsUtilities.truncateDateTime(progSys.getExpirationDate()).equals( 
    		    				CommonsUtilities.truncateDateTime( progFile.getExpirationDate() ))){
        			map=new HashMap<String, Object>();
    	    		map.put("field", PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_LBL_EXPIRATION_DATE));
    	    		map.put("system", progSys !=null ? CommonsUtilities.convertDatetoString(progSys.getExpirationDate()) : null);
    	    		map.put("file", progFile !=null ? CommonsUtilities.convertDatetoString(progFile.getExpirationDate()) : null);
    	    		lstResul.add( map );
    			}
    			
    			if((amoPaymentScheduleReg.getProgramAmortizationCoupons().size()!=amoPaymentScheduleFile.getProgramAmortizationCoupons().size())
    					|| progSys.getAmortizationFactor().compareTo( progFile.getAmortizationFactor() )!=0){
        			map=new HashMap<String, Object>();
    	    		map.put("field", PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_LBL_AMORTIZATION_FACTOR));
    	    		map.put("system", progSys !=null ? progSys.getAmortizationFactor() : null);
    	    		map.put("file", progFile !=null ? progFile.getAmortizationFactor() : null);
    	    		lstResul.add( map );
    			}

    		}
    	}
    	return lstResul;
    }
    
    
    /**
     * Calculate coupon amount program interest.
     *
     * @param security the security
     */
    public void calculateCouponAmountProgramInterest(Security security){
		BigDecimal lastNewNominalValue=null;	
		for(ProgramInterestCoupon progInt : security.getInterestPaymentSchedule().getProgramInterestCoupons()){
			if(lastNewNominalValue==null){
				lastNewNominalValue=security.getInitialNominalValue();
			}
			
			progInt.setCouponAmount( CommonsUtilities.roundAmountCoupon(lastNewNominalValue.multiply(progInt.getInterestRate(),MathContext.UNLIMITED).divide(BigDecimal.valueOf(100)  , MathContext.UNLIMITED)  ));
			//else{
			for(ProgramAmortizationCoupon proAmo : security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons()){
				if(CommonsUtilities.isEqualDate(progInt.getExperitationDate(),proAmo.getExpirationDate())){
					lastNewNominalValue=calculateNewNominalValue(security.getInitialNominalValue(), 
							lastNewNominalValue, proAmo.getAmortizationFactor());
					break;
				}
			}
		}
    }
    
    /**
     * Calculate Coupon Program Interest  .
     *
     * @param security the security
     * @param intPayScheduleReqHi the int pay schedule req hi
     */
    public void calculateCouponAmountProgramInterestHistory(Security security, IntPaymentScheduleReqHi intPayScheduleReqHi){
		BigDecimal lastNewNominalValue=null;	
		for(ProgramIntCouponReqHi progInt : intPayScheduleReqHi.getProgramIntCouponReqHis()){
			if(lastNewNominalValue==null){
				lastNewNominalValue=security.getInitialNominalValue();
			}
			progInt.setCouponAmount( 
					CommonsUtilities.roundAmountCoupon( lastNewNominalValue.multiply(progInt.getInterestRate(),
							MathContext.DECIMAL128).divide(BigDecimal.valueOf(100)  , MathContext.DECIMAL128)) );
			/*	If Program Amortization equals Program Interest then
			 *  Calculated the Amount using updated after Amortization   
			 */
				for(ProgramAmortizationCoupon proAmo : security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons()){					
					if(CommonsUtilities.isEqualDate(progInt.getExpirationDate(),proAmo.getExpirationDate())){
						lastNewNominalValue=calculateNewNominalValue(security.getInitialNominalValue(), 
								lastNewNominalValue, proAmo.getAmortizationFactor());
						break;
					}
				}
			}
    }
       


    /**
     * *
     * Calculate To New Coupon Amount .
     *
     * @param security the security
     * @param intPaySchedule the int pay schedule
     * @param interesRate the interes rate
     * @param couponNumber the coupon number
     * @return the big decimal
     */
    public BigDecimal calculateModifyCouponAmountProgramInterest(Security security, InterestPaymentSchedule intPaySchedule,
    														BigDecimal interesRate, Integer couponNumber){
		BigDecimal lastNewNominalValue=null;	
		for(ProgramInterestCoupon progInt : intPaySchedule.getProgramInterestCoupons()){
			if(lastNewNominalValue==null){
				lastNewNominalValue=security.getInitialNominalValue();
			}
			if(couponNumber.equals(progInt.getCouponNumber())){
				return CommonsUtilities.roundAmountCoupon( lastNewNominalValue.multiply(interesRate,
						MathContext.DECIMAL128).divide(BigDecimal.valueOf(100)  , MathContext.DECIMAL128));
			}else{

				progInt.setCouponAmount(
						CommonsUtilities.roundAmountCoupon( lastNewNominalValue.multiply(progInt.getInterestRate(),
								MathContext.DECIMAL128).divide(BigDecimal.valueOf(100)  , MathContext.DECIMAL128)) );
			}
			/*	If Program Amortization equals Program Interest then
			 *  Calculated the Amount using updated after Amortization   
			 */
			for(ProgramAmortizationCoupon proAmo : security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons()){					
				if(CommonsUtilities.isEqualDate(progInt.getExperitationDate(),proAmo.getExpirationDate())){
					lastNewNominalValue=calculateNewNominalValue(security.getInitialNominalValue(), 
							lastNewNominalValue, proAmo.getAmortizationFactor());
					break;
				}
			}
			}
		return null;
    }
    
    /**
     * Calculate modify coupon amount program amo.
     *
     * @param security the security
     * @param listProgramAmoCouponReqHi the list program amo coupon req hi
     * @param listProgramAmortizationCoupon the list program amortization coupon
     * @param amortizationFactor the amortization factor
     * @param couponNumber the coupon number
     * @return the list
     */
    public List<ProgramAmoCouponReqHi> calculateModifyCouponAmountProgramAmo(Security security,
    													List<ProgramAmoCouponReqHi>  listProgramAmoCouponReqHi,
    													List<ProgramAmortizationCoupon> listProgramAmortizationCoupon,
    													BigDecimal amortizationFactor, Integer couponNumber) {
    	
    	List<ProgramAmoCouponReqHi>  listProgramAmoCouponReqTmp=new ArrayList<>();
    	for (ProgramAmoCouponReqHi programAmortizationCoupon : listProgramAmoCouponReqHi) {
    		listProgramAmoCouponReqTmp.add(SerializationUtils.clone(programAmortizationCoupon));
		}
    	if(couponNumber.equals(listProgramAmoCouponReqHi.size())){
    		return listProgramAmoCouponReqHi;
    	} else {
    		
    		BigDecimal originFactorAmo=null;
    		BigDecimal newFactorAmoNextCoupon=null;
    		
    		for (ProgramAmortizationCoupon progAmoCoupon : listProgramAmortizationCoupon) {
				if(progAmoCoupon.getCouponNumber().equals(couponNumber)){
					originFactorAmo=progAmoCoupon.getAmortizationFactor();
					break;
				}
			}
    		/**
    		 * Get the difference of the modification
    		 */
    		BigDecimal sustrac=CommonsUtilities.subtractTwoDecimal(originFactorAmo, amortizationFactor, 4);
    		
    		/**
    		 * Change Amortization Factor To Next Coupon 
    		 */
    		for (ProgramAmortizationCoupon progAmoCoupon : listProgramAmortizationCoupon) {
    			for (ProgramAmoCouponReqHi progAmoHi : listProgramAmoCouponReqTmp) {
    				
    				if(progAmoCoupon.getCouponNumber().equals(progAmoHi.getCouponNumber())&&
    						progAmoHi.getCouponNumber().equals(couponNumber+1)){
    					newFactorAmoNextCoupon=CommonsUtilities.addTwoDecimal(progAmoCoupon.getAmortizationFactor(), sustrac, 4);
    					progAmoHi.setAmortizationFactor(newFactorAmoNextCoupon);
    					break;
    				}
    			}
    		}
    		
    		
    		/**
    		 * Calculate New Coupon Amount for All Coupon Book
    		 */
    		BigDecimal nominalValue = security.getInitialNominalValue();
    		for (ProgramAmoCouponReqHi progAmoHi : listProgramAmoCouponReqTmp) {
    			Double amortizationAmount = progAmoHi.getAmortizationFactor().doubleValue();
    			progAmoHi.setCouponAmount(CommonsUtilities.getPercentageOf(nominalValue,amortizationAmount));
    		}
    		
    		
    		/**
    		 * Verify Sum All Factor Amortization Equal 100
    		 */
    		
    	}
    	return listProgramAmoCouponReqTmp;
    	
    }
    
    /**
     * Validate coupon amortizations.
     *
     * @param listProgramAmoCouponReqHi the list program amo coupon req hi
     * @return the boolean
     */
    public Boolean validateCouponAmortizations(List<ProgramAmoCouponReqHi> listProgramAmoCouponReqHi){
    	
    	/**
    	 * No Amortizations Factor Negatives
    	 */
    	for (ProgramAmoCouponReqHi programAmoCouponReqHi : listProgramAmoCouponReqHi) {
			
    		if(!(Validations.validateIsNotNullAndPositive(programAmoCouponReqHi.getAmortizationFactor()))){
    			return false;
    		}
		}
    	
    	/**
    	 * Sum Coupons Amortizations equal 1
    	 */
    	BigDecimal sumAmortizationFactor=new BigDecimal(0);
    	for (ProgramAmoCouponReqHi programAmoCouponReqHi : listProgramAmoCouponReqHi) {
    		sumAmortizationFactor=CommonsUtilities.addTwoDecimal(sumAmortizationFactor, programAmoCouponReqHi.getAmortizationFactor(), 4);
		}
    	if(!sumAmortizationFactor.setScale(4).equals(new BigDecimal(100).setScale(4))){
    		return false;
    	}
    	return true;
    }
    
	/**
	 * Calculate new nominal value.
	 *
	 * @param initialNominalValue the initial nominal value
	 * @param lastNewNominalValue the last new nominal value
	 * @param amortizationFactor the amortization factor
	 * @return the big decimal
	 */
	public BigDecimal calculateNewNominalValue(BigDecimal initialNominalValue, BigDecimal lastNewNominalValue, BigDecimal amortizationFactor){
			BigDecimal sustrac=amortizationFactor.multiply(initialNominalValue, MathContext.DECIMAL128).divide(BigDecimal.valueOf(100)  , MathContext.DECIMAL128);
			return lastNewNominalValue.subtract( sustrac  )  ;
	}
    
	/**
	 * Checks if is tax exempt.
	 *
	 * @param security the security
	 * @param haveNit the have nit
	 * @return the integer
	 */
	public Integer isTaxExempt(Security security, boolean haveNit){
		Integer result=BooleanType.NO.getCode();
		if(security.isFixedInstrumentTypeAcc()){
			int yearsTerm=CommonsUtilities.getYearsBetween(security.getIssuanceDate(),security.getExpirationDate());
			if(yearsTerm > MIN_YEARS_TAX_PAY.intValue()){
				result= BooleanType.YES.getCode(); 
			}else if(haveNit){
				result= BooleanType.YES.getCode(); 
			}
		}else if(haveNit){
			result= BooleanType.YES.getCode(); 
		}
		return result;
	}
}
