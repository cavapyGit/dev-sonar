package com.pradera.securities.issuancesecuritie.util;

public class CouponUtilXml {
    final public String FILE_NAME="cupones";
	
	final public String NAME_ROOT_XML="CUPONES";
	final public String NAME_NODE_ELEMENT="CUPON";
	
	final public String ID_CUP_PK="ID_CUP_PK";
    final public String CUP_SERIE= "CUP_SERIE";
    final public String CUP_FECHA_VENCI= "CUP_FECHA_VENCI";
    final public String CUP_AMORTIZAK= "CUP_AMORTIZAK";
    final public String CUP_DSC= "CUP_DSC";
    final public String CUP_DSM= "CUP_DSM";
    final public String CUP_NRO_CUPON= "CUP_NRO_CUPON";
    final public String CUP_INST= "CUP_INST";
    final public String CUP_TASA= "CUP_TASA";
}
