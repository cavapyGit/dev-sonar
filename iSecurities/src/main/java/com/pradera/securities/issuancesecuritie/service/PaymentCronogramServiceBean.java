package com.pradera.securities.issuancesecuritie.service;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.component.corporateevents.service.CorporateEventComponentServiceBean;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.securities.service.SecuritiesServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.corporatives.CorporativeEventType;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.issuancesecuritie.AmoPaymentScheduleReqHi;
import com.pradera.model.issuancesecuritie.AmortizationPaymentSchedule;
import com.pradera.model.issuancesecuritie.IntPaymentScheduleReqHi;
import com.pradera.model.issuancesecuritie.InterestPaymentSchedule;
import com.pradera.model.issuancesecuritie.ProgramAmoCouponReqHi;
import com.pradera.model.issuancesecuritie.ProgramAmortizationCoupon;
import com.pradera.model.issuancesecuritie.ProgramIntCouponReqHi;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.CapitalPaymentModalityType;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.PaymentScheduleReqRegisterType;
import com.pradera.model.issuancesecuritie.type.PaymentScheduleStateType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleSituationType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;
import com.pradera.model.issuancesecuritie.type.RequestStateType;
import com.pradera.securities.issuancesecuritie.to.CouponInformationTO;
import com.pradera.securities.issuancesecuritie.to.PaymentScheduleResultTO;
import com.pradera.securities.issuancesecuritie.to.PaymentScheduleTO;
import com.pradera.securities.issuancesecuritie.util.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PaymentCronogramServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class PaymentCronogramServiceBean extends CrudDaoServiceBean {
	
	/** The corporate event component service bean. */
	@EJB
	private CorporateEventComponentServiceBean corporateEventComponentServiceBean;
	
	/** The security service bean. */
	@EJB
	private SecurityServiceBean securityServiceBean;
	
	@Inject
	private SecuritiesServiceBean securitiesServiceBean;
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	
	/**
	 * Metodo para buscar un InterestPaymentSchedule por codigo de valor y estado.
	 *
	 * @param idSecurityCode the id security code
	 * @return the interest payment schedule
	 * @throws ServiceException the service exception
	 */
	public InterestPaymentSchedule findActiveInterestPaymentScheduleBySecurity(String idSecurityCode) throws ServiceException{

		try {
			Map<String,Object> params=new HashMap<String, Object>();
			params.put("idSecurityPrm", idSecurityCode);
			params.put("statePrm", PaymentScheduleStateType.REGTERED.getCode());
			InterestPaymentSchedule interestPaymentSchedule=findObjectByNamedQuery(InterestPaymentSchedule.FIND_BY_SECURITY_AND_STATE, params);
			return interestPaymentSchedule;
		} catch(NonUniqueResultException nuex){
			log.info("Interest Payment Schedule have more codes same!");
			return null;
		} catch(NoResultException nex) {
			log.info("Interest Payment Schedule not found");
			return null;
		}
	}
	
	/**
	 * Metodo para buscar un AmortizationPaymentSchedule por codigo de valor y estado.
	 *
	 * @param idSecurityCode the id security code
	 * @return the amortization payment schedule
	 * @throws ServiceException the service exception
	 */
	public AmortizationPaymentSchedule findActiveAmortizationPaymentSchedule(String idSecurityCode) throws ServiceException{
		try {
			Map<String,Object> params=new HashMap<String, Object>();
			params.put("idSecurityPrm", idSecurityCode);
			params.put("statePrm", PaymentScheduleStateType.REGTERED.getCode());
			AmortizationPaymentSchedule amortizationPaymentSchedule=findObjectByNamedQuery(AmortizationPaymentSchedule.FIND_BY_SECURITY_AND_STATE, params);
			return amortizationPaymentSchedule;
		} catch(NonUniqueResultException nuex){
			log.info("Amortization Payment Schedule have more codes same!");
			return null;
		} catch(NoResultException nex) {
			log.info("Amortization Payment Schedule not found");
			return null;
		}	
	}
	
	/**
	 * Metodo para buscar un ProgramInterestCoupon por id de cronograma.
	 *
	 * @param idIntPayCronPkPrm the id int pay cron pk prm
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ProgramInterestCoupon> findProgramInterestCouponsServiceBean(Long idIntPayCronPkPrm) throws ServiceException{
		CriteriaBuilder cb=em.getCriteriaBuilder();
		CriteriaQuery<ProgramInterestCoupon> cq=cb.createQuery(ProgramInterestCoupon.class);
		Root<ProgramInterestCoupon> progIntCouponRoot=cq.from(ProgramInterestCoupon.class);
		List<Predicate> criteriaParameters=new ArrayList<Predicate>();
		
		Join<ProgramInterestCoupon, CorporativeOperation> corporativeOperationJoin = (Join)progIntCouponRoot.fetch("corporativeOperation",JoinType.LEFT);
		
		cq.orderBy(cb.asc(progIntCouponRoot.get("couponNumber")) );
		cq.select(progIntCouponRoot);
		criteriaParameters.add( cb.equal(progIntCouponRoot.get("interestPaymentSchedule").<Long>get("idIntPaymentSchedulePk")  , idIntPayCronPkPrm) );
		
		if(criteriaParameters.size()==0){
			throw new RuntimeException("no criteria");
		}else if(criteriaParameters.size() == 1){
			cq.where( criteriaParameters.get(0) );
		}else {
			cq.where( cb.and( criteriaParameters.toArray(new Predicate[criteriaParameters.size()])  ) );
		}
		
		TypedQuery<ProgramInterestCoupon> proIntCoupon = em.createQuery(cq);
		return proIntCoupon.getResultList();
	}
	
	/**
	 * Find program amortization coupons service bean.
	 *
	 * @param idAmortCronPkPrm the id amort cron pk prm
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ProgramAmortizationCoupon> findProgramAmortizationCouponsServiceBean(Long idAmortCronPkPrm) throws ServiceException{
		CriteriaBuilder cb=em.getCriteriaBuilder();
		CriteriaQuery<ProgramAmortizationCoupon> cq=cb.createQuery(ProgramAmortizationCoupon.class);
		Root<ProgramAmortizationCoupon> progAmoCouponRoot=cq.from(ProgramAmortizationCoupon.class);
		List<Predicate> criteriaParameters=new ArrayList<Predicate>();
		
		Join<ProgramAmortizationCoupon, CorporativeOperation> corporativeOperationJoin = (Join)progAmoCouponRoot.fetch("corporativeOperation",JoinType.LEFT);
		
		cq.orderBy(cb.asc(progAmoCouponRoot.get("couponNumber")) );
		
		cq.select(progAmoCouponRoot);
		
		criteriaParameters.add( cb.equal( 
				progAmoCouponRoot.get("amortizationPaymentSchedule").<Long>get("idAmoPaymentSchedulePk")  , idAmortCronPkPrm) );
		
	//	criteriaParameters.add(cb.notEqual(corporativeOperationJoin.get("state"),CorporateProcessStateType.ANNULLED.getCode()   ));
		
		if(criteriaParameters.size()==0){
			throw new RuntimeException("no criteria");
		}else if(criteriaParameters.size() == 1){
			cq.where( criteriaParameters.get(0) );
		}else {
			cq.where( cb.and( criteriaParameters.toArray(new Predicate[criteriaParameters.size()])  ) );
		}
		
		TypedQuery<ProgramAmortizationCoupon> progAmortCoupon = em.createQuery(cq);
		
		return progAmortCoupon.getResultList();
	}
	
	/**
	 * Find program amortization and interest coupons by date.
	 *
	 * @param securityTO the security to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<CouponInformationTO> findProgramAmortizationAndInterestCouponsByDate(SecurityTO securityTO) throws ServiceException{

		StringBuilder sbQuery = new StringBuilder("Select ");
		sbQuery.append(" SEC.idSecurityCodePk, "); //0
		sbQuery.append(" SEC.description, ");  //1
		sbQuery.append(" SEC.instrumentType, "); //2
		sbQuery.append(" SEC.expirationDate, "); //3
		sbQuery.append(" ISR.idIssuerPk, "); //4
		sbQuery.append(" ISR.mnemonic, "); //5
		sbQuery.append(" PIC.couponNumber as couponNumberInterest, "); //6
		sbQuery.append(" PAC.couponNumber as couponNumberAmortization, "); //7
		sbQuery.append(" COP.deliveryDate, "); //8
		sbQuery.append(" PAC.amortizationAmount, "); //9
		sbQuery.append(" COP.deliveryFactor, "); //10
		sbQuery.append(" COP.paymentAmount, "); //11
		sbQuery.append(" COP.situation, "); //12
		sbQuery.append(" COP.indPayed, "); //13
		sbQuery.append(" PIC.experitationDate, "); //14 
		sbQuery.append(" PAC.expirationDate, "); //15
		sbQuery.append(" SEC.stateSecurity "); //16

		sbQuery.append(" From CorporativeOperation COP  ");
		sbQuery.append(" Left Join COP.programInterestCoupon PIC ");
		sbQuery.append(" Left Join COP.programAmortizationCoupon PAC ");		
		sbQuery.append(" Inner Join COP.securities SEC ");
		sbQuery.append(" Inner Join SEC.issuer ISR ");
		sbQuery.append(" Inner Join COP.corporativeEventType CET ");
		sbQuery.append(" Where  1=1 ");
		if(Validations.validateIsNotNull(securityTO.getSelectDate())){
			sbQuery.append("and  Trunc(COP.deliveryDate) = :selectedDatePrm ");
		}
		if(Validations.validateIsNotNull(securityTO.getIssuanceDate())){
			sbQuery.append("and  (trunc(COP.deliveryDate) between trunc(:initialDatePrm) and trunc(:finalDatePrm)) ");
			                     
		}
		sbQuery.append(" and CET.corporativeEventType in (:lstCorporativeEventTypesPrm)");
		sbQuery.append(" and CET.instrumentType=:instrumentTypePrm");
		
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdIssuerCodePk())){
			sbQuery.append(" and  ISR.idIssuerPk=:idIssuerPkPrm ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdSecurityCodePk())){
			sbQuery.append(" and ").append(" SEC.idSecurityCodePk=:idSecurityCodePkPrm ");
		}
		if(Validations.validateIsNotNullAndPositive(securityTO.getSecurityClass())){
			sbQuery.append(" and ").append(" SEC.securityClass=:securityClassPrm ");
		}
		if(Validations.validateIsNotNullAndPositive(securityTO.getIndicator1())){
			if(securityTO.getIndicator1().compareTo(GeneralConstants.ONE_VALUE_INTEGER)==0){
				sbQuery.append(" and PIC.couponNumber is not null ");
			}else if(securityTO.getIndicator1().compareTo(GeneralConstants.TWO_VALUE_INTEGER)==0){
				sbQuery.append(" and PAC.couponNumber is not null ");
			}
		}
		sbQuery.append(" Order By SEC.idSecurityCodePk, PIC.couponNumber,  PAC.couponNumber ");

		Query query = em.createQuery(sbQuery.toString());
		
		List<Integer> lstCorporativeEventTypes = new ArrayList<Integer>();
		lstCorporativeEventTypes.add(Integer.valueOf(1276));
		lstCorporativeEventTypes.add(Integer.valueOf(1277));
		lstCorporativeEventTypes.add(Integer.valueOf(1264));
		if(Validations.validateIsNotNull(securityTO.getSelectDate())){
			query.setParameter("selectedDatePrm", securityTO.getSelectDate(), TemporalType.DATE);
		}
		query.setParameter("lstCorporativeEventTypesPrm", lstCorporativeEventTypes);
		query.setParameter("instrumentTypePrm", InstrumentType.FIXED_INCOME.getCode());
		
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdIssuerCodePk())){
			query.setParameter("idIssuerPkPrm", securityTO.getIdIssuerCodePk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdSecurityCodePk())){
			query.setParameter("idSecurityCodePkPrm", securityTO.getIdSecurityCodePk());
		}
		if(Validations.validateIsNotNullAndPositive(securityTO.getSecurityClass())){
			query.setParameter("securityClassPrm", securityTO.getSecurityClass());
		}
		if(Validations.validateIsNotNull(securityTO.getIssuanceDate())){
			query.setParameter("initialDatePrm", securityTO.getIssuanceDate(), TemporalType.DATE);
			query.setParameter("finalDatePrm", securityTO.getExpirationDate(), TemporalType.DATE);
		}
		
		List<Object[]> lstResult = (List<Object[]>)query.getResultList();
		
		List<CouponInformationTO> lstCouponInformationTO = new ArrayList<CouponInformationTO>();
		CouponInformationTO objCouponInformationTO = null;

		for (Object[] arrObjResult : lstResult) {
			objCouponInformationTO = new CouponInformationTO();
			objCouponInformationTO.setIdSecurityCodePk(arrObjResult[0].toString());
			objCouponInformationTO.setSecurityDescription(arrObjResult[1]!=null?arrObjResult[1].toString():"");
			objCouponInformationTO.setSecurityInstrumentType(Integer.valueOf(arrObjResult[2].toString()));
			objCouponInformationTO.setSecurityExpirationDate((Date)arrObjResult[3]);
			objCouponInformationTO.setIdIssuerFk(arrObjResult[4].toString());
			objCouponInformationTO.setIssuerMnemonic(arrObjResult[5].toString());
			
			if(Validations.validateIsNotNull(arrObjResult[6])){
				objCouponInformationTO.setCouponNumber(Integer.valueOf(arrObjResult[6].toString()));
				objCouponInformationTO.setPaymentTypeDescription("INTERES");
				objCouponInformationTO.setIndInterest(true);
			} else if(Validations.validateIsNotNull(arrObjResult[7])){
				objCouponInformationTO.setCouponNumber(Integer.valueOf(arrObjResult[7].toString()));
				objCouponInformationTO.setPaymentTypeDescription("AMORTIZACION");
				objCouponInformationTO.setIndInterest(false);
			}
			
			objCouponInformationTO.setPaymentDate((Date)arrObjResult[8]);
			
			if(Validations.validateIsNotNull(arrObjResult[9])){
				objCouponInformationTO.setAmortizationAmount((BigDecimal)arrObjResult[9]);
			}
			
			if(Validations.validateIsNotNull(arrObjResult[10])){
				objCouponInformationTO.setInterestRate((BigDecimal)arrObjResult[10]);
			}
			
			if(Validations.validateIsNotNull(arrObjResult[11])){
				objCouponInformationTO.setPaymentAmount((BigDecimal)arrObjResult[11]);
			}
						
			if(Validations.validateIsNotNull(arrObjResult[12])){
				objCouponInformationTO.setSituation(Integer.valueOf(arrObjResult[12].toString()));
			}
			
			if(Validations.validateIsNotNull(arrObjResult[13])){
				objCouponInformationTO.setIndPayed(Integer.valueOf(arrObjResult[13].toString()));
			}
			
			if(arrObjResult[14]!=null){
				objCouponInformationTO.setInteresExpirationDate((Date) arrObjResult[14]);
			}
			
			if(arrObjResult[15]!=null){
				objCouponInformationTO.setAmortizationExpirationDate((Date) arrObjResult[15]);
			}
			
			if(arrObjResult[16] != null) {
				objCouponInformationTO.setStateSecurity((Integer) arrObjResult[16]);
			}
			
			lstCouponInformationTO.add(objCouponInformationTO);
		}
		
		return lstCouponInformationTO;
	}
	
	/**
	 * Find dates program amortization and interest coupons.
	 *
	 * @param initialDate the initial date
	 * @param endDate the end date
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Date> findDatesProgramAmortizationAndInterestCoupons(Date initialDate, Date endDate) throws ServiceException{

		StringBuilder sbQuery = new StringBuilder("Select ");
		sbQuery.append(" distinct COP.deliveryDate "); //8

		sbQuery.append(" From CorporativeOperation COP  ");
		sbQuery.append(" Left Join COP.programInterestCoupon PIC ");
		sbQuery.append(" Left Join COP.programAmortizationCoupon PAC ");	
		sbQuery.append(" Inner Join COP.corporativeEventType CET ");
		sbQuery.append(" Inner Join COP.securities SEC ");
		sbQuery.append(" Inner Join SEC.issuer ISR ");
		sbQuery.append(" Where Trunc(COP.deliveryDate) between :initialDatePrm and :endDatePrm and CET.corporativeEventType in (:lstCorporativeEventTypesPrm) ");		
		sbQuery.append(" and CET.instrumentType=:instrumentTypePrm");
		
		TypedQuery<Date> query = em.createQuery(sbQuery.toString(), Date.class);
		
		List<Integer> lstCorporativeEventTypes = new ArrayList<Integer>();
		lstCorporativeEventTypes.add(ImportanceEventType.INTEREST_PAYMENT.getCode());
		lstCorporativeEventTypes.add(ImportanceEventType.CAPITAL_AMORTIZATION.getCode());
		lstCorporativeEventTypes.add(ImportanceEventType.CASH_DIVIDENDS.getCode());

		query.setParameter("instrumentTypePrm", InstrumentType.FIXED_INCOME.getCode());
		query.setParameter("initialDatePrm", initialDate, TemporalType.DATE);
		query.setParameter("endDatePrm", endDate, TemporalType.DATE);
		query.setParameter("lstCorporativeEventTypesPrm", lstCorporativeEventTypes);
		
		
		return (List<Date>)query.getResultList();
	}
	
	/**
	 * Metodo para Buscar una lista de cronogramas de pago de interes
	 *
	 * @param paymentScheduleTO the payment schedule to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<PaymentScheduleResultTO> finIntPaymentScheduleReqHisLiteServiceBean(PaymentScheduleTO paymentScheduleTO) throws ServiceException{
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select ");
		sbQuery.append(" 	IPSRH.idIntScheduleReqHisPk, "); //0 
		sbQuery.append(" 	SEC.idSecurityCodePk, ");  //1
		sbQuery.append(" 	SEC.idIsinCode, "); //2
		sbQuery.append(" 	SEC.description, "); //3
		sbQuery.append(" 	SEC.issuance.idIssuanceCodePk, "); //4
		sbQuery.append(" 	SEC.issuanceDate, "); //5
		sbQuery.append(" 	max(PICRH.expirationDate), "); //6
		sbQuery.append(" 	count(PICRH.idProgIntCouponReqHisPk), "); //7
		sbQuery.append(" 	IPSRH.requestState, "); //8
		sbQuery.append(" 	IPSRH.registryDate, "); //9
		sbQuery.append(" 	SEC.securityClass, "); //10
		sbQuery.append(" 	SEC.currency, "); //11
		sbQuery.append(" 	SEC.currentNominalValue "); //12

		sbQuery.append(" From IntPaymentScheduleReqHi IPSRH  ");
		sbQuery.append(" 	Inner Join IPSRH.security SEC ");
		sbQuery.append(" 	Left Outer Join IPSRH.programIntCouponReqHis PICRH ");
		
		sbQuery.append(" Where  1=1 ");
		sbQuery.append(" 	and  (trunc(IPSRH.registryDate) between trunc(:initialDatePrm) and trunc(:finalDatePrm)) ");
		if(Validations.validateIsNotNullAndNotEmpty(paymentScheduleTO.getIdPaymentScheduleReqHiPk())){
			sbQuery.append(" 	and IPSRH.idIntScheduleReqHisPk =:idIntScheduleReqHisPkPrm ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(paymentScheduleTO.getIdIssuerPk())){
			sbQuery.append(" 	and SEC.issuer.idIssuerPk =:idIssuerPkPrm ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(paymentScheduleTO.getIssuance().getIdIssuanceCodePk())){
			sbQuery.append(" 	and SEC.issuance.idIssuanceCodePk =:idIssuanceCodePkPrm ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(paymentScheduleTO.getIdSecurityCodePk())){
			sbQuery.append(" 	and SEC.idSecurityCodePk =:idSecurityCodePkPrm ");
		}
		if(Validations.validateIsNotNullAndPositive((paymentScheduleTO.getSecurityClass()))){
			sbQuery.append(" 	and SEC.securityClass =:securityClassPrm ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(paymentScheduleTO.getIdIsinCode())){
			sbQuery.append(" 	and SEC.idIsinCode =:idIsinCodePrm ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(paymentScheduleTO.getSecurityDescription())){
			sbQuery.append(" 	and SEC.description =:descriptionPrm ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(paymentScheduleTO.getRequestState())){
			sbQuery.append(" 	and IPSRH.requestState =:requestStatePrm ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(paymentScheduleTO.getRegistryType())){
			sbQuery.append(" 	and IPSRH.registryType =:registryTypePrm ");
		}
		
		sbQuery.append(" Group By ");
		sbQuery.append(" 	IPSRH.idIntScheduleReqHisPk, ");  
		sbQuery.append(" 	SEC.idSecurityCodePk, ");  
		sbQuery.append(" 	SEC.idIsinCode, "); 
		sbQuery.append(" 	SEC.description, "); 
		sbQuery.append(" 	SEC.issuance.idIssuanceCodePk, "); 
		sbQuery.append(" 	SEC.issuanceDate, "); 
		sbQuery.append(" 	IPSRH.requestState, "); 
		sbQuery.append(" 	IPSRH.registryDate, ");
		sbQuery.append(" 	SEC.securityClass, "); 
		sbQuery.append(" 	SEC.currency, "); 
		sbQuery.append(" 	SEC.currentNominalValue "); 

		Query query = em.createQuery(sbQuery.toString());
		
		query.setParameter("initialDatePrm", paymentScheduleTO.getBeginDate(), TemporalType.DATE);
		query.setParameter("finalDatePrm", paymentScheduleTO.getEndDate(), TemporalType.DATE);
		if(Validations.validateIsNotNullAndNotEmpty(paymentScheduleTO.getIdPaymentScheduleReqHiPk())){
			query.setParameter("idIntScheduleReqHisPkPrm", paymentScheduleTO.getIdPaymentScheduleReqHiPk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(paymentScheduleTO.getIdIssuerPk())){
			query.setParameter("idIssuerPkPrm", paymentScheduleTO.getIdIssuerPk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(paymentScheduleTO.getIssuance().getIdIssuanceCodePk())){
			query.setParameter("idIssuanceCodePkPrm", paymentScheduleTO.getIssuance().getIdIssuanceCodePk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(paymentScheduleTO.getIdSecurityCodePk())){
			query.setParameter("idSecurityCodePkPrm", paymentScheduleTO.getIdSecurityCodePk());
		}
		if(Validations.validateIsNotNullAndPositive((paymentScheduleTO.getSecurityClass()))){
			query.setParameter("securityClassPrm", paymentScheduleTO.getSecurityClass());
		}
		if(Validations.validateIsNotNullAndNotEmpty(paymentScheduleTO.getIdIsinCode())){
			query.setParameter("idIsinCodePrm", paymentScheduleTO.getIdIsinCode());
		}
		if(Validations.validateIsNotNullAndNotEmpty(paymentScheduleTO.getSecurityDescription())){
			query.setParameter("descriptionPrm", paymentScheduleTO.getSecurityDescription());
		}
		if(Validations.validateIsNotNullAndNotEmpty(paymentScheduleTO.getRequestState())){
			query.setParameter("requestStatePrm", paymentScheduleTO.getRequestState());
		}
		if(Validations.validateIsNotNullAndNotEmpty(paymentScheduleTO.getRegistryType())){
			query.setParameter("registryTypePrm", paymentScheduleTO.getRegistryType());
		}
		
		List<Object[]> lstResult = (List<Object[]>)query.getResultList();
		List<PaymentScheduleResultTO> lstPaymentScheduleResultTO = new ArrayList<PaymentScheduleResultTO>();
		PaymentScheduleResultTO objPaymentScheduleResultTO = null;

		for (Object[] arrObjResult : lstResult) {
			objPaymentScheduleResultTO = new PaymentScheduleResultTO();
			if(Validations.validateIsNotNullAndNotEmpty(arrObjResult[0]))
				objPaymentScheduleResultTO.setIdPaymentScheduleReqHi(new Long(arrObjResult[0].toString()));
			if(Validations.validateIsNotNullAndNotEmpty(arrObjResult[1]))
				objPaymentScheduleResultTO.setIdSecurityCodePk(arrObjResult[1].toString());
			if(Validations.validateIsNotNullAndNotEmpty(arrObjResult[2]))
				objPaymentScheduleResultTO.setIdIsinCodePk(arrObjResult[2].toString());
			if(Validations.validateIsNotNullAndNotEmpty(arrObjResult[3]))
				objPaymentScheduleResultTO.setSecurityDescription(arrObjResult[3].toString());
			if(Validations.validateIsNotNullAndNotEmpty(arrObjResult[4]))
				objPaymentScheduleResultTO.setIdIssuanceCodePk(arrObjResult[4].toString());
			if(Validations.validateIsNotNullAndNotEmpty(arrObjResult[5]))
				objPaymentScheduleResultTO.setIssuanceDate((Date)(arrObjResult[5]));
			if(Validations.validateIsNotNullAndNotEmpty(arrObjResult[6]))
				objPaymentScheduleResultTO.setExpirationDate((Date)(arrObjResult[6]));
			if(Validations.validateIsNotNullAndNotEmpty(arrObjResult[7]))
				objPaymentScheduleResultTO.setCouponsCount(new Long(arrObjResult[7].toString()));
			if(Validations.validateIsNotNullAndNotEmpty(arrObjResult[8]))
				objPaymentScheduleResultTO.setRequestState(new Integer(arrObjResult[8].toString()));
			if(Validations.validateIsNotNullAndNotEmpty(arrObjResult[9]))
				objPaymentScheduleResultTO.setRegistryDate((Date)(arrObjResult[9]));
			if(Validations.validateIsNotNullAndNotEmpty(arrObjResult[10]))
				objPaymentScheduleResultTO.setSecurityClass(new Integer(arrObjResult[10].toString()));
			if(Validations.validateIsNotNullAndNotEmpty(arrObjResult[11]))
				objPaymentScheduleResultTO.setCurrency(new Integer(arrObjResult[11].toString()));
			if(Validations.validateIsNotNullAndNotEmpty(arrObjResult[12]))
				objPaymentScheduleResultTO.setCurrentNominalValue(new BigDecimal(arrObjResult[12].toString()));
			lstPaymentScheduleResultTO.add(objPaymentScheduleResultTO);
		}
		
		return lstPaymentScheduleResultTO;
	}
	
	/**
	 * Metodo para Buscar una lista de cronogramas de pago de Amortizacion
	 *
	 * @param paymentScheduleTO the payment schedule to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<PaymentScheduleResultTO> finAmoPaymentScheduleReqHisLiteServiceBean(PaymentScheduleTO paymentScheduleTO) throws ServiceException{
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		  List<Predicate> criteriaParameters = new ArrayList<Predicate>();
		  CriteriaQuery<PaymentScheduleResultTO> criteriaQuery = criteriaBuilder.createQuery(PaymentScheduleResultTO.class);
		  Root<AmoPaymentScheduleReqHi> amoPayScheReqHiRoot = criteriaQuery.from(AmoPaymentScheduleReqHi.class);
		  Join<AmoPaymentScheduleReqHi, ProgramIntCouponReqHi> program = amoPayScheReqHiRoot.join("programAmoCouponReqHis", JoinType.LEFT);
		  
		  criteriaQuery.multiselect(
				  amoPayScheReqHiRoot.get("idAmoScheduleReqHisPk"),
				  amoPayScheReqHiRoot.get("security").get("idSecurityCodePk"),
				  amoPayScheReqHiRoot.get("security").get("idIsinCode"),
				  amoPayScheReqHiRoot.get("security").get("description"),
				  amoPayScheReqHiRoot.get("security").get("issuance").get("idIssuanceCodePk"),
				  amoPayScheReqHiRoot.get("security").get("issuanceDate"),
				  criteriaBuilder.greatest(program.<Date>get("expirationDate")).alias("expirationDate"),
				  criteriaBuilder.count(program).alias("couponsCount"),
				  amoPayScheReqHiRoot.<Integer>get("requestState"),
				  amoPayScheReqHiRoot.get("registryDate"),
				  
				  amoPayScheReqHiRoot.get("security").get("securityClass"),
				  amoPayScheReqHiRoot.get("security").get("currency"),
				  amoPayScheReqHiRoot.get("security").get("currentNominalValue")
		      );
		  criteriaQuery.groupBy( 
				  amoPayScheReqHiRoot.get("idAmoScheduleReqHisPk"), 
				  amoPayScheReqHiRoot.get("security").get("idSecurityCodePk"), 
				  amoPayScheReqHiRoot.get("security").get("idIsinCode"), 
				  amoPayScheReqHiRoot.get("security").get("description"),
				  amoPayScheReqHiRoot.get("security").get("issuance").get("idIssuanceCodePk"),
				  amoPayScheReqHiRoot.get("security").get("issuanceDate"),
				  amoPayScheReqHiRoot.get("requestState"),
				  amoPayScheReqHiRoot.get("registryDate"),
				  
				  amoPayScheReqHiRoot.get("security").get("securityClass"),
				  amoPayScheReqHiRoot.get("security").get("currency"),
				  amoPayScheReqHiRoot.get("security").get("currentNominalValue")
				  );
		  
		  
		  criteriaParameters.add(criteriaBuilder.between(criteriaBuilder.function("trunc", Date.class, amoPayScheReqHiRoot.get("registryDate")).as(Date.class), paymentScheduleTO.getBeginDate() , paymentScheduleTO.getEndDate()));
		
		  criteriaParameters.add(criteriaBuilder.equal(amoPayScheReqHiRoot.get("registryType"), paymentScheduleTO.getRegistryType()));
			if(StringUtils.isNotBlank(paymentScheduleTO.getIdIssuerPk())){
				criteriaParameters.add(criteriaBuilder.equal(amoPayScheReqHiRoot.get("security").get("issuer").get("idIssuerPk"),paymentScheduleTO.getIdIssuerPk()));
			}
			if(StringUtils.isNotBlank( paymentScheduleTO.getIssuance().getIdIssuanceCodePk() )){
				criteriaParameters.add(criteriaBuilder.equal(amoPayScheReqHiRoot.get("security").get("issuance").get("idIssuanceCodePk"),
						paymentScheduleTO.getIssuance().getIdIssuanceCodePk()));
			}
			if(StringUtils.isNotBlank( paymentScheduleTO.getIdIsinCode() )){
				criteriaParameters.add(criteriaBuilder.equal(amoPayScheReqHiRoot.get("security").get("idSecurityCodePk"),
						paymentScheduleTO.getIdSecurityCodePk() ));
			}
			
			if(Validations.validateIsNotNullAndPositive( paymentScheduleTO.getSecurityClass() )){
				criteriaParameters.add(criteriaBuilder.equal(amoPayScheReqHiRoot.get("security").get("securityClass"),
						paymentScheduleTO.getSecurityClass() ));
			}
			
			if(StringUtils.isNotBlank( paymentScheduleTO.getIdIsinCode() )){
				criteriaParameters.add(criteriaBuilder.equal(amoPayScheReqHiRoot.get("security").get("idIsinCode"),
						paymentScheduleTO.getIdIsinCode()));
			}
			if(StringUtils.isNotBlank( paymentScheduleTO.getSecurityDescription() )){
				criteriaParameters.add(criteriaBuilder.equal(amoPayScheReqHiRoot.get("security").get("description"),
						paymentScheduleTO.getSecurityDescription()));
			}
			if(Validations.validateIsNotNull( paymentScheduleTO.getRequestState())){
				criteriaParameters.add(criteriaBuilder.equal(amoPayScheReqHiRoot.get("requestState"),
						paymentScheduleTO.getRequestState()));
			}
			if(Validations.validateIsNotNull( paymentScheduleTO.getRegistryType())){
				criteriaParameters.add(criteriaBuilder.equal(amoPayScheReqHiRoot.get("registryType"),
						paymentScheduleTO.getRegistryType()));
			}
 
			criteriaQuery.where(criteriaBuilder.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()]))); 
			TypedQuery<PaymentScheduleResultTO> operationsCriteria = em.createQuery(criteriaQuery);
			
			List<PaymentScheduleResultTO> result = null;
		  try{
			  result = operationsCriteria.getResultList();
			  }catch(NoResultException ex){
			   return null;
		   }
		  return result;
	}
	
	/**
	 * Validate int payment schedule req hi service bean.
	 *
	 * @param idIntScheduleReqHisPk the id int schedule req his pk
	 * @param idSecurityCodePk the id security code pk
	 * @param registryType the registry type
	 * @param requestStateArg the request state arg
	 * @return the int payment schedule req hi
	 * @throws ServiceException the service exception
	 */
	public IntPaymentScheduleReqHi validateIntPaymentScheduleReqHiServiceBean(Long idIntScheduleReqHisPk, String idSecurityCodePk, Integer registryType, Object[] requestStateArg) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append("Select i from IntPaymentScheduleReqHi i where i.security.idSecurityCodePk=:idSecurityCodePkPrm");
		if(registryType != null){
			sbQuery.append(" and i.registryType = :registryTypePrm");
		}
		if(requestStateArg!=null){
			sbQuery.append(" and i.requestState in (");
			for(int i=0;i<requestStateArg.length;i++){
				sbQuery.append(":statePrm").append(i);
				if(i<requestStateArg.length-1){
					sbQuery.append(" , ");
				}
			}
			sbQuery.append(") ");
		}
		
		if(idIntScheduleReqHisPk!=null){
			sbQuery.append(" and i.idIntScheduleReqHisPk != :idIntScheduleReqHisPk");
		}
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSecurityCodePkPrm", idSecurityCodePk);
		if(registryType != null){
			query.setParameter("registryTypePrm", registryType);
		}
		if(requestStateArg!=null){
			for(int i=0;i<requestStateArg.length;i++){
				query.setParameter("statePrm"+i, requestStateArg[i]);
			}
		}
		if(idIntScheduleReqHisPk!=null){
			query.setParameter("idIntScheduleReqHisPk", idIntScheduleReqHisPk);
		}
		IntPaymentScheduleReqHi intPaymenScheReqHi=null;
		try{
			intPaymenScheReqHi=(IntPaymentScheduleReqHi)query.getSingleResult();
		} catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}
		return intPaymenScheReqHi;
	}
	
	/**
	 * Validate amo payment schedule req hi service bean.
	 *
	 * @param idAmoScheduleReqHisPk the id amo schedule req his pk
	 * @param idSecurityCodePk the id security code pk
	 * @param registryType the registry type
	 * @param requestStateArg the request state arg
	 * @return the amo payment schedule req hi
	 * @throws ServiceException the service exception
	 */
	public AmoPaymentScheduleReqHi validateAmoPaymentScheduleReqHiServiceBean(Long idAmoScheduleReqHisPk, String idSecurityCodePk,Integer registryType, Object[] requestStateArg) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append("Select i from AmoPaymentScheduleReqHi i where i.security.idSecurityCodePk=:idSecurityCodePkPrm");
		if(registryType!=null){
			sbQuery.append(" and i.registryType = :registryTypePrm");
		}
		if(requestStateArg!=null){
			sbQuery.append(" and i.requestState in (");
			for(int i=0;i<requestStateArg.length;i++){
				sbQuery.append(":statePrm").append(i);
				if(i<requestStateArg.length-1){
					sbQuery.append(" , ");
				}
			}
			sbQuery.append(") ");
		}
		
		if(idAmoScheduleReqHisPk!=null){
			sbQuery.append(" and i.idAmoScheduleReqHisPk != :idAmoScheduleReqHisPk");
		}
        
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSecurityCodePkPrm", idSecurityCodePk);
		if(registryType!=null){
			query.setParameter("registryTypePrm", registryType);
		}
		if(requestStateArg!=null){
			for(int i=0;i<requestStateArg.length;i++){
				query.setParameter("statePrm"+i, requestStateArg[i]);
			}
		}
		
		if(idAmoScheduleReqHisPk!=null){
			query.setParameter("idAmoScheduleReqHisPk", idAmoScheduleReqHisPk);
		}
		
		AmoPaymentScheduleReqHi amoPaymenScheReqHi=null;
		//em.flush(); //revertido salida a produccion issues: 100,148,143
		try{
			amoPaymenScheReqHi=(AmoPaymentScheduleReqHi)query.getSingleResult();
		} catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}
		
		return amoPaymenScheReqHi;
	}
	
	/**
	 * Metodo para Buscar una solicitud de cronograma de interes
	 *
	 * @param paymentScheduleTO the payment schedule to
	 * @return the int payment schedule req hi
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public IntPaymentScheduleReqHi finIntPaymentScheduleReqHisServiceBean(PaymentScheduleTO paymentScheduleTO ){
		  CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		  List<Predicate> criteriaParameters = new ArrayList<Predicate>();
		  CriteriaQuery<IntPaymentScheduleReqHi> criteriaQuery = criteriaBuilder.createQuery(IntPaymentScheduleReqHi.class);
		  Root<IntPaymentScheduleReqHi> intPayScheReqHiRoot = criteriaQuery.from(IntPaymentScheduleReqHi.class);
		  Join<IntPaymentScheduleReqHi, ProgramAmoCouponReqHi> program = (Join)intPayScheReqHiRoot.fetch("programIntCouponReqHis");
		  criteriaQuery.select(intPayScheReqHiRoot);
		  
		  criteriaQuery.orderBy(criteriaBuilder.asc(program.get("couponNumber")) );
		  
			if(Validations.validateIsNotNullAndNotEmpty( paymentScheduleTO.getIdPaymentScheduleReqHiPk() )){
				criteriaParameters.add(criteriaBuilder.equal(intPayScheReqHiRoot.<String>get("idIntScheduleReqHisPk"), paymentScheduleTO.getIdPaymentScheduleReqHiPk() ));
			}
			
			if(!criteriaParameters.isEmpty()){
				if (criteriaParameters.size() == 1) {
					criteriaQuery.where(criteriaParameters.get(0));
			    } else {
			    	criteriaQuery.where(criteriaBuilder.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()])));
			    }
			}
		  
			TypedQuery<IntPaymentScheduleReqHi> intPaymentScheduleReqHi=em.createQuery(criteriaQuery);
			return  intPaymentScheduleReqHi.getSingleResult();
	}
	
	/**
	 * Metodo para Buscar una solicitud de cronograma de Amortizacion
	 *
	 * @param paymentScheduleTO the payment schedule to
	 * @return the amo payment schedule req hi
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public AmoPaymentScheduleReqHi finAmoPaymentScheduleReqHisServiceBean(PaymentScheduleTO paymentScheduleTO ){
		  CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		  List<Predicate> criteriaParameters = new ArrayList<Predicate>();
		  CriteriaQuery<AmoPaymentScheduleReqHi> criteriaQuery = criteriaBuilder.createQuery(AmoPaymentScheduleReqHi.class);
		  Root<AmoPaymentScheduleReqHi> amoPayScheReqHiRoot = criteriaQuery.from(AmoPaymentScheduleReqHi.class);
		  Join<IntPaymentScheduleReqHi, ProgramAmoCouponReqHi> program = (Join)amoPayScheReqHiRoot .fetch("programAmoCouponReqHis");
		  
		  criteriaQuery.select(amoPayScheReqHiRoot);
		  criteriaQuery.orderBy(criteriaBuilder.asc(program.get("couponNumber")) );
		  
			if(Validations.validateIsNotNullAndNotEmpty( paymentScheduleTO.getIdPaymentScheduleReqHiPk() )){
				criteriaParameters.add(criteriaBuilder.equal(amoPayScheReqHiRoot.<String>get("idAmoScheduleReqHisPk"), paymentScheduleTO.getIdPaymentScheduleReqHiPk() ));
			}
			
			if(!criteriaParameters.isEmpty()){
				if (criteriaParameters.size() == 1) {
					criteriaQuery.where(criteriaParameters.get(0));
			    } else {
			    	criteriaQuery.where(criteriaBuilder.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()])));
			    }
			}
		  
			TypedQuery<AmoPaymentScheduleReqHi> amoPaymentScheduleReqHi=em.createQuery(criteriaQuery);
			return  amoPaymentScheduleReqHi.getSingleResult();
	}
	
	/**
	 * Metodo para actualizar el estado de IntPaymentScheduleReqHi.
	 *
	 * @param intPaymentScheduleReqHi the int payment schedule req hi
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateIntPayScheReqHiReqStateServiceBean(IntPaymentScheduleReqHi intPaymentScheduleReqHi,LoggerUser loggerUser) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update IntPaymentScheduleReqHi i ");
		sbQuery.append(" Set i.requestState=:statePrm, ");
		sbQuery.append(" i.rejectMotive=:rejectMotivePrm, ");
		sbQuery.append(" i.rejectOtherMotive=:rejectOtherMotivePrm, ");
		sbQuery.append(" i.lastModifyDate=:lastModifyDatePrm, ");
		sbQuery.append(" i.lastModifyUser=:lastModifyUserPrm, ");
		sbQuery.append(" i.lastModifyIp=:lastModifyIpPrm, ");
		sbQuery.append(" i.lastModifyApp=:lastModifyAppPrm ");
		sbQuery.append(" where i.idIntScheduleReqHisPk=:idIntSchePrm");
		
		Query query = em.createQuery(sbQuery.toString());
		
		query.setParameter("statePrm",intPaymentScheduleReqHi.getRequestState());
		query.setParameter("rejectMotivePrm",intPaymentScheduleReqHi.getRejectMotive());
		query.setParameter("rejectOtherMotivePrm",intPaymentScheduleReqHi.getRejectOtherMotive());
		query.setParameter("lastModifyDatePrm",loggerUser.getAuditTime());
		query.setParameter("lastModifyUserPrm",loggerUser.getUserName());
		query.setParameter("lastModifyIpPrm",loggerUser.getIpAddress());
		query.setParameter("lastModifyAppPrm",loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("idIntSchePrm",intPaymentScheduleReqHi.getIdIntScheduleReqHisPk());
	    query.executeUpdate();
	}
	
	/**
	 * Metodo para actualizar el estado de AmoPaymentScheduleReqHi.
	 *
	 * @param amoPaymentScheduleReqHi the amo payment schedule req hi
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateAmoPayScheReqHiReqStateServiceBean(AmoPaymentScheduleReqHi amoPaymentScheduleReqHi,LoggerUser loggerUser) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update AmoPaymentScheduleReqHi a ");
		sbQuery.append(" Set a.requestState=:statePrm, ");
		sbQuery.append(" a.rejectMotive=:rejectMotivePrm, ");
		sbQuery.append(" a.rejectOtherMotive=:rejectOtherMotivePrm, ");
		sbQuery.append(" a.lastModifyDate=:lastModifyDatePrm, ");
		sbQuery.append(" a.lastModifyUser=:lastModifyUserPrm, ");
		sbQuery.append(" a.lastModifyIp=:lastModifyIpPrm, ");
		sbQuery.append(" a.lastModifyApp=:lastModifyAppPrm ");
		sbQuery.append(" where a.idAmoScheduleReqHisPk=:idAmoSchePrm");
		
		Query query = em.createQuery(sbQuery.toString());
		
		query.setParameter("statePrm",amoPaymentScheduleReqHi.getRequestState());
		query.setParameter("rejectMotivePrm",amoPaymentScheduleReqHi.getRejectMotive());
		query.setParameter("rejectOtherMotivePrm",amoPaymentScheduleReqHi.getRejectOtherMotive());
		query.setParameter("lastModifyDatePrm",loggerUser.getAuditTime());
		query.setParameter("lastModifyUserPrm",loggerUser.getUserName());
		query.setParameter("lastModifyIpPrm",loggerUser.getIpAddress());
		query.setParameter("lastModifyAppPrm",loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("idAmoSchePrm",amoPaymentScheduleReqHi.getIdAmoScheduleReqHisPk());
	    query.executeUpdate();
	}

	/**
	 * Metodo para eliminar un Program Interest Coupon por Parent Service.
	 *
	 * @param intPaySchedule the int pay schedule
	 * @param programState the program state
	 * @throws ServiceException the service exception
	 */
	public void deleteProgramInterestCouponByParentServiceBean(InterestPaymentSchedule intPaySchedule, Integer programState) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Delete ProgramInterestCoupon p where p.interestPaymentSchedule.idIntPaymentSchedulePk=:idIntPaymentPrm");
		if(programState!=null){
			sbQuery.append(" and p.stateProgramInterest=:programState");
		}
		Query query=em.createQuery( sbQuery.toString() );
		query.setParameter("idIntPaymentPrm", intPaySchedule.getIdIntPaymentSchedulePk());
		if(programState!=null){
			query.setParameter("programState", programState);
		}
		int deleted = query.executeUpdate();
	}

	/**
	 * Metodo para eliminar un Program Amortization Coupon por Parent Service.
	 *
	 * @param amoPaySchedule the amo pay schedule
	 * @param programState the program state
	 * @throws ServiceException the service exception
	 */
	public void deleteProgramAmortizationCouponByParentServiceBean(AmortizationPaymentSchedule amoPaySchedule, Integer programState) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Delete ProgramAmortizationCoupon p where p.amortizationPaymentSchedule.idAmoPaymentSchedulePk=:idAmoPaymentPrm");
		if(programState!=null){
			sbQuery.append(" and p.stateProgramAmortization=:programState");
		}
		Query query=em.createQuery( sbQuery.toString() );
		query.setParameter("idAmoPaymentPrm", amoPaySchedule.getIdAmoPaymentSchedulePk());
		if(programState!=null){
			query.setParameter("programState", programState);
		}
		int deleted = query.executeUpdate();
	}
	
	/**
	 * Delete program interest coupon service bean.
	 *
	 * @param programInterestCoupon the program interest coupon
	 * @throws ServiceException the service exception
	 */
	public void deleteProgramInterestCouponServiceBean(ProgramInterestCoupon programInterestCoupon) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Delete ProgramInterestCoupon p where p.idProgramInterestPk=:idProgramInterestPkPrm");
		Query query=em.createQuery( sbQuery.toString() );
		query.setParameter("idProgramInterestPkPrm", programInterestCoupon.getIdProgramInterestPk());
		int deleted = query.executeUpdate();
	}

	/**
	 * Delete program amortization coupon service bean.
	 *
	 * @param programAmortizationCoupon the program amortization coupon
	 * @throws ServiceException the service exception
	 */
	public void deleteProgramAmortizationCouponServiceBean(ProgramAmortizationCoupon programAmortizationCoupon) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Delete ProgramAmortizationCoupon p where p.idProgramAmortizationPk=:idProgramAmortizationPkPrm");
		Query query=em.createQuery( sbQuery.toString() );
		query.setParameter("idProgramAmortizationPkPrm", programAmortizationCoupon.getIdProgramAmortizationPk());
		int deleted = query.executeUpdate();
	}
	
	/**
	 * Metodo para Confirmar la Reestructuracion del cronograma de Interes
	 *
	 * @param intPaymentScheduleReqHi the int payment schedule req hi
	 * @param interestPaymentSchedule the interest payment schedule
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void confirmIntPaymentScheduleRestructuringServiceBean(IntPaymentScheduleReqHi intPaymentScheduleReqHi, 
																	InterestPaymentSchedule interestPaymentSchedule,
																	LoggerUser loggerUser) throws ServiceException{
        //Change to confirmed state
        intPaymentScheduleReqHi.setRequestState( RequestStateType.CONFIRMED.getCode() );
        updateIntPayScheReqHiReqStateServiceBean(intPaymentScheduleReqHi, loggerUser);
        
        //
        //create a history for the payment schedule
        IntPaymentScheduleReqHi intPayScheReqHiToHistory=new IntPaymentScheduleReqHi();
        
        intPayScheReqHiToHistory.setSecurity( interestPaymentSchedule.getSecurity()  );
        intPayScheReqHiToHistory.setDataFromIntPaymentSchedule(interestPaymentSchedule, loggerUser, PaymentScheduleReqRegisterType.HISTORY.getCode());
        
        intPayScheReqHiToHistory.setProgramIntCouponReqHis(new ArrayList<ProgramIntCouponReqHi>());
        
        ProgramIntCouponReqHi programIntCouponReqHi=null; 
        
        for( ProgramInterestCoupon programInt : interestPaymentSchedule.getProgramInterestCoupons()){        	
        	if(programInt.getStateProgramInterest().equals( ProgramScheduleStateType.PENDING.getCode() )){
        			
                	programIntCouponReqHi=new ProgramIntCouponReqHi();
                	programIntCouponReqHi.setIntPaymentScheduleReqHi(intPayScheReqHiToHistory);
                	
                	programIntCouponReqHi.setDataFromProgramInterestCoupon(programInt, loggerUser);         	               	
                	
                	intPayScheReqHiToHistory.getProgramIntCouponReqHis().add( programIntCouponReqHi );
                	
                	CorporativeOperation cOperation=new CorporativeOperation();
                	cOperation.setProgramInterestCoupon(programInt);
                	corporateEventComponentServiceBean.deleteCorporativeOperationServiceBean(cOperation);

        	}
        }
        create(intPayScheReqHiToHistory);
        
        
        //Restructuring the current payment schedule
        InterestPaymentSchedule interestPaymentScheduleConf=new InterestPaymentSchedule();
        Security securityUpd=new Security();
        
        securityUpd.setIdIsinCode( interestPaymentSchedule.getSecurity().getIdIsinCode() );
        securityUpd.setInterestType(intPaymentScheduleReqHi.getInterestType());
        securityUpd.setInterestRate( intPaymentScheduleReqHi.getInterestFactor() );
        securityUpd.setPeriodicity( intPaymentScheduleReqHi.getPeriodicity() );
        securityUpd.setCalendarMonth( intPaymentScheduleReqHi.getCalendarMonth() );
        securityUpd.setCalendarType( intPaymentScheduleReqHi.getCalendarType() );
        securityUpd.setCalendarDays( intPaymentScheduleReqHi.getCalendarDays() );
        securityUpd.setRateType( intPaymentScheduleReqHi.getRateType() );
        securityUpd.setRateValue( intPaymentScheduleReqHi.getRateValue() );
        securityUpd.setOperationType( intPaymentScheduleReqHi.getOperationType() );
        securityUpd.setSpread( intPaymentScheduleReqHi.getSpread() );
        securityUpd.setMinimumRate( intPaymentScheduleReqHi.getMinimumRate() );
        securityUpd.setMaximumRate( intPaymentScheduleReqHi.getMaximumRate() );
        securityUpd.setAudit(loggerUser);
        if(intPaymentScheduleReqHi.isAllCouponsPendingPayment()){
        	securityServiceBean.updateSecurityIntPayScheField(securityUpd);
        }
        interestPaymentScheduleConf.setIdIntPaymentSchedulePk(interestPaymentSchedule.getIdIntPaymentSchedulePk());
        interestPaymentScheduleConf.setSecurity(interestPaymentSchedule.getSecurity());
        
        interestPaymentScheduleConf.setDataFromIntPaymentScheduleReqHi(intPaymentScheduleReqHi, loggerUser);
        
        interestPaymentScheduleConf.setProgramInterestCoupons( new ArrayList<ProgramInterestCoupon>() );
        ProgramInterestCoupon programInterestCoupon=null;
        
    	for(ProgramIntCouponReqHi proIntReq : intPaymentScheduleReqHi.getProgramIntCouponReqHis()){
    			programInterestCoupon=new ProgramInterestCoupon();
    			programInterestCoupon.setInterestPaymentCronogram( interestPaymentScheduleConf );
    			
    			programInterestCoupon.setDataFromProgramIntCouponReqHi(proIntReq, loggerUser);
    			
    			interestPaymentScheduleConf.getProgramInterestCoupons().add( programInterestCoupon );
    	}

        deleteProgramInterestCouponByParentServiceBean(interestPaymentScheduleConf,ProgramScheduleStateType.PENDING.getCode());
        interestPaymentScheduleConf=updateWithOutFlush(interestPaymentScheduleConf);
        
        CorporativeOperation corporativeOperation=null;
		CorporativeEventType interestPaymentCorp;
		interestPaymentCorp=corporateEventComponentServiceBean.findCorporativeEventType(new CorporativeEventType(
																						ImportanceEventType.INTEREST_PAYMENT.getCode(),
																						interestPaymentScheduleConf.getSecurity().getInstrumentType(),
																						com.pradera.model.corporatives.type.CorporativeEventType.RIGHTS_TO_DELIVER.getCode()));	
	
        
        for(ProgramInterestCoupon programInterestCouponToCO : interestPaymentScheduleConf.getProgramInterestCoupons()){
        	if(programInterestCouponToCO.getStateProgramInterest().equals( ProgramScheduleStateType.PENDING.getCode() )){
    			corporativeOperation=new CorporativeOperation();
    			
    			corporativeOperation.setAudit(loggerUser);
    			corporativeOperation.setDataFromProgramIntCoupon(programInterestCouponToCO,interestPaymentCorp, interestPaymentScheduleConf.getSecurity(), loggerUser);
    			corporateEventComponentServiceBean.saveCoprorativeOperation(corporativeOperation);
        	}
        }
        
	}
	
	/**
	 * Metodo para Confirmar la Reestructuracion del cronograma de Amortizacion
	 *
	 * @param amoPaymentScheduleReqHi the amo payment schedule req hi
	 * @param amortizationPaymentSchedule the amortization payment schedule
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void confirmAmoPaymentScheduleRestructuringServiceBean(AmoPaymentScheduleReqHi amoPaymentScheduleReqHi, 
																  AmortizationPaymentSchedule amortizationPaymentSchedule,
																  LoggerUser loggerUser) throws ServiceException{ 
        //Change to confirmed state
        amoPaymentScheduleReqHi.setRequestState( RequestStateType.CONFIRMED.getCode() );
        updateAmoPayScheReqHiReqStateServiceBean(amoPaymentScheduleReqHi, loggerUser);	
        BigDecimal lastNewNominalValue=null;

        //
        //create a history for the payment schedule
        AmoPaymentScheduleReqHi amoPayScheReqHiToHistory=new AmoPaymentScheduleReqHi();
        amoPayScheReqHiToHistory.setSecurity( amortizationPaymentSchedule.getSecurity() );
        
        amoPayScheReqHiToHistory.setDataFromAmoPaymentSchedule(amortizationPaymentSchedule, loggerUser, PaymentScheduleReqRegisterType.HISTORY.getCode());
        
        amoPayScheReqHiToHistory.setProgramAmoCouponReqHis( new ArrayList<ProgramAmoCouponReqHi>() );
        
        ProgramAmoCouponReqHi programAmoCouponReqHi=null; 
        
        for( ProgramAmortizationCoupon programAmo : amortizationPaymentSchedule.getProgramAmortizationCoupons()){
        		if(programAmo.getStateProgramAmortization().equals( ProgramScheduleStateType.EXPIRED.getCode())){
        			lastNewNominalValue=programAmo.getCorporativeOperation().getNewNominalValue();
        		}else if(programAmo.getStateProgramAmortization().equals( ProgramScheduleStateType.PENDING.getCode())){
        			
        			programAmoCouponReqHi=new ProgramAmoCouponReqHi();
        			//programAmoCouponReqHi.setIdProgAmoCouponReqHisPk( programAmo.getIdProgramAmortizationPk() );
        			programAmoCouponReqHi.setAmoPaymentScheduleReqHi(amoPayScheReqHiToHistory);
        			
        			programAmoCouponReqHi.setDataFromProgramAmortizationCoupon(programAmo, loggerUser);
                	
        			amoPayScheReqHiToHistory.getProgramAmoCouponReqHis().add( programAmoCouponReqHi );
        			
                	CorporativeOperation cOperation=new CorporativeOperation();
                	cOperation.setProgramAmortizationCoupon(programAmo);
//                	cOperation.setState( CorporateProcessStateType.ANNULLED.getCode() );
//                	corporateEventComponentServiceBean.updateCorporativeOperationServiceBean(cOperation,loggerUser);
                	
                	corporateEventComponentServiceBean.deleteCorporativeOperationServiceBean(cOperation);
        			
        		}
        }
        create(amoPayScheReqHiToHistory);
        
        AmortizationPaymentSchedule amortizationPaymentScheduleConf=new AmortizationPaymentSchedule();
        
        amortizationPaymentScheduleConf.setIdAmoPaymentSchedulePk(amortizationPaymentSchedule.getIdAmoPaymentSchedulePk());
        amortizationPaymentScheduleConf.setSecurity(amoPaymentScheduleReqHi.getSecurity());
        
        amortizationPaymentScheduleConf.setDataFromAmoPaymentScheduleReqHi(amoPaymentScheduleReqHi,loggerUser); 
        
        amortizationPaymentScheduleConf.setProgramAmortizationCoupons(new ArrayList<ProgramAmortizationCoupon>());
        ProgramAmortizationCoupon programAmortizationCoupon=null;
        

		for(ProgramAmoCouponReqHi proAmoReq : amoPaymentScheduleReqHi.getProgramAmoCouponReqHis()){
			programAmortizationCoupon=new ProgramAmortizationCoupon();
			programAmortizationCoupon.setAmortizationPaymentSchedule(amortizationPaymentScheduleConf);
			programAmortizationCoupon.setDataFromProgramAmoCouponReqHi(proAmoReq, loggerUser);
			amortizationPaymentScheduleConf.getProgramAmortizationCoupons().add( programAmortizationCoupon );  
		}
        
        deleteProgramAmortizationCouponByParentServiceBean(amortizationPaymentScheduleConf,ProgramScheduleStateType.PENDING.getCode());
        amortizationPaymentScheduleConf=updateWithOutFlush(amortizationPaymentScheduleConf);
        
        CorporativeOperation corporativeOperation=null;
     
        	for (int i = 0 ; i< amortizationPaymentScheduleConf.getProgramAmortizationCoupons().size();i++) {
				ProgramAmortizationCoupon programAmortizationCouponToCO = (ProgramAmortizationCoupon)amortizationPaymentScheduleConf.getProgramAmortizationCoupons().get(i);
				if(CapitalPaymentModalityType.PARTIAL.getCode().equals(amortizationPaymentScheduleConf.getSecurity().getCapitalPaymentModality())){
					if(i == ( amortizationPaymentScheduleConf.getProgramAmortizationCoupons().size()-1)){
						programAmortizationCouponToCO.setUltimateAmortization(BooleanType.YES.getBooleanValue());
					}else{
						programAmortizationCouponToCO.setUltimateAmortization(BooleanType.NO.getBooleanValue());
					}					
				}
        	if(programAmortizationCouponToCO.getStateProgramAmortization().equals( ProgramScheduleStateType.PENDING.getCode() )){
	        	corporativeOperation=new CorporativeOperation();
	    		CorporativeEventType capitalAmortizacionCorp;
	    		capitalAmortizacionCorp=corporateEventComponentServiceBean.findCorporativeEventType(new CorporativeEventType(
	    																							ImportanceEventType.CAPITAL_AMORTIZATION.getCode(),
	    																							amortizationPaymentScheduleConf.getSecurity().getInstrumentType(),
	    																							com.pradera.model.corporatives.type.CorporativeEventType.RIGHTS_TO_DELIVER.getCode()));
				
				corporativeOperation.setAudit(loggerUser);
				corporativeOperation.setDataFromProgramAmortCoupon(programAmortizationCouponToCO,capitalAmortizacionCorp, amortizationPaymentScheduleConf.getSecurity(), loggerUser);
				lastNewNominalValue = corporativeOperation.calculateAmortizationNominalValues(amortizationPaymentScheduleConf.getSecurity().getInitialNominalValue(),amortizationPaymentScheduleConf.getSecurity().isAtMaturityAmortPaymentModality(), lastNewNominalValue);
				
				corporateEventComponentServiceBean.saveCoprorativeOperation(corporativeOperation);
			}
        }
        
	}
	
	/**
	 * issue 1248: elimina las tablas asociadas a un corporativo cuyo cupon(interes/amortizacion) esta en estado vencido, solo aplica para los VENCIDOS
	 * @param idCorporativeOperationPk
	 */
	public void deleteTablesAssociatedOneCorporative(Long idCorporativeOperationPk){
		
		// issue 1232: eliminacion de tablas asociadas a los corporativos cuyo cupon(interes/amortizacio) ya vencio
		List<String> lstQuery=new ArrayList<>();
		
		StringBuilder sd;
		
		// eliminacion de los REPORT_CORPORATIVE_RESULT de un CORPORATIVE_OPERATION
		sd=new StringBuilder();
		sd.append(" DELETE FROM REPORT_CORPORATIVE_RESULT                                                                                                                       ");
		sd.append(" WHERE ID_CORPORATIVE_PROCESS_RESULT IN (SELECT RCR.ID_CORPORATIVE_PROCESS_RESULT FROM REPORT_CORPORATIVE_RESULT RCR                                         ");
		sd.append(" 										INNER JOIN CORPORATIVE_PROCESS_RESULT CPR ON CPR.ID_CORPORATIVE_PROCESS_RESULT = RCR.ID_CORPORATIVE_PROCESS_RESULT  ");
		sd.append(" 										WHERE CPR.ID_CORPORATIVE_OPERATION_FK = :idCorporativeOperationPk )													");
		lstQuery.add(sd.toString());
		
		sd=new StringBuilder();
		sd.append(" DELETE FROM BLOCK_CORPORATIVE_RESULT                                                                                                                     ");
		sd.append(" WHERE ID_BLOCK_CORPORATIVE_RESULT IN (SELECT BCR.ID_BLOCK_CORPORATIVE_RESULT FROM BLOCK_CORPORATIVE_RESULT BCR                                           ");
		sd.append(" 									  INNER JOIN CORPORATIVE_PROCESS_RESULT CPR ON CPR.ID_CORPORATIVE_PROCESS_RESULT = BCR.ID_CORPORATIVE_PROCESS_RESULT ");
		sd.append(" 									  WHERE CPR.ID_CORPORATIVE_OPERATION_FK = :idCorporativeOperationPk)											     ");
		lstQuery.add(sd.toString());
		
		// eliminacion de los CORPORATIVE_RESULT_DETAIL de un CORPORATIVE_OPERATION
		lstQuery.add(" DELETE FROM CORPORATIVE_RESULT_DETAIL WHERE ID_CORPORATIVE_OPERATION_FK = :idCorporativeOperationPk ");
		
		// eliminacion de los CORPORATIVE_PROCESS_RESULT de un CORPORATIVE_OPERATION
		lstQuery.add(" DELETE FROM CORPORATIVE_PROCESS_RESULT WHERE ID_CORPORATIVE_OPERATION_FK = :idCorporativeOperationPk ");
		
		lstQuery.add(" DELETE FROM SECURITIES_OPERATION WHERE ID_CORPORATIVE_OPERATION_FK = :idCorporativeOperationPk");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idCorporativeOperationPk", idCorporativeOperationPk);
		for(String query:lstQuery){
			// es un metodo generico que ejecuta sentencias SQL nativas con parametros
			securityServiceBean.executeRemovedSecurityQueryNative(query, parameters);
		}
	}
	
	/**
	 * Metodo para Confirmar la Modificacion del cronograma de Interes
	 *
	 * @param intPaymentScheduleReqHi the int payment schedule req hi
	 * @param interestPaymentSchedule the interest payment schedule
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void confirmIntPaymentScheduleModificationServiceBean(IntPaymentScheduleReqHi intPaymentScheduleReqHi, 
																 InterestPaymentSchedule interestPaymentSchedule,
																 LoggerUser loggerUser) throws ServiceException{
		// actualizando el nro de cupones
		Integer newNumberCoupons=null;
		if(Validations.validateIsNotNull(intPaymentScheduleReqHi)
				&& Validations.validateIsNotNull(intPaymentScheduleReqHi.getSecurity())
				&& Validations.validateIsNotNull(intPaymentScheduleReqHi.getSecurity().getIdSecurityCodePk())
				&& Validations.validateListIsNotNullAndNotEmpty(intPaymentScheduleReqHi.getProgramIntCouponReqHis())){
			newNumberCoupons = intPaymentScheduleReqHi.getProgramIntCouponReqHis().size();
		}
		
        //Change to confirmed state
        intPaymentScheduleReqHi.setRequestState( RequestStateType.CONFIRMED.getCode() );
        updateIntPayScheReqHiReqStateServiceBean(intPaymentScheduleReqHi, loggerUser);
        
        //
        //create a history for the payment schedule
        IntPaymentScheduleReqHi intPayScheReqHiToHistory=new IntPaymentScheduleReqHi();
        
        intPayScheReqHiToHistory.setSecurity( interestPaymentSchedule.getSecurity()  );
        intPayScheReqHiToHistory.setDataFromIntPaymentSchedule(interestPaymentSchedule, loggerUser, PaymentScheduleReqRegisterType.HISTORY.getCode());
      
        intPayScheReqHiToHistory.setProgramIntCouponReqHis(new ArrayList<ProgramIntCouponReqHi>());
        
        //issue 1232:  eliminacion de cupones y corporativos para aluego ser regenerados
        for( ProgramInterestCoupon programInt : interestPaymentSchedule.getProgramInterestCoupons()){
        	CorporativeOperation cOperation=new CorporativeOperation();
        	cOperation.setIdCorporativeOperationPk(programInt.getCorporativeOperation().getIdCorporativeOperationPk());
        	cOperation.setProgramInterestCoupon(programInt);
        	
        	// si el cupon esta en estado VENCIDO se debera eliminar las tablas asociadas a CORPORATIVE_OPERATION
        	if(!programInt.getStateProgramInterest().equals(ProgramScheduleStateType.PENDING.getCode())){
        		log.info("::: Cupon en estado distinto de VIGENTE, se eliminaran las tablas asociadas a CORPORATIVE_OPERATION");
        		deleteTablesAssociatedOneCorporative(cOperation.getIdCorporativeOperationPk());
        	}
        	
        	corporateEventComponentServiceBean.deleteCorporativeOperationServiceBean(cOperation);
        	deleteProgramInterestCouponServiceBean(programInt);
        }
        create(intPayScheReqHiToHistory);
        
        InterestPaymentSchedule interestPaymentScheduleConf=new InterestPaymentSchedule();
        
        interestPaymentScheduleConf.setIdIntPaymentSchedulePk(interestPaymentSchedule.getIdIntPaymentSchedulePk());
        interestPaymentScheduleConf.setSecurity(intPaymentScheduleReqHi.getSecurity());
        
        interestPaymentScheduleConf.setDataFromIntPaymentScheduleReqHi(intPaymentScheduleReqHi, loggerUser);
        
        interestPaymentScheduleConf.setProgramInterestCoupons( new ArrayList<ProgramInterestCoupon>() );
        ProgramInterestCoupon programInterestCoupon=null;
        
        if(Validations.validateIsNotNull(newNumberCoupons)){
        	// issue 1232: reconstruyendo los cupones de acuerdo a la tabla ProgramIntCouponReqHi
        	for(int i = 1; i <= newNumberCoupons; i++){
        		for(ProgramIntCouponReqHi proIntReq : intPaymentScheduleReqHi.getProgramIntCouponReqHis()){
        			if(proIntReq.getCouponNumber().intValue() == i){
        				programInterestCoupon=new ProgramInterestCoupon();
            			programInterestCoupon.setInterestPaymentCronogram( interestPaymentScheduleConf );
            			programInterestCoupon.setSituationProgramInterest( ProgramScheduleSituationType.PENDING.getCode() );
            			programInterestCoupon.setDataFromProgramIntCouponReqHi(proIntReq, loggerUser);
            			interestPaymentScheduleConf.getProgramInterestCoupons().add( programInterestCoupon );
            			break;
        			}
        		}
            }	
        }else{
        	log.error("::: Error: No se pudo obtener la nueva cantidad de cupones");
        	throw new ServiceException();
        }
        
        interestPaymentScheduleConf=updateWithOutFlush(interestPaymentScheduleConf);
        CorporativeOperation cOperation=null;
		CorporativeEventType interestPaymentCorp;
		interestPaymentCorp=corporateEventComponentServiceBean.findCorporativeEventType(new CorporativeEventType(
																						ImportanceEventType.INTEREST_PAYMENT.getCode(),
																						interestPaymentSchedule.getSecurity().getInstrumentType(),
																						com.pradera.model.corporatives.type.CorporativeEventType.RIGHTS_TO_DELIVER.getCode()));	
	
        
		// issue 1232: reconstruccion de corporativos
		// esta recosntruccion esta basada en la generacion de corporativos en le registro de valor. Class: SecuritiesServiceBean -> method: registrySecurityServiceBean()
        for(ProgramInterestCoupon programIntCoupon : interestPaymentScheduleConf.getProgramInterestCoupons()){
			cOperation=new CorporativeOperation();
			cOperation.setDataFromProgramIntCoupon(programIntCoupon,interestPaymentCorp, interestPaymentSchedule.getSecurity() , loggerUser);
			cOperation.setCreationDate(CommonsUtilities.currentDate());
			if (cOperation.getIndTax().equals(BooleanType.YES.getCode())) {
				BigDecimal value = securitiesServiceBean.findTaxFactor();
				cOperation.setTaxFactor(value);
			} else if (!cOperation.getIndTax().equals(BooleanType.YES.getCode())) {
				cOperation.setTaxFactor(null);
			}
			corporateEventComponentServiceBean.saveCoprorativeOperation(cOperation);      
        }
        
        // issue 1232: aplicando cambios desde el valor
        Security sec = em.find(Security.class, intPaymentScheduleReqHi.getSecurity().getIdSecurityCodePk());
        sec.setInterestPaymentSchedule(interestPaymentScheduleConf);
		sec.setNumberCoupons(newNumberCoupons);
		em.merge(sec);
		em.flush();
	}
	
	/**
	 * Metodo para Confirmar la Modificacion del cronograma de Amortizacion
	 *
	 * @param amoPaymentScheduleReqHi the amo payment schedule req hi
	 * @param amortizationPaymentSchedule the amortization payment schedule
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	/**
	// revertido salida a produccion issues: 100,148,143
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void confirmAmoPaymentScheduleModificationServiceFacadeNew(AmoPaymentScheduleReqHi amoPaymentScheduleReqHi, AmortizationPaymentSchedule amortizationPaymentSchedule, LoggerUser loggerUser) throws ServiceException {
		List<CorporativeOperation> cOperationDeletedList = new ArrayList<CorporativeOperation>();
		List<CorporativeOperation> cOperationInsertedList = new ArrayList<CorporativeOperation>();
		CorporativeOperation cOperationDeleted = new CorporativeOperation();
		List<Integer> lstCouponNumberToModify = new ArrayList<Integer>();
		//Change to confirmed state
        amoPaymentScheduleReqHi.setRequestState( RequestStateType.CONFIRMED.getCode() );
        updateAmoPayScheReqHiReqStateServiceBean(amoPaymentScheduleReqHi, loggerUser);        
        //create a history for the payment schedule
        AmoPaymentScheduleReqHi amoPayScheReqHiToHistory = new AmoPaymentScheduleReqHi();
        amoPayScheReqHiToHistory.setSecurity( amortizationPaymentSchedule.getSecurity() );        
        amoPayScheReqHiToHistory.setDataFromAmoPaymentSchedule(amortizationPaymentSchedule, loggerUser, PaymentScheduleReqRegisterType.HISTORY.getCode());        
        amoPayScheReqHiToHistory.setProgramAmoCouponReqHis( new ArrayList<ProgramAmoCouponReqHi>() );
        ProgramAmoCouponReqHi programAmoCouponReqHi = null;
        //obtenemos el capital amortizado
        CorporativeEventType capitalAmortizacionCorp = corporateEventComponentServiceBean.findCorporativeEventType(new CorporativeEventType(ImportanceEventType.CAPITAL_AMORTIZATION.getCode(),
																																			amortizationPaymentSchedule.getSecurity().getInstrumentType(),
																																			com.pradera.model.corporatives.type.CorporativeEventType.RIGHTS_TO_DELIVER.getCode()));
        //recorremos los cupones del schedule actual
        for( ProgramAmortizationCoupon programAmo : amortizationPaymentSchedule.getProgramAmortizationCoupons() ) {        	
        	//recorremos los cupones del schedule his
        	for( ProgramAmoCouponReqHi pro : amoPaymentScheduleReqHi.getProgramAmoCouponReqHis() ) {
        		//si cupon esta vigente y tienen el mismo numero de cupon
        		if( pro.getStateProgramAmortizaton().equals( ProgramScheduleStateType.PENDING.getCode() ) && pro.getCouponNumber().equals( programAmo.getCouponNumber() )) {        			
        			//seteando valores
        			cOperationDeletedList.add(programAmo.getCorporativeOperation());        					
        			programAmoCouponReqHi = new ProgramAmoCouponReqHi();
        			programAmoCouponReqHi.setAmoPaymentScheduleReqHi(amoPayScheReqHiToHistory);        			
        			programAmoCouponReqHi.setDataFromProgramAmortizationCoupon(programAmo, loggerUser);        			
        			lstCouponNumberToModify.add(programAmoCouponReqHi.getCouponNumber());        			
        			amoPayScheReqHiToHistory.getProgramAmoCouponReqHis().add( programAmoCouponReqHi );
            		CorporativeOperation cOperationInserted = new CorporativeOperation();	
            		//si la modalidad es de pago parciales
            		if(CapitalPaymentModalityType.PARTIAL.getCode().equals(amortizationPaymentSchedule.getSecurity().getCapitalPaymentModality())) {
    					//si es el ultimo cupon
            			if(amortizationPaymentSchedule.getProgramAmortizationCoupons().indexOf(programAmo) == (amortizationPaymentSchedule.getProgramAmortizationCoupons().size()-1)) {
    						programAmo.setUltimateAmortization(BooleanType.YES.getBooleanValue());
    					} else {
    						programAmo.setUltimateAmortization(BooleanType.NO.getBooleanValue());    						
    					}					
    				}
            		cOperationInserted.setDataFromProgramAmortCoupon( programAmo, capitalAmortizacionCorp, amortizationPaymentSchedule.getSecurity(), loggerUser );
            		cOperationInserted.setOldNominalValue( cOperationDeleted.getOldNominalValue() );
            		cOperationInserted.setNewNominalValue( cOperationDeleted.getNewNominalValue() ); 
            		//save corporative operation
            		//TODO: descomentar para usar el metodo alterno al UPDATE, se debera deployar iCoreComponent REVISION: 2012
//            		corporateEventComponentServiceBean.saveCoprorativeOperation( cOperationInserted );        			
//            		em.flush();
//            		cOperationInsertedList.add(cOperationInserted);
//            		//update tables depending corporative deleted
//            		corporateEventComponentServiceBean.updateCorporativeResultDetail( cOperationInserted.getIdCorporativeOperationPk(), cOperationDeleted.getIdCorporativeOperationPk(), loggerUser );      
//            		corporateEventComponentServiceBean.updateCorporativeProcessResult( cOperationInserted.getIdCorporativeOperationPk(), cOperationDeleted.getIdCorporativeOperationPk(), loggerUser );      
//            		Long idSecuritiesOperation = corporateEventComponentServiceBean.getSecuritiesOperationId( cOperationDeleted.getIdCorporativeOperationPk() );
//        			if (idSecuritiesOperation != null) {
//        				corporateEventComponentServiceBean.updateSecuritiesOperation( cOperationInserted.getIdCorporativeOperationPk(), idSecuritiesOperation, loggerUser );
//        			}
        			break;
        		}
        	}
        }
        //crea la nueva cuponera con los valores seteados
        create(amoPayScheReqHiToHistory);        
        //seteamos datos
        AmortizationPaymentSchedule amortizationPaymentScheduleConf = new AmortizationPaymentSchedule();        
        amortizationPaymentScheduleConf.setIdAmoPaymentSchedulePk(amortizationPaymentSchedule.getIdAmoPaymentSchedulePk());
        amortizationPaymentScheduleConf.setSecurity(amoPaymentScheduleReqHi.getSecurity());        
        amortizationPaymentScheduleConf.setDataFromAmoPaymentScheduleReqHi(amoPaymentScheduleReqHi, loggerUser);        
        amortizationPaymentScheduleConf.setProgramAmortizationCoupons(new ArrayList<ProgramAmortizationCoupon>());
        ProgramAmortizationCoupon programAmortizationCoupon = null;
        //si el schedule es de pago proporcional
        if(amortizationPaymentSchedule.isProportionalAmortizationType()) {        
        	List<Long> lstProgAmoCoupons = new ArrayList<Long>();
        	//recorremos los cupones del schedule actual
        	for(ProgramAmortizationCoupon proAmo : amortizationPaymentSchedule.getProgramAmortizationCoupons()) {
            	//recorremos los cupones del schedule his
        		for(ProgramAmoCouponReqHi proAmoReq : amoPaymentScheduleReqHi.getProgramAmoCouponReqHis()) {            		
            		//si cupon esta vigente y los numeros de cupon son iguales
        			if(proAmo.getStateProgramAmortization().equals( ProgramScheduleStateType.PENDING.getCode() ) && proAmo.getCouponNumber().equals( proAmoReq.getCouponNumber() )) {            			
            			//seteamos datos
        				programAmortizationCoupon = new ProgramAmortizationCoupon();
            		    programAmortizationCoupon.setAmortizationPaymentSchedule(amortizationPaymentScheduleConf);            			
            			programAmortizationCoupon.setDataFromProgramAmoCouponReqHi(proAmoReq, loggerUser);
            			amortizationPaymentScheduleConf.getProgramAmortizationCoupons().add( programAmortizationCoupon );
            			lstProgAmoCoupons.add(proAmo.getIdProgramAmortizationPk());
            			break;
            		}
            	}
            }
        	//actualiza cuponeta con datos seteados
            amortizationPaymentScheduleConf = update(amortizationPaymentScheduleConf); 
            em.flush();        
        //si el schedule es de pago no proporcional
        } else if(amortizationPaymentSchedule.isNotProportionalAmortizationType()) {
        	List<Long> lstProgAmoCoupons = new ArrayList<Long>();
        	//recorremos los cupones del schedule actual
        	for(ProgramAmortizationCoupon proAmo : amortizationPaymentSchedule.getProgramAmortizationCoupons()) {
            	//recorremos los cupones del schedule his
        		for(ProgramAmoCouponReqHi proAmoReq : amoPaymentScheduleReqHi.getProgramAmoCouponReqHis()) {            		
            		//si cupon esta vigente y los numeros de cupon son iguales
        			if(proAmo.getStateProgramAmortization().equals( ProgramScheduleStateType.PENDING.getCode() ) && proAmo.getCouponNumber().equals( proAmoReq.getCouponNumber() )) {            			
            			//seteamos datos
        				programAmortizationCoupon = new ProgramAmortizationCoupon();
            		    programAmortizationCoupon.setAmortizationPaymentSchedule(amortizationPaymentScheduleConf);            			
            			programAmortizationCoupon.setDataFromProgramAmoCouponReqHi(proAmoReq, loggerUser);
            			amortizationPaymentScheduleConf.getProgramAmortizationCoupons().add( programAmortizationCoupon );
            			lstProgAmoCoupons.add(proAmo.getIdProgramAmortizationPk());
            			break;
            		}
            	}
            }
        	//actualiza cuponeta con datos seteados
            amortizationPaymentScheduleConf = update(amortizationPaymentScheduleConf); 
            em.flush();          
        }
	}	
	//metodo para borrar los corporativos y cupones anteriores a la modificacion
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void deleteCorpOperationProgAmoCoupOld(AmoPaymentScheduleReqHi amoPaymentScheduleReqHi, AmortizationPaymentSchedule amortizationPaymentSchedule) throws ServiceException {		
		CorporativeOperation cOperationDeleted = null;	
		//TODO: descomentar para usar el metodo alterno al UPDATE, se debera deployar iCoreComponent REVISION: 2012
//		List<Long> idProgramAmortizationPksCorpList = corporateEventComponentServiceBean.getCorpOperation(amortizationPaymentSchedule.getSecurity().getIdSecurityCodePk());
//		List<Long> idProgramAmortizationPksCoupList = corporateEventComponentServiceBean.getProAmoCoupons(amortizationPaymentSchedule.getIdAmoPaymentSchedulePk());
//		List<Long> corpOperationModifiedList = corporateEventComponentServiceBean.getProAmoCoupons(amortizationPaymentSchedule.getIdAmoPaymentSchedulePk());
//		for(int j=0; j<idProgramAmortizationPksCorpList.size(); j++) {
//			for(int i=0; i<idProgramAmortizationPksCoupList.size(); i++) {
//				//en el mismo orden y en la misma posicion
//				if(j == i) {
//					if( !idProgramAmortizationPksCorpList.get(j).equals(idProgramAmortizationPksCoupList.get(j)) ) {
//						Long newProgAmortizationPk = corporateEventComponentServiceBean.corpOperationModified(idProgramAmortizationPksCorpList.get(j));
//						//relacionamos nuevos corporativos con nuevos cupones
//						corporateEventComponentServiceBean.updateCorporativeOperationProgramAmoPk(newProgAmortizationPk, corpOperationModifiedList.get(j) );
//			        	em.flush();	        			
//					}
//					break;
//				}				
//			}
//		}
//		for( ProgramAmortizationCoupon programAmo : amortizationPaymentSchedule.getProgramAmortizationCoupons()) {        	
//        	for(ProgramAmoCouponReqHi pro : amoPaymentScheduleReqHi.getProgramAmoCouponReqHis()) {
//        		if(pro.getStateProgramAmortizaton().equals( ProgramScheduleStateType.PENDING.getCode() ) && pro.getCouponNumber().equals( programAmo.getCouponNumber() )) {        			
//        			cOperationDeleted = programAmo.getCorporativeOperation();
//        			//borramos corporativos y cupones anteriores
//        			corporateEventComponentServiceBean.deleteCorporativeOperationServiceBean( cOperationDeleted );
//        			em.flush();
//        			break;        			
//        		}
//        	}       	
//        }	
	}
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void deleteProgramAmortizationCouponOld(AmoPaymentScheduleReqHi amoPaymentScheduleReqHi, AmortizationPaymentSchedule amortizationPaymentSchedule) throws ServiceException {		
		for( ProgramAmortizationCoupon programAmo : amortizationPaymentSchedule.getProgramAmortizationCoupons()) {        	
        	for(ProgramAmoCouponReqHi pro : amoPaymentScheduleReqHi.getProgramAmoCouponReqHis()) {
        		if(pro.getStateProgramAmortizaton().equals( ProgramScheduleStateType.PENDING.getCode() ) && pro.getCouponNumber().equals( programAmo.getCouponNumber() )) {        			
        			deleteProgramAmortizationCouponServiceBean(programAmo);
        			break;        			
        		}
        	}       	
        }		
	}
	// MODIFICA SOLO LOS CUPONES DE AMORTIZACION
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void confirmAmoPaymentScheduleModificationUpdateServiceFacadeNew(AmoPaymentScheduleReqHi amoPaymentScheduleReqHi, AmortizationPaymentSchedule amortizationPaymentSchedule, LoggerUser loggerUser) throws ServiceException {
		//ubicar n-esimo y el ultimo cupon y modificar
		BigDecimal newNominalValueMod = null;
		BigDecimal oldNominalValueMod = null;
		Long idProgramAmortizationPk = null;
		String securityCode = null;
		boolean sw = true;
		BigDecimal originOldNominalValue = null;
		//sint sup = amoPaymentScheduleReqHi.getProgramAmoCouponReqHis().get(1).getCouponNumber();
        for( ProgramAmortizationCoupon programAmo : amortizationPaymentSchedule.getProgramAmortizationCoupons() ) {
        	for( ProgramAmoCouponReqHi pro : amoPaymentScheduleReqHi.getProgramAmoCouponReqHis() ) {
        		if( programAmo.getStateProgramAmortization().equals( ProgramScheduleStateType.PENDING.getCode() ) && pro.getCouponNumber().equals( programAmo.getCouponNumber() )) {        			
        			BigDecimal amortizationFactor = pro.getAmortizationFactor();
        			BigDecimal couponAmount = pro.getCouponAmount();
        			idProgramAmortizationPk = programAmo.getIdProgramAmortizationPk();        			
        			CorporativeOperation corpOperation = getCorporativeOperation(idProgramAmortizationPk);        			
        			BigDecimal newNominalValue = corpOperation.getOldNominalValue().subtract(couponAmount);
        			securityCode = amortizationPaymentSchedule.getSecurity().getIdSecurityCodePk();
        			if (sw) {
        				oldNominalValueMod = newNominalValue;
        				originOldNominalValue = corpOperation.getNewNominalValue();
        				sw = false;
        				updateCorporativeOperation( amortizationFactor, newNominalValue, idProgramAmortizationPk, securityCode, loggerUser, true );
        			} else {
        				updateCorporativeOperation( amortizationFactor, couponAmount, idProgramAmortizationPk, securityCode, loggerUser, false );
        			}        			
        			updateProgramAmortizationCoupon( couponAmount, amortizationFactor, idProgramAmortizationPk, loggerUser );	
        			break;
        		}
        	}
        }
        //ubicar cupones intermedios y alctualizar
        int inf = amoPaymentScheduleReqHi.getProgramAmoCouponReqHis().get(0).getCouponNumber();
        int sup = amoPaymentScheduleReqHi.getProgramAmoCouponReqHis().get(1).getCouponNumber();
        sw = true;        
        for( ProgramAmortizationCoupon programAmo : amortizationPaymentSchedule.getProgramAmortizationCoupons() ) {
        	if( programAmo.getStateProgramAmortization().equals(ProgramScheduleStateType.PENDING.getCode()) ) {
        		if( programAmo.getCouponNumber() > inf && programAmo.getCouponNumber() < sup ) {
        			idProgramAmortizationPk = programAmo.getIdProgramAmortizationPk();
        			securityCode = amortizationPaymentSchedule.getSecurity().getIdSecurityCodePk();
    				newNominalValueMod = oldNominalValueMod.subtract(programAmo.getAmortizationAmount());        				
        			updateListCorporativeOperations( newNominalValueMod, oldNominalValueMod, idProgramAmortizationPk, securityCode );       
        			oldNominalValueMod = newNominalValueMod;    			        			
        		}
        	}        	
        }
        //actualizar a comfirmado la solicitud
        amoPaymentScheduleReqHi.setRequestState( RequestStateType.CONFIRMED.getCode() );
        updateAmoPayScheReqHiReqStateServiceBean( amoPaymentScheduleReqHi, loggerUser );
	}
	public void updateCorporativeOperation( BigDecimal deliveryFactor, BigDecimal nominalValue, Long idProgramAmortizationFk, String securityCode, LoggerUser loggerUser, boolean flag ) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		if (flag) {
			sbQuery.append(" Update CorporativeOperation a ");
			sbQuery.append(" Set a.deliveryFactor =:deliveryFactor, ");
			sbQuery.append(" a.newNominalValue =:nominalValue, ");	
		} else {
			sbQuery.append(" Update CorporativeOperation a ");
			sbQuery.append(" Set a.deliveryFactor =:deliveryFactor, ");
			sbQuery.append(" a.oldNominalValue =:nominalValue, ");	
		}
		sbQuery.append(" a.lastModifyDate =:lastModifyDatePrm, ");
		sbQuery.append(" a.lastModifyUser =:lastModifyUserPrm, ");
		sbQuery.append(" a.lastModifyIp =:lastModifyIpPrm, ");
		sbQuery.append(" a.lastModifyApp =:lastModifyAppPrm ");		
		sbQuery.append(" where a.programAmortizationCoupon.idProgramAmortizationPk =:idProgramAmortizationFk ");
		sbQuery.append(" and a.securities.idSecurityCodePk =:securityCode ");
		sbQuery.append(" and a.accordType = 1400");
		Query query = em.createQuery(sbQuery.toString());		
		query.setParameter("deliveryFactor",deliveryFactor);
		query.setParameter("nominalValue",nominalValue);
		query.setParameter("lastModifyDatePrm",loggerUser.getAuditTime());
		query.setParameter("lastModifyUserPrm",loggerUser.getUserName());
		query.setParameter("lastModifyIpPrm",loggerUser.getIpAddress());
		query.setParameter("lastModifyAppPrm",loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("idProgramAmortizationFk",idProgramAmortizationFk);
		query.setParameter("securityCode",securityCode);
		query.executeUpdate();
	}
	public void updateProgramAmortizationCoupon( BigDecimal anortizationAmount, BigDecimal amortizationFactor, Long idProgramAmortizationPk, LoggerUser loggerUser ) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update ProgramAmortizationCoupon a ");
		sbQuery.append(" Set a.amortizationAmount=:anortizationAmount, ");
		sbQuery.append(" a.couponAmount=:anortizationAmount, ");	
		sbQuery.append(" a.amortizationFactor=:amortizationFactor, ");
		sbQuery.append(" a.lastModifyDate=:lastModifyDatePrm, ");
		sbQuery.append(" a.lastModifyUser=:lastModifyUserPrm, ");
		sbQuery.append(" a.lastModifyIp=:lastModifyIpPrm, ");
		sbQuery.append(" a.lastModifyApp=:lastModifyAppPrm ");		
		sbQuery.append(" where a.idProgramAmortizationPk=:idProgramAmortizationPk ");
		Query query = em.createQuery(sbQuery.toString());		
		query.setParameter("anortizationAmount",anortizationAmount);
		query.setParameter("amortizationFactor",amortizationFactor);
		query.setParameter("lastModifyDatePrm",loggerUser.getAuditTime());
		query.setParameter("lastModifyUserPrm",loggerUser.getUserName());
		query.setParameter("lastModifyIpPrm",loggerUser.getIpAddress());
		query.setParameter("lastModifyAppPrm",loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("idProgramAmortizationPk",idProgramAmortizationPk);
		query.executeUpdate();
	}
	public List<CorporativeOperation> getListCorporativeOperationNewOldNominalValue( String idSecurityCode ) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT co ");
		sbQuery.append("FROM CorporativeOperation co ");
		sbQuery.append("WHERE 1 = 1	");
		sbQuery.append("AND co.securities.idSecurityCodePk = :idSecurityCodePk ");
		sbQuery.append("AND co.accordType = 1400 ");
		sbQuery.append("AND co.state = 1309 ");			
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSecurityCodePk", idSecurityCode);
		return (List<CorporativeOperation>)query.getResultList();		
	}
	public CorporativeOperation getCorporativeOperation( Long idProgramAmortizationPk ) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT co ");
		sbQuery.append("FROM CorporativeOperation co ");
		sbQuery.append("WHERE co.programAmortizationCoupon.idProgramAmortizationPk = :idProgramAmortizationPk ");
		sbQuery.append("AND co.accordType = 1400 ");
		sbQuery.append("AND co.state = 1309 ");			
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idProgramAmortizationPk", idProgramAmortizationPk);
		return (CorporativeOperation)query.getSingleResult(); // getResultList();
	}
	public void updateListCorporativeOperations( BigDecimal newNominalValue, BigDecimal oldNominalValue, Long idProgramAmortizationPk, String securityCode ) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" update CorporativeOperation a ");
		sbQuery.append(" set a.oldNominalValue =:oldNominalValue, ");
		sbQuery.append(" a.newNominalValue =:newNominalValue ");		
		sbQuery.append(" where a.programAmortizationCoupon.idProgramAmortizationPk =:idProgramAmortizationPk ");
		sbQuery.append(" and a.securities.idSecurityCodePk =:securityCode ");
		sbQuery.append(" and a.accordType = 1400 ");		
		Query query = em.createQuery(sbQuery.toString());		
		query.setParameter("newNominalValue", newNominalValue);
		query.setParameter("oldNominalValue", oldNominalValue);
		query.setParameter("idProgramAmortizationPk", idProgramAmortizationPk);
		query.setParameter("securityCode", securityCode);
		query.executeUpdate();
	}	
	*/
	/**METODO ANTERIOR A LA MODIFICACION DE LA CUPONERA DE AMORTIZACIONES*/
	public void confirmAmoPaymentScheduleModificationServiceFacade(AmoPaymentScheduleReqHi amoPaymentScheduleReqHi, AmortizationPaymentSchedule amortizationPaymentSchedule, LoggerUser loggerUser) throws ServiceException{
	
		// obteneiendo el nuevo nro de amortizaciones
		Integer newNumberCoupons=null;
		if(Validations.validateIsNotNull(amoPaymentScheduleReqHi)
				&& Validations.validateIsNotNull(amoPaymentScheduleReqHi.getSecurity())
				&& Validations.validateIsNotNull(amoPaymentScheduleReqHi.getSecurity().getIdSecurityCodePk())
				&& Validations.validateListIsNotNullAndNotEmpty(amoPaymentScheduleReqHi.getProgramAmoCouponReqHis())){
			newNumberCoupons = amoPaymentScheduleReqHi.getProgramAmoCouponReqHis().size();
		}

		//Change to confirmed state
		amoPaymentScheduleReqHi.setRequestState( RequestStateType.CONFIRMED.getCode() );
		updateAmoPayScheReqHiReqStateServiceBean(amoPaymentScheduleReqHi, loggerUser);
		
        //create a history for the payment schedule
        AmoPaymentScheduleReqHi amoPayScheReqHiToHistory=new AmoPaymentScheduleReqHi();
        amoPayScheReqHiToHistory.setSecurity( amortizationPaymentSchedule.getSecurity() );
        amoPayScheReqHiToHistory.setDataFromAmoPaymentSchedule(amortizationPaymentSchedule, loggerUser, PaymentScheduleReqRegisterType.HISTORY.getCode());
        amoPayScheReqHiToHistory.setProgramAmoCouponReqHis( new ArrayList<ProgramAmoCouponReqHi>() );
        ProgramAmoCouponReqHi programAmoCouponReqHi=null; 
        
        // issue 1232: eliminando corporativos y amortizaciones para luego ser regenerados
        for( ProgramAmortizationCoupon programAmo : amortizationPaymentSchedule.getProgramAmortizationCoupons()){
			amoPayScheReqHiToHistory.getProgramAmoCouponReqHis().add( programAmoCouponReqHi );
			CorporativeOperation cOperation=new CorporativeOperation();
			cOperation.setIdCorporativeOperationPk(programAmo.getCorporativeOperation().getIdCorporativeOperationPk());
			cOperation.setProgramAmortizationCoupon( programAmo );
			
			// si el cupon esta en estado VENCIDO se debera eliminar las tablas asociadas a CORPORATIVE_OPERATION
        	if(!programAmo.getStateProgramAmortization().equals(ProgramScheduleStateType.PENDING.getCode())){
        		log.info("::: Cupon en estado distinto de VIGENTE, se eliminaran las tablas asociadas a CORPORATIVE_OPERATION");
        		deleteTablesAssociatedOneCorporative(cOperation.getIdCorporativeOperationPk());
        	}
			
			corporateEventComponentServiceBean.deleteCorporativeOperationServiceBean(cOperation);
			deleteProgramAmortizationCouponServiceBean(programAmo);
        }
        create(amoPayScheReqHiToHistory);
        
        AmortizationPaymentSchedule amortizationPaymentScheduleConf=new AmortizationPaymentSchedule();
        
        amortizationPaymentScheduleConf.setIdAmoPaymentSchedulePk(amortizationPaymentSchedule.getIdAmoPaymentSchedulePk());
        amortizationPaymentScheduleConf.setSecurity(amoPaymentScheduleReqHi.getSecurity());
        
        amortizationPaymentScheduleConf.setDataFromAmoPaymentScheduleReqHi(amoPaymentScheduleReqHi, loggerUser);
        
        amortizationPaymentScheduleConf.setProgramAmortizationCoupons(new ArrayList<ProgramAmortizationCoupon>());
        ProgramAmortizationCoupon programAmortizationCoupon=null;
        Date paymentFirstExpirationDate=null;
        
        if(amortizationPaymentSchedule.isProportionalAmortizationType()){
        	
        	BigDecimal amountAmortization=BigDecimal.ZERO;
        	
        	// issue 1232: reconstruyendo las amortizaciones de acuerdo a la tabla: ProgramAmoCouponReqHi 
        	if(Validations.validateIsNotNull(newNumberCoupons)){
        		for(int i = 1; i <= newNumberCoupons; i++){
        			for(ProgramAmoCouponReqHi proAmoReq : amoPaymentScheduleReqHi.getProgramAmoCouponReqHis()){
        				if(proAmoReq.getCouponNumber().intValue() == i){
        					// capturando fecha vencimiento de la 1ra amortizacion para actualizar en 
        					// AmortizationPaymentSchedule y Security
        					if(i==1){
        						paymentFirstExpirationDate = proAmoReq.getExpirationDate();	
        					}
        					
        					programAmortizationCoupon=new ProgramAmortizationCoupon();
            				programAmortizationCoupon.setAmortizationPaymentSchedule(amortizationPaymentScheduleConf);
            				programAmortizationCoupon.setDataFromProgramAmoCouponReqHi(proAmoReq, loggerUser);
            				programAmortizationCoupon.setSituationProgramAmortization( ProgramScheduleSituationType.PENDING.getCode() );
            				if(Validations.validateIsNotNull(programAmortizationCoupon.getAmortizationAmount())){
            					amountAmortization=programAmortizationCoupon.getCouponAmount();
            					programAmortizationCoupon.setAmortizationAmount(amountAmortization);
            				}
            				
            				amortizationPaymentScheduleConf.getProgramAmortizationCoupons().add( programAmortizationCoupon ); 
        					break;
        				}
                	}
        		}
        	}else{
            	log.error("::: Error: No se pudo obtener la nueva cantidad de cupones");
            	throw new ServiceException();
            }
            	
        	if(Validations.validateIsNotNull(paymentFirstExpirationDate)){
        		amortizationPaymentScheduleConf.setPaymentFirstExpirationDate(paymentFirstExpirationDate);
        	}
        	amortizationPaymentScheduleConf.setPeriodicityDays(null);
        	amortizationPaymentScheduleConf.setNumberPayments(newNumberCoupons);
            amortizationPaymentScheduleConf=update(amortizationPaymentScheduleConf);
            
            CorporativeOperation corporativeOperation=null;
            BigDecimal lastNewNominalValue=null;
    		CorporativeEventType capitalAmortizacionCorp;
    		capitalAmortizacionCorp=corporateEventComponentServiceBean.findCorporativeEventType(new CorporativeEventType(
    																							ImportanceEventType.CAPITAL_AMORTIZATION.getCode(),
    																							amortizationPaymentSchedule.getSecurity().getInstrumentType(),
    																							com.pradera.model.corporatives.type.CorporativeEventType.RIGHTS_TO_DELIVER.getCode()));
            
    		// issue 1232: reconstruccion de los corporativos
    		// esta recosntruccion esta en base al registro de valor, Class: SecuritiesServiceBean -> method: registrySecurityServiceBean()
    		for (int i = 0 ; i<amortizationPaymentScheduleConf.getProgramAmortizationCoupons().size();i++) {
				ProgramAmortizationCoupon programAmoCoupon = (ProgramAmortizationCoupon)amortizationPaymentScheduleConf.getProgramAmortizationCoupons().get(i);
				// identificando la ultima amortizacion
				if(CapitalPaymentModalityType.PARTIAL.getCode().equals(amortizationPaymentSchedule.getSecurity().getCapitalPaymentModality())){
					if(i == (amortizationPaymentScheduleConf.getProgramAmortizationCoupons().size()-1)){
						programAmoCoupon.setUltimateAmortization(BooleanType.YES.getBooleanValue());
					}else{
						programAmoCoupon.setUltimateAmortization(BooleanType.NO.getBooleanValue());
					}					
				}
				
				corporativeOperation = new CorporativeOperation();
				corporativeOperation.setDataFromProgramAmortCoupon(programAmoCoupon,capitalAmortizacionCorp, amortizationPaymentSchedule.getSecurity(), loggerUser);
				corporativeOperation.setCreationDate(CommonsUtilities.currentDate());
				lastNewNominalValue = corporativeOperation.calculateAmortizationNominalValues(amortizationPaymentSchedule.getSecurity().getInitialNominalValue(),amortizationPaymentSchedule.getSecurity().isAtMaturityAmortPaymentModality(), lastNewNominalValue);
				if (corporativeOperation.getIndTax().equals(BooleanType.YES.getCode())) {
					BigDecimal value = securitiesServiceBean.findTaxFactor();
					corporativeOperation.setTaxFactor(value);
				} else if (!corporativeOperation.getIndTax().equals(BooleanType.YES.getCode())) {
					corporativeOperation.setTaxFactor(null);
				}
				corporateEventComponentServiceBean.saveCoprorativeOperation(corporativeOperation);
            }
        }else if(amortizationPaymentSchedule.isNotProportionalAmortizationType()){
        	
        		ProgramAmoCouponReqHi proAmoReq = amoPaymentScheduleReqHi.getProgramAmoCouponReqHis().get( amoPaymentScheduleReqHi.getProgramAmoCouponReqHis().size()-1 );
        	
    			programAmortizationCoupon=new ProgramAmortizationCoupon();
    //			programAmortizationCoupon.setIdProgramAmortizationPk( proAmo.getIdProgramAmortizationPk() );
    			programAmortizationCoupon.setAmortizationPaymentSchedule(amortizationPaymentScheduleConf);
    			
    			programAmortizationCoupon.setDataFromProgramAmoCouponReqHi(proAmoReq, loggerUser);
    			
    			amortizationPaymentScheduleConf.getProgramAmortizationCoupons().add( programAmortizationCoupon );  
    			
    		    create(programAmortizationCoupon); 	
    		    
    			CorporativeOperation corporativeOperation=new CorporativeOperation();
    			CorporativeEventType capitalAmortizacionCorp;
    			capitalAmortizacionCorp=corporateEventComponentServiceBean.findCorporativeEventType(new CorporativeEventType(
																			    					ImportanceEventType.CAPITAL_AMORTIZATION.getCode(),
																			    					amortizationPaymentScheduleConf.getSecurity().getInstrumentType(),
																			    					com.pradera.model.corporatives.type.CorporativeEventType.RIGHTS_TO_DELIVER.getCode()));
    			
    			corporativeOperation.setDataFromProgramAmortCoupon(programAmortizationCoupon,capitalAmortizacionCorp, amortizationPaymentScheduleConf.getSecurity(), loggerUser);
    			corporateEventComponentServiceBean.saveCoprorativeOperation(corporativeOperation);
        }
        
        // issue 1232: aplicando cambios desde el valor
        Security sec = em.find(Security.class, amoPaymentScheduleReqHi.getSecurity().getIdSecurityCodePk());
        sec.setAmortizationPaymentSchedule(amortizationPaymentScheduleConf);
        sec.setPaymentFirstExpirationDate(paymentFirstExpirationDate);
		em.merge(sec);
		em.flush();
	}                                         
		
	/**
	 * Metodo para Registrar un IntPaymentScheduleReqHi
	 *
	 * @param intPaymentScheduleReqHi the int payment schedule req hi
	 * @param security the security
	 * @throws ServiceException the service exception
	 */
	public void registerIntPaymentScheduleReqHiServiceBean(IntPaymentScheduleReqHi intPaymentScheduleReqHi, Security security) throws ServiceException{
		validateIntPaymentScheRequestAlreadyRegisteredServiceBean(intPaymentScheduleReqHi,security);
		create(intPaymentScheduleReqHi);
	}
	
	/**
	 * Metodo para registrar un AmoPaymentScheduleReqHi
	 *
	 * @param amoPaymentScheduleReqHi the amo payment schedule req hi
	 * @param security the security
	 * @throws ServiceException the service exception
	 */
	public void registerAmoPaymentScheduleReqHiServiceFacade(AmoPaymentScheduleReqHi amoPaymentScheduleReqHi, Security security) throws ServiceException{
		validateAmoPaymentScheRequestAlreadyRegisteredServiceBean(amoPaymentScheduleReqHi, security);
		/*
		//revertido salida a produccion issues: 100,148,143
		List<ProgramAmoCouponReqHi> lista = amoPaymentScheduleReqHi.getProgramAmoCouponReqHis();
		
		for(ProgramAmoCouponReqHi programAmoCouponReqHi : lista){
			programAmoCouponReqHi.setAmoPaymentScheduleReqHi(amoPaymentScheduleReqHi);
			programAmoCouponReqHi.setRegistryUser(amoPaymentScheduleReqHi.getRegistryUser());
			programAmoCouponReqHi.setRegistryDate(amoPaymentScheduleReqHi.getRegistryDate());
		}		
		createWithFlush(amoPaymentScheduleReqHi);
		*/
		create( amoPaymentScheduleReqHi );
	}
	
	/**
	 * Metodo para validar si IntPaymentScheduleReqHi ya se encuentra registrado
	 *
	 * @param intPaymentScheduleReqHi the int payment schedule req hi
	 * @param security the security
	 * @throws ServiceException the service exception
	 */
	public void validateIntPaymentScheRequestAlreadyRegisteredServiceBean(IntPaymentScheduleReqHi intPaymentScheduleReqHi, Security security) throws ServiceException{
		Object[] argObj=new Object[4];
		argObj[0]=security.getIdSecurityCodePk();

		Object[] requestArg = {RequestStateType.REGISTERED.getCode(),RequestStateType.APPROVED.getCode()}; 
		IntPaymentScheduleReqHi intPayScheRequestValidate=validateIntPaymentScheduleReqHiServiceBean(intPaymentScheduleReqHi.getIdIntScheduleReqHisPk(), 
																									security.getIdSecurityCodePk(), 
																									null, 
																									requestArg);
		if(intPayScheRequestValidate!=null){
			if(PaymentScheduleReqRegisterType.REQUEST_MODIFICATION.getCode().equals( intPayScheRequestValidate.getRegistryType() )){
				argObj[1]=PropertiesUtilities.getMessage(PropertiesConstants.PAYMENT_CRON_LBL_MODIFICATION, null);
			}else if(PaymentScheduleReqRegisterType.REQUEST_RESTRUCTURING.getCode().equals( intPayScheRequestValidate.getRegistryType() )){
				argObj[1]=PropertiesUtilities.getMessage(PropertiesConstants.PAYMENT_CRON_LBL_RESTRUCTURATION, null);
			}
			if(intPayScheRequestValidate.getRequestState().equals( RequestStateType.REGISTERED.getCode() )){
				argObj[2]=RequestStateType.REGISTERED.getValue();
			}else if(intPayScheRequestValidate.getRequestState().equals( RequestStateType.APPROVED.getCode() )){
				argObj[2]=RequestStateType.APPROVED.getValue();
			}
			argObj[3]=intPayScheRequestValidate.getIdIntScheduleReqHisPk();

			throw new ServiceException(ErrorServiceType.PAYMENT_CRONOGRAM_ERROR_INT_REQ_PENDING,argObj);
		}
	}
	
	/**
	 * Metodo para validar si AmoPaymentScheduleReqHi ya se encuentra registrado
	 *
	 * @param amoPaymentScheduleReqHi the amo payment schedule req hi
	 * @param security the security
	 * @throws ServiceException the service exception
	 */
	public void validateAmoPaymentScheRequestAlreadyRegisteredServiceBean(AmoPaymentScheduleReqHi amoPaymentScheduleReqHi, Security security) throws ServiceException{
		Object[] argObj=new Object[4];
		argObj[0]=security.getIdSecurityCodePk();

		Object[] requestArg = {RequestStateType.REGISTERED.getCode(),RequestStateType.APPROVED.getCode()}; 
		AmoPaymentScheduleReqHi amoPayScheRequestValidate=validateAmoPaymentScheduleReqHiServiceBean(amoPaymentScheduleReqHi.getIdAmoScheduleReqHisPk(), 
																									security.getIdSecurityCodePk(), 
																									null, 
																									requestArg);
		if(amoPayScheRequestValidate!=null){
			if(PaymentScheduleReqRegisterType.REQUEST_MODIFICATION.getCode().equals( amoPayScheRequestValidate.getRegistryType() )){
				argObj[1]=PropertiesUtilities.getMessage(PropertiesConstants.PAYMENT_CRON_LBL_MODIFICATION, null);
			}else if(PaymentScheduleReqRegisterType.REQUEST_RESTRUCTURING.getCode().equals( amoPayScheRequestValidate.getRegistryType() )){
				argObj[1]=PropertiesUtilities.getMessage(PropertiesConstants.PAYMENT_CRON_LBL_RESTRUCTURATION, null);
			}
			if(amoPayScheRequestValidate.getRequestState().equals( RequestStateType.REGISTERED.getCode() )){
				argObj[2]=RequestStateType.REGISTERED.getValue();
			}else if(amoPayScheRequestValidate.getRequestState().equals( RequestStateType.APPROVED.getCode() )){
				argObj[2]=RequestStateType.APPROVED.getValue();
			}
			argObj[3]=String.valueOf(amoPayScheRequestValidate.getIdAmoScheduleReqHisPk());

			throw new ServiceException(ErrorServiceType.PAYMENT_CRONOGRAM_ERROR_AMO_REQ_PENDING,argObj);
		}		
		
//		//calcula el total de las amortizaciones
//		BigDecimal totalAmortFactor = BigDecimal.ZERO;		
//		for(ProgramAmoCouponReqHi coupon :amoPaymentScheduleReqHi.getProgramAmoCouponReqHis()){
//			totalAmortFactor = totalAmortFactor.add(coupon.getAmortizationFactor());
//		}
//		
//		if(amoPaymentScheduleReqHi!=null && amoPaymentScheduleReqHi.getProgramAmoCouponReqHis()!=null && !amoPaymentScheduleReqHi.getProgramAmoCouponReqHis().isEmpty()){
//			if(!(CommonsUtilities.getPercentOnDecimals(totalAmortFactor).intValue() == BigDecimal.ONE.intValue())){ 
//				System.out.println("++++++++++TOTAL AMORT FACTOR "+totalAmortFactor.toString());
//				throw new ServiceException(ErrorServiceType.PAYMENT_CRONOGRAM_FACTAMORT_INCOMPLETE,argObj);
//				
//			}
//		}	
		
	}
	
	public Boolean verifyDefinitiveStateCorporativeOperation(Security security) {
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append(" Select co From CorporativeOperation co ");
		sbQuery.append(" where co.securities.idSecurityCodePk = :idSecurityCodePk");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSecurityCodePk", security.getIdSecurityCodePk());
		
		List<CorporativeOperation> result = query.getResultList();
		
		return result.stream().anyMatch(operation -> operation.getState().equals(CorporateProcessStateType.DEFINITIVE.getCode()));
				
	}
	
} 