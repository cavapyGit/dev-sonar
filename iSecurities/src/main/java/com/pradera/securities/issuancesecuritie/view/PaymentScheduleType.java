package com.pradera.securities.issuancesecuritie.view;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Enum PaymentScheduleType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public enum PaymentScheduleType {
    
	/** The txt cron interest rate. */
	TXT_CRON_INTEREST_RATE("frmSecurityMgmt:tviewGeneral:txtCronInterestRate",null),
	
	/** The txt cron int spread escalon. */
	TXT_CRON_INT_SPREAD_ESCALON("frmSecurityMgmt:tviewGeneral:txtCronIntSpreadEscalon",null),
	
	/** The txt cron minium rate. */
	TXT_CRON_MINIUM_RATE("frmSecurityMgmt:tviewGeneral:txtCronMiniumRate",null),
	
	/** The txt cron maxium rate. */
	TXT_CRON_MAXIUM_RATE("frmSecurityMgmt:tviewGeneral:txtCronMaxiumRate",null),
	
	/** The txt cron rate value. */
	TXT_CRON_RATE_VALUE("frmSecurityMgmt:tviewGeneral:txtCronRateValue",null),
	
	/** The txt cron payments number. */
	TXT_CRON_PAYMENTS_NUMBER("frmSecurityMgmt:tviewGeneral:txtCronPaymentsNumber",null),
	
	/** The txt cron amortization factor. */
	TXT_CRON_AMORTIZATION_FACTOR("frmSecurityMgmt:tviewGeneral:txtCronAmortizationFactor",null),
	
	/** The txt cron periodicity days. */
	TXT_CRON_PERIODICITY_DAYS("frmSecurityMgmt:tviewGeneral:txtCronAmortPeriodicityDays",null),
	
	/** The txt search request number. */
	TXT_SEARCH_REQUEST_NUMBER("frmPaymentScheSearch:txtRequestNumber",null),
	
	/** The txt search isin code. */
	TXT_SEARCH_ISIN_CODE("frmPaymentScheSearch:txtSecurityIsinCode",null),
	
	/** The txt search cbo security class. */
	TXT_SEARCH_CBO_SECURITY_CLASS("frmPaymentScheSearch:cboSecurityClass",null),
	
	/** The cal first exp payment. */
	CAL_FIRST_EXP_PAYMENT("frmSecurityMgmt:tviewGeneral:calFirstExpAmoCoupon",null),
	
	/** The cbo cron operation type. */
	CBO_CRON_OPERATION_TYPE("frmSecurityMgmt:tviewGeneral:cboCronOperationType",null),
	
	/** The cbo cron rate type. */
	CBO_CRON_RATE_TYPE("frmSecurityMgmt:tviewGeneral:cboCronRateType",null),
	
	/** The cbo cron maxium rate. */
	CBO_CRON_MAXIUM_RATE("frmSecurityMgmt:tviewGeneral:cboCronCalendarDay",null),
	
	/** The cbo cron calendar type. */
	CBO_CRON_CALENDAR_TYPE("frmSecurityMgmt:tviewGeneral:cboCronCalendarType",null),
	
	/** The cbo cron periodicity amort. */
	CBO_CRON_PERIODICITY_AMORT("frmSecurityMgmt:tviewGeneral:cboCronPerAmortization",null),
	
	/** The cbo cron periodicity days. */
	CBO_CRON_PERIODICITY_DAYS("frmSecurityMgmt:tviewGeneral:txtCronAmortPeriodicityDays",null);
	
	/** The value. */
	private String value;
	
	/** The label. */
	private String label;
	
	/**
	 * Instantiates a new payment schedule type.
	 *
	 * @param value the value
	 * @param label the label
	 */
	private PaymentScheduleType(String value, String label) {
		this.value=value;
		this.label=label;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue(){
		return this.value;
	}
	
	/**
	 * Gets the label.
	 *
	 * @return the label
	 */
	public String getLabel(){
		return this.label;
	}
}
