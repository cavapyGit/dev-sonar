package com.pradera.securities.issuancesecuritie.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.securities.issuancesecuritie.to.SecurityRemovedModelTO;

public class SecurityRemovedDataModel extends ListDataModel<SecurityRemovedModelTO> implements SelectableDataModel<SecurityRemovedModelTO>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	 // primer constructor
	public SecurityRemovedDataModel(){}
	
	// segundo constructor
	public SecurityRemovedDataModel(List<SecurityRemovedModelTO> lst){
		super(lst);
	}

	@Override
	public SecurityRemovedModelTO getRowData(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getRowKey(SecurityRemovedModelTO arg0) {
		// TODO Auto-generated method stub
		return null;
	} 

}
