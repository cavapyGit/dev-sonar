package com.pradera.securities.issuancesecuritie.service;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.SecuritySendBbv;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.securities.issuancesecuritie.to.CuponTmpEdvTO;
import com.pradera.securities.issuancesecuritie.to.EmisionTmpEdvTO;
import com.pradera.securities.issuancesecuritie.util.CouponUtilXml;
import com.pradera.securities.issuancesecuritie.util.SecurityUtilXml;
import com.itextpdf.text.log.SysoLogger;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary. Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class ProcessSecurityBbvServiceBean.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 05/02/2014
 */
@Stateless
//@TransactionManagement(TransactionManagementType.CONTAINER)
public class ProcessSecurityBbvServiceBean extends CrudDaoServiceBean {

	@Inject
	private PraderaLogger praderaLogger;

	@Resource(name = "java:jboss/datasources/iDepositaryBBVDS")
	javax.sql.DataSource dataSourceBBV;

//	@PersistenceContext(unitName = "iDepositaryBBVDS")
//	EntityManager embbv;

	/**
	 * Funcion para listar depf que ingresaron en un fecha
	 * 
	 * @param today
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	public List<Security> getSecurityAvilable(Date today)
			throws ServiceException {

		StringBuilder builder = new StringBuilder();
		List<Integer> listSearchSecurity = new ArrayList<>();
		listSearchSecurity.add(420);
		listSearchSecurity.add(1954);
		
		List<Security> listSecurity = new ArrayList<>();

		builder.append(" select s.idSecurityCodePk, s.issuanceDate, s.expirationDate, s.numberCoupons ");
		builder.append(" from Security s ");
		builder.append(" where trunc(s.registryDate) = :today ");
		builder.append(" and s.securityClass in :securityClass ");
		Query queryString = em.createQuery(builder.toString());
		queryString.setParameter("today", today);
		queryString.setParameter("securityClass", listSearchSecurity);

		List<Object[]> listBussProcess = (List<Object[]>) queryString.getResultList();

		try {
			Security security;
			for (Object[] lista : listBussProcess) {
				security = new Security();

				security.setIdSecurityCodePk(String.valueOf(lista[0]));
				security.setIssuanceDate((Date) lista[1]);
				security.setExpirationDate((Date) lista[2]);
				security.setNumberCoupons((Integer) lista[3]);
				listSecurity.add(security);
			}

		} catch (Exception e) {
			praderaLogger.error(e.getMessage());
		}
		return listSecurity;
	}

	/**
	 * Funcion para listar los dps que fueron enviados a la bbv
	 * 
	 * @param today
	 * @param securitySend
	 * @param numberProcess
	 * @return List<SecuritySendBbv> listSecuritySend
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	public List<SecuritySendBbv> getSecurityAvilableSended(Date today,
			String securitySend, Integer numberProcess) throws ServiceException {
		StringBuilder builder = new StringBuilder();
		builder.append(" select s ");
		builder.append(" from SecuritySendBbv s ");
		builder.append(" where trunc(s.registryDate) = :today ");
		if (securitySend != null) {
			builder.append(" and s.sendState = :sendState ");
		}
		if (numberProcess != null) {
			builder.append(" and s.numberProcess = :numberProcess ");
		}

		Query queryString = em.createQuery(builder.toString());
		queryString.setParameter("today", today);
		if (securitySend != null) {
			queryString.setParameter("sendState", securitySend);
		}
		if (numberProcess != null) {
			queryString.setParameter("numberProcess", numberProcess);
		}

		// List<Object[]> listBussProcess = (List<Object[]>)
		// queryString.getResultList();
		List<SecuritySendBbv> listSecuritySend = new ArrayList<>();
		try {
			listSecuritySend = (List<SecuritySendBbv>) queryString.getResultList();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return listSecuritySend;
	}

	public void saveListSecurity(List<SecuritySendBbv> listSecuritySendBbv)
			throws ServiceException {
		saveAll(listSecuritySendBbv);
	}

	/**
	 * Funcion para devolver el correlativo de envio
	 * 
	 * @return Integer numberProcess
	 * @throws ServiceException
	 */
	public Integer getNumberProcess(Date today) throws ServiceException {

		Integer numberProcess;

		StringBuilder builder = new StringBuilder();
		builder.append(" select max(s.numberProcess) ");
		builder.append(" from SecuritySendBbv s ");
		builder.append(" where trunc(s.registryDate) = :today ");
		Query queryString = em.createQuery(builder.toString());
		queryString.setParameter("today", today);

		Integer result = (Integer) queryString.getSingleResult();
		if (result != null) {
			numberProcess = result + 1;
		} else {
			numberProcess = 1;
		}

		return numberProcess;
	}

	/**
	 * Funcion para devolver valores enviados
	 * 
	 * @param today
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	public List<Integer> getListNumberProcess(Date today)
			throws ServiceException {

		StringBuilder builder = new StringBuilder();
		List<Integer> result = new ArrayList<>();

		builder.append(" select DISTINCT(s.numberProcess) ");
		builder.append(" from SecuritySendBbv s ");
		builder.append(" where trunc(s.registryDate) = :today ");
		builder.append(" order by s.numberProcess desc ");
		Query queryString = em.createQuery(builder.toString());
		queryString.setParameter("today", today);

		try {
			result = (List<Integer>) queryString.getResultList();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}

	/**
	 * Registro de datos en la tabla de la bbv
	 * 
	 * @param idSecurityCode
	 * @throws ServiceException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void sendSecuritiesToBBV(Object[] security,List<Object[]> listCupon)	throws ServiceException {
		
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		StringBuilder insertSecurity = new StringBuilder();
		insertSecurity.append(" INSERT  INTO EMISION_TMP ( ");
		insertSecurity.append(" EMN_SERIE, ");
		insertSecurity.append(" EMN_CODINST, ");
		insertSecurity.append(" EMN_CODEM, ");
		insertSecurity.append(" EMN_CODEMISION, ");
		insertSecurity.append(" EMN_MONEDA_GEN, ");
		insertSecurity.append(" EMN_MONEDA, ");
		insertSecurity.append(" EMN_METODO_VALUACION, ");
		insertSecurity.append(" EMN_FECHA_EMISION, ");
		insertSecurity.append(" EMN_PLAZO, ");
		insertSecurity.append(" EMN_FECHA_VENCI, ");
		insertSecurity.append(" EMN_TREM, ");
		insertSecurity.append(" EMN_TASA_TXT, ");
		insertSecurity.append(" EMN_TDESC, ");
		insertSecurity.append(" EMN_PLAZO_AMORTIZA, ");
		insertSecurity.append(" EMN_NRO_CUPONES, ");
		insertSecurity.append(" EMN_VALOR_EMISION, ");
		insertSecurity.append(" EMN_MONTO_AJUSTE, ");
		insertSecurity.append(" EMN_VN, ");
		insertSecurity.append(" EMN_AGENTE_REGISTRADOR, ");
		insertSecurity.append(" EMN_TIPOACCION, ");
		insertSecurity.append(" EMN_STATUS, ");
		insertSecurity.append(" EMN_FECHA_BV, ");
		insertSecurity.append(" EMN_DSC, ");
		insertSecurity.append(" EMN_DSM, ");
		insertSecurity.append(" EMN_DIASREDIMIDOS, ");
		insertSecurity.append(" EMN_TIPOREGISTRO_MESA, ");
		insertSecurity.append(" EMN_TIPOGENERICOVALOR, ");
		insertSecurity.append(" EMN_LCUP_NEGOCIABLE, ");
		insertSecurity.append(" EMN_LSUBPRODUCTO, ");
		insertSecurity.append(" EMN_EDV, ");
		insertSecurity.append(" EMN_LFCI_NEG, ");
		insertSecurity.append(" EMN_PREPAGO, ");
		insertSecurity.append(" EMN_LSUBOR, ");
		insertSecurity.append(" EMN_LTASAFIJA, ");
		insertSecurity.append(" EMN_TIPOBONO, ");
		insertSecurity.append(" EMN_SERIEACC, ");
		insertSecurity.append(" EMN_LTASA_DIF, ");
		insertSecurity.append(" EMN_TIR, ");
		insertSecurity.append(" EMN_LHABILITADO, ");
		insertSecurity.append(" EMN_FEAJUSTE_BCB, ");
		insertSecurity.append(" EMN_ESTADO, ");
		insertSecurity.append(" EMN_FECHA_ENVIO, ");
		insertSecurity.append(" EMN_NEGOCIABLE ");
		insertSecurity.append(" ) ");
		
		try {
			//Insertamos el valor
			insertSecurity.append(" values(");
			insertSecurity.append("'"+security[0]+"',");//EMN_SERIE
			insertSecurity.append("'"+security[1]+"',");//EMN_CODINST
			insertSecurity.append("'"+security[2]+"',");//EMN_CODEM
			insertSecurity.append("null,");		   //EMN_CODEMISION
			insertSecurity.append("'"+security[3]+"',");//EMN_MONEDA_GEN
			insertSecurity.append("'"+security[4]+"',");//EMN_MONEDA
			insertSecurity.append("'"+security[5]+"',");//EMN_METODO_VALUACION
			insertSecurity.append("to_date('"+security[6]+"','dd/MM/yyyy'),");//EMN_FECHA_EMISION
			insertSecurity.append("'"+security[7]+"',");//EMN_PLAZO
			insertSecurity.append("to_date('"+security[8]+"','dd/MM/yyyy'),");//EMN_FECHA_VENCI
			insertSecurity.append(security[9]+",");//EMN_TREM
			insertSecurity.append(security[10]+",");//EMN_TASA_TXT
			insertSecurity.append(security[11]+",");//EMN_TDESC
			insertSecurity.append(security[12]+",");//EMN_PLAZO_AMORTIZA
			insertSecurity.append(security[13]+",");//EMN_NRO_CUPONES
			insertSecurity.append(security[14]+",");//EMN_VALOR_EMISION
			insertSecurity.append(security[15]+",");//EMN_MONTO_AJUSTE
			insertSecurity.append(security[16]+",");//EMN_VN
			insertSecurity.append("'"+security[17]+"',");//EMN_AGENTE_REGISTRADOR
			insertSecurity.append("null,");   		//EMN_TIPOACCION
			insertSecurity.append("'"+security[18]+"',");//EMN_STATUS
			insertSecurity.append("null,");   		//EMN_FECHA_BV
			insertSecurity.append("'"+security[19]+"',");//EMN_DSC
			insertSecurity.append("null,");   		//EMN_DSM
			insertSecurity.append(security[20]+",");//EMN_DIASREDIMIDOS
			insertSecurity.append("'"+security[21]+"',");//EMN_TIPOREGISTRO_MESA
			insertSecurity.append(security[22]+",");//EMN_TIPOGENERICOVALOR
			insertSecurity.append("'"+security[23]+"',");//EMN_LCUP_NEGOCIABLE
			insertSecurity.append("'"+security[24]+"',");//EMN_LSUBPRODUCTO
			insertSecurity.append("'"+security[25]+"',");//EMN_EDV
			insertSecurity.append("'"+security[26]+"',");//EMN_LFCI_NEG
			insertSecurity.append("'"+security[27]+"',");//EMN_PREPAGO
			insertSecurity.append("'"+security[28]+"',");//EMN_LSUBOR
			insertSecurity.append("'"+security[29]+"',");//EMN_LTASAFIJA
			insertSecurity.append("'"+security[30]+"',");//EMN_TIPOBONO
			insertSecurity.append("null,");			//EMN_SERIEACC
			insertSecurity.append("'"+security[31]+"',");//EMN_LTASA_DIF
			insertSecurity.append("null,");			//EMN_TIR
			insertSecurity.append("'"+security[32]+"',");//EMN_LHABILITADO
			insertSecurity.append("null,");			//EMN_FEAJUSTE_BCB
			insertSecurity.append("'"+security[33]+"',");//EMN_ESTADO
			
			// Esta fecha debe ser revisada. si enviar la fecha de registro de sistema o fecha de proceso
			//insertSecurity.append("to_date('"+convertirDate(new Date())+"','dd/MM/yyyy'),");//EMN_FECHA_ENVIO
			insertSecurity.append("TO_DATE(TO_CHAR(sysdate, 'dd/MM/yyyy hh24:mi:ss'), 'dd/MM/yyyy hh24:mi:ss'),");//EMN_FECHA_ENVIO
			insertSecurity.append("'"+security[35]+"'");    //EMN_NEGOCIABLE
			insertSecurity.append(" )");
			
//			Query queryInsertSecurity = embbv.createNativeQuery(insertSecurity.toString());
//			queryInsertSecurity.executeUpdate();
			connection = dataSourceBBV.getConnection();
			preparedStatement = connection.prepareStatement(insertSecurity.toString());
			preparedStatement.execute();
			
			//verificamos si existen mas de un cupon
			if (listCupon.size()>1){
				StringBuilder insertCupon;
				for (Object[] object : listCupon){
					insertCupon = new StringBuilder();
					
					insertCupon.append(" INSERT INTO CUPON_TMP ");
					insertCupon.append(" (CUP_SERIE,CUP_FECHA_VENCI,CUP_AMORTIZAK,CUP_DSC,CUP_DSM,CUP_NRO_CUPON,CUP_INST,CUP_TASA) ");
					insertCupon.append(" values(");
					insertCupon.append("'"+object[0]+"',");//CUP_SERIE
					insertCupon.append("to_date('"+object[1]+"','dd/MM/yyyy'),");//CUP_FECHA_VENCI
					insertCupon.append(object[2]+",");//CUP_AMORTIZAK
					insertCupon.append("'"+object[3]+"',");//CUP_DSC
					insertCupon.append("'"+object[4]+"',");//CUP_DSM
					insertCupon.append(object[5]+",");//CUP_NRO_CUPON
					insertCupon.append("'"+object[6]+"',");//CUP_INST
					insertCupon.append(object[7]);//CUP_TASA
					insertCupon.append(" )");
					
//					Query queryInsertCupon = embbv.createNativeQuery(insertCupon.toString());
//					queryInsertCupon.executeUpdate();
					
//					connection = dataSourceBBV.getConnection();
					preparedStatement = connection.prepareStatement(insertCupon.toString());
					preparedStatement.execute();
					preparedStatement.close();
				}
			}
			
		} catch (SQLException e){
			e.printStackTrace();
		} 
		catch (Exception e) {
			e.printStackTrace();
			throw new EJBException(e);
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}
	public Date convertStringToDate(String dateString){
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss"); 
		Date date = null;
		try {
			date = dt.parse(dateString);
		} catch (ParseException e){
			e.printStackTrace();
		} 
        return date;
	}
	public Object[] getSecurityToSend(String idSecurityCode) throws ServiceException {
		
		StringBuilder selectSecurity = new StringBuilder();
		selectSecurity.append(" SELECT  id_security_code_only  EMN_SERIE, ");
		selectSecurity.append(" (case when security_class = 420 then 'DPF'  when  security_class = 1954 then 'PGS' END) EMN_CODINST, ");
		selectSecurity.append("(SELECT MNEMONIC FROM IDEPOSITARY.ISSUER WHERE ID_ISSUER_PK=ID_ISSUER_FK) EMN_CODEM, ");
		selectSecurity.append(" SUBSTR (id_security_code_only,4,1) EMN_MONEDA_GEN, ");
		selectSecurity.append(" DECODE(CURRENCY,1304,'ECU',1734,'UFV',127,'BOB',430,'USD',1853,'DMV') EMN_MONEDA, ");
		selectSecurity.append(" (CASE WHEN NUMBER_COUPONS > 1 THEN 'CUP' ELSE 'IAV' END) EMN_METODO_VALUACION, ");
		selectSecurity.append(" TO_CHAR (ISSUANCE_DATE,'DD/MM/YYYY') EMN_FECHA_EMISION, ");
		selectSecurity.append(" (EXPIRATION_DATE-ISSUANCE_DATE) EMN_PLAZO, ");
		selectSecurity.append(" TO_CHAR (EXPIRATION_DATE,'DD/MM/YYYY') EMN_FECHA_VENCI, ");
		selectSecurity.append(" INTEREST_RATE EMN_TREM,  null EMN_TASA_TXT, ");
		selectSecurity.append(" null EMN_TDESC, ");
		selectSecurity.append(" NVL(DECODE(number_coupons, 1, 0,");
		selectSecurity.append(" PERIODICITY_DAYS),0) EMN_PLAZO_AMORTIZA, ");
		selectSecurity.append(" (CASE WHEN NUMBER_COUPONS > 1 THEN NUMBER_COUPONS ELSE 0 END) EMN_NRO_CUPONES, ");
		selectSecurity.append(" INITIAL_NOMINAL_VALUE EMN_VALOR_EMISION,  ");
		selectSecurity.append(" 0 EMN_MONTO_AJUSTE,  INITIAL_NOMINAL_VALUE EMN_VN, ");
		selectSecurity.append(" SUBSTR (id_security_code_only,1,3) EMN_AGENTE_REGISTRADOR,  ");
		selectSecurity.append(" (CASE WHEN STATE_SECURITY =2033 THEN 'VE'  WHEN STATE_SECURITY =1372 THEN 'VE'  WHEN STATE_SECURITY =641 THEN 'VE'  WHEN STATE_SECURITY = 131 THEN 'VI'  END) EMN_STATUS,  ");
		selectSecurity.append(" 'INSERT-EDV'  EMN_DSC, ");
		selectSecurity.append(" 0 EMN_DIASREDIMIDOS,  ");
		selectSecurity.append(" 'N' EMN_TIPOREGISTRO_MESA,  ");
		selectSecurity.append(" (CASE WHEN NUMBER_COUPONS > 1 THEN 7 ELSE 6 END) EMN_TIPOGENERICOVALOR, ");
		selectSecurity.append(" 'F' EMN_LCUP_NEGOCIABLE,   ");
		selectSecurity.append(" 'F' EMN_LSUBPRODUCTO,   ");
		selectSecurity.append(" 'T' EMN_EDV, ");
		selectSecurity.append(" 'F' EMN_LFCI_NEG, ");
		selectSecurity.append(" 'F' EMN_PREPAGO, ");
		selectSecurity.append(" 'F' EMN_LSUBOR, ");
		selectSecurity.append(" 'T' EMN_LTASAFIJA, ");
		selectSecurity.append(" 'NN' EMN_TIPOBONO, ");
		selectSecurity.append(" 'F' EMN_LTASA_DIF, ");
		selectSecurity.append(" 'T' EMN_LHABILITADO, ");
		selectSecurity.append(" 'CAP' EMN_ESTADO,  ");
		selectSecurity.append(" SYSDATE EMN_FECHA_ENVIO,  ");
		selectSecurity.append(" (case when not_traded=0 then 'T' else 'F' end) EMN_NEGOCIABLE ");
		selectSecurity.append(" FROM IDEPOSITARY.SECURITY  WHERE  SECURITY_CLASS IN (420, 1954) ");
		selectSecurity.append(" AND ID_SECURITY_CODE_PK = :idSecurityPk ");
		
		Query queryString = em.createNativeQuery(selectSecurity.toString());
		queryString.setParameter("idSecurityPk", idSecurityCode);
		return (Object[]) queryString.getSingleResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getCuponToSend(String idSecurityCode) throws ServiceException {
		
		StringBuilder existCupon = new StringBuilder();
		existCupon.append(" SELECT  SUBSTR(B.ID_SECURITY_CODE_FK,5) CUP_SERIE,  ");
		existCupon.append(" TO_CHAR(EXPERITATION_DATE,'DD/MM/YYYY') CUP_FECHA_VENCI, "); 
		existCupon.append(" (SELECT (case when( a.EXPERITATION_DATE= c.EXPRITATION_DATE)then C.COUPON_AMOUNT else 0 end)  ");
		existCupon.append(" FROM IDEPOSITARY.PROGRAM_AMORTIZATION_COUPON C ,IDEPOSITARY.AMORTIZATION_PAYMENT_SCHEDULE D   ");
		existCupon.append(" WHERE D.ID_AMO_PAYMENT_SCHEDULE_PK = C.ID_AMO_PAYMENT_SCHEDULE_FK  ");
		existCupon.append(" AND  B.ID_SECURITY_CODE_FK=D.ID_SECURITY_CODE_FK  ");
		existCupon.append(" ) CUP_AMORTIZAK,  ");
		existCupon.append(" 'INSERT-EDV' CUP_DSC,NULL CUP_DSM,  ");
		existCupon.append(" A.COUPON_NUMBER CUP_NRO_CUPON,   ");
		existCupon.append(" SUBSTR(B.ID_SECURITY_CODE_FK,1,3) CUP_INST,  ");
		existCupon.append(" NVL(A.INTEREST_RATE,0) CUP_TASA  ");
		existCupon.append(" FROM IDEPOSITARY.PROGRAM_INTEREST_COUPON A ,IDEPOSITARY.INTEREST_PAYMENT_SCHEDULE B  ");
		existCupon.append(" WHERE ID_INT_PAYMENT_SCHEDULE_FK =ID_INT_PAYMENT_SCHEDULE_PK  ");
		existCupon.append(" AND SUBSTR(B.ID_SECURITY_CODE_FK,1,3)='DPF'  ");
		existCupon.append(" AND  ID_SECURITY_CODE_FK = :idSecurityCodePk  ");
		
		Query queryExistCupon = em.createNativeQuery(existCupon.toString());
		queryExistCupon.setParameter("idSecurityCodePk", idSecurityCode);
		List<Object[]> listObject = queryExistCupon.getResultList();
		return listObject;
	}

	/**
	 * Funcion para devolver los valores enviados a la bbv
	 * 
	 * @param numberProcess
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	public List<SecuritySendBbv> getSecuritySended(Integer numberProcess,Date registryDate)
			throws ServiceException {
		StringBuilder builder = new StringBuilder();

		builder.append(" select s ");
		builder.append(" from SecuritySendBbv s ");
		builder.append(" where s.numberProcess = :numberProces ");
		builder.append(" and s.registryDate = :registryDate ");

		Query queryString = em.createQuery(builder.toString());
		queryString.setParameter("numberProces", numberProcess);
		queryString.setParameter("registryDate", registryDate);

		List<SecuritySendBbv> listSecuritySend = new ArrayList<>();
		listSecuritySend = (List<SecuritySendBbv>) queryString.getResultList();
		return listSecuritySend;
	}
	public List<String> getSecuritySendedSerie(Integer numberProcess,Date registryDate)
			throws ServiceException {
		StringBuilder builder = new StringBuilder();

		builder.append(" select s.security ");
		builder.append(" from SecuritySendBbv s ");
		builder.append(" where s.numberProcess = :numberProces ");
		builder.append(" and s.registryDate = :registryDate ");

		Query queryString = em.createQuery(builder.toString());
		queryString.setParameter("numberProces", numberProcess);
		queryString.setParameter("registryDate", registryDate);

		List<String> listSecuritySend = new ArrayList<>();
		listSecuritySend = (List<String>) queryString.getResultList();
		return listSecuritySend;
	}
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean isRegisteredSecurity(String serie){
		Connection conn = null;
		PreparedStatement prep = null;
		try {
			conn = dataSourceBBV.getConnection();
			//String query="SELECT (case when (count(EMN_SERIE) >= 1) then 'true' else 'false' end) as existe FROM emision_tmp WHERE EMN_SERIE ='BISE11565909' and trunc(EMN_FECHA_ENVIO) =TO_DATE('"+convertirFecha(fecha)+"','dd/MM/yyyy')";prep = conn.prepareStatement(query);
			String query="SELECT (case when (count(EMN_SERIE) >= 1) then 'true' else 'false' end) as existe FROM emision_tmp WHERE EMN_SERIE ='"+serie+"'";prep = conn.prepareStatement(query);
			ResultSet resultSet=prep.executeQuery();
			boolean res=false;
			if(resultSet.next()){
				res=Boolean.parseBoolean(resultSet.getString("existe"));
			}
			prep.close();
			conn.close();
			return res;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	} 
	public Document getDocumentXmlSecurity(Integer numberProcess,Date fechaCalendar) throws ParseException{
		List<SecuritySendBbv> listSecuritySended = null;
		try {
			listSecuritySended = this.getSecuritySended(numberProcess, fechaCalendar);
		} catch (ServiceException e1) {
			e1.printStackTrace();
		}
		List<EmisionTmpEdvTO> edvs = new ArrayList<>();
		EmisionTmpEdvTO edv;
		for (SecuritySendBbv bbv : listSecuritySended) {
			edv=getEmisionTmpEdv(bbv.getSecurity());
			if(edv!=null)
				edvs.add(edv);
		}
		try{
			SecurityUtilXml securityUtilXml=new SecurityUtilXml();
			
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            DOMImplementation implementation = builder.getDOMImplementation();
            Document document = implementation.createDocument(null,securityUtilXml.NAME_ROOT_XML, null);
            document.setXmlVersion("1.0");
            Element raiz = document.getDocumentElement();
            Element nodo;
            Element item;
            for (EmisionTmpEdvTO emisionTmpEdv : edvs) {
            	nodo = document.createElement(securityUtilXml.NAME_NODE_ELEMENT);
            	
            	item = document.createElement(securityUtilXml.EMN_SERIE);
            	item.setTextContent(emisionTmpEdv.getEmnSerie());     
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_CODINST);
            	item.setTextContent(emisionTmpEdv.getEmnCodinst());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_CODEM);
            	item.setTextContent(emisionTmpEdv.getEmnCodem());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_MONEDA_GEN);
            	item.setTextContent(emisionTmpEdv.getEmnMonedaGen());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_MONEDA);
            	item.setTextContent(emisionTmpEdv.getEmnMoneda());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_METODO_VALUACION);
            	item.setTextContent(emisionTmpEdv.getEmnMetodoValuacion());  
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_FECHA_EMISION);
            	item.setTextContent(CommonsUtilities.convertDateToString(emisionTmpEdv.getEmnFechaEmision(),CommonsUtilities.DATE_PATTERN));
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_PLAZO);
            	item.setTextContent(emisionTmpEdv.getEmnPlazo());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_FECHA_VENCI);
            	item.setTextContent(CommonsUtilities.convertDateToString(emisionTmpEdv.getEmnFechaVenci(),CommonsUtilities.DATE_PATTERN));
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_TREM);
            	item.setTextContent(emisionTmpEdv.getEmnTrem().toString());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_TASA_TXT);
            	item.setTextContent(emisionTmpEdv.getEmnTasaTxt());  
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_TDESC);
            	item.setTextContent(emisionTmpEdv.getEmnTdesc().toString());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_PLAZO_AMORTIZA);
            	item.setTextContent(emisionTmpEdv.getEmnPlazoAmortiza());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_NRO_CUPONES);
            	item.setTextContent(emisionTmpEdv.getEmnNroCupones().toString());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_VALOR_EMISION);
            	item.setTextContent(emisionTmpEdv.getEmnValorEmision().toString());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_MONTO_AJUSTE);
            	item.setTextContent(emisionTmpEdv.getEmnMontoAjuste().toString());   
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_VN);
            	item.setTextContent(emisionTmpEdv.getEmnVn().toString());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_AGENTE_REGISTRADOR);
            	item.setTextContent(emisionTmpEdv.getEmnAgenteRegistrador());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_TIPOACCION);
            	item.setTextContent(emisionTmpEdv.getEmnTipoaccion());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_STATUS);
            	item.setTextContent(emisionTmpEdv.getEmnStatus());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_FECHA_BV);
            	item.setTextContent(emisionTmpEdv.getEmnFechaBv()!=null?CommonsUtilities.convertDateToString(emisionTmpEdv.getEmnFechaBv(),CommonsUtilities.DATE_PATTERN):"");
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_DSC);
            	item.setTextContent(emisionTmpEdv.getEmnDsc());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_DSM);
            	item.setTextContent(emisionTmpEdv.getEmnDsm());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_DIASREDIMIDOS);
            	item.setTextContent(emisionTmpEdv.getEmnDiasredimidos().toString());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_TIPOREGISTRO_MESA);
            	item.setTextContent(emisionTmpEdv.getEmnTiporegistroMesa());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_TIPOGENERICOVALOR);
            	item.setTextContent(emisionTmpEdv.getEmnTipogenericovalor().toString());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_LCUP_NEGOCIABLE);
            	item.setTextContent(emisionTmpEdv.getEmnLcupNegociable());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_LSUBPRODUCTO);
            	item.setTextContent(emisionTmpEdv.getEmnLsubproducto());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_EDV);
            	item.setTextContent(emisionTmpEdv.getEmnEdv());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_LFCI_NEG);
            	item.setTextContent(emisionTmpEdv.getEmnLfciNeg());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_PREPAGO);
            	item.setTextContent(emisionTmpEdv.getEmnSerie());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_LSUBOR);
            	item.setTextContent(emisionTmpEdv.getEmnLsubor());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_LTASAFIJA);
            	item.setTextContent(emisionTmpEdv.getEmnLtasafija());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_TIPOBONO);
            	item.setTextContent(emisionTmpEdv.getEmnTipobono());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_SERIEACC);
            	item.setTextContent(emisionTmpEdv.getEmnSerieacc());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_LTASA_DIF);
            	item.setTextContent(emisionTmpEdv.getEmnLtasaDif());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_TIR);
            	item.setTextContent(emisionTmpEdv.getEmnTir()!=null?emisionTmpEdv.getEmnTir().toString():"");
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_LHABILITADO);
            	item.setTextContent(emisionTmpEdv.getEmnLhabilitado());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_FEAJUSTE_BCB);
            	item.setTextContent(emisionTmpEdv.getEmnFeajusteBcb()==null?"":CommonsUtilities.convertDateToString(emisionTmpEdv.getEmnFeajusteBcb(),CommonsUtilities.DATE_PATTERN));
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_ESTADO);
            	item.setTextContent(emisionTmpEdv.getEmnEstado());
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_FECHA_ENVIO);
            	item.setTextContent(emisionTmpEdv.getEmnFechaEnvio()==null?"":CommonsUtilities.convertDateToString(emisionTmpEdv.getEmnFechaEnvio(),CommonsUtilities.DATE_PATTERN));
            	nodo.appendChild(item);
            	
            	item = document.createElement(securityUtilXml.EMN_NEGOCIABLE);
            	item.setTextContent(emisionTmpEdv.getEmnNegociable());
            	nodo.appendChild(item);
            	
            	nodo.appendChild(item);
            	raiz.appendChild(nodo);
			}
            return document;
		}catch(NoResultException | ParserConfigurationException ex){
			ex.printStackTrace();
		}
		return null;
	}
	public Document getDocumentXmlCoupon(Integer numberProcess,Date fechaCalendar) throws ParseException{
		List<String> listSecuritySended = null;
		try {
			listSecuritySended = this.getSecuritySendedSerie(numberProcess, fechaCalendar);
		} catch (ServiceException e1) {
			e1.printStackTrace();
		}
		List<CuponTmpEdvTO> cuponTmpEdvs = new ArrayList<>();
		for (String serie : listSecuritySended) {
			List<CuponTmpEdvTO> tmpEdvs=this.getCuponTmpEdv(serie);
			if(tmpEdvs!=null)
				cuponTmpEdvs.addAll(tmpEdvs);
		}
		try{
			CouponUtilXml couponUtilXml=new CouponUtilXml();
			
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            DOMImplementation implementation = builder.getDOMImplementation();
            Document document = implementation.createDocument(null,couponUtilXml.NAME_ROOT_XML, null);
            document.setXmlVersion("1.0");
            Element raiz = document.getDocumentElement();
            Element nodo;
            Element item;
            for (CuponTmpEdvTO cuTmpEdv : cuponTmpEdvs) {
            	nodo = document.createElement(couponUtilXml.NAME_NODE_ELEMENT);
            	
            	item = document.createElement(couponUtilXml.CUP_SERIE);
            	item.setTextContent(cuTmpEdv.getCupSerie());    
            	nodo.appendChild(item);
            	
            	item = document.createElement(couponUtilXml.CUP_FECHA_VENCI);
            	item.setTextContent(CommonsUtilities.convertDateToString(cuTmpEdv.getCupFechaVenci(),CommonsUtilities.DATE_PATTERN));  
            	nodo.appendChild(item);
            	
            	item = document.createElement(couponUtilXml.CUP_AMORTIZAK);
            	item.setTextContent(cuTmpEdv.getCupAmortizak()!=null?cuTmpEdv.getCupAmortizak().toString():"");  
            	nodo.appendChild(item);
            	
            	item = document.createElement(couponUtilXml.CUP_DSC);
            	item.setTextContent(cuTmpEdv.getCupDsc());  
            	nodo.appendChild(item);
            	
            	item = document.createElement(couponUtilXml.CUP_DSM);
            	item.setTextContent(cuTmpEdv.getCupDsm()!=null?cuTmpEdv.getCupDsm().toString():"");  
            	nodo.appendChild(item);

            	item = document.createElement(couponUtilXml.CUP_NRO_CUPON);
            	item.setTextContent(cuTmpEdv.getCupNroCupon().toString());  
            	nodo.appendChild(item);
            	
            	item = document.createElement(couponUtilXml.CUP_INST);
            	item.setTextContent(cuTmpEdv.getCupInst());  
            	nodo.appendChild(item);
            	
            	item = document.createElement(couponUtilXml.CUP_TASA);
            	item.setTextContent(cuTmpEdv.getCupTasa().toString());  
            	nodo.appendChild(item);
            	
            	raiz.appendChild(nodo);
			}
            return document;
		}catch(NoResultException | ParserConfigurationException ex){
			ex.printStackTrace();
		}
		return null;
	}
	public EmisionTmpEdvTO getEmisionTmpEdv(String idSecurityCodePk){
		Object[] security=null;
		try {
			security=this.getSecurityToSend(idSecurityCodePk);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		EmisionTmpEdvTO emisionTmpEdv=new EmisionTmpEdvTO();
		//0.- EMN_SERIE
		emisionTmpEdv.setEmnSerie(security[0]!=null?String.valueOf(security[0]):"");
		// 1.- EMN_CODINST
		emisionTmpEdv.setEmnCodinst(security[1]!=null?String.valueOf(security[1]):"");
		// 2.- EMN_CODEM
		emisionTmpEdv.setEmnCodem(security[2]!=null?String.valueOf(security[2]):"");
		// 3.- EMN_MONEDA_GEN
        emisionTmpEdv.setEmnMonedaGen(security[3]!=null?String.valueOf(security[3]):"");
        // 4 .- EMN_MONEDA
        emisionTmpEdv.setEmnMoneda(security[4]!=null?String.valueOf(security[4]):"");
        // 5.- EMN_METODO_VALUACION
        emisionTmpEdv.setEmnMetodoValuacion(security[5]!=null?String.valueOf(security[5]):"");
        // 6.- EMN_FECHA_EMISION
        emisionTmpEdv.setEmnFechaEmision(CommonsUtilities.convertStringtoDate(security[6].toString()));
        // 7.- EMN_PLAZO        
        emisionTmpEdv.setEmnPlazo(security[7]!=null?String.valueOf(security[7]):"");
        // 8.- EMN_FECHA_VENCI
        //emisionTmpEdv.setEmnFechaVenci(convertStringToDate(security[8].toString()));
        emisionTmpEdv.setEmnFechaVenci(CommonsUtilities.convertStringtoDate(security[8].toString()));
        // 9.- EMN_TREM)
        emisionTmpEdv.setEmnTrem(((BigDecimal)(security[9]==null?BigDecimal.ZERO:security[9])));
        //10.- EMN_TASA_TXT
        emisionTmpEdv.setEmnTasaTxt(security[10]!=null?String.valueOf(security[10]):"");
        // 11.- EMN_TDESC
        emisionTmpEdv.setEmnTdesc(((BigDecimal)(security[11]==null?BigDecimal.ZERO:security[11])));
        // 12.- EMN_PLAZO_AMORTIZA
        emisionTmpEdv.setEmnPlazoAmortiza(security[12]!=null?String.valueOf(security[12]):"");
        // 13.- EMN_NRO_CUPONES
        emisionTmpEdv.setEmnNroCupones(((BigDecimal)(security[13]==null?BigDecimal.ZERO:security[13])).intValue());
        // 14.- EMN_VALOR_EMISION
        emisionTmpEdv.setEmnValorEmision(((BigDecimal)(security[14]==null?BigDecimal.ZERO:security[14])));
        // 15.- EMN_MONTO_AJUSTE
        emisionTmpEdv.setEmnMontoAjuste(((BigDecimal)(security[15]==null?BigDecimal.ZERO:security[15])));
        // 16.- EMN_VN
        emisionTmpEdv.setEmnVn(((BigDecimal)(security[16]==null?BigDecimal.ZERO:security[16])));
        // 17.- EMN_AGENTE_REGISTRADOR
        emisionTmpEdv.setEmnAgenteRegistrador(security[17]!=null?String.valueOf(security[17]):"");
        // 18.- EMN_STATUS
        emisionTmpEdv.setEmnStatus(security[18]!=null?String.valueOf(security[18]):"");
        // 19.- EMN_DSC
        emisionTmpEdv.setEmnDsc(security[19]!=null?String.valueOf(security[19]):"");
        // 20.- EMN_DIASREDIMIDOS
        emisionTmpEdv.setEmnDiasredimidos(((BigDecimal)(security[20]==null?BigDecimal.ZERO:security[20])).shortValue());
        // 21.- EMN_TIPOREGISTRO_MESA
        emisionTmpEdv.setEmnTiporegistroMesa(security[21]!=null?String.valueOf(security[21]):"");
        // 22.- EMN_TIPOGENERICOVALOR 
        emisionTmpEdv.setEmnTipogenericovalor(((BigDecimal)(security[22]==null?BigDecimal.ZERO:security[22])).shortValue());
        // 23.- EMN_LCUP_NEGOCIABLE
        emisionTmpEdv.setEmnLcupNegociable(security[23]!=null?String.valueOf(security[23]):"");
        // 24.- EMN_LSUBPRODUCTO
        emisionTmpEdv.setEmnLsubproducto(security[24]!=null?String.valueOf(security[24]):"");
        // 25.- EMN_EDV
        emisionTmpEdv.setEmnEdv(security[25]!=null?String.valueOf(security[25]):"");
        // 26.- EMN_LFCI_NEG
        emisionTmpEdv.setEmnLfciNeg(security[26]==null?String.valueOf(security[26]):"");
        // 27.- EMN_PREPAGO
        emisionTmpEdv.setEmnPrepago(security[27]!=null?String.valueOf(security[27]):"");
        // 28.- EMN_LSUBOR
        emisionTmpEdv.setEmnLsubor(security[28]!=null?String.valueOf(security[28]):"");
        // 29.- EMN_LTASAFIJA
        emisionTmpEdv.setEmnLtasafija(security[29]!=null?String.valueOf(security[29]):"");
        // 30.- EMN_TIPOBONO
        emisionTmpEdv.setEmnTipobono(security[30]!=null?String.valueOf(security[30]):"");
        // 31.- EMN_LTASA_DIF
        emisionTmpEdv.setEmnLtasaDif(security[31]!=null?String.valueOf(security[31]):"");
        //32 .- EMN_LHABILITADO
        emisionTmpEdv.setEmnLhabilitado(security[32]!=null?String.valueOf(security[32]):"");
        // 33.- EMN_ESTADO
        emisionTmpEdv.setEmnEstado(security[33]!=null?String.valueOf(security[33]):"");
        // 34.- EMN_FECHA_ENVIO
        emisionTmpEdv.setEmnFechaEnvio(convertStringToDate(security[34].toString()));
        // 35.- EMN_NEGOCIABLE
        emisionTmpEdv.setEmnNegociable(security[35]!=null?String.valueOf(security[35]):"");
		return emisionTmpEdv;
	}
	public List<CuponTmpEdvTO> getCuponTmpEdv(String idSecurityCodePk){
		List<Object[]> listCuponObject=null;
		List<CuponTmpEdvTO> listCupon=new ArrayList<>();
		try {
			listCuponObject = this.getCuponToSend(idSecurityCodePk);
			CuponTmpEdvTO cuponTmpEdv=null;
			for (Object[] cupon : listCuponObject) {
				cuponTmpEdv=new CuponTmpEdvTO();
				//0.-CUP_SERIE
				cuponTmpEdv.setCupSerie(cupon[0]!=null?String.valueOf(cupon[0]):"");
				// 1.- CUP_FECHA_VENCI
				cuponTmpEdv.setCupFechaVenci(CommonsUtilities.convertStringtoDate(cupon[1].toString()));
				// 2.- CUP_AMORTIZAK
				cuponTmpEdv.setCupAmortizak(((BigDecimal)(cupon[2]==null?BigDecimal.ZERO:cupon[2])));
				// 3.- CUP_DSC
				cuponTmpEdv.setCupDsc(cupon[3]!=null?String.valueOf(cupon[3]):"");
				// 4.- CUP_DSM
				cuponTmpEdv.setCupDsm(cupon[4]!=null?String.valueOf(cupon[4]):"");
				// 5.- CUP_NRO_CUPON
				cuponTmpEdv.setCupNroCupon(((BigDecimal)(cupon[5]==null?BigDecimal.ZERO:cupon[5])).intValue());
				// 6.- CUP_INST
				cuponTmpEdv.setCupInst(cupon[6]!=null?String.valueOf(cupon[6]):"");
				// 7.- CUP_TASA
				cuponTmpEdv.setCupTasa(((BigDecimal)(cupon[7]==null?BigDecimal.ZERO:cupon[7])));
				listCupon.add(cuponTmpEdv);
			}
		} catch (ServiceException e1) {
			e1.printStackTrace();
		}
		return listCupon;
	}
}
