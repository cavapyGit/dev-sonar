package com.pradera.securities.issuancesecuritie.util;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PropertiesConstants.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class PropertiesConstants {

	/**
	 * Instantiates a new properties constants.
	 */
	private PropertiesConstants() {
		super();
	}
	
	/** The Constant ALT_NEED_ONE_FIELD. */
	public final static String ALT_NEED_ONE_FIELD = "error.required.one.field";
	
	/** The Constant ALT_NEED_ONE_FIELD. */
	public final static String ERROR_SECURITY_CLASS_FAILED = "error.security.class.not.exits";
		
	
	/** The Constant MESSAGES_ALERT. */
	public final static String MESSAGES_ALERT = "messages.alert";
	//General 
	/** The Constant LBL_CONFIRM_HEADER_REGISTER. */
	public final static String LBL_CONFIRM_HEADER_REGISTER="lbl.confirm.header.register";
	
	/** The Constant LBL_CONFIRM_HEADER_MODIFICATION. */
	public final static String LBL_CONFIRM_HEADER_MODIFICATION="lbl.confirm.header.modification";		
	
	/** The Constant LBL_CONFIRM_HEADER_BLOCK. */
	public final static String LBL_CONFIRM_HEADER_BLOCK="lbl.confirm.header.block";		
	
	/** The Constant LBL_CONFIRM_HEADER_UNBLOCK. */
	public final static String LBL_CONFIRM_HEADER_UNBLOCK="lbl.confirm.header.unblock";	
	
	/** The Constant LBL_COUPON_AMOUNT. */
	public final static String LBL_COUPON_AMOUNT="lbl.coupon.amount";
	
	/** The Constant LBL_MOTIVE_BLOCK. */
	public final static String LBL_MOTIVE_BLOCK="lbl.motive.block";
	
	/** The Constant LBL_MOTIVE_UNBLOCK. */
	public final static String LBL_MOTIVE_UNBLOCK="lbl.motive.unblock";
	
	/** The Constant LBL_HEADER_ALERT_REGISTER. */
	public final static String LBL_HEADER_ALERT_REGISTER="lbl.header.alert.register";
	
	/** The Constant LBL_HEADER_ALERT_MODIFY. */
	public final static String LBL_HEADER_ALERT_MODIFY="lbl.header.alert.modify";
	
	/** The Constant LBL_HEADER_ALERT_SUCCESS. */
	public final static String LBL_HEADER_ALERT_SUCCESS="lbl.header.alert.success";
	
	/** The Constant LBL_HEADER_ALERT. */
	public final static String LBL_HEADER_ALERT="lbl.header.alert";
	
	/** The Constant LBL_HEADER_ALERT_ERROR. */
	public final static String LBL_HEADER_ALERT_ERROR="lbl.header.alert.error";
	
	/** The Constant LBL_HEADER_ALERT_APPROVE. */
	public final static String LBL_HEADER_ALERT_APPROVE="lbl.header.alert.approve";
	
	/** The Constant LBL_HEADER_ALERT_ANNUL. */
	public final static String LBL_HEADER_ALERT_ANNUL="lbl.header.alert.annul";
	
	public final static String LBL_HEADER_ALERT_REVIEW="lbl.header.alert.review";	
	
	/** The Constant LBL_HEADER_ALERT_REJECT. */
	public final static String LBL_HEADER_ALERT_REJECT="lbl.header.alert.reject";
	
	/** The Constant LBL_HEADER_ALERT_CONFIRM. */
	public final static String LBL_HEADER_ALERT_CONFIRM="lbl.header.alert.confirm";
		
	/** The Constant FUPL_ERROR_MAX_SIZE. */
	public final static String FUPL_ERROR_MAX_SIZE="fupl.error.max.size";
	
	/** The Constant FUPL_ERROR_INVALID_FILE_TYPE. */
	public final static String FUPL_ERROR_INVALID_FILE_TYPE="fupl.error.invalid.file.type";
	
	/** The Constant FUPL_SUCCESS_UPLOAD. */
	public final static String FUPL_SUCCESS_UPLOAD="fupl.success.upload";
	
	/** The Constant CONFIRM_HEADER_BLOCK. */
	public final static String CONFIRM_HEADER_BLOCK="lbl.confirm.header.block";
	
	/** The Constant CONFIRM_HEADER_UNBLOCK. */
	public final static String CONFIRM_HEADER_UNBLOCK="lbl.confirm.header.unblock";
	//General

	//Generic errors
	/** The Constant ERROR_COMBO_REQUIRED. */
	public final static String ERROR_COMBO_REQUIRED="error.combo.required";
	
	/** The Constant ERROR_FIELD_REQUIRED. */
	public final static String ERROR_FIELD_REQUIRED="error.field.required";
	
	/** The Constant ERROR_RECORD_REQUIRED. */
	public final static String ERROR_RECORD_REQUIRED="error.record.required";
	
	/** The Constant ERROR_RECORD_REQUIRED. */
	public final static String SECURITY_AUTHORIZED_CORRECT="security.authorized.correct";
	
	/** The Constant ERROR_RECORD_REQUIRED. */
	public final static String SECURITY_AUTHORIZED="security.authorized";
	
	/** The Constant ERROR_LESS_THAN_OR_EQUAL_TO. */
	public final static String ERROR_LESS_THAN_OR_EQUAL_TO="error.less.than.or.equal.to";
	
	/** The Constant ERROR_GREATER_THAN_OR_EQUAL_TO. */
	public final static String ERROR_GREATER_THAN_OR_EQUAL_TO="error.greater.than.or.equal.to";
	
	/** The Constant ERROR_GREATER_THAN. */
	public final static String ERROR_GREATER_THAN="error.greater.than";
	
	/** The Constant ERROR_LESS_THAN. */
	public final static String ERROR_LESS_THAN="error.less.than";
	
	/** The Constant ERROR_MULTIPLO. */
	public final static String ERROR_MULTIPLO="error.multiplo";
	//Generic errors
	
	/** The Constant ISSUANCE_LBL_AMOUNT_ZERO. */
	//Issuance
	public final static String ISSUANCE_LBL_AMOUNT_ZERO="issuance.lbl.amount.zero";
	
	/** The Constant ISSUANCE_LBL_TRANCHES_BELOW_CURRENT. */
	public final static String ISSUANCE_LBL_TRANCHES_BELOW_CURRENT="issuance.lbl.tranches.below.current";
	
	/** The Constant ISSUANCE_LBL_PROGRAM_EXPIRATION_DATE. */
	public final static String ISSUANCE_LBL_PROGRAM_EXPIRATION_DATE="issuance.lbl.program.expiration.date";
	
	/** The Constant ISSUANCE_ERROR_MIXED_ISSUE_TYPE. */
	public final static String ISSUANCE_ERROR_MIXED_ISSUE_TYPE="issuance.msg.error.mixed.issue.type";
	
	/** The Constant ISSUANCE_ERROR_CERTIFICATE_FILE. */
	public final static String ISSUANCE_ERROR_CERTIFICATE_FILE="issuance.msg.error.certificate.file";
	
	/** The Constant ISSUANCE_ERROR_DESCRIPTION. */
	public final static String ISSUANCE_ERROR_DESCRIPTION="issuance.msg.error.description.in.use";
	
	/** The Constant ISSUANCE_CONFIRM_REGISTER. */
	public final static String ISSUANCE_CONFIRM_REGISTER="issuance.confirm.register";
	
	/** The Constant ISSUANCE_ERROR_REGISTER_BLOCK_ISSUER. */
	public final static String ISSUANCE_ERROR_REGISTER_BLOCK_ISSUER="issuance.msg.error.registry.block.issuer";
	
	/** The Constant ISSUANCE_CONFIRM_MODIFY. */
	public final static String ISSUANCE_CONFIRM_MODIFY="issuance.confirm.modify";
	
	public final static String ISSUANCE_CONFIRM_REVIEW="issuance.confirm.review";
	
	public final static String ISSUANCE_CONFIRM_APPROVE="issuance.confirm.approve";
	
	public final static String ISSUANCE_CONFIRM_CONFIRM="issuance.confirm.confirm";
	
	public final static String ISSUANCE_CONFIRM_REJECT="issuance.confirm.reject";
	
	/** The Constant ISSUANCE_MSG_REGISTER_SUCCESS. */
	public final static String ISSUANCE_MSG_REGISTER_SUCCESS="issuance.msg.register.success";
	
	/** The Constant ISSUANCE_MSG_MODIFY_SUCCESS. */
	public final static String ISSUANCE_MSG_MODIFY_SUCCESS="issuance.msg.modify.success";
	
	/** The Constant ISSUANCE_ERROR_NOT_FOUND. */
	public final static String ISSUANCE_ERROR_NOT_FOUND="issuance.error.not.found";
	
	/** The Constant ISSUANCE_ERROR_ISSUANCE_CERTIFICATE_REQUIRED. */
	public final static String ISSUANCE_ERROR_ISSUANCE_CERTIFICATE_REQUIRED="issuance.error.issuance.certificate.required";
	
	/** The Constant ISSUANCE_ERROR_DPA_DPF_MIN_DAYS_TERM. */
	public final static String ISSUANCE_ERROR_DPA_DPF_MIN_DAYS_TERM="issuance.error.dpa.dpf.min.days.term";
	
	/** The Constant ISSUANCE_ERROR_EXP_DATE_LESS_SEC_EXP_DATE. */
	public final static String ISSUANCE_ERROR_EXP_DATE_LESS_SEC_EXP_DATE="issuance.error.expiration.date.less.security.date";
	
	/** The Constant ISSUANCE_ERROR_SECURITY_CLASS_SETUP. */
	public final static String ISSUANCE_ERROR_SECURITY_CLASS_SETUP="issuance.error.security.class.inconsistent.setup";
	
	/** The Constant ISSUANCE_ERROR_COUNTRY_INCORRECT. */
	public final static String ISSUANCE_ERROR_COUNTRY_INCORRECT="issuance.error.country.incorrect";
	
	/** The Constant ISSUANCE_ERROR_EXPIRATION_DATE_LESS. */
	public final static String ISSUANCE_ERROR_EXPIRATION_DATE_LESS="issuance.error.expiration.date.less";
	
	public final static String ISSUANCE_SECURITY_REQUEST_VALIDATION_NOT_IN_REGISTERED_STATE="issuance.security.request.validation.ni.registered.state";
	public final static String ISSUANCE_SECURITY_REQUEST_VALIDATION_NOT_IN_CONFIRMED_STATE="issuance.security.request.validation.ni.confirmed.state";
	public final static String ISSUANCE_SECURITY_REQUEST_VALIDATION_NOT_IN_APPROBED_STATE="issuance.security.request.validation.ni.approbed.state";
	public final static String ISSUANCE_SECURITY_REQUEST_VALIDATION_NOT_IN_REJECTED_STATE="issuance.security.request.validation.ni.rejected.state";
	public final static String ISSUANCE_SECURITY_REQUEST_VALIDATION_NOT_IN_REVIEWED_STATE="issuance.security.request.validation.ni.reviewed.state";
	public final static String ISSUANCE_SECURITY_REQUEST_VALIDATION_NOT_IN_CONFIRMED_STATE_NOT_MODIFY="issuance.security.request.validation.ni.confirmed.not.modify";
//	
	public final static String ISSUANCE_REQUEST_CONFIRMED_STATE_CORRECT="issuance.request.confirmed.state.correct";
	public final static String ISSUANCE_REQUEST_APPROBED_STATE_CORRECT="issuance.request.approbed.state.correct";
	public final static String ISSUANCE_REQUEST_REJECTED_STATE_CORRECT="issuance.request.rejected.state.correct";
	public final static String ISSUANCE_REQUEST_REVIEWED_STATE_CORRECT="issuance.request.reviewed.state.correct";
	public final static String ISSUANCE_REQUEST_CONFIRMED_TO_REJECT="issuance.request.confirmed.to.reject";
	public final static String ISSUANCE_REQUEST_REJECT_TO_REJECT="issuance.request.reject.to.reject";
	public final static String ISSUANCE_REQUEST_EXPIRED_TO_REJECT="issuance.request.expired.to.reject";
	public final static String ISSUANCE_REQUEST_SUSPENDED_TO_REJECT="issuance.request.suspended.to.reject";
	public final static String ISSUANCE_REQUEST_INCORRECT_STATE="issuance.request.incorrect.state";
			
	//Issuance
	
	//Issuer Start
	/** The Constant ISSUER_ERROR_REQUIRED. */
	public final static String ISSUER_ERROR_REQUIRED="issuer.error.required";
	
	/** The Constant ISSUER_ERROR_NOT_FOUND. */
	public final static String ISSUER_ERROR_NOT_FOUND="issuer.error.not.found";
	
	/** The Constant ISSUER_ERROR_YES_FOUND. */
	public final static String ISSUER_ERROR_YES_FOUND="issuer.error.yes.found";
	
	/** The Constant ISSUER_ERROR_STATE_BLOCKED. */
	public final static String ISSUER_ERROR_STATE_BLOCKED="issuer.error.state.blocked";
	
	//Issuer End
	
	//Certiicate issuance
	/** The Constant ISSUANCE_CERTIFICATE_NUMBER_REPEATED. */
	public final static String ISSUANCE_CERTIFICATE_NUMBER_REPEATED="issuance.certificate.error.certificate.number.repeated";
	
	/** The Constant ISSUANCE_CERTIFICATE_SERIE_REPEATED. */
	public final static String ISSUANCE_CERTIFICATE_SERIE_REPEATED="issuance.certificate.error.certificate.serie.repeated";
	//Certificate issuance
	
	// START - PLACEMENTE SEGMENT
	/** The Constant PLACEMENTESEGMENT_VALIDATION_ISSUER_EMPTY. */
	public final static String PLACEMENTESEGMENT_VALIDATION_ISSUER_EMPTY="placementsegment.validation.issuer.empty";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_ISSUER_ACCOUNTEMPTY. */
	public final static String PLACEMENTESEGMENT_VALIDATION_ISSUER_ACCOUNTEMPTY="placementsegment.validation.issuer.accountempty";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_ISSUER_NOTACTIVE. */
	public final static String PLACEMENTESEGMENT_VALIDATION_ISSUER_ACCOUNT_NOTACTIVE="placementsegment.validation.issuer.account.notactive";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_ISSUANCE_EMPTY. */
	public final static String PLACEMENTESEGMENT_VALIDATION_ISSUANCE_EMPTY="placementsegment.validation.issuance.empty";
	
	/** The Constant PLACEMENTESEGMENT_ISSUANCE_NOT_PLACEMENT. */
	public final static String PLACEMENTESEGMENT_ISSUANCE_NOT_PLACEMENT="placementsegment.validation.issuance.not.placement";
	
	/** The Constant PLACEMENTESEGMENT_PLACEMENTS_TRANCES_COMPLETE. */
	public final static String PLACEMENTESEGMENT_PLACEMENTS_TRANCES_COMPLETE="placementsegment.validation.placements.tranches.complete";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_ISSUANCE_SECURITIES_EMPTY. */
	public final static String PLACEMENTESEGMENT_VALIDATION_ISSUANCE_SECURITIES_EMPTY="placementsegment.validation.issuance.securities.empty";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_ISSUANCE_EXPIRED. */
	public final static String PLACEMENTESEGMENT_VALIDATION_ISSUANCE_EXPIRED="placementsegment.validation.issuance.expired";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_ISSUANCE_INCONSITENT_DATA. */
	public final static String PLACEMENTESEGMENT_VALIDATION_ISSUANCE_INCONSITENT_DATA="placementsegment.validation.issuance.inconsistent.data";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_SECURITIES_AUTHORIZED_SECURITY. */
	public final static String PLACEMENTESEGMENT_VALIDATION_SECURITIES_AUTHORIZED_SECURITY="placementsegment.validation.securities.authorizedSecurity";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_SECURITIES_AMMOUNT. */
	public final static String PLACEMENTESEGMENT_VALIDATION_SECURITIES_AMMOUNT="placementsegment.validation.securities.ammount";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_SECURITIES_AMMOUNT_ZERO. */
	public final static String PLACEMENTESEGMENT_VALIDATION_SECURITIES_AMMOUNT_ZERO="placementsegment.validation.securities.ammount.zero";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_SECURITIES_AMMOUNT_FLOATING. */
	public final static String PLACEMENTESEGMENT_VALIDATION_SECURITIES_AMMOUNT_FLOATING="placementsegment.validation.securities.ammount.floating";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_SECURITIES_AMMOUNT_FIXED. */
	public final static String PLACEMENTESEGMENT_VALIDATION_SECURITIES_AMMOUNT_FIXED="placementsegment.validation.securities.ammount.fixed";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_SECURITIES_AMMOUNT_NOMINALVAL. */
	public final static String PLACEMENTESEGMENT_VALIDATION_SECURITIES_AMMOUNT_NOMINALVAL="placementsegment.validation.securities.ammount.nominalval";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_SECURITIES_EMPTY. */
	public final static String PLACEMENTESEGMENT_VALIDATION_SECURITIES_EMPTY="placementsegment.validation.securities.empty";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_AMOUNTTOPLACE_ZERO. */
	public final static String PLACEMENTESEGMENT_VALIDATION_AMOUNTTOPLACE_ZERO="placementsegment.validation.amounttoplace.zero";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_PARTICIPANT_STRUCTING. */
	public final static String PLACEMENTESEGMENT_VALIDATION_PARTICIPANT_STRUCTING="placementsegment.validation.participant.structing";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_PLACEMENT_AMOUNT. */
	public final static String PLACEMENTESEGMENT_VALIDATION_PLACEMENT_AMOUNT="placementsegment.validation.placement.ammount";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_PLACEMENT_AMOUNT_ALL. */
	public final static String PLACEMENTESEGMENT_VALIDATION_PLACEMENT_AMOUNT_ALL="placementsegment.validation.placement.ammount.all";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_AMOUNTAVAILABLE. */
	public final static String PLACEMENTESEGMENT_VALIDATION_AMOUNTAVAILABLE="placementsegment.validation.amountavailable";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_SECURITY_ISIN. */
	public final static String PLACEMENTESEGMENT_VALIDATION_SECURITY_ISIN="placementsegment.validation.security.isin";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_SECURITY_ISIN_NOTSELECTED. */
	public final static String PLACEMENTESEGMENT_VALIDATION_SECURITY_ISIN_NOTSELECTED="placementsegment.validation.security.isin.notselected";
	
	public final static String PLACEMENTESEGMENT_NEGATIVE_CIRCULATION_AMOUNT="placementsegment.negative.circulation.amount";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_CLOSING_DATE_NO_ENTERED. */
	public final static String PLACEMENTESEGMENT_VALIDATION_CLOSING_DATE_NO_ENTERED="placementsegment.validation.closing.date.no.entered";
	
	/** The Constant ISSUER_VALIDATION_BLOCK. */
	public final static String ISSUER_VALIDATION_BLOCK="issuer.validation.block";
	
	/** The Constant ISSUER_VALIDATION_ISSUER. */
	public final static String ISSUER_VALIDATION_ISSUER="placementsegment.validation.issuer";
	
	/** The Constant VALIDATION_RANGE_DATES. */
	public final static String VALIDATION_RANGE_DATES="validation.range.dates";
	
	/** The Constant ISSUER_VALIDATION_ISSUANCE. */
	public final static String ISSUER_VALIDATION_ISSUANCE="placementsegment.validation.issuance";
	
	/** The Constant PLACEMENTESEGMENT_MESSAGE_CONFIRM. */
	public final static String PLACEMENTESEGMENT_MESSAGE_CONFIRM="placementsegment.message.confirm";
	
	/** The Constant PLACEMENTESEGMENT_MESSAGE_CONFIRM_OK. */
	public final static String PLACEMENTESEGMENT_MESSAGE_CONFIRM_OK="placementsegment.message.confirm.ok";
	
	/** The Constant PLACEMENTESEGMENT_MESSAGE_REGISTER_OK. */
	public final static String PLACEMENTESEGMENT_MESSAGE_REGISTER="placementsegment.message.register";
	
	/** The Constant PLACEMENTESEGMENT_MESSAGE_REGISTER_OK. */
	public final static String PLACEMENTESEGMENT_MESSAGE_REGISTER_OK="placementsegment.message.register.ok";
	
	/** The Constant PLACEMENTESEGMENT_MESSAGE_OPENED_OK. */
	public final static String PLACEMENTESEGMENT_MESSAGE_OPENED_OK="placementsegment.message.opened.ok";
	
	/** The Constant PLACEMENTESEGMENT_MESSAGE_MODIFY. */
	public final static String PLACEMENTESEGMENT_MESSAGE_MODIFY="placementsegment.message.modify";
	
	/** The Constant PLACEMENTESEGMENT_MESSAGE_MODIFY_OK. */
	public final static String PLACEMENTESEGMENT_MESSAGE_MODIFY_OK="placementsegment.message.modify.ok";
	
	/** The Constant PLACEMENTESEGMENT_MESSAGE_REJECT. */
	public final static String PLACEMENTESEGMENT_MESSAGE_REJECT="placementsegment.message.reject";
	
	/** The Constant PLACEMENTESEGMENT_MESSAGE_REJECT_OK. */
	public final static String PLACEMENTESEGMENT_MESSAGE_REJECT_OK="placementsegment.message.reject.ok";
	
	/** The Constant PLACEMENTESEGMENT_MESSAGE_SUSPEND. */
	public final static String PLACEMENTESEGMENT_MESSAGE_SUSPEND="placementsegment.message.suspend";
	
	/** The Constant PLACEMENTESEGMENT_MESSAGE_SUSPEND_OK. */
	public final static String PLACEMENTESEGMENT_MESSAGE_SUSPEND_OK="placementsegment.message.suspend.ok";
	
	/** The Constant PLACEMENTESEGMENT_MESSAGE_SUSPEND. */
	public final static String PLACEMENTESEGMENT_MESSAGE_REOPEN_REQUEST="placementsegment.message.reopen.request";
	
	/** The Constant PLACEMENTESEGMENT_MESSAGE_SUSPEND_OK. */
	public final static String PLACEMENTESEGMENT_MESSAGE_REOPEN_REQUEST_OK="placementsegment.message.reopen.request.ok";
	
	/** The Constant PLACEMENTESEGMENT_MESSAGE_SUSPEND. */
	public final static String PLACEMENTESEGMENT_MESSAGE_REOPEN="placementsegment.message.reopen";
	
	/** The Constant PLACEMENTESEGMENT_MESSAGE_SUSPEND_OK. */
	public final static String PLACEMENTESEGMENT_MESSAGE_REOPEN_OK="placementsegment.message.reopen.ok";
	
	/** The Constant PLACEMENTESEGMENT_MESSAGE_SUSPEND_OK. */
	public final static String PLACEMENTESEGMENT_VALIDATION_REOPEN_NOFILE="placementsegment.validation.reopen.nofile";
			
	/** The Constant PLACEMENTESEGMENT_MESSAGE_SUSPEND_UP. */
	public final static String PLACEMENTESEGMENT_MESSAGE_SUSPEND_UP="placementsegment.message.suspend.up";
			
	/** The Constant PLACEMENTESEGMENT_MESSAGE_SUSPEND_UP_OK. */
	public final static String PLACEMENTESEGMENT_MESSAGE_SUSPEND_UP_OK="placementsegment.message.suspend.up.ok";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_DO_ACTION. */
	public final static String PLACEMENTESEGMENT_VALIDATION_DO_ACTION="placementsegment.validation.do.action";
	
	/** The Constant OPT_VALUEHOLDBALANCE_VALIDATION_HOLDERACCOUNT. */
	public final static String OPT_VALUEHOLDBALANCE_VALIDATION_HOLDERACCOUNT="opt.valueholdbalance.validation.holderaccount";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_ISSUANCE_NOTISSUER. */
	public final static String PLACEMENTESEGMENT_VALIDATION_ISSUANCE_NOTISSUER="placementsegment.validation.issuance.notissuer";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_ISSUANCE_SUSPEND. */
	public final static String PLACEMENTESEGMENT_VALIDATION_ISSUANCE_SUSPEND="placementsegment.validation.issuance.suspend";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_HOLDER. */
	public final static String PLACEMENTESEGMENT_VALIDATION_HOLDER="placementsegment.validation.holder";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_HOLDER_NOT_EXIST. */
	public final static String PLACEMENTESEGMENT_VALIDATION_HOLDER_NOT_EXIST="placementsegment.validation.holder.not.exist";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_ISSUER_STATE. */
	public final static String PLACEMENTESEGMENT_VALIDATION_ISSUER_STATE="placementsegment.validation.issuer.state";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_OPENED. */
	public final static String PLACEMENTESEGMENT_VALIDATION_OPENED="placementsegment.validation.opened";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_NOT_OPENED. */
	public final static String PLACEMENTESEGMENT_VALIDATION_NOT_OPENED="placementsegment.validation.not.opened";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_AVAILABLE. */
	public final static String PLACEMENTESEGMENT_VALIDATION_AVAILABLE="placementsegment.validation.available";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_PLACEMENTNUMBER. */
	public final static String PLACEMENTESEGMENT_VALIDATION_PLACEMENTNUMBER="placementsegment.validation.placementnumber";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_MODIFY_STATE. */
	public final static String PLACEMENTESEGMENT_VALIDATION_MODIFY_STATE="placementsegment.validation.modify.state";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_MODIFY_STATE. */
	public final static String PLACEMENTESEGMENT_VALIDATION_REOPEN_STATE="placementsegment.validation.reopen.state";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_MODIFY_STATE. */
	public final static String PLACEMENTESEGMENT_VALIDATION_REOPEN_APROVE="placementsegment.validation.reopen.aprove";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_MODIFY_STATE. */
	public final static String PLACEMENTESEGMENT_VALIDATION_REOPEN_REQUEST="placementsegment.validation.reopen.request";
	
	/** The Constant PLACEMENTESEGMENT_ERROR_MODIFY_FOR_STATE. */
	public final static String PLACEMENTESEGMENT_ERROR_MODIFY_FOR_STATE="placementsegment.validation.error.modify.for.state";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_SUSPEND. */
	public final static String PLACEMENTESEGMENT_VALIDATION_SUSPEND="placementsegment.validation.suspend";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_CONFIRM. */
	public final static String PLACEMENTESEGMENT_VALIDATION_CONFIRM="placementsegment.validation.confirm";
	
	/** The Constant PLACEMENTESEGMENT_VALIDATION_SUSPENDUP. */
	public final static String PLACEMENTESEGMENT_VALIDATION_SUSPENDUP="placementsegment.validation.suspendup";
	
	/** The Constant PLACEMENTESEGMENT_HOLDERACCOUNT_CREATE. */
	public final static String PLACEMENTESEGMENT_HOLDERACCOUNT_CREATE="placementsegment.holderaccount.create";
	// END - PLACEMENTE SEGMENT
	
	/** The Constant SECURITY_LBL_EXPIRATION_DATE. */
	//START - SECURITY
	public final static String SECURITY_LBL_EXPIRATION_DATE="security.lbl.expired.date";
	
	/** The Constant SECURITY_LBL_ISSUANCE_DATE. */
	public final static String SECURITY_LBL_ISSUANCE_DATE="security.lbl.issuance.date";
	
	/** The Constant SECURITY_LBL_INTEREST_PERIODICITY. */
	public final static String SECURITY_LBL_INTEREST_PERIODICITY="security.lbl.interest.payment.periodicity";
	
	/** The Constant SECURITY_LBL_INTEREST_PAYMENT_MODE. */
	public final static String SECURITY_LBL_INTEREST_PAYMENT_MODE="security.lbl.interest.payment.mode";
	
	/** The Constant SECURITY_LBL_AMORTIZATION_FACTOR. */
	public final static String SECURITY_LBL_AMORTIZATION_FACTOR="security.lbl.amortization.factor";
	
	/** The Constant SECURITY_LBL_AMORTIZATION_FACTOR_TOTAL. */
	public final static String SECURITY_LBL_AMORTIZATION_FACTOR_TOTAL="security.lbl.amortization.factor.total";
	
	/** The Constant SECURITY_LBL_MASSIVE. */
	public final static String SECURITY_LBL_MASSIVE="isecurities.security.massive.register.title";
	
	/** The Constant SECURITY_CONFIRM_REGISTER. */
	public final static String SECURITY_CONFIRM_REGISTER="security.confirm.register";
	
	/** The Constant SECURITY_CONFIRM_MODIFY. */
	public final static String SECURITY_CONFIRM_MODIFY="security.confirm.modify";
	
	/** The Constant SECURITY_CONFIRM_RANGE_REMOVE. */
	public final static String SECURITY_CONFIRM_RANGE_REMOVE="security.confirm.range.remove";
	
	/** The Constant SECURITY_MSG_REGISTER_SUCCESS. */
	public final static String SECURITY_MSG_REGISTER_SUCCESS="security.register.success";
	
	/** The Constant SECURITY_MSG_MODIFY_SUCCESS. */
	public final static String SECURITY_MSG_MODIFY_SUCCESS="security.modify.success";
	
	/** The Constant SECURITY_MSG_CFI_CODE_SUCCESS_GENERATE. */
	public final static String SECURITY_MSG_CFI_CODE_SUCCESS_GENERATE="security.msg.cfi.code.success.generate";
	
	/** The Constant SECURITY_BLOCK_SUCCESS. */
	public final static String SECURITY_BLOCK_SUCCESS="security.block.success";
	
	/** The Constant SECURITY_UNBLOCK_SUCCESS. */
	public final static String SECURITY_UNBLOCK_SUCCESS="security.unblock.success";
	
	/** The Constant SECURITY_CONFIRM_BLOCK. */
	public final static String SECURITY_CONFIRM_BLOCK="security.confirm.block";
	
	/** The Constant SECURITY_CONFIRM_UNBLOCK. */
	public final static String SECURITY_CONFIRM_UNBLOCK="security.confirm.unblock";
	
	/** The Constant SECURITY_CONFIRM_GENERATE_FILE. */
	public final static String SECURITY_CONFIRM_GENERATE_FILE="security.confirm.generate.file";
	
	/** The Constant SECURITY_ERROR_INTERNATIONAL_MECHANISM_REQUIRED. */
	public final static String SECURITY_ERROR_INTERNATIONAL_MECHANISM_REQUIRED="security.error.international.mechanism.required";
	
	/** The Constant SECURITY_ERROR_NEGOTIATION_MECHANISM_REQUIRED. */
	public final static String SECURITY_ERROR_NEGOTIATION_MECHANISM_REQUIRED="security.error.negotiation.mechanism.required";
	
	/** The Constant SECURITY_ERROR_MNEMONIC_IN_USE. */
	public final static String SECURITY_ERROR_MNEMONIC_IN_USE="security.error.mnemonic.in.use";
	
	/** The Constant SECURITY_ERROR_SERIAL_IN_USE. */
	public final static String SECURITY_ERROR_SERIAL_IN_USE="security.error.serial.in.use";
	
	/** The Constant SECURITY_ERROR_MONTHS_TERM_EXCEEDS. */
	public final static String SECURITY_ERROR_MONTHS_TERM_EXCEEDS="security.error.months.term.exceeds";
	
	/** The Constant SECURITY_ERROR_PAYMENT_SCHEDULE_AGAIN. */
	public final static String SECURITY_ERROR_PAYMENT_SCHEDULE_AGAIN="security.error.payment.schedule.again";
	
	/** The Constant SECURITY_ERROR_PHYSICAL_QUANTITY. */
	public final static String SECURITY_ERROR_PHYSICAL_QUANTITY="security.error.physical.quantity";
	
	/** The Constant SECURITY_ERROR_PAYMENT_INCORRECT_TOT_FACTOR. */
	public final static String SECURITY_ERROR_PAYMENT_INCORRECT_TOT_FACTOR="security.error.payment.incorrect.total.factor";
	
	/** The Constant SECURITY_ERROR_PAYMENT_INCORRECT_TOT_COUPON. */
	public final static String SECURITY_ERROR_PAYMENT_INCORRECT_TOT_COUPON="security.error.payment.incorrect.total.coupon.amount";
	
	/** The Constant SECURITY_ERROR_PAYMENT_REGENERATE. */
	public final static String SECURITY_ERROR_PAYMENT_REGENERATE="security.error.payment.schedule.regenerate";
	
	/** The Constant SECURITY_ERROR_PAYMENT_SCHE_GENERATE_AGAIN. */
	public final static String SECURITY_ERROR_PAYMENT_SCHE_GENERATE_AGAIN="security.error.payment.schedule.generate.again";
	
	/** The Constant SECURITY_ERROR_NO_EARLY_REDEMPTION. */
	public final static String SECURITY_ERROR_NO_EARLY_REDEMPTION="security.error.no.early.redemption";
	
	/** The Constant SECURITY_ERROR_INCORRECT_ISSUER_PREPAID. */
	public final static String SECURITY_ERROR_INCORRECT_ISSUER_PREPAID="security.error.incorrect.issuer.prepaid";
	
	/** The Constant SECURITY_ERROR_INCORRECT_ISSUER_PREPAID. */
	public final static String SECURITY_ERROR_EXIST="security.error.exist";
	
	/** The Constant SECURITY_ERROR_INCORRECT_CLASS_PREPAID. */
	public final static String SECURITY_ERROR_INCORRECT_CLASS_PREPAID="security.error.incorrect.class.prepaid";
	
	/** The Constant SECURITY_ERROR_AMORTIZATION_FACTOR_REQUIRED. */
	public final static String SECURITY_ERROR_AMORTIZATION_FACTOR_REQUIRED="security.error.amortization.factor.required";
	
	/** The Constant SECURITY_ERROR_AMORTIZATION_ZERO. */
	public final static String SECURITY_ERROR_AMORTIZATION_ZERO="security.error.amortization.factor.zero";
	
	/** The Constant SECURITY_ERROR_INTEREST_RATE_REQUIRED. */
	public final static String SECURITY_ERROR_INTEREST_RATE_REQUIRED="security.error.interest.rate.required";
	
	/** The Constant SECURITY_ERROR_MONTH_TERM. */
	public final static String SECURITY_ERROR_MONTH_TERM="security.error.months.term";
	
	/** The Constant SECURITY_ERROR_NUMBER_COUPONS. */
	public final static String SECURITY_ERROR_NUMBER_COUPONS="security.error.number.coupons";
	
	/** The Constant SECURITY_ERROR_REGISTRY_BLOCK_ISUUER. */
	public final static String SECURITY_ERROR_REGISTRY_BLOCK_ISUUER="security.error.registry.block.issuer";
	
	/** The Constant SECURITY_ERROR_REGISTRY_BLOCK_ISSUANCE. */
	public final static String SECURITY_ERROR_REGISTRY_BLOCK_ISSUANCE="security.error.registry.block.issuance";
	
	/** The Constant SECURITY_ERROR_BLOCK_STATE_NO_REGISTERED. */
	public final static String SECURITY_ERROR_BLOCK_STATE_NO_REGISTERED="security.error.block.state.no.registered";
	
	/** The Constant SECURITY_ERROR_UNBLOCK_STATE_NO_BLOCK. */
	public final static String SECURITY_ERROR_UNBLOCK_STATE_NO_BLOCK="security.error.unblock.state.no.block";
	
	/** The Constant SECURITY_ERROR_UNBLOCK_STATE_ISSUER_BLOCK. */
	public final static String SECURITY_ERROR_UNBLOCK_STATE_ISSUER_BLOCK="security.error.unblock.state.issuer.block";
	
	/** The Constant SECURITY_ERROR_ISIN_LENGTH. */
	public final static String SECURITY_ERROR_ISIN_LENGTH="security.error.isin.length";	
	
	/** The Constant SECURITY_ERROR_ISIN_CHECK_DIGIT. */
	public final static String SECURITY_ERROR_ISIN_CHECK_DIGIT="security.error.isin.check.digit";
	
	/** The Constant SECURITY_ERROR_CODE_NOT_FOUND. */
	public final static String SECURITY_ERROR_CODE_NOT_FOUND="security.error.code.not.found";
	
	/** The Constant SECURITY_ERROR_VAR_INT_AT_MATURITY. */
	public final static String SECURITY_ERROR_VAR_INT_AT_MATURITY="security.error.variable.int.at.maturity";
	
	/** The Constant SECURITY_ERROR_PERIOD_DAYS_EXCEEDS. */
	public final static String SECURITY_ERROR_PERIOD_DAYS_EXCEEDS="security.error.periodicity.days.exceeds";
	
	/** The Constant SECURITY_ERROR_PERIOD_EXEEDS_MONTHS_TERM. */
	public final static String SECURITY_ERROR_PERIOD_EXEEDS_MONTHS_TERM="security.error.periodicity.exceeds.months.term";
	
	/** The Constant SECURITY_ERROR_SPREAD_LESS_RATE_VALUE. */
	public final static String SECURITY_ERROR_SPREAD_LESS_RATE_VALUE="security.error.spread.less.rate.value";
	
	/** The Constant SECURITY_ERROR_EXIST_PDF_OR_DPA. */
	public final static String SECURITY_ERROR_EXIST_PDF_OR_DPA="security.error.exist.dpf.or.dpa";
	
	/** The Constant SECURITY_ERROR_FILE_DIFF_FOUNDS. */
	public final static String SECURITY_ERROR_FILE_DIFF_FOUNDS="security.error.file.diff.founds";
	
	/** The Constant SECURITY_ERROR_FILE_DIFF_SCREEN_FILE. */
	public final static String SECURITY_ERROR_FILE_DIFF_SCREEN_FILE="security.error.file.diff.screen.file";
	
	/** The Constant SECURITY_ERROR_COMPARATIVE_EQUALS. */
	public final static String SECURITY_ERROR_COMPARATIVE_EQUALS="security.msg.comparative.equals";
	
	//END - SECURITY
	
	/** The Constant PROGRAM_AMORTIZATION_COUPON_REQUIRED. */
	//START - PROGRAM AMORTIZATION COUPON
	public final static String PROGRAM_AMORTIZATION_COUPON_REQUIRED="program.amortization.coupon.required";
	
	/** The Constant PROGRAM_INTEREST_COUPON_REQUIRED. */
	public final static String PROGRAM_INTEREST_COUPON_REQUIRED="program.interest.coupon.required";
	//END - PROGRAM AMORTIZATION COUPON
	
	/** The Constant PAYMENT_CRON_CONF_REGISTER_MOD_INT. */
	//START PAYMENT CRONOGRAM
	public final static String PAYMENT_CRON_CONF_REGISTER_MOD_INT="payment.cronogram.confirm.register.mod.int";
	
	/** The Constant PAYMENT_CRON_CONF_REGISTER_MOD_AMO. */
	public final static String PAYMENT_CRON_CONF_REGISTER_MOD_AMO="payment.cronogram.confirm.register.mod.amo";
	
	/** The Constant PAYMENT_CRON_REGISTER_MOD_INT_SUCCESS. */
	public final static String PAYMENT_CRON_REGISTER_MOD_INT_SUCCESS="payment.cronogram.register.mod.int.success";
	
	/** The Constant PAYMENT_CRON_REGISTER_MOD_AMO_SUCCESS. */
	public final static String PAYMENT_CRON_REGISTER_MOD_AMO_SUCCESS="payment.cronogram.register.mod.amo.success";
	
	/** The Constant PAYMENT_CRON_ERROR_REQUIRED. */
	public final static String PAYMENT_CRON_ERROR_REQUIRED="payment.cronogram.error.required";
	
	/** The Constant PAYMENT_CRON_ERROR_INT_PRO_MOD_REQUIRED. */
	public final static String PAYMENT_CRON_ERROR_INT_PRO_MOD_REQUIRED="payment.cronogram.error.int.pro.mod.required";
	
	/** The Constant PAYMENT_CRON_ERROR_AMO_PRO_MOD_REQUIRED. */
	public final static String PAYMENT_CRON_ERROR_AMO_PRO_MOD_REQUIRED="payment.cronogram.error.amo.pro.mod.required";
	
	/** The Constant PAYMENT_CRON_ERROR_INT_REQ_PEND. */
	public final static String PAYMENT_CRON_ERROR_INT_REQ_PEND="payment.cronogram.error.int.req.pending";
	
	/** The Constant PAYMENT_CRON_ERROR_AMO_REQ_PEND. */
	public final static String PAYMENT_CRON_ERROR_AMO_REQ_PEND="payment.cronogram.error.amo.req.pending";
	
	/** The Constant PAYMENT_CRON_ERROR_ISSUER_BLOKED. */
	public final static String PAYMENT_CRON_ERROR_ISSUER_BLOKED="payment.cronogram.error.issuer.blocked";
	
	/** The Constant PAYMENT_CRON_ERROR_APPROVE_ISSUER_BLOCKED. */
	public final static String PAYMENT_CRON_ERROR_APPROVE_ISSUER_BLOCKED="payment.cronogram.error.approve.issuer.blocked";
	
	/** The Constant PAYMENT_CRON_ERROR_CONFIRM_ISSUER_BLOCKED. */
	public final static String PAYMENT_CRON_ERROR_CONFIRM_ISSUER_BLOCKED="payment.cronogram.error.confirm.issuer.blocked";
	
	/** The Constant PAYMENT_CRON_ERROR_APPROVE_SECURITY_BLOCKED. */
	public final static String PAYMENT_CRON_ERROR_APPROVE_SECURITY_BLOCKED="payment.cronogram.error.approve.security.blocked";
	
	/** The Constant PAYMENT_CRON_ERROR_CONFIRM_SECURITY_BLOCKED. */
	public final static String PAYMENT_CRON_ERROR_CONFIRM_SECURITY_BLOCKED="payment.cronogram.error.confirm.security.blocked";
	
	/** The Constant PAYMENT_CRON_ERROR_APPROVE_STATE_NO_PENDING. */
	public final static String PAYMENT_CRON_ERROR_APPROVE_STATE_NO_PENDING="payment.cronogram.error.approve.state.no.pending";
	
	/** The Constant PAYMENT_CRON_ERROR_ANNUL_STATE_NO_PENDING. */
	public final static String PAYMENT_CRON_ERROR_ANNUL_STATE_NO_PENDING="payment.cronogram.error.annul.state.no.pending";
	
	/** The Constant PAYMENT_CRON_ERROR_REJECT_STATE_NO_APPROVE. */
	public final static String PAYMENT_CRON_ERROR_REJECT_STATE_NO_APPROVE="payment.cronogram.error.reject.state.no.pending";
	
	/** The Constant PAYMENT_CRON_ERROR_CONFIRM_STATE_NO_APPROVE. */
	public final static String PAYMENT_CRON_ERROR_CONFIRM_STATE_NO_APPROVE="payment.cronogram.error.confirm.state.no.pending";
	
	/** The Constant PAYMENT_CRON_ERROR_VARIABLE_INSTRUMENT. */
	public final static String PAYMENT_CRON_ERROR_VARIABLE_INSTRUMENT="payment.cronogram.error.variable.instrument";
	
	public final static String PAYMENT_CRONOGRAM_ERROR_STATE_SECURITY_INVALID="payment.cronogram.error.state.security.invalid";
	
	/** The Constant PAYMENT_CRON_ERROR_NO_INSTRUMENT_DESMATERIALIZE. */
	public final static String PAYMENT_CRON_ERROR_NO_INSTRUMENT_DESMATERIALIZE="payment.cronogram.error.no.instrument.dematerialize";
	
	/** The Constant PAYMENT_CRON_ERROR_MOD_SECURITY_STATE_NO_REGISTERED. */
	public final static String PAYMENT_CRON_ERROR_MOD_SECURITY_STATE_NO_REGISTERED="payment.cronogram.error.mod.security.state.no.registered";
	
	/** The Constant PAYMENT_CRON_ERROR_RES_SECURITY_STATE_NO_REGISTERED. */
	public final static String PAYMENT_CRON_ERROR_RES_SECURITY_STATE_NO_REGISTERED="payment.cronogram.error.res.security.state.no.registered";
	
	/** The Constant PAYMENT_CRON_ERROR_MOD_NO_DESMATERIALIZE. */
	public final static String PAYMENT_CRON_ERROR_MOD_NO_DESMATERIALIZE="payment.cronogram.error.mod.no.dematerialize";
	
	/** The Constant PAYMENT_CRON_ERROR_RES_NO_DESMATERIALIZE. */
	public final static String PAYMENT_CRON_ERROR_RES_NO_DESMATERIALIZE="payment.cronogram.error.res.no.dematerialize";
	
	/** The Constant PAYMENT_CRON_ERROR_RESTRUCTURE_AGAIN. */
	public final static String PAYMENT_CRON_ERROR_RESTRUCTURE_AGAIN="payment.cronogram.error.restructure.again";
	
	/** The Constant PAYMENT_CRON_ERROR_FACTOR_NO_APPLIED. */
	public final static String PAYMENT_CRON_ERROR_FACTOR_NO_APPLIED="payment.cronogram.error.factor.no.applied";
	
	/** The Constant PAYMENT_CRON_ERROR_HAS_SPLIT_SECURITIES. */
	public final static String PAYMENT_CRON_ERROR_HAS_SPLIT_SECURITIES="payment.cronogram.error.has.split.securities";
	
	/** The Constant PAYMENT_CRON_IS_COUPON. */
	public final static String PAYMENT_CRON_IS_COUPON="payment.cronogram.error.is_coupon";
	
	/** The Constant PAYMENT_CRON_IS_DETACHED. */
	public final static String PAYMENT_CRON_IS_DETACHED="payment.cronogram.error.is.detached";
	
	/** The Constant PAYMENT_CRON_ERROR_MORE_PAY_THAT_COUP. */
	public final static String PAYMENT_CRON_ERROR_MORE_PAY_THAT_COUP="payment.cronogram.error.more.payments.that.coupons";
	
	/** The Constant PAYMENT_CRON_ERROR_PAY_AMO_NOT_INT_COU_ASO. */
	public final static String PAYMENT_CRON_ERROR_PAY_AMO_NOT_INT_COU_ASO="payment.cronogram.error.pay.amo.not.int.cou.aso";
	
	/** The Constant PAYMENT_CRON_ERROR_COUP_DAYS. */
	public final static String PAYMENT_CRON_ERROR_COUP_DAYS="payment.cronogram.error.coup.days";
	
	/** The Constant PAYMENT_CRON_ERROR_PAYM_DAYS. */
	public final static String PAYMENT_CRON_ERROR_PAYM_DAYS="payment.cronogram.error.paym.days";
	
	/** The Constant PAYMENT_CRON_LBL_APPROVE_REQ_QUESTION. */
	public final static String PAYMENT_CRON_LBL_APPROVE_REQ_QUESTION="payment.cronogram.lbl.approve.request.question";
	
	/** The Constant PAYMENT_CRON_LBL_ANNULATION_REQ_QUESTION. */
	public final static String PAYMENT_CRON_LBL_ANNULATION_REQ_QUESTION="payment.cronogram.lbl.annulation.request.question";
	
	/** The Constant PAYMENT_CRON_LBL_REJECT_REQ_QUESTION. */
	public final static String PAYMENT_CRON_LBL_REJECT_REQ_QUESTION="payment.cronogram.lbl.reject.request.question";
	
	/** The Constant PAYMENT_CRON_LBL_CONFIRM_REQ_QUESTION. */
	public final static String PAYMENT_CRON_LBL_CONFIRM_REQ_QUESTION="payment.cronogram.lbl.confirm.request.question";
	
	/** The Constant PAYMENT_CRON_LBL_MODIFICATION. */
	public final static String PAYMENT_CRON_LBL_MODIFICATION="payment.cronogram.lbl.modification";
	
	/** The Constant PAYMENT_CRON_LBL_RESTRUCTURATION. */
	public final static String PAYMENT_CRON_LBL_RESTRUCTURATION="payment.cronogram.lbl.restructuration";
	
	/** The Constant PAYMENT_CRON_LBL_NRO_COUPON. */
	public final static String PAYMENT_CRON_LBL_NRO_COUPON="payment.cronogram.lbl.nro.coupon";
	
	/** The Constant PAYMENT_CRON_LBL_RATE. */
	public final static String PAYMENT_CRON_LBL_RATE="payment.cronogram.lbl.rate";
	
	/** The Constant PAYMENT_CRON_LBL_TEA. */
	public final static String PAYMENT_CRON_LBL_TEA="payment.cronogram.lbl.tea";
	
	
	/** The Constant PAYMENT_CRON_LBL_APPROVE_REQ_SUCCESS. */
	public final static String PAYMENT_CRON_LBL_APPROVE_REQ_SUCCESS="payment.cronogram.lbl.approve.request.success";
	
	/** The Constant PAYMENT_CRON_LBL_ANNULATION_REQ_SUCCESS. */
	public final static String PAYMENT_CRON_LBL_ANNULATION_REQ_SUCCESS="payment.cronogram.lbl.annulation.request.success";
	
	/** The Constant PAYMENT_CRON_LBL_REJECT_REQ_SUCCESS. */
	public final static String PAYMENT_CRON_LBL_REJECT_REQ_SUCCESS="payment.cronogram.lbl.reject.request.success";
	
	/** The Constant PAYMENT_CRON_LBL_CONFIRM_REQ_SUCCESS. */
	public final static String PAYMENT_CRON_LBL_CONFIRM_REQ_SUCCESS="payment.cronogram.lbl.confirm.request.success";
	
	/** The Constant PAYMENT_CRON_ERROR_INT_PRO_REST_REQUIRED. */
	public final static String PAYMENT_CRON_ERROR_INT_PRO_REST_REQUIRED="payment.cronogram.error.int.pro.rest.required";
	
	/** The Constant PAYMENT_CRON_ERROR_AMO_PRO_REST_REQUIRED. */
	public final static String PAYMENT_CRON_ERROR_AMO_PRO_REST_REQUIRED="payment.cronogram.error.amo.pro.rest.required";
	
	/** The Constant PAYMENT_CRON_CONF_REGISTER_REST_INT. */
	public final static String PAYMENT_CRON_CONF_REGISTER_REST_INT="payment.cronogram.confirm.register.rest.int";
	
	/** The Constant PAYMENT_CRON_CONF_REGISTER_REST_AMO. */
	public final static String PAYMENT_CRON_CONF_REGISTER_REST_AMO="payment.cronogram.confirm.register.rest.amo";
	
	/** The Constant PAYMENT_CRON_REGISTER_REST_INT_SUCCESS. */
	public final static String PAYMENT_CRON_REGISTER_REST_INT_SUCCESS="payment.cronogram.register.rest.int.success";
	
	/** The Constant PAYMENT_CRON_REGISTER_REST_AMO_SUCCESS. */
	public final static String PAYMENT_CRON_REGISTER_REST_AMO_SUCCESS="payment.cronogram.register.rest.amo.success";
	
	//START PAYMENT CRONOGRAM
	
	//START - ISSUER
	
		/** The Constant ERROR_ISSUER_MSG_MODIFIED_SAVE. */
	public static final String ERROR_ISSUER_MSG_MODIFIED_SAVE="issuer.msg.modified.save";
	
		/** The Constant ERROR_ADM_ISSUER_DOC_NUM_MAX_9_CHAR. */
		public static final String ERROR_ADM_ISSUER_DOC_NUM_MAX_9_CHAR = "error.adm.issuer.documentnumber.max9.characters";
		
		/** The Constant ERROR_ADM_ISSUER_DOC_NUM_MAX_20_CHAR. */
		public static final String ERROR_ADM_ISSUER_DOC_NUM_MAX_20_CHAR = "error.adm.issuer.documentnumber.max20.characters";
		
		/** The Constant ERROR_ADM_ISSUER_NO_LEGAL_REPRESENTATIVE. */
		public static final String ERROR_ADM_ISSUER_NO_LEGAL_REPRESENTATIVE = "error.adm.issuer.no.legal.representative";
		
		/** The Constant ERROR_ADM_ISSUER_NO_INT_DEPOSITORY. */
		public static final String ERROR_ADM_ISSUER_NO_INT_DEPOSITORY = "error.adm.issuer.no.int.depository";
		
		/** The Constant ERROR_ADM_ISSUER_NO_NEGOTIATION_MECHANISM. */
		public static final String ERROR_ADM_ISSUER_NO_NEGOTIATION_MECHANISM = "error.adm.issuer.no.negotiation.mechanism";
		
		/** The Constant ERROR_ADM_ISSUER_DOC_NUM_FOREIGN_REPEATED. */
		public static final String ERROR_ADM_ISSUER_DOC_NUM_FOREIGN_REPEATED = "error.adm.issuer.docnum.foreign.repeated";
		
		/** The Constant ERROR_ADM_ISSUER_DOC_NUM_NATIONAL_REPEATED. */
		public static final String ERROR_ADM_ISSUER_DOC_NUM_NATIONAL_REPEATED = "error.adm.issuer.docnum.national.repeated";
				
		/** The Constant ERROR_ADM_ISSUER_MNEMONIC_REPEATED. */
		public static final String ERROR_ADM_ISSUER_MNEMONIC_REPEATED = "error.adm.issuer.mnemonic.repeated";
		
		/** The Constant ERROR_ADM_ISSUER_RESOLUTION_SIV_REPEATED. */
		public static final String ERROR_ADM_ISSUER_RESOLUTION_SIV_REPEATED = "error.adm.issuer.resolution.siv.repeated";
		
		/** The Constant ERROR_ADM_ISSUER_RNC_NOT_FOUND. */
		public static final String ERROR_ADM_ISSUER_RNC_NOT_FOUND = "error.adm.issuer.rnc.not.found";
		
		/** The Constant ERROR_ADM_ISSUER_TYPE_DOCUMENT_OFAC. */
		public static final String ERROR_ADM_ISSUER_TYPE_DOCUMENT_OFAC = "error.adm.issuer.type.document.ofac";
		
		/** The Constant ERROR_ADM_ISSUER_RNC_NOT_FOUND. */
		public static final String ERROR_ADM_ISSUER_TYPE_DOCUMENT_PEP = "error.adm.issuer.type.document.pep";
		
		/** The Constant ERROR_ADM_ISSUER_RNC_NOT_FOUND. */
		public static final String ERROR_ADM_ISSUER_TYPE_DOCUMENT_PNA = "error.adm.issuer.type.document.pna";
		
		/** The Constant ERROR_ADM_ISSUER_DOC_NUM_NATIONAL_REPEATED. */
		public static final String ERROR_ADM_ISSUER_INVALID_RNC_FORMAT = "error.adm.issuer.invalid.rnc.format";
		
		/** The Constant ERROR_ADM_ISSUER_SAME_DOCUMENT_TYPE. */
		public static final String ERROR_ADM_ISSUER_SAME_DOCUMENT_TYPE = "error.adm.issuer.same.document.type";
		
		/** The Constant ERROR_ADM_ISSUER_SAME_NATIONALITY. */
		public static final String ERROR_ADM_ISSUER_SAME_NATIONALITY = "error.adm.issuer.same.nationality";
		
		/** The Constant LBL_CONFIRM_PARTICIPANT_REGISTER. */
		public static final String LBL_CONFIRM_ISSUER_REGISTER = "lbl.confirm.issuer.register";
		
		/** The Constant LBL_SUCCESS_PARTICIPANT_REGISTER. */
		public static final String LBL_SUCCESS_ISSUER_REGISTER = "lbl.success.issuer.register";

		/** The Constant LBL_SUCCESS_PARTICIPANT_REGISTER. */
		public static final String LBL_SUCCESS_ISSUER_CONFIRM = "lbl.success.issuer.confirm";

		/** The Constant LBL_SUCCESS_PARTICIPANT_REGISTER. */
		public static final String LBL_SUCCESS_ISSUER_MODIFY = "lbl.success.issuer.modify";
		
		/** The Constant LBL_SUCCESS_ISSUER_AND_HOLDER_REGISTER. */
		public static final String LBL_SUCCESS_ISSUER_AND_HOLDER_REGISTER = "lbl.success.issuer.and.holder.register";

		/** The Constant LBL_SUCCESS_ISSUER_AND_HOLDER_REGISTER. */
		public static final String LBL_SUCCESS_ISSUER_AND_HOLDER_CONFIRM = "lbl.success.issuer.and.holder.confirm";

		/** The Constant LBL_SUCCESS_ISSUER_AND_HOLDER_REGISTER. */
		public static final String LBL_SUCCESS_ISSUER_AND_HOLDER_MODIFY = "lbl.success.issuer.and.holder.modify";
		
		/** The Constant ADM_ISSUER_VALIDATION_ROLE_PARTICIPANT. */
		public static final String ADM_ISSUER_VALIDATION_ROLE_PARTICIPANT = "adm.issuer.validation.role.participant";
		
		/** The Constant ADM_ISSUER_VALIDATION_RESIDENCE_COUNTRY. */
		public static final String ADM_ISSUER_VALIDATION_RESIDENCE_COUNTRY = "adm.issuer.validation.residence.country";
		
		/** The Constant ADM_ISSUER_VALIDATION_DOCUMENT_TYPE. */
		public static final String ADM_ISSUER_VALIDATION_DOCUMENT_TYPE = "adm.issuer.validation.document.type";
		
		/** The Constant ADM_ISSUER_VALIDATION_DOCUMENT_NUMBER. */
		public static final String ADM_ISSUER_VALIDATION_DOCUMENT_NUMBER = "adm.issuer.validation.document.number";
		
		/** The Constant ADM_ISSUER_VALIDATION_TRADE_NAME. */
		public static final String ADM_ISSUER_VALIDATION_TRADE_NAME = "adm.issuer.validation.trade.name";
		
		/** The Constant ADM_ISSUER_VALIDATION_MNEMONIC. */
		public static final String ADM_ISSUER_VALIDATION_MNEMONIC = "adm.issuer.validation.mnemonic";
		
		/** The Constant ADM_ISSUER_VALIDATION_ACCOUNT_TYPE. */
		public static final String ADM_ISSUER_VALIDATION_ACCOUNT_TYPE = "adm.issuer.validation.account.type";
		
		/** The Constant ADM_ISSUER_VALIDATION_ACCOUNT_CLASS. */
		public static final String ADM_ISSUER_VALIDATION_ACCOUNT_CLASS = "adm.issuer.validation.account.class";
		
		/** The Constant ADM_ISSUER_VALIDATION_ECONOM_SECTOR. */
		public static final String ADM_ISSUER_VALIDATION_ECONOM_SECTOR = "adm.issuer.validation.economic.sector";
		
		/** The Constant ADM_ISSUER_VALIDATION_ECONOM_ACTIVITY. */
		public static final String ADM_ISSUER_VALIDATION_ECONOM_ACTIVITY = "adm.issuer.validation.economic.activity";
		
		/** The Constant ADM_ISSUER_VALIDATION_CONTRACT_DATE. */
		public static final String ADM_ISSUER_VALIDATION_DEPOSITARY_CONTRACT_DATE = "adm.issuer.validation.depositary.contract.date";
		
		/** The Constant ADM_ISSUER_VALIDATION_EFFECTIVE_DATE. */
		public static final String ADM_ISSUER_VALIDATION_EFFECTIVE_DATE = "adm.issuer.validation.effective.date";
		
		/** The Constant ADM_ISSUER_VALIDATION_INSCRIPTION_BV_DATE. */
		public static final String ADM_ISSUER_VALIDATION_INSCRIPTION_BV_DATE = "adm.issuer.validation.inscription.bv.date";
				
		/** The Constant ADM_ISSUER_VALIDATION_PROVINCE. */
		public static final String ADM_ISSUER_VALIDATION_DEPARTMENT = "adm.issuer.validation.department";
		
		/** The Constant ADM_ISSUER_VALIDATION_PROVINCE. */
		public static final String ADM_ISSUER_VALIDATION_PROVINCE = "adm.issuer.validation.province";
		
		/** The Constant ADM_ISSUER_VALIDATION_SECTOR. */
		public static final String ADM_ISSUER_VALIDATION_SECTOR = "adm.issuer.validation.sector";
		
		/** The Constant ADM_ISSUER_VALIDATION_ADDRESS. */
		public static final String ADM_ISSUER_VALIDATION_ADDRESS = "adm.issuer.validation.address";
		
		/** The Constant ADM_ISSUER_VALIDATION_QUANTITY. */
		public static final String ADM_ISSUER_VALIDATION_QUANTITY = "adm.issuer.validation.quantity";
		
		/** The Constant ADM_ISSUER_VALIDATION_CONTACT_EMAIL. */
		public static final String ADM_ISSUER_VALIDATION_CONTACT_EMAIL = "adm.issuer.validation.contact.email";
		
		/** The Constant ADM_ISSUER_VALIDATION_SOCIETY_TYPE. */
		public static final String ADM_ISSUER_VALIDATION_SOCIETY_TYPE = "adm.issuer.validation.society.type";
		
		/** The Constant ADM_ISSUER_VALIDATION_SIV_DATE. */
		public static final String ADM_ISSUER_VALIDATION_SIV_DATE = "adm.issuer.validation.siv.resol.date";
		
		/** The Constant ADM_ISSUER_VALIDATION_SUPERINT_RESOLUTION. */
		public static final String ADM_ISSUER_VALIDATION_SUPERINT_RESOLUTION = "adm.issuer.validation.superint.resolution";
		
		/** The Constant ADM_ISSUER_REQUEST_VALIDATION_REQUEST_TYPE. */
		public static final String ADM_ISSUER_REQUEST_VALIDATION_REQUEST_TYPE = "adm.issuerrequest.validation.request.type";
		
		/** The Constant ADM_ISSUER_REQUEST_VALIDATION_ISSUER_DOCUMENT_TYPE. */
		public static final String ADM_ISSUER_REQUEST_VALIDATION_ISSUER_DOCUMENT_TYPE = "adm.issuerrequest.validation.issuer.document.type";
		
		/** The Constant ADM_ISSUER_REQUEST_VALIDATION_ISSUER_DOCUMENT_NUMBER. */
		public static final String ADM_ISSUER_REQUEST_VALIDATION_ISSUER_DOCUMENT_NUMBER = "adm.issuerrequest.validation.issuer.document.number";
		
		/** The Constant ADM_ISSUER_REQUEST_VALIDATION_PARTICIPANT. */
		public static final String ADM_ISSUER_REQUEST_VALIDATION_ISSUER = "adm.issuerrequest.validation.issuer";
		
		/** The Constant ADM_ISSUER_REQUEST_VALIDATION_MOTIVE. */
		public static final String ADM_ISSUER_REQUEST_VALIDATION_MOTIVE = "adm.issuerrequest.validation.motive";
		
		/** The Constant ADM_ISSUER_REQUEST_VALIDATION_FILES_SUSTENT. */
		public static final String ADM_ISSUER_REQUEST_VALIDATION_FILES_SUSTENT = "adm.issuerrequest.validation.files.sustent";
		
		/** The Constant LBL_CONFIRM_PARTICIPANT_REQUEST_REGISTER. */
		public static final String LBL_CONFIRM_ISSUER_REQUEST_REGISTER = "lbl.confirm.issuerrequest.register";
		
		/** The Constant ERROR_ADM_ISSUER_BLOCK_PREVIOUS_REQUEST. */
		public static final String ERROR_ADM_ISSUER_BLOCK_PREVIOUS_REQUEST = "error.adm.issuer.block.previous.request";
		
		/** The Constant LBL_SUCCESS_PARTICIPANT_REQUEST_REGISTER. */
		public static final String LBL_SUCCESS_ISSUER_REQUEST_REGISTER = "lbl.success.issuerrequest.register";
		
		/** The Constant ADM_ISSUER_REQUEST_VALIDATION_SEARCH_STATE_REQUEST. */
		public static final String ADM_ISSUER_REQUEST_VALIDATION_SEARCH_STATE_REQUEST = "adm.issuerrequest.validation.search.staterequest";
		
		/** The Constant ADM_ISSUER_REQUEST_VALIDATION_SEARCH_INI_DATE_REQ. */
		public static final String ADM_ISSUER_REQUEST_VALIDATION_SEARCH_INI_DATE_REQ = "adm.issuerrequest.validation.search.inidate.req";
		
		/** The Constant ADM_ISSUER_REQUEST_VALIDATION_SEARCH_END_DATE_REQ. */
		public static final String ADM_ISSUER_REQUEST_VALIDATION_SEARCH_END_DATE_REQ = "adm.issuerrequest.validation.search.enddate.req";
		
		/** The Constant ADM_ISSUER_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_SYS_DATE. */
		public static final String ADM_ISSUER_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_SYS_DATE = "adm.issuerrequest.validation.search.inidate.great.sysdate";
		
		/** The Constant ADM_ISSUER_REQUEST_VALIDATION_SEARCH_END_DATE_GREAT_SYS_DATE. */
		public static final String ADM_ISSUER_REQUEST_VALIDATION_SEARCH_END_DATE_GREAT_SYS_DATE = "adm.issuerrequest.validation.search.enddate.great.sysdate";
		
		/** The Constant ADM_ISSUER_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_END_DATE. */
		public static final String ADM_ISSUER_REQUEST_VALIDATION_SEARCH_INI_DATE_GREAT_END_DATE = "adm.issuerrequest.validation.search.inidate.great.enddate";
		
		/** The Constant ADM_ISSUER_REQUEST_VALIDATION_SEARCH_END_DATE_LESS_INI_DATE. */
		public static final String ADM_ISSUER_REQUEST_VALIDATION_SEARCH_END_DATE_LESS_INI_DATE = "adm.issuerrequest.validation.search.enddate.less.inidate";
		
		/** The Constant ADM_ISSUER_REQUEST_VALIDATION_CONFIRM_PARTICIPANT_NOT_REGISTERED. */
		public static final String ADM_ISSUER_REQUEST_VALIDATION_CONFIRM_ISSUER_NOT_REGISTERED = "adm.issuerrequest.validation.confirm.issuer.not.registered";
		
		/** The Constant ADM_ISSUER_REQUEST_VALIDATION_CONFIRM_PARTICIPANT_NOT_BLOCKED. */
		public static final String ADM_ISSUER_REQUEST_VALIDATION_CONFIRM_ISSUER_NOT_BLOCKED = "adm.issuerrequest.validation.confirm.issuer.not.blocked";
		
		/** The Constant ADM_ISSUER_REQUEST_VALIDATION_CONFIRM_PARTICIPANT_NOT_REGISTERED_OR_BLOCKED. */
		public static final String ADM_ISSUER_REQUEST_VALIDATION_CONFIRM_PARTICIPANT_NOT_REGISTERED_OR_BLOCKED = "adm.issuerrequest.validation.confirm.participant.not.registered.or.blocked";
		
		/** The Constant LBL_CONFIRM_PARTICIPANT_REQUEST_CONFIRMATION. */
		public static final String LBL_CONFIRM_ISSUER_REQUEST_CONFIRMATION = "lbl.confirm.issuerrequest.confirmation";

		/** The Constant LBL_CONFIRM_PARTICIPANT_REQUEST_CONFIRMATION. */
		public static final String LBL_CONFIRM_ISSUER_ONFIRMATION = "lbl.confirm.issuer.confirmation";
		
		/** The Constant LBL_SUCCESS_PARTICIPANT_REQUEST_CONFINRMATION. */
		public static final String LBL_SUCCESS_ISSUER_REQUEST_CONFINRMATION = "lbl.success.issuerrequest.confirmation";
		
		/** The Constant ADM_ISSUER_REQUEST_VALIDATION_REJECT_MOTIVE. */
		public static final String ADM_ISSUER_REQUEST_VALIDATION_REJECT_MOTIVE = "adm.issuerrequest.validation.reject.motive";
		
		/** The Constant ADM_ISSUER_REQUEST_VALIDATION_REJECT_OTHER_MOTIVE. */
		public static final String ADM_ISSUER_REQUEST_VALIDATION_REJECT_OTHER_MOTIVE= "adm.issuerrequest.validation.reject.other.motive";
		
		/** The Constant ADM_ISSUER_REQUEST_VALIDATION_ISSUER_NOTFOUND_BY_DOC. */
		public static final String ADM_ISSUER_REQUEST_VALIDATION_ISSUER_NOTFOUND_BY_DOC="adm.issuerrequest.validation.issuer.notfound.by.doc";
		
		/** The Constant ADM_ISSUER_REQUEST_VALIDATION_NOT_IN_REGISTERED_STATE. */
		public static final String ADM_ISSUER_REQUEST_VALIDATION_NOT_IN_REGISTERED_STATE="adm.issuerrequest.validation.issuerrequest.not.registered.state";

		/** The Constant ADM_ISSUER_REQUEST_VALIDATION_NOT_IN_REGISTERED_STATE. */
		public static final String ADM_ISSUER_REQUEST_VALIDATION_NOT_IN_PENDIG_STATE="adm.issuerrequest.validation.issuerrequest.not.pendig.state";
		
		/** The Constant LBL_CONFIRM_PARTICIPANT_REQUEST_REJECT. */
		public static final String LBL_CONFIRM_ISSUER_REQUEST_REJECT = "lbl.confirm.issuerrequest.reject";
		
		/** The Constant LBL_SUCCESS_PARTICIPANT_REQUEST_REJECT. */
		public static final String LBL_SUCCESS_ISSUER_REQUEST_REJECT = "lbl.success.issuerrequest.reject";
		
		/** The Constant LBL_CONFIRM_ISSUER_MODIFICATION_REQUEST_REGISTER. */
		public static final String LBL_CONFIRM_ISSUER_MODIFICATION_REQUEST_REGISTER = "lbl.confirm.issuer.modification.request.register";
		
		/** The Constant LBL_SUCCESS_ISSUER_MODIFICATION_REQUEST_REGISTER. */
		public static final String LBL_SUCCESS_ISSUER_MODIFICATION_REQUEST_REGISTER = "lbl.success.issuer.modification.request.register";
		
		/** The Constant ERROR_ADM_MODIF_ISSUER_NOT_EXISTS_REGISTERED. */
		public static final String  ERROR_ADM_MODIF_ISSUER_NOT_EXISTS_REGISTERED = "adm.issuer.modification.validate.issuer.not.registered";
		
		/** The Constant ADM_ISSUER_VALIDATION_EMAIL_FORMAT. */
		public static final String ADM_ISSUER_VALIDATION_EMAIL_FORMAT = "adm.issuer.validation.email.format";
		
		/** The Constant ADM_ISSUER_VALIDATION_CONTACT_EMAIL_FORMAT. */
		public static final String ADM_ISSUER_VALIDATION_CONTACT_EMAIL_FORMAT = "adm.issuer.validation.contactemail.format";
		
		/** The Constant ADM_ISSUER_VALIDATION_ADDRESS_FORMAT. */
		public static final String ADM_ISSUER_VALIDATION_ADDRESS_FORMAT = "adm.issuer.validation.address.format";
		
		/** The Constant ADM_ISSUER_VALIDATION_WEB_PAGE_FORMAT. */
		public static final String ADM_ISSUER_VALIDATION_WEB_PAGE_FORMAT = "adm.issuer.validation.web.page.format";
		
		/** The Constant ERROR_ADM_ISSUER_FILE_NO_COMPLETE. */
		public static final String ERROR_ADM_ISSUER_FILE_NO_COMPLETE = "error.adm.issuer.file.no.complete";
		
		/** The Constant ADM_ISSUER_HOLDER_WILL_BE_CREATED. */
		public static final String ADM_ISSUER_HOLDER_WILL_BE_CREATED = "adm.issuer.holder.will.be.created";

		/** The Constant ADM_ISSUER_HOLDER_WILL_BE_CREATED. */
		public static final String ADM_ISSUER_WILL_BE_CREATED = "adm.issuer.will.be.created";
		
		/** The Constant ADM_ISSUER_HOLDER_FOUND_BLOCKED. */
		public static final String ADM_ISSUER_HOLDER_FOUND_BLOCKED = "adm.issuer.holder.found.blocked";
		
		/** The Constant ADM_ISSUER_HOLDER_FOUND_NOT_REGISTERED. */
		public static final String ADM_ISSUER_HOLDER_FOUND_NOT_REGISTERED = "adm.issuer.holder.found.not.registered";
		
		/** The Constant AMD_PARTICIPANT_HOLDER_DOCUMENT_DEFAULT_NAME_PREFIX. */
		public static final String AMD_ISSUER_HOLDER_DOCUMENT_DEFAULT_NAME_PREFIX = "adm.issuer.holder.document.default.name.prefix";
		
		/** The Constant AMD_PARTICIPANT_HOLDER_DOCUMENT_DEFAULT_CONTENT. */
		public static final String AMD_ISSUER_HOLDER_DOCUMENT_DEFAULT_CONTENT = "adm.issuer.holder.document.default.content";
		
		/** The Constant AMD_PARTICIPANT_HOLDER_DOCUMENT_DEFAULT_EXTENSION. */
		public static final String AMD_ISSUER_HOLDER_DOCUMENT_DEFAULT_EXTENSION = "adm.issuer.holder.document.default.extension";
				
		/** The Constant ADM_ISSUER_VALIDATION_PHONE_ONLY_ZERO. */
		public static final String ADM_ISSUER_VALIDATION_PHONE_ONLY_ZERO = "adm.issuer.validation.phone.only.zero";
		
		/** The Constant ADM_ISSUER_VALIDATION_FAX_ONLY_ZERO. */
		public static final String ADM_ISSUER_VALIDATION_FAX_ONLY_ZERO = "adm.issuer.validation.fax.only.zero";
		
		/** The Constant ADM_ISSUER_VALIDATION_CONTACT_PHONE_ONLY_ZERO. */
		public static final String ADM_ISSUER_VALIDATION_CONTACT_PHONE_ONLY_ZERO = "adm.issuer.validation.contact.phone.only.zero";

		/** The Constant AMD_ISSUER_REPRESENTATIVE_MESSAGE_IMPORT. */
		public static final String AMD_ISSUER_REPRESENTATIVE_MESSAGE_IMPORT = "adm.issuer.representative.message.import";
		
		/** The Constant ERROR_AMD_ISSUER_DIFFERENTIATED_REPRE_NO_RESIDENT. */
		public static final String ERROR_AMD_ISSUER_DIFFERENTIATED_REPRE_NO_RESIDENT = "error.adm.issuer.differentiated.representative.no.resident";
		
		/** The Constant ADM_ISSUER_OTHER_HOLDER_FOUND_REGISTERED. */
		public static final String ADM_ISSUER_OTHER_HOLDER_FOUND_REGISTERED="adm.issuer.other.holder.found.registered";
		
		/** The Constant ADM_ISSUER_VALIDATION_CONTACT_PHONE_ONLY_ZERO. */
		public static final String ADM_ISSUER_VALIDATION_HOLDER = "adm.issuer.holder.document.default.holder";
		// END - ISSUER
		
		//START - GENERAL
		/** The Constant EXP_REGULAR_ALPHANUMERIC. */
		public static final String EXP_REGULAR_ALPHANUMERIC = "exp.regular.alphanumeric";
		
		/** The Constant EXP_REGULAR_NUMERIC_INTEGER. */
		public static final String EXP_REGULAR_NUMERIC_INTEGER = "exp.regular.numeric.integer";
		
		/** The Constant MESSAGE_DATA_REQUIRED. */
		public static final String MESSAGE_DATA_REQUIRED = "message.data.required";
		
		/** The Constant MESSAGE_DATA_REQUIRED. */
		public static final String OBSERVATION_DATA_REQUIRED = "observation.data.required";
		
		/** The Constant ERROR_RECORD_NOT_SELECTED. */
		public static final String ERROR_RECORD_NOT_SELECTED = "error.record.not.selected";
		
		/** The Constant ERROR_GENERATION_FILE_NOT_ALLOWED. */
		public static final String ERROR_GENERATION_FILE_NOT_ALLOWED = "security.generation.file.user.not.allowed";
		
		/** The Constant ERROR_RECORD_SELECTED_NOT_REGISTERED_STATE. */
		public static final String ERROR_RECORD_SELECTED_NOT_REGISTERED_STATE = "error.record.selected.not.registered.state";

		/** The Constant LBL_CONFIRM_HEADER_RECORD. */
		public static final String LBL_CONFIRM_HEADER_RECORD = "lbl.confirm.header.register";
		
		/** The Constant ERROR_ADM_REPRESENTATIVE_DATE_BIRTHDAY_DATE_TODAY. */
		public static final String ERROR_ADM_REPRESENTATIVE_DATE_BIRTHDAY_DATE_TODAY = "error.representative.date.birthday.date.today";
		
		/** The Constant LBL_WARNING_ACTION. */
		public static final String LBL_WARNING_ACTION="lbl.warning.action";
		
		/** The Constant ERROR_ADM_REPRESENTATIVE_DATE_BIRTHDAY_DATE_ONENINEZEROZEO. */
		public static final String ERROR_ADM_REPRESENTATIVE_DATE_BIRTHDAY_DATE_ONENINEZEROZEO="error.representative.date.birthday.date.oneninezerozero";
		
		/** The Constant ERROR_AMD_REPRESENTATIVE_AGE_NO_MAJOR. */
		public static final String ERROR_AMD_REPRESENTATIVE_AGE_NO_MAJOR = "error.representative.age.no.major";
		
		/** The Constant ERROR_REPRESENTATIVE_DOC_NUM_LEGTH_9_11. */
		public static final String ERROR_REPRESENTATIVE_DOC_NUM_LEGTH_9_11 = "error.representative.docnumber.lenght.11.or.9";
		
		/** The Constant ERROR_REPRESENTATIVE_DOC_NUM_CHECK_DIGIT. */
		public static final String ERROR_REPRESENTATIVE_DOC_NUM_CHECK_DIGIT = "error.representative.docnumber.check.digit.invalid";
		
		/** The Constant ERROR_REPRESENTATIVE_DOC_NUM_LEGTH_6_15. */
		public static final String ERROR_REPRESENTATIVE_DOC_NUM_LEGTH_6_15= "error.representative.docnumber.lenght.6.to.15";
		
		/** The Constant ERROR_REPRESENTATIVE_DOC_NUM_LEGTH_11. */
		public static final String ERROR_REPRESENTATIVE_DOC_NUM_LEGTH_11 = "error.representative.docnumber.lenght.11";
		
		/** The Constant WARNING_FILE_REQUEST_REPRESENTATIVE_NO_COMPLETE. */
		public static final String WARNING_FILE_REQUEST_REPRESENTATIVE_NO_COMPLETE = "warning.file.representative.no.complete";
		
		/** The Constant WARNING_NOT_FILE_REPRESENTATIVE_CERTIFICATION. */
		public static final String WARNING_NOT_FILE_REPRESENTATIVE_CERTIFICATION = "warning.not.file.representative.certification";
		
		/** The Constant WARNING_SAME_NATIONALITY. */
		public static final String WARNING_SAME_NATIONALITY = "warning.same.nationality";
		
		/** The Constant ERROR_SAME_TYPE_DOCUMENT. */
		public static final String ERROR_SAME_TYPE_DOCUMENT = "error.same.type.document";
		
		/** The Constant WARNING_DELETE_REPRESENTATIVE_NO_RESIDENT. */
		public static final String WARNING_DELETE_REPRESENTATIVE_NO_RESIDENT = "warning.delete.representative.no.resident";
		//END - GENERAL
		
		/** The Constant LABEL_VIEW_COUPONS. */
		public static final String LABEL_VIEW_COUPONS = "payment.cronogram.query.lbl.view.coupons";
		
		/** The Constant LABEL_VIEW. */
		public static final String LABEL_VIEW = "lbl.view";
		
		/** The Constant LABEL_NO_COUPONS_PAYMENT_CRON_QUERY. */
		public static final String LABEL_NO_COUPONS_PAYMENT_CRON_QUERY = "payment.cronogram.query.alert.no.copons";
		
		/**  The Constant for displaying the session expired message on Idle time. */
		public static final String IDLE_TIME_EXPIRED = "idle.session.expired";
		
		// PROPERTIES CONSTANST FOR VALUATOR TERM RANGES
		
		/** The Constant VALUATOR PROCESS CONFIG RANGE REGISTER SUCCES. */
		public static final String VALUATOR_PROCESS_CONFIG_RANGE_REGISTER_SUCCES="valuator.process.config.range.msg.register.success";
		/** The Constant VALUATOR PROCESS CONFIG RANGE MODIFY SUCCESS. */
		public static final String VALUATOR_PROCESS_CONFIG_RANGE_MODIFY_SUCCESS="valuator.process.config.range.msg.modify.success";
		/** The Constant VALUATOR PROCESS CONFIG RANGE REGISTER CONFIRM. */
		public static final String VALUATOR_PROCESS_CONFIG_RANGE_REGISTER_CONFIRM="valuator.process.config.range.lbl.register.confirm";
		/** The Constant VALUATOR PROCESS CONFIG RANGE MODIFY SUCCESS. */
		public static final String VALUATOR_PROCESS_CONFIG_RANGE_MODIFY_CONFIRM="valuator.process.config.range.lbl.modify.confirm";
		/** The Constant VALUATOR PROCESS CONFIG RANGE DELETE CONFIRM. */
		public static final String VALUATOR_PROCESS_CONFIG_RANGE_DELETE_CONFIRM="valuator.process.config.range.lbl.delete.confirm";
		/** The Constant VALUATOR PROCESS CONFIG RANGE DELETE SUCCESS. */
		public static final String VALUATOR_PROCESS_CONFIG_RANGE_DELETE_SUCCESS="valuator.process.config.range.msg.delete.success";
		
		// END - PROPERTIES CONSTANST FOR VALUATOR TERM RANGES

		// PROPERTIES CONSTANST FOR VALUATOR SIMULATOR
		
		/** The Constant VALUATOR PROCESS SIMULATOR REGISTER SUCCES. */
		public static final String VALUATOR_PROCESS_SIMULATION_REGISTER_SUCCES="valuator.process.simulation.msg.register.success";
		/** The Constant VALUATOR PROCESS SIMULATOR REGISTER CONFIRM. */
		public static final String VALUATOR_PROCESS_SIMULATION_REGISTER_CONFIRM="valuator.process.simulation.lbl.register.confirm";
		
		/** The Constant VALUATOR PROCESS SIMULATOR REGISTER CONFIRM. */
		public static final String VALUATOR_PROCESS_SIMULATION_BALANCE_SEARCH_BUTTON="valuator.process.simulation.balance.search.button";
		/** The Constant VALUATOR PROCESS SIMULATOR REGISTER CONFIRM. */
		public static final String VALUATOR_PROCESS_SIMULATION_CODE_SEARCH_BUTTON="valuator.process.simulation.code.search.button";
		/** The Constant VALUATOR PROCESS SIMULATOR REGISTER CONFIRM. */
		public static final String VALUATOR_PROCESS_SIMULATION_BALANCE_NO_RESULTS="valuator.process.simulation.balance.no.results";
		/** The Constant VALUATOR PROCESS SIMULATOR REGISTER CONFIRM. */
		public static final String VALUATOR_PROCESS_SIMULATION_CODE_NO_RESULTS="valuator.process.simulation.code.no.results.balance";
		
		
		/** The Constant MSG VALIDATION CUI NOT FOUND. */
		public static final String MSG_VALIDATION_CUI_NOT_FOUND="msg.valuator.simulation.validation.cui.not.found";
		
		/** The Constant MSG VALIDATION HOLDER ACCOUNT NOT REGISTER. */
		public static final String MSG_VALIDATION_HOLDER_NOT_REGISTER="msg.valuator.simulation.validation.holder.not.register";
		
		/** The Constant MSG VALIDATION CUI NOT FOUND. */
		public static final String MSG_VALIDATION_HOLDER_ACCOUNT_NOT_FOUND="msg.valuator.simulation.validation.holder.account.not.found";
		
		/** The Constant MSG VALIDATION HOLDER ACCOUNT NOT REGISTER. */
		public static final String MSG_VALIDATION_HOLDER_ACCOUNT_NOT_REGISTER="msg.valuator.simulation.validation.holder.account.not.register";
		
		/** The Constant ISSUER_INVESTMENT_FUNDS_CLOSED. */
		public static final String ISSUER_INVESTMENT_FUNDS_CLOSED="issuer.investment.funds.closed";
		
		/** The Constant ISSUER_ONLY_CREATE. */
		public static final String ISSUER_ONLY_CREATE = "issuer.only.create";
		
		/** The Constant ISSUER_SOCIETY_SECURITIZATION. */
		public static final String ISSUER_SOCIETY_SECURITIZATION="issuer.society.securitization";
		// END - PROPERTIES CONSTANST FOR VALUATOR TERM RANGES
		
		/**  THE CONSTANT FOR SECURITY CLASS VALUATOR*. */
		public static final String VALUATOR_PROCESS_REGISTER_CONFIRM = "valuator.process.register.confirm";
		
		/** The Constant VALUATOR_PROCESS_REGISTER_CONFIRM_OK. */
		public static final String VALUATOR_PROCESS_REGISTER_CONFIRM_OK="valuator.process.register.confirm.ok";
		
		/** The Constant VALUATOR_PROCESS_UPDATE_CONFIRM. */
		public static final String VALUATOR_PROCESS_UPDATE_CONFIRM = "valuator.process.update.confirm";
		
		/** The Constant VALUATOR_PROCESS_UPDATE_CONFIRM_OK. */
		public static final String VALUATOR_PROCESS_UPDATE_CONFIRM_OK="valuator.process.update.confirm.ok";
		
		/** The Constant VALUATOR_PROCESS_DELETE_CONFIRM. */
		public static final String VALUATOR_PROCESS_DELETE_CONFIRM = "valuator.process.delete.confirm";
		
		/** The Constant VALUATOR_PROCESS_DELETE_CONFIRM_OK. */
		public static final String VALUATOR_PROCESS_DELETE_CONFIRM_OK="valuator.process.delete.confirm.ok";
		
		/** The Constant VALUATOR_PROCESS_MSG_NO_SELECTED. */
		public static final String VALUATOR_PROCESS_MSG_NO_SELECTED="valuator.process.msg.no.selected";
		
		/** The Constant VALUATOR_PROCESS_CLASS_CHANGE_INSTRUMENT_TYPE_CONFIRM. */
		public static final String VALUATOR_PROCESS_CLASS_CHANGE_INSTRUMENT_TYPE_CONFIRM = "valuator.process.class.change.instrument.type.confirm";
		
		/** The Constant COMMONS_LABEL_PARTICIPANT_CUI_INCORRECT. */
		public static final String COMMONS_LABEL_PARTICIPANT_CUI_INCORRECT = "commons.label.participant.cui.incorrect";
		
		/**  CONSTANT MARKETFACT FILE *. */
		public static final String VALUATOR_CORE_MARKETFACT_REGISTER_CONFIRM = "valuator.core.marketfact.register.confirm";
		
		/** The Constant VALUATOR_CORE_MARKETFACT_REGISTER_CONFIRM_OK. */
		public static final String VALUATOR_CORE_MARKETFACT_REGISTER_CONFIRM_OK="valuator.core.marketfact.register.confirm.ok";
		
		/** The Constant VALUATOR_CORE_MARKETFACT_FILE_EMPTY_FIELDS. */
		public static final String VALUATOR_CORE_MARKETFACT_FILE_EMPTY_FIELDS="valuator.core.marketfact.file.empty.fields";
		
		/** The Constant VALUATOR_CORE_MARKETFACT_FILE_INCOMPLETE. */
		public static final String VALUATOR_CORE_MARKETFACT_FILE_INCOMPLETE="valuator.core.marketfact.file.incomplete";
		
		/** The Constant VALUATOR_CORE_MARKETFACT_CHANGE_FILE_SOURCE_CONFIRM. */
		public static final String VALUATOR_CORE_MARKETFACT_CHANGE_FILE_SOURCE_CONFIRM = "valuator.core.marketfact.change.file.source.confirm";
		
		/** The Constant VALUATOR_EXECUTOR_PROCESS_MESSAGE_OK. */
		public static final String VALUATOR_EXECUTOR_PROCESS_MESSAGE_OK="valuator.executor.process.message.ok";
		
		/** The Constant VALUATOR_EXECUTOR_PROCESS_MESSAGE_OK_PARTICIPANT. */
		public static final String VALUATOR_EXECUTOR_PROCESS_MESSAGE_OK_PARTICIPANT="valuator.executor.process.message.ok.participant";
		
		/** The Constant VALUATOR_EXECUTOR_PROCESS_MESSAGE_OK_CUI. */
		public static final String VALUATOR_EXECUTOR_PROCESS_MESSAGE_OK_CUI="valuator.executor.process.message.ok.cui";
		
		/** The Constant VALUATOR_EXECUTOR_PROCESS_MESSAGE_OK_INCONSISTENCE. */
		public static final String VALUATOR_EXECUTOR_PROCESS_MESSAGE_OK_INCONSISTENCE="valuator.executor.process.message.ok.inconsistence";
		
		/** Validation send security to bbv*/
		public static final String SEND_SECURITY_REGISTER_CONFIRM = "send.security.register.confirm";
		/** mensaje de respuesta */
		public static final String SEND_SECURITY_SUCCESS_CONFIRM = "send.security.success.confirm";
		
		/** The Constant DIALOG_HEADER_CONFIRM. */
		// confirmation dialogs headers
		public static final String DIALOG_HEADER_CONFIRM="lbl.header.alert.confirm";
		
		
		/** The Constant DIALOG_HEADER_CONFIRM. */
		// confirmation dialogs headers
		public static final String CONFIRM_EXPIRATION_SECURITY="security.expiration.confirm.ok";
		
		/** The Confirm Validations Balance Available Not Found*/
		public static final String CONFIRM_EXPIRATION_BALANCES_NOT_FOUND= "msg.retirement.validation.balances.not.found";
		

		/** The Confirm Validations Balance Available Not Found*/
		public static final String CONFIRM_EXPIRATION_WITHDRAW= "msg.process.expired.confirm.ok";
		
		public static final String CONFIRM_EXPIRATION_SECURITY_BY_ISSUER="security.expiration.by.issuer.confirm.ok";
		
		public static final String CONFIRM_EXPIRATION_SECURITY_BY_ISSUER_NOT_FOUND="security.expiration.by.issuer.not.found";
		
		public static final String CONFIRM_EXPIRATION_SECURITY_BY_ISSUER_SUCESS_SAVE="security.expiration.by.issuer.sucess.save";
		
		// issue 891: implementacion de eliminacion de valor
		public static final String MESSAGES_CONFIRM = "messages.confirm";
		public static final String MESSAGES_SUCCESFUL="messages.succesful";
		public static final String MSG_CONFIRM_REGISTER_SECURITYREMOVED="msg.confirm.register.securityRemoved";
		public static final String MSG_VALIDATION_REQUIRED_SECURITY_AUTHORIZED="msg.validation.required.security.authorized";
		public static final String MSG_VALIDATION_REQUIRED_REGISTRYDATE_EQUALS_CURRENTDATE="msg.validation.required.registryDate.equals.currentDate";
		public static final String MSG_SUCCESFUL_REGISTER_SECURITYREMOVED="msg.succesful.register.securityRemoved";
		public static final String LBL_MSG_CONFIRM_APPROVED_SECURITYREMOVED="lbl.msg.confirm.approved.securityRemoved";
		public static final String MSG_SUCCESFUL_APPROVED_SECURITYREMOVED="msg.succesful.approved.securityRemoved";
		public static final String LBL_MSG_CONFIRM_ANNULATE_SECURITYREMOVED="lbl.msg.confirm.annulate.securityRemoved";
		public static final String MSG_SUCCESFUL_ANNULATE_SECURITYREMOVED="msg.succesful.annulate.securityRemoved";
		public static final String LBL_MSG_CONFIRM_REJECT_SECURITYREMOVED="lbl.msg.confirm.reject.securityRemoved";
		public static final String MSG_SUCCESFUL_REJECT_SECURITYREMOVED="msg.succesful.reject.securityRemoved";
		public static final String LBL_MSG_CONFIRM_CONFIRMED_SECURITYREMOVED="lbl.msg.confirm.confirmed.securityRemoved";
		public static final String MSG_SUCCESFUL_CONFIRMED_SECURITYREMOVED="msg.succesful.confirmed.securityRemoved";
		public static final String MSG_ERROR_SECURITYREMOVED_REQUIRED_SELECTED="msg.error.securityRemoved.required.selected";
		public static final String MSG_ERROR_SECURITYREMOVED_REQUIRED_STATEREGISTERED="msg.error.securityRemoved.required.stateRegistered";
		public static final String MSG_ERROR_SECURITYREMOVED_REQUIRED_STATEAPPROVED="msg.error.securityRemoved.required.stateApproved";
		
		public final static String ERROR_RANGE_NUMBER_SHARE="security.error.range.number.share";
}