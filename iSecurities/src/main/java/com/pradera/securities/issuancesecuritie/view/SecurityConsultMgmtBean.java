package com.pradera.securities.issuancesecuritie.view;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.OperationUserTO;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.business.to.SecurityConsultTO;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.HolderSearchTO;
import com.pradera.core.component.utils.DocumentValidator;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.securities.issuancesecuritie.facade.IssuanceSecuritiesServiceFacade;
import com.pradera.securities.issuancesecuritie.facade.SecurityQuoteServiceFacade;
import com.pradera.securities.issuancesecuritie.facade.SecurityServiceFacade;
import com.pradera.securities.issuancesecuritie.util.SecuritiesUtils;
import com.pradera.securities.issuer.facade.IssuerServiceFacade;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SecurityConsultMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class SecurityConsultMgmtBean extends GenericBaseBean implements Serializable{

	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The log. */
	@Inject
	private transient PraderaLogger log;
	
	/** The country residence. */
	@Inject 
	@Configurable 
	Integer countryResidence;
	
	/** The id issuer bc. */
	@Inject 
	@Configurable 
	String idIssuerBC;
	
	/** The id issuer tgn. */
	@Inject 
	@Configurable 
	String idIssuerTGN;
	
	/** The issuer helper bean. */
	
	/** The user info. */
	@Inject UserInfo userInfo;
	
	/** The user privilege. */
	@Inject UserPrivilege userPrivilege;
	
	/** The document validator. */
	@Inject DocumentValidator documentValidator;
	
	/** The securities utils. */
	@Inject SecuritiesUtils securitiesUtils;
	
	/** The valuator history detail bean. */
	@Inject
	ValuatorHistoryDetailBean valuatorHistoryDetailBean; 
	
	/** The logged issuer. */
	private Issuer loggedIssuer;
	
	/** The general parameter facade. */
	@EJB
	private GeneralParametersFacade generalParameterFacade;
	
	/** The issuance securities service facade. */
	@EJB
	private IssuanceSecuritiesServiceFacade issuanceSecuritiesServiceFacade; 
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	/** The issuer service facade. */
	@EJB
	private IssuerServiceFacade issuerServiceFacade;
	
	/** The security quote facade. */
	@EJB SecurityQuoteServiceFacade securityQuoteFacade;
	
	/** The secuity service facade. */
	@EJB
	private SecurityServiceFacade securityServiceFacade;
	
	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
	/** The holder helper search*/
	private Holder holder;
	
	/** The issuer helper search. */
	private Issuer issuerHelperSearch;
	
	/** The issuance data model. */
	private IssuanceDataModel issuanceDataModel;
	
	/** The security data model. */
	private GenericDataModel<SecurityConsultTO> securityDataModel;
	
	/** The search filters concat. */
	private String searchFiltersConcat;
	
	
	/** The lst cbo instrument type. */
	private List<ParameterTable> lstCboInstrumentType;
		
	/** The lst cbo securitie class. */
	private List<ParameterTable> lstCboSecuritieClass;
	
	/** The lst cbo filter securitie class. */
	private List<ParameterTable> lstCboFilterSecuritieClass;

	/** The lst cbo filter currency. */
	private List<ParameterTable> lstCboFilterCurrency;

	/** The parameter table to. */
	private ParameterTableTO parameterTableTO;
    
    /** The lst security selection. */
    private List<Security> 			 lstSecuritySelection;

    /** The security. */
	private Security security;
	
	/** The security t osearch. */
	private SecurityTO securityTOsearch;
		
	/**
	 * Instantiates a new security mgmt bean.
	 */
	public SecurityConsultMgmtBean(){

	}
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		try {
			super.setParametersTableMap(new HashMap<Integer,Object>());
			security=new Security();
			holder = new Holder();
			securityTOsearch=new SecurityTO();
			issuerHelperSearch=new Issuer();
			
			loadCboInstrumentType();
			loadCboFilterCurency();
			loadCboFilterSecuritieClass();
			//Solo Agencias de Bolsa Mostrara las Clase de Valor: DPF DPA 
			if(Validations.validateIsNotNullAndPositive(userInfo.getUserAccountSession().getParticipantCode())){
				//Si es Agencia de Bolsa
				if(Validations.validateListIsNotNullAndNotEmpty(accountsFacade.getParticipantForAgencia(userInfo.getUserAccountSession().getParticipantCode()))){
					
					List<ParameterTable> lstSecuritieClass = new ArrayList<ParameterTable>();
					List<Integer>lstSecurityClass = new ArrayList<Integer>();
					for(ParameterTable objParameterTable : lstCboFilterSecuritieClass){
						if(objParameterTable.getParameterTablePk().equals(SecurityClassType.DPA.getCode()) || 
								objParameterTable.getParameterTablePk().equals(SecurityClassType.DPF.getCode())){
							lstSecuritieClass.add(objParameterTable);
							lstSecurityClass.add(objParameterTable.getParameterTablePk());
						}
					}
					lstCboFilterSecuritieClass = lstSecuritieClass;
					securityTOsearch.setLstSecurityClass(lstSecurityClass);
				}
			}
			setViewOperationType(ViewOperationsType.CONSULT.getCode());
			searchFiltersConcat=buildCiteriaSearchConcat();
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire( new ExceptionToCatchEvent(e) );
		}
	}
	
	/**
	 * Load combo instrument type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCboInstrumentType() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
		lstCboInstrumentType=generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}
	
	/**
	 * Load combo filter currency.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboFilterCurency() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.CURRENCY.getCode() );
		lstCboFilterCurrency=generalParameterFacade.getListParameterTableServiceBean( parameterTableTO );
		billParametersTableMap(lstCboFilterCurrency, true);
	}
	
	
	

	/**
	 * Load combo filter securities class.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboFilterSecuritieClass() throws ServiceException {
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.SECURITIES_CLASS.getCode() );
		lstCboFilterSecuritieClass=generalParameterFacade.getListParameterTableServiceBean( parameterTableTO );
		billParametersTableMap(lstCboFilterSecuritieClass,true);
	}
	
	/**
	 * Bill parameters table map.
	 *
	 * @param lstParameterTable the list parameter table
	 * @param isObj the is object
	 */
	public void billParametersTableMap(List<ParameterTable> lstParameterTable, boolean isObj){
		for(ParameterTable pTable : lstParameterTable){
			if(isObj){
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable);
			}else{
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable.getParameterName());
			}
		}
	}
	
	/**
	 * Change filter field.
	 */
	public void changeFilterField(){
		try {
			String strSearchFiltersAux=buildCiteriaSearchConcat();
			
			if(!strSearchFiltersAux.equals(searchFiltersConcat)){
				securityDataModel=null;
				security=new Security();
			}
			
			searchFiltersConcat=buildCiteriaSearchConcat();
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	
	/**
	 * Validate is valid user.
	 *
	 * @param security the security
	 * @return the list
	 */
	private List<String> validateIsValidUser(Security security) {	
		List<OperationUserTO> lstOperationUserParam = new ArrayList<OperationUserTO>();
		List<String> lstOperationUserResult;	
		OperationUserTO operationUserTO = new OperationUserTO();  
		operationUserTO.setOperNumber(security.getIdSecurityCodePk());
		operationUserTO.setUserName(security.getRegistryUser());
		lstOperationUserParam.add(operationUserTO);
		lstOperationUserResult = getIsSubsidiary(userInfo,lstOperationUserParam);
		return lstOperationUserResult;
	}
	
	
	
	
	
	/**
	 * Clean security search handler.
	 *
	 * @param event the event
	 */
	public void cleanSecuritySearchHandler(ActionEvent event){
		try { 
			securityDataModel=null;
			securityTOsearch=new SecurityTO();
			security=null;
			holder =new Holder();
			securityTOsearch.setIssuanceDate(null);
			securityTOsearch.setExpirationDate(null);
			
			searchFiltersConcat=buildCiteriaSearchConcat();
			
			if((userInfo.getUserAccountSession().isIssuerInstitucion()|| userInfo.getUserAccountSession().isIssuerDpfInstitucion()) &&
					Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getIssuerCode())){
				Issuer issuerFilter=new Issuer();
				issuerFilter.setIdIssuerPk( userInfo.getUserAccountSession().getIssuerCode() );
				loggedIssuer=new Issuer();
				loggedIssuer=issuerServiceFacade.findIssuerByFiltersServiceFacade(issuerFilter);
				
			} else {
				issuerHelperSearch = new Issuer();
			}
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	
	/**
	 * Search securities handler.
	 *
	 * @param event the event
	 */
	public void searchSecuritiesHandler(ActionEvent event){
		JSFUtilities.hideGeneralDialogues();
		try {
//			security=new Security();
			securityTOsearch.setIdIssuerCodePk( issuerHelperSearch.getIdIssuerPk() );
			securityTOsearch.setIdHolderPk( holder.getIdHolderPk());
			
				List<SecurityConsultTO> lstSecurity = null;
				if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
					Participant objParticipant = accountsFacade.getParticipantByPk(userInfo.getUserAccountSession().getParticipantCode());					
					securityTOsearch.setIdParticipant(objParticipant.getIdParticipantPk());
					HolderAccountTO holderAccountTO = new HolderAccountTO();
					holderAccountTO.setParticipantTO(objParticipant.getIdParticipantPk());
					List<Long> lstHolderPk = new ArrayList<Long>();
					HolderSearchTO holderSearchTO = new HolderSearchTO();
					holderSearchTO.setDocumentType(objParticipant.getDocumentType());
					holderSearchTO.setDocumentNumber(objParticipant.getDocumentNumber());
					holderSearchTO.setState(HolderStateType.REGISTERED.getCode());
					List<Holder> lstHolder = helperComponentFacade.findHolders(holderSearchTO);
					if(lstHolder!=null && lstHolder.size()>0){
						for(Holder objHolder : lstHolder){
							lstHolderPk.add(objHolder.getIdHolderPk());
						}
					}
					holderAccountTO.setLstHolderPk(lstHolderPk);
					List<HolderAccount> lstHolderAccount = null;
					if(holderAccountTO.getLstHolderPk()!=null && holderAccountTO.getLstHolderPk().size()>0){
						 lstHolderAccount = accountsFacade.getHolderAccountListComponentServiceFacade(holderAccountTO);
					}
					List<Long> lstHolderAccountPk = new ArrayList<Long>();
					if(lstHolderAccount!=null && lstHolderAccount.size()>0){
						for(HolderAccount objHolderAccount : lstHolderAccount){
							lstHolderAccountPk.add(objHolderAccount.getIdHolderAccountPk());
						}
					}
					securityTOsearch.setLstHolderAccountPk(lstHolderAccountPk);
//					if(securityTOsearch.getIdParticipant()!=null && securityTOsearch.getLstHolderAccountPk()!=null 
//							&& securityTOsearch.getLstHolderAccountPk().size()>0){
//						lstSecurity=securityServiceFacade.findSecuritiesListForParticipantInvestor(securityTOsearch);
//					}
				}else{
					 lstSecurity=securityServiceFacade.findSecuritiesForConsult(securityTOsearch);
				}
				securityDataModel=new GenericDataModel(lstSecurity);

			loadUserValidation();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}


	
	/**
	 * Builds the criteria search concat.
	 *
	 * @return the string
	 */
	public String buildCiteriaSearchConcat(){
		StringBuilder strbAux=new StringBuilder();
		strbAux.append(issuerHelperSearch.getIdIssuerPk());
//		strbAux.append(issuanceHelperSearch.getIdIssuanceCodePk());
		strbAux.append(securityTOsearch.getAlternativeCode());
		strbAux.append(securityTOsearch.getSecurityCurrency());
		strbAux.append(securityTOsearch.getSecurityState());
		strbAux.append(securityTOsearch.getSecurityClass());
		strbAux.append(securityTOsearch.getIssuanceDate());
		strbAux.append(securityTOsearch.getExpirationDate());
		return strbAux.toString();
	}
	
	/**
	 * Detail operation handler.
	 *
	 * @param securityParam the security param
	 * @return the string
	 */
	public String detailOperationHandler(Security securityParam) {
		String actionMessage = null;
		try {
			security=securityParam;

		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return actionMessage;
	}
	
	/**
	 * Load user validation.
	 */
	private void loadUserValidation() {
		// TODO Auto-generated method stub
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		if(userPrivilege.getUserAcctions().isModify()){
			privilegeComponent.setBtnModifyView(Boolean.TRUE);
		}
		if(userPrivilege.getUserAcctions().isBlock()){
			privilegeComponent.setBtnBlockView(Boolean.TRUE);
		}
		if(userPrivilege.getUserAcctions().isUnblock()){
			privilegeComponent.setBtnUnblockView(Boolean.TRUE);
		}
		this.userPrivilege.setPrivilegeComponent(privilegeComponent);
	}

	/**
	 * @return the issuanceSecuritiesServiceFacade
	 */
	public IssuanceSecuritiesServiceFacade getIssuanceSecuritiesServiceFacade() {
		return issuanceSecuritiesServiceFacade;
	}

	/**
	 * @param issuanceSecuritiesServiceFacade the issuanceSecuritiesServiceFacade to set
	 */
	public void setIssuanceSecuritiesServiceFacade(
			IssuanceSecuritiesServiceFacade issuanceSecuritiesServiceFacade) {
		this.issuanceSecuritiesServiceFacade = issuanceSecuritiesServiceFacade;
	}

	/**
	 * @return the issuerHelperSearch
	 */
	public Issuer getIssuerHelperSearch() {
		return issuerHelperSearch;
	}

	/**
	 * @param issuerHelperSearch the issuerHelperSearch to set
	 */
	public void setIssuerHelperSearch(Issuer issuerHelperSearch) {
		this.issuerHelperSearch = issuerHelperSearch;
	}

	/**
	 * @return the issuanceDataModel
	 */
	public IssuanceDataModel getIssuanceDataModel() {
		return issuanceDataModel;
	}

	/**
	 * @param issuanceDataModel the issuanceDataModel to set
	 */
	public void setIssuanceDataModel(IssuanceDataModel issuanceDataModel) {
		this.issuanceDataModel = issuanceDataModel;
	}


	/**
	 * @return the securityDataModel
	 */
	public GenericDataModel<SecurityConsultTO> getSecurityDataModel() {
		return securityDataModel;
	}

	/**
	 * @param securityDataModel the securityDataModel to set
	 */
	public void setSecurityDataModel(
			GenericDataModel<SecurityConsultTO> securityDataModel) {
		this.securityDataModel = securityDataModel;
	}

	/**
	 * @return the lstCboInstrumentType
	 */
	public List<ParameterTable> getLstCboInstrumentType() {
		return lstCboInstrumentType;
	}

	/**
	 * @param lstCboInstrumentType the lstCboInstrumentType to set
	 */
	public void setLstCboInstrumentType(List<ParameterTable> lstCboInstrumentType) {
		this.lstCboInstrumentType = lstCboInstrumentType;
	}

	/**
	 * @return the lstCboSecuritieClass
	 */
	public List<ParameterTable> getLstCboSecuritieClass() {
		return lstCboSecuritieClass;
	}

	/**
	 * @param lstCboSecuritieClass the lstCboSecuritieClass to set
	 */
	public void setLstCboSecuritieClass(List<ParameterTable> lstCboSecuritieClass) {
		this.lstCboSecuritieClass = lstCboSecuritieClass;
	}

	/**
	 * @return the lstCboFilterSecuritieClass
	 */
	public List<ParameterTable> getLstCboFilterSecuritieClass() {
		return lstCboFilterSecuritieClass;
	}

	/**
	 * @param lstCboFilterSecuritieClass the lstCboFilterSecuritieClass to set
	 */
	public void setLstCboFilterSecuritieClass(
			List<ParameterTable> lstCboFilterSecuritieClass) {
		this.lstCboFilterSecuritieClass = lstCboFilterSecuritieClass;
	}

	/**
	 * @return the lstCboFilterCurrency
	 */
	public List<ParameterTable> getLstCboFilterCurrency() {
		return lstCboFilterCurrency;
	}

	/**
	 * @param lstCboFilterCurrency the lstCboFilterCurrency to set
	 */
	public void setLstCboFilterCurrency(List<ParameterTable> lstCboFilterCurrency) {
		this.lstCboFilterCurrency = lstCboFilterCurrency;
	}

	/**
	 * @return the parameterTableTO
	 */
	public ParameterTableTO getParameterTableTO() {
		return parameterTableTO;
	}

	/**
	 * @param parameterTableTO the parameterTableTO to set
	 */
	public void setParameterTableTO(ParameterTableTO parameterTableTO) {
		this.parameterTableTO = parameterTableTO;
	}

	/**
	 * @return the lstSecuritySelection
	 */
	public List<Security> getLstSecuritySelection() {
		return lstSecuritySelection;
	}

	/**
	 * @param lstSecuritySelection the lstSecuritySelection to set
	 */
	public void setLstSecuritySelection(List<Security> lstSecuritySelection) {
		this.lstSecuritySelection = lstSecuritySelection;
	}

	/**
	 * @return the securityTOsearch
	 */
	public SecurityTO getSecurityTOsearch() {
		return securityTOsearch;
	}

	/**
	 * @param securityTOsearch the securityTOsearch to set
	 */
	public void setSecurityTOsearch(SecurityTO securityTOsearch) {
		this.securityTOsearch = securityTOsearch;
	}

	/**
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * @param holder the holder to set
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * @param security the security to set
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}
	
	

	
}
