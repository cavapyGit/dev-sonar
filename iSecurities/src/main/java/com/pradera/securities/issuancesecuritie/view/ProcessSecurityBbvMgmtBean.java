package com.pradera.securities.issuancesecuritie.view;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.xml.transform.TransformerConfigurationException;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.SecuritySendBbv;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.securities.issuancesecuritie.facade.ProcessSecurityBbvFacade;
import com.pradera.securities.issuancesecuritie.to.ResumeSecuritySendBbvTO;
import com.pradera.securities.issuancesecuritie.util.CouponUtilXml;
import com.pradera.securities.issuancesecuritie.util.PropertiesConstants;
import com.pradera.securities.issuancesecuritie.util.SecurityUtilXml;

@DepositaryWebBean
@LoggerCreateBean
public class ProcessSecurityBbvMgmtBean extends GenericBaseBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	UserPrivilege userPrivilege;
	
	@Inject
	UserInfo userInfo;
	
	private Integer stockQuantity;
	private GenericDataModel<ResumeSecuritySendBbvTO> listSecuritySendBbvDataModel;
	private GenericDataModel<SecuritySendBbv> listRevertionDataMode;
	private GenericDataModel<SecuritySendBbv> listSendedDataMode;
	
	@EJB
	private ProcessSecurityBbvFacade processSecurityBbvFacade;
	
	private List<Security> listSecurity;
	private List<SecuritySendBbv> listSecurityRedention;
	private String fechaActual;
	private Date fechaCalendar;
	private Integer totalEnviados;
	private Integer totalExitosos;
	private Integer totalFallidos;
	private boolean habilitarFecha;
	private transient StreamedContent fileDownloadSecurity;
	private transient StreamedContent fileDownloadCoupon;
	private Integer numberProcessAux;
	@PostConstruct
	public void init()
	{
//		today = CommonsUtilities.currentDate();
		fechaCalendar = CommonsUtilities.truncateDateTime(new Date());
		stockQuantity = new Integer(0);
		totalEnviados = new Integer(0);
		totalExitosos = new Integer(0);
		totalFallidos = new Integer(0);
		habilitarFecha=true;
		
		try {
			
//			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");  
//			fechaActual = sdf.format(fechaCalendar); //Esta fecha sera la actual
//			fechaActual = "03/09/2015";   // fecha de prueba
			
			loadSecuritySended();
			validarTipoUsuario();
			
		} catch (ServiceException e) {
//			PraderaLogger(e.getMessage());
		}
	}
	
	private void validarTipoUsuario(){
		
		if (userInfo.getUserAccountSession().isDepositaryInstitution()){
			habilitarFecha=false; // En caso de ser usuario EDV
		}else{
//			habilitarFecha=true;
			// usuario externo
			if (CommonsUtilities.currentDate().equals(fechaCalendar)){
				habilitarFecha=false;  
			}else{
				habilitarFecha=true;
			}
			
		}
	}

	/**
	 * Metodo que permite cargar todos los valores disponibles para envio a la bbv
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	public void loadSecuritySended() throws ServiceException{
		listSecurity = new ArrayList<>();
		listSecurityRedention = new ArrayList<>();
		
//		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");  
//		fechaActual = sdf.format(fechaCalendar);
//		fechaActual = "03/09/2015";   // fecha de prueba

		Object[] result = processSecurityBbvFacade.listSecurityAvailable(fechaCalendar);
		
		// dpf disponibles
		listSecurity =(List<Security>)result[0]; 
		stockQuantity = listSecurity.size();
		
		// DPFs revertidos
		listSecurityRedention = (List<SecuritySendBbv>)result[1];
		
		// DPF procesados
		List<ResumeSecuritySendBbvTO> listResumeSecuritySendBbvTO = processSecurityBbvFacade.getListSecurityProcesed(fechaCalendar);
		
		totalEnviados = new Integer(0);
		totalExitosos = new Integer(0);
		totalFallidos = new Integer(0);
		for (ResumeSecuritySendBbvTO lista :listResumeSecuritySendBbvTO){
			totalEnviados = totalEnviados + lista.getSended();
			totalExitosos = totalExitosos + lista.getRegistered();
			totalFallidos = totalFallidos + lista.getErrorReception();
		}
		
		listSecuritySendBbvDataModel= new GenericDataModel<>(listResumeSecuritySendBbvTO);

	}
	
	/**
	 * metodo que envia los valores a las tablas de la bbv
	 * @throws ServiceException
	 */
	@LoggerAuditWeb
	public void searchSecurityAndProcess() throws ServiceException {
		
//		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");  
//		fechaActual = sdf.format(fechaCalendar); //Esta fecha sera la actual
		
		Integer numberProces = processSecurityBbvFacade.serachAndProcess(listSecurity,fechaCalendar);
		
		Object[] argObj = new Object[1];
		argObj[0] = numberProces.toString();
		showMessageOnDialog(PropertiesUtilities.getGenericMessage("lbl.header.alert.confirm"),PropertiesUtilities.getMessage("send.security.success.confirm")+"  "+numberProces.toString());
		JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		
		loadSecuritySended();
		validarTipoUsuario();
		
	}
	
	public void beforeSaveExternalInterface(){
		
		if (listSecurity.size()>0){
			showMessageOnDialogOnlyKey(PropertiesConstants.DIALOG_HEADER_CONFIRM, PropertiesConstants.SEND_SECURITY_REGISTER_CONFIRM);
			JSFUtilities.executeJavascriptFunction("wrConfirmSaveSendSecurity.show()");
		}else{
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage("lbl.header.alert.confirm"),PropertiesUtilities.getMessage("send.header.alert.error"));
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}
	}
	
	public void selectSendProcess(ResumeSecuritySendBbvTO resumeSecuritySendBbvTO) throws ServiceException {
		
		System.out.println("PROCES::: "+resumeSecuritySendBbvTO.getNumberProcess());
		
		List<SecuritySendBbv> listSecuritySended = processSecurityBbvFacade.getListSecurityProcesedByNumber(resumeSecuritySendBbvTO.getNumberProcess(),fechaCalendar);
		listSendedDataMode= new GenericDataModel<>(listSecuritySended);
		numberProcessAux=resumeSecuritySendBbvTO.getNumberProcess();
	}
	
	public void onChangeProcessDate() throws ServiceException {
//		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");  
//		fechaActual = sdf.format(fechaCalendar); //Esta fecha sera la actual
		
		loadSecuritySended();
		validarTipoUsuario();
	}
	public StreamedContent getEmisionFileSecurity(){
		try {
			byte[] bs=processSecurityBbvFacade.getDocumentXmlSecurity(numberProcessAux,fechaCalendar);
			 if(Validations.validateIsNotNull(bs)){						
				 InputStream stream = new ByteArrayInputStream(bs);
				 fileDownloadSecurity = new DefaultStreamedContent(stream, "xml",new SecurityUtilXml().FILE_NAME+".xml"); 
			 }
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return fileDownloadSecurity;
	}
	public StreamedContent getEmisionFileCoupon(){try {
		byte[] bs=processSecurityBbvFacade.getDocumentXmlCoupon(numberProcessAux,fechaCalendar);
		 if(Validations.validateIsNotNull(bs)){						
			 InputStream stream = new ByteArrayInputStream(bs);
			 fileDownloadCoupon = new DefaultStreamedContent(stream, "xml",new CouponUtilXml().FILE_NAME+".xml"); 
		 }
	} catch (TransformerConfigurationException e) {
		e.printStackTrace();
	} catch (ParseException e) {
		e.printStackTrace();
	}
	return fileDownloadCoupon;
	}

	public Integer getStockQuantity() {
		return stockQuantity;
	}

	public void setStockQuantity(Integer stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	public GenericDataModel<ResumeSecuritySendBbvTO> getListSecuritySendBbvDataModel() {
		return listSecuritySendBbvDataModel;
	}

	public void setListSecuritySendBbvDataModel(
			GenericDataModel<ResumeSecuritySendBbvTO> listSecuritySendBbvDataModel) {
		this.listSecuritySendBbvDataModel = listSecuritySendBbvDataModel;
	}

	public Date getFechaCalendar() {
		return fechaCalendar;
	}

	public void setFechaCalendar(Date fechaCalendar) {
		this.fechaCalendar = fechaCalendar;
	}

	public List<SecuritySendBbv> getListSecurityRedention() {
		return listSecurityRedention;
	}

	public void setListSecurityRedention(List<SecuritySendBbv> listSecurityRedention) {
		this.listSecurityRedention = listSecurityRedention;
	}

	public GenericDataModel<SecuritySendBbv> getListRevertionDataMode() {
		return listRevertionDataMode;
	}

	public void setListRevertionDataMode(
			GenericDataModel<SecuritySendBbv> listRevertionDataMode) {
		this.listRevertionDataMode = listRevertionDataMode;
	}

	public GenericDataModel<SecuritySendBbv> getListSendedDataMode() {
		return listSendedDataMode;
	}

	public void setListSendedDataMode(
			GenericDataModel<SecuritySendBbv> listSendedDataMode) {
		this.listSendedDataMode = listSendedDataMode;
	}

	public Integer getTotalEnviados() {
		return totalEnviados;
	}

	public void setTotalEnviados(Integer totalEnviados) {
		this.totalEnviados = totalEnviados;
	}

	public Integer getTotalExitosos() {
		return totalExitosos;
	}

	public void setTotalExitosos(Integer totalExitosos) {
		this.totalExitosos = totalExitosos;
	}

	public Integer getTotalFallidos() {
		return totalFallidos;
	}

	public void setTotalFallidos(Integer totalFallidos) {
		this.totalFallidos = totalFallidos;
	}

	public boolean isHabilitarFecha() {
		return habilitarFecha;
	}

	public void setHabilitarFecha(boolean habilitarFecha) {
		this.habilitarFecha = habilitarFecha;
	}

	public StreamedContent getFileDownloadSecurity() {
		return fileDownloadSecurity;
	}

	public void setFileDownloadSecurity(StreamedContent fileDownloadSecurity) {
		this.fileDownloadSecurity = fileDownloadSecurity;
	}

	public StreamedContent getFileDownloadCoupon() {
		return fileDownloadCoupon;
	}

	public void setFileDownloadCoupon(StreamedContent fileDownloadCoupon) {
		this.fileDownloadCoupon = fileDownloadCoupon;
	}
}
