package com.pradera.securities.issuancesecuritie.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.issuancesecuritie.Security;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SecurityDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class SecurityDataModel extends ListDataModel<Security> implements SelectableDataModel<Security>,Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 969987519464929763L;

	/**
	 * Instantiates a new issuance data model.
	 */
	public SecurityDataModel(){
		super();
	}
	
	/**
	 * Instantiates a new issuance data model.
	 *
	 * @param data the data
	 */
	public SecurityDataModel(List<Security> data){
		super(data);
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(Security security) {
		// TODO Auto-generated method stub
		return security.getIdSecurityCodePk();
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public Security getRowData(String rowKey) {
		// TODO Auto-generated method stub
		List<Security> lstSecurity=(List<Security>)getWrappedData();
		for(Security security:lstSecurity){
			if(security.getIdSecurityCodePk().equals( rowKey ))
				return security;
		}
		return null;
	}
	
	


}
