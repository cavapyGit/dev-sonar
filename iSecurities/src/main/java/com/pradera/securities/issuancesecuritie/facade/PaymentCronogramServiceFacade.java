package com.pradera.securities.issuancesecuritie.facade;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.issuancesecuritie.AmoPaymentScheduleReqHi;
import com.pradera.model.issuancesecuritie.AmortizationPaymentSchedule;
import com.pradera.model.issuancesecuritie.IntPaymentScheduleReqHi;
import com.pradera.model.issuancesecuritie.InterestPaymentSchedule;
import com.pradera.model.issuancesecuritie.ProgramAmoCouponReqHi;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.PaymentScheduleReqRegisterType;
import com.pradera.model.issuancesecuritie.type.RequestStateType;
import com.pradera.securities.issuancesecuritie.service.PaymentCronogramServiceBean;
import com.pradera.securities.issuancesecuritie.to.PaymentScheduleResultTO;
import com.pradera.securities.issuancesecuritie.to.PaymentScheduleTO;
import com.pradera.securities.issuancesecuritie.util.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PaymentCronogramServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class PaymentCronogramServiceFacade {
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	/** The payment cronogram service bean. */
	@EJB
	PaymentCronogramServiceBean paymentCronogramServiceBean;

	/**
	 * Metodo para Registrar un IntPaymentScheduleReqHi
	 *
	 * @param intPaymentScheduleReqHi the int payment schedule req hi
	 * @param security the security
	 * @throws ServiceException the service exception
	 */
	public void registerIntPaymentScheduleReqHiServiceFacade(IntPaymentScheduleReqHi intPaymentScheduleReqHi, Security security) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		intPaymentScheduleReqHi.setRegistryDate(loggerUser.getAuditTime());
		intPaymentScheduleReqHi.setRegistryUser(loggerUser.getUserName());
		
		paymentCronogramServiceBean.registerIntPaymentScheduleReqHiServiceBean(intPaymentScheduleReqHi,security);
	}
	
	/**
	 * Metodo para registrar un AmoPaymentScheduleReqHi
	 *
	 * @param amoPaymentScheduleReqHi the amo payment schedule req hi
	 * @param security the security
	 * @throws ServiceException the service exception
	 */
	public void registerAmoPaymentScheduleReqHiServiceFacade(AmoPaymentScheduleReqHi amoPaymentScheduleReqHi, Security security) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		for(ProgramAmoCouponReqHi prog : amoPaymentScheduleReqHi.getProgramAmoCouponReqHis()){
			prog.setRegistryUser(loggerUser.getUserName());
			prog.setRegisterDate(CommonsUtilities.currentDateTime());
		}
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		amoPaymentScheduleReqHi.setRegistryDate(loggerUser.getAuditTime());
		amoPaymentScheduleReqHi.setRegistryUser(loggerUser.getUserName());
		
		paymentCronogramServiceBean.registerAmoPaymentScheduleReqHiServiceFacade(amoPaymentScheduleReqHi, security);
	}
	
	/**
	 * Metodo para validar si IntPaymentScheduleReqHi ya se encuentra registrado
	 *
	 * @param intPaymentScheduleReqHi the int payment schedule req hi
	 * @param security the security
	 * @throws ServiceException the service exception
	 */
	public void validateIntPaymentScheRequestAlreadyRegisteredServiceFacade(IntPaymentScheduleReqHi intPaymentScheduleReqHi, Security security) throws ServiceException{
		paymentCronogramServiceBean.validateIntPaymentScheRequestAlreadyRegisteredServiceBean(intPaymentScheduleReqHi, security);
	}
	
	/**
	 * Metodo para validar si AmoPaymentScheduleReqHi ya se encuentra registrado
	 *
	 * @param amoPaymentScheduleReqHi the amo payment schedule req hi
	 * @param security the security
	 * @throws ServiceException the service exception
	 */
	public void validateAmoPaymentScheRequestAlreadyRegisteredServiceFacade(AmoPaymentScheduleReqHi amoPaymentScheduleReqHi, Security security) throws ServiceException{
		paymentCronogramServiceBean.validateAmoPaymentScheRequestAlreadyRegisteredServiceBean(amoPaymentScheduleReqHi, security);
	}
	
	/**
	 * Metodo para actualizar el estado de IntPaymentScheduleReqHi.
	 *
	 * @param intPaymentScheduleReqHi the int payment schedule req hi
	 * @param viewOperationsType the view operations type
	 * @throws ServiceException the service exception
	 */
	public void updateIntPayScheReqHiReqStateServiceFacade(IntPaymentScheduleReqHi intPaymentScheduleReqHi, ViewOperationsType viewOperationsType) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		switch (viewOperationsType) {
			case APPROVE: loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove()); break;
			case ANULATE: loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular()); break;
			case REJECT: loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject()); break;
		}
		 paymentCronogramServiceBean.updateIntPayScheReqHiReqStateServiceBean(intPaymentScheduleReqHi, loggerUser);
	}
	
	/**
	 * Metodo para actualizar el estado de AmoPaymentScheduleReqHi.
	 *
	 * @param amoPaymentScheduleReqHi the amo payment schedule req hi
	 * @param viewOperationsType the view operations type
	 * @throws ServiceException the service exception
	 */
	public void updateAmoPayScheReqHiReqStateServiceFacade(AmoPaymentScheduleReqHi amoPaymentScheduleReqHi,ViewOperationsType viewOperationsType) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		switch (viewOperationsType) {
			case APPROVE: loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove()); break;
			case ANULATE: loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular()); break;
			case REJECT: loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject()); break;
		}
        
		paymentCronogramServiceBean.updateAmoPayScheReqHiReqStateServiceBean(amoPaymentScheduleReqHi, loggerUser);		
	}
	
	/**
	 * Metodo para Confirmar la Reestructuracion del cronograma de Amortizacion
	 *
	 * @param amoPaymentScheduleReqHi the amo payment schedule req hi
	 * @param amortizationPaymentSchedule the amortization payment schedule
	 * @throws ServiceException the service exception
	 */
	public void confirmAmoPaymentScheduleRestructuringServiceFacade(AmoPaymentScheduleReqHi amoPaymentScheduleReqHi, 
		AmortizationPaymentSchedule amortizationPaymentSchedule) throws ServiceException{

		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());  
		
		paymentCronogramServiceBean.confirmAmoPaymentScheduleRestructuringServiceBean(amoPaymentScheduleReqHi, amortizationPaymentSchedule, loggerUser);
	}	
	
	/**
	 * Metodo para Confirmar la Reestructuracion del cronograma de Interes
	 *
	 * @param intPaymentScheduleReqHi the int payment schedule req hi
	 * @param interestPaymentSchedule the interest payment schedule
	 * @throws ServiceException the service exception
	 */
	public void confirmIntPaymentScheduleRestructuringServiceFacade(IntPaymentScheduleReqHi intPaymentScheduleReqHi, 
		InterestPaymentSchedule interestPaymentSchedule) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());   
		
		paymentCronogramServiceBean.confirmIntPaymentScheduleRestructuringServiceBean(intPaymentScheduleReqHi, interestPaymentSchedule, loggerUser);        
	}
	
	/**
	 * Metodo para Confirmar la Modificacion del cronograma de Interes
	 *
	 * @param intPaymentScheduleReqHi the int payment schedule req hi
	 * @param interestPaymentSchedule the interest payment schedule
	 * @throws ServiceException the service exception
	 */
	public void confirmIntPaymentScheduleModificationServiceFacade(IntPaymentScheduleReqHi intPaymentScheduleReqHi, InterestPaymentSchedule interestPaymentSchedule) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		
		paymentCronogramServiceBean.confirmIntPaymentScheduleModificationServiceBean(intPaymentScheduleReqHi, interestPaymentSchedule, loggerUser);
	}
	
	/**
	 * Metodo para Confirmar la Modificacion del cronograma de Amortizacion
	 *
	 * @param amoPaymentScheduleReqHi the amo payment schedule req hi
	 * @param amortizationPaymentSchedule the amortization payment schedule
	 * @throws ServiceException the service exception
	 */
	public void confirmAmoPaymentScheduleModificationServiceFacade(AmoPaymentScheduleReqHi amoPaymentScheduleReqHi, AmortizationPaymentSchedule amortizationPaymentSchedule) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());           
        /**
        //metodo revertido salida a produccion issues: 100,148,143
        paymentCronogramServiceBean.confirmAmoPaymentScheduleModificationServiceFacadeNew(amoPaymentScheduleReqHi, amortizationPaymentSchedule, loggerUser);    
        paymentCronogramServiceBean.deleteCorpOperationProgAmoCoupOld(amoPaymentScheduleReqHi, amortizationPaymentSchedule);
        paymentCronogramServiceBean.deleteProgramAmortizationCouponOld(amoPaymentScheduleReqHi, amortizationPaymentSchedule);
        */
        paymentCronogramServiceBean.confirmAmoPaymentScheduleModificationServiceFacade(amoPaymentScheduleReqHi, amortizationPaymentSchedule, loggerUser);
	}
	/**
	//metodo revertido salida a produccion issues: 100,148,143
	public void confirmAmoPaymentScheduleModificationUpdateServiceFacade(AmoPaymentScheduleReqHi amoPaymentScheduleReqHi, AmortizationPaymentSchedule amortizationPaymentSchedule) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());           
        paymentCronogramServiceBean.confirmAmoPaymentScheduleModificationUpdateServiceFacadeNew(amoPaymentScheduleReqHi, amortizationPaymentSchedule, loggerUser);           
	}*/
	
	/**
	 * Metodo para Buscar una lista de cronogramas de pago de interes 
	 *
	 * @param paymentScheduleTO the payment schedule to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<PaymentScheduleResultTO> finIntPaymentScheduleReqHisLiteServiceFacade(PaymentScheduleTO paymentScheduleTO) throws ServiceException{
		return paymentCronogramServiceBean.finIntPaymentScheduleReqHisLiteServiceBean(paymentScheduleTO);
	}

	
	/**
	 * Metodo para Buscar una lista de cronogramas de pago de Amortizacion
	 *
	 * @param paymentScheduleTO the payment schedule to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<PaymentScheduleResultTO> finAmoPaymentScheduleReqHisLiteServiceFacade(PaymentScheduleTO paymentScheduleTO) throws ServiceException{
		return paymentCronogramServiceBean.finAmoPaymentScheduleReqHisLiteServiceBean(paymentScheduleTO);
	}
	
	/**
	 * Metodo para Buscar una solicitud de cronograma de interes
	 *
	 * @param paymentScheduleTO the payment schedule to
	 * @return the int payment schedule req hi
	 * @throws ServiceException the service exception
	 */
	public IntPaymentScheduleReqHi finIntPaymentScheduleReqHisServiceFacade(PaymentScheduleTO paymentScheduleTO ) throws ServiceException{
		return paymentCronogramServiceBean.finIntPaymentScheduleReqHisServiceBean(paymentScheduleTO );
	}
	
	/**
	 * Metodo para Buscar una solicitud de cronograma de Amortizacion
	 *
	 * @param paymentScheduleTO the payment schedule to
	 * @return the amo payment schedule req hi
	 * @throws ServiceException the service exception
	 */
	public AmoPaymentScheduleReqHi finAmoPaymentScheduleReqHisServiceFacade(PaymentScheduleTO paymentScheduleTO) throws ServiceException{
		return paymentCronogramServiceBean.finAmoPaymentScheduleReqHisServiceBean(paymentScheduleTO);
	}
	
	public Boolean verifyDefinitiveStateCorporativeOperation(Security security) throws ServiceException {
		return paymentCronogramServiceBean.verifyDefinitiveStateCorporativeOperation(security);
	}
	
}
