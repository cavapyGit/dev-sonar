package com.pradera.securities.issuancesecuritie.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.pradera.commons.utils.GeneralConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CouponInformationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class CouponInformationTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1476434199741042195L;

	/** The id security code pk. */
	private String idSecurityCodePk;
	
	/** The id isin code fk. */
	private String idIsinCodeFk;
	
	/** The security description. */
	private String securityDescription;
	
	/** The id issuer fk. */
	private String idIssuerFk;
	
	/** The issuer mnemonic. */
	private String issuerMnemonic;

	
	/** The security instrument type. */
	private Integer securityInstrumentType;
	
	
	/** The security expiration date. */
	private Date securityExpirationDate;
	
	/** The interes expiration date. */
	private Date interesExpirationDate;
	
	/** The amortization expiration date. */
	private Date amortizationExpirationDate;
	
	/** The coupon number. */
	private Integer couponNumber;
	
	/** The interest rate. */
	private BigDecimal interestRate;
	
	/** The amortization amount. */
	private BigDecimal amortizationAmount;
	
	/** The payment amount. */
	private BigDecimal paymentAmount;
	
	/** The payment date. */
	private Date paymentDate;
	
	/** The payment type description. */
	private String paymentTypeDescription;
	
	/** The coupon state. */
	private Integer situation;
	
	/** The ind payed. */
	private Integer indPayed;
	
	/** The ind interest. */
	private boolean indInterest;
	
	private Integer stateSecurity;
	
	/**
	 * Instantiates a new coupon information to.
	 */
	public CouponInformationTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the id isin code fk.
	 *
	 * @return the id isin code fk
	 */
	public String getIdIsinCodeFk() {
		return idIsinCodeFk;
	}

	/**
	 * Sets the id isin code fk.
	 *
	 * @param idIsinCodeFk the new id isin code fk
	 */
	public void setIdIsinCodeFk(String idIsinCodeFk) {
		this.idIsinCodeFk = idIsinCodeFk;
	}

	/**
	 * Gets the security description.
	 *
	 * @return the security description
	 */
	public String getSecurityDescription() {
		return securityDescription;
	}

	/**
	 * Sets the security description.
	 *
	 * @param securityDescription the new security description
	 */
	public void setSecurityDescription(String securityDescription) {
		this.securityDescription = securityDescription;
	}

	/**
	 * Gets the id issuer fk.
	 *
	 * @return the id issuer fk
	 */
	public String getIdIssuerFk() {
		return idIssuerFk;
	}

	/**
	 * Sets the id issuer fk.
	 *
	 * @param idIssuerFk the new id issuer fk
	 */
	public void setIdIssuerFk(String idIssuerFk) {
		this.idIssuerFk = idIssuerFk;
	}

	/**
	 * Gets the issuer mnemonic.
	 *
	 * @return the issuer mnemonic
	 */
	public String getIssuerMnemonic() {
		return issuerMnemonic;
	}

	/**
	 * Sets the issuer mnemonic.
	 *
	 * @param issuerMnemonic the new issuer mnemonic
	 */
	public void setIssuerMnemonic(String issuerMnemonic) {
		this.issuerMnemonic = issuerMnemonic;
	}


	/**
	 * Gets the issuer mnemonic code.
	 *
	 * @return the issuer mnemonic code
	 */
	public String getIssuerMnemonicCode() {
		if(Validations.validateIsNotNull(idIssuerFk) && Validations.validateIsNotNull(issuerMnemonic)){
			StringBuilder sb = new StringBuilder();
			sb.append(issuerMnemonic).
			append(GeneralConstants.DASH).
			append(idIssuerFk);
			
			return sb.toString();
		} else {
			return "";
		}		
	}


	/**
	 * Gets the security expiration date.
	 *
	 * @return the security expiration date
	 */
	public Date getSecurityExpirationDate() {
		return securityExpirationDate;
	}

	/**
	 * Sets the security expiration date.
	 *
	 * @param securityExpirationDate the new security expiration date
	 */
	public void setSecurityExpirationDate(Date securityExpirationDate) {
		this.securityExpirationDate = securityExpirationDate;
	}

	/**
	 * Gets the coupon number.
	 *
	 * @return the coupon number
	 */
	public Integer getCouponNumber() {
		return couponNumber;
	}

	/**
	 * Sets the coupon number.
	 *
	 * @param couponNumber the new coupon number
	 */
	public void setCouponNumber(Integer couponNumber) {
		this.couponNumber = couponNumber;
	}

	/**
	 * Gets the interest rate.
	 *
	 * @return the interest rate
	 */
	public BigDecimal getInterestRate() {
		return interestRate;
	}

	/**
	 * Sets the interest rate.
	 *
	 * @param interestRate the new interest rate
	 */
	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}

	/**
	 * Gets the payment date.
	 *
	 * @return the payment date
	 */
	public Date getPaymentDate() {
		return paymentDate;
	}

	/**
	 * Sets the payment date.
	 *
	 * @param paymentDate the new payment date
	 */
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	/**
	 * Gets the payment type description.
	 *
	 * @return the payment type description
	 */
	public String getPaymentTypeDescription() {
		return paymentTypeDescription;
	}

	/**
	 * Sets the payment type description.
	 *
	 * @param paymentTypeDescription the new payment type description
	 */
	public void setPaymentTypeDescription(String paymentTypeDescription) {
		this.paymentTypeDescription = paymentTypeDescription;
	}



	/**
	 * Gets the payment amount.
	 *
	 * @return the payment amount
	 */
	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}

	/**
	 * Sets the payment amount.
	 *
	 * @param paymentAmount the new payment amount
	 */
	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	/**
	 * Gets the situation.
	 *
	 * @return the situation
	 */
	public Integer getSituation() {
		return situation;
	}

	/**
	 * Sets the situation.
	 *
	 * @param situation the new situation
	 */
	public void setSituation(Integer situation) {
		this.situation = situation;
	}

	/**
	 * Gets the amortization amount.
	 *
	 * @return the amortization amount
	 */
	public BigDecimal getAmortizationAmount() {
		return amortizationAmount;
	}

	/**
	 * Sets the amortization amount.
	 *
	 * @param amortizationAmount the new amortization amount
	 */
	public void setAmortizationAmount(BigDecimal amortizationAmount) {
		this.amortizationAmount = amortizationAmount;
	}

	/**
	 * Gets the security instrument type.
	 *
	 * @return the security instrument type
	 */
	public Integer getSecurityInstrumentType() {
		return securityInstrumentType;
	}

	/**
	 * Sets the security instrument type.
	 *
	 * @param securityInstrumentType the new security instrument type
	 */
	public void setSecurityInstrumentType(Integer securityInstrumentType) {
		this.securityInstrumentType = securityInstrumentType;
	}
	
	/**
	 * Gets the security instrument type description.
	 *
	 * @return the security instrument type description
	 */
	public String getSecurityInstrumentTypeDescription() {
		if(securityInstrumentType!=null && InstrumentType.get(securityInstrumentType)!= null){	
			return InstrumentType.get(securityInstrumentType).getValue();							
		} else {
			return "";
		}
	}
	
	/**
	 * Gets the coupon state description.
	 *
	 * @return the coupon state description
	 */
	public String getSituationDescription() {
		if(situation!=null && ProgramScheduleStateType.get(situation)!= null){			
				return ProgramScheduleStateType.get(securityInstrumentType).getValue();							
		} else {
			return "";
		}		
	}

	/**
	 * Gets the ind payed.
	 *
	 * @return the ind payed
	 */
	public Integer getIndPayed() {
		return indPayed;
	}

	/**
	 * Sets the ind payed.
	 *
	 * @param indPayed the new ind payed
	 */
	public void setIndPayed(Integer indPayed) {
		this.indPayed = indPayed;
	}

	/**
	 * Gets the id security code pk.
	 *
	 * @return the id security code pk
	 */
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	/**
	 * Sets the id security code pk.
	 *
	 * @param idSecurityCodePk the new id security code pk
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	/**
	 * Gets the interes expiration date.
	 *
	 * @return the interes expiration date
	 */
	public Date getInteresExpirationDate() {
		return interesExpirationDate;
	}

	/**
	 * Sets the interes expiration date.
	 *
	 * @param interesExpirationDate the new interes expiration date
	 */
	public void setInteresExpirationDate(Date interesExpirationDate) {
		this.interesExpirationDate = interesExpirationDate;
	}

	/**
	 * Gets the amortization expiration date.
	 *
	 * @return the amortization expiration date
	 */
	public Date getAmortizationExpirationDate() {
		return amortizationExpirationDate;
	}

	/**
	 * Sets the amortization expiration date.
	 *
	 * @param amortizationExpirationDate the new amortization expiration date
	 */
	public void setAmortizationExpirationDate(Date amortizationExpirationDate) {
		this.amortizationExpirationDate = amortizationExpirationDate;
	}

	/**
	 * Checks if is ind interest.
	 *
	 * @return true, if is ind interest
	 */
	public boolean isIndInterest() {
		return indInterest;
	}

	/**
	 * Sets the ind interest.
	 *
	 * @param indInterest the new ind interest
	 */
	public void setIndInterest(boolean indInterest) {
		this.indInterest = indInterest;
	}

	public Integer getStateSecurity() {
		return stateSecurity;
	}

	public void setStateSecurity(Integer stateSecurity) {
		this.stateSecurity = stateSecurity;
	}
	
}
