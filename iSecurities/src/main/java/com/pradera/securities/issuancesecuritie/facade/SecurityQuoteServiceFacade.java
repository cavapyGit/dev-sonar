/*
 * 
 */
package com.pradera.securities.issuancesecuritie.facade;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.SecurityQuote;
import com.pradera.model.issuancesecuritie.type.IssuanceStateType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.SecurityQuoteStateType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.issuancesecuritie.type.SecurityType;
import com.pradera.securities.issuancesecuritie.service.SecurityQuoteServiceBean;
import com.pradera.securities.issuancesecuritie.to.SecurityQuoteTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SecurityQuoteServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class SecurityQuoteServiceFacade {
	
	/** The security quote service bean. */
	@EJB
	SecurityQuoteServiceBean securityQuoteServiceBean;
	
	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;

	/**
	 * Metodo para Guardar la Cotizacion de un Valor.
	 *
	 * @param securityQuote the security quote
	 * @return the security quote
	 * @throws ServiceException the service exception
	 */
	public SecurityQuote saveSecurityQuoteFacade(SecurityQuote securityQuote) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		validateQuoteDate(securityQuote);
		return securityQuoteServiceBean.saveSecurityQuoteService(securityQuote);
	}
	
	/**
	 * Metodo para validar un Emisor
	 *
	 * @param securityQuote the security quote
	 * @throws ServiceException the service exception
	 */
	public void validateIssuerFacade(SecurityQuote securityQuote) throws ServiceException{
		Issuer issuer = securityQuote.getIssuer();
		if(issuer==null || issuer.getHolder()==null){
			throw new ServiceException(ErrorServiceType.SECURITY_QUOTE_ISSUER_HOLDER);
		}
		if(!issuer.getHolder().getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.SECURITY_QUOTE_ISSUER_HOLDER_STATE);
		}
		if(!issuer.getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.SECURITY_QUOTE_ISSUER_STATE);
		}
	}
	
	/**
	 * Metodo para validar la Emision
	 *
	 * @param securityQuote the security quote
	 * @throws ServiceException the service exception
	 */
	public void validateIssuanceFacade(SecurityQuote securityQuote) throws ServiceException{
		Issuance issuance = new Issuance();
		if(!issuance.getStateIssuance().equals(IssuanceStateType.AUTHORIZED.getCode())){
			throw new ServiceException(ErrorServiceType.SECURITY_QUOTE_ISSUANCE_STATE);
		}
		if(!issuance.getSecurityType().equals(SecurityType.PAR_QUO.getCode())){
			throw new ServiceException(ErrorServiceType.SECURITY_QUOTE_ISSUANCE_TYPE);
		}
	}
	
	/**
	 * Metodo para validar el Valor
	 *
	 * @param securityQuote the security quote
	 * @throws ServiceException the service exception
	 */
	public void validateSecurityFacade(SecurityQuote securityQuote) throws ServiceException{
		Issuance issuance = new Issuance();// = securityQuote.getIssuance();
		Security security = securityQuote.getSecurity();
		if(!security.getIssuance().getIdIssuanceCodePk().equals(issuance.getIdIssuanceCodePk())){
			throw new ServiceException(ErrorServiceType.SECURITY_QUOTE_SECURITY_ISSUANCE);
		}
		if(!security.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.SECURITY_QUOTE_ISIN_STATE);
		}
		if(!security.getSecurityType().equals(SecurityType.PAR_QUO.getCode())){
			throw new ServiceException(ErrorServiceType.SECURITY_QUOTE_SECURITY_TYPE);
		}
	}
	/**
	 * Metodo para validar la Fecha de cotizacion
	 *
	 * @param securityQuote the security quote
	 * @throws ServiceException the service exception
	 */
	public void validateQuoteDate(SecurityQuote securityQuote) throws ServiceException{
		Date date = securityQuote.getQuoteDate();
		String isinCode = securityQuote.getSecurity().getIdIsinCode();
		List<Integer> stateValidate = Arrays.asList(SecurityQuoteStateType.CONFIRMED.getCode());
		for(SecurityQuote securitQuote: securityQuoteServiceBean.getSecurityQuoteByIsin(isinCode, date)){
			if(stateValidate.contains(securitQuote.getQuoteState())){
				throw new ServiceException(ErrorServiceType.SECURITY_QUOTE_ISIN_EXIST_DATE);
			}
		}
	}
	/**
	 * Metodo para obtener una lista de cotizaciones de un valor
	 *
	 * @param securityQuoteTO the security quote to
	 * @return the security quote list facade
	 * @throws ServiceException the service exception
	 */
	public List<SecurityQuoteTO> getSecurityQuoteListFacade(SecurityQuoteTO securityQuoteTO) throws ServiceException{
		return securityQuoteServiceBean.getSecurityQuoteList(securityQuoteTO);
	}
	
	/**
	 * Metodo para Rechazar una cotizacion de un valor.
	 *
	 * @param securityQuoteParam the security quote param
	 * @return the security quote
	 * @throws ServiceException the service exception
	 */
	public SecurityQuote rejectSecurityQuoteFacade(SecurityQuote securityQuoteParam) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		securityQuoteServiceBean.validateState(securityQuoteParam.getIdSecurityQuotePk(), Arrays.asList(SecurityQuoteStateType.REGISTERED.getCode()));
		return securityQuoteServiceBean.rejectSecurityQuoteService(securityQuoteParam);
	}
	
	/**
	 * Confirmar la Cotizacion de una valor
	 *
	 * @param securityQuoteParam the security quote param
	 * @return the security quote
	 * @throws ServiceException the service exception
	 */
	public SecurityQuote confirmSecurityQuoteFacade(SecurityQuote securityQuoteParam) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		validateQuoteDate(securityQuoteParam);
		securityQuoteServiceBean.validateState(securityQuoteParam.getIdSecurityQuotePk(), Arrays.asList(SecurityQuoteStateType.REGISTERED.getCode()));
		return securityQuoteServiceBean.confirmSecurityQuote(securityQuoteParam);
	}
	
	/**
	 * Metodo para Obtener una lista de Valores por Parametros
	 *
	 * @param securitiesTO the securities to
	 * @return the security list by parameters
	 * @throws ServiceException the service exception
	 */
	public List<Security> getSecurityListByParameters(SecurityQuoteTO securitiesTO) throws ServiceException{
		return securityQuoteServiceBean.getSecurityListByParameters(securitiesTO);
	}
	
	/**
	 * Metodo para obtener la cotizacion de un valor por su Pk
	 *
	 * @param idSecurityQuotePk the id security quote pk
	 * @return the security quote from pk facade
	 * @throws ServiceException the service exception
	 */
	public SecurityQuote getSecurityQuoteFromPkFacade(Long idSecurityQuotePk) throws ServiceException{
		return securityQuoteServiceBean.getSecurityQuoteFromPk(idSecurityQuotePk);
	}
	
}