package com.pradera.securities.issuancesecuritie.view;


import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.EditableValueHolder;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.ToggleEvent;

import com.edv.collection.economic.rigth.PaymentAgent;
import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.sessionuser.OperationUserTO;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericColumnModel;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.accounts.to.InternationalDepositoryTO;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.HolderSearchTO;
import com.pradera.core.component.utils.DocumentValidator;
import com.pradera.core.component.utils.PersonTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.securities.to.GenerationFileDetailCouponTRCRTO;
import com.pradera.integration.component.securities.to.GenerationFileSecurityTRCRTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.InternationalDepositoryStateType;
import com.pradera.model.accounts.type.MechanismModalityStateType;
import com.pradera.model.accounts.type.NegotiationMechanismStateType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.generalparameter.FinancialIndicator;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.FinancialIndicatorStateType;
import com.pradera.model.generalparameter.type.GeographicLocationStateType;
import com.pradera.model.generalparameter.type.GeographicLocationType;
import com.pradera.model.generalparameter.type.IndicatorType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.AmortizationPaymentSchedule;
import com.pradera.model.issuancesecuritie.CfiAttribute;
import com.pradera.model.issuancesecuritie.CfiAttributePK;
import com.pradera.model.issuancesecuritie.CfiCategory;
import com.pradera.model.issuancesecuritie.CfiGroup;
import com.pradera.model.issuancesecuritie.CfiGroupPK;
import com.pradera.model.issuancesecuritie.InterestPaymentSchedule;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.ProgramAmortizationCoupon;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.SecurityCodeFormat;
import com.pradera.model.issuancesecuritie.SecurityForeignDepository;
import com.pradera.model.issuancesecuritie.SecurityHistoryState;
import com.pradera.model.issuancesecuritie.SecurityInvestor;
import com.pradera.model.issuancesecuritie.SecurityNegotiationMechanism;
import com.pradera.model.issuancesecuritie.SecuritySerialRange;
import com.pradera.model.issuancesecuritie.type.AmortizationOnType;
import com.pradera.model.issuancesecuritie.type.AmortizationType;
import com.pradera.model.issuancesecuritie.type.CalendarDayType;
import com.pradera.model.issuancesecuritie.type.CalendarMonthType;
import com.pradera.model.issuancesecuritie.type.CalendarType;
import com.pradera.model.issuancesecuritie.type.CapitalPaymentModalityType;
import com.pradera.model.issuancesecuritie.type.CfiParameterType;
import com.pradera.model.issuancesecuritie.type.EncoderAgentType;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.InterestClassType;
import com.pradera.model.issuancesecuritie.type.InterestPaymentModalityType;
import com.pradera.model.issuancesecuritie.type.InterestPeriodicityType;
import com.pradera.model.issuancesecuritie.type.InterestType;
import com.pradera.model.issuancesecuritie.type.IssuanceStateType;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.OperationType;
import com.pradera.model.issuancesecuritie.type.PaymentScheduleStateType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleSituationType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;
import com.pradera.model.issuancesecuritie.type.SecurityAuthorizedType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityInversionistStateType;
import com.pradera.model.issuancesecuritie.type.SecurityMotiveBlockType;
import com.pradera.model.issuancesecuritie.type.SecuritySourceType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.issuancesecuritie.type.SecurityTermType;
import com.pradera.model.issuancesecuritie.type.SecurityType;
import com.pradera.model.negotiation.MechanismModality;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.NegotiationModalityStateType;
import com.pradera.model.negotiation.type.NegotiationModalityType;
import com.pradera.model.valuatorprocess.ValuatorProcessClass;
import com.pradera.model.valuatorprocess.ValuatorTermRange;
import com.pradera.securities.issuancesecuritie.facade.IssuanceSecuritiesServiceFacade;
import com.pradera.securities.issuancesecuritie.facade.SecurityQuoteServiceFacade;
import com.pradera.securities.issuancesecuritie.facade.SecurityServiceFacade;
import com.pradera.securities.issuancesecuritie.to.IssuanceTO;
import com.pradera.securities.issuancesecuritie.to.SecurityQuoteTO;
import com.pradera.securities.issuancesecuritie.util.PropertiesConstants;
import com.pradera.securities.issuancesecuritie.util.SecuritiesUtils;
import com.pradera.securities.issuer.facade.IssuerServiceFacade;
import com.pradera.securities.issuer.to.IssuerTO;
import com.pradera.securities.issuer.to.PhysicalCertificateTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SecurityMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class SecurityMgmtBean extends GenericBaseBean implements Serializable{

	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The log. */
	@Inject
	private transient PraderaLogger log;
	
	/** The country residence. */
	@Inject 
	@Configurable 
	Integer countryResidence;
	
	/** The id issuer bc. */
	@Inject 
	@Configurable 
	String idIssuerBC;
	
	/** The id issuer tgn. */
	@Inject 
	@Configurable 
	String idIssuerTGN;
	
	/** The issuer helper bean. */
	
	/** The user info. */
	@Inject UserInfo userInfo;
	
	/** The user privilege. */
	@Inject UserPrivilege userPrivilege;
	
	/** The document validator. */
	@Inject DocumentValidator documentValidator;
	
	/** The securities utils. */
	@Inject SecuritiesUtils securitiesUtils;
	
	/** The valuator history detail bean. */
	@Inject
	ValuatorHistoryDetailBean valuatorHistoryDetailBean; 
	
	/** The logged issuer. */
	private Issuer loggedIssuer;
	
	/** The general parameter facade. */
	@EJB
	private GeneralParametersFacade generalParameterFacade;
	
	/** The issuance securities service facade. */
	@EJB
	private IssuanceSecuritiesServiceFacade issuanceSecuritiesServiceFacade; 
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	/** The issuer service facade. */
	@EJB
	private IssuerServiceFacade issuerServiceFacade;
	
	/** The security quote facade. */
	@EJB SecurityQuoteServiceFacade securityQuoteFacade;
	
	/** The secuity service facade. */
	@EJB
	private SecurityServiceFacade securityServiceFacade;
	
	@EJB
	private ParameterServiceBean parameterServiceBean;
	
	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
	/** The issuance data model. */
	private IssuanceDataModel issuanceDataModel;
	
	/** The security data model. */
	private SecurityDataModel securityDataModel;
	
	/** The security quote list. */
	private GenericDataModel<SecurityQuoteTO> securityQuoteList;
	
	/** The lst cbo cfi categories. */
	private List<CfiCategory> lstCboCFICategories;
	
	/** The lst cbo cfi groups. */
	private List<CfiGroup> lstCboCFIGroups;
	
	/** The lst cbo cfi attribute1. */
	private List<CfiAttribute> lstCboCFIAttribute1;
	
	/** The lst cbo cfi attribute2. */
	private List<CfiAttribute> lstCboCFIAttribute2;
	
	/** The lst cbo cfi attribute3. */
	private List<CfiAttribute> lstCboCFIAttribute3;
	
	/** The lst cbo cfi attribute4. */
	private List<CfiAttribute> lstCboCFIAttribute4;
	
	/** The lst cbo instrument type. */
	private List<ParameterTable> lstCboInstrumentType;
	
	/** The lst cbo securitie type. */
	private List<ParameterTable> lstCboSecuritieType;
	
	/** The lst cbo securitie class. */
	private List<ParameterTable> lstCboSecuritieClass;
	
	/** The lst cbo issuance state. */
	private List<ParameterTable> lstCboIssuanceState;
	
	/** The lst cbo geographic location. */
	private List<GeographicLocation> lstCboGeographicLocation;
	
	/** The lst cbo geographic location. */
	private List<GeographicLocation> lstCboGeographicLocationDep;
	
	/** The lst cbo calendar month. */
	private List<ParameterTable> lstCboCalendarMonth;
	
	/** The lst cbo issuer document type. */
	private List<ParameterTable> lstCboIssuerDocumentType;
	
	/** The lst cbo currency. */
	private List<ParameterTable> lstCboCurrency;
	
	/** The lst cbo issuance type. */
	private List<ParameterTable> lstCboIssuanceType;
	
	/** The lst cbo security state. */
	private List<ParameterTable> lstCboSecurityState;
	
	/** The lst cbo security state. */
	private List<ParameterTable> lstCboSecurityOnlyRegisterState;
	
	/** The lst cbo boolean type. */
	private List<BooleanType> lstCboBooleanType;
	
	/** The lst cbo interest type. */
	private List<ParameterTable> lstCboInterestType;
	
	private List<ParameterTable> lstCboEncoderAgent;
	
	/** The lst cbo calendar type. */
	private List<ParameterTable> lstCboCalendarType;
	
	/** The lst cbo calendar days. */
	private List<ParameterTable> lstCboCalendarDays;
	
	/** The lst cbo rate type. */
	private List<ParameterTable> lstCboRateType;
	
	/** The lst cbo operation type. */
	private List<ParameterTable> lstCboOperationType;
	
	/** The lst cbo interest periodicity. */
	private List<ParameterTable> lstCboInterestPeriodicity;
	
	/** The lst cbo yield. */
	private List<ParameterTable> lstCboYield;
	
	/** The lst cbo cash nominal. */
	private List<ParameterTable> lstCboCashNominal;
	
	/** The lst cbo amortization type. */
	private List<ParameterTable> lstCboAmortizationType;
	
	/** The lst cbo amortization on. */
	private List<ParameterTable> lstCboAmortizationOn;
	
	/** The lst cbo indexed to. */
	private List<ParameterTable> lstCboIndexedTo;
	
	/** The lst cbo investor type. */
	private List<ParameterTable> lstCboInvestorType;
	
	/** The lst cbo financial index. */
	private List<ParameterTable> lstCboFinancialIndex;
	
	/** The lst cbo security block motive. */
	private List<ParameterTable> lstCboSecurityBlockMotive;
	
	/** The lst cbo security unblock motive. */
	private List<ParameterTable> lstCboSecurityUnblockMotive;
	
	/** The lst cbo security motive aux. */
	private List<ParameterTable> lstCboSecurityMotiveAux; //Auxiliar
	
	/** The lst cbo credit rating scales. */
	private List<ParameterTable> lstCboCreditRatingScales;
	
	/** The lst cbo securitie term scales. */
	private List<ParameterTable> lstCboSecuritieTerm;
	
	/** The lst cbo economic activity. */
	private List<ParameterTable> lstCboEconomicActivity;
	
	/** The lst cbo economic sector. */
	private List<ParameterTable> lstCboEconomicSector;
	
	/** The lst cbo payment modality. */
	private List<ParameterTable> lstCboPaymentModality;
	
	/** The lst cbo interest payment modality. */
	private List<ParameterTable> lstCboInterestPaymentModality;
	
	/** The lst cbo financial periodicity. */
	private List<ParameterTable> lstCboFinancialPeriodicity;
	
	/** The lst cbo filter securitie class. */
	private List<ParameterTable> lstCboFilterSecuritieClass;
	
	/** The lst cbo securitie class trg. */
	private List<ParameterTable> lstCboSecuritieClassTrg;
	
	/** The lst cbo filter currency. */
	private List<ParameterTable> lstCboFilterCurrency;
	
	/** The lst cbo convertible stock type. */
	private List<ParameterTable> lstCboConvertibleStockType;
	
    /** The lst dtb international depository. */
	private List<InternationalDepository> lstDtbInternationalDepository;
    
    /** The lst dtb negotiation modalities. */
    private List<NegotiationModality> lstDtbNegotiationModalities;

    /** The lst program interest diff. */
    private List<Map<String,Object>> lstProgramInterestDiff;
    
    /** The lst program amortization diff. */
    private List<Map<String,Object>> lstProgramAmortizationDiff;
    
    /** The lst security selection. */
    private List<Security> 			 lstSecuritySelection;
    
    private List<PaymentAgent> listPaymentAgents;
    
    List<SecuritySerialRange> lstSecuritySerialRange;
    
	List<PhysicalCertificateTO> lstResult;
	/** The security. */
	private Security security;

	/** The security uploaded. */
	private Security securityUploaded;
	
	/** The security code format. */
	private SecurityCodeFormat securityCodeFormat;
	
	/** The security history state. */
	private SecurityHistoryState securityHistoryState;

	/** The program amortization coupon. */
	private ProgramAmortizationCoupon programAmortizationCoupon; 
		
	/** The security t osearch. */
	private SecurityTO securityTOsearch;
	
	/** The parameter table to. */
	private ParameterTableTO parameterTableTO;
	
	/** The cfi category. */
	private CfiCategory cfiCategory;
	
	/** The cfi group. */
	private CfiGroup cfiGroup;
	
	/** The cfi attribute1. */
	private CfiAttribute cfiAttribute1;
	
	/** The cfi attribute2. */
	private CfiAttribute cfiAttribute2;
	
	/** The cfi attribute3. */
	private CfiAttribute cfiAttribute3;
	
	/** The cfi attribute4. */
	private CfiAttribute cfiAttribute4;
	
	/** The aux vldtn international deposits. */
	private Integer auxVldtnInternationalDeposits;
	
	/** The aux vldtn negotiation mechanism. */
	private Integer auxVldtnNegotiationMechanism;
	
	/** The bl interest for expiration. */
	private boolean blAmortizationForExpiration, blInterestForExpiration; //, blShortTerm, blLongTerm
	
	/** The aux vldt investor type. */
	private Integer auxVldtInvestorType=Integer.valueOf(1);
	
	/** The aux vldt investor origin. */
	private Integer auxVldtInvestorOrigin=Integer.valueOf(1);
	
	/** The payment number amortization. */
	private Integer paymentNumberAmortization;
	
	/** The _360_ calendar day. */
	private Integer _360_CalendarDay=CalendarDayType._360.getCode();
	
	/** The variabel interest type code. */
	private Integer variableInterestTypeCode=InterestType.VARIABLE.getCode();
	
	/** The cero interest type code. */
	private Integer ceroInterestTypeCode=InterestType.CERO.getCode();
	
	/** The initial state search form. */
	String initialStateSearchForm;
	
	/** The security initial state. */
	private Security securityInitialState;
	
	/** The security initial state str. */
	private String securityInitialStateStr;
	
	/** The is re generated. */
	private boolean isReGenerated;
	
	/** The is first re generated. */
	private boolean isFirstReGenerated=true;
	
	/** The redirect from payment crono query. */
	private boolean redirectFromPaymentCronoQuery;
	
	/** The redirect from payment crono adv query. */
	private boolean redirectFromPaymentCronoAdvQuery;
	
	/** The is Exchange Rate. */
	private boolean isExchangeRate;
	
	/** The issuer helper search. */
	private Issuer issuerHelperSearch;
	
	/** The issuer helper mgmt. */
	private Issuer issuerHelperMgmt;
	
	
	/** The issuance helper search. */
	private Issuance issuanceHelperSearch;
	
	/** The issuance helper mgmt. */
	private Issuance issuanceHelperMgmt;
	
	/** The search filters concat. */
	private String searchFiltersConcat;
	
	/** The mechanism columns model. */
	private List<GenericColumnModel> mechanismColumnsModel;
	
	/*Valuator start*/
	
	/** The valuator process class. */
	private ValuatorProcessClass valuatorProcessClass;
	
	/** The valuator term range. */
	private ValuatorTermRange valuatorTermRange;
	
	/** The massive interface type. */
	private String massiveInterfaceType = ComponentConstant.INTERFACE_SECURITIES_REGISTER;
	
	/** The bln is placement segment. */
	private boolean blnIsPlacementSegment;
	/*Valuator end*/
	
	/** The bl not trded. */
	private boolean blNotTrded;
	
	private boolean selectAllCouponsInterest;
	private boolean selectAllCouponsAmortization;
	private int paymentAmortizationDays;
	
	private CommonsUtilities commonsUtilities; 
	
	private SecuritySerialRange securitySerialRangeSelected;
	
	
	/**Habilita y deshabilita agente pagador*/
	private boolean disableAgent;
	
	/**
	 * Instantiates a new security mgmt bean.
	 */
	public SecurityMgmtBean(){

	}
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		try {
			super.setParametersTableMap(new HashMap<Integer,Object>());
			
			commonsUtilities=new CommonsUtilities();
			 
			security=new Security(); 
			securityCodeFormat =  new SecurityCodeFormat();
			securityHistoryState=new SecurityHistoryState();
			
			valuatorProcessClass=new ValuatorProcessClass();
			valuatorTermRange=new ValuatorTermRange();
			
			
			disableAgent =false;
			securityTOsearch=new SecurityTO();

			security.setIssuer( new Issuer() );
			security.setInterestPaymentSchedule(new InterestPaymentSchedule());
			security.setAmortizationPaymentSchedule(new AmortizationPaymentSchedule());
			initialStateSearchForm=securityTOsearch.toString();
			
			issuerHelperSearch=new Issuer();
			issuerHelperMgmt=new Issuer();
			issuanceHelperSearch=new Issuance();
			issuanceHelperMgmt=new Issuance();
			
			
			allHolidays=generalParameterFacade.getAllHolidaysByCountry(countryResidence) ;
			
			loadCboCFICategories();
			loadCboInstrumentType();
			loadCboSecuritiesState();
			loadCboSecuritiesRegisterState();
			loadCboIssuerDocumentType();
			loadCboFilterSecuritieClass();
			loadCboFilterCurency();
			loadCboSecuritieTerm();
			loadCboConvertibleStockType(); 
			loadCboPaymentScheduleSituation();
			loadCboPaymentScheduleState();
			loadCboSecuritieClassTarget();
			
			if((userInfo.getUserAccountSession().isIssuerInstitucion()|| userInfo.getUserAccountSession().isIssuerDpfInstitucion()) && 
					Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getIssuerCode())){
				Issuer issuerFilter=new Issuer();
				issuerFilter.setIdIssuerPk(userInfo.getUserAccountSession().getIssuerCode());
				loggedIssuer=new Issuer();
				loggedIssuer=issuerServiceFacade.findIssuerByFiltersServiceFacade(issuerFilter);				
				issuerHelperSearch=loggedIssuer;
				if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
					List<ParameterTable> lstSecuritieClass = new ArrayList<ParameterTable>();
					List<Integer>lstSecurityClass = new ArrayList<Integer>();
					for(ParameterTable objParameterTable : lstCboFilterSecuritieClass){
						if(objParameterTable.getParameterTablePk().equals(SecurityClassType.DPA.getCode()) || 
								objParameterTable.getParameterTablePk().equals(SecurityClassType.DPF.getCode())){
							lstSecuritieClass.add(objParameterTable);
							lstSecurityClass.add(objParameterTable.getParameterTablePk());
						}
					}
					lstCboFilterSecuritieClass = lstSecuritieClass;
					securityTOsearch.setLstSecurityClass(lstSecurityClass);
				}
			}
			setViewOperationType(ViewOperationsType.CONSULT.getCode());
			searchFiltersConcat=buildCiteriaSearchConcat();
			blNotTrded = false;
			
			//por defecto ningun cupon esta seleccionado
			selectAllCouponsInterest=false;
			selectAllCouponsAmortization=false;
			paymentAmortizationDays=0;
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire( new ExceptionToCatchEvent(e) );
		}
	}
	
	/** metodo para seleccionar o deseleccionar todos los cupones de interEs segun se marque o no la casilla global */
	public void selectAndUnselectAllCouponsAmortization() {
		if (selectAllCouponsAmortization) {
			// seleccionando todos los cupones vigentes
			for(ProgramAmortizationCoupon programAmortizationCoupon : security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons()){
				if (programAmortizationCoupon.getStateProgramAmortization().equals(ProgramScheduleStateType.PENDING.getCode())) {
					programAmortizationCoupon.setSelected(true);
				}
			}
		} else {
			// deseleccionando todos los cupones vigentes
			for(ProgramAmortizationCoupon programAmortizationCoupon : security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons()){
				if (programAmortizationCoupon.getStateProgramAmortization().equals(ProgramScheduleStateType.PENDING.getCode())) {
					programAmortizationCoupon.setSelected(false);
				}
			}
		}
	}
	
	/**
	 * metodo para cambiar los dias de pago para calcular las las fechas de vencimiento en la cuponera de amortizaciones
	 * @param programInterestCoupon
	 */
	public void changePaymentDaysToAmortization(ProgramAmortizationCoupon programAmortizationCoupon){
		try {
			
			int paymentDays=programAmortizationCoupon.getPaymentAmortizationDays();
			if(paymentDays<0){
				//si un numero negativo setear en 0 por defecto
				paymentDays=0;
				programAmortizationCoupon.setPaymentAmortizationDays(paymentDays);
			}
			Calendar calendar=Calendar.getInstance();
			calendar.setTime(programAmortizationCoupon.getBeginingDate());
			calendar.add(Calendar.DAY_OF_YEAR, paymentDays);
			Date dateAdded=calendar.getTime();
			
			//======================== BEGIN RECONSTRUCCION DE FECHAS EN CASCADA
			//antes de setear las fechas validando que los dias de pago no hagan salir en negativo al siguiente cupon
			int indexCouponModified=security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().indexOf(programAmortizationCoupon);
			//HALLANDO EL LIMITE DE DIAS DE PAGO PERMITIDOS PARA VALIDAR QUE NO SE EXCEDA DICHO LIMITE
			int progAmoCouponSize=security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().size();
			ProgramAmortizationCoupon programAmortizationCouponNext;
			Date dateExpirationAmortizationCoupoNext;
			//sumatoria de dias pago de cupones anteriores al cupon modificado
			int summationPaymentDaysBefore=0;
			for(int i=0;i<progAmoCouponSize;i++){
				if(indexCouponModified>i){
					//aumentando en una unidad para los cupones siguientes al modificado
					summationPaymentDaysBefore++;
				}else{
					if(indexCouponModified!=i){
						//hallando la sumatoria de dias de pago de los cupones anteriores al modificado
						programAmortizationCouponNext=security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().get(i);
						summationPaymentDaysBefore=summationPaymentDaysBefore+CommonsUtilities.getDaysBetween(programAmortizationCouponNext.getBeginingDate(), programAmortizationCouponNext.getExpirationDate());
					}
				}
			}
			int limitMaxPaymentDays=summationPaymentDaysBefore+CommonsUtilities.getDaysBetween(programAmortizationCoupon.getBeginingDate(), programAmortizationCoupon.getExpirationDate());
			if(limitMaxPaymentDays>security.getSecurityDaysTerm()){
				//SE RECHAZA EL CALCULO EN CASCADA Y SE CALCULA SOLO PARA EL SIGUIENTE CUPON
				indexCouponModified++;
				if(security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().get(indexCouponModified)!=null){
					programAmortizationCouponNext=security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().get(indexCouponModified);
					// obteniendo la fecha de vencimiento del siguiente cupon
					Date dateExpirationProgAmortCoupNext=programAmortizationCouponNext.getExpirationDate();
					
					// verificando si dateAdded es mayor a dateExpirationProgAmortCoupNext
					int paymentDaysToAmortizacion;
					if(dateAdded.compareTo(dateExpirationProgAmortCoupNext)>0){
						//si es mayor realizar un recalculo
						// reduciendo en 1 dia la fecha de vencimiento del siguiente cupon
						calendar.setTime(dateExpirationProgAmortCoupNext);
						calendar.add(Calendar.DAY_OF_YEAR, -1);
						dateAdded=calendar.getTime();
						paymentDaysToAmortizacion=CommonsUtilities.getDaysBetween(programAmortizationCoupon.getBeginingDate(),dateAdded);
						programAmortizationCoupon.setPaymentAmortizationDays(paymentDaysToAmortizacion);
					}
				}
				
				programAmortizationCoupon.setExpirationDate(dateAdded);
				changeProgramAmortizationDate(programAmortizationCoupon, "E", true);
			}else{
				//SE REALIZARA UN CALCULO EN CASCADA SI ASI SE REQUIERE
				for(int i = indexCouponModified; i< (progAmoCouponSize-1);i++ ){
					//obteniendo el siguiente cupon para verificar si se sobrepasa su fecha de vencimiento
					i++;
					if(security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().get(i)!=null){
						programAmortizationCouponNext=security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().get(i);
						dateExpirationAmortizationCoupoNext=programAmortizationCouponNext.getExpirationDate();
						
						programAmortizationCoupon.setExpirationDate(dateAdded);
						changeProgramAmortizationDate(programAmortizationCoupon, "E", true);
						//verificando si dateAdded es mayor a dateExpirationAmortizationCoupoNext
						//si fuera mayor hay que seguir recalculando (volver al metodo recursivo)
						if(dateAdded.compareTo(dateExpirationAmortizationCoupoNext)>0){
							//realizando diferencia de 1 dia entre [fecVencimiento - fecInicial]
							calendar.setTime(dateAdded);
							calendar.add(Calendar.DAY_OF_YEAR, 1);
							programAmortizationCouponNext.setExpirationDate(calendar.getTime());
							//modificando la fecha inicial
							programAmortizationCouponNext.setBeginingDate(programAmortizationCoupon.getExpirationDate());
							programAmortizationCouponNext.setPaymentAmortizationDays(1);
							//realizando recursividad
							changePaymentDaysToAmortization(programAmortizationCouponNext);
						}else{
							//saliendo del bucle por que ya no es necesario hacer recalculo para los demas cupones
							break;
						}
					}
				}
			}
			//======================== END RECONSTRUCCION DE FECHAS EN CASCADA
			
//			index++;
//			ProgramAmortizationCoupon programAmortizationCouponNext;
//			if(security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().get(index)!=null){
//				programAmortizationCouponNext=security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().get(index);
//				// obteniendo la fecha de vencimiento del siguiente cupon
//				Date dateExpirationProgAmortCoupNext=programAmortizationCouponNext.getExpirationDate();
//				
//				// verificando si dateAux es mayor a dateExpirationProgAmortCoupNext
//				int paymentDaysToAmortizacion;
//				if(dateAux.compareTo(dateExpirationProgAmortCoupNext)>0){
//					//si es mayor realizar un recalculo
//					// reduciendo en 1 dia la fecha de vencimiento del siguiente cupon
//					calendar.setTime(dateExpirationProgAmortCoupNext);
//					calendar.add(Calendar.DAY_OF_YEAR, -1);
//					dateAux=calendar.getTime();
//					paymentDaysToAmortizacion=CommonsUtilities.getDaysBetween(programAmortizationCoupon.getBeginingDate(),dateAux);
//					programAmortizationCoupon.setPaymentAmortizationDays(paymentDaysToAmortizacion);
//				}
//			}
//			
//			programAmortizationCoupon.setExpirationDate(dateAux);
//			changeProgramAmortizationDate(programAmortizationCoupon, "E", true);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/** metodo para seleccionar o deseleccionar todos los cupones de interEs segun se marque o no la casilla global */
	public void selectAndUnselectAllCouponsInterest() {
		if (selectAllCouponsInterest) {
			// seleccionando todos los cupones vigentes
			for(ProgramInterestCoupon programInterestCoupon : security.getInterestPaymentSchedule().getProgramInterestCoupons()){
				if (programInterestCoupon.getStateProgramInterest().equals(ProgramScheduleStateType.PENDING.getCode())) {
					programInterestCoupon.setIsSelected(true);
				}
			}
		} else {
			// deseleccionando todos los cupones vigentes
			for(ProgramInterestCoupon programInterestCoupon : security.getInterestPaymentSchedule().getProgramInterestCoupons()){
				if (programInterestCoupon.getStateProgramInterest().equals(ProgramScheduleStateType.PENDING.getCode())) {
					programInterestCoupon.setIsSelected(false);
				}
			}
		}
	}
	
	/**
	 * metodo para cambiar los dias de pago para calcular las las fechas de vencimiento en la cuponera de Interes
	 * @param programInterestCoupon
	 */
	public void changePaymentDays(ProgramInterestCoupon programInterestCoupon){
		try {
			
			int paymentDays=programInterestCoupon.getPaymentDays();
			if(paymentDays<0){
				//si un numero negativo setear en 0 por defecto
				paymentDays=0;
				programInterestCoupon.setPaymentDays(paymentDays);
			}
			
			//calculando la fecha con los dias adiconados
			Calendar calendar=Calendar.getInstance();
			calendar.setTime(programInterestCoupon.getBeginingDate());
			calendar.add(Calendar.DAY_OF_YEAR, paymentDays);
			Date dateAdded=calendar.getTime();
			
			//======================== BEGIN RECONSTRUCCION DE FECHAS EN CASCADA
			//indice del cupon modificado actualmente
			int indexCouponModified=security.getInterestPaymentSchedule().getProgramInterestCoupons().indexOf(programInterestCoupon);
			//HALLANDO EL LIMITE DE DIAS DE PAGO PERMITIDOS PARA VALIDAR QUE NO SE EXCEDA DICHO LIMITE
			int progIntCouponSize=security.getInterestPaymentSchedule().getProgramInterestCoupons().size();
			ProgramInterestCoupon programInterestCouponNext;
			Date dateExpirationInterestCouponNext;
			//sumatoria de dias pago de cupones anteriores al cupon modificado
			int summationPaymentDaysBefore=0;
			for(int i = 0; i< progIntCouponSize;i++ ){
				indexCouponModified=security.getInterestPaymentSchedule().getProgramInterestCoupons().indexOf(programInterestCoupon);
				if(indexCouponModified>i){
					//aumentando en una unidad para los cupones siguientes al modificado
					summationPaymentDaysBefore++;
				}else{
					if(indexCouponModified!=i){
						//hallando la sumatoria de dias de pago de los cupones anteriores al modificado
						programInterestCouponNext=security.getInterestPaymentSchedule().getProgramInterestCoupons().get(i);
						summationPaymentDaysBefore=summationPaymentDaysBefore+CommonsUtilities.getDaysBetween(programInterestCouponNext.getBeginingDate(),programInterestCouponNext.getExperitationDate());
					}
				}
			}
			int limitMaxPaymentDays=summationPaymentDaysBefore+CommonsUtilities.getDaysBetween(programInterestCoupon.getBeginingDate(),programInterestCoupon.getExperitationDate());
			if(limitMaxPaymentDays>security.getSecurityDaysTerm()){
				//SE RECHAZA EL CALCULO EN CASCADA Y SE CALCULA SOLO PARA EL SIGUIENTE CUPON
				indexCouponModified++;
				if(security.getInterestPaymentSchedule().getProgramInterestCoupons().get(indexCouponModified)!=null){
					programInterestCouponNext=security.getInterestPaymentSchedule().getProgramInterestCoupons().get(indexCouponModified);
					// obteniendo la fecha de vencimiento del siguiente cupon
					dateExpirationInterestCouponNext=programInterestCouponNext.getExperitationDate();
					
					//verificando si dateAdded es mayor a dateExpirationInterestCouponNext
					int paymentDaysToInterest;
					if(dateAdded.compareTo(dateExpirationInterestCouponNext)>0){
						//si es mayor realizar un recalculo
						// reduciendo en 1 dia la fecha de vencimiento del siguiente cupon
						calendar.setTime(dateExpirationInterestCouponNext);
						calendar.add(Calendar.DAY_OF_YEAR, -1);
						dateAdded=calendar.getTime();
						paymentDaysToInterest=CommonsUtilities.getDaysBetween(programInterestCoupon.getBeginingDate(),dateAdded);
						programInterestCoupon.setPaymentDays(paymentDaysToInterest);
					}
				}
				programInterestCoupon.setExperitationDate(dateAdded);
				changeProgramInterestDate(programInterestCoupon, "E", true);
			}else{
				//SE REALIZARA UN CALCULO EN CASCADA SI ASI SE REQUIERE
				for(int i = indexCouponModified; i< (progIntCouponSize-1);i++ ){
					//obteniendo el siguiente cupon para verificar si se sobrepasa su fecha de vencimiento
					i++;
					if(security.getInterestPaymentSchedule().getProgramInterestCoupons().get(i)!=null){
						programInterestCouponNext=security.getInterestPaymentSchedule().getProgramInterestCoupons().get(i);
						dateExpirationInterestCouponNext=programInterestCouponNext.getExperitationDate();
						
						programInterestCoupon.setExperitationDate(dateAdded);
						changeProgramInterestDate(programInterestCoupon, "E", true);
						//verificando si dateAdded es mayor a dateExpirationInterestCouponNext
						//si fuera mayor hay que seguir recalculando (volver al metodo recursivo)
						if(dateAdded.compareTo(dateExpirationInterestCouponNext)>0){
							
							//realizando diferencia de 1 dia entre [fecVencimiento - fecInicial]
							calendar.setTime(dateAdded);
							calendar.add(Calendar.DAY_OF_YEAR, 1);
							programInterestCouponNext.setExperitationDate(calendar.getTime());
							//modificando la fecha inicial
							programInterestCouponNext.setBeginingDate(programInterestCoupon.getExperitationDate());
							programInterestCouponNext.setPaymentDays(1);
							//realizando recursividad
							changePaymentDays(programInterestCouponNext);
							
						}else{
							//saliendo del bucle por que ya no es necesario hacer recalculo para los demas cupones
							break;
						}
					}
				}
			}
			//======================== END RECONSTRUCCION DE FECHAS EN CASCADA

			//======================================= codigo en reconstruccion =======================
			//int index=security.getInterestPaymentSchedule().getProgramInterestCoupons().indexOf(programInterestCoupon);
			//index++;
			//obteniendo el siguiente cupon
//			ProgramInterestCoupon programInterestCouponNext;
//			if(security.getInterestPaymentSchedule().getProgramInterestCoupons().get(index)!=null){
//				programInterestCouponNext=security.getInterestPaymentSchedule().getProgramInterestCoupons().get(index);
//				// obteniendo la fecha de vencimiento del siguiente cupon
//				Date dateExpirationInterestCouponNext=programInterestCouponNext.getExperitationDate();
//				
//				//verificando si dateAdded es mayor a dateExpirationInterestCouponNext
//				int paymentDaysToInterest;
//				if(dateAdded.compareTo(dateExpirationInterestCouponNext)>0){
//					//si es mayor realizar un recalculo
//					// reduciendo en 1 dia la fecha de vencimiento del siguiente cupon
//					calendar.setTime(dateExpirationInterestCouponNext);
//					calendar.add(Calendar.DAY_OF_YEAR, -1);
//					dateAdded=calendar.getTime();
//					paymentDaysToInterest=CommonsUtilities.getDaysBetween(programInterestCoupon.getBeginingDate(),dateAdded);
//					programInterestCoupon.setPaymentDays(paymentDaysToInterest);
//				}
//			}
//			
//			programInterestCoupon.setExperitationDate(dateAdded);
//			changeProgramInterestDate(programInterestCoupon, "E", true);
			//======================================= codigo en reconstruccion =======================
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	
	/**
	 * Registration operation handler.
	 *
	 * @return the string
	 */
	public String registerOperationHandler() { 
		try {
			defaultRegisterView();
			
			loadGeneralsCombos();
			valuatorHistoryDetailBean.setViewOperationType(getViewOperationType());
			valuatorHistoryDetailBean.setSecurity(security);
			valuatorHistoryDetailBean.initializeComponent();
			
			return "securityMgmtRule";
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}
	
	/**
	 * Register massive operation handler.
	 *
	 * @return the string
	 */
	public String registerMassiveOperationHandler() { 
		return "searchInterfaceFilesMgmt";
	}
	
	/**
	 * Default register view.
	 *
	 * @throws Exception the exception
	 */
	private void defaultRegisterView() throws Exception{
		setViewOperationType( ViewOperationsType.REGISTER.getCode() );
		security=new Security();
		security.setIssuer(new Issuer());
		security.setIssuance(new Issuance());
		security.getIssuance().setGeographicLocation(new GeographicLocation());
		
		listPaymentAgents = null;
		
		programAmortizationCoupon=new ProgramAmortizationCoupon();
		lstDtbNegotiationModalities=null;
		//Default values on register 
		security.defaultValues();
		security.setSecurityOrigin(new Security());
		security.setAmortizationPaymentSchedule( new AmortizationPaymentSchedule() );
		security.setInterestPaymentSchedule( new InterestPaymentSchedule() );
		
		issuanceHelperMgmt=new Issuance();
		issuerHelperMgmt=new Issuer();
		if((userInfo.getUserAccountSession().isIssuerInstitucion()|| userInfo.getUserAccountSession().isIssuerDpfInstitucion()) &&
				Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getIssuerCode())){
			Issuer issuerFilter=new Issuer();
			issuerFilter.setIdIssuerPk( userInfo.getUserAccountSession().getIssuerCode() );
			loggedIssuer=new Issuer();
			loggedIssuer=issuerServiceFacade.findIssuerByFiltersServiceFacade(issuerFilter);
			
			issuerHelperMgmt=issuerHelperSearch;
		}
	}
	
	/**
	 * Validate is valid user.
	 *
	 * @param security the security
	 * @return the list
	 */
	private List<String> validateIsValidUser(Security security) {	
		List<OperationUserTO> lstOperationUserParam = new ArrayList<OperationUserTO>();
		List<String> lstOperationUserResult;	
		OperationUserTO operationUserTO = new OperationUserTO();  
		operationUserTO.setOperNumber(security.getIdSecurityCodePk());
		operationUserTO.setUserName(security.getRegistryUser());
		lstOperationUserParam.add(operationUserTO);
		lstOperationUserResult = getIsSubsidiary(userInfo,lstOperationUserParam);
		return lstOperationUserResult;
	}
	/**
	 * Modification operation handler.
	 *
	 * @return the string
	 */
	public String modificationOperationHandler(){
		try {
			if(security==null){
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null,
						PropertiesConstants.ERROR_RECORD_REQUIRED, null);
				JSFUtilities
						.executeJavascriptFunction("PF('cnfwMsgCustomValidationSec').show()");
				return null;
			}
			
			setViewOperationType( ViewOperationsType.MODIFY.getCode() );

			SecurityTO securityTO=new SecurityTO();
			securityTO.setIdSecurityCodePk( security.getIdSecurityCodePk() );

			security=securityServiceFacade.findSecurityServiceFacade( securityTO );
			lstSecuritySerialRange = securityServiceFacade.findSecuritySerialRange(security.getIdSecurityCodePk());
			
			if(security.getValuatorProcessClass()==null){
				security.setValuatorProcessClass(new ValuatorProcessClass());
			}
			
			issuerHelperMgmt=security.getIssuer();
			issuanceHelperMgmt=security.getIssuance();
			
			billParameterTableById(security.getSecurityType());
			billParameterTableById(security.getStateSecurity());
			
			//for old securities with no security inverstor type
			if(security.getSecurityInvestor()==null){
				security.setSecurityInvestor(new SecurityInvestor());
				security.getSecurityInvestor().setSecurity(security);
				security.getSecurityInvestor().setSecurityInvestorState(SecurityInversionistStateType.REGISTERED.getCode());
			}
			loadGeneralsCombos();
			loadDtbNegotiationModality(security.getInstrumentType());
			if(security.isPartQuoteSecurityType()){
				loadeSecurityQuoteList();
			}
			if(security.isFixedInstrumentTypeAcc()){
				loadCboSecurityTypeByInstrumentType(security.getInstrumentType());
			}
			if(security.isVariableInstrumentTypeAcc()){
				security.setInterestPaymentSchedule(new InterestPaymentSchedule());
				security.setAmortizationPaymentSchedule( new AmortizationPaymentSchedule() );
			}
			if(security.isFixedInstrumentTypeAcc()){
				
				if(security.getCurrency().equals(CurrencyType.UFV.getCode()) || 
						security.getCurrency().equals(CurrencyType.DMV.getCode())){
					this.setExchangeRate(true);
				}else{
					this.setExchangeRate(false);
				}
				
				security.setInterestPaymentSchedule( 
						securityServiceFacade.findActiveInterestPaymentScheduleBySecurity(security.getIdSecurityCodePk()) );
				security.setAmortizationPaymentSchedule(
						securityServiceFacade.findActiveAmortizationPaymentSchedule(security.getIdSecurityCodePk()));
				
				List<ProgramInterestCoupon> lstProgramInterestCoupon=null;
				List<ProgramAmortizationCoupon> lstProgramAmortizationCoupon=null;
				if(security.getInterestPaymentSchedule()!=null){
					lstProgramInterestCoupon=securityServiceFacade.findProgramInterestCouponsServiceFacade(security.getInterestPaymentSchedule().getIdIntPaymentSchedulePk()  );
				}else{
					security.setInterestPaymentSchedule(new InterestPaymentSchedule());
				}
				if(security.getAmortizationPaymentSchedule()!=null){
					 lstProgramAmortizationCoupon=securityServiceFacade.findProgramAmortizationCouponsServiceFacade(security.getAmortizationPaymentSchedule().getIdAmoPaymentSchedulePk());
				}else{
					security.setAmortizationPaymentSchedule(new AmortizationPaymentSchedule());
				}

				
				security.getInterestPaymentSchedule().setAllCouponsPendingPayment( Boolean.TRUE );
				security.getAmortizationPaymentSchedule().setAllCouponsPendingPayment( Boolean.TRUE );
				
				if(lstProgramInterestCoupon!=null){
					for(ProgramInterestCoupon proIntCoup : lstProgramInterestCoupon){
						if(ProgramScheduleStateType.EXPIRED.getCode().equals(proIntCoup.getStateProgramInterest())){
							security.getInterestPaymentSchedule().setAllCouponsPendingPayment(Boolean.FALSE);
							break;
						}	
					}
					security.getInterestPaymentSchedule().setProgramInterestCoupons( lstProgramInterestCoupon );
				}

				if(lstProgramAmortizationCoupon!=null){
					for(ProgramAmortizationCoupon proAmoCoup : lstProgramAmortizationCoupon){
						if(ProgramScheduleStateType.EXPIRED.getCode().equals( proAmoCoup.getStateProgramAmortization() )){
							security.getAmortizationPaymentSchedule().setAllCouponsPendingPayment(Boolean.FALSE);
							break;
						}
					}
					//calculando los dias de pago para la pantalla de consulta
					int paymentDayToCouponAmortization;
					for(ProgramAmortizationCoupon proAmoCoup : lstProgramAmortizationCoupon){
						paymentDayToCouponAmortization=CommonsUtilities.getDaysBetween(proAmoCoup.getBeginingDate(), proAmoCoup.getExpirationDate());
						proAmoCoup.setPaymentAmortizationDays(paymentDayToCouponAmortization);
					}
					security.getAmortizationPaymentSchedule().setProgramAmortizationCoupons( lstProgramAmortizationCoupon  );	
				}
			}
			loadCboEconomicActivity(security.getIssuer().getEconomicSector());
			securityInitialState=(Security)security.clone();
			
			valuatorHistoryDetailBean.setViewOperationType(getViewOperationType());
			valuatorHistoryDetailBean.setSecurity(security);
			valuatorHistoryDetailBean.initializeComponent();
			
			lstResult = issuanceSecuritiesServiceFacade.searchPhysicalCerticate(security.getIdSecurityCodePk());
			
			if(Validations.validateIsNotNullAndNotEmpty(security.getNotTraded()) && 
					BooleanType.YES.getCode().equals(security.getNotTraded())){
				blNotTrded = Boolean.TRUE;
				lstDtbNegotiationModalities = new ArrayList<NegotiationModality>();
			} else {
				blNotTrded = Boolean.FALSE;
			}
			
			/*Buscamos la lista de agente pagadores de acuerdo a la emision*/
			if(security.getIssuance()!=null&&(!security.getIssuance().getSecurityClass().equals(SecurityClassType.DPF)||security.getIssuance().getSecurityClass().equals(SecurityClassType.DPA))){
				listPaymentAgents = new ArrayList<>();
				listPaymentAgents = parameterServiceBean.getListPaymentAgentContacsByEcomicActivity(null);
				disableAgent = false;
			}else
				disableAgent = true;
			
			return "securityMgmtRule";
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}
	
	/**
	 * Generation file operation handler.
	 *
	 * @throws ServiceException the service exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void generationFileOperationHandler() throws ServiceException, IOException{
		
		if(!userInfo.getUserAccountSession().isParticipantInstitucion()){
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.ERROR_GENERATION_FILE_NOT_ALLOWED));
			JSFUtilities.showSimpleValidationDialog();
			
		}
		else if(Validations.validateListIsNotNullAndNotEmpty(lstSecuritySelection)){
						
			showMessageOnDialog(
					PropertiesConstants.LBL_HEADER_ALERT_CONFIRM, null, 
					PropertiesConstants.SECURITY_CONFIRM_GENERATE_FILE, null);
			JSFUtilities.executeJavascriptFunction("PF('cnfwConfirmOperation').show();");
			
			
		}else{
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			JSFUtilities.showSimpleValidationDialog();
			
		}
		
	}
	
	/**
	 * Process operation generate file.
	 */
	public void processOperationGenerateFile(){
		
		List<Object> listClass=new ArrayList<>();
		Long idParticipant=null;
		String mnemonic="";
		if(userInfo.getUserAccountSession().isParticipantInstitucion()||
				userInfo.getUserAccountSession().isAfpInstitution()){
			idParticipant=userInfo.getUserAccountSession().getParticipantCode();
			mnemonic=securityServiceFacade.gettingMnemonicParticipant(idParticipant);
		}
		
		Map<String,Object> parameter=new HashMap<String,Object>();
		parameter.put(GeneralConstants.PARTICIPANT_CODE, mnemonic);
		
		File xmlGenericFile = null;
		
		InstitutionType institutionType = InstitutionType.PARTICIPANT;
		String fileName=CommonsUtilities.createNameFile(GeneralConstants.TEMPLATE_NAME_SECURITY_TR_CR, parameter);
		
		try {
			List<File> filesToSend = new ArrayList<>();
			
			listClass=securityServiceFacade.createBeanFileTrCr(lstSecuritySelection);
			
			for (Object object : listClass) {
				List<GenerationFileDetailCouponTRCRTO> listDetailCouponTrCr=null;
				GenerationFileSecurityTRCRTO securityTrCr=(GenerationFileSecurityTRCRTO)object;
				listDetailCouponTrCr=securityServiceFacade.findSecuritiesCouponDetailTRCR(securityTrCr.getIdSecurityCodePk());			
				securityTrCr.setListGenerationFileDetailCoupon(listDetailCouponTrCr);
			}
			
			xmlGenericFile=CommonsUtilities.generateXmlFile(listClass, fileName, ComponentConstant.WAY_INTERFACE_FILE,
										ComponentConstant.INTERFACE_TAG_ISSUANCES,ComponentConstant.INTERFACE_TAG_ISSUANCE);
			filesToSend.add(xmlGenericFile);
			
			securityServiceFacade.sendReport(institutionType, filesToSend,idParticipant,fileName,ReportIdType.EXTERNAL_INTERFACE_GENERATOR.getCode());
			
		} catch (ServiceException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Detail operation handler.
	 *
	 * @param securityParam the security param
	 * @return the string
	 */
	public String detailOperationHandler(Security securityParam) {
		String actionMessage = null;
		try {
			security=securityParam;
			actionMessage = modificationOperationHandler();
			if(actionMessage!=null && !actionMessage.isEmpty()){
				setViewOperationType( ViewOperationsType.DETAIL.getCode() );
				actionMessage = "securityDetailRule";
			}
//			valuatorHistoryDetailBean.setViewOperationType(getViewOperationType());
//			valuatorHistoryDetailBean.setSecurity(security);
//			valuatorHistoryDetailBean.initializeComponent();
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return actionMessage;
	}
	
	/**
	 * Change tab general.
	 *
	 * @param event the event
	 */
	public void changeTabGeneral(TabChangeEvent event){
		try {
		//	Integer viewOpeationtioTypeToComp=Integer.valueOf(JSFUtilities.getRequestParameterMap("viewOpeationtioTypeToComp").toString());
			if(event.getTab().getClientId().equals( SecurityMgmtType.TAB_VALUATOR_HISTORY.getValue() )){
				if(isViewOperationRegister()){
					valuatorHistoryDetailBean.setViewOperationType(getViewOperationType());
					valuatorHistoryDetailBean.initializeComponent();
				}
			}

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change search form.
	 */
	public void changeSearchForm(){
		try {
			String currentStateSearchForm=securityTOsearch.toString();
			if(!initialStateSearchForm.equals( currentStateSearchForm )){
				securityDataModel=null;
			}
			initialStateSearchForm=securityTOsearch.toString();
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Select checkbox int depository.
	 *
	 * @throws Exception the exception
	 */
	public void selectCheckboxIntDepository() throws Exception{
		for(SecurityForeignDepository sec : security.getSecurityForeignDepositories()){
			for(InternationalDepository idep : lstDtbInternationalDepository){
				if(idep.getIdInternationalDepositoryPk().equals( sec.getId().getIdInternationalDepositoryPk() )){
					idep.setSelected(Boolean.TRUE);
					break;
				}
			}
		}
	}
	
	/**
	 * Checks if is assigned sec neg mechanism.
	 *
	 * @param mechModality the mech modality
	 * @return true, if is assigned sec neg mechanism
	 */
	public boolean isAssignedSecNegMechanism(MechanismModality mechModality){
		for(SecurityNegotiationMechanism secNeg : security.getSecurityNegotiationMechanisms()){
			if(mechModality.getIdConverted().equals( secNeg.getMechanismModality().getIdConverted() )){
				return true;
			}
		}
		return false;
	}
	
	
	/**
	 * Load generals combos.
	 *
	 * @throws Exception the exception
	 */
	public void loadGeneralsCombos() throws Exception{
		loadCboIssuanceStateType();
		loadCboBooleanType();
		loadCboInterestType();
		loadCboEncoderAgent();
		loadCboPaymentModality();
		loadCboCalendarType();
		loadCboCalendarDays();
		loadCboRateType();
		loadCboOperationType();
		loadCboInterestPeriodicity();
		loadCboYield();
		loadCboCashNominal();
		loadCboAmortizationType();
		loadCboAmortizationOn();
		loadCboIndexedTo();
		loadCboFinancialIndex();
		loadDtbInternationalDepository();
		loadCboGeographicLocation();
		getLstDepartments(); //issue 708
		loadCboCurrency();
		loadCboIssuanceType();
		loadCboSecuritiesState();
		loadCboSecuritiesRegisterState();		
		loadCboInterestPaymentModality();
		loadCboMonthType();
		loadCboFinancialPeriodicity();
		loadCboCreditRatingScales();
	}
	
	/**
	 * Cfi creation handle.
	 *
	 * @param event the event
	 */
	public void cfiCreationHandler(ActionEvent event){
		try {
			cfiCategory=new CfiCategory();
			cfiGroup=new CfiGroup(new CfiGroupPK());
			cfiAttribute1=new CfiAttribute(new CfiAttributePK());
			cfiAttribute2=new CfiAttribute(new CfiAttributePK());
			cfiAttribute3=new CfiAttribute(new CfiAttributePK());
			cfiAttribute4=new CfiAttribute(new CfiAttributePK());
			
			lstCboCFIGroups=new ArrayList<CfiGroup>();
			lstCboCFIAttribute1=new ArrayList<CfiAttribute>();
			lstCboCFIAttribute2=new ArrayList<CfiAttribute>();
			lstCboCFIAttribute3=new ArrayList<CfiAttribute>();
			lstCboCFIAttribute4=new ArrayList<CfiAttribute>();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Cfi generate handler.
	 *
	 * @param event the event
	 */
	public void cfiGenerateHandler(ActionEvent event){
		try {
			StringBuilder sbCFICode=new StringBuilder();
			sbCFICode.append( cfiCategory.getIdCategoryPk() ).
			append( cfiGroup.getId().getIdGroupPk() ).
			append( cfiAttribute1.getId().getIdAttributePk()).
			append(cfiAttribute2.getId().getIdAttributePk()).
			append( cfiAttribute3.getId().getIdAttributePk() ).
			append( cfiAttribute4.getId().getIdAttributePk() );
			security.setCfiCode( sbCFICode.toString() );
			
			JSFUtilities.executeJavascriptFunction("PF('dlgwCfiRegistry').hide()");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	//HELPER EMISOR
	/**
	 * After issuer handler.
	 */
	public void afterIssuerHandler(){
		try {

			if(issuerHelperMgmt!=null && Validations.validateIsNotNullAndNotEmpty(issuerHelperMgmt.getIdIssuerPk())){
				if(isViewOperationRegister()){
					
					if(issuerHelperMgmt.getStateIssuer().equals( IssuerStateType.BLOCKED.getCode() )){
						Object[] objArg=new Object[1];
						objArg[0]=issuerHelperMgmt.getIdIssuerPk();
						JSFUtilities.showMessageOnDialog(null, null, PropertiesConstants.SECURITY_ERROR_REGISTRY_BLOCK_ISUUER, 
								objArg);
						JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationSec').show();");
						issuerHelperMgmt=new Issuer();
						
						security.setIssuer(null);
						return;
					}
					if(security.isPrepaid()){						
						if(issuerHelperMgmt.getIdIssuerPk().equals(idIssuerBC) || issuerHelperMgmt.getIdIssuerPk().equals(idIssuerTGN)){
							JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
									 PropertiesConstants.SECURITY_ERROR_INCORRECT_ISSUER_PREPAID,null);
							JSFUtilities.showSimpleValidationDialog();
							
							issuerHelperMgmt=new Issuer();				
							security.setIssuer(null);
							return;
						}
					}
					security.setIssuer( issuerHelperMgmt);
					
					security.setIssuance(new Issuance());
					security.getIssuance().setGeographicLocation(new GeographicLocation());	
				}
			}else{
				if(isViewOperationRegister()){
					security.setIssuer(null);
				}
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * After security origin handler.
	 */
	public void afterSecurityOriginHandler(){
		try {
			if(Validations.validateIsNotNullAndNotEmpty(security.getSecurityOrigin().getIdSecurityCodePk())){
				SecurityTO secTO=new SecurityTO();
				secTO.setIdSecurityCodePk( security.getSecurityOrigin().getIdSecurityCodePk() );
				
				Security secTemp=securityServiceFacade.findSecurityServiceFacade( secTO );
				security.setSecurityOrigin(secTemp);
				
				if(security.getSecurityOrigin().isBbxSecurityClass() || security.getSecurityOrigin().isBtxSecurityClass()){	
					registerOperationHandler();
					
					JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
							 PropertiesConstants.SECURITY_ERROR_INCORRECT_CLASS_PREPAID,null);
					JSFUtilities.showSimpleValidationDialog();
					return;					
				}
				//if(security.getSecurityOrigin().getIssuer().getIdIssuerPk().equals(idIssuerBC) || security.getSecurityOrigin().getIssuer().getIdIssuerPk().equals(idIssuerTGN)){
				if(security.getSecurityOrigin().getIssuer().getIdIssuerPk().equals(idIssuerTGN)){
					registerOperationHandler();
					
					JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
							 PropertiesConstants.SECURITY_ERROR_INCORRECT_ISSUER_PREPAID,null);
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
				if(!security.getSecurityOrigin().isEarlyRedemption()){
					registerOperationHandler();
					
					JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
							 PropertiesConstants.SECURITY_ERROR_NO_EARLY_REDEMPTION,null);
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
				
				SecurityTO securityTO=new SecurityTO();
				securityTO.setIdSecurityCodePk( security.getSecurityOrigin().getIdSecurityCodePk() );
	
				Security securityOrigin=security.getSecurityOrigin().clone();
				security=security.getSecurityOrigin().clone();
				modificationOperationHandler();
				security.setSecurityOrigin(securityOrigin);
				
				setViewOperationType( ViewOperationsType.REGISTER.getCode() );
				
				security.setIndPrepaid(BooleanType.YES.getCode());
				security.setIdSecurityCodeOnly(null);
				security.setIdSecurityCodePk(null);
				security.setIdIsinCode(null);//no ISIN				
				security.setCfiCode(null);//no CFI
				
				security.setAlternativeCode(null);
				security.setInterestPaymentSchedule(new InterestPaymentSchedule());
				security.setAmortizationPaymentSchedule(new AmortizationPaymentSchedule());
				security.getSecurityInvestor().setIdSecurityInvestorPk(null);
				
				security.setExpirationDate(null);
				security.setInterestType( InterestType.CERO.getCode() );
				selectExpiredDateHandler();
				changeCboInterestTypeHandler();
				
				security.setCapitalPaymentModality( CapitalPaymentModalityType.AT_MATURITY.getCode() );
				chageCboAmortPaymentModalityHandler();
				
				security.setShareCapital(BigDecimal.ZERO);
				security.setShareBalance(BigDecimal.ZERO);
				security.setCirculationAmount(BigDecimal.ZERO);
				security.setCirculationBalance(BigDecimal.ZERO);
				
				security.setDesmaterializedBalance(BigDecimal.ZERO);
				security.setPhysicalBalance( BigDecimal.ZERO);
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	
	/**
	 * Sets the issuance selected.
	 *
	 * @param issuanceSelected the new issuance selected
	 * @throws Exception the exception
	 */
	public void setIssuanceSelected(Issuance issuanceSelected) throws Exception{
		
		security.setCashNominal(InterestClassType.EFFECTIVE.getCode() );
		
		security.setIssuance( issuanceSelected );
		security.setInstrumentType( issuanceSelected.getInstrumentType() );
		security.setIssuanceForm( issuanceSelected.getIssuanceType() );
		security.setSecurityType( issuanceSelected.getSecurityType() );
		security.setCurrency( issuanceSelected.getCurrency() );
		security.setSecurityClass( issuanceSelected.getSecurityClass() );
		security.setEconomicActivity( security.getIssuer().getEconomicActivity() );
		
		billParameterTableById(security.getSecurityType());
		
		security.defaultValues();

		if(isViewOperationRegister()){
			security.setIssuanceCountry( issuanceSelected.getGeographicLocation() );
			
			if(security.isFixedInstrumentTypeAcc()){
				loadCboSecurityTypeByInstrumentType(security.getInstrumentType());
				
			}

		}

		if(security.getIssuance().getGeographicLocation().getIdGeographicLocationPk().equals( countryResidence )){
			security.setSecuritySource( SecuritySourceType.NATIONAL.getCode() );
		}else {
			security.setSecuritySource( SecuritySourceType.FOREIGN.getCode() );
		}
		loadCboEconomicActivity(security.getIssuer().getEconomicSector());
		
		
		if(issuanceSelected.isDpfDpaSecurityClass()){
			security.setNotTraded(BooleanType.NO.getCode()); 
		} else {
			security.setNotTraded(BooleanType.YES.getCode()); 
		}
	}

	/**
	 * Validate not empty fields.
	 *
	 * @param securityTOsearch the security t osearch
	 * @return true, if successful
	 */
	private boolean validateNotEmptyFields(SecurityTO securityTOsearch) {
		if(Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getIdIssuanceCodePk())
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getIdIssuerCodePk())
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getIdSecurityCodePk())
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getSecurityCurrency())
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getAlternativeCode())				
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getDescriptionSecurity())
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getIssuanceDate())
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getExpirationDate())
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getSecurityClass())
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getSecurityState())
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getIdIsinCodePk())
				|| Validations.validateIsNotNullAndNotEmpty(securityTOsearch.getIssuanceDateMax())){
			return true;
		}
		return false;
	}

	/**
	 * Search securities handler.
	 *
	 * @param event the event
	 */
	public void searchSecuritiesHandler(ActionEvent event){
		JSFUtilities.hideGeneralDialogues();
		try {
			
			securityTOsearch.setIdIssuerCodePk( issuerHelperSearch.getIdIssuerPk() );
			securityTOsearch.setIdIssuanceCodePk( issuanceHelperSearch.getIdIssuanceCodePk() );
			
			if(validateNotEmptyFields(securityTOsearch)){
				List<Security> lstSecurity = null;
				if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
					Participant objParticipant = accountsFacade.getParticipantByPk(userInfo.getUserAccountSession().getParticipantCode());					
					securityTOsearch.setIdParticipant(objParticipant.getIdParticipantPk());
					HolderAccountTO holderAccountTO = new HolderAccountTO();
					holderAccountTO.setParticipantTO(objParticipant.getIdParticipantPk());
					List<Long> lstHolderPk = new ArrayList<Long>();
					HolderSearchTO holderSearchTO = new HolderSearchTO();
					holderSearchTO.setDocumentType(objParticipant.getDocumentType());
					holderSearchTO.setDocumentNumber(objParticipant.getDocumentNumber());
					holderSearchTO.setState(HolderStateType.REGISTERED.getCode());
					List<Holder> lstHolder = helperComponentFacade.findHolders(holderSearchTO);
					if(lstHolder!=null && lstHolder.size()>0){
						for(Holder objHolder : lstHolder){
							lstHolderPk.add(objHolder.getIdHolderPk());
						}
					}
					holderAccountTO.setLstHolderPk(lstHolderPk);
					List<HolderAccount> lstHolderAccount = null;
					if(holderAccountTO.getLstHolderPk()!=null && holderAccountTO.getLstHolderPk().size()>0){
						 lstHolderAccount = accountsFacade.getHolderAccountListComponentServiceFacade(holderAccountTO);
					}						
					List<Long> lstHolderAccountPk = new ArrayList<Long>();
					if(lstHolderAccount!=null && lstHolderAccount.size()>0){
						for(HolderAccount objHolderAccount : lstHolderAccount){
							lstHolderAccountPk.add(objHolderAccount.getIdHolderAccountPk());
						}
					}
					securityTOsearch.setLstHolderAccountPk(lstHolderAccountPk);
					if(securityTOsearch.getIdParticipant()!=null && securityTOsearch.getLstHolderAccountPk()!=null 
							&& securityTOsearch.getLstHolderAccountPk().size()>0){
						lstSecurity=securityServiceFacade.findSecuritiesListForParticipantInvestor(securityTOsearch);
					}
				}else{
					 lstSecurity=securityServiceFacade.findSecuritiesServiceFacade(securityTOsearch);
					 
					 //issue 891
					 //recorremos la lista para mostrar en pantalla la autorizacion
					 if(Validations.validateIsNotNullAndNotEmpty(lstSecurity)){
						 for (int i = 0; i < lstSecurity.size(); i++) {
							 	if(Validations.validateIsNotNullAndNotEmpty(lstSecurity.get(i).getIndAuthorized())){
							 		if (lstSecurity.get(i).getIndAuthorized().equals(GeneralConstants.ZERO_VALUE_INTEGER)){
							 			lstSecurity.get(i).setAuthorizedName(SecurityAuthorizedType.NOT_AUTHORIZED.getValue());
							 		}else{
							 			lstSecurity.get(i).setAuthorizedName(SecurityAuthorizedType.AUTHORIZED.getValue());
							 		}
							 	}else{
							 		lstSecurity.get(i).setAuthorizedName(SecurityAuthorizedType.AUTHORIZED.getValue());
							 	}
						  }
					 }
				}
				securityDataModel=new SecurityDataModel(lstSecurity);
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ALT_NEED_ONE_FIELD));
				JSFUtilities.showSimpleValidationDialog();
			}		
			loadUserValidation();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Search file generation securities.
	 *
	 * @param event the event
	 */
	public void searchFileGenerationSecurities(ActionEvent event){
		JSFUtilities.hideGeneralDialogues();
		try {
			
			securityTOsearch.setIdIssuerCodePk( issuerHelperSearch.getIdIssuerPk() );
			securityTOsearch.setIdIssuanceCodePk( issuanceHelperSearch.getIdIssuanceCodePk() );
			securityTOsearch.setSecurityState(SecurityStateType.REGISTERED.getCode().toString());
			if(validateNotEmptyFields(securityTOsearch)){

				List<Security> lstSecurity=securityServiceFacade.findSecuritiesServiceFacade(securityTOsearch);
				securityDataModel=new SecurityDataModel(lstSecurity);
				
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ALT_NEED_ONE_FIELD));
				JSFUtilities.showSimpleValidationDialog();
			}
			loadUserValidationGeneration();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	/**
	 * Change filter field.
	 */
	public void changeFilterField(){
		try {
			String strSearchFiltersAux=buildCiteriaSearchConcat();
			
			if(!strSearchFiltersAux.equals(searchFiltersConcat)){
				securityDataModel=null;
				security=new Security();
			}
			
			searchFiltersConcat=buildCiteriaSearchConcat();
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Builds the citeria search concat.
	 *
	 * @return the string
	 */
	public String buildCiteriaSearchConcat(){
		StringBuilder strbAux=new StringBuilder();
		strbAux.append(issuerHelperSearch.getIdIssuerPk());
		strbAux.append(issuanceHelperSearch.getIdIssuanceCodePk());
		strbAux.append(securityTOsearch.getAlternativeCode());
		strbAux.append(securityTOsearch.getSecurityCurrency());
		strbAux.append(securityTOsearch.getSecurityState());
		strbAux.append(securityTOsearch.getSecurityClass());
		strbAux.append(securityTOsearch.getIssuanceDate());
		strbAux.append(securityTOsearch.getExpirationDate());
		return strbAux.toString();
	}
	
	/**
	 * Load user validation.
	 */
	private void loadUserValidation() {
		// TODO Auto-generated method stub
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		if(userPrivilege.getUserAcctions().isModify()){
			privilegeComponent.setBtnModifyView(Boolean.TRUE);
		}
		if(userPrivilege.getUserAcctions().isBlock()){
			privilegeComponent.setBtnBlockView(Boolean.TRUE);
		}
		if(userPrivilege.getUserAcctions().isUnblock()){
			privilegeComponent.setBtnUnblockView(Boolean.TRUE);
		}
		this.userPrivilege.setPrivilegeComponent(privilegeComponent);
	}
	
	/**
	 * Load User Generation File.
	 */
	private void loadUserValidationGeneration() {
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		if(userPrivilege.getUserAcctions().isGenerate()){
			privilegeComponent.setBtnGenerateView(Boolean.TRUE);
		}
		this.userPrivilege.setPrivilegeComponent(privilegeComponent);
	}
	
	public Boolean validateCountryIsin() {
		try {
			if(!securityServiceFacade.validateCountryInIsin( security )){
				security.setIdSecurityCodeOnly(null);
				security.setIdSecurityCodePk(null);
				
				JSFUtilities.showMessageOnDialog(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES,PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
				GeneralConstants.PROPERTY_FILE_EXCEPTIONS, ErrorServiceType.SECURITY_COUNTRY_NOT_MATCH_ENCODE.getMessage(), null);
				JSFUtilities.resetComponent(SecurityMgmtType.TXT_SECURITY_CODE.getValue());
				JSFUtilities.showSimpleValidationDialog();
				
				return Boolean.FALSE;
			}
			
			return Boolean.TRUE;
		} catch (ServiceException e) {
			e.printStackTrace();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
				     , "Error al intentar validar el país de la emisión y el codigo de valor");
			JSFUtilities.showSimpleValidationDialog();
			return Boolean.FALSE;
		}
	}
	
	/**
	 * Issuance select handler.
	 */
	public void issuanceSelectHandler(){
		try {
			Issuance issuance=null;
			/*deshabilitamos el campo agente pagador*/
			disableAgent = true;
			
			if(isViewOperationRegister()){
				
				if(issuanceHelperMgmt!=null && 
						Validations.validateIsNotNullAndNotEmpty(issuanceHelperMgmt.getIdIssuanceCodePk())){
					IssuanceTO issuanceTOmgmt=new IssuanceTO();
					issuanceTOmgmt.setIdIssuanceCodePk( issuanceHelperMgmt.getIdIssuanceCodePk() );	
					issuanceTOmgmt.setIssuanceState(SecurityStateType.REGISTERED.getCode());				
					issuance=issuanceSecuritiesServiceFacade.findIssuanceServiceFacade( issuanceTOmgmt );
					
					
					IssuerTO issuerTO=new IssuerTO();
					issuerTO.setIdIssuerPk( issuance.getIssuer().getIdIssuerPk() );
					issuance.setIssuer( issuerServiceFacade.findIssuerServiceFacade(issuerTO));
					/*Buscamos la lista de agente pagadores de acuerdo a la emision*/
					if(issuance!=null&&(!issuance.getSecurityClass().equals(SecurityClassType.DPF.getCode())||issuance.getSecurityClass().equals(SecurityClassType.DPA.getCode()))){
						listPaymentAgents = new ArrayList<>();
						listPaymentAgents = parameterServiceBean.getListPaymentAgentContacsByEcomicActivity(null);
						disableAgent = false;
					}else
						disableAgent = true;
				}
				if(issuance==null){
					JSFUtilities.addContextMessage("frmSecurityMgmt:tviewGeneral:txtIssuance", 
							FacesMessage.SEVERITY_ERROR, PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_ERROR_NOT_FOUND), 
							PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_ERROR_NOT_FOUND));
					
					
					security.setIssuance(new Issuance());
					security.getIssuance().setGeographicLocation( new GeographicLocation() );
					/*Limpiamos la lista de agentes pagadores*/
					listPaymentAgents = null;
					
//					JSFUtilities.showMessageOnDialog(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES,PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
//	 						 GeneralConstants.PROPERTY_FILE_MESSAGES, PropertiesConstants.ISSUANCE_ERROR_NOT_FOUND, null);
//					JSFUtilities.executeJavascriptFunction("cnfwMsgCustomValidationSec.show()");
					return;
				}
				if(!issuance.getStateIssuance().equals( IssuanceStateType.AUTHORIZED.getCode() )){
					JSFUtilities.addContextMessage("frmSecurityMgmt:tviewGeneral:txtIssuance", 
							FacesMessage.SEVERITY_ERROR, PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_ERROR_REGISTRY_BLOCK_ISSUANCE), 
							PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_ERROR_NOT_FOUND));	
					security.setIssuance(new Issuance());
					security.getIssuance().setGeographicLocation( new GeographicLocation() );
					return;
				}
				if(security.isPrepaid()){
					if(security.isBbxSecurityClass() || security.isBtxSecurityClass()){	
						JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
								 PropertiesConstants.SECURITY_ERROR_INCORRECT_CLASS_PREPAID,null);
						JSFUtilities.showSimpleValidationDialog();
						
						security.setIssuance(new Issuance());
						security.getIssuance().setGeographicLocation( new GeographicLocation() );
						return;
					}
					if(issuerHelperMgmt.getIdIssuerPk().equals(idIssuerBC) || issuerHelperMgmt.getIdIssuerPk().equals(idIssuerTGN)){
						JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
								 PropertiesConstants.SECURITY_ERROR_INCORRECT_ISSUER_PREPAID,null);
						JSFUtilities.showSimpleValidationDialog();
						
						issuerHelperMgmt=new Issuer();				
						security.setIssuer(null);
						
						security.setIssuance(new Issuance());
						security.getIssuance().setGeographicLocation( new GeographicLocation() );
						return;
					}
				}
				if(issuance.isDpfSecurityClass() || issuance.isDpaSecurityClass()){					
					SecurityTO secTO=new SecurityTO();
					secTO.setIdIssuanceCodePk( issuance.getIdIssuanceCodePk() );
					secTO.setStateSecurity( SecurityStateType.REGISTERED.getCode() );
					Integer securityQuantity=securityServiceFacade.countSecurityByIssuance( secTO );
					
					if(securityQuantity.compareTo( GeneralConstants.ZERO_VALUE_INTEGER )>0){
						JSFUtilities.addContextMessage("frmSecurityMgmt:tviewGeneral:txtIssuance", 
								FacesMessage.SEVERITY_ERROR, PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_ERROR_REGISTRY_BLOCK_ISSUANCE), 
								PropertiesUtilities.getMessage(PropertiesConstants.ISSUANCE_ERROR_NOT_FOUND));
						
						JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
														 PropertiesConstants.SECURITY_ERROR_EXIST_PDF_OR_DPA,null);
						JSFUtilities.showSimpleValidationDialog();
						
						security.setIssuance(new Issuance());
						security.getIssuance().setGeographicLocation( new GeographicLocation() );
						return;		
					}
				}
//				Issuer previouslyIssuer=new Issuer();
//				previouslyIssuer=security.getIssuer();
				
				String cfiCode=security.getCfiCode();
				String idSecurityCodeOnly=security.getIdSecurityCodeOnly();
				
				JSFUtilities.resetComponent("frmSecurityMgmt:tviewGeneral:tabPerformance");
				JSFUtilities.resetComponent("frmSecurityMgmt:tviewGeneral:tabNegotiationDepOthers");
				JSFUtilities.resetComponent("frmSecurityMgmt:tviewGeneral:tabAmounts");
				
				JSFUtilities.resetComponent("frmSecurityMgmt:tviewGeneral:fldSecurityData");
				JSFUtilities.resetComponent("frmSecurityMgmt:tviewGeneral:fldCouponInfo");
				
				Integer originalEncoderAgent = security.getEncoderAgent();
				String originalIdSecurityCodeOnly = security.getIdSecurityCodeOnly();
				
				security = new Security();	
				
				security.setEncoderAgent(originalEncoderAgent);
				security.setIdSecurityCodeOnly(originalIdSecurityCodeOnly);

				security.setIssuer(issuerHelperMgmt);
				security.setCfiCode(cfiCode);
				security.setIdSecurityCodeOnly(idSecurityCodeOnly);				
				setIssuanceSelected(issuance);
				security.setIssuance( issuance );
				security.setIssuer(issuance.getIssuer());
				setIssuerHelperMgmt(issuance.getIssuer() );
				
				if(issuance.isDpfSecurityClass() || issuance.isDpaSecurityClass()){
					security.setInitialNominalValue(issuance.getIssuanceAmount());
					
					insertInitialNominalValueHandler();
					
					if(!issuance.getIssuanceType().equals(IssuanceType.DEMATERIALIZED.getCode())){
						security.setShareCapital(issuance.getIssuanceAmount());
						security.setCirculationAmount( issuance.getIssuanceAmount());
						
						security.setShareBalance( security.getShareCapital().divide(security.getInitialNominalValue()));
						security.setCirculationBalance( security.getCirculationAmount().divide(security.getInitialNominalValue()));
						
						if(isViewOperationRegister()){
							security.setPhysicalBalance(BigDecimal.ZERO);
							security.setDesmaterializedBalance(BigDecimal.ZERO);	
							
							if(security.isDesmaterializedIssuanceForm()){
								security.setDesmaterializedBalance(security.getShareBalance());
							}else{
								security.setPhysicalBalance(security.getCirculationBalance());
							}
						}else if(isViewOperationModify()){
							if(!security.isDesmaterializedIssuanceForm()){
								security.setPhysicalBalance(security.getCirculationBalance().subtract(security.getDesmaterializedBalance()));
							}
						}
					}
				}

				loadDtbNegotiationModality(security.getInstrumentType());
				
				if(Validations.validateIsNotNullAndNotEmpty(security.getIdSecurityCodeOnly())){
					ParameterTable pTable=(ParameterTable)super.getParametersTableMap().get(security.getSecurityClass());
					
					Security securityAux=new Security();
					String secClass=pTable.getText1();
					
					StringBuilder code=new StringBuilder();
					//code.append(secClass);
					//code.append("-");
					code.append(security.getIdSecurityCodeOnly());
					securityAux.setIdSecurityCodePk(code.toString());
					
					if(securityServiceFacade.validateSecurityCodeServiceFacade( securityAux )){
						security.setIdSecurityCodeOnly(null);
						security.setIdSecurityCodePk(null);
						
						JSFUtilities.showMessageOnDialog(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES,PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
						GeneralConstants.PROPERTY_FILE_EXCEPTIONS, ErrorServiceType.SECURITY_CODE_REPEATED.getMessage(), null);
						JSFUtilities.resetComponent(SecurityMgmtType.TXT_SECURITY_CODE.getValue());
						JSFUtilities.showSimpleValidationDialog();
					} else {
						this.validateCountryIsin();
					}
					
				}
				valuatorHistoryDetailBean.setSecurity( security );
				security.setIssuanceDate(issuance.getIssuanceDate());
				if(issuance.getExpirationDate()!=null){
					security.setExpirationDate(issuance.getExpirationDate());
					if(security.isFixedInstrumentTypeAcc()){
						selectExpiredDateHandler();
					}
				}
				if(security.isVariableInstrumentTypeAcc()) 
					security.setIndEarlyRedemption(BooleanType.NO.getCode());
				if(issuance.isFixedIncomeInstrumentType() && !issuance.isAccRfSecurityClass()){
					security.setInterestType(InterestType.FIXED.getCode());
					changeCboInterestTypeHandler();
				}		
				if(security.getSecurityClass().equals(SecurityClassType.VTD.getCode())){
					security.setIndSecuritization(BooleanType.YES.getCode());
				}
				if(security.isCeroInterestBySecuriyClass()){
					security.setInterestType( InterestType.CERO.getCode() );
					changeCboInterestTypeHandler();
				}
				
				if(SecurityClassType.BSU.getCode().equals( issuance.getSecurityClass() )){
					security.setIndConvertibleStock(BooleanType.YES.getCode()); 
					security.setIndSubordinated(BooleanType.YES.getCode()); 
				}
				
//				if(issuance.isDpfSecurityClass())
//					security.setIndTaxExempt(BooleanType.YES.getCode());
//				else
					security.setIndTaxExempt(BooleanType.YES.getCode());
			}
		} catch (Exception e) {
			excepcion.fire(  new ExceptionToCatchEvent(e) );
		}
	}
	
	
	
	/**
	 * Checks if is disabled key securities description.
	 *
	 * @return the boolean
	 */
	public Boolean isDisabledKeySecuritiesDescription(){
//		if(SecurityClassType.BBS.getCode().equals( security.getSecurityClass() )){
//			return true;
//		}
//		if(SecurityClassType.BBX.getCode().equals( security.getSecurityClass() )){
//			return true;
//		}
//		if(SecurityClassType.BCB.getCode().equals( security.getSecurityClass() )){
//			return true;
//		}
//		if(SecurityClassType.BTS.getCode().equals( security.getSecurityClass() )){
//			return true;
//		}
//		if(SecurityClassType.BTX.getCode().equals( security.getSecurityClass() )){
//			return true;
//		}
//		if(SecurityClassType.CDS.getCode().equals( security.getSecurityClass() )){
//			return true;
//		}
//		if(SecurityClassType.DPA.getCode().equals( security.getSecurityClass() )){
//			return true;
//		}
//		if(SecurityClassType.DPF.getCode().equals( security.getSecurityClass() )){
//			return true;
//		}
//		if(SecurityClassType.LBS.getCode().equals( security.getSecurityClass() )){
//			return true;
//		}
//		if(SecurityClassType.LTS.getCode().equals( security.getSecurityClass() )){
//			return true;
//		}
		return false;
	}
	
	/**
	 * Checks if is disabled valuator class.
	 *
	 * @return the boolean
	 */
	public Boolean isDisabledValuatorClass(){
		
		if(SecurityClassType.ANR.getCode().equals( security.getSecurityClass() )){
			return true;
		}
		if(SecurityClassType.CSP.getCode().equals( security.getSecurityClass() )){
			return true;
		}
		if(SecurityClassType.CDI.getCode().equals( security.getSecurityClass() )){
			return true;
		}
		if(SecurityClassType.CNC.getCode().equals( security.getSecurityClass() )){
			return true;
		}
		if(SecurityClassType.NCF.getCode().equals( security.getSecurityClass() )){
			return true;
		}
		if(SecurityClassType.NCM.getCode().equals( security.getSecurityClass() )){
			return true;
		}
		
		return false;
	}
	
	/**
	 * Insert initial nominal value handler.
	 */
	public void insertInitialNominalValueHandler(){
		try {
			
			security.setCurrentNominalValue( security.getInitialNominalValue() );
			security.setMinimumInvesment(security.getInitialNominalValue());
			security.setMaximumInvesment(BigDecimal.ZERO);
			
			security.setShareCapital(BigDecimal.valueOf(0));
			security.setShareBalance(BigDecimal.valueOf(0));
			security.setCirculationAmount( BigDecimal.valueOf(0) );
			security.setCirculationBalance(BigDecimal.valueOf(0));
			security.setDesmaterializedBalance(BigDecimal.valueOf(0));
			security.setPhysicalBalance(BigDecimal.valueOf(0));

			JSFUtilities.resetComponent("frmSecurityMgmt:tviewGeneral:txtMiniumInversion");
			JSFUtilities.resetComponent("frmSecurityMgmt:tviewGeneral:txtMaximumInversion");
			JSFUtilities.resetComponent("frmSecurityMgmt:tviewGeneral:fldDataAmounts");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Insert interest rate.
	 */
	public void insertInterestRate(){
		try {
			security.setMinimumRate(null);
			security.setMaximumRate(null);

			JSFUtilities.resetComponent(SecurityMgmtType.TXT_MINIUM_RATE.getValue());
			JSFUtilities.resetComponent(SecurityMgmtType.TXT_MAXIUM_RATE.getValue());
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Validate multiplo ini nominal value.
	 *
	 * @param context the context
	 * @param component the component
	 * @param value the value
	 * @throws ValidatorException the validator exception
	 */
	public void validateMultiploIniNominalValue(FacesContext context, UIComponent component, Object value) throws ValidatorException {	
		if(value!=null && security.getInitialNominalValue() != null){
			InputText input=(InputText)component;
			String propertyBean=(String)component.getAttributes().get("propertyBean");
			
			BigDecimal currentValue=(BigDecimal)value;
			Object[] parameters = {input.getLabel(),PropertiesUtilities.getMessage("security.lbl.initial.nominal.value")};
			if(currentValue.compareTo(BigDecimal.ZERO)==0){
				return;
			}
			if(currentValue.compareTo(security.getInitialNominalValue())==1){
				if(currentValue.remainder( security.getInitialNominalValue() ).compareTo(BigDecimal.ZERO)!=0){
					String strMsg =  PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MULTIPLO, parameters);
					FacesMessage msg = new FacesMessage(strMsg);
					msg.setSeverity(FacesMessage.SEVERITY_ERROR);
					  
					if(propertyBean.equals("minimumInvesment")){
						security.setMinimumInvesment(null);
					}else if(propertyBean.equals("maximumInvesment")){
						security.setMaximumInvesment(null);
					}
					EditableValueHolder editableValueHolder = (EditableValueHolder) input;
					editableValueHolder.setSubmittedValue(null);
					editableValueHolder.setValue(null);
					editableValueHolder.setLocalValueSet(false);
					editableValueHolder.setValid(true);
					editableValueHolder.resetValue();
					throw new ValidatorException(msg);
				}
			}else if(currentValue.compareTo(security.getInitialNominalValue())==-1){
				if(security.getInitialNominalValue().remainder( currentValue ).compareTo(BigDecimal.ZERO)!=0){
					String strMsg =  PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MULTIPLO, parameters);
					FacesMessage msg = new FacesMessage(strMsg);
					msg.setSeverity(FacesMessage.SEVERITY_ERROR);
					
					if(propertyBean.equals("minimumInvesment")){
						security.setMinimumInvesment(null);
					}else if(propertyBean.equals("maximumInvesment")){
						security.setMaximumInvesment(null);
					}
				
					/*
					ConfirmDialog dlgReqValidation=(ConfirmDialog)JSFUtilities.findViewComponent("dlgMsgRequiredValidation");
					if(!dlgReqValidation.isRendered()){
						JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
								 PropertiesConstants.ERROR_MULTIPLO, parameters);
						JSFUtilities.executeJavascriptFunction("cnfwMsgCustomValidationSec.show()");
					}*/
					
					throw new ValidatorException(msg);
				}				
			}

		}
	}
	
	/**
	 * Insert share capital.
	 */
	public void insertShareCapital(){
		try {
			if(security.getShareCapital()==null){
				security.setShareBalance(BigDecimal.ZERO);
			}else 
			if(security.getInitialNominalValue()!=null){
				security.setShareBalance( security.getShareCapital().divide(security.getInitialNominalValue()) );
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Insert valores fisicos
	 */
	public void insertPhisicalBalance() {
		try {
			if (security.getDesmaterializedBalance().add(security.getPhysicalBalance().add(security.getPhysicalDepositBalance())).multiply(security.getInitialNominalValue()).compareTo(security.getCirculationAmount()) == 1) {
				security.setPhysicalBalance(BigDecimal.ZERO);
				JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR,null,PropertiesConstants.SECURITY_ERROR_PHYSICAL_QUANTITY,null);
				JSFUtilities.showSimpleValidationDialog();	
			} 
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Insert circulation amount.
	 */
	public void insertCirculationAmount(){
		try {
			if(security.getInitialNominalValue()!=null && security.getCirculationAmount()!=null){
				security.setCirculationBalance( security.getCirculationAmount().divide( security.getInitialNominalValue() ) );
				if(security.getDesmaterializedBalance()!=null){
					security.setPhysicalBalance(security.getCirculationBalance().subtract(security.getDesmaterializedBalance()));
				}else{
					security.setPhysicalBalance(security.getCirculationBalance());
					security.setDesmaterializedBalance(BigDecimal.ZERO);
				}
			}else{
				security.setCirculationBalance( BigDecimal.ZERO );
				security.setPhysicalBalance(BigDecimal.ZERO);				
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change security serial.
	 */
	public void changeSecuritySerial(){
		try {
			if(securityServiceFacade.validateSecuritySerieServiceFacade(security)){
				JSFUtilities
				.addContextMessage(
						"frmSecurityMgmt:tviewGeneral:txtSecuritySerial",
						FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities
								.getMessage(PropertiesConstants.SECURITY_ERROR_SERIAL_IN_USE),
						PropertiesUtilities
								.getMessage(PropertiesConstants.SECURITY_ERROR_SERIAL_IN_USE));
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.SECURITY_ERROR_SERIAL_IN_USE, 
						null);
				JSFUtilities.showSimpleValidationAltDialog();	
				security.setSecuritySerial(null);
				return;
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Metodo para validar que el valor sea de emision del BCB, excepto BBX o BTX issue 578
	 * @return
	 */
	public boolean isSecurityClassBCB() {
		if(SecurityClassType.BTS.getCode().equals(security.getSecurityClass())
				|| SecurityClassType.LTS.getCode().equals(security.getSecurityClass())
				|| SecurityClassType.BBS.getCode().equals(security.getSecurityClass())
				|| SecurityClassType.LBS.getCode().equals(security.getSecurityClass())
				|| SecurityClassType.BRS.getCode().equals(security.getSecurityClass())
				|| SecurityClassType.LRS.getCode().equals(security.getSecurityClass())
				){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Validacion de la cantidad ingresada en cantidad BCB issue 578
	 * @return
	 * @throws ServiceException 
	 */
	public void validateQuantityBcb(Integer cause) throws ServiceException {
		//UNO VALIDA EMISION POR VALOR
		//DOS VALIDA CANTIDAD COLOCADA BCB
		if(cause.equals(GeneralConstants.ONE_VALUE_INTEGER)){
			
			if(Validations.validateIsNotNullAndNotEmpty(security.getIssuanceQuantitySecBcb())){
				BigDecimal quantityIssuance = securityServiceFacade.getIssuanceAmount(security.getIssuance().getIdIssuanceCodePk());
				if(security.getIssuanceQuantitySecBcb().compareTo(quantityIssuance) > 0){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage("security.lbl.issuance.sec.bcb.maximum"));
							JSFUtilities.showSimpleValidationDialog();
					security.setIssuanceQuantitySecBcb(null);
					JSFUtilities.resetComponent("frmSecurityMgmt:tviewGeneral:txtIssuanceSecBcb");
					return;
				}
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(security.getQuantityBcb())
					&& Validations.validateIsNotNullAndNotEmpty(security.getIssuanceQuantitySecBcb())){
				
				BigDecimal quantity = security.getQuantityBcb().multiply(new BigDecimal(1000));
				if(quantity.compareTo(security.getIssuanceQuantitySecBcb()) > 0){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage("security.lbl.quantity.bcb.maximum"));
							JSFUtilities.showSimpleValidationDialog();
					security.setIssuanceQuantitySecBcb(null);
					JSFUtilities.resetComponent("frmSecurityMgmt:tviewGeneral:txtIssuanceSecBcb");
					return;
				}
			}
			
		}else{
			if(Validations.validateIsNotNullAndNotEmpty(security.getQuantityBcb())
					&& Validations.validateIsNotNullAndNotEmpty(security.getIssuanceQuantitySecBcb())){
				
				BigDecimal quantity = security.getQuantityBcb().multiply(new BigDecimal(1000));
				
				if(quantity.compareTo(security.getIssuanceQuantitySecBcb()) > 0){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage("security.lbl.quantity.bcb.maximum"));
							JSFUtilities.showSimpleValidationDialog();
					security.setQuantityBcb(null);
					JSFUtilities.resetComponent("frmSecurityMgmt:tviewGeneral:txtQuantityBcb");
				}
			}
		}
	}

	/**
	 * Disabled the serial value field.
	 *
	 * @return true, if is disabled security serial
	 */
	public boolean isDisabledSecuritySerial(){

		if(SecurityClassType.BBS.getCode().equals( security.getSecurityClass() )){
			return true;
		}
		if(SecurityClassType.BBX.getCode().equals( security.getSecurityClass() )){
			return true;
		}
		if(SecurityClassType.BCB.getCode().equals( security.getSecurityClass() )){
			return true;
		}
		if(SecurityClassType.BTS.getCode().equals( security.getSecurityClass() )){
			return true;
		}
		if(SecurityClassType.BTX.getCode().equals( security.getSecurityClass() )){
			return true;
		}
		if(SecurityClassType.CDB.getCode().equals( security.getSecurityClass() )){
			return true;
		}
		if(SecurityClassType.CDD.getCode().equals( security.getSecurityClass() )){
			return true;
		}
		if(SecurityClassType.CDI.getCode().equals( security.getSecurityClass() )){
			return true;
		}
		if(SecurityClassType.CDS.getCode().equals( security.getSecurityClass() )){
			return true;
		}
		if(SecurityClassType.CNC.getCode().equals( security.getSecurityClass() )){
			return true;
		}
		if(SecurityClassType.CUP.getCode().equals( security.getSecurityClass() )){
			return true;
		}
		if(SecurityClassType.DPA.getCode().equals( security.getSecurityClass() )){
			return true;
		}
		if(SecurityClassType.DPF.getCode().equals( security.getSecurityClass() )){
			return true;
		}
		if(SecurityClassType.LBS.getCode().equals( security.getSecurityClass() )){
			return true;
		}
		if(SecurityClassType.LCB.getCode().equals( security.getSecurityClass() )){
			return true;
		}
		if(SecurityClassType.LTC.getCode().equals( security.getSecurityClass() )){
			return true;
		}
		if(SecurityClassType.LTS.getCode().equals( security.getSecurityClass() )){
			return true;
		}
		if(SecurityClassType.NCF.getCode().equals( security.getSecurityClass() )){
			return true;
		}
		if(SecurityClassType.NCM.getCode().equals( security.getSecurityClass() )){
			return true;
		}
		if(SecurityClassType.PGB.getCode().equals( security.getSecurityClass() )){
			return true;
		}
		if(SecurityClassType.PGR.getCode().equals( security.getSecurityClass() )){
			return true;
		}
		if(SecurityClassType.PGS.getCode().equals( security.getSecurityClass() )){
			return true;
		}

		
		
		return false;
	}
	
	
	/**
	 * Before save security handler.
	 */
	public void beforeSaveSecurityHandler(){
		try {
			boolean errorNegIntMech=true;
			boolean errorProgIntCoupon=false;
			boolean errorProgAmortCoupon=false;
			
			JSFUtilities.scrollToComponent("opnlTitle");
						
			if(isViewOperationModify() && (security.isFixedInstrumentTypeAcc() && !isReGenerated)){
				
				if(!CommonsUtilities.convertDatetoString(security.getIssuanceDate()).equals( 
						CommonsUtilities.convertDatetoString(securityInitialState.getIssuanceDate()))){
					
						JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
														 PropertiesConstants.SECURITY_ERROR_PAYMENT_SCHEDULE_AGAIN, null);
						JSFUtilities.showSimpleValidationDialog();	
						return;
						
					}
				
				if(CommonsUtilities.convertDatetoString(security.getIssuanceDate()).equals( 
					CommonsUtilities.convertDatetoString(securityInitialState.getIssuanceDate()))){
					
					if(!isFirstReGenerated && security.getSecurityMonthsTerm().compareTo( securityInitialState.getSecurityMonthsTerm() )==0){
						
						JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
								 PropertiesConstants.SECURITY_ERROR_PAYMENT_SCHEDULE_AGAIN, null);
						JSFUtilities.showSimpleValidationDialog();	
						return;
					}
					if(security.getSecurityMonthsTerm().compareTo( securityInitialState.getSecurityMonthsTerm() )!=0){
						JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
								 PropertiesConstants.SECURITY_ERROR_PAYMENT_SCHEDULE_AGAIN, null);
						JSFUtilities.showSimpleValidationDialog();	
						return;
					}

				}
			}
			
			if(isViewOperationModify()) {
				Integer correct = 0;
				List<SecuritySerialRange> lstCompareSecuritySerialRange = lstSecuritySerialRange.stream()
				 .filter(x->x.getStateRange()==1)
				 .collect(Collectors.toList());
				if(Validations.validateListIsNotNullAndNotEmpty(lstResult)
					&& Validations.validateListIsNotNullAndNotEmpty(lstCompareSecuritySerialRange)) {
					for(int i=0;i<lstResult.size();i++){
						for(int j=0;j<lstCompareSecuritySerialRange.size();j++){
							if((lstResult.get(i).getSerialCode().equals(lstCompareSecuritySerialRange.get(j).getSerialRange()))) {
								if((lstCompareSecuritySerialRange.get(j).getNumberShareInitial())<=lstResult.get(i).getCertificateFrom()) {
									if((lstCompareSecuritySerialRange.get(j).getNumberShareFinal())>=lstResult.get(i).getCertificateTo()) {
										correct=correct+1;
									}
								}
							}
						}
					}
					
					
					if(correct<lstResult.size()) {
						JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
								 PropertiesConstants.SECURITY_ERROR_EXIST, null);
						JSFUtilities.showSimpleValidationDialog();	
						return;
					}
				}
				if(Validations.validateListIsNotNullAndNotEmpty(lstResult)
					&& Validations.validateListIsNullOrEmpty(lstCompareSecuritySerialRange)) {
					JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
							 PropertiesConstants.SECURITY_ERROR_EXIST, null);
					JSFUtilities.showSimpleValidationDialog();	
					return;
				}
			}
			
			if(security.isFixedInstrumentTypeAcc()){
				security.getAmortizationPaymentSchedule().calculateTotalFactorAndAmount();
				if(security.getAmortizationPaymentSchedule().getTotalAmortizationFactor().compareTo( GeneralConstants.HUNDRED_VALUE_BIGDECIMAL )!=0 ){
					JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
							 PropertiesConstants.SECURITY_ERROR_PAYMENT_INCORRECT_TOT_FACTOR, null);
					JSFUtilities.showSimpleValidationDialog();	
					return;
				}
				if(security.getInitialNominalValue().compareTo( security.getAmortizationPaymentSchedule().getTotalAmortizationCoupon() )!=0){
					JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
							 PropertiesConstants.SECURITY_ERROR_PAYMENT_INCORRECT_TOT_COUPON, null);
					JSFUtilities.showSimpleValidationDialog();	
					return;
				}
				if(!security.isCeroInterestType()){
					StringBuilder strWrongsCoupons=new StringBuilder();
					
					if(security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().size() > 
					   security.getInterestPaymentSchedule().getProgramInterestCoupons().size()){
						JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
								 PropertiesConstants.PAYMENT_CRON_ERROR_MORE_PAY_THAT_COUP, null);
						JSFUtilities.showSimpleValidationDialog();	
						return;
					}
					
					for(ProgramInterestCoupon proInt : security.getInterestPaymentSchedule().getProgramInterestCoupons()){
						if(proInt.getPaymentDays().intValue()<=0){
							strWrongsCoupons.append("Nro. ").append(proInt.getCouponNumber()).append("<br/>");
						}
					}
					if(strWrongsCoupons.length()!=0){
						Object[] argObj=new Object[1];
						argObj[0]=strWrongsCoupons.toString();
						JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
								 PropertiesConstants.PAYMENT_CRON_ERROR_COUP_DAYS, argObj);
						JSFUtilities.showSimpleValidationDialog();	
						return;
					}
					for(ProgramAmortizationCoupon proAmo : security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons()){
						boolean found=false;
						for(ProgramInterestCoupon proInt : security.getInterestPaymentSchedule().getProgramInterestCoupons()){
							if(CommonsUtilities.isEqualDate(proAmo.getExpirationDate(), proInt.getExperitationDate())){
								found=true;
								break;
							}
						}
						if(!found){
							Object[] argObj=new Object[1];
							argObj[0]=proAmo.getCouponNumber();
							JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
									 PropertiesConstants.PAYMENT_CRON_ERROR_PAY_AMO_NOT_INT_COU_ASO, argObj);
							JSFUtilities.showSimpleValidationDialog();	
							return;
						}
					}
				}
				StringBuilder strWrongsCoupons=new StringBuilder();
				for(ProgramAmortizationCoupon proAmo : security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons()){
					int paymentDays=CommonsUtilities.getDaysBetween(proAmo.getBeginingDate(), proAmo.getExpirationDate());
					if(paymentDays<=0){
						strWrongsCoupons.append("Nro. ").append(proAmo.getCouponNumber()).append("<br/>");
					}
				}
				if(strWrongsCoupons.length()!=0){
					Object[] argObj=new Object[1];
					argObj[0]=strWrongsCoupons.toString();
					JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
							 PropertiesConstants.PAYMENT_CRON_ERROR_PAYM_DAYS, argObj);
					JSFUtilities.showSimpleValidationDialog();	
					return;
				}
			}
			
			if(blNotTrded){
				security.setNotTraded(BooleanType.YES.getCode());				
			} else {
				security.setNotTraded(BooleanType.NO.getCode());
			}
			
			/* Data Validate */
			if(security.isFixedInstrumentTypeAcc() && !security.isPhysicalIssuanceForm()){
				if(security.getSecurityMonthsTerm()==null){
					throw new ServiceException(ErrorServiceType.SECURITY_INCONSISTENT);
				}	
			}						
			
			for(NegotiationModality negotiationModality : lstDtbNegotiationModalities){
				for(NegotiationMechanism negotiationMechanism : negotiationModality.getNegotiationMechanisms()){
					if(negotiationMechanism.isSelected()){
						errorNegIntMech=false;
						break;
					}
				}
			}
			
			if(security.isDpfDpaSecurityClass() && BooleanType.YES.getCode().equals(security.getNotTraded())){
				errorNegIntMech = false;
			}
			
			if(errorNegIntMech){
				JSFUtilities.addContextMessage("frmSecurityMgmt:tviewGeneral:dtbNegotiationModalities", FacesMessage.SEVERITY_ERROR, 
						PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_ERROR_NEGOTIATION_MECHANISM_REQUIRED), 
						PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_ERROR_NEGOTIATION_MECHANISM_REQUIRED));
				return;
			}

			if(security.isFixedInstrumentTypeAcc()){				

					if(security.getInterestPaymentSchedule().getProgramInterestCoupons()==null ||
									security.getInterestPaymentSchedule().getProgramInterestCoupons().isEmpty()){
						errorProgIntCoupon=true;
						if(security.isCeroInterestType() && security.getInterestRate().compareTo(BigDecimal.ZERO)==0){
							errorProgIntCoupon=false;
						}
					}
					
					if(security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons()!=null && 
							security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().isEmpty()){
						errorProgAmortCoupon=true;
					}
					
					if(errorProgIntCoupon && errorProgAmortCoupon){
						JSFUtilities.addContextMessage("frmSecurityMgmt:tviewGeneral:cmbAuxCron", FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities.getMessage(PropertiesConstants.PAYMENT_CRON_ERROR_REQUIRED),
								PropertiesUtilities.getMessage(PropertiesConstants.PAYMENT_CRON_ERROR_REQUIRED));
						
						JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR,null, 
								PropertiesConstants.PAYMENT_CRON_ERROR_REQUIRED, null);
						JSFUtilities.showSimpleValidationDialog();
						return;
					}	
					
					if(isViewOperationRegister()){
						if(security.isFixedInstrumentTypeAcc() ){
							if(Validations.validateIsNotNull(securityInitialStateStr)){
								if(!securityInitialStateStr.equals(security.toString())){
									JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
											 PropertiesConstants.SECURITY_ERROR_PAYMENT_SCHE_GENERATE_AGAIN, null);
									JSFUtilities.showSimpleValidationDialog();	
									return;
								}
							}
						}
					}
			}
						
			if(isDisabledKeySecuritiesDescription()){
				//security.setDescription(security.getIdSecurityCodeOnly());
			}
			
			
			Object[] argObj=new Object[1];
			if(isViewOperationRegister()){
				argObj[0]=security.getMnemonic();
				showMessageOnDialog(
						PropertiesConstants.LBL_HEADER_ALERT_REGISTER,
						null,
						PropertiesConstants.SECURITY_CONFIRM_REGISTER,
						argObj);	
			}else if(isViewOperationModify()){
				argObj[0]=security.getIdSecurityCodeOnly();
				showMessageOnDialog(
						PropertiesConstants.LBL_HEADER_ALERT_MODIFY,
						null,
						PropertiesConstants.SECURITY_CONFIRM_MODIFY,
						argObj);	
			}
			JSFUtilities.executeJavascriptFunction("PF('cnfwSecurityMgmt').show()");
		} catch (ServiceException se) {
			if(se.getMessage()!=null){
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, null, se.getErrorService().getMessage(), null);
				JSFUtilities.showSimpleValidationDialog();
			}else{
				excepcion.fire(new ExceptionToCatchEvent(se));
			}
			
		}
	}
	
	
	
	/**
	 * Save security handler.
	 */
	@LoggerAuditWeb
	public void saveSecurityHandler(){
		try {
			Object[] argObj=new Object[1];
			JSFUtilities.executeJavascriptFunction("PF('cnfwSecurityMgmt').hide()");

			if(isDisabledValuatorClass()){
				security.setValuatorProcessClass(null);
			}
			
			if(!security.isPrepaid()){
				security.setBolCfiCode(isNotGeneratedCfiCode());
			}
						
			if(isViewOperationRegister()){
				
				//issue 891 MC de valor 
				security.setIndAuthorized(GeneralConstants.ZERO_VALUE_INTEGER);
				Integer indAutomatic = GeneralConstants.ZERO_VALUE_INTEGER;
				
				securityServiceFacade.registrySecurityServiceFacade(security, lstDtbInternationalDepository, lstDtbNegotiationModalities,indAutomatic, null);
				argObj[0]=security.getIdSecurityCodeOnly();
				showMessageOnDialog(
						PropertiesConstants.LBL_HEADER_ALERT_SUCCESS,
						null,
						PropertiesConstants.SECURITY_MSG_REGISTER_SUCCESS,
						argObj);	
				
			}else if(isViewOperationModify()){
				argObj[0]=security.getIdSecurityCodeOnly();
				securityServiceFacade.updateSecurityServiceFacade(security, lstDtbInternationalDepository, lstDtbNegotiationModalities,isReGenerated);
				showMessageOnDialog(
						PropertiesConstants.LBL_HEADER_ALERT_SUCCESS,
						null,
						PropertiesConstants.SECURITY_MSG_MODIFY_SUCCESS,
						argObj);
			}

			/*Cleant Issuer and Issuance helper's -- start*/
			if(!userInfo.getUserAccountSession().isIssuerInstitucion() && !userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
				issuerHelperSearch=new Issuer();
			}
			issuanceHelperSearch=new Issuance();
			/*Cleant Issuer and Issuance helper's -- end*/
			
			securityTOsearch=new SecurityTO();
			securityDataModel=null;
			setViewOperationType(ViewOperationsType.CONSULT.getCode());
			//JSFUtilities.executeJavascriptFunction("cnfwEndTransactionSec.show()");
			JSFUtilities.showSimpleEndTransactionDialog("searchSecurityMgmt");
			
		} catch (ServiceException se) {
			if(se.getErrorService().equals(ErrorServiceType.SECURITY_SHORT_CFI_CODE_NO_GENERATE ))
				log.error("Error, no se pudo generar el Codigo CFI Corto.");
			if(se.getMessage()!=null){
				JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, null, se.getErrorService().getMessage(), null);
				JSFUtilities.showSimpleValidationDialog();
				if(se.getErrorService().equals( ErrorServiceType.SECURITY_CFI_CODE_NO_GENERATE )){
					security.setCfiCode(null);
					JSFUtilities.putViewMap("enableCfiGenerate", true);
				}
			}
		}
	}
	
	
	/**
	 * Payment schedule file handler.
	 *
	 * @param event the event
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws InvalidFormatException the invalid format exception
	 * @throws InstantiationException the instantiation exception
	 * @throws IllegalAccessException the illegal access exception
	 */
	public void paymentScheduleFileHandler(FileUploadEvent event) throws IOException, InvalidFormatException, InstantiationException, IllegalAccessException{
		try {
			String fDisplayName=event.getFile().getFileName();
			
			 securityUploaded=securitiesUtils.castPaymentScheduleFile(event.getFile().getInputstream());
			 
			 if(securityUploaded == null) {
				 return;
			 }
			 
			 String strDiffSecurity=securitiesUtils.isDifferentSecurityFile(security, securityUploaded);
			 
			 lstProgramInterestDiff=new ArrayList<Map<String,Object>>();
			 lstProgramAmortizationDiff=new ArrayList<Map<String,Object>>();
			 
			 if(!strDiffSecurity.isEmpty()){
				 JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),strDiffSecurity);
				 JSFUtilities.showSimpleValidationDialog();
				 return;
			 }
			 if(security.isIndistinctPeriodicity()){
				 generateProgramInterestScheduleFile(securityUploaded.getInterestPaymentSchedule().getProgramInterestCoupons());
				 
				 security.setNumberCoupons( securityUploaded.getInterestPaymentSchedule().getProgramInterestCoupons().size() );
				 security.setCouponFirstExpirationDate( 
						 securityUploaded.getInterestPaymentSchedule().getProgramInterestCoupons().get(0).getExperitationDate() );
			 } else {
				 lstProgramInterestDiff=securitiesUtils.isDifferentProramIntFile(security.getInterestPaymentSchedule(), 
						 securityUploaded.getInterestPaymentSchedule());
			 }
			 if(security.isIndistinctAmortizationPeriodicity()){
				 generateProgramAmortizationCouponFile(securityUploaded.getAmortizationPaymentSchedule().getProgramAmortizationCoupons());
				 
				 setPaymentNumberAmortization( securityUploaded.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().size() );
				 security.setPaymentFirstExpirationDate( 
						 securityUploaded.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().get(0).getExpirationDate() ); 	 
			 }else{
				 lstProgramAmortizationDiff=securitiesUtils.isDifferentProramAmoFile(security.getAmortizationPaymentSchedule(), 
						 securityUploaded.getAmortizationPaymentSchedule());	
			 }
			 if(lstProgramInterestDiff.isEmpty() && lstProgramAmortizationDiff.isEmpty()){
				 String msg=PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_ERROR_COMPARATIVE_EQUALS);
				 
				 JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),msg);
				 JSFUtilities.showSimpleValidationDialog();
			 } else {
				 JSFUtilities.executeJavascriptFunction("PF('dlgwPaymenScheduleDiff').show();");
			 }
		}catch (ServiceException se){
			if(se.getMessage()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, null, se.getErrorService().getMessage(), null);
				JSFUtilities.showSimpleValidationDialog();
			}else{
				excepcion.fire(new ExceptionToCatchEvent(se));
			}
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Accept differences payment schedule.
	 */
	public void acceptDifferencesPaymentSchedule(){
		try {
			if(!securityUploaded.getInterestPaymentSchedule().getProgramInterestCoupons().isEmpty()){
				generateProgramInterestScheduleFile(securityUploaded.getInterestPaymentSchedule().getProgramInterestCoupons());
			}
			if(!securityUploaded.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().isEmpty()){
				generateProgramAmortizationCouponFile(securityUploaded.getAmortizationPaymentSchedule().getProgramAmortizationCoupons());
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Generate program interest schedule file.
	 *
	 * @param lstProgramInterestCouponFile the lst program interest coupon file
	 * @throws ServiceException the service exception
	 */
	public void generateProgramInterestScheduleFile(List<ProgramInterestCoupon> lstProgramInterestCouponFile) throws ServiceException{
		security.getInterestPaymentSchedule().setSecurity(security);
		security.getInterestPaymentSchedule().setPeriodicity(security.getPeriodicity());
		security.getInterestPaymentSchedule().setPeriodicityDays( security.getPeriodicityDays() );
		security.getInterestPaymentSchedule().setExpirationDate(security.getExpirationDate());
		security.getInterestPaymentSchedule().setCalendarType( security.getCalendarType() );
		security.getInterestPaymentSchedule().setInterestPeriodicity( security.getInterestRatePeriodicity() );
		security.getInterestPaymentSchedule().setInterestType( security.getInterestType() );
		security.getInterestPaymentSchedule().setInterestFactor( security.getInterestRate() );
		security.getInterestPaymentSchedule().setRegistryDays(security.getStockRegistryDays());
		security.getInterestPaymentSchedule().setYield( security.getYield() );
		security.getInterestPaymentSchedule().setCashNominal(security.getCashNominal());
		security.getInterestPaymentSchedule().setSpread( security.getSpread() );
		security.getInterestPaymentSchedule().setScheduleState( PaymentScheduleStateType.REGTERED.getCode() );
		
		security.getInterestPaymentSchedule().setCalendarMonth( security.getCalendarMonth() );
		security.getInterestPaymentSchedule().setCalendarDays( security.getCalendarDays() );
		security.getInterestPaymentSchedule().setRateType( security.getRateType() );
		security.getInterestPaymentSchedule().setRateValue( security.getRateValue() );
		security.getInterestPaymentSchedule().setOperationType( security.getOperationType() );
		security.getInterestPaymentSchedule().setMinimumRate( security.getMinimumRate() );
		security.getInterestPaymentSchedule().setMaximumRate( security.getMaximumRate() );
		
		ProgramInterestCoupon programInterestCoupon=null;
		ProgramInterestCoupon programInterestCouponFile=null;
		security.getInterestPaymentSchedule().setProgramInterestCoupons( new ArrayList<ProgramInterestCoupon>() );
		
		Date previousExpirationDate=null;
		
		for(int i=1;i<=lstProgramInterestCouponFile.size();i++){
			
			programInterestCoupon=new ProgramInterestCoupon();
			programInterestCouponFile=lstProgramInterestCouponFile.get(i-1);
			
			programInterestCoupon.setCouponNumber(Integer.valueOf(i));
			programInterestCoupon.setCurrency( security.getCurrency() );
			//Begin date
			if(previousExpirationDate==null){
				programInterestCoupon.setBeginingDate(security.getIssuanceDate() );	
			}else{
				programInterestCoupon.setBeginingDate( previousExpirationDate );
			}
			//Expiration date
			programInterestCoupon.setExperitationDate( programInterestCouponFile.getExperitationDate() );
			previousExpirationDate=programInterestCoupon.getExperitationDate();
			
			//Payment Date
			programInterestCoupon.setPaymentDate( generalParameterFacade.workingDateCalculateServiceFacade(programInterestCoupon.getExperitationDate(), 
					   GeneralConstants.ONE_VALUE_INTEGER) );
			
			Integer paymentDays=null;
			if(security.isExtendedTerm()){
				paymentDays=CommonsUtilities.getDaysBetween(programInterestCoupon.getBeginingDate(),programInterestCoupon.getPaymentDate());
			}else{
				paymentDays=CommonsUtilities.getDaysBetween(programInterestCoupon.getBeginingDate(),programInterestCoupon.getExperitationDate());
			}
			programInterestCoupon.setPaymentDays(paymentDays);
			
			programInterestCoupon.setCorporativeDate( CommonsUtilities.currentDate() );
			
			//Registry Date
			programInterestCoupon.setRegistryDate(CommonsUtilities.addDaysToDate(programInterestCoupon.getExperitationDate(), security.getStockRegistryDays()*-1));
			programInterestCoupon.setRegistryDate(generalParameterFacade.workingDateCalculateServiceFacade(programInterestCoupon.getRegistryDate(), -1));
			//Cut Off Date
			programInterestCoupon.setCutoffDate( programInterestCoupon.getRegistryDate() );
			programInterestCoupon.setInterestPaymentCronogram( security.getInterestPaymentSchedule() );
			programInterestCoupon.setStateProgramInterest( ProgramScheduleStateType.PENDING.getCode() );			
			programInterestCoupon.setInterestRate(programInterestCouponFile.getInterestRate());
			programInterestCoupon.setInterestFactor( security.getInterestPaymentSchedule().getInterestFactor() );
			
			programInterestCoupon.setIndRounding(BooleanType.NO.getCode()); 
			programInterestCoupon.setPayedAmount(BigDecimal.ZERO);
			
			programInterestCoupon.setLastModifyApp( Integer.valueOf(1) );
			programInterestCoupon.setLastModifyDate( CommonsUtilities.currentDateTime() );
			programInterestCoupon.setLastModifyIp( userInfo.getUserAccountSession().getIpAddress()  );
			programInterestCoupon.setLastModifyUser(userInfo.getUserAccountSession().getUserName() );
			programInterestCoupon.setCouponAmount(programInterestCouponFile.getCouponAmount());
			
			security.getInterestPaymentSchedule().getProgramInterestCoupons().add( programInterestCoupon );			
		}
	}
	
	/**
	 * Generate program amortization coupon file.
	 *
	 * @param lstProgramAmortizationCoupon the lst program amortization coupon
	 * @throws ServiceException the service exception
	 */
	public void generateProgramAmortizationCouponFile(List<ProgramAmortizationCoupon> lstProgramAmortizationCoupon) throws ServiceException{
		security.getAmortizationPaymentSchedule().setSecurity(security);
		security.getAmortizationPaymentSchedule().setAmortizationType( security.getAmortizationType() );
		security.getAmortizationPaymentSchedule().setAmortizationFactor( security.getAmortizationFactor() );
		security.getAmortizationPaymentSchedule().setCalendarType( security.getCalendarType()); //cambiar por calendar type
		security.getAmortizationPaymentSchedule().setPeriodicity( security.getAmortizationPeriodicity() );
		security.getAmortizationPaymentSchedule().setPeriodicityDays( security.getAmortizationPeriodicityDays() );
		security.getAmortizationPaymentSchedule().setRegistryDays( security.getStockRegistryDays() );
		security.getAmortizationPaymentSchedule().setCorporativeDays( Integer.valueOf(0) );
		security.getAmortizationPaymentSchedule().setScheduleState( PaymentScheduleStateType.REGTERED.getCode() );
	
		security.getAmortizationPaymentSchedule().setPaymentModality( security.getCapitalPaymentModality() );
		security.getAmortizationPaymentSchedule().setPaymentFirstExpirationDate( security.getPaymentFirstExpirationDate() );

		security.getAmortizationPaymentSchedule().setProgramAmortizationCoupons( new ArrayList<ProgramAmortizationCoupon>() );
		
		Date previousExpirationDate=null;
		ProgramAmortizationCoupon programAmortizationCoupon=null;
		ProgramAmortizationCoupon programAmortizationCouponFile=null;
		
		for(int i=1;i<=lstProgramAmortizationCoupon.size();i++){
			programAmortizationCouponFile=lstProgramAmortizationCoupon.get(i-1);
			programAmortizationCoupon=new ProgramAmortizationCoupon();
			
			programAmortizationCoupon.setCouponNumber( i );		
			if(Validations.validateIsNullOrEmpty(previousExpirationDate)){
				programAmortizationCoupon.setBeginingDate(security.getIssuanceDate());
			}else{
				programAmortizationCoupon.setBeginingDate(previousExpirationDate);
			}
			programAmortizationCoupon.setExpirationDate(programAmortizationCouponFile.getExpirationDate());
			previousExpirationDate = programAmortizationCoupon.getExpirationDate();
			
			programAmortizationCoupon.setAmortizationFactor(programAmortizationCouponFile.getAmortizationFactor());
			programAmortizationCoupon.setPaymentDate( generalParameterFacade.workingDateCalculateServiceFacade(programAmortizationCouponFile.getExpirationDate(), 
					   GeneralConstants.ONE_VALUE_INTEGER) );
			
			programAmortizationCoupon.setCorporativeDate(CommonsUtilities.currentDate());

			//Currency
			programAmortizationCoupon.setCurrency( security.getCurrency() );
			//Registry Date
			programAmortizationCoupon.setRegistryDate( CommonsUtilities.addDaysToDate(programAmortizationCoupon.getExpirationDate(), 
					security.getStockRegistryDays()*-1) );
			programAmortizationCoupon.setRegistryDate(generalParameterFacade.workingDateCalculateServiceFacade(programAmortizationCoupon.getRegistryDate(), -1));
			
			//CutOffDate
			programAmortizationCoupon.setCutoffDate( programAmortizationCoupon.getRegistryDate() ); 
			programAmortizationCoupon.setIndRounding(BooleanType.NO.getCode());
			programAmortizationCoupon.setStateProgramAmortization(ProgramScheduleStateType.PENDING.getCode());
			programAmortizationCoupon.setAmortizationPaymentSchedule(security.getAmortizationPaymentSchedule());
			programAmortizationCoupon.setPayedAmount(BigDecimal.ZERO);
			programAmortizationCoupon.setCouponAmount(security.getInitialNominalValue().multiply(programAmortizationCoupon.getAmortizationFactor()).divide(BigDecimal.valueOf(100)  , MathContext.DECIMAL128));
			programAmortizationCoupon.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			programAmortizationCoupon.setRegisterDate(CommonsUtilities.currentDateTime());
			programAmortizationCoupon.setIndRounding(Integer.valueOf(2));
			programAmortizationCoupon.setLastModifyApp( Integer.valueOf(1) );
			programAmortizationCoupon.setLastModifyDate( CommonsUtilities.currentDateTime() );
			programAmortizationCoupon.setLastModifyIp( userInfo.getUserAccountSession().getIpAddress()  );
			programAmortizationCoupon.setLastModifyUser(userInfo.getUserAccountSession().getUserName() );
			
			security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().add( programAmortizationCoupon );
		}
//		securitiesUtils.calculateCouponAmountProgramInterest(security);
	}
	
	/**
	 * Checks if is not generated cfi code.
	 *
	 * @return true, if is generated cfi code
	 */
	public boolean isNotGeneratedCfiCode(){
		
		if(SecurityClassType.ACE.getCode().equals( security.getSecurityClass() )){
			return false;
		}
		if(SecurityClassType.ANR.getCode().equals( security.getSecurityClass() )){
			return false;
		}
		if(SecurityClassType.BB.getCode().equals( security.getSecurityClass() )){
			return false;
		}
		if(SecurityClassType.BCA.getCode().equals( security.getSecurityClass() )){
			return false;
		}
		if(SecurityClassType.CDB.getCode().equals( security.getSecurityClass() )){
			return false;
		}
		if(SecurityClassType.CDD.getCode().equals( security.getSecurityClass() )){
			return false;
		}
		if(SecurityClassType.CDI.getCode().equals( security.getSecurityClass() )){
			return false;
		}
		if(SecurityClassType.CSP.getCode().equals( security.getSecurityClass() )){
			return false;
		}
		if(SecurityClassType.CNC.getCode().equals( security.getSecurityClass() )){
			return false;
		}
		if(SecurityClassType.DPA.getCode().equals( security.getSecurityClass() )){
			return false;
		}
		if(SecurityClassType.DPF.getCode().equals( security.getSecurityClass() )){
			return false;
		}
		if(SecurityClassType.LCB.getCode().equals( security.getSecurityClass() )){
			return false;
		}
		if(SecurityClassType.LTC.getCode().equals( security.getSecurityClass() )){
			return false;
		}
		if(SecurityClassType.NCF.getCode().equals( security.getSecurityClass() )){
			return false;
		}
		if(SecurityClassType.NCM.getCode().equals( security.getSecurityClass() )){
			return false;
		}
		if(SecurityClassType.PGR.getCode().equals( security.getSecurityClass() )){
			return false;
		}
		if(SecurityClassType.PGS.getCode().equals( security.getSecurityClass() )){
			return false;
		}
		if(SecurityClassType.PGE.getCode().equals( security.getSecurityClass() )){
			return false;
		}
		if(SecurityClassType.CFF.getCode().equals( security.getSecurityClass() )){
			return false;
		}
		if(SecurityClassType.FCT.getCode().equals( security.getSecurityClass() )){
			return false;
		}
		if(SecurityClassType.AOP.getCode().equals( security.getSecurityClass() )){
			return false;
		}
		if(SecurityClassType.CPP.getCode().equals( security.getSecurityClass() )){
			return false;
		}
		if(SecurityClassType.BBX.getCode().equals( security.getSecurityClass() )){
			return false;
		}
		if(SecurityClassType.BTX.getCode().equals( security.getSecurityClass() )){
			return false;
		}
		if(SecurityClassType.BOP.getCode().equals( security.getSecurityClass() )){
			return false;
		}
		if(security.getIssuance().getSecurityType().equals(SecurityType.PRIVATE.getCode())) {
			return false;
		}
		return true;
	}

	/**
	 * Enabled the Alternate code field.
	 *
	 * @return true, if is disabled security alternate code
	 */
	public boolean isDisabledSecurityAlternateCode(){

		if(SecurityClassType.DPA.getCode().equals( security.getSecurityClass() )){
			return false;
		}
		
		if(SecurityClassType.DPF.getCode().equals( security.getSecurityClass() )){
			return false;
		}
		
		return true;
	}
	
	
	/**
	 * Clean security search handler.
	 *
	 * @param event the event
	 */
	public void cleanSecuritySearchHandler(ActionEvent event){
		try { 
			securityDataModel=null;
			securityTOsearch=new SecurityTO();
			security=null;
			issuanceHelperSearch=new Issuance();			
			securityTOsearch.setIssuanceDate(null);
			securityTOsearch.setExpirationDate(null);
			
			searchFiltersConcat=buildCiteriaSearchConcat();
			
			if((userInfo.getUserAccountSession().isIssuerInstitucion()|| userInfo.getUserAccountSession().isIssuerDpfInstitucion()) &&
					Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getIssuerCode())){
				Issuer issuerFilter=new Issuer();
				issuerFilter.setIdIssuerPk( userInfo.getUserAccountSession().getIssuerCode() );
				loggedIssuer=new Issuer();
				loggedIssuer=issuerServiceFacade.findIssuerByFiltersServiceFacade(issuerFilter);
				
				issuerHelperMgmt=issuerHelperSearch;
			} else {
				issuerHelperSearch = new Issuer();
			}
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clean security mgmt handler.
	 */
	public void cleanSecurityMgmtHandler(){
		try {
			defaultRegisterView();
			
			valuatorHistoryDetailBean.setViewOperationType(getViewOperationType());
			valuatorHistoryDetailBean.setSecurity(security);
			valuatorHistoryDetailBean.initializeComponent();
			
			JSFUtilities.resetComponent("frmSecurityMgmt");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Check digint generate.
	 *
	 * @param strbIsinCode the strb isin code
	 * @return the string
	 * @throws Exception the exception
	 */
	public String checkDigintGenerate(String strbIsinCode) throws Exception{
		StringBuilder isinCode=new StringBuilder(strbIsinCode);
		BigDecimal isinCodeDecimal=null;
		BigDecimal sum=new BigDecimal(BigInteger.ZERO);
		BigDecimal factor=new BigDecimal(2);
		BigDecimal checkDigit=new BigDecimal(BigInteger.ZERO);
		
		for(int i=0;i<isinCode.length();i++){
			char character=isinCode.charAt(i);
			if(Character.isLetter(character)){
				int asiiCode=(int)character;
				asiiCode=asiiCode-55;
				isinCode.replace(i, i+1, String.valueOf(asiiCode));
			}
		}
		isinCodeDecimal=new BigDecimal(isinCode.toString());
		
		while(isinCodeDecimal.compareTo(BigDecimal.ZERO)!=0){
			BigDecimal currentDigit=isinCodeDecimal.remainder(BigDecimal.TEN);
			isinCodeDecimal=isinCodeDecimal.divide(BigDecimal.TEN, BigDecimal.ROUND_FLOOR);
			if(currentDigit.multiply(factor).compareTo(BigDecimal.TEN)==-1){
				sum=sum.add( currentDigit.multiply(factor) );
			}else{
				BigDecimal multiplyCurrentDigit=currentDigit.multiply(factor);
				while(multiplyCurrentDigit.compareTo(BigDecimal.ZERO)!=0){
					BigDecimal individualDigit=multiplyCurrentDigit.remainder(BigDecimal.TEN);
					multiplyCurrentDigit=multiplyCurrentDigit.divide(BigDecimal.TEN, BigDecimal.ROUND_FLOOR);
					sum=sum.add( individualDigit );
				}
			}
			if(factor.compareTo(BigDecimal.valueOf(2))==0)
				factor=new BigDecimal(1);
			else
				factor=new BigDecimal(2);
		}
		checkDigit=sum.remainder(BigDecimal.TEN);
		if(checkDigit.compareTo(BigDecimal.ZERO)==0){
			return "0";
		}
		checkDigit=BigDecimal.TEN.subtract( checkDigit );
		return checkDigit.toString();
	}
	
	/**
	 * Change cbo interest type handler.
	 */
	public void changeCboInterestTypeHandler(){
		try {
			if(isViewOperationRegister()){
				
				security.setYield(null);
				security.setInterestRate(null);
	
	//			security.setFinancialIndex(null);
	
				/*Fields of mixed interest type -Start*/
				security.setRateType(null);
				security.setRateValue(null);
				security.setOperationType(null);
				security.setSpread(null);
				security.setInterestRatePeriodicity(null);
				security.setMinimumRate(null);
				security.setMaximumRate(null);
				/*Fields of mixed interest type -End*/
				
				JSFUtilities.resetComponent(SecurityMgmtType.TXT_MINIUM_RATE.getValue());
				JSFUtilities.resetComponent(SecurityMgmtType.TXT_MAXIUM_RATE.getValue());
			}
			if(security.isCeroInterestType()){
				security.setInterestRate(BigDecimal.ZERO);
				security.setInterestPaymentModality( InterestPaymentModalityType.AT_MATURITY.getCode() );
				security.setCapitalPaymentModality( CapitalPaymentModalityType.AT_MATURITY.getCode() );
				
				changeCboInterestPaymentModalityHandler();
				chageCboAmortPaymentModalityHandler();
			}
			
			changeCboRateTypeHandler();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change cbo rate type handler.
	 */
	public void changeCboRateTypeHandler(){
		try {
			if(security.getRateType()!=null){
				Map<String, Object> map=new HashMap<String, Object>();
				map.put("indicatorType", IndicatorType.TASA.getCode());
				map.put("indicatorCode", security.getRateType());
				map.put("status", FinancialIndicatorStateType.VIGENTE.getCode());
				map.put("currentDate",CommonsUtilities.currentDate());
				FinancialIndicator financialIndicator=null;
				try {
					financialIndicator=generalParameterFacade.findFinancialIndicatorServiceFacade(map);

					security.setRateValue( financialIndicator.getRateValue() );
					security.setInterestRatePeriodicity( financialIndicator.getPeriodicityType() ); 
					security.setOperationType(null);
					security.setSpread(null);
				} catch (ServiceException se) {
					security.setRateType(null);
					security.setRateValue( null );
					security.setOperationType(null);
					security.setSpread(null);
					security.setInterestRatePeriodicity( null );
					if(se.getErrorService()!=null){
						String message=PropertiesUtilities.getExceptionMessage(se.getErrorService().getMessage());
						JSFUtilities.addContextMessage(SecurityMgmtType.CBO_RATE_TYPE.getValue(), 
													   FacesMessage.SEVERITY_ERROR, message, message);
					}
				}
			}else{
				security.setRateValue(null);
				security.setOperationType(null);
				security.setSpread(null);
			}
			calculateInterestRateHandler();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Calculate interest rate handler.
	 *
	 * @throws Exception the exception
	 */
	public void calculateInterestRateHandler() throws Exception{
		try {
			if(security.isMixedInterestType()){
				if(security.getRateType()!=null && security.getOperationType()!=null && security.getSpread()!=null){
					if(security.getOperationType().equals( OperationType.SUM.getCode() )){
						security.setInterestRate(security.getRateValue().add(security.getSpread()));
					}else if(security.getOperationType().equals( OperationType.SUBTRACTION.getCode() )){
						security.setInterestRate(security.getRateValue().subtract(security.getSpread()));
					}
				}else{
					security.setInterestRate(null);
				}
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Back to search page handler.
	 *
	 * @return the string
	 */
	public String backToSearchPageHandler(){
		try {
			setViewOperationType(ViewOperationsType.CONSULT.getCode());
	//		issuerHelperBean.setIssuerDescription( issuanceTOsearch.getIssuerBusinessName() );
			return "searchSecurityMgmt";
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}
	
	/**
	 * Back to search page handler.
	 *
	 * @return the string
	 */
	public String backToSearchPage(){
		try {
			setViewOperationType(ViewOperationsType.CONSULT.getCode());
			
			searchSecuritiesHandler(null);
			
			return "searchSecurityMgmt";
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}
	
	/**
	 * Before block handler.
	 */
	public void beforeBlockHandler(){
		try {
			securityHistoryState=new SecurityHistoryState();
			if(security==null){
				showMessageOnDialog(
						PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.ERROR_RECORD_REQUIRED, null);	
				JSFUtilities.showSimpleValidationDialog();
				return;	
			}else if( !(security.getStateSecurity().equals( SecurityStateType.REGISTERED.getCode()) || security.getStateSecurity().equals( SecurityStateType.GUARDA_MIXTA.getCode()) || security.getStateSecurity().equals( SecurityStateType.GUARDA_EXCLUSIVE.getCode()) ) ){
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.SECURITY_ERROR_BLOCK_STATE_NO_REGISTERED, null);
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			List<String> lstOperationUser = validateIsValidUser(security);
			  if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
				  String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
					String bodyMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_INVALID_USER_BLOCK_SECURITY,new Object[]{StringUtils.join(lstOperationUser,",")});
					showMessageOnDialog(headerMessage, bodyMessage);
					JSFUtilities.showSimpleValidationDialog();
					return;
			  }
			  
			security.setIssuer(new Issuer());
			JSFUtilities.resetComponent(SecurityMgmtType.FRM_MOTIVE.getValue());
			
			if(lstCboSecurityBlockMotive==null || lstCboSecurityBlockMotive.isEmpty()){
				loadCboSecurityBlockMotive();
			}
			lstCboSecurityMotiveAux=lstCboSecurityBlockMotive;
			showMessageOnDialog(PropertiesConstants.LBL_MOTIVE_BLOCK, null, null, null);
			setViewOperationType(ViewOperationsType.BLOCK.getCode());
			
			JSFUtilities.executeJavascriptFunction("PF('dlgwMotive').show()");
			JSFUtilities.updateComponent(":cnfMsgCustomValidationSec,:dlgMotive");
		} catch (Exception e) {
			excepcion.fire( new ExceptionToCatchEvent(e) );
		}
	}
	
	/**
	 * Metodo para autorizar un valor 
	 */
	public String securityBeforeAuthorize(){
		
		//En caso de no seleccionar un registro sale un mensaje
		if(security==null){
			showMessageOnDialog(
					PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.ERROR_RECORD_REQUIRED, null);	
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}else{
			//validamos que no se encuentre autorizado para entrar a la pantalla de detalle
			if(Validations.validateIsNotNullAndNotEmpty(security.getIndAuthorized())){
		 		if (security.getIndAuthorized().equals(GeneralConstants.ZERO_VALUE_INTEGER)){
		 			
		 			//Muestra el detalle del registro y el boton de "Autorizar"
					detailOperationHandler(security);
					setViewOperationType( ViewOperationsType.AUTHORIZE.getCode());
					String actionMessage = "securityDetailRule";
					return actionMessage;
		 			
		 		}else{
		 			showMessageOnDialog(
							PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.SECURITY_AUTHORIZED, null);	
					JSFUtilities.showSimpleValidationDialog();
					return null;		
		 		}
		 	}else{
				showMessageOnDialog(
						PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.SECURITY_AUTHORIZED, null);	
				JSFUtilities.showSimpleValidationDialog();
				return null;		
		 	}
		}
	}
	
	
	/**
	 * Metodo para mostrar el mensaje antes de confirmar "si o no"
	 */
	public void securityAuthorizeMessage(){
		
		showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null,
				PropertiesConstants.ERROR_RECORD_REQUIRED, null);
		JSFUtilities.executeJavascriptFunction("PF('cnfSecurityAuthorize').show()");
		
	}
	
	/**
	 * Metodo para cambiar el indicador y datos de auditoria
	 */
	@LoggerAuditWeb
	public void securityAuthorize(){
		try {
			//cerramos mensaje
			JSFUtilities.executeJavascriptFunction("PF('cnfSecurityAuthorize').hide()");
			
			//Actualizamos los datos en el valor
			securityServiceFacade.securityAuthorize(security);
			
			//Mostramos mensaje de "aceptar" para volver a la pantalla de administracion
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null,
					PropertiesConstants.SECURITY_AUTHORIZED_CORRECT, null);
			JSFUtilities.executeJavascriptFunction("PF('cnfSecurityAuthorizeCorrect').show()");
			
		} catch (Exception e) {
			excepcion.fire( new ExceptionToCatchEvent( e ) );
		}
	}
	
	/**
	 * Before unblock handler.
	 */
	public void beforeUnblockHandler(){
		try {
			securityHistoryState=new SecurityHistoryState();
			if(security==null){
				showMessageOnDialog(
						PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, PropertiesConstants.ERROR_RECORD_REQUIRED, null);	
				JSFUtilities.showSimpleValidationDialog();
				return;			
			}else if(!(security.getStateSecurity().equals( SecurityStateType.BLOCKED.getCode()) || security.getStateSecurity().equals( SecurityStateType.GUARDA_EXCLUSIVE.getCode()) || security.getStateSecurity().equals( SecurityStateType.GUARDA_MIXTA.getCode()) )){
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR,null,PropertiesConstants.SECURITY_ERROR_UNBLOCK_STATE_NO_BLOCK, null);
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			List<String> lstOperationUser = validateIsValidUser(security);
			  if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
				  String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
					String bodyMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_INVALID_USER_UNBLOCK_SECURITY,new Object[]{StringUtils.join(lstOperationUser,",")});
					showMessageOnDialog(headerMessage, bodyMessage);
					JSFUtilities.showSimpleValidationDialog();
					return;
			  }
			  
			SecurityTO securityTO=new SecurityTO();
			//TODO changed because not all securities have isin code
			securityTO.setIdSecurityCodePk( security.getIdSecurityCodePk() );
			security=securityServiceFacade.findSecurityServiceFacade( securityTO );
			
			if(security.getIssuer().getStateIssuer().equals( IssuerStateType.BLOCKED.getCode() )){
				showMessageOnDialog(
						null, null, PropertiesConstants.SECURITY_ERROR_UNBLOCK_STATE_ISSUER_BLOCK, null);	
				JSFUtilities.showSimpleValidationDialog();
				return;	
			}
			if(lstCboSecurityUnblockMotive==null || lstCboSecurityUnblockMotive.isEmpty()){
				loadCboSecurityUnblockMotive();
			}
			lstCboSecurityMotiveAux=lstCboSecurityUnblockMotive;
			JSFUtilities.resetComponent(SecurityMgmtType.FRM_MOTIVE.getValue());
			showMessageOnDialog(PropertiesConstants.LBL_MOTIVE_UNBLOCK, null, null, null);
			setViewOperationType(ViewOperationsType.UNBLOCK.getCode());
			JSFUtilities.executeJavascriptFunction("PF('dlgwMotive').show()");
			
		} catch (Exception e) {
			excepcion.fire( new ExceptionToCatchEvent( e ) );
		}
	}
	
	/**
	 * Process motive handler.
	 */
	public void processMotiveHandler(){
		try {
			JSFUtilities.executeJavascriptFunction("PF('dlgwMotive').hide()");
			Object[] argObj=new Object[1];
			argObj[0]=security.getIdSecurityCodePk();
			if(isViewOperationBlock()){
				showMessageOnDialog(
						PropertiesConstants.LBL_HEADER_ALERT_CONFIRM, null, 
						PropertiesConstants.SECURITY_CONFIRM_BLOCK, argObj);
				JSFUtilities.executeJavascriptFunction("PF('cnfwConfirmOperation').show()");
			}else if(isViewOperationUnBlock()){
				showMessageOnDialog(
						PropertiesConstants.LBL_HEADER_ALERT_CONFIRM, null, 
						PropertiesConstants.SECURITY_CONFIRM_UNBLOCK, argObj);
				JSFUtilities.executeJavascriptFunction("PF('cnfwConfirmOperation').show()");
			}
			
		} catch (Exception e) {
			excepcion.fire( new ExceptionToCatchEvent( e ) );
		}
	}
	
	/**
	 * Process operation handler.
	 */
	@LoggerAuditWeb
	public void processOperationHandler(){
		try {
			JSFUtilities.executeJavascriptFunction("PF('cnfwConfirmOperation').hide()");
			Object[] argObj=new Object[1];
			argObj[0]=security.getIdSecurityCodePk();
			
			securityHistoryState.setOldState(security.getStateSecurity());
			securityHistoryState.setSecurity(security);
			
			if(isViewOperationBlock()){
				
				if(securityHistoryState.getMotive()!=null && securityHistoryState.getMotive().equals(SecurityMotiveBlockType.GUARDA_EXCLUSIVA.getCode()) ) {
					security.setStateSecurity(SecurityStateType.GUARDA_EXCLUSIVE.getCode());
				}else if(securityHistoryState.getMotive()!=null && securityHistoryState.getMotive().equals(SecurityMotiveBlockType.GUARDA_MIXTA.getCode()) ) {
					security.setStateSecurity(SecurityStateType.GUARDA_MIXTA.getCode());
				}else {
					security.setStateSecurity(SecurityStateType.BLOCKED.getCode());
				}
				showMessageOnDialog(
						PropertiesConstants.LBL_HEADER_ALERT_SUCCESS,
						null,
						PropertiesConstants.SECURITY_BLOCK_SUCCESS,
						argObj);		
			}else if(isViewOperationUnBlock()){
				security.setStateSecurity(SecurityStateType.REGISTERED.getCode());
				showMessageOnDialog(
						PropertiesConstants.LBL_HEADER_ALERT_SUCCESS,
						null,
						PropertiesConstants.SECURITY_UNBLOCK_SUCCESS,
						argObj);	
			}
			securityHistoryState.setNewState(security.getStateSecurity());
			
			securityServiceFacade.updateSecurityStateServiceFacade(security,getViewOperationType(),securityHistoryState);
			
			searchSecuritiesHandler(null);
			
			JSFUtilities.executeJavascriptFunction("PF('cnfwEndTransactionSec').show()");
		} catch (Exception e) {
			excepcion.fire( new ExceptionToCatchEvent( e ) );
		}
	}
	
	/**
	 * Select issuance date handler.
	 */
	public void selectIssuanceDateHandler(){
		try {
			if(security.isFixedInstrumentTypeAcc()){
				isReGenerated=false;
				//cleanInterestConditionAndAmortization();				
				if(isViewOperationRegister()){
					cleanConditionTermsFields();
					cleanInterestFields();
					
					cleanFldsAmortization();
				}else if(isViewOperationModify()){
					selectExpiredDateHandler();
				}	
			}
		} catch (Exception e) {
			excepcion.fire( new ExceptionToCatchEvent( e ) );
		}
	}
	
	/**
	 * Clean condition terms fields.
	 */
	public void cleanConditionTermsFields(){
		security.setExpirationDate(null);
		security.setSecurityDaysTerm(null);
		security.setSecurityMonthsTerm(null);
		security.setSecurityTerm(null);
		security.setNumberCoupons(null);
		
		JSFUtilities.resetComponent( SecurityMgmtType.CAL_EXPIRED_DATE.getValue() );
		JSFUtilities.resetComponent( SecurityMgmtType.TXT_DAYS_TERMS.getValue() );
		JSFUtilities.resetComponent( SecurityMgmtType.TXT_MONTHS_TERMS.getValue() );
		JSFUtilities.resetComponent( SecurityMgmtType.CBO_SECURITY_TERM.getValue() );
		JSFUtilities.resetComponent(SecurityMgmtType.TXT_NRO_COUPONS.getValue());
	}
	
	/**
	 * Clean interest fields.
	 */
	public void cleanInterestFields(){
		security.setInterestPaymentModality(null);
		security.setInterestRate(null);
		security.setPeriodicity(null);
		security.setPeriodicityDays(null);
		security.setCalendarMonth(null);
		security.setCalendarType(null);
		security.setCalendarDays(null);
		security.setCouponFirstExpirationDate(null);
		
		JSFUtilities.resetComponent( SecurityMgmtType.CBO_INT_PAYMENT_MODE.getValue());
		JSFUtilities.resetComponent( SecurityMgmtType.TXT_INTEREST_RATE.getValue() );
		JSFUtilities.resetComponent( SecurityMgmtType.CBO_INTEREST_PERIODICITY.getValue() );
		JSFUtilities.resetComponent( SecurityMgmtType.TXT_PERIODICITY_DAYS.getValue() );
		JSFUtilities.resetComponent( SecurityMgmtType.CBO_CALENDAR_MONTH.getValue() );
		JSFUtilities.resetComponent( SecurityMgmtType.CBO_CALENDAR_TYPE.getValue() );
		JSFUtilities.resetComponent( SecurityMgmtType.CBO_CALENDAR_DAY.getValue() );
		JSFUtilities.resetComponent( SecurityMgmtType.CAL_FIRST_PAY_COUPON.getValue() );
	}
	
	/**
	 * Clean interest condition and amortization.
	 *
	 * @throws Exception the exception
	 */
	public void cleanInterestConditionAndAmortization() throws Exception{
		//interest condition
	//	security.setExpirationDate(null);
		security.setCouponFirstExpirationDate(null);
	//	security.setSecurityMonthsTerm(null);
		security.setNumberCoupons(null);
		
		security.setInterestPaymentModality(null);
		security.setInterestRate(null);
		security.setPeriodicity(null);
		security.setPeriodicityDays(null);
		security.setCalendarMonth(null);
		security.setCalendarType(null);
		security.setCalendarDays(null);
		
		JSFUtilities.resetComponent("frmSecurityMgmt:tviewGeneral:calFirstPayCoupon");
		JSFUtilities.resetComponent("frmSecurityMgmt:tviewGeneral:txtNroCoupons");
		
		JSFUtilities.resetComponent(SecurityMgmtType.CBO_INT_PAYMENT_MODE.getValue());
		JSFUtilities.resetComponent(SecurityMgmtType.TXT_INTEREST_RATE.getValue());
		JSFUtilities.resetComponent(SecurityMgmtType.CBO_INTEREST_PERIODICITY.getValue());
		JSFUtilities.resetComponent(SecurityMgmtType.TXT_PERIODICITY_DAYS.getValue());
		JSFUtilities.resetComponent(SecurityMgmtType.CBO_CALENDAR_MONTH.getValue());
		JSFUtilities.resetComponent(SecurityMgmtType.CBO_CALENDAR_TYPE.getValue());
		JSFUtilities.resetComponent(SecurityMgmtType.CBO_CALENDAR_DAY.getValue());
	}
	

	
	/**
	 * Clean flds amortization.
	 *
	 * @throws Exception the exception
	 */
	public void cleanFldsAmortization() throws Exception{
		//amortization
		security.setCapitalPaymentModality(null);
		security.setAmortizationType(null);
		security.setAmortizationPeriodicity(null);
		security.setAmortizationPeriodicityDays(null);
		setPaymentNumberAmortization(null);
		security.setAmortizationFactor(null);
		
		JSFUtilities.resetComponent("frmSecurityMgmt:tviewGeneral:cboAmortPaymentMode");
		JSFUtilities.resetComponent("frmSecurityMgmt:tviewGeneral:cboAmortizationType");
		JSFUtilities.resetComponent("frmSecurityMgmt:tviewGeneral:cboPerAmortization");
		JSFUtilities.resetComponent("frmSecurityMgmt:tviewGeneral:txtPaymentsNumber");
		JSFUtilities.resetComponent("frmSecurityMgmt:tviewGeneral:txtAmortizationFactor");
		JSFUtilities.resetComponent("frmSecurityMgmt:tviewGeneral:cboAmortizationOn");
		JSFUtilities.resetComponent(SecurityMgmtType.TXT_AMORT_PERIODICITY_DAYS.getValue());
	}	
	
	/**
	 * Generate nro coupons.
	 *
	 * @throws Exception the exception
	 */
	public void generateNroCoupons() throws Exception{
		if(security.isAtMaturityIntPaymentModality()){
			security.setNumberCoupons(1);
		}else{
			if(security.getPeriodicity()!=null && security.getSecurityMonthsTerm()!=null){
				
				Integer periodityMonth=InterestPeriodicityType.get(security.getPeriodicity()).getIndicator1();
				Object[] argObj=new Object[2];
				if(periodityMonth.intValue()>security.getSecurityMonthsTerm().intValue()){
					
					argObj[0]=PropertiesUtilities.getMessage("security.lbl.interest.payment.periodicity");
					argObj[1]=PropertiesUtilities.getMessage("security.lbl.months.terms.value");
						
					String strMsg =  PropertiesUtilities.getMessage(PropertiesConstants.ERROR_LESS_THAN_OR_EQUAL_TO, argObj);
					JSFUtilities.addContextMessage("frmSecurityMgmt:tviewGeneral:cboInterestPayPeriodicity", FacesMessage.SEVERITY_ERROR, strMsg, strMsg);
					return;
				}
				
				Integer numberCoupons=security.getSecurityMonthsTerm().intValue() / InterestPeriodicityType.get(security.getPeriodicity().intValue()).getIndicator1().intValue();
				security.setNumberCoupons( numberCoupons );
			}
		}
		
	}
	
	/**
	 * Generate payment cronogram.
	 *
	 * @param event the event
	 */
	public void generatePaymentCronogram(ActionEvent event){
		try {
			JSFUtilities.putViewMap("enablePaymentScheUpl", true);	
			Date nextYear=null;
			Date septemberLeap=null;
			
			if(security.getCurrency().equals(CurrencyType.UFV.getCode()) || 
					security.getCurrency().equals(CurrencyType.DMV.getCode())){
				this.setExchangeRate(true);
			}else{
				this.setExchangeRate(false);
			}
			
			Date securityRegistryDate=isViewOperationRegister() ? CommonsUtilities.currentDate() : security.getRegistryDate();

			if(isViewOperationModify()){
				if(CommonsUtilities.convertDatetoString(security.getIssuanceDate()).equals( 
					CommonsUtilities.convertDatetoString(securityInitialState.getIssuanceDate())) 
					&& security.getSecurityMonthsTerm().compareTo( securityInitialState.getSecurityMonthsTerm() )==0){
					
					JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
													 PropertiesConstants.SECURITY_ERROR_PAYMENT_REGENERATE, null);
					JSFUtilities.showSimpleValidationDialog();	
					return; 
				}
				isReGenerated=true;
				isFirstReGenerated=false;
			}
			Date previousExpirationDate=null;
			if(security.isCeroInterestType() ||  security.isIndistinctPeriodicity()){
				security.setInterestPaymentSchedule(new InterestPaymentSchedule());
			}else{
					security.getInterestPaymentSchedule().setDataFromSecurity(security);
					ProgramInterestCoupon programInterestCoupon=null;
					security.getInterestPaymentSchedule().setProgramInterestCoupons( new ArrayList<ProgramInterestCoupon>() );
					
					if (security.getInterestPaymentModality().equals(InterestPaymentModalityType.AT_MATURITY.getCode())){
						security.setNumberCoupons(GeneralConstants.ONE_VALUE_INTEGER );
					}
					for(int i=1;i<=security.getNumberCoupons().intValue();i++){
						programInterestCoupon=new ProgramInterestCoupon();
						programInterestCoupon.setCouponNumber(Integer.valueOf(i));
						programInterestCoupon.setCurrency( security.getCurrency() );
						//Begin date
						if(previousExpirationDate==null){
							programInterestCoupon.setBeginingDate(security.getIssuanceDate() );	
						}else{
							programInterestCoupon.setBeginingDate( previousExpirationDate );
						}
						if(i==security.getNumberCoupons().intValue()){
							//Expiration Date
							programInterestCoupon.setExperitationDate(security.getExpirationDate());
							
						}else{
							//Expiration Date
							if(programInterestCoupon.getCouponNumber().equals( GeneralConstants.ONE_VALUE_INTEGER )){
								programInterestCoupon.setExperitationDate( security.getCouponFirstExpirationDate() );
							}else{
								if(security.isByDaysInterestPeriodicity()){
									programInterestCoupon.setExperitationDate(  CommonsUtilities.addDaysToDate(programInterestCoupon.getBeginingDate(), 
											security.getPeriodicityDays() ) );	
								}else{
									programInterestCoupon.setExperitationDate(  CommonsUtilities.addMonthsToDate(programInterestCoupon.getBeginingDate(), 
											InterestPeriodicityType.get(security.getPeriodicity()).getIndicator1() ) );
								}
							}
						}
						previousExpirationDate=programInterestCoupon.getExperitationDate();
						//Payment Date
						programInterestCoupon.setPaymentDate( generalParameterFacade.workingDateCalculateServiceFacade(programInterestCoupon.getExperitationDate(), 
								   GeneralConstants.ONE_VALUE_INTEGER) );
						Integer paymentDays=null;
						if(security.isExtendedTerm()){
							paymentDays=CommonsUtilities.getDaysBetween(programInterestCoupon.getBeginingDate(),programInterestCoupon.getPaymentDate());
						}else{
							paymentDays=CommonsUtilities.getDaysBetween(programInterestCoupon.getBeginingDate(),programInterestCoupon.getExperitationDate());
						}
						programInterestCoupon.setPaymentDays(paymentDays);
						
						if(programInterestCoupon.getCurrency().equals(CurrencyType.UFV.getCode()) || 
								programInterestCoupon.getCurrency().equals(CurrencyType.DMV.getCode())){
							programInterestCoupon.setExchangeDateRate(programInterestCoupon.getPaymentDate());
							programInterestCoupon.setCurrencyPayment(CurrencyType.PYG.getCode());
						}
						
						//Corporative Process Date
	//					programInterestCoupon.setCorporativeDate( CommonsUtilities.addDaysToDate(programInterestCoupon.getExperitationDate(), 
	//							security.getCorporativeProcessDays() * -1) );
						programInterestCoupon.setCorporativeDate( CommonsUtilities.currentDate() );
						
						//Registry Date
						programInterestCoupon.setRegistryDate(CommonsUtilities.addDaysToDate(programInterestCoupon.getExperitationDate(), security.getStockRegistryDays()*-1));
						programInterestCoupon.setRegistryDate(generalParameterFacade.workingDateCalculateServiceFacade(programInterestCoupon.getRegistryDate(), -1));
						//Cut Off Date
						programInterestCoupon.setCutoffDate( programInterestCoupon.getRegistryDate() );
						
						programInterestCoupon.setInterestPaymentCronogram( security.getInterestPaymentSchedule() );
						
						programInterestCoupon.setInterestFactor( security.getInterestRate() );
						
						programInterestCoupon.setStateProgramInterest( ProgramScheduleStateType.PENDING.getCode() );
						programInterestCoupon.setSituationProgramInterest( ProgramScheduleSituationType.PENDING.getCode() );
						
//						if(security.isHavePaymentScheduleNoDesm()){
						if(CommonsUtilities.isLessOrEqualDate(programInterestCoupon.getExperitationDate(), securityRegistryDate, true)){
							programInterestCoupon.setStateProgramInterest( ProgramScheduleStateType.EXPIRED.getCode() );
							programInterestCoupon.setSituationProgramInterest( ProgramScheduleSituationType.ISSUER_PAID.getCode() );
							programInterestCoupon.setInterestRate(BigDecimal.ZERO);
						}
							
//						}
						if(programInterestCoupon.isPendingState()){
							programInterestCoupon.setInterestRate(securitiesUtils.interestRateCalculate(security, 
																										security.getPeriodicity(), 
																										security.getInterestRate(), 
																										programInterestCoupon.getBeginingDate(), 
																										programInterestCoupon.getPaymentDays(),
																										programInterestCoupon.getCouponNumber(),
																										septemberLeap,
																										nextYear));
						}			
//						if(security.isPhysicalIssuanceForm()){
//							programInterestCoupon.setStateProgramInterest( ProgramScheduleStateType.EXPIRED.getCode() );
//							programInterestCoupon.setSituationProgramInterest( ProgramScheduleSituationType.ISSUER_PAID.getCode());
//						}
						programInterestCoupon.setIndRounding(BooleanType.NO.getCode()); 
						programInterestCoupon.setPayedAmount(BigDecimal.ZERO);
						
						if(security.isIndistinctAmortizationPeriodicity()){
							programInterestCoupon.setCouponAmount(null);
						}
						programInterestCoupon.setLastModifyApp( Integer.valueOf(1) );
						programInterestCoupon.setLastModifyDate( CommonsUtilities.currentDateTime() );
						programInterestCoupon.setLastModifyIp( userInfo.getUserAccountSession().getIpAddress()  );
						programInterestCoupon.setLastModifyUser(userInfo.getUserAccountSession().getUserName() );
						if(programInterestCoupon.getCouponNumber().compareTo( security.getNumberCoupons() )==0){
							programInterestCoupon.setLastCoupon(true);
						}
						
						security.getInterestPaymentSchedule().getProgramInterestCoupons().add( programInterestCoupon );
					}
					
					if (security.getInterestPaymentModality().equals(InterestPaymentModalityType.AT_MATURITY.getCode())){
						security.setNumberCoupons(GeneralConstants.ZERO_VALUE_INTEGER);
					}
				}
				if(security.isIndistinctAmortizationPeriodicity()){
					security.setAmortizationPaymentSchedule(new AmortizationPaymentSchedule());
				}else{
					security.getAmortizationPaymentSchedule().setDataSecurity(security);	
					security.getAmortizationPaymentSchedule().setProgramAmortizationCoupons(new ArrayList<ProgramAmortizationCoupon>());				 
				if(security.isProportionalAmortizationType() || security.isAtMaturityAmortPaymentModality()){					
					security.getAmortizationPaymentSchedule().setProgramAmortizationCoupons( new ArrayList<ProgramAmortizationCoupon>());
					Integer numberPayments = getPaymentNumberAmortization();
					security.getAmortizationPaymentSchedule().setNumberPayments( numberPayments );
					ProgramAmortizationCoupon programAmortizationCoupon=null;		
					previousExpirationDate=null;
					BigDecimal amortFactorTotal=BigDecimal.ZERO;
					
					for(int i=1;i<=numberPayments;i++){
						programAmortizationCoupon=new ProgramAmortizationCoupon();
	
						//Coupon Number
						programAmortizationCoupon.setCouponNumber( Integer.valueOf(i) );
						//Begin date
						if(Validations.validateIsNullOrEmpty(previousExpirationDate))
							programAmortizationCoupon.setBeginingDate(security.getIssuanceDate());
						else{
							programAmortizationCoupon.setBeginingDate(previousExpirationDate);
						}				
						
						//Expiration date
						if(i==numberPayments){//ultimo cupon debe tener la fecha de vencimiento del valor
							programAmortizationCoupon.setExpirationDate(security.getExpirationDate());
							
							if(amortFactorTotal.compareTo(GeneralConstants.HUNDRED_VALUE_BIGDECIMAL)==-1){
								BigDecimal sustrac=GeneralConstants.HUNDRED_VALUE_BIGDECIMAL.subtract(amortFactorTotal);
								sustrac=CommonsUtilities.roundRateFactor(sustrac);						
								programAmortizationCoupon.setAmortizationFactor( sustrac);
							}
						}else{		
							if(programAmortizationCoupon.getCouponNumber().equals( GeneralConstants.ONE_VALUE_INTEGER )){
								programAmortizationCoupon.setExpirationDate( security.getPaymentFirstExpirationDate() );
							}else{
								if(security.isByDaysAmortizationPeriodicity()){
									programAmortizationCoupon.setExpirationDate(CommonsUtilities.addDaysToDate(programAmortizationCoupon.getBeginingDate()
											, security.getAmortizationPeriodicityDays()));	
								}else{
									programAmortizationCoupon.setExpirationDate(CommonsUtilities.addMonthsToDate(programAmortizationCoupon.getBeginingDate()
											, InterestPeriodicityType.get(security.getAmortizationPeriodicity()).getIndicator1()));	
								}
							}
							
							BigDecimal securityFactor = security.getAmortizationFactor();
							BigDecimal couponFactor = CommonsUtilities.round(securityFactor, GeneralConstants.FOUR_VALUE_INTEGER, false);
							
							programAmortizationCoupon.setAmortizationFactor(couponFactor);
							amortFactorTotal=amortFactorTotal.add(couponFactor);
							
							if(security.isHavePaymentScheduleNoDesm() && security.isPartialAmortPaymentModality() && security.isProportionalAmortizationType()){
								if(CommonsUtilities.isLessOrEqualDate(programAmortizationCoupon.getExpirationDate(), securityRegistryDate, true)){
									programAmortizationCoupon.setAmortizationFactor(BigDecimal.ZERO);
									amortFactorTotal=amortFactorTotal.subtract(security.getAmortizationFactor());
								}	
							}
						}
						previousExpirationDate = programAmortizationCoupon.getExpirationDate();
						programAmortizationCoupon.setCouponAmount(security.getInitialNominalValue().multiply(programAmortizationCoupon.getAmortizationFactor()).divide(BigDecimal.valueOf(100)  , MathContext.DECIMAL128));
						//Payment Date 
						programAmortizationCoupon.setPaymentDate( generalParameterFacade.workingDateCalculateServiceFacade(programAmortizationCoupon.getExpirationDate(), 
																														   GeneralConstants.ONE_VALUE_INTEGER) );
						programAmortizationCoupon.setCorporativeDate(CommonsUtilities.currentDate());
						//Currency
						programAmortizationCoupon.setCurrency( security.getCurrency() );
						
						if(programAmortizationCoupon.getCurrency().equals(CurrencyType.UFV.getCode()) || 
								programAmortizationCoupon.getCurrency().equals(CurrencyType.DMV.getCode())){
							programAmortizationCoupon.setExchangeDateRate(programAmortizationCoupon.getPaymentDate());
							programAmortizationCoupon.setCurrencyPayment(CurrencyType.PYG.getCode());
						}
						
						//Registry Date
						programAmortizationCoupon.setRegistryDate( CommonsUtilities.addDaysToDate(programAmortizationCoupon.getExpirationDate(), 
																security.getStockRegistryDays()*-1) );
						programAmortizationCoupon.setRegistryDate(generalParameterFacade.workingDateCalculateServiceFacade(programAmortizationCoupon.getRegistryDate(), -1));
						//CutOffDate
						programAmortizationCoupon.setCutoffDate( programAmortizationCoupon.getRegistryDate() ); 
						programAmortizationCoupon.setIndRounding(BooleanType.NO.getCode());
						if(security.getAmortizationOn().equals( AmortizationOnType.CAPITAL.getCode() )){
							if(Validations.validateIsNullOrEmpty(security.getAmortizationPaymentSchedule().getAmortizationFactor())){
								String strMsg =  PropertiesUtilities.getMessage(PropertiesConstants.PAYMENT_CRON_ERROR_FACTOR_NO_APPLIED);
								JSFUtilities.addContextMessage("frmSecurityMgmt:tviewGeneral:dtbAmortizationCoupons", FacesMessage.SEVERITY_ERROR, strMsg, strMsg);						
								security.getAmortizationPaymentSchedule().setProgramAmortizationCoupons(null);
								break;
							}						
							programAmortizationCoupon.setAmortizationAmount( 
									security.getCurrentNominalValue().multiply( security.getAmortizationPaymentSchedule().getAmortizationFactor() ).divide(BigDecimal.valueOf(100.0)) );
							
						}else if(security.getAmortizationOn().equals( AmortizationOnType.NOMINAL_VALUE.getCode() )){
							if(Validations.validateIsNullOrEmpty(security.getAmortizationPaymentSchedule().getAmortizationFactor())){
								String strMsg =  PropertiesUtilities.getMessage(PropertiesConstants.PAYMENT_CRON_ERROR_FACTOR_NO_APPLIED);
								JSFUtilities.addContextMessage("frmSecurityMgmt:tviewGeneral:dtbAmortizationCoupons", FacesMessage.SEVERITY_ERROR, strMsg, strMsg);						
								security.getAmortizationPaymentSchedule().setProgramAmortizationCoupons(null);
								break;
							}	
							programAmortizationCoupon.setAmortizationAmount( 
									security.getInitialNominalValue().multiply( security.getAmortizationPaymentSchedule().getAmortizationFactor() ).divide(BigDecimal.valueOf(100.0)) );
						}
						programAmortizationCoupon.setStateProgramAmortization(ProgramScheduleStateType.PENDING.getCode());
						programAmortizationCoupon.setSituationProgramAmortization( ProgramScheduleSituationType.PENDING.getCode() );
						if(security.isHavePaymentScheduleNoDesm() && security.isPartialAmortPaymentModality() && security.isProportionalAmortizationType()){
							if(CommonsUtilities.isLessOrEqualDate(programAmortizationCoupon.getExpirationDate(), securityRegistryDate, true)
								//|| security.isPhysicalIssuanceForm() //issue 631
								){
								programAmortizationCoupon.setStateProgramAmortization(ProgramScheduleStateType.EXPIRED.getCode());
								programAmortizationCoupon.setSituationProgramAmortization( ProgramScheduleSituationType.ISSUER_PAID.getCode() );
							}
						}				
						programAmortizationCoupon.setAmortizationPaymentSchedule(security.getAmortizationPaymentSchedule());
						programAmortizationCoupon.setPayedAmount(BigDecimal.ZERO);
						programAmortizationCoupon.setRegistryUser(userInfo.getUserAccountSession().getUserName());
						programAmortizationCoupon.setRegisterDate(CommonsUtilities.currentDateTime());
						programAmortizationCoupon.setIndRounding(Integer.valueOf(2));
						programAmortizationCoupon.setLastModifyApp( Integer.valueOf(1) );
						programAmortizationCoupon.setLastModifyDate( CommonsUtilities.currentDateTime() );
						programAmortizationCoupon.setLastModifyIp( userInfo.getUserAccountSession().getIpAddress()  );
						programAmortizationCoupon.setLastModifyUser(userInfo.getUserAccountSession().getUserName() );
						if(programAmortizationCoupon.getCouponNumber().compareTo( getPaymentNumberAmortization() )==0){
							programAmortizationCoupon.setLastPayment( true );
						}
						
						int paymentDaysToAmortization=0;
						if(security.isExtendedTerm()){
							paymentDaysToAmortization=CommonsUtilities.getDaysBetween(programAmortizationCoupon.getBeginingDate(),programAmortizationCoupon.getPaymentDate());
						}else{
							paymentDaysToAmortization=CommonsUtilities.getDaysBetween(programAmortizationCoupon.getBeginingDate(),programAmortizationCoupon.getExpirationDate());
						}
						
						//calculando el paymentDays para la cuponera de Amortizaciones
						programAmortizationCoupon.setPaymentAmortizationDays(paymentDaysToAmortization);
						
						security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().add( programAmortizationCoupon );
					}
					security.getAmortizationPaymentSchedule().calculateTotalFactorAndAmount();
				}
			}
			if(!security.isIndistinctAmortizationPeriodicity()){
				securitiesUtils.calculateCouponAmountProgramInterest(security);
			}
				
		if(isViewOperationRegister()){
			securityInitialStateStr=security.toString();
		}
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire( new ExceptionToCatchEvent( e ) );
		}
	}
	
	/**
	 * Change program interest date.
	 *
	 * @param programInterestCoupon the program interest coupon
	 * @param paramMethod the param method
	 * @param resursive the resursive
	 */
	public void changeProgramInterestDate(ProgramInterestCoupon programInterestCoupon,
										  String paramMethod,boolean resursive){
		try {
			Date nextYear=null;
			Date septemberLeap=null;
			
			if(paramMethod.equals("E")){
				programInterestCoupon.setPaymentDate( generalParameterFacade.workingDateCalculateServiceFacade(programInterestCoupon.getExperitationDate(), 
						   GeneralConstants.ONE_VALUE_INTEGER) );
			}else if(paramMethod.equals("P")){
				programInterestCoupon.setExperitationDate( generalParameterFacade.workingDateCalculateServiceFacade(programInterestCoupon.getPaymentDate(), 
						   GeneralConstants.ONE_VALUE_INTEGER * -1) );
			}
			programInterestCoupon.setExchangeDateRate(programInterestCoupon.getPaymentDate());
			Integer paymentDays=null;
			if(security.isExtendedTerm()){
				paymentDays=CommonsUtilities.getDaysBetween(programInterestCoupon.getBeginingDate(),programInterestCoupon.getPaymentDate());
			}else{
				paymentDays=CommonsUtilities.getDaysBetween(programInterestCoupon.getBeginingDate(),programInterestCoupon.getExperitationDate());
			}
			programInterestCoupon.setPaymentDays(paymentDays);
			
			programInterestCoupon.setInterestRate(securitiesUtils.interestRateCalculate(security, 
								security.getPeriodicity(), 
								programInterestCoupon.getInterestFactor(), 
								programInterestCoupon.getBeginingDate(), 
								programInterestCoupon.getPaymentDays(),
								programInterestCoupon.getCouponNumber(),
								septemberLeap,
								nextYear));
						
			programInterestCoupon.setRegistryDate( CommonsUtilities.addDaysToDate(programInterestCoupon.getExperitationDate(), 
					security.getStockRegistryDays()*-1) );
			programInterestCoupon.setRegistryDate( generalParameterFacade.workingDateCalculateServiceFacade(programInterestCoupon.getRegistryDate(), 
					-1) );	
			
			programInterestCoupon.setCutoffDate( programInterestCoupon.getRegistryDate() );
			
			if(resursive){
				int ind=security.getInterestPaymentSchedule().getProgramInterestCoupons().indexOf(programInterestCoupon);
				ind++;
				if(security.getInterestPaymentSchedule().getProgramInterestCoupons().get(ind)!=null){
					Date expirationDate=programInterestCoupon.getExperitationDate();
					programInterestCoupon=security.getInterestPaymentSchedule().getProgramInterestCoupons().get(ind);
					programInterestCoupon.setBeginingDate(expirationDate);
					changeProgramInterestDate(programInterestCoupon,paramMethod,false);
				}
			}
			if(!resursive){
				securitiesUtils.calculateCouponAmountProgramInterest(security);
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change program amortization date.
	 *
	 * @param programAmortizationCoupon the program amortization coupon
	 * @param paramMethod the param method
	 * @param resursive the resursive
	 */
	public void changeProgramAmortizationDate(ProgramAmortizationCoupon programAmortizationCoupon,
											  String paramMethod,
											  boolean resursive){
		try {
	
			if(paramMethod.equals("E")){
				programAmortizationCoupon.setPaymentDate( generalParameterFacade.workingDateCalculateServiceFacade(programAmortizationCoupon.getExpirationDate(), 
						   GeneralConstants.ONE_VALUE_INTEGER) );
			}else if(paramMethod.equals("P")){
				programAmortizationCoupon.setExpirationDate( generalParameterFacade.workingDateCalculateServiceFacade(programAmortizationCoupon.getPaymentDate(), 
						   GeneralConstants.ONE_VALUE_INTEGER * -1) );
			}
			programAmortizationCoupon.setExchangeDateRate(programAmortizationCoupon.getPaymentDate());
			programAmortizationCoupon.setRegistryDate( CommonsUtilities.addDaysToDate(programAmortizationCoupon.getExpirationDate(), 
			security.getStockRegistryDays()*-1) );
			programAmortizationCoupon.setRegistryDate(generalParameterFacade.workingDateCalculateServiceFacade(programAmortizationCoupon.getRegistryDate(), -1));
			//CutOffDate
			programAmortizationCoupon.setCutoffDate( programAmortizationCoupon.getRegistryDate() ); 
			int paymentDaysToAmortizacion=0;
			if(security.isExtendedTerm()){
				paymentDaysToAmortizacion=CommonsUtilities.getDaysBetween(programAmortizationCoupon.getBeginingDate(),programAmortizationCoupon.getPaymentDate());
			}else{
				paymentDaysToAmortizacion=CommonsUtilities.getDaysBetween(programAmortizationCoupon.getBeginingDate(),programAmortizationCoupon.getExpirationDate());
			}
			programAmortizationCoupon.setPaymentAmortizationDays(paymentDaysToAmortizacion);
			if(resursive){
				int ind=security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().indexOf(programAmortizationCoupon);
				ind++;
				if(security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().get(ind)!=null){
					Date expirationDate=programAmortizationCoupon.getExpirationDate();
					programAmortizationCoupon=security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().get(ind);
					programAmortizationCoupon.setBeginingDate(expirationDate);
					changeProgramAmortizationDate(programAmortizationCoupon,paramMethod,false);
				}
			}
			
//			int paymentDaysToAmortizacion=0;
//			if(security.isExtendedTerm()){
//				paymentDaysToAmortizacion=CommonsUtilities.getDaysBetween(programAmortizationCoupon.getBeginingDate(),programAmortizationCoupon.getPaymentDate());
//			}else{
//				paymentDaysToAmortizacion=CommonsUtilities.getDaysBetween(programAmortizationCoupon.getBeginingDate(),programAmortizationCoupon.getExpirationDate());
//			}
//			programAmortizationCoupon.setPaymentAmortizationDays(paymentDaysToAmortizacion);
			
			if(!resursive){
				securitiesUtils.calculateCouponAmountProgramInterest(security);
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	
	/**
	 * Change interest rate.
	 *
	 * @param programInterestCoupon the program interest coupon
	 */
	public void changeInterestRate(ProgramInterestCoupon programInterestCoupon){
		try {
			Date nextYear=null;
			Date septemberLeap=null;
			if (Validations.validateIsNull(programInterestCoupon.getInterestFactor())) {
				JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_WARNING_ACTION, null, 
										 PropertiesConstants.SECURITY_ERROR_INTEREST_RATE_REQUIRED, null);
				JSFUtilities.showSimpleValidationDialog();	
				return;
			}
			programInterestCoupon.setInterestRate(securitiesUtils.interestRateCalculate(security, 
					security.getPeriodicity(), 
					programInterestCoupon.getInterestFactor(), 
					programInterestCoupon.getBeginingDate(), 
					programInterestCoupon.getPaymentDays(),
					programInterestCoupon.getCouponNumber(),
					septemberLeap,
					nextYear));
			
			securitiesUtils.calculateCouponAmountProgramInterest(security);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	/**
	//metodo revertido salida a produccion issues: 100,148,143
	public void changeCouponAmount(ProgramInterestCoupon programCouponAmount){
		try {
			if(programCouponAmount.getCouponAmount().equals(BigDecimal.ZERO)){
				JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_WARNING_ACTION, null, 
							PropertiesConstants.PROGRAM_INTEREST_COUPON_REQUIRED, null);
				JSFUtilities.showSimpleValidationDialog();	
				programCouponAmount.getCouponAmount();
				return;
			}	
			programCouponAmount.setCouponAmount(programCouponAmount.getCouponAmount());	
			securitiesUtils.calculateCouponAmountProgramInterest(security);
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	*/
	
	/**
	 * Recalculate interest amount.
	 */
	public void recalculateInterestAmount(){
		if(!security.isIndistinctAmortizationPeriodicity()){
			securitiesUtils.calculateCouponAmountProgramInterest(security);
		}
	}
		
	/**
	 * Change amortization factor.
	 *
	 * @param programAmortizationCoupon the program amortization coupon
	 */
	public void changeAmortizationFactor(ProgramAmortizationCoupon programAmortizationCoupon){
		try {
			if (programAmortizationCoupon.getAmortizationFactor()==null) {
				JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_WARNING_ACTION, null, 
										 PropertiesConstants.SECURITY_ERROR_AMORTIZATION_FACTOR_REQUIRED, null);
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			if (programAmortizationCoupon.getAmortizationFactor().equals(BigDecimal.ZERO)) {
				JSFUtilities.showMessageOnDialog(PropertiesConstants.LBL_WARNING_ACTION, null, 
										 PropertiesConstants.SECURITY_ERROR_AMORTIZATION_ZERO, null);
				JSFUtilities.showSimpleValidationDialog();	
				programAmortizationCoupon.setAmortizationFactor(security.getAmortizationPaymentSchedule().getAmortizationFactor());
				return;
			}
			programAmortizationCoupon.setCouponAmount(security.getInitialNominalValue().multiply(programAmortizationCoupon.getAmortizationFactor()).divide(BigDecimal.valueOf(100)  , MathContext.DECIMAL128));

			if(security.getAmortizationOn().equals( AmortizationOnType.CAPITAL.getCode() )){						
				programAmortizationCoupon.setAmortizationAmount( 
						security.getCurrentNominalValue().multiply( programAmortizationCoupon.getAmortizationFactor() ).divide(BigDecimal.valueOf(100.0)) );
				
			}else if(security.getAmortizationOn().equals( AmortizationOnType.NOMINAL_VALUE.getCode() )){
				programAmortizationCoupon.setAmortizationAmount( 
						security.getInitialNominalValue().multiply( programAmortizationCoupon.getAmortizationFactor() ).divide(BigDecimal.valueOf(100.0)) );
			}
			security.getAmortizationPaymentSchedule().calculateTotalFactorAndAmount();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Loade security quote list.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadeSecurityQuoteList() throws ServiceException{
			List<SecurityQuoteTO> securityQuoteList = null;
			SecurityQuoteTO quoteTO = new SecurityQuoteTO();
			quoteTO.setSecurity(security);

			securityQuoteList = securityQuoteFacade.getSecurityQuoteListFacade(quoteTO);
//			for(SecurityQuoteTO secQuoTo : securityQuoteList){
//				secQuoTo.getSecurity().setDescription(securitieDescription.get(secQuoTo.getSecurity().getIdIsinCode()));
//				secQuoTo.setQuoteStateDescription(SecurityQuoteStateType.get(secQuoTo.getQuoteState()).getValue());
//				secQuoTo.setRequestTypeDescription(requestTypeDescription.get(secQuoTo.getRequestType()));
//			}

			this.securityQuoteList = new GenericDataModel<SecurityQuoteTO>(securityQuoteList);
	}
	
	
//	public void showIsinDetail(String isinCode){
//		securityHelpBean.setIsinCode(isinCode);
//		securityHelpBean.setName("securityHelp");
//		securityHelpBean.searchSecurityHandler();
//	}
	
	/*Load Controls Start*/
	
	/**
	 * Load cbo cfi categories.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboCFICategories() throws ServiceException{
		lstCboCFICategories=securityServiceFacade.getCFICategoriesServiceFacade();
	}
	
	/**
	 * Load cbo cfi groups by category.
	 *
	 */
	public void loadCboCFIGroupsByCategoryHandler() {
		try {
			lstCboCFIGroups=securityServiceFacade.getCFIGroupServiceFacade(cfiCategory.getIdCategoryPk());
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Load cbo cfi attributes handler.
	 */
	public void loadCboCFIAttributesHandler(){
		try {
			lstCboCFIAttribute1=securityServiceFacade.getCFIAttributesServiceFacade(cfiCategory.getIdCategoryPk(), 
					cfiGroup.getId().getIdGroupPk(), CfiParameterType.ATTRIBUTE1.getCodeCd());
			lstCboCFIAttribute2=securityServiceFacade.getCFIAttributesServiceFacade(cfiCategory.getIdCategoryPk(), 
					cfiGroup.getId().getIdGroupPk(), CfiParameterType.ATTRIBUTE2.getCodeCd());
			lstCboCFIAttribute3=securityServiceFacade.getCFIAttributesServiceFacade(cfiCategory.getIdCategoryPk(), 
					cfiGroup.getId().getIdGroupPk(), CfiParameterType.ATTRIBUTE3.getCodeCd());
			lstCboCFIAttribute4=securityServiceFacade.getCFIAttributesServiceFacade(cfiCategory.getIdCategoryPk(), 
					cfiGroup.getId().getIdGroupPk(), CfiParameterType.ATTRIBUTE4.getCodeCd());
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Load cbo instrument type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCboInstrumentType() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
		lstCboInstrumentType=generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}
	
	/**
	 * Load cbo security type by instrument type.
	 *
	 * @param instrument the instrument
	 * @throws ServiceException the service exception
	 */
	private void loadCboSecurityTypeByInstrumentType(Integer instrument) throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk( MasterTableType.SECURITIES_TYPE.getCode() );
		parameterTableTO.setIdRelatedParameterFk( instrument );
		lstCboSecuritieType=generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}
	
	/**
	 * Load cbo securitie term.
	 *
	 * @throws ServiceException the service exception
	 */
	public void  loadCboSecuritieTerm() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk(MasterTableType.SECURITIE_TERM.getCode());
		lstCboSecuritieTerm=generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		billParametersTableMap(lstCboSecuritieTerm, true);
	}
	
	/**
	 * Load cbo issuance state type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCboIssuanceStateType() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk(MasterTableType.ISSUANCE_STATE.getCode());
		lstCboIssuanceState=generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}
	
	/**
	 * Load cbo convertible stock type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCboConvertibleStockType() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk(MasterTableType.CONVERTIBLE_STOCK_TYPE.getCode());
		lstCboConvertibleStockType=generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}
	
	
	/**
	 * Load cbo geographic location.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboGeographicLocation() throws ServiceException{
		GeographicLocationTO geoLocation=new GeographicLocationTO();
		geoLocation.setGeographicLocationType(GeographicLocationType.COUNTRY.getCode());
		geoLocation.setState(GeographicLocationStateType.REGISTERED.getCode());
		lstCboGeographicLocation=generalParameterFacade.getListGeographicLocationServiceFacade(geoLocation);
	}
	
	/**
	 * Load cbo geographic location.
	 *
	 * @throws ServiceException the service exception
	 */
	public void getLstDepartments() throws ServiceException{
		GeographicLocationTO geoLocation=new GeographicLocationTO();
		geoLocation.setGeographicLocationType(GeographicLocationType.DEPARTMENT.getCode());
		geoLocation.setState(GeographicLocationStateType.REGISTERED.getCode());
		geoLocation.setIdLocationReferenceFk(countryResidence);
		
		lstCboGeographicLocationDep = generalParameterFacade.getListGeographicLocationServiceFacade(geoLocation);
	}
	
	
	/**
	 * Load cbo currency.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboCurrency() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		
		lstCboCurrency=generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}
	
	/**
	 * Load cbo interest type.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboInterestType() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.INTEREST_TYPE.getCode() );
		
		lstCboInterestType=generalParameterFacade.getListParameterTableServiceBean( parameterTableTO );
	}
	
	public void loadCboEncoderAgent() throws ServiceException {
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.ENCODER_AGENT.getCode() );
		
		lstCboEncoderAgent=generalParameterFacade.getListParameterTableServiceBean( parameterTableTO );
	}
	
	/**
	 * Load cbo issue type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCboIssuanceType() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.ISSUANCE_TYPE.getCode() );
		lstCboIssuanceType=generalParameterFacade.getListParameterTableServiceBean( parameterTableTO );
	}
	
	/**
	 * Load cbo security block motive.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboSecurityBlockMotive() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.SECURITY_BLOCK_MOTIVE.getCode() );
		parameterTableTO.setOrderbyParameterName(BooleanType.YES.getCode());
		lstCboSecurityBlockMotive=generalParameterFacade.getComboParameterTable( parameterTableTO );
	}
	
	/**
	 * Load cbo security unblock motive.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboSecurityUnblockMotive() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.SECURITY_UNBLOCK_MOTIVE.getCode() );
		parameterTableTO.setOrderbyParameterName(BooleanType.YES.getCode());
		lstCboSecurityUnblockMotive=generalParameterFacade.getComboParameterTable( parameterTableTO );
	}
	
	/**
	 * Load cbo boolean type.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboBooleanType() throws ServiceException{
		lstCboBooleanType=BooleanType.list;
	}
	
	/**
	 * Load cbo rate type.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboRateType() throws ServiceException{
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.RATE_TYPE.getCode() );
		parameterTableTO.setOrderbyParameterTableCd( BooleanType.YES.getCode() );
		lstCboRateType=generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}
	
	/**
	 * Load cbo interest periodicity.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboInterestPeriodicity() throws ServiceException{
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.INTEREST_PERIODICITY.getCode() );
		parameterTableTO.setOrderbyParameterTableCd( BooleanType.YES.getCode() );
		lstCboInterestPeriodicity=generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}
	
	/**
	 * Load cbo payment modality.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboPaymentModality() throws ServiceException{
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.PAYMENT_MODALITY.getCode() );
		lstCboPaymentModality=generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}
	
	/**
	 * Load cbo interest payment modality.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboInterestPaymentModality() throws ServiceException{
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.INTEREST_PAYMENT_MODALITY.getCode() );
		lstCboInterestPaymentModality=generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}
	
	/**
	 * Load cbo operation type.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboOperationType() throws ServiceException{
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.OPERATION_TYPE.getCode() );
		lstCboOperationType=generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);		
	}
	
	/**
	 * Load cbo calendar type.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboCalendarType() throws ServiceException{
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.CALENDAR_TYPE.getCode() );
		lstCboCalendarType=generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);	
	}
	
	/**
	 * Load cbo calendar days.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboCalendarDays() throws ServiceException{
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.CALENDAR_DAYS.getCode() );
		lstCboCalendarDays=generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);		
	}
	
	/**
	 * Load cbo yield.
	 *
	 * @throws ServiceException the service exception
	 */
	public  void loadCboYield() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.SECURITY_YIELD_TYPE.getCode() );
		lstCboYield=generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}
	
	/**
	 * Load cbo cash nominal.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboCashNominal() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.CASH_NOMINAL.getCode() );
		lstCboCashNominal=generalParameterFacade.getListParameterTableServiceBean( parameterTableTO );
	}
	
	/**
	 * Load cbo amortization type.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboAmortizationType() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.AMORTIZATION_TYPE.getCode() );
		lstCboAmortizationType=generalParameterFacade.getListParameterTableServiceBean( parameterTableTO );
	}
	
	/**
	 * Load cbo amortization on.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboAmortizationOn() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.AMORTIZATION_ON.getCode() );
		lstCboAmortizationOn=generalParameterFacade.getListParameterTableServiceBean( parameterTableTO );
	}
	
	/**
	 * Load cbo indexed to.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboIndexedTo() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.CURRENCY.getCode() );		
		lstCboIndexedTo=generalParameterFacade.getListParameterTableServiceBean( parameterTableTO );
	}
	
	/**
	 * Load cbo month type.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboMonthType() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.MONTH_TYPE.getCode() );
		lstCboCalendarMonth=generalParameterFacade.getListParameterTableServiceBean( parameterTableTO );
	}
	
	/**
	 * Load cbo financial index.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboFinancialIndex() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.FINANCIAL_INDEX.getCode() );
		lstCboFinancialIndex=generalParameterFacade.getListParameterTableServiceBean( parameterTableTO );
	}

	/**
	 * Load cbo filter securitie class.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboFilterSecuritieClass() throws ServiceException {
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.SECURITIES_CLASS.getCode() );
		lstCboFilterSecuritieClass=generalParameterFacade.getListParameterTableServiceBean( parameterTableTO );
		billParametersTableMap(lstCboFilterSecuritieClass,true);
	}
	
	/**
	 * Load cbo securitie class target.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboSecuritieClassTarget() throws ServiceException {
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		
		parameterTableTO.setLstParameterTablePk( new ArrayList<Integer>() );
		parameterTableTO.getLstParameterTablePk().add( SecurityClassType.ACC.getCode() );
		parameterTableTO.getLstParameterTablePk().add( SecurityClassType.ACC_RF.getCode());
		
		lstCboSecuritieClassTrg=generalParameterFacade.getListParameterTableServiceBean( parameterTableTO );
	}	
	/**
	 * Load cbo filter curency.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboFilterCurency() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.CURRENCY.getCode() );
		lstCboFilterCurrency=generalParameterFacade.getListParameterTableServiceBean( parameterTableTO );
		billParametersTableMap(lstCboFilterCurrency, true);
	}
	
	/**
	 * Load dtb international depository.
	 *
	 * @throws ServiceException the service exception
	 * @throws Exception the exception
	 */
	public void loadDtbInternationalDepository() throws ServiceException, Exception{
		 InternationalDepositoryTO filterInternationalDepository = new InternationalDepositoryTO();		
		filterInternationalDepository.setStateIntDepository(Integer.valueOf(InternationalDepositoryStateType.ACTIVE.getCode()));
		lstDtbInternationalDepository=securityServiceFacade.getListInternationalDepositoryServiceFacade(filterInternationalDepository);
		if(isViewOperationModify() || isViewOperationDetail()){
			selectCheckboxIntDepository();
		}
	}
	
	/**
	 * Load dtb negotiation modality.
	 *
	 * @param instrumentType the instrument type
	 * @throws ServiceException the service exception
	 */
	public void loadDtbNegotiationModality(Integer instrumentType) throws ServiceException{		
		lstDtbNegotiationModalities=securityServiceFacade.getListNegotiationModalitiesServiceFacade(NegotiationModalityStateType.ACTIVE.getCode(), 
				MechanismModalityStateType.ACTIVE.getCode(),instrumentType);
		
		List<NegotiationMechanism>lstNegotiationMechanismData=securityServiceFacade.getListMechanismServiceFacade(NegotiationMechanismStateType.ACTIVE.getCode());
		createMechanismDynamicColumns(lstNegotiationMechanismData);
		
		for(NegotiationModality mod : lstDtbNegotiationModalities){
			List<NegotiationMechanism> lstNegotiationMechanism=new ArrayList<NegotiationMechanism>();
			for(NegotiationMechanism negMechanismType : lstNegotiationMechanismData){
				lstNegotiationMechanism.add(new NegotiationMechanism(negMechanismType.getIdNegotiationMechanismPk(),negMechanismType.getMechanismName(),false,true));
			}
			mod.setNegotiationMechanisms(lstNegotiationMechanism);
			int index=0;
			for(NegotiationMechanism negMec : mod.getNegotiationMechanisms()){
				index++;
				for(MechanismModality mecModality : mod.getMechanismModalities()){
					if(negMec.getIdNegotiationMechanismPk().equals( mecModality.getId().getIdNegotiationMechanismPk() )){
						if(mecModality.getId().getIdNegotiationModalityPk().equals( NegotiationModalityType.PRIMARY_PLACEMENT_EQUITIES.getCode() )){
							/**Issue 2433*/
							if(security.isVariableInstrumentType() && security.isMixedIssuanceForm() &&
									!(SecurityClassType.ACC.getCode().equals(security.getSecurityClass())||
											SecurityClassType.ACC_RF.getCode().equals(security.getSecurityClass())||
											(SecurityClassType.ACP.getCode().equals(security.getSecurityClass()))||
											SecurityClassType.CFC.getCode().equals(security.getSecurityClass()))){
								continue;
							}
						}
						if(mecModality.getId().getIdNegotiationModalityPk().equals( NegotiationModalityType.PRIMARY_PLACEMENT_FIXED_INCOME.getCode() )){
							/**Issue 2433*/
							if(security.isMixedIssuanceForm() || security.isPhysicalIssuanceForm()&&
									//Acciones Preferentes
									(SecurityClassType.ACC_RF.getCode().equals(security.getSecurityClass()))
									){
								continue;
							}
						}
						if(mecModality.getId().getIdNegotiationModalityPk().equals( NegotiationModalityType.COUPON_SPLIT_FIXED_INCOME.getCode() )){
							if(security.isFixedInstrumentTypeAcc() && (BooleanType.NO.getCode().equals(security.getIndSplitCoupon()))){
								continue;
							}
						}
						negMec.setDisabled(false);
						if(isViewOperationRegister()) {
							if(security.getIssuance().getSecurityType().equals( SecurityType.PRIVATE.getCode() )) {
								if(negMec.getIdNegotiationMechanismPk().equals(NegotiationMechanismType.OTC.getCode()) ||
									negMec.getIdNegotiationMechanismPk().equals(NegotiationMechanismType.BOLSA.getCode())){
									negMec.setSelected(true);
									mechanismColumnsModel.get(index-1).setHeaderValueBol(true);
								}
							}
							else if(security.getIssuance().getSecurityType().equals( SecurityType.PUBLIC.getCode() )){
									 if(negMec.getIdNegotiationMechanismPk().equals(NegotiationMechanismType.OTC.getCode()) ||
										 negMec.getIdNegotiationMechanismPk().equals(NegotiationMechanismType.BOLSA.getCode()) ||
										  negMec.getIdNegotiationMechanismPk().equals(NegotiationMechanismType.SIRTEX.getCode())){
										negMec.setSelected(true);
										mechanismColumnsModel.get(index-1).setHeaderValueBol(true);
									 }
								}
						}
					}
				}
			}
		}
		
		if(isViewOperationModify() || isViewOperationDetail()){
			StringBuilder sbIdConverted=new StringBuilder();
			for(NegotiationModality negModality : lstDtbNegotiationModalities){
				for(NegotiationMechanism negMec : negModality.getNegotiationMechanisms()){
					sbIdConverted.delete(0, sbIdConverted.length());
					sbIdConverted.append(negMec.getIdNegotiationMechanismPk()).append("-").append(negModality.getIdNegotiationModalityPk());
					if(isAssignedSecNegMechanism(sbIdConverted.toString())){
						negMec.setSelected(true);
					}
				}
			}
		}
	}
	
	/**
	 * Creates the mechanism dynamic columns.
	 *
	 * @param lstNegotiationMechanism the lst negotiation mechanism
	 */
	public void createMechanismDynamicColumns(List<NegotiationMechanism> lstNegotiationMechanism){
		mechanismColumnsModel=new ArrayList<GenericColumnModel>();
		for(NegotiationMechanism negMec : lstNegotiationMechanism){
			mechanismColumnsModel.add(new GenericColumnModel(negMec.getDescription(), null,"chk"+negMec.getMechanismName().trim(),false));
		}
		
	}
	
	/**
	 * Checks if is assigned sec neg mechanism.
	 *
	 * @param idConverted the id converted
	 * @return true, if is assigned sec neg mechanism
	 */
	public boolean isAssignedSecNegMechanism(String idConverted){
		for(SecurityNegotiationMechanism secNeg : security.getSecurityNegotiationMechanisms()){
			if(idConverted.equals( secNeg.getMechanismModality().getIdConverted() )){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Load cbo securities state.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboSecuritiesState() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.SECURITIES_STATE.getCode() );
		lstCboSecurityState=generalParameterFacade.getListParameterTableServiceBean( parameterTableTO );
		billParametersTableMap(lstCboSecurityState);
	}
    
	/**
	 * Load cbo securities register state.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboSecuritiesRegisterState() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.SECURITIES_STATE.getCode() );
		parameterTableTO.setParameterTablePk( SecurityStateType.REGISTERED.getCode() );
		lstCboSecurityOnlyRegisterState=generalParameterFacade.getListParameterTableServiceBean( parameterTableTO );
		billParametersTableMap(lstCboSecurityOnlyRegisterState);
	}
	
	/**
	 * Load cbo payment schedule state.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboPaymentScheduleState() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.PROGRAM_SCHEDULE_STATE.getCode() );
		List<ParameterTable> lst=generalParameterFacade.getListParameterTableServiceBean( parameterTableTO );
		
		billParametersTableMap(lst);
	}
	
	/**
	 * Load cbo payment schedule situation.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboPaymentScheduleSituation() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.COUPON_SITUATION.getCode() );
		List<ParameterTable> lst=generalParameterFacade.getListParameterTableServiceBean( parameterTableTO );
		
		billParametersTableMap(lst);
	}
	
	/**
	 * Load cbo credit rating scales.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboCreditRatingScales() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
//		parameterTableTO.setMasterTableFk(MasterTableType.CREDIT_RATING_SCALES_LONG_TERM.getCode());
		parameterTableTO.setLstMasterTableFk(Arrays.asList(MasterTableType.CREDIT_RATING_SCALES_SHORT_TERM.getCode(),
														   MasterTableType.CREDIT_RATING_SCALES_LONG_TERM.getCode(),
														   MasterTableType.CREDIT_RATING_SCALES_EQUITIES.getCode()));
		
		lstCboCreditRatingScales=generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}
	
	/**
	 * Load cbo economic sectors.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCboEconomicSectors() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.ECONOMIC_SECTOR.getCode());
		lstCboEconomicSector = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
	}
	
	/**
	 * Load cbo economic activity.
	 *
	 * @param economicSector the economic sector
	 * @throws ServiceException the service exception
	 */
	 public void loadCboEconomicActivity(Integer economicSector) throws ServiceException{
		  lstCboEconomicActivity=generalParameterFacade.getListEconomicActivityBySector(economicSector);
	 }
	 
	/**
	 * Load cbo financial periodicity.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboFinancialPeriodicity() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.FINANCIAL_PERIOD.getCode());
		lstCboFinancialPeriodicity= generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);		
	}
	//#CORDINAR CON WAKATA
	/**
	 * Load cbo issuer document type.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboIssuerDocumentType() throws ServiceException{
		PersonTO personTO=new PersonTO();
		personTO.setPersonType( PersonType.JURIDIC.getCode() );
		personTO.setFlagIssuerIndicator( BooleanType.YES.getBooleanValue() );
		lstCboIssuerDocumentType=documentValidator.getDocumentTypeResult(personTO);
	}
	
	/*Load Controls End*/
	
	/**
	 * Prints the security class.
	 *
	 * @param securityClass the security class
	 * @return the string
	 */
	public String printSecurityClass(Integer securityClass){
		ParameterTable pTable=(ParameterTable) super.getParametersTableMap().get(securityClass);
		return pTable.getText1() + " - "+pTable.getParameterName();
	}
	
	
	/**
 * Validate month term.
 *
 * @param context the context
 * @param component the component
 * @param value the value
 * @throws ValidatorException the validator exception
 */
public void validateMonthTerm(FacesContext context, UIComponent component, Object value) throws ValidatorException {	
		Integer monthsTerms=(Integer)value;
		if(value!=null && !monthsTerms.equals(GeneralConstants.ZERO_VALUE_INTEGER)){
			Integer result=monthsTerms % Integer.valueOf(12);
			if(result.compareTo(Integer.valueOf(0))!=0){
				String strMsg =  PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_ERROR_MONTH_TERM, null);
				FacesMessage msg = new FacesMessage(strMsg);
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				security.setSecurityMonthsTerm(null);
				throw new ValidatorException(msg);
			}
		}
 	}
	
	/**
	 * Select expired date handler.
	 */
	public void selectExpiredDateHandler(){
		try {
			if(!isViewOperationModify()){
				cleanDataDependExpirationDate();
			}
			Integer months=null;
			Integer days=null;
			
			if(security.getIssuanceDate()!=null && security.getExpirationDate()!=null){
				months=CommonsUtilities.getMonthsBetween(security.getIssuanceDate(), security.getExpirationDate());
				days=CommonsUtilities.getDaysBetween(security.getIssuanceDate(), security.getExpirationDate());
			}
			
			security.setSecurityMonthsTerm( months );
			security.setSecurityDaysTerm( days );
			
			processSecuityTerm();
			calculateNroCouponsAndFirstPayment();
			
			changeCboInterestTypeHandler();
			calculateAmortFactorAndNumberCoupons();
			
			if(Validations.validateIsNotNullAndPositive( security.getInstrumentType() )){
				if(security.getIssuanceDate()!=null && security.getExpirationDate()!=null){
					boolean haveNit = false;
					security.setIndTaxExempt(BooleanType.NO.getCode());
				}
			}
		} catch (Exception e) {
			excepcion.fire( new ExceptionToCatchEvent( e ) );
		}
	}
	
	/**
	 * Clean data depend expiration date.
	 */
	public void cleanDataDependExpirationDate(){
		/*Interest Data*/
		security.setInterestPaymentModality(null);
		security.setPeriodicity(null);
		security.setPeriodicityDays(null);
		security.setCouponFirstExpirationDate(null);
		
		JSFUtilities.resetComponent(SecurityMgmtType.CBO_INT_PAYMENT_MODE.getValue());
		JSFUtilities.resetComponent(SecurityMgmtType.CBO_INTEREST_PERIODICITY.getValue());
		JSFUtilities.resetComponent( SecurityMgmtType.TXT_PERIODICITY_DAYS.getValue());
		JSFUtilities.resetComponent( SecurityMgmtType.CAL_FIRST_PAY_COUPON.getValue());
		
		/* Amortization Data */
		security.setCapitalPaymentModality(null);
		security.setAmortizationType(null);
		security.setAmortizationPeriodicity(null);
		security.setAmortizationPeriodicityDays(null);
		security.setPaymentFirstExpirationDate(null);
		setPaymentNumberAmortization(null);
		security.setAmortizationFactor(null);
		
		
		JSFUtilities.resetComponent(SecurityMgmtType.CBO_AMORT_PAYMENT_MODE.getValue());
		JSFUtilities.resetComponent(SecurityMgmtType.CBO_AMORTIZATION_TYPE.getValue());
		JSFUtilities.resetComponent(SecurityMgmtType.CBO_PER_AMORTIZATION.getValue());
		JSFUtilities.resetComponent(SecurityMgmtType.TXT_AMORT_PERIODICITY_DAYS.getValue());
		JSFUtilities.resetComponent(SecurityMgmtType.CAL_FIRST_EXP_AMO_COUPON.getValue());
		JSFUtilities.resetComponent(SecurityMgmtType.TXT_PAYMENTS_NUMBER.getValue());
		JSFUtilities.resetComponent(SecurityMgmtType.TXT_AMORTIZATION_FACTOR.getValue());
		
	}
	

	
	/**
	 * Select coupon first expired date.
	 */
	public void selectCouponFirstExpiredDate(){
		try {
			Date beginDateSecudondCoupon=security.getCouponFirstExpirationDate();
			int remaindsDaysTerm=CommonsUtilities.getDaysBetween(beginDateSecudondCoupon, security.getExpirationDate());
			int remaindsMonthsTerm=CommonsUtilities.getMonthsBetween(beginDateSecudondCoupon, security.getExpirationDate());
			
			BigDecimal coupons = getNroCouponsForInterest(remaindsMonthsTerm,remaindsDaysTerm);
			if(coupons!=null){
				security.setNumberCoupons(coupons.intValue()+1);
			}
		} catch (Exception e) {
			excepcion.fire( new ExceptionToCatchEvent( e ) );
		}
	}
	
	/**
	 * Select payment first expired date.
	 */
	public void selectPaymentFirstExpiredDate(){
		try {
			Date beginDateSecondCoupon=security.getPaymentFirstExpirationDate();
			int remaindsDaysTerm=CommonsUtilities.getDaysBetween(beginDateSecondCoupon, security.getExpirationDate());
			int remaindsMonthsTerm=CommonsUtilities.getMonthsBetween(beginDateSecondCoupon, security.getExpirationDate());
			
			BigDecimal coupons = getNroCouponsForAmortization(remaindsMonthsTerm,remaindsDaysTerm);
			
			if(coupons!=null){
				coupons=coupons.add(BigDecimal.ONE);
				setPaymentNumberAmortization(coupons.intValue());
				
				if(security.isHavePaymentScheduleNoDesm() && security.isProportionalAmortizationType() && security.isPartialAmortPaymentModality()){
					coupons=BigDecimal.valueOf(getNroCouponsAmortMixedIssueFixedInst(coupons.intValue()));
				}
				BigDecimal facAmort = null;				
				BigDecimal actualPercent = new BigDecimal("100");
				
				facAmort = actualPercent.divide(coupons,MathContext.DECIMAL128);
				facAmort=CommonsUtilities.roundRateFactor(facAmort);
				security.setAmortizationFactor(facAmort);
			}
		} catch (Exception e) {
			excepcion.fire( new ExceptionToCatchEvent( e ) );
		}
	}
	
	/**
	 * Calculate amort factor and number coupons.
	 */
	public void calculateAmortFactorAndNumberCoupons(){
		try {
			if(security.isAtMaturityAmortPaymentModality()){
				setPaymentNumberAmortization(GeneralConstants.ONE_VALUE_INTEGER);
			}else if(security.isPartialAmortPaymentModality()){
				if(security.isByDaysAmortizationPeriodicity()){
					if(security.getAmortizationPeriodicityDays()!=null && security.getAmortizationPeriodicityDays()>0){
						security.setPaymentFirstExpirationDate(CommonsUtilities.addDaysToDate(security.getIssuanceDate(), 
								security.getAmortizationPeriodicityDays()));
					}
				}else{
					if(security.getAmortizationPeriodicity()!=null){
						security.setPaymentFirstExpirationDate( CommonsUtilities.addMonthsToDate(security.getIssuanceDate(), 
								   InterestPeriodicityType.get(security.getAmortizationPeriodicity()).getIndicator1()) );
					}
					if(security.isIndistinctAmortizationPeriodicity()){
						security.setPaymentFirstExpirationDate(null);
					}
				}
			}
			
			Date beginDateSecondCoupon=security.getPaymentFirstExpirationDate();
			int remaindsDaysTerm=CommonsUtilities.getDaysBetween(beginDateSecondCoupon, security.getExpirationDate());
			int remaindsMonthsTerm=CommonsUtilities.getMonthsBetween(beginDateSecondCoupon, security.getExpirationDate());
			
			BigDecimal coupons = getNroCouponsForAmortization(remaindsMonthsTerm,remaindsDaysTerm);
			
			if(coupons!=null){
				coupons=coupons.add(BigDecimal.ONE);
				setPaymentNumberAmortization(coupons.intValue());
				
				if(security.isHavePaymentScheduleNoDesm() && security.isProportionalAmortizationType() && security.isPartialAmortPaymentModality()){
					coupons=BigDecimal.valueOf(getNroCouponsAmortMixedIssueFixedInst(coupons.intValue()));
				}
				BigDecimal facAmort = null;				
				BigDecimal actualPercent = new BigDecimal("100");
				
				facAmort = actualPercent.divide(coupons,MathContext.DECIMAL128);
				facAmort=CommonsUtilities.roundRateFactor(facAmort);
				security.setAmortizationFactor(facAmort);
			}
		} catch (Exception e) {
			excepcion.fire( new ExceptionToCatchEvent( e ) );
		}
	}
	
	/**
	 * Insert payment numbers.
	 */
	public void insertPaymentNumbers(){
		try {
			if (Validations.validateIsNotNull(getPaymentNumberAmortization())) {
				BigDecimal facAmort = null;				
				BigDecimal actualPercent = new BigDecimal("100");
				BigDecimal coupons=BigDecimal.valueOf(getPaymentNumberAmortization());
				facAmort = actualPercent.divide(coupons,MathContext.DECIMAL128);
				facAmort=CommonsUtilities.roundRateFactor(facAmort);
				security.setAmortizationFactor(facAmort);
			}
		} catch (Exception e) {
			excepcion.fire( new ExceptionToCatchEvent( e ) );
		}
	}
	
	/**
	 * Process secuity term.
	 */
	public void processSecuityTerm(){
		if(security.getIssuanceDate()!=null && security.getExpirationDate()!=null){
			
			if(security.getSecurityDaysTerm()<=SecurityTermType.SHORT_TERM_INS.getMaxTermDays()){
				security.setSecurityTerm( SecurityTermType.SHORT_TERM_INS.getCode() );
			}else if(security.getSecurityDaysTerm()<=SecurityTermType.MEDIUM_TERM_INS.getMaxTermDays()){
				security.setSecurityTerm( SecurityTermType.MEDIUM_TERM_INS.getCode() );
			}else {
				security.setSecurityTerm( SecurityTermType.LONG_TERM_INS.getCode() );
			}
			/**
			 * SI ES CORTO PLAZO LOS DATOS DE INTERES Y AMORTIZACION SIEMPRE SON AL VENCIMIENTO 
			 */
			if(isViewOperationRegister()){
				security.setCapitalPaymentModality(null);
				security.setInterestPaymentModality(null);	
			}
			if(security.isShortSecurityTerm()){
			//	security.setCapitalPaymentModality(CapitalPaymentModalityType.AT_MATURITY.getCode());
			//	security.setInterestPaymentModality(InterestPaymentModalityType.AT_MATURITY.getCode());
			}
			
			chageCboAmortPaymentModalityHandler();
			changeCboInterestPaymentModalityHandler();
			
			//Raal Method
		//	forExpiration();
			/**
			 * SI ES LARGO PLAZO LA FECHA DE VENCIMIENTO NO SE DIGITA SINO SE CALCULA EN
			 * REFERENCIA AL PLAZO EN MESES EL CUAL DEBE SER DIVISIBLE EN 12 (AÃ‘O)
			 */
	//		if(SecuritieType.LONG_TERM_INS.getCode().equals(security.getSecurityType())
	//				&& IssuanceType.DEMATERIALIZED.getCode().equals(security.getIssuance().getIssuanceType())){
	//			blLongTerm = true;					
	//		}
		}
	}
	
	/**
	 * Insert months terms handler.
	 */
	public void insertMonthsTermsHandler(){
		try {
			security.setExpirationDate(null);
			if(!isViewOperationModify()){
				cleanFldsAmortization();
			}
			if(security.getIssuanceDate()!=null && security.getSecurityMonthsTerm()!=null){
				Date expirationDate = CommonsUtilities.addMonthsToDate(security.getIssuanceDate(), security.getSecurityMonthsTerm());
				
				if(expirationDate.after(security.getIssuance().getExpirationDate())){
				
					String strMsg =  PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_ERROR_MONTHS_TERM_EXCEEDS);

					JSFUtilities.addContextMessage(SecurityMgmtType.TXT_MONTHS_TERMS.getValue(), FacesMessage.SEVERITY_ERROR, strMsg, strMsg);
					security.setSecurityMonthsTerm(null);
					security.setExpirationDate(null);
				}else{
					security.setExpirationDate(expirationDate);
					calculateNroCouponsAndFirstPayment();
					isReGenerated=false;
					if(isViewOperationModify())
						changePeriodicityAmort();
				}
			}
		} catch (Exception e) {
			excepcion.fire( new ExceptionToCatchEvent( e ) );
		}
	}
	
	/**
	 * Validate term.
	 */
	public void validateTerm(){
		try {
			if(Validations.validateIsNotNullAndNotEmpty(security.getSecurityMonthsTerm())){				
				Integer monthTerm = security.getSecurityMonthsTerm();			
				BigDecimal month = new BigDecimal(monthTerm.toString());
				BigDecimal year = new BigDecimal("12");
				BigDecimal result = null;
				try {
					result = month.divide(year);
				} catch (ArithmeticException e) {
					security.setSecurityMonthsTerm(null);
					security.setExpirationDate(null);
					return;
				}
				BigDecimal decimal = result.remainder(BigDecimal.ONE);					
				if(decimal.compareTo(BigDecimal.ZERO) == 1){//si es mayor a 0
					security.setSecurityMonthsTerm(null);
					security.setExpirationDate(null);
					return;
				}else{
					if(Validations.validateIsNotNullAndNotEmpty(security.getIssuanceDate())){
						Date expirationDate = CommonsUtilities.addMonthsToDate(security.getIssuanceDate(), monthTerm);
						if(expirationDate.after(security.getIssuance().getExpirationDate())){
							security.setSecurityMonthsTerm(null);
							security.setExpirationDate(null);
						}else{
							security.setExpirationDate(expirationDate);
							getAmortizationFactor();
							calculateNroCouponsAndFirstPayment();
						}
					}else{
						security.setSecurityMonthsTerm(null);
					}
				}					
			}
		} catch (Exception e) {
			excepcion.fire( new ExceptionToCatchEvent( e ) );
		}		
	}
	
	/**
	 * Validate interest periodicity.
	 *
	 * @param context the context
	 * @param component the component
	 * @param value the value
	 * @throws ValidatorException the validator exception
	 */
	public void validateInterestPeriodicity(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		if(value!=null && security.getSecurityMonthsTerm()!=null){
			Integer periodictyId=Integer.valueOf( value.toString());
			Integer periodityMonth=InterestPeriodicityType.get(periodictyId).getIndicator1();
			Object[] argObj=new Object[2];
			if(periodityMonth.intValue()>security.getSecurityMonthsTerm().intValue()){
				try {
					argObj[0]=PropertiesUtilities.getMessage("security.lbl.interest.payment.periodicity");
					argObj[1]=PropertiesUtilities.getMessage("security.lbl.months.terms.value");
				} catch (Exception e) {
					e.printStackTrace();
				}

				String strMsg =  PropertiesUtilities.getMessage(PropertiesConstants.ERROR_LESS_THAN_OR_EQUAL_TO, argObj);
				FacesMessage msg = new FacesMessage(strMsg);
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				throw new ValidatorException(msg);
			}
		}
	}
	
	/**
	 * Validate nro coupons.
	 *
	 * @param context the context
	 * @param component the component
	 * @param value the value
	 * @throws ValidatorException the validator exception
	 */
	public void validateNroCoupons(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		if(security.getPeriodicity() != null){
			if(security.getPeriodicity().intValue() > 0){
				
			}else{
				int result=security.getPeriodicity().intValue()/security.getPeriodicity();
				if(Integer.valueOf( value.toString() ) > result){
					String strMsg =  PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_ERROR_NUMBER_COUPONS, null);
					FacesMessage msg = new FacesMessage(strMsg);
					msg.setSeverity(FacesMessage.SEVERITY_ERROR);
					throw new ValidatorException(msg);
				}
			}
			return;
		}
		if(security.getSecurityMonthsTerm() !=null && security.getSecurityMonthsTerm().intValue() > 0){
			return;
		}
		
	}

	/**
	 * Validate investor type.
	 *
	 * @param context the context
	 * @param component the component
	 * @param value the value
	 * @throws ValidatorException the validator exception
	 */
	public void validateInvestorType(FacesContext context, UIComponent component, Object value) throws ValidatorException{
		if(!security.getSecurityInvestor().isBoolIndNatural() && !security.getSecurityInvestor().isBoolIndJuridical()){
			HtmlInputText input=(HtmlInputText)component;
			Object[] parameter = {input.getLabel()};
			String strMsg =  PropertiesUtilities.getMessage(PropertiesConstants.ERROR_COMBO_REQUIRED, parameter);
			FacesMessage msg = 
					new FacesMessage(strMsg);
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				throw new ValidatorException(msg);
		}
	}
	
	/**
	 * Validate investor origin.
	 *
	 * @param context the context
	 * @param component the component
	 * @param value the value
	 * @throws ValidatorException the validator exception
	 */
	public void validateInvestorOrigin(FacesContext context, UIComponent component, Object value) throws ValidatorException{
		if(!security.getSecurityInvestor().isBoolIndLocal() && !security.getSecurityInvestor().isBoolIndForeign()){
			HtmlInputText input=(HtmlInputText)component;
			Object[] parameter = {input.getLabel()};
			String strMsg =  PropertiesUtilities.getMessage(PropertiesConstants.ERROR_COMBO_REQUIRED, parameter);
			FacesMessage msg = 
					new FacesMessage(strMsg);
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				throw new ValidatorException(msg);
		}
	}
	
	/**
	 * Validate isi ncode.
	 *
	 * @param context the context
	 * @param component the component
	 * @param value the value
	 * @throws ValidatorException the validator exception
	 * @throws Exception the exception
	 */
	public void validateISINcode(FacesContext context, UIComponent component, Object value) throws ValidatorException, Exception{
		if(value!=null && StringUtils.isNotBlank(value.toString())){
			Boolean skipValidation=Boolean.parseBoolean(JSFUtilities.getRequestParameterMap("skipValidation"));
			
			InputText input=(InputText)component;
			String strValue=value.toString().trim();
			String message=null;
			 
			if(strValue.length()!=12){
				message=PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_VALIDATION_MESSAGES, PropertiesConstants.SECURITY_ERROR_ISIN_LENGTH,null);
				
				JSFUtilities.showMessageOnDialog(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES,PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
				GeneralConstants.PROPERTY_FILE_VALIDATION_MESSAGES, PropertiesConstants.SECURITY_ERROR_ISIN_LENGTH, null);
				JSFUtilities.showSimpleValidationDialog();
				
				JSFUtilities.resetComponent(input.getClientId());
				
			//	JSFUtilities.addContextMessage(SecurityMgmtType.TXT_ID_ISIN_CODE.getValue(),FacesMessage.SEVERITY_ERROR,message,message);	
				if(skipValidation)
					JSFUtilities.showSimpleValidationDialog();
				FacesMessage msg = 
						new FacesMessage(message);
					msg.setSeverity(FacesMessage.SEVERITY_ERROR);
					throw new ValidatorException(msg);
			}
			String checkDigit=strValue.substring(11);
			String compareCheckDigit=checkDigintGenerate(strValue.substring(0, 11));
			
			if(!checkDigit.equals(compareCheckDigit)){
				message=PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_VALIDATION_MESSAGES, PropertiesConstants.SECURITY_ERROR_ISIN_CHECK_DIGIT,null);
				
				JSFUtilities.showMessageOnDialog(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES,PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
				GeneralConstants.PROPERTY_FILE_VALIDATION_MESSAGES, PropertiesConstants.SECURITY_ERROR_ISIN_CHECK_DIGIT, null);
				JSFUtilities.resetComponent(input.getClientId());
				
				if(skipValidation)
					JSFUtilities.showSimpleValidationDialog();
				FacesMessage msg = 
						new FacesMessage(message);
					msg.setSeverity(FacesMessage.SEVERITY_ERROR);
					throw new ValidatorException(msg);
			}
			security.setIdIsinCode(strValue);
			if(securityServiceFacade.validateSecurityISINServiceFacade( security )){
				message=PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_EXCEPTIONS, ErrorServiceType.SECURITY_ISIN_REPEATED.getMessage(),null);

				JSFUtilities.showMessageOnDialog(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES,PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
				GeneralConstants.PROPERTY_FILE_EXCEPTIONS, ErrorServiceType.SECURITY_ISIN_REPEATED.getMessage(), null);
				JSFUtilities.resetComponent(input.getClientId());
				JSFUtilities.showSimpleValidationDialog();
				
				FacesMessage msg = 
						new FacesMessage(message);
					msg.setSeverity(FacesMessage.SEVERITY_ERROR);
					throw new ValidatorException(msg);
			}
			
		}
	}
	
	/**
	 * Validate security code.
	 *
	 * @param context the context
	 * @param component the component
	 * @param value the value
	 * @throws ValidatorException the validator exception
	 * @throws Exception the exception
	 */
	public void validateSecurityCode() throws ValidatorException, Exception{
		//security.setDescription("");
		String value = security.getIdSecurityCodeOnly();
		if(value!=null && StringUtils.isNotBlank(value.toString())){
			if(security.getSecurityClass()==null){
				return;
			}
			billParameterTableById(security.getSecurityClass(), true);
			ParameterTable pTable=(ParameterTable)super.getParametersTableMap().get(security.getSecurityClass());
			
			Security securityAux=new Security();
			String secClass=pTable.getText1();
			
			StringBuilder code=new StringBuilder();
			//code.append(secClass);
			//code.append("-");
			code.append(value.toString());
			securityAux.setIdSecurityCodePk(code.toString());
			
			if(securityServiceFacade.validateSecurityCodeServiceFacade( securityAux )){
				JSFUtilities.showMessageOnDialog(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES,PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
				GeneralConstants.PROPERTY_FILE_EXCEPTIONS, ErrorServiceType.SECURITY_CODE_REPEATED.getMessage(), null);
				JSFUtilities.showSimpleValidationDialog();				
			} else {
				this.validateIdSecurityCodeOnlyLength();
			}
						
//			securityCodeFormat = securityServiceFacade.validateSecurityCodeFormat(security);
			/** REVISAR FORMATO ***/
//			if(securityCodeFormat==null){
//				JSFUtilities.showMessageOnDialog(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES,PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
//				GeneralConstants.PROPERTY_FILE_EXCEPTIONS, ErrorServiceType.SECURITY_CODE_INCORRECT_INSTRUMENT_CLASS.getMessage(), null);
//				JSFUtilities.resetComponent(input.getClientId());
//				JSFUtilities.showSimpleValidationDialog();
//				
//				FacesMessage msg = 
//						new FacesMessage(message);
//					msg.setSeverity(FacesMessage.SEVERITY_ERROR);
//					throw new ValidatorException(msg); 
//			}else{
//				try {
//					securitiesUtils.processValidateSecurityCodeFormat(security,securityCodeFormat,value);
//				} catch (ServiceException e) {
//					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//						     , PropertiesUtilities.getExceptionMessage(e.getMessage()));
//					JSFUtilities.resetComponent(input.getClientId());
//					JSFUtilities.showSimpleValidationAltDialog();
//				   message = PropertiesUtilities.getExceptionMessage(e.getMessage());
//				   FacesMessage msg = 
//							new FacesMessage(message);
//						msg.setSeverity(FacesMessage.SEVERITY_ERROR);
//						throw new ValidatorException(msg); 
//				} 
//				
//				
//			}
			
			//security.setDescription(securityAux.getIdSecurityCodePk());
		}	
	}
	
	/*Validators End*/
	
	/**
	 * Sum total share capital for issuance.
	 *
	 * @return the big decimal
	 * @throws ServiceException the service exception
	 */
	public BigDecimal sumTotalShareCapitalForIssuance() throws ServiceException{
		
		BigDecimal amount= new BigDecimal(0.00);
		
		SecurityTO securityTO = new SecurityTO();
		
		securityTO.setIdIssuerCodePk(security.getIssuer().getIdIssuerPk());
		securityTO.setIdIssuanceCodePk(security.getIssuance().getIdIssuanceCodePk());
		
		List<Security> lstSecurities = securityServiceFacade.findSecurities(securityTO);
		
		for (Security auxSecurities : lstSecurities) {
			if(!auxSecurities.getIdSecurityCodePk().equals(security.getIdSecurityCodePk())){
				if(!auxSecurities.isCoupon()){
					amount = auxSecurities.getShareCapital().add(amount);
				}
			}
		}
		
 		return amount;
	}
	
	public void addToRange() {
		if(security.getNumberShareInitial() !=null && security.getNumberShareFinal()!=null) {
			
			if(Validations.validateListIsNotNullAndNotEmpty(lstSecuritySerialRange)) {
				lstSecuritySerialRange = lstSecuritySerialRange.stream()
						 .filter(x->x.getStateRange()==1)
						 .collect(Collectors.toList());
				for(SecuritySerialRange object: lstSecuritySerialRange) {
					if(Validations.validateIsNotNullAndNotEmpty(object.getSerialRange())
							&& Validations.validateIsNotNullAndNotEmpty(security.getSerialRange())) {
						if(object.getSerialRange().equals(security.getSerialRange())
								&& object.getNumberShareInitial().equals(security.getNumberShareInitial())
								&& object.getNumberShareFinal().equals(security.getNumberShareFinal())
							) {
								
								
								JSFUtilities.showMessageOnDialog("Error","Ya existe una serie/rango con los mismos datos");
								JSFUtilities.showSimpleValidationDialog();
								
								return;
							}
					}else {
						if( object.getNumberShareInitial().equals(security.getNumberShareInitial())
								&& object.getNumberShareFinal().equals(security.getNumberShareFinal())
							) {
								
								
								JSFUtilities.showMessageOnDialog("Error","Ya existe un Desde/Hasta con los mismos datos");
								JSFUtilities.showSimpleValidationDialog();
								
								return;
							}
					}
					
					if(Validations.validateIsNotNullAndNotEmpty(object.getSerialRange())
							&& Validations.validateIsNotNullAndNotEmpty(security.getSerialRange())) {
						if(object.getSerialRange().equals(security.getSerialRange())) {
							if(object.getNumberShareInitial()==(security.getNumberShareInitial())
							|| object.getNumberShareFinal()>=(security.getNumberShareInitial())
							|| security.getNumberShareInitial()>=(security.getShareBalance().intValue())) {
								JSFUtilities.showMessageOnDialog("Error","La serie/rango no corresponde");
								JSFUtilities.showSimpleValidationDialog();
								
								return;
							}
							if(object.getNumberShareInitial()>=(security.getNumberShareFinal())
							|| object.getNumberShareFinal()>=(security.getNumberShareFinal())
							|| object.getNumberShareInitial()>(security.getNumberShareFinal())
							|| security.getNumberShareFinal()>(security.getShareBalance().intValue())) {
								JSFUtilities.showMessageOnDialog("Error","La serie/rango no corresponde");
								JSFUtilities.showSimpleValidationDialog();
								
								return;
							}
							
						}
					}else {
						if(object.getNumberShareInitial()==(security.getNumberShareInitial())
						|| object.getNumberShareFinal()>=(security.getNumberShareInitial())
						|| security.getNumberShareInitial()>=(security.getShareBalance().intValue())) {
							JSFUtilities.showMessageOnDialog("Error","La serie/rango no corresponde");
							JSFUtilities.showSimpleValidationDialog();
							
							return;
						}
						if(object.getNumberShareInitial()>=(security.getNumberShareFinal())
						|| object.getNumberShareFinal()>=(security.getNumberShareFinal())
						|| object.getNumberShareInitial()>(security.getNumberShareFinal())
						|| security.getNumberShareFinal()>(security.getShareBalance().intValue())) {
							JSFUtilities.showMessageOnDialog("Error","La serie/rango no corresponde");
							JSFUtilities.showSimpleValidationDialog();
							
							return;
						}
					}
					
				}
			}
		
			if(lstSecuritySerialRange == null) {
				lstSecuritySerialRange = new ArrayList<SecuritySerialRange>();
			}
			
			if(!(security.getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode())
					&& security.getIssuanceForm().equals(IssuanceType.DEMATERIALIZED.getCode()))) {
				if(Validations.validateIsNullOrEmpty(security.getShareCapital())) {
					JSFUtilities.showMessageOnDialog("Error","Debe registrar el Capital Contable");
					JSFUtilities.showSimpleValidationDialog();
					
					return;
				}
				if(security.getNumberShareInitial()>=(security.getShareBalance().intValue())
						|| security.getNumberShareFinal()>(security.getShareBalance().intValue())) {
							JSFUtilities.showMessageOnDialog("Error","El rango supera la cantidad de valores contables");
							JSFUtilities.showSimpleValidationDialog();
							
							return;
						}
			}

			SecuritySerialRange securitySerialRange = new SecuritySerialRange();
			securitySerialRange.setSecurity(security);
			securitySerialRange.setSerialRange(security.getSerialRange());
			securitySerialRange.setNumberShareInitial(security.getNumberShareInitial());
			securitySerialRange.setNumberShareFinal(security.getNumberShareFinal());
			securitySerialRange.setStateRange(1);	
			
		
			lstSecuritySerialRange.add(securitySerialRange);
			security.setSecuritySerialRange(lstSecuritySerialRange);

			security.setSerialRange(null);
			security.setNumberShareInitial(null);
			security.setNumberShareFinal(null);
			
		}
		
		
	}
	
	public void beforeRemoveToRange(SecuritySerialRange current) {
		if(lstSecuritySerialRange != null) {
			securitySerialRangeSelected= current;
			showMessageOnDialog(
					PropertiesConstants.LBL_HEADER_ALERT_MODIFY,
					null,
					PropertiesConstants.SECURITY_CONFIRM_RANGE_REMOVE,
					null);
			JSFUtilities.executeJavascriptFunction("PF('cnfwSecurityRemoveMgmt').show()");
		}
	}

	public void removeToRange() {
		JSFUtilities.executeJavascriptFunction("PF('cnfwSecurityRemoveMgmt').hide()");
		if(lstSecuritySerialRange != null) {
				SecuritySerialRange removeObject = null;
				
				for(SecuritySerialRange object: lstSecuritySerialRange) {
					if(Validations.validateIsNotNullAndNotEmpty(object.getSerialRange())
							&& Validations.validateIsNotNullAndNotEmpty(security.getSerialRange())) {
						if(object.getSerialRange().equals(securitySerialRangeSelected.getSerialRange())
								&& object.getNumberShareInitial().equals(securitySerialRangeSelected.getNumberShareInitial())
								&& object.getNumberShareFinal().equals(securitySerialRangeSelected.getNumberShareFinal())
						) {
							if(object.getIdSecuritySerialRangePk()!=null) {
								object.setStateRange(0);
							}else {
								removeObject = object;
							}
							break;
						}
					}else {
						if(object.getNumberShareInitial().equals(securitySerialRangeSelected.getNumberShareInitial())
								&& object.getNumberShareFinal().equals(securitySerialRangeSelected.getNumberShareFinal())
						) {
							if(object.getIdSecuritySerialRangePk()!=null) {
								object.setStateRange(0);
							}else {
								removeObject = object;
							}
							break;
						}
					}
				}

				if(removeObject!=null) {
					lstSecuritySerialRange.remove(removeObject);
				}
				
				security.setSecuritySerialRange(lstSecuritySerialRange);
				
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						"Se removió el rango satisfactoriamente");
				JSFUtilities.executeJavascriptFunction("PF('endTrasaction').show()");
		}
		
	}
	
	/*
	 * Start Indicators for dynamic fields in view
	 * */
	
	/**
	 * Checks if is fixed instrument type.
	 *
	 * @return true, if is fixed instrument type
	 */
	public boolean isFixedInstrumentType(){
		if(security.getInstrumentType()!=null && security.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode())){
			return true;
		}
		return false;
	}
	
	/**
	 * Gets the lst cbo security state.
	 *
	 * @return the lst cbo security state
	 */
	public List<ParameterTable> getLstCboSecurityState() {
		return lstCboSecurityState;
	}

	/**
	 * Sets the lst cbo security state.
	 *
	 * @param lstCboSecurityState the new lst cbo security state
	 */
	public void setLstCboSecurityState(List<ParameterTable> lstCboSecurityState) {
		this.lstCboSecurityState = lstCboSecurityState;
	}

	/**
	 * Gets the lst cbo security only register state.
	 *
	 * @return the lstCboSecurityOnlyRegisterState
	 */
	public List<ParameterTable> getLstCboSecurityOnlyRegisterState() {
		return lstCboSecurityOnlyRegisterState;
	}

	/**
	 * Sets the lst cbo security only register state.
	 *
	 * @param lstCboSecurityOnlyRegisterState the lstCboSecurityOnlyRegisterState to set
	 */
	public void setLstCboSecurityOnlyRegisterState(
			List<ParameterTable> lstCboSecurityOnlyRegisterState) {
		this.lstCboSecurityOnlyRegisterState = lstCboSecurityOnlyRegisterState;
	}

	/**
	 * Checks if is variable instrument type.
	 *
	 * @return true, if is variable instrument type
	 */
	public boolean isVariableInstrumentType(){
		if(security.getInstrumentType()!=null && security.getInstrumentType().equals( InstrumentType.VARIABLE_INCOME.getCode() )){
			return true;			
		}
		return false;
	}
	
	
	/**
	 * Checks if is variable interest type.
	 *
	 * @return true, if is variable interest type
	 */
	public boolean isVariableInterestType(){
		if(security.getInterestType()!=null && security.getInterestType().equals( InterestType.VARIABLE.getCode() )){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is fixed interest type.
	 *
	 * @return true, if is fixed interest type
	 */
	public boolean isFixedInterestType(){
		if(InterestType.FIXED.getCode().equals(security.getInterestType())){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is mixed interest type.
	 *
	 * @return true, if is mixed interest type
	 */
	public boolean isMixedInterestType(){
		if(security.getInterestType()!=null && security.getInterestType().equals( InterestType.MIXED.getCode() )){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is proportional amortization type.
	 *
	 * @return true, if is proportional amortization type
	 */
	public boolean isProportionalAmortizationType(){
		if(AmortizationType.PROPORTIONAL.getCode().equals(security.getAmortizationType()))
			return true;		
		return false;
	}
	
	/**
	 * Checks if is mixed issuance form.
	 *
	 * @return true, if is mixed issuance form
	 */
	public boolean isMixedIssuanceForm() {
		if (security.getIssuanceForm() != null
				&& security.getIssuanceForm().equals(
						IssuanceType.MIXED.getCode())) {
			return true;
		}
		return false;
	}
	
	/*
	 * End Indicators for dynamic fields in view
	 * */
	
	/**
	 * Change cbo calendar month.
	 */
	public void changeCboCalendarMonth(){
		try {
			security.setCalendarType(null);
			JSFUtilities.resetComponent(SecurityMgmtType.CBO_CALENDAR_TYPE.getValue());
			if(security.is_30CalendarMonth()){
				security.setCalendarType( CalendarType.COMMERCIAL.getCode() );
			}
			changeCboCalendarType();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change cbo calendar type.
	 */
	public void changeCboCalendarType(){
		try {
			security.setCalendarDays(null);
			JSFUtilities.resetComponent(SecurityMgmtType.CBO_CALENDAR_DAY.getValue());
			if(security.isCommercialCalendarType()){
				security.setCalendarDays( CalendarDayType._360.getCode() );
			}else if(security.isCalendarCalendarType()){
				security.setCalendarDays( CalendarDayType._365.getCode() );
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Chance cbo allow split coupon handler.
	 */
	public void chanceCboAllowSplitCouponHandler(){
		try {
			lstDtbNegotiationModalities=new ArrayList<NegotiationModality>();
			
			if(security.getInstrumentType()!=null){
				loadDtbNegotiationModality(security.getInstrumentType());
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change cbo interest payment modality handler.
	 */

	public void changeCboInterestPaymentModalityHandler(){
		try {	
			if(isViewOperationRegister()){
				if(!security.isCeroInterestType()){
					security.setInterestRate(null);
				}	
				security.setPeriodicity(null);
				security.setPeriodicityDays(null);
				
				security.setCalendarMonth(CalendarMonthType.ACTUAL.getCode());
				changeCboCalendarMonth();
				security.setCalendarType(CalendarType.COMMERCIAL.getCode());
				changeCboCalendarType();
				security.setCalendarDays(CalendarDayType._360.getCode());
				
			}	
			if(security.isAtMaturityIntPaymentModality()){
				if(security.isCeroInterestType()){
					security.setCouponFirstExpirationDate(null);
					security.setNumberCoupons(null);
				}else{
					security.setCouponFirstExpirationDate(security.getExpirationDate());
					
					if (security.getInterestPaymentModality().equals(InterestPaymentModalityType.AT_MATURITY.getCode())){
						security.setNumberCoupons(GeneralConstants.ZERO_VALUE_INTEGER);
					}else{
						security.setNumberCoupons(GeneralConstants.ONE_VALUE_INTEGER );
					}
					
				}
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change cbo conv securities.
	 */
	public void changeCboConvSecurities(){
		try {
			security.setConvertibleStockType(null);
			security.setIdSecurityClassTrg(null);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Insert interest periodicity days handler.
	 */
	public void insertInterestPeriodicityDaysHandler(){
		if(security.getSecurityDaysTerm()!=null){
			if(security.getPeriodicityDays().compareTo(security.getSecurityDaysTerm())==1){
				security.setPeriodicityDays(null);
				String message=PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_ERROR_PERIOD_DAYS_EXCEEDS);
				JSFUtilities.addContextMessage(SecurityMgmtType.TXT_PERIODICITY_DAYS.getValue(),
											   FacesMessage.SEVERITY_ERROR,message,message);
				return;
			}
			changeCboInterestPeriodicityHandler();
		}
	}
	
	/**
	 * Insert spread handler.
	 */
	public void insertSpreadHandler(){
		try {
			if(security.getOperationType()!=null && security.getOperationType().equals( OperationType.SUBTRACTION.getCode() )){
				if(security.getRateValue()!=null && security.getSpread().compareTo( security.getRateValue() )==1){
					String message=PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_ERROR_SPREAD_LESS_RATE_VALUE);
					JSFUtilities.addContextMessage(SecurityMgmtType.TXT_SPREAD_ESCALON.getValue(),
												   FacesMessage.SEVERITY_ERROR,message,message);
					security.setSpread(null);
					return;
				}
			}
			
			calculateInterestRateHandler();
		} catch (Exception e) {
			excepcion.fire( new ExceptionToCatchEvent( e ) );
		}
	}
	
	/**
	 * Change cbo operation type.
	 */
	public void changeCboOperationType(){
		try {
			security.setSpread(null);
			calculateInterestRateHandler();
		} catch (Exception e) {
			excepcion.fire( new ExceptionToCatchEvent( e ) );
		}
	}
	
	/**
	 * Change cbo interest periodicity handler.
	 */

	public void changeCboInterestPeriodicityHandler(){
		try {
			if(security.isByDaysInterestPeriodicity()){				
//				security.setCalendarMonth(CalendarMonthType.ACTUAL.getCode());
//				security.setCalendarType( CalendarType.CALENDAR.getCode() );
//				security.setCalendarDays( CalendarDayType.ACTUAL.getCode() );
			}else {
				security.setPeriodicityDays(null);
			}
			security.setCouponFirstExpirationDate(null);
			security.setNumberCoupons(null);
			JSFUtilities.resetComponent("frmSecurityMgmt:tviewGeneral:calFirstPayCoupon");
			JSFUtilities.resetComponent("frmSecurityMgmt:tviewGeneral:txtNroCoupons");
		
			calculateNroCouponsAndFirstPayment();
		} catch (Exception e) {
			excepcion.fire( new ExceptionToCatchEvent( e ) );
		}
	}
	
	
	/**
	 * Chage cbo amort payment modality handler.
	 */
	public void chageCboAmortPaymentModalityHandler(){
		try {
			JSFUtilities.resetComponent(SecurityMgmtType.CBO_AMORTIZATION_TYPE.getValue());
			JSFUtilities.resetComponent(SecurityMgmtType.CBO_AMORTIZATION_ON.getValue());
			if(isViewOperationRegister()){
				security.setAmortizationType(null);
				security.setAmortizationOn(null);
				security.setPaymentFirstExpirationDate( null );
			}
			if(security.getCapitalPaymentModality()==null){
				security.setAmortizationOn(null);
			}else{
				security.setAmortizationOn(AmortizationOnType.CAPITAL.getCode());
			}			
			
			
			if(security.isAtMaturityAmortPaymentModality()){
				security.setAmortizationType(null);
				
				
				security.setPaymentFirstExpirationDate( security.getExpirationDate() );
				
				setPaymentNumberAmortization(Integer.valueOf(GeneralConstants.ONE_VALUE_INTEGER));
				security.setAmortizationFactor(new BigDecimal(100));
				
			}
			changeCboAmortizationTypeHandler();
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change cbo amortization type handler.
	 */
	public void changeCboAmortizationTypeHandler(){
		try {
 			if(isViewOperationRegister()){
				JSFUtilities.resetComponent(SecurityMgmtType.CBO_PER_AMORTIZATION.getValue());
				JSFUtilities.resetComponent(SecurityMgmtType.CBO_PER_AMORTIZATION.getValue());
				
				security.setAmortizationPeriodicity(null);
				security.setAmortizationPeriodicityDays(null);
			}
			
			if(security.isNotProportionalAmortizationType()){
				security.setPaymentFirstExpirationDate( null );
				security.setAmortizationPeriodicity( InterestPeriodicityType.INDISTINCT.getCode() );
				security.setAmortizationPeriodicityDays( null );
				changePeriodicityAmort();

			}else if(security.isProportionalAmortizationType()){
				security.setAmortizationPeriodicity(security.getPeriodicity());
				security.setAmortizationPeriodicityDays( security.getPeriodicityDays() );
				security.setPaymentFirstExpirationDate( security.getCouponFirstExpirationDate() );
				changePeriodicityAmort();
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * For expiration.
	 */
	public void forExpiration(){
		try {
			if(CapitalPaymentModalityType.AT_MATURITY.getCode().equals(security.getCapitalPaymentModality())){			
				blAmortizationForExpiration = true;
				security.setAmortizationFactor(new BigDecimal(100));
				security.setAmortizationType(AmortizationType.PROPORTIONAL.getCode());
				security.setAmortizationPeriodicity(null);				
			}else{
				blAmortizationForExpiration = false;
				security.setAmortizationFactor(null);
				security.setAmortizationType(null);
				security.setAmortizationPeriodicity(null);
			}
			
			if(CapitalPaymentModalityType.AT_MATURITY.getCode().equals(security.getInterestPaymentModality())){
				if(InterestType.VARIABLE.getCode().equals(security.getInterestType())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_ERROR_VAR_INT_AT_MATURITY));
					JSFUtilities.executeJavascriptFunction("PF('cnfwValDi').show();");
					security.setInterestPaymentModality(null);
					security.setYield(null);
					security.setFinancialIndex(null);
					security.setRateType(null);
					security.setSpread(null);
					security.setMinimumRate(null);
					security.setMaximumRate(null);
				}	
				blInterestForExpiration = true;
				security.setInterestRate(null);
				security.setPeriodicity(null);
				security.setInterestRatePeriodicity(null);
				calculateNroCouponsAndFirstPayment();
			}else{
				blInterestForExpiration = false;
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change periodicity amort.
	 */
	public void changePeriodicityAmort(){
		try {
			security.setPaymentFirstExpirationDate(null);
			setPaymentNumberAmortization(null);
			security.setAmortizationPeriodicityDays(null);
			security.setAmortizationFactor(null);
			
			if(security.getSecurityMonthsTerm()!=null && security.getAmortizationPeriodicity()!=null){
				calculateAmortFactorAndNumberCoupons();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Insert amort periodicity days.
	 */
	public void insertAmortPeriodicityDays(){
		if(security.getSecurityMonthsTerm()!=null && security.getAmortizationPeriodicity()!=null){
			calculateAmortFactorAndNumberCoupons();
		}
	}
	
	/**
	 * Gets the nro coupons amort mixed issue fixed inst.
	 *
	 * @param numberPayments the number payments
	 * @return the nro coupons amort mixed issue fixed inst
	 * @throws Exception the exception
	 */
	private int getNroCouponsAmortMixedIssueFixedInst(Integer numberPayments) throws Exception{
		int coupons=0;
		if(security.isProportionalAmortizationType()){
			
			ProgramAmortizationCoupon programAmortizationCoupon=null;		
			Date previousExpirationDate=null;
			
			for(int i=1;i<=numberPayments;i++){
				programAmortizationCoupon=new ProgramAmortizationCoupon();
				//Begin date
				if(Validations.validateIsNullOrEmpty(previousExpirationDate))
					programAmortizationCoupon.setBeginingDate(security.getIssuanceDate());
				else{
					programAmortizationCoupon.setBeginingDate(previousExpirationDate);
				}	
				//Expiration date
				if(i==numberPayments){//ultimo cupon debe tener la fecha de vencimiento del valor
					programAmortizationCoupon.setExpirationDate(security.getExpirationDate());
				}else{
					if(security.isByDaysAmortizationPeriodicity()){
						programAmortizationCoupon.setExpirationDate(CommonsUtilities.addDaysToDate(programAmortizationCoupon.getBeginingDate()
								, security.getAmortizationPeriodicityDays()));
					}else{
						programAmortizationCoupon.setExpirationDate(CommonsUtilities.addMonthsToDate(programAmortizationCoupon.getBeginingDate()
								, InterestPeriodicityType.get(security.getAmortizationPeriodicity()).getIndicator1()));
					}

				}
				previousExpirationDate = programAmortizationCoupon.getExpirationDate();
				Date registryDate=isViewOperationRegister() ? CommonsUtilities.currentDate() : security.getRegistryDate();
				if(!CommonsUtilities.isLessOrEqualDate(programAmortizationCoupon.getExpirationDate(), registryDate, true))
					coupons++;
				
			}
		}
		return coupons;
	}
	
	/**
	 * Gets the amortization factor.
	 *
	 * @return the amortization factor
	 */
	public void getAmortizationFactor(){
		try {
			if(isProportionalAmortizationType()){
				if(!security.isByDaysAmortizationPeriodicity()){
					security.setAmortizationPeriodicityDays( null );
				}
				BigDecimal facAmort = null;				
				BigDecimal actualPercent = new BigDecimal("100");
				BigDecimal coupons = getNroCouponsForAmortization(security.getSecurityMonthsTerm(),security.getSecurityDaysTerm());
					try{
						if(Validations.validateIsNotNullAndNotEmpty(coupons))
							facAmort = actualPercent.divide(coupons);
					}catch (ArithmeticException e) {
						facAmort = null;
					}
					security.setAmortizationFactor(facAmort);
				}else
					security.setAmortizationFactor(null);			
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
	}
	
	/**
	 * Calculate nro coupons and first payment.
	 *
	 * @throws Exception the exception
	 */
	public void calculateNroCouponsAndFirstPayment() throws Exception{
		if(security.isAtMaturityIntPaymentModality()){
			security.setNumberCoupons(1);
			security.setCouponFirstExpirationDate(security.getExpirationDate());
		}else if(security.isByCouponIntPaymentModality()){
			BigDecimal coupons = getNroCouponsForInterest(security.getSecurityMonthsTerm(),security.getSecurityDaysTerm());
			if(Validations.validateIsNotNullAndNotEmpty(coupons)){
				if(Validations.validateIsNotNullAndNotEmpty(security.getIssuance())
						&& Validations.validateIsNotNullAndNotEmpty(security.getPeriodicity())){
					security.setNumberCoupons(new Integer(coupons.toString()));
					Date expirationDate=null;
					if(security.isByDaysInterestPeriodicity()){
						if(security.getPeriodicityDays()!=null && security.getPeriodicityDays()>0){
							expirationDate = CommonsUtilities.addDaysToDate(security.getIssuanceDate(), 
																			security.getPeriodicityDays());
						}
					}else{
						expirationDate = CommonsUtilities.addMonthsToDate(security.getIssuanceDate(), 
								   InterestPeriodicityType.get(security.getPeriodicity()).getIndicator1());
					}

					security.setCouponFirstExpirationDate(expirationDate);
				}else{
					security.setNumberCoupons(null);
					security.setCouponFirstExpirationDate(null);
				}
			}else{
				security.setNumberCoupons(null);
				security.setCouponFirstExpirationDate(null);
			}
		}
	}
	
	/**
	 * Enabled the Alternate code field.
	 *
	 * @return true, if  secirities  is disable Placement Segments
	 */
	public boolean isPlacementSegments() {
		if(isViewOperationModify()){
			try{
				blnIsPlacementSegment=securityServiceFacade.isPlacementSegments( security.getIdSecurityCodePk());
			} catch (Exception e) {
				e.printStackTrace();
				excepcion.fire( new ExceptionToCatchEvent(e) );
			} 
		}
		return blnIsPlacementSegment;
	}
	/**
	 * Gets the nro coupons for amortization.
	 *
	 * @param securityMonths the security months
	 * @param securityDays the security days
	 * @return the nro coupons for amortization
	 * @throws Exception the exception
	 */


	private BigDecimal getNroCouponsForAmortization(Integer securityMonths, Integer securityDays) throws Exception{
		if(Validations.validateIsNotNullAndNotEmpty(securityMonths) &&
				Validations.validateIsNotNullAndNotEmpty(securityDays) &&
				Validations.validateIsNotNullAndNotEmpty(security.getAmortizationPeriodicity())){
			BigDecimal coupons = null;
			BigDecimal monthsTerm = new BigDecimal(securityMonths);
			BigDecimal daysTerm = new BigDecimal( securityDays );
			BigDecimal periodicity=null;
			
			if(security.isByDaysAmortizationPeriodicity()){
				if(security.getAmortizationPeriodicityDays()==null || security.getAmortizationPeriodicityDays()<=0){
					return null;
				}
				periodicity=BigDecimal.valueOf( security.getAmortizationPeriodicityDays() );
				coupons=daysTerm.divide( periodicity,RoundingMode.DOWN );
			}else if(!security.isIndistinctAmortizationPeriodicity()){
				periodicity=new BigDecimal( InterestPeriodicityType.get( security.getAmortizationPeriodicity()).getIndicator1() );
				coupons=monthsTerm.divide(periodicity,RoundingMode.DOWN);
			}
			return coupons;
		}
		return null;
	}
	
	/**
	 * Gets the nro coupons for interest.
	 *
	 * @param securityMonths the security months
	 * @param securityDays the security days
	 * @return the nro coupons for interest
	 * @throws Exception the exception
	 */

	private BigDecimal getNroCouponsForInterest(Integer securityMonths, Integer securityDays) throws Exception{
		if(Validations.validateIsNotNullAndNotEmpty(securityMonths) &&
				Validations.validateIsNotNullAndNotEmpty(securityDays) &&		
				Validations.validateIsNotNullAndNotEmpty(security.getPeriodicity())){
			BigDecimal coupons = null;
			BigDecimal monthsTerm = new BigDecimal(securityMonths);
			BigDecimal daysTerm = new BigDecimal(securityDays);
			BigDecimal periodicity=null;
			
			if(security.isByDaysInterestPeriodicity() && (security.getPeriodicityDays()==null || security.getPeriodicityDays()<=0)){
				return null;
			}
			if(security.isByDaysInterestPeriodicity()){
				periodicity=BigDecimal.valueOf(security.getPeriodicityDays());
				coupons = daysTerm.divide(periodicity,RoundingMode.DOWN);
			}else if(!security.isIndistinctPeriodicity()){
				periodicity = new BigDecimal(InterestPeriodicityType.get(security.getPeriodicity().intValue()).getIndicator1());
				coupons = monthsTerm.divide(periodicity,RoundingMode.DOWN);
			}	
			return coupons;
		}
		return null;
	}

	public void changeEncoderAgent() {
		security.setIdSecurityCodeOnly(null);
		security.setIdIsinCode(null);
		security.setAlternativeCode(null);
	}
	
	public void validateIdSecurityCodeOnlyLength() {
		if(Validations.validateIsNotNullAndNotEmpty(security.getIdSecurityCodeOnly()) &&
				this.validateCountryIsin() && 
				Validations.validateIsNotNull(security.getEncoderAgent())) {
			if(!security.getEncoderAgent().equals(EncoderAgentType.CAVAPY.getCode())) {
				if(security.getIdSecurityCodeOnly().length() != 12) {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						     , "La longitud del código de valor debe ser de 12");
					JSFUtilities.showSimpleValidationDialog();
				}
			}
			
		}
	}
	
	/**
	 * Bill parameters table map.
	 *
	 * @param lstParameterTable the lst parameter table
	 */
	public void billParametersTableMap(List<ParameterTable> lstParameterTable){
		billParametersTableMap(lstParameterTable,false);
	}
	
	/**
	 * Bill parameters table map.
	 *
	 * @param lstParameterTable the lst parameter table
	 * @param isObj the is obj
	 */
	public void billParametersTableMap(List<ParameterTable> lstParameterTable, boolean isObj){
		for(ParameterTable pTable : lstParameterTable){
			if(isObj){
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable);
			}else{
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable.getParameterName());
			}
		}
	}
	
	/**
	 * Bill parameter table by id.
	 *
	 * @param parameterPk the parameter pk
	 * @throws ServiceException the service exception
	 */
	public void billParameterTableById(Integer parameterPk) throws ServiceException{
		billParameterTableById(parameterPk, false);
	}
	
	/**
	 * Bill parameter table by id.
	 *
	 * @param parameterPk the parameter pk
	 * @param isObj the is obj
	 * @throws ServiceException the service exception
	 */
	public void billParameterTableById(Integer parameterPk, boolean isObj) throws ServiceException{
		ParameterTable pTable = generalParameterFacade.getParamDetailServiceFacade(parameterPk);
		if(pTable!=null){
			if(isObj){
				super.getParametersTableMap().put(pTable.getParameterTablePk()  , pTable);
			}else{
				super.getParametersTableMap().put(pTable.getParameterTablePk()  , pTable.getParameterName());
			}
		}
	}
	
	/**
	 * Change cbo conv securities.
	 */
	public void changeCboNotTraded(){
		try {			
			if(blNotTrded){
				security.setNotTraded(BooleanType.YES.getCode());
				lstDtbNegotiationModalities = new ArrayList<NegotiationModality>();
			} else {
				security.setNotTraded(BooleanType.NO.getCode());
				loadDtbNegotiationModality(security.getInstrumentType());
			}			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
	}

	public void validateRangeNumberShare() throws ServiceException{
		if(Validations.validateIsNotNullAndPositive(security.getNumberShareInitial()) &&
				Validations.validateIsNotNullAndPositive(security.getNumberShareFinal()) &&
				Validations.validateIsNotNullAndNotEmpty(security.getIssuance().getIdIssuanceCodePk()) &&
					Validations.validateIsNotNullAndNotEmpty(security.getSubClass())){
			
			if(!securityServiceFacade.validateRangeNumberShare(security)) {
				security.setNumberShareInitial(null);
				security.setNumberShareFinal(null);
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_ERROR, null,
						PropertiesConstants.ERROR_RANGE_NUMBER_SHARE, null);
				JSFUtilities
						.executeJavascriptFunction("PF('cnfwMsgCustomValidationSec').show()");
			}

		}
	}

	public void onClose(CloseEvent event) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Panel Closed",
				"Closed panel id:'" + event.getComponent().getId() + "'");
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public void onToggle(ToggleEvent event) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, event.getComponent().getId() + " toggled",
				"Status:" + event.getVisibility().name());
		FacesContext.getCurrentInstance().addMessage(null, message);
	}
	
	
	public boolean isSelectAllCouponsInterest() {
		return selectAllCouponsInterest;
	}

	public void setSelectAllCouponsInterest(boolean selectAllCouponsInterest) {
		this.selectAllCouponsInterest = selectAllCouponsInterest;
	}
	
	public boolean isSelectAllCouponsAmortization() {
		return selectAllCouponsAmortization;
	}

	public void setSelectAllCouponsAmortization(boolean selectAllCouponsAmortization) {
		this.selectAllCouponsAmortization = selectAllCouponsAmortization;
	}
	
	public int getPaymentAmortizationDays() {
		return paymentAmortizationDays;
	}

	public void setPaymentAmortizationDays(int paymentAmortizationDays) {
		this.paymentAmortizationDays = paymentAmortizationDays;
	}

	/**
	 * Insert months terms handler.
	 */
	public void initSecuritiesSource(){
		security.setSecurityOrigin(new Security());		
	}
	
	/**
	 * Gets the lst cbo cfi categories.
	 *
	 * @return the lst cbo cfi categories
	 */
	public List<CfiCategory> getLstCboCFICategories() {
		return lstCboCFICategories;
	}

	/**
	 * Sets the lst cbo cfi categories.
	 *
	 * @param lstCboCFICategories the new lst cbo cfi categories
	 */
	public void setLstCboCFICategories(List<CfiCategory> lstCboCFICategories) {
		this.lstCboCFICategories = lstCboCFICategories;
	}
	

	/**
	 * Gets the cfi category.
	 *
	 * @return the cfi category
	 */
	public CfiCategory getCfiCategory() {
		return cfiCategory;
	}

	/**
	 * Sets the cfi category.
	 *
	 * @param cfiCategory the new cfi category
	 */
	public void setCfiCategory(CfiCategory cfiCategory) {
		this.cfiCategory = cfiCategory;
	}

	/**
	 * Gets the cfi group.
	 *
	 * @return the cfi group
	 */
	public CfiGroup getCfiGroup() {
		return cfiGroup;
	}

	/**
	 * Sets the cfi group.
	 *
	 * @param cfiGroup the new cfi group
	 */
	public void setCfiGroup(CfiGroup cfiGroup) {
		this.cfiGroup = cfiGroup;
	}

	/**
	 * Gets the cfi attribute1.
	 *
	 * @return the cfi attribute1
	 */
	public CfiAttribute getCfiAttribute1() {
		return cfiAttribute1;
	}

	/**
	 * Sets the cfi attribute1.
	 *
	 * @param cfiAttribute1 the new cfi attribute1
	 */
	public void setCfiAttribute1(CfiAttribute cfiAttribute1) {
		this.cfiAttribute1 = cfiAttribute1;
	}

	/**
	 * Gets the cfi attribute2.
	 *
	 * @return the cfi attribute2
	 */
	public CfiAttribute getCfiAttribute2() {
		return cfiAttribute2;
	}

	/**
	 * Sets the cfi attribute2.
	 *
	 * @param cfiAttribute2 the new cfi attribute2
	 */
	public void setCfiAttribute2(CfiAttribute cfiAttribute2) {
		this.cfiAttribute2 = cfiAttribute2;
	}

	/**
	 * Gets the cfi attribute3.
	 *
	 * @return the cfi attribute3
	 */
	public CfiAttribute getCfiAttribute3() {
		return cfiAttribute3;
	}

	/**
	 * Sets the cfi attribute3.
	 *
	 * @param cfiAttribute3 the new cfi attribute3
	 */
	public void setCfiAttribute3(CfiAttribute cfiAttribute3) {
		this.cfiAttribute3 = cfiAttribute3;
	}

	/**
	 * Gets the cfi attribute4.
	 *
	 * @return the cfi attribute4
	 */
	public CfiAttribute getCfiAttribute4() {
		return cfiAttribute4;
	}

	/**
	 * Sets the cfi attribute4.
	 *
	 * @param cfiAttribute4 the new cfi attribute4
	 */
	public void setCfiAttribute4(CfiAttribute cfiAttribute4) {
		this.cfiAttribute4 = cfiAttribute4;
	}

	/**
	 * Gets the lst cbo cfi groups.
	 *
	 * @return the lst cbo cfi groups
	 */
	public List<CfiGroup> getLstCboCFIGroups() {
		return lstCboCFIGroups;
	}

	/**
	 * Sets the lst cbo cfi groups.
	 *
	 * @param lstCboCFIGroups the new lst cbo cfi groups
	 */
	public void setLstCboCFIGroups(List<CfiGroup> lstCboCFIGroups) {
		this.lstCboCFIGroups = lstCboCFIGroups;
	}

	/**
	 * Gets the lst cbo cfi attribute1.
	 *
	 * @return the lst cbo cfi attribute1
	 */
	public List<CfiAttribute> getLstCboCFIAttribute1() {
		return lstCboCFIAttribute1;
	}

	/**
	 * Sets the lst cbo cfi attribute1.
	 *
	 * @param lstCboCFIAttribute1 the new lst cbo cfi attribute1
	 */
	public void setLstCboCFIAttribute1(List<CfiAttribute> lstCboCFIAttribute1) {
		this.lstCboCFIAttribute1 = lstCboCFIAttribute1;
	}

	/**
	 * Gets the lst cbo cfi attribute2.
	 *
	 * @return the lst cbo cfi attribute2
	 */
	public List<CfiAttribute> getLstCboCFIAttribute2() {
		return lstCboCFIAttribute2;
	}

	/**
	 * Sets the lst cbo cfi attribute2.
	 *
	 * @param lstCboCFIAttribute2 the new lst cbo cfi attribute2
	 */
	public void setLstCboCFIAttribute2(List<CfiAttribute> lstCboCFIAttribute2) {
		this.lstCboCFIAttribute2 = lstCboCFIAttribute2;
	}

	/**
	 * Gets the lst cbo cfi attribute3.
	 *
	 * @return the lst cbo cfi attribute3
	 */
	public List<CfiAttribute> getLstCboCFIAttribute3() {
		return lstCboCFIAttribute3;
	}

	/**
	 * Sets the lst cbo cfi attribute3.
	 *
	 * @param lstCboCFIAttribute3 the new lst cbo cfi attribute3
	 */
	public void setLstCboCFIAttribute3(List<CfiAttribute> lstCboCFIAttribute3) {
		this.lstCboCFIAttribute3 = lstCboCFIAttribute3;
	}

	/**
	 * Gets the lst cbo cfi attribute4.
	 *
	 * @return the lst cbo cfi attribute4
	 */
	public List<CfiAttribute> getLstCboCFIAttribute4() {
		return lstCboCFIAttribute4;
	}

	/**
	 * Sets the lst cbo cfi attribute4.
	 *
	 * @param lstCboCFIAttribute4 the new lst cbo cfi attribute4
	 */
	public void setLstCboCFIAttribute4(List<CfiAttribute> lstCboCFIAttribute4) {
		this.lstCboCFIAttribute4 = lstCboCFIAttribute4;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets the issuance data model.
	 *
	 * @return the issuance data model
	 */
	public IssuanceDataModel getIssuanceDataModel() {
		return issuanceDataModel;
	}

	/**
	 * Sets the issuance data model.
	 *
	 * @param issuanceDataModel the new issuance data model
	 */
	public void setIssuanceDataModel(IssuanceDataModel issuanceDataModel) {
		this.issuanceDataModel = issuanceDataModel;
	}

	/**
	 * Gets the lst cbo instrument type.
	 *
	 * @return the lst cbo instrument type
	 */
	public List<ParameterTable> getLstCboInstrumentType() {
		return lstCboInstrumentType;
	}

	/**
	 * Sets the lst cbo instrument type.
	 *
	 * @param lstCboInstrumentType the new lst cbo instrument type
	 */
	public void setLstCboInstrumentType(List<ParameterTable> lstCboInstrumentType) {
		this.lstCboInstrumentType = lstCboInstrumentType;
	}

	/**
	 * Gets the lst cbo securitie type.
	 *
	 * @return the lst cbo securitie type
	 */
	public List<ParameterTable> getLstCboSecuritieType() {
		return lstCboSecuritieType;
	}

	/**
	 * Sets the lst cbo securitie type.
	 *
	 * @param lstCboSecuritieType the new lst cbo securitie type
	 */
	public void setLstCboSecuritieType(List<ParameterTable> lstCboSecuritieType) {
		this.lstCboSecuritieType = lstCboSecuritieType;
	}

	/**
	 * Gets the lst cbo securitie class.
	 *
	 * @return the lst cbo securitie class
	 */
	public List<ParameterTable> getLstCboSecuritieClass() {
		return lstCboSecuritieClass;
	}

	/**
	 * Sets the lst cbo search securitie class.
	 *
	 * @param lstCboSecuritieClass the new lst cbo search securitie class
	 */
	public void setLstCboSearchSecuritieClass(
			List<ParameterTable> lstCboSecuritieClass) {
		this.lstCboSecuritieClass = lstCboSecuritieClass;
	}

	/**
	 * Gets the lst cbo issuance state.
	 *
	 * @return the lst cbo issuance state
	 */
	public List<ParameterTable> getLstCboIssuanceState() {
		return lstCboIssuanceState;
	}

	/**
	 * Sets the lst cbo issuance state.
	 *
	 * @param lstCboIssuanceState the new lst cbo issuance state
	 */
	public void setLstCboIssuanceState(List<ParameterTable> lstCboIssuanceState) {
		this.lstCboIssuanceState = lstCboIssuanceState;
	}

	/**
	 * Gets the lst cbo geographic location.
	 *
	 * @return the lst cbo geographic location
	 */
	public List<GeographicLocation> getLstCboGeographicLocation() {
		return lstCboGeographicLocation;
	}

	/**
	 * Sets the lst cbo geographic location.
	 *
	 * @param lstCboGeographicLocation the new lst cbo geographic location
	 */
	public void setLstCboGeographicLocation(
			List<GeographicLocation> lstCboGeographicLocation) {
		this.lstCboGeographicLocation = lstCboGeographicLocation;
	}

	/**
	 * Gets the lst cbo currency.
	 *
	 * @return the lst cbo currency
	 */
	public List<ParameterTable> getLstCboCurrency() {
		return lstCboCurrency;
	}

	/**
	 * Sets the lst cbo currency.
	 *
	 * @param lstCboCurrency the new lst cbo currency
	 */
	public void setLstCboCurrency(List<ParameterTable> lstCboCurrency) {
		this.lstCboCurrency = lstCboCurrency;
	}

	/**
	 * Gets the lst cbo issuance type.
	 *
	 * @return the lst cbo issuance type
	 */
	public List<ParameterTable> getLstCboIssuanceType() {
		return lstCboIssuanceType;
	}

	/**
	 * Sets the lst cbo issuance type.
	 *
	 * @param lstCboIssuanceType the new lst cbo issuance type
	 */
	public void setLstCboIssuanceType(List<ParameterTable> lstCboIssuanceType) {
		this.lstCboIssuanceType = lstCboIssuanceType;
	}

	/**
	 * Gets the lst cbo boolean type.
	 *
	 * @return the lst cbo boolean type
	 */
	public List<BooleanType> getLstCboBooleanType() {
		return lstCboBooleanType;
	}

	/**
	 * Sets the lst cbo boolean type.
	 *
	 * @param lstCboBooleanType the new lst cbo boolean type
	 */
	public void setLstCboBooleanType(List<BooleanType> lstCboBooleanType) {
		this.lstCboBooleanType = lstCboBooleanType;
	}

	/**
	 * Gets the lst cbo interest type.
	 *
	 * @return the lst cbo interest type
	 */
	public List<ParameterTable> getLstCboInterestType() {
		return lstCboInterestType;
	}

	/**
	 * Sets the lst cbo interest type.
	 *
	 * @param lstCboInterestType the new lst cbo interest type
	 */
	public void setLstCboInterestType(List<ParameterTable> lstCboInterestType) {
		this.lstCboInterestType = lstCboInterestType;
	}

	/**
	 * Gets the lst cbo calendar type.
	 *
	 * @return the lst cbo calendar type
	 */
	public List<ParameterTable> getLstCboCalendarType() {
		return lstCboCalendarType;
	}

	/**
	 * Sets the lst cbo calendar type.
	 *
	 * @param lstCboCalendarType the new lst cbo calendar type
	 */
	public void setLstCboCalendarType(List<ParameterTable> lstCboCalendarType) {
		this.lstCboCalendarType = lstCboCalendarType;
	}

	/**
	 * Gets the lst cbo calendar days.
	 *
	 * @return the lst cbo calendar days
	 */
	public List<ParameterTable> getLstCboCalendarDays() {
		return lstCboCalendarDays;
	}

	/**
	 * Sets the lst cbo calendar days.
	 *
	 * @param lstCboCalendarDays the new lst cbo calendar days
	 */
	public void setLstCboCalendarDays(List<ParameterTable> lstCboCalendarDays) {
		this.lstCboCalendarDays = lstCboCalendarDays;
	}

	

	/**
	 * Gets the lst cbo payment modality.
	 *
	 * @return the lst cbo payment modality
	 */
	public List<ParameterTable> getLstCboPaymentModality() {
		return lstCboPaymentModality;
	}

	/**
	 * Sets the lst cbo payment modality.
	 *
	 * @param lstCboPaymentModality the new lst cbo payment modality
	 */
	public void setLstCboPaymentModality(List<ParameterTable> lstCboPaymentModality) {
		this.lstCboPaymentModality = lstCboPaymentModality;
	}

	/**
	 * Gets the lst cbo rate type.
	 *
	 * @return the lst cbo rate type
	 */
	public List<ParameterTable> getLstCboRateType() {
		return lstCboRateType;
	}

	/**
	 * Sets the lst cbo rate type.
	 *
	 * @param lstCboRateType the new lst cbo rate type
	 */
	public void setLstCboRateType(List<ParameterTable> lstCboRateType) {
		this.lstCboRateType = lstCboRateType;
	}

	/**
	 * Gets the lst cbo operation type.
	 *
	 * @return the lst cbo operation type
	 */
	public List<ParameterTable> getLstCboOperationType() {
		return lstCboOperationType;
	}

	/**
	 * Sets the lst cbo operation type.
	 *
	 * @param lstCboOperationType the new lst cbo operation type
	 */
	public void setLstCboOperationType(List<ParameterTable> lstCboOperationType) {
		this.lstCboOperationType = lstCboOperationType;
	}

	/**
	 * Gets the lst cbo interest periodicity.
	 *
	 * @return the lst cbo interest periodicity
	 */
	public List<ParameterTable> getLstCboInterestPeriodicity() {
		return lstCboInterestPeriodicity;
	}

	/**
	 * Sets the lst cbo interest periodicity.
	 *
	 * @param lstCboInterestPeriodicity the new lst cbo interest periodicity
	 */
	public void setLstCboInterestPeriodicity(
			List<ParameterTable> lstCboInterestPeriodicity) {
		this.lstCboInterestPeriodicity = lstCboInterestPeriodicity;
	}

	/**
	 * Sets the lst cbo securitie class.
	 *
	 * @param lstCboSecuritieClass the new lst cbo securitie class
	 */
	public void setLstCboSecuritieClass(List<ParameterTable> lstCboSecuritieClass) {
		this.lstCboSecuritieClass = lstCboSecuritieClass;
	}

	/**
	 * Gets the lst cbo yield.
	 *
	 * @return the lst cbo yield
	 */
	public List<ParameterTable> getLstCboYield() {
		return lstCboYield;
	}

	/**
	 * Sets the lst cbo yield.
	 *
	 * @param lstCboYield the new lst cbo yield
	 */
	public void setLstCboYield(List<ParameterTable> lstCboYield) {
		this.lstCboYield = lstCboYield;
	}

	/**
	 * Gets the lst cbo cash nominal.
	 *
	 * @return the lst cbo cash nominal
	 */
	public List<ParameterTable> getLstCboCashNominal() {
		return lstCboCashNominal;
	}

	/**
	 * Sets the lst cbo cash nominal.
	 *
	 * @param lstCboCashNominal the new lst cbo cash nominal
	 */
	public void setLstCboCashNominal(List<ParameterTable> lstCboCashNominal) {
		this.lstCboCashNominal = lstCboCashNominal;
	}

	/**
	 * Gets the lst cbo amortization type.
	 *
	 * @return the lst cbo amortization type
	 */
	public List<ParameterTable> getLstCboAmortizationType() {
		return lstCboAmortizationType;
	}

	/**
	 * Sets the lst cbo amortization type.
	 *
	 * @param lstCboAmortizationType the new lst cbo amortization type
	 */
	public void setLstCboAmortizationType(
			List<ParameterTable> lstCboAmortizationType) {
		this.lstCboAmortizationType = lstCboAmortizationType;
	}

	/**
	 * Gets the lst cbo amortization on.
	 *
	 * @return the lst cbo amortization on
	 */
	public List<ParameterTable> getLstCboAmortizationOn() {
		return lstCboAmortizationOn;
	}

	/**
	 * Sets the lst cbo amortization on.
	 *
	 * @param lstCboAmortizationOn the new lst cbo amortization on
	 */
	public void setLstCboAmortizationOn(List<ParameterTable> lstCboAmortizationOn) {
		this.lstCboAmortizationOn = lstCboAmortizationOn;
	}

	/**
	 * Gets the lst cbo indexed to.
	 *
	 * @return the lst cbo indexed to
	 */
	public List<ParameterTable> getLstCboIndexedTo() {
		return lstCboIndexedTo;
	}

	/**
	 * Sets the lst cbo indexed to.
	 *
	 * @param lstCboIndexedTo the new lst cbo indexed to
	 */
	public void setLstCboIndexedTo(List<ParameterTable> lstCboIndexedTo) {
		this.lstCboIndexedTo = lstCboIndexedTo;
	}

	/**
	 * Gets the lst cbo investor type.
	 *
	 * @return the lst cbo investor type
	 */
	public List<ParameterTable> getLstCboInvestorType() {
		return lstCboInvestorType;
	}

	/**
	 * Sets the lst cbo investor type.
	 *
	 * @param lstCboInvestorType the new lst cbo investor type
	 */
	public void setLstCboInvestorType(List<ParameterTable> lstCboInvestorType) {
		this.lstCboInvestorType = lstCboInvestorType;
	}

	/**
	 * Gets the lst cbo financial index.
	 *
	 * @return the lst cbo financial index
	 */
	public List<ParameterTable> getLstCboFinancialIndex() {
		return lstCboFinancialIndex;
	}

	/**
	 * Sets the lst cbo financial index.
	 *
	 * @param lstCboFinancialIndex the new lst cbo financial index
	 */
	public void setLstCboFinancialIndex(List<ParameterTable> lstCboFinancialIndex) {
		this.lstCboFinancialIndex = lstCboFinancialIndex;
	}

	/**
	 * Gets the lst dtb international depository.
	 *
	 * @return the lst dtb international depository
	 */
	public List<InternationalDepository> getLstDtbInternationalDepository() {
		return lstDtbInternationalDepository;
	}

	/**
	 * Sets the lst dtb international depository.
	 *
	 * @param lstDtbInternationalDepository the new lst dtb international depository
	 */
	public void setLstDtbInternationalDepository(
			List<InternationalDepository> lstDtbInternationalDepository) {
		this.lstDtbInternationalDepository = lstDtbInternationalDepository;
	}

	/**
	 * Gets the aux vldtn international deposits.
	 *
	 * @return the aux vldtn international deposits
	 */
	public Integer getAuxVldtnInternationalDeposits() {
		return auxVldtnInternationalDeposits;
	}

	/**
	 * Sets the aux vldtn international deposits.
	 *
	 * @param auxVldtnInternationalDeposits the new aux vldtn international deposits
	 */
	public void setAuxVldtnInternationalDeposits(
			Integer auxVldtnInternationalDeposits) {
		this.auxVldtnInternationalDeposits = auxVldtnInternationalDeposits;
	}

	/**
	 * Gets the aux vldtn negotiation mechanism.
	 *
	 * @return the aux vldtn negotiation mechanism
	 */
	public Integer getAuxVldtnNegotiationMechanism() {
		return auxVldtnNegotiationMechanism;
	}

	/**
	 * Sets the aux vldtn negotiation mechanism.
	 *
	 * @param auxVldtnNegotiationMechanism the new aux vldtn negotiation mechanism
	 */
	public void setAuxVldtnNegotiationMechanism(Integer auxVldtnNegotiationMechanism) {
		this.auxVldtnNegotiationMechanism = auxVldtnNegotiationMechanism;
	}

	/**
	 * Gets the security t osearch.
	 *
	 * @return the security t osearch
	 */
	public SecurityTO getSecurityTOsearch() {
		return securityTOsearch;
	}

	/**
	 * Sets the security t osearch.
	 *
	 * @param securityTOsearch the new security t osearch
	 */
	public void setSecurityTOsearch(SecurityTO securityTOsearch) {
		this.securityTOsearch = securityTOsearch;
	}

	/**
	 * Gets the security data model.
	 *
	 * @return the security data model
	 */
	public SecurityDataModel getSecurityDataModel() {
		return securityDataModel;
	}

	/**
	 * Sets the security data model.
	 *
	 * @param securityDataModel the new security data model
	 */
	public void setSecurityDataModel(SecurityDataModel securityDataModel) {
		this.securityDataModel = securityDataModel;
	}

//	public SecurityTO getSecurityTOmgmt() {
//		return securityTOmgmt;
//	}
//
//	public void setSecurityTOmgmt(SecurityTO securityTOmgmt) {
//		this.securityTOmgmt = securityTOmgmt;
//	}

	/**
 * Gets the security history state.
 *
 * @return the security history state
 */
public SecurityHistoryState getSecurityHistoryState() {
		return securityHistoryState;
	}

	/**
	 * Sets the security history state.
	 *
	 * @param securityHistoryState the new security history state
	 */
	public void setSecurityHistoryState(SecurityHistoryState securityHistoryState) {
		this.securityHistoryState = securityHistoryState;
	}

	/**
	 * Gets the lst cbo security block motive.
	 *
	 * @return the lst cbo security block motive
	 */
	public List<ParameterTable> getLstCboSecurityBlockMotive() {
		return lstCboSecurityBlockMotive;
	}

	/**
	 * Sets the lst cbo security block motive.
	 *
	 * @param lstCboSecurityBlockMotive the new lst cbo security block motive
	 */
	public void setLstCboSecurityBlockMotive(
			List<ParameterTable> lstCboSecurityBlockMotive) {
		this.lstCboSecurityBlockMotive = lstCboSecurityBlockMotive;
	}

	/**
	 * Gets the lst cbo security unblock motive.
	 *
	 * @return the lst cbo security unblock motive
	 */
	public List<ParameterTable> getLstCboSecurityUnblockMotive() {
		return lstCboSecurityUnblockMotive;
	}

	/**
	 * Sets the lst cbo security unblock motive.
	 *
	 * @param lstCboSecurityUnblockMotive the new lst cbo security unblock motive
	 */
	public void setLstCboSecurityUnblockMotive(
			List<ParameterTable> lstCboSecurityUnblockMotive) {
		this.lstCboSecurityUnblockMotive = lstCboSecurityUnblockMotive;
	}

	/**
	 * Gets the lst cbo security motive aux.
	 *
	 * @return the lst cbo security motive aux
	 */
	public List<ParameterTable> getLstCboSecurityMotiveAux() {
		return lstCboSecurityMotiveAux;
	}

	/**
	 * Sets the lst cbo security motive aux.
	 *
	 * @param lstCboSecurityMotiveAux the new lst cbo security motive aux
	 */
	public void setLstCboSecurityMotiveAux(
			List<ParameterTable> lstCboSecurityMotiveAux) {
		this.lstCboSecurityMotiveAux = lstCboSecurityMotiveAux;
	}

	/**
	 * Gets the lst cbo credit rating scales.
	 *
	 * @return the lst cbo credit rating scales
	 */
	public List<ParameterTable> getLstCboCreditRatingScales() {
		return lstCboCreditRatingScales;
	}

	/**
	 * Sets the lst cbo credit rating scales.
	 *
	 * @param lstCboCreditRatingScales the new lst cbo credit rating scales
	 */
	public void setLstCboCreditRatingScales(
			List<ParameterTable> lstCboCreditRatingScales) {
		this.lstCboCreditRatingScales = lstCboCreditRatingScales;
	}

	/**
	 * Gets the program amortization coupon.
	 *
	 * @return the program amortization coupon
	 */
	public ProgramAmortizationCoupon getProgramAmortizationCoupon() {
		return programAmortizationCoupon;
	}

	public CommonsUtilities getCommonsUtilities() {
		return commonsUtilities;
	}

	public void setCommonsUtilities(CommonsUtilities commonsUtilities) {
		this.commonsUtilities = commonsUtilities;
	}

	/**
	 * Sets the program amortization coupon.
	 *
	 * @param programAmortizationCoupon the new program amortization coupon
	 */
	public void setProgramAmortizationCoupon(
			ProgramAmortizationCoupon programAmortizationCoupon) {
		this.programAmortizationCoupon = programAmortizationCoupon;
	}

	/**
	 * Gets the lst cbo economic activity.
	 *
	 * @return the lst cbo economic activity
	 */
	public List<ParameterTable> getLstCboEconomicActivity() {
		return lstCboEconomicActivity;
	}

	/**
	 * Sets the lst cbo economic activity.
	 *
	 * @param lstCboEconomicActivity the new lst cbo economic activity
	 */
	public void setLstCboEconomicActivity(
			List<ParameterTable> lstCboEconomicActivity) {
		this.lstCboEconomicActivity = lstCboEconomicActivity;
	}

	/**
	 * Gets the lst cbo economic sector.
	 *
	 * @return the lst cbo economic sector
	 */
	public List<ParameterTable> getLstCboEconomicSector() {
		return lstCboEconomicSector;
	}

	/**
	 * Sets the lst cbo economic sector.
	 *
	 * @param lstCboEconomicSector the new lst cbo economic sector
	 */
	public void setLstCboEconomicSector(List<ParameterTable> lstCboEconomicSector) {
		this.lstCboEconomicSector = lstCboEconomicSector;
	}

	/**
	 * Checks if is bl amortization for expiration.
	 *
	 * @return true, if is bl amortization for expiration
	 */
	public boolean isBlAmortizationForExpiration() {
		return blAmortizationForExpiration;
	}

	/**
	 * Sets the bl amortization for expiration.
	 *
	 * @param blAmortizationForExpiration the new bl amortization for expiration
	 */
	public void setBlAmortizationForExpiration(boolean blAmortizationForExpiration) {
		this.blAmortizationForExpiration = blAmortizationForExpiration;
	}

	/**
	 * Checks if is bl interest for expiration.
	 *
	 * @return true, if is bl interest for expiration
	 */
	public boolean isBlInterestForExpiration() {
		return blInterestForExpiration;
	}

	/**
	 * Sets the bl interest for expiration.
	 *
	 * @param blInterestForExpiration the new bl interest for expiration
	 */
	public void setBlInterestForExpiration(boolean blInterestForExpiration) {
		this.blInterestForExpiration = blInterestForExpiration;
	}

//	public boolean isBlShortTerm() {
//		return blShortTerm;
//	}
//
//	public void setBlShortTerm(boolean blShortTerm) {
//		this.blShortTerm = blShortTerm;
//	}
//
//	public boolean isBlLongTerm() {
//		return blLongTerm;
//	}
//
//	public void setBlLongTerm(boolean blLongTerm) {
//		this.blLongTerm = blLongTerm;
//	}

	/**
 * Gets the lst dtb negotiation modalities.
 *
 * @return the lst dtb negotiation modalities
 */
public List<NegotiationModality> getLstDtbNegotiationModalities() {
		return lstDtbNegotiationModalities;
	}

	/**
	 * Sets the lst dtb negotiation modalities.
	 *
	 * @param lstDtbNegotiationModalities the new lst dtb negotiation modalities
	 */
	public void setLstDtbNegotiationModalities(
			List<NegotiationModality> lstDtbNegotiationModalities) {
		this.lstDtbNegotiationModalities = lstDtbNegotiationModalities;
	}

	/**
	 * Gets the aux vldt investor type.
	 *
	 * @return the aux vldt investor type
	 */
	public Integer getAuxVldtInvestorType() {
		return auxVldtInvestorType;
	}

	/**
	 * Sets the aux vldt investor type.
	 *
	 * @param auxVldtInvestorType the new aux vldt investor type
	 */
	public void setAuxVldtInvestorType(Integer auxVldtInvestorType) {
		this.auxVldtInvestorType = auxVldtInvestorType;
	}

	/**
	 * Gets the aux vldt investor origin.
	 *
	 * @return the aux vldt investor origin
	 */
	public Integer getAuxVldtInvestorOrigin() {
		return auxVldtInvestorOrigin;
	}

	/**
	 * Sets the aux vldt investor origin.
	 *
	 * @param auxVldtInvestorOrigin the new aux vldt investor origin
	 */
	public void setAuxVldtInvestorOrigin(Integer auxVldtInvestorOrigin) {
		this.auxVldtInvestorOrigin = auxVldtInvestorOrigin;
	}

	/**
	 * Gets the lst cbo interest payment modality.
	 *
	 * @return the lst cbo interest payment modality
	 */
	public List<ParameterTable> getLstCboInterestPaymentModality() {
		return lstCboInterestPaymentModality;
	}

	/**
	 * Sets the lst cbo interest payment modality.
	 *
	 * @param lstCboInterestPaymentModality the new lst cbo interest payment modality
	 */
	public void setLstCboInterestPaymentModality(
			List<ParameterTable> lstCboInterestPaymentModality) {
		this.lstCboInterestPaymentModality = lstCboInterestPaymentModality;
	}

//	public BigDecimal getRateTypeValue() {
//		return rateTypeValue;
//	}
//
//	public void setRateTypeValue(BigDecimal rateTypeValue) {
//		this.rateTypeValue = rateTypeValue;
//	}

	/**
 * Gets the payment number amortization.
 *
 * @return the payment number amortization
 */
public Integer getPaymentNumberAmortization() {
		return paymentNumberAmortization;
	}

	/**
	 * Sets the payment number amortization.
	 *
	 * @param paymentNumberAmortization the new payment number amortization
	 */
	public void setPaymentNumberAmortization(Integer paymentNumberAmortization) {
		this.paymentNumberAmortization = paymentNumberAmortization;
	}

	/**
	 * Gets the lst cbo calendar month.
	 *
	 * @return the lst cbo calendar month
	 */
	public List<ParameterTable> getLstCboCalendarMonth() {
		return lstCboCalendarMonth;
	}

	/**
	 * Sets the lst cbo calendar month.
	 *
	 * @param lstCboCalendarMonth the new lst cbo calendar month
	 */
	public void setLstCboCalendarMonth(List<ParameterTable> lstCboCalendarMonth) {
		this.lstCboCalendarMonth = lstCboCalendarMonth;
	}

	/**
	 * Gets the _360_ calendar day.
	 *
	 * @return the _360_ calendar day
	 */
	public Integer get_360_CalendarDay() {
		return _360_CalendarDay;
	}

	/**
	 * Sets the _360_ calendar day.
	 *
	 * @param _360_CalendarDay the new _360_ calendar day
	 */
	public void set_360_CalendarDay(Integer _360_CalendarDay) {
		this._360_CalendarDay = _360_CalendarDay;
	}

	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * Gets the lst cbo financial periodicity.
	 *
	 * @return the lst cbo financial periodicity
	 */
	public List<ParameterTable> getLstCboFinancialPeriodicity() {
		return lstCboFinancialPeriodicity;
	}

	/**
	 * Sets the lst cbo financial periodicity.
	 *
	 * @param lstCboFinancialPeriodicity the new lst cbo financial periodicity
	 */
	public void setLstCboFinancialPeriodicity(
			List<ParameterTable> lstCboFinancialPeriodicity) {
		this.lstCboFinancialPeriodicity = lstCboFinancialPeriodicity;
	}

	/**
	 * Gets the security quote list.
	 *
	 * @return the security quote list
	 */
	public GenericDataModel<SecurityQuoteTO> getSecurityQuoteList() {
		return securityQuoteList;
	}

	/**
	 * Sets the security quote list.
	 *
	 * @param securityQuoteList the new security quote list
	 */
	public void setSecurityQuoteList(
			GenericDataModel<SecurityQuoteTO> securityQuoteList) {
		this.securityQuoteList = securityQuoteList;
	}

	/**
	 * Gets the logged issuer.
	 *
	 * @return the logged issuer
	 */
	public Issuer getLoggedIssuer() {
		return loggedIssuer;
	}

	/**
	 * Sets the logged issuer.
	 *
	 * @param loggedIssuer the new logged issuer
	 */
	public void setLoggedIssuer(Issuer loggedIssuer) {
		this.loggedIssuer = loggedIssuer;
	}

	/**
	 * Gets the lst cbo issuer document type.
	 *
	 * @return the lst cbo issuer document type
	 */
	public List<ParameterTable> getLstCboIssuerDocumentType() {
		return lstCboIssuerDocumentType;
	}

	/**
	 * Sets the lst cbo issuer document type.
	 *
	 * @param lstCboIssuerDocumentType the new lst cbo issuer document type
	 */
	public void setLstCboIssuerDocumentType(
			List<ParameterTable> lstCboIssuerDocumentType) {
		this.lstCboIssuerDocumentType = lstCboIssuerDocumentType;
	}

	/**
	 * Checks if is redirect from payment crono query.
	 *
	 * @return true, if is redirect from payment crono query
	 */
	public boolean isRedirectFromPaymentCronoQuery() {
		return redirectFromPaymentCronoQuery;
	}

	/**
	 * Sets the redirect from payment crono query.
	 *
	 * @param redirectFromPaymentCronoQuery the new redirect from payment crono query
	 */
	public void setRedirectFromPaymentCronoQuery(
			boolean redirectFromPaymentCronoQuery) {
		this.redirectFromPaymentCronoQuery = redirectFromPaymentCronoQuery;
	}

	/**
	 * Gets the lst cbo filter securitie class.
	 *
	 * @return the lst cbo filter securitie class
	 */
	public List<ParameterTable> getLstCboFilterSecuritieClass() {
		return lstCboFilterSecuritieClass;
	}

	/**
	 * Sets the lst cbo filter securitie class.
	 *
	 * @param lstCboFilterSecuritieClass the new lst cbo filter securitie class
	 */
	public void setLstCboFilterSecuritieClass(
			List<ParameterTable> lstCboFilterSecuritieClass) {
		this.lstCboFilterSecuritieClass = lstCboFilterSecuritieClass;
	}

	/**
	 * Gets the lst cbo filter currency.
	 *
	 * @return the lst cbo filter currency
	 */
	public List<ParameterTable> getLstCboFilterCurrency() {
		return lstCboFilterCurrency;
	}

	/**
	 * Sets the lst cbo filter currency.
	 *
	 * @param lstCboFilterCurrency the new lst cbo filter currency
	 */
	public void setLstCboFilterCurrency(List<ParameterTable> lstCboFilterCurrency) {
		this.lstCboFilterCurrency = lstCboFilterCurrency;
	}

	/**
	 * Gets the variable interest type code.
	 *
	 * @return the variable interest type code
	 */
	public Integer getVariableInterestTypeCode() {
		return variableInterestTypeCode;
	}

	/**
	 * Sets the variable interest type code.
	 *
	 * @param variableInterestTypeCode the new variable interest type code
	 */
	public void setVariableInterestTypeCode(Integer variableInterestTypeCode) {
		this.variableInterestTypeCode = variableInterestTypeCode;
	}

	/**
	 * Gets the lst cbo securitie term.
	 *
	 * @return the lst cbo securitie term
	 */
	public List<ParameterTable> getLstCboSecuritieTerm() {
		return lstCboSecuritieTerm;
	}

	/**
	 * Sets the lst cbo securitie term.
	 *
	 * @param lstCboSecuritieTerm the new lst cbo securitie term
	 */
	public void setLstCboSecuritieTerm(List<ParameterTable> lstCboSecuritieTerm) {
		this.lstCboSecuritieTerm = lstCboSecuritieTerm;
	}

	/**
	 * Gets the issuer helper search.
	 *
	 * @return the issuer helper search
	 */
	public Issuer getIssuerHelperSearch() {
		return issuerHelperSearch;
	}

	/**
	 * Sets the issuer helper search.
	 *
	 * @param issuerHelperSearch the new issuer helper search
	 */
	public void setIssuerHelperSearch(Issuer issuerHelperSearch) {
		this.issuerHelperSearch = issuerHelperSearch;
	}

	/**
	 * Gets the issuer helper mgmt.
	 *
	 * @return the issuer helper mgmt
	 */
	public Issuer getIssuerHelperMgmt() {
		return issuerHelperMgmt;
	}

	/**
	 * Sets the issuer helper mgmt.
	 *
	 * @param issuerHelperMgmt the new issuer helper mgmt
	 */
	public void setIssuerHelperMgmt(Issuer issuerHelperMgmt) {
		this.issuerHelperMgmt = issuerHelperMgmt;
	}

	/**
	 * Gets the issuance helper search.
	 *
	 * @return the issuance helper search
	 */
	public Issuance getIssuanceHelperSearch() {
		return issuanceHelperSearch;
	}

	/**
	 * Sets the issuance helper search.
	 *
	 * @param issuanceHelperSearch the new issuance helper search
	 */
	public void setIssuanceHelperSearch(Issuance issuanceHelperSearch) {
		this.issuanceHelperSearch = issuanceHelperSearch;
	}

	/**
	 * Gets the issuance helper mgmt.
	 *
	 * @return the issuance helper mgmt
	 */
	public Issuance getIssuanceHelperMgmt() {
		return issuanceHelperMgmt;
	}

	/**
	 * Sets the issuance helper mgmt.
	 *
	 * @param issuanceHelperMgmt the new issuance helper mgmt
	 */
	public void setIssuanceHelperMgmt(Issuance issuanceHelperMgmt) {
		this.issuanceHelperMgmt = issuanceHelperMgmt;
	}

	/**
	 * Gets the search filters concat.
	 *
	 * @return the search filters concat
	 */
	public String getSearchFiltersConcat() {
		return searchFiltersConcat;
	}

	/**
	 * Sets the search filters concat.
	 *
	 * @param searchFiltersConcat the new search filters concat
	 */
	public void setSearchFiltersConcat(String searchFiltersConcat) {
		this.searchFiltersConcat = searchFiltersConcat;
	}

	/**
	 * Gets the cero interest type code.
	 *
	 * @return the cero interest type code
	 */
	public Integer getCeroInterestTypeCode() {
		return ceroInterestTypeCode;
	}

	/**
	 * Sets the cero interest type code.
	 *
	 * @param ceroInterestTypeCode the new cero interest type code
	 */
	public void setCeroInterestTypeCode(Integer ceroInterestTypeCode) {
		this.ceroInterestTypeCode = ceroInterestTypeCode;
	}

	/**
	 * Gets the mechanism columns model.
	 *
	 * @return the mechanism columns model
	 */
	public List<GenericColumnModel> getMechanismColumnsModel() {
		return mechanismColumnsModel;
	}

	/**
	 * Sets the mechanism columns model.
	 *
	 * @param mechanismColumnsModel the new mechanism columns model
	 */
	public void setMechanismColumnsModel(
			List<GenericColumnModel> mechanismColumnsModel) {
		this.mechanismColumnsModel = mechanismColumnsModel;
	}

	/**
	 * Gets the valuator process class.
	 *
	 * @return the valuator process class
	 */
	public ValuatorProcessClass getValuatorProcessClass() {
		return valuatorProcessClass;
	}

	/**
	 * Sets the valuator process class.
	 *
	 * @param valuatorProcessClass the new valuator process class
	 */
	public void setValuatorProcessClass(ValuatorProcessClass valuatorProcessClass) {
		this.valuatorProcessClass = valuatorProcessClass;
	}

	/**
	 * Gets the valuator term range.
	 *
	 * @return the valuator term range
	 */
	public ValuatorTermRange getValuatorTermRange() {
		return valuatorTermRange;
	}

	/**
	 * Sets the valuator term range.
	 *
	 * @param valuatorTermRange the new valuator term range
	 */
	public void setValuatorTermRange(ValuatorTermRange valuatorTermRange) {
		this.valuatorTermRange = valuatorTermRange;
	}

	/**
	 * Gets the lst program interest diff.
	 *
	 * @return the lst program interest diff
	 */
	public List<Map<String, Object>> getLstProgramInterestDiff() {
		return lstProgramInterestDiff;
	}

	/**
	 * Sets the lst program interest diff.
	 *
	 * @param lstProgramInterestDiff the lst program interest diff
	 */
	public void setLstProgramInterestDiff(
			List<Map<String, Object>> lstProgramInterestDiff) {
		this.lstProgramInterestDiff = lstProgramInterestDiff;
	}

	/**
	 * Gets the lst program amortization diff.
	 *
	 * @return the lst program amortization diff
	 */
	public List<Map<String, Object>> getLstProgramAmortizationDiff() {
		return lstProgramAmortizationDiff;
	}

	/**
	 * Sets the lst program amortization diff.
	 *
	 * @param lstProgramAmortizationDiff the lst program amortization diff
	 */
	public void setLstProgramAmortizationDiff(
			List<Map<String, Object>> lstProgramAmortizationDiff) {
		this.lstProgramAmortizationDiff = lstProgramAmortizationDiff;
	}
	

	/**
	 * Gets the lst security selection.
	 *
	 * @return the lstSecuritySelection
	 */
	public List<Security> getLstSecuritySelection() {
		return lstSecuritySelection;
	}

	/**
	 * Sets the lst security selection.
	 *
	 * @param lstSecuritySelection the lstSecuritySelection to set
	 */
	public void setLstSecuritySelection(List<Security> lstSecuritySelection) {
		this.lstSecuritySelection = lstSecuritySelection;
	}

	/**
	 * Checks if is redirect from payment crono adv query.
	 *
	 * @return true, if is redirect from payment crono adv query
	 */
	public boolean isRedirectFromPaymentCronoAdvQuery() {
		return redirectFromPaymentCronoAdvQuery;
	}

	/**
	 * Sets the redirect from payment crono adv query.
	 *
	 * @param redirectFromPaymentCronoAdvQuery the new redirect from payment crono adv query
	 */
	public void setRedirectFromPaymentCronoAdvQuery(
			boolean redirectFromPaymentCronoAdvQuery) {
		this.redirectFromPaymentCronoAdvQuery = redirectFromPaymentCronoAdvQuery;
	}

	/**
	 * Gets the lst cbo convertible stock type.
	 *
	 * @return the lst cbo convertible stock type
	 */
	public List<ParameterTable> getLstCboConvertibleStockType() {
		return lstCboConvertibleStockType;
	}

	/**
	 * Sets the lst cbo convertible stock type.
	 *
	 * @param lstCboConvertibleStockType the new lst cbo convertible stock type
	 */
	public void setLstCboConvertibleStockType(
			List<ParameterTable> lstCboConvertibleStockType) {
		this.lstCboConvertibleStockType = lstCboConvertibleStockType;
	}

	/**
	 * Gets the lst cbo securitie class trg.
	 *
	 * @return the lst cbo securitie class trg
	 */
	public List<ParameterTable> getLstCboSecuritieClassTrg() {
		return lstCboSecuritieClassTrg;
	}

	/**
	 * Sets the lst cbo securitie class trg.
	 *
	 * @param lstCboSecuritieClassTrg the new lst cbo securitie class trg
	 */
	public void setLstCboSecuritieClassTrg(
			List<ParameterTable> lstCboSecuritieClassTrg) {
		this.lstCboSecuritieClassTrg = lstCboSecuritieClassTrg;
	}

	/**
	 * Gets the massive interface type.
	 *
	 * @return the massive interface type
	 */
	public String getMassiveInterfaceType() {
		return massiveInterfaceType;
	}

	/**
	 * Sets the massive interface type.
	 *
	 * @param massiveInterfaceType the new massive interface type
	 */
	public void setMassiveInterfaceType(String massiveInterfaceType) {
		this.massiveInterfaceType = massiveInterfaceType;
	}

	/**
	 * Checks if is exchange rate.
	 *
	 * @return true, if is exchange rate
	 */
	public boolean isExchangeRate() {
		return isExchangeRate;
	}

	/**
	 * Sets the exchange rate.
	 *
	 * @param isExchangeRate the new exchange rate
	 */
	public void setExchangeRate(boolean isExchangeRate) {
		this.isExchangeRate = isExchangeRate;
	}

	/**
	 * Checks if is bln is placement segment.
	 *
	 * @return the blnIsPlacementSegment
	 */
	public boolean isBlnIsPlacementSegment() {
		return blnIsPlacementSegment;
	}

	/**
	 * Sets the bln is placement segment.
	 *
	 * @param blnIsPlacementSegment the blnIsPlacementSegment to set
	 */
	public void setBlnIsPlacementSegment(boolean blnIsPlacementSegment) {
		this.blnIsPlacementSegment = blnIsPlacementSegment;
	}

	/**
	 * @return the blNotTrded
	 */
	public boolean isBlNotTrded() {
		return blNotTrded;
	}

	/**
	 * @param blNotTrded the blNotTrded to set
	 */
	public void setBlNotTrded(boolean blNotTrded) {
		this.blNotTrded = blNotTrded;
	}

	public List<GeographicLocation> getLstCboGeographicLocationDep() {
		return lstCboGeographicLocationDep;
	}

	public void setLstCboGeographicLocationDep(
			List<GeographicLocation> lstCboGeographicLocationDep) {
		this.lstCboGeographicLocationDep = lstCboGeographicLocationDep;
	}

	public boolean isDisableAgent() {
		return disableAgent;
	}

	public void setDisableAgent(boolean disableAgent) {
		this.disableAgent = disableAgent;
	}

	public List<PaymentAgent> getListPaymentAgents() {
		return listPaymentAgents;
	}

	public void setListPaymentAgents(List<PaymentAgent> listPaymentAgents) {
		this.listPaymentAgents = listPaymentAgents;
	}

	public List<SecuritySerialRange> getLstSecuritySerialRange() {
		return lstSecuritySerialRange;
	}

	public void setLstSecuritySerialRange(List<SecuritySerialRange> lstSecuritySerialRange) {
		this.lstSecuritySerialRange = lstSecuritySerialRange;
	}
	
	public SecuritySerialRange getSecuritySerialRangeSelected() {
		return securitySerialRangeSelected;
	}

	public void setSecuritySerialRangeSelected(SecuritySerialRange securitySerialRangeSelected) {
		this.securitySerialRangeSelected = securitySerialRangeSelected;
	}
	
	public List<PhysicalCertificateTO> getLstResult() {
		return lstResult;
	}

	public void setLstResult(List<PhysicalCertificateTO> lstResult) {
		this.lstResult = lstResult;
	}

	public List<ParameterTable> getLstCboEncoderAgent() {
		return lstCboEncoderAgent;
	}

	public void setLstCboEncoderAgent(List<ParameterTable> lstCboEncoderAgent) {
		this.lstCboEncoderAgent = lstCboEncoderAgent;
	}
	
}
