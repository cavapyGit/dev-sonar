package com.pradera.securities.issuancesecuritie.service;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.CorrelativeServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.component.VaultControlComponent;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.custody.payroll.PayrollDetail;
import com.pradera.model.custody.payroll.PayrollDetailStateType;
import com.pradera.model.custody.payroll.PayrollHeader;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationCupon;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.transfersecurities.DailySecurityClassBalance;
import com.pradera.model.custody.transfersecurities.TotalSecurityClassBalance;
import com.pradera.model.custody.type.DematerializationCertificateMotiveType;
import com.pradera.model.custody.type.PhysicalCertificateStateType;
import com.pradera.model.custody.type.SituationType;
import com.pradera.model.custody.type.VaultType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.PeriodType;
import com.pradera.model.issuancesecuritie.AmortizationPaymentSchedule;
import com.pradera.model.issuancesecuritie.InterestPaymentSchedule;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.IssuanceAux;
import com.pradera.model.issuancesecuritie.IssuanceCertificate;
import com.pradera.model.issuancesecuritie.IssuanceCertificateAux;
import com.pradera.model.issuancesecuritie.IssuanceFile;
import com.pradera.model.issuancesecuritie.IssuanceFileAux;
import com.pradera.model.issuancesecuritie.IssuanceObservation;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.ProgramAmortizationCoupon;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.SecurityInvestor;
import com.pradera.model.issuancesecuritie.SecurityNegotiationMechanism;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegment;
import com.pradera.model.issuancesecuritie.type.AmortizationOnType;
import com.pradera.model.issuancesecuritie.type.AmortizationType;
import com.pradera.model.issuancesecuritie.type.CalendarDayType;
import com.pradera.model.issuancesecuritie.type.CalendarMonthType;
import com.pradera.model.issuancesecuritie.type.CalendarType;
import com.pradera.model.issuancesecuritie.type.CapitalPaymentModalityType;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.InterestClassType;
import com.pradera.model.issuancesecuritie.type.InterestPaymentModalityType;
import com.pradera.model.issuancesecuritie.type.InterestPeriodicityType;
import com.pradera.model.issuancesecuritie.type.InterestType;
import com.pradera.model.issuancesecuritie.type.IssuanceAuxStateType;
import com.pradera.model.issuancesecuritie.type.IssuanceCertificateStateType;
import com.pradera.model.issuancesecuritie.type.IssuanceStateType;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.PaymentScheduleStateType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleSituationType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityInversionistStateType;
import com.pradera.model.issuancesecuritie.type.SecurityPlacementType;
import com.pradera.model.issuancesecuritie.type.SecuritySourceType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.issuancesecuritie.type.SecurityTermType;
import com.pradera.model.issuancesecuritie.type.SecurityType;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.securities.issuancesecuritie.facade.SecurityServiceFacade;
import com.pradera.securities.issuancesecuritie.to.IssuanceTO;
import com.pradera.securities.issuancesecuritie.util.SecuritiesUtils;
import com.pradera.securities.placementsegment.service.PlacementSegmentServiceBean;
import com.pradera.securities.placementsegment.to.PlacementSegmentTO;
import com.pradera.securities.query.to.IssuanceBalanceMovDetailResultTO;
import com.pradera.securities.query.to.IssuanceBalanceMovResultTO;
import com.pradera.securities.query.to.IssuanceBalanceMovTO;
import com.pradera.securities.issuer.to.PhysicalCertificateTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class IssuanceServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class IssuanceServiceBean extends CrudDaoServiceBean{
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The securities utils. */
	@Inject SecuritiesUtils securitiesUtils;
	
	/** The correlative service bean. */
	@EJB
	CorrelativeServiceBean correlativeServiceBean;
	
	/** The parameter service bean. */
	@EJB
	ParameterServiceBean parameterServiceBean;
	
	/** The placement segment service bean. */
	@EJB
	PlacementSegmentServiceBean placementSegmentServiceBean;
	
	/** The security service facade. */
	@EJB
	SecurityServiceFacade 	securityServiceFacade;
	
	
	
	/**
	 * Metodo para Buscar una lista de emisiones.
	 *
	 * @param issuanceTO the issuance to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Issuance> findIssuancesServiceBean(IssuanceTO issuanceTO) throws ServiceException{
		CriteriaBuilder cb=em.getCriteriaBuilder();
		CriteriaQuery<Issuance> cq=cb.createQuery(Issuance.class);
		Root<Issuance> issuanceRoot=cq.from(Issuance.class);
		issuanceRoot.fetch("issuer");
		issuanceRoot.fetch("geographicLocation");

		List<Predicate> criteriaParameters=new ArrayList<Predicate>();
		cq.select(issuanceRoot);
		cq.distinct(true);
		
		if(Validations.validateIsNotNullAndNotEmpty( issuanceTO.getIdIssuerPk() )){
			criteriaParameters.add(cb.equal(issuanceRoot.<String>get("issuer").get("idIssuerPk"), issuanceTO.getIdIssuerPk()));
		}
		if(Validations.validateIsNotNullAndNotEmpty( issuanceTO.getInstrumentType() ) && !issuanceTO.getInstrumentType().equals(0)){
			criteriaParameters.add(cb.equal(issuanceRoot.<Integer>get("instrumentType"), issuanceTO.getInstrumentType()));
		}
		if(Validations.validateIsNotNullAndNotEmpty( issuanceTO.getSecurityType() ) && !issuanceTO.getSecurityType().equals(0)){
			criteriaParameters.add(cb.equal(issuanceRoot.<Integer>get("securityType"), issuanceTO.getSecurityType()));
		}
		if(Validations.validateIsNotNullAndPositive(issuanceTO.getSecurityClass())){
			criteriaParameters.add(cb.equal(issuanceRoot.<Integer>get("securityClass"), issuanceTO.getSecurityClass()));
		}else if(Validations.validateListIsNotNullAndNotEmpty(issuanceTO.getLstSecurityClass())){
			criteriaParameters.add(cb.isTrue(issuanceRoot.<Integer>get("securityClass").in(issuanceTO.getLstSecurityClass())));
		}
		if(Validations.validateIsNotNullAndNotEmpty( issuanceTO.getIdIssuanceCodePk()  )){
			criteriaParameters.add(cb.like(issuanceRoot.<String>get("idIssuanceCodePk"), issuanceTO.getIdIssuanceCodePk()));
		}
		if(Validations.validateIsNotNullAndNotEmpty( issuanceTO.getDescription()  )){
			criteriaParameters.add(cb.like(issuanceRoot.<String>get("description"), "%" + issuanceTO.getDescription() + "%"));
		}
		if(Validations.validateIsNotNullAndNotEmpty( issuanceTO.getIssuanceState() ) && !issuanceTO.getIssuanceState().equals(0)){
			criteriaParameters.add(cb.equal(issuanceRoot.<Integer>get("stateIssuance"), issuanceTO.getIssuanceState()));
		}
		if(Validations.validateIsNotNullAndNotEmpty( issuanceTO.getIssuanceDateInitial() ) &&
				Validations.validateIsNotNullAndNotEmpty( issuanceTO.getIssuanceDateFinal() )){
			criteriaParameters.add(cb.between(issuanceRoot.<Date>get("registryDate"), issuanceTO.getIssuanceDateInitial(), CommonsUtilities.addDaysToDate(issuanceTO.getIssuanceDateFinal(), 1)));
		}
		cq.where( cb.and( criteriaParameters.toArray(new Predicate[criteriaParameters.size()]) ) );
		
		TypedQuery<Issuance> issuanceCriteria=em.createQuery(cq);
		return (List<Issuance>) issuanceCriteria.getResultList();
	}
	public List<IssuanceAux> findIssuancesAuxServiceBean(IssuanceTO issuanceTO) throws ServiceException{
		CriteriaBuilder cb=em.getCriteriaBuilder();
		CriteriaQuery<IssuanceAux> cq=cb.createQuery(IssuanceAux.class);
		Root<IssuanceAux> issuanceRoot=cq.from(IssuanceAux.class);
		issuanceRoot.fetch("issuer");
		issuanceRoot.fetch("geographicLocation");

		List<Predicate> criteriaParameters=new ArrayList<Predicate>();
		cq.select(issuanceRoot);
		cq.distinct(true);
		
		if(Validations.validateIsNotNullAndNotEmpty( issuanceTO.getIdIssuerPk() )){
			criteriaParameters.add(cb.equal(issuanceRoot.<String>get("issuer").get("idIssuerPk"), issuanceTO.getIdIssuerPk()));
		}
		if(Validations.validateIsNotNullAndNotEmpty( issuanceTO.getInstrumentType() ) && !issuanceTO.getInstrumentType().equals(0)){
			criteriaParameters.add(cb.equal(issuanceRoot.<Integer>get("instrumentType"), issuanceTO.getInstrumentType()));
		}
		if(Validations.validateIsNotNullAndNotEmpty( issuanceTO.getSecurityType() ) && !issuanceTO.getSecurityType().equals(0)){
			criteriaParameters.add(cb.equal(issuanceRoot.<Integer>get("securityType"), issuanceTO.getSecurityType()));
		}
		if(Validations.validateIsNotNullAndPositive(issuanceTO.getSecurityClass())){
			criteriaParameters.add(cb.equal(issuanceRoot.<Integer>get("securityClass"), issuanceTO.getSecurityClass()));
		}else if(Validations.validateListIsNotNullAndNotEmpty(issuanceTO.getLstSecurityClass())){
			criteriaParameters.add(cb.isTrue(issuanceRoot.<Integer>get("securityClass").in(issuanceTO.getLstSecurityClass())));
		}
		if(Validations.validateIsNotNullAndNotEmpty( issuanceTO.getIdIssuanceCodePk()  )){
			criteriaParameters.add(cb.like(issuanceRoot.<String>get("idIssuanceCodePk"), issuanceTO.getIdIssuanceCodePk()));
		}
		if(Validations.validateIsNotNullAndNotEmpty( issuanceTO.getDescription()  )){
			criteriaParameters.add(cb.like(issuanceRoot.<String>get("description"), "%" + issuanceTO.getDescription() + "%"));
		}
		if(Validations.validateIsNotNullAndNotEmpty( issuanceTO.getIssuanceState() ) && !issuanceTO.getIssuanceState().equals(0)){
			criteriaParameters.add(cb.equal(issuanceRoot.<Integer>get("stateChk"), issuanceTO.getIssuanceState()));
		}
		if(Validations.validateIsNotNullAndNotEmpty( issuanceTO.getIssuanceDateInitial() ) &&
				Validations.validateIsNotNullAndNotEmpty( issuanceTO.getIssuanceDateFinal() )){
			criteriaParameters.add(cb.between(issuanceRoot.<Date>get("registryDate"), issuanceTO.getIssuanceDateInitial(), CommonsUtilities.addDaysToDate(issuanceTO.getIssuanceDateFinal(), 1)));
		}
		criteriaParameters.add(cb.notEqual(issuanceRoot.<Integer>get("stateChk"), IssuanceAuxStateType.CONFIRMED.getCode()));		                                                                          
		
		cq.where( cb.and( criteriaParameters.toArray(new Predicate[criteriaParameters.size()]) ) );
		TypedQuery<IssuanceAux> issuanceCriteria=em.createQuery(cq);
		return (List<IssuanceAux>) issuanceCriteria.getResultList();
	}
	
	/**
	 * Metodo para Buscar un emision.
	 *
	 * @param issuanceTO the issuance to
	 * @return the issuance
	 * @throws ServiceException the service exception
	 */
	public Issuance findIssuanceServiceBean(IssuanceTO issuanceTO) throws ServiceException{
		CriteriaBuilder cb=em.getCriteriaBuilder();
		CriteriaQuery<Issuance> cq=cb.createQuery(Issuance.class);
		Root<Issuance> issuanceRoot=cq.from(Issuance.class);
		issuanceRoot.fetch("issuer");
		issuanceRoot.fetch("geographicLocation");
		
		List<Predicate> criteriaParameters=new ArrayList<Predicate>();
		cq.select(issuanceRoot);
		
		if(Validations.validateIsNotNullAndNotEmpty( issuanceTO.getIdIssuanceCodePk())){
			criteriaParameters.add(cb.equal(issuanceRoot.<String>get("idIssuanceCodePk"), issuanceTO.getIdIssuanceCodePk()));
		}

		cq.where( cb.and( criteriaParameters.toArray(new Predicate[criteriaParameters.size()]) ) );

		Issuance issuance=null;
		TypedQuery<Issuance> issuanceCriteria=em.createQuery(cq);
		try {
			issuance = issuanceCriteria.getSingleResult();
		} catch(NonUniqueResultException nuex){
			log.info("Issuance have more codes same!");
		} 
		catch(NoResultException nex) {
			log.info("Issuance not found");
		}

		if(issuance!=null){
			issuance.setAmountRegisteredSegment(BigDecimal.ZERO);
			issuance.setRegisteredTranches(GeneralConstants.ZERO_VALUE_INTEGER);
			issuance.setConfirmedTranches( GeneralConstants.ZERO_VALUE_INTEGER );
			
			SecurityTO securityTO=new SecurityTO();
			securityTO.setIdIssuanceCodePk( issuance.getIdIssuanceCodePk() );
			issuance.setSecuritiesInNegotiations( securityServiceFacade.securityInNegotiations(securityTO) );
			
			Map<String,Object> params=new HashMap<String, Object>();
			params.put("idIssuanceCodePrm", issuance.getIdIssuanceCodePk());
			
			Integer securitiesRegistered = Integer.parseInt(parameterServiceBean.findObjectByNamedQuery(
					Security.SECURITIES_BY_ISSUANCE, params).toString());
			issuance.setSecuritiesRegisted( securitiesRegistered>0 );
			
			if(issuanceTO.isNeedPlaceSegments()){
				PlacementSegmentTO placementSegmentTO=new PlacementSegmentTO();
				placementSegmentTO.setIdIssuancePk( issuance.getIdIssuanceCodePk() );
				List<PlacementSegment> lstPlacementSegment=(List<PlacementSegment>)placementSegmentServiceBean.getListPlacementSegmentService(placementSegmentTO);
			
				issuance.setAmountRegisteredSegment(BigDecimal.ZERO);
				issuance.setRegisteredTranches(GeneralConstants.ZERO_VALUE_INTEGER);
				issuance.setAmountConfirmedSegment( BigDecimal.ZERO);
				issuance.setConfirmedTranches(GeneralConstants.ZERO_VALUE_INTEGER);
				issuance.setClosedTranches( GeneralConstants.ZERO_VALUE_INTEGER );
				issuance.setAmountOpenedSegment(BigDecimal.ZERO);
				issuance.setValidTranches( GeneralConstants.ZERO_VALUE_INTEGER );
				issuance.setOpenedTranches(GeneralConstants.ZERO_VALUE_INTEGER );
				
				for(PlacementSegment placSeg : lstPlacementSegment){
					
					issuance.setValidTranches( issuance.getValidTranches()+1 );
					
					if(placSeg.isRegisteredState()){
						issuance.setAmountRegisteredSegment( issuance.getAmountRegisteredSegment().add( placSeg.getAmountToPlace() ) );
						issuance.setRegisteredTranches( issuance.getRegisteredTranches()+1 );
					}
					if(placSeg.isConfirmedState()){
						issuance.setAmountConfirmedSegment( issuance.getAmountConfirmedSegment().add( placSeg.getAmountToPlace() ) );
						issuance.setConfirmedTranches( issuance.getConfirmedTranches()+1 );
					}
					if(placSeg.isClosedState()){
						issuance.setClosedTranches( issuance.getClosedTranches()+1 );
					}
					if(placSeg.isOpenedState()){
						issuance.setAmountOpenedSegment( issuance.getAmountOpenedSegment().add( placSeg.getAmountToPlace() ) );
						issuance.setOpenedTranches( issuance.getOpenedTranches()+1 );
					}
				}		
			}
			
//			Map<String, Object> params = new HashMap<String, Object>();
//			params.put("idIssuancePrm", issuance.getIdIssuanceCodePk());
//			params.put("statePrm", PlacementSegmentStateType.REGISTERED.getCode());
//			Object[] result= parameterServiceBean.findObjectByNamedQuery(PlacementSegment.PLAC_ISSUANCE_DETAILS, params);
//		
//			Integer resultI=result[0]==null ? null :Integer.parseInt(result[0].toString()) ; 
//			BigDecimal resultB=result[1]==null ? null : BigDecimal.valueOf( Double.parseDouble( result[0].toString() ) ) ; 
//			
//			if(Validations.validateIsNotNullAndPositive(resultI)){
//				issuance.setRegisteredTranches( issuance.getRegisteredTranches()+resultI);
//			}
//			if(Validations.validateIsNotNullAndPositive( resultB )){
//				issuance.setAmountRegisteredSegment( issuance.getAmountRegisteredSegment().add(resultB) );
//			}
//			params.put("statePrm", PlacementSegmentStateType.CONFIRMED.getCode());
//			result= parameterServiceBean.findObjectByNamedQuery(PlacementSegment.PLAC_ISSUANCE_DETAILS, params);
//			resultI=result[0]==null ? null :Integer.parseInt(result[0].toString()) ; 
//			if(Validations.validateIsNotNullAndPositive(resultI)){
//				issuance.setConfirmedTranches( issuance.getConfirmedTranches()+resultI);
//			}
		}
		//Some cases is necesary get Issuance with all securities

		//END - Some cases is necesary get Issuance with all securities
		if(issuance!=null && issuanceTO.isNeedHolder()){
			issuance.getIssuer().getHolder().toString();
		}
		return issuance;
	}	
	public IssuanceAux findIssuanceAuxServiceBean(IssuanceTO issuanceTO) throws ServiceException{
		CriteriaBuilder cb=em.getCriteriaBuilder();
		CriteriaQuery<IssuanceAux> cq=cb.createQuery(IssuanceAux.class);
		Root<IssuanceAux> issuanceRoot=cq.from(IssuanceAux.class);
		issuanceRoot.fetch("issuer");
		issuanceRoot.fetch("geographicLocation");
		
		List<Predicate> criteriaParameters=new ArrayList<Predicate>();
		cq.select(issuanceRoot);
		
		if(Validations.validateIsNotNullAndNotEmpty( issuanceTO.getIdIssuanceCodePk())){
			criteriaParameters.add(cb.equal(issuanceRoot.<String>get("idIssuanceCodePk"), issuanceTO.getIdIssuanceCodePk()));
		}

		cq.where( cb.and( criteriaParameters.toArray(new Predicate[criteriaParameters.size()]) ) );

		IssuanceAux issuance=null;
		TypedQuery<IssuanceAux> issuanceCriteria=em.createQuery(cq);
		try {
			issuance = issuanceCriteria.getSingleResult();
		} catch(NonUniqueResultException nuex){
			log.info("Issuance have more codes same!");
		} 
		catch(NoResultException nex) {
			log.info("Issuance not found");
		}

		if(issuance!=null){
			issuance.setAmountRegisteredSegment(BigDecimal.ZERO);
			issuance.setRegisteredTranches(GeneralConstants.ZERO_VALUE_INTEGER);
			issuance.setConfirmedTranches( GeneralConstants.ZERO_VALUE_INTEGER );
			
			SecurityTO securityTO=new SecurityTO();
			securityTO.setIdIssuanceCodePk( issuance.getIdIssuanceCodePk() );
			issuance.setSecuritiesInNegotiations( securityServiceFacade.securityInNegotiations(securityTO) );
			
			Map<String,Object> params=new HashMap<String, Object>();
			params.put("idIssuanceCodePrm", issuance.getIdIssuanceCodePk());
			
			Integer securitiesRegistered = Integer.parseInt(parameterServiceBean.findObjectByNamedQuery(
					Security.SECURITIES_BY_ISSUANCE, params).toString());
			issuance.setSecuritiesRegisted( securitiesRegistered>0 );
			
			if(issuanceTO.isNeedPlaceSegments()){
				PlacementSegmentTO placementSegmentTO=new PlacementSegmentTO();
				placementSegmentTO.setIdIssuancePk( issuance.getIdIssuanceCodePk() );
				List<PlacementSegment> lstPlacementSegment=(List<PlacementSegment>)placementSegmentServiceBean.getListPlacementSegmentService(placementSegmentTO);
			
				issuance.setAmountRegisteredSegment(BigDecimal.ZERO);
				issuance.setRegisteredTranches(GeneralConstants.ZERO_VALUE_INTEGER);
				issuance.setAmountConfirmedSegment( BigDecimal.ZERO);
				issuance.setConfirmedTranches(GeneralConstants.ZERO_VALUE_INTEGER);
				issuance.setClosedTranches( GeneralConstants.ZERO_VALUE_INTEGER );
				issuance.setAmountOpenedSegment(BigDecimal.ZERO);
				issuance.setValidTranches( GeneralConstants.ZERO_VALUE_INTEGER );
				issuance.setOpenedTranches(GeneralConstants.ZERO_VALUE_INTEGER );
				
				for(PlacementSegment placSeg : lstPlacementSegment){
					
					issuance.setValidTranches( issuance.getValidTranches()+1 );
					
					if(placSeg.isRegisteredState()){
						issuance.setAmountRegisteredSegment( issuance.getAmountRegisteredSegment().add( placSeg.getAmountToPlace() ) );
						issuance.setRegisteredTranches( issuance.getRegisteredTranches()+1 );
					}
					if(placSeg.isConfirmedState()){
						issuance.setAmountConfirmedSegment( issuance.getAmountConfirmedSegment().add( placSeg.getAmountToPlace() ) );
						issuance.setConfirmedTranches( issuance.getConfirmedTranches()+1 );
					}
					if(placSeg.isClosedState()){
						issuance.setClosedTranches( issuance.getClosedTranches()+1 );
					}
					if(placSeg.isOpenedState()){
						issuance.setAmountOpenedSegment( issuance.getAmountOpenedSegment().add( placSeg.getAmountToPlace() ) );
						issuance.setOpenedTranches( issuance.getOpenedTranches()+1 );
					}
				}		
			}
		}
		//Some cases is necesary get Issuance with all securities
		//END - Some cases is necesary get Issuance with all securities
		if(issuance!=null && issuanceTO.isNeedHolder()){
			issuance.getIssuer().getHolder().toString();
		}
		return issuance;
	}
	
	/**
	 * Metodo para Buscar una lista de certificados de emision.
	 *
	 * @param idIssuanceCodePkPrm the id issuance code pk prm
	 * @param issuanceCertificateState the issuance certificate state
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<IssuanceCertificate> findIssuanceCertificatesServiceBean(String idIssuanceCodePkPrm, Integer issuanceCertificateState) throws ServiceException{
		StringBuilder strbQuery=new StringBuilder();
		strbQuery.append("Select I From IssuanceCertificate I where I.issuance.idIssuanceCodePk=:idIssuanceCodePkPrm ");
		if(issuanceCertificateState!=null){
			strbQuery.append(" and I.stateCertificate=:issuanceCertSatePrm");
		}
		Query query=em.createQuery(strbQuery.toString());
		query.setParameter("idIssuanceCodePkPrm", idIssuanceCodePkPrm);
		if(issuanceCertificateState!=null){
			query.setParameter("issuanceCertSatePrm", issuanceCertificateState);
		}
		return (List<IssuanceCertificate>)query.getResultList();
	}
	
	/**
	 * Metodo para Buscar una lista de archivos de emision.
	 *
	 * @param idIssuanceCodePkPrm the id issuance code pk prm
	 * @param issuanceFileState the issuance file state
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<IssuanceFile> findIssuanceFilesServiceBean(String idIssuanceCodePkPrm, Integer issuanceFileState) throws ServiceException{
		StringBuilder strbQuery=new StringBuilder();
		strbQuery.append("Select I From IssuanceFile I where I.issuance.idIssuanceCodePk=:idIssuanceCodePkPrm ");
		if(issuanceFileState!=null){
			strbQuery.append(" and I.documentState=:issuanceCertSatePrm");
		}
		Query query=em.createQuery(strbQuery.toString());
		query.setParameter("idIssuanceCodePkPrm", idIssuanceCodePkPrm);
		if(issuanceFileState!=null){
			query.setParameter("issuanceCertSatePrm", issuanceFileState);
		}
		return (List<IssuanceFile>)query.getResultList();
	}
	@SuppressWarnings("unchecked")
	public List<IssuanceFileAux> findIssuanceFilesAuxServiceBean(String idIssuanceCodePkPrm, Integer issuanceFileState) throws ServiceException{
		StringBuilder strbQuery=new StringBuilder();
		strbQuery.append("Select I From IssuanceFileAux I where I.issuance.idIssuanceCodePk=:idIssuanceCodePkPrm ");
		if(issuanceFileState!=null){
			strbQuery.append(" and I.documentState=:issuanceCertSatePrm");
		}
		Query query=em.createQuery(strbQuery.toString());
		query.setParameter("idIssuanceCodePkPrm", idIssuanceCodePkPrm);
		if(issuanceFileState!=null){
			query.setParameter("issuanceCertSatePrm", issuanceFileState);
		}
		return (List<IssuanceFileAux>)query.getResultList();
	}
	@SuppressWarnings("unchecked")
	public List<IssuanceObservation> findIssuanceObsService (String idIssuanceCodePk) throws ServiceException{
		StringBuilder strbQuery=new StringBuilder();
		strbQuery.append("Select I From IssuanceObservation I where I.issuance.idIssuanceCodePk=:idIssuanceCodePkPrm ");
		
		Query query=em.createQuery(strbQuery.toString());
		query.setParameter("idIssuanceCodePkPrm", idIssuanceCodePk);
		
		return (List<IssuanceObservation>)query.getResultList();
	}
	
public List<PhysicalCertificateTO> searchPhysicalCerticate(String to) throws ServiceException{
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT  "); 
		sbQuery.append("    subt.fecha_ingreso, ");
		sbQuery.append("    (SELECT pt.DESCRIPTION FROM PARAMETER_TABLE pt WHERE pt.PARAMETER_TABLE_PK = subt.motive) AS motivo_ingreso, 				");
		sbQuery.append("	(SELECT LISTAGG(had.ID_HOLDER_FK, '-') WITHIN GROUP (ORDER BY had.ID_HOLDER_FK) 											");
		sbQuery.append("	FROM HOLDER_ACCOUNT_DETAIL had WHERE had.ID_HOLDER_ACCOUNT_FK =subt.ID_HOLDER_ACCOUNT_FK) AS RUT, 							");
		sbQuery.append("	(SELECT ha.ACCOUNT_NUMBER FROM HOLDER_ACCOUNT ha WHERE ha.ID_HOLDER_ACCOUNT_PK = subt.ID_HOLDER_ACCOUNT_FK) AS CUENTA, 		");
		sbQuery.append("	FN_GET_HOLDER_DESC(subt.ID_HOLDER_ACCOUNT_FK, '-',0) AS NOBRE_CLIENTE, 														");
		sbQuery.append("	(SELECT pt.PARAMETER_NAME FROM PARAMETER_TABLE pt WHERE pt.PARAMETER_TABLE_PK =subt.security_class) AS tipo_valor, 			");
		sbQuery.append("	subt.ID_SECURITY_CODE_FK AS codigo_valor, 																					"); 
		sbQuery.append("	(subt.SERIAL_NUMBER) AS serie, 																								");
		sbQuery.append("	CASE WHEN subt.cupon_electronico = 1 THEN 'SI' WHEN (subt.cupon_electronico = 0 AND subt.numero_cupon IS NULL) THEN NULL 	"); 
		sbQuery.append("	ELSE 'NO' END AS cupon_electronico, 																						");
		sbQuery.append("	(SELECT i.MNEMONIC FROM ISSUER i WHERE i.ID_ISSUER_PK=subt.ID_ISSUER_FK)  AS emisor, 										");
		sbQuery.append("	(SELECT pt.DESCRIPTION FROM PARAMETER_TABLE pt WHERE pt.PARAMETER_TABLE_PK=subt.security_currency) AS moneda, 				");
		sbQuery.append("	subt.SECURITY_NOMINAL_VALUE AS valor_nominal, 																				");
		sbQuery.append("	subt.EXPIRATION_DATE AS fecha_vencimiento, 																					");
		sbQuery.append("    CASE WHEN subt.quantity IS NULL THEN 1 ELSE subt.quantity END AS CANTIDAD, 													");
		sbQuery.append("	subt.CERTIFICATE_FROM AS desde , subt.CERTIFICATE_TO AS hasta, subt.REFERENCE_PART_CODE AS observacion, 					");
		sbQuery.append("	subt.cupon_electronico as electronico, 																						");
		sbQuery.append("	subt.numero_cupon as cuponNumber,																							");
		sbQuery.append("	subt.ID_ANNOTATION_OPERATION_PK as idAnnotationOperation																	");
		sbQuery.append("	FROM 																														");
		sbQuery.append("	(																															");
		sbQuery.append("	SELECT aao.ID_ANNOTATION_OPERATION_PK, aao.ID_SECURITY_CODE_FK , aao.ID_PHYSICAL_CERTIFICATE_FK, aao.SERIAL_NUMBER, aao.CERTIFICATE_NUMBER, NULL AS numero_cupon, 0 AS cupon_electronico, ");  
		sbQuery.append("	(cb.ARMARIO ||'-'|| cb.NIVEL ||'-'|| cb.CAJA ||'-'|| cb.FOLIO ||'-'|| cb.INDICE_CAJA) AS boveda , aao.ID_ISSUER_FK,			");
		sbQuery.append("	aao.ID_HOLDER_ACCOUNT_FK,																									");
		sbQuery.append("	cb.REGISTRY_DATE AS fecha_ingreso , aao.motive AS motive, pc.EXPIRATION_DATE, 												");
		sbQuery.append("	(aao.CERTIFICATE_TO - aao.CERTIFICATE_FROM + 1) AS quantity, 																");
		sbQuery.append("	aao.CERTIFICATE_FROM, aao.CERTIFICATE_TO, aao.REFERENCE_PART_CODE, aao.SECURITY_NOMINAL_VALUE , 							");
		sbQuery.append("	aao.security_class, 																										");
		sbQuery.append("	aao.security_currency, 																										");
		sbQuery.append("	ha2.ID_PARTICIPANT_FK 																										");
		sbQuery.append("	FROM ACCOUNT_ANNOTATION_OPERATION aao  																						");
		sbQuery.append("	INNER JOIN PHYSICAL_CERTIFICATE pc ON pc.ID_PHYSICAL_CERTIFICATE_PK = aao.ID_PHYSICAL_CERTIFICATE_FK 						");
		sbQuery.append("	INNER JOIN CONTROL_BOVEDA cb ON cb.ID_SECURITY_CODE_FK = aao.ID_SECURITY_CODE_FK AND cb.ID_PHYSICAL_CERTIFICATE_FK = aao.ID_PHYSICAL_CERTIFICATE_FK ");
		sbQuery.append("	INNER JOIN HOLDER_ACCOUNT ha2 ON aao.ID_HOLDER_ACCOUNT_FK = ha2.ID_HOLDER_ACCOUNT_PK 										");
		sbQuery.append("	WHERE 																														");
		sbQuery.append("	aao.ID_SECURITY_CODE_FK IS NOT NULL AND aao.STATE_ANNOTATION = 858 AND cb.ID_PHYSICAL_CERTIFICATE_FK IS NOT null 			");
		sbQuery.append("	UNION 																														");
		sbQuery.append("	SELECT aao.ID_ANNOTATION_OPERATION_PK, aao.ID_SECURITY_CODE_FK , aao.ID_PHYSICAL_CERTIFICATE_FK, aao.SERIAL_NUMBER, aao.CERTIFICATE_NUMBER, aac.COUPON_NUMBER, aao.IND_ELECTRONIC_CUPON , "); 
		sbQuery.append("	(cb.ARMARIO ||'-'|| cb.NIVEL ||'-'|| cb.CAJA ||'-'|| cb.FOLIO ||'-'|| cb.INDICE_CAJA) AS boveda, aao.ID_ISSUER_FK, 			");
		sbQuery.append("	aao.ID_HOLDER_ACCOUNT_FK, 																									");
		sbQuery.append("	cb.REGISTRY_DATE, aao.motive AS motive, aac.EXPIRATION_DATE,  																");
		sbQuery.append("	(aao.CERTIFICATE_TO - aao.CERTIFICATE_FROM + 1) AS quantity, 																");
		sbQuery.append("	aao.CERTIFICATE_FROM, aao.CERTIFICATE_TO, aao.REFERENCE_PART_CODE, aac.CUPON_AMOUNT, 										");
		sbQuery.append("	aao.security_class, 																										");
		sbQuery.append("	aao.security_currency, 																										");
		sbQuery.append("	ha2.ID_PARTICIPANT_FK 																										");
		sbQuery.append("	FROM ACCOUNT_ANNOTATION_OPERATION aao 																						");
		sbQuery.append("	INNER JOIN ACCOUNT_ANNOTATION_CUPON aac ON aac.ID_ANNOTATION_OPERATION_FK = aao.ID_ANNOTATION_OPERATION_PK 					");
		sbQuery.append("	INNER JOIN PHYSICAL_CERTIFICATE pc ON pc.ID_PHYSICAL_CERTIFICATE_PK = aao.ID_PHYSICAL_CERTIFICATE_FK 						");
		sbQuery.append("	INNER JOIN CONTROL_BOVEDA cb ON cb.ID_SECURITY_CODE_FK = aao.ID_SECURITY_CODE_FK AND cb.COUPON_NUMBER = aac.COUPON_NUMBER  	");
		sbQuery.append("	INNER JOIN HOLDER_ACCOUNT ha2 ON aao.ID_HOLDER_ACCOUNT_FK = ha2.ID_HOLDER_ACCOUNT_PK 										");
		sbQuery.append("	WHERE  																														");
		sbQuery.append("	aao.ID_SECURITY_CODE_FK IS NOT NULL AND aao.STATE_ANNOTATION = 858  														");
		sbQuery.append("	AND aao.IND_ELECTRONIC_CUPON = 0 AND aao.IND_SERIALIZABLE = 0 AND cb.ID_PHYSICAL_CERTIFICATE_FK IS NOT null 				");
		sbQuery.append("	UNION  																														");
		sbQuery.append("	SELECT aao.ID_ANNOTATION_OPERATION_PK, aao.ID_SECURITY_CODE_FK , aao.ID_PHYSICAL_CERTIFICATE_FK, aao.SERIAL_NUMBER, aao.CERTIFICATE_NUMBER, aac.COUPON_NUMBER, aao.IND_ELECTRONIC_CUPON, ");  
		sbQuery.append("	NULL AS boveda , aao.ID_ISSUER_FK, 																							");
		sbQuery.append("	aao.ID_HOLDER_ACCOUNT_FK, 																									");
		sbQuery.append("	cb.REGISTRY_DATE , aao.motive AS motive, aac.EXPIRATION_DATE,																");
		sbQuery.append("	(aao.CERTIFICATE_TO - aao.CERTIFICATE_FROM + 1) AS quantity, 																");
		sbQuery.append("	aao.CERTIFICATE_FROM, aao.CERTIFICATE_TO, aao.REFERENCE_PART_CODE, aac.CUPON_AMOUNT,  										");
		sbQuery.append("	aao.security_class, 																										");
		sbQuery.append("	aao.security_currency, 																										");
		sbQuery.append("	ha2.ID_PARTICIPANT_FK  																										");
		sbQuery.append("	FROM ACCOUNT_ANNOTATION_OPERATION aao 																						");
		sbQuery.append("	INNER JOIN ACCOUNT_ANNOTATION_CUPON aac ON aac.ID_ANNOTATION_OPERATION_FK = aao.ID_ANNOTATION_OPERATION_PK  				");
		sbQuery.append("	INNER JOIN PHYSICAL_CERTIFICATE pc ON pc.ID_PHYSICAL_CERTIFICATE_PK = aao.ID_PHYSICAL_CERTIFICATE_FK  						");
		sbQuery.append("	INNER JOIN CONTROL_BOVEDA cb ON cb.ID_SECURITY_CODE_FK = aao.ID_SECURITY_CODE_FK AND cb.ID_PHYSICAL_CERTIFICATE_FK = aao.ID_PHYSICAL_CERTIFICATE_FK ");
		sbQuery.append("	INNER JOIN HOLDER_ACCOUNT ha2 ON aao.ID_HOLDER_ACCOUNT_FK = ha2.ID_HOLDER_ACCOUNT_PK  										");
		sbQuery.append("	WHERE 																														");
		sbQuery.append("	aao.ID_SECURITY_CODE_FK IS NOT NULL AND aao.STATE_ANNOTATION = 858  														");
		sbQuery.append("	AND aao.IND_ELECTRONIC_CUPON = 1 AND aao.IND_SERIALIZABLE = 0 AND cb.ID_PHYSICAL_CERTIFICATE_FK IS NOT null  				");
		sbQuery.append("	) subt 																														");
		sbQuery.append("	WHERE 1 = 1 																												");
		
		if(Validations.validateIsNotNull(to)) {
			sbQuery.append("  AND subt.ID_SECURITY_CODE_FK = :getIdSecurityCodePk");
		}
		
		sbQuery.append("	ORDER BY subt.ID_ANNOTATION_OPERATION_PK DESC, subt.numero_cupon ASC NULLS FIRST  ");

		 		
		Query query = em.createNativeQuery(sbQuery.toString());
		

		if(Validations.validateIsNotNull(to) ) {
			query.setParameter("getIdSecurityCodePk", to);
		}
		
		List<Object[]> lstResult = query.getResultList();
		List<PhysicalCertificateTO> listPhysicalCertificateTO = new ArrayList<>();
		
        for (Object[] objResult:lstResult) {
        	PhysicalCertificateTO physicalCertificateTO = new PhysicalCertificateTO();
        	if(objResult[0]!=null) {
        		physicalCertificateTO.setEntryDate((Date)objResult[0]);
        	}
        	if(objResult[1]!=null) {
        		physicalCertificateTO.setMotiveDescription(Validations.validateIsNotNull(objResult[1]) ? objResult[1].toString() : "");
        	}
        	if(objResult[2]!=null) {
        		physicalCertificateTO.setRut(Validations.validateIsNotNull(objResult[2]) ? objResult[2].toString() : "");
        	}
        	if(objResult[3]!=null) {
        		physicalCertificateTO.setAccountNumber(new Integer(objResult[3].toString()));
        	}
        	if(objResult[4]!=null) {
        		physicalCertificateTO.setName(Validations.validateIsNotNull(objResult[4]) ? objResult[4].toString() : "");
        	}
        	if(objResult[5]!=null) {
        		physicalCertificateTO.setSecurityClassDescription(Validations.validateIsNotNull(objResult[5]) ? objResult[5].toString() : "");
        	}
        	if(objResult[6]!=null) {
        		physicalCertificateTO.setIdSecurityCodePk(Validations.validateIsNotNull(objResult[6]) ? objResult[6].toString() : "");
        	}
        	if(objResult[7]!=null) {
        		physicalCertificateTO.setSerialCode(Validations.validateIsNotNull(objResult[7]) ? objResult[7].toString() : "");
        	}
        	if(objResult[8]!=null) {
        		physicalCertificateTO.setElectronic(Validations.validateIsNotNull(objResult[8]) ? objResult[8].toString() : "");
        	}
        	if(objResult[9]!=null) {
        		physicalCertificateTO.setIssuer(Validations.validateIsNotNull(objResult[9]) ? objResult[9].toString() : "");
        	}
        	if(objResult[10]!=null) {
        		physicalCertificateTO.setCurrencyDescription(Validations.validateIsNotNull(objResult[10]) ? objResult[10].toString() : "");
        	}
        	if(objResult[11]!=null) {
        		physicalCertificateTO.setSecurityNominalValue((BigDecimal)objResult[11]);
        	}
        	if(objResult[12]!=null) {
        		physicalCertificateTO.setExpirationDate((Date)objResult[12]);
        	}
        	if(objResult[13]!=null) {
        		physicalCertificateTO.setQuantity(new Integer(objResult[13].toString()));
        	}
        	if(objResult[14]!=null) {
        		physicalCertificateTO.setCertificateFrom(new Integer(objResult[14].toString()));
        	}
        	if(objResult[15]!=null) {
        		physicalCertificateTO.setCertificateTo(new Integer(objResult[15].toString()));
        	}
        	if(objResult[16]!=null) {
        		physicalCertificateTO.setComments(Validations.validateIsNotNull(objResult[16]) ? objResult[16].toString() : "");
        	}
        	if(objResult[17]!=null) {
        		physicalCertificateTO.setIndElectronicCupon(new Integer(objResult[17].toString()));
        	}
        	if(objResult[18]!=null) {
        		physicalCertificateTO.setCuponNumber(new Integer(objResult[18].toString()));
        	}
        	if(objResult[19]!=null) {
        		physicalCertificateTO.setIdAnnotationOperation(new Long(objResult[19].toString()));
        	}
        	
        	listPhysicalCertificateTO.add(physicalCertificateTO);
        }
		
		return listPhysicalCertificateTO;
	}
							
	/**
	 * Metodo para obtener el numero de emisiones por idIssuerPk.
	 *
	 * @param idIssuer the id issuer
	 * @return the issuance sequence code service bean
	 * @throws ServiceException the service exception
	 */
	public String getIssuanceSequenceCodeServiceBean(String idIssuer) throws ServiceException {
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append("Select count(I) from Issuance i where i.issuer.idIssuerPk=:idIssuerPkPrm");
		Query query = em.createQuery(sbQuery.toString());
	
		query.setParameter("idIssuerPkPrm", idIssuer);

		Integer issuanceCode=Integer.parseInt( query.getSingleResult().toString() );
		issuanceCode++;
		StringBuilder sbIssuanceCode=new StringBuilder();
		if(issuanceCode!=null){
			String strIssuanceCode =issuanceCode.toString();
			if(strIssuanceCode.length()==1){
				sbIssuanceCode.append("0").append( strIssuanceCode );
			}else{
				sbIssuanceCode.append( strIssuanceCode );
			}
			strIssuanceCode=sbIssuanceCode.toString();
		}
		return sbIssuanceCode.toString();
	}
	
	/**
	 * Metodo para obtener el numero de emisiones por descripcion de la emision.
	 *
	 * @param issuance object Issuance
	 * @return number result
	 */
	public int validateIssuanceDescriptionServiceBean(Issuance issuance){
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append("Select count(I) from Issuance i where i.description=:descriptionPrm");
		if(issuance.getIdIssuanceCodePk()!=null){
			sbQuery.append(" and i.idIssuanceCodePk != :idIssuanceCodePkPrm");
		}
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("descriptionPrm", issuance.getDescription());
		if(issuance.getIdIssuanceCodePk()!=null){
			query.setParameter("idIssuanceCodePkPrm", issuance.getIdIssuanceCodePk());
		}
		return Integer.parseInt(  query.getSingleResult().toString()  );
	}
	
	/**
	 * Exists security code.
	 *
	 * @param idSecurityCode the id security code
	 * @return the string
	 */
	public String existsSecurityCode(String idSecurityCode){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select sec.idSecurityCodePk from Security sec where sec.idSecurityCodePk = :idSecurityCode ");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSecurityCode", idSecurityCode);
		
		try {
			return (String) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	/**
	 * Metodo para el registro de una emision.
	 *
	 * @param issuance object Issuance
	 * @param logger object Logger User
	 * @return result correct
	 * @throws ServiceException Service Exception
	 */
	@SuppressWarnings("unused")
	public boolean registryIssuanceServiceBean(Issuance issuance, LoggerUser logger) throws ServiceException{
		registryIssuanceServiceBeanAndReturnEntity(issuance, logger);
		return true;
	}
	
	public Issuance registryIssuanceServiceBeanAndReturnEntity(Issuance issuance, LoggerUser logger) throws ServiceException{
		//Validamos el estado del emisor
		Map<String,Object> param = new HashMap<String,Object>();
		
		param.put("idIssuerPkParam", issuance.getIssuer().getIdIssuerPk());
		Issuer issuer = findObjectByNamedQuery(Issuer.ISSUER_STATE, param);
		if(!issuer.getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.ISSUER_BLOCK);
		}
		
        //issuance.setIdIssuanceCodePk(getGenerecateIssuanceCode(issuance,logger));
        
		if (issuance.isFixedIncomeInstrumentType()) {
			// issuance.setInitialNominalValue(null);
			// issuance.setCurrentNominalValue(null);
			issuance.setCirculationAmount(issuance.getPlacedAmount());
			// issuance.setCirculationBalance( issuance.getPlacedBalance() );
		}
		if (issuance.isPhysicalIssueType()) {
			issuance.setNumberTotalTranches(null);
			issuance.setNumberPlacementTranches(null);
		}
		
		issuance.setStateIssuance(IssuanceStateType.AUTHORIZED.getCode());
		issuance.setCurrentTranche(Integer.valueOf(0));

		issuance.setRegistryUser(logger.getUserName());
		issuance.setRegistryDate(logger.getAuditTime());
		
		if (issuance.getIssuanceCertificates() != null) {
			for (IssuanceCertificate ic : issuance.getIssuanceCertificates()) {
				ic.setHolder(issuance.getIssuer().getHolder());
				ic.setIssuance(issuance);
				ic.setRegistryUser(logger.getUserName());
				ic.setRegistryDate(logger.getAuditTime());
				ic.setStateCertificate(IssuanceCertificateStateType.REGISTERED
						.getCode());
			}
		}
		if (Validations.validateListIsNotNullAndNotEmpty(issuance.getIssuanceFiles())) {
			for (IssuanceFile objIssuanceFile: issuance.getIssuanceFiles()) {
				objIssuanceFile.setDocumentState(IssuanceCertificateStateType.REGISTERED.getCode());
			}
		}
		
		issuance.setAmountConfirmedSegments(BigDecimal.ZERO);
		issuance.setAmountOpenedSegments( BigDecimal.ZERO );
		issuance.setIndHaveCertificate(Integer.valueOf(0));
		
		if(logger != null){
			log.info("ISSUANCE registryIssuanceServiceBean ");
			log.info("WSERROR Privilege: " + logger.getIdPrivilegeOfSystem());
			log.info("WSERROR user : " + logger.getUserName());
			log.info("WSERROR address : " + logger.getIpAddress());
		}
		
		issuance.setAudit(logger);
		
		issuance = create(issuance);
		return issuance;
	}
	
	/**
	 * Metodo para el registro de una emision aux
	 *
	 * @param issuance object Issuance
	 * @param logger object Logger User
	 * @return result correct
	 * @throws ServiceException Service Exception
	 */
	@SuppressWarnings("unused")
	public boolean registryIssuanceAuxServiceBean(IssuanceAux issuance, LoggerUser logger) throws ServiceException {
		registryIssuanceAuxServiceBeanAndReturnEntity(issuance, logger);
		return true;
	}
	
	public IssuanceAux registryIssuanceAuxServiceBeanAndReturnEntity(IssuanceAux issuance, LoggerUser logger) throws ServiceException{
		//Validamos el estado del emisor
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("idIssuerPkParam", issuance.getIssuer().getIdIssuerPk());
		Issuer issuer = findObjectByNamedQuery(Issuer.ISSUER_STATE, param);
		if(!issuer.getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.ISSUER_BLOCK);
		}		
        issuance.setIdIssuanceCodePk(getGenerecateIssuanceAuxCode(issuance,logger));
		if (issuance.isFixedIncomeInstrumentType()) {
			issuance.setCirculationAmount(issuance.getPlacedAmount());
		}
		if (issuance.isPhysicalIssueType()) {
			issuance.setNumberTotalTranches(null);
			issuance.setNumberPlacementTranches(null);
		}		
		issuance.setStateIssuance(IssuanceStateType.AUTHORIZED.getCode());
		issuance.setCurrentTranche(Integer.valueOf(0));
		issuance.setRegistryUser(logger.getUserName());
		issuance.setRegistryDate(logger.getAuditTime());		
		if (issuance.getIssuanceCertificates() != null) {
			for (IssuanceCertificateAux ic : issuance.getIssuanceCertificates()) {
				ic.setHolder(issuance.getIssuer().getHolder());
				ic.setIssuance(issuance);
				ic.setRegistryUser(logger.getUserName());
				ic.setRegistryDate(logger.getAuditTime());
				ic.setStateCertificate(IssuanceCertificateStateType.REGISTERED.getCode());
			}
		}
		issuance.setIndHaveCertificate(BooleanType.NO.getCode());	
		if (Validations.validateListIsNotNullAndNotEmpty(issuance.getIssuanceFiles())) {
			issuance.setIndHaveCertificate(BooleanType.YES.getCode());	
			for (IssuanceFileAux objIssuanceFile: issuance.getIssuanceFiles()) {
				objIssuanceFile.setDocumentState(IssuanceCertificateStateType.REGISTERED.getCode());
			}
		}		
		issuance.setAmountConfirmedSegments(BigDecimal.ZERO);
		issuance.setAmountOpenedSegments( BigDecimal.ZERO );
		if(logger != null){
			log.info("ISSUANCE registryIssuanceServiceBean ");
			log.info("WSERROR Privilege: " + logger.getIdPrivilegeOfSystem());
			log.info("WSERROR user : " + logger.getUserName());
			log.info("WSERROR address : " + logger.getIpAddress());
		}		
		issuance.setAudit(logger);		
		issuance = create(issuance);
		return issuance;
	}
	
	/**
	 * Metodo para guardar los totales diarios por moneda y clase.
	 *
	 * @param lstDailySecurity the lst daily security
	 * @param logger object Logger User
	 * @return result correct
	 * @throws ServiceException Service Exception
	 */
	public boolean saveDailySecClass(List<DailySecurityClassBalance> lstDailySecurity , LoggerUser logger) throws ServiceException{
		boolean blState;
		 for (DailySecurityClassBalance dailySecBalance :lstDailySecurity){
			 dailySecBalance.setAudit(logger);
			 create(dailySecBalance);
		 }
		blState=true;
		return blState;
	}
	
	/**
	 * Metodo para guardar los totales Diarios aculumados de los valores agrupados por moneda y clase.
	 *
	 * @param lstTotalSecurity the lst total security
	 * @param sendDate the send date
	 * @param logger the logger
	 * @return result correct
	 * @throws ServiceException Service Exception
	 */
	public boolean saveTotalSecClass(List<TotalSecurityClassBalance> lstTotalSecurity , Date sendDate,LoggerUser logger) throws ServiceException{
		boolean blState;
		 for (TotalSecurityClassBalance totalSecBalance :lstTotalSecurity){
			 em.detach(totalSecBalance);
			 totalSecBalance.setProcessDate(sendDate);
			 totalSecBalance.setIdTotalSecClassBalancePk(null);
			 totalSecBalance.setAudit(logger);
			 create(totalSecBalance);
		 }
		 blState=true;
		return blState;
	}
	
	/**
	 * save issuance observations maker checker
	 */
	public boolean saveIssuanceObservations(IssuanceObservation obs) {
		boolean obsState;
		em.detach(obs);
		create(obs);
		obsState=true;
		return obsState;
	}
	
	/**
	 * Delete process daily security class.
	 *
	 * @param date the date
	 */
	public void deleteProcessDailySecClass(Date date){
		StringBuilder sb=new StringBuilder();
		
		sb.append("DELETE DailySecurityClassBalance where trunc(processDate)= trunc(:processDate)");
		
		Query query= em.createQuery(sb.toString());
		query.setParameter("processDate", date);
		query.executeUpdate();
	}
	
	/**
	 * Delete process total security class.
	 *
	 * @param date the date
	 */
	public void deleteProcessTotalSecClass(Date date){
		StringBuilder sb=new StringBuilder();
		
		sb.append("DELETE TotalSecurityClassBalance where trunc(processDate)= trunc(:processDate)");
		
		Query query= em.createQuery(sb.toString());
		query.setParameter("processDate", date);
		query.executeUpdate();
	}
	
	/**
	 * UPDATE ISSUANCE_AUX STATE */
	public void updateIssuanceAuxState(String idIssuanceCodePk, Integer state){
        StringBuilder sb=new StringBuilder();		
		sb.append(" update ISSUANCE_AUX set STATE_CHK =:state  ");
		sb.append(" where ID_ISSUANCE_CODE_PK = :idIssuanceCodePk ");
		Query query= em.createNativeQuery(sb.toString());		
		query.setParameter("state", state);
		query.setParameter("idIssuanceCodePk", idIssuanceCodePk);
		query.executeUpdate();
	}
	
	/**
	 * Update last Date The Parameter Top  
	 * @param dateSend
	 * @param parameterDateTop
	 */
	public void updateParameterTop(Date dateSend, Integer parameterDateTop){
		StringBuilder sb=new StringBuilder();
		
		sb.append(" Update ParameterTable PT set text1=:processDate  ");
		sb.append(" 	   Where parameterTablePk=:parameterDateTop ");
		
		Query query= em.createQuery(sb.toString());
		query.setParameter("processDate", CommonsUtilities.convertDatetoString(dateSend));
		query.setParameter("parameterDateTop", parameterDateTop);
		query.executeUpdate();
		
	}
	
	/**
	 * Generate Issuance Code.
	 *
	 * @param issuance object Issuance
	 * @param logger object Logger User
	 * @return code
	 * @throws ServiceException Service Exception
	 */
	public String getGenerecateIssuanceCode(Issuance issuance, LoggerUser logger) throws ServiceException{				
		ParameterTable pTableSecClass=parameterServiceBean.getParameterTableById(issuance.getSecurityClass());
		
//		Correlative correlativeFilter=new Correlative();
//		correlativeFilter.setIssuer(issuance.getIssuer());
//		correlativeFilter.setCorrelativeCd(CorrelativeType.ISSUER_CODE_COR.getValue());
//		correlativeFilter.setAudit(logger);
//		Correlative currentCorrelative=correlativeServiceBean.getCorrelativeServiceBean(correlativeFilter);
//		if(currentCorrelative == null){
//			currentCorrelative=new Correlative();
//			currentCorrelative.setIssuer(issuance.getIssuer());
//			currentCorrelative.setCorrelativeCd(CorrelativeType.ISSUER_CODE_COR.getValue());
//			currentCorrelative.setDescription( CorrelativeType.ISSUER_CODE_COR.getValue() );
//			currentCorrelative.setCorrelativeDate(CommonsUtilities.currentDateTime());
//			currentCorrelative.setCorrelativeText("000001");
//			create(currentCorrelative);
//		}else{
//			currentCorrelative.setCorrelativeText(securitiesUtils.alphaNumericSequnceIssuanceCode(currentCorrelative.getCorrelativeText()));
//			update(currentCorrelative);
//		}
		
		Long numberCorrelative = getNextCorrelativeOperations(GeneralConstants.STR_SQ_ISSUANCE_PK + issuance.getIssuer().getMnemonic());		
		String strNumberCorrelative = StringUtils.leftPad(numberCorrelative.toString(), 8, "0");
		String setCorrelativeText = securitiesUtils.alphaNumericSequnceIssuanceCode(strNumberCorrelative);		
		StringBuilder sbIssuanceCodePk = new StringBuilder();
		sbIssuanceCodePk.append((issuance.getIssuer().getIdIssuerPk()).substring(0, 2)); //2 country
		sbIssuanceCodePk.append(pTableSecClass.getText1());	//3
	//	sbIssuanceCodePk.append((issuance.getIssuer().getIdIssuerPk()).substring(2, 5));
		sbIssuanceCodePk.append(issuance.getIssuer().getMnemonic()); //3
		sbIssuanceCodePk.append(setCorrelativeText); //6
		
		return sbIssuanceCodePk.toString();
	}
	
	/**
	 * Generate IssuanceAux Code.
	 *
	 * @param issuance object Issuance
	 * @param logger object Logger User
	 * @return code
	 * @throws ServiceException Service Exception
	 */
	public String getGenerecateIssuanceAuxCode(IssuanceAux issuance, LoggerUser logger) throws ServiceException{				
		ParameterTable pTableSecClass=parameterServiceBean.getParameterTableById(issuance.getSecurityClass());
//		Long numberCorrelative = getNextCorrelativeOperations(GeneralConstants.STR_SQ_ISSUANCE_PK + issuance.getIssuer().getMnemonic());	
		Long numberCorrelative = getCorrelativeIssuanceByIssuer(issuance.getIssuer().getIdIssuerPk(),issuance.getSecurityClass());
		String strNumberCorrelative = StringUtils.leftPad(numberCorrelative.toString(), 8, "0");
		String setCorrelativeText = securitiesUtils.alphaNumericSequnceIssuanceCode(strNumberCorrelative);		
		StringBuilder sbIssuanceCodePk = new StringBuilder();
		sbIssuanceCodePk.append((issuance.getIssuer().getIdIssuerPk()).substring(0, 2)); //2 country
		sbIssuanceCodePk.append(pTableSecClass.getText1());	//3
		sbIssuanceCodePk.append(issuance.getIssuer().getMnemonic()); //3
		sbIssuanceCodePk.append(setCorrelativeText); //6
		
		return sbIssuanceCodePk.toString();
	}
	/**
	 * Metodo para Obtener issuance balance movement.
	 *
	 * @param issuanceBalanceMovTO object IssuanceBalanceMovTO
	 * @return object IssuanceBalanceMovResultTO
	 * @throws ServiceException Service Exception
	 */
	public List<IssuanceBalanceMovResultTO> getIssuanceBalanceMovement(IssuanceBalanceMovTO issuanceBalanceMovTO) throws ServiceException{
		StringBuilder stringSql = new StringBuilder();

		stringSql.append("SELECT ");
//		if(issuanceBalanceMovTO.getIssuanceType().equals(IssuanceType.DEMATERIALIZED.getCode())){
//			stringSql.append("	issua.ISSUANCE_AMOUNT, issua.PLACED_AMOUNT, (issua.ISSUANCE_AMOUNT -  issua.PLACED_AMOUNT) AMOUNT_TO_PLACED, ");
//		}else{
//			stringSql.append("	issua.ISSUANCE_AMOUNT, issua.PLACED_AMOUNT, 0 AMOUNT_TO_PLACED, ");
//		}
		
		stringSql.append(" issua.ISSUANCE_AMOUNT, issua.PLACED_AMOUNT, ");
		stringSql.append(" CASE issua.ISSUANCE_TYPE WHEN ");
		stringSql.append(IssuanceType.DEMATERIALIZED.getCode().toString());
		stringSql.append(" THEN (issua.ISSUANCE_AMOUNT - issua.PLACED_AMOUNT) ");
		stringSql.append(" ELSE 0 END AS AMOUNT_TO_PLACED, ");
		
		stringSql.append("	issua.AMORTIZATION_AMOUNT, (issua.ISSUANCE_AMOUNT - issua.AMORTIZATION_AMOUNT ) BY_AMORTIZATION, issua.AMORTIZATION_AMOUNT INTEREST, ");
		stringSql.append(" issua.ID_ISSUANCE_CODE_PK ");
		stringSql.append("FROM issuance issua WHERE 1 = 1");
		
		if(issuanceBalanceMovTO.getIdIssuerPk() != null) {
			stringSql.append(" AND ID_ISSUER_FK = :issuerPk");
		}
		
		if(issuanceBalanceMovTO.getIdIssuancePk() != null) {
			stringSql.append(" AND id_issuance_code_pk = :issuanceCodeParam");
		}
		
		Query queryJpql = em.createNativeQuery(stringSql.toString());
		//queryJpql.setParameter("issuanceCodeParam", issuanceBalanceMovTO.getIdIssuancePk());
		
		if(issuanceBalanceMovTO.getIdIssuerPk() != null) {
			queryJpql.setParameter("issuerPk", issuanceBalanceMovTO.getIdIssuerPk());
		}
		
		if(issuanceBalanceMovTO.getIdIssuancePk() != null) {
			queryJpql.setParameter("issuanceCodeParam", issuanceBalanceMovTO.getIdIssuancePk());
		}
		
		
		List<IssuanceBalanceMovResultTO> issuanceBalanceMovResultTO = new ArrayList<IssuanceBalanceMovResultTO>();
		
		List<Object[]> resultObject = (List<Object[]>)queryJpql.getResultList();
		if(resultObject==null){
			return null;
		}
		
		for(int i = 0; i < resultObject.size(); i++) {
			IssuanceBalanceMovResultTO result = new IssuanceBalanceMovResultTO();
			
			result.setIdIssuerPk(issuanceBalanceMovTO.getIdIssuerPk());
			result.setIssuanceAmount((BigDecimal)resultObject.get(i)[0]);
			result.setAmountPlaced((BigDecimal)resultObject.get(i)[1]);
			result.setAmountByPlaced((BigDecimal)(resultObject.get(i)[2]==null?BigDecimal.ZERO:resultObject.get(i)[2]));
			result.setAmountAmortization((BigDecimal)(resultObject.get(i)[3]==null?BigDecimal.ZERO:resultObject.get(i)[3]));
			result.setAmountByAmortization((BigDecimal)(resultObject.get(i)[4]==null?BigDecimal.ZERO:resultObject.get(i)[4]));
			result.setInterestPayment((BigDecimal)(resultObject.get(i)[5]==null?BigDecimal.ZERO:resultObject.get(i)[5]));
			result.setIdIssuancePk(resultObject.get(i)[6].toString());

			
			issuanceBalanceMovResultTO.add(result);
		}
		
		return issuanceBalanceMovResultTO;
	}
	
	/**
	 * Metodo para Obtener los detalles issuance balance movement.
	 *
	 * @param issuanceBalanceMovTO object IssuanceBalanceMovTO
	 * @return List object IssuanceBalanceMovTO
	 * @throws ServiceException Service Exception
	 */
	@SuppressWarnings("unchecked")
	public List<IssuanceBalanceMovDetailResultTO> getIssuanceBalanceMovDetService(IssuanceBalanceMovTO issuanceBalanceMovTO, List<IssuanceBalanceMovResultTO> issuanceBalMovSessionList) throws ServiceException{
		StringBuilder stringSql = new StringBuilder();
		stringSql.append("SELECT ");
		stringSql.append("	issuOpe.OPERATION_DATE,	issuOpe.ID_ISSUANCE_OPERATION_PK, secOpe.ID_SECURITY_CODE_FK, sec.DESCRIPTION, ");
		stringSql.append("	mvt.MOVEMENT_NAME, issuOpe.CASH_AMOUNT, sec.CURRENT_NOMINAL_VALUE, secOpe.STOCK_QUANTITY ");
		stringSql.append(" ,issuOpe.ID_ISSUANCE_CODE_FK ");
		stringSql.append(" FROM issuance_operation issuOpe ");
		stringSql.append(" INNER JOIN issuance_movement issuMov ON issuMov.ID_ISSUANCE_OPERATION_FK = issuOpe.ID_ISSUANCE_OPERATION_PK ");
		stringSql.append(" INNER JOIN MOVEMENT_TYPE mvt ON mvt.ID_MOVEMENT_TYPE_PK = issuMov.ID_MOVEMENT_TYPE_FK ");
		stringSql.append(" INNER JOIN SECURITIES_OPERATION secOpe ON secOpe.ID_SECURITIES_OPERATION_PK = issuOpe.ID_SECURITIES_OPERATION_FK ");
		stringSql.append(" INNER JOIN SECURITY sec ON sec.ID_SECURITY_CODE_PK = secOpe.ID_SECURITY_CODE_FK ");
		stringSql.append(" WHERE issuOpe.ID_ISSUANCE_CODE_FK in (:listIssuanceCodeParam) ");
		stringSql.append(" AND trunc(issuMov.MOVEMENT_DATE) between trunc(:initialDate) and trunc(:finalDate) ");
		stringSql.append(" ORDER BY issuOpe.ID_ISSUANCE_OPERATION_PK ASC ");
		
		List<String> issuanceList = new ArrayList<String>();
		for(int i = 0; i < issuanceBalMovSessionList.size(); i++) {
			String iterator = issuanceBalMovSessionList.get(i).getIdIssuancePk();
			issuanceList.add(iterator);
		}
		
		Query queryJpql = em.createNativeQuery(stringSql.toString());
		queryJpql.setParameter("listIssuanceCodeParam", issuanceList);
		queryJpql.setParameter("initialDate", issuanceBalanceMovTO.getInitialDate(), TemporalType.DATE);
		queryJpql.setParameter("finalDate", issuanceBalanceMovTO.getFinalDate(), TemporalType.DATE);
		List<IssuanceBalanceMovDetailResultTO> issuanceBalanceMovDetList = new ArrayList<IssuanceBalanceMovDetailResultTO>();
		
		List<Object[]> resultObject = (List<Object[]>)queryJpql.getResultList();
		if(resultObject==null){
			return null;
		}
		for(Object[] objectData: resultObject){
			IssuanceBalanceMovDetailResultTO issuanceBalanceMovDet = new IssuanceBalanceMovDetailResultTO();
			issuanceBalanceMovDet.setIdIssuerPk(issuanceBalanceMovTO.getIdIssuerPk());
			issuanceBalanceMovDet.setMovementDate((Date) objectData[0]);
			issuanceBalanceMovDet.setOperationNumber(new Integer(objectData[1].toString()));
			issuanceBalanceMovDet.setIsinCode((String) objectData[2]);
			issuanceBalanceMovDet.setIsinDescription((String) objectData[3]);
			issuanceBalanceMovDet.setMovementDescription((String) objectData[4]);
			issuanceBalanceMovDet.setAmountOperation((BigDecimal) (objectData[5]==null?BigDecimal.ZERO:objectData[5]));
			issuanceBalanceMovDet.setNominalValue((BigDecimal) (objectData[6]==null?BigDecimal.ZERO:objectData[6]));
			issuanceBalanceMovDet.setSecurityQuantity((BigDecimal) (objectData[7]==null?0:objectData[7]));
			issuanceBalanceMovDet.setIdIssuancePk((String) objectData[8]);

			issuanceBalanceMovDetList.add(issuanceBalanceMovDet);
		}
		return issuanceBalanceMovDetList;
	}
	
	public Object[] getIssuerDetails(String idIssuanceCode){
		
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" select SEC.issuanceDate,ISS.idIssuerPk,a.indicator4,c.indicator4");  // 0 1 2 3
		stringBuffer.append(" from Issuance SEC, Issuer ISS, ParameterTable a, ParameterTable c "); 
		stringBuffer.append(" where SEC.issuer.idIssuerPk = ISS.idIssuerPk "); 
		stringBuffer.append(" and ISS.economicActivity = a.parameterTablePk ");
		stringBuffer.append(" and SEC.securityClass = c.parameterTablePk ");
		stringBuffer.append(" and SEC.idIssuanceCodePk = :idIssuanceCode ");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idIssuanceCode", idIssuanceCode);
		
		return (Object[])findObjectByQueryString(stringBuffer.toString(), parameters);
	}
	
	
	
	
	/**
	 * Metodo para saber el monto de la emision issue 578
	 * @param idIssuancePk
	 * @return
	 */
	public BigDecimal getSumAmountSecBcb(String idIssuancePk){
		
		StringBuilder querySql = new StringBuilder();
		
		querySql.append("  SELECT SUM(SEC.ISSUANCE_QUANTITY_SEC_BCB)             					         ");
		querySql.append("  FROM ISSUANCE ISS                               									 ");
		querySql.append("  INNER JOIN SECURITY SEC ON SEC.ID_ISSUANCE_CODE_FK = ISS.ID_ISSUANCE_CODE_PK      ");
		querySql.append("  WHERE ISS.ID_ISSUANCE_CODE_PK = :idIssuancePk    								 "); 
		
		Query query = em.createNativeQuery(querySql.toString());
		query.setParameter("idIssuancePk", idIssuancePk);
		BigDecimal result = (BigDecimal)query.getSingleResult();
		
		if(!Validations.validateIsNotNullAndNotEmpty(result)){
			result = new BigDecimal(0);
		}
		return result;
	}

	public  Long getCorrelativeIssuanceByIssuer(String idIssuerPk, Integer idClassPk) {
		Long idReturn = 0l;
		Query query = em.createQuery("SELECT MAX(iss.idIssuanceCodePk) FROM IssuanceAux iss WHERE iss.issuer.idIssuerPk = :idIssuerPk and  securityClass = :idClassPk");
		query.setParameter("idIssuerPk", idIssuerPk);
		query.setParameter("idClassPk", idClassPk);
		try{
			String maxCode=(String)query.getSingleResult();
			if(maxCode!=null) {
				int i=maxCode.length();
				idReturn = Long.parseLong(maxCode.substring(i-6, i));
				
			}
			return ++idReturn;
				
		}catch (NoResultException e) {
			return 1L;
		}
		
	}
	
	public List<Issuance> findIssuanceWithAccountAnnotation(AccountAnnotationOperation accountAnnotation) throws ServiceException {
		StringBuilder strbQuery = new StringBuilder();
		strbQuery.append(" SELECT i FROM Issuance i  ");
		strbQuery.append(" JOIN FETCH i.issuer isue ");
		strbQuery.append(" WHERE isue.idIssuerPk = :idIssuerPk ");
		strbQuery.append(" AND i.instrumentType in (:instrumentType) ");
		strbQuery.append(" AND i.currency = :currency ");
		strbQuery.append(" AND i.securityType = :securityType ");
		strbQuery.append(" AND i.issuanceType in (:issuanceType) ");
		strbQuery.append(" AND i.securityClass = :securityClass ");
		strbQuery.append(" AND i.issuanceDate <= :issuanceDate ");
		strbQuery.append(" AND add_months(i.issuanceDate,12) > :issuanceDate ");
		
		Query query=em.createQuery(strbQuery.toString());
		
		List<Integer> lstIssuanceType = new ArrayList<Integer>();
		//lstIssuanceType.add(IssuanceType.MIXED.getCode());
		lstIssuanceType.add(IssuanceType.PHYSICAL.getCode());
		
		List<Integer> lstInstrumentType = new ArrayList<Integer>();
		lstInstrumentType.add(InstrumentType.FIXED_INCOME.getCode());
		//lstInstrumentType.add(InstrumentType.MIXED_INCOME.getCode());
		
		query.setParameter("idIssuerPk", accountAnnotation.getIssuer().getIdIssuerPk());
		query.setParameter("instrumentType", lstInstrumentType);
		query.setParameter("currency", accountAnnotation.getSecurityCurrency());
		query.setParameter("securityType", SecurityType.PRIVATE.getCode());
		query.setParameter("issuanceType", lstIssuanceType);
		query.setParameter("securityClass", accountAnnotation.getSecurityClass());
		/*
		Date date = accountAnnotation.getExpeditionDate();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		Integer year = calendar.get(Calendar.YEAR);
		*/
		query.setParameter("issuanceDate", accountAnnotation.getExpeditionDate());
		
		return query.getResultList();		
	}

	public List<AccountAnnotationCupon> accountAnnotationCupons(Long idAnnotationOperationPk) throws ServiceException {
		StringBuilder strbQuery = new StringBuilder();
		strbQuery.append(" SELECT aao FROM AccountAnnotationOperation aao  ");
		strbQuery.append(" JOIN FETCH aao.accountAnnotationCupons aac ");
		strbQuery.append(" WHERE aao.idAnnotationOperationPk = :idAnnotationOperationPk ");
		
		
		TypedQuery<AccountAnnotationOperation> query=em.createQuery(strbQuery.toString(),AccountAnnotationOperation.class);
		query.setParameter("idAnnotationOperationPk", idAnnotationOperationPk);
		try {
			AccountAnnotationOperation accountAO = query.getSingleResult();
			return accountAO.getAccountAnnotationCupons();
		}catch (NoResultException e) {
			return null;
		}
	}

	public CorporativeOperation CorporativeInterest(Date expirationDate, String idSecurityCodePk) throws ServiceException {
		StringBuilder strbQuery = new StringBuilder();
		strbQuery.append(" SELECT co FROM CorporativeOperation co  ");
		strbQuery.append(" JOIN FETCH co.programInterestCoupon pic ");
		strbQuery.append(" WHERE co.securities.idSecurityCodePk = :idSecurityCodePk and pic.experitationDate = :experitationDate ");
		
		TypedQuery<CorporativeOperation> query=em.createQuery(strbQuery.toString(),CorporativeOperation.class);
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		query.setParameter("experitationDate", expirationDate);
		try {
			return query.getSingleResult();
		}catch (NoResultException e) {
			return null;
		}
	}

	public CorporativeOperation CorporativeAmortization(Date expirationDate, String idSecurityCodePk) throws ServiceException {
		StringBuilder strbQuery = new StringBuilder();
		strbQuery.append(" SELECT co FROM CorporativeOperation co  ");
		strbQuery.append(" JOIN FETCH co.programAmortizationCoupon pac ");
		strbQuery.append(" WHERE co.securities.idSecurityCodePk = :idSecurityCodePk and pac.expirationDate = :expirationDate ");
		
		TypedQuery<CorporativeOperation> query=em.createQuery(strbQuery.toString(),CorporativeOperation.class);
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		query.setParameter("expirationDate", expirationDate);
		try {
			return query.getSingleResult();
		}catch (NoResultException e) {
			return null;
		}
	}
	
	public Security findSecurityWithAccountAnnotation(AccountAnnotationOperation accountAnnotation) throws ServiceException {
		/*
			- EMISOR 
		    - EMISION
		    
		    - SERIE 
		    - F.VENCIMIENTO 
		    - VALOR NOMINAL 
		    - TASA 
		    - F. EMISION 
		    - PERIDIOCIDAD 
		 */
		StringBuilder strbQuery = new StringBuilder();
		strbQuery.append(" SELECT s FROM Security s  ");
		strbQuery.append(" JOIN FETCH s.issuer isue ");
		strbQuery.append(" JOIN FETCH s.issuance isua ");
		strbQuery.append(" JOIN FETCH s.interestPaymentSchedule ips ");
		strbQuery.append(" JOIN FETCH ips.programInterestCoupons pic ");
//		strbQuery.append(" JOIN FETCH s.amortizationPaymentSchedule aps ");
//		strbQuery.append(" JOIN FETCH aps.programAmortizationCoupons pac ");
		strbQuery.append(" WHERE isue.idIssuerPk = :idIssuerPk ");
		//strbQuery.append(" AND isua.idIssuanceCodePk = :idIssuanceCodePk ");
		strbQuery.append(" AND s.securitySerial = :securitySerial ");
		strbQuery.append(" AND s.expirationDate = :expirationDate ");
		/*strbQuery.append(" AND s.initialNominalValue = :initialNominalValue ");*/
		/*strbQuery.append(" AND s.rateValue = :rateValue ");*/
		/*strbQuery.append(" AND s.periodicity = :periodicity ");*/
		strbQuery.append(" AND s.issuanceDate = :issuanceDate ");
		strbQuery.append(" AND s.currency =: currency ");
		//strbQuery.append(" AND s.securityType =: securityType ");
		strbQuery.append(" AND s.securityClass =: securityClass ");
		//strbQuery.append(" AND s.instrumentType =: instrumentType ");
		//strbQuery.append(" AND isua.issuanceType =: issuanceType ");
		
		
		TypedQuery<Security> query=em.createQuery(strbQuery.toString(),Security.class);

		query.setParameter("idIssuerPk", accountAnnotation.getIssuer().getIdIssuerPk());
		//query.setParameter("idIssuanceCodePk", accountAnnotation.getIssuance().getIdIssuanceCodePk());
		query.setParameter("securitySerial", accountAnnotation.getSerialNumber()+accountAnnotation.getCertificateNumber());
		query.setParameter("expirationDate", accountAnnotation.getSecurityExpirationDate());
		query.setParameter("issuanceDate", accountAnnotation.getExpeditionDate());
		query.setParameter("currency", accountAnnotation.getSecurityCurrency());
		//query.setParameter("securityType", SecurityType.PRIVATE.getCode());
		query.setParameter("securityClass", accountAnnotation.getSecurityClass());
		//query.setParameter("instrumentType", InstrumentType.FIXED_INCOME.getCode());
		//query.setParameter("issuanceType", IssuanceType.PHYSICAL.getCode());
		
		try {
			return query.getSingleResult();
		}catch (NoResultException e) {
			return null;
		}
	}

	public Issuance fillIssuanceByIssuanceAux(IssuanceAux issuanceAux){
		 Issuance issuance = new Issuance();
		 issuance.setAmortizationAmount(issuanceAux.getAmortizationAmount());
		 issuance.setPlacedAmount(issuanceAux.getPlacedAmount());
		 issuance.setCirculationAmount(issuanceAux.getCirculationAmount());
		 issuance.setIssuanceAmount(issuanceAux.getIssuanceAmount());
		 issuance.setAmountConfirmedSegments(issuanceAux.getAmountConfirmedSegments());
		 issuance.setAmountOpenedSegments(issuanceAux.getAmountOpenedSegments());
		 issuance.setSecurityType(issuanceAux.getSecurityType());
		 issuance.setSecurityClass(issuanceAux.getSecurityClass());
		 issuance.setCreditRatingScales(issuanceAux.getCreditRatingScales());
		 issuance.setCurrency(issuanceAux.getCurrency());
		 issuance.setCurrentTranche(issuanceAux.getCurrentTranche());
		 issuance.setDescription(issuanceAux.getDescription());
		 issuance.setEndorsement(issuanceAux.getEndorsement());
		 issuance.setExpirationDate(issuanceAux.getExpirationDate());
		 issuance.setGeographicLocation(issuanceAux.getGeographicLocation());
		 issuance.setIdIssuanceCodePk(issuanceAux.getIdIssuanceCodePk());
		 issuance.setIndHaveCertificate(issuanceAux.getIndHaveCertificate());
		 issuance.setIndPrimaryPlacement(issuanceAux.getIndPrimaryPlacement());
		 issuance.setIndRegulatorReport(issuanceAux.getIndRegulatorReport());
		 issuance.setInstrumentType(issuanceAux.getInstrumentType());
		 issuance.setIssuanceAmount(issuanceAux.getIssuanceAmount());
		 issuance.setIssuanceDate(issuanceAux.getIssuanceDate());
		 issuance.setIssuanceType(issuanceAux.getIssuanceType());
		 issuance.setIssuer(issuanceAux.getIssuer());
		 issuance.setNegotiationFactor(issuanceAux.getNegotiationFactor());
		 issuance.setNumberPlacementTranches(issuanceAux.getNumberPlacementTranches());
		 issuance.setNumberTotalTranches(issuanceAux.getNumberTotalTranches());
		 issuance.setStateIssuance(issuanceAux.getStateIssuance());
		 issuance.setPlacementType(issuanceAux.getPlacementType());
		 issuance.setResolutionDate(issuanceAux.getResolutionDate());
		 issuance.setResolutionNumber(issuanceAux.getResolutionNumber());
		 issuance.setTestimonyNumber(issuanceAux.getTestimonyNumber());
		 issuance.setTestimonyDate(issuanceAux.getTestimonyDate());
		 issuance.setIdIssuanceCodePk(issuanceAux.getIdIssuanceCodePk());

		 List<IssuanceFile> lstIssFile= new ArrayList<>();
		 IssuanceFile issFile= new IssuanceFile();
		 if(issuanceAux.getIssuanceFiles()!=null) {
			 for(IssuanceFileAux objIssFile:issuanceAux.getIssuanceFiles()) {
				 issFile = new IssuanceFile();
				 issFile.setDocumentDate(objIssFile.getDocumentDate());
				 issFile.setDocumentFile(objIssFile.getDocumentFile());
				 issFile.setDocumentNumber(objIssFile.getDocumentNumber());
				 issFile.setDocumentState(objIssFile.getDocumentState());
				 issFile.setFileName(objIssFile.getFileName());
				 issFile.setIssuance(issuance);
				 issFile.setMaxDocumentDate(objIssFile.getMaxDocumentDate());
				 lstIssFile.add(issFile);
			 }
			 issuance.setIssuanceFiles(lstIssFile);
		 }
		
		 return issuance;
     }

	public Issuance registerNewIssuanceWithAccountAnnotationOperation(AccountAnnotationOperation accountAnnotationOperation,LoggerUser loggerUser) throws ServiceException {
		if(accountAnnotationOperation.getIndSerializable() == 0) {
			IssuanceAux newIssuanceAux = instanceIssuanceAux(accountAnnotationOperation);
			newIssuanceAux = registryIssuanceAuxServiceBeanAndReturnEntity(newIssuanceAux, loggerUser);
			
			Issuance newIssuance = fillIssuanceByIssuanceAux(newIssuanceAux);
			newIssuance = registryIssuanceServiceBeanAndReturnEntity(newIssuance, loggerUser);

			return newIssuance;
		}
		return null;
	}
	
	public List<PhysicalCertificate> registerNewPhysicalCertificateWithAccountAnnotationOperation(AccountAnnotationOperation accountAnnotationOperation) throws ServiceException {
		List<PhysicalCertificate> lstPhysicalCertificate = instancePhysicalCertificate(accountAnnotationOperation);
		List<PhysicalCertificate> lstPhysicalCertificateOrder = new ArrayList<PhysicalCertificate>();
		
		for(PhysicalCertificate physicalCertificate: lstPhysicalCertificate) {
			if(physicalCertificate.getIdPendingSecurityCodePk().equals("")) {
				lstPhysicalCertificateOrder.add(physicalCertificate);
				break;
			}
		}
		for(PhysicalCertificate physicalCertificate: lstPhysicalCertificate) {
			if(!physicalCertificate.getIdPendingSecurityCodePk().equals("")) {
				lstPhysicalCertificateOrder.add(physicalCertificate);
			}
		}
		
		lstPhysicalCertificate = securityServiceFacade.registryPhysicalCertificateAndCupons(lstPhysicalCertificateOrder);
		
		return lstPhysicalCertificate;
	}
	
	public Security updateIssuanceClearWithAnnotationOperation(Security newSecurity, BigDecimal issuanceCirculationAmount) throws ServiceException {

		Issuance issuance = newSecurity.getIssuance();
		newSecurity.getIssuance().setCirculationAmount(issuanceCirculationAmount);
		issuance = update(issuance);
		newSecurity.setIssuance(issuance);
		
		return newSecurity;
	}
	
	public Security registerNewSecurityWithAccountAnnotationOperation(AccountAnnotationOperation accountAnnotationOperation,LoggerUser loggerUser) throws ServiceException {

		Security newSecurity = instanceSecurity(accountAnnotationOperation);
		newSecurity.setIndElectronicCupon(accountAnnotationOperation.getIndElectronicCupon());
		newSecurity.setValuatorProcessClass(null);
		newSecurity.setIndIsManaged(1);
		
		List<InternationalDepository> lstDtbInternationalDepository = null;
		List<NegotiationModality> lstDtbNegotiationModalities = null;

		if( !(accountAnnotationOperation.getMotive().equals(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode()) || 
			  accountAnnotationOperation.getMotive().equals(DematerializationCertificateMotiveType.GUARDA_MANAGED.getCode())
			)){
			lstDtbNegotiationModalities = staticLstDtbNegotiationModalities();
		}
		
		Integer indAutomatic = GeneralConstants.ONE_VALUE_INTEGER;
		
		securityServiceFacade.registrySecurityServiceFacade(newSecurity, lstDtbInternationalDepository, lstDtbNegotiationModalities,indAutomatic, accountAnnotationOperation);
		
		if(newSecurity.getSecurityNegotiationMechanisms()!=null) {
			for(SecurityNegotiationMechanism securityNegotiationMechanism: newSecurity.getSecurityNegotiationMechanisms()) {
				securityServiceFacade.registrySecurityNegotiationMechanism(securityNegotiationMechanism);
			}
		}

		return newSecurity;
	}
	
	public List<NegotiationModality> staticLstDtbNegotiationModalities(){
		
		List<NegotiationModality> lstDtbNegotiationModalities = new ArrayList<NegotiationModality>();
		NegotiationModality negotiationModality = null;
		NegotiationMechanism negotiationMechanism = null;
		List<NegotiationMechanism> lstDtbNegotiationMechanism = null;
		
		negotiationModality = new NegotiationModality();
		negotiationModality.setIdNegotiationModalityPk(20L);
		lstDtbNegotiationMechanism = new ArrayList<NegotiationMechanism>();
		negotiationMechanism = new NegotiationMechanism();
		negotiationMechanism.setIdNegotiationMechanismPk(1L);
		negotiationMechanism.setSelected(true);
		lstDtbNegotiationMechanism.add(negotiationMechanism);
		negotiationModality.setNegotiationMechanisms(lstDtbNegotiationMechanism);
		lstDtbNegotiationModalities.add(negotiationModality);

		negotiationModality = new NegotiationModality();
		negotiationModality.setIdNegotiationModalityPk(16L);
		lstDtbNegotiationMechanism = new ArrayList<NegotiationMechanism>();
		negotiationMechanism = new NegotiationMechanism();
		negotiationMechanism.setIdNegotiationMechanismPk(3L);
		negotiationMechanism.setSelected(true);
		lstDtbNegotiationMechanism.add(negotiationMechanism);
		negotiationModality.setNegotiationMechanisms(lstDtbNegotiationMechanism);
		lstDtbNegotiationModalities.add(negotiationModality);

		negotiationModality = new NegotiationModality();
		negotiationModality.setIdNegotiationModalityPk(12L);
		lstDtbNegotiationMechanism = new ArrayList<NegotiationMechanism>();
		negotiationMechanism = new NegotiationMechanism();
		negotiationMechanism.setIdNegotiationMechanismPk(3L);
		negotiationMechanism.setSelected(true);
		lstDtbNegotiationMechanism.add(negotiationMechanism);
		negotiationMechanism = new NegotiationMechanism();
		negotiationMechanism.setIdNegotiationMechanismPk(1L);
		negotiationMechanism.setSelected(true);
		lstDtbNegotiationMechanism.add(negotiationMechanism);
		negotiationModality.setNegotiationMechanisms(lstDtbNegotiationMechanism);
		lstDtbNegotiationModalities.add(negotiationModality);

		negotiationModality = new NegotiationModality();
		negotiationModality.setIdNegotiationModalityPk(3L);
		lstDtbNegotiationMechanism = new ArrayList<NegotiationMechanism>();
		negotiationMechanism = new NegotiationMechanism();
		negotiationMechanism.setIdNegotiationMechanismPk(3L);
		negotiationMechanism.setSelected(true);
		lstDtbNegotiationMechanism.add(negotiationMechanism);
		negotiationMechanism = new NegotiationMechanism();
		negotiationMechanism.setIdNegotiationMechanismPk(1L);
		negotiationMechanism.setSelected(true);
		lstDtbNegotiationMechanism.add(negotiationMechanism);
		negotiationModality.setNegotiationMechanisms(lstDtbNegotiationMechanism);
		lstDtbNegotiationModalities.add(negotiationModality);

		negotiationModality = new NegotiationModality();
		negotiationModality.setIdNegotiationModalityPk(1L);
		lstDtbNegotiationMechanism = new ArrayList<NegotiationMechanism>();
		negotiationMechanism = new NegotiationMechanism();
		negotiationMechanism.setIdNegotiationMechanismPk(3L);
		negotiationMechanism.setSelected(true);
		lstDtbNegotiationMechanism.add(negotiationMechanism);
		negotiationMechanism = new NegotiationMechanism();
		negotiationMechanism.setIdNegotiationMechanismPk(1L);
		negotiationMechanism.setSelected(true);
		lstDtbNegotiationMechanism.add(negotiationMechanism);
		negotiationModality.setNegotiationMechanisms(lstDtbNegotiationMechanism);
		lstDtbNegotiationModalities.add(negotiationModality);
		
		return lstDtbNegotiationModalities;
	}

	public boolean registerListConfirmIssuanceAndRequest(List<AccountAnnotationOperation> lstAccountAnnotationOperation,LoggerUser loggerUser) throws ServiceException {
		Boolean sucessIssuanceAux = false;
		Boolean sucessIssuance = false;
		
		if( lstAccountAnnotationOperation !=null && lstAccountAnnotationOperation.size()>0 ) {
			List<IssuanceAux> lstIssuanceAux = new ArrayList<IssuanceAux>();
			List<Issuance> lstIssuance = new ArrayList<Issuance>();
			
			for(AccountAnnotationOperation current: lstAccountAnnotationOperation) {
				lstIssuanceAux.add(instanceIssuanceAux(current));
				lstIssuance.add(instanceIssuance(current));
			}
			
			sucessIssuanceAux = registerListConfirmIssuanceAux(lstIssuanceAux, loggerUser);
			if(sucessIssuanceAux) {
				sucessIssuance = registerListConfirmIssuance(lstIssuance, loggerUser);
			}
		}
		
		return sucessIssuance;
	}
	
	private List<PhysicalCertificate> instancePhysicalCertificate(AccountAnnotationOperation accountAnnotationOperation) {
		List<PhysicalCertificate> lstPhysicalCertificate = new ArrayList<PhysicalCertificate>();
		
		PhysicalCertificate physicalCertificate = new PhysicalCertificate();
		physicalCertificate.setIssuer(accountAnnotationOperation.getIssuer());
		physicalCertificate.setSecurities(accountAnnotationOperation.getSecurity());
		physicalCertificate.setCurrency(accountAnnotationOperation.getSecurityCurrency());	
		physicalCertificate.setExpirationDate(accountAnnotationOperation.getSecurityExpirationDate());
		if(accountAnnotationOperation.getIndSerializable()!=null && accountAnnotationOperation.getIndSerializable().equals(1)) {
			physicalCertificate.setInstrumentType(InstrumentType.VARIABLE_INCOME.getCode());
		}else {
			physicalCertificate.setInstrumentType(InstrumentType.FIXED_INCOME.getCode());
		}
		physicalCertificate.setSecurityClass(accountAnnotationOperation.getSecurityClass());
		physicalCertificate.setCertificateNumber(accountAnnotationOperation.getCertificateNumber());
		physicalCertificate.setCertificateDate(CommonsUtilities.currentDate());
		physicalCertificate.setNominalValue(accountAnnotationOperation.getSecurityNominalValue());
		physicalCertificate.setCertificateQuantity(accountAnnotationOperation.getTotalBalance());
		physicalCertificate.setNominalAmount(physicalCertificate.getNominalValue().multiply(physicalCertificate.getCertificateQuantity()));
		physicalCertificate.setSerialNumber(accountAnnotationOperation.getSerialNumber());
		physicalCertificate.setBranchOffice(accountAnnotationOperation.getBranchOffice());
		if(accountAnnotationOperation.getIndSerializable()!=null && accountAnnotationOperation.getIndSerializable().equals(1)) {
			physicalCertificate.setClassType(SecurityClassType.ANR.getCodeCd());
		}else {
			physicalCertificate.setClassType(SecurityClassType.DPF.getCodeCd());
		}
		physicalCertificate.setHolderAccount(accountAnnotationOperation.getHolderAccount());
		physicalCertificate.setIdPendingSecurityCodePk("");
		physicalCertificate.setIssueDate(CommonsUtilities.currentDate());
		physicalCertificate.setIndElectroniCupon(accountAnnotationOperation.getIndElectronicCupon());
		physicalCertificate.setInterestRate(accountAnnotationOperation.getSecurityInterestRate());
		physicalCertificate.setPaymentPeriodicity(accountAnnotationOperation.getPeriodicity());
		physicalCertificate.setCouponCount((accountAnnotationOperation.getAccountAnnotationCupons()!=null)?accountAnnotationOperation.getAccountAnnotationCupons().size():0);
		physicalCertificate.setSituation(PayrollDetailStateType.VAULT.getCode());//SituationType.CUSTODY_VAULT.getCode()
		physicalCertificate.setState(PhysicalCertificateStateType.CONFIRMED.getCode());
		physicalCertificate.setVaultLocation(PayrollDetailStateType.VAULT.getCode());
		physicalCertificate.setRegisterDate(CommonsUtilities.currentDate());
		physicalCertificate.setRegisterUser("SYSTEM");
		physicalCertificate.setPaymentDate(accountAnnotationOperation.getSecurityExpirationDate());//fecha de expiracion
		physicalCertificate.setMotive(accountAnnotationOperation.getMotive());

		PhysicalCertificate physicalCertificatecupon = null;
		if(accountAnnotationOperation.getAccountAnnotationCupons()!=null && physicalCertificate.getIndElectroniCupon() !=null && physicalCertificate.getIndElectroniCupon() == 0 ) {
			Integer i = 0;
			for(AccountAnnotationCupon cupon: accountAnnotationOperation.getAccountAnnotationCupons()) {
				i++;
				physicalCertificatecupon = new PhysicalCertificate();
				physicalCertificatecupon.setIssuer(accountAnnotationOperation.getIssuer());
				physicalCertificatecupon.setSecurities(accountAnnotationOperation.getSecurity());
				physicalCertificatecupon.setCurrency(accountAnnotationOperation.getSecurityCurrency());	
				physicalCertificatecupon.setExpirationDate(cupon.getExpirationDate());
				physicalCertificatecupon.setInstrumentType(InstrumentType.FIXED_INCOME.getCode());
				physicalCertificatecupon.setSecurityClass(accountAnnotationOperation.getSecurityClass());
				physicalCertificatecupon.setCertificateNumber(accountAnnotationOperation.getCertificateNumber());
				physicalCertificatecupon.setCertificateDate(CommonsUtilities.currentDate());
				physicalCertificatecupon.setNominalValue(cupon.getCuponAmount());//accountAnnotationOperation.getSecurityNominalValue()
				physicalCertificatecupon.setSerialNumber(cupon.getSerialNumber());
				physicalCertificatecupon.setBranchOffice(accountAnnotationOperation.getBranchOffice());
				if(cupon.getSecurityTotalBalance()!=null) {
					physicalCertificatecupon.setCertificateQuantity(cupon.getSecurityTotalBalance());//cupon.getCuponAmount()
					System.out.println("**** registro con el security total balance");
				}else {
					physicalCertificatecupon.setCertificateQuantity(cupon.getCuponAmount());//cupon.getCuponAmount()
					System.out.println("**** registro con el cupon amount");
				}
				physicalCertificatecupon.setClassType(SecurityClassType.DPF.getCodeCd());
				physicalCertificatecupon.setHolderAccount(accountAnnotationOperation.getHolderAccount());
				physicalCertificatecupon.setIdPendingSecurityCodePk(accountAnnotationOperation.getSecurity().getIdSecurityCodePk()+"-"+cupon.getCuponNumber());
				physicalCertificatecupon.setIssueDate(CommonsUtilities.currentDate());
				physicalCertificatecupon.setIndElectroniCupon(accountAnnotationOperation.getIndElectronicCupon());
				physicalCertificatecupon.setInterestRate(cupon.getInterestRate());
				physicalCertificatecupon.setPaymentPeriodicity(accountAnnotationOperation.getPeriodicity());
				physicalCertificatecupon.setCouponCount(1);
				physicalCertificatecupon.setIdProgramInterestFk(cupon.getProgramInterestCoupon().getIdProgramInterestPk());
				physicalCertificatecupon.setPhysicalCertificateReference(physicalCertificate.getIdPhysicalCertificatePk());
				physicalCertificatecupon.setSituation(PayrollDetailStateType.VAULT.getCode());//SituationType.CUSTODY_VAULT.getCode()
				physicalCertificatecupon.setState(PhysicalCertificateStateType.CONFIRMED.getCode());
				physicalCertificatecupon.setVaultLocation(PayrollDetailStateType.VAULT.getCode());
				physicalCertificatecupon.setRegisterDate(CommonsUtilities.currentDate());
				physicalCertificatecupon.setIndCupon(1);
				physicalCertificatecupon.setCuponNumber(cupon.getCuponNumber());
				physicalCertificatecupon.setPaymentDate(cupon.getPaymentDate());
				physicalCertificatecupon.setBeginingDate(cupon.getBeginingDate());
				physicalCertificatecupon.setRegistryDate(cupon.getRegistryDate());
				physicalCertificatecupon.setNominalAmount(cupon.getCuponAmount());
				physicalCertificatecupon.setRegisterUser("SYSTEM");
				physicalCertificatecupon.setMotive(accountAnnotationOperation.getMotive());
				
				lstPhysicalCertificate.add(physicalCertificatecupon);
			}
		}
		
		lstPhysicalCertificate.add(physicalCertificate);
		
		return lstPhysicalCertificate;
	}
	
	private IssuanceAux instanceIssuanceAux(AccountAnnotationOperation accountAnnotationOperation) {
		//CommonsUtilities.getHoursMinutSecondsBetween(null, null)
		Date date = accountAnnotationOperation.getExpeditionDate();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		Integer year = calendar.get(Calendar.YEAR);
		
		Date firstDayOfYear = CommonsUtilities.convertStringtoDate("01/01/"+year);

		
		IssuanceAux issuance = new IssuanceAux();
		issuance.setCurrency(accountAnnotationOperation.getSecurityCurrency());	
		issuance.setDescription(accountAnnotationOperation.getSerialNumber());
		issuance.setAmortizationAmount(BigDecimal.ZERO);
		issuance.setCirculationAmount(BigDecimal.ZERO);
		issuance.setCreditRatingScales(2321);
		issuance.setExpirationDate(accountAnnotationOperation.getSecurityExpirationDate());
		issuance.setIssuanceDate(accountAnnotationOperation.getExpeditionDate());
		issuance.setGeographicLocation(new GeographicLocation());
		issuance.getGeographicLocation().setIdGeographicLocationPk(18156);
		issuance.setIndExchangeSecurities(0);
		issuance.setIndHaveCertificate(0);
		issuance.setIndPrimaryPlacement(0);
		issuance.setIndRegulatorReport(0);
		issuance.setInstrumentType(InstrumentType.FIXED_INCOME.getCode());
		issuance.setIssuanceAmount(accountAnnotationOperation.getSecurityNominalValue().multiply(accountAnnotationOperation.getTotalBalance()));
		issuance.setIssuanceDate(firstDayOfYear);
		issuance.setIssuanceType(IssuanceType.PHYSICAL.getCode());
		issuance.setIssuer(accountAnnotationOperation.getIssuer());
		issuance.setNumberPlacementTranches(0);
		issuance.setNumberTotalTranches(0);
		issuance.setPlacedAmount(BigDecimal.ZERO);
		issuance.setPlacementType(SecurityPlacementType.FIXED.getCode());//691;
		issuance.setRegistryDate(firstDayOfYear);
		issuance.setSecuritiesInNegotiations(false);
		issuance.setSecuritiesRegisted(false);
		issuance.setSecurityClass(accountAnnotationOperation.getSecurityClass());
		issuance.setSecurityType(SecurityType.PRIVATE.getCode());
		issuance.setStateChk(IssuanceAuxStateType.CONFIRMED.getCode());

		return issuance;
	}
	
	private Issuance instanceIssuance(AccountAnnotationOperation accountAnnotationOperation) {
		Issuance issuance = new Issuance();
		issuance.setCurrency(accountAnnotationOperation.getSecurityCurrency());	
		issuance.setDescription(accountAnnotationOperation.getSerialNumber());
		issuance.setAmortizationAmount(BigDecimal.ZERO);
		issuance.setCirculationAmount(BigDecimal.ZERO);
		issuance.setCreditRatingScales(2321);
		issuance.setExpirationDate(accountAnnotationOperation.getSecurityExpirationDate());
		issuance.setIssuanceDate(accountAnnotationOperation.getExpeditionDate());
		issuance.setGeographicLocation(new GeographicLocation());
		issuance.getGeographicLocation().setIdGeographicLocationPk(18156);
		issuance.setIndExchangeSecurities(0);
		issuance.setIndHaveCertificate(0);
		issuance.setIndPrimaryPlacement(0);
		issuance.setIndRegulatorReport(0);
		issuance.setInstrumentType(InstrumentType.FIXED_INCOME.getCode());
		issuance.setIssuanceAmount(accountAnnotationOperation.getSecurityNominalValue().multiply(accountAnnotationOperation.getTotalBalance()));
		issuance.setIssuanceDate(issuance.getIssuanceDate());
		issuance.setIssuanceType(IssuanceType.PHYSICAL.getCode());
		issuance.setIssuer(accountAnnotationOperation.getIssuer());
		issuance.setNumberPlacementTranches(0);
		issuance.setNumberTotalTranches(0);
		issuance.setPlacedAmount(BigDecimal.ZERO);
		issuance.setPlacementType(SecurityPlacementType.FIXED.getCode());//691;
		issuance.setRegistryDate(CommonsUtilities.currentDate());
		issuance.setSecuritiesInNegotiations(false);
		issuance.setSecuritiesRegisted(false);
		issuance.setSecurityClass(accountAnnotationOperation.getSecurityClass());
		issuance.setSecurityType(SecurityType.PRIVATE.getCode());
		issuance.setStateIssuance(IssuanceStateType.AUTHORIZED.getCode());
		return issuance;
	}
	
	private Security instanceSecurity(AccountAnnotationOperation accountAnnotationOperation) {
				
		Boolean cuponZero = (accountAnnotationOperation.getAccountAnnotationCupons() == null || (accountAnnotationOperation.getAccountAnnotationCupons()!=null && accountAnnotationOperation.getAccountAnnotationCupons().size() == 0) );
		
		Security security = new Security();
		security.setModifyIssuance(false);
		security.setIssuance(accountAnnotationOperation.getIssuance());
		security.setIssuer(accountAnnotationOperation.getIssuer());
		security.setIndPrepaid(0);
		security.setDescription(accountAnnotationOperation.getSerialNumber());
		security.setMnemonic(accountAnnotationOperation.getSerialNumber());
		security.setSecuritySerial(accountAnnotationOperation.getSerialNumber()+accountAnnotationOperation.getCertificateNumber());
		security.setCashNominal(InterestClassType.EFFECTIVE.getCode());
		security.setInstrumentType(InstrumentType.FIXED_INCOME.getCode());
		security.setCurrency(accountAnnotationOperation.getSecurityCurrency());
		security.setIndConvertibleStock(0);
		security.setSecuritySource(SecuritySourceType.NATIONAL.getCode());
		security.setIssuanceCountry(new GeographicLocation());
		security.getIssuanceCountry().setIdGeographicLocationPk(18156);
		security.setSecurityType(SecurityType.PRIVATE.getCode());
		security.setSecurityClass(accountAnnotationOperation.getSecurityClass());
		security.setIssuanceForm(IssuanceType.PHYSICAL.getCode());
		security.setIssuanceDate(accountAnnotationOperation.getExpeditionDate());
		security.setIndEarlyRedemption(0);
		security.setExpirationDate(accountAnnotationOperation.getSecurityExpirationDate());
		security.setIndIssuanceProtest(0);
		
		if(cuponZero) {
			
			security.setInterestType(InterestType.CERO.getCode());
			security.setInterestPaymentModality(InterestPaymentModalityType.AT_MATURITY.getCode());
			security.setNumberCoupons(0);
			security.setCalendarDays(CalendarDayType._365.getCode());
			
		}else {
			
			if(accountAnnotationOperation.getPeriodicity() == null || accountAnnotationOperation.getPeriodicity().equals(InterestPaymentModalityType.AT_MATURITY.getCode())) {
				security.setInterestType(InterestType.FIXED.getCode());
				security.setPeriodicity(null);
				security.setInterestPaymentModality(InterestPaymentModalityType.AT_MATURITY.getCode());
			}else {
				security.setInterestType(accountAnnotationOperation.getSecurityInterestType());
				security.setPeriodicity(accountAnnotationOperation.getPeriodicity());
				security.setInterestPaymentModality(InterestPaymentModalityType.BY_COUPON.getCode());
			}

			security.setCalendarDays(CalendarDayType._365.getCode());
			security.setInterestRate(accountAnnotationOperation.getSecurityInterestRate());

			if(accountAnnotationOperation.getAccountAnnotationCupons() == null) {
				security.setNumberCoupons(1);
			}else {
				security.setNumberCoupons(accountAnnotationOperation.getAccountAnnotationCupons().size());
			}
			
			security.setCouponFirstExpirationDate(accountAnnotationOperation.getAccountAnnotationCupons().get(0).getExpirationDate());
			security.setPaymentFirstExpirationDate(accountAnnotationOperation.getAccountAnnotationCupons().get(0).getPaymentDate());
		}
		
		security.setAmortizationType(AmortizationType.PROPORTIONAL.getCode());
		security.setAmortizationFactor(new BigDecimal(100));
		security.setStockRegistryDays(1);
		security.setCorporativeProcessDays(0);
		security.setPlacedAmount(BigDecimal.ZERO);
		security.setMinimumInvesment(accountAnnotationOperation.getSecurityNominalValue());
		security.setMaximumInvesment(BigDecimal.ZERO);
		security.setIndSplitCoupon(1);
		security.setIndIsFractionable(1);
		security.setIndIsCoupon(0);
		

		if(accountAnnotationOperation.getIndExpirationCupons()!=null && accountAnnotationOperation.getIndExpirationCupons().equals(1)) {  
			security.setStateExpirate(true);
		}else {
			security.setStateExpirate(false);
		}
		
		if( !accountAnnotationOperation.getMotive().equals(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode()) ){
			security.setIndPaymentBenefit(1);
		}else {
			security.setIndPaymentBenefit(0);
		}
		
		security.setIndIssuanceManagement(0);
		security.setIndReceivableCustody(0);
		security.setIndSecuritization(0);
		security.setIndTraderSecondary(1);
		security.setCalendarType(CalendarType.CALENDAR.getCode());
		security.setIndCapitalizableInterest(0);
		security.setRegistryDate(CommonsUtilities.currentDate());
		security.setAmortizationPeriodicity(null);
		security.setCapitalPaymentModality(CapitalPaymentModalityType.AT_MATURITY.getCode());	
		security.setAmortizationOn(AmortizationOnType.CAPITAL.getCode());
		security.setInscriptionDate(accountAnnotationOperation.getExpeditionDate());
		security.setIndHasSplitSecurities(0);
		security.setIndHolderDetail(0);
		security.setIndTaxExempt(accountAnnotationOperation.getIndTaxExoneration());
		security.setCalendarMonth(CalendarMonthType.ACTUAL.getCode());
		security.setIndIsDetached(0);
		security.setIdIsinCode(accountAnnotationOperation.getCertificateNumber());
		security.setSecurityTerm(SecurityTermType.SHORT_TERM_INS.getCode());
		security.setIndSubordinated(0);
		security.setIndExtendedTerm(0);
		Integer securityDaysTerm = 0;
		try {
			securityDaysTerm = CommonsUtilities.diffDatesInDays(security.getIssuanceDate(), security.getExpirationDate());
			Integer securityMonthsTerm = securityDaysTerm/30;
			security.setSecurityMonthsTerm(securityMonthsTerm); 
		}catch(Exception e){
			securityDaysTerm = 1;
			security.setSecurityMonthsTerm(1);
		}
		security.setSecurityDaysTerm(securityDaysTerm);
		
		String codEmisor = accountAnnotationOperation.getIssuer().getMnemonic();
		String moneda = ( accountAnnotationOperation.getSecurityCurrency().equals(CurrencyType.PYG.getCode()) )?"G":"D";
		String serie = accountAnnotationOperation.getSerialNumber()+accountAnnotationOperation.getCertificateNumber();
		security.setIdSecurityCodeOnly(codEmisor+moneda+serie);
		

		BigDecimal montoValor= accountAnnotationOperation.getTotalBalance().multiply(accountAnnotationOperation.getSecurityNominalValue());
		security.setShareCapital(BigDecimal.ZERO);//montoValor
		security.setShareBalance(BigDecimal.ZERO);//accountAnnotationOperation.getTotalBalance()
		security.setCirculationAmount(BigDecimal.ZERO);//montoValor
		security.setCirculationBalance(BigDecimal.ZERO);//accountAnnotationOperation.getTotalBalance()
		security.setAmortizationAmount(montoValor);
		security.setPhysicalBalance(accountAnnotationOperation.getTotalBalance());
		security.setDesmaterializedBalance(BigDecimal.ZERO);//montoValor
		security.setPhysicalDepositBalance(BigDecimal.ZERO);//accountAnnotationOperation.getTotalBalance()
		
		security.setIndPrepaid(0);
		security.setInitialNominalValue(accountAnnotationOperation.getSecurityNominalValue());
		security.setCurrentNominalValue(accountAnnotationOperation.getSecurityNominalValue());
		security.setReferencePartCode(accountAnnotationOperation.getReferencePartCode());//observacion
		security.setIndPaymentReporting(0);

		security.setAmortizationPaymentSchedule(new AmortizationPaymentSchedule());
		security.setNotTraded(0);

		Date notExpirationDate = CommonsUtilities.addDate(CommonsUtilities.currentDate(), -1);
		
		if(!cuponZero) {

			security.setInterestPaymentSchedule(new InterestPaymentSchedule());
			security.getInterestPaymentSchedule().setDataFromSecurity(security);
			security.getInterestPaymentSchedule().setProgramInterestCoupons(new ArrayList<ProgramInterestCoupon>());
			
			InterestPaymentSchedule interestPaymentSchedule = new InterestPaymentSchedule();
			interestPaymentSchedule.setSecurity(security);
			interestPaymentSchedule.setPeriodicity(refactorInterestPaymentPeridiocity(accountAnnotationOperation.getPeriodicity()));
			interestPaymentSchedule.setExpirationDate(security.getExpirationDate());
			interestPaymentSchedule.setCalendarType(CalendarType.CALENDAR.getCode());
			interestPaymentSchedule.setInterestType(accountAnnotationOperation.getSecurityInterestType());
			interestPaymentSchedule.setInterestFactor(accountAnnotationOperation.getSecurityInterestRate());
			interestPaymentSchedule.setRegistryDays(1);
			interestPaymentSchedule.setCashNominal(567);//security.getCurrentNominalValue().intValue()
			interestPaymentSchedule.setScheduleState(PaymentScheduleStateType.REGTERED.getCode());
			interestPaymentSchedule.setCalendarMonth(CalendarMonthType.ACTUAL.getCode());
			interestPaymentSchedule.setCalendarDays(CalendarDayType._365.getCode());
			
			security.setInterestPaymentSchedule(interestPaymentSchedule);
			security.getInterestPaymentSchedule().setProgramInterestCoupons(new ArrayList<ProgramInterestCoupon>());
			ProgramInterestCoupon programInterestCoupon = null;
			for( AccountAnnotationCupon cupon: accountAnnotationOperation.getAccountAnnotationCupons() ) {
				programInterestCoupon = new ProgramInterestCoupon();
				programInterestCoupon.setInterestPaymentCronogram(interestPaymentSchedule);
				programInterestCoupon.setBeginingDate(cupon.getBeginingDate());
				programInterestCoupon.setExperitationDate(cupon.getExpirationDate());
				programInterestCoupon.setCouponNumber(cupon.getCuponNumber());
				programInterestCoupon.setRegistryDate(cupon.getRegistryDate());
				programInterestCoupon.setCutoffDate(programInterestCoupon.getRegistryDate());
				programInterestCoupon.setPaymentDate(cupon.getPaymentDate());
				programInterestCoupon.setCorporativeDate(programInterestCoupon.getPaymentDate());
				programInterestCoupon.setPaymentDays(cupon.getPaymentDays());
				programInterestCoupon.setCurrency(security.getCurrency());
				programInterestCoupon.setInterestFactor(cupon.getInterestFactor());
				programInterestCoupon.setInterestRate(cupon.getInterestRate());
				programInterestCoupon.setIndRounding(0);
				programInterestCoupon.setPayedAmount(BigDecimal.ZERO);
				programInterestCoupon.setCouponAmount(cupon.getCuponAmount());
				
				if(cupon.getExpirationDate() !=null && cupon.getExpirationDate().after(notExpirationDate)) {
					programInterestCoupon.setStateProgramInterest(ProgramScheduleStateType.PENDING.getCode());
					programInterestCoupon.setSituationProgramInterest(ProgramScheduleSituationType.PENDING.getCode());
				}else {
					programInterestCoupon.setStateProgramInterest(ProgramScheduleStateType.EXPIRED.getCode());
					programInterestCoupon.setSituationProgramInterest(ProgramScheduleSituationType.ISSUER_PAID.getCode());
				}
				//programInterestCoupon.setRegistryDate(CommonsUtilities.currentDate());
				security.getInterestPaymentSchedule().getProgramInterestCoupons().add(programInterestCoupon);
			}
		}
		
		AmortizationPaymentSchedule amortizationPaymentSchedule = new AmortizationPaymentSchedule();
		amortizationPaymentSchedule.setSecurity(security);
		amortizationPaymentSchedule.setAmortizationFactor(new BigDecimal(100));
		amortizationPaymentSchedule.setRegistryDays(1);
		amortizationPaymentSchedule.setCorporativeDays(0);
		amortizationPaymentSchedule.setScheduleState(PaymentScheduleStateType.REGTERED.getCode());
		amortizationPaymentSchedule.setCalendarType(CalendarType.CALENDAR.getCode());
		amortizationPaymentSchedule.setPaymentModality(CapitalPaymentModalityType.AT_MATURITY.getCode());
		amortizationPaymentSchedule.setPaymentFirstExpirationDate(security.getPaymentFirstExpirationDate());
		amortizationPaymentSchedule.setNumberPayments(1);
		security.setAmortizationPaymentSchedule(amortizationPaymentSchedule);

		security.getAmortizationPaymentSchedule().setProgramAmortizationCoupons(new ArrayList<ProgramAmortizationCoupon>());
		ProgramAmortizationCoupon programAmortizationCoupon = new ProgramAmortizationCoupon();
		programAmortizationCoupon.setAmortizationPaymentSchedule(amortizationPaymentSchedule);
		programAmortizationCoupon.setCouponNumber(1);
		programAmortizationCoupon.setCurrency(security.getCurrency());
		programAmortizationCoupon.setAmortizationFactor(new BigDecimal(100));
		programAmortizationCoupon.setPaymentDate((accountAnnotationOperation.getPaymentDate()!=null)? accountAnnotationOperation.getPaymentDate() : security.getExpirationDate() );//
		programAmortizationCoupon.setRegistryDate(accountAnnotationOperation.getRegistryDate());
		programAmortizationCoupon.setCutoffDate(accountAnnotationOperation.getRegistryDate());
		programAmortizationCoupon.setCorporativeDate((accountAnnotationOperation.getPaymentDate()!=null)? accountAnnotationOperation.getPaymentDate() : security.getExpirationDate() );
		programAmortizationCoupon.setBeginingDate(accountAnnotationOperation.getBeginingDate());
		programAmortizationCoupon.setExpirationDate(accountAnnotationOperation.getSecurityExpirationDate());
		programAmortizationCoupon.setIndRounding(2);
		programAmortizationCoupon.setRegisterDate(programAmortizationCoupon.getPaymentDate());
		programAmortizationCoupon.setAmortizationAmount(security.getAmortizationAmount());
		programAmortizationCoupon.setPayedAmount(BigDecimal.ZERO);
		programAmortizationCoupon.setCouponAmount(programAmortizationCoupon.getAmortizationAmount());
		programAmortizationCoupon.setRegisterDate(accountAnnotationOperation.getRegistryDate());
		programAmortizationCoupon.setRegistryUser("SYSTEM");
		security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().add(programAmortizationCoupon);

		if(accountAnnotationOperation.getSecurityExpirationDate()!=null && accountAnnotationOperation.getSecurityExpirationDate().after(notExpirationDate)) {
			programAmortizationCoupon.setStateProgramAmortization(ProgramScheduleStateType.PENDING.getCode());
			programAmortizationCoupon.setSituationProgramAmortization(ProgramScheduleSituationType.PENDING.getCode());
		}else {
			programAmortizationCoupon.setStateProgramAmortization(ProgramScheduleStateType.EXPIRED.getCode());
			programAmortizationCoupon.setSituationProgramAmortization(ProgramScheduleSituationType.ISSUER_PAID.getCode());
		}
		
		SecurityInvestor securityInvestor = new SecurityInvestor();
		securityInvestor.setSecurity(security);
		securityInvestor.setIndNatural(1);
		securityInvestor.setIndJuridical(1);
		securityInvestor.setIndLocal(1);
		securityInvestor.setIndForeign(1);
		securityInvestor.setSecurityInvestorState(SecurityInversionistStateType.REGISTERED.getCode());
		securityInvestor.setIndCfi(0);
		
		security.setSecurityInvestor(securityInvestor);
		security.setMotiveAnnotation(accountAnnotationOperation.getMotive());
		
		return security;
	}
	
	public Integer refactorInterestPaymentPeridiocity(Integer periodicity) {
		//
		if(periodicity.equals(PeriodType.MONTHLY.getCode())) {
			return InterestPeriodicityType.MONTHLY.getCode();
		}else if(periodicity.equals(PeriodType.BIMONTHLY.getCode())) {
			return InterestPeriodicityType.BIMONTHLY.getCode();
		}else if(periodicity.equals(PeriodType.QUARTERLY.getCode())) {
			return InterestPeriodicityType.QUARTERLY.getCode();
		}else if(periodicity.equals(PeriodType.FOURMONTHLY.getCode())) {
			return InterestPeriodicityType.FOURMONTHLY.getCode();
		}else if(periodicity.equals(PeriodType.BIANNUAL.getCode())) {
			return InterestPeriodicityType.BIANNUAL.getCode();
		}else if(periodicity.equals(PeriodType.ANNUAL.getCode())) {
			return InterestPeriodicityType.ANNUAL.getCode();
		}
		
		return periodicity;
	}
	
	
	public boolean registerListConfirmIssuanceAux(List<IssuanceAux> lstIssuanceAux,LoggerUser loggerUser){
		if(lstIssuanceAux!=null && lstIssuanceAux.size()>0) {
			try {
				for(IssuanceAux current: lstIssuanceAux) {
					registryIssuanceAuxServiceBean(current, loggerUser);
					current.setStateChk(IssuanceAuxStateType.CONFIRMED.getCode());
				}
			} catch (ServiceException e) {
				e.printStackTrace();
				return false;
			}
			return true;
		}
		return false;
	}
	
	public boolean registerListConfirmIssuance(List<Issuance> lstIssuance,LoggerUser loggerUser) throws ServiceException {
		if(lstIssuance!=null && lstIssuance.size()>0) {
			try {
				for(Issuance current: lstIssuance) {
					registryIssuanceServiceBean(current, loggerUser);
				}
			} catch (ServiceException e) {
				e.printStackTrace();
				return false;
			}
			return true;
		}
		return false;
	}
	
	public PayrollHeader findPayrollHeaderWithAccountAnnotationOperation(Long idAnnotationOperationPk){
		StringBuilder strbQuery=new StringBuilder();
		strbQuery.append("SELECT ph FROM PayrollHeader ph "
				+ " JOIN FETCH  ph.lstPayrollDetail prd "
				+ " JOIN FETCH  prd.accountAnnotationOperation aao "
				+ " WHERE aao.idAnnotationOperationPk =: idAnnotationOperationPk and prd.indCupon = 0 "
				+ " ORDER BY prd.idPayrollDetailPk ASC ");
		Query query=em.createQuery(strbQuery.toString());
		query.setParameter("idAnnotationOperationPk", idAnnotationOperationPk);
		
		List<PayrollHeader> lstPayrollHeader = query.getResultList();
		return lstPayrollHeader.get(0);
	}

	public PayrollHeader findPayrollHeader(Long idPayrollHeaderPk) throws ServiceException{
		StringBuilder strbQuery=new StringBuilder();
		strbQuery.append("SELECT ph FROM PayrollHeader ph "
				+ " JOIN FETCH  ph.lstPayrollDetail prd "
				+ " JOIN FETCH  prd.accountAnnotationOperation aao "
				+ " JOIN FETCH  aao.accountAnnotationCupons aac "
				+ " JOIN FETCH  aao.issuer ie "
				+ " LEFT JOIN FETCH  aao.security sec "
				+ " WHERE ph.idPayrollHeaderPk =: idPayrollHeaderPk "
				+ " AND prd.currentVault =: currentVault "
				+ " AND prd.indSecurityCreated =: indSecurityCreated "
				+ " AND aao.indSerializable =: indSerializable "
				+ " ORDER BY prd.idPayrollDetailPk ASC ");
		
		Query query=em.createQuery(strbQuery.toString());
		query.setParameter("idPayrollHeaderPk", idPayrollHeaderPk);
		query.setParameter("currentVault", VaultType.VAULT_CAVAPY.getCode());//boveda cavapy
		query.setParameter("indSecurityCreated", 0);//valor no creado
		query.setParameter("indSerializable", 0);//valores no serializados - osea los que no estan amarrados a un valor al crearse
		
		return (PayrollHeader)query.getSingleResult();
	}

	public PayrollDetail findPayrollDetailPendingCreate(Long idPayrollDetailPk) throws ServiceException{
		StringBuilder strbQuery=new StringBuilder();
		strbQuery.append("SELECT prd FROM PayrollDetail prd "
				+ " JOIN FETCH  prd.accountAnnotationOperation aao "
				+ " LEFT JOIN FETCH  aao.accountAnnotationCupons aac "
				+ " LEFT JOIN FETCH  aao.security sec "
				+ " JOIN FETCH  aao.issuer ie "
				+ " WHERE prd.idPayrollDetailPk =: idPayrollDetailPk "
				+ " AND prd.currentVault =: currentVault "
				+ " AND prd.indSecurityCreated != : notIndSecurityCreated "
				+ " ORDER BY prd.idPayrollDetailPk ASC ");
		
		Query query=em.createQuery(strbQuery.toString());
		query.setParameter("idPayrollDetailPk", idPayrollDetailPk);
		query.setParameter("currentVault", VaultType.VAULT_CAVAPY.getCode());//boveda cavapy
		query.setParameter("notIndSecurityCreated", 1);//valor no creado
		
		return (PayrollDetail)query.getSingleResult();
	}
	

	public List<PayrollDetail> findListPayrollDetailWithAnnotationOperation(Long idAnnotationOperationPk) throws ServiceException{
		StringBuilder strbQuery=new StringBuilder();
		strbQuery.append("SELECT prd FROM PayrollDetail prd "
				+ " JOIN FETCH  prd.accountAnnotationOperation aao "
				+ " WHERE aao.idAnnotationOperationPk =: idAnnotationOperationPk "
				+ " AND prd.currentVault =: currentVault "
				+ " AND prd.indSecurityCreated =: indSecurityCreated "
				+ " ORDER BY prd.idPayrollDetailPk ASC ");
		
		Query query=em.createQuery(strbQuery.toString());
		query.setParameter("idAnnotationOperationPk", idAnnotationOperationPk);
		query.setParameter("currentVault", VaultType.VAULT_CAVAPY.getCode());//boveda cavapy
		query.setParameter("indSecurityCreated", 0);//valor no creado
		
		return query.getResultList();
	}
	
	//ok
	public VaultControlComponent getVaultControlComponentWithSecurity(String idSecurityCodeFk) {
		StringBuilder strbQuery=new StringBuilder();
		strbQuery.append(" SELECT v FROM VaultControlComponent v WHERE v.idSecurityCodeFk = :idSecurityCodeFk ");
		
		TypedQuery<VaultControlComponent> query=em.createQuery(strbQuery.toString(), VaultControlComponent.class);
		query.setParameter("idSecurityCodeFk", idSecurityCodeFk);
		
		List<VaultControlComponent> lstVault = query.getResultList();
		if(lstVault != null) {
			return lstVault.get(0);
		}
		
		return null;
	}
	
	public void updateIssuanceAmountWithSecurity(String idSecurityCodePk){
        StringBuilder sb=new StringBuilder();		
        sb.append(" UPDATE ISSUANCE   ");
        sb.append(" SET AMORTIZATION_AMOUNT = ( SELECT sum(nvl(s.AMORTIZATION_AMOUNT,0))   ");
        sb.append(" 							FROM SECURITY s   ");
        sb.append(" 							WHERE s.ID_ISSUANCE_CODE_FK=ID_ISSUANCE_CODE_PK AND s.STATE_SECURITY != :stateSecurityNotIn )   ");
        sb.append(" WHERE STATE_ISSUANCE != :stateIssuanceNotIn   ");
        sb.append(" AND ID_ISSUANCE_CODE_PK = (SELECT s1.ID_ISSUANCE_CODE_FK FROM SECURITY s1 WHERE s1.ID_SECURITY_CODE_PK= :idSecurityCodePk)  ");
        
		Query query= em.createNativeQuery(sb.toString());		
		query.setParameter("stateSecurityNotIn", SecurityStateType.EXPIRED.getCode());
		query.setParameter("stateIssuanceNotIn", IssuanceStateType.EXPIRED.getCode());
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		query.executeUpdate();
	}
	
}
