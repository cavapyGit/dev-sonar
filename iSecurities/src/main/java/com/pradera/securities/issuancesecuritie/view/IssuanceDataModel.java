package com.pradera.securities.issuancesecuritie.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.issuancesecuritie.Issuance;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class IssuanceDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class IssuanceDataModel extends ListDataModel<Issuance> implements SelectableDataModel<Issuance>, Serializable{

	/**
	 * Instantiates a new issuance data model.
	 */
	public IssuanceDataModel(){
		super();
	}
	
	/**
	 * Instantiates a new issuance data model.
	 *
	 * @param data the data
	 */
	public IssuanceDataModel(List<Issuance> data){
		super(data);
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(Issuance issuane) {
		// TODO Auto-generated method stub
		return issuane.getIdIssuanceCodePk();
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public Issuance getRowData(String rowKey) {
		// TODO Auto-generated method stub
		List<Issuance> lstIssuance=(List<Issuance>)getWrappedData();
		for(Issuance issuance:lstIssuance){
			if(issuance.getIdIssuanceCodePk().equals( rowKey ))
				return issuance;
		}
		return null;
	}
	
	


}
