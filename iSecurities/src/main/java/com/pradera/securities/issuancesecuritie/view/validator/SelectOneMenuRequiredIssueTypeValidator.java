package com.pradera.securities.issuancesecuritie.view.validator;

import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.primefaces.component.selectonemenu.SelectOneMenu;

import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.securities.issuancesecuritie.util.PropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class SelectOneMenuIssueTypeValidator.
 * This validator is used when  choosing, Mixed issuance type with Fixed income instrument type
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@FacesValidator("com.pradera.secuities.issuancesecuritie.view.validator.SelectOneMenuIssueTypeValidator")
public class SelectOneMenuRequiredIssueTypeValidator implements Validator{

	/* (non-Javadoc)
	 * @see javax.faces.validator.Validator#validate(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
	 */
	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		SelectOneMenu combo;
		Locale locale = context.getViewRoot().getLocale();
		FacesMessage fMsg;
		
		if(component.getAttributes().get("attrInstrumentType")==null || value == null){
			return;
		}
		Integer instrumentType=Integer.valueOf( component.getAttributes().get("attrInstrumentType").toString() );	

		if(Integer.valueOf( value.toString() ).equals(IssuanceType.MIXED.getCode())
				&& instrumentType.equals( InstrumentType.FIXED_INCOME.getCode() )){
			combo = (SelectOneMenu) component;
			Object[] parameters = {combo.getLabel() };
			String strMsg =  PropertiesUtilities.getMessage(locale,PropertiesConstants.ISSUANCE_ERROR_MIXED_ISSUE_TYPE, parameters);
			fMsg = new FacesMessage(strMsg);
			fMsg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(fMsg);
		}
	}
	

}