package com.pradera.securities.issuancesecuritie.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.configuration.ValuatorSetup;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.HelperBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.valuatorprocess.SecurityClassValuator;
import com.pradera.model.valuatorprocess.ValuatorProcessClass;
import com.pradera.model.valuatorprocess.ValuatorProcessSetup;
import com.pradera.model.valuatorprocess.ValuatorSecurityCode;
import com.pradera.model.valuatorprocess.ValuatorTermRange;
import com.pradera.model.valuatorprocess.type.ValuatorSecurityCodSituation;
import com.pradera.securities.valuator.classes.facade.SecuritiesClassValuatorFacade;
import com.pradera.securities.valuator.config.facade.ValuatorRangeFacade;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorHistoryDetailBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@HelperBean
public class ValuatorHistoryDetailBean extends GenericBaseBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6279989547821004996L;

	/** The valuator setup. */
	@Inject 
	@ValuatorSetup 
	private ValuatorProcessSetup valuatorSetup;
	
	/** The general parameter facade. */
	@EJB
	private GeneralParametersFacade generalParameterFacade;
	
	/** The securities class valuator facade. */
	@EJB
	private SecuritiesClassValuatorFacade securitiesClassValuatorFacade;
	
	/** The valuator range facade. */
	@EJB
	private ValuatorRangeFacade valuatorRangeFacade;
	
	/** The security. */
	private Security security; 
	
	/** The valuator term range. */
	private ValuatorTermRange valuatorTermRange;
	
	/** The parameter table in view. */
	private ParameterTable parameterTableInView;
	
	/** The parameter table to. */
	private ParameterTableTO parameterTableTO;
	
	/** The lst cbo security class valuator. */
	private List<SecurityClassValuator> lstCboSecurityClassValuator;
	
	/** The lst dtb valuator security code. */
	private List<ValuatorSecurityCode> lstDtbValuatorSecurityCode;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		try {
			super.setParametersTableMap(new HashMap<Integer,Object>());
			security=(Security) JSFUtilities.getValueExpression("#{cc.attrs.security}", Security.class);
			setViewOperationType( (Integer) JSFUtilities.getValueExpression("#{cc.attrs.viewOperationType}", Integer.class)  );
			parameterTableInView=new ParameterTable();
			parameterTableTO=new ParameterTableTO();
			loadParamsInformationType();
			loadParamsValuatorSituation();
			loadParamsValuatorType();
//			if(!isViewOperationRegister()){
//				initializeComponent();
//			}
		} catch (Exception e) {
			excepcion.fire( new ExceptionToCatchEvent(e) );
		}
	}
	
	/**
	 * Initialize component.
	 *
	 * @throws ServiceException the service exception
	 */
	public void initializeComponent() throws ServiceException{
		
		valuatorTermRange=new ValuatorTermRange();
		lstCboSecurityClassValuator=new ArrayList<SecurityClassValuator>();
		if(isViewOperationRegister()){
			boolean dataCompleted=true;
			if(security.getInstrumentType()==null || security.getSecurityClass()==null || security.getCurrency()==null){			
				dataCompleted=false;
			}
			if(security.isFixedInstrumentTypeAcc()){
				if(security.getSecurityDaysTerm()==null){
					dataCompleted=false;
				}
			}
			if(!dataCompleted){
				security.setValuatorProcessClass(new ValuatorProcessClass());
				return;
			}
		}

		loadParametersDescriptions();
		laodCboValuatorClass();
		if(security.isFixedInstrumentTypeAcc()){
			loadValuatorTermRange();
		}
		
		if(!isViewOperationRegister()){
			loadDtbDtbValuatorSecurityCode();
		}
	}
	
	/**
	 * Load valuator term range.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadValuatorTermRange() throws ServiceException{
		valuatorTermRange=valuatorRangeFacade.getValuatorRangeBySecurityDaysFacade(security.getSecurityDaysTerm());
	}
	
	/**
	 * Load parameters descriptions.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadParametersDescriptions() throws ServiceException{
		billParameterTableById(security.getSecurityClass(),true);
		billParameterTableById(security.getCurrency(),true);
		billParameterTableById(security.getInstrumentType());
	}
	
	/**
	 * Load dtb dtb valuator security code.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadDtbDtbValuatorSecurityCode() throws ServiceException{
		List<ValuatorSecurityCode> valuatorCodes = securitiesClassValuatorFacade.getValuatorSecurityCodeBySecurity(security);
		if(valuatorCodes!=null && !valuatorCodes.isEmpty()){
			for(int i=0; i<valuatorCodes.size(); i++){
				if(i!=0){
					valuatorCodes.get(i).setValuatorSituation(ValuatorSecurityCodSituation.HISTORY.getCode());
				}
			}
		}
		lstDtbValuatorSecurityCode=valuatorCodes;
	}
	
	/**
	 * Change cbo valuator class val.
	 */
	public void changeCboValuatorClassVal(){
		try {
			ValuatorProcessClass valProcessClass=securitiesClassValuatorFacade.getValuatorProcessClassByPkFacade(security.getValuatorProcessClass().getIdValuatorClassPk());
			if(valProcessClass==null){				
				security.setValuatorProcessClass(new ValuatorProcessClass());
			}else{
				security.setValuatorProcessClass(valProcessClass);
			}
		} catch (Exception e) {
			excepcion.fire( new ExceptionToCatchEvent(e) );
		}
	}
	
	/**
	 * Laod cbo valuator class.
	 *
	 * @throws ServiceException the service exception
	 */
	public void laodCboValuatorClass() throws ServiceException{
		SecurityClassValuator sec=new SecurityClassValuator();
		sec.setIdSecurityClassFk(security.getSecurityClass());
		lstCboSecurityClassValuator=securitiesClassValuatorFacade.getValuatorClassesBySecClassFacade(sec);
		
		ValuatorProcessClass valuatorProcessClass = securitiesClassValuatorFacade.getValuatorProcessClass(security,lstCboSecurityClassValuator);
		
		security.setValuatorProcessClass(valuatorProcessClass);
	}
	
	/**
	 * Load params valuator type.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadParamsValuatorType() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk(MasterTableType.VALUATOR_TYPE.getCode());
		List<ParameterTable> lstParamTbl=generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		billParametersTableMap(lstParamTbl);
	}
	
	/**
	 * Load params information type.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadParamsInformationType() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk(MasterTableType.VALUATOR_INFORMATION_TYPE.getCode());
		List<ParameterTable> lstParamTbl=generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		billParametersTableMap(lstParamTbl);
	}
	
	/**
	 * Load params valuator situation.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadParamsValuatorSituation() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk(MasterTableType.VALUATOR_SITUATION.getCode());
		List<ParameterTable> lstParamTbl=generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		billParametersTableMap(lstParamTbl);
	}
	
	/**
	 * Bill parameter table by id.
	 *
	 * @param parameterPk the parameter pk
	 * @throws ServiceException the service exception
	 */
	public void billParameterTableById(Integer parameterPk) throws ServiceException{
		billParameterTableById(parameterPk, false);
	}
	
	/**
	 * Bill parameter table by id.
	 *
	 * @param parameterPk the parameter pk
	 * @param isObj the is obj
	 * @throws ServiceException the service exception
	 */
	public void billParameterTableById(Integer parameterPk, boolean isObj) throws ServiceException{
		ParameterTable pTable = generalParameterFacade.getParamDetailServiceFacade(parameterPk);
		if(pTable!=null){
			if(isObj){
				super.getParametersTableMap().put(pTable.getParameterTablePk()  , pTable);
			}else{
				super.getParametersTableMap().put(pTable.getParameterTablePk()  , pTable.getParameterName());
			}
		}else{
			if(isObj){
				super.getParametersTableMap().put(parameterPk  , new ParameterTable());
			}else{
				super.getParametersTableMap().put(parameterPk  , "");
			}
		}
	}
	
	/**
	 * Bill parameters table map.
	 *
	 * @param lstParameterTable the lst parameter table
	 */
	public void billParametersTableMap(List<ParameterTable> lstParameterTable){
		billParametersTableMap(lstParameterTable,false);
	}
	
	/**
	 * Bill parameters table map.
	 *
	 * @param lstParameterTable the lst parameter table
	 * @param isObj the is obj
	 */
	public void billParametersTableMap(List<ParameterTable> lstParameterTable, boolean isObj){
		for(ParameterTable pTable : lstParameterTable){
			if(isObj){
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable);
			}else{
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable.getParameterName());
			}
		}
	}
	
	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets the valuator term range.
	 *
	 * @return the valuator term range
	 */
	public ValuatorTermRange getValuatorTermRange() {
		return valuatorTermRange;
	}

	/**
	 * Sets the valuator term range.
	 *
	 * @param valuatorTermRange the new valuator term range
	 */
	public void setValuatorTermRange(ValuatorTermRange valuatorTermRange) {
		this.valuatorTermRange = valuatorTermRange;
	}

	/**
	 * Gets the lst cbo security class valuator.
	 *
	 * @return the lst cbo security class valuator
	 */
	public List<SecurityClassValuator> getLstCboSecurityClassValuator() {
		return lstCboSecurityClassValuator;
	}

	/**
	 * Sets the lst cbo security class valuator.
	 *
	 * @param lstCboSecurityClassValuator the new lst cbo security class valuator
	 */
	public void setLstCboSecurityClassValuator(
			List<SecurityClassValuator> lstCboSecurityClassValuator) {
		this.lstCboSecurityClassValuator = lstCboSecurityClassValuator;
	}
	
	/**
	 * Gets the lst dtb valuator security code.
	 *
	 * @return the lst dtb valuator security code
	 */
	public List<ValuatorSecurityCode> getLstDtbValuatorSecurityCode() {
		return lstDtbValuatorSecurityCode;
	}

	/**
	 * Sets the lst dtb valuator security code.
	 *
	 * @param lstDtbValuatorSecurityCode the new lst dtb valuator security code
	 */
	public void setLstDtbValuatorSecurityCode(
			List<ValuatorSecurityCode> lstDtbValuatorSecurityCode) {
		this.lstDtbValuatorSecurityCode = lstDtbValuatorSecurityCode;
	}

	/**
	 * Gets the subordinate code.
	 *
	 * @return the subordinate code
	 */
	public String getSubordinateCode(){
		if(security.isFixedInstrumentTypeAcc() && security.isSubordinated()){
			return valuatorSetup.getSubOrdinatedCode();
		}
		return BigDecimal.ZERO.toString();
	}
	
	/**
	 * Gets the prepaid code.
	 *
	 * @return the prepaid code
	 */
	public String getPrepaidCode(){
		if(security.isFixedInstrumentTypeAcc() && security.isEarlyRedemption()){
			if(security.getIndPrepaid()!=null && security.getIndPrepaid().equals(BooleanType.YES.getCode())){
				return "Q";
			}else{
				return valuatorSetup.getPrepaidCode();
			}
		}
		return BigDecimal.ZERO.toString();
	}
	
	/**
	 * Gets the parameter table in view.
	 *
	 * @param parameterPk the parameter pk
	 * @return the parameter table in view
	 */
	public ParameterTable getParameterTableInView(Integer parameterPk){
		if(parameterPk!=null && super.getParametersTableMap().get(parameterPk)!=null){
			if(super.getParametersTableMap().get(parameterPk) instanceof ParameterTable){
				parameterTableInView=(ParameterTable)super.getParametersTableMap().get(parameterPk);
			}else{
				parameterTableInView.setParameterTablePk( parameterPk );
				parameterTableInView.setParameterName( super.getParametersTableMap().get(parameterPk).toString() );
			}		
		}else{
			parameterTableInView=new ParameterTable();
		}
		return parameterTableInView;
	}
	
}
