package com.pradera.securities.issuancesecuritie.util;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Enum PaymentScheduleFileRegisterType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public enum PaymentScheduleFileRegisterType {
	 
	/** The security register. */
	SECURITY_REGISTER(new Integer(1),"java.lang.String","java.util.Date","java.util.Date",null),
	
	/** The program interest. */
	PROGRAM_INTEREST(new Integer(2),"java.lang.Integer","java.util.Date","java.math.BigDecimal","java.math.BigDecimal"),
	
	/** The program amortization. */
	PROGRAM_AMORTIZATION(new Integer(3),"java.lang.Integer","java.util.Date","java.math.BigDecimal",null);
	
	/** The register type. */
	Integer registerType;
	
	/** The second col data type. */
	String secondColDataType;
	
	/** The three col data type. */
	String threeColDataType;
	
	/** The four col data type. */
	String fourColDataType;
	
	/** The five col data type. */
	String fiveColDataType;
	
	
	/** The Constant list. */
	public static final List<PaymentScheduleFileRegisterType> list = new ArrayList<PaymentScheduleFileRegisterType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, PaymentScheduleFileRegisterType> lookup = new HashMap<Integer, PaymentScheduleFileRegisterType>();
	
	static {
		for (PaymentScheduleFileRegisterType p : EnumSet.allOf(PaymentScheduleFileRegisterType.class)) {
			list.add(p);
			lookup.put(p.getRegisterType(), p);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param registerType the register type
	 * @return the payment schedule file register type
	 */
	public static PaymentScheduleFileRegisterType get(Integer registerType) {
		return lookup.get(registerType);
	}
	
	/**
	 * Instantiates a new payment schedule file register type.
	 *
	 * @param registerType the register type
	 * @param secondColDataType the second col data type
	 * @param threeColDataType the three col data type
	 * @param fourColDataType the four col data type
	 * @param fiveColDataType the five col data type
	 */
	private PaymentScheduleFileRegisterType(Integer registerType,
			String secondColDataType, String threeColDataType,
			String fourColDataType, String fiveColDataType) {
		this.registerType = registerType;
		this.secondColDataType = secondColDataType;
		this.threeColDataType = threeColDataType;
		this.fourColDataType = fourColDataType;
		this.fiveColDataType = fiveColDataType;
	}
	
	/**
	 * Gets the register type.
	 *
	 * @return the register type
	 */
	public Integer getRegisterType() {
		return registerType;
	}
	
	/**
	 * Sets the register type.
	 *
	 * @param registerType the new register type
	 */
	public void setRegisterType(Integer registerType) {
		this.registerType = registerType;
	}
	
	/**
	 * Gets the second col data type.
	 *
	 * @return the second col data type
	 */
	public String getSecondColDataType() {
		return secondColDataType;
	}
	
	/**
	 * Sets the second col data type.
	 *
	 * @param secondColDataType the new second col data type
	 */
	public void setSecondColDataType(String secondColDataType) {
		this.secondColDataType = secondColDataType;
	}
	
	/**
	 * Gets the three col data type.
	 *
	 * @return the three col data type
	 */
	public String getThreeColDataType() {
		return threeColDataType;
	}
	
	/**
	 * Sets the three col data type.
	 *
	 * @param threeColDataType the new three col data type
	 */
	public void setThreeColDataType(String threeColDataType) {
		this.threeColDataType = threeColDataType;
	}
	
	/**
	 * Gets the four col data type.
	 *
	 * @return the four col data type
	 */
	public String getFourColDataType() {
		return fourColDataType;
	}
	
	/**
	 * Sets the four col data type.
	 *
	 * @param fourColDataType the new four col data type
	 */
	public void setFourColDataType(String fourColDataType) {
		this.fourColDataType = fourColDataType;
	}
	
	/**
	 * Gets the five col data type.
	 *
	 * @return the five col data type
	 */
	public String getFiveColDataType() {
		return fiveColDataType;
	}
	
	/**
	 * Sets the five col data type.
	 *
	 * @param fiveColDataType the new five col data type
	 */
	public void setFiveColDataType(String fiveColDataType) {
		this.fiveColDataType = fiveColDataType;
	}
	
	
}
