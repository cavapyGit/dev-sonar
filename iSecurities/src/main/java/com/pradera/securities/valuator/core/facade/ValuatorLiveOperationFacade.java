package com.pradera.securities.valuator.core.facade;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.interceptores.Performance;
import com.pradera.securities.valuator.core.service.ValuatorLiveOperationService;
import com.pradera.securities.valuator.core.service.ValuatorSecuritiesService;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class ValuatorSecuritiesFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/12/2014
 */
@Singleton
@Performance
//@Interceptors({ContextHolderInterceptor.class,MonitoringEventInterceptor.class})
public class ValuatorLiveOperationFacade {

	/** The valuator live operation. */
	@EJB private ValuatorLiveOperationService liveOperService;
	
	/** The securities service. */
	@EJB private ValuatorSecuritiesService securitiesService;
	
	/** The live operation facade. */
	@EJB private ValuatorLiveOperationFacade liveOperationFacade;
	
	/** The transaction registry. */
	@Resource TransactionSynchronizationRegistry transactionRegistry;
	
	/**
	 * Update live operation.
	 *
	 * @param securityList the security list
	 * @param processDate the process date
	 * @param processPk the process pk
	 * @throws ServiceException the service exception
	 */
//	@TransactionTimeout(unit=TimeUnit.HOURS,value=3)
//	public void updateLiveOperation(List<String> securityList, Date processDate, Long processPk) throws ServiceException{
//		
//		/**List to handle all securities homologated*/
//		Map<String,Object[]> securityHomologMap = liveOperService.getSecurityHomologated(processDate,processPk);
//		
//		/**List to handle valuator price result related with live operation*/
//		Map<String,List<ValuatorPriceResult>> pricesForLiveOperation = liveOperService.getPriceResultForLiveOperation(processDate, processPk); 
//		
//		/**Invokate logic to update price on CustodyOperation*/
////		updateCustodyMarketPrice(processDate,processPk,securityHomologMap);
//		liveOperationFacade.updateCustodyMarketPriceV_2(processDate,processPk,securityHomologMap,pricesForLiveOperation);
//		
//		/**Invokate logic to update price on ReportingOperation*/
////		liveOperService.updateReportingMarketPrice(processDate,processPk,securityHomologMap);
//		liveOperationFacade.updateReportingPrice_V_2(processDate, processPk, securityHomologMap, pricesForLiveOperation);
//		
//		/** Actualizamos los HM en operaciones SELID*/ //issue 1040
//		/**List to handle valuator price result related with live operation*/
//		Map<String,List<ValuatorPriceResult>> pricesForLiveSelidOperation = liveOperService.getPriceResultForLiveSelidOperation(processDate, processPk); 
//		liveOperationFacade.updateSelidOperationPrice(processDate, processPk, securityHomologMap, pricesForLiveSelidOperation);
//		
//		/**Invocate logic to update price on PhysicalSecurities*/
////		liveOperService.updatePhysicalDepositMarketPrice(processDate,processPk,securityHomologMap);
//		liveOperationFacade.updatePhysicalPrice_V_2(processDate, processPk, securityHomologMap, pricesForLiveOperation);
//	}
	
	/**
	 * Update custody market price.
	 *
	 * @param processDate the process date
	 * @param processPk the process pk
	 * @param homologatMap the security homolog map
	 * @throws ServiceException the service exception
	 */
//	@ValuatorDispatcherEvent(eventType=ValuatorEventType.EVENT_4_1_UPDATE_BLOCK_OPERATION)
//	public void updateCustodyMarketPrice(Date processDate, Long processPk, Map<String,Object[]> homologatMap) throws ServiceException{
//		Map<Long,List<MarketFactForLiveOperation>> liveOperationsMap = liveOperService.getCustodyBlockMarketFact(processDate, processPk);
//		
//		/**Iterate accreditationOperation and union in liveOperationMap*/
//		Map<Long,List<MarketFactForLiveOperation>> accredOperationsMap = liveOperService.getCustodyAccreditationMarketFact(processDate,processPk);
//		for(Long key:	accredOperationsMap.keySet()){
//			if(liveOperationsMap.get(key)==null){
//				liveOperationsMap.put(key, accredOperationsMap.get(key));
//			}
//		}
//		if(liveOperationsMap!=null && !liveOperationsMap.isEmpty()){
//			firsJump:
//			for(Long key: liveOperationsMap.keySet()){
//				List<MarketFactForLiveOperation> liveOperationList = liveOperationsMap.get(key);
//				for(MarketFactForLiveOperation liveOperation: liveOperationList){
//					String securityCode = liveOperation.getSecurityCodePk();
//					if(homologatMap.get(securityCode)!=null){
//						BigDecimal quantity = BigDecimal.ZERO;
//						BigDecimal initialQuantity = BigDecimal.ZERO;
//						for(MarketFactForLiveOperation _liveOperation: liveOperationList){
//							if(_liveOperation.getActive().equals(BooleanType.YES.getCode())){
//								quantity = quantity.add(_liveOperation.getMarketQuantity());
//								if(_liveOperation.getInitialQuantity()!=null){
//									initialQuantity = initialQuantity.add(_liveOperation.getInitialQuantity());
//								}
//							}
//						}
//						
//						/**Homologate*/
//						Object custodyMarkFact = null;
//						Long custodyOperation = liveOperation.getCustodyOperationPk();
//						BigDecimal markRate  = (BigDecimal) homologatMap.get(securityCode)[0];
//						BigDecimal markPrice = (BigDecimal) homologatMap.get(securityCode)[1];
//						Date	   markDate  = (Date) 		homologatMap.get(securityCode)[2];
//						
//						if(liveOperation.getIndBlockOperation().equals(BooleanType.YES.getCode())){
//							liveOperService.updateCustodyBlockMarketFactByCustody(Arrays.asList(custodyOperation));
//							custodyMarkFact = liveOperService.popuBloclkMarketFact(markDate, markRate, markPrice, custodyOperation, quantity, initialQuantity);
//						}else{
//							liveOperService.updateCustodyAccredMarketFactByCustody(Arrays.asList(custodyOperation));
//							custodyMarkFact = liveOperService.pupulateAccreditMarketFact(markDate, markRate, markPrice, custodyOperation, quantity);
//						}
//						liveOperService.create(custodyMarkFact);
//						
//						/**Adding new marketFactObject in Map*/
//						continue firsJump;
//					}
//					
//					Integer indBlockOp		= liveOperation.getIndBlockOperation();
//					Long custMrkFact		= liveOperation.getCustodyOperationMrkFactPk();
//					Long custOpPk			= liveOperation.getCustodyOperationPk();
//					Date marketDate 	  	= liveOperation.getMarketDate();
//					BigDecimal marketRate 	= liveOperation.getMarketRate();
//					BigDecimal quantity 	= liveOperation.getMarketQuantity();
//					BigDecimal actQuantity	= liveOperation.getInitialQuantity();
//					ValuatorPriceResult priceResult = liveOperService.findValuatPrice(securityCode, marketDate, processDate, marketRate, null);
//					if(priceResult==null){
//						/**whenever exist error throw serviceException*/
//						throw new ServiceException(ErrorServiceType.VALUATOR_LIVEOPERATION_CUSTODY, new Object[]{securityCode,marketRate,marketDate});
//					}
//					BigDecimal mrkPrice = priceResult.getMarketPrice();
//					liveOperService.updateCustoyMarketOper(custMrkFact, custOpPk, marketRate, marketDate, mrkPrice, indBlockOp, quantity, actQuantity);
//				}
//			}
//		}
//		
//		/**Code to finish process*/
//		Integer detailPk = ValuatorDetailType.OPERATIONS.getCode();
//		Long eventPk 	 = ValuatorEventType.EVENT_4_1_UPDATE_BLOCK_OPERATION.getCode(); 
//		securitiesService.finishValuatorExecutionDetail(processPk, detailPk, eventPk,false);
//	}
//	
//	/**
//	 * Update custody market price v_2.
//	 *
//	 * @param processDate the process date
//	 * @param processPk the process pk
//	 * @param homologatMap the homologat map
//	 * @param prices the prices
//	 * @throws ServiceException the service exception
//	 */
//	@ValuatorDispatcherEvent(eventType=ValuatorEventType.EVENT_4_1_UPDATE_BLOCK_OPERATION)
//	public void updateCustodyMarketPriceV_2(Date processDate, Long processPk, Map<String,Object[]> homologatMap, Map<String,List<ValuatorPriceResult>> prices)
//																																			throws ServiceException{
//		/**Getting loggerUser*/
//		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
//		
//		/**Reading custody operation to update his prices*/
//		Map<Long,List<MarketFactForLiveOperation>> liveOperationsMap = liveOperService.getCustodyBlockMarketFact(processDate, processPk);
//		
//		/**Iterate accreditationOperation and union in liveOperationMap*/
//		Map<Long,List<MarketFactForLiveOperation>> accredOperationsMap = liveOperService.getCustodyAccreditationMarketFact(processDate,processPk);
//		for(Long key:	accredOperationsMap.keySet()){
//			if(liveOperationsMap.get(key)==null){
//				liveOperationsMap.put(key, accredOperationsMap.get(key));
//			}
//		}
//		
//		/**Map to handle rows in valuatorProcess - live operation, either update or insert*/
//		Map<Integer,List<Object>> blockingInfoMap = getMapForLiveOperation();
//		
//		if(liveOperationsMap!=null && !liveOperationsMap.isEmpty()){
//			firsJump:
//			for(Long key: liveOperationsMap.keySet()){
//				List<MarketFactForLiveOperation> liveOperationList = liveOperationsMap.get(key);
//				for(MarketFactForLiveOperation liveOperation: liveOperationList){
//					String securityCode = liveOperation.getSecurityCodePk();
//					if(homologatMap.get(securityCode)!=null){
//						BigDecimal quantity = BigDecimal.ZERO;
//						BigDecimal initialQuantity = BigDecimal.ZERO;
//						for(MarketFactForLiveOperation _liveOperation: liveOperationList){
//							if(_liveOperation.getActive().equals(BooleanType.YES.getCode())){
//								quantity = quantity.add(_liveOperation.getMarketQuantity());
//								if(_liveOperation.getInitialQuantity()!=null){
//									initialQuantity = initialQuantity.add(_liveOperation.getInitialQuantity());
//								}
//							}
//						}
//						
//						/**Homologate*/
//						Object custodyMarkFact = null;
//						Long custodyOperation = liveOperation.getCustodyOperationPk();
//						BigDecimal markRate  = (BigDecimal) homologatMap.get(securityCode)[0];
//						BigDecimal markPrice = (BigDecimal) homologatMap.get(securityCode)[1];
//						Date	   markDate  = (Date) 		homologatMap.get(securityCode)[2];
//						
//						if(liveOperation.getIndBlockOperation().equals(BooleanType.YES.getCode())){
//							custodyMarkFact = liveOperService.popuBloclkMarketFact(markDate, markRate, markPrice, custodyOperation, quantity, initialQuantity);
//						}else{
//							custodyMarkFact = liveOperService.pupulateAccreditMarketFact(markDate, markRate, markPrice, custodyOperation, quantity);
//						}
//						/**Adding new marketFactObject in Map*/
//						blockingInfoMap.get(ValuatorExecutionType.FOR_INS.getCode()).add(custodyMarkFact);
//						continue firsJump;
//					}
//					
//					Integer indBlockOp		= liveOperation.getIndBlockOperation();
//					Long custOpPk			= liveOperation.getCustodyOperationPk();
//					Date marketDate 	  	= liveOperation.getMarketDate();
//					BigDecimal marketRate 	= liveOperation.getMarketRate();
//					BigDecimal quantity 	= liveOperation.getMarketQuantity();
//					BigDecimal actQuantity	= liveOperation.getInitialQuantity();
//					ValuatorPriceResult priceResult = liveOperService.findValuatPrice(securityCode, marketDate, marketRate, null,prices.get(securityCode));
//					if(priceResult==null){
//						/**whenever exist error throw serviceException*/
//						throw new ServiceException(ErrorServiceType.VALUATOR_LIVEOPERATION_CUSTODY, new Object[]{securityCode,marketRate,marketDate});
//					}
//					BigDecimal marketPrice = priceResult.getMarketPrice();
//					Object custodyMrkFact = null;
//					if(indBlockOp.equals(BooleanType.YES.getCode())){
//						custodyMrkFact = liveOperService.popuBloclkMarketFact(marketDate, marketRate, marketPrice, custOpPk, quantity, actQuantity);
//					}else{
//						custodyMrkFact = liveOperService.pupulateAccreditMarketFact(marketDate, marketRate, marketPrice, custOpPk, quantity);
//					}
//					blockingInfoMap.get(ValuatorExecutionType.FOR_INS.getCode()).add(custodyMrkFact);
//				}
//			}
//		}
//		
//		/**Saving data on BD*/
//		liveOperService.updateCustodyOperationPrice(processPk, loggerUser);
//		liveOperService.saveAll(blockingInfoMap.get(ValuatorExecutionType.FOR_INS.getCode()));
//		liveOperService.flushTransaction();
//		
//		/**Code to finish process*/
//		Integer detailPk = ValuatorDetailType.OPERATIONS.getCode();
//		Long eventPk 	 = ValuatorEventType.EVENT_4_1_UPDATE_BLOCK_OPERATION.getCode(); 
//		securitiesService.finishValuatorExecutionDetail(processPk, detailPk, eventPk,false);
//	}
//	
//	
//	/**
//	 * Update reporting market price_ v_2.
//	 *
//	 * @param processDate the proc date
//	 * @param processPk the process pk
//	 * @param securityHomologMap the security homolog map
//	 * @param prices the prices
//	 * @throws ServiceException the service exception
//	 */
//	@ValuatorDispatcherEvent(eventType=ValuatorEventType.EVENT_4_2_UPDATE_REPORTING_OPERATION)
//	public void updateReportingPrice_V_2(Date processDate, Long processPk, Map<String,Object[]> securityHomologMap,Map<String,List<ValuatorPriceResult>> prices) 
//																																			throws ServiceException{
//		/**Getting loggerUser*/
//		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
//		
//		/**Read reporting operation to do update*/
//		Map<Long,List<MarketFactForLiveOperation>> liveOperationsMap = liveOperService.getSettlementAccountMarketFact(processDate,processPk);
//		
//		/**Map to handle rows in valuatorProcess - live operation, either update or insert*/
//		Map<Integer,List<Object>> reportingInfoMap = getMapForLiveOperation();
//		
//		if(liveOperationsMap!=null && !liveOperationsMap.isEmpty()){
//			for(Long key: liveOperationsMap.keySet()){
//				List<MarketFactForLiveOperation> liveOperationList = liveOperationsMap.get(key);
//				String securityCodePk 	= liveOperationList.get(0).getSecurityCodePk();
//				/**If securityCode it's not homologated*/
//				if(securityHomologMap.get(securityCodePk)==null){
//					for(MarketFactForLiveOperation liveOperation: liveOperationList){
//						if(liveOperation.getActive().equals(BooleanType.YES.getCode())){
//							String securityCode 	= liveOperation.getSecurityCodePk();
//							Date markDate 	  		= liveOperation.getMarketDate();
//							BigDecimal marketRate 	= liveOperation.getMarketRate();
//							Integer role 			= liveOperation.getRole();
//							BigDecimal quantity 	= liveOperation.getMarketQuantity();
//							Long settAccPk			= liveOperation.getSettlementAccOperationPk();
//							Long mechanism			= liveOperation.getMechanismType();
//							Long operationPk = null;
//							
//							/**If it's seller the price calculated have same marketDate than processDate and marketRate Zero*/
//							if(role.equals(ComponentConstant.SALE_ROLE) && !mechanism.equals(NegotiationMechanismType.SIRTEX.getCode())){
//								operationPk = liveOperation.getMechanismOperationPk();
//								marketRate  = BigDecimal.ZERO;
//								markDate	= processDate;
//							}
//							//issue 617
//							if(role.equals(ComponentConstant.SALE_ROLE) && mechanism.equals(NegotiationMechanismType.SIRTEX.getCode())){
//								operationPk = liveOperation.getMechanismOperationPk();
//								markDate	= processDate;
//							}
//							/**Find the pricre calculated by the process*/
//							ValuatorPriceResult priceResult = liveOperService.findValuatPrice(securityCode, markDate, marketRate, operationPk,prices.get(securityCode));
//							/**whenever exist error throw serviceException*/
//							if(priceResult==null){
//								throw new ServiceException(ErrorServiceType.VALUATOR_LIVEOPERATION_OPERATION, new Object[]{securityCode,marketRate,markDate});
//							}
//							/**Get the price && rate*/
//							BigDecimal mrkPrice = priceResult.getMarketPrice();
//							BigDecimal mrkRate  = liveOperation.getMarketRate();
//							/**Update reporting operation according with the price generated*/
//									   markDate	= liveOperation.getMarketDate();
//							SettlementAccountMarketfact accountMrk = liveOperService.popuSettlMarketFact(markDate, mrkRate, mrkPrice, quantity, settAccPk);
//							/**Setting object into map to persist later*/
//							reportingInfoMap.get(ValuatorExecutionType.FOR_INS.getCode()).add(accountMrk);
//						}
//					}
//				}else{
//					for(MarketFactForLiveOperation liveOperation: liveOperationList){
//						Boolean isHomologated = Boolean.FALSE;
//						Integer role = liveOperation.getRole();
//						if(role.equals(ComponentConstant.SALE_ROLE)){
//							Date marketDate 	  	= liveOperation.getMarketDate();
//							BigDecimal marketRate 	= liveOperation.getMarketRate();
//							String securityCode 	= liveOperation.getSecurityCodePk();
//							BigDecimal quantity 	= liveOperation.getMarketQuantity();
//							Long operationPk 		= liveOperation.getMechanismOperationPk();
//							Long settAccPk			= liveOperation.getSettlementAccOperationPk();
//								
//							ValuatorPriceResult priceResult = liveOperService.findValuatPrice(securityCode, processDate, BigDecimal.ZERO, operationPk,prices.get(securityCode));
//							if(priceResult==null){
//								/**In this case is sirtex*/
//								priceResult = liveOperService.findValuatorPriceHomologated(securityCode,processPk);
//								marketRate = priceResult.getMarketRate();
//								marketDate = priceResult.getMarketDate();
//							}
//							BigDecimal mrkPrice = priceResult.getMarketPrice();
//							/**Populate object and getting object*/
//							SettlementAccountMarketfact settlMrkFact = liveOperService.popuSettlMarketFact(marketDate, marketRate, mrkPrice, quantity, settAccPk);
//							/**Setting object into map to persist later*/
//							reportingInfoMap.get(ValuatorExecutionType.FOR_INS.getCode()).add(settlMrkFact);
//						}else if(role.equals(ComponentConstant.PURCHARSE_ROLE) && !isHomologated){
//							Long settAccPk			= liveOperation.getSettlementAccOperationPk();
//							Map<Long,BigDecimal> settAccOperationMap = liveOperService.getMarketFactToHomologate(liveOperationList, role,settAccPk);
//							for(Long settKey: settAccOperationMap.keySet()){
//								String securityCode  = liveOperation.getSecurityCodePk();
//								BigDecimal markRate  = (BigDecimal) securityHomologMap.get(securityCode)[0];
//								BigDecimal markPrice = (BigDecimal) securityHomologMap.get(securityCode)[1];
//								Date markDate 		 = (Date) securityHomologMap.get(securityCode)[2];
//								BigDecimal quantity  = settAccOperationMap.get(settKey);
//
//								/**Setting object according with information*/
//								SettlementAccountMarketfact settlMrkFact = liveOperService.popuSettlMarketFact(markDate, markRate, markPrice, quantity, settKey);
//								/**Setting object into at map to be persisted*/
//								reportingInfoMap.get(ValuatorExecutionType.FOR_INS.getCode()).add(settlMrkFact);
//							}
//							isHomologated = Boolean.TRUE;
//						}
//					}
//				}
//			}
//		}
//
//		/**Saving data on BD*/
//		liveOperService.updateReportingOperationPrice(processPk, loggerUser);
//		liveOperService.saveAll(reportingInfoMap.get(ValuatorExecutionType.FOR_INS.getCode()));
//		liveOperService.flushTransaction();
//		
//		/**Code to finish process*/
//		Integer detailPk = ValuatorDetailType.OPERATIONS.getCode();
//		Long eventPk 	 = ValuatorEventType.EVENT_4_2_UPDATE_REPORTING_OPERATION.getCode(); 
//		securitiesService.finishValuatorExecutionDetail(processPk, detailPk, eventPk,false);
//	}
//	
//	/**
//	 * issue 1040
//	 *
//	 * @param processDate the proc date
//	 * @param processPk the process pk
//	 * @param securityHomologMap the security homolog map
//	 * @param prices the prices
//	 * @throws ServiceException the service exception
//	 */
//	//@ValuatorDispatcherEvent(eventType=ValuatorEventType.EVENT_4_2_UPDATE_REPORTING_OPERATION)
//	public void updateSelidOperationPrice(Date processDate, Long processPk, Map<String,Object[]> securityHomologMap,Map<String,List<ValuatorPriceResult>> prices) 
//																																			throws ServiceException{
//		/**Getting loggerUser*/
//		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
//		
//		/**Read reporting operation to do update*/
//		Map<Long,List<MarketFactForLiveOperation>> liveOperationsMap = liveOperService.getSelidSettlementAccountMarketFact(processDate,processPk);
//		
//		/**Map to handle rows in valuatorProcess - live operation, either update or insert*/
//		Map<Integer,List<Object>> reportingInfoMap = getMapForLiveOperation();
//		
//		if(liveOperationsMap!=null && !liveOperationsMap.isEmpty()){
//			for(Long key: liveOperationsMap.keySet()){
//				List<MarketFactForLiveOperation> liveOperationList = liveOperationsMap.get(key);
//				String securityCodePk 	= liveOperationList.get(0).getSecurityCodePk();
//				/**If securityCode it's not homologated*/
//				if(securityHomologMap.get(securityCodePk)==null){
//					for(MarketFactForLiveOperation liveOperation: liveOperationList){
//						if(liveOperation.getActive().equals(BooleanType.YES.getCode())){
//							String securityCode 	= liveOperation.getSecurityCodePk();
//							Date markDate 	  		= liveOperation.getMarketDate();
//							BigDecimal marketRate 	= liveOperation.getMarketRate();
//							Integer role 			= liveOperation.getRole();
//							BigDecimal quantity 	= liveOperation.getMarketQuantity();
//							Long settAccPk			= liveOperation.getSettlementAccOperationPk();
//							Long mechanism			= liveOperation.getMechanismType();
//							Long operationPk = null;
//							
//							/**If it's seller the price calculated have same marketDate than processDate and marketRate Zero*/
//							if(role.equals(ComponentConstant.SALE_ROLE) && !mechanism.equals(NegotiationMechanismType.SIRTEX.getCode())){
//								operationPk = liveOperation.getMechanismOperationPk();
//								marketRate  = BigDecimal.ZERO;
//								markDate	= processDate;
//							}
//							/**Find the pricre calculated by the process*/
//							ValuatorPriceResult priceResult = liveOperService.findSelidValuatPrice(securityCode, markDate, marketRate, operationPk,prices.get(securityCode));
//							/**whenever exist error throw serviceException*/
//							if(priceResult==null){
//								throw new ServiceException(ErrorServiceType.VALUATOR_LIVEOPERATION_OPERATION, new Object[]{securityCode,marketRate,markDate});
//							}
//							/**Get the price && rate*/
//							BigDecimal mrkPrice = priceResult.getMarketPrice();
//							BigDecimal mrkRate  = liveOperation.getMarketRate();
//							/**Update reporting operation according with the price generated*/
//									   markDate	= liveOperation.getMarketDate();
//							SettlementAccountMarketfact accountMrk = liveOperService.popuSettlMarketFact(markDate, mrkRate, mrkPrice, quantity, settAccPk);
//							/**Setting object into map to persist later*/
//							reportingInfoMap.get(ValuatorExecutionType.FOR_INS.getCode()).add(accountMrk);
//						}
//					}
//				}else{
//					for(MarketFactForLiveOperation liveOperation: liveOperationList){
//						Boolean isHomologated = Boolean.FALSE;
//						Integer role = liveOperation.getRole();
//						if(role.equals(ComponentConstant.SALE_ROLE)){
//							Date marketDate 	  	= liveOperation.getMarketDate();
//							BigDecimal marketRate 	= liveOperation.getMarketRate();
//							String securityCode 	= liveOperation.getSecurityCodePk();
//							BigDecimal quantity 	= liveOperation.getMarketQuantity();
//							Long operationPk 		= liveOperation.getMechanismOperationPk();
//							Long settAccPk			= liveOperation.getSettlementAccOperationPk();
//								
//							ValuatorPriceResult priceResult = liveOperService.findSelidValuatPrice(securityCode, processDate, BigDecimal.ZERO, operationPk,prices.get(securityCode));
//							if(priceResult==null){
//								/**In this case is sirtex*/
//								priceResult = liveOperService.findValuatorPriceHomologated(securityCode,processPk);
//								marketRate = priceResult.getMarketRate();
//								marketDate = priceResult.getMarketDate();
//							}
//							BigDecimal mrkPrice = priceResult.getMarketPrice();
//							/**Populate object and getting object*/
//							SettlementAccountMarketfact settlMrkFact = liveOperService.popuSettlMarketFact(marketDate, marketRate, mrkPrice, quantity, settAccPk);
//							/**Setting object into map to persist later*/
//							reportingInfoMap.get(ValuatorExecutionType.FOR_INS.getCode()).add(settlMrkFact);
//						}else if(role.equals(ComponentConstant.PURCHARSE_ROLE) && !isHomologated){
//							Long settAccPk			= liveOperation.getSettlementAccOperationPk();
//							Map<Long,BigDecimal> settAccOperationMap = liveOperService.getMarketFactToHomologate(liveOperationList, role,settAccPk);
//							for(Long settKey: settAccOperationMap.keySet()){
//								String securityCode  = liveOperation.getSecurityCodePk();
//								BigDecimal markRate  = (BigDecimal) securityHomologMap.get(securityCode)[0];
//								BigDecimal markPrice = (BigDecimal) securityHomologMap.get(securityCode)[1];
//								Date markDate 		 = (Date) securityHomologMap.get(securityCode)[2];
//								BigDecimal quantity  = settAccOperationMap.get(settKey);
//
//								/**Setting object according with information*/
//								SettlementAccountMarketfact settlMrkFact = liveOperService.popuSettlMarketFact(markDate, markRate, markPrice, quantity, settKey);
//								/**Setting object into at map to be persisted*/
//								reportingInfoMap.get(ValuatorExecutionType.FOR_INS.getCode()).add(settlMrkFact);
//							}
//							isHomologated = Boolean.TRUE;
//						}
//					}
//				}
//			}
//		}
//
//		/**Saving data on BD*/
//		liveOperService.updateSelidOperationPrice(processPk, loggerUser);
//		liveOperService.saveAll(reportingInfoMap.get(ValuatorExecutionType.FOR_INS.getCode()));
//		liveOperService.flushTransaction();
//		
//		/**Code to finish process*/
//		Integer detailPk = ValuatorDetailType.OPERATIONS.getCode();
//		Long eventPk 	 = ValuatorEventType.EVENT_4_2_UPDATE_REPORTING_OPERATION.getCode(); 
//		securitiesService.finishValuatorExecutionDetail(processPk, detailPk, eventPk,false);
//	}
//	
//	/**
//	 * Update physical price_ v_2.
//	 *
//	 * @param processDate the process date
//	 * @param processPk the process pk
//	 * @param securityHomologMap the security homolog map
//	 * @param prices the prices
//	 * @throws ServiceException the service exception
//	 */
//	@ValuatorDispatcherEvent(eventType=ValuatorEventType.EVENT_4_3_UPDATE_PHYSICAL_CERTIFICATE)
//	public void updatePhysicalPrice_V_2(Date processDate, Long processPk, Map<String,Object[]> securityHomologMap,Map<String,List<ValuatorPriceResult>> prices) 
//																																				throws ServiceException{
//		/**Getting loggerUser*/
//		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
//		
//		/**Read information from physical*/
//		Map<Long,List<MarketFactForLiveOperation>> liveOperationMap = liveOperService.getPhysicalCertificateMarkFact(processDate, processPk);
//		
//		/**Map to handle rows in valuatorProcess - live operation, either update or insert*/
//		Map<Integer,List<Object>> physicalInfoMap = getMapForLiveOperation();
//
//		if(liveOperationMap!=null && !liveOperationMap.isEmpty()){
//			for(Long key: liveOperationMap.keySet()){
//				List<MarketFactForLiveOperation> liveOperationList = liveOperationMap.get(key);
//				for(MarketFactForLiveOperation liveOperation: liveOperationList){
//					if(liveOperation.getActive().equals(BooleanType.YES.getCode())){
//						String securityCode = liveOperation.getSecurityCodePk();
//						BigDecimal marketPrice = null;
//						BigDecimal marketRate = null;
//						Date marketDate = null;
//						if(securityHomologMap.get(securityCode)!=null){
//							Object[] marketInfo = securityHomologMap.get(securityCode);
//							marketRate  = (BigDecimal) marketInfo[0];
//							marketPrice = (BigDecimal) marketInfo[1];
//							marketDate  = (Date) marketInfo[2];
//						}else{
//							marketRate = liveOperation.getMarketRate();
//							marketDate = liveOperation.getMarketDate();
//							/**Getting price from data in memory*/
//							ValuatorPriceResult price = liveOperService.findValuatPrice(securityCode, marketDate, marketRate, null, prices.get(securityCode));
//							marketPrice = price.getMarketPrice();
//							marketDate  = liveOperation.getMarketDate();
//						}
//						
//						/**Getting data*/
//						Long custodyOperation = liveOperation.getCustodyOperationPk();
//						BigDecimal quantity   = liveOperation.getMarketQuantity();
//						
//						/**Getting information from data*/
//						PhysicalCertMarketfact certifMarkFact = liveOperService.popuPhysicalMarketFact(marketDate, marketRate,marketPrice, custodyOperation, quantity);
//						/**Setting object into at map to be persisted*/
//						physicalInfoMap.get(ValuatorExecutionType.FOR_INS.getCode()).add(certifMarkFact);
//					}
//				}
//			}
//		}
//		
//		/**Saving data on BD*/
//		liveOperService.updatePhysicalCertificatePrice(processPk, loggerUser);
//		liveOperService.saveAll(physicalInfoMap.get(ValuatorExecutionType.FOR_INS.getCode()));
//		
//		/**Code to finish process*/
//		Integer detailPk = ValuatorDetailType.OPERATIONS.getCode();
//		Long eventPk 	 = ValuatorEventType.EVENT_4_3_UPDATE_PHYSICAL_CERTIFICATE.getCode(); 
//		securitiesService.finishValuatorExecutionDetail(processPk, detailPk, eventPk,false);
//	}
//	
//	
//	/**
//	 * Gets the map for live operation.
//	 * Method to get list of live operation either or block,accreditation or reporting
//	 * @return the map for live operation
//	 */
//	public Map<Integer,List<Object>> getMapForLiveOperation(){
//		Map<Integer,List<Object>> mapForLiveOperation = new HashMap<>();
//		mapForLiveOperation.put(ValuatorExecutionType.FOR_INS.getCode(), new ArrayList<Object>());
//		mapForLiveOperation.put(ValuatorExecutionType.FOR_UPD.getCode(), new ArrayList<Object>());
//		return mapForLiveOperation;
//	}
}