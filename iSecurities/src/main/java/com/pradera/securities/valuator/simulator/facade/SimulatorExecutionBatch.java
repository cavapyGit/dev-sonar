package com.pradera.securities.valuator.simulator.facade;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;

import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SimulatorExecutionBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@BatchProcess(name = "SimulatorExecutionBatch")
@RequestScoped
public class SimulatorExecutionBatch implements JobExecution, Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The simulator batch facade. */
	@EJB private ValuatorSimulatorBatchFacade simulatorBatchFacade;
	
	/**
	 * Instantiates a new simulator execution batch.
	 */
	public SimulatorExecutionBatch() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		// TODO Auto-generated method stub
		
		Long idValuatorProcessSimulator = null;
		
		List<ProcessLoggerDetail> lstProcessLoggerDetails = processLogger.getProcessLoggerDetails();
				
		for (ProcessLoggerDetail objDetail: lstProcessLoggerDetails){
//			if (StringUtils.equalsIgnoreCase(ValuatorConstants.PARAM_VALUATOR_PROCESS_SIMULATOR, objDetail.getParameterName())) {
//				idValuatorProcessSimulator = new Long(objDetail.getParameterValue());
//			}
		}
		
		try {
			simulatorBatchFacade.registerValuatorSimulatioBalance(idValuatorProcessSimulator);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}

}
