package com.pradera.securities.valuator.core.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorInconsistenciesTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class ValuatorInconsistenciesTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -9039073338453940980L;
	
	/** The document number. */
	private String documentNumber;
	
	/** The affectation pk. */
	private Long affectationPk;
	
	/** The accreditation pk. */
	private Long accreditationPk;
	
	/** The mechanism operation pk. */
	private Long mechanismOperationPk;
	
	/** The operation number. */
	private String operationNumber;
	
	/** The operation ballot. */
	private String operationBallot;
	
	/** The operation sequential. */
	private String operationSequential;
	
	/** The role. */
	private Integer role;

	/** The participant pk. */
	private Long participantPk;
	
	/** The institution mnemonic. */
	private String institutionMnemonic;
	
	/** The security code. */
	private String securityCode;
	
	/** The holder account pk. */
	private Long holderAccountPk;
	
	/** The account number. */
	private String accountNumber;
	
	/** The cui code. */
	private Long cuiCode;
	
	/** The market date. */
	private Date marketDate;
	
	/** The market rate. */
	private BigDecimal marketRate;
	
	/** The market price. */
	private BigDecimal marketPrice;
	
	/** The market date inconsist. */
	private Date marketDateInconsist;
	
	/** The market rate inconsist. */
	private BigDecimal marketRateInconsist;
	
	/** The market price inconsist. */
	private BigDecimal marketPriceInconsist;
	
	/** The market quantity. */
	private BigDecimal marketQuantity;

	/**
	 * Gets the document number.
	 *
	 * @return the document number
	 */
	public String getDocumentNumber() {
		return documentNumber;
	}

	/**
	 * Sets the document number.
	 *
	 * @param documentNumber the new document number
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	/**
	 * Gets the affectation pk.
	 *
	 * @return the affectation pk
	 */
	public Long getAffectationPk() {
		return affectationPk;
	}

	/**
	 * Sets the affectation pk.
	 *
	 * @param affectationPk the new affectation pk
	 */
	public void setAffectationPk(Long affectationPk) {
		this.affectationPk = affectationPk;
	}

	/**
	 * Gets the accreditation pk.
	 *
	 * @return the accreditation pk
	 */
	public Long getAccreditationPk() {
		return accreditationPk;
	}

	/**
	 * Sets the accreditation pk.
	 *
	 * @param accreditationPk the new accreditation pk
	 */
	public void setAccreditationPk(Long accreditationPk) {
		this.accreditationPk = accreditationPk;
	}

	/**
	 * Gets the mechanism operation pk.
	 *
	 * @return the mechanism operation pk
	 */
	public Long getMechanismOperationPk() {
		return mechanismOperationPk;
	}

	/**
	 * Sets the mechanism operation pk.
	 *
	 * @param mechanismOperationPk the new mechanism operation pk
	 */
	public void setMechanismOperationPk(Long mechanismOperationPk) {
		this.mechanismOperationPk = mechanismOperationPk;
	}

	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public String getOperationNumber() {
		return operationNumber;
	}

	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(String operationNumber) {
		this.operationNumber = operationNumber;
	}

	/**
	 * Gets the operation ballot.
	 *
	 * @return the operation ballot
	 */
	public String getOperationBallot() {
		return operationBallot;
	}

	/**
	 * Sets the operation ballot.
	 *
	 * @param operationBallot the new operation ballot
	 */
	public void setOperationBallot(String operationBallot) {
		this.operationBallot = operationBallot;
	}

	/**
	 * Gets the operation sequential.
	 *
	 * @return the operation sequential
	 */
	public String getOperationSequential() {
		return operationSequential;
	}

	/**
	 * Sets the operation sequential.
	 *
	 * @param operationSequential the new operation sequential
	 */
	public void setOperationSequential(String operationSequential) {
		this.operationSequential = operationSequential;
	}

	/**
	 * Gets the participant pk.
	 *
	 * @return the participant pk
	 */
	public Long getParticipantPk() {
		return participantPk;
	}

	/**
	 * Sets the participant pk.
	 *
	 * @param participantPk the new participant pk
	 */
	public void setParticipantPk(Long participantPk) {
		this.participantPk = participantPk;
	}

	/**
	 * Gets the institution mnemonic.
	 *
	 * @return the institution mnemonic
	 */
	public String getInstitutionMnemonic() {
		return institutionMnemonic;
	}

	/**
	 * Sets the institution mnemonic.
	 *
	 * @param institutionMnemonic the new institution mnemonic
	 */
	public void setInstitutionMnemonic(String institutionMnemonic) {
		this.institutionMnemonic = institutionMnemonic;
	}

	/**
	 * Gets the security code.
	 *
	 * @return the security code
	 */
	public String getSecurityCode() {
		return securityCode;
	}

	/**
	 * Sets the security code.
	 *
	 * @param securityCode the new security code
	 */
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * Gets the cui code.
	 *
	 * @return the cui code
	 */
	public Long getCuiCode() {
		return cuiCode;
	}

	/**
	 * Sets the cui code.
	 *
	 * @param cuiCode the new cui code
	 */
	public void setCuiCode(Long cuiCode) {
		this.cuiCode = cuiCode;
	}

	/**
	 * Gets the market date.
	 *
	 * @return the market date
	 */
	public Date getMarketDate() {
		return marketDate;
	}

	/**
	 * Sets the market date.
	 *
	 * @param marketDate the new market date
	 */
	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	/**
	 * Gets the market rate.
	 *
	 * @return the market rate
	 */
	public BigDecimal getMarketRate() {
		return marketRate;
	}

	/**
	 * Sets the market rate.
	 *
	 * @param marketRate the new market rate
	 */
	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	/**
	 * Gets the market price.
	 *
	 * @return the market price
	 */
	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	/**
	 * Sets the market price.
	 *
	 * @param marketPrice the new market price
	 */
	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	/**
	 * Gets the market date inconsist.
	 *
	 * @return the market date inconsist
	 */
	public Date getMarketDateInconsist() {
		return marketDateInconsist;
	}

	/**
	 * Sets the market date inconsist.
	 *
	 * @param marketDateInconsist the new market date inconsist
	 */
	public void setMarketDateInconsist(Date marketDateInconsist) {
		this.marketDateInconsist = marketDateInconsist;
	}

	/**
	 * Gets the market rate inconsist.
	 *
	 * @return the market rate inconsist
	 */
	public BigDecimal getMarketRateInconsist() {
		return marketRateInconsist;
	}

	/**
	 * Sets the market rate inconsist.
	 *
	 * @param marketRateInconsist the new market rate inconsist
	 */
	public void setMarketRateInconsist(BigDecimal marketRateInconsist) {
		this.marketRateInconsist = marketRateInconsist;
	}

	/**
	 * Gets the market price inconsist.
	 *
	 * @return the market price inconsist
	 */
	public BigDecimal getMarketPriceInconsist() {
		return marketPriceInconsist;
	}

	/**
	 * Sets the market price inconsist.
	 *
	 * @param marketPriceInconsist the new market price inconsist
	 */
	public void setMarketPriceInconsist(BigDecimal marketPriceInconsist) {
		this.marketPriceInconsist = marketPriceInconsist;
	}

	/**
	 * Gets the market quantity.
	 *
	 * @return the market quantity
	 */
	public BigDecimal getMarketQuantity() {
		return marketQuantity;
	}

	/**
	 * Sets the market quantity.
	 *
	 * @param marketQuantity the new market quantity
	 */
	public void setMarketQuantity(BigDecimal marketQuantity) {
		this.marketQuantity = marketQuantity;
	}

	/**
	 * Gets the holder account pk.
	 *
	 * @return the holder account pk
	 */
	public Long getHolderAccountPk() {
		return holderAccountPk;
	}

	/**
	 * Sets the holder account pk.
	 *
	 * @param holderAccountPk the new holder account pk
	 */
	public void setHolderAccountPk(Long holderAccountPk) {
		this.holderAccountPk = holderAccountPk;
	}

	/**
	 * Gets the role.
	 *
	 * @return the role
	 */
	public Integer getRole() {
		return role;
	}

	/**
	 * Sets the role.
	 *
	 * @param role the new role
	 */
	public void setRole(Integer role) {
		this.role = role;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String message =  (documentNumber!=null?"Nro Documento: " + documentNumber:"")
			+ (affectationPk!=null?", Pk Affectacion: " + affectationPk:"")
			+ (accreditationPk!=null?", Pk Accreditacion:" + accreditationPk         :"")
			+ (mechanismOperationPk!=null?", Pk Operation:" + mechanismOperationPk        :"")
			+ (operationNumber!=null?", Nro Operacion:" + operationNumber            :"")
			+ (operationBallot!=null?", Papeleta: " + operationBallot                :"")
			+ (operationSequential!=null?", Sequencial: " + operationSequential          :"")
			+ (participantPk!=null?", Pk Participant: " + participantPk            :"")
			+ (holderAccountPk!=null?", Pk HolderAccount: " + holderAccountPk            :"")
			+ (institutionMnemonic!=null?", Mnemonico:  "+ institutionMnemonic           :"")
			+ (securityCode!=null?", Código Valor: " + securityCode          :"")
			+ (accountNumber!=null?", Nro Cuenta: " + accountNumber                :"")
			+ (cuiCode!=null?", Codigo Cui: " + cuiCode                      :"")
			+ (marketDate!=null?", Fecha Mcdo: " + marketDate                   :"")
			+ (marketRate!=null?", Tasa Mcdo: " + marketRate                    :"")
			+ (marketPrice!=null?", Precio Mcdo: " + marketPrice                 :"")
			+ (marketDateInconsist!=null?", Fecha Mcdo Operacion:"+ marketDateInconsist  :"")
			+ (marketRateInconsist!=null?", Tasa Mcdo Operacion: "+ marketRateInconsist  :"")
			+ (marketPriceInconsist!=null?", Precio Mco Operacion: "+ marketPriceInconsist:"") 
			+ (marketQuantity!=null?", Cantidad Operacion: " + marketQuantity	      :"\n");
		return message;
	}
}