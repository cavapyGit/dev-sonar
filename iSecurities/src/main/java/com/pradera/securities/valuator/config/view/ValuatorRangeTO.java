package com.pradera.securities.valuator.config.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorRangeTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class ValuatorRangeTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id term range pk. */
	private Long idTermRangePk;

	/** The last modify app. */
	private Integer lastModifyApp;

	/** The last modify date. */
	private Date lastModifyDate;

	/** The last modify ip. */
	private String lastModifyIp;

	/** The last modify user. */
	private String lastModifyUser;

	/** The max range. */
	private BigDecimal maxRange;

	/** The min range. */
	private BigDecimal minRange;
	
	/** The new max range. */
	private BigDecimal newMaxRange;

	/** The range code. */
	private String rangeCode;

	/** The registry date. */
	private Date registryDate;

	/** The registry user. */
	private String registryUser;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/**
	 * Instantiates a new valuator range to.
	 */
	public ValuatorRangeTO() {
	}

	/**
	 * Instantiates a new valuator range to.
	 *
	 * @param idTermRangePk the id term range pk
	 * @param lastModifyApp the last modify app
	 * @param lastModifyDate the last modify date
	 * @param lastModifyIp the last modify ip
	 * @param lastModifyUser the last modify user
	 * @param maxRange the max range
	 * @param minRange the min range
	 * @param rangeCode the range code
	 * @param registryDate the registry date
	 * @param registryUser the registry user
	 * @param initialDate the initial date
	 * @param finalDate the final date
	 */
	public ValuatorRangeTO(Long idTermRangePk, Integer lastModifyApp,
			Date lastModifyDate, String lastModifyIp, String lastModifyUser,
			BigDecimal maxRange, BigDecimal minRange, String rangeCode,
			Date registryDate, String registryUser, Date initialDate,
			Date finalDate) {
		super();
		this.idTermRangePk = idTermRangePk;
		this.lastModifyApp = lastModifyApp;
		this.lastModifyDate = lastModifyDate;
		this.lastModifyIp = lastModifyIp;
		this.lastModifyUser = lastModifyUser;
		this.maxRange = maxRange;
		this.minRange = minRange;
		this.rangeCode = rangeCode;
		this.registryDate = registryDate;
		this.registryUser = registryUser;
		this.initialDate = initialDate;
		this.finalDate = finalDate;
	}

	/**
	 * Instantiates a new valuator range to.
	 *
	 * @param idTermRangePk the id term range pk
	 * @param rangeCode the range code
	 * @param maxRange the max range
	 * @param minRange the min range
	 * @param registryDate the registry date
	 */
	public ValuatorRangeTO(Long idTermRangePk, String rangeCode,
			BigDecimal maxRange, BigDecimal minRange,
			Date registryDate) {
		super();
		this.idTermRangePk = idTermRangePk;
		this.maxRange = maxRange;
		this.minRange = minRange;
		this.rangeCode = rangeCode;
		this.registryDate = registryDate;
	}

	/**
	 * Instantiates a new valuator range to.
	 *
	 * @param maxRange the max range
	 * @param minRange the min range
	 */
	public ValuatorRangeTO(BigDecimal maxRange, BigDecimal minRange) {
		super();
		this.maxRange = maxRange;
		this.minRange = minRange;
	}

	/**
	 * Gets the id term range pk.
	 *
	 * @return the id term range pk
	 */
	public Long getIdTermRangePk() {
		return this.idTermRangePk;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the max range.
	 *
	 * @return the max range
	 */
	public BigDecimal getMaxRange() {
		return this.maxRange;
	}

	/**
	 * Sets the max range.
	 *
	 * @param maxRange the new max range
	 */
	public void setMaxRange(BigDecimal maxRange) {
		this.maxRange = maxRange;
	}

	/**
	 * Gets the min range.
	 *
	 * @return the min range
	 */
	public BigDecimal getMinRange() {
		return this.minRange;
	}

	/**
	 * Sets the min range.
	 *
	 * @param minRange the new min range
	 */
	public void setMinRange(BigDecimal minRange) {
		this.minRange = minRange;
	}

	/**
	 * Gets the new max range.
	 *
	 * @return the new max range
	 */
	public BigDecimal getNewMaxRange() {
		return newMaxRange;
	}

	/**
	 * Sets the new max range.
	 *
	 * @param newMaxRange the new new max range
	 */
	public void setNewMaxRange(BigDecimal newMaxRange) {
		this.newMaxRange = newMaxRange;
	}

	/**
	 * Gets the range code.
	 *
	 * @return the range code
	 */
	public String getRangeCode() {
		return this.rangeCode;
	}

	/**
	 * Sets the range code.
	 *
	 * @param rangeCode the new range code
	 */
	public void setRangeCode(String rangeCode) {
		this.rangeCode = rangeCode;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Sets the id term range pk.
	 *
	 * @param idTermRangePk the new id term range pk
	 */
	public void setIdTermRangePk(Long idTermRangePk) {
		this.idTermRangePk = idTermRangePk;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}
	
	
}