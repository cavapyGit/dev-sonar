package com.pradera.securities.valuator.core.service; 

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.valuatorprocess.ValuatorProcessExecution;
import com.pradera.model.valuatorprocess.type.ValuatorProcessExecutionDateType;
import com.pradera.securities.valuator.core.view.ValuatorExecutionDetailTO;
import com.pradera.securities.valuator.core.view.ValuatorExecutionFailedTO;
import com.pradera.securities.valuator.core.view.ValuatorProcessExecutionTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorProcessDetailService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@Stateless
public class ValuatorProcessDetailService extends CrudDaoServiceBean{
	
	/** The max results query. */
	@Inject
	@Configurable
	private Integer maxResultsQuery;
	
	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup IdepositarySetup idepositarySetup;
		
	/***
	 * Method to find a valuator process execution by Id.
	 * @param idProcessExecutionPk the id process execution
	 * @return a object ValuatorProcessExecution type
	 * @throws ServiceException the service exception
	 */
	public ValuatorProcessExecution viewProcessExecution(Long idProcessExecutionPk) throws ServiceException {
		return find(ValuatorProcessExecution.class, idProcessExecutionPk);
	}
	
	/***
	 * Method to get a list of ValuatorProcessExecutionTO by ValuatorProcessExecutionTO.
	 * @param valuatorProcessExecutionTO the TO valuator process simulator
	 * @return a list of ValuatorProcessExecutionTO
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ValuatorProcessExecutionTO> getValuatorProcessServiceBean(ValuatorProcessExecutionTO valuatorProcessExecutionTO) throws ServiceException {
		StringBuilder selectSQL = new StringBuilder();
		StringBuilder whereSQL = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();

		selectSQL.append("Select 																					");
		selectSQL.append("	vpe.idProcessExecutionPk, vpo.operationNumber, vpe.beginTime, vpe.executionType,		");
		selectSQL.append("	vpe.finishTime, vpe.processState, vpe.registryDate, vpe.valuatorType 					");
		
		selectSQL.append("From ValuatorProcessExecution vpe															");
		selectSQL.append("Left Join vpe.valuatorProcessOperation vpo												");
		
		whereSQL.append("Where 1 = 1 ");
		if (valuatorProcessExecutionTO.getDateType() != null) {
			String executionDateType = null;
			switch (ValuatorProcessExecutionDateType.get(valuatorProcessExecutionTO.getDateType())) {
				case DATE_TYPE:	executionDateType = "processDate"; break;
			}
			whereSQL.append("And vpe.").append(executionDateType).append("	>= :initialDate 						");
			whereSQL.append("And vpe.").append(executionDateType).append("	<= :finalDate 							");
			parameters.put("initialDate", valuatorProcessExecutionTO.getInitialDate());
			parameters.put("finalDate", valuatorProcessExecutionTO.getFinalDate());
		}
		
		if (valuatorProcessExecutionTO.getExecutionType() != null) {
			whereSQL.append("And vpe.executionType = :executionType ");
			parameters.put("executionType",valuatorProcessExecutionTO.getExecutionType());
		}
		if (valuatorProcessExecutionTO.getProcessState() != null) {
			whereSQL.append("And vpe.processState = :processState ");
			parameters.put("processState",valuatorProcessExecutionTO.getProcessState());
		}
		if (valuatorProcessExecutionTO.getOperationNumber() != null) {
			whereSQL.append("And vpo.operationNumber = :operationNumber ");
			parameters.put("operationNumber",valuatorProcessExecutionTO.getOperationNumber());
		}
		if (valuatorProcessExecutionTO.getValuatorType() != null) {
			whereSQL.append("And vpe.valuatorType = :valuatorType ");
			parameters.put("valuatorType",valuatorProcessExecutionTO.getValuatorType());
		}
		whereSQL.append("Order By vpe.idProcessExecutionPk Desc	");
		try {
			String finalQuery = selectSQL.toString().concat(whereSQL.toString());
			List<Object[]> dataObjectList = findListByQueryString(finalQuery,parameters);
			List<ValuatorProcessExecutionTO> processExecutionList = new ArrayList<>();
			for(Object[] data: dataObjectList){
				ValuatorProcessExecutionTO executionTO = new ValuatorProcessExecutionTO();
				executionTO.setIdProcessExecutionPk((Long) data[0]);
				executionTO.setOperationNumber((Long) data[1]);
				executionTO.setBeginTime((Date) data[2]);
				executionTO.setExecutionType((Integer) data[3]);
				executionTO.setFinishTime((Date) data[4]);
				executionTO.setProcessState((Integer) data[5]);
				executionTO.setRegistryDate((Date) data[6]);
				executionTO.setValuatorType((Integer) data[7]);
				processExecutionList.add(executionTO);
			}
			return processExecutionList;
		} catch (NoResultException ex) {
			return null;
		}
	}

	/***
	 * Method to get a list of ValuatorExecutionDetailTO by id process execution.
	 * @param idProcessExecutionPk the id process execution
	 * @return a list of ValuatorExecutionDetailTO
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ValuatorExecutionDetailTO> getValuatorExecutionDetail(Long idProcessExecutionPk) throws ServiceException {
		StringBuilder selectSQL = new StringBuilder();
		StringBuilder whereSQL = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		selectSQL.append("Select 																			");
		selectSQL.append("	ved.idExecutionDetailPk, ved.detailType, ved.beginTime, ved.finishTime,			");
		selectSQL.append("	ved.orderExecution, ved.quantityProcess, ved.quantityInconsistent,				");
		selectSQL.append("	ved.quantityFinished, ved.processState, vpe.idProcessExecutionPk 				");
		
		selectSQL.append("From ValuatorExecutionDetail ved													");
		selectSQL.append("Inner Join ved.valuatorProcessExecution vpe										");

		whereSQL.append("Where 1 = 1 ");
		if (idProcessExecutionPk != null) {
			whereSQL.append("And vpe.idProcessExecutionPk = :idProcessExecutionPk ");
			parameters.put("idProcessExecutionPk",idProcessExecutionPk);
		}
		try {
			
			List<Object[]> dataObjectList = findListByQueryString(selectSQL.toString().concat(whereSQL.toString()),parameters);
			List<ValuatorExecutionDetailTO> processExecutionList = new ArrayList<>();
			for(Object[] data: dataObjectList){
				ValuatorExecutionDetailTO detailTO = new ValuatorExecutionDetailTO();
				detailTO.setIdExecutionDetailPk((Long) data[0]);
				detailTO.setDetailType((Integer) data[1]);
				detailTO.setBeginTime((Date) data[2]);
				detailTO.setFinishTime((Date) data[3]);
				detailTO.setOrderExecution((Integer) data[4]);
				detailTO.setQuantityProcess((Long) data[5]);
				detailTO.setQuantityInconsistent((Long) data[6]);
				detailTO.setQuantityFinished((Long) data[7]);
				detailTO.setProcessState((Integer) data[8]);

				ValuatorProcessExecution valuatorProcessExecution = new ValuatorProcessExecution();
				valuatorProcessExecution.setIdProcessExecutionPk((Long) data[9]);
				detailTO.setValuatorProcessExecution(valuatorProcessExecution);

				processExecutionList.add(detailTO);
			}
			
			return processExecutionList;
		} catch (NoResultException ex) {
			return null;
		}
	}

	/**
	 * *
	 * Method to get a list of ValuatorExecutionDetailTO by id process execution.
	 *
	 * @param idExecutionDetailPk the id execution detail pk
	 * @return a list of ValuatorExecutionDetailTO
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ValuatorExecutionFailedTO> getValuatorExecutionFailed(Long idExecutionDetailPk) throws ServiceException {
		StringBuilder selectSQL = new StringBuilder();
		StringBuilder whereSQL = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		selectSQL.append("Select new com.pradera.securities.valuator.core.view.ValuatorExecutionFailedTO(		");
		selectSQL.append("	vef.idExecutionFailedPk, part.idParticipantPk, part.mnemonic,					");
		selectSQL.append("	ha.accountNumber, sec.idSecurityCodePk, vef.motive )							");
		
		selectSQL.append("From ValuatorExecutionFailed vef													");
		selectSQL.append("Inner Join vef.valuatorExecutionDetail ved										");
		selectSQL.append("Inner Join vef.holderAccountBalance hab											");
		selectSQL.append("Inner Join hab.participant part													");
		selectSQL.append("Inner Join hab.holderAccount ha													");
		selectSQL.append("Inner Join hab.security sec														");
		

		whereSQL.append("Where 1 = 1 ");
		if (idExecutionDetailPk != null) {
			whereSQL.append("And ved.idExecutionDetailPk = :idExecutionDetailPk ");
			parameters.put("idExecutionDetailPk",idExecutionDetailPk);
		}
		try {
			return findListByQueryString(selectSQL.toString().concat(whereSQL.toString()),parameters);
		} catch (NoResultException ex) {
			return null;
		}
	}
	
}