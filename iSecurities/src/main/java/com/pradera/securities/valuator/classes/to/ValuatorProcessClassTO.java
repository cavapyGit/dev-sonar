package com.pradera.securities.valuator.classes.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorProcessClassTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class ValuatorProcessClassTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	

	/** The id valuator class pk. */
	private String idValuatorClassPk;

	/** The class description. */
	private String classDescription;

	/** The currency pk. */
	private Integer currencyPk;
	
	/** The currency name. */
	private String currencyName;

	/** The min relevant amount. */
	private BigDecimal minRelevantAmount;

	/** The registry date. */
	private Date registryDate;


	/** The top relevant amount. */
	private BigDecimal topRelevantAmount;
	
	
	/**
	 * Instantiates a new valuator process class to.
	 */
	public ValuatorProcessClassTO() {
		super();
	}

	/**
	 * Instantiates a new valuator process class to.
	 *
	 * @param idValuatorClassPk the id valuator class pk
	 * @param classDescription the class description
	 * @param currencyPk the currency pk
	 * @param currencyName the currency name
	 * @param minRelevantAmount the min relevant amount
	 * @param registryDate the registry date
	 * @param topRelevantAmount the top relevant amount
	 */
	public ValuatorProcessClassTO(String idValuatorClassPk,
			String classDescription, Integer currencyPk, String currencyName,
			BigDecimal minRelevantAmount, Date registryDate,BigDecimal topRelevantAmount) {
		super();
		this.idValuatorClassPk = idValuatorClassPk;
		this.classDescription = classDescription;
		this.currencyPk = currencyPk;
		this.currencyName = currencyName;
		this.minRelevantAmount = minRelevantAmount;
		this.registryDate = registryDate;
		this.topRelevantAmount = topRelevantAmount;
	}

	/**
	 * Gets the id valuator class pk.
	 *
	 * @return the id valuator class pk
	 */
	public String getIdValuatorClassPk() {
		return idValuatorClassPk;
	}

	/**
	 * Sets the id valuator class pk.
	 *
	 * @param idValuatorClassPk the new id valuator class pk
	 */
	public void setIdValuatorClassPk(String idValuatorClassPk) {
		this.idValuatorClassPk = idValuatorClassPk;
	}

	/**
	 * Gets the class description.
	 *
	 * @return the class description
	 */
	public String getClassDescription() {
		return classDescription;
	}

	/**
	 * Sets the class description.
	 *
	 * @param classDescription the new class description
	 */
	public void setClassDescription(String classDescription) {
		this.classDescription = classDescription;
	}

	/**
	 * Gets the currency pk.
	 *
	 * @return the currency pk
	 */
	public Integer getCurrencyPk() {
		return currencyPk;
	}

	/**
	 * Sets the currency pk.
	 *
	 * @param currencyPk the new currency pk
	 */
	public void setCurrencyPk(Integer currencyPk) {
		this.currencyPk = currencyPk;
	}

	/**
	 * Gets the currency name.
	 *
	 * @return the currency name
	 */
	public String getCurrencyName() {
		return currencyName;
	}

	/**
	 * Sets the currency name.
	 *
	 * @param currencyName the new currency name
	 */
	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	/**
	 * Gets the min relevant amount.
	 *
	 * @return the min relevant amount
	 */
	public BigDecimal getMinRelevantAmount() {
		return minRelevantAmount;
	}

	/**
	 * Sets the min relevant amount.
	 *
	 * @param minRelevantAmount the new min relevant amount
	 */
	public void setMinRelevantAmount(BigDecimal minRelevantAmount) {
		this.minRelevantAmount = minRelevantAmount;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the top relevant amount.
	 *
	 * @return the top relevant amount
	 */
	public BigDecimal getTopRelevantAmount() {
		return topRelevantAmount;
	}

	/**
	 * Sets the top relevant amount.
	 *
	 * @param topRelevantAmount the new top relevant amount
	 */
	public void setTopRelevantAmount(BigDecimal topRelevantAmount) {
		this.topRelevantAmount = topRelevantAmount;
	}
	
	
}
