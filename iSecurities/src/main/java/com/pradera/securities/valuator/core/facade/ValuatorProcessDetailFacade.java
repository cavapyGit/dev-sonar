package com.pradera.securities.valuator.core.facade;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.configuration.ValuatorSetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.valuatorprocess.ValuatorExecutionDetail;
import com.pradera.model.valuatorprocess.ValuatorProcessExecution;
import com.pradera.model.valuatorprocess.ValuatorProcessSetup;
import com.pradera.securities.valuator.core.service.ValuatorProcessDetailService;
import com.pradera.securities.valuator.core.view.ValuatorExecutionDetailTO;
import com.pradera.securities.valuator.core.view.ValuatorExecutionFailedTO;
import com.pradera.securities.valuator.core.view.ValuatorProcessExecutionTO;

@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ValuatorProcessDetailFacade {
	
	/** The transaction registry. */
	@Resource private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The valuator simulator service. */
	@EJB ValuatorProcessDetailService valuatorProcessDetailService;
	
	/** The participant service bean. */
	@EJB ParticipantServiceBean participantServiceBean;
	
	/** The parameter facade. */
	@EJB GeneralParametersFacade parameterFacade;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup private IdepositarySetup idepositarySetup;
	
	/** The valuator setup. */
	@Inject @ValuatorSetup private ValuatorProcessSetup valuatorSetup;
	
	/***
	 * Method to find a valuator process execution by Id.
	 * @param idProcessExecutionPk the id process execution
	 * @return a object ValuatorProcessExecution type
	 * @throws ServiceException the service exception
	 */
	public ValuatorProcessExecution viewProcessExecution(Long idProcessExecutionPk) throws ServiceException {
		ValuatorProcessExecution valuatorProcessExecution = valuatorProcessDetailService.viewProcessExecution(idProcessExecutionPk);
		valuatorProcessExecution.getValuatorProcessOperation().getOperationNumber();
		return valuatorProcessExecution;
	}
	
	/**
	 * Gets the security class parameter.
	 *
	 * @return the security class parameter
	 */
	public Map<Integer,String> getSecurityClassParameter(){
		Map<Integer,String> securityClassMap = new HashMap<Integer, String>();
		ParameterTableTO parameterFilter = new ParameterTableTO();
		parameterFilter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		parameterFilter.setState(ComponentConstant.ONE);
		try {
			List<ParameterTable> securityClassList = parameterFacade.getListParameterTableServiceBean(parameterFilter);
			for(ParameterTable pTable: securityClassList){
				securityClassMap.put(pTable.getParameterTablePk(), pTable.getText1());
			}
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return securityClassMap;
	}
	
	/** The compare by pk. */
	public static Comparator<ValuatorExecutionDetail> COMPARE_BY_PK = new Comparator<ValuatorExecutionDetail>() {
		 public int compare(ValuatorExecutionDetail o1, ValuatorExecutionDetail o2) {
				/**ORDENAMOS LOS CUPONES POR NUMERO DE CUPON*/
				return o1.getIdExecutionDetailPk().compareTo(o2.getIdExecutionDetailPk());
//		        if ( resultado != 0 ) { return resultado; }
//				return resultado;
			}
	    };
	
	/**
	 * *
	 * Method to get notification track from execution.
	 *
	 * @param processExecutionPk the process execution pk
	 * @return the execution monitor
	 * @throws ServiceException the service exception
	 */
//	public ValuatorNotificationTrack getExecutionMonitor(Long processExecutionPk) throws ServiceException{
//		Map<Integer,String> securityClassMap = getSecurityClassParameter();
//		
//		ValuatorNotificationTrack monitorTrack = new ValuatorNotificationTrack();
//		monitorTrack.setExecutionTrackDetail(new ArrayList<ValuatorTrackDetail>());
//		
//		ValuatorProcessExecution execution = valuatorProcessDetailService.viewProcessExecution(processExecutionPk);
//		
//		monitorTrack.setBeginTime(execution.getBeginTime());
//		monitorTrack.setFinishTime(execution.getFinishTime());
//		
//		List<ValuatorExecutionDetail> detailList = execution.getValuatorExecutionDetails();
//		Collections.sort(detailList,COMPARE_BY_PK);
//		
//		for(ValuatorExecutionDetail processDetail: detailList){
//			
//			if(processDetail.getSecurityClass()==null){
//				ValuatorTrackDetail monitorTrackDetail = new ValuatorTrackDetail();
//				monitorTrackDetail.setEventTypeCode(processDetail.getValuatorEventBehavior().getIdValuatorEventPk());
//				monitorTrackDetail.setEventName(processDetail.getValuatorEventBehavior().getEventName());
//				monitorTrackDetail.setSequence(processDetail.getValuatorEventBehavior().getExecutionSequence());
//				monitorTrackDetail.setBeginTime(processDetail.getBeginTime());
//				monitorTrackDetail.setFinishTime(processDetail.getFinishTime());
//				monitorTrackDetail.setProcessState(processDetail.getProcessState());
//				
//				if(!processDetail.getValuatorExecutionDetails().isEmpty() && processDetail.getValuatorProcessExecution()!=null){
//					monitorTrackDetail.setSecurityClassList(new ArrayList<ValuatorTrackDetail>());
//					
//					for(ValuatorExecutionDetail subDetail: processDetail.getValuatorExecutionDetails()){
//						ValuatorTrackDetail monitorSubDetail = new ValuatorTrackDetail();
//						monitorSubDetail.setBeginTime(subDetail.getBeginTime());
//						monitorSubDetail.setFinishTime(subDetail.getFinishTime());
//						monitorSubDetail.setProcessState(subDetail.getProcessState());
//						monitorSubDetail.setRowQuantity(subDetail.getQuantityProcess());
//						monitorSubDetail.setClassDescription(securityClassMap.get(subDetail.getSecurityClass()));
//						monitorTrackDetail.getSecurityClassList().add(monitorSubDetail);
//					}
//				}
//				monitorTrack.getExecutionTrackDetail().add(monitorTrackDetail);
//			}
//		}
//		return monitorTrack;
//	}

	/***
	 * Method to get a list of ValuatorProcessExecutionTO by ValuatorProcessExecutionTO.
	 * @param valuatorProcessExecutionTO the TO valuator process simulator
	 * @return a list of ValuatorProcessExecutionTO
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.VALUATOR_EXECUTION_SIMULATION_QUERY)
	public List<ValuatorProcessExecutionTO> getValuatorProcessFacade(ValuatorProcessExecutionTO valuatorProcessExecutionTO) throws ServiceException{
		List<ValuatorProcessExecutionTO> list = valuatorProcessDetailService.getValuatorProcessServiceBean(valuatorProcessExecutionTO);
		return list;
	}

	/***
	 * Method to get a list of ValuatorExecutionDetailTO by id process execution.
	 * @param idProcessExecutionPk the id process execution
	 * @return a list of ValuatorExecutionDetailTO
	 * @throws ServiceException the service exception
	 */
	public List<ValuatorExecutionDetailTO> getValuatorExecutionDetail(Long idProcessExecutionPk) throws ServiceException {
		List<ValuatorExecutionDetailTO> list = valuatorProcessDetailService.getValuatorExecutionDetail(idProcessExecutionPk);
		return list;
	}

	/**
	 * *
	 * Method to get a list of ValuatorExecutionFailedTO by id execution detail.
	 *
	 * @param idExecutionDetailPk the id execution detail pk
	 * @return a list of ValuatorExecutionFailedTO
	 * @throws ServiceException the service exception
	 */
	public List<ValuatorExecutionFailedTO> getValuatorExecutionFailed(Long idExecutionDetailPk) throws ServiceException {
		List<ValuatorExecutionFailedTO> list = valuatorProcessDetailService.getValuatorExecutionFailed(idExecutionDetailPk);
		return list;
	}
	
}