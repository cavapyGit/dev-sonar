package com.pradera.securities.valuator.core.files.facade;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.ProcessLogger;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright EDV BOLIVIA 2016.</li>
 * </ul>
 * 
 * The Class ValuatorBalancePsyBatch.
 *
 * @author OQUISBERT.
 * @version 1.0 , 07-OCT-2016
 */

@BatchProcess(name="ValuatorBalancePsyBatch")
@RequestScoped
public class ValuatorBalancePsyBatch implements Serializable,JobExecution{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	
	/** The accreditation certificates service facade. */
	@EJB
	private BalancePsyFacade balancePsyFacade;
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {

			
			log.info("::::::::::::: INICIO DEL BATCH :::::::::::::");
			
			try {
				balancePsyFacade.processDailyBalancePsy();
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			
			log.info("::::::::::::: FIN BATCH BALANCE PSY  :::::::::::::");
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}
}