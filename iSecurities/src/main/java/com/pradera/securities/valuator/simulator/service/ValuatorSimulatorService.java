package com.pradera.securities.valuator.simulator.service; 

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountBalancePK;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.valuatorprocess.ValuatorProcessSimulator;
import com.pradera.model.valuatorprocess.ValuatorSimulationBalance;
import com.pradera.model.valuatorprocess.ValuatorSimulatorCode;
import com.pradera.model.valuatorprocess.ValuatorSimulatorSecurity;
import com.pradera.securities.valuator.simulator.view.DetailSimulationTO;
import com.pradera.securities.valuator.simulator.view.ValuatorProcessSimulatorTO;
import com.pradera.securities.valuator.simulator.view.ValuatorSimulationBalanceTO;
import com.pradera.securities.valuator.simulator.view.ValuatorSimulatorCodeTO;
import com.pradera.securities.valuator.simulator.view.ValuatorSimulatorSecurityTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorSimulatorService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@Stateless
public class ValuatorSimulatorService extends CrudDaoServiceBean{
	
	/** The max results query. */
	@Inject
	@Configurable
	private Integer maxResultsQuery;
	
	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup IdepositarySetup idepositarySetup;
	
	/**
	 * Creates the valuator process simulator
	 * and the valuator process operation.
	 *
	 * @param valuatorProcessSimulator the valuator process simulator
	 * @return the valuator process simulator
	 * @throws ServiceException the service exception
	 */
	public ValuatorProcessSimulator saveNewProcessSimulator(ValuatorProcessSimulator valuatorProcessSimulator) throws ServiceException {
		create(valuatorProcessSimulator.getValuatorProcessOperation());		
		return create(valuatorProcessSimulator);
	}
	
	/**
	 * Creates the list of valuator simulation balances.
	 *
	 * @param valuatorSimulationBalances the list of valuator simulation balances
	 * @throws ServiceException the service exception
	 */
	public void saveNewListSimulationBalance(List<ValuatorSimulationBalance> valuatorSimulationBalances) throws ServiceException {
		saveAll(valuatorSimulationBalances);
	}
	
	/**
	 * Creates the list of valuator simulator codes
	 * and the list of valuator simulator securities
	 * for each valuator simulator code.
	 *
	 * @param valuatorSimulatorCodes the list of valuator simulator codes
	 * @throws ServiceException the service exception
	 */
	public void saveNewListSimulatorCode(List<ValuatorSimulatorCode> valuatorSimulatorCodes) throws ServiceException {
		for (ValuatorSimulatorCode code : valuatorSimulatorCodes) {
			code = create(code);
			List<ValuatorSimulatorSecurity> valuatorSimulatorSecurities = code.getValuatorSimulatorSecurities();
			for (ValuatorSimulatorSecurity valuatorSimulatorSecurity : valuatorSimulatorSecurities) {
				valuatorSimulatorSecurity.setValuatorSimulatorCode(code);
				create(valuatorSimulatorSecurity);
			}
		}
	}
	
	/***
	 * Method to find a valuator process Simulator by Id.
	 * @param idProcessSimulatorPk the id process simulator
	 * @return a object ValuatorProcessSimulator type
	 * @throws ServiceException the service exception
	 */
	public ValuatorProcessSimulator viewProcessSimulator(Long idProcessSimulatorPk) throws ServiceException {
		return find(ValuatorProcessSimulator.class, idProcessSimulatorPk);
	}
	
	/***
	 * Method to get a list of ValuatorProcessSimulatorTO by ValuatorProcessSimulatorTO.
	 * @param valuatorProcessSimulatorTO the TO valuator process simulator
	 * @return a list of ValuatorProcessSimulatorTO
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ValuatorProcessSimulatorTO> getValuatorSimulatorServiceBean(ValuatorProcessSimulatorTO valuatorProcessSimulatorTO) throws ServiceException {
		StringBuilder selectSQL = new StringBuilder();
		StringBuilder whereSQL = new StringBuilder();
		StringBuilder orderBySQL = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();

		selectSQL.append("Select new com.pradera.securities.valuator.simulator.view.ValuatorProcessSimulatorTO(	");
		selectSQL.append("	vps.idValuatorSimulatorPk, vps.registryDate, vps.simulationForm,					");
		selectSQL.append("	p.mnemonic, s.idSecurityCodePk, vps.processState )									");
		selectSQL.append("From ValuatorProcessSimulator vps														");
		selectSQL.append("Left Join vps.participant p															");
		selectSQL.append("Left Join vps.holder h																");
		selectSQL.append("Left Join vps.holderAccount ha														");
		selectSQL.append("Left Join vps.security s																");
		
		whereSQL.append("Where 1 = 1 ");
		if (valuatorProcessSimulatorTO.getParticipant().getIdParticipantPk() != null) {
			whereSQL.append("And p.idParticipantPk = :idParticipantPk ");
			parameters.put("idParticipantPk",valuatorProcessSimulatorTO.getParticipant().getIdParticipantPk());
		}
		if (valuatorProcessSimulatorTO.getHolder().getIdHolderPk() != null) {
			whereSQL.append("And h.idHolderPk = :idHolderPk ");
			parameters.put("idHolderPk",valuatorProcessSimulatorTO.getHolder().getIdHolderPk());
		}
		if (valuatorProcessSimulatorTO.getHolderAccount().getIdHolderAccountPk() != null) {
			whereSQL.append("And ha.idHolderAccountPk = :idHolderAccountPk ");
			parameters.put("idHolderAccountPk",valuatorProcessSimulatorTO.getHolderAccount().getIdHolderAccountPk());
		}
		if (valuatorProcessSimulatorTO.getSecurity().getIdSecurityCodePk() != null) {
			whereSQL.append("And s.idSecurityCodePk = :idSecurityCodePk ");
			parameters.put("idSecurityCodePk",valuatorProcessSimulatorTO.getSecurity().getIdSecurityCodePk());
		}
		if (valuatorProcessSimulatorTO.getSimulationForm() != null) {
			whereSQL.append("And vps.simulationForm = :simulationForm ");
			parameters.put("simulationForm",valuatorProcessSimulatorTO.getSimulationForm());
		}
		if (valuatorProcessSimulatorTO.getIdValuatorSimulatorPk() != null) {
			whereSQL.append("And vps.idValuatorSimulatorPk = :idValuatorSimulatorPk ");
			parameters.put("idValuatorSimulatorPk",valuatorProcessSimulatorTO.getIdValuatorSimulatorPk());
		}
		if (valuatorProcessSimulatorTO.getSimulationInitialDate() != null) {
			whereSQL.append("And Trunc(vps.registryDate) >= :initialDate ");
			parameters.put("initialDate",valuatorProcessSimulatorTO.getSimulationInitialDate());
		}
		if (valuatorProcessSimulatorTO.getSimulationFinalDate() != null) {
			whereSQL.append("And Trunc(vps.registryDate) <= :finalDate ");
			parameters.put("finalDate",valuatorProcessSimulatorTO.getSimulationFinalDate());
		}

		orderBySQL.append(" Order By vps.idValuatorSimulatorPk ");
		
//		selectSQL.append("	vps.idValuatorSimulatorPk, vps.registryDate, vps.simulationForm,					");
//		selectSQL.append("	p.mnemonic, s.idSecurityCodePk, vps.processState )									");
		
		try {
			return findListByQueryString(selectSQL.toString().concat(whereSQL.toString()).concat(orderBySQL.toString()),parameters);
//			List<ValuatorProcessSimulatorTO> listWithData = new ArrayList<>();
//			List<Object[]> objectData = findListByQueryString(selectSQL.toString().concat(whereSQL.toString()).concat(orderBySQL.toString()),parameters);
//			
//			for(Object[] data: objectData){
//				ValuatorProcessSimulatorTO simulatorProcess = new ValuatorProcessSimulatorTO()
//			}
		} catch (NoResultException ex) {
			return null;
		}
	}

	/**
	 * *
	 * Method to get a list of ValuatorSimulatorCodeTO by ValuatorProcessSimulator.
	 *
	 * @param valuatorProcessSimulator the valuator process simulator
	 * @return a list of ValuatorSimulatorCodeTO
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ValuatorSimulatorCodeTO> getValuatorSimulatorCode(ValuatorProcessSimulator valuatorProcessSimulator) throws ServiceException {
		StringBuilder selectSQL = new StringBuilder();
		StringBuilder whereSQL = new StringBuilder();
		StringBuilder groupBySQL = new StringBuilder();
		StringBuilder havingSQL = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		Long idHolderPk = valuatorProcessSimulator.getHolder().getIdHolderPk();
		Long idParticipantPk = valuatorProcessSimulator.getParticipant().getIdParticipantPk();
		String idSecurityCodePk = valuatorProcessSimulator.getSecurity().getIdSecurityCodePk();
		Long accountId = valuatorProcessSimulator.getHolderAccount().getIdHolderAccountPk();
		
		List<Long> accountsID = null;
		if(idHolderPk != null ){
			if(accountId == null){
				accountsID = getAccountsID(idHolderPk, idParticipantPk);
			}else{
				accountsID = Arrays.asList(accountId);
			}
		}
		
		if(idHolderPk != null && idSecurityCodePk != null)
			accountsID = getAccountsID(idHolderPk, idParticipantPk);
		
		selectSQL.append("Select distinct new com.pradera.securities.valuator.simulator.view.ValuatorSimulatorCodeTO(		");
		selectSQL.append("	vc.idValuatorCodePk, vc.idValuatorCode )													");
		
		selectSQL.append("From HolderAccountBalance hab																	");
		selectSQL.append("Inner Join hab.security s																		");
		selectSQL.append("Inner Join hab.participant p																	");
		selectSQL.append("Inner Join hab.holderAccount ha																");
		
		selectSQL.append("Left Join s.listValuatorSecurityCode valSec													");
		selectSQL.append("Left Join valSec.valuatorCode vc																");

		whereSQL.append("Where 1 = 1 ");
		whereSQL.append("And s.instrumentType = :instrumentType And hab.totalBalance > :oneValue						");
		parameters.put("instrumentType",InstrumentType.FIXED_INCOME.getCode());
		parameters.put("oneValue",BigDecimal.ZERO);
		if (idParticipantPk != null) {
			whereSQL.append("And p.idParticipantPk = :idParticipantPk ");
			parameters.put("idParticipantPk",idParticipantPk);
		}
		if (idHolderPk != null && accountsID != null) {
			whereSQL.append("And ha.idHolderAccountPk IN :accountsID ");
			parameters.put("accountsID", accountsID);
		}
		if (idSecurityCodePk != null) {
			whereSQL.append("And s.idSecurityCodePk = :idSecurityCodePk ");
			parameters.put("idSecurityCodePk", idSecurityCodePk);
		}

		groupBySQL.append(" Group By vc.idValuatorCodePk, vc.idValuatorCode ");
		havingSQL.append(" Having vc.idValuatorCodePk = Max(vc.idValuatorCodePk)");
		
		List<ValuatorSimulatorCodeTO> listCodeTO = null;
		try {
			listCodeTO = findListByQueryString(selectSQL.toString().concat(whereSQL.toString()).concat(groupBySQL.toString()).concat(havingSQL.toString()),parameters);
			
			if(listCodeTO != null){
				for (ValuatorSimulatorCodeTO codeTO : listCodeTO) {
					List<ValuatorSimulatorSecurityTO> listSecurityTO = getValuatorSimulatorSecurity(codeTO.getIdValuatorCodePk(),idParticipantPk,idSecurityCodePk,accountsID);
					for (ValuatorSimulatorSecurityTO securityTO : listSecurityTO) {
						if(securityTO.getInterestRate() != null){
							securityTO.setInterestRate(securityTO.getInterestRate().divide(GeneralConstants.HUNDRED_VALUE_BIGDECIMAL));
						}
						securityTO.setValueNominalRate(Boolean.FALSE);
					}
					codeTO.setSecurityTO(new GenericDataModel<>(listSecurityTO));
				}
			}
			
			return listCodeTO;
		} catch (NoResultException ex) {
			return null;
		}
	}

	/***
	 * Method to get a list of ValuatorSimulationBalanceTO by ValuatorProcessSimulator.
	 * @param valuatorProcessSimulator valuator process simulator
	 * @return a list of ValuatorSimulationBalanceTO
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ValuatorSimulationBalanceTO> getValuatorSimulationBalance(ValuatorProcessSimulator valuatorProcessSimulator) throws ServiceException {
		StringBuilder selectSQL = new StringBuilder();
		StringBuilder whereSQL = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		Long idHolderPk = valuatorProcessSimulator.getHolder().getIdHolderPk();
		Long idParticipantPk = valuatorProcessSimulator.getParticipant().getIdParticipantPk();
		String idSecurityCodePk = valuatorProcessSimulator.getSecurity().getIdSecurityCodePk();
		Long accountId = valuatorProcessSimulator.getHolderAccount().getIdHolderAccountPk();
		
		List<Long> accountsID = null;
		
		if(idHolderPk != null ){
			if(accountId == null){
				accountsID = getAccountsID(idHolderPk, idParticipantPk);
			}else{
				accountsID = Arrays.asList(accountId);
			}
		}
			
		
		selectSQL.append("Select 																				");
		selectSQL.append("	p.idParticipantPk, ha.idHolderAccountPk,s.idSecurityCodePk, p.mnemonic, 			");
		selectSQL.append("	vc.idValuatorCode, ha.alternateCode, ha.description, 								");
		selectSQL.append("	hab.totalBalance, hab.availableBalance 												");
		selectSQL.append("From HolderAccountBalance hab															");
		selectSQL.append("Inner Join hab.security s																");
		selectSQL.append("Inner Join hab.participant p															");
		selectSQL.append("Inner Join hab.holderAccount ha														");
		selectSQL.append("inner join hab.holderMarketfactBalance hmb											");
		selectSQL.append("Left Join hmb.valuatorCode vc															");
		whereSQL.append("Where 1 = 1 And hab.totalBalance > 0													");
		if (idParticipantPk != null) {
			whereSQL.append("And p.idParticipantPk = :idParticipantPk ");
			parameters.put("idParticipantPk",idParticipantPk);
		}
		if (idHolderPk != null && accountsID != null) {
			whereSQL.append("And ha.idHolderAccountPk IN :accountsID ");
			parameters.put("accountsID", accountsID);
		}
		if (idSecurityCodePk != null) {
			whereSQL.append("And s.idSecurityCodePk = :idSecurityCodePk ");
			parameters.put("idSecurityCodePk", idSecurityCodePk);
		}
		try {
			List<ValuatorSimulationBalanceTO> balanceDataList = new ArrayList<>();
			List<Object[]> objectData = findListByQueryString(selectSQL.toString().concat(whereSQL.toString()),parameters);
			for(Object[] data: objectData){
				ValuatorSimulationBalanceTO balanceData = new ValuatorSimulationBalanceTO();
				
				/**GEtting data from query and BD*/
				Long participantPk = (Long) data[0];
				Long holderAccountPk = (Long) data[1];
				String securityCode = (String) data[2];
				String partMnemonic = (String) data[3];
				String valuatorCode = (String) data[4];
				String alternatCode= (String) data[5];
				String accountDesc = (String) data[6];
				BigDecimal totalBalance= (BigDecimal) data[7];
				BigDecimal availableBalance= (BigDecimal) data[8];
				
				/**Setting TO with data*/
				HolderAccountBalance holderAccountBalance = new HolderAccountBalance();
				HolderAccountBalancePK idHolderAccountBalance = new HolderAccountBalancePK();
				idHolderAccountBalance.setIdParticipantPk(participantPk);
				idHolderAccountBalance.setIdHolderAccountPk(holderAccountPk);
				idHolderAccountBalance.setIdSecurityCodePk(securityCode);
				holderAccountBalance.setId(idHolderAccountBalance);
				
				/**Assignment entities*/
				Security security = new Security(securityCode);
				Participant participant = new Participant(participantPk, partMnemonic);
				HolderAccount holderAccount = new HolderAccount();
				holderAccount.setIdHolderAccountPk(holderAccountPk);
				holderAccount.setAlternateCode(alternatCode);
				holderAccount.setDescription(accountDesc);
				holderAccount.setHolder(new Holder());
				holderAccountBalance.setSecurity(security);
				holderAccountBalance.setParticipant(participant);
				holderAccountBalance.setHolderAccount(holderAccount);
				holderAccountBalance.setTotalBalance(totalBalance);
				holderAccountBalance.setAvailableBalance(availableBalance);
				
				/**Putting data on object*/
				balanceData.setHolderAccountBalance(holderAccountBalance);
				balanceData.setValuatorCode(valuatorCode);
				
				/**Setting data on list*/
				balanceDataList.add(balanceData);
			}
			return balanceDataList;
		} catch (NoResultException ex) {
			return null;
		}
	}

	/***
	 * Method to get a list of ValuatorSimulationBalanceTO by Id.
	 * @param idValuatorSimulatorPk the id valuator simulator
	 * @return a list of ValuatorSimulationBalanceTO
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ValuatorSimulationBalanceTO> getValuatorSimulationBalanceDetail(Long idValuatorSimulatorPk) throws ServiceException {
		StringBuilder selectSQL = new StringBuilder();
		StringBuilder whereSQL = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		selectSQL.append("Select new com.pradera.securities.valuator.simulator.view.ValuatorSimulationBalanceTO(	");
		selectSQL.append("	vsb.valuatorCode, s.idSecurityCodePk, p.idParticipantPk, p.mnemonic, 				");
		selectSQL.append("	ha.idHolderAccountPk, ha.accountNumber, ha.description, hab.totalBalance,			");
		selectSQL.append("	hab.availableBalance, vsb.simulationRate )											");
		
		selectSQL.append("From ValuatorSimulationBalance vsb													");
		selectSQL.append("Inner Join vsb.valuatorProcessSimulator vps											");
		selectSQL.append("Inner Join vsb.holderAccountBalance hab												");
		selectSQL.append("Inner Join hab.security s																");
		selectSQL.append("Inner Join hab.participant p															");
		selectSQL.append("Inner Join hab.holderAccount ha														");
		
		whereSQL.append("Where 1 = 1 ");
		if (idValuatorSimulatorPk != null) {
			whereSQL.append("And vps.idValuatorSimulatorPk = :idValuatorSimulatorPk ");
			parameters.put("idValuatorSimulatorPk",idValuatorSimulatorPk);
		}
		
		try {
			return findListByQueryString(selectSQL.toString().concat(whereSQL.toString()),parameters);
		} catch (NoResultException ex) {
			return null;
		}
	}

	/***
	 * Method to get a list of ValuatorSimulatorCodeTO by Id.
	 * @param idValuatorSimulatorPk the id valuator simulator
	 * @return a list of ValuatorSimulatorCodeTO
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ValuatorSimulatorCodeTO> getValuatorSimulationCodeDetail(Long idValuatorSimulatorPk) throws ServiceException {
		StringBuilder selectSQL = new StringBuilder();
		StringBuilder whereSQL = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		selectSQL.append("Select new com.pradera.securities.valuator.simulator.view.ValuatorSimulatorCodeTO(	");
		selectSQL.append("	vsc.idSimulatorCodePk, vc.idValuatorCode, vsc.averageRate )						");
		
		selectSQL.append("From ValuatorSimulatorCode vsc													");
		selectSQL.append("Inner Join vsc.valuatorProcessSimulator vps										");
		selectSQL.append("Inner Join vsc.valuatorCode vc													");
		
		whereSQL.append("Where 1 = 1 ");
		if (idValuatorSimulatorPk != null) {
			whereSQL.append("And vps.idValuatorSimulatorPk = :idValuatorSimulatorPk ");
			parameters.put("idValuatorSimulatorPk",idValuatorSimulatorPk);
		}
		
		List<ValuatorSimulatorCodeTO> listCodeTO = null;
		try {
			listCodeTO = findListByQueryString(selectSQL.toString().concat(whereSQL.toString()),parameters);
			
			if(listCodeTO != null){
				for (ValuatorSimulatorCodeTO codeTO : listCodeTO) {
					List<ValuatorSimulatorSecurityTO> listSecurityTO = getValuatorSimulationSecurityDetail(codeTO.getIdSimulatorCodePk());
					for (ValuatorSimulatorSecurityTO securityTO : listSecurityTO) {
						if(securityTO.getInterestRate() != null)
							securityTO.setInterestRate(securityTO.getInterestRate().divide(GeneralConstants.HUNDRED_VALUE_BIGDECIMAL));
					}
					codeTO.setSecurityTO(new GenericDataModel<>(listSecurityTO));
				}
			}
			
			return listCodeTO;
		} catch (NoResultException ex) {
			return null;
		}
	}

	/***
	 * Method to get a list of ValuatorSimulatorSecurityTO by Id.
	 * @param idSimulatorCodePk the id simulator code
	 * @return a list of ValuatorSimulatorSecurityTO
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ValuatorSimulatorSecurityTO> getValuatorSimulationSecurityDetail(Long idSimulatorCodePk) throws ServiceException {
		StringBuilder selectSQL = new StringBuilder();
		StringBuilder whereSQL = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		selectSQL.append("Select new com.pradera.securities.valuator.simulator.view.ValuatorSimulatorSecurityTO(	");
		selectSQL.append("	sec.idSecurityCodePk, sec.interestRate, vss.averateRate, vss.idNominalValue )		");
		
		selectSQL.append("From ValuatorSimulatorSecurity vss													");
		selectSQL.append("Inner Join vss.security sec															");
		selectSQL.append("Inner Join vss.valuatorSimulatorCode vsc												");
		
		whereSQL.append("Where 1 = 1 ");
		whereSQL.append("And sec.instrumentType = :instrumentType ");
		parameters.put("instrumentType",InstrumentType.FIXED_INCOME.getCode());
		if (idSimulatorCodePk != null) {
			whereSQL.append("And vsc.idSimulatorCodePk = :idSimulatorCodePk ");
			parameters.put("idSimulatorCodePk",idSimulatorCodePk);
		}
		
		try {
			return findListByQueryString(selectSQL.toString().concat(whereSQL.toString()),parameters);
		} catch (NoResultException ex) {
			return null;
		}
	}

	/**
	 * *
	 * Method to get a list of ValuatorSimulatorSecurityTO by Id.
	 *
	 * @param idValuatorCodePk the id valuator code
	 * @param idPartcipant the id partcipant
	 * @param securityCode the security code
	 * @param accoutsId the accouts id
	 * @return a list of ValuatorSimulatorSecurityTO
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ValuatorSimulatorSecurityTO> getValuatorSimulatorSecurity(Long idValuatorCodePk, Long idPartcipant, String securityCode, List<Long> accoutsId) throws ServiceException {
		StringBuilder selectSQL = new StringBuilder();
		StringBuilder whereSQL = new StringBuilder();
		StringBuilder groupBySQL = new StringBuilder();
		StringBuilder havingSQL = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		StringBuilder subQuerySQL = new StringBuilder();
		subQuerySQL.append("Select Distinct hab.security.idSecurityCodePk From HolderAccountBalance hab		");
		subQuerySQL.append("Where hab.participant.idParticipantPk = :idParticipantParam						");
		parameters.put("idParticipantParam", idPartcipant);
		if(securityCode!=null && !securityCode.isEmpty()){
			subQuerySQL.append("And hab.security.idSecurityCodePk = :idSecurityCodeParam					");
			parameters.put("idSecurityCodeParam", securityCode);
		}
		if(accoutsId!=null && !accoutsId.isEmpty()){
			subQuerySQL.append("And hab.holderAccount.idHolderAccountPk IN (:accountsParam)					");
			parameters.put("accountsParam", accoutsId);
		}
		
		selectSQL.append("Select new com.pradera.securities.valuator.simulator.view.ValuatorSimulatorSecurityTO(	");
		selectSQL.append("	s.idSecurityCodePk, s.interestRate )							");
		
		selectSQL.append("From ValuatorSecurityCode valSec														");
		selectSQL.append("Inner Join valSec.security s															");
		selectSQL.append("Left Join valSec.valuatorCode vc														");

		whereSQL.append("Where 1 = 1 ");
		whereSQL.append("And s.instrumentType = :instrumentType 												");
		whereSQL.append("And s.idSecurityCodePk In ( 															");
		whereSQL.append(subQuerySQL.toString());
		whereSQL.append(")																						");
		parameters.put("instrumentType",InstrumentType.FIXED_INCOME.getCode());
		if (idValuatorCodePk != null) {
			whereSQL.append("And vc.idValuatorCodePk = :idValuatorCodePk ");
			parameters.put("idValuatorCodePk", idValuatorCodePk);
		}

		groupBySQL.append(" Group By s.idSecurityCodePk, s.interestRate ");
		havingSQL.append(" Having s.idSecurityCodePk = Max(s.idSecurityCodePk)");
		try {
			return findListByQueryString(selectSQL.toString().concat(whereSQL.toString()).concat(groupBySQL.toString()).concat(havingSQL.toString()),parameters);
		} catch (NoResultException ex) {
			return null;
		}
	}

	/***
	 * Method to get a list of account Ids Long type by holder Id and participant Id.
	 * @param idHolderPk the id holder
	 * @param idParticipantPk the id participant
	 * @return a list of account Ids Long type
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Long> getAccountsID(Long idHolderPk, Long idParticipantPk) throws ServiceException{
		StringBuilder idAccountSql = new StringBuilder();
		Map<String, Object> parametersIdAccount = new HashMap<String, Object>();
		idAccountSql.append("Select distinct(ha.idHolderAccountPk)									");
		idAccountSql.append("From HolderAccount	ha											");
		idAccountSql.append("Inner Join	ha.holderAccountDetails det							");
		idAccountSql.append("Where 1 = 1 													");
		if (idParticipantPk != null) {
			idAccountSql.append("And ha.participant.idParticipantPk = :idParticipantPk ");
			parametersIdAccount.put("idParticipantPk", idParticipantPk);
		}
		if (idHolderPk != null) {
			idAccountSql.append("And det.holder.idHolderPk = :idHolderPk ");
			parametersIdAccount.put("idHolderPk", idHolderPk);
		}
		idAccountSql.append(" And ha.stateAccount in (:accountStateActive,:accountStateBlock)");
		parametersIdAccount.put("accountStateActive", HolderAccountStatusType.ACTIVE.getCode());
		parametersIdAccount.put("accountStateBlock", HolderAccountStatusType.BLOCK.getCode());
		return findListByQueryString(idAccountSql.toString(),parametersIdAccount);
	}

	/***
	 * Method to get a list of HolderAccount by holder Id and participant Id.
	 * @param idHolderPk the id holder
	 * @param idParticipantPk the id participant
	 * @return a list of HolderAccount
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccount> getAccountsHolderAndParticipant(Long idHolderPk, Long idParticipantPk) throws ServiceException{
		
		List<Long> accountsID = null;
		
		if(idHolderPk != null)
			accountsID = getAccountsID(idHolderPk, idParticipantPk);
		
		if(accountsID!=null && !accountsID.isEmpty()){
			StringBuilder stringBuilderSql = new StringBuilder();
			Map<String,Object> parameters = new HashMap<String,Object>();
			
			stringBuilderSql.append("Select Distinct account From HolderAccount	account						");
			stringBuilderSql.append("Inner Join Fetch account.holderAccountDetails detailAccount			");
			stringBuilderSql.append("Inner Join Fetch detailAccount.holder holderAcc						");
			stringBuilderSql.append("Where 1 = 1 															");
			
			if (idParticipantPk != null) {
				stringBuilderSql.append("And account.participant.idParticipantPk = :idParticipantPk ");
				parameters.put("idParticipantPk", idParticipantPk);
			}
			if (accountsID != null) {
				stringBuilderSql.append("And account.idHolderAccountPk IN :accountsID ");
				parameters.put("accountsID", accountsID);
			}
			
			return findListByQueryString(stringBuilderSql.toString(),parameters);
		}else{
			return new ArrayList<HolderAccount>();
		}
	}

	/***
	 * Method to fin the LastOperationNumber by operationType.
	 * @param operationType the operation type
	 * @return the LastOperationNumber Long type
	 * @throws ServiceException the service exception
	 */
	public Long findLastOperationNumber(Long operationType) throws ServiceException {
		
		StringBuilder selectSQL = new StringBuilder();
		StringBuilder whereSQL = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();

		selectSQL.append("select (MAX(vpo.operationNumber)+1)				");
		selectSQL.append("from ValuatorProcessOperation vpo					");
		whereSQL.append("where vpo.operationType  = :operationType 			");
		parameters.put("operationType", operationType);
		
		try {
			Long operationNumber = (Long)findObjectByQueryString(selectSQL.toString().concat(whereSQL.toString()),parameters);
			if(operationNumber == null)
				return GeneralConstants.ONE_VALUE_LONG;
			return operationNumber;
		} catch(NoResultException ex){
		   return GeneralConstants.ONE_VALUE_LONG;
		}
	}

	/***
	 * Method to execute security process class on HashMap by list of securityCodes.
	 * @param securityCodes the list of security codes
	 * @return a list of security By Class
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Integer> executeSecurityProcessClass(List<String> securityCodes) throws ServiceException{
		StringBuffer stringBuffer = new StringBuffer();
		
		stringBuffer.append("Select sec.idSecurityCodePk, sec.securityClass 			");
		stringBuffer.append("From Security sec											");
		stringBuffer.append("Left Join sec.valuatorProcessClass val						");
		stringBuffer.append("Where sec.stateSecurity IN (131, 834, 1372)				");//REGISTRADO, BLOQUEADO, SUSPENDIDO
		stringBuffer.append("And val.idValuatorClassPk IS NULL							");
		
		if (securityCodes != null && securityCodes.isEmpty()) {
			stringBuffer.append("And sec.idSecurityCodePk IN (:securityCodes)			");
		}
		
		Query query = em.createQuery(stringBuffer.toString());
		
		if (securityCodes != null && securityCodes.isEmpty()) {
			query.setParameter("securityCodes", securityCodes);
		}

		try {
			List<Object[]> dataFromBd = query.getResultList();
			Map<String, Integer> securityByClass = new HashMap<String,Integer>();
			for (Object[] data : dataFromBd) {
				securityByClass.put(data[0].toString(), Integer.valueOf(data[1].toString()));
			}
			return securityByClass;
		} catch (NoResultException ex) {
			return null;
		}
	}

	/**
	 * Update security.
	 * Method to update the valuatorProcessClass of a Security
	 * @param idValuatorClassPk the id valuator class
	 * @param securityCode the security code
	 * @throws ServiceException the service exception
	 */
	public void updateSecurity(String idValuatorClassPk, String securityCode) throws ServiceException{
		StringBuffer stringBuffer = new StringBuffer();
		
		stringBuffer.append("Update Security sec													");
		stringBuffer.append("Set sec.valuatorProcessClass.idValuatorClassPk = :idValuatorClassPk	");
		stringBuffer.append("Where sec.idSecurityCodePk = :securityCode								");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idValuatorClassPk", idValuatorClassPk);
		query.setParameter("securityCode", securityCode);
		
		query.executeUpdate();
	}
	
	/**
	 * List detail simulation.
	 *
	 * @param idProcessSimulatorPk the id process simulator pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<DetailSimulationTO> listDetailSimulation(Long idProcessSimulatorPk) throws ServiceException {
		
		
		StringBuffer stringBuffer = new StringBuffer();
		List<DetailSimulationTO> lista  = new ArrayList<DetailSimulationTO>();
		DetailSimulationTO detailSimulationTO = null;
		
		stringBuffer.append("Select vsb	");
		stringBuffer.append("From ValuatorSimulationBalance vsb ");
		stringBuffer.append("join fetch vsb.holderAccountBalance hab ");
		stringBuffer.append("join fetch hab.security s ");
		stringBuffer.append("Where vsb.valuatorProcessSimulator.valuatorProcessOperation.idValuatorOperationPk = :idValuatorProcess ");

		Query query = em.createQuery(stringBuffer.toString());
		
		query.setParameter("idValuatorProcess", idProcessSimulatorPk);
		
		List<ValuatorSimulationBalance> listValuatorSimulation = query.getResultList(); 
		for (ValuatorSimulationBalance balance : listValuatorSimulation){
			detailSimulationTO = new DetailSimulationTO();
			detailSimulationTO.setMarketRate(balance.getSimulationRate());
			
			if (balance.getSimulationPrice()!=null){
				detailSimulationTO.setMarketPrice(balance.getSimulationPrice());
				
				detailSimulationTO.setTotalValorized(balance.getSimulationPrice().multiply(balance.getTotalBalance()));
				detailSimulationTO.setTotalAvailable(balance.getSimulationPrice().multiply(balance.getAvailableBalance()));
			}else{
				detailSimulationTO.setMarketPrice(new BigDecimal("0"));
				detailSimulationTO.setTotalValorized(detailSimulationTO.getMarketPrice().multiply(balance.getTotalBalance()));
				detailSimulationTO.setTotalAvailable(detailSimulationTO.getMarketPrice().multiply(balance.getAvailableBalance()));
			}
			
			detailSimulationTO.setTotalBalance(balance.getTotalBalance());
			detailSimulationTO.setAvailableAvailable(balance.getAvailableBalance());
			
			detailSimulationTO.setSecurityCode(balance.getHolderAccountBalance().getSecurity().getIdSecurityCodePk());
			lista.add(detailSimulationTO);
			
		}
		return lista;	
	}
}