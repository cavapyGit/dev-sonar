package com.pradera.securities.valuator.core.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;

import com.pradera.commons.configuration.ValuatorSetup;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.valuatorprocess.ValuatorProcessExecution;
import com.pradera.model.valuatorprocess.ValuatorProcessSetup;
import com.pradera.securities.valuator.core.facade.ValuatorProcessValidation;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorProcessHistory.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@Stateless
public class ValuatorProcessHistory extends CrudDaoServiceBean {

	/** The valuator setup. */
	@Inject @ValuatorSetup ValuatorProcessSetup valuatorSetup;
	
	/**
	 * Process history.
	 *
	 * @param listParticipants the list participants
	 * @param listHolderAccount the list holder account
	 * @param listSecuritys the list securitys
	 * @param execution the execution
	 * @throws ServiceException the service exception
	 */
	public void processHistory(List<Long> listParticipants, List<Long> listHolderAccount, List<String> listSecuritys,ValuatorProcessExecution execution) throws ServiceException{
		
		
		for(List<String> listSecurity: ValuatorProcessValidation.getStringByMaxRow(listSecuritys)){
			/**GET SECURITY AND RATES WITH THE PROCESS EXECUTION ID*/
			Long processExecution = execution.getIdProcessExecutionPk();
			execution = find(ValuatorProcessExecution.class, processExecution);
			
			/**IF HAVE HISTORICAL, WE MUST INSERT AND SAVE*/
			if(valuatorSetup.getIndHistory().equals(BooleanType.YES.getCode())){
				StringBuilder sbQuery= new StringBuilder();
				sbQuery.append(" insert into holder_marketfact_history 																										");
				sbQuery.append(" (ID_MARKETFACT_HISTORY_PK,ID_HOLDER_ACCOUNT_FK,ID_SECURITY_CODE_FK,ID_PARTICIPANT_FK,MARKET_DATE,MARKET_RATE,MARKET_PRICE,VALUATOR_AMOUNT,	");
				sbQuery.append(" 	VALUATOR_AMOUNT_OHER,TOTAL_BALANCE,REGISTRY_USER,REGISTRY_DATE,																			");
				sbQuery.append(" 	LAST_MODIFY_USER,LAST_MODIFY_DATE,LAST_MODIFY_IP,LAST_MODIFY_APP,AVAILABLE_BALANCE,ID_PROCES_EXECUTION_FK,THEORIC_PRICE)				");
				sbQuery.append(" (select SQ_ID_VALUATOR_MARKFAC_HYS_PK.NEXTVAL,ID_HOLDER_ACCOUNT_FK,ID_SECURITY_CODE_FK,ID_PARTICIPANT_FK,									");
				sbQuery.append(" 	MARKET_DATE,MARKET_RATE,MARKET_PRICE, NVL((MARKET_PRICE*TOTAL_BALANCE),0),0,TOTAL_BALANCE, 												");
				sbQuery.append(" 'ADMIN', :registryDate, 																");
				sbQuery.append(" 'ADMIN', :lastModificationDate, 														");
				sbQuery.append(" LAST_MODIFY_IP, LAST_MODIFY_APP, 														");
				sbQuery.append(" AVAILABLE_BALANCE, :processExecutionPk,MARKET_PRICE									");
				sbQuery.append(" from holder_marketfact_balance 														");
				sbQuery.append(" where IND_ACTIVE=1 AND (TOTAL_BALANCE > :oneParam or REPORTED_BALANCE > :oneParam) 	");
				
				if (listParticipants!=null && listParticipants.size()>0){
					sbQuery.append(" AND ID_PARTICIPANT_FK IN(:participantList)											");
				}
				if (listHolderAccount!=null && listHolderAccount.size()>0){
					sbQuery.append(" AND ID_HOLDER_ACCOUNT_FK IN(:holderAccountList))									");
				}
				if (listSecurity!=null && listSecurity.size()>0){
					sbQuery.append(" AND ID_SECURITY_CODE_FK IN(:securityList)											");
				}
				sbQuery.append("									)													");
				
				Query query = em.createNativeQuery(sbQuery.toString());
				query.setParameter("registryDate", execution.getProcessDate());
				query.setParameter("lastModificationDate", execution.getLastModifyDate());
				query.setParameter("processExecutionPk", processExecution);
				query.setParameter("oneParam", BooleanType.NO.getCode());
				
				if (listParticipants!=null && listParticipants.size()>0){
					query.setParameter("participantList", listParticipants);
				}
				if (listHolderAccount!=null && listHolderAccount.size()>0){
					query.setParameter("holderAccountList", listHolderAccount);
				}
				if (listSecurity!=null && listSecurity.size()>0){
					query.setParameter("securityList", listSecurity);
				}
				query.executeUpdate();
				
				/**PROCESS TO SAVE THEORIC PRICE*/
//				StringBuilder queryPrice = new StringBuilder();
//				queryPrice.append("Select sec.idSecurityCodePk ,pric.marketRate, pric.marketPrice 		");
//				queryPrice.append("From ValuatorPriceResult pric										");
//				queryPrice.append("Inner Join pric.security sec											");
//				queryPrice.append("Inner Join pric.valuatorProcessExecution exec						");
//				queryPrice.append("Where exec.idProcessExecutionPk = :executorPk						");
//				queryPrice.append("And	 pric.indHomologated = :zeroParam								");
//				
//				/**Query to get data from query*/
//				Query queryPric = em.createQuery(queryPrice.toString());
//				queryPric.setParameter("executorPk", processExecution);
//				queryPric.setParameter("zeroParam", BooleanType.NO.getCode());
//				
//				/**Get query and then we process this data*/
//				List<Object[]> lstObjectData = queryPric.getResultList();
//				for(Object[] data: lstObjectData){
//					StringBuilder queryUpdate = new StringBuilder();
//					queryUpdate.append("Update HOLDER_MARKETFACT_HISTORY  											");
//					queryUpdate.append("Set	THEORIC_PRICE = :theoricPrice		 									");
//					queryUpdate.append("Where ID_SECURITY_CODE_FK = :securityCode									");
//					queryUpdate.append("And	  MARKET_RATE = :marketRate												");
//					
//					Query queryToUpdate = em.createNativeQuery(queryUpdate.toString());
//					queryToUpdate.setParameter("securityCode", data[0]);
//					queryToUpdate.setParameter("marketRate", data[1]);
//					queryToUpdate.setParameter("theoricPrice", data[2]);				
//					queryToUpdate.executeUpdate();
//				}
//				
//				/**Update theoric prices in historic homologate*/
//				Map<String, Map<Object, BigDecimal>> dataRate = valuatorSetup.getTheoricMarketPriceMap();
//				if(dataRate!=null && !dataRate.isEmpty()){
//					for(String securityCode: dataRate.keySet()){
//						for(Object rate: dataRate.get(securityCode).keySet()){
//							StringBuilder queryUpdateHomologate = new StringBuilder();
//							queryUpdateHomologate.append("Update HOLDER_MARKETFACT_HISTORY  							");
//							queryUpdateHomologate.append("Set	THEORIC_PRICE = :theoricPrice		 					");
//							queryUpdateHomologate.append("Where ID_SECURITY_CODE_FK = :securityCode						");
//							queryUpdateHomologate.append("And	  THEORIC_PRICE is null									");
//							
//							Query queryToUpdateHomolog = em.createNativeQuery(queryUpdateHomologate.toString());
//							queryToUpdateHomolog.setParameter("theoricPrice", dataRate.get(securityCode).get(rate));
//							queryToUpdateHomolog.setParameter("securityCode", securityCode);
//							queryToUpdateHomolog.executeUpdate();
//						}
//					}
//				}
			}	
		}
	}
	
	/**
	 * Instantiates a new valuator process history.
	 */
	public ValuatorProcessHistory() {
		// TODO Auto-generated constructor stub
	}
}