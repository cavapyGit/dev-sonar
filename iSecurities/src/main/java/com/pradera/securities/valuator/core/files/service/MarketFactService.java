package com.pradera.securities.valuator.core.files.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.commons.configuration.ValuatorSetup;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.valuatorprocess.ValuatorMarketfactConsolidat;
import com.pradera.model.valuatorprocess.ValuatorMarketfactFile;
import com.pradera.model.valuatorprocess.ValuatorMarketfactHistory;
import com.pradera.model.valuatorprocess.ValuatorMarketfactMechanism;
import com.pradera.model.valuatorprocess.ValuatorProcessSetup;
import com.pradera.model.valuatorprocess.type.ValuatorMarkFactActiveType;
import com.pradera.model.valuatorprocess.type.ValuatorMarkFactFileType;
import com.pradera.securities.valuator.core.files.to.ValuatorMarketfactConsolidatTo;
import com.pradera.securities.valuator.core.files.to.ValuatorMarketfactFileTO;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MarketFactService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@Stateless
public class MarketFactService extends CrudDaoServiceBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** Inject Valuator Setup to handle consolidate file on memory. */
	@Inject @ValuatorSetup ValuatorProcessSetup valuatorProcessSetup;

	/**
	 * Search valuator marketfact file service.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ValuatorMarketfactFileTO> searchValuatorMarketfactFileService(ValuatorMarketfactFileTO filter)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select new com.pradera.securities.valuator.core.files.to.ValuatorMarketfactFileTO("+
					"	vmf.idValuatorFilePk,"+
					"	vmf.fileName,"+
					"	vmf.fileState,"+
					"	vmf.processType,"+
					"	vmf.marketfactFile,"+
					"	vmf.marketfactType,"+
					"	vmf.registryDate,"+
					"	vmf.rowQuantity,"+
					"	vmf.fileSource)"+
					"	from ValuatorMarketfactFile vmf"+
					"	where 1=1");
		if(Validations.validateIsNotNullAndNotEmpty(filter.getMarketfactType())){
			sbQuery.append(" And vmf.marketfactType= :marketfactType");
			parameters.put("marketfactType", filter.getMarketfactType());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getProcessType())){
			sbQuery.append(" And vmf.processType= :processType");
			parameters.put("processType", filter.getProcessType());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getFileState())){
			sbQuery.append(" And vmf.fileState= :fileState");
			parameters.put("fileState", filter.getFileState());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getFileSource())){
			sbQuery.append(" And vmf.fileSource= :fileSource");
			parameters.put("fileSource", filter.getFileSource());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getProcessDate())){
			sbQuery.append(" And vmf.processDate= :processDate");
			parameters.put("processDate", filter.getProcessDate());
		}
		if(Validations.validateIsNotNull(filter.getInitalDate())&& 
			Validations.validateIsNotNull(filter.getEndDate())){
			sbQuery.append(" And  TRUNC(vmf.registryDate) BETWEEN :initialDate And :finalDate");
			parameters.put("initialDate",filter.getInitalDate());
			parameters.put("finalDate",filter.getEndDate());
		}
		sbQuery.append(" order by vmf.idValuatorFilePk");
		try {
			return findListByQueryString(sbQuery.toString(),parameters);
		} catch (NoResultException ex) {
			return null;
		}
	}
	
	/**
	 * Replace holidays file.
	 *
	 * @param processDate the process date
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void replaceHolidaysFile(Date processDate, LoggerUser loggerUser) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("update ValuatorMarketfactFile vmf"+
				"	SET vmf.fileState = :fileState"+
				" , vmf.lastModifyApp = :lastModifyApp "+
				" , vmf.lastModifyDate = :lastModifyDate "+
				" , vmf.lastModifyIp = :lastModifyIp "+
				" , vmf.lastModifyUser = :lastModifyUser "+				
				"	where vmf.fileState = :fileStateOld And trunc(vmf.processDate) = trunc(:processDate)	");
		Query query = this.em.createQuery(sbQuery.toString());
		query.setParameter("fileState", ValuatorMarkFactFileType.REPLACE.getCode());
		query.setParameter("fileStateOld", ValuatorMarkFactFileType.PENDING.getCode());
		query.setParameter("processDate", processDate);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		query.executeUpdate();
	}
	
	/**
	 * Save marketfact files service.
	 *
	 * @param lstMarketfactFiles the lst marketfact files
	 * @throws ServiceException the service exception
	 */
	public void saveMarketfactFilesService(List<ValuatorMarketfactFile> lstMarketfactFiles)throws ServiceException{
		for(ValuatorMarketfactFile obj : lstMarketfactFiles){
			create(obj);
		}
	}
	
	/**
	 * Change file state service.
	 *
	 * @param filter the filter
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void changeFileStateService(ValuatorMarketfactFileTO filter,LoggerUser loggerUser)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("update ValuatorMarketfactFile vmf"+
				"	SET vmf.fileState = :fileState"+
				" , vmf.lastModifyApp = :lastModifyApp "+
				" , vmf.lastModifyDate = :lastModifyDate "+
				" , vmf.lastModifyIp = :lastModifyIp "+
				" , vmf.lastModifyUser = :lastModifyUser "+				
				"	where vmf.idValuatorFilePk = :idValuatorFilePk");
		Query query = this.em.createQuery(sbQuery.toString());
		query.setParameter("fileState", filter.getFileState());
		query.setParameter("idValuatorFilePk", filter.getIdValuatorFilePk());
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		query.executeUpdate();
	}
	
	/**
	 * Search valuator marketfact mechanism service.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ValuatorMarketfactMechanism> searchValuatorMarketfactMechanismService()throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select vmfm from ValuatorMarketfactMechanism  vmfm order by vmfm.idValuatorMechanismPk");
		return findListByQueryString(sbQuery.toString());	
	}
	
	/**
	 * Search valuator marketfact mechanism service.
	 *
	 * @param marketfactType the marketfact type
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ValuatorMarketfactMechanism> searchValuatorMarketfactMechanismService(Integer marketfactType, Date processDate)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select vmfm from  ValuatorMarketfactMechanism vmfm ");
		sbQuery.append(" where vmfm.marketfactType = :marketfactType ");
		sbQuery.append(" and   trunc(vmfm.informationDate) = trunc(:informationDate)	");
		sbQuery.append(" order by vmfm.idValuatorMechanismPk" );
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("marketfactType", marketfactType);
		query.setParameter("informationDate", processDate);
		return (List<ValuatorMarketfactMechanism>)query.getResultList();
	}
	
	/**
	 * Search valuator marketfact mechanism service no equals.
	 *
	 * @param marketfactType the marketfact type
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ValuatorMarketfactMechanism> searchValuatorMarketfactMechanismServiceNoEquals(Integer marketfactType)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select vmfm from  ValuatorMarketfactMechanism vmfm ");
		sbQuery.append(" where vmfm.marketfactType <> :marketfactType ");
		sbQuery.append(" order by vmfm.idValuatorMechanismPk" );
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("marketfactType", marketfactType);
		return (List<ValuatorMarketfactMechanism>)query.getResultList();
	}
	
	/**
	 * Save market fact history service.
	 *
	 * @param lstMarketFactHistory the lst market fact history
	 */
	public void saveMarketFactHistoryService(List<ValuatorMarketfactHistory> lstMarketFactHistory){
		for(ValuatorMarketfactHistory obj : lstMarketFactHistory){
			create(obj);
		}
	}
	
	/**
	 * Exist market fact mechanism today service.
	 *
	 * @param filter the filter
	 * @return the boolean
	 * @throws ServiceException the service exception
	 */
	public Boolean existMarketFactMechanismTodayService(ValuatorMarketfactFileTO filter)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select count(*) from ValuatorMarketfactMechanism "+
				" where TRUNC(informationDate) = :registryDate ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("registryDate", filter.getProcessDate());
		Long count= (Long) query.getSingleResult();
		if(count > GeneralConstants.ZERO_VALUE_LONG)
			return Boolean.TRUE;
		else return Boolean.FALSE;
	}
	
	
	/**
	 * Exist market fact mechanism before service.
	 *
	 * @param filter the filter
	 * @return the boolean
	 * @throws ServiceException the service exception
	 */
	public Boolean existMarketFactMechanismBeforeService(ValuatorMarketfactFileTO filter)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select count(*) from ValuatorMarketfactMechanism "+
				" where TRUNC(informationDate) < :registryDate ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("registryDate", filter.getProcessDate());
		Long count= (Long) query.getSingleResult();
		if(count.equals(GeneralConstants.ZERO_VALUE_LONG))
			return Boolean.FALSE;
		else return Boolean.TRUE;
	}
	
	/**
	 * Exist marketfact consolidat service.
	 *
	 * @return the boolean
	 * @throws ServiceException the service exception
	 */
	public Boolean existMarketfactConsolidatService()throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select count(*) from ValuatorMarketfactConsolidat ");
		Query query = em.createQuery(sbQuery.toString());
		Long count= (Long) query.getSingleResult();
		if(count > GeneralConstants.ZERO_VALUE_LONG)
			return Boolean.TRUE;
		else return Boolean.FALSE;
	}
	
	/**
	 * Delete all market fact file mechanism service.
	 *
	 * @throws ServiceException the service exception
	 */
	public void deleteAllMarketFactFileMechanismService()throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("DELETE FROM ValuatorMarketfactMechanism");
		Query query = this.em.createQuery(sbQuery.toString());
		query.executeUpdate();
	}
	
	/**
	 * Delete all market fact consolidat service.
	 *
	 * @throws ServiceException the service exception
	 */
	public void deleteAllMarketFactConsolidatService()throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("DELETE FROM ValuatorMarketfactConsolidat");
		Query query = this.em.createQuery(sbQuery.toString());
		query.executeUpdate();
	}
	
	/**
	 * Delete market fact file mechanism service.
	 *
	 * @param filter the filter
	 * @throws ServiceException the service exception
	 */
	public void deleteMarketFactFileMechanismService(ValuatorMarketfactFileTO filter)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("DELETE FROM ValuatorMarketfactMechanism vmfm"+
					" where vmfm.valuatorMarketfactFile.idValuatorFilePk = :idValuatorFilePk");
		Query query = this.em.createQuery(sbQuery.toString());
		query.setParameter("idValuatorFilePk", filter.getIdValuatorFilePk());
		query.executeUpdate();
	}
	
	/**
	 * Save valuator market fact mechanism service.
	 *
	 * @param lstMechanism the lst mechanism
	 * @throws ServiceException the service exception
	 */
	public void saveValuatorMarketFactMechanismService(List<ValuatorMarketfactMechanism> lstMechanism)throws ServiceException{ 
		for(ValuatorMarketfactMechanism obj : lstMechanism){
			create(obj);
		}
	}
	
	/**
	 * Save valuator marketfact consolidat.
	 *
	 * @param lstConsolidat the lst consolidat
	 * @throws ServiceException the service exception
	 */
	public void saveValuatorMarketfactConsolidat(List<ValuatorMarketfactConsolidat> lstConsolidat)throws ServiceException{
		ConcurrentHashMap<String, List<ValuatorMarketfactConsolidat>> marketFactByValCode = new ConcurrentHashMap<String, List<ValuatorMarketfactConsolidat>>();
		for(ValuatorMarketfactConsolidat obj : lstConsolidat){
			create(obj);
			
			for(ValuatorMarketfactMechanism marketFactMechanism: obj.getValuatorMarketfactMechanisms()){
				marketFactMechanism.setValuatorMarketfactConsolidat(obj);
				update(marketFactMechanism);
			}
			
			if(obj.getMarketfactActive() == null || obj.getMarketfactActive().equals(ValuatorMarkFactActiveType.AC.getCode())){
				String valuatorCode = obj.getValuatorCode();
				if(marketFactByValCode.get(valuatorCode)==null){
					marketFactByValCode.put(valuatorCode, new ArrayList<ValuatorMarketfactConsolidat>());
				}
				marketFactByValCode.get(valuatorCode).add(obj);
			}
		}		
		valuatorProcessSetup.setMarketFactConsolidat(marketFactByValCode);
	}
	
	/**
	 * Gets the consolidate to mechanism service.
	 *
	 * @param marketfactType the marketfact type
	 * @return the consolidate to mechanism service
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ValuatorMarketfactConsolidatTo>  getConsolidateToMechanismService(Integer marketfactType, Date processDate)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT new com.pradera.securities.valuator.core.files.to.ValuatorMarketfactConsolidatTo( ");
		sbQuery.append("	avg(nvl(vmfm.averagePrice,0)),");
		sbQuery.append("	avg(nvl(vmfm.averageRate,0)),");
		sbQuery.append("	avg(nvl(vmfm.negotiationAmount,0)),");
		sbQuery.append("	max(vmfm.informationDate),");
		sbQuery.append("	max(vmfm.marketfactDate),"); 
		sbQuery.append("	max(vmfm.minimunAmount),");
		sbQuery.append("	max(vmfm.valuatorType),");
		sbQuery.append("	vmfm.marketfactActive,");
		sbQuery.append("	vmfm.securityClass,");
		sbQuery.append("	vmfm.securityCode,");
		sbQuery.append("	vmfm.valuatorCode)");
		sbQuery.append("	FROM ValuatorMarketfactMechanism vmfm");
		sbQuery.append("	WHERE vmfm.marketfactType = :marketfactType");
		sbQuery.append("	And trunc(vmfm.informationDate) = trunc(:processDate)");
		sbQuery.append("	GROUP BY ");
		sbQuery.append("	vmfm.marketfactActive,");
		sbQuery.append("	vmfm.securityClass,");
		sbQuery.append("	vmfm.securityCode,");
		sbQuery.append("	vmfm.valuatorCode");	
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("marketfactType", marketfactType);	
		query.setParameter("processDate", processDate);
		return (List<ValuatorMarketfactConsolidatTo>)query.getResultList();
	}
	
//	@SuppressWarnings("unchecked")
//	public List<ValuatorMarketfactConsolidat> searchValuatorMarketfactConsolidat()throws ServiceException{
//		StringBuilder sbQuery = new StringBuilder();
//		sbQuery.append("select vmfc from ValuatorMarketfactConsolidat  vmfc order by vmfc.idValuatorConsolidatPk");
//		return findListByQueryString(sbQuery.toString());	
//	}
	
//	public void addConsolidatToMechanism(List<ValuatorMarketfactMechanism> lstMechanism)throws ServiceException{
//		updateList(lstMechanism);
//	}
	
}
