package com.pradera.securities.valuator.core.files.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.pradera.integration.common.validation.Validations;
import com.pradera.model.valuatorprocess.ValuatorMarketfactMechanism;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorMarketfactMechanismType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class ValuatorMarketfactMechanismType implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The v marketfact mechanism. */
	private ValuatorMarketfactMechanism vMarketfactMechanism;
	
	/** The id valuator mechanism pk. */
	private Long idValuatorMechanismPk;
	
	/** The average price. */
	private BigDecimal averagePrice;			
	
	/** The average rate. */
	private BigDecimal averageRate;
	
	/** The information date. */
	private Date informationDate;
	
	/** The marketfact date. */
	private Date marketfactDate;
	
	/** The marketfact active. */
	private String marketfactActive;
	
	/** The marketfact type. */
	private Integer marketfactType;
	
	/** The mechanism code. */
	private String mechanismCode;
	
	/** The minimun amount. */
	private BigDecimal minimunAmount;
	
	/** The negotiation amount. */
	private BigDecimal negotiationAmount;
	
	/** The registry date. */
	private Date registryDate;
	
	/** The registry user. */
	private String registryUser;
	
	/** The security class. */
	private String securityClass;
	
	/** The security code. */
	private String securityCode;
	
	/** The valuator code. */
	private String valuatorCode;
	
	/** The valuator type. */
	private String valuatorType;
	
	
	/**
	 * Gets the v marketfact mechanism.
	 *
	 * @return the v marketfact mechanism
	 */
	public ValuatorMarketfactMechanism getvMarketfactMechanism() {
		vMarketfactMechanism= new ValuatorMarketfactMechanism();
		vMarketfactMechanism.setAveragePrice(averagePrice);
		vMarketfactMechanism.setAverageRate(averageRate);
		vMarketfactMechanism.setInformationDate(informationDate);		
		vMarketfactMechanism.setMarketfactActive(marketfactActive);
		vMarketfactMechanism.setMarketfactType(marketfactType);
		vMarketfactMechanism.setMechanismCode(mechanismCode);
		vMarketfactMechanism.setMinimunAmount(minimunAmount);
		vMarketfactMechanism.setNegotiationAmount(negotiationAmount);
		vMarketfactMechanism.setSecurityClass(securityClass);
		vMarketfactMechanism.setSecurityCode(securityCode);
		vMarketfactMechanism.setValuatorCode(valuatorCode);
		vMarketfactMechanism.setValuatorType(valuatorType);
		vMarketfactMechanism.setMarketfactDate(marketfactDate);
		if(Validations.validateIsNull(marketfactDate))
			vMarketfactMechanism.setMarketfactDate(informationDate);
		
		return vMarketfactMechanism;
	}
	
	/**
	 * Sets the v marketfact mechanism.
	 *
	 * @param vMarketfactMechanism the new v marketfact mechanism
	 */
	public void setvMarketfactMechanism(
			ValuatorMarketfactMechanism vMarketfactMechanism) {
		this.vMarketfactMechanism = vMarketfactMechanism;
	}
	
	/**
	 * Gets the id valuator mechanism pk.
	 *
	 * @return the id valuator mechanism pk
	 */
	public Long getIdValuatorMechanismPk() {
		return idValuatorMechanismPk;
	}
	
	/**
	 * Sets the id valuator mechanism pk.
	 *
	 * @param idValuatorMechanismPk the new id valuator mechanism pk
	 */
	public void setIdValuatorMechanismPk(Long idValuatorMechanismPk) {
		this.idValuatorMechanismPk = idValuatorMechanismPk;
	}
	
	/**
	 * Gets the average price.
	 *
	 * @return the average price
	 */
	public BigDecimal getAveragePrice() {
		return averagePrice;
	}
	
	/**
	 * Sets the average price.
	 *
	 * @param averagePrice the new average price
	 */
	public void setAveragePrice(BigDecimal averagePrice) {
		this.averagePrice = averagePrice;
	}
	
	/**
	 * Gets the average rate.
	 *
	 * @return the average rate
	 */
	public BigDecimal getAverageRate() {
		return averageRate;
	}
	
	/**
	 * Sets the average rate.
	 *
	 * @param averageRate the new average rate
	 */
	public void setAverageRate(BigDecimal averageRate) {
		this.averageRate = averageRate;
	}
	
	/**
	 * Gets the information date.
	 *
	 * @return the information date
	 */
	public Date getInformationDate() {
		return informationDate;
	}
	
	/**
	 * Sets the information date.
	 *
	 * @param informationDate the new information date
	 */
	public void setInformationDate(Date informationDate) {
		this.informationDate = informationDate;
	}
	
	/**
	 * Gets the marketfact active.
	 *
	 * @return the marketfact active
	 */
	public String getMarketfactActive() {
		return marketfactActive;
	}
	
	/**
	 * Sets the marketfact active.
	 *
	 * @param marketfactActive the new marketfact active
	 */
	public void setMarketfactActive(String marketfactActive) {
		this.marketfactActive = marketfactActive;
	}
	
	/**
	 * Gets the marketfact type.
	 *
	 * @return the marketfact type
	 */
	public Integer getMarketfactType() {
		return marketfactType;
	}
	
	/**
	 * Sets the marketfact type.
	 *
	 * @param marketfactType the new marketfact type
	 */
	public void setMarketfactType(Integer marketfactType) {
		this.marketfactType = marketfactType;
	}
	
	/**
	 * Gets the mechanism code.
	 *
	 * @return the mechanism code
	 */
	public String getMechanismCode() {
		return mechanismCode;
	}
	
	/**
	 * Sets the mechanism code.
	 *
	 * @param mechanismCode the new mechanism code
	 */
	public void setMechanismCode(String mechanismCode) {
		this.mechanismCode = mechanismCode;
	}
	
	/**
	 * Gets the minimun amount.
	 *
	 * @return the minimun amount
	 */
	public BigDecimal getMinimunAmount() {
		return minimunAmount;
	}
	
	/**
	 * Sets the minimun amount.
	 *
	 * @param minimunAmount the new minimun amount
	 */
	public void setMinimunAmount(BigDecimal minimunAmount) {
		this.minimunAmount = minimunAmount;
	}
	
	/**
	 * Gets the negotiation amount.
	 *
	 * @return the negotiation amount
	 */
	public BigDecimal getNegotiationAmount() {
		return negotiationAmount;
	}
	
	/**
	 * Sets the negotiation amount.
	 *
	 * @param negotiationAmount the new negotiation amount
	 */
	public void setNegotiationAmount(BigDecimal negotiationAmount) {
		this.negotiationAmount = negotiationAmount;
	}
	
	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}
	
	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}
	
	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}
	
	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}
	
	/**
	 * Gets the security class.
	 *
	 * @return the security class
	 */
	public String getSecurityClass() {
		return securityClass;
	}
	
	/**
	 * Sets the security class.
	 *
	 * @param securityClass the new security class
	 */
	public void setSecurityClass(String securityClass) {
		this.securityClass = securityClass;
	}
	
	/**
	 * Gets the security code.
	 *
	 * @return the security code
	 */
	public String getSecurityCode() {
		return securityCode;
	}
	
	/**
	 * Sets the security code.
	 *
	 * @param securityCode the new security code
	 */
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
	
	/**
	 * Gets the valuator type.
	 *
	 * @return the valuator type
	 */
	public String getValuatorType() {
		return valuatorType;
	}
	
	/**
	 * Sets the valuator type.
	 *
	 * @param valuatorType the new valuator type
	 */
	public void setValuatorType(String valuatorType) {
		this.valuatorType = valuatorType;
	}
	
	/**
	 * Gets the marketfact date.
	 *
	 * @return the marketfact date
	 */
	public Date getMarketfactDate() {
		return marketfactDate;
	}
	
	/**
	 * Sets the marketfact date.
	 *
	 * @param marketfactDate the new marketfact date
	 */
	public void setMarketfactDate(Date marketfactDate) {
		this.marketfactDate = marketfactDate;
	}
	
	/**
	 * Gets the valuator code.
	 *
	 * @return the valuator code
	 */
	public String getValuatorCode() {
		return valuatorCode;
	}
	
	/**
	 * Sets the valuator code.
	 *
	 * @param valuatorCode the new valuator code
	 */
	public void setValuatorCode(String valuatorCode) {
		this.valuatorCode = valuatorCode;
	}
	
}
