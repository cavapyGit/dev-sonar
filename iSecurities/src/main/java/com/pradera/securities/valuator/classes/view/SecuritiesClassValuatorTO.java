package com.pradera.securities.valuator.classes.view;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.valuatorprocess.SecurityClassValuator;
import com.pradera.model.valuatorprocess.ValuatorProcessClass;
import com.pradera.securities.valuator.classes.to.SecurityClassValuatorResultTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SecuritiesClassValuatorTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class SecuritiesClassValuatorTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	/** The register date. */
	private Date registerDate;
	
	/** The lst valor class. */
	private List<ParameterTable> lstValorClass;
	
	/** The lst intrument. */
	private List<ParameterTable> lstIntrument;
	
	/** The lst formulas. */
	private List<ParameterTable> lstFormulas;
	
	/** The lst currency. */
	private List<ParameterTable> lstCurrency;
	
	/** The lst valuator process class. */
	private GenericDataModel<ValuatorProcessClass> lstValuatorProcessClass;
	
	/** The lst data model security class valuator. */
	private GenericDataModel<SecurityClassValuator> lstDataModelSecurityClassValuator;
	
	/** The lst dm security class valuator result. */
	private GenericDataModel<SecurityClassValuatorResultTO> lstDMSecurityClassValuatorResult;
	
	/** The valuator process class selected. */
	private ValuatorProcessClass valuatorProcessClassSelected;
	
	/** The security class valuator selected. */
	private SecurityClassValuator securityClassValuatorSelected;
	
	/** The valuator class pk. */
	private Integer valuatorClassPk;
	
	/** The Intrument. */
	private Integer Intrument;
	
	/** The security to selected. */
	private SecurityClassValuatorResultTO securityToSelected;

/** The security class valuator result. */
//	private Integer formulaTypePk;
	private SecurityClassValuatorResultTO securityClassValuatorResult;
	
	
	/**
	 * Instantiates a new securities class valuator to.
	 */
	public SecuritiesClassValuatorTO() {
		super();
		// TODO Auto-generated constructor stub
	}
		
	/**
	 * Gets the register date.
	 *
	 * @return the register date
	 */
	public Date getRegisterDate() {
		return registerDate;
	}

	/**
	 * Sets the register date.
	 *
	 * @param registerDate the new register date
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	/**
	 * Gets the valuator process class selected.
	 *
	 * @return the valuator process class selected
	 */
	public ValuatorProcessClass getValuatorProcessClassSelected() {
		return valuatorProcessClassSelected;
	}

	/**
	 * Sets the valuator process class selected.
	 *
	 * @param valuatorProcessClassSelected the new valuator process class selected
	 */
	public void setValuatorProcessClassSelected(
			ValuatorProcessClass valuatorProcessClassSelected) {
		this.valuatorProcessClassSelected = valuatorProcessClassSelected;
	}

	/**
	 * Gets the lst valor class.
	 *
	 * @return the lst valor class
	 */
	public List<ParameterTable> getLstValorClass() {
		return lstValorClass;
	}

	/**
	 * Sets the lst valor class.
	 *
	 * @param lstValorClass the new lst valor class
	 */
	public void setLstValorClass(List<ParameterTable> lstValorClass) {
		this.lstValorClass = lstValorClass;
	}

	/**
	 * Gets the lst formulas.
	 *
	 * @return the lst formulas
	 */
	public List<ParameterTable> getLstFormulas() {
		return lstFormulas;
	}

	/**
	 * Sets the lst formulas.
	 *
	 * @param lstFormulas the new lst formulas
	 */
	public void setLstFormulas(List<ParameterTable> lstFormulas) {
		this.lstFormulas = lstFormulas;
	}		
	
	/**
	 * Gets the lst valuator process class.
	 *
	 * @return the lst valuator process class
	 */
	public GenericDataModel<ValuatorProcessClass> getLstValuatorProcessClass() {
		return lstValuatorProcessClass;
	}

	/**
	 * Sets the lst valuator process class.
	 *
	 * @param lstValuatorProcessClass the new lst valuator process class
	 */
	public void setLstValuatorProcessClass(
			GenericDataModel<ValuatorProcessClass> lstValuatorProcessClass) {
		this.lstValuatorProcessClass = lstValuatorProcessClass;
	}

	/**
	 * Gets the valuator class pk.
	 *
	 * @return the valuator class pk
	 */
	public Integer getValuatorClassPk() {
		return valuatorClassPk;
	}

	/**
	 * Sets the valuator class pk.
	 *
	 * @param valuatorClassPk the new valuator class pk
	 */
	public void setValuatorClassPk(Integer valuatorClassPk) {
		this.valuatorClassPk = valuatorClassPk;
	}

	/**
	 * Gets the security class valuator selected.
	 *
	 * @return the security class valuator selected
	 */
	public SecurityClassValuator getSecurityClassValuatorSelected() {
		return securityClassValuatorSelected;
	}

	/**
	 * Sets the security class valuator selected.
	 *
	 * @param securityClassValuatorSelected the new security class valuator selected
	 */
	public void setSecurityClassValuatorSelected(
			SecurityClassValuator securityClassValuatorSelected) {
		this.securityClassValuatorSelected = securityClassValuatorSelected;
	}

	/**
	 * Gets the lst data model security class valuator.
	 *
	 * @return the lst data model security class valuator
	 */
	public GenericDataModel<SecurityClassValuator> getLstDataModelSecurityClassValuator() {
		return lstDataModelSecurityClassValuator;
	}

	/**
	 * Sets the lst data model security class valuator.
	 *
	 * @param lstDataModelSecurityClassValuator the new lst data model security class valuator
	 */
	public void setLstDataModelSecurityClassValuator(
			GenericDataModel<SecurityClassValuator> lstDataModelSecurityClassValuator) {
		this.lstDataModelSecurityClassValuator = lstDataModelSecurityClassValuator;
	}
		
	/**
	 * Gets the lst dm security class valuator result.
	 *
	 * @return the lst dm security class valuator result
	 */
	public GenericDataModel<SecurityClassValuatorResultTO> getLstDMSecurityClassValuatorResult() {
		return lstDMSecurityClassValuatorResult;
	}

	/**
	 * Sets the lst dm security class valuator result.
	 *
	 * @param lstDMSecurityClassValuatorResult the new lst dm security class valuator result
	 */
	public void setLstDMSecurityClassValuatorResult(
			GenericDataModel<SecurityClassValuatorResultTO> lstDMSecurityClassValuatorResult) {
		this.lstDMSecurityClassValuatorResult = lstDMSecurityClassValuatorResult;
	}

	/**
	 * Gets the security class valuator result.
	 *
	 * @return the security class valuator result
	 */
	public SecurityClassValuatorResultTO getSecurityClassValuatorResult() {
		return securityClassValuatorResult;
	}

	/**
	 * Sets the security class valuator result.
	 *
	 * @param securityClassValuatorResult the new security class valuator result
	 */
	public void setSecurityClassValuatorResult(
			SecurityClassValuatorResultTO securityClassValuatorResult) {
		this.securityClassValuatorResult = securityClassValuatorResult;
	}

	/**
	 * Gets the security to selected.
	 *
	 * @return the security to selected
	 */
	public SecurityClassValuatorResultTO getSecurityToSelected() {
		return securityToSelected;
	}

	/**
	 * Sets the security to selected.
	 *
	 * @param securityToSelected the new security to selected
	 */
	public void setSecurityToSelected(
			SecurityClassValuatorResultTO securityToSelected) {
		this.securityToSelected = securityToSelected;
	}

	/**
	 * Gets the intrument.
	 *
	 * @return the intrument
	 */
	public Integer getIntrument() {
		return Intrument;
	}

	/**
	 * Sets the intrument.
	 *
	 * @param intrument the new intrument
	 */
	public void setIntrument(Integer intrument) {
		Intrument = intrument;
	}

	/**
	 * Gets the lst intrument.
	 *
	 * @return the lst intrument
	 */
	public List<ParameterTable> getLstIntrument() {
		return lstIntrument;
	}

	/**
	 * Sets the lst intrument.
	 *
	 * @param lstIntrument the new lst intrument
	 */
	public void setLstIntrument(List<ParameterTable> lstIntrument) {
		this.lstIntrument = lstIntrument;
	}

	/**
	 * Gets the lst currency.
	 *
	 * @return the lst currency
	 */
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}

	/**
	 * Sets the lst currency.
	 *
	 * @param lstCurrency the new lst currency
	 */
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}
	
}
