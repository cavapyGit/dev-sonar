package com.pradera.securities.valuator.core.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.AccessTimeout;
import javax.ejb.EJB;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.transaction.TransactionSynchronizationRegistry;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.commons.configuration.ValuatorSetup;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.funds.BeginEndDay;
import com.pradera.model.issuancesecuritie.ProgramAmortizationCoupon;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.valuatorprocess.ValuatorCode;
import com.pradera.model.valuatorprocess.ValuatorEventBehavior;
import com.pradera.model.valuatorprocess.ValuatorExecutionDetail;
import com.pradera.model.valuatorprocess.ValuatorExecutionParameters;
import com.pradera.model.valuatorprocess.ValuatorMarketfactConsolidat;
import com.pradera.model.valuatorprocess.ValuatorMarketfactFile;
import com.pradera.model.valuatorprocess.ValuatorPriceReport;
import com.pradera.model.valuatorprocess.ValuatorProcessExecution;
import com.pradera.model.valuatorprocess.ValuatorProcessSetup;
import com.pradera.model.valuatorprocess.ValuatorSecurityCode;
import com.pradera.model.valuatorprocess.ValuatorTermRange;
import com.pradera.model.valuatorprocess.type.ValuatorDetailType;
import com.pradera.model.valuatorprocess.type.ValuatorEventType;
import com.pradera.model.valuatorprocess.type.ValuatorFormuleType;
import com.pradera.model.valuatorprocess.type.ValuatorInformationType;
import com.pradera.model.valuatorprocess.type.ValuatorMarkFactActiveType;
import com.pradera.model.valuatorprocess.type.ValuatorMarkFactFileType;
import com.pradera.model.valuatorprocess.type.ValuatorSecMarkFactType;
import com.pradera.model.valuatorprocess.type.ValuatorSecurityCodSituation;
import com.pradera.model.valuatorprocess.type.ValuatorSecurityCodState;
import com.pradera.model.valuatorprocess.type.ValuatorType;
import com.pradera.securities.query.to.SecurityReportResultTO;
import com.pradera.securities.valuator.core.service.ValuatorLiveOperationService;
import com.pradera.securities.valuator.core.service.ValuatorProcessAditional;
import com.pradera.securities.valuator.core.service.ValuatorRangesService;
import com.pradera.securities.valuator.core.service.ValuatorSecuritiesService;
import com.pradera.securities.valuator.core.view.ValuatorInconsistenciesTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class ValuatorSecuritiesFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/12/2014
 */
@Singleton
@Performance
//@Interceptors({ContextHolderInterceptor.class,MonitoringEventInterceptor.class})
public class ValuatorSecuritiesFacade {
	/** The user session. */ 
	@Inject private UserInfo userInfo;

	/** The range service. */
	@EJB private ValuatorRangesService rangeService;
	
	/** The parameter service bean. */
	@EJB private ParameterServiceBean parameterServiceBean;
	
	/** The valuator aditional. */
	@EJB private ValuatorProcessAditional valuatorAditional;
	
	/** The holiday service bean. */
	@EJB private HolidayQueryServiceBean holidayServiceBean;
	
	/** The securities service. */
	@EJB private ValuatorSecuritiesService securitiesService;
	
	/** The live operation service. */
	@EJB private ValuatorLiveOperationService liveOperationService;
	
	/** The batch process service facade. */
	@EJB private BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The transaction registry. */
	@Resource TransactionSynchronizationRegistry transactionRegistry;
	
	/** The valuator setup. */
	@Inject @ValuatorSetup ValuatorProcessSetup valuatorSetup;
	
//	/** The market fact event. */
//	@Inject Event<MarketFactForEvent> marketFactEvent;
//	
//	/** The notification event. */
//	@Inject Event<ValuatorNotificationEvent> notificationEvent;

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		
	}

	/**
	 * Execute fixed income valuator.
	 *
	 * @param executionId the execution pk
	 * @param detailType the detail type
	 * @param currency the currency
	 * @param processDate the valuator date
	 * @throws ServiceException the service exception
	 */
	@TransactionTimeout(unit=TimeUnit.HOURS,value=3)
//	@ValuatorDispatcherEvent(eventType=ValuatorEventType.EVENT_2_1_PRICE_FIXED_INCOME)
	public void executeFixedIncomeValuator(final Long executionId, final Integer detailType, Integer currency, final Date processDate) throws ServiceException{
		final LoggerUser logUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		/**Creating indicators of yes and not*/
		Integer indYes = BooleanType.YES.getCode();
		Integer indNot = BooleanType.NO.getCode();
		/**Assignment instrument type for this method*/
		Integer instrumentType = InstrumentType.FIXED_INCOME.getCode();
		/**Getting Id of valuator event*/
		Long eventCode = ValuatorEventType.EVENT_2_1_PRICE_FIXED_INCOME.getCode();
		Map<Integer,List<String>> securityCodes = securitiesService.findSecurityList(instrumentType,executionId);
		/**Variable to handle securityCode with valuatorCode concatenated*/
		final ConcurrentMap<String,Long> valuatorCodeMap = valuatorAditional.getValuatorSecurityCodeOnMap(null,executionId,instrumentType);
		/**Find all valuatorCode with id*/
		ConcurrentMap<Long,String> valuatorCodes 		 = valuatorAditional.getValuatorCodeOnMap(instrumentType);
		/**Getting list of reporting Operation*/
		List<MechanismOperation> operations = securitiesService.getOperationBySecurityCode(executionId,InstrumentType.FIXED_INCOME.getCode());
		/*
		//issue 1040
		List<MechanismOperation> operationsSelid = securitiesService.getSelidOperationBySecurityCode(executionId,InstrumentType.FIXED_INCOME.getCode());
		
		if(operationsSelid!=null && !operationsSelid.isEmpty()){
			processSelidOperationPrice(executionId,detailType,processDate,operationsSelid,logUser);
		}*/
		
		/**If exists reporting operations*/
		if(operations!=null && !operations.isEmpty()){
			/**Processing reporting operation and update price*/
			processOperationPrice(executionId,detailType,processDate,operations,logUser);
		}
		for(final Integer secClass: securityCodes.keySet()){
			List<String> securityList = securityCodes.get(secClass);
//			notificationEvent.fire(getNotification(eventCode,executionId,detailType, indYes, indNot,secClass,securityList.size(),indNot,null));
			try{
				/**Getting size of all process to execute*/
				Double totalProgress = new Double(ValuatorProcessValidation.getStringByMaxRow(securityList).size());
				/**Variable to handle current process*/
				Double currentProgress = new Double(ComponentConstant.ZERO);
				for(List<String> chunkOfSecurity: ValuatorProcessValidation.getStringByMaxRow(securityList)){
					/**Method to get SecuritiesCode in array*/
					String[] securitiesCode = chunkOfSecurity.toArray(new String[chunkOfSecurity.size()]);
					/**Main method to generate Valuator code and prices*/
					processValuatorCode(executionId,detailType,instrumentType, processDate, secClass,logUser, valuatorCodeMap,valuatorCodes,securitiesCode);
					/**Calculating current percentage by each iteration*/
//					Double percentage = (++currentProgress/totalProgress)*ValuatorConstants.ONE_HUNDRED;
//					notificationEvent.fire(getNotification(eventCode,executionId,detailType, indYes, indNot,secClass,securityList.size(),indNot,percentage.intValue()));
				}
				/**Send event to finish the process*/
//				notificationEvent.fire(getNotification(eventCode, executionId, detailType, indNot, indYes,secClass,securityList.size(),indNot,null));
			}catch(Exception ex){
//				notificationEvent.fire(getNotification(eventCode, executionId, detailType, indNot, indYes,secClass,securityList.size(),indYes,null));
				finishValuatorExecutionDetail(executionId, detailType, eventCode,true);
				throw ex;
			}
		}
		/**Insert new prices in database*/
		valuatorAditional.processPriceBySecurityRate(executionId, logUser);
		/**Send event to finish the process*/
		finishValuatorExecutionDetail(executionId, detailType, eventCode,false);
	}
	
	/**
	 * Metodo para buscar los valores en REPORTO
	 */
	public List<SecurityReportResultTO> searchSecurityReport (Date currenDate) {
		return securitiesService.searchSecurityReport(currenDate);
	}
	
	/**
	 * Metodo para buscar los valores en REPORTO
	 * @throws ServiceException 
	 */
	public Security searchSecurity (String idSecurityCode) throws ServiceException {
		return securitiesService.searchSecurity(idSecurityCode);
	}
	
	/**
	 * 
	 * @param security
	 * @param valuatorDate
	 * @param execDetailTyp
	 * @param marketRate
	 * @param marketDate
	 * @param instrumentType
	 * @return
	 * @throws ServiceException
	 */
	public List<BigDecimal> valuatorPriceReport (
			Security security, Date valuatorDate, Integer execDetailTyp, BigDecimal marketRate, Date marketDate,Integer instrumentType, Integer indHistoric, Integer indAsfi) throws ServiceException {
		
		//insertPriceResult(executionId, valuatorDetType, null,null, processDate, rate, null, operation, null, null, processDate, null, null,loggerUser);
		
		List<BigDecimal> lstBigdecimalReport = new ArrayList<BigDecimal>();
		
		String valuatorCode           = security!=null?security.getValuatorProcessClass().getIdValuatorClassPk():null;
		Integer securityClass		  = security!=null?security.getSecurityClass():null;
		Integer formuleType			  = security!=null?valuatorAditional.findFormuleType(securityClass, valuatorCode):null;
		//String securityCode 		  = security!=null?security.getIdSecurityCodePk():null;
		//Integer instrumentType 		  = security!=null?security.getInstrumentType():null;
		
		//Correccion para el problema detectado en valores con cupones en el issue 1290
		//Reduccion de tiempo al momento de llenar la cuponer issue 1290
		if (formuleType.equals(ValuatorFormuleType.WITH_COUPONS.getCode())) {
			//security.setAmortizationPaymentSchedule(securitiesService.findAmortizationPaymentScheduleForPending(security.getIdSecurityCodePk()));
			//security.setInterestPaymentSchedule(securitiesService.findInterestPaymentScheduleForPending(security.getIdSecurityCodePk()));
			
			List <String> securityCodeList = new ArrayList<String>();
			
			securityCodeList.add(security.getIdSecurityCodePk());
			
			Map<String,List<ProgramInterestCoupon>> interestMap 		= securitiesService.getProgramInteresCouponMap(securityCodeList, valuatorDate,securityClass);
			
			if(interestMap.isEmpty()) {
				interestMap 		= securitiesService.getProgramInteresCouponMap(securityCodeList, CommonsUtilities.addDate(valuatorDate, -1),securityClass);
			}
			
			Map<String,List<ProgramAmortizationCoupon>> amortizationMap = securitiesService.getProgramAmortizationCouponMap(securityCodeList, valuatorDate);
			
			if(amortizationMap.isEmpty()) {
				amortizationMap 		= securitiesService.getProgramAmortizationCouponMap(securityCodeList, CommonsUtilities.addDate(valuatorDate, -1));
			}
			
			String securityCode = security.getIdSecurityCodePk();
			List<ProgramAmortizationCoupon> amortizationList = amortizationMap.get(securityCode);
			List<ProgramInterestCoupon> 	interestList 	 = interestMap.get(securityCode);
			
			if(amortizationList!=null && !amortizationList.isEmpty()){
				security.getAmortizationPaymentSchedule().setProgramAmortizationCoupons(amortizationList);
			}
			
			if(interestList!=null && !interestList.isEmpty()){
				security.getInterestPaymentSchedule().setProgramInterestCoupons(interestList);
			}
		}
		
		//Calculo del precio
		BigDecimal marketPrice = valuatorAditional.calculateMarketPriceReport(
				security, null, formuleType, marketRate, marketDate,valuatorDate, execDetailTyp, indHistoric, indAsfi);
		lstBigdecimalReport.add(marketPrice);
		
		if(instrumentType.equals(InstrumentType.FIXED_INCOME.getCode())) {
			//Calculo del plazo economico
			BigDecimal economicTerm = valuatorAditional.calculateEconomicTermReport(security, marketRate, marketDate, valuatorDate,indHistoric);
			lstBigdecimalReport.add(economicTerm);
		}
		
		return lstBigdecimalReport;
		
	}
	
/**
 * Execute chuck of security.
 *
 * @param chunkOfSecurity the chunk of security
 * @param securityClass the security class
 * @param processDate the process date
 * @param execPk the exec pk
 * @param execDetType the exec det type
 * @param valuatorCodesMap the valuator codes map
 * @param logUser the log user
 * @param instrumentType the instrument type
 * @return the boolean
 * @throws ServiceException the service exception
 */
	public Boolean executeChuckOfSecurity(List<String> chunkOfSecurity, Integer securityClass, Date processDate, Long execPk, Integer execDetType,
										  ConcurrentMap<String,Long> valuatorCodesMap, LoggerUser logUser, Integer instrumentType) throws ServiceException{
		try{
			/**Method to get SecuritiesCode in array*/
			String[] securitiesCode = chunkOfSecurity.toArray(new String[chunkOfSecurity.size()]);
			/**Main method to generate Valuator code and prices*/
			processValuatorCode(execPk,execDetType,instrumentType, processDate, securityClass,logUser, valuatorCodesMap,null, securitiesCode);
		}catch(Exception ex){
			throw ex;
		}
		return true;
	}
	
	/**
	 * Execute variable income valuator.
	 *
	 * @param executionId the execution id
	 * @param detailType the detail type
	 * @param currency the currency
	 * @param procDate the proc date
	 * @throws ServiceException the service exception
	 */
//	@ValuatorDispatcherEvent(eventType=ValuatorEventType.EVENT_2_2_PRICE_VARIABLE_INCOME)
	public void executeVariableIncomeValuator(Long executionId, Integer detailType, Integer currency, Date procDate) throws ServiceException{
		/**Getting logger user*/
		LoggerUser logUsser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		/**Creating indicators of yes and not*/
		Integer indYes = BooleanType.YES.getCode();
		Integer indNot = BooleanType.NO.getCode();
		/**Assignment instrument type for this method*/
		Integer instrumentType = InstrumentType.VARIABLE_INCOME.getCode();
		/**Getting value to handle valuator event*/
		Long eventCode = ValuatorEventType.EVENT_2_2_PRICE_VARIABLE_INCOME.getCode();
		/**Get securities in variable income*/
		Map<Integer,List<String>> securityCodes = securitiesService.findSecurityList(instrumentType,executionId);
		/**Variable to handle securityCode with valuatorCode concatenated*/
		ConcurrentMap<String,Long> valuatorSecCodes = valuatorAditional.getValuatorSecurityCodeOnMap(null,executionId,instrumentType);
		/**Find all valuatorCode with id*/
		ConcurrentMap<Long,String> valuatorCodes 	= valuatorAditional.getValuatorCodeOnMap(instrumentType);
		/**Getting list of reporting Operation*/
		List<MechanismOperation> operations = securitiesService.getOperationBySecurityCode(executionId,instrumentType);
		
		/*
		//issue 1040
		List<MechanismOperation> operationsSelid = securitiesService.getSelidOperationBySecurityCode(executionId,instrumentType);
		if(operationsSelid!=null && !operationsSelid.isEmpty()){
			processSelidOperationPrice(executionId,detailType,procDate,operationsSelid,logUsser);
		}*/
		
		if(operations!=null && !operations.isEmpty()){
			/**Processing reporting operation and update price*/
			processOperationPrice(executionId,detailType,procDate,operations,logUsser);
		}
		/**Variable to handle current percentage, in variable income is smaller than fixed income*/
//		Integer fullPercentage = ValuatorConstants.ONE_HUNDRED.intValue();
		/**Iterate all securities of variable income*/
		for(Integer secClass: securityCodes.keySet()){
			/**getting list of securities*/
			List<String> securityLst = securityCodes.get(secClass);
			/**fire event to show in monitoring*/
//			notificationEvent.fire(getNotification(eventCode,executionId,detailType, indYes, indNot,secClass,securityLst.size(),indNot,null));
			try{
				/**Method to get SecuritiesCode in array*/
				String[] securitiesCode = securityLst.toArray(new String[securityLst.size()]);
				/**Main method to generate Valuator code and prices*/
				processValuatorCode(executionId,detailType,instrumentType,procDate,secClass,logUsser,valuatorSecCodes,valuatorCodes,securitiesCode);
				/**Update Percentage*/
//				notificationEvent.fire(getNotification(eventCode,executionId,detailType, indYes, indNot,secClass,securityLst.size(),indNot,fullPercentage));
			}catch(Exception ex){
//				notificationEvent.fire(getNotification(eventCode, executionId, detailType, indNot, indYes,secClass,securityLst.size(),indYes,null));
				throw ex;
			}
			/**process prices on security*/
			valuatorAditional.processPriceBySecurityRate(executionId, logUsser);
			/**Finish the process to culminate in monitoring*/
//			notificationEvent.fire(getNotification(eventCode, executionId, detailType, indNot, indYes,secClass,securityLst.size(),indNot,null));
		}
		/**Code to finish process*/
		Integer detailPk = ValuatorDetailType.SECURITY.getCode();
		Long eventPk 	 = ValuatorEventType.EVENT_2_2_PRICE_VARIABLE_INCOME.getCode();
		finishValuatorExecutionDetail(executionId, detailPk, eventPk,false);
	}
	
	/**
	 * Process operation price.
	 * Method to generate Prices on operations with reporting
	 *
	 * @param executionId the execution id
	 * @param execDetType the exec det type
	 * @param processDate the process date
	 * @param operationss the operationss
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	private void processOperationPrice(Long executionId, Integer execDetType, Date processDate, List<MechanismOperation> operationss, LoggerUser loggerUser)
																																		throws ServiceException{		
		for(List<MechanismOperation> operations: ValuatorProcessValidation.getOperationByMaxRow(operationss)){
			List<Long> operationList = new ArrayList<>();
			for(MechanismOperation operation: operations){
			    BigDecimal rate = operation.getAmountRate()!=null?operation.getAmountRate():BigDecimal.ZERO;//ValuatorConstants.UNKNOW_RATE;
			    Integer valuatorDetType = ValuatorDetailType.OPERATIONS.getCode();
			    insertPriceResult(executionId, valuatorDetType, null,null, processDate, rate, null, operation, null, null, processDate, null, null,loggerUser);
			    operationList.add(operation.getIdMechanismOperationPk());
			}
			/**Update SettlementAccountMarketFact only for reportingOperations*/
//			liveOperationService.updateSettlementAccountMarketFact(operationList, ComponentConstant.PURCHARSE_ROLE, ComponentConstant.TERM_PART);
		}
	}
	
	/**
	 * Process operation price. //issue 1040
	 * Method to generate Prices on operations with reporting
	 *
	 * @param executionId the execution id
	 * @param execDetType the exec det type
	 * @param processDate the process date
	 * @param operationss the operationss
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	private void processSelidOperationPrice(Long executionId, Integer execDetType, Date processDate, List<MechanismOperation> operationss, LoggerUser loggerUser)
																																		throws ServiceException{		
		for(List<MechanismOperation> operations: ValuatorProcessValidation.getOperationByMaxRow(operationss)){
			List<Long> operationList = new ArrayList<>();
			for(MechanismOperation operation: operations){
			    BigDecimal rate = operation.getAmountRate()!=null?operation.getAmountRate():BigDecimal.ZERO;//.UNKNOW_RATE;
			    Integer valuatorDetType = ValuatorDetailType.OPERATIONS.getCode();
			    insertPriceResult(executionId, valuatorDetType, null,null, processDate, rate, null, operation, null, null, processDate, null, null,loggerUser);
			    operationList.add(operation.getIdMechanismOperationPk());
			}
			/**Update SettlementAccountMarketFact only for reportingOperations*/
//			liveOperationService.updateSelidSettlementAccountMarketFact(operationList, ComponentConstant.PURCHARSE_ROLE, ComponentConstant.TERM_PART);
		}
	}
	
	/**
	 * Execute balance valuator.
	 *
	 * @param executionId the execution id
	 * @param valuatorType the valuator type
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	@TransactionTimeout(unit=TimeUnit.HOURS,value=3)
//	@ValuatorDispatcherEvent(eventType=ValuatorEventType.EVENT_UPDATING_PORTFOLY)
	public void executeBalanceValuator(Long executionId, Integer valuatorType)throws ServiceException{
		
		if(valuatorType.equals(ValuatorType.BY_SECURITY.getCode()) || valuatorType.equals(ValuatorType.ENTIRE_PORTFOLIO.getCode()) ||
		   valuatorType.equals(ValuatorType.BY_INVESTOR.getCode()) || valuatorType.equals(ValuatorType.BY_PARTICIPANT.getCode())){
			/**HERE WE SHOULD CALL A STORE PROCEDURE*/
			securitiesService.executeBalanceValuator(executionId);
		}
		
		/**Code to finish process*/
		Integer detailPk = ValuatorDetailType.BALANCE.getCode();
		Long eventPk 	 = ValuatorEventType.EVENT_UPDATING_PORTFOLY.getCode();
		finishValuatorExecutionDetail(executionId, detailPk, eventPk,false);
	}
	
	/**
	 * Metodo para verificar la reversion de saldos en operaciones selid
	 * @return
	 */
	public Boolean verifyUnblockBalanceSelidBatchBoolean() {
		
		Boolean result = securitiesService.verifyUnblockBalanceSelidBatchBoolean();
		
		return result;
	}
	
	/**
	 * Execute operations valuator.
	 *
	 * @param executionId the execution id
	 * @param participants the participants
	 * @param securities the securities
	 * @param accounts the accounts
	 * @throws ServiceException the service exception
	 */
	public void executeOperationsValuator(Long executionId, List<Long> participants, List<String> securities, List<Long> accounts) throws ServiceException{
		/**HERE WE PROCESS AL OPERATIONS AFFECTATE BY VALUATOR PROCESS*/
	}

	/**
	 * *
	 * METHOD TO PROCESS VALUATOR CODE ON SECURITY.
	 *
	 * @param execPk the execution pk
	 * @param execDetType the exec det type
	 * @param instType the instrument type
	 * @param proDate the valuator date
	 * @param securityClass the security class
	 * @param loggUsr the logg usr
	 * @param valuatorCodMap the valuator cod map
	 * @param valuatorCodes the valuator codes
	 * @param securityCode the security code
	 * @throws ServiceException the service exception
	 */
	private void processValuatorCode(Long execPk, Integer execDetType,Integer instType, Date proDate, Integer securityClass, LoggerUser loggUsr,
									 ConcurrentMap<String,Long> valuatorCodMap, ConcurrentMap<Long,String> valuatorCodes,String... securityCode)
											 																						throws ServiceException{
//		logger.warn("################################# Begin of process ########################################");
		List<String> securityCodeList = Arrays.asList(securityCode);
		/**Get all securities from his ID*/
		List<Security> securities 			   						= valuatorAditional.findSecurityForValuatorCode(securityCodeList,securityClass);
		/**Get all securities' Interest and coupons*/
		Map<String,List<ProgramInterestCoupon>> interestMap 		= securitiesService.getProgramInteresCouponMap(securityCodeList, proDate,securityClass);
		/**Get all securities' amortization and coupons*/
		Map<String,List<ProgramAmortizationCoupon>> amortizationMap = securitiesService.getProgramAmortizationCouponMap(securityCodeList, proDate);
		/**Configure securities and interest with coupons*/
		securities	    											= securitiesService.populateSecurityWithCoupons(securities, interestMap, amortizationMap);
		/**Get securities and acquisition rates(HolderAccountBalance table)*/
		Map<String,Map<Date,List<BigDecimal>>> acquisitionRatesMap	= securitiesService.getSecurityWithAdquisitionRate(securityCodeList,instType);
		/**Get securities and Physical rates(PhysicalCertMarketfact table)*/
		Map<String,Map<Date,List<BigDecimal>>> physicalRatesMap		= securitiesService.getPhysicalSecurityRate(securityCodeList,instType);
		
//		logger.warn("################################# Begin of SubIteration	1.-	########################################");
		/**Join rates from desmaterializateMap and acquisitonMap*/
		if(physicalRatesMap!=null && !physicalRatesMap.isEmpty()){
			if(acquisitionRatesMap!=null && !acquisitionRatesMap.isEmpty()){
				acquisitionRatesMap = valuatorAditional.joinRateFromPhysAndDesmaterial(physicalRatesMap,acquisitionRatesMap);
			}else{
				acquisitionRatesMap = physicalRatesMap;
			}
		}
		
		/**FIND ALL VALUATOR CODES FOR SECURITIES RECEIVED*/
		Map<String,List<ValuatorMarketfactConsolidat>> markFactConsolidMap = securitiesService.getMarketFactConsolidat(null, null);
		
		if(markFactConsolidMap==null || markFactConsolidMap.isEmpty()){
			throw new ServiceException(ErrorServiceType.VALUATOR_MARKETFACT_INFO_EMPTY);
		}
		
		for(Security security: securities){
			security.setSecurityClass(securityClass);
			String securityPk   	= security.getIdSecurityCodePk();
			String valuatorType 	= security.getValuatorProcessClass().getIdValuatorClassPk();
			BigDecimal interestRate = security.getInterestRate();
			BigDecimal days = null;
			String newValCode = null;
			BigDecimal economicTerm = null;
			ValuatorTermRange rangeObject = null;
			/**Verified if valuatorType is not null*/
			if(valuatorType==null){
				throw new ServiceException(ErrorServiceType.VALUATOR_CORE_PROCESS_EMPTY_VALUATORCLASS, new String[]{security.getIdSecurityCodePk()});
			}
			/**Getting formula from formuleType*/
			Integer formuleType 	= valuatorAditional.findValuatorProcess(valuatorType).getFormuleType();
			/**Adding some validations for fixed income*/
			if(instType.equals(InstrumentType.FIXED_INCOME.getCode())){
				economicTerm = valuatorAditional.calculateEconomicTerm(security, interestRate, proDate, proDate);
				if(BigDecimal.ZERO.compareTo(economicTerm)>=0){
					/**ERROR BY INCONSISTENT DATA, TERM RANGE CAN'T BE NEGATIVE*/
					throw new ServiceException(ErrorServiceType.VALUATOR_CORE_PROCESS_NEGATIVE_DAYS, new String[]{securityPk});
				}
				
				rangeObject = rangeService.findTermRange(economicTerm);
				if(formuleType!=null && instType.equals(InstrumentType.FIXED_INCOME.getCode())){
					days = economicTerm.subtract(rangeObject.getMinRange());
				}
			}
			
			/**Generate Valuator Code*/
			newValCode = generateValuatorCode(security, economicTerm, proDate, proDate);
			/**FIND IF VALUATOR CODE EXIST IN BD*/
			Long oldValCodeKey   = getKeyFromHashMap(valuatorCodes, newValCode);
			/**METHOD TO REPLICATE VALUATOR CODE ON SECURITY AND VERIFIED IF THIS EXISTS IN IT*/
			ValuatorSecurityCode secCode = procesValuatorCode(securityPk, newValCode, oldValCodeKey, rangeObject,days, valuatorCodMap, valuatorCodes,proDate,loggUsr);
			/**METHOD TO PROCESS MARKETFACT FILES FROM BBV TO CALCULATE PRICE*/
			String oldValCode = null;
			if(valuatorCodes!=null && oldValCodeKey!=null && valuatorCodes.get(oldValCodeKey)!=null){
				oldValCode = valuatorCodes.get(oldValCodeKey);
			}
			if(oldValCode==null){
				oldValCode = newValCode;
			}
			/**Method to generate price*/
			generateSecurityMarketPrice(execPk,execDetType,security,secCode, markFactConsolidMap,valuatorCodes,acquisitionRatesMap,proDate,loggUsr);
//			logger.warn(security.getIdSecurityCodePk());
		}
//		logger.warn("################################# Final os SubIteration	1.-	########################################");
//		logger.warn("################################# Final of process 			########################################");
	}
	
	/**
	 * *
	 * Method to generate valuatorCode.
	 *
	 * @param security the security
	 * @param economicTerm the economic term
	 * @param marketDate the market date
	 * @param processDate the process date
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String generateValuatorCode(Security security, BigDecimal economicTerm, Date marketDate, Date processDate) throws ServiceException{
		String securityCodePk     = security.getIdSecurityCodePk();
		Integer instrumentType 	  = security.getInstrumentType();
		String valuatorCode       = null;
		String currencyCod  	  = valuatorSetup.getParameters().get(security.getCurrency());
		
		String valuatorType = security.getValuatorProcessClass().getIdValuatorClassPk();
		String mnemonic     = security.getIssuer().getMnemonic();
		String subOrdCode, prepaidCode = null, rangeCode= null;
		
		if(economicTerm!=null){
			if(BigDecimal.ZERO.compareTo(economicTerm)>=0){
				/**ERROR BY INCONSISTENT DATA, TERM RANGE CAN'T BE NEGATIVE*/
				throw new ServiceException(ErrorServiceType.VALUATOR_CORE_PROCESS_NEGATIVE_DAYS, new String[]{securityCodePk});
			}
			/**Round economic term*/
			economicTerm = CommonsUtilities.round(economicTerm, 0);
			/**Getting valuator term range*/
			ValuatorTermRange rangeObject = rangeService.findTermRange(economicTerm);
			/**Getting range code*/
			rangeCode    = rangeObject.getRangeCode();
		}
		
		/**Verified if security have subOrdinate indicator*/
		if(security.getIndSubordinated()!=null && security.getIndSubordinated().equals(BooleanType.YES.getCode())){
			subOrdCode = valuatorSetup.getSubOrdinatedCode();
		}else{
//			subOrdCode = ValuatorConstants.PARAM_ZERO;
		}
		
		boolean hasEarlyRedemption = (security.getIndEarlyRedemption()!=null &&  security.getIndEarlyRedemption().equals(BooleanType.YES.getCode()));
		boolean isDpfOrDpa = (security.getSecurityClass().equals(SecurityClassType.DPF.getCode())||security.getSecurityClass().equals(SecurityClassType.DPA.getCode()));
		/**Verified if security have earlyRedemption indicator*/
		if(hasEarlyRedemption && !isDpfOrDpa){
			if(security.getIndPrepaid()!=null && security.getIndPrepaid().equals(BooleanType.YES.getCode())){
				prepaidCode = "Q";
			}else{
				prepaidCode = valuatorSetup.getPrepaidCode();
			}
		}else{
//			prepaidCode = ValuatorConstants.PARAM_ZERO;
		}
		
		/**Generate new valuator Code*/
		if(instrumentType.equals(InstrumentType.FIXED_INCOME.getCode())){
			valuatorCode = valuatorAditional.getFixedValuatorCode(securityCodePk,valuatorType, mnemonic, currencyCod, rangeCode, prepaidCode,"");// subOrdCode);
		}else if(instrumentType.equals(InstrumentType.VARIABLE_INCOME.getCode())){
			valuatorCode = valuatorAditional.getVariableValuatorCode(securityCodePk,valuatorType, mnemonic, currencyCod, security.getSecuritySerial());	
		}
		
		return valuatorCode;
	}
	
	/**
	 * *
	 * Method to generate Price according with investors with many rate and determinate if have homologation.
	 *
	 * @param execPk the exec pk
	 * @param execDetType the exec det type
	 * @param security the security
	 * @param secCodeKey the sec code key
	 * @param marketFactConsolidatesMap the market fact consolidates map
	 * @param valuatorCodeMap the valuator code map
	 * @param acquisitionRatesMap the acquisition rates map
	 * @param processDate the process date
	 * @param loggUsr the logg usr
	 * @throws ServiceException the service exception
	 */
	public void generateSecurityMarketPrice(Long execPk, Integer execDetType, Security security, ValuatorSecurityCode secCodeKey,
										   	 Map<String,List<ValuatorMarketfactConsolidat>> marketFactConsolidatesMap, ConcurrentMap<Long,String> valuatorCodeMap,
										   	 Map<String,Map<Date,List<BigDecimal>>> acquisitionRatesMap, Date processDate,
										   	 LoggerUser loggUsr) throws ServiceException{
		/**GETTING VALUES FROM SECURITY OBJECT*/
		Integer instrumentType	= security.getInstrumentType();
		/**Get valuator Code*/
		String valuatorCode = null;
		/**Find securityCode and get Acquisition Rate according with existent balances*/
		Map<Date,List<BigDecimal>> _lstadquisitRate = acquisitionRatesMap.get(security.getIdSecurityCodePk());
		if(_lstadquisitRate!=null && !_lstadquisitRate.isEmpty()){
			iteration:
			for(Date markettDate : _lstadquisitRate.keySet()){
				for(BigDecimal rateOrPrice: _lstadquisitRate.get(markettDate)){
					/**Generate valuatorCode according with instrumentType*/
					if(instrumentType.equals(InstrumentType.FIXED_INCOME.getCode())){
						/**Get economic term*/
						BigDecimal economicTerm = valuatorAditional.calculateEconomicTerm(security, rateOrPrice, markettDate, processDate);
						/**Get new valuator code*/
						valuatorCode = generateValuatorCode(security, economicTerm, markettDate, processDate);
						/**Verified if exist valuatorCode on Map, however we must insert at database*/
					}else if(instrumentType.equals(InstrumentType.VARIABLE_INCOME.getCode())){
						/**Get new valuator code, for variable income*/
						valuatorCode = generateValuatorCode(security, null, markettDate, processDate);
					}
					/**If valuatorCode exist on another security before*/
					if(getKeyFromHashMap(valuatorCodeMap,valuatorCode)==null){
						securitiesService.createValuatorCode(valuatorCode, valuatorCodeMap,loggUsr);
					}
					/**List to get all marketFact according with valuatorCode*/
					if(marketFactConsolidatesMap.get(valuatorCode)!=null){
						break iteration;/**Jump to a first for*/
					}
					valuatorCode = null;
				}
			}
		}
		
		/**If valuator code is != null, exists in the fixed and historic file*/
		if(valuatorCode!=null){
			/**CONFIG PARAMETERS TO SEND SECURITY_MARKETFACT*/
			Integer valuatorType    = ValuatorSecMarkFactType.BY_MARKET.getCode();
			Integer infoType 		= ValuatorInformationType.BY_VALUATOR_CODE.getCode();
			/**Getting valuator File from files loaded*/
			List<ValuatorMarketfactConsolidat> marketFactFromFile = marketFactConsolidatesMap.get(valuatorCode);
			/**Indicator to know if it's historic*/
			Integer indHyst 	= BooleanType.NO.getCode();
			/**Variables to handle marketFact*/
			BigDecimal markRate=null, markPrice = null;
			Date markDate = null;
			/**Array to get dateRate or Price*/
			Object[] arrayDateAndRateOrPrice = null;
			if(instrumentType.equals(InstrumentType.FIXED_INCOME.getCode())){
				arrayDateAndRateOrPrice = getMarketDateAndRateOrPrice(marketFactFromFile, instrumentType);
				markRate = (BigDecimal) arrayDateAndRateOrPrice[0];
				markDate = (Date) arrayDateAndRateOrPrice[1];
				indHyst = (Integer) arrayDateAndRateOrPrice[2];
				
				/**Part to verified if this have iteration*/
				BigDecimal economicTerm = valuatorAditional.calculateEconomicTerm(security, markRate, markDate, processDate);
				/**Getting new valuatorCode*/
				String newValuatorCode 	= generateValuatorCode(security, economicTerm, markDate, processDate);
				/**Getting the new marketRate with rate of an other valuatorCode*/
				BigDecimal newMarkRate = null;
				/**Variable to handle if iteration have been finish*/
				boolean iterationFinish = false;
				/**Variable to get economic term with lower value*/
				BigDecimal lowerEconomicTerm = economicTerm;
				/**ValuatorCode iterated, only when exist's iteration*/
				String valuatorCodeIterated = null;
				do{
					/**If newValuatorCode it's equals than valuatorCode nothing happen*/
					if(newValuatorCode.equals(valuatorCode)){
						iterationFinish = true;
					}else if (valuatorCodeIterated!=null){/**Else exist's iteration*/
						/**If valuatorCodeIterated it's equals than new ValuatorCode, then it's a simple iteration*/
						if(valuatorCodeIterated.equals(newValuatorCode)){
							valuatorCode = newValuatorCode;
							markRate = newMarkRate;
						}else{/**else, we are talking about a multiple iteration*/
							/**if the lowerEconomiTerm it's the same than original economic term, the original
							 * rate it's associated with the lower range*/
							if(lowerEconomicTerm.equals(economicTerm)){
								markRate = newMarkRate;
							}else{/**The original rate, it's not the lower range then we must to get the last rate gotten*/
								valuatorCode = newValuatorCode;
							}
						}
						iterationFinish = true;
					}else{
						/**Getting marketFact with new valuatorCode*/
						marketFactFromFile = marketFactConsolidatesMap.get(newValuatorCode);
						/**If exist's marketFact with new valuatorCode*/
						if(marketFactFromFile!=null){
							/**If valuator code doesn't exist, create new valuatorCode*/
							if(getKeyFromHashMap(valuatorCodeMap,newValuatorCode)==null){
								securitiesService.createValuatorCode(newValuatorCode, valuatorCodeMap,loggUsr);
							}
							/**Getting marketFact with fixed and historic*/
							arrayDateAndRateOrPrice = getMarketDateAndRateOrPrice(marketFactFromFile, instrumentType);
							/**Getting the new marketRate with rate of an other valuatorCode*/
							newMarkRate = (BigDecimal) arrayDateAndRateOrPrice[0];
							/**Part to verified if this have multiple iteration*/
							BigDecimal newEconomicTerm = valuatorAditional.calculateEconomicTerm(security, newMarkRate, markDate, processDate);
							/**If lowerEconomicTerm is greater than newEconomicTerm, then newEconomicTerm is lower than this*/
							if(lowerEconomicTerm.compareTo(newEconomicTerm)>0){
								lowerEconomicTerm = newEconomicTerm;
							}
							/**calculate valuatorCodeIterate to know what range we need to get*/
							valuatorCodeIterated = generateValuatorCode(security, newEconomicTerm, markDate, processDate);
						}else{
							valuatorCode 	= newValuatorCode;
							iterationFinish = true;
						}
					}
				}while(!iterationFinish);
			}else if(instrumentType.equals(InstrumentType.VARIABLE_INCOME.getCode())){
				arrayDateAndRateOrPrice = getMarketDateAndRateOrPrice(marketFactFromFile, instrumentType);
//				markRate = ValuatorConstants.UNKNOW_RATE;
				markPrice = (BigDecimal) arrayDateAndRateOrPrice[0];
				markDate = (Date) arrayDateAndRateOrPrice[1];
			}
			/**Get new valuatorCode*/
			Long valCodKey = getKeyFromHashMap(valuatorCodeMap, valuatorCode);
			/**INVOKATE METHOD TO GENERATE SECURITY MARKET_FACT ACORDING WITH PRICE*/
			insertPriceResult(execPk,execDetType,secCodeKey,valCodKey,markDate,markRate,markPrice,null,indHyst,security,processDate,valuatorType,infoType,loggUsr);
		}else{
			if(instrumentType.equals(InstrumentType.FIXED_INCOME.getCode())){
				/**Execute price by acquisition rate*/
				/**Assignment the valuatorType and informationType*/
				Integer valuatorType    = ValuatorSecMarkFactType.BY_SECURITY.getCode();
				Integer infoType 		= ValuatorInformationType.BY_SECURITY_CODE.getCode();
				
				/**Find securityCode and get Acquisition Rate according with existent balances*/
				if(_lstadquisitRate!=null && !_lstadquisitRate.isEmpty()){
					for(Date markDate : _lstadquisitRate.keySet()){
						for(BigDecimal rate: _lstadquisitRate.get(markDate)){
							/**If rate it's 0 doesn't enter at the valuator process*/
//							if(rate.equals(BigDecimal.ZERO))continue;
							/**Get economic term*/
							BigDecimal economicTerm = valuatorAditional.calculateEconomicTerm(security, rate, markDate, processDate);
							/**Get new valuator code*/
							valuatorCode = generateValuatorCode(security, economicTerm, markDate, processDate);
							/**Get valuatorCode*/
							Long valCodKey = getKeyFromHashMap(valuatorCodeMap, valuatorCode);
							/**If valuatorCode doesn't exist*/
							if(valCodKey==null){
								/**Create new ValuatorCode*/
								valCodKey = securitiesService.createValuatorCode(valuatorCode, valuatorCodeMap,loggUsr).getIdValuatorCodePk();
							}
							insertPriceResult(execPk,execDetType,secCodeKey,valCodKey,markDate,rate,null,null,null,security,processDate,valuatorType,infoType,loggUsr);
						}
					}
				}
			}
		}
	}

	/**
	 * *.
	 *
	 * @param valuatorDate the valuator date
	 */
	@SuppressWarnings("unchecked")
	@Lock(LockType.READ)
	@TransactionTimeout(unit=TimeUnit.HOURS,value=1)
	@AccessTimeout(unit=TimeUnit.HOURS,value=1)
	public void reloadMarketPrices(Date valuatorDate){
		valuatorSetup.setSecuritiesRatePriceTmp(new ArrayBlockingQueue<>(200000));
		Object dataWithPrices = parameterServiceBean.getMarketPriceResults(valuatorDate);
		for(Object[] data: (List<Object[]>)dataWithPrices){
			String securityCode = (String) data[0];
			BigDecimal markRate = (BigDecimal) data[1];
			BigDecimal markPric = (BigDecimal) data[2];
			Long operationPk 	= null;
			if(data[3] instanceof BigDecimal){
				operationPk 	= ((BigDecimal) data[3]).longValue();
			}
			if(data[3] instanceof Long){
				operationPk 	= (Long) data[3];
			}						
			Date marketDate 	= (Date) data[4];
			Integer valuatorType=operationPk == null? ValuatorDetailType.SECURITY.getCode():ValuatorDetailType.OPERATIONS.getCode();
			
//			MarketFactForEvent marketFactEvet = new MarketFactForEvent();
//			
//			marketFactEvet.setMarketRate(markRate);
//			marketFactEvet.setMarketPrice(markPric);
//			marketFactEvet.setMarketDate(marketDate);
//			marketFactEvet.setProcessDate(valuatorDate);
//			marketFactEvet.setSecurityCode(securityCode);
//			marketFactEvet.setMechanismOperationId(operationPk);
//			marketFactEvet.setValuatorOperationType(valuatorType);
			
//			if(!valuatorSetup.getSecuritiesRatePriceTmp().contains(marketFactEvet))
//				valuatorSetup.getSecuritiesRatePriceTmp().add(marketFactEvet);
		}
	}

	/**
	 * *.
	 */
	@Lock(LockType.READ)
	public void finishValuatorEvent(){ 
//		ValuatorNotificationEvent notificationError = new ValuatorNotificationEvent();
//		notificationError.setIndHasError(BooleanType.YES.getCode());
//		notificationEvent.fire(notificationError);
	}
	
	/**
	 * Generate security market fact.
	 *
	 * @param executionPk the execution pk
	 * @param execDetailTyp the exec detail typ
	 * @param secCodeKey the sec code key
	 * @param valCodePk the val code pk
	 * @param marketDate the market date
	 * @param marketRate the market rate
	 * @param marketPrice the market price
	 * @param operation the operation
	 * @param indHistoric the ind historic
	 * @param security the security
	 * @param valuatorDate the valuator date
	 * @param valuatorType the valuator type
	 * @param infType the information type
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	private void insertPriceResult(Long executionPk, Integer execDetailTyp,ValuatorSecurityCode secCodeKey, Long valCodePk, Date marketDate,
								   BigDecimal marketRate, BigDecimal marketPrice, MechanismOperation operation, Integer indHistoric,Security security, 
								   Date valuatorDate, Integer valuatorType, Integer infType, LoggerUser loggerUser) throws ServiceException{
		
		String valuatorCode           = security!=null?security.getValuatorProcessClass().getIdValuatorClassPk():null;
		Integer securityClass		  = security!=null?security.getSecurityClass():null;
		Integer formuleType			  = security!=null?valuatorAditional.findFormuleType(securityClass, valuatorCode):null;
		String securityCode 		  = security!=null?security.getIdSecurityCodePk():operation.getSecurities().getIdSecurityCodePk();
		Long operationID 			  = operation!=null?operation.getIdMechanismOperationPk():null;
		Integer instrumentType 		  = security!=null?security.getInstrumentType():null;
		
		//issue 1296: si se trata de un PGE, BOP se determina el formuleType en funcion de: si tiene o no cupones
		if(Validations.validateIsNotNull(security)
				&& Validations.validateIsNotNull(security.getSecurityClass())
				&& (security.getSecurityClass().equals(SecurityClassType.PGE.getCode())
					|| security.getSecurityClass().equals(SecurityClassType.BOP.getCode()) )
		){
			Map<String, Object> parameters = new HashMap<String, Object>();		
			parameters.put("id_security_code_pk", securityCode);
			@SuppressWarnings("unchecked")
			List<Object>  result = securitiesService.findByNativeQuery("SELECT NUMBER_COUPONS FROM SECURITY WHERE ID_SECURITY_CODE_PK = :id_security_code_pk", parameters);
			String numberCouponsString =  result.get(0).toString();
			security.setNumberCoupons(Integer.parseInt(numberCouponsString));
			if(security.getNumberCoupons().intValue() == 0 || security.getNumberCoupons().intValue() == 1){
				formuleType = ValuatorFormuleType.WITHOUT_COUPONS.getCode();
			}else{
				formuleType = ValuatorFormuleType.WITH_COUPONS.getCode();
			}
		}

		/**Declare variable to handle economicTerm and equivalentRate*/
		BigDecimal economicTerm = null, equivalentRat= null;
		/**Verified if price have been generated*/
		if(valuatorAditional.existMarketPrice(securityCode, marketRate, operationID, valuatorDate, execDetailTyp, marketDate)){
		}else{
			/**When it's fixedIncome marketPrice is null*/
			if(marketPrice==null){
				marketPrice = valuatorAditional.calculateMarketPrice(security, operation, formuleType, marketRate, marketDate,valuatorDate, execDetailTyp);
			}
			/**Verified if security is different than null an is fixed income*/
			if(security!=null && instrumentType.equals(InstrumentType.FIXED_INCOME.getCode())){
				/**If it's fixed income generate economicTerm and equivalentRate*/
				economicTerm = valuatorAditional.calculateEconomicTerm(security, marketRate, marketDate, valuatorDate);
				equivalentRat= valuatorAditional.calculateEquivalentRate(security, marketRate, marketDate, valuatorDate);
				/**Assignment ValuatorSecurityCode to new register SecurityMarketFact only for fixed income*/
				/*ValuatorSecurityCode valSecurityCode = secCodeKey;
				if(valSecurityCode!=null && !valuatorAditional.existSecurityInProcess(securityCode)){
					/*ValuatorSecurityMarketfact securityMarketFact = securitiesService.getValuatorMarketFactSecurity();
					securityMarketFact.setValuatorType(valuatorType);
					securityMarketFact.setInformationType(infType);
					securityMarketFact.setCirculationBalance(circulationBalance);
					securityMarketFact.setValuatorSecurityCode(valSecurityCode);
					securityMarketFact.setAverageRate(marketRate);
					securityMarketFact.setMarketPrice(marketPrice);
					securityMarketFact.setMarketAmount(marketPrice.multiply(circulationBalance));
					securityMarketFact.setRegistryUser(USER_SESSION);
					securityMarketFact.setRegistryDate(CommonsUtilities.currentDateTime());
					securityMarketFact.setMarketDate(marketDate);
					valuatorAditional.createValuatorSecurityCode(securityMarketFact,loggerUser);
				}*/
			}
			/**THROW EVENT TO REGISTER SECURITY WITH RATE AND PRICE ON VALUATOR_SETUP*/
//			marketFactEvent.fire(getMarketFactEvent(executionPk,securityCode,valuatorDate,marketRate,marketPrice,valuatorType,
//													operationID,marketDate,equivalentRat,economicTerm,valCodePk));
		}
	}
//	}

	/**
 * *
 * Method to get rate Or price according with instrument type .
 *
 * @param marketFactConsolidates the market fact consolidates
 * @param instrumentType the instrument type
 * @return the market date and rate or price
 * @throws ServiceException the service exception
 */
	private Object[] getMarketDateAndRateOrPrice(List<ValuatorMarketfactConsolidat> marketFactConsolidates, Integer instrumentType) throws ServiceException{

		String markFactActiveType = null;
		Integer marketFactType 	  = null;
		ValuatorMarketfactConsolidat marketFactConsolidat = null;
		
		if(instrumentType.equals(InstrumentType.VARIABLE_INCOME.getCode())){
			marketFactType = ValuatorMarkFactFileType.VARIABLE_FILE.getCode();
			marketFactConsolidat = getMarketFactConsolitade(marketFactConsolidates, marketFactType, markFactActiveType,null);
		}else if(instrumentType.equals(InstrumentType.FIXED_INCOME.getCode())){
			marketFactType = ValuatorMarkFactFileType.HISTORY_FILE.getCode();
			marketFactConsolidat = getMarketFactConsolitade(marketFactConsolidates, marketFactType, markFactActiveType,null);
			if(marketFactConsolidat==null){
				markFactActiveType = ValuatorMarkFactActiveType.AC.getCode();
				marketFactType = ValuatorMarkFactFileType.FIXED_FILE.getCode();
				marketFactConsolidat = getMarketFactConsolitade(marketFactConsolidates, marketFactType, markFactActiveType,null);
			}
		}
		
		
		if(marketFactConsolidat!=null){
			switch(ValuatorMarkFactFileType.get(marketFactType)){
				case  FIXED_FILE: 
							return new Object[]{marketFactConsolidat.getAverageRate(), marketFactConsolidat.getMarketfactDate(),BooleanType.NO.getCode()};
				case HISTORY_FILE:
					return new Object[]{marketFactConsolidat.getAverageRate(), marketFactConsolidat.getMarketfactDate(),BooleanType.YES.getCode()};
				case  VARIABLE_FILE:
							return new Object[]{marketFactConsolidat.getAveragePrice(), marketFactConsolidat.getMarketfactDate()};
				default:
					break;
			}
		}
		return null;
	}
	
	/**
	 * Gets the market fact consolitade.
	 *
	 * @param consolidates the consolidates
	 * @param marketFactType the market fact type
	 * @param valMarkType the val mark type
	 * @param securityCode the security code
	 * @return the market fact consolitade
	 * @throws ServiceException the service exception
	 */
	private ValuatorMarketfactConsolidat getMarketFactConsolitade(List<ValuatorMarketfactConsolidat> consolidates, Integer marketFactType, 
																  String valMarkType, String securityCode) throws ServiceException{
		for(ValuatorMarketfactConsolidat markFactConsolidat: consolidates){
			if(markFactConsolidat.getMarketfactType().equals(marketFactType)){
				if(markFactConsolidat.getMarketfactType().equals(ValuatorMarkFactFileType.HISTORY_FILE.getCode())){
					return markFactConsolidat;
				}else if(markFactConsolidat.getMarketfactType().equals(ValuatorMarkFactFileType.VARIABLE_FILE.getCode())){
					return markFactConsolidat;
				}
				if(valMarkType!=null && valMarkType.equals(markFactConsolidat.getMarketfactActive())){
					if(securityCode==null){
						return markFactConsolidat;
					}
					if(securityCode.equals(markFactConsolidat.getSecurityCode())){
						return markFactConsolidat;
					}
				}
			}
		}
		return null;
	}

	/**
	 * *
	 * Method to get key from any HashMap receiving like parameter the value.
	 *
	 * @param hashMap the hash map
	 * @param value the value
	 * @return the key from hash map
	 * @throws ServiceException the service exception
	 */
	public Long getKeyFromHashMap(ConcurrentMap<Long,String> hashMap, String value) throws ServiceException{
		if(hashMap!=null){
			for(Long key: hashMap.keySet()){
				if(hashMap.get(key).contains(value)){
					return key;
				}
			}
		}
		return null;
	}

	/**
	 * *
	 * Method to generate, verified and save Valuator code on database.
	 * 	
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @param newValuatorCode the new valuator code
	 * @param oldValuatorCode the old valuator code
	 * @param rangeObject the range object
	 * @param daysRange the days range
	 * @param valuatorSecurityCode the valuator security code
	 * @param mapValuatorCode the map valuator code
	 * @param processDate the process date
	 * @param loggerUser the logger user
	 * @return the valuator security code
	 * @throws ServiceException the service exception
	 */
	private ValuatorSecurityCode procesValuatorCode(String idSecurityCodePk,String newValuatorCode, Long oldValuatorCode, ValuatorTermRange rangeObject, 
								  				  	BigDecimal daysRange, ConcurrentMap<String,Long> valuatorSecurityCode, ConcurrentMap<Long,String> mapValuatorCode,
								  				  	Date processDate, LoggerUser loggerUser) throws ServiceException{
		/**VALIDATE IF VALUATOR CODE EXIST ON BD OR IN ANY SECURITY*/
		ValuatorCode valuatorCodeObject = null;
		String securityValuatorCode = null;
		if(oldValuatorCode==null){
			/**CREATE NEW VALUATOR OBJECT*/
			valuatorCodeObject =  securitiesService.createValuatorCode(newValuatorCode, mapValuatorCode,loggerUser);
		}else{
			/**REFERENCE VALUATOR CODE A ONE EXISTS*/
			valuatorCodeObject = new ValuatorCode(oldValuatorCode);
			securityValuatorCode = idSecurityCodePk.concat(oldValuatorCode.toString());
			/**If range code doesn't exist*/
			if(rangeObject!=null){
				securityValuatorCode = securityValuatorCode.concat(rangeObject.getIdTermRangePk().toString());
			}
		}
		
		if(securityValuatorCode!=null && !securityValuatorCode.isEmpty()){
			Long idValuatorSecurityPk = valuatorSecurityCode.get(securityValuatorCode);
			if(idValuatorSecurityPk!=null){
				return new ValuatorSecurityCode(idValuatorSecurityPk);
			}
		}
		
		/**GET TERM RANGE*/
		
		/**CREATE VALUATOR SECURITY CODE, TO HANDLE HISTORY*/
		ValuatorSecurityCode securityCode = new ValuatorSecurityCode();
		securityCode.setValuatorCode(valuatorCodeObject);
		securityCode.setSecurityCode(idSecurityCodePk);
		securityCode.setValuatorState(ValuatorSecurityCodState.REGISTERED.getCode());
		securityCode.setValuatorSituation(ValuatorSecurityCodSituation.CURRENT.getCode());
		securityCode.setInitialDate(processDate);
		securityCode.setValuatorTermRange(rangeObject);
		/**If this is different than null*/
		if(rangeObject!=null){
			securityCode.setFinalDate(CommonsUtilities.addDaysToDate(CommonsUtilities.currentDate(), daysRange.intValue()));
		}
		valuatorAditional.createValuatorSecurityCode(securityCode,loggerUser);
		return securityCode;
	}
	
	/**
	 * Gets the market fact event.
	 *
	 * @param execPk the execution pk
	 * @param secCode the security code
	 * @param processDate the market date
	 * @param markRate the market rate
	 * @param markPrice the market price
	 * @param valType the val type
	 * @param operation the operation
	 * @param marketDate the market date
	 * @param tre the tre
	 * @param economicTerm the economic term
	 * @param valCodPk the val cod pk
	 * @return the market fact event
	 */
//	private MarketFactForEvent getMarketFactEvent(Long execPk, String secCode, Date processDate, BigDecimal markRate, BigDecimal markPrice, 
//												  Integer valType, Long operation, Date marketDate, BigDecimal tre, BigDecimal economicTerm, Long valCodPk){
//		MarketFactForEvent marketFactEvent = new MarketFactForEvent();
//		marketFactEvent.setProcessDate(processDate);
//		marketFactEvent.setValuatorRate(tre);
//		marketFactEvent.setEconomicTerm(economicTerm);
//		marketFactEvent.setMarketDate(marketDate);
//		marketFactEvent.setValuatorExecutionId(execPk);
//		marketFactEvent.setMarketPrice(markPrice);
//		marketFactEvent.setMarketRate(markRate);
//		marketFactEvent.setMechanismOperationId(operation);
//		marketFactEvent.setSecurityCode(secCode);
//		marketFactEvent.setValuatorCodPk(valCodPk);
//		if(valType!=null && valType.equals(ValuatorSecMarkFactType.BY_MARKET.getCode())){
//			marketFactEvent.setIndHomolog(BooleanType.YES.getCode());
//		}else if(valType!=null && valType.equals(ValuatorSecMarkFactType.BY_SECURITY.getCode())){
//			marketFactEvent.setIndHomolog(BooleanType.NO.getCode());
//		}else{
//			marketFactEvent.setIndHomolog(BooleanType.NO.getCode());
//		}
//		return marketFactEvent;
//	}
	
	/**
	 * Register batch.
	 *
	 * @param details the details
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public void registerBatch(Map<String,Object> details) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser.getUserAction().getIdPrivilegeExecute()!=null){
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeExecute());
		}else{
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		}
		
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.VALUATOR_EXECUTION_CORE_PROCESS.getCode());
		try {
			batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(),businessProcess,details);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Register process execution.
	 *
	 * @param processExecution the process execution
	 * @param blValidate the bl validate
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public void registerProcessExecution(ValuatorProcessExecution processExecution, boolean blValidate) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser.getUserAction().getIdPrivilegeExecute()!=null){
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeExecute());
		}else{
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		}
		valuatorAditional.registerProcessExecution(processExecution,loggerUser,blValidate);
	}
	
	/**
	 * Validate securities in portfolly.
	 *
	 * @param parameters the parameters
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public void validateSecuritiesInPortfolly(List<ValuatorExecutionParameters> parameters) throws ServiceException{
		valuatorAditional.validateValuatorParameters(parameters,true);
	}
	

	/**
	 * *
	 * Method to register ProcessExecutionDetail with quantity of register that processed.
	 *
	 * @param _executionDetail the _execution detail
	 * @param participants the participants
	 * @param securities the securities
	 * @param accounts the accounts
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public void registerProcessExecutionDetail(List<ValuatorExecutionDetail> _executionDetail, List<Long> participants, List<String> securities, 
											   List<Long> accounts) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser.getUserAction().getIdPrivilegeExecute()!=null){
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeExecute());
		}else{
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		}
		
		/**Iterate execution details*/
		for(ValuatorExecutionDetail executionDetail: _executionDetail){
			
			/**if it's the main detail, assigment quantity*/
			if(executionDetail.getOrderExecution()!=null){
				/**CONFIG EXECUTION_DETAIL TO WILL BE PERSIST*/
				Long quantityProcess = 0L;
				/**PICK UP ACCOUNTING FOR ALL KIND OF ROWS*/
				if(executionDetail.getDetailType().equals(ValuatorDetailType.SECURITY.getCode())){
					if(securities==null || securities.isEmpty()){
						quantityProcess = securitiesService.findQuantitySecurities();
					}else{
						quantityProcess = (long) securities.size();
					}
				}else if(executionDetail.getDetailType().equals(ValuatorDetailType.BALANCE.getCode())){
				}else if(executionDetail.getDetailType().equals(ValuatorDetailType.OPERATIONS.getCode())){
				}
				
				executionDetail.setQuantityProcess(quantityProcess);
			}
			
			Long executionPk = executionDetail.getValuatorProcessExecution().getIdProcessExecutionPk();
			Long eventCodee = executionDetail.getValuatorEventBehavior().getIdValuatorEventPk();
			Integer detailType = executionDetail.getDetailType();
					
			if(valuatorAditional.getExecutionDetail(executionPk, eventCodee, detailType, null)==null){
				/**PERSIST EXECUTION DETAIL*/
				securitiesService.registerExecutionDetail(executionDetail);
			}
		}
	}
	
	
	/**
	 *  Metodo para registrar el valuatorPriceReport
	 */
	public void registerValuatorPriceReport (ValuatorPriceReport valuatorPriceReport) {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser.getUserAction().getIdPrivilegeExecute()!=null){
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeExecute());
		}else{
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		}
		try {
			securitiesService.registerValuatorPriceReport(valuatorPriceReport);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * *.
	 *
	 * @param processPk the process pk
	 * @param executionDetailPk the execution detail pk
	 * @param valuatorEventPk the valuator event pk
	 * @param hasError the has error
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public void finishValuatorExecutionDetail(Long processPk, Integer executionDetailPk, Long valuatorEventPk,boolean hasError) throws ServiceException{
		securitiesService.finishValuatorExecutionDetail(processPk, executionDetailPk, valuatorEventPk,hasError);
	}
	
	/**
	 * Finish valuator execution process.
	 *
	 * @param processPk the process pk
	 * @param errorMessage the error message
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public void finishValuatorExecutionProcess(Long processPk, String errorMessage) throws ServiceException{
		securitiesService.finishValuatorExecutionProcess(processPk, errorMessage);
	}
	
	/**
	 * Gets the holder account by cui.
	 *
	 * @param cuiCode the cui code
	 * @return the holder account by cui
	 * @throws ServiceException the service exception
	 */
	public List<Long> getHolderAccountByCui(List<Long> cuiCode) throws ServiceException{
		return null;
	}
	
	/**
	 * Find valuator process number.
	 *
	 * @param operationType the operation type
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public Long findValuatorProcessNumber(Long operationType) throws ServiceException{
		return securitiesService.findValuatorProcessNumber(operationType);
	}
	
	/**
	 * Gets the accounts from holder.
	 *
	 * @param holders the holders
	 * @return the accounts from holder
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public List<Long> getAccountsFromHolder(List<Long> holders) throws ServiceException{
		return securitiesService.getAccountsFromHolder(holders);
	}
	
	/**
	 * Gets the security code by execution.
	 *
	 * @param processPk the process pk
	 * @return the security code by execution
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public List<String> getSecurityCodeByExecution(Long processPk) throws ServiceException{
		return securitiesService.getSecurityCodeByExecution(processPk);
	}
	
	/**
	 * Gets the valuator event behavior.
	 *
	 * @return the valuator event behavior
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public List<ValuatorEventBehavior> getValuatorEventBehavior() throws ServiceException{
		return securitiesService.getValuatorEventBehavior();
	}
	
	/**
	 * Gets the process execution.
	 *
	 * @param executionPk the execution pk
	 * @return the process execution
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public ValuatorProcessExecution getProcessExecution(Long executionPk) throws ServiceException{
		return securitiesService.find(ValuatorProcessExecution.class, executionPk);
	}
	
	/**
	 * Gets the notification.
	 *
	 * @param eventCode the event code
	 * @param processPk the process pk
	 * @param detailType the detail type
	 * @param indBegin the ind begin
	 * @param indFinish the ind finish
	 * @param securityClass the security class
	 * @param rowQuantity the row quantity
	 * @param hasError the has error
	 * @param percentag the percentag
	 * @return the notification
	 */
//	public ValuatorNotificationEvent getNotification(Long eventCode, Long processPk, Integer detailType, Integer indBegin,Integer indFinish, 
//														  							Integer securityClass, Integer rowQuantity, Integer hasError, Integer percentag){
//		ValuatorNotificationEvent notificationEvent = new ValuatorNotificationEvent();
//		notificationEvent.setEventTypeCode(eventCode);
//		notificationEvent.setSecurityClass(securityClass);
//		notificationEvent.setRowQuantity(rowQuantity.longValue());
//		notificationEvent.setIndHasError(hasError);
//		notificationEvent.setPercentage(percentag);
//		if(indBegin.equals(BooleanType.YES.getCode())){
//			notificationEvent.setBeginTime(CommonsUtilities.currentDateTime());
//			notificationEvent.setIndBegin(BooleanType.YES.getCode());
//		}else if(indBegin.equals(BooleanType.NO.getCode())){
//			notificationEvent.setFinishTime(CommonsUtilities.currentDateTime());
//			notificationEvent.setIndFinish(BooleanType.YES.getCode());
//		}
//		
//		if(percentag==null){
//			/**Invoke method to save tracking of event by security*/
//			valuatorAditional.createSecurityClassEvent(eventCode, processPk, detailType, securityClass, indBegin, rowQuantity,hasError);
//		}
//		return notificationEvent;
//	}
	
	/**
	 * *
	 * Method to verify if marketfact has been loaded.
	 *
	 * @param processExecution the process execution
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public void verifyMarketFile(ValuatorProcessExecution processExecution) throws ServiceException{
		/**Getting date of process*/
		Date processDate = processExecution.getProcessDate();
		/**Verify if process day is holiday or not*/
		boolean isHoliday = holidayServiceBean.isNonWorkingDayServiceBean(processDate,BooleanType.YES.getBooleanValue());
		List<Integer> marketFactFileToReview = null;
		/**When is holiday only history is enough*/
		if(isHoliday){
			marketFactFileToReview = Arrays.asList(ValuatorMarkFactFileType.HISTORY_FILE.getCode());
		}else{
			marketFactFileToReview = Arrays.asList(ValuatorMarkFactFileType.HISTORY_FILE.getCode(),
												   ValuatorMarkFactFileType.VARIABLE_FILE.getCode(),
												   ValuatorMarkFactFileType.FIXED_FILE.getCode());
		}
		
		/**Getting list of files loaded in these day of process*/
		List<ValuatorMarketfactFile> marketFactFiles = valuatorAditional.verifyMarketFile(processExecution);
		/**Validate if  marketFact file has been loaded*/
		if(marketFactFiles.isEmpty()){
			throw new ServiceException(ErrorServiceType.VALUATOR_FILES_NOT_LOADED,new Object[]{CommonsUtilities.convertDatetoString(processDate)});
		}
		/**Verify if marketFact file has been loaded completely*/
		if(marketFactFiles.size() != marketFactFileToReview.size()){
			throw new ServiceException(ErrorServiceType.VALUATOR_FILES_INCOMPLETED,new Object[]{CommonsUtilities.convertDatetoString(processDate)});
		}
	}
	
	/**
	 * **
	 * Verified if begin end day is finished.
	 *
	 * @param processExecution the process execution
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public void verifyBeginEndDay(ValuatorProcessExecution processExecution) throws ServiceException{
		/**Variable to handle indicator like one*/
		Integer indYes = BooleanType.YES.getCode();
		/**Getting process date from process*/
		Date processDate = processExecution.getProcessDate();
		/**Getting begin end day*/
		BeginEndDay beginEndDay = valuatorAditional.searchBeginEnd(processDate);
		/**Verify if process day is holiday or not*/
		boolean isNotHoliday = !holidayServiceBean.isNonWorkingDayServiceBean(processDate,BooleanType.YES.getBooleanValue());
		if(isNotHoliday){
			/**Verify if begin end day is created*/
			if(beginEndDay == null){
				throw new ServiceException(ErrorServiceType.VALUATOR_BEGIN_DAY_NOT_CREATED,new Object[]{CommonsUtilities.convertDatetoString(processDate)});
			}
			/**Verify if begin end day has been closed*/
			if(beginEndDay.getIndSituation().equals(indYes)){
				throw new ServiceException(ErrorServiceType.VALUATOR_BEGIN_DAY_NOT_CLOSED,new Object[]{CommonsUtilities.convertDatetoString(processDate)});
			}
		}
	}
	
	/**
	 * Verify inconsistences operations.
	 *
	 * @param processExecution the process execution
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public void verifyInconsistencesOperations(ValuatorProcessExecution processExecution) throws ServiceException{
		/**Getting loggerUser*/
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		/**Indicator to know if process if for all portFolly*/
		Integer indAllPortFolly = processExecution.getIndAllPortFolly();
		/**If process is all portFolly begin process*/
		if(indAllPortFolly.equals(BooleanType.YES.getCode())){
			/**Verified inconsistencies in affectations*/
			List<ValuatorInconsistenciesTO> affectations = valuatorAditional.getAffectationInconsist();
			/**Verified inconsistencies in rebloking*/
			List<ValuatorInconsistenciesTO> rebloking = valuatorAditional.getReblokingInconsist();
			/**Verified inconsistencies in accreditation*/
			List<ValuatorInconsistenciesTO> accreditations = valuatorAditional.getAccreditationInconsist();
			/**Verified inconsistencies in reporting*/
			List<ValuatorInconsistenciesTO> reportings = valuatorAditional.getReportingInconsist(ComponentConstant.PURCHARSE_ROLE);
			/**Verified inconsistencies in reported*/
			List<ValuatorInconsistenciesTO> reporteds = valuatorAditional.getReportingInconsist(ComponentConstant.SALE_ROLE);
			/**Verify if exists inconsistencies*/
			if(!affectations.isEmpty() || !accreditations.isEmpty() || !reportings.isEmpty() || !reporteds.isEmpty() || !rebloking.isEmpty()){
				/**Message to empty list*/
				String emptyMessage = "";//ValuatorConstants.EMPTY_MESSAGE;
				
				/**Error to send message at user with all details*/
				List<Object> paramsError = new ArrayList<>();
				paramsError.add(affectations.isEmpty()?emptyMessage:Arrays.toString(affectations.toArray()));
				paramsError.add(accreditations.isEmpty()?emptyMessage:Arrays.toString(accreditations.toArray()));
				paramsError.add(reportings.isEmpty()?emptyMessage:Arrays.toString(reportings.toArray()));
				paramsError.add(reporteds.isEmpty()?emptyMessage:Arrays.toString(reporteds.toArray()));
				paramsError.add(rebloking.isEmpty()?emptyMessage:Arrays.toString(rebloking.toArray()));
				
				/**List to handle all inconsistencies*/
				List<ValuatorInconsistenciesTO> allInconsistencies = new ArrayList<>();
				allInconsistencies.addAll(affectations);
				allInconsistencies.addAll(accreditations);
				allInconsistencies.addAll(reportings);
				allInconsistencies.addAll(reporteds);
				allInconsistencies.addAll(rebloking);
				/**Save inconsistencies on database in another thread*/
				securitiesService.insertValuatorInconsistencies(allInconsistencies, processExecution,loggerUser);
				
				/**throw error and stop process*/
				throw new ServiceException(ErrorServiceType.VALUATOR_INCONSISTENCIES_OPERATION,paramsError.toArray());
			}
		}
	}
	

	public static void main(String[] args) {

//		String object = "abcdefgh";
//		int size = object.length();
//		char c[]=new char[size];
//		object.getChars(0, size, c, 0);
//		CharArrayReader r1 = new CharArrayReader(c);
//		CharArrayReader r2 = new CharArrayReader(c,1,4);
//		int i;
//		int j;
//		System.out.print("--------------------");
//		try{
//			while((i=r1.read())==(j=r2.read())){
//				System.out.print((char)i);
//			}
//		}catch(IOException e){
//			
//		}
//		permutiansBeging("abc");
		int []a = {3,4,5,6,7,8,9};
		int []b = {30,40,50,60,70,80,90};
		int m = 7;
		mergeArray(a, b, m);
		for(int i=0; i<2*m; i++){
			System.out.print(b[i]+" ");
		}
	}
	
	static void mergeArray(int []a, int []b, int size){
		List<Integer> list = new ArrayList<>();
		for(int i=0; i<a.length; i++){
			list.add(a[i]);
		}
		for(int i=0; i<b.length; i++){
			list.add(b[i]);
		}

		int []newArray = new int[list.size()];
		for(int i=0; i<list.size(); i++){
			newArray[i] = list.get(i); 
		}
		 b = newArray;
	}
	
	static void permutians(String word, String prefix){
		if(word.length()==0){
			System.out.print(prefix);
		}
		
		for(int i=0; i<word.length(); i++){
			char c = word.charAt(i);
			String l = word.substring(0,i);
			String r = word.substring(i+1);
			permutians(l+r, c+prefix);
		}
	}
	static void permutiansBeging(String word){
		permutians(word,"");
	}
	
	/**
	 * Metodo para actualizar el indicador ind_active en holderMarketfactBalance
	 * cuando el registro tiene todos los saldos en 0
	 */
	public void updateHolderMarketfactBalance(){
		securitiesService.updateHolderMarketfactBalance();
	}
	
	/**
	 * Metodo para buscar los valores en REPORTO para calculo de precio curva issue 1290
	 */
	public List<SecurityReportResultTO> searchSecurityReportForPriceCurveAGB(Date currenDate) {
		return securitiesService.searchSecurityReportForPriceCurveAGB(currenDate);
	}
	
	/**
	 * Metodo para buscar los valores en REPORTO para calculo de precio curva issue 1290
	 */
	public List<SecurityReportResultTO> searchSecurityReportForPriceCurveASFI(Date currenDate) {
		return securitiesService.searchSecurityReportForPriceCurveASFI(currenDate);
	}
	
	/**
	 * Metodo para buscar registros de valuator price report en la fecha
	 */
	public List<ValuatorPriceReport> searchValuatorPriceReportByCurrenDate(Date currenDate) {
		return securitiesService.searchValuatorPriceReportByCurrenDate(currenDate);
	}
	
	/**
	 * Metodo para borrar la tabla de VPReport por calculo de precio historico
	 */
	public void restartDataValuatorPricesReportHist() {
		 securitiesService.restartDataValuatorPricesReportHist();
	}
	
	/**
	 * Metodo para buscar registros de valuator price report en la fecha
	 */
	public Integer indHistoricPrice() {
		return securitiesService.indHistoricPrice();
	}
	
	
}