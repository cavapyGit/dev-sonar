package com.pradera.securities.valuator.core.files.facade;

import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.securities.valuator.core.files.service.BalancePsyService;


// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright EDV BOLIVIA 2016.</li>
 * </ul>
 * 
 * The Class BalancePsyFacade.
 *
 * @author OQUISBERT.
 * @version 1.0 , 07-OCT-2016
 */

@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class BalancePsyFacade {

	/** The transaction registry. */
	@Resource TransactionSynchronizationRegistry transactionRegistry;
	
	/** The market fact service. */
	@EJB
	BalancePsyService balancePsyService;
	
	@TransactionTimeout(unit=TimeUnit.HOURS,value=1)
	public void processDailyBalancePsy() throws ServiceException {
		LoggerUser objLoggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		balancePsyService.processDailyBalancePsy(objLoggerUser);
	}
}
