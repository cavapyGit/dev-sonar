package com.pradera.securities.valuator.classes.to;

import java.io.Serializable;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SecurityClassValuatorResultTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class SecurityClassValuatorResultTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	 
 	/**  *. */
	private Long idSecurityValuatorPk; 	
	
	/** Clase_valor*. */
	private Integer idSecurityClassFk;	
	
	/** id Valuator Class*. */
	private String idValuatorClass;
	
	/** The formule type. */
	private Integer formuleType;
	
	/** The registry date. */
	private String registryDate;
	
	/** The registry user. */
	private String registryUser;
	
	/** The class state. */
	private Integer classState;
	
	/** The text1. */
	private String text1;
	
	/** The description. */
	private String description;
	
	/** The instrument type. */
	private Integer instrumentType;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	
	/**
	 * Instantiates a new security class valuator result to.
	 */
	public SecurityClassValuatorResultTO() {
		super();
		// TODO Auto-generated constructor stub
	}
		
	/**
	 * Instantiates a new security class valuator result to.
	 *
	 * @param idSecurityValuatorPk the id security valuator pk
	 * @param idSecurityClassFk the id security class fk
	 * @param idValuatorClass the id valuator class
	 * @param formuleType the formule type
	 */
	public SecurityClassValuatorResultTO(Long idSecurityValuatorPk,
			Integer idSecurityClassFk,String idValuatorClass, Integer formuleType) {
		super();
		this.idSecurityValuatorPk = idSecurityValuatorPk;
		this.idSecurityClassFk=idSecurityClassFk;
		this.idValuatorClass = idValuatorClass;
		this.formuleType = formuleType;
	}

	/**
	 * Instantiates a new security class valuator result to.
	 *
	 * @param idSecurityClassFk the id security class fk
	 * @param registryDate the registry date
	 * @param text1 the text1
	 * @param description the description
	 */
	public SecurityClassValuatorResultTO(Integer idSecurityClassFk,
			String registryDate, String text1, String description) {
		super();
		this.idSecurityClassFk = idSecurityClassFk;
		this.registryDate = registryDate;
		this.text1 = text1;
		this.description = description;
	}
	
	/**
	 * Instantiates a new security class valuator result to.
	 *
	 * @param idSecurityClassFk the id security class fk
	 * @param registryDate the registry date
	 */
	public SecurityClassValuatorResultTO(Integer idSecurityClassFk,
			String registryDate) {
		super();
		this.idSecurityClassFk = idSecurityClassFk;
		this.registryDate = registryDate;
	}
	
	
	/**
	 * Instantiates a new security class valuator result to.
	 *
	 * @param idSecurityClassFk the id security class fk
	 * @param text1 the text1
	 * @param description the description
	 * @param instrumentType the instrument type
	 */
	public SecurityClassValuatorResultTO(Integer idSecurityClassFk,
			String text1, String description, Integer instrumentType) {
		super();
		this.idSecurityClassFk = idSecurityClassFk;
		this.text1 = text1;
		this.description = description;
		this.instrumentType = instrumentType;
	}

	/**
	 * Gets the id security valuator pk.
	 *
	 * @return the id security valuator pk
	 */
	public Long getIdSecurityValuatorPk() {
		return idSecurityValuatorPk;
	}
	
	/**
	 * Sets the id security valuator pk.
	 *
	 * @param idSecurityValuatorPk the new id security valuator pk
	 */
	public void setIdSecurityValuatorPk(Long idSecurityValuatorPk) {
		this.idSecurityValuatorPk = idSecurityValuatorPk;
	}
	
	/**
	 * Gets the id security class fk.
	 *
	 * @return the id security class fk
	 */
	public Integer getIdSecurityClassFk() {
		return idSecurityClassFk;
	}
	
	/**
	 * Sets the id security class fk.
	 *
	 * @param idSecurityClassFk the new id security class fk
	 */
	public void setIdSecurityClassFk(Integer idSecurityClassFk) {
		this.idSecurityClassFk = idSecurityClassFk;
	}
	
	/**
	 * Gets the formule type.
	 *
	 * @return the formule type
	 */
	public Integer getFormuleType() {
		return formuleType;
	}
	
	/**
	 * Sets the formule type.
	 *
	 * @param formuleType the new formule type
	 */
	public void setFormuleType(Integer formuleType) {
		this.formuleType = formuleType;
	}
	
	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public String getRegistryDate() {
		return registryDate;
	}
	
	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(String registryDate) {
		this.registryDate = registryDate;
	}
	
	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}
	
	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}
	
	/**
	 * Gets the class state.
	 *
	 * @return the class state
	 */
	public Integer getClassState() {
		return classState;
	}
	
	/**
	 * Sets the class state.
	 *
	 * @param classState the new class state
	 */
	public void setClassState(Integer classState) {
		this.classState = classState;
	}

	/**
	 * Gets the text1.
	 *
	 * @return the text1
	 */
	public String getText1() {
		return text1;
	}
	
	/**
	 * Sets the text1.
	 *
	 * @param text1 the new text1
	 */
	public void setText1(String text1) {
		this.text1 = text1;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}
	
	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	
	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}
	
	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}
	
	/**
	 * Gets the id valuator class.
	 *
	 * @return the id valuator class
	 */
	public String getIdValuatorClass() {
		return idValuatorClass;
	}
	
	/**
	 * Sets the id valuator class.
	 *
	 * @param idValuatorClass the new id valuator class
	 */
	public void setIdValuatorClass(String idValuatorClass) {
		this.idValuatorClass = idValuatorClass;
	}

	/**
	 * Gets the instrument type.
	 *
	 * @return the instrument type
	 */
	public Integer getInstrumentType() {
		return instrumentType;
	}

	/**
	 * Sets the instrument type.
	 *
	 * @param instrumentType the new instrument type
	 */
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}
		
}
