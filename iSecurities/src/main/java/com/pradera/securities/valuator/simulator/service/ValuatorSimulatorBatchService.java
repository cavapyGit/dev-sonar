package com.pradera.securities.valuator.simulator.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.issuancesecuritie.AmortizationPaymentSchedule;
import com.pradera.model.issuancesecuritie.InterestPaymentSchedule;
import com.pradera.model.issuancesecuritie.ProgramAmortizationCoupon;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.valuatorprocess.ValuatorProcessClass;
import com.pradera.model.valuatorprocess.ValuatorProcessSimulator;
import com.pradera.model.valuatorprocess.ValuatorSimulationBalance;
import com.pradera.model.valuatorprocess.ValuatorSimulatorSecurity;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorSimulatorBatchService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ValuatorSimulatorBatchService extends CrudDaoServiceBean{
	
	/**
	 * List valuator simulator.
	 *
	 * @param idProcessSimulator the id process simulator
	 * @return the map
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public Map<String,Object[]> listValuatorSimulator(Long idProcessSimulator) throws ServiceException{
		
		StringBuilder jpqlQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String,Object>();

		jpqlQuery.append(" 	SELECT s.idSecurityCodePk, vss.idNominalValue, vss.averateRate, sc.idValuatorCode ");
		jpqlQuery.append("	FROM ValuatorSimulatorSecurity vss ");
		jpqlQuery.append("	Inner join vss.valuatorSimulatorCode vsc ");
		jpqlQuery.append("  Inner join vsc.valuatorProcessSimulator vps ");
		jpqlQuery.append("  Inner join vss.security s ");
		jpqlQuery.append("  left join vsc.valuatorCode sc ");

		jpqlQuery.append("	WHERE vps.valuatorProcessOperation.idValuatorOperationPk = :idProcessSimulator	");
		
		parameters.put("idProcessSimulator", idProcessSimulator);
		List<Object[]> objectData = findListByQueryString(jpqlQuery.toString(), parameters);
		
		Map<String,Object[]> mapData = new HashMap<String,Object[]>();
		  
		for(Object[] data: objectData){
		   String securityCode = (String) data[0];
		   Integer indNominalValue = (Integer) data[1];
		   BigDecimal averateRate = (BigDecimal) data[2];
		   
		   String valuatorCode ="";
		   if (data[3]!=null){
			   valuatorCode = (String) data[3];
		   }else{
			   valuatorCode=null;
		   }
		   mapData.put(securityCode,new Object[] {indNominalValue,averateRate,valuatorCode});
		 }
		 return mapData;
	}

	/**
	 * List holder account balances.
	 *
	 * @param idProcessSimulator the id process simulator
	 * @param idParticipant the id participant
	 * @param idHolderAccount the id holder account
	 * @param idHolder the id holder
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccountBalance> listHolderAccountBalances (Long idProcessSimulator,Long idParticipant, Long idHolderAccount, Long idHolder){
		
		StringBuilder jpqlQuery = new StringBuilder();		
		jpqlQuery.append(" 	SELECT hab FROM HolderAccountBalance hab ");
		jpqlQuery.append("  Inner join hab.participant p ");
		jpqlQuery.append("  Inner join hab.holderAccount ha ");
		jpqlQuery.append("  Inner join hab.security s  ");
//		jpqlQuery.append("  Inner join s.lstValuatorSimulatorSecurity svss  ");
		
		jpqlQuery.append("	WHERE 1 = 1	");
		jpqlQuery.append(" 	AND s.idSecurityCodePk in (");
		jpqlQuery.append(" 	SELECT vss.security.idSecurityCodePk ");
		jpqlQuery.append("	FROM ValuatorSimulatorSecurity vss ");
		jpqlQuery.append("	Inner join vss.valuatorSimulatorCode vsc ");
		jpqlQuery.append("  Inner join vsc.valuatorProcessSimulator vps ");
		jpqlQuery.append("	WHERE vps.valuatorProcessOperation.idValuatorOperationPk = :idProcessSimulator	)");
		
		if (idParticipant!=null){
			jpqlQuery.append("	AND p.idParticipantPk = :idParticipant ");
		}
		if (idHolderAccount!=null){
			jpqlQuery.append(" 	AND ha.idHolderAccountPk = :idHolderAccount ");
		}
		if (idHolder!=null){
			jpqlQuery.append(" AND ha.idHolderAccountPk in (select had.holderAccount.idHolderAccountPk from HolderAccountDetail had where had.holder.idHolderPk = :idHolder) ");
		}
		
		Query queryJpql = em.createQuery(jpqlQuery.toString());		
		queryJpql.setParameter("idProcessSimulator", idProcessSimulator);
		if (idParticipant!=null){
			queryJpql.setParameter("idParticipant", idParticipant);
		}
		if (idHolderAccount!=null){
			queryJpql.setParameter("idHolderAccount", idHolderAccount);
		}
		if (idHolder!=null){
			queryJpql.setParameter("idHolder", idHolder);
		}
		
		List<HolderAccountBalance> lista = queryJpql.getResultList();
		return lista;
		
		
	}
	
	/**
	 * List simulator securities.
	 *
	 * @param idProcessSimulator the id process simulator
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<ValuatorSimulatorSecurity> listSimulatorSecurities (Long idProcessSimulator){
		
		StringBuilder jpqlQuery = new StringBuilder();		
		
		jpqlQuery.append(" 	SELECT vss ");
		jpqlQuery.append("	FROM ValuatorSimulatorSecurity vss ");
		jpqlQuery.append("	Inner join vss.valuatorSimulatorCode vsc ");
		jpqlQuery.append("  Inner join vsc.valuatorProcessSimulator vps ");
		jpqlQuery.append("  Inner join vss.security s ");
		jpqlQuery.append("  left join vsc.valuatorCode sc ");

		jpqlQuery.append("	WHERE vps.valuatorProcessOperation.idValuatorOperationPk = :idProcessSimulator	");
		
		Query queryJpql = em.createQuery(jpqlQuery.toString());		
		queryJpql.setParameter("idProcessSimulator", idProcessSimulator);
		List<ValuatorSimulatorSecurity> lista = queryJpql.getResultList();
		return lista;
		
	}
	
	
	/**
	 * List simulation balances.
	 *
	 * @param idProcessSimulator the id process simulator
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<ValuatorSimulationBalance> listSimulationBalances(Long idProcessSimulator){
		
		StringBuilder jpqlQuery = new StringBuilder();		
		
		jpqlQuery.append(" 	SELECT vsb ");
		jpqlQuery.append("	FROM ValuatorSimulationBalance vsb ");
		jpqlQuery.append("  Inner join vsb.valuatorProcessSimulator vps ");
		jpqlQuery.append("  Inner join vsb.holderAccountBalance hab");
		jpqlQuery.append("  Inner join hab.security s");
		jpqlQuery.append("	WHERE vps.valuatorProcessOperation.idValuatorOperationPk = :idProcessSimulator	");
		
		Query queryJpql = em.createQuery(jpqlQuery.toString());		
		queryJpql.setParameter("idProcessSimulator", idProcessSimulator);
		List<ValuatorSimulationBalance> lista = queryJpql.getResultList();
		return lista;
	}
	
	/**
	 * List object simulation balances.
	 *
	 * @param idProcessSimulator the id process simulator
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> listObjectSimulationBalances(Long idProcessSimulator){
		
		StringBuilder jpqlQuery = new StringBuilder();		
		jpqlQuery.append(" 	SELECT s.idSecurityCodePk,vsb.simulationRate  ");
		jpqlQuery.append("	FROM ValuatorSimulationBalance vsb ");
		jpqlQuery.append("  Inner join vsb.valuatorProcessSimulator vps ");
		jpqlQuery.append("  Inner join vsb.holderAccountBalance hab");
		jpqlQuery.append("  Inner join hab.security s");
		jpqlQuery.append("	WHERE vps.valuatorProcessOperation.idValuatorOperationPk = :idProcessSimulator	");
		
		Query queryJpql = em.createQuery(jpqlQuery.toString());		
		queryJpql.setParameter("idProcessSimulator", idProcessSimulator);
		
		List<Object[]> objectData = queryJpql.getResultList();
		return objectData;
		
	}
	
	/**
	 * Valuator process simulator.
	 *
	 * @param idProcessSimulator the id process simulator
	 * @return the valuator process simulator
	 */
	public ValuatorProcessSimulator valuatorProcessSimulator(Long idProcessSimulator){

		ValuatorProcessSimulator valuatorProcessSimulator = new ValuatorProcessSimulator();
		StringBuilder jpqlQuery = new StringBuilder();		
		jpqlQuery.append(" 	SELECT vps ");
		jpqlQuery.append("	FROM ValuatorProcessSimulator vps ");
		jpqlQuery.append("  Left join fetch vps.participant p ");
		jpqlQuery.append("  Left join fetch vps.holderAccount ha ");
		jpqlQuery.append("  Left join fetch vps.holder h ");
		jpqlQuery.append("	WHERE vps.valuatorProcessOperation.idValuatorOperationPk = :idProcessSimulator	");
		Query queryJpql = em.createQuery(jpqlQuery.toString());		
		queryJpql.setParameter("idProcessSimulator", idProcessSimulator);
		
		valuatorProcessSimulator = (ValuatorProcessSimulator)queryJpql.getSingleResult();
		return valuatorProcessSimulator;
	}
	
	/**
	 * Simulation security.
	 *
	 * @param idSecurityCode the id security code
	 * @return the security
	 */
	@SuppressWarnings("unchecked")
	public Security simulationSecurity(String  idSecurityCode){

		Security security = new Security();
		StringBuilder jpqlQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String,Object>();
		
		jpqlQuery.append(" 	SELECT vpc.idValuatorClassPk,vpc.formuleType ,s.issuanceDate,s.expirationDate,s.currentNominalValue,s.interestRate ");
		jpqlQuery.append("	FROM Security s ");
		jpqlQuery.append("  Inner join s.valuatorProcessClass vpc ");
		jpqlQuery.append("	WHERE s.idSecurityCodePk = :idSecurityCode	");
		
		parameters.put("idSecurityCode", idSecurityCode);
		List<Object[]> objectData = findListByQueryString(jpqlQuery.toString(), parameters);
		
		for(Object[] data: objectData){
			 ValuatorProcessClass valuatorProcessClass = new ValuatorProcessClass();
			 String idValuatorClassPk = (String) data[0];
			 Integer formuleType = (Integer) data[1];
			 valuatorProcessClass.setIdValuatorClassPk(idValuatorClassPk);
			 valuatorProcessClass.setFormuleType(formuleType);
			 
			 Date issuanceDate = (Date) data[2];
			 Date expirationDate = (Date)data[3];
			 BigDecimal currentNominalValue = (BigDecimal)data[4];
			 BigDecimal interestRate = (BigDecimal) data[5];
			 
			 security.setValuatorProcessClass(valuatorProcessClass);
			 security.setIssuanceDate(issuanceDate);
			 security.setExpirationDate(expirationDate);
			 security.setCurrentNominalValue(currentNominalValue);
			 security.setInterestRate(interestRate);
		}
		return security;
	}
	
	/**
	 * Interest payment schedule.
	 *
	 * @param idSecurityCode the id security code
	 * @param simulationDate the simulation date
	 * @return the interest payment schedule
	 */
	@SuppressWarnings("unchecked")
	public InterestPaymentSchedule interestPaymentSchedule(String  idSecurityCode, Date simulationDate){
		
		List<ProgramInterestCoupon> listInterestCoupons = new ArrayList<ProgramInterestCoupon>();
		
		/*
		StringBuilder jpqlQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String,Object>();
		
		jpqlQuery.append(" 	SELECT pic.experitationDate, pic.beginingDate, pic.couponNumber ");
		jpqlQuery.append("	FROM ProgramInterestCoupon pic ");
		jpqlQuery.append("  Inner join pic.interestPaymentSchedule ips ");
		jpqlQuery.append("  Inner join ips.security s ");
		jpqlQuery.append("	WHERE s.idSecurityCodePk = :idSecurityCode	");
		jpqlQuery.append("	and pic.paymentDate > :dateSimulation ");
		
		parameters.put("idSecurityCode", idSecurityCode);
		parameters.put("dateSimulation", simulationDate );
		List<Object[]> objectData = findListByQueryString(jpqlQuery.toString(), parameters);
		*/
		
		
		StringBuilder querySQL = new StringBuilder();
		
		querySQL.append("SELECT SEC.ID_SECURITY_CODE_PK , CUP.COUPON_NUMBER , CUP.BEGINING_DATE, 													");
		querySQL.append("       CUP.EXPERITATION_DATE, CUP.STATE_PROGRAM_INTEREST, CUP.COUPON_AMOUNT												");
		querySQL.append("FROM PROGRAM_INTEREST_COUPON CUP																							");
		querySQL.append("INNER JOIN INTEREST_PAYMENT_SCHEDULE SCHE ON SCHE.ID_INT_PAYMENT_SCHEDULE_PK = CUP.ID_INT_PAYMENT_SCHEDULE_FK			  	");
		querySQL.append("INNER JOIN SECURITY SEC ON SEC.ID_SECURITY_CODE_PK = SCHE.ID_SECURITY_CODE_FK											 	");
		querySQL.append("WHERE TRUNC(CUP.PAYMENT_DATE) > TRUNC(:valuatorDate) AND SEC.ID_SECURITY_CODE_PK IN (:securityCodes)						");
		querySQL.append("																															");
		querySQL.append("UNION ALL																													");
		querySQL.append("																															");
		querySQL.append("SELECT PROD_.ID_SECURITY_CODE_PK , CUP_.COUPON_NUMBER , CUP_.BEGINING_DATE, 												");
		querySQL.append("       CUP_.EXPERITATION_DATE, 0	,0																						");
		querySQL.append("FROM PROGRAM_INTEREST_COUPON CUP_																							");
		querySQL.append("INNER JOIN INTEREST_PAYMENT_SCHEDULE SCHE_ ON SCHE_.ID_INT_PAYMENT_SCHEDULE_PK = CUP_.ID_INT_PAYMENT_SCHEDULE_FK			");
		querySQL.append("INNER JOIN SECURITY SEC_ ON SEC_.ID_SECURITY_CODE_PK = SCHE_.ID_SECURITY_CODE_FK											");
		querySQL.append("INNER JOIN SECURITY PROD_ ON PROD_.ID_REF_SECURITY_CODE_FK = SEC_.ID_SECURITY_CODE_PK 										");
		querySQL.append("WHERE TRUNC(CUP_.PAYMENT_DATE) > TRUNC(:valuatorDate) AND PROD_.ID_SECURITY_CODE_PK IN (:securityCodes)					");
		querySQL.append("AND CUP_.COUPON_NUMBER NOT IN (																							");
		querySQL.append("   SELECT SB_CUP.COUPON_NUMBER																								");
		querySQL.append("   FROM PROGRAM_INTEREST_COUPON SB_CUP																						");
		querySQL.append("   INNER JOIN INTEREST_PAYMENT_SCHEDULE SB_SCHE ON SB_SCHE.ID_INT_PAYMENT_SCHEDULE_PK = SB_CUP.ID_INT_PAYMENT_SCHEDULE_FK	");
		querySQL.append("   INNER JOIN SECURITY SB_SEC ON SB_SEC.ID_SECURITY_CODE_PK = SB_SCHE.ID_SECURITY_CODE_FK 									");
		querySQL.append("   WHERE SB_SEC.ID_SECURITY_CODE_PK = PROD_.ID_SECURITY_CODE_PK)															");
		
		
		Query query = em.createNativeQuery(querySQL.toString());
		query.setParameter("valuatorDate", simulationDate);
		query.setParameter("securityCodes", idSecurityCode); 
		List<Object[]> objectData =query.getResultList();
		
		
		for(Object[] data: objectData){
			ProgramInterestCoupon interestCoupon = new ProgramInterestCoupon();
			Date experitationDate = (Date) data[3];
			Date beginingDate = (Date)data[2];
			Integer couponNumber = ((BigDecimal)data[1]).intValue();
			
			Integer stateProgramInterest = ((BigDecimal)data[4]).intValue();
			BigDecimal couponAmount = (BigDecimal)data[5];
			
			interestCoupon.setExperitationDate(experitationDate);
			interestCoupon.setBeginingDate(beginingDate);
			interestCoupon.setCouponNumber(couponNumber);
			
			interestCoupon.setStateProgramInterest(stateProgramInterest);
			interestCoupon.setCouponAmount(couponAmount);
			
			listInterestCoupons.add(interestCoupon);
		}
		
		InterestPaymentSchedule interestPaymentSchedule = new InterestPaymentSchedule();
		interestPaymentSchedule.setProgramInterestCoupons(listInterestCoupons);
		return interestPaymentSchedule;
		
	}
	
	/**
	 * Amortization payment schedule.
	 *
	 * @param idSecurityCode the id security code
	 * @param simulationDate the simulation date
	 * @return the amortization payment schedule
	 */
	@SuppressWarnings("unchecked")
	public AmortizationPaymentSchedule  amortizationPaymentSchedule(String  idSecurityCode, Date simulationDate){
		
		List<ProgramAmortizationCoupon>  listProgramAmortization = new ArrayList<ProgramAmortizationCoupon>();
		StringBuilder querySQL = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String,Object>();
		
		querySQL.append("Select schedul.security.idSecurityCodePk, amortCup.couponNumber,      ");
		querySQL.append("  amortCup.beginingDate, amortCup.expirationDate,        ");
		querySQL.append("  amortCup.couponAmount, corporat.oldNominalValue        ");
		querySQL.append("From ProgramAmortizationCoupon amortCup           ");
		querySQL.append("Inner Join amortCup.amortizationPaymentSchedule schedul       ");
		querySQL.append("Left  Join amortCup.corporativeOperation    corporat       ");
		querySQL.append("Where TRUNC(amortCup.paymentDate) > TRUNC(:valuatorDate)        ");
		querySQL.append("    And  schedul.security.idSecurityCodePk IN (:securityCodes)     ");
		querySQL.append("Order By schedul.security.idSecurityCodePk Asc, amortCup.couponNumber Asc   ");
		
		parameters.put("securityCodes", idSecurityCode);
		parameters.put("valuatorDate", simulationDate );
		
		List<Object[]> objectData = findListByQueryString(querySQL.toString(), parameters);
		for(Object[] data: objectData){
			ProgramAmortizationCoupon programAmortizationCoupon = new ProgramAmortizationCoupon();
//			String idSecurityCodePk = (String) data[0];
			Integer couponNumber = (Integer)data[1];
			Date beginingDate = (Date)data[2];
			Date expirationDate = (Date)data[3];
			BigDecimal couponAmount = (BigDecimal)data[4];
			BigDecimal oldNominalValue = (BigDecimal)data[5];
			
			programAmortizationCoupon.setCouponNumber(couponNumber);
			programAmortizationCoupon.setBeginingDate(beginingDate);
			programAmortizationCoupon.setExpirationDate(expirationDate);
			programAmortizationCoupon.setNominalValue(oldNominalValue);
			programAmortizationCoupon.setCouponAmount(couponAmount);
			
			listProgramAmortization.add(programAmortizationCoupon);
			
		}
		
		AmortizationPaymentSchedule amortizationPaymentSchedule = new AmortizationPaymentSchedule();
		amortizationPaymentSchedule.setProgramAmortizationCoupons(listProgramAmortization);
		
		return amortizationPaymentSchedule;
		
	}
	
	/**
	 * Save new list simulator balance.
	 *
	 * @param listSimulationBalance the list simulation balance
	 * @throws ServiceException the service exception
	 */
	public void saveNewListSimulatorBalance(List<ValuatorSimulationBalance> listSimulationBalance) throws ServiceException {
		for (ValuatorSimulationBalance balance : listSimulationBalance) {
			create(balance);
		}
	}
	
	/**
	 * Update price simulation balance.
	 *
	 * @param param the param
	 */
	public void updatePriceSimulationBalance(Object[] param){
		
		ValuatorSimulationBalance simulationBalance = new ValuatorSimulationBalance();
		StringBuilder jpqlQuery = new StringBuilder();		
		jpqlQuery.append(" SELECT vsb ");
		jpqlQuery.append(" FROM ValuatorSimulationBalance vsb ");
		jpqlQuery.append(" WHERE vsb.holderAccountBalance.security.idSecurityCodePk = :idSecurityCodePk");
		jpqlQuery.append(" and vsb.simulationRate = :simulationRate");
		jpqlQuery.append(" and vsb.valuatorProcessSimulator.valuatorProcessOperation.idValuatorOperationPk = :valuatorProcessSimulatorPk");
		
		
		Query queryJpql = em.createQuery(jpqlQuery.toString());		
		queryJpql.setParameter("idSecurityCodePk",param[0].toString());
		queryJpql.setParameter("simulationRate",(BigDecimal)param[1]);
		queryJpql.setParameter("valuatorProcessSimulatorPk",(Long)param[2]);
		
		simulationBalance = (ValuatorSimulationBalance)queryJpql.getSingleResult();
		
		simulationBalance.setSimulationPrice((BigDecimal)param[3]);
		em.merge(simulationBalance);
		
		/*
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update ValuatorSimulationBalance vsb ");
		sbQuery.append(" Set vsb.simulationPrice = :price ");
		sbQuery.append(" where vsb.holderAccountBalance.security.idSecurityCodePk = :idSecurityCodePk");
		sbQuery.append(" and vsb.simulationRate = :simulationRate");
		sbQuery.append(" and vsb.valuatorProcessSimulator.valuatorProcessOperation.idValuatorOperationPk = :valuatorProcessSimulatorPk");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSecurityCodePk",param[0].toString());
		query.setParameter("simulationRate",(BigDecimal)param[1]);
		query.setParameter("valuatorProcessSimulatorPk",(Long)param[2]);
		query.setParameter("price",(BigDecimal)param[3]);
		*/
		
		
	}
	

}
