package com.pradera.securities.valuator.core.view;

import java.io.Serializable;
import java.util.Date;

import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountBalancePK;
import com.pradera.model.issuancesecuritie.Security;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorExecutionFailedTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class ValuatorExecutionFailedTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id execution failed pk. */
	private Long idExecutionFailedPk;
	
	/** The motive. */
	private Integer motive;
	
	/** The registry date. */
	private Date registryDate;
	
	/** The registry user. */
	private String registryUser;
	
	/** The holder account balance. */
	private HolderAccountBalance holderAccountBalance;
	
	/** The valuator execution detail. */
	private ValuatorExecutionDetailTO valuatorExecutionDetail;

	/**
	 * Instantiates a new valuator execution failed to.
	 */
	public ValuatorExecutionFailedTO() {
	}
	
	/**
	 * Instantiates a new valuator execution failed to.
	 *
	 * @param idExecutionFailedPk the id execution failed pk
	 * @param idParticipantPk the id participant pk
	 * @param mnemonic the mnemonic
	 * @param accountNumber the account number
	 * @param idSecurityCodePk the id security code pk
	 * @param motive the motive
	 */
	public ValuatorExecutionFailedTO( Long idExecutionFailedPk, Long idParticipantPk,
			String mnemonic, Integer accountNumber, String idSecurityCodePk, Integer motive) {
		
		HolderAccountBalance holderAccountBalance = new HolderAccountBalance();
		
		HolderAccountBalancePK idHolderAccountBalance = new HolderAccountBalancePK();
		idHolderAccountBalance.setIdParticipantPk(idParticipantPk);
		idHolderAccountBalance.setIdSecurityCodePk(idSecurityCodePk);
		holderAccountBalance.setId(idHolderAccountBalance);
		
		Security security = new Security(idSecurityCodePk);
		Participant participant = new Participant(idParticipantPk, mnemonic);
		HolderAccount holderAccount = new HolderAccount();
		holderAccount.setAccountNumber(accountNumber);
		holderAccount.setHolder(new Holder());
		
		holderAccountBalance.setSecurity(security);
		holderAccountBalance.setParticipant(participant);
		holderAccountBalance.setHolderAccount(holderAccount);
		
		this.idExecutionFailedPk = idExecutionFailedPk;
		this.holderAccountBalance = holderAccountBalance;
		this.motive = motive;
	}
	
	/**
	 * Gets the id execution failed pk.
	 *
	 * @return the id execution failed pk
	 */
	public Long getIdExecutionFailedPk() {
		return this.idExecutionFailedPk;
	}

	/**
	 * Sets the id execution failed pk.
	 *
	 * @param idExecutionFailedPk the new id execution failed pk
	 */
	public void setIdExecutionFailedPk(Long idExecutionFailedPk) {
		this.idExecutionFailedPk = idExecutionFailedPk;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the holder account balance.
	 *
	 * @return the holder account balance
	 */
	public HolderAccountBalance getHolderAccountBalance() {
		return this.holderAccountBalance;
	}

	/**
	 * Sets the holder account balance.
	 *
	 * @param holderAccountBalance the new holder account balance
	 */
	public void setHolderAccountBalance(HolderAccountBalance holderAccountBalance) {
		this.holderAccountBalance = holderAccountBalance;
	}

	/**
	 * Gets the valuator execution detail.
	 *
	 * @return the valuator execution detail
	 */
	public ValuatorExecutionDetailTO getValuatorExecutionDetail() {
		return this.valuatorExecutionDetail;
	}

	/**
	 * Sets the valuator execution detail.
	 *
	 * @param valuatorExecutionDetail the new valuator execution detail
	 */
	public void setValuatorExecutionDetail(ValuatorExecutionDetailTO valuatorExecutionDetail) {
		this.valuatorExecutionDetail = valuatorExecutionDetail;
	}

	/**
	 * Gets the motive.
	 *
	 * @return the motive
	 */
	public Integer getMotive() {
		return motive;
	}

	/**
	 * Sets the motive.
	 *
	 * @param motive the new motive
	 */
	public void setMotive(Integer motive) {
		this.motive = motive;
	}
}