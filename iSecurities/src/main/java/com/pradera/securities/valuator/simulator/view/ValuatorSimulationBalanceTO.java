package com.pradera.securities.valuator.simulator.view;

import java.math.BigDecimal;

import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountBalancePK;
import com.pradera.model.issuancesecuritie.Security;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorSimulationBalanceTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class ValuatorSimulationBalanceTO {
	
	/** The valuator code. */
	private String valuatorCode;

	/** The holder account balance. */
	private HolderAccountBalance holderAccountBalance;
	
	/** The simulation rate. */
	private BigDecimal simulationRate;
	
	/**
	 * Instantiates a new valuator simulation balance to.
	 */
	public ValuatorSimulationBalanceTO() {}

	/**
	 * Instantiates a new valuator simulation balance to.
	 *
	 * @param valuatorCode the valuator code
	 * @param idSecurityCodePk the id security code pk
	 * @param idParticipantPk the id participant pk
	 * @param mnemonic the mnemonic
	 * @param idHolderAccountPk the id holder account pk
	 * @param accountNumber the account number
	 * @param description the description
	 * @param totalBalance the total balance
	 * @param availableBalance the available balance
	 */
	public ValuatorSimulationBalanceTO(String valuatorCode, String idSecurityCodePk,
			Long idParticipantPk, String mnemonic, Long idHolderAccountPk, Integer accountNumber,
			String description, BigDecimal totalBalance, BigDecimal availableBalance) {
		super();
		
		HolderAccountBalance holderAccountBalance = new HolderAccountBalance();
		
		HolderAccountBalancePK idHolderAccountBalance = new HolderAccountBalancePK();
		idHolderAccountBalance.setIdParticipantPk(idParticipantPk);
		idHolderAccountBalance.setIdHolderAccountPk(idHolderAccountPk);
		idHolderAccountBalance.setIdSecurityCodePk(idSecurityCodePk);
		holderAccountBalance.setId(idHolderAccountBalance);
		
		Security security = new Security(idSecurityCodePk);
		Participant participant = new Participant(idParticipantPk, mnemonic);
		HolderAccount holderAccount = new HolderAccount();
		holderAccount.setIdHolderAccountPk(idHolderAccountPk);
		holderAccount.setAccountNumber(accountNumber);
		holderAccount.setDescription(description);
		holderAccount.setHolder(new Holder());
		
		holderAccountBalance.setSecurity(security);
		holderAccountBalance.setParticipant(participant);
		holderAccountBalance.setHolderAccount(holderAccount);
		
		holderAccountBalance.setTotalBalance(totalBalance);
		holderAccountBalance.setAvailableBalance(availableBalance);
		
		this.valuatorCode = valuatorCode;
		this.holderAccountBalance = holderAccountBalance;
	}
	
	public ValuatorSimulationBalanceTO(String valuatorCode, String idSecurityCodePk,
			Long idParticipantPk, String mnemonic, Long idHolderAccountPk, Integer accountNumber,
			String description, HolderAccountBalance hab) {
		super();
		
		HolderAccount holderAccount = new HolderAccount();
		holderAccount.setIdHolderAccountPk(idHolderAccountPk);
		holderAccount.setAccountNumber(accountNumber);
		holderAccount.setDescription(description);
		holderAccount.setHolder(new Holder());
		
		this.valuatorCode = valuatorCode;
		this.holderAccountBalance = hab;
	}

	/**
	 * Instantiates a new valuator simulation balance to.
	 *
	 * @param valuatorCode the valuator code
	 * @param idSecurityCodePk the id security code pk
	 * @param idParticipantPk the id participant pk
	 * @param mnemonic the mnemonic
	 * @param idHolderAccountPk the id holder account pk
	 * @param accountNumber the account number
	 * @param description the description
	 * @param totalBalance the total balance
	 * @param availableBalance the available balance
	 * @param simulationRate the simulation rate
	 */
	public ValuatorSimulationBalanceTO(String valuatorCode, String idSecurityCodePk,
			Long idParticipantPk, String mnemonic, Long idHolderAccountPk, Integer accountNumber,
			String description, BigDecimal totalBalance, BigDecimal availableBalance, BigDecimal simulationRate) {
		this(valuatorCode,idSecurityCodePk,idParticipantPk,mnemonic,idHolderAccountPk,accountNumber,description,totalBalance,availableBalance);
		this.simulationRate = simulationRate;
	}
	
	/**
	 * Gets the valuator code.
	 *
	 * @return the valuator code
	 */
	public String getValuatorCode() {
		return valuatorCode;
	}

	/**
	 * Sets the valuator code.
	 *
	 * @param valuatorCode the new valuator code
	 */
	public void setValuatorCode(String valuatorCode) {
		this.valuatorCode = valuatorCode;
	}

	/**
	 * Gets the holder account balance.
	 *
	 * @return the holder account balance
	 */
	public HolderAccountBalance getHolderAccountBalance() {
		return holderAccountBalance;
	}

	/**
	 * Sets the holder account balance.
	 *
	 * @param holderAccountBalance the new holder account balance
	 */
	public void setHolderAccountBalance(HolderAccountBalance holderAccountBalance) {
		this.holderAccountBalance = holderAccountBalance;
	}

	/**
	 * Gets the simulation rate.
	 *
	 * @return the simulation rate
	 */
	public BigDecimal getSimulationRate() {
		return simulationRate;
	}

	/**
	 * Sets the simulation rate.
	 *
	 * @param simulationRate the new simulation rate
	 */
	public void setSimulationRate(BigDecimal simulationRate) {
		this.simulationRate = simulationRate;
	}
}