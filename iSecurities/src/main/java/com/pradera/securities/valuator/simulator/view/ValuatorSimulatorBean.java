
package com.pradera.securities.valuator.simulator.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

//import org.jboss.solder.exception.control.ExceptionToCatch;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.to.SecuritiesSearcherTO;
import com.pradera.core.component.helperui.view.SecuritiesPartHolAccount;
import com.pradera.core.component.helperui.view.SecurityHelpBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.valuatorprocess.ValuatorProcessSimulator;
import com.pradera.model.valuatorprocess.type.ValuatorSimulatorForm;
import com.pradera.model.valuatorprocess.type.ValuatorSimulatorType;
import com.pradera.securities.issuancesecuritie.util.PropertiesConstants;
import com.pradera.securities.valuator.simulator.facade.ValuatorSimulatorFacade;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorSimulatorBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@DepositaryWebBean
public class ValuatorSimulatorBean extends GenericBaseBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant REGISTER_SIMULATOR. */
	private static final String REGISTER_SIMULATOR = "registerValuatorSimulator";
	
	/** The Constant SEARCH_SIMULATOR. */
	private static final String SEARCH_SIMULATOR = "searchSimulator";
	
	/** The Constant DETAIL_SIMULATION. */
	private static final String DETAIL_SIMULATION = "detailSimulation";
	
	/** The valuator simulator facade. */
	@EJB private ValuatorSimulatorFacade valuatorSimulatorFacade;
	
	/** The general parameters facade. */
	@EJB private GeneralParametersFacade generalParametersFacade;
	
	/** The valuator process simulator. */
	private ValuatorProcessSimulator valuatorProcessSimulator;
	
	/** The valuator simulator to. */
	private ValuatorProcessSimulatorTO valuatorSimulatorTO;
	
	/** The valuator simulator selected. */
	private ValuatorProcessSimulatorTO valuatorSimulatorSelected;
	
	/** The lst state. */
	private List<ParameterTable> lstState;
	
	/** The participant list register. */
	private List<Participant> participantListRegister;
	
	/** The lst account register. */
	private List<HolderAccount> lstAccountRegister;
	
	/** The selected account register. */
	private HolderAccount selectedAccountRegister;
	
	/** The lst state search. */
	private List<ParameterTable> lstStateSearch;
	
	/** The participant list search. */
	private List<Participant> participantListSearch;
	
	/** The lst account search. */
	private List<HolderAccount> lstAccountSearch;
	
	/** The selected account search. */
	private HolderAccount selectedAccountSearch;
	
	/** The parameter simulation form. */
	private Map<Integer, String> parameterSimulationForm;
	
	/** The parameter process state. */
	private Map<Integer, String> parameterProcessState;
	
	/** The participant logout. */
	private Long participantLogout;
	
	/** The lst requester user types. */
	private List<ParameterTable> lstRequesterUserTypes;
	
	/** The valuator data model. */
	private GenericDataModel<ValuatorProcessSimulatorTO> valuatorDataModel;
	
	/** The valuator data model code. */
	private GenericDataModel<ValuatorSimulatorCodeTO> valuatorDataModelCode;
	
	/** The valuator data model balance. */
	private GenericDataModel<ValuatorSimulationBalanceTO> valuatorDataModelBalance;
	
	/** The simulation form. */
	private Map<Integer,String> simulationForm = new HashMap<Integer, String>();
	
	/** The detail simulation to model. */
	private GenericDataModel<DetailSimulationTO> detailSimulationTOModel;
	

	/** The user info. */
	@Inject private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject private UserPrivilege userPrivilege;
	
	/** The depositary setup. */
	@Inject @DepositarySetup IdepositarySetup depositarySetup;
	
	/** The securities part hol account. */
	@Inject SecuritiesPartHolAccount securitiesPartHolAccount;
	
	/** The parameter service. */
	@EJB
	private ParameterServiceBean parameterService;
	
	/** The security help bean. */
	@Inject
	SecurityHelpBean securityHelpBean;

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		loadPriviligies();
		cleanSearchForm();
		simulationFormType();
	}

	/**
	 * Load participant privileges.
	 */
	private void loadParticipantPrivileges() {
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			participantLogout = userInfo.getUserAccountSession().getParticipantCode();
			valuatorSimulatorTO.getParticipant().setIdParticipantPk(participantLogout);
		}
	}
	
	/**
	 * Load priviligies.
	 */
	private void loadPriviligies() {
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}

	/**
	 * New valuator process simulator.
	 *
	 * @return the string
	 */
	public String newValuatorProcessSimulator(){
		setViewOperationType(ViewOperationsType.REGISTER.getCode());
		cleanRegister();
		return REGISTER_SIMULATOR;
	}
	
	/**
	 * Clean register.
	 */
	public void cleanRegister(){
		JSFUtilities.resetViewRoot();
		try {
			loadSimulatorForm();
			loadAllParticipant();
			createObjectValuatorProcessSimulator();
			loadRequesterUserTypes();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		valuatorProcessSimulator.setRegistryDate(CommonsUtilities.currentDateTime());
	}
	
	/**
	 * Gets the state account name.
	 *
	 * @param idStateAccount the id state account
	 * @return the state account name
	 */
	public String getStateAccountName(Integer idStateAccount){
		return HolderAccountStatusType.get(idStateAccount).getValue();
	}
	
	/**
	 * Gets the simulation form description.
	 *
	 * @return the simulation form description
	 */
	public String getSimulationFormDescription(){
		return ValuatorSimulatorForm.get(valuatorProcessSimulator.getSimulationForm()).getDescription();
	}
	
	/**
	 * Simulation form type.
	 */
	private void simulationFormType(){
		simulationForm = new HashMap<Integer, String>();
		ParameterTableTO  filter = new ParameterTableTO();
		filter.setMasterTableFk(MasterTableType.SIMULATOR_SIMULATION_FORMS.getCode());
		for(ParameterTable param : parameterService.getListParameterTableServiceBean(filter)) {
			simulationForm.put(param.getParameterTablePk(), param.getDescription());
		}
	}
	
	/**
	 * Gets the simulation form label for no results.
	 *
	 * @return the simulation form label for no results
	 */
	public String getSimulationFormLabelForNoResults(){
		String messageProperties = "";
		Integer simulationForm = null;
		if(valuatorProcessSimulator.getSimulationForm() != null)
			simulationForm = valuatorProcessSimulator.getSimulationForm();
		
		switch(ValuatorSimulatorForm.get(simulationForm)){
			case BALANCE: 			messageProperties = PropertiesConstants.VALUATOR_PROCESS_SIMULATION_BALANCE_NO_RESULTS;
									break;
			case BY_VALUATION_CODE: messageProperties = PropertiesConstants.VALUATOR_PROCESS_SIMULATION_CODE_NO_RESULTS;
									break;
			default:				break;
		}
		
		return PropertiesUtilities.getMessage(messageProperties);
	}
	
	/**
	 * Creates the object valuator process simulator.
	 */
	public void createObjectValuatorProcessSimulator(){
		valuatorProcessSimulator = new ValuatorProcessSimulator();
		valuatorProcessSimulator.setParticipant(new Participant());
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			valuatorProcessSimulator.getParticipant().setIdParticipantPk(participantLogout);
		}
		valuatorProcessSimulator.setHolder(new Holder());
		valuatorProcessSimulator.setHolderAccount(new HolderAccount());
		valuatorProcessSimulator.setSecurity(new Security());
		lstAccountRegister = new ArrayList<HolderAccount>();
		selectedAccountRegister = null;
	}

	/**
	 * Load all participant.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadAllParticipant() throws ServiceException {
		Participant participantTO = new Participant();
		List<Participant> list = valuatorSimulatorFacade.getLisParticipantServiceBean(participantTO);
		if(getViewOperationType() == null)
			this.setParticipantListSearch(list);
		else if( getViewOperationType().equals(ViewOperationsType.REGISTER.getCode())
				|| getViewOperationType().equals(ViewOperationsType.DETAIL.getCode()) )
			this.setParticipantListRegister(list);
	}
	
	/**
	 * Load simulator form.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadSimulatorForm() throws ServiceException{
		parameterSimulationForm = new HashMap<Integer, String>();
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.SIMULATOR_SIMULATION_FORMS.getCode());
		List<ParameterTable> list = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		for (ParameterTable parameterTable : list) {
			parameterSimulationForm.put(parameterTable.getParameterTablePk(), parameterTable.getParameterName());
		}
		if(getViewOperationType() == null)
			this.setLstStateSearch(list);
		else if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode())
				|| getViewOperationType().equals(ViewOperationsType.DETAIL.getCode()) )
			this.setLstState(list);
	}
	
	/**
	 * Load process state.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadProcessState() throws ServiceException{
		parameterProcessState = new HashMap<Integer, String>();
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.SIMULATOR_SIMULATION_PROCESS_STATES.getCode());
		List<ParameterTable> list = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		for (ParameterTable parameterTable : list) {
			parameterProcessState.put(parameterTable.getParameterTablePk(), parameterTable.getParameterName());
		}
	}
	
	/**
	 * Load requester user types.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadRequesterUserTypes() throws ServiceException{
		parameterProcessState = new HashMap<Integer, String>();
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.SIMULATOR_SIMULATION_REQUESTER_USER_TYPES.getCode());
		List<ParameterTable> list = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		setLstRequesterUserTypes(list);
		
		if( getViewOperationType().equals(ViewOperationsType.REGISTER.getCode())){
			Integer institutionType = userInfo.getUserAccountSession().getInstitutionType();
			for (ParameterTable parameterTable : list) {
				if(InstitutionType.get(institutionType).getValue().equals(parameterTable.getDescription()))
					valuatorProcessSimulator.setRequesterType(parameterTable.getParameterTablePk());
			}
		}
	}
	
	/**
	 * Clean search form.
	 */
	public void cleanSearchForm() {
		JSFUtilities.resetViewRoot();
		try {
			loadSimulatorForm();
			loadProcessState();
			loadAllParticipant();
			createObjectValuatorSimulatorTO();
			loadParticipantPrivileges();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Creates the object valuator simulator to.
	 */
	public void createObjectValuatorSimulatorTO(){
		valuatorSimulatorTO = new ValuatorProcessSimulatorTO();
		valuatorSimulatorTO.setSimulationInitialDate(CommonsUtilities.currentDateTime());
		valuatorSimulatorTO.setSimulationFinalDate(CommonsUtilities.currentDateTime());
		valuatorSimulatorTO.setParticipant(new Participant());
		valuatorSimulatorTO.setHolder(new Holder());
		valuatorSimulatorTO.setHolderAccount(new HolderAccount());
		valuatorSimulatorTO.setSecurity(new Security());
		
		valuatorDataModel = null;
		selectedAccountSearch = null;
		valuatorSimulatorSelected = null;
		lstAccountSearch = new ArrayList<HolderAccount>();
	}
	
	/**
	 * Back form.
	 *
	 * @return the string
	 */
	public String backForm(){
		setViewOperationType(null);
		return SEARCH_SIMULATOR;
	}
	
	/**
	 * Back form deatil.
	 *
	 * @return the string
	 */
	public String backFormDeatil(){
		return SEARCH_SIMULATOR;
	}
	
	/**
	 * Change participant handler register.
	 */
	public void changeParticipantHandlerRegister(){
		if(valuatorProcessSimulator.getParticipant() == null)
			valuatorProcessSimulator.setParticipant(new Participant());
		valuatorProcessSimulator.setHolder(new Holder());
		valuatorProcessSimulator.setHolderAccount(new HolderAccount());
		valuatorProcessSimulator.setSecurity(new Security());
		lstAccountRegister = new ArrayList<HolderAccount>();
		selectedAccountRegister = null;
		cleanSearchValuatorSimulatorSecurities();
	}
	
	/**
	 * Change participant handler search.
	 */
	public void changeParticipantHandlerSearch(){
		valuatorSimulatorTO.setHolder(new Holder());
		valuatorSimulatorTO.setHolderAccount(new HolderAccount());
		valuatorSimulatorTO.setSecurity(new Security());
		lstAccountSearch = new ArrayList<HolderAccount>();
		selectedAccountSearch = null;
	}
	
	/**
	 * Search holder handler search.
	 *
	 * @throws ServiceException the service exception
	 */
	public void searchHolderHandlerSearch() throws ServiceException {	
		try {
			executeAction();
			JSFUtilities.hideGeneralDialogues();
			valuatorSimulatorTO.setHolderAccount(new HolderAccount());
			valuatorSimulatorTO.setSecurity(new Security());
			lstAccountSearch = null;
			if(valuatorSimulatorTO.getHolder() == null)
				valuatorSimulatorTO.setHolder(new Holder());
			Long idHolderPk = this.valuatorSimulatorTO.getHolder().getIdHolderPk();
			if(valuatorSimulatorTO.getParticipant() == null)
				valuatorSimulatorTO.setParticipant(new Participant());
			Long idParticipantPk = valuatorSimulatorTO.getParticipant().getIdParticipantPk();
			selectedAccountSearch = null;
			securitiesPartHolAccount.setDescription(null);
			
			if (Validations.validateIsNotNull(idHolderPk)) {
				lstAccountSearch = valuatorSimulatorFacade.getAccountsHolderAndParticipant(idHolderPk, idParticipantPk);
				
				if(Validations.validateListIsNullOrEmpty(lstAccountSearch)){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
							PropertiesUtilities.getGenericMessage(PropertiesConstants.COMMONS_LABEL_PARTICIPANT_CUI_INCORRECT));
					valuatorSimulatorTO.setHolderAccount(new HolderAccount());
					JSFUtilities.showSimpleValidationDialog();
				} else {
					fillDataParametersAccountTO(lstAccountSearch);
				}
			}
		} catch (Exception e) {
			//excepcion.fire(new ExceptionToCatch(e));
			e.printStackTrace();
		}
	}
	
	/**
	 * Search holder handler register.
	 *
	 * @throws ServiceException the service exception
	 */
	public void searchHolderHandlerRegister() throws ServiceException {	
		try {
			executeAction();
			JSFUtilities.hideGeneralDialogues();
			valuatorProcessSimulator.setHolderAccount(new HolderAccount());
			valuatorProcessSimulator.setSecurity(new Security());
			lstAccountRegister = null;
			Long idHolderPk = this.valuatorProcessSimulator.getHolder().getIdHolderPk();
			Long idParticipantPk = valuatorProcessSimulator.getParticipant().getIdParticipantPk();
			selectedAccountRegister = null;
			securitiesPartHolAccount.setDescription(null);
			
			if (Validations.validateIsNotNull(idHolderPk)) {
				lstAccountRegister = valuatorSimulatorFacade.getAccountsHolderAndParticipant(idHolderPk, idParticipantPk);
				
				if(Validations.validateListIsNullOrEmpty(lstAccountRegister)){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
							PropertiesUtilities.getGenericMessage(PropertiesConstants.COMMONS_LABEL_PARTICIPANT_CUI_INCORRECT));
					valuatorProcessSimulator.setHolderAccount(new HolderAccount());
					JSFUtilities.showSimpleValidationDialog();
				} else {
					fillDataParametersAccountTO(lstAccountRegister);
				}
			}
		} catch (ServiceException e) {
			//excepcion.fire(new ExceptionToCatch(e));
			e.printStackTrace();
		}
		cleanSearchValuatorSimulatorSecurities();
	}
	
	/**
	 * Fill data parameters account to.
	 *
	 * @param lstAccount the lst account
	 * @throws ServiceException the service exception
	 */
	public void fillDataParametersAccountTO(List<HolderAccount> lstAccount) throws ServiceException{
		List<ParameterTable> lstHolderAccountState = generalParametersFacade.getListParameterTableServiceBean(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
		List<HolderAccount> lstAccountAux = lstAccount;
		for (HolderAccount objAccount : lstAccountAux) {
			for (ParameterTable parameterTable : lstHolderAccountState) {
				if(objAccount.getIdHolderAccountPk().equals(Long.parseLong(parameterTable.getParameterTablePk().toString()))){
					objAccount.setDescription(parameterTable.getParameterName());
					break;
				}
			}
		}
	}
	
	/**
	 * Change select account register.
	 */
	public void changeSelectAccountRegister(){
		valuatorProcessSimulator.setHolderAccount(new HolderAccount());
		if(selectedAccountRegister == null)
			selectedAccountRegister = new HolderAccount();
		valuatorProcessSimulator.getHolderAccount().setIdHolderAccountPk(selectedAccountRegister.getIdHolderAccountPk());
		valuatorProcessSimulator.getHolderAccount().setAccountNumber(selectedAccountRegister.getAccountNumber());
		valuatorProcessSimulator.setSecurity(new Security());
		valuatorProcessSimulator.getSecurity().setIdSecurityCodePk(null);
		securitiesPartHolAccount.setDescription(null);
		SecuritiesSearcherTO securitiesSearcherTO = new SecuritiesSearcherTO();
		if(valuatorProcessSimulator.getParticipant() != null)
			securitiesSearcherTO.setParticipantCode(valuatorProcessSimulator.getParticipant().getIdParticipantPk());
		securitiesSearcherTO.setHolderAccount(valuatorProcessSimulator.getHolderAccount());
		securitiesPartHolAccount.setSecuritiesSearcher(securitiesSearcherTO);
		cleanSearchValuatorSimulatorSecurities();
	}
	
	/**
	 * Change select account search.
	 */
	public void changeSelectAccountSearch(){
		valuatorSimulatorTO.setHolderAccount(new HolderAccount());
		if(selectedAccountSearch == null)
			selectedAccountSearch = new HolderAccount();
		valuatorSimulatorTO.getHolderAccount().setIdHolderAccountPk(selectedAccountSearch.getIdHolderAccountPk());
		valuatorSimulatorTO.getHolderAccount().setAccountNumber(selectedAccountSearch.getAccountNumber());
		valuatorSimulatorTO.setSecurity(new Security());
		valuatorSimulatorTO.getSecurity().setIdSecurityCodePk(null);
		securitiesPartHolAccount.setDescription(null);
		SecuritiesSearcherTO securitiesSearcherTO = new SecuritiesSearcherTO();
		if(valuatorSimulatorTO.getParticipant() != null)
			securitiesSearcherTO.setParticipantCode(valuatorSimulatorTO.getParticipant().getIdParticipantPk());
		securitiesSearcherTO.setHolderAccount(valuatorSimulatorTO.getHolderAccount());
		securitiesPartHolAccount.setSecuritiesSearcher(securitiesSearcherTO);
	}
	
	/**
	 * Before save process simulator.
	 */
	public void beforeSaveProcessSimulator(){
		String messageProperties = "";
		Boolean haveValidation = Boolean.FALSE;
		
		switch(ViewOperationsType.get(getViewOperationType())){
			case REGISTER: 	messageProperties = PropertiesConstants.VALUATOR_PROCESS_SIMULATION_REGISTER_CONFIRM;
							break;
			default:		break;
		}
		if(isBalanceSimulationForm())
			haveValidation = validateListSimulationBalance();
		
		if(isByValuationCodeSimulationForm())
			haveValidation = validateListSimulationCode();
		
		if(!haveValidation){
			Long idProcessSimulatorPk = valuatorProcessSimulator.getIdValuatorSimulatorPk();
			String valuatorProcessSimulator = null;
			if(idProcessSimulatorPk!=null){
				valuatorProcessSimulator = idProcessSimulatorPk.toString();
			}
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(
					GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(messageProperties,valuatorProcessSimulator));
			JSFUtilities.executeJavascriptFunction("PF('cnfwValuatorProcessSimulator').show()");
		}
	}
	
	/**
	 * Validate list simulation balance.
	 *
	 * @return the boolean
	 */
	public Boolean validateListSimulationBalance(){		
		Boolean haveValidation = false;
		String propertieKey = null;
		
		if(valuatorDataModelBalance!=null){
			if(valuatorDataModelBalance.getDataList().isEmpty()){
				propertieKey = ErrorServiceType.VALUATOR_SIMULATION_EMPTY_LIST_RATE.getMessage();
				haveValidation = true;
			}else{
				for (ValuatorSimulationBalanceTO to : valuatorDataModelBalance.getDataList()) {
					if(to.getSimulationRate()==null){
						propertieKey = ErrorServiceType.VALUATOR_SIMULATION_EMPTY_VALUES_BALANCE_LIST_RATE.getMessage();
						haveValidation = true;
						break;
					}
				}
			}
			if(haveValidation){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(
						GeneralConstants.LBL_HEADER_ALERT_ERROR),
						PropertiesUtilities.getExceptionMessage(propertieKey));
				JSFUtilities.showSimpleValidationAltDialog();
			}
		}
		return haveValidation;
	}
	
	/**
	 * Validate list simulation code.
	 *
	 * @return the boolean
	 */
	public Boolean validateListSimulationCode(){		
		Boolean haveValidation = false;
		String propertieKey = null;
		
		if(valuatorDataModelCode!=null){
			if(valuatorDataModelCode.getDataList().isEmpty()){
				propertieKey = ErrorServiceType.VALUATOR_SIMULATION_EMPTY_LIST_RATE.getMessage();
				haveValidation = true;
			}else{
				for (ValuatorSimulatorCodeTO to : valuatorDataModelCode.getDataList()) {
					if(to.getAverageRate() == null){
						propertieKey = ErrorServiceType.VALUATOR_SIMULATION_EMPTY_VALUES_CODE_LIST_RATE.getMessage();
						haveValidation = true;
						break;
					}
				}
			}
			if(haveValidation){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(
						GeneralConstants.LBL_HEADER_ALERT_ERROR),
						PropertiesUtilities.getExceptionMessage(propertieKey));
				JSFUtilities.showSimpleValidationAltDialog();
			}
		}
		return haveValidation;
	}
	
	/**
	 * Synchronize rates.
	 *
	 * @param codeTO the code to
	 */
	public void synchronizeRates(ValuatorSimulatorCodeTO codeTO){
		if(codeTO.getSecurityTO() != null && codeTO.getAverageRate() != null){
			for (ValuatorSimulatorSecurityTO codeListTo : codeTO.getSecurityTO().getDataList()) {
				if(!codeListTo.getValueNominalRate())
					codeListTo.setAverateRate(codeTO.getAverageRate());
			}
		}
	}
	
	/**
	 * Change rates.
	 *
	 * @param SecurityTO the security to
	 * @param averageRate the average rate
	 */
	public void changeRates(ValuatorSimulatorSecurityTO SecurityTO, BigDecimal averageRate){
		if(SecurityTO.getValueNominalRate())
			SecurityTO.setAverateRate(SecurityTO.getInterestRate());
		else
			SecurityTO.setAverateRate(averageRate);
	}
	
	/**
	 * Save new process simulator.
	 */
	@LoggerAuditWeb
	public void saveNewProcessSimulator(){
		try{
			switch(ViewOperationsType.get(getViewOperationType())){
				case REGISTER: 	registerProcessSimulator();break;
				default:		break;
			}
		}catch(ServiceException ex){
			if(ex.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(
						GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getExceptionMessage(
								ex.getErrorService().getMessage(),
								ex.getParams()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
		JSFUtilities.showSimpleEndTransactionDialog(SEARCH_SIMULATOR);
		searchProcessSimulators();
		valuatorSimulatorSelected = null;
	}
	
	/**
	 * Validate simulation type.
	 */
	public void validateSimulationType(){
		if(( valuatorProcessSimulator.getHolder() == null
				|| valuatorProcessSimulator.getHolder().getIdHolderPk() == null )
				&& ( valuatorProcessSimulator.getHolderAccount() == null
				|| valuatorProcessSimulator.getHolderAccount().getIdHolderAccountPk() == null )
				&& ( valuatorProcessSimulator.getSecurity() == null
				|| valuatorProcessSimulator.getSecurity().getIdSecurityCodePk() == null )){
			valuatorProcessSimulator.setSimulationType(ValuatorSimulatorType.SIMULATION_TOTAL.getCode());
		}else{
			valuatorProcessSimulator.setSimulationType(ValuatorSimulatorType.SIMULATION_PARTIAL.getCode());
		}
	}

	/**
	 * View process simulator.
	 *
	 * @param selectedTO the selected to
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String viewProcessSimulator(ValuatorProcessSimulatorTO selectedTO) throws ServiceException {
		if(selectedTO!=null){
			setViewOperationType(ViewOperationsType.DETAIL.getCode());
			cleanRegister();
			valuatorProcessSimulator = valuatorSimulatorFacade.viewProcessSimulator(selectedTO.getIdValuatorSimulatorPk());
			searchValuatorSimulatorSecuritiesDetail();
			if(valuatorProcessSimulator.getParticipant() == null)
				valuatorProcessSimulator.setParticipant(new Participant());
			if(valuatorProcessSimulator.getHolder() == null)
				valuatorProcessSimulator.setHolder(new Holder());
			if(valuatorProcessSimulator.getHolderAccount() == null)
				valuatorProcessSimulator.setHolderAccount(new HolderAccount());
			if(valuatorProcessSimulator.getSecurity() == null)
				valuatorProcessSimulator.setSecurity(new Security());
			return REGISTER_SIMULATOR;
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getExceptionMessage(
							ErrorServiceType.VALUATOR_SIMULATION_UNSELECTED.getMessage()));
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}
	}
	
	/**
	 * Register process simulator.
	 *
	 * @throws ServiceException the service exception
	 */
	public void registerProcessSimulator() throws ServiceException {
		ValuatorProcessSimulator processSimulator 	  = valuatorProcessSimulator;
		List<ValuatorSimulatorCodeTO> valuatoCodeList = valuatorDataModelCode!=null?valuatorDataModelCode.getDataList():null;
		List<ValuatorSimulationBalanceTO> balanceList = valuatorDataModelBalance!=null?valuatorDataModelBalance.getDataList():null;
		processSimulator = valuatorSimulatorFacade.registerProcessSimulator(processSimulator,balanceList,valuatoCodeList);
		valuatorSimulatorFacade.registerSimulatorBatch(processSimulator.getIdValuatorSimulatorPk());
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.VALUATOR_PROCESS_SIMULATION_REGISTER_SUCCES,
														   new Object[]{processSimulator.getIdValuatorSimulatorPk().toString()}));
	}
	
	/**
	 * Search process simulators.
	 */
	@LoggerAuditWeb
	public void searchProcessSimulators(){
		List<ValuatorProcessSimulatorTO> list = null;
		try {
			list = valuatorSimulatorFacade.getValuatorSimulatorFacade(valuatorSimulatorTO);
		} catch (ServiceException ex) {
			if(ex.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(
						GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getExceptionMessage(ex.getMessage()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
		if(list==null){
			valuatorDataModel = new GenericDataModel<ValuatorProcessSimulatorTO>();
		}else{
			valuatorDataModel = new GenericDataModel<ValuatorProcessSimulatorTO>(list);
		}
	}
	
	/**
	 * Search valuator simulator code.
	 */
	public void searchValuatorSimulatorCode(){
		List<ValuatorSimulatorCodeTO> list = null;
		try {
			list = valuatorSimulatorFacade.getValuatorSimulatorCode(valuatorProcessSimulator);
		} catch (ServiceException ex) {
			if(ex.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(
						GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getExceptionMessage(ex.getMessage()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
		if(list==null){
			valuatorDataModelCode = new GenericDataModel<ValuatorSimulatorCodeTO>();
		}else{
			valuatorDataModelCode = new GenericDataModel<ValuatorSimulatorCodeTO>(list);
		}
	}
	
	/**
	 * Search valuator simulator balance.
	 */
	public void searchValuatorSimulatorBalance(){
		List<ValuatorSimulationBalanceTO> list = null;
		try {
			list = valuatorSimulatorFacade.getValuatorSimulationBalance(valuatorProcessSimulator);
		} catch (ServiceException ex) {
			if(ex.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(
						GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getExceptionMessage(ex.getMessage()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
		if(list==null){
			valuatorDataModelBalance = new GenericDataModel<ValuatorSimulationBalanceTO>();
		}else{
			valuatorDataModelBalance = new GenericDataModel<ValuatorSimulationBalanceTO>(list);
		}
	}

	/**
	 * Search valuator simulator balance detail.
	 */
	public void searchValuatorSimulatorBalanceDetail(){
		List<ValuatorSimulationBalanceTO> list = null;
		try {
			list = valuatorSimulatorFacade.getValuatorSimulationBalanceDetail(valuatorProcessSimulator.getIdValuatorSimulatorPk());
			
		} catch (ServiceException ex) {
			if(ex.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(
						GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getExceptionMessage(ex.getMessage()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
		if(list==null){
			valuatorDataModelBalance = new GenericDataModel<ValuatorSimulationBalanceTO>();
		}else{
			valuatorDataModelBalance = new GenericDataModel<ValuatorSimulationBalanceTO>(list);
		}
	}
	
	/**
	 * Search valuator simulator code detail.
	 */
	public void searchValuatorSimulatorCodeDetail(){
		List<ValuatorSimulatorCodeTO> list = null;
		try {
			list = valuatorSimulatorFacade.getValuatorSimulationCodeDetail(valuatorProcessSimulator.getIdValuatorSimulatorPk());
			
		} catch (ServiceException ex) {
			if(ex.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(
						GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getExceptionMessage(ex.getMessage()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
		if(list==null){
			valuatorDataModelCode = new GenericDataModel<ValuatorSimulatorCodeTO>();
		}else{
			valuatorDataModelCode = new GenericDataModel<ValuatorSimulatorCodeTO>(list);
		}
	}
	
	/**
	 * Clean search valuator simulator securities.
	 */
	public void cleanSearchValuatorSimulatorSecurities(){
		valuatorDataModelBalance = null;
		valuatorDataModelCode = null;
	}
	
	/**
	 * Search valuator simulator securities.
	 */
	public void searchValuatorSimulatorSecurities(){
		Integer simulationForm= valuatorProcessSimulator.getSimulationForm();
		Long holderPk	   = valuatorProcessSimulator.getHolder().getIdHolderPk();
		String securityCode= valuatorProcessSimulator.getSecurity().getIdSecurityCodePk();
		Long participantPk = valuatorProcessSimulator.getParticipant().getIdParticipantPk();
		
		if(participantPk==null && holderPk==null && securityCode==null ){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
								PropertiesUtilities.getExceptionMessage(ErrorServiceType.VALUATOR_SIMULATION_PART_CUI_SECURITY_UNSELECTED.getMessage()));
			JSFUtilities.showSimpleValidationDialog();
		}else{
			if(simulationForm != null){
				switch(ValuatorSimulatorForm.get(simulationForm)){
					case BALANCE: 			searchValuatorSimulatorBalance();break;
					case BY_VALUATION_CODE: searchValuatorSimulatorCode();break;
					default:break;
				}
			}
		}
	}
	
	/**
	 * Search valuator simulator securities detail.
	 */
	public void searchValuatorSimulatorSecuritiesDetail(){
		switch(ValuatorSimulatorForm.get(valuatorProcessSimulator.getSimulationForm())){
			case BALANCE: 			searchValuatorSimulatorBalanceDetail();
									break;
			case BY_VALUATION_CODE: searchValuatorSimulatorCodeDetail();
									break;
			default:				break;
		}
	}
	
	/**
	 * If results securities.
	 *
	 * @return the boolean
	 */
	public Boolean ifResultsSecurities(){
		Boolean results = Boolean.FALSE;
		
		if(valuatorProcessSimulator.getSimulationForm() != null){
			
			switch(ValuatorSimulatorForm.get(valuatorProcessSimulator.getSimulationForm())){
				case BALANCE:			if(valuatorDataModelBalance != null && valuatorDataModelBalance.getSize() == ComponentConstant.ZERO)
											results = Boolean.TRUE;
										break;
				case BY_VALUATION_CODE:	if(valuatorDataModelCode != null && valuatorDataModelCode.getSize() == ComponentConstant.ZERO)
											results = Boolean.TRUE; 
										break;
				default:				break;
			}
		}
		return results;
	}
	
	/**
	 * Checks if is balance simulation form.
	 *
	 * @return true, if is balance simulation form
	 */
	public boolean isBalanceSimulationForm(){
		return ValuatorSimulatorForm.BALANCE.getCode().equals(this.valuatorProcessSimulator.getSimulationForm());
	}
	
	/**
	 * Checks if is by valuation code simulation form.
	 *
	 * @return true, if is by valuation code simulation form
	 */
	public boolean isByValuationCodeSimulationForm(){
		return ValuatorSimulatorForm.BY_VALUATION_CODE.getCode().equals(this.valuatorProcessSimulator.getSimulationForm());
	}
	
	/**
	 * Execute security process class.
	 */
	public void executeSecurityProcessClass(){
		List<String> securityCodes = null;
		try {
			valuatorSimulatorFacade.executeSecurityProcessClass(securityCodes);
			
		} catch (ServiceException ex) {
			if(ex.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(
						GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getExceptionMessage(ex.getMessage()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}
	
	
	/**
	 * Detail process simulator.
	 *
	 * @param selectedTO the selected to
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String detailProcessSimulator(ValuatorProcessSimulatorTO selectedTO) throws ServiceException {
		if(selectedTO!=null){
			
			valuatorProcessSimulator = valuatorSimulatorFacade.viewProcessSimulator(selectedTO.getIdValuatorSimulatorPk());
			
			List<DetailSimulationTO> listDetailSimulation = valuatorSimulatorFacade.listDetailSimulation(selectedTO.getIdValuatorSimulatorPk());
			
			detailSimulationTOModel = new GenericDataModel<DetailSimulationTO>();
			detailSimulationTOModel = new GenericDataModel<DetailSimulationTO>(listDetailSimulation);
			return DETAIL_SIMULATION;
			
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getExceptionMessage(
							ErrorServiceType.VALUATOR_SIMULATION_UNSELECTED.getMessage()));
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}
	}
	
	/**
	 * Show security detail.
	 *
	 * @param idSecurity the id security
	 */
	public void showSecurityDetail(String idSecurity){
		securityHelpBean.setSecurityCode(idSecurity);
		securityHelpBean.setName("securityHelp");
		securityHelpBean.searchSecurityHandler();
		
	}
	
	
	
	/**
	 * Gets the valuator simulator facade.
	 *
	 * @return the valuator simulator facade
	 */
	public ValuatorSimulatorFacade getValuatorSimulatorFacade() {
		return valuatorSimulatorFacade;
	}

	/**
	 * Sets the valuator simulator facade.
	 *
	 * @param valuatorSimulatorFacade the new valuator simulator facade
	 */
	public void setValuatorSimulatorFacade(
			ValuatorSimulatorFacade valuatorSimulatorFacade) {
		this.valuatorSimulatorFacade = valuatorSimulatorFacade;
	}

	/**
	 * Gets the general parameters facade.
	 *
	 * @return the general parameters facade
	 */
	public GeneralParametersFacade getGeneralParametersFacade() {
		return generalParametersFacade;
	}

	/**
	 * Sets the general parameters facade.
	 *
	 * @param generalParametersFacade the new general parameters facade
	 */
	public void setGeneralParametersFacade(
			GeneralParametersFacade generalParametersFacade) {
		this.generalParametersFacade = generalParametersFacade;
	}

	/**
	 * Gets the valuator process simulator.
	 *
	 * @return the valuator process simulator
	 */
	public ValuatorProcessSimulator getValuatorProcessSimulator() {
		return valuatorProcessSimulator;
	}

	/**
	 * Sets the valuator process simulator.
	 *
	 * @param valuatorProcessSimulator the new valuator process simulator
	 */
	public void setValuatorProcessSimulator(
			ValuatorProcessSimulator valuatorProcessSimulator) {
		this.valuatorProcessSimulator = valuatorProcessSimulator;
	}

	/**
	 * Gets the valuator simulator to.
	 *
	 * @return the valuator simulator to
	 */
	public ValuatorProcessSimulatorTO getValuatorSimulatorTO() {
		return valuatorSimulatorTO;
	}

	/**
	 * Sets the valuator simulator to.
	 *
	 * @param valuatorSimulatorTO the new valuator simulator to
	 */
	public void setValuatorSimulatorTO(
			ValuatorProcessSimulatorTO valuatorSimulatorTO) {
		this.valuatorSimulatorTO = valuatorSimulatorTO;
	}

	/**
	 * Gets the valuator simulator selected.
	 *
	 * @return the valuator simulator selected
	 */
	public ValuatorProcessSimulatorTO getValuatorSimulatorSelected() {
		return valuatorSimulatorSelected;
	}

	/**
	 * Sets the valuator simulator selected.
	 *
	 * @param valuatorSimulatorSelected the new valuator simulator selected
	 */
	public void setValuatorSimulatorSelected(
			ValuatorProcessSimulatorTO valuatorSimulatorSelected) {
		this.valuatorSimulatorSelected = valuatorSimulatorSelected;
	}

	/**
	 * Gets the lst state.
	 *
	 * @return the lst state
	 */
	public List<ParameterTable> getLstState() {
		return lstState;
	}

	/**
	 * Sets the lst state.
	 *
	 * @param lstState the new lst state
	 */
	public void setLstState(List<ParameterTable> lstState) {
		this.lstState = lstState;
	}

	/**
	 * Gets the participant list register.
	 *
	 * @return the participant list register
	 */
	public List<Participant> getParticipantListRegister() {
		return participantListRegister;
	}

	/**
	 * Sets the participant list register.
	 *
	 * @param participantListRegister the new participant list register
	 */
	public void setParticipantListRegister(List<Participant> participantListRegister) {
		this.participantListRegister = participantListRegister;
	}

	/**
	 * Gets the lst account register.
	 *
	 * @return the lst account register
	 */
	public List<HolderAccount> getLstAccountRegister() {
		return lstAccountRegister;
	}

	/**
	 * Sets the lst account register.
	 *
	 * @param lstAccountRegister the new lst account register
	 */
	public void setLstAccountRegister(List<HolderAccount> lstAccountRegister) {
		this.lstAccountRegister = lstAccountRegister;
	}

	/**
	 * Gets the selected account register.
	 *
	 * @return the selected account register
	 */
	public HolderAccount getSelectedAccountRegister() {
		return selectedAccountRegister;
	}

	/**
	 * Sets the selected account register.
	 *
	 * @param selectedAccountRegister the new selected account register
	 */
	public void setSelectedAccountRegister(HolderAccount selectedAccountRegister) {
		this.selectedAccountRegister = selectedAccountRegister;
	}

	/**
	 * Gets the lst state search.
	 *
	 * @return the lst state search
	 */
	public List<ParameterTable> getLstStateSearch() {
		return lstStateSearch;
	}

	/**
	 * Sets the lst state search.
	 *
	 * @param lstStateSearch the new lst state search
	 */
	public void setLstStateSearch(List<ParameterTable> lstStateSearch) {
		this.lstStateSearch = lstStateSearch;
	}

	/**
	 * Gets the participant list search.
	 *
	 * @return the participant list search
	 */
	public List<Participant> getParticipantListSearch() {
		return participantListSearch;
	}

	/**
	 * Sets the participant list search.
	 *
	 * @param participantListSearch the new participant list search
	 */
	public void setParticipantListSearch(List<Participant> participantListSearch) {
		this.participantListSearch = participantListSearch;
	}

	/**
	 * Gets the lst account search.
	 *
	 * @return the lst account search
	 */
	public List<HolderAccount> getLstAccountSearch() {
		return lstAccountSearch;
	}

	/**
	 * Sets the lst account search.
	 *
	 * @param lstAccountSearch the new lst account search
	 */
	public void setLstAccountSearch(List<HolderAccount> lstAccountSearch) {
		this.lstAccountSearch = lstAccountSearch;
	}

	/**
	 * Gets the selected account search.
	 *
	 * @return the selected account search
	 */
	public HolderAccount getSelectedAccountSearch() {
		return selectedAccountSearch;
	}

	/**
	 * Sets the selected account search.
	 *
	 * @param selectedAccountSearch the new selected account search
	 */
	public void setSelectedAccountSearch(HolderAccount selectedAccountSearch) {
		this.selectedAccountSearch = selectedAccountSearch;
	}

	/**
	 * Gets the parameter simulation form.
	 *
	 * @return the parameter simulation form
	 */
	public Map<Integer, String> getParameterSimulationForm() {
		return parameterSimulationForm;
	}

	/**
	 * Sets the parameter simulation form.
	 *
	 * @param parameterSimulationForm the parameter simulation form
	 */
	public void setParameterSimulationForm(
			Map<Integer, String> parameterSimulationForm) {
		this.parameterSimulationForm = parameterSimulationForm;
	}

	/**
	 * Gets the parameter process state.
	 *
	 * @return the parameter process state
	 */
	public Map<Integer, String> getParameterProcessState() {
		return parameterProcessState;
	}

	/**
	 * Sets the parameter process state.
	 *
	 * @param parameterProcessState the parameter process state
	 */
	public void setParameterProcessState(Map<Integer, String> parameterProcessState) {
		this.parameterProcessState = parameterProcessState;
	}

	/**
	 * Gets the participant logout.
	 *
	 * @return the participant logout
	 */
	public Long getParticipantLogout() {
		return participantLogout;
	}

	/**
	 * Sets the participant logout.
	 *
	 * @param participantLogout the new participant logout
	 */
	public void setParticipantLogout(Long participantLogout) {
		this.participantLogout = participantLogout;
	}

	/**
	 * Gets the lst requester user types.
	 *
	 * @return the lst requester user types
	 */
	public List<ParameterTable> getLstRequesterUserTypes() {
		return lstRequesterUserTypes;
	}

	/**
	 * Sets the lst requester user types.
	 *
	 * @param lstRequesterUserTypes the new lst requester user types
	 */
	public void setLstRequesterUserTypes(List<ParameterTable> lstRequesterUserTypes) {
		this.lstRequesterUserTypes = lstRequesterUserTypes;
	}

	/**
	 * Gets the valuator data model.
	 *
	 * @return the valuator data model
	 */
	public GenericDataModel<ValuatorProcessSimulatorTO> getValuatorDataModel() {
		return valuatorDataModel;
	}

	/**
	 * Sets the valuator data model.
	 *
	 * @param valuatorDataModel the new valuator data model
	 */
	public void setValuatorDataModel(
			GenericDataModel<ValuatorProcessSimulatorTO> valuatorDataModel) {
		this.valuatorDataModel = valuatorDataModel;
	}

	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * Gets the depositary setup.
	 *
	 * @return the depositary setup
	 */
	public IdepositarySetup getDepositarySetup() {
		return depositarySetup;
	}

	/**
	 * Sets the depositary setup.
	 *
	 * @param depositarySetup the new depositary setup
	 */
	public void setDepositarySetup(IdepositarySetup depositarySetup) {
		this.depositarySetup = depositarySetup;
	}

	/**
	 * Gets the securities part hol account.
	 *
	 * @return the securities part hol account
	 */
	public SecuritiesPartHolAccount getSecuritiesPartHolAccount() {
		return securitiesPartHolAccount;
	}

	/**
	 * Sets the securities part hol account.
	 *
	 * @param securitiesPartHolAccount the new securities part hol account
	 */
	public void setSecuritiesPartHolAccount(
			SecuritiesPartHolAccount securitiesPartHolAccount) {
		this.securitiesPartHolAccount = securitiesPartHolAccount;
	}

	/**
	 * Gets the valuator data model code.
	 *
	 * @return the valuator data model code
	 */
	public GenericDataModel<ValuatorSimulatorCodeTO> getValuatorDataModelCode() {
		return valuatorDataModelCode;
	}

	/**
	 * Sets the valuator data model code.
	 *
	 * @param valuatorDataModelCode the new valuator data model code
	 */
	public void setValuatorDataModelCode(
			GenericDataModel<ValuatorSimulatorCodeTO> valuatorDataModelCode) {
		this.valuatorDataModelCode = valuatorDataModelCode;
	}

	/**
	 * Gets the valuator data model balance.
	 *
	 * @return the valuator data model balance
	 */
	public GenericDataModel<ValuatorSimulationBalanceTO> getValuatorDataModelBalance() {
		return valuatorDataModelBalance;
	}

	/**
	 * Sets the valuator data model balance.
	 *
	 * @param valuatorDataModelBalance the new valuator data model balance
	 */
	public void setValuatorDataModelBalance(
			GenericDataModel<ValuatorSimulationBalanceTO> valuatorDataModelBalance) {
		this.valuatorDataModelBalance = valuatorDataModelBalance;
	}

	/**
	 * Gets the simulation form.
	 *
	 * @return the simulation form
	 */
	public Map<Integer, String> getSimulationForm() {
		return simulationForm;
	}

	/**
	 * Sets the simulation form.
	 *
	 * @param simulationForm the simulation form
	 */
	public void setSimulationForm(Map<Integer, String> simulationForm) {
		this.simulationForm = simulationForm;
	}

	/**
	 * Gets the detail simulation to model.
	 *
	 * @return the detail simulation to model
	 */
	public GenericDataModel<DetailSimulationTO> getDetailSimulationTOModel() {
		return detailSimulationTOModel;
	}

	/**
	 * Sets the detail simulation to model.
	 *
	 * @param detailSimulationTOModel the new detail simulation to model
	 */
	public void setDetailSimulationTOModel(
			GenericDataModel<DetailSimulationTO> detailSimulationTOModel) {
		this.detailSimulationTOModel = detailSimulationTOModel;
	}
	
	
}