package com.pradera.securities.valuator.classes.facade;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.valuatorprocess.SecurityClassValuator;
import com.pradera.model.valuatorprocess.ValuatorProcessClass;
import com.pradera.model.valuatorprocess.ValuatorSecurityCode;
import com.pradera.securities.valuator.classes.service.SecuritiesClassValuatorService;
import com.pradera.securities.valuator.classes.to.SecurityClassValuatorResultTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SecuritiesClassValuatorFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class SecuritiesClassValuatorFacade extends CrudDaoServiceBean{
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	/** The securities class valuator service. */
	@EJB
	SecuritiesClassValuatorService securitiesClassValuatorService;
	
	/**
	 * Gets the securities class valuator facade.
	 *
	 * @param instrumentType the instrument type
	 * @return the securities class valuator facade
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.VALUATOR_EXECUTION_CLASS_QUERY)
	public List<ValuatorProcessClass> getSecuritiesClassValuatorFacade(Integer instrumentType) throws ServiceException{
		return securitiesClassValuatorService.getListValuatorProcessClass(instrumentType);
	}
	
	/**
	 * Exist security class.
	 *
	 * @param securityClassFk the security class fk
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.VALUATOR_EXECUTION_CLASS_QUERY)
	public Long existSecurityClass(Integer securityClassFk)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		return securitiesClassValuatorService.existSecurityClass(securityClassFk);
	}
	
	/**
	 * Register list security class.
	 *
	 * @param lstSecurityClassValuator the lst security class valuator
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.VALUATOR_EXECUTION_CLASS_REGISTER)
	public void registerListSecurityClass(List<SecurityClassValuator> lstSecurityClassValuator)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		securitiesClassValuatorService.registerListSecurityClass(lstSecurityClassValuator);
	}
	
/**
 * Serch security class valuator result facade.
 *
 * @param filter the filter
 * @return the list
 * @throws ServiceException the service exception
 */
//	}
	@ProcessAuditLogger (process = BusinessProcessType.VALUATOR_EXECUTION_CLASS_QUERY)
	public List<SecurityClassValuatorResultTO> serchSecurityClassValuatorResultFacade(SecurityClassValuatorResultTO filter)throws ServiceException{
		return securitiesClassValuatorService.serchSecurityClassValuatorResult(filter);
	}

	/**
	 * Gets the list security class valuator result to facade.
	 *
	 * @param filter the filter
	 * @return the list security class valuator result to facade
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.VALUATOR_EXECUTION_CLASS_QUERY)
	public List<SecurityClassValuatorResultTO> getListSecurityClassValuatorResultTOFacade(SecurityClassValuatorResultTO filter)throws ServiceException{
		return securitiesClassValuatorService.getListSecurityClassValuatorResultTOService(filter);
	}
	
	/**
	 * Update securities class valuator facade.
	 *
	 * @param lstScv the lst scv
	 * @param filter the filter
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.VALUATOR_EXECUTION_CLASS_REGISTER)
	public void updateSecuritiesClassValuatorFacade(List<SecurityClassValuator> lstScv,SecurityClassValuatorResultTO filter)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		securitiesClassValuatorService.updateSecuritiesClassValuatorService(lstScv, filter);
	}
	
	/**
	 * Delete securities class valuator service.
	 *
	 * @param filter the filter
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.VALUATOR_EXECUTION_CLASS_DELETE)
	public void deleteSecuritiesClassValuatorService(SecurityClassValuatorResultTO filter)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		securitiesClassValuatorService.deleteSecuritiesClassValuatorService(filter);
	}
	
	/**
	 * Gets the valuator classes by sec class facade.
	 *
	 * @param secClassValuator the sec class valuator
	 * @return the valuator classes by sec class facade
	 * @throws ServiceException the service exception
	 */
	public List<SecurityClassValuator> getValuatorClassesBySecClassFacade(SecurityClassValuator secClassValuator) throws ServiceException{
		return securitiesClassValuatorService.getValuatorClassesBySecClassService(secClassValuator);
	}
		
	/**
	 * Gets the valuator process class by pk facade.
	 *
	 * @param idValuatorClassPk the id valuator class pk
	 * @return the valuator process class by pk facade
	 * @throws ServiceException the service exception
	 */
	public ValuatorProcessClass getValuatorProcessClassByPkFacade(String idValuatorClassPk) throws ServiceException{
		return securitiesClassValuatorService.getValuatorProcessClassByPkService(idValuatorClassPk);
	}


	/**
	 * Gets the valuator process class.
	 *
	 * @param security the security
	 * @param lstCboSecurityClassValuator the lst cbo security class valuator
	 * @return the valuator process class
	 */
	public ValuatorProcessClass getValuatorProcessClass(Security security,List<SecurityClassValuator> lstCboSecurityClassValuator) {
		return securitiesClassValuatorService.getValuatorProcessClass(security,lstCboSecurityClassValuator);
	}
	
	/**
	 * Gets the valuator security code by security.
	 *
	 * @param security the security
	 * @return the valuator security code by security
	 * @throws ServiceException the service exception
	 */
	public List<ValuatorSecurityCode> getValuatorSecurityCodeBySecurity(Security security) throws ServiceException{
		return securitiesClassValuatorService.getValuatorSecurityCodeBySecurity(security);
	}
}