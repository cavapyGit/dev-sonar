package com.pradera.securities.valuator.simulator.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.configuration.ValuatorSetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.valuatorprocess.SecurityClassValuator;
import com.pradera.model.valuatorprocess.ValuatorCode;
import com.pradera.model.valuatorprocess.ValuatorProcessClass;
import com.pradera.model.valuatorprocess.ValuatorProcessOperation;
import com.pradera.model.valuatorprocess.ValuatorProcessSetup;
import com.pradera.model.valuatorprocess.ValuatorProcessSimulator;
import com.pradera.model.valuatorprocess.ValuatorSimulationBalance;
import com.pradera.model.valuatorprocess.ValuatorSimulatorCode;
import com.pradera.model.valuatorprocess.ValuatorSimulatorSecurity;
import com.pradera.model.valuatorprocess.type.ValuatorSimulatorForm;
import com.pradera.model.valuatorprocess.type.ValuatorSimulatorStateType;
import com.pradera.model.valuatorprocess.type.ValuatorSimulatorType;
import com.pradera.securities.valuator.simulator.service.ValuatorSimulatorService;
import com.pradera.securities.valuator.simulator.view.DetailSimulationTO;
import com.pradera.securities.valuator.simulator.view.ValuatorProcessSimulatorTO;
import com.pradera.securities.valuator.simulator.view.ValuatorSimulationBalanceTO;
import com.pradera.securities.valuator.simulator.view.ValuatorSimulatorCodeTO;
import com.pradera.securities.valuator.simulator.view.ValuatorSimulatorSecurityTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorSimulatorFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ValuatorSimulatorFacade {
	
	/** The transaction registry. */
	@Resource private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The valuator simulator service. */
	@EJB private ValuatorSimulatorService valuatorSimulatorService;
	
	/** The participant service bean. */
	@EJB
	ParticipantServiceBean participantServiceBean;
	
	
	/** The batch process service facade. */
	@EJB
	BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The user info. */
	@Inject private UserInfo userInfo;

	
	/** The idepositary setup. */
	@Inject @DepositarySetup private IdepositarySetup idepositarySetup;
	
	/** The valuator setup. */
	@Inject @ValuatorSetup private ValuatorProcessSetup valuatorSetup;
	
	/**
	 * Creates the valuator process simulator.
	 *
	 * @param valuatorProcessSimulator the valuator process simulator
	 * @return the valuator process simulator
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.VALUATOR_EXECUTION_SIMULATION_REGISTER)
	public ValuatorProcessSimulator registerProcessSimulator(ValuatorProcessSimulator valuatorProcessSimulator, List<ValuatorSimulationBalanceTO> balancesTO, 
															 List<ValuatorSimulatorCodeTO> valCodesTO) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

		ValuatorProcessSimulator valuatorSimulator = valuatorProcessSimulator;
		
		/**Creating valuator process operation*/
		ValuatorProcessOperation valuatorProcessOperation = new ValuatorProcessOperation();
		valuatorProcessOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		valuatorProcessOperation.setRegistryUser(userInfo.getUserAccountSession().getUserName());
		valuatorProcessOperation.setOperationType(BusinessProcessType.VALUATOR_EXECUTION_SIMULATION_REGISTER.getCode());
		
		/**Assignment valuatorOperation at simulation process*/
		valuatorSimulator.setValuatorProcessOperation(valuatorProcessOperation);
		valuatorSimulator.setBeginTime(CommonsUtilities.currentDateTime());
		valuatorSimulator.setFinishTime(CommonsUtilities.currentDateTime());
		valuatorSimulator.setRegistryUser(userInfo.getUserAccountSession().getUserName());
		valuatorSimulator.setProcessState(ValuatorSimulatorStateType.REGISTERED.getCode());
		valuatorSimulator.setSimulationType(getSimulationType(valuatorSimulator));
		
		/**Function for generate last modify number*/
		Long operationType = valuatorProcessOperation.getOperationType();
		Long operationNumbr = valuatorSimulatorService.findLastOperationNumber(operationType);
		valuatorSimulator.getValuatorProcessOperation().setOperationNumber(operationNumbr);
		
		/**Verified and validating all parameters on simulation process, neither for holder account participant or security*/
		if(valuatorSimulator.getHolder()==null || valuatorSimulator.getHolder().getIdHolderPk()==null){
			valuatorSimulator.setHolder(null);
		}
		if(valuatorSimulator.getHolderAccount()!=null){
			Long idHolderAccount = valuatorSimulator.getHolderAccount().getIdHolderAccountPk();
			if (idHolderAccount==null || idHolderAccount.toString().isEmpty()){
				valuatorSimulator.setHolderAccount(null);
			}
		}
		if(valuatorSimulator.getParticipant()==null || valuatorSimulator.getParticipant().getIdParticipantPk()==null){
			valuatorSimulator.setParticipant(null);
		}
		if(valuatorSimulator.getSecurity()!=null){
			String securityCode = valuatorSimulator.getSecurity().getIdSecurityCodePk();
			if(securityCode==null || securityCode.isEmpty()){
				valuatorSimulator.setSecurity(null);
			}
		}
		
		/**Save new valuator simulator process*/
		valuatorSimulatorService.saveNewProcessSimulator(valuatorSimulator);
		
		/**Variables to handle simulation Type*/
		Integer simulationForm = valuatorSimulator.getSimulationForm();
		
		if(simulationForm.equals(ValuatorSimulatorForm.BALANCE.getCode())){
			List<ValuatorSimulationBalance> simulationBalances = new ArrayList<>();
			for (ValuatorSimulationBalanceTO balanceTO : balancesTO) {
				ValuatorSimulationBalance balance = new ValuatorSimulationBalance();
				
				/**Setting data in valuator simulator balance*/
				balance.setValuatorCode(balanceTO.getValuatorCode());
//				HolderAccountBalance hab = valuatorSimulatorService.find(HolderAccountBalance.class, balanceTO.getHolderAccountBalance().getId());
				balance.setHolderAccountBalance(valuatorSimulatorService.find(HolderAccountBalance.class, balanceTO.getHolderAccountBalance().getId()));
				balance.setSimulationRate(balanceTO.getSimulationRate());
				balance.setTotalBalance(balanceTO.getHolderAccountBalance().getTotalBalance());
				balance.setAvailableBalance(balanceTO.getHolderAccountBalance().getAvailableBalance());
				balance.setSimulationAmount(BigDecimal.ZERO);
				balance.setSimulationAvailableAmount(BigDecimal.ZERO);
				balance.setIndNominalRate(ComponentConstant.ZERO);
				balance.setProcessState(ValuatorSimulatorStateType.REGISTERED.getCode());
				balance.setValuatorProcessSimulator(valuatorSimulator);
				
				/**Adding object at list*/
				simulationBalances.add(balance);
			}
			/**Insert all valuator Balances*/
			valuatorSimulatorService.saveNewListSimulationBalance(simulationBalances);
			
		}else if(simulationForm.equals(ValuatorSimulatorForm.BY_VALUATION_CODE.getCode())){
			List<ValuatorSimulatorCode> simulatorValuatorCodes = new ArrayList<>();
			for (ValuatorSimulatorCodeTO to : valCodesTO){
				
				/**Object for valuator simulator code like object*/
				ValuatorSimulatorCode valuatorSimulatorCode = new ValuatorSimulatorCode();
				valuatorSimulatorCode.setValuatorCode(new ValuatorCode(to.getIdValuatorCodePk()));
				valuatorSimulatorCode.setValuatorProcessSimulator(valuatorSimulator);
				valuatorSimulatorCode.setAverageRate(to.getAverageRate());
				
				/**List for all securities*/
				List<ValuatorSimulatorSecurity> securities = new ArrayList<>();
				if(to.getSecurityTO() != null)
					for (ValuatorSimulatorSecurityTO securityTO : to.getSecurityTO().getDataList()) {
						
						/**Object for simulator security */
						ValuatorSimulatorSecurity simulatorSecurity = new ValuatorSimulatorSecurity();
						simulatorSecurity.setSecurity(new Security(securityTO.getIdSecurityCodePk()));
						simulatorSecurity.setAverateRate(securityTO.getAverateRate());
						
						if (securityTO.getValueNominalRate()){
							simulatorSecurity.setIdNominalValue(new Integer(1));
						}else {
							simulatorSecurity.setIdNominalValue(new Integer(0));
						}
						securities.add(simulatorSecurity);
					}
				valuatorSimulatorCode.setValuatorSimulatorSecurities(securities);
				simulatorValuatorCodes.add(valuatorSimulatorCode);
			}
			valuatorSimulatorService.saveNewListSimulatorCode(simulatorValuatorCodes);
		}
		
		return valuatorSimulator;
	}

	/**
	 * Creates the list of valuator simulation balances.
	 *
	 * @param valuatorSimulationBalances the list of valuator simulation balances
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.VALUATOR_EXECUTION_SIMULATION_REGISTER)
	public void registerSimulationBalances(List<ValuatorSimulationBalance> valuatorSimulationBalances) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		valuatorSimulatorService.saveNewListSimulationBalance(valuatorSimulationBalances);
	}

	/**
	 * Creates the list of valuator simulator codes.
	 *
	 * @param valuatorSimulatorCodes the list of valuator simulator codes
	 * @throws ServiceException the service exception
	 */
	public void registerSimulatorCodes(List<ValuatorSimulatorCode> valuatorSimulatorCodes) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		valuatorSimulatorService.saveNewListSimulatorCode(valuatorSimulatorCodes);
	}
	
	/**
	 * Register simulator batch.
	 *
	 * @param idValuatorSimulatorPk the id valuator simulator pk
	 * @throws ServiceException the service exception
	 */
	public void registerSimulatorBatch(Long idValuatorSimulatorPk) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		simulatorExecution(idValuatorSimulatorPk);
	}
	
	/**
	 * Simulator execution.
	 *
	 * @param valuatorProcessSimulator the valuator process simulator
	 */
	public void simulatorExecution(Long valuatorProcessSimulator){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		  loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAdd());
		  
		  BusinessProcess businessProcess = new BusinessProcess();
		  businessProcess.setIdBusinessProcessPk(BusinessProcessType.VALUATOR_EXECUTION_SIMULATION_EXECUTION.getCode());
		  try {
			  HashMap<String, Object> details = new HashMap<>();
//			  details.put(ValuatorConstants.PARAM_VALUATOR_PROCESS_SIMULATOR, valuatorProcessSimulator);
			  batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(),businessProcess,details);
		  } catch (ServiceException e) {
		   e.printStackTrace();
		  }
	}

	/***
	 * Method to find a valuator process Simulator by Id.
	 * @param idProcessSimulatorPk the id process simulator
	 * @return a object ValuatorProcessSimulator type
	 * @throws ServiceException the service exception
	 */
	public ValuatorProcessSimulator viewProcessSimulator(Long idProcessSimulatorPk) throws ServiceException {
		ValuatorProcessSimulator valuatorProcessSimulator = valuatorSimulatorService.viewProcessSimulator(idProcessSimulatorPk);
		if(valuatorProcessSimulator.getParticipant() != null)
			valuatorProcessSimulator.getParticipant().getIdParticipantPk();
		if(valuatorProcessSimulator.getHolder() != null)
			valuatorProcessSimulator.getHolder().getIdHolderPk();
		if(valuatorProcessSimulator.getHolderAccount() != null)
			valuatorProcessSimulator.getHolderAccount().getIdHolderAccountPk();
		if(valuatorProcessSimulator.getSecurity() != null)
			valuatorProcessSimulator.getSecurity().getIdSecurityCodePk();
		return valuatorProcessSimulator;
	}
	
	/**
	 * List detail simulation.
	 *
	 * @param idProcessSimulatorPk the id process simulator pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<DetailSimulationTO> listDetailSimulation(Long idProcessSimulatorPk) throws ServiceException {
		List<DetailSimulationTO> list = valuatorSimulatorService.listDetailSimulation(idProcessSimulatorPk);
		return list;
		
	}

	/***
	 * Method to get a list of ValuatorProcessSimulatorTO by ValuatorProcessSimulatorTO.
	 * @param valuatorProcessSimulatorTO the TO valuator process simulator
	 * @return a list of ValuatorProcessSimulatorTO
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.VALUATOR_EXECUTION_SIMULATION_QUERY)
	public List<ValuatorProcessSimulatorTO> getValuatorSimulatorFacade(ValuatorProcessSimulatorTO valuatorProcessSimulatorTO) throws ServiceException{
		List<ValuatorProcessSimulatorTO> list = valuatorSimulatorService.getValuatorSimulatorServiceBean(valuatorProcessSimulatorTO);
		return list;
	}

	/**
	 * *
	 * Method to get a list of ValuatorSimulatorCodeTO by ValuatorProcessSimulator.
	 *
	 * @param valuatorProcessSimulator the valuator process simulator
	 * @return a list of ValuatorSimulatorCodeTO
	 * @throws ServiceException the service exception
	 */
	public List<ValuatorSimulatorCodeTO> getValuatorSimulatorCode(ValuatorProcessSimulator valuatorProcessSimulator) throws ServiceException {
		List<ValuatorSimulatorCodeTO> list = valuatorSimulatorService.getValuatorSimulatorCode(valuatorProcessSimulator);
		return list;
	}

	/***
	 * Method to get a list of ValuatorSimulationBalanceTO by ValuatorProcessSimulator.
	 * @param valuatorProcessSimulator valuator process simulator
	 * @return a list of ValuatorSimulationBalanceTO
	 * @throws ServiceException the service exception
	 */
	public List<ValuatorSimulationBalanceTO> getValuatorSimulationBalance(ValuatorProcessSimulator valuatorProcessSimulator) throws ServiceException {
		List<ValuatorSimulationBalanceTO> list = valuatorSimulatorService.getValuatorSimulationBalance(valuatorProcessSimulator);
		return list;
	}

	/***
	 * Method to get a list of ValuatorSimulationBalanceTO by Id.
	 * @param idValuatorSimulatorPk the id valuator simulator
	 * @return a list of ValuatorSimulationBalanceTO
	 * @throws ServiceException the service exception
	 */
	public List<ValuatorSimulationBalanceTO> getValuatorSimulationBalanceDetail(Long idValuatorSimulatorPk) throws ServiceException {
		List<ValuatorSimulationBalanceTO> list = valuatorSimulatorService.getValuatorSimulationBalanceDetail(idValuatorSimulatorPk);
		return list;
	}

	/***
	 * Method to get a list of ValuatorSimulatorCodeTO by Id.
	 * @param idValuatorSimulatorPk the id valuator simulator
	 * @return a list of ValuatorSimulatorCodeTO
	 * @throws ServiceException the service exception
	 */
	public List<ValuatorSimulatorCodeTO> getValuatorSimulationCodeDetail(Long idValuatorSimulatorPk) throws ServiceException {
		List<ValuatorSimulatorCodeTO> list = valuatorSimulatorService.getValuatorSimulationCodeDetail(idValuatorSimulatorPk);
		return list;
	}

	/***
	 * Method to get a list of HolderAccount by holder Id and participant Id.
	 * @param idHolderPk the id holder
	 * @param idParticipantPk the id participant
	 * @return a list of HolderAccount
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> getAccountsHolderAndParticipant(Long idHolderPk, Long idParticipantPk) throws ServiceException{
		List<HolderAccount> list = valuatorSimulatorService.getAccountsHolderAndParticipant(idHolderPk, idParticipantPk);
		for (HolderAccount ha : list) {
			if(ha.getParticipant() != null)
				ha.getParticipant().getIdParticipantPk();
		}
		return list;
	}

	/***
	 * Method to fin the LastOperationNumber by operationType.
	 * @param operationType the operation type
	 * @return the LastOperationNumber Long type
	 * @throws ServiceException the service exception
	 */
	public Long findLastOperationNumber(Long operationType) throws ServiceException {
		return valuatorSimulatorService.findLastOperationNumber(operationType);
	}

	/***
	 * Method to execute security process class on HashMap by list of securityCodes.
	 * @param securityCodes the list of security codes
	 * @return a list of security By Class
	 * @throws ServiceException the service exception
	 */
	public void executeSecurityProcessClass(List<String> securityCodes) throws ServiceException{
		Map<String, Integer> securityByClass = valuatorSimulatorService.executeSecurityProcessClass(securityCodes);
		
		for (String securityCode : securityByClass.keySet()) {
			Integer securityClass = securityByClass.get(securityCode);
			
			List<ValuatorProcessClass> processClassList = getProcessClassList(securityClass);
			
			if(!processClassList.isEmpty() && processClassList.get(0)!= null){
				valuatorSimulatorService.updateSecurity(processClassList.get(0).getIdValuatorClassPk(), securityCode);
			}
		}
	}
	
	/**
	 * *
	 * Method to get list of valuator process class by securityClass.
	 *
	 * @param securityClass the security class
	 * @return a list of valuator process class
	 */
	public List<ValuatorProcessClass> getProcessClassList(Integer securityClass){
		List<ValuatorProcessClass> processClassList = new ArrayList<ValuatorProcessClass>();
		for(SecurityClassValuator secClass: valuatorSetup.getSecurityClassValuators()){
			if(secClass.getIdSecurityClassFk().equals(securityClass)){
				processClassList.add(secClass.getValuatorProcessClass());
			}
		}
		return processClassList;
	}

	/**
	 * Gets the lis participant service bean.
	 *
	 * @param filter the filter
	 * @return the lis participant service bean
	 * @throws ServiceException the service exception
	 */
	public List<Participant> getLisParticipantServiceBean(Participant filter) throws ServiceException{
		return participantServiceBean.getLisParticipantServiceBean(filter);
	}
	
	
	/**
	 * Gets the simulation type.
	 *
	 * @param processSimulator the process simulator
	 * @return the simulation type
	 */
	public Integer getSimulationType(ValuatorProcessSimulator processSimulator){
		if( ( processSimulator.getHolder() == null || processSimulator.getHolder().getIdHolderPk() == null ) && 
		    ( processSimulator.getHolderAccount() == null || processSimulator.getHolderAccount().getIdHolderAccountPk() == null ) && 
		    ( processSimulator.getSecurity() == null || processSimulator.getSecurity().getIdSecurityCodePk() == null )){
			
			return ValuatorSimulatorType.SIMULATION_TOTAL.getCode();
		}else{
			return ValuatorSimulatorType.SIMULATION_PARTIAL.getCode();
		}
	}
}