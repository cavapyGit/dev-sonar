package com.pradera.securities.valuator.core.view;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.valuatorprocess.ValuatorProcessSetup;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorProcessSetupBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@DepositaryWebBean
public class ValuatorProcessSetupBean extends GenericBaseBean implements Serializable{
		
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The valuator process setup to. */
	private ValuatorProcessSetupTO valuatorProcessSetupTO;
	
	/** The valuator process. */
	private ValuatorProcessSetup valuatorProcess;
	
	/** The v cheked. */
	private boolean vCheked;
	
	/**
	 * Inits the.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException{
		//valuatorProcessSetupTO = new ValuatorProcessSetupTO();
		valuatorProcess = new ValuatorProcessSetup();
		//valuatorProcessSetupTO.setIndAutomatic(BooleanType.NO.getCode());
		//valuatorProcessSetupTO.setIndHistory(BooleanType.NO.getCode());
		//valuatorProcessSetupTO.setIndManual(BooleanType.NO.getCode());		
	}
	
	
	
    /**
     * Checks if is cheked.
     *
     * @return true, if is cheked
     */
    public boolean isCheked() {
        return vCheked;
    }
 
    /**
     * Sets the v cheked.
     *
     * @param vCheked the new v cheked
     */
    public void setvCheked(boolean vCheked) {
        this.vCheked = vCheked;
    }

	/**
	 * Gets the valuator process setup to.
	 *
	 * @return the valuator process setup to
	 */
	public ValuatorProcessSetupTO getValuatorProcessSetupTO() {
		return valuatorProcessSetupTO;
	}

	/**
	 * Sets the valuator process setup to.
	 *
	 * @param valuatorProcessSetupTO the new valuator process setup to
	 */
	public void setValuatorProcessSetupTO(
			ValuatorProcessSetupTO valuatorProcessSetupTO) {
		this.valuatorProcessSetupTO = valuatorProcessSetupTO;
	}


	/**
	 * Gets the valuator process.
	 *
	 * @return the valuator process
	 */
	public ValuatorProcessSetup getValuatorProcess() {
		return valuatorProcess;
	}


	/**
	 * Sets the valuator process.
	 *
	 * @param valuatorProcess the new valuator process
	 */
	public void setValuatorProcess(ValuatorProcessSetup valuatorProcess) {
		this.valuatorProcess = valuatorProcess;
	}
    
}
