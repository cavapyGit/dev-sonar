package com.pradera.securities.valuator.simulator.view;

import java.math.BigDecimal;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorSimulatorSecurityTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class ValuatorSimulatorSecurityTO {

	/** The id valuator code pk. */
	private Long idValuatorCodePk;
	
	/** The id security code pk. */
	private String idSecurityCodePk;
	
	/** The interest rate. */
	private BigDecimal interestRate;

	/** The nominal rate. */
	private BigDecimal nominalRate;
	
	/** The averate rate. */
	private BigDecimal averateRate;

	/** The value nominal rate. */
	private Boolean valueNominalRate;

	/**
	 * Instantiates a new valuator simulator security to.
	 */
	public ValuatorSimulatorSecurityTO() {}

	/**
	 * Instantiates a new valuator simulator security to.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @param interestRate the interest rate
	 */
	public ValuatorSimulatorSecurityTO(//Long idValuatorCodePk,
			String idSecurityCodePk, BigDecimal interestRate) {
		super();
//		this.idValuatorCodePk = idValuatorCodePk;
		this.idSecurityCodePk = idSecurityCodePk;
		this.interestRate = interestRate;
	}
	
	/**
	 * Instantiates a new valuator simulator security to.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @param interestRate the interest rate
	 * @param averateRate the averate rate
	 * @param valueNominalRate the value nominal rate
	 */
	public ValuatorSimulatorSecurityTO(String idSecurityCodePk, BigDecimal interestRate,
			BigDecimal averateRate, Boolean valueNominalRate) {
		this.idSecurityCodePk = idSecurityCodePk;
		this.interestRate = interestRate;
		this.averateRate = averateRate;
		this.valueNominalRate = valueNominalRate;
	}

	/**
	 * Gets the id valuator code pk.
	 *
	 * @return the id valuator code pk
	 */
	public Long getIdValuatorCodePk() {
		return idValuatorCodePk;
	}

	/**
	 * Sets the id valuator code pk.
	 *
	 * @param idValuatorCodePk the new id valuator code pk
	 */
	public void setIdValuatorCodePk(Long idValuatorCodePk) {
		this.idValuatorCodePk = idValuatorCodePk;
	}

	/**
	 * Gets the id security code pk.
	 *
	 * @return the id security code pk
	 */
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	/**
	 * Sets the id security code pk.
	 *
	 * @param idSecurityCodePk the new id security code pk
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	/**
	 * Gets the interest rate.
	 *
	 * @return the interest rate
	 */
	public BigDecimal getInterestRate() {
		return interestRate;
	}

	/**
	 * Sets the interest rate.
	 *
	 * @param interestRate the new interest rate
	 */
	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}

	/**
	 * Gets the nominal rate.
	 *
	 * @return the nominal rate
	 */
	public BigDecimal getNominalRate() {
		return nominalRate;
	}

	/**
	 * Sets the nominal rate.
	 *
	 * @param nominalRate the new nominal rate
	 */
	public void setNominalRate(BigDecimal nominalRate) {
		this.nominalRate = nominalRate;
	}

	/**
	 * Gets the averate rate.
	 *
	 * @return the averate rate
	 */
	public BigDecimal getAverateRate() {
		return averateRate;
	}

	/**
	 * Sets the averate rate.
	 *
	 * @param averateRate the new averate rate
	 */
	public void setAverateRate(BigDecimal averateRate) {
		this.averateRate = averateRate;
	}

	/**
	 * Gets the value nominal rate.
	 *
	 * @return the value nominal rate
	 */
	public Boolean getValueNominalRate() {
		return valueNominalRate;
	}

	/**
	 * Sets the value nominal rate.
	 *
	 * @param valueNominalRate the new value nominal rate
	 */
	public void setValueNominalRate(Boolean valueNominalRate) {
		this.valueNominalRate = valueNominalRate;
	}
	
	
}