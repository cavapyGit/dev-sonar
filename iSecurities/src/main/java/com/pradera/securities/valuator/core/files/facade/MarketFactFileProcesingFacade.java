package com.pradera.securities.valuator.core.files.facade;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import org.apache.commons.io.FileUtils;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.negotiation.constant.NegotiationConstant;
import com.pradera.model.valuatorprocess.ValuatorMarketfactConsolidat;
import com.pradera.model.valuatorprocess.ValuatorMarketfactFile;
import com.pradera.model.valuatorprocess.ValuatorMarketfactMechanism;
import com.pradera.model.valuatorprocess.type.ValuatorMarkFactFileType;
import com.pradera.securities.utils.NegotiationUtils;
import com.pradera.securities.valuator.core.files.to.MarketfactValidationTO;
import com.pradera.securities.valuator.core.files.to.ValuatorMarketfactConsolidatTo;
import com.pradera.securities.valuator.core.files.to.ValuatorMarketfactFileTO;
import com.pradera.securities.valuator.core.files.to.ValuatorMarketfactMechanismType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MarketFactFileProcesingFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , Aug 21, 2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class MarketFactFileProcesingFacade implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The market fact facade. */
	@EJB MarketFactFacade marketFactFacade;
	
	/**
	 * Start process.
	 *
	 * @param filter the filter
	 */
	public void startProcess(ValuatorMarketfactFileTO filter) {

		Boolean existMechanism=Boolean.FALSE;
		try{
		
		/**VERIFICACION PROCESS: exiten Archivos procesando del mismo origen*/
		filter.setFileState(ValuatorMarkFactFileType.PROCESS.getCode());
		List<ValuatorMarketfactFileTO> lstProcess=marketFactFacade.searchValuatorMarketfactFileFacade(filter);	
		if(lstProcess.size()>GeneralConstants.ZERO_VALUE_INTEGER){
			return;
		}
		/**FIN VERIFICACION PROCESS**/

		existMechanism=marketFactFacade.existMarketFactMechanismTodayFacade(filter);
		if(existMechanism){ /** si exiten archivos del dia de hoy en VALUATOR_MARKETFACT_MECHANISM */
			/**VERIFICACION RECEPCIONADO DEL MISMO ORIGEN*/
			filter.setFileState(ValuatorMarkFactFileType.RECEPCIONADO.getCode());
			List<ValuatorMarketfactFileTO> lstRecepcionado=marketFactFacade.searchValuatorMarketfactFileFacade(filter);	
			if(Validations.validateIsNotNullAndNotEmpty(lstRecepcionado)){
				marketFactFacade.changeFileStatefacade(lstRecepcionado, ValuatorMarkFactFileType.REPLACE.getCode()); //cambiar de estado a "REEMPLAZADO"
				marketFactFacade.deleteMarketFactFileMechanismFacade(lstRecepcionado);  //eliminar resgistros de VALUATOR_MARKETFACT_MECHANISM pertenecientes a los archivos reemplazados
			}
		}else{
			existMechanism=marketFactFacade.existMarketFactMechanismBeforfacade(filter); //filter use registryDate
			if(existMechanism){
				/**guarda mechanism en historical , elimina todos los registros de mechanism**/
				marketFactFacade.saveMechanismToHistorical();  
			}
			
		}
		/*
		 *PROCESAMIENTO DE ARCHIVOS 
		 */
		processMarketFactFile(filter);
		/*
		 * CONSOLIDAT 
		 */
		processMarketFactFileConsolidate(filter);
		/**UPDATE MECHANISM  -- ADD CONSOLIDAT***/
//		addConsolidatToMechanism();
		/**FIN DE PROCESAMIENTO**/
		}catch(ServiceException e){
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Process market fact file.
	 *
	 * @param filter the filter
	 * @throws ServiceException the service exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void processMarketFactFile(ValuatorMarketfactFileTO filter)throws ServiceException, IOException{
		filter.setFileState(ValuatorMarkFactFileType.PENDING.getCode());		
		
		List<ValuatorMarketfactFileTO> lstPending=marketFactFacade.searchValuatorMarketfactFileFacade(filter);
		
		for(ValuatorMarketfactFileTO tmpMarketfactFile : lstPending){
			List<ValuatorMarketfactMechanism> lstMechanism=new ArrayList<ValuatorMarketfactMechanism>();
			/**CAMBIO DE ESTADO - PENDIENTE A PROCESO**/
			tmpMarketfactFile.setFileState(ValuatorMarkFactFileType.PROCESS.getCode());			
			marketFactFacade.changeFileStateFacade(tmpMarketfactFile); 
			/**FIN DE CAMBIO DE ESTADO**/
			/**PROCESAMIENTO DE DOCUMENTO - GUARDA REGISTROS EN MARKETFAC_MECHANISM**/
			tmpMarketfactFile.setStreamFileDir(streamFile(tmpMarketfactFile.getMarketfactType()));
			tmpMarketfactFile.setTempMarketfactFile(getFile(tmpMarketfactFile.getMarketfactFile()));
			tmpMarketfactFile.setRootTag(NegotiationConstant.MARKETFACT_ROOT_TAG);
			tmpMarketfactFile.setRegistryUser(filter.getRegistryUser());
			MarketfactValidationTO validate= new MarketfactValidationTO();
			NegotiationUtils.validateFileOperationStruct(validate,tmpMarketfactFile);
			lstMechanism=createListValuatorMarketFactMechanism(validate.getLstMarketfactFIles(),tmpMarketfactFile);
			
			marketFactFacade.saveValuatorMarketFactMechanismFacade(lstMechanism);
			
			/**FIN DE PROCESAMIENTO**/
			/**CAMBIO DE ESTADO - PROCESO A RECEPCIONADO**/
			tmpMarketfactFile.setFileState(ValuatorMarkFactFileType.RECEPCIONADO.getCode());			
			marketFactFacade.changeFileStateFacade(tmpMarketfactFile); 
			/**FIN DE CAMBIO DE ESTADO**/
		}
	}
	
	/**
	 * Process market fact file consolidate.
	 *
	 * @param filter the filter
	 */
	public void  processMarketFactFileConsolidate(ValuatorMarketfactFileTO filter){
		try {
			if(marketFactFacade.existMarketfactConsolidatFacade()){
				marketFactFacade.deleteAllMarketFactConsolidatFacade();
			}
			filter.setMarketfactType(ValuatorMarkFactFileType.FIXED_FILE.getCode());
			List<ValuatorMarketfactConsolidat> lstConsolidat=getListValuatorMarketfactConsolidat(filter);
			
			filter.setMarketfactType(ValuatorMarkFactFileType.VARIABLE_FILE.getCode());
			List<ValuatorMarketfactConsolidat> lstConsolidateVariable=getListValuatorMarketfactConsolidat(filter);
			
			filter.setMarketfactType(ValuatorMarkFactFileType.HISTORY_FILE.getCode());
			List<ValuatorMarketfactConsolidat> lstConsolidateHistorical=getListValuatorMarketfactConsolidat(filter);
			
			for(ValuatorMarketfactConsolidat obj : lstConsolidateVariable){
				lstConsolidat.add(obj);
			}
			for(ValuatorMarketfactConsolidat obj: lstConsolidateHistorical){
				lstConsolidat.add(obj);
			}
			marketFactFacade.saveValuatorMarketfactConsolidat(lstConsolidat);
			
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Gets the list valuator marketfact consolidat.
	 *
	 * @param filter the filter
	 * @return the list valuator marketfact consolidat
	 * @throws ServiceException the service exception
	 */
	public List<ValuatorMarketfactConsolidat> getListValuatorMarketfactConsolidat(ValuatorMarketfactFileTO filter) throws ServiceException{
		List<ValuatorMarketfactConsolidatTo> lstConsolidateTO=marketFactFacade.getConsolidateToMechanismFacade(filter.getMarketfactType(),filter.getProcessDate());
		List<ValuatorMarketfactMechanism>  lstMechanism= marketFactFacade.searchValuatorMarketfactMechanismFacade(filter.getMarketfactType(),filter.getProcessDate());
		List<ValuatorMarketfactConsolidat> lstConsolidate= new ArrayList<ValuatorMarketfactConsolidat>();
		for(ValuatorMarketfactConsolidatTo objTO : lstConsolidateTO){
			ValuatorMarketfactConsolidat objConsolidat= objTO.getValuatorMarketfactConsolidat();
			objConsolidat.setMarketfactType(filter.getMarketfactType());
			objConsolidat.setRegistryUser(filter.getRegistryUser()!=null?filter.getRegistryUser():"");// ValuatorConstants.BATCH_USER);
			objConsolidat.setRegistryDate(CommonsUtilities.currentDateTime());
			
			List<ValuatorMarketfactMechanism> lstTmpMechanism=new ArrayList<ValuatorMarketfactMechanism>();
			for(int i=0; i<lstMechanism.size();i++){
				ValuatorMarketfactMechanism objMechanism=lstMechanism.get(i);
				if(isEquals(objConsolidat, objMechanism)){
//					objMechanism.setValuatorMarketfactConsolidat(objConsolidat);
					lstTmpMechanism.add(objMechanism);
					lstMechanism.remove(i);
					i--;
				}
			}
			objConsolidat.setValuatorMarketfactMechanisms(lstTmpMechanism);
			lstConsolidate.add(objConsolidat);
			
		}
		return lstConsolidate;
	}
	
	/**
	 * Checks if is equals.
	 *
	 * @param objConsolidat the obj consolidat
	 * @param objMechanism the obj mechanism
	 * @return the boolean
	 */
	public Boolean isEquals(ValuatorMarketfactConsolidat objConsolidat,ValuatorMarketfactMechanism objMechanism){
		Boolean tmp=Boolean.FALSE;
		String consolidate=objConsolidat.getMarketfactActive()+objConsolidat.getSecurityClass()+
				objConsolidat.getSecurityCode()+objConsolidat.getValuatorCode();
		String mechanism=objMechanism.getMarketfactActive()+objMechanism.getSecurityClass()+
				objMechanism.getSecurityCode()+objMechanism.getValuatorCode();
		if(consolidate.equals(mechanism)){
			tmp=Boolean.TRUE;
		}
		return tmp;
	}
	
	/**
	 * Creates the list valuator market fact mechanism.
	 *
	 * @param lstMechanismType the lst mechanism type
	 * @param tmpMarketfactFile the tmp marketfact file
	 * @return the list
	 */
	public List<ValuatorMarketfactMechanism> createListValuatorMarketFactMechanism(List<ValuatorMarketfactMechanismType> lstMechanismType,ValuatorMarketfactFileTO tmpMarketfactFile){
		ValuatorMarketfactFile vmf=new ValuatorMarketfactFile();
		vmf.setIdValuatorFilePk(tmpMarketfactFile.getIdValuatorFilePk());
		
		List<ValuatorMarketfactMechanism> lstMechanism = new ArrayList<ValuatorMarketfactMechanism>();
		for(ValuatorMarketfactMechanismType obj : lstMechanismType){
			ValuatorMarketfactMechanism mechanism = obj.getvMarketfactMechanism();
			mechanism.setRegistryUser(tmpMarketfactFile.getRegistryUser()!=null?tmpMarketfactFile.getRegistryUser():"");//ValuatorConstants.BATCH_USER);
			mechanism.setRegistryDate(CommonsUtilities.currentDateTime());	
			mechanism.setMarketfactType(tmpMarketfactFile.getMarketfactType());
			mechanism.setValuatorMarketfactFile(vmf);
			lstMechanism.add(mechanism);
		}
		return lstMechanism;
	}
	
	/**
	 * Stream file.
	 *
	 * @param fileType the file type
	 * @return the string
	 */
	public String streamFile(Integer fileType){
		String Stream=GeneralConstants.EMPTY_STRING;		
		switch(ValuatorMarkFactFileType.get(fileType)){
		case FIXED_FILE			: Stream=NegotiationConstant.BBV_FIXED_FILE_STREAM;break;
		case VARIABLE_FILE		: Stream=NegotiationConstant.BBV_VARIABLE_FILE_STREAM;break;	
		case HISTORY_FILE		: Stream=NegotiationConstant.BBV_HISTORY_FILE_STREAM;break;
		default 				:break;
		}
		return Stream;
	}
	
	/**
	 * Gets the file.
	 *
	 * @param temp the temp
	 * @return the file
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public File getFile(byte[] temp) throws IOException{
		File file;
		file = File.createTempFile("tempMcnfile", ".tmp");
		FileUtils.writeByteArrayToFile(file, temp);        					//remueve las comillas de los datos
		return file;
	}
}