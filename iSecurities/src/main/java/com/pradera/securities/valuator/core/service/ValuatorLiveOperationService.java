package com.pradera.securities.valuator.core.service;	

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import com.pradera.commons.configuration.ValuatorSetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.model.valuatorprocess.ValuatorProcessSetup;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorLiveOperationService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@Singleton
@Performance
@Interceptors({ContextHolderInterceptor.class})
public class ValuatorLiveOperationService extends CrudDaoServiceBean{
	
	/** The valuator setup. */
	@Inject @ValuatorSetup ValuatorProcessSetup valuatorSetup;
	
	/** The securities service. */
	@EJB private ValuatorSecuritiesService securitiesService;
	
	/** The user session. */
	private String USER_SESSION = "ADMIN";
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
	}


	/**
	 * Update physical deposit market price.
	 *
	 * @param processDate the process date
	 * @param processPk the process pk
	 * @param securityHomologMap the security homolog map
	 * @throws ServiceException the service exception
	 */
//	@ValuatorDispatcherEvent(eventType=ValuatorEventType.EVENT_4_3_UPDATE_PHYSICAL_CERTIFICATE)
//	public void updatePhysicalDepositMarketPrice(Date processDate, Long processPk, Map<String,Object[]> securityHomologMap) throws ServiceException{
//		Map<Long,List<MarketFactForLiveOperation>> liveOperationMap = getPhysicalCertificateMarkFact(processDate, processPk);
//
//		if(liveOperationMap!=null && !liveOperationMap.isEmpty()){
//			for(Long key: liveOperationMap.keySet()){
//				List<MarketFactForLiveOperation> liveOperationList = liveOperationMap.get(key);
//				Integer indHistoric = haveHistoricInd(liveOperationList);
//				for(MarketFactForLiveOperation liveOperation: liveOperationList){
//					if(liveOperation.getActive().equals(BooleanType.YES.getCode())){
//						String securityCode = liveOperation.getSecurityCodePk();
//						BigDecimal marketPrice = null;
//						BigDecimal marketRate = null;
//						Date marketDate = null;
//						if(securityHomologMap.get(securityCode)!=null){
//							Object[] marketInfo = securityHomologMap.get(securityCode);
//							marketRate  = (BigDecimal) marketInfo[0];
//							marketPrice = (BigDecimal) marketInfo[1];
//							marketDate  = (Date) marketInfo[2];
//						}else{
//							marketRate = liveOperation.getMarketRate();
//							marketDate = liveOperation.getMarketDate();
//							ValuatorPriceResult price = findValuatPrice(securityCode, marketDate, processDate, marketRate, null);
//							marketPrice = price.getMarketPrice();
//							marketDate  = liveOperation.getMarketDate();
//						}
//						
//						Long physicalMarkFact = liveOperation.getPhysicalMrkFactPk();
//						Long custodyOperation = liveOperation.getCustodyOperationPk();
//						BigDecimal quantity   = liveOperation.getMarketQuantity();
//						
//						updatePhysicalMarketOper(physicalMarkFact, custodyOperation, marketRate, marketDate, marketPrice, indHistoric, quantity);
//					}
//				}
//			}
//		}
//		
//		/**Code to finish process*/
//		Integer detailPk = ValuatorDetailType.OPERATIONS.getCode();
//		Long eventPk 	 = ValuatorEventType.EVENT_4_3_UPDATE_PHYSICAL_CERTIFICATE.getCode(); 
//		securitiesService.finishValuatorExecutionDetail(processPk, detailPk, eventPk,false);
//	}
//	
//	/**
//	 * Have historic ind.
//	 *
//	 * @param liveOperationList the live operation list
//	 * @return the integer
//	 */
//	private Integer haveHistoricInd(List<MarketFactForLiveOperation> liveOperationList){
//		for(MarketFactForLiveOperation liveOperation: liveOperationList){
//			if(liveOperation.getActive().equals(BooleanType.NO.getCode())){
//				return BooleanType.YES.getCode();
//			}
//		}
//		return BooleanType.NO.getCode();
//	}
//	
//	/**
//	 * Update reporting market price.
//	 *
//	 * @param procDate the process date
//	 * @param processPk the process pk
//	 * @param securityHomologMap the security homolog map
//	 * @throws ServiceException the service exception
//	 */
//	@ValuatorDispatcherEvent(eventType=ValuatorEventType.EVENT_4_2_UPDATE_REPORTING_OPERATION)
//	public void updateReportingMarketPrice(Date procDate, Long processPk, Map<String,Object[]> securityHomologMap) throws ServiceException{
//		Map<Long,List<MarketFactForLiveOperation>> liveOperationsMap = getSettlementAccountMarketFact(procDate,processPk);
//		
//		if(liveOperationsMap!=null && !liveOperationsMap.isEmpty()){
//			for(Long key: liveOperationsMap.keySet()){
//				List<MarketFactForLiveOperation> liveOperationList = liveOperationsMap.get(key);
//				
//				Integer indHistoric 	= BooleanType.NO.getCode() ;
//				String securityCodePk 	= null;
//				for(MarketFactForLiveOperation liveOperation: liveOperationList){
//					securityCodePk = liveOperation.getSecurityCodePk();
//					if(liveOperation.getActive().equals(BooleanType.NO.getCode())){
//						indHistoric = BooleanType.YES.getCode() ;break;
//					}
//				}
//				
//				if(securityHomologMap.get(securityCodePk)==null){/**If securityCode it's not homologated*/
//					for(MarketFactForLiveOperation liveOperation: liveOperationList){
//						if(liveOperation.getActive().equals(BooleanType.YES.getCode())){
//							String securityCode 	= liveOperation.getSecurityCodePk();
//							Date markDate 	  		= liveOperation.getMarketDate();
//							BigDecimal marketRate 	= liveOperation.getMarketRate();
//							Integer role 			= liveOperation.getRole();
//							BigDecimal quantity 	= liveOperation.getMarketQuantity();
//							Long settAccPk			= liveOperation.getSettlementAccOperationPk();
//							Long settMrkPk			= liveOperation.getSettlemetMarketFactPk();
//							Long mechanism			= liveOperation.getMechanismType();
//							Long operationPk = null;
//							
//							/**If it's seller the price calculated have same marketDate than processDate and marketRate Zero*/
//							if(role.equals(ComponentConstant.SALE_ROLE) && !mechanism.equals(NegotiationMechanismType.SIRTEX.getCode())){
//								operationPk = liveOperation.getMechanismOperationPk();
//								marketRate  = BigDecimal.ZERO;
//								markDate	= procDate;
//							}
//							/**Find the price calculated by the process*/
//							ValuatorPriceResult priceResult = findValuatPrice(securityCode, markDate, procDate, marketRate, operationPk);
//							/**whenever exist error throw serviceException*/
//							if(priceResult==null){
//								throw new ServiceException(ErrorServiceType.VALUATOR_LIVEOPERATION_OPERATION, new Object[]{securityCode,marketRate,markDate});
//							}
//							/**Get the price*/
//							BigDecimal mrkPrice = priceResult.getMarketPrice();
//							/**Update reporting operation according with the price generated*/
//							markDate 	  	= liveOperation.getMarketDate();
//							updateSettlMarketFact(settMrkPk, role, liveOperation.getMarketRate(), markDate, mrkPrice, indHistoric, settAccPk, quantity);
//						}
//					}
//				}else{
//					for(MarketFactForLiveOperation liveOperation: liveOperationList){
//						Boolean isHomologated = Boolean.FALSE;
//						if(liveOperation.getActive().equals(BooleanType.YES.getCode())){
//							Integer role = liveOperation.getRole();
//							if(role.equals(ComponentConstant.SALE_ROLE)){
//								String securityCode 	= liveOperation.getSecurityCodePk();
//								Date marketDate 	  	= liveOperation.getMarketDate();
//								BigDecimal marketRate 	= liveOperation.getMarketRate();
//								BigDecimal quantity 	= liveOperation.getMarketQuantity();
//								Long settAccPk			= liveOperation.getSettlementAccOperationPk();
//								Long settMrkPk			= liveOperation.getSettlemetMarketFactPk();
//								Long operationPk 		= liveOperation.getMechanismOperationPk();
//									
//								ValuatorPriceResult priceResult = findValuatPrice(securityCode, procDate, procDate, BigDecimal.ZERO, operationPk);
//								if(priceResult==null){
//									/**In this case is sirtex*/
//									priceResult = findValuatorPriceHomologated(securityCode,processPk);
//									marketRate = priceResult.getMarketRate();
//									marketDate = priceResult.getMarketDate();
//								}
//								BigDecimal mrkPrice = priceResult.getMarketPrice();
//								updateSettlMarketFact(settMrkPk, role, marketRate, marketDate, mrkPrice, indHistoric, settAccPk, quantity);
//							}else if(role.equals(ComponentConstant.PURCHARSE_ROLE) && !isHomologated){
//								Map<Long,BigDecimal> settAccOperationMap = getMarketFactToHomologate(liveOperationList, role);
//								updateSettlementMarketFactBySettOp(new ArrayList<Long>(settAccOperationMap.keySet()));
//								for(Long settKey: settAccOperationMap.keySet()){
//									String securityCode  = liveOperation.getSecurityCodePk();
//									BigDecimal markRate  = (BigDecimal) securityHomologMap.get(securityCode)[0];
//									BigDecimal markPrice = (BigDecimal) securityHomologMap.get(securityCode)[1];
//									Date markDate 		 = (Date) securityHomologMap.get(securityCode)[2];
//									BigDecimal quantity  = settAccOperationMap.get(settKey);
//									
//									SettlementAccountMarketfact settlMrkFact = popuSettlMarketFact(markDate, markRate, markPrice, quantity, settKey);
//									create(settlMrkFact);
//								}
//								isHomologated = Boolean.TRUE;
//							}
//						}
//					}
//				}
//			}
//		}
//		
//		/**Code to finish process*/
//		Integer detailPk = ValuatorDetailType.OPERATIONS.getCode();
//		Long eventPk 	 = ValuatorEventType.EVENT_4_2_UPDATE_REPORTING_OPERATION.getCode(); 
//		securitiesService.finishValuatorExecutionDetail(processPk, detailPk, eventPk,false);
//	}
//	
//
//
//	/**
//	 * Gets the market fact to homologate.
//	 *
//	 * @param marketFactOperations the market fact operations
//	 * @param rol the rol
//	 * @return the market fact to homologate
//	 */
//	public Map<Long,BigDecimal> getMarketFactToHomologate(List<MarketFactForLiveOperation> marketFactOperations, Integer rol){
//		Map<Long,BigDecimal> settlementAccOperationMap = new HashMap<>();
//		for(MarketFactForLiveOperation liveOperation: marketFactOperations){
//			Integer role = liveOperation.getRole();
//			if(role.equals(rol)){
//				Long settAccOperationPk = liveOperation.getSettlementAccOperationPk();
//				if(settlementAccOperationMap.get(settAccOperationPk)==null){
//					settlementAccOperationMap.put(settAccOperationPk, BigDecimal.ZERO);
//				}
//				BigDecimal currentQuantity = settlementAccOperationMap.get(settAccOperationPk);
//				BigDecimal quantityOperation = liveOperation.getMarketQuantity();
//				settlementAccOperationMap.put(settAccOperationPk, currentQuantity.add(quantityOperation));
//			}
//		}
//		return settlementAccOperationMap;
//	}
//	
//	public Map<Long,BigDecimal> getMarketFactToHomologate(List<MarketFactForLiveOperation> marketFactOperations, Integer rol, Long SettleAccPk){
//		Map<Long,BigDecimal> settlementAccOperationMap = new HashMap<>();
//		for(MarketFactForLiveOperation liveOperation: marketFactOperations){
//			if(SettleAccPk.equals(liveOperation.getSettlementAccOperationPk())){
//				Integer role = liveOperation.getRole();
//				if(role.equals(rol)){
//					Long settAccOperationPk = liveOperation.getSettlementAccOperationPk();
//					if(settlementAccOperationMap.get(settAccOperationPk)==null){
//						settlementAccOperationMap.put(settAccOperationPk, BigDecimal.ZERO);
//					}
//					BigDecimal currentQuantity = settlementAccOperationMap.get(settAccOperationPk);
//					BigDecimal quantityOperation = liveOperation.getMarketQuantity();
//					settlementAccOperationMap.put(settAccOperationPk, currentQuantity.add(quantityOperation));
//				}
//			}
//		}
//		return settlementAccOperationMap;
//	}
//	
//	
//	/**
//	 * Gets the physical certificate mark fact.
//	 *
//	 * @param processDate the process date
//	 * @param processPk the process pk
//	 * @return the physical certificate mark fact
//	 * @throws ServiceException the service exception
//	 */
//	@SuppressWarnings("unchecked")
//	public Map<Long,List<MarketFactForLiveOperation>> getPhysicalCertificateMarkFact(Date processDate, Long processPk) throws ServiceException{
//		StringBuilder querySQL = new StringBuilder();
//		Map<String,Object> parameters = new HashMap<String,Object>();
//		querySQL.append("Select mark.IdPhysicalCertMarketfactPk, phys.securities.idSecurityCodePk, 			");
//		querySQL.append("		mark.marketRate, mark.marketDate, mark.marketPrice, mark.quantity,			");
//		querySQL.append("		mark.indActive, phys.idPhysicalCertificatePk 								");
//		querySQL.append("From PhysicalCertMarketfact mark													");
//		querySQL.append("Inner Join mark.physicalCertificate phys											");
//		querySQL.append("Where phys.state IN (:stateList)	And phys.situation	In(:situationList)			");
//		querySQL.append("And   mark.indActive = :oneParam													");
//		querySQL.append("And   phys.securities.idSecurityCodePk In (										");
//		querySQL.append("					SELECT Distinct pric.securityCode								");
//		querySQL.append("					FROM ValuatorPriceResult pric									");
//		querySQL.append("					WHERE pric.valuatorProcessExecution.idProcessExecutionPk = :pk)	");
//		parameters.put("pk", processPk);
//		querySQL.append("Order By mark.indActive	Asc														");
//		
//		parameters.put("oneParam", BooleanType.YES.getCode());
//		parameters.put("situationList", Arrays.asList(SituationType.CUSTODY_VAULT.getCode(),
//													  SituationType.DEPOSITARY_OPERATIONS.getCode(),
//													  SituationType.DEPOSITARY_VAULT.getCode()));
//		
//		parameters.put("stateList", Arrays.asList(PhysicalCertificateStateType.CONFIRMED.getCode(),
//												  PhysicalCertificateStateType.AUTHORIZED.getCode(),
//												  StateType.DEPOSITED.getCode()));
//		
//		List<Object[]> objectData = findListByQueryString(querySQL.toString(), parameters);
//		Map<Long,List<MarketFactForLiveOperation>> physicalMarketFactMap = new HashMap<>();
//		for(Object[] data:  objectData){
//			if(physicalMarketFactMap.get(data[0])==null){
//				physicalMarketFactMap.put((Long)data[0], new ArrayList<MarketFactForLiveOperation>());
//			}
//			physicalMarketFactMap.get((Long)data[0]).add(populatePhysicalOperation(data));
//		}
//		return physicalMarketFactMap;
//	}
//	
//	/**
//	 * Populate physical operation.
//	 *
//	 * @param data the data
//	 * @return the market fact for live operation
//	 */
//	private MarketFactForLiveOperation populatePhysicalOperation(Object[] data) {
//		// TODO Auto-generated method stub
//		MarketFactForLiveOperation physicalOperation = new MarketFactForLiveOperation();
//		physicalOperation.setPhysicalMrkFactPk((Long) data[0]);
//		physicalOperation.setSecurityCodePk((String) data[1]);
//		physicalOperation.setMarketRate((BigDecimal) data[2]);
//		physicalOperation.setMarketDate((Date) data[3]);
//		physicalOperation.setMarketPrice((BigDecimal) data[4]);
//		physicalOperation.setMarketQuantity((BigDecimal) data[5]);
//		physicalOperation.setActive((Integer) data[6]);
//		physicalOperation.setCustodyOperationPk((Long) data[7]);
//		return physicalOperation;
//	}
//	
//	/**
//	 * Update physical market oper.
//	 *
//	 * @param custodyMrkFactId the custody mrk fact id
//	 * @param custodyOperationPk the custody operation pk
//	 * @param marketRate the market rate
//	 * @param marketDate the market date
//	 * @param marketPrice the market price
//	 * @param indHistoric the ind historic
//	 * @param quantity the quantity
//	 */
//	public void updatePhysicalMarketOper(Long custodyMrkFactId, Long custodyOperationPk, BigDecimal marketRate, Date marketDate, BigDecimal marketPrice, 
//			Integer indHistoric, BigDecimal quantity){
//		StringBuilder querySQL = new StringBuilder();
//		if (indHistoric.equals(BooleanType.YES.getCode())) {
//			querySQL.append("Update PhysicalCertMarketfact Set marketPrice = :marketPrice Where IdPhysicalCertMarketfactPk = :mrkFactPk");
//			Query query = em.createQuery(querySQL.toString());
//			query.setParameter("marketPrice", marketPrice);
//			query.setParameter("mrkFactPk", custodyMrkFactId);
//			query.executeUpdate();
//		} else {
//			PhysicalCertMarketfact physicalMarkFact = null;
//			querySQL.append("Update PhysicalCertMarketfact Set indActive = :zeroParam Where IdPhysicalCertMarketfactPk = :mrkFactPk");
//			physicalMarkFact = popuPhysicalMarketFact(marketDate, marketRate,marketPrice, custodyOperationPk, quantity);
//			Query query = em.createQuery(querySQL.toString());
//			query.setParameter("zeroParam", ComponentConstant.ZERO);
//			query.setParameter("mrkFactPk", custodyMrkFactId);
//			query.executeUpdate();
//			create(physicalMarkFact);
//		}
//	}
//	
//	/**
//	 * Popu physical market fact.
//	 *
//	 * @param mrkDate the mrk date
//	 * @param mrkRate the mrk rate
//	 * @param mrkPrice the mrk price
//	 * @param physicalCertificateFk the physical certificate fk
//	 * @param quantity the quantity
//	 * @return the physical cert marketfact
//	 */
//	public PhysicalCertMarketfact popuPhysicalMarketFact(Date mrkDate, BigDecimal mrkRate, BigDecimal mrkPrice, Long physicalCertificateFk, BigDecimal quantity){
//		PhysicalCertMarketfact physicalMarkFact = new PhysicalCertMarketfact();
//		physicalMarkFact.setMarketDate(mrkDate);
//		physicalMarkFact.setMarketRate(mrkRate);
//		physicalMarkFact.setMarketPrice(mrkPrice);
//		physicalMarkFact.setQuantity(quantity);
//		physicalMarkFact.setIndActive(ComponentConstant.ONE);
//		physicalMarkFact.setRegistryDate(CommonsUtilities.currentDateTime());
//		physicalMarkFact.setRegistryUser(USER_SESSION);
//		physicalMarkFact.setPhysicalCertificate(new PhysicalCertificate());
//		physicalMarkFact.getPhysicalCertificate().setIdPhysicalCertificatePk(physicalCertificateFk);
//		return physicalMarkFact;
//	}
//
//
//	/**
//	 * Gets the custody block market fact.
//	 *
//	 * @param processDate the process date
//	 * @param processPk the process pk
//	 * @return the custody block market fact
//	 * @throws ServiceException the service exception
//	 */
//	@SuppressWarnings("unchecked")
//	public Map<Long,List<MarketFactForLiveOperation>> getCustodyBlockMarketFact(Date processDate, Long processPk) throws ServiceException{
//		StringBuilder querySQL = new StringBuilder();
//		Map<String,Object> parameters = new HashMap<>();
//		querySQL.append("Select bop.idBlockOperationPk,	bop.securities.idSecurityCodePk, 						");
//		querySQL.append("		bop.participant.idParticipantPk, bop.holderAccount.idHolderAccountPk,			"); 
//		querySQL.append("		mark.marketDate, mark.marketRate, mark.marketPrice, mark.actualBlockBalance,	");
//		querySQL.append("		mark.indActive, mark.idBlockMarketFactOperatPk, mark.originalBlockBalance		");
//		querySQL.append("From BlockMarketFactOperation mark														");
//		querySQL.append("Inner Join mark.blockOperation bop														");
//		querySQL.append("Where bop.securities.idSecurityCodePk IN(												");
//		querySQL.append("					SELECT Distinct pric.securityCode									");
//		querySQL.append("					FROM ValuatorPriceResult pric										");
//		querySQL.append("					WHERE pric.valuatorProcessExecution.idProcessExecutionPk = :pk	)	");
//		querySQL.append("And   mark.actualBlockBalance > 0	And bop.blockState = :blockState					");
//		querySQL.append("And   mark.indActive = :oneParam															");
//		querySQL.append("Order By mark.indActive Asc															"); 
//		parameters.put("blockState", AffectationStateType.BLOCKED.getCode());
//		parameters.put("pk", processPk);
//		parameters.put("oneParam", BooleanType.YES.getCode());
//		List<Object[]> objectData = findListByQueryString(querySQL.toString(), parameters);
//		Map<Long,List<MarketFactForLiveOperation>> settlementMarketFactMap = new HashMap<>();
//		for(Object[] data:  objectData){
//			if(settlementMarketFactMap.get(data[0])==null){
//				settlementMarketFactMap.put((Long)data[0], new ArrayList<MarketFactForLiveOperation>());
//			}
//			settlementMarketFactMap.get(data[0]).add(populateCustodyLiveOperation(data, BooleanType.YES.getCode()));
//		}
//		return settlementMarketFactMap;
//	}
//	
//	/**
//	 * Gets the custody accreditation market fact.
//	 *
//	 * @param processDate the process date
//	 * @param processPk the process pk
//	 * @return the custody accreditation market fact
//	 * @throws ServiceException the service exception
//	 */
//	@SuppressWarnings("unchecked")
//	public Map<Long,List<MarketFactForLiveOperation>> getCustodyAccreditationMarketFact(Date processDate, Long processPk) throws ServiceException{
//		StringBuilder querySQL = new StringBuilder();
//		Map<String,Object> parameters = new HashMap<>();
//		querySQL.append("Select bop.idAccreditationDetailPk,			 										");
//		querySQL.append("		sec.idSecurityCodePk, part.idParticipantPk, ha.idHolderAccountPk, 				");
//		querySQL.append("		mark.marketDate, mark.marketRate, mark.marketPrice, mark.quantityToAccredit,	");
//		querySQL.append("		mark.indActive, mark.idAccreditationMarketFactPk, null							");
//		querySQL.append("From AccreditationMarketFact mark														");
//		querySQL.append("Inner Join mark.accreditationDetailFk bop												");
//		querySQL.append("Inner Join bop.accreditationOperation acOper											");
//		querySQL.append("Inner Join bop.idParticipantFk part													");
//		querySQL.append("Inner Join bop.holderAccount ha														");
//		querySQL.append("Inner Join bop.security sec															");
//		querySQL.append("Where sec.idSecurityCodePk IN(															");
//		querySQL.append("					SELECT Distinct pric.securityCode									");
//		querySQL.append("					FROM ValuatorPriceResult pric										");
//		querySQL.append("					WHERE pric.valuatorProcessExecution.idProcessExecutionPk = :pk	)	");		
//		querySQL.append("And acOper.accreditationState IN (:confirmedState)	And mark.indActive = :oneParam		");
//		querySQL.append("Order By mark.indActive Asc															");
//		
//		parameters.put("pk", processPk);
//		parameters.put("oneParam", BooleanType.YES.getCode());
//		parameters.put("confirmedState",Arrays.asList(AccreditationOperationStateType.CONFIRMED.getCode()));
//		
//		List<Object[]> objectData = findListByQueryString(querySQL.toString(), parameters);
//		Map<Long,List<MarketFactForLiveOperation>> settlementMarketFactMap = new HashMap<>(); 
//		for(Object[] data:  objectData){
//			if(settlementMarketFactMap.get(data[0])==null){
//				settlementMarketFactMap.put((Long)data[0], new ArrayList<MarketFactForLiveOperation>());
//			}
//			settlementMarketFactMap.get(data[0]).add(populateCustodyLiveOperation(data, BooleanType.NO.getCode()));
//		}
//		return settlementMarketFactMap;
//	}
//	
//	/**
//	 * Populate custody live operation.
//	 *
//	 * @param data the data
//	 * @param indBlockOperation the ind block operation
//	 * @return the market fact for live operation
//	 */
//	private MarketFactForLiveOperation populateCustodyLiveOperation(Object[] data, Integer indBlockOperation){
//		MarketFactForLiveOperation liveOperation = new MarketFactForLiveOperation();
//		liveOperation.setCustodyOperationPk((Long) data[0]);
//		liveOperation.setIndBlockOperation(indBlockOperation);
//		liveOperation.setSecurityCodePk((String) data[1]);
//		liveOperation.setParticipantPk((Long) data[2]);
//		liveOperation.setHolderAccountPk((Long) data[3]);
//		liveOperation.setMarketDate((Date) data[4]);
//		liveOperation.setMarketRate((BigDecimal) data[5]);
//		liveOperation.setMarketPrice((BigDecimal) data[6]);
//		liveOperation.setMarketQuantity((BigDecimal) data[7]);
//		liveOperation.setActive((Integer) data[8]);
//		liveOperation.setCustodyOperationMrkFactPk((Long) data[9]);
//		if(indBlockOperation.equals(BooleanType.YES.getCode())){
//			liveOperation.setInitialQuantity((BigDecimal)data[10]);
//		}
//		return liveOperation;
//	}
//	
//	/**
//	 * Gets the settlement account market fact.
//	 *
//	 * @param processDate the process date
//	 * @param processPk the process pk
//	 * @return the settlement account market fact
//	 * @throws ServiceException the service exception
//	 */
//	@SuppressWarnings("unchecked")
//	public Map<Long,List<MarketFactForLiveOperation>> getSettlementAccountMarketFact(Date processDate, Long processPk) throws ServiceException{
//		StringBuilder querySQL = new StringBuilder();
//		Map<String,Object> parameters = new HashMap<>();
//		querySQL.append("Select mech.idMechanismOperationPk, 0,	mark.idSettAccountMarketfactPk,		 			");
//		querySQL.append("		sec.idSecurityCodePk, part.idParticipantPk, ha.idHolderAccountPk, 				");
//		querySQL.append("		mark.marketDate, mark.marketRate, mark.marketPrice, mark.marketQuantity,		");
//		querySQL.append("		mark.indActive, hao.role, sett.idSettlementAccountPk,							");
//		querySQL.append("		mech.mechanisnModality.id.idNegotiationMechanismPk								");
//		querySQL.append("From SettlementAccountMarketfact mark													");
//		querySQL.append("Inner Join mark.settlementAccountOperation sett										");
//		querySQL.append("Inner Join sett.settlementOperation settOp												");
//		querySQL.append("Inner Join sett.holderAccountOperation hao												");
//		querySQL.append("Inner Join hao.inchargeStockParticipant part											");
//		querySQL.append("Inner Join hao.holderAccount ha														");
//		querySQL.append("Inner Join hao.mechanismOperation mech													");
//		querySQL.append("Inner Join mech.securities sec															");
//		querySQL.append("Where hao.operationPart = :part And													");
//		querySQL.append("	   mech.operationState IN (:casSettled) 	And											");
//		querySQL.append("	   mech.indReportingBalance = :oneParam And											");
//		querySQL.append("	   sett.operationState = :regState		And											");
//		querySQL.append("	   mark.indActive = :oneParam			And											");
//		querySQL.append("	   settOp.indPartial = :zeroParam		And											");
//		querySQL.append("	   mark.indActive = :oneParam			And											");
//		querySQL.append("	   sec.idSecurityCodePk IN(															");
//		querySQL.append("					SELECT Distinct pric.securityCode									");
//		querySQL.append("					FROM ValuatorPriceResult pric										");
//		querySQL.append("					WHERE pric.valuatorProcessExecution.idProcessExecutionPk = :pk	)	");
//		
//		// Traemos todos los que estan autorizados
//		StringBuilder sbQuery = new StringBuilder();
//		sbQuery.append("	SELECT SU FROM SettlementUnfulfillment SU");
//		sbQuery.append("	inner join fetch SU.operationUnfulfillment OU ");
//		sbQuery.append("	inner join fetch OU.settlementOperation SO ");
//		sbQuery.append("	WHERE SU.requestState = 1543 ");
//
//		Query query = em.createQuery(sbQuery.toString());
//		
//		List <SettlementUnfulfillment> tets = (List <SettlementUnfulfillment>)query.getResultList();
//		
//		List<Long> lstSettperation = new ArrayList<Long>();
//		
//		for (SettlementUnfulfillment oepartion: tets) {
//			lstSettperation.add(oepartion.getOperationUnfulfillment().getSettlementOperation().getIdSettlementOperationPk());
//		}
//		
//		if(tets!=null&&tets.size()>0)
//			querySQL.append("	and   sett.settlementOperation.idSettlementOperationPk NOT IN(	:lstSettlementOperation) 						    ");
//		
//		querySQL.append("Order By mech.idMechanismOperationPk,	mark.indActive Asc								");
//		parameters.put("part", ComponentConstant.TERM_PART);
//		parameters.put("oneParam", ComponentConstant.ONE);
//		parameters.put("zeroParam", ComponentConstant.ZERO);
//		parameters.put("regState", HolderAccountOperationStateType.CONFIRMED.getCode());
//		
//		List<Integer> states = new ArrayList<Integer>();
//		states.add(MechanismOperationStateType.CASH_SETTLED.getCode());
//		states.add(MechanismOperationStateType.CANCELED_TERM_STATE.getCode());
//		parameters.put("casSettled", states);
//		parameters.put("pk", processPk);
//		
//		if(tets!=null&&tets.size()>0)
//			parameters.put("lstSettlementOperation", lstSettperation);
//		
//		List<Object[]> objectData = findListByQueryString(querySQL.toString(), parameters);
//		Map<Long,List<MarketFactForLiveOperation>> settlementMarketFactMap = new HashMap<>(); 
//		for(Object[] data:  objectData){
//			if(settlementMarketFactMap.get(data[0])==null){
//				settlementMarketFactMap.put((Long)data[0], new ArrayList<MarketFactForLiveOperation>());
//			}
//			settlementMarketFactMap.get(data[0]).add(populateSettlementLiveOperation(data));
//		}
//		return settlementMarketFactMap;
//	}
//	
//	/**
//	 * Gets the settlement account market fact. issue 1040
//	 *
//	 * @param processDate the process date
//	 * @param processPk the process pk
//	 * @return the settlement account market fact
//	 * @throws ServiceException the service exception
//	 */
//	@SuppressWarnings("unchecked")
//	public Map<Long,List<MarketFactForLiveOperation>> getSelidSettlementAccountMarketFact(Date processDate, Long processPk) throws ServiceException{
//		
//		StringBuilder querySql = new StringBuilder();
//		
//		querySql.append("   SELECT                                                                                                                 ");
//		querySql.append("      MO.ID_MECHANISM_OPERATION_PK,                                                                                       ");
//		querySql.append("      0,                                                                                                                  ");
//		querySql.append("      SAM.ID_ACCOUNT_MARKETFACT_PK,                                                                                       ");
//		querySql.append("      SEC.ID_SECURITY_CODE_PK,                                                                                            ");
//		querySql.append("      PAR.ID_PARTICIPANT_PK,                                                                                              ");
//		querySql.append("      HA.ID_HOLDER_ACCOUNT_PK,                                                                                            ");
//		querySql.append("      SAM.MARKET_DATE,                                                                                                    ");
//		querySql.append("      SAM.MARKET_RATE,                                                                                                    ");
//		querySql.append("      SAM.MARKET_PRICE,                                                                                                   ");
//		querySql.append("      SAM.MARKET_QUANTITY,                                                                                                ");
//		querySql.append("      SAM.IND_ACTIVE,                                                                                                     ");
//		querySql.append("      HAO.ROLE,                                                                                                           ");
//		querySql.append("      SAO.ID_SETTLEMENT_ACCOUNT_PK,                                                                                       ");
//		querySql.append("      MO.ID_NEGOTIATION_MECHANISM_FK                                                                                      ");
//		querySql.append("   from SETTLEMENT_ACCOUNT_MARKETFACT SAM                                                                                 ");
//		querySql.append("   inner join SETTLEMENT_ACCOUNT_OPERATION SAO on SAM.ID_SETTLEMENT_ACCOUNT_FK = SAO.ID_SETTLEMENT_ACCOUNT_PK             ");
//		querySql.append("   inner join SETTLEMENT_OPERATION SO on SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK                   ");
//		querySql.append("   inner join HOLDER_ACCOUNT_OPERATION HAO on SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = HAO.ID_HOLDER_ACCOUNT_OPERATION_PK     ");
//		querySql.append("   inner join Participant PAR on HAO.ID_INCHARGE_STOCK_PARTICIPANT = PAR.ID_PARTICIPANT_PK                                ");
//		querySql.append("   inner join HOLDER_ACCOUNT HA on HAO.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK                                     ");
//		querySql.append("   inner join MECHANISM_OPERATION MO on HAO.ID_MECHANISM_OPERATION_FK=MO.ID_MECHANISM_OPERATION_PK                        ");
//		querySql.append("   inner join SECURITY SEC on MO.ID_SECURITY_CODE_FK=SEC.ID_SECURITY_CODE_PK                                              ");
//		querySql.append("   inner join SETTLEMENT_DATE_OPERATION SDO ON SO.ID_SETTLEMENT_OPERATION_PK = SDO.ID_SETTLEMENT_OPERATION_FK             ");
//		querySql.append("   INNER JOIN SETTLEMENT_REQUEST SR ON SR.ID_SETTLEMENT_REQUEST_PK = SDO.ID_SETTLEMENT_REQUEST_FK                         ");
//		querySql.append("   where                                                                                                                  ");
//		querySql.append("       HAO.OPERATION_PART = 1                                                                                             ");
//		querySql.append("       and MO.OPERATION_STATE = 614                                                                                       ");
//		//querySql.append("       --and MO.IND_REPORTING_BALANCE=1                                                                                 ");
//		querySql.append("       and SAO.OPERATION_STATE = 625                                                                                      ");
//		querySql.append("       and SAM.IND_ACTIVE = 1                                                                                             ");
//		querySql.append("       and SO.IND_PARTIAL = 0                                                                                             ");
//		querySql.append("       AND SR.REQUEST_TYPE = 2018                                                                                         ");
//		querySql.append("       AND SR.REQUEST_STATE = 1543                                                                                        ");
//		querySql.append("       AND SO.IND_EXTENDED = 1                                                                                            ");
//		querySql.append("        AND HAO.ROLE = 2                                                                                            	   ");
//		querySql.append("       AND SO.IND_UNFULFILLED <> 1                                                                                        ");
//		querySql.append("       and  SEC.ID_SECURITY_CODE_PK in ( select distinct VPR.ID_SECURITY_CODE_FK                                          ");
//		querySql.append("   									 from VALUATOR_PRICE_RESULT VPR                                                    ");
//		querySql.append("   									 where VPR.ID_PROCESS_EXECUTION_FK = :pk )                                         ");
//		querySql.append("   order by                                                                                                               ");
//		querySql.append("       MO.ID_MECHANISM_OPERATION_PK,                                                                                      ");
//		querySql.append("       SAM.IND_ACTIVE Asc                                                                                                 ");
//		
//		Query querySec = em.createNativeQuery(querySql.toString());
//		
//		querySec.setParameter("pk", processPk);
//		
//		List<Object[]>  objectData = (List<Object[]>)querySec.getResultList();
//		
//		Map<Long,List<MarketFactForLiveOperation>> settlementMarketFactMap = new HashMap<>(); 
//		
//		for(Object[] data:  objectData){
//			
//			Long test2 = null;
//			BigDecimal test = (BigDecimal) data[0];
//			test2 = test.longValue();
//			
//			if(settlementMarketFactMap.get(test2)==null){
//				
//				settlementMarketFactMap.put(test2, new ArrayList<MarketFactForLiveOperation>());
//			}
//			settlementMarketFactMap.get(test2).add(populateSelidSettlementLiveOperation(data));
//		}
//		return settlementMarketFactMap;
//	}
//	
//	/**
//	 * Populate settlement live operation.
//	 *
//	 * @param data the data
//	 * @return the market fact for live operation
//	 */
//	private MarketFactForLiveOperation populateSettlementLiveOperation(Object[] data){
//		MarketFactForLiveOperation liveOperation = new MarketFactForLiveOperation();
//		liveOperation.setMechanismOperationPk((Long) data[0]);
//		liveOperation.setSettlemetMarketFactPk((Long) data[2]);
//		liveOperation.setSecurityCodePk((String) data[3]);
//		liveOperation.setParticipantPk((Long) data[4]);
//		liveOperation.setHolderAccountPk((Long) data[5]);
//		liveOperation.setMarketDate((Date) data[6]);
//		liveOperation.setMarketRate((BigDecimal) data[7]);
//		liveOperation.setMarketPrice((BigDecimal) data[8]);
//		liveOperation.setMarketQuantity((BigDecimal) data[9]);
//		liveOperation.setActive((Integer) data[10]);
//		liveOperation.setRole((Integer) data[11]);
//		liveOperation.setSettlementAccOperationPk((Long) data[12]);
//		liveOperation.setMechanismType((Long) data[13]);
//		return liveOperation;
//	}
//	
//	/**
//	 * Populate settlement live operation. issue 1040
//	 *
//	 * @param data the data
//	 * @return the market fact for live operation
//	 */
//	private MarketFactForLiveOperation populateSelidSettlementLiveOperation(Object[] data){
//		MarketFactForLiveOperation liveOperation = new MarketFactForLiveOperation();
//		liveOperation.setMechanismOperationPk(Long.parseLong(data[0].toString()));
//		liveOperation.setSettlemetMarketFactPk(Long.parseLong(data[2].toString()));
//		liveOperation.setSecurityCodePk(data[3].toString());
//		liveOperation.setParticipantPk(Long.parseLong(data[4].toString()));
//		liveOperation.setHolderAccountPk(Long.parseLong(data[5].toString()));
//		liveOperation.setMarketDate((Date) data[6]);
//		liveOperation.setMarketRate((BigDecimal) data[7]);
//		liveOperation.setMarketPrice((BigDecimal) data[8]);
//		liveOperation.setMarketQuantity((BigDecimal) data[9]);
//		liveOperation.setActive(Integer.valueOf(data[10].toString()));
//		liveOperation.setRole(Integer.valueOf(data[11].toString()));
//		liveOperation.setSettlementAccOperationPk(Long.parseLong(data[12].toString()));
//		liveOperation.setMechanismType(Long.parseLong(data[13].toString()));
//		return liveOperation;
//	}
//	
//	/**
//	 * Gets the security homologated.
//	 *
//	 * @param processDate the process date
//	 * @param processPk the process pk
//	 * @return the security homologated
//	 */
//	@SuppressWarnings("unchecked")
//	public Map<String,Object[]> getSecurityHomologated(Date processDate, Long processPk){
//		StringBuilder querySQL = new StringBuilder();
//		querySQL.append("Select pric.securityCode, pric.marketRate, pric.marketPrice, pric.marketDate					");
//		querySQL.append("From ValuatorPriceResult pric 																	");
//		querySQL.append("Where pric.indHomologated = :oneParam 		And													");
//		querySQL.append("	   trunc(pric.processDate) = trunc(:processDate) And										");
//		querySQL.append("	   pric.valuatorProcessExecution.idProcessExecutionPk = :processPk							");
//		Query query = em.createQuery(querySQL.toString());
//		query.setParameter("oneParam", ComponentConstant.ONE);
//		query.setParameter("processDate", processDate);
//		query.setParameter("processPk", processPk);
//		
//		List<Object[]> objectData = query.getResultList();
//		Map<String,Object[]> securityMap = new HashMap<>();
//		for(Object[] data: objectData){
//			if(securityMap.get(data[0])==null){
//			}
//			Object[] dataArray = {data[1],data[2],data[3]};
//			securityMap.put((String)data[0], dataArray);
//		}
//		return securityMap;
//	}
//	
//	/**
//	 * Gets the price result for live operation.
//	 * Method to get all valuatorPrice result related with live operation
//	 * @param processDate the process date
//	 * @param processPk the process pk
//	 * @return the price result for live operation
//	 * @throws ServiceException the service exception
//	 */
//	@SuppressWarnings("unchecked")
//	public Map<String,List<ValuatorPriceResult>> getPriceResultForLiveOperation(Date processDate, Long processPk) throws ServiceException{
//		StringBuilder querySQL = new StringBuilder();
//		StringBuilder phyQuerySQL = new StringBuilder();
//		
//		querySQL.append("Select pric																					");
//		querySQL.append("From ValuatorPriceResult pric 																	");
//		querySQL.append("Inner Join pric.security sec 																	");
//		querySQL.append("Inner Join sec.holderAccountBalances hab														");
//		querySQL.append("Where pric.indHomologated = :zeroParam 													And	");
//		querySQL.append("	   trunc(pric.processDate) = trunc(:processDate) 										And	");
//		querySQL.append("	   pric.valuatorProcessExecution.idProcessExecutionPk = :processPk						And	");
//		querySQL.append("	  (hab.pawnBalance > :zeroParam			Or 	hab.banBalance > :zeroParam 			Or		");
//		querySQL.append("	   hab.otherBlockBalance > :zeroParam	Or 	hab.accreditationBalance > :zeroParam 	Or		");
//		querySQL.append("	   hab.purchaseBalance > :zeroParam		Or 	hab.saleBalance > :zeroParam 			Or		");
//		querySQL.append("	   hab.reportingBalance > :zeroParam	Or 	hab.reportedBalance > :zeroParam 		  )		");
////		querySQL.append("	   sec.physicalBalance > :zeroParam															");
////		querySQL.append("	   sec.idSecurityCodePk In(																	");
//		
//		/**Including also physical certificate*/
//		phyQuerySQL.append("Select pric																					");
//		phyQuerySQL.append("From ValuatorPriceResult pric 																");
//		phyQuerySQL.append("Where pric.securityCode In(			 														");
//		phyQuerySQL.append("		Select Distinct phys.securities.idSecurityCodePk									");
//		phyQuerySQL.append("		From PhysicalCertMarketfact mark													");
//		phyQuerySQL.append("		Inner Join mark.physicalCertificate phys											");
//		phyQuerySQL.append("		Where phys.state IN (:stateList)	And phys.situation	In(:situationList)	And   	");
//		phyQuerySQL.append("			  mark.indActive = :oneParam												   )");
//		phyQuerySQL.append("And pric.valuatorProcessExecution.idProcessExecutionPk = :processPk							");
//
//		/**Setting data*/
//		Query query = em.createQuery(querySQL.toString());
//		query.setParameter("processPk", processPk);
//		query.setParameter("processDate", processDate);
//		query.setParameter("zeroParam", ComponentConstant.ZERO);
//
//		/**Getting data of state and situation*/
//		Integer[] stateData = {PhysicalCertificateStateType.CONFIRMED.getCode(),PhysicalCertificateStateType.AUTHORIZED.getCode(),StateType.DEPOSITED.getCode()};
//		Integer[] situtationData = {SituationType.CUSTODY_VAULT.getCode(),SituationType.DEPOSITARY_OPERATIONS.getCode(),SituationType.DEPOSITARY_VAULT.getCode()};
//		
//		/**Setting data at physical*/
//		Query phyQuery = em.createQuery(phyQuerySQL.toString());
//		phyQuery.setParameter("oneParam", ComponentConstant.ONE);
//		phyQuery.setParameter("stateList", Arrays.asList(stateData));
//		phyQuery.setParameter("situationList", Arrays.asList(situtationData));
//		phyQuery.setParameter("processPk", processPk);
//
//		/**Map to iterate all prices with securityCode like key*/
//		Map<String,List<ValuatorPriceResult>> mapWithPrices = new HashMap<>();
//		
//		/**Iterate all prices*/
//		List<ValuatorPriceResult> desmaterializationPrices = (List<ValuatorPriceResult>)query.getResultList();
//		List<ValuatorPriceResult> physicalPrices = (List<ValuatorPriceResult>)phyQuery.getResultList();
//		
//		/**Verify if physical prices is different than null*/
//		if(physicalPrices!=null && !physicalPrices.isEmpty()){
//			desmaterializationPrices.addAll(physicalPrices);
//		}
//		
//		for(ValuatorPriceResult price: desmaterializationPrices){
//			String securityCode = price.getSecurityCode();
//			if(mapWithPrices.get(securityCode)==null){
//				mapWithPrices.put(securityCode, new ArrayList<ValuatorPriceResult>());
//			}
//			mapWithPrices.get(securityCode).add(price);
//		}
//		/**end of method*/
//		return mapWithPrices;
//	}
//	
//	/**
//	 * Gets the price result for live operation. issue 1040
//	 * Method to get all valuatorPrice result related with live operation
//	 * @param processDate the process date
//	 * @param processPk the process pk
//	 * @return the price result for live operation
//	 * @throws ServiceException the service exception
//	 */
//	@SuppressWarnings("unchecked")
//	public Map<String,List<ValuatorPriceResult>> getPriceResultForLiveSelidOperation(Date processDate, Long processPk) throws ServiceException{
//		StringBuilder querySQL = new StringBuilder();
//		StringBuilder phyQuerySQL = new StringBuilder();
//		
//		querySQL.append("Select pric																					");
//		querySQL.append("From ValuatorPriceResult pric 																	");
//		querySQL.append("Inner Join pric.security sec 																	");
//		querySQL.append("Inner Join sec.holderAccountBalances hab														");
//		querySQL.append("Where pric.indHomologated in( :zeroParam, :oneParam ) 													And	");
//		querySQL.append("	   trunc(pric.processDate) = trunc(:processDate) 										And	");
//		querySQL.append("	   pric.valuatorProcessExecution.idProcessExecutionPk = :processPk						And	");
//		querySQL.append("	  (hab.pawnBalance > :zeroParam			Or 	hab.banBalance > :zeroParam 			Or		");
//		querySQL.append("	   hab.availableBalance > :zeroParam	Or		");
//		querySQL.append("	   hab.otherBlockBalance > :zeroParam	Or 	hab.accreditationBalance > :zeroParam 	Or		");
//		querySQL.append("	   hab.purchaseBalance > :zeroParam		Or 	hab.saleBalance > :zeroParam 			Or		");
//		querySQL.append("	   hab.reportingBalance > :zeroParam	Or 	hab.reportedBalance > :zeroParam 		  )		");
////		querySQL.append("	   sec.physicalBalance > :zeroParam															");
////		querySQL.append("	   sec.idSecurityCodePk In(																	");
//		
//		/**Including also physical certificate*/
//		phyQuerySQL.append("Select pric																					");
//		phyQuerySQL.append("From ValuatorPriceResult pric 																");
//		phyQuerySQL.append("Where pric.securityCode In(			 														");
//		phyQuerySQL.append("		Select Distinct phys.securities.idSecurityCodePk									");
//		phyQuerySQL.append("		From PhysicalCertMarketfact mark													");
//		phyQuerySQL.append("		Inner Join mark.physicalCertificate phys											");
//		phyQuerySQL.append("		Where phys.state IN (:stateList)	And phys.situation	In(:situationList)	And   	");
//		phyQuerySQL.append("			  mark.indActive = :oneParam												   )");
//		phyQuerySQL.append("And pric.valuatorProcessExecution.idProcessExecutionPk = :processPk							");
//
//		/**Setting data*/
//		Query query = em.createQuery(querySQL.toString());
//		query.setParameter("processPk", processPk);
//		query.setParameter("processDate", processDate);
//		query.setParameter("zeroParam", ComponentConstant.ZERO);
//		query.setParameter("oneParam", ComponentConstant.ONE);
//
//		/**Getting data of state and situation*/
//		Integer[] stateData = {PhysicalCertificateStateType.CONFIRMED.getCode(),PhysicalCertificateStateType.AUTHORIZED.getCode(),StateType.DEPOSITED.getCode()};
//		Integer[] situtationData = {SituationType.CUSTODY_VAULT.getCode(),SituationType.DEPOSITARY_OPERATIONS.getCode(),SituationType.DEPOSITARY_VAULT.getCode()};
//		
//		/**Setting data at physical*/
//		Query phyQuery = em.createQuery(phyQuerySQL.toString());
//		phyQuery.setParameter("oneParam", ComponentConstant.ONE);
//		phyQuery.setParameter("stateList", Arrays.asList(stateData));
//		phyQuery.setParameter("situationList", Arrays.asList(situtationData));
//		phyQuery.setParameter("processPk", processPk);
//
//		/**Map to iterate all prices with securityCode like key*/
//		Map<String,List<ValuatorPriceResult>> mapWithPrices = new HashMap<>();
//		
//		/**Iterate all prices*/
//		List<ValuatorPriceResult> desmaterializationPrices = (List<ValuatorPriceResult>)query.getResultList();
//		List<ValuatorPriceResult> physicalPrices = (List<ValuatorPriceResult>)phyQuery.getResultList();
//		
//		/**Verify if physical prices is different than null*/
//		if(physicalPrices!=null && !physicalPrices.isEmpty()){
//			desmaterializationPrices.addAll(physicalPrices);
//		}
//		
//		for(ValuatorPriceResult price: desmaterializationPrices){
//			String securityCode = price.getSecurityCode();
//			if(mapWithPrices.get(securityCode)==null){
//				mapWithPrices.put(securityCode, new ArrayList<ValuatorPriceResult>());
//			}
//			mapWithPrices.get(securityCode).add(price);
//		}
//		/**end of method*/
//		return mapWithPrices;
//	}
//	
//	/**
//	 * Update custoy market oper.
//	 *
//	 * @param custodyMrkFactId the custody mrk fact id
//	 * @param custodyOperationPk the custody operation pk
//	 * @param marketRate the market rate
//	 * @param marketDate the market date
//	 * @param marketPrice the market price
//	 * @param indBlockOperation the ind block operation
//	 * @param quantity the quantity
//	 * @param initQuantity the init quantity
//	 * @return the object
//	 */
//	public Object updateCustoyMarketOper(Long custodyMrkFactId, Long custodyOperationPk, BigDecimal marketRate, Date marketDate, BigDecimal marketPrice, 
//									   Integer indBlockOperation, BigDecimal quantity, BigDecimal initQuantity){
//		StringBuilder querySQL = new StringBuilder();
////		if(indHistoric.equals(BooleanType.YES.getCode())){
////			if(indBlockOperation.equals(BooleanType.YES.getCode())){
////				querySQL.append("Update BlockMarketFactOperation Set marketPrice = :marketPrice Where idBlockMarketFactOperatPk = :mrkFactPk");
////			}else{
////				querySQL.append("Update AccreditationMarketFact Set marketPrice = :marketPrice Where idAccreditationMarketFactPk = :mrkFactPk");
////			}
////			Query query = em.createQuery(querySQL.toString());
////			query.setParameter("marketPrice", marketPrice);
////			query.setParameter("mrkFactPk", custodyMrkFactId);
////			query.executeUpdate();
////		}else{
//			Object custodyMarkFact = null;
//			if(indBlockOperation.equals(BooleanType.YES.getCode())){
//				querySQL.append("Update BlockMarketFactOperation Set indActive = :zeroParam Where idBlockMarketFactOperatPk = :mrkFactPk");
//				custodyMarkFact = popuBloclkMarketFact(marketDate, marketRate, marketPrice, custodyOperationPk, quantity, initQuantity);
//			}else{
//				querySQL.append("Update AccreditationMarketFact Set indActive = :zeroParam Where idAccreditationMarketFactPk = :mrkFactPk");
//				custodyMarkFact = pupulateAccreditMarketFact(marketDate, marketRate, marketPrice, custodyOperationPk, quantity);
//			}
//			
//			Query query = em.createQuery(querySQL.toString());
//			query.setParameter("zeroParam", ComponentConstant.ZERO);
//			query.setParameter("mrkFactPk", custodyMrkFactId);
//			query.executeUpdate();
//		
//			create(custodyMarkFact);
//			
//			return custodyMarkFact;
////		}
//	}
//	
//	
//	
//	/**
//	 * Popu settl market fact.
//	 *
//	 * @param mrkDate the mrk date
//	 * @param mrkRate the mrk rate
//	 * @param mrkPrice the mrk price
//	 * @param quantity the quantity
//	 * @param settAccountOperationPk the sett account operation pk
//	 * @return the settlement account marketfact
//	 */
//	public SettlementAccountMarketfact popuSettlMarketFact(Date mrkDate, BigDecimal mrkRate, BigDecimal mrkPrice, BigDecimal quantity, 
//																		   Long settAccountOperationPk){
//		SettlementAccountMarketfact settlementMarkFact = new SettlementAccountMarketfact();
//		settlementMarkFact.setMarketDate(mrkDate);
//		settlementMarkFact.setMarketRate(mrkRate.equals(ValuatorConstants.UNKNOW_RATE)?null:mrkRate);
//		settlementMarkFact.setMarketPrice(mrkPrice);
//		settlementMarkFact.setIndActive(ComponentConstant.ONE);
//		settlementMarkFact.setMarketQuantity(quantity);
//		settlementMarkFact.setSettlementAccountOperation(new SettlementAccountOperation(settAccountOperationPk));				
//		return settlementMarkFact;
//	}
//	
//	/**
//	 * Popu bloclk market fact.
//	 *
//	 * @param mrkDate the mrk date
//	 * @param mrkRate the mrk rate
//	 * @param mrkPrice the mrk price
//	 * @param blockOperationFk the block operation fk
//	 * @param quantity the quantity
//	 * @param initialQuantity the initial quantity
//	 * @return the block market fact operation
//	 */
//	public BlockMarketFactOperation popuBloclkMarketFact(Date mrkDate, BigDecimal mrkRate, BigDecimal mrkPrice, Long blockOperationFk, BigDecimal quantity, 
//														 BigDecimal initialQuantity){
//		BlockMarketFactOperation blockOperationMarkFact = new BlockMarketFactOperation();
//		blockOperationMarkFact.setMarketDate(mrkDate);
//		blockOperationMarkFact.setMarketRate(mrkRate.equals(ValuatorConstants.UNKNOW_RATE)?null:mrkRate);
//		blockOperationMarkFact.setMarketPrice(mrkPrice);
//		blockOperationMarkFact.setIndActive(ComponentConstant.ONE);
//		blockOperationMarkFact.setOriginalBlockBalance(initialQuantity);
//		blockOperationMarkFact.setActualBlockBalance(quantity);
//		blockOperationMarkFact.setBlockMarketState(BlockMarketFactOperationStateType.REGISTER.getCode());
//		blockOperationMarkFact.setRegistryDate(CommonsUtilities.currentDateTime());
//		blockOperationMarkFact.setRegistryUser(USER_SESSION);
//		blockOperationMarkFact.setBlockOperation(new BlockOperation());
//		blockOperationMarkFact.getBlockOperation().setIdBlockOperationPk(blockOperationFk);
//		return blockOperationMarkFact;
//	}
//	
//	/**
//	 * Pupulate accredit market fact.
//	 *
//	 * @param mrkDate the mrk date
//	 * @param mrkRate the mrk rate
//	 * @param mrkPrice the mrk price
//	 * @param accreditationFk the accreditation fk
//	 * @param quantity the quantity
//	 * @return the accreditation market fact
//	 */
//	public AccreditationMarketFact pupulateAccreditMarketFact(Date mrkDate, BigDecimal mrkRate, BigDecimal mrkPrice, Long accreditationFk, BigDecimal quantity){
//		AccreditationMarketFact accreditationMarkFact = new AccreditationMarketFact();
//		accreditationMarkFact.setMarketDate(mrkDate);
//		accreditationMarkFact.setMarketRate(mrkRate.equals(ValuatorConstants.UNKNOW_RATE)?null:mrkRate);
//		accreditationMarkFact.setMarketPrice(mrkPrice);
//		accreditationMarkFact.setIndActive(ComponentConstant.ONE);
//		accreditationMarkFact.setQuantityToAccredit(quantity);
//		accreditationMarkFact.setAccreditationDetailFk(new AccreditationDetail());
//		accreditationMarkFact.getAccreditationDetailFk().setIdAccreditationDetailPk(accreditationFk);
//		return accreditationMarkFact;
//	}
//	
//	/**
//	 * Update settlement market fact by sett op.
//	 *
//	 * @param keyList the key list
//	 */
//	public void updateSettlementMarketFactBySettOp(List<Long> keyList) {
//		// TODO Auto-generated method stub
//		StringBuilder querySQL = new StringBuilder();
//		querySQL.append("Update SettlementAccountMarketfact Set indActive = :zeroParam ");
//		querySQL.append("Where settlementAccountOperation.idSettlementAccountPk IN (:keyList)");
//		Query query = em.createQuery(querySQL.toString());
//		query.setParameter("zeroParam", ComponentConstant.ZERO);
//		query.setParameter("keyList", keyList);
//		query.executeUpdate();
//	}
//	
//	/**
//	 * Update custody accred market fact by custody.
//	 *
//	 * @param keyList the key list
//	 */
//	public void updateCustodyAccredMarketFactByCustody(List<Long> keyList) {
//		// TODO Auto-generated method stub
//		StringBuilder querySQL = new StringBuilder();
//		querySQL.append("Update AccreditationMarketFact Set indActive = :zeroParam ");
//		querySQL.append("Where accreditationDetailFk.idAccreditationDetailPk IN (:keyList)");
//		Query query = em.createQuery(querySQL.toString());
//		query.setParameter("zeroParam", ComponentConstant.ZERO);
//		query.setParameter("keyList", keyList);
//		query.executeUpdate();
//	}
//	
//	/**
//	 * Update custody block market fact by custody.
//	 *
//	 * @param keyList the key list
//	 */
//	public void updateCustodyBlockMarketFactByCustody(List<Long> keyList) {
//		// TODO Auto-generated method stub
//		StringBuilder querySQL = new StringBuilder();
//		querySQL.append("Update BlockMarketFactOperation Set indActive = :zeroParam ");
//		querySQL.append("Where blockOperation.idBlockOperationPk IN (:keyList)");
//		Query query = em.createQuery(querySQL.toString());
//		query.setParameter("zeroParam", ComponentConstant.ZERO);
//		query.setParameter("keyList", keyList);
//		query.executeUpdate();
//	}
//	
//	/**
//	 * Update settl market fact.
//	 *
//	 * @param settMarktFactPk the sett markt fact pk
//	 * @param role the role
//	 * @param marketRate the market rate
//	 * @param marketDate the market date
//	 * @param marketPrice the market price
//	 * @param indHistoric the ind historic
//	 * @param settAccOperationPk the sett acc operation pk
//	 * @param quantity the quantity
//	 */
//	public void updateSettlMarketFact(Long settMarktFactPk, Integer role, BigDecimal marketRate, Date marketDate, BigDecimal marketPrice, 
//													Integer indHistoric, Long settAccOperationPk, BigDecimal quantity){
//		StringBuilder querySQL = new StringBuilder();
////		if(indHistoric.equals(BooleanType.YES.getCode())){
////			querySQL.append("Update SettlementAccountMarketfact Set marketPrice = :marketPrice Where idSettAccountMarketfactPk = :settMrkPk");
////			Query query = em.createQuery(querySQL.toString());
////			query.setParameter("marketPrice", marketPrice);
////			query.setParameter("settMrkPk", settMarktFactPk);
////			query.executeUpdate();
////		}else{
//			querySQL.append("Update SettlementAccountMarketfact Set indActive = :zeroParam Where idSettAccountMarketfactPk = :settMrkPk");
//			Query query = em.createQuery(querySQL.toString());
//			query.setParameter("zeroParam", ComponentConstant.ZERO);
//			query.setParameter("settMrkPk", settMarktFactPk);
//			query.executeUpdate();
//			
//			SettlementAccountMarketfact settlementMarkFact = popuSettlMarketFact(marketDate, marketRate, marketPrice, quantity, settAccOperationPk);
//			create(settlementMarkFact);
////		}
//	}
//	
//	/**
//	 * Find valuat price.
//	 *
//	 * @param securityCode the security code
//	 * @param marketDate the market date
//	 * @param processDate the process date
//	 * @param marketRate the market rate
//	 * @param operationPk the operation pk
//	 * @return the valuator price result
//	 */
//	public ValuatorPriceResult findValuatPrice(String securityCode, Date marketDate, Date processDate, BigDecimal marketRate, Long operationPk){
//		StringBuilder querySQL = new StringBuilder();
//		Map<String,Object> parameters = new HashMap<>();
//		querySQL.append("Select pricRes 																		");
//		querySQL.append("From ValuatorPriceResult pricRes														");
//		querySQL.append("Left Outer Join pricRes.mechanismOperation mech										");
//		querySQL.append("Where Trunc(pricRes.marketDate) 	= Trunc(:marketDate) 	And							");
//		querySQL.append("	   Trunc(pricRes.processDate) 	= Trunc(:processDate) 	And							");
//		querySQL.append("	   pricRes.marketRate 			= :marketRate		 	And							");
//		querySQL.append("	   pricRes.securityCode			= :secCode			 								");
//		parameters.put("marketDate", marketDate);
//		parameters.put("processDate", processDate);
//		parameters.put("marketRate", marketRate);
//		parameters.put("secCode", securityCode);
//		
//		if(operationPk!=null && !operationPk.equals(BigDecimal.ZERO.longValue())){
//			querySQL.append("  And mech.idMechanismOperationPk	= :operationPk	 								");
//			parameters.put("operationPk", operationPk);
//		}
//		return (ValuatorPriceResult) findObjectByQueryString(querySQL.toString(), parameters);
//	}
//	
//	
//	/**
//	 * Find valuat price.
//	 *
//	 * @param securityCode the security code
//	 * @param marketDate the market date
//	 * @param procDate the proc date
//	 * @param marketRate the market rate
//	 * @param operationPk the operation pk
//	 * @param prices the prices
//	 * @return the valuator price result
//	 */
//	public ValuatorPriceResult findValuatPrice(String securityCode, Date marketDate, BigDecimal marketRate, Long operationPk, List<ValuatorPriceResult> prices)
//																														throws ServiceException{
//		if(prices!=null){
//			for(ValuatorPriceResult price: prices){
//				if(operationPk!=null){
//					if(operationPk.equals(price.getMechanismOperationPk()))return price;
//				}else{
//					if(price.getMarketDate().equals(marketDate)){
//						if(price.getMarketRate().equals(ValuatorConstants.UNKNOW_RATE))return price;
//						else if(price.getMarketRate().equals(marketRate))return price;
//					}
//				}
//			}
//		}
//		throw new ServiceException();
//	}
//	
//	/**
//	 * 
//	 * @param securityCode
//	 * @param marketDate
//	 * @param marketRate
//	 * @param operationPk
//	 * @param prices
//	 * @return
//	 * @throws ServiceException
//	 */
//	public ValuatorPriceResult findSelidValuatPrice(String securityCode, Date marketDate, BigDecimal marketRate, Long operationPk, List<ValuatorPriceResult> prices)
//			throws ServiceException{
//		if(prices!=null){
//			for(ValuatorPriceResult price: prices){
//				if(operationPk!=null){
//					if(operationPk.equals(price.getMechanismOperationPk())){
//						return price;
//					}else{
//						return price;
//					}
//				}else{
//						if(price.getMarketDate().equals(marketDate)){
//							if(price.getMarketRate().equals(ValuatorConstants.UNKNOW_RATE))
//								return price;
//							else if(price.getMarketRate().equals(marketRate))
//								return price;
//						}
//					}	
//				}	
//			}
//		throw new ServiceException();
//	}
//	
//	/**
//	 * Find valuator price homologated.
//	 *
//	 * @param securityCode the security code
//	 * @param processPk the process pk
//	 * @return the valuator price result
//	 */
//	public ValuatorPriceResult findValuatorPriceHomologated(String securityCode, Long processPk) {
//		StringBuilder querySQL = new StringBuilder();
//		Map<String,Object> parameters = new HashMap<>();
//		querySQL.append("Select pricRes 																		");
//		querySQL.append("From ValuatorPriceResult pricRes														");
//		querySQL.append("Where pricRes.indHomologated = :oneParam				 	And							");
//		querySQL.append("	   pricRes.securityCode	  = :secCode			 		And							");
//		querySQL.append("	   pricRes.valuatorProcessExecution.idProcessExecutionPk = :processPk				");
//		parameters.put("processPk", processPk);
//		parameters.put("secCode", securityCode);
//		parameters.put("oneParam", BooleanType.YES.getCode());
//		return (ValuatorPriceResult) findObjectByQueryString(querySQL.toString(), parameters);
//	}
//	
//	/**
//	 * Update settlement account market fact.
//	 *
//	 * @param operations the operations
//	 * @param role the role
//	 * @param part the part
//	 * @throws ServiceException the service exception
//	 */
//	@SuppressWarnings("unchecked")
//	public void updateSettlementAccountMarketFact(List<Long> operations, Integer role, Integer part) throws ServiceException{
//		StringBuilder querySQL = new StringBuilder();
//		Map<String,Object> parameters = new HashMap<>();
//		querySQL.append("Select sec.idSecurityCodePk, part.idParticipantPk, ha.idHolderAccountPk, 				");
//		querySQL.append("		mark.marketDate, mark.marketRate, mark.idSettAccountMarketfactPk				");
//		querySQL.append("From SettlementAccountMarketfact mark													");
//		querySQL.append("Inner Join mark.settlementAccountOperation sett										");
//		querySQL.append("Inner Join sett.settlementOperation settOpe											");
//		querySQL.append("Inner Join sett.holderAccountOperation hao												");
//		querySQL.append("Inner Join hao.inchargeStockParticipant part											");
//		querySQL.append("Inner Join hao.holderAccount ha														");
//		querySQL.append("Inner Join hao.mechanismOperation mech													");
//		querySQL.append("Inner Join mech.securities sec															");
//		querySQL.append("Where hao.operationPart = :part And hao.role = :role 						And			");
//		querySQL.append("	   mark.indActive = :oneParam And mech.operationState = :casSettled 	And			");
//		querySQL.append("	   mech.indReportingBalance = :oneParam And mark.marketPrice Is Null	And			");
//		querySQL.append("	   mech.idMechanismOperationPk IN(:operations)							And 		");
//		querySQL.append("	   settOpe.indPartial = :zeroParam	And mark.indActive = :oneParam					");
//		
//		parameters.put("part", part);
//		parameters.put("operations", operations);
//		parameters.put("role", role);
//		parameters.put("oneParam", ComponentConstant.ONE);
//		parameters.put("zeroParam", ComponentConstant.ZERO);
//		parameters.put("casSettled", MechanismOperationStateType.CASH_SETTLED.getCode());
//		
//		List<Object[]> objectData = findListByQueryString(querySQL.toString(), parameters);
//		for(Object[] data: objectData){
//			/**Query to get marketPrice from holderMarketFactBalance*/
//			StringBuilder subQuerySQL = new StringBuilder();
//			subQuerySQL.append("Select hmba.marketPrice From HolderMarketFactBalance hmba							");
//			subQuerySQL.append("Inner Join hmba.security sec														");
//			subQuerySQL.append("Inner Join hmba.participant part													");
//			subQuerySQL.append("Inner Join hmba.holderAccount ha													");
//			subQuerySQL.append("Where sec.idSecurityCodePk||part.idParticipantPk||ha.idHolderAccountPk = :balKey 	");
//			subQuerySQL.append("  And trunc(hmba.marketDate) = trunc(:marketDate) And hmba.marketRate = :marketRate	");
//			subQuerySQL.append("  And hmba.marketPrice Is Not Null And hmba.indActive = 1							");
//			Map<String,Object> subParameters = new HashMap<>();
//			
//			String balanceKey 	= data[0].toString().concat(data[1].toString()).concat(data[2].toString());
//			Date markDate 		= (Date) data[3];
//			BigDecimal markRate = (BigDecimal) data[4];
//			subParameters.put("balKey", balanceKey);
//			subParameters.put("marketDate", markDate);
//			subParameters.put("marketRate", markRate);
//			/**Get marketPrice from subBalance*/
//			BigDecimal marketPrice = (BigDecimal) findObjectByQueryString(subQuerySQL.toString(), subParameters);
//			
//			/**Update marketPrice on settlemetAccountMarketFact*/
//			StringBuilder updateSQL = new StringBuilder();
//			updateSQL.append("Update SettlementAccountMarketfact Set marketPrice = :marketPrice Where idSettAccountMarketfactPk = :settMrkId");
//			Query query = em.createQuery(updateSQL.toString());
//			query.setParameter("marketPrice", marketPrice);
//			query.setParameter("settMrkId", data[5]);
//			query.executeUpdate();
//		}
//	}	
//	
//	/**
//	 * Update settlement account market fact. issue 1040
//	 *
//	 * @param operations the operations
//	 * @param role the role
//	 * @param part the part
//	 * @throws ServiceException the service exception
//	 */
//	@SuppressWarnings("unchecked")
//	public void updateSelidSettlementAccountMarketFact(List<Long> operations, Integer role, Integer part) throws ServiceException{
//
//		StringBuilder querySql = new StringBuilder();
//		
//		querySql.append("   SELECT                                                                                                                     ");
//		querySql.append("       SEC.ID_SECURITY_CODE_PK,                                                                                               ");
//		querySql.append("       PAR.ID_PARTICIPANT_PK,                                                                                                 ");
//		querySql.append("       HA.ID_HOLDER_ACCOUNT_PK,                                                                                               ");
//		querySql.append("       SAM.MARKET_DATE,                                                                                                       ");
//		querySql.append("       SAM.MARKET_RATE,                                                                                                       ");
//		querySql.append("       SAM.ID_ACCOUNT_MARKETFACT_PK                                                                                           ");
//		querySql.append("                                                                                                                              ");
//		querySql.append("   from SETTLEMENT_ACCOUNT_MARKETFACT SAM                                                                                     ");
//		querySql.append("       inner join SETTLEMENT_ACCOUNT_OPERATION SAO on SAM.ID_SETTLEMENT_ACCOUNT_FK=SAO.ID_SETTLEMENT_ACCOUNT_PK               ");
//		querySql.append("       inner join SETTLEMENT_OPERATION SO on SAO.ID_SETTLEMENT_OPERATION_FK=SO.ID_SETTLEMENT_OPERATION_PK                     ");
//		querySql.append("       inner join HOLDER_ACCOUNT_OPERATION HAO on SAO.ID_HOLDER_ACCOUNT_OPERATION_FK=HAO.ID_HOLDER_ACCOUNT_OPERATION_PK       ");
//		querySql.append("       inner join Participant PAR on HAO.ID_INCHARGE_STOCK_PARTICIPANT=PAR.ID_PARTICIPANT_PK                                  ");
//		querySql.append("       inner join HOLDER_ACCOUNT HA on HAO.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK                                       ");
//		querySql.append("       inner join MECHANISM_OPERATION MO on HAO.ID_MECHANISM_OPERATION_FK=MO.ID_MECHANISM_OPERATION_PK                        ");
//		querySql.append("       inner join SECURITY SEC on MO.ID_SECURITY_CODE_FK=SEC.ID_SECURITY_CODE_PK                                              ");
//		querySql.append("       inner join SETTLEMENT_DATE_OPERATION SDO ON SO.ID_SETTLEMENT_OPERATION_PK = SDO.ID_SETTLEMENT_OPERATION_FK             ");
//		querySql.append("       inner join SETTLEMENT_REQUEST SR ON SR.ID_SETTLEMENT_REQUEST_PK = SDO.ID_SETTLEMENT_REQUEST_FK                         ");
//		querySql.append("                                                                                                                              ");
//		querySql.append("   where 1 = 1                                                                                                                ");
//		querySql.append("       and HAO.OPERATION_PART=1                                                                                               ");
//		querySql.append("       and HAO.ROLE=1                                                                                                         ");
//		querySql.append("       and SAM.IND_ACTIVE=1                                                                                                   ");
//		querySql.append("       and MO.OPERATION_STATE=614                                                                                             ");
//		querySql.append("       and SAM.MARKET_PRICE is null                                                                                           ");
//		querySql.append("       and  MO.ID_MECHANISM_OPERATION_PK in ( :operations )                                                                   ");
//		querySql.append("       and SO.IND_PARTIAL=0                                                                                                   ");
//		querySql.append("       and SAM.IND_ACTIVE=1                                                                                                   ");
//		querySql.append("       AND SR.REQUEST_TYPE = 2018                                                                                             ");
//		querySql.append("       AND SR.REQUEST_STATE = 1543                                                                                            ");
//		querySql.append("       AND SO.IND_EXTENDED = 1                                                                                                ");
//		querySql.append("       AND SO.IND_UNFULFILLED <> 1                                                                                            ");
//				
//		Query querySec = em.createNativeQuery(querySql.toString());
//		querySec.setParameter("operations", operations);
//		
//		List<Object[]> objectData = (List<Object[]>)querySec.getResultList();
//		
//		for(Object[] data: objectData){
//			/**Query to get marketPrice from holderMarketFactBalance*/
//			StringBuilder subQuerySQL = new StringBuilder();
//			subQuerySQL.append("Select hmba.marketPrice From HolderMarketFactBalance hmba							");
//			subQuerySQL.append("Inner Join hmba.security sec														");
//			subQuerySQL.append("Inner Join hmba.participant part													");
//			subQuerySQL.append("Inner Join hmba.holderAccount ha													");
//			subQuerySQL.append("Where sec.idSecurityCodePk||part.idParticipantPk||ha.idHolderAccountPk = :balKey 	");
//			subQuerySQL.append("  And trunc(hmba.marketDate) = trunc(:marketDate) And hmba.marketRate = :marketRate	");
//			subQuerySQL.append("  And hmba.marketPrice Is Not Null And hmba.indActive = 1							");
//			Map<String,Object> subParameters = new HashMap<>();
//			
//			String balanceKey 	= data[0].toString().concat(data[1].toString()).concat(data[2].toString());
//			Date markDate 		= (Date) data[3];
//			BigDecimal markRate = (BigDecimal) data[4];
//			subParameters.put("balKey", balanceKey);
//			subParameters.put("marketDate", markDate);
//			subParameters.put("marketRate", markRate);
//			/**Get marketPrice from subBalance*/
//			BigDecimal marketPrice = (BigDecimal) findObjectByQueryString(subQuerySQL.toString(), subParameters);
//			
//			/**Update marketPrice on settlemetAccountMarketFact*/
//			StringBuilder updateSQL = new StringBuilder();
//			updateSQL.append("Update SettlementAccountMarketfact Set marketPrice = :marketPrice Where idSettAccountMarketfactPk = :settMrkId");
//			Query query = em.createQuery(updateSQL.toString());
//			query.setParameter("marketPrice", marketPrice);
//			query.setParameter("settMrkId", data[5]);
//			query.executeUpdate();
//		}
//	}	
//	
//	/**
//	 * Update physical certificate price.
//	 *
//	 * @param executionPk the execution pk
//	 * @param loggerUser the logger user
//	 * @throws ServiceException the service exception
//	 */
//	public void updatePhysicalCertificatePrice(Long executionPk, LoggerUser loggerUser) throws ServiceException{
//		
//		/**Variables to handle queries to execute*/
//		StringBuilder selectSQL = new StringBuilder();
//		StringBuilder updateSQL = new StringBuilder();
//		
//		/**Map for parameters in the execution*/
//		Map<String,Object> parameters = new HashMap<String,Object>();
//		
//		/**Main part of the query to do in the update*/
//		updateSQL.append("Update PhysicalCertMarketfact mrk_u Set 											");
//		updateSQL.append("	 mrk_u.indActive = :zeroParam,													");
//		updateSQL.append(" 	 mrk_u.lastModifyApp = :lastModifyApp, mrk_u.lastModifyDate = :lastModifyDate, 	");
//		updateSQL.append(" 	 mrk_u.lastModifyIp = :lastModifyIp, mrk_u.lastModifyUser = :lastModifyUser 	");
//		updateSQL.append("Where mrk_u.IdPhysicalCertMarketfactPk In(										");
//		
//		/**Second part of the query to do the select*/
//		selectSQL.append("Select Distinct mark.IdPhysicalCertMarketfactPk									");
//		selectSQL.append("From PhysicalCertMarketfact mark													");
//		selectSQL.append("Inner Join mark.physicalCertificate phys											");
//		selectSQL.append("Where phys.state IN (:stateList)	And phys.situation	In(:situationList)			");
//		selectSQL.append("And   mark.indActive = :oneParam													");
//		selectSQL.append("And   phys.securities.idSecurityCodePk In (										");
//		selectSQL.append("					SELECT Distinct pric.securityCode								");
//		selectSQL.append("					FROM ValuatorPriceResult pric									");
//		selectSQL.append("					WHERE pric.valuatorProcessExecution.idProcessExecutionPk = :pk))");
//		
//		/**Setting all parameters*/
//		parameters.put("pk", executionPk);
//		parameters.put("oneParam", BooleanType.YES.getCode());
//		parameters.put("zeroParam", BooleanType.NO.getCode());
//		parameters.put("lastModifyIp", loggerUser.getIpAddress());
//		parameters.put("lastModifyUser", loggerUser.getUserName());
//		parameters.put("lastModifyDate", loggerUser.getAuditTime());
//		parameters.put("lastModifyApp", loggerUser.getIdPrivilegeOfSystem()*-1);
//
//		/**Getting data of state and situation*/
//		Integer[] stateData = {PhysicalCertificateStateType.CONFIRMED.getCode(),PhysicalCertificateStateType.AUTHORIZED.getCode(),StateType.DEPOSITED.getCode()};
//		Integer[] situtationData = {SituationType.CUSTODY_VAULT.getCode(),SituationType.DEPOSITARY_OPERATIONS.getCode(),SituationType.DEPOSITARY_VAULT.getCode()};
//		
//		/**Setting in Map*/
//		parameters.put("stateList", Arrays.asList(stateData));
//		parameters.put("situationList", Arrays.asList(situtationData));
//		
//		/**Execute query in BD*/
//		updateByQuery(updateSQL.toString().concat(selectSQL.toString()), parameters);
//	}
//	
//	/**
//	 * Update custody operation price.
//	 *
//	 * @param executionPk the execution pk
//	 * @param loggerUser the logger user
//	 * @throws ServiceException the service exception
//	 */
//	public void updateCustodyOperationPrice(Long executionPk, LoggerUser loggerUser) throws ServiceException{
//		/**Variables to handle queries to execute*/
//		StringBuilder queryAffecSQL = new StringBuilder();
//		StringBuilder updateAffecSQL = new StringBuilder();
//		
//		/**Map for parameters in the execution*/
//		Map<String,Object> parameters = new HashMap<String,Object>();
//		
//		/**Begin Query for affectation*/
//		updateAffecSQL.append("Update BlockMarketFactOperation mrk_u Set 											");
//		updateAffecSQL.append("	 mrk_u.indActive = :zeroParam,														");
//		updateAffecSQL.append(" 	 mrk_u.lastModifyApp = :lastModifyApp, mrk_u.lastModifyDate = :lastModifyDate, 	");
//		updateAffecSQL.append(" 	 mrk_u.lastModifyIp = :lastModifyIp, mrk_u.lastModifyUser = :lastModifyUser 	");
//		updateAffecSQL.append("Where mrk_u.idBlockMarketFactOperatPk In(											");
//		
//		queryAffecSQL.append("Select Distinct mark.idBlockMarketFactOperatPk										");
//		queryAffecSQL.append("From BlockMarketFactOperation mark													");
//		queryAffecSQL.append("Inner Join mark.blockOperation bop													");
//		queryAffecSQL.append("Where bop.securities.idSecurityCodePk IN(												");
//		queryAffecSQL.append("					SELECT Distinct pric.securityCode									");
//		queryAffecSQL.append("					FROM ValuatorPriceResult pric										");
//		queryAffecSQL.append("					WHERE pric.valuatorProcessExecution.idProcessExecutionPk = :pk	)	");
//		queryAffecSQL.append("And   mark.actualBlockBalance > 0	And bop.blockState = :blockState					");
//		queryAffecSQL.append("And   mark.indActive = :oneParam													   )");
//
//		/**Setting parameters for affectation*/
//		parameters.put("pk", executionPk);
//		parameters.put("oneParam", BooleanType.YES.getCode());
//		parameters.put("zeroParam", BooleanType.NO.getCode());
//		parameters.put("lastModifyIp", loggerUser.getIpAddress());
//		parameters.put("lastModifyUser", loggerUser.getUserName());
//		parameters.put("lastModifyDate", loggerUser.getAuditTime());
//		parameters.put("lastModifyApp", loggerUser.getIdPrivilegeOfSystem()*-1);
//		parameters.put("blockState", AffectationStateType.BLOCKED.getCode());
//		
//		/**Execute query in BD*/
//		updateByQuery(updateAffecSQL.toString().concat(queryAffecSQL.toString()), parameters);
//		
//		
//		/************************Begin of update For Accreditation****************************/
//		StringBuilder queryAccredSQL = new StringBuilder();
//		StringBuilder updateAccredSQL = new StringBuilder();
//		
//		/**Map for parameters in the execution*/
//		Map<String,Object> parametersAccre = new HashMap<String,Object>();
//		
//		/**Begin Query for accreditation*/
//		updateAccredSQL.append("Update AccreditationMarketFact mrk_u Set											");
//		updateAccredSQL.append("	 mrk_u.indActive = :zeroParam,													");
//		updateAccredSQL.append(" 	 mrk_u.lastModifyApp = :lastModifyApp, mrk_u.lastModifyDate = :lastModifyDate, 	");
//		updateAccredSQL.append(" 	 mrk_u.lastModifyIp = :lastModifyIp, mrk_u.lastModifyUser = :lastModifyUser 	");
//		updateAccredSQL.append("Where mrk_u.idAccreditationMarketFactPk In(											");
//		
//		queryAccredSQL.append("Select Distinct mark.idAccreditationMarketFactPk										");
//		queryAccredSQL.append("From AccreditationMarketFact mark													");
//		queryAccredSQL.append("Inner Join mark.accreditationDetailFk bop											");
//		queryAccredSQL.append("Inner Join bop.accreditationOperation acOper											");
//		queryAccredSQL.append("Inner Join bop.idParticipantFk part													");
//		queryAccredSQL.append("Inner Join bop.holderAccount ha														");
//		queryAccredSQL.append("Inner Join bop.security sec															");
//		queryAccredSQL.append("Where sec.idSecurityCodePk IN(														");
//		queryAccredSQL.append("					SELECT Distinct pric.securityCode									");
//		queryAccredSQL.append("					FROM ValuatorPriceResult pric										");
//		queryAccredSQL.append("					WHERE pric.valuatorProcessExecution.idProcessExecutionPk = :pk	)	");		
//		queryAccredSQL.append("And acOper.accreditationState IN (:confirmedState)	And mark.indActive = :oneParam)");
//		
//		/**Setting parameters for affectation*/
//		parametersAccre.put("pk", executionPk);
//		parametersAccre.put("oneParam", BooleanType.YES.getCode());
//		parametersAccre.put("zeroParam", BooleanType.NO.getCode());
//		parametersAccre.put("lastModifyIp", loggerUser.getIpAddress());
//		parametersAccre.put("lastModifyUser", loggerUser.getUserName());
//		parametersAccre.put("lastModifyDate", loggerUser.getAuditTime());
//		parametersAccre.put("lastModifyApp", loggerUser.getIdPrivilegeOfSystem()*-1);
//		parametersAccre.put("confirmedState",Arrays.asList(AccreditationOperationStateType.CONFIRMED.getCode()));
//		
//		/**Execute query in BD*/
//		updateByQuery(updateAccredSQL.toString().concat(queryAccredSQL.toString()), parametersAccre);
//	}
//	
//	/**
//	 * Update reporting operation price.
//	 *
//	 * @param executionPk the execution pk
//	 * @param loggerUser the logger user
//	 * @throws ServiceException the service exception
//	 */
//	public void updateReportingOperationPrice(Long executionPk, LoggerUser loggerUser) throws ServiceException{
//		
//		/**Variables to handle queries to execute*/
//		StringBuilder selectSQL = new StringBuilder();
//		StringBuilder updateSQL = new StringBuilder();
//		
//		/**Map for parameters in the execution*/
//		Map<String,Object> parameters = new HashMap<String,Object>();
//		
//		updateSQL.append("Update SettlementAccountMarketfact mrk_u Set											");
//		updateSQL.append("	 mrk_u.indActive = :zeroParam,														");
//		updateSQL.append(" 	 mrk_u.lastModifyApp = :lastModifyApp, mrk_u.lastModifyDate = :lastModifyDate, 		");
//		updateSQL.append(" 	 mrk_u.lastModifyIp = :lastModifyIp, mrk_u.lastModifyUser = :lastModifyUser 		");
//		updateSQL.append("Where mrk_u.idSettAccountMarketfactPk In(												");
//		
//		selectSQL.append("Select Distinct mark.idSettAccountMarketfactPk								 		");
//		selectSQL.append("From SettlementAccountMarketfact mark													");
//		selectSQL.append("Inner Join mark.settlementAccountOperation sett										");
//		selectSQL.append("Inner Join sett.settlementOperation settOp											");
//		selectSQL.append("Inner Join sett.holderAccountOperation hao											");
//		selectSQL.append("Inner Join hao.inchargeStockParticipant part											");
//		selectSQL.append("Inner Join hao.holderAccount ha														");
//		selectSQL.append("Inner Join hao.mechanismOperation mech												");
//		selectSQL.append("Inner Join mech.securities sec														");
//		selectSQL.append("Where hao.operationPart = :part And													");
//		selectSQL.append("	   mech.operationState  in ( :casSettled )	And											");
//		
//		// Traemos todos los que estan autorizados
//		StringBuilder sbQuery = new StringBuilder();
//		sbQuery.append("	SELECT SU FROM SettlementUnfulfillment SU");
//		sbQuery.append("	inner join fetch SU.operationUnfulfillment OU ");
//		sbQuery.append("	inner join fetch OU.settlementOperation SO ");
//		sbQuery.append("	WHERE SU.requestState = 1543 ");
//
//		Query query = em.createQuery(sbQuery.toString());
//		
//		@SuppressWarnings("unchecked")
//		List <SettlementUnfulfillment> tets = (List <SettlementUnfulfillment>)query.getResultList();
//		List<Long> lstSettperation = new ArrayList<Long>();
//		for (SettlementUnfulfillment oepartion: tets) {
//			lstSettperation.add(oepartion.getOperationUnfulfillment().getSettlementOperation().getIdSettlementOperationPk());
//		}
//		
//		if(tets!=null&&tets.size()>0)
//			selectSQL.append("	   sett.settlementOperation.idSettlementOperationPk NOT IN(	:lstSettlementOperation) 	and					    ");
//		selectSQL.append("	   mech.indReportingBalance = :oneParam And											");
//		selectSQL.append("	   sett.operationState = :regState		And											");
//		selectSQL.append("	   mark.indActive = :oneParam			And											");
//		selectSQL.append("	   settOp.indPartial = :zeroParam		And											");
//		selectSQL.append("	   mark.indActive = :oneParam			And											");
//		selectSQL.append("	   sec.idSecurityCodePk IN(															");
//		selectSQL.append("					SELECT Distinct pric.securityCode									");
//		selectSQL.append("					FROM ValuatorPriceResult pric										");
//		selectSQL.append("					WHERE pric.valuatorProcessExecution.idProcessExecutionPk = :pk	))	");
//		
//
//		/**Setting parameters*/
//		parameters.put("pk", executionPk);
//		parameters.put("oneParam", ComponentConstant.ONE);
//		parameters.put("part", ComponentConstant.TERM_PART);
//		parameters.put("zeroParam", ComponentConstant.ZERO);
//		parameters.put("lastModifyIp", loggerUser.getIpAddress());
//		parameters.put("lastModifyUser", loggerUser.getUserName());
//		parameters.put("lastModifyDate", loggerUser.getAuditTime());
//		parameters.put("lastModifyApp", loggerUser.getIdPrivilegeOfSystem()*-1);
//		parameters.put("regState", HolderAccountOperationStateType.CONFIRMED.getCode());
//		//parameters.put("casSettled", MechanismOperationStateType.CASH_SETTLED.getCode());
//		
//		List<Integer> states = new ArrayList<Integer>();
//		states.add(MechanismOperationStateType.CASH_SETTLED.getCode());
//		states.add(MechanismOperationStateType.CANCELED_TERM_STATE.getCode());
//		parameters.put("casSettled", states);
//		
//		if(tets!=null&&tets.size()>0)
//			parameters.put("lstSettlementOperation", lstSettperation);
//		
//		/**Execute query in BD*/
//		updateByQuery(updateSQL.toString().concat(selectSQL.toString()), parameters);
//	}
//	
//	/**
//	 * Update reporting operation price. issue 1040
//	 *
//	 * @param executionPk the execution pk
//	 * @param loggerUser the logger user
//	 * @throws ServiceException the service exception
//	 */
//	public void updateSelidOperationPrice(Long executionPk, LoggerUser loggerUser) throws ServiceException{
//		
//		StringBuilder querySql = new StringBuilder();
//
//		querySql.append("   UPDATE SETTLEMENT_ACCOUNT_MARKETFACT    			   ");
//		querySql.append("   SET IND_ACTIVE = 0,                    				   ");
//		querySql.append("       LAST_MODIFY_APP = :lastModifyApp,                  ");
//		querySql.append("       LAST_MODIFY_DATE = :lastModifyDate,                ");
//		querySql.append("       LAST_MODIFY_IP = :lastModifyIp,                    ");
//		querySql.append("       LAST_MODIFY_USEr = :lastModifyUser                 ");
//		querySql.append("   WHERE ID_ACCOUNT_MARKETFACT_PK IN (      			   ");
//		querySql.append("                                                          ");
//		querySql.append("   SELECT  DISTINCT                                                                                                       ");
//		querySql.append("      SAM.ID_ACCOUNT_MARKETFACT_PK                                                                                        ");
//		querySql.append("   from SETTLEMENT_ACCOUNT_MARKETFACT SAM                                                                                 ");
//		querySql.append("   inner join SETTLEMENT_ACCOUNT_OPERATION SAO on SAM.ID_SETTLEMENT_ACCOUNT_FK = SAO.ID_SETTLEMENT_ACCOUNT_PK             ");
//		querySql.append("   inner join SETTLEMENT_OPERATION SO on SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK                   ");
//		querySql.append("   inner join HOLDER_ACCOUNT_OPERATION HAO on SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = HAO.ID_HOLDER_ACCOUNT_OPERATION_PK     ");
//		querySql.append("   inner join Participant PAR on HAO.ID_INCHARGE_STOCK_PARTICIPANT = PAR.ID_PARTICIPANT_PK                                ");
//		querySql.append("   inner join HOLDER_ACCOUNT HA on HAO.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK                                     ");
//		querySql.append("   inner join MECHANISM_OPERATION MO on HAO.ID_MECHANISM_OPERATION_FK=MO.ID_MECHANISM_OPERATION_PK                        ");
//		querySql.append("   inner join SECURITY SEC on MO.ID_SECURITY_CODE_FK=SEC.ID_SECURITY_CODE_PK                                              ");
//		querySql.append("   inner join SETTLEMENT_DATE_OPERATION SDO ON SO.ID_SETTLEMENT_OPERATION_PK = SDO.ID_SETTLEMENT_OPERATION_FK             ");
//		querySql.append("   INNER JOIN SETTLEMENT_REQUEST SR ON SR.ID_SETTLEMENT_REQUEST_PK = SDO.ID_SETTLEMENT_REQUEST_FK                         ");
//		querySql.append("   where                                                                                                                  ");
//		querySql.append("       HAO.OPERATION_PART = 1                                                                                             ");
//		querySql.append("       and MO.OPERATION_STATE = 614                                                                                       ");
//		//querySql.append("       --and MO.IND_REPORTING_BALANCE=1                                                                                   ");
//		querySql.append("       and SAO.OPERATION_STATE = 625                                                                                      ");
//		querySql.append("       and SAM.IND_ACTIVE = 1                                                                                             ");
//		querySql.append("       and SO.IND_PARTIAL = 0                                                                                             ");
//		querySql.append("       and SAM.IND_ACTIVE = 1                                                                                             ");
//		querySql.append("       AND SR.REQUEST_TYPE = 2018                                                                                         ");
//		querySql.append("       AND SR.REQUEST_STATE = 1543                                                                                        ");
//		querySql.append("       AND SO.IND_EXTENDED = 1                                                                                            ");
//		querySql.append("       AND HAO.ROLE = 2                                                                                          ");
//		querySql.append("       AND SO.IND_UNFULFILLED <> 1                                                                                        ");
//		querySql.append("       and  SEC.ID_SECURITY_CODE_PK in ( select distinct VPR.ID_SECURITY_CODE_FK                                          ");
//		querySql.append("   									 from VALUATOR_PRICE_RESULT VPR                                                    ");
//		querySql.append("   									 where VPR.ID_PROCESS_EXECUTION_FK = :pk )                                        ");
//		querySql.append("   )                                        ");
//		
//		Query query = em.createNativeQuery(querySql.toString());
//		
//		query.setParameter("pk", executionPk);
//		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
//		query.setParameter("lastModifyUser", loggerUser.getUserName());
//		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
//		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem()*-1);
//		
//		query.executeUpdate();
//	}
}