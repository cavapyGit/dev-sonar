package com.pradera.securities.valuator.simulator.view;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.valuatorprocess.ValuatorProcessOperation;
import com.pradera.model.valuatorprocess.ValuatorSimulationBalance;
import com.pradera.model.valuatorprocess.ValuatorSimulatorCode;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorProcessSimulatorTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class ValuatorProcessSimulatorTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id valuator simulator pk. */
	private Long idValuatorSimulatorPk;

	/** The begin time. */
	private Date beginTime;

	/** The finish time. */
	private Date finishTime;

	/** The holder account. */
	private HolderAccount holderAccount;

	/** The holder. */
	private Holder holder;

	/** The participant. */
	private Participant participant;

	/** The security. */
	private Security security;

	/** The process state. */
	private Integer processState;

	/** The registry date. */
	private Date registryDate;

	/** The registry user. */
	private String registryUser;

	/** The requester type. */
	private Integer requesterType;

	/** The simulation date. */
	private Date simulationDate;
	
	/** The simulation initial date. */
	private Date simulationInitialDate;
	
	/** The simulation final date. */
	private Date simulationFinalDate;

	/** The simulation name. */
	private String simulationName;

	/** The simulation type. */
	private Integer simulationType;
	
	/** The simulation form. */
	private Integer simulationForm;

	/** The valuator process operation. */
	private ValuatorProcessOperation valuatorProcessOperation;

	/** The valuator simulation balances. */
	private List<ValuatorSimulationBalance> valuatorSimulationBalances;

	/** The valuator simulator codes. */
	private List<ValuatorSimulatorCode> valuatorSimulatorCodes;
	
	/**
	 * Instantiates a new valuator process simulator to.
	 */
	public ValuatorProcessSimulatorTO() { }

	/**
	 * Instantiates a new valuator process simulator to.
	 *
	 * @param idValuatorSimulatorPk the id valuator simulator pk
	 * @param registryDate the registry date
	 * @param simulationForm the simulation form
	 * @param mnemonic the mnemonic
	 * @param idSecurityCodePk the id security code pk
	 * @param processState the process state
	 */
	public ValuatorProcessSimulatorTO(Long idValuatorSimulatorPk, Date registryDate, Integer simulationForm,
			String mnemonic, String idSecurityCodePk, Integer processState
			) {
		Participant participant = new Participant();
		Security security = new Security();
		this.idValuatorSimulatorPk = idValuatorSimulatorPk;
		participant.setMnemonic(mnemonic);
		this.participant = participant;
		security.setIdSecurityCodePk(idSecurityCodePk);
		this.security = security;
		this.processState = processState;
		this.registryDate = registryDate;
		this.simulationForm = simulationForm;
	}

	/**
	 * Gets the id valuator simulator pk.
	 *
	 * @return the id valuator simulator pk
	 */
	public Long getIdValuatorSimulatorPk() {
		return idValuatorSimulatorPk;
	}

	/**
	 * Sets the id valuator simulator pk.
	 *
	 * @param idValuatorSimulatorPk the new id valuator simulator pk
	 */
	public void setIdValuatorSimulatorPk(Long idValuatorSimulatorPk) {
		this.idValuatorSimulatorPk = idValuatorSimulatorPk;
	}

	/**
	 * Gets the begin time.
	 *
	 * @return the begin time
	 */
	public Date getBeginTime() {
		return beginTime;
	}

	/**
	 * Sets the begin time.
	 *
	 * @param beginTime the new begin time
	 */
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	/**
	 * Gets the finish time.
	 *
	 * @return the finish time
	 */
	public Date getFinishTime() {
		return finishTime;
	}

	/**
	 * Sets the finish time.
	 *
	 * @param finishTime the new finish time
	 */
	public void setFinishTime(Date finishTime) {
		this.finishTime = finishTime;
	}

	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets the process state.
	 *
	 * @return the process state
	 */
	public Integer getProcessState() {
		return processState;
	}

	/**
	 * Sets the process state.
	 *
	 * @param processState the new process state
	 */
	public void setProcessState(Integer processState) {
		this.processState = processState;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the requester type.
	 *
	 * @return the requester type
	 */
	public Integer getRequesterType() {
		return requesterType;
	}

	/**
	 * Sets the requester type.
	 *
	 * @param requesterType the new requester type
	 */
	public void setRequesterType(Integer requesterType) {
		this.requesterType = requesterType;
	}

	/**
	 * Gets the simulation date.
	 *
	 * @return the simulation date
	 */
	public Date getSimulationDate() {
		return simulationDate;
	}

	/**
	 * Sets the simulation date.
	 *
	 * @param simulationDate the new simulation date
	 */
	public void setSimulationDate(Date simulationDate) {
		this.simulationDate = simulationDate;
	}

	/**
	 * Gets the simulation name.
	 *
	 * @return the simulation name
	 */
	public String getSimulationName() {
		return simulationName;
	}

	/**
	 * Sets the simulation name.
	 *
	 * @param simulationName the new simulation name
	 */
	public void setSimulationName(String simulationName) {
		this.simulationName = simulationName;
	}

	/**
	 * Gets the simulation type.
	 *
	 * @return the simulation type
	 */
	public Integer getSimulationType() {
		return simulationType;
	}

	/**
	 * Sets the simulation type.
	 *
	 * @param simulationType the new simulation type
	 */
	public void setSimulationType(Integer simulationType) {
		this.simulationType = simulationType;
	}

	/**
	 * Gets the valuator process operation.
	 *
	 * @return the valuator process operation
	 */
	public ValuatorProcessOperation getValuatorProcessOperation() {
		return valuatorProcessOperation;
	}

	/**
	 * Sets the valuator process operation.
	 *
	 * @param valuatorProcessOperation the new valuator process operation
	 */
	public void setValuatorProcessOperation(
			ValuatorProcessOperation valuatorProcessOperation) {
		this.valuatorProcessOperation = valuatorProcessOperation;
	}

	/**
	 * Gets the valuator simulation balances.
	 *
	 * @return the valuator simulation balances
	 */
	public List<ValuatorSimulationBalance> getValuatorSimulationBalances() {
		return valuatorSimulationBalances;
	}

	/**
	 * Sets the valuator simulation balances.
	 *
	 * @param valuatorSimulationBalances the new valuator simulation balances
	 */
	public void setValuatorSimulationBalances(
			List<ValuatorSimulationBalance> valuatorSimulationBalances) {
		this.valuatorSimulationBalances = valuatorSimulationBalances;
	}

	/**
	 * Gets the valuator simulator codes.
	 *
	 * @return the valuator simulator codes
	 */
	public List<ValuatorSimulatorCode> getValuatorSimulatorCodes() {
		return valuatorSimulatorCodes;
	}

	/**
	 * Sets the valuator simulator codes.
	 *
	 * @param valuatorSimulatorCodes the new valuator simulator codes
	 */
	public void setValuatorSimulatorCodes(
			List<ValuatorSimulatorCode> valuatorSimulatorCodes) {
		this.valuatorSimulatorCodes = valuatorSimulatorCodes;
	}

	/**
	 * Gets the simulation initial date.
	 *
	 * @return the simulation initial date
	 */
	public Date getSimulationInitialDate() {
		return simulationInitialDate;
	}

	/**
	 * Sets the simulation initial date.
	 *
	 * @param simulationInitialDate the new simulation initial date
	 */
	public void setSimulationInitialDate(Date simulationInitialDate) {
		this.simulationInitialDate = simulationInitialDate;
	}

	/**
	 * Gets the simulation final date.
	 *
	 * @return the simulation final date
	 */
	public Date getSimulationFinalDate() {
		return simulationFinalDate;
	}

	/**
	 * Sets the simulation final date.
	 *
	 * @param simulationFinalDate the new simulation final date
	 */
	public void setSimulationFinalDate(Date simulationFinalDate) {
		this.simulationFinalDate = simulationFinalDate;
	}

	/**
	 * Gets the simulation form.
	 *
	 * @return the simulation form
	 */
	public Integer getSimulationForm() {
		return simulationForm;
	}

	/**
	 * Sets the simulation form.
	 *
	 * @param simulationForm the new simulation form
	 */
	public void setSimulationForm(Integer simulationForm) {
		this.simulationForm = simulationForm;
	}
}