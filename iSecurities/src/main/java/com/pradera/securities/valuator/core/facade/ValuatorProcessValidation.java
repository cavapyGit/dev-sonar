package com.pradera.securities.valuator.core.facade;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.valuatorprocess.ValuatorProcessExecution;
import com.pradera.model.valuatorprocess.type.ValuatorDetailType;
import com.pradera.model.valuatorprocess.type.ValuatorEventType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class ValuatorSecuritiesFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/12/2014
 */
@Stateless
@Performance
//@Interceptors({ContextHolderInterceptor.class,MonitoringEventInterceptor.class})
public class ValuatorProcessValidation extends CrudDaoServiceBean{

	/** The transaction registry. */
	@Resource TransactionSynchronizationRegistry transactionRegistry;
	
	/** The max rows. */
	public static Integer MAX_ROWS = 1000;

	/** The valuator securities facade. */
	@EJB private ValuatorSecuritiesFacade valuatorSecuritiesFacade;
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){

	}
	
	/**
	 * *
	 * Method to validate securities in balances.
	 *
	 * @param securities the securities
	 * @param instrument the instrument
	 * @param accounts the accounts
	 * @param holders the holders
	 * @param participants the participants
	 * @param processPk the process pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
//	@ValuatorDispatcherEvent(eventType=ValuatorEventType.EVENT_1_1_TRANSIT_BALANCES)
	public List<String> validateSecurityInBalances(List<String> securities, Integer instrument, List<Long> accounts, List<Long> holders, 
												   List<Long> participants, Long processPk)throws ServiceException{
		/**FIST, VALIDATE SECURITIES*/
		List<String> lstSecurity = null;
		if(securities!=null && !securities.isEmpty()){
			lstSecurity = getSecurities(instrument,securities,accounts,holders,participants);
			if(Validations.validateListIsNullOrEmpty(lstSecurity)){
				throw new ServiceException(ErrorServiceType.VALUATOR_SECURITY_BALANCE_EMPTY,new String[]{securities.toString()});
			}
		}
		
		/**SECOND, VALIDATE SECURITIES IN TRANSIT*/
		List<String> lstTransBal = getSecuritiesTransit(instrument, ComponentConstant.TRANSIT_BALANCE, securities,accounts,holders,participants);
		if(Validations.validateListIsNotNullAndNotEmpty(lstTransBal)){
			throw new ServiceException(ErrorServiceType.VALUATOR_SECURITY_BALANCE_TRANSIT,new String[]{lstTransBal.toString()});
		}
		
		/**THIRD, VALIDATE SECURITIES IN SALE*/
		List<String> lstSaleSecurity = getSecuritiesTransit(instrument, ComponentConstant.SALE_BALANCE, securities,accounts,holders,participants);
		if(Validations.validateListIsNotNullAndNotEmpty(lstSaleSecurity)){
			throw new ServiceException(ErrorServiceType.VALUATOR_SECURITY_BALANCE_SALE,new String[]{lstSaleSecurity.toString()});
		}
		
		/**FOUR, VALIDATE SECURITIES IN BUY*/
		List<String> lstPurchaseSecurity = getSecuritiesTransit(instrument, ComponentConstant.PURCHASE_BALANCE, securities,accounts,holders,participants);
		if(Validations.validateListIsNotNullAndNotEmpty(lstPurchaseSecurity)){
			throw new ServiceException(ErrorServiceType.VALUATOR_SECURITY_BALANCE_PURCHASE,new String[]{lstPurchaseSecurity.toString()});
		}
		
		Integer detailPk = ValuatorDetailType.VALIDATIONS.getCode();
		Long eventPk 	 = ValuatorEventType.EVENT_1_1_TRANSIT_BALANCES.getCode(); 
		valuatorSecuritiesFacade.finishValuatorExecutionDetail(processPk, detailPk, eventPk,false);
		
		return lstSecurity;
	}
	
	/**
	 * *
	 * Method to verify if marketFact files has been loaded .
	 *
	 * @param processExecution the process execution
	 * @throws ServiceException the service exception
	 */
//	@ValuatorDispatcherEvent(eventType=ValuatorEventType.EVENT_1_2_MARKETFACT_FILE)
	public void validateMarketFactFile(ValuatorProcessExecution processExecution) throws ServiceException{
		/**Variables to handle when process is finish*/
		Long processPk = processExecution.getIdProcessExecutionPk();
		Long eventCode = ValuatorEventType.EVENT_1_2_MARKETFACT_FILE.getCode();
		Integer detailType = ValuatorDetailType.VALIDATIONS.getCode();
		try{
			valuatorSecuritiesFacade.verifyMarketFile(processExecution);
		}catch(Exception e){
			valuatorSecuritiesFacade.finishValuatorExecutionDetail(processPk, detailType, eventCode, true);
			throw e;
		}
		valuatorSecuritiesFacade.finishValuatorExecutionDetail(processPk, detailType, eventCode, false);
	}
	
	/**
	 * *
	 * Method to verify if marketFact has inconsistencies.
	 *
	 * @param processExecution the process execution
	 * @throws ServiceException the service exception
	 */
//	@ValuatorDispatcherEvent(eventType=ValuatorEventType.EVENT_1_3_INCONSISTENTS)
	public void validateMarketFactInconsistencies(ValuatorProcessExecution processExecution) throws ServiceException{
		/**Variables to handle when process is finish*/
		Long processPk = processExecution.getIdProcessExecutionPk();
		Long eventCode = ValuatorEventType.EVENT_1_3_INCONSISTENTS.getCode();
		Integer detailType = ValuatorDetailType.VALIDATIONS.getCode();
		try{
			valuatorSecuritiesFacade.verifyInconsistencesOperations(processExecution);
		}catch(Exception e){
			valuatorSecuritiesFacade.finishValuatorExecutionDetail(processPk, detailType, eventCode, true);
			throw e;
		}
		valuatorSecuritiesFacade.finishValuatorExecutionDetail(processPk, detailType, eventCode, false);
	}
	
	/**
	 * *
	 * Method to verified if begin end day is closed.
	 *
	 * @param processExecution the process execution
	 * @throws ServiceException the service exception
	 */
//	@ValuatorDispatcherEvent(eventType=ValuatorEventType.EVENT_1_5_DAY_END)
	public void validateBegindEndDay(ValuatorProcessExecution processExecution) throws ServiceException{
		/**Variables to handle when process is finish*/
		Long processPk = processExecution.getIdProcessExecutionPk();
		Long eventCode = ValuatorEventType.EVENT_1_5_DAY_END.getCode();
		Integer detailType = ValuatorDetailType.VALIDATIONS.getCode();
		try{
			valuatorSecuritiesFacade.verifyBeginEndDay(processExecution);
		}catch(Exception e){
			valuatorSecuritiesFacade.finishValuatorExecutionDetail(processPk, detailType, eventCode, true);
			throw e;
		}
		valuatorSecuritiesFacade.finishValuatorExecutionDetail(processPk, detailType, eventCode, false);
	}
	
	/**
	 * **.
	 *
	 * @param index the index
	 * @param lstSecurities the lst securities
	 * @return the securities by index
	 * @throws ServiceException the service exception
	 */
	public static List<String> getSecuritiesByIndex(Integer index, List<String> lstSecurities) throws ServiceException{
		Integer iterationNumber = lstSecurities.size()/MAX_ROWS;
		Integer maxNumber = null;		
		if(!iterationNumber.equals(index)){
			maxNumber = MAX_ROWS*(index+1);
		}else{
			maxNumber = lstSecurities.size();
		}
		List<String> lstFinalSecurities = new ArrayList<String>();
		for(int i=index*MAX_ROWS; i<maxNumber; i++){
			lstFinalSecurities.add(lstSecurities.get(i).toString());
		}
		return lstFinalSecurities;
	}
	

	/**
	 * Gets the securities by security index.
	 *
	 * @param index the index
	 * @param lstSecurities the lst securities
	 * @return the securities by security index
	 * @throws ServiceException the service exception
	 */
	public static List<Security> getSecuritiesBySecurityIndex(Integer index, List<Security> lstSecurities) throws ServiceException{
		Integer iterationNumber = lstSecurities.size()/MAX_ROWS;
		Integer maxNumber = null;		
		if(!iterationNumber.equals(index)){
			maxNumber = MAX_ROWS*(index+1);
		}else{
			maxNumber = lstSecurities.size();
		}
		List<Security> lstFinalSecurities = new ArrayList<Security>();
		for(int i=index*MAX_ROWS; i<maxNumber; i++){
			lstFinalSecurities.add(lstSecurities.get(i));
		}
		return lstFinalSecurities;
	}
	
	/**
	 * Gets the operation by index.
	 *
	 * @param index the index
	 * @param lstOperations the lst operations
	 * @return the operation by index
	 * @throws ServiceException the service exception
	 */
	public static List<MechanismOperation> getOperationByIndex(Integer index, List<MechanismOperation> lstOperations) throws ServiceException{
		Integer iterationNumber = lstOperations.size()/MAX_ROWS;
		Integer maxNumber = null;		
		if(!iterationNumber.equals(index)){
			maxNumber = MAX_ROWS*(index+1);
		}else{
			maxNumber = lstOperations.size();
		}
		List<MechanismOperation> lstFinalOperation = new ArrayList<MechanismOperation>();
		for(int i=index*MAX_ROWS; i<maxNumber; i++){
			lstFinalOperation.add(lstOperations.get(i));
		}
		return lstFinalOperation;
	}
	
	/**
	 * Gets the securities transit.
	 *
	 * @param instrumentType the instrument type
	 * @param balanceType the balance type
	 * @param securities the securities
	 * @param accounts the accounts
	 * @param holders the holders
	 * @param participants the participants
	 * @return the securities transit
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<String> getSecuritiesTransit(Integer instrumentType, Integer balanceType,List<String> securities, List<Long> accounts, 
											 										List<Long> holders, List<Long> participants) throws ServiceException{
		StringBuilder querySQL = new StringBuilder();
		StringBuilder whereSQL = new StringBuilder();
		String querySql = (String) getSecurityQuery(instrumentType, securities, accounts, holders, participants).get(0);
		Map<String,Object> parameters = (Map<String, Object>) getSecurityQuery(instrumentType, securities, accounts, holders, participants).get(1);
		
		querySQL.append("SELECT DISTINCT hab.security.idSecurityCodePk								");
		querySQL.append("FROM HolderAccountBalance				hab									");
//		querySQL.append("INNER JOIN hab.security 				sec									");
//		whereSQL.append("WHERE sec.idSecurityCodePk IN 												");
		whereSQL.append("WHERE hab.security.idSecurityCodePk IN 									");
		whereSQL.append("(").append(querySql).append(")");
		
		if(balanceType.equals(ComponentConstant.TRANSIT_BALANCE)){
			whereSQL.append("	AND hab.transitBalance > 0											");
		}
		
		if(balanceType.equals(ComponentConstant.SALE_BALANCE)){
			whereSQL.append("	AND hab.saleBalance > 0												");
		}
		
		if(balanceType.equals(ComponentConstant.PURCHASE_BALANCE)){
			whereSQL.append("	AND hab.purchaseBalance > 0											");
		}
		
		if(holders!=null && !holders.isEmpty()){
			querySQL.append("INNER JOIN hab.holderAccount			ha								");
			querySQL.append("INNER JOIN ha.holderAccountDetails		had								");
			whereSQL.append("AND had.holder.idHolderPk IN :holderLst								");
			parameters.put("holderLst", holders);
		}
		
		if(accounts!=null && !accounts.isEmpty()){
//			querySQL.append("INNER JOIN hab.holderAccount			ha								");
			whereSQL.append("AND hab.holderAccount.idHolderAccountPk IN :accountLst					");
			parameters.put("accountLst", accounts);
		}
		
		if(participants!=null && !participants.isEmpty()){
//			querySQL.append("INNER JOIN hab.participant				part							");
			whereSQL.append("AND hab.participant.idParticipantPk IN :participantsLst				");
			parameters.put("participantsLst", participants);
		}
		
		Query query = em.createQuery(querySQL.toString().concat(whereSQL.toString()));
		for (String entry : parameters.keySet()) {
			query.setParameter(entry,parameters.get(entry));
		}
		return query.getResultList();
	}
	
	/**
	 * *
	 * Method to split all securities in chunks.
	 *
	 * @param securitiesCode the securities code
	 * @return the string list by max row
	 * @throws ServiceException the service exception
	 */
	public static List<List<Security>> getStringListByMaxRow(List<Security> securitiesCode) throws ServiceException{
		Integer iterationNumber = securitiesCode.size()/MAX_ROWS;
		List<List<Security>> getListByRow = new ArrayList<>();
		
		for(int i=0; i <= iterationNumber; i++){
			List<Security> subLisSecurity = getSecuritiesBySecurityIndex(i, securitiesCode);
			if(!subLisSecurity.isEmpty()){
				getListByRow.add(subLisSecurity);
			}
		}
		return getListByRow;
	}
	
	/**
	 * *
	 * Method to split all list in chunks.
	 *
	 * @param securitiesCode the securities code
	 * @return the string by max row
	 * @throws ServiceException the service exception
	 */
	public static List<List<String>> getStringByMaxRow(List<String> securitiesCode) throws ServiceException{
		Integer iterationNumber = securitiesCode.size()/MAX_ROWS;
		List<List<String>> getListByRow = new ArrayList<>();
		
		for(int i=0; i <= iterationNumber; i++){
			List<String> subListString = getSecuritiesByIndex(i, securitiesCode);
			if(!subListString.isEmpty()){
				getListByRow.add(subListString);
			}
		}
		return getListByRow;
	}
	
	/**
	 * *
	 * Method to split all operations in chunks.
	 *
	 * @param securitiesCode the securities code
	 * @return the operation by max row
	 * @throws ServiceException the service exception
	 */
	public static List<List<MechanismOperation>> getOperationByMaxRow(List<MechanismOperation> securitiesCode) throws ServiceException{
		Integer iterationNumber = securitiesCode.size()/MAX_ROWS;
		List<List<MechanismOperation>> getListByRow = new ArrayList<>();
		
		for(int i=0; i <= iterationNumber; i++){
			List<MechanismOperation> subListOperation = getOperationByIndex(i, securitiesCode);
			if(!subListOperation.isEmpty()){
				getListByRow.add(subListOperation);
			}
		}
		return getListByRow;
	}
	
	/**
	 * Gets the security query.
	 *
	 * @param instrumentType the instrument type
	 * @param securities the securities
	 * @param accounts the accounts
	 * @param holders the holders
	 * @param participants the participants
	 * @return the security query
	 * @throws ServiceException the service exception
	 */
	public List<Object> getSecurityQuery(Integer instrumentType, List<String> securities, List<Long> accounts,
																	 List<Long> holders, List<Long> participants) throws ServiceException{
		StringBuilder querySQL = new StringBuilder();
		StringBuilder whereSQL = new StringBuilder();
		Map<String,Object> parameters = new HashMap<>();
		
		querySQL.append("SELECT DISTINCT hab.security.idSecurityCodePk								");
		querySQL.append("FROM HolderAccountBalance				hab									");
		whereSQL.append("WHERE 1 = 1 																");
		
		if(instrumentType!=null){
			whereSQL.append("AND hab.security.instrumentType = :instrumentType						");
			parameters.put("instrumentType", instrumentType);
		}
		
		if(securities!=null && !securities.isEmpty()){
			whereSQL.append("AND hab.security.idSecurityCodePk IN :securitieLst						");
			parameters.put("securitieLst", securities);
		}
		
		if(accounts!=null && !accounts.isEmpty()){
//			querySQL.append("INNER JOIN hab.holderAccount			ha								");
			whereSQL.append("AND hab.holderAccount.idHolderAccountPk IN :accountLst					");
			parameters.put("accountLst", accounts);
		}
		
		if(holders!=null && !holders.isEmpty()){
			querySQL.append("INNER JOIN hab.holderAccount			ha								");
			querySQL.append("INNER JOIN ha.holderAccountDetails		had								");
			whereSQL.append("AND had.holder.idHolderPk IN :holderLst								");
			parameters.put("holderLst", holders);
		}
		
		if(participants!=null && !participants.isEmpty()){
//			querySQL.append("INNER JOIN hab.participant				part							");
			whereSQL.append("AND hab.participant.idParticipantPk IN :participantsLst				");
			parameters.put("participantsLst", participants);
		}
			
		whereSQL.append("AND hab.totalBalance > 0													");
		
		List<Object> queryAndParameter = new ArrayList<>();
		queryAndParameter.add(querySQL.toString().concat(whereSQL.toString()));
		queryAndParameter.add(parameters);
		
		return queryAndParameter;
	}

	/**
	 * *
	 * Method to get all participant in portFolly.
	 *
	 * @param instrumentType the instrument type
	 * @return the participant on portfolly
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Long> getParticipantOnPortfolly(Integer instrumentType) throws ServiceException{
		StringBuilder querySQL = new StringBuilder();
		Map<String,Object> parameters = new HashMap<>();
		
		querySQL.append("Select Distinct p.idParticipantPk							");
		querySQL.append("From HolderAccountBalance 	hab								");
		querySQL.append("Inner Join hab.security sec								");
		querySQL.append("Inner Join hab.participant p								");
		querySQL.append("Where 1 = 1 												");
		
		if(instrumentType!=null){
			querySQL.append("And sec.instrumentType = :instrumentType				");
			parameters.put("instrumentType", instrumentType);
		}
		querySQL.append("And (hab.totalBalance > 0 Or hab.reportedBalance > 0)		");
		querySQL.append("Order By p.idParticipantPk									");
		return findListByQueryString(querySQL.toString(), parameters);
	}
	
	
	/**
	 * *
	 * Method to return all securities.
	 *
	 * @param instrumentType the instrument type
	 * @param securities the securities
	 * @param accounts the accounts
	 * @param holders the holders
	 * @param participants the participants
	 * @return the securities
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<String> getSecurities(Integer instrumentType, List<String> securities, List<Long> accounts, List<Long> holders, List<Long> participants) 
									  throws ServiceException{
		String querySql = (String) getSecurityQuery(instrumentType, securities, accounts, holders, participants).get(0);
		Map<String,Object> parameters = (Map<String, Object>) getSecurityQuery(instrumentType, securities, accounts, holders, participants).get(1);
		
		Query query = em.createQuery(querySql);
		for (String entry : parameters.keySet()) {
			query.setParameter(entry,parameters.get(entry));
		}
		return query.getResultList();
	}
}