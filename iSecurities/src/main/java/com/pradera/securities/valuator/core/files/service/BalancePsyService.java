package com.pradera.securities.valuator.core.files.service;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;

import com.pradera.commons.configuration.ValuatorSetup;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.valuatorprocess.ValuatorProcessSetup;


// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright EDV BOLIVIA 2016.</li>
 * </ul>
 * 
 * The Class BalancePsyService.
 *
 * @author OQUISBERT.
 * @version 1.0 , 07-OCT-2016
 */

@Stateless
public class BalancePsyService extends CrudDaoServiceBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** Inject Valuator Setup to handle consolidate file on memory. */
	@Inject @ValuatorSetup ValuatorProcessSetup valuatorProcessSetup;

	public void processDailyBalancePsy(LoggerUser loggerUser) throws ServiceException{	
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" BEGIN SP_PROCESS_BALANCE_PSY(");
		stringBuffer.append(" 	:processDate, ");
		stringBuffer.append(" 	:lastUser, ");
		stringBuffer.append(" 	:lastApp, ");
		stringBuffer.append(" 	:lastDate, ");
		stringBuffer.append(" 	:lastIP); ");
		stringBuffer.append(" 	END; ");

		Query query= em.createNativeQuery(stringBuffer.toString());
		
		query.setParameter("processDate", CommonsUtilities.truncateDateTime(CommonsUtilities.currentDate()));
		query.setParameter("lastUser", loggerUser.getUserName());
		query.setParameter("lastApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastDate", loggerUser.getAuditTime());
		query.setParameter("lastIP", loggerUser.getIpAddress());
		
		query.executeUpdate();
	}
}
