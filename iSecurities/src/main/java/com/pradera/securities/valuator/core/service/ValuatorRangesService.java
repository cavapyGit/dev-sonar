package com.pradera.securities.valuator.core.service; 

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.pradera.commons.configuration.ValuatorSetup;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.valuatorprocess.ValuatorProcessSetup;
import com.pradera.model.valuatorprocess.ValuatorTermRange;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorRangesService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@Stateless
public class ValuatorRangesService extends CrudDaoServiceBean{
	
	/** The valuator setup. */
	@Inject @ValuatorSetup ValuatorProcessSetup valuatorSetup;

	/**
	 * Find term range.
	 *
	 * @param range the range
	 * @return the valuator term range
	 * @throws ServiceException the service exception
	 */
	public ValuatorTermRange findTermRange(BigDecimal range) throws ServiceException{
		List<ValuatorTermRange> ranges = valuatorSetup.getValuatorTermRange();
		BigDecimal rangeRound = CommonsUtilities.round(range, 0);
//		Integer count = GeneralConstants.ZERO_VALUE_INTEGER;
		for(ValuatorTermRange rangeObject: ranges){
//			count++;
			if(rangeRound.compareTo(rangeObject.getMinRange())>=0 && rangeObject.getMaxRange().compareTo(rangeRound)>=0){
//				if(count.equals(ranges.size())){
//					rangeObject.setMaxRange(range);
//				}
				return rangeObject;
			}
		}
		throw new ServiceException();
	}
}