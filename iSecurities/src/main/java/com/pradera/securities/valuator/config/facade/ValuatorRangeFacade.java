package com.pradera.securities.valuator.config.facade;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.valuatorprocess.ValuatorTermRange;
import com.pradera.securities.valuator.config.service.ValuatorRangeService;
import com.pradera.securities.valuator.config.view.ValuatorRangeTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorRangeFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ValuatorRangeFacade {
	
	/** The transaction registry. */
	@Resource private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The valuator configuration range service. */
	@EJB private ValuatorRangeService valuatorRangeService;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup private IdepositarySetup idepositarySetup;

	/**
	 * Creates the valuator term range.
	 *
	 * @param valuatorTermRange the valuator term range
	 * @return the valuator term range
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.VALUATOR_EXECUTION_RANGE_REGISTER)
	public ValuatorTermRange registerTermRange(ValuatorTermRange valuatorTermRange) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		valuatorTermRange.setAudit(loggerUser);
		return valuatorRangeService.saveNewTermRange(valuatorTermRange);
	}

	/**
	 * Update valuatorTermRange.
	 * Method to update the ValuatorTermRange
	 * @param valuatorTermRange the valuator term range
	 * @return the valuator term range
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.VALUATOR_EXECUTION_RANGE_MODIFY)
	public ValuatorTermRange modifyTermRange(ValuatorTermRange valuatorTermRange) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		valuatorTermRange.setAudit(loggerUser);
		return valuatorRangeService.modifyTermRange(valuatorTermRange);
	}

	/**
	 * Delete valuatorTermRange.
	 * Method to delete the ValuatorTermRange
	 * @param valuatorTermRange the valuator term range
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.VALUATOR_EXECUTION_RANGE_DELETE)
	public void deleteTermRange(ValuatorTermRange valuatorTermRange) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeDelete());
		valuatorTermRange.setAudit(loggerUser);
		valuatorRangeService.deleteTermRange(valuatorTermRange);
	}

	/***
	 * Method to find a valuator term range by Id.
	 * @param idTermRangePk the id term range
	 * @return a object ValuatorTermRange type
	 * @throws ServiceException the service exception
	 */
	public ValuatorTermRange viewTermRange(Long idTermRangePk) throws ServiceException {
		return valuatorRangeService.viewTermRange(idTermRangePk);
	}
	
	/**
	 * *
	 * Method to get a list of ValuatorRangeTO by ValuatorRangeTO.
	 *
	 * @param valuatorTermRangeTO the valuator term range to
	 * @return a list of ValuatorRangeTO
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.VALUATOR_EXECUTION_RANGE_QUERY)
	public List<ValuatorRangeTO> getValuatorRangeFacade(ValuatorRangeTO valuatorTermRangeTO) throws ServiceException{
		List<ValuatorRangeTO> list = valuatorRangeService.getValuatorRangeServiceBean(valuatorTermRangeTO);
		return list;
	}
	
	/**
	 * Method to validate is the ValuatorTermRange exists.
	 * @param valuatorTermRange the valuator term range
	 * @return boolean value
	 * @throws ServiceException the service exception
	 */
	public Boolean valuatorTermRangeExists(ValuatorTermRange valuatorTermRange) throws ServiceException{
		return valuatorRangeService.valuatorTermRangeExists(valuatorTermRange);
	}

	/***
	 * Method to find a valuator term range by securityDays.
	 * @param securityDays the security days
	 * @return a object ValuatorTermRange type
	 * @throws ServiceException the service exception
	 */
	public ValuatorTermRange getValuatorRangeBySecurityDaysFacade(Integer securityDays) throws ServiceException{
		return valuatorRangeService.getValuatorRangeBySecurityDaysService(securityDays);
	}
}