package com.pradera.securities.valuator.core.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

import com.pradera.model.valuatorprocess.SecurityClassValuator;
import com.pradera.model.valuatorprocess.ValuatorMarketfactConsolidat;
import com.pradera.model.valuatorprocess.ValuatorProcessClass;
import com.pradera.model.valuatorprocess.ValuatorTermRange;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorProcessSetupTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class ValuatorProcessSetupTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id valuator setup pk. */
	private Long idValuatorSetupPk;

	
	/** The execution hour. */
	private Date executionHour;

	
	/** The ind automatic. */
	private Integer indAutomatic;

	
	/** The ind manual. */
	private Integer indManual;

	
	/** The ind valuator custody. */
	private Integer indValuatorCustody;

	
	/** The ind valuator negotiations. */
	private Integer indValuatorNegotiations;

	
	/** The ind valuator portfoly. */
	private Integer indValuatorPortfoly;

	
	/** The ind valuator security. */
	private Integer indValuatorSecurity;

	
	/** The ind valuatos guarantees. */
	private Integer indValuatosGuarantees;
	
	
	/** The coefficient io. */
	private BigDecimal coefficientIO;
	
	
	/** The length code fixed. */
	private Integer lengthCodeFixed;
	
	
	/** The length code variable. */
	private Integer lengthCodeVariable;
	
	
	/** The prepaid code. */
	private String prepaidCode;
	
	
	/** The sub ordinated code. */
	private String subOrdinatedCode;
	
	
	/** The ind history. */
	private Integer  indHistory;
	
	
	/** The user target. */
	private String userTarget;
	
	
	/** The parameters. */
	private Map<Integer,String> parameters;
	
	
	/** The security class valuators. */
	private List<SecurityClassValuator> securityClassValuators;
	
	
	/** The valuator process class. */
	private List<ValuatorProcessClass> valuatorProcessClass;
	
	
	/** The valuator term range. */
	private List<ValuatorTermRange> valuatorTermRange;
	
	
	/** The security result price map. */
	private ConcurrentHashMap<Date,Map<String,Map<Integer,Map<Object,Map<Date,List<BigDecimal>>>>>> securityResultPriceMap;
//	private ConcurrentHashMap<Date,Map<String,Map<Integer,Map<BigDecimal,BigDecimal>>>> securityResultPriceMap;
	
	
	/** The market fact consolidat. */
private ConcurrentHashMap<String,List<ValuatorMarketfactConsolidat>> marketFactConsolidat;
	
	
	/** The array with prices. */
	private Object arrayWithPrices;
	
	
	/** The theoric market price map. */
	private ConcurrentHashMap<String,Map<Object,BigDecimal>> theoricMarketPriceMap;
	
	
	/** The valuator code map. */
	private ConcurrentHashMap<Long,String> valuatorCodeMap;
	
	
	/** The securities rate price tmp. */
	private ArrayBlockingQueue<Object> securitiesRatePriceTmp;

	/**
	 * Instantiates a new valuator process setup to.
	 */
	public ValuatorProcessSetupTO() {
	}

	/**
	 * Gets the id valuator setup pk.
	 *
	 * @return the id valuator setup pk
	 */
	public Long getIdValuatorSetupPk() {
		return this.idValuatorSetupPk;
	}

	/**
	 * Sets the id valuator setup pk.
	 *
	 * @param idValuatorSetupPk the new id valuator setup pk
	 */
	public void setIdValuatorSetupPk(Long idValuatorSetupPk) {
		this.idValuatorSetupPk = idValuatorSetupPk;
	}

	/**
	 * Gets the execution hour.
	 *
	 * @return the execution hour
	 */
	public Date getExecutionHour() {
		return this.executionHour;
	}

	/**
	 * Sets the execution hour.
	 *
	 * @param executionHour the new execution hour
	 */
	public void setExecutionHour(Date executionHour) {
		this.executionHour = executionHour;
	}

	/**
	 * Gets the ind automatic.
	 *
	 * @return the ind automatic
	 */
	public Integer getIndAutomatic() {
		return this.indAutomatic;
	}

	/**
	 * Sets the ind automatic.
	 *
	 * @param indAutomatic the new ind automatic
	 */
	public void setIndAutomatic(Integer indAutomatic) {
		this.indAutomatic = indAutomatic;
	}

	/**
	 * Gets the ind manual.
	 *
	 * @return the ind manual
	 */
	public Integer getIndManual() {
		return this.indManual;
	}

	/**
	 * Sets the ind manual.
	 *
	 * @param indManual the new ind manual
	 */
	public void setIndManual(Integer indManual) {
		this.indManual = indManual;
	}

	/**
	 * Gets the ind valuator custody.
	 *
	 * @return the ind valuator custody
	 */
	public Integer getIndValuatorCustody() {
		return this.indValuatorCustody;
	}

	/**
	 * Sets the ind valuator custody.
	 *
	 * @param indValuatorCustody the new ind valuator custody
	 */
	public void setIndValuatorCustody(Integer indValuatorCustody) {
		this.indValuatorCustody = indValuatorCustody;
	}

	/**
	 * Gets the ind valuator negotiations.
	 *
	 * @return the ind valuator negotiations
	 */
	public Integer getIndValuatorNegotiations() {
		return this.indValuatorNegotiations;
	}

	/**
	 * Sets the ind valuator negotiations.
	 *
	 * @param indValuatorNegotiations the new ind valuator negotiations
	 */
	public void setIndValuatorNegotiations(Integer indValuatorNegotiations) {
		this.indValuatorNegotiations = indValuatorNegotiations;
	}

	/**
	 * Gets the ind valuator portfoly.
	 *
	 * @return the ind valuator portfoly
	 */
	public Integer getIndValuatorPortfoly() {
		return this.indValuatorPortfoly;
	}

	/**
	 * Sets the ind valuator portfoly.
	 *
	 * @param indValuatorPortfoly the new ind valuator portfoly
	 */
	public void setIndValuatorPortfoly(Integer indValuatorPortfoly) {
		this.indValuatorPortfoly = indValuatorPortfoly;
	}

	/**
	 * Gets the ind valuator security.
	 *
	 * @return the ind valuator security
	 */
	public Integer getIndValuatorSecurity() {
		return this.indValuatorSecurity;
	}

	/**
	 * Sets the ind valuator security.
	 *
	 * @param indValuatorSecurity the new ind valuator security
	 */
	public void setIndValuatorSecurity(Integer indValuatorSecurity) {
		this.indValuatorSecurity = indValuatorSecurity;
	}

	/**
	 * Gets the ind valuatos guarantees.
	 *
	 * @return the ind valuatos guarantees
	 */
	public Integer getIndValuatosGuarantees() {
		return this.indValuatosGuarantees;
	}

	/**
	 * Sets the ind valuatos guarantees.
	 *
	 * @param indValuatosGuarantees the new ind valuatos guarantees
	 */
	public void setIndValuatosGuarantees(Integer indValuatosGuarantees) {
		this.indValuatosGuarantees = indValuatosGuarantees;
	}

	/**
	 * Gets the coefficient io.
	 *
	 * @return the coefficient io
	 */
	public BigDecimal getCoefficientIO() {
		return coefficientIO;
	}

	/**
	 * Sets the coefficient io.
	 *
	 * @param coefficientIO the new coefficient io
	 */
	public void setCoefficientIO(BigDecimal coefficientIO) {
		this.coefficientIO = coefficientIO;
	}

	/**
	 * Gets the length code fixed.
	 *
	 * @return the length code fixed
	 */
	public Integer getLengthCodeFixed() {
		return lengthCodeFixed;
	}

	/**
	 * Sets the length code fixed.
	 *
	 * @param lengthCodeFixed the new length code fixed
	 */
	public void setLengthCodeFixed(Integer lengthCodeFixed) {
		this.lengthCodeFixed = lengthCodeFixed;
	}

	/**
	 * Gets the length code variable.
	 *
	 * @return the length code variable
	 */
	public Integer getLengthCodeVariable() {
		return lengthCodeVariable;
	}

	/**
	 * Sets the length code variable.
	 *
	 * @param lengthCodeVariable the new length code variable
	 */
	public void setLengthCodeVariable(Integer lengthCodeVariable) {
		this.lengthCodeVariable = lengthCodeVariable;
	}

	/**
	 * Gets the parameters.
	 *
	 * @return the parameters
	 */
	public Map<Integer, String> getParameters() {
		return parameters;
	}

	/**
	 * Sets the parameters.
	 *
	 * @param parameters the parameters
	 */
	public void setParameters(Map<Integer, String> parameters) {
		this.parameters = parameters;
	}

	/**
	 * Gets the prepaid code.
	 *
	 * @return the prepaid code
	 */
	public String getPrepaidCode() {
		return prepaidCode;
	}

	/**
	 * Sets the prepaid code.
	 *
	 * @param prepaidCode the new prepaid code
	 */
	public void setPrepaidCode(String prepaidCode) {
		this.prepaidCode = prepaidCode;
	}

	/**
	 * Gets the sub ordinated code.
	 *
	 * @return the sub ordinated code
	 */
	public String getSubOrdinatedCode() {
		return subOrdinatedCode;
	}

	/**
	 * Sets the sub ordinated code.
	 *
	 * @param subOrdinatedCode the new sub ordinated code
	 */
	public void setSubOrdinatedCode(String subOrdinatedCode) {
		this.subOrdinatedCode = subOrdinatedCode;
	}

	/**
	 * Gets the security class valuators.
	 *
	 * @return the security class valuators
	 */
	public List<SecurityClassValuator> getSecurityClassValuators() {
		return securityClassValuators;
	}

	/**
	 * Sets the security class valuators.
	 *
	 * @param securityClassValuators the new security class valuators
	 */
	public void setSecurityClassValuators(List<SecurityClassValuator> securityClassValuators) {
		this.securityClassValuators = securityClassValuators;
	}

	/**
	 * Gets the valuator process class.
	 *
	 * @return the valuator process class
	 */
	public List<ValuatorProcessClass> getValuatorProcessClass() {
		return valuatorProcessClass;
	}

	/**
	 * Sets the valuator process class.
	 *
	 * @param valuatorProcessClass the new valuator process class
	 */
	public void setValuatorProcessClass(List<ValuatorProcessClass> valuatorProcessClass) {
		this.valuatorProcessClass = valuatorProcessClass;
	}

	/**
	 * Gets the valuator term range.
	 *
	 * @return the valuator term range
	 */
	public List<ValuatorTermRange> getValuatorTermRange() {
		return valuatorTermRange;
	}

	/**
	 * Sets the valuator term range.
	 *
	 * @param valuatorTermRange the new valuator term range
	 */
	public void setValuatorTermRange(List<ValuatorTermRange> valuatorTermRange) {
		this.valuatorTermRange = valuatorTermRange;
	}

	/**
	 * Gets the securities rate price tmp.
	 *
	 * @return the securities rate price tmp
	 */
	public ArrayBlockingQueue<Object> getSecuritiesRatePriceTmp() {
		return securitiesRatePriceTmp;
	}

	/**
	 * Sets the securities rate price tmp.
	 *
	 * @param securitiesRatePriceTmp the new securities rate price tmp
	 */
	public void setSecuritiesRatePriceTmp(
			ArrayBlockingQueue<Object> securitiesRatePriceTmp) {
		this.securitiesRatePriceTmp = securitiesRatePriceTmp;
	}

	/**
	 * Gets the market fact consolidat.
	 *
	 * @return the market fact consolidat
	 */
	public ConcurrentHashMap<String, List<ValuatorMarketfactConsolidat>> getMarketFactConsolidat() {
		return marketFactConsolidat;
	}

	/**
	 * Sets the market fact consolidat.
	 *
	 * @param marketFactConsolidat the market fact consolidat
	 */
	public void setMarketFactConsolidat(
			ConcurrentHashMap<String, List<ValuatorMarketfactConsolidat>> marketFactConsolidat) {
		this.marketFactConsolidat = marketFactConsolidat;
	}

	/**
	 * Gets the security result price map.
	 *
	 * @return the security result price map
	 */
	public ConcurrentHashMap<Date, Map<String, Map<Integer, Map<Object, Map<Date,List<BigDecimal>>>>>> getSecurityResultPriceMap() {
		return securityResultPriceMap;
	}

	/**
	 * Sets the security result price map.
	 *
	 * @param securityResultPriceMap the security result price map
	 */
	public void setSecurityResultPriceMap(ConcurrentHashMap<Date, Map<String, Map<Integer, Map<Object, Map<Date,List<BigDecimal>>>>>> securityResultPriceMap) {
		this.securityResultPriceMap = securityResultPriceMap;
	}

	/**
	 * Gets the ind history.
	 *
	 * @return the ind history
	 */
	public Integer getIndHistory() {
		return indHistory;
	}

	/**
	 * Sets the ind history.
	 *
	 * @param indHistory the new ind history
	 */
	public void setIndHistory(Integer indHistory) {
		this.indHistory = indHistory;
	}

	/**
	 * Gets the theoric market price map.
	 *
	 * @return the theoric market price map
	 */
	public ConcurrentHashMap<String, Map<Object, BigDecimal>> getTheoricMarketPriceMap() {
		return theoricMarketPriceMap;
	}

	/**
	 * Sets the theoric market price map.
	 *
	 * @param theoricMarketPriceMap the theoric market price map
	 */
	public void setTheoricMarketPriceMap(
			ConcurrentHashMap<String, Map<Object, BigDecimal>> theoricMarketPriceMap) {
		this.theoricMarketPriceMap = theoricMarketPriceMap;
	}

	/**
	 * Gets the valuator code map.
	 *
	 * @return the valuator code map
	 */
	public ConcurrentHashMap<Long, String> getValuatorCodeMap() {
		return valuatorCodeMap;
	}

	/**
	 * Sets the valuator code map.
	 *
	 * @param valuatorCodeMap the valuator code map
	 */
	public void setValuatorCodeMap(ConcurrentHashMap<Long, String> valuatorCodeMap) {
		this.valuatorCodeMap = valuatorCodeMap;
	}

	/**
	 * Gets the array with prices.
	 *
	 * @return the array with prices
	 */
	public Object getArrayWithPrices() {
		return arrayWithPrices;
	}

	/**
	 * Sets the array with prices.
	 *
	 * @param arrayWithPrices the new array with prices
	 */
	public void setArrayWithPrices(Object arrayWithPrices) {
		this.arrayWithPrices = arrayWithPrices;
	}
	
	/**
	 * Gets the user target.
	 *
	 * @return the user target
	 */
	public String getUserTarget() {
		return userTarget;
	}

	/**
	 * Sets the user target.
	 *
	 * @param userTarget the new user target
	 */
	public void setUserTarget(String userTarget) {
		this.userTarget = userTarget;
	}
	
}