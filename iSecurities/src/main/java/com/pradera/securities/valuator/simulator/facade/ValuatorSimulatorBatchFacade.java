package com.pradera.securities.valuator.simulator.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.issuancesecuritie.AmortizationPaymentSchedule;
import com.pradera.model.issuancesecuritie.InterestPaymentSchedule;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.process.type.ProcessStateType;
import com.pradera.model.valuatorprocess.ValuatorProcessClass;
import com.pradera.model.valuatorprocess.ValuatorProcessSimulator;
import com.pradera.model.valuatorprocess.ValuatorSimulationBalance;
import com.pradera.model.valuatorprocess.ValuatorSimulatorSecurity;
import com.pradera.model.valuatorprocess.type.ValuatorDetailType;
import com.pradera.model.valuatorprocess.type.ValuatorSimulatorForm;
import com.pradera.securities.valuator.core.service.ValuatorProcessAditional;
import com.pradera.securities.valuator.simulator.service.ValuatorSimulatorBatchService;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorSimulatorBatchFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ValuatorSimulatorBatchFacade {

	/** The valuator simulator batch service. */
	@EJB private ValuatorSimulatorBatchService valuatorSimulatorBatchService;
	
	/** The valuator process aditional. */
	@EJB private ValuatorProcessAditional valuatorProcessAditional;

	
	
	/**
	 * Register valuator simulatio balance.
	 *
	 * @param idProcessSimulator the id process simulator
	 * @throws ServiceException the service exception
	 */
	public void registerValuatorSimulatioBalance(Long idProcessSimulator) throws ServiceException{
			ValuatorProcessSimulator valuatorProcessSimulator = valuatorSimulatorBatchService.valuatorProcessSimulator(idProcessSimulator);
			
			if (valuatorProcessSimulator.getSimulationForm().intValue()== ValuatorSimulatorForm.BY_VALUATION_CODE.getCode().intValue()){
				Map<String,Object[]> mapSimulatorCode = valuatorSimulatorBatchService.listValuatorSimulator(idProcessSimulator);
				Long idParticipant = null;
				Long idHolderAccount = null;
				Long idHolder = null;
				if (valuatorProcessSimulator.getParticipant()!=null){
					idParticipant=valuatorProcessSimulator.getParticipant().getIdParticipantPk();
				}else{
					idParticipant=null;
				}
				
				if (valuatorProcessSimulator.getHolderAccount()!=null){
					idHolderAccount=valuatorProcessSimulator.getHolderAccount().getIdHolderAccountPk();
				}else{
					idHolderAccount=null;
				}
				
				if (valuatorProcessSimulator.getHolder()!=null){
					idHolder=valuatorProcessSimulator.getHolder().getIdHolderPk();
				}else{
					idHolder = null;
				}
 				
				List<HolderAccountBalance> listHolderBalance = valuatorSimulatorBatchService.listHolderAccountBalances(idProcessSimulator,idParticipant,idHolderAccount,idHolder);
				List<ValuatorSimulationBalance> listSimulationBalance = new ArrayList<ValuatorSimulationBalance>();
				for (HolderAccountBalance holderAccountBalance : listHolderBalance) {
					ValuatorSimulationBalance simulationBalance = new ValuatorSimulationBalance();
					simulationBalance.setHolderAccountBalance(holderAccountBalance);
					simulationBalance.setValuatorProcessSimulator(valuatorProcessSimulator);
					Object[] object = mapSimulatorCode.get(holderAccountBalance.getSecurity().getIdSecurityCodePk());
					Integer indNominalValue = (Integer) object[0];
					BigDecimal averateRate = (BigDecimal) object[1];
					if (object[2] !=null){
						String valuatorCode = (String)object[2]+"";
						simulationBalance.setValuatorCode(valuatorCode);
					}
					
					simulationBalance.setIndNominalRate(indNominalValue);
					simulationBalance.setSimulationRate(averateRate);
					simulationBalance.setSimulationPrice(BigDecimal.ZERO);
					simulationBalance.setSimulationDate(valuatorProcessSimulator.getSimulationDate());
					simulationBalance.setTotalBalance(holderAccountBalance.getTotalBalance());
					simulationBalance.setAvailableBalance(holderAccountBalance.getAvailableBalance());
					simulationBalance.setSimulationAmount(BigDecimal.ZERO);
					simulationBalance.setSimulationAvailableAmount(BigDecimal.ZERO);
					simulationBalance.setProcessState(ProcessStateType.REGISTERED.getCode());
					listSimulationBalance.add(simulationBalance);
				}
				valuatorSimulatorBatchService.saveNewListSimulatorBalance(listSimulationBalance);
				generateSimulationPrice(valuatorProcessSimulator.getIdValuatorSimulatorPk());
			}else if (valuatorProcessSimulator.getSimulationForm().intValue()== ValuatorSimulatorForm.BALANCE.getCode().intValue()){
				generateSimulationPriceByBalance(idProcessSimulator);
			}
	}
	
	/**
	 * Generate simulation price by balance.
	 *
	 * @param idProcessSimulator the id process simulator
	 */
	private void generateSimulationPriceByBalance(Long idProcessSimulator) {
		ValuatorProcessSimulator valuatorProcessSimulator = valuatorSimulatorBatchService.find(ValuatorProcessSimulator.class, idProcessSimulator);
		List<Object[]> listObjectBalances = valuatorSimulatorBatchService.listObjectSimulationBalances(idProcessSimulator);
		Map<String,Map<BigDecimal, BigDecimal>> securityRate = new HashMap<>();
		
		for (Object[] data : listObjectBalances){			
			String securityCode = data[0].toString();			
			if (securityRate.get(securityCode)==null){
				Map<BigDecimal,BigDecimal> ratePrice = new HashMap<>();
				ratePrice.put((BigDecimal)data[1], null);
				securityRate.put(securityCode, ratePrice);
			}else{
				Map<BigDecimal,BigDecimal> ratePrice = securityRate.get(securityCode);
				ratePrice.put((BigDecimal)data[1], null);
			}
		}
		
		for (Entry<String, Map<BigDecimal, BigDecimal>> securityCod : securityRate.entrySet()){
			String secucityCode = securityCod.getKey();
			Map<BigDecimal,BigDecimal> ratePrice = securityCod.getValue();
			
			Security security  = valuatorSimulatorBatchService.simulationSecurity(secucityCode);
			ValuatorProcessClass processClass = security.getValuatorProcessClass();
			InterestPaymentSchedule interestPaymentSchedule = valuatorSimulatorBatchService.interestPaymentSchedule(secucityCode,valuatorProcessSimulator.getSimulationDate());
			AmortizationPaymentSchedule amortizationPaymentSchedule  = valuatorSimulatorBatchService.amortizationPaymentSchedule(secucityCode, valuatorProcessSimulator.getSimulationDate());
			security.setInterestPaymentSchedule(interestPaymentSchedule);
			security.setAmortizationPaymentSchedule(amortizationPaymentSchedule);
			
			BigDecimal price = null;
			for (Entry<BigDecimal, BigDecimal> rateMap : ratePrice.entrySet()){
				BigDecimal rate= rateMap.getKey();
				try {
					Date mrkDate = valuatorProcessSimulator.getSimulationDate();
					BigDecimal mrkRate = rate;
					Integer formuleType= processClass.getFormuleType();
					price = valuatorProcessAditional.calculateMarketPrice(security, null, formuleType, mrkRate,mrkDate, mrkDate, ValuatorDetailType.SECURITY.getCode());
				} catch (ServiceException e) {
					e.printStackTrace();
				}
				ratePrice.put(rateMap.getKey(), price);
			}
		}
		
		//Actualizacion de precios en la tabla
		Object[] object=new Object[4];
		for (Entry<String, Map<BigDecimal, BigDecimal>> securityCod : securityRate.entrySet()){
			String secucityCode = securityCod.getKey();
			Map<BigDecimal,BigDecimal> ratePrice = securityCod.getValue();
			object[0]=secucityCode;
			for (Entry<BigDecimal, BigDecimal> rateMap : ratePrice.entrySet()){
				BigDecimal rate= rateMap.getKey();
				object[1]=rate;
				object[2]=idProcessSimulator;
				object[3]=rateMap.getValue();
				valuatorSimulatorBatchService.updatePriceSimulationBalance(object);
			}
		}
	}
	
	
	/**
	 * Generate simulation price.
	 *
	 * @param idProcessSimulator the id process simulator
	 */
	private void generateSimulationPrice(Long idProcessSimulator) {
		Map<String,BigDecimal> securityPrice = new HashMap<>(); 
		ValuatorProcessSimulator valuatorProcessSimulator = valuatorSimulatorBatchService.find(ValuatorProcessSimulator.class, idProcessSimulator);
		List<ValuatorSimulatorSecurity> listSimulatorSecurities = valuatorSimulatorBatchService.listSimulatorSecurities(idProcessSimulator);
		
		for (ValuatorSimulatorSecurity simulatorSecurity : listSimulatorSecurities){
			Security security  = valuatorSimulatorBatchService.simulationSecurity(simulatorSecurity.getSecurity().getIdSecurityCodePk());
			ValuatorProcessClass processClass = security.getValuatorProcessClass();
			
			InterestPaymentSchedule interestPaymentSchedule = valuatorSimulatorBatchService.interestPaymentSchedule(simulatorSecurity.getSecurity().getIdSecurityCodePk(),valuatorProcessSimulator.getSimulationDate());
			AmortizationPaymentSchedule amortizationPaymentSchedule  = valuatorSimulatorBatchService.amortizationPaymentSchedule(simulatorSecurity.getSecurity().getIdSecurityCodePk(), valuatorProcessSimulator.getSimulationDate());

			security.setInterestPaymentSchedule(interestPaymentSchedule);
			security.setAmortizationPaymentSchedule(amortizationPaymentSchedule);
			
			BigDecimal price = null;
			try {
				Integer formuleType = processClass.getFormuleType();
				BigDecimal mrkRate = simulatorSecurity.getAverateRate();
				Date mrkDate = valuatorProcessSimulator.getSimulationDate();
				Date simulationDate = valuatorProcessSimulator.getSimulationDate();
				price = valuatorProcessAditional.calculateMarketPrice(security, null, formuleType, mrkRate, mrkDate, simulationDate, ValuatorDetailType.SECURITY.getCode());
			} catch (ServiceException e) {
				e.printStackTrace();
			}
			securityPrice.put(simulatorSecurity.getSecurity().getIdSecurityCodePk(), price);
		}
		
		List<ValuatorSimulationBalance> listSimulationBalances = valuatorSimulatorBatchService.listSimulationBalances(idProcessSimulator);
		for (ValuatorSimulationBalance simulationBalance :listSimulationBalances){
			BigDecimal simulationPrice = securityPrice.get(simulationBalance.getHolderAccountBalance().getSecurity().getIdSecurityCodePk());
			BigDecimal simulationAmount = simulationBalance.getTotalBalance().multiply(simulationPrice);
			BigDecimal simulationAvailAmount = simulationBalance.getAvailableBalance() .multiply(simulationPrice);
			simulationBalance.setSimulationPrice(simulationPrice);
			simulationBalance.setSimulationAmount(simulationAmount);
			simulationBalance.setSimulationAvailableAmount(simulationAvailAmount);
		}
		valuatorSimulatorBatchService.updateList(listSimulationBalances);
	}
}