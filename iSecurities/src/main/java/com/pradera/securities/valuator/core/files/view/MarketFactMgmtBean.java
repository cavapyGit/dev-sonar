package com.pradera.securities.valuator.core.files.view;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.beanio.BeanIOConfigurationException;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.negotiation.constant.NegotiationConstant;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.valuatorprocess.ValuatorMarketfactFile;
import com.pradera.model.valuatorprocess.type.ValuatorMarkFactFileType;
import com.pradera.securities.issuancesecuritie.util.PropertiesConstants;
import com.pradera.securities.utils.NegotiationUtils;
import com.pradera.securities.valuator.core.files.facade.MarketFactFacade;
import com.pradera.securities.valuator.core.files.to.MarketFactMgmtTO;
import com.pradera.securities.valuator.core.files.to.MarketfactValidationTO;
import com.pradera.securities.valuator.core.files.to.ValuatorMarketfactFileTO;
import com.pradera.securities.valuator.core.files.to.ValuatorMarketfactMechanismType;

@DepositaryWebBean
@LoggerCreateBean
public class MarketFactMgmtBean extends GenericBaseBean implements Serializable{
	private static final long serialVersionUID = 1L;
	private static final String REGISTER_FILES="registerFiles";
	private static final String SEARCH_FILES="searchFiles";
	private static final Integer REGISTERED = GeneralConstants.ONE_VALUE_INTEGER;
	private static final String QUOTATION_MARKS="\"";

	private String fUploadFileTypes = "txt|dat|bbv|plain";
	private Pattern fUploadFileTypesPattern=Pattern.compile("([^\\s]+(\\.(?i)("+fUploadFileTypes+"))$)");
	private String fUploadFileTypesDisplay="*.txt, *.dat, *.bbv";

	@EJB private GeneralParametersFacade generalParameterFacade;
	@EJB private MarketFactFacade marketfactFacade;
	@EJB private BatchProcessServiceFacade batchProcessServiceFacade;
	
	private MarketFactMgmtTO marketFactMgmtTO = new MarketFactMgmtTO();
	private ValuatorMarketfactFileTO tmpMarketfactFile;
	private List<ValuatorMarketfactFileTO> lstTmpMarketfactFile;
	
	private Map<Integer,String> parameterDescription = new HashMap<Integer,String>();
	private Map<Integer,Integer> mapMarketfactType = new HashMap<Integer,Integer>();
	private boolean isSelectedFileType;
	private Integer tmpFileSource;
	
	@Inject @Configurable Integer countryResidence;
	
	@Inject	private UserInfo userInfo;
	@Inject	private UserPrivilege userPrivilege;
	@Inject @DepositarySetup IdepositarySetup depositarySetup;
//	@Inject @ValuatorSetup ValuatorProcessSetup valuatorSetup;  //++ ver si lo utilizo
	
	@PostConstruct
	public void init() {
		marketFactMgmtTO= new MarketFactMgmtTO();
		listHolidays();
		defaultView();
		loadPriviligies();
		loadCboMarketfactType();
		loadCboFileSource();
		loadCboFileLoading();
		loadCboMarketfactState();
		loadCboMarketfactProcess();
	}
	
	private void defaultView(){
		marketFactMgmtTO.setValuatorMarketfactFileTO(new ValuatorMarketfactFileTO());
		marketFactMgmtTO.getValuatorMarketfactFileTO().setInitalDate(new Date());
		marketFactMgmtTO.getValuatorMarketfactFileTO().setEndDate(new Date());
		marketFactMgmtTO.setLstValuatorMarketfactFileTO(null);
	}
	
	public String loadViewRegister(){
		marketFactMgmtTO.setRegistryDate(getCurrentSystemDate());
		marketFactMgmtTO.setProcessDate(getCurrentSystemDate());
		setViewOperationType(ViewOperationsType.REGISTER.getCode());
		lstTmpMarketfactFile = new ArrayList<ValuatorMarketfactFileTO>();
		tmpMarketfactFile = new ValuatorMarketfactFileTO();
		marketFactMgmtTO.setLstRegisterVMarketfactFileTO(new GenericDataModel<ValuatorMarketfactFileTO>());
		marketFactMgmtTO.setLstErrors(new GenericDataModel<RecordValidationType>());
		mapMarketfactType.clear();
		marketFactMgmtTO.setMarketfactTypePk(null);
		marketFactMgmtTO.setFileSource(null);
		setSelectedFileType(Boolean.FALSE);
		loadCboMarketfactType();
		loadCboFileSource();
		return REGISTER_FILES;
	}
	private void loadPriviligies() {
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnCancel(Boolean.TRUE);
		privilegeComponent.setBtnRegisterView(Boolean.TRUE);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}
	
	@LoggerAuditWeb
	private void loadCboMarketfactType(){
		try{
			/**Loading variables by default*/
			marketFactMgmtTO.setMarketfactTypePk(null);
			
			ParameterTableTO parameterTableTO = new ParameterTableTO();		
			parameterTableTO.setMasterTableFk(MasterTableType.MARKETFACT_FILE_TYPE.getCode());
			parameterTableTO.setState(REGISTERED);
			
			/**If loading type is holiday, only holiday must be loaded*/
			if(marketFactMgmtTO.getFileLoadingType()!=null && marketFactMgmtTO.getFileLoadingType().equals(ValuatorMarkFactFileType.HOLIDAY_DAY.getCode())){
				parameterTableTO.setParameterTablePk(ValuatorMarkFactFileType.HISTORY_FILE.getCode());
				marketFactMgmtTO.setProcessDate(marketfactFacade.getNextHolidateDate(getCurrentSystemDate()));
			}else{
				marketFactMgmtTO.setProcessDate(CommonsUtilities.currentDate());
			}
			
			marketFactMgmtTO.setLstMarketfactType(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO));
		for(ParameterTable obj: marketFactMgmtTO.getLstMarketfactType()){
			parameterDescription.put(obj.getParameterTablePk(), obj.getParameterName());
		}
		}catch(ServiceException ex){
			ex.printStackTrace();
		}
	}
	
	private void listHolidays() {
		try {
			allHolidays=generalParameterFacade.getAllHolidaysByCountry(countryResidence);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@LoggerAuditWeb
	private void loadCboFileSource(){
		try{
			ParameterTableTO parameterTableTO = new ParameterTableTO();		
			parameterTableTO.setMasterTableFk(MasterTableType.MARKETFACT_FILE_SOURCE.getCode());
			parameterTableTO.setState(REGISTERED);
			marketFactMgmtTO.setLstFileSource(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO));
			if(marketFactMgmtTO.getLstFileSource()!=null && marketFactMgmtTO.getLstFileSource().size()==ComponentConstant.ONE){
				marketFactMgmtTO.setFileSource(marketFactMgmtTO.getLstFileSource().get(0).getParameterTablePk());
			}
		for(ParameterTable obj: marketFactMgmtTO.getLstFileSource()){
			parameterDescription.put(obj.getParameterTablePk(), obj.getParameterName());
		}
		}catch(ServiceException ex){
			ex.printStackTrace();
		}
	}
	
	/**
	 * Load combo file loading.
	 */
	private void loadCboFileLoading(){
		try{
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(REGISTERED);
			parameterTableTO.setOrderbyParameterTableCd(BooleanType.YES.getCode());		
			parameterTableTO.setMasterTableFk(MasterTableType.MARKETFACT_FILE_LOADING.getCode());
			
			marketFactMgmtTO.setLstFileLoading(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO));
			if(marketFactMgmtTO.getLstFileLoading()!=null && !marketFactMgmtTO.getLstFileLoading().isEmpty()){
				marketFactMgmtTO.setFileLoadingType(marketFactMgmtTO.getLstFileLoading().get(0).getParameterTablePk());
			}
		for(ParameterTable obj: marketFactMgmtTO.getLstFileLoading()){
			parameterDescription.put(obj.getParameterTablePk(), obj.getParameterName());
		}
		}catch(ServiceException ex){
			ex.printStackTrace();
		}
	}
	
	@LoggerAuditWeb
	private void loadCboMarketfactState(){
		try{
			ParameterTableTO parameterTableTO = new ParameterTableTO();		
			parameterTableTO.setMasterTableFk(MasterTableType.MARKETFACT_FILE_STATE.getCode());
			parameterTableTO.setState(REGISTERED);
			marketFactMgmtTO.setLstMarketfactState(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO));
		for(ParameterTable obj: marketFactMgmtTO.getLstMarketfactState()){
			parameterDescription.put(obj.getParameterTablePk(), obj.getParameterName());
		}
		}catch(ServiceException ex){
			ex.printStackTrace();
		}
	}
	@LoggerAuditWeb
	private void loadCboMarketfactProcess(){
		try{
			ParameterTableTO parameterTableTO = new ParameterTableTO();		
			parameterTableTO.setMasterTableFk(MasterTableType.MARKETFACT_PROCESS_TYPE.getCode());
			parameterTableTO.setState(REGISTERED);
			marketFactMgmtTO.setLstMarketfactProcess(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO));
		for(ParameterTable obj: marketFactMgmtTO.getLstMarketfactProcess()){
			parameterDescription.put(obj.getParameterTablePk(), obj.getParameterName());
		}
		}catch(ServiceException ex){
			
		}
	}
	private void loadListErrors(List<RecordValidationType> lstErrors){
		if(Validations.validateIsNotNullAndNotEmpty(lstErrors)){
			marketFactMgmtTO.setLstErrors(new GenericDataModel<RecordValidationType>(lstErrors));
		}else{
			marketFactMgmtTO.setLstErrors(new GenericDataModel<RecordValidationType>());
		}
		
	}
	@LoggerAuditWeb
	public void searchValuatorMarketfactFileTO(){
		ValuatorMarketfactFileTO filter=marketFactMgmtTO.getValuatorMarketfactFileTO();
		try {
			List<ValuatorMarketfactFileTO> lstVmfTO=marketfactFacade.searchValuatorMarketfactFileFacade(filter);
			if(Validations.validateIsNotNullAndNotEmpty(lstVmfTO))
				marketFactMgmtTO.setLstValuatorMarketfactFileTO(new GenericDataModel<ValuatorMarketfactFileTO>(lstVmfTO));
			else marketFactMgmtTO.setLstValuatorMarketfactFileTO(new GenericDataModel<ValuatorMarketfactFileTO>());
			
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	public void cleanSearch(){
		defaultView();
	}
	public String getParameterName(Integer paramterPk){
		return parameterDescription.get(paramterPk);
	}

	public void fileUploadHandler(FileUploadEvent event) {
		MarketfactValidationTO marketfactValidation=null;
		try {
			if(Validations.validateIsNull(marketFactMgmtTO.getFileSource())){
				JSFUtilities.addContextMessage("frmFileMarketFactRegister:cboFileSource", 
						FacesMessage.SEVERITY_ERROR,
					       PropertiesUtilities
					         .getMessage(PropertiesConstants.VALUATOR_CORE_MARKETFACT_FILE_EMPTY_FIELDS),
					       PropertiesUtilities
					         .getMessage(PropertiesConstants.VALUATOR_CORE_MARKETFACT_FILE_EMPTY_FIELDS));
				return;
			}
			String fDisplayName = fUploadValidateFile(event.getFile(), null, null,fUploadFileTypes);
			if (fDisplayName != null) {
				setSelectedFileType(Boolean.FALSE);
				tmpMarketfactFile= new ValuatorMarketfactFileTO();
				tmpMarketfactFile.setMarketfactType(marketFactMgmtTO.getMarketfactTypePk());
				tmpMarketfactFile.setFileName(fDisplayName);	
				byte[] temp=event.getFile().getContents();
				File file;
				file = File.createTempFile("tempMcnfile", ".tmp");
				FileUtils.writeByteArrayToFile(file, temp);
				formatFile(file);         					//remueve las comillas de los datos
				tmpMarketfactFile.setTempMarketfactFile(file);
				tmpMarketfactFile.setMarketfactFile(FileUtils.readFileToByteArray(file));// asigna byte[] formateado(sin comillas)				
				
				marketfactValidation = validateFile(marketFactMgmtTO.getMarketfactTypePk());
				
				if(marketfactValidation.getRejectedMarketfactfiles()>GeneralConstants.ZERO_VALUE_INT){
					throw new ServiceException(ErrorServiceType.VALUATOR_CORE_MARKETFACT_FILE_FORMAT_INVALID);
				}
				
				Date processDate = marketFactMgmtTO.getProcessDate();
				for(ValuatorMarketfactMechanismType marketFactData: marketfactValidation.getLstMarketfactFIles()){
					Date informationDate = marketFactData.getInformationDate();
					if(!informationDate.equals(processDate)){
						throw new ServiceException(ErrorServiceType.VALUATOR_CORE_MARKETFACT_FILE_DATE_INVALID,
												   new Object[]{CommonsUtilities.convertDatetoString(informationDate),
																CommonsUtilities.convertDatetoString(processDate)});
					}
				}
				
				createTemMarketfactFile(marketfactValidation);
				setTmpFileSource(marketFactMgmtTO.getFileSource());
				loadListErrors(null);
				
			}
			} catch (ServiceException e) {
				if(e.getErrorService() != null){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
										PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
				}
				JSFUtilities.showSimpleValidationDialog();
				loadListErrors(marketfactValidation.getLstErrors());
				marketFactMgmtTO.setMarketfactTypePk(null);
			}catch (BeanIOConfigurationException
					| IllegalArgumentException
					| ParserConfigurationException | IOException e) {
				e.printStackTrace();
				marketFactMgmtTO.setMarketfactTypePk(null);
			}
	}
	
	public MarketfactValidationTO validateFile(Integer fileType) throws BeanIOConfigurationException,
		IllegalArgumentException, ParserConfigurationException, IOException, ServiceException{
		tmpMarketfactFile.setRootTag(NegotiationConstant.MARKETFACT_ROOT_TAG);
		tmpMarketfactFile.setStreamFileDir(streamFile(fileType));
		return NegotiationUtils.validateMarketfactFile(tmpMarketfactFile);
	}
	public void formatFile(File file){
		String fileString;
		try {
			fileString = FileUtils.readFileToString(file);
			fileString=fileString.replaceAll(QUOTATION_MARKS, GeneralConstants.EMPTY_STRING);
			FileUtils.writeStringToFile(file, fileString);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public void createTemMarketfactFile(MarketfactValidationTO marketfactValidation){
		tmpMarketfactFile.setRowQuantity(Long.valueOf(marketfactValidation.getTotalCount()));
		tmpMarketfactFile.setFileState(ValuatorMarkFactFileType.PENDING.getCode());
		tmpMarketfactFile.setProcessType(ValuatorMarkFactFileType.MANUAL.getCode());
		tmpMarketfactFile.setMarketfactType(marketFactMgmtTO.getMarketfactTypePk());
		tmpMarketfactFile.setFileSource(marketFactMgmtTO.getFileSource());
		tmpMarketfactFile.setRegistryDate(CommonsUtilities.currentDateTime());
		tmpMarketfactFile.setProcessDate(marketFactMgmtTO.getProcessDate());
		/**    ADD TO LIST  */
		lstTmpMarketfactFile.add(tmpMarketfactFile);
		mapMarketfactType.put(tmpMarketfactFile.getMarketfactType(), tmpMarketfactFile.getMarketfactType());
		marketFactMgmtTO.setLstRegisterVMarketfactFileTO( new GenericDataModel<ValuatorMarketfactFileTO>(lstTmpMarketfactFile));
		marketFactMgmtTO.setMarketfactTypePk(null);
	}

	public void removefileHandler() {
		tmpMarketfactFile.setMarketfactFile(null);
		tmpMarketfactFile.setFileName(null);
	}
	public void removeFile(ValuatorMarketfactFileTO tmpMarketfactFile){
		lstTmpMarketfactFile.remove(tmpMarketfactFile);
		mapMarketfactType.remove(tmpMarketfactFile.getMarketfactType());
		marketFactMgmtTO.setLstRegisterVMarketfactFileTO( new GenericDataModel<ValuatorMarketfactFileTO>(lstTmpMarketfactFile));
		marketFactMgmtTO.setMarketfactTypePk(null);
		tmpMarketfactFile.setFileName(null);
	}
	
	
	public StreamedContent getMarketfactStreamContent(ValuatorMarketfactFileTO marketfactFile) throws IOException{
		if(marketfactFile!=null){
			InputStream inputStream = new ByteArrayInputStream(marketfactFile.getMarketfactFile());
			StreamedContent streamedContentFile = new DefaultStreamedContent(inputStream,null, marketfactFile.getFileName());
			inputStream.close();
			return streamedContentFile;
		}
		return null;
	}
	public void noChangeFileSource(){
		marketFactMgmtTO.setFileSource(getTmpFileSource());
	}
	public void changeFileSource(){
		for(int i=0; i<lstTmpMarketfactFile.size();i++ ){
			ValuatorMarketfactFileTO obj=lstTmpMarketfactFile.get(i);			
			obj.setFileSource(marketFactMgmtTO.getFileSource());
			lstTmpMarketfactFile.set(i, obj);
		}
	}
	public void beforechangeFileSource(){
		if(lstTmpMarketfactFile.size()>GeneralConstants.ZERO_VALUE_INT){
			String messageproperties=PropertiesConstants.VALUATOR_CORE_MARKETFACT_CHANGE_FILE_SOURCE_CONFIRM;
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),PropertiesUtilities.getMessage(messageproperties));
				JSFUtilities.executeJavascriptFunction("PF('cnfwChangeFileSource').show()");
		}
	}
	public void chooseMarketfactType(){
		try{
			validateExistMarketfactType(marketFactMgmtTO.getMarketfactTypePk());
			setSelectedFileType(Boolean.TRUE);
		}catch(ServiceException e){
			if(e.getErrorService() != null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
			}
			JSFUtilities.showSimpleValidationDialog();
			marketFactMgmtTO.setMarketfactTypePk(null);
		}	
	}
	public void chooseLoadingType(){
		loadCboMarketfactType();
	}
	public void validateExistMarketfactType(Integer marketfactType) throws ServiceException{
		if(Validations.validateIsNotNull(mapMarketfactType.get(marketfactType))){
			throw new ServiceException(ErrorServiceType.VALUATOR_CORE_MARKETFACT_FILE_EXIST);
		}
	}
	
	public void beforeRegisterMarketfactFiles(){
		if(lstTmpMarketfactFile.size()==marketFactMgmtTO.getLstMarketfactType().size()){
			String messageproperties=PropertiesConstants.VALUATOR_CORE_MARKETFACT_REGISTER_CONFIRM;
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),PropertiesUtilities.getMessage(messageproperties));
				JSFUtilities.executeJavascriptFunction("PF('cnfwMarketfactRegister').show()");
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.VALUATOR_CORE_MARKETFACT_FILE_INCOMPLETE));
			JSFUtilities.showSimpleValidationDialog();
		}
					
	}
	
	@LoggerAuditWeb
	public void saveMarketfactFiles(){
		List<ValuatorMarketfactFile> listValuatorMarketfactFile=getListValuatorMarketfactFile(lstTmpMarketfactFile);
		try {
			boolean workingDay = marketFactMgmtTO.getFileLoadingType().equals(ValuatorMarkFactFileType.WORKING_DAY.getCode()); 
			marketfactFacade.saveValuatorMarketfactFilesFacade(listValuatorMarketfactFile,workingDay,marketFactMgmtTO.getProcessDate());
			if(workingDay){
				processMarketfactFile();
			}
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.VALUATOR_CORE_MARKETFACT_REGISTER_CONFIRM_OK));
					JSFUtilities.showSimpleEndTransactionDialog(SEARCH_FILES);
		} catch (ServiceException e) {
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
			}
			JSFUtilities.showSimpleEndTransactionDialog(SEARCH_FILES);
		}
	}
	
	public void processMarketfactFile(){
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.VALUATOR_EXECUTION_MARKETFACT_FILE_PROCESS.getCode());
		try {
			Map<String, Object> details = new HashMap<String, Object>();
			details.put(GeneralConstants.REGISTER_DATE, CommonsUtilities.convertDatetoString(getCurrentSystemDate()));
			details.put(GeneralConstants.FILE_SOURCE, tmpMarketfactFile.getFileSource());
			details.put(GeneralConstants.PROCESS_DATE,CommonsUtilities.convertDatetoStringForTemplate(marketFactMgmtTO.getProcessDate(),CommonsUtilities.DATETIME_PATTERN));
			batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(),businessProcess,details);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	public List<ValuatorMarketfactFile> getListValuatorMarketfactFile(List<ValuatorMarketfactFileTO> lstVmfTO){
		List<ValuatorMarketfactFile> listValuatorMarketfactFile = new ArrayList<ValuatorMarketfactFile>();
		for(ValuatorMarketfactFileTO vmfTO : lstVmfTO){
			ValuatorMarketfactFile objVmf=new ValuatorMarketfactFile();
			objVmf.setFileName(vmfTO.getFileName());
			objVmf.setFileState(vmfTO.getFileState());
			objVmf.setProcessType(vmfTO.getProcessType());
			objVmf.setMarketfactFile(vmfTO.getMarketfactFile());
			objVmf.setProcessDate(vmfTO.getProcessDate());
			objVmf.setMarketfactType(vmfTO.getMarketfactType());
			objVmf.setRowQuantity(vmfTO.getRowQuantity());
			objVmf.setFileSource(vmfTO.getFileSource());
			objVmf.setRegistryDate(CommonsUtilities.currentDateTime());
			objVmf.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			
			listValuatorMarketfactFile.add(objVmf);	
		}
		return listValuatorMarketfactFile;
	}
	
	public String streamFile(Integer fileType){
		String Stream=GeneralConstants.EMPTY_STRING;		
		switch(ValuatorMarkFactFileType.get(fileType)){
		case FIXED_FILE			: Stream=NegotiationConstant.BBV_FIXED_FILE_STREAM;break;
		case VARIABLE_FILE		: Stream=NegotiationConstant.BBV_VARIABLE_FILE_STREAM;break;	
		case HISTORY_FILE		: Stream=NegotiationConstant.BBV_HISTORY_FILE_STREAM;break;
		default 					:break;
		}
		return Stream;
	}
	
	public String getfUploadFileTypes() {
		return fUploadFileTypes;
	}

	public void setfUploadFileTypes(String fUploadFileTypes) {
		this.fUploadFileTypes = fUploadFileTypes;
	}

	public Pattern getfUploadFileTypesPattern() {
		return fUploadFileTypesPattern;
	}

	public void setfUploadFileTypesPattern(Pattern fUploadFileTypesPattern) {
		this.fUploadFileTypesPattern = fUploadFileTypesPattern;
	}

	public String getfUploadFileTypesDisplay() {
		return fUploadFileTypesDisplay;
	}

	public void setfUploadFileTypesDisplay(String fUploadFileTypesDisplay) {
		this.fUploadFileTypesDisplay = fUploadFileTypesDisplay;
	}
	
	public MarketFactMgmtTO getMarketFactMgmtTO() {
		return marketFactMgmtTO;
	}
	public void setMarketFactMgmtTO(MarketFactMgmtTO marketFactMgmtTO) {
		this.marketFactMgmtTO = marketFactMgmtTO;
	}

	public Map<Integer, String> getParameterDescription() {
		return parameterDescription;
	}

	public void setParameterDescription(Map<Integer, String> parameterDescription) {
		this.parameterDescription = parameterDescription;
	}
	
	public ValuatorMarketfactFileTO getTmpMarketfactFile() {
		return tmpMarketfactFile;
	}

	public void setTmpMarketfactFile(ValuatorMarketfactFileTO tmpMarketfactFile) {
		this.tmpMarketfactFile = tmpMarketfactFile;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public List<ValuatorMarketfactFileTO> getLstTmpMarketfactFile() {
		return lstTmpMarketfactFile;
	}

	public void setLstTmpMarketfactFile(
			List<ValuatorMarketfactFileTO> lstTmpMarketfactFile) {
		this.lstTmpMarketfactFile = lstTmpMarketfactFile;
	}

	public boolean isSelectedFileType() {
		return isSelectedFileType;
	}

	public void setSelectedFileType(boolean isSelectedFileType) {
		this.isSelectedFileType = isSelectedFileType;
	}

	public Integer getTmpFileSource() {
		return tmpFileSource;
	}

	public void setTmpFileSource(Integer tmpFileSource) {
		this.tmpFileSource = tmpFileSource;
	}
	
}
