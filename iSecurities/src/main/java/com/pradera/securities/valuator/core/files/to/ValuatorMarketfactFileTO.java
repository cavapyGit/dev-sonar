package com.pradera.securities.valuator.core.files.to;

import java.io.File;
import java.io.Serializable;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorMarketfactFileTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class ValuatorMarketfactFileTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id valuator file pk. */
	private Long idValuatorFilePk;
	
	/** The file name. */
	private String fileName;
	
	/** The file state. */
	private Integer fileState;
	
	/** The process type. */
	private Integer processType;
    
    /** The marketfact file. */
    private byte[] marketfactFile;
	
	/** The marketfact type. */
	private Integer marketfactType;
	
	/** The registry date. */
	private Date registryDate;
	
	/** The process date. */
	private Date processDate;
	
	/** The row quantity. */
	private Long rowQuantity;
	
	/** The file source. */
	private Integer fileSource;
	
	/** The temp marketfact file. */
	private File tempMarketfactFile;
	
	/** The root tag. */
	private String rootTag;
	
	/** The stream file dir. */
	private String streamFileDir;
	
	/** The inital date. */
	private Date initalDate;
	
	/** The end date. */
	private Date endDate;
	
	/** The registry user. */
	private String registryUser;
	
	
	/**
	 * Instantiates a new valuator marketfact file to.
	 */
	public ValuatorMarketfactFileTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Instantiates a new valuator marketfact file to.
	 *
	 * @param idValuatorFilePk the id valuator file pk
	 * @param fileName the file name
	 * @param fileState the file state
	 * @param processType the process type
	 * @param marketfactFile the marketfact file
	 * @param marketfactType the marketfact type
	 * @param registryDate the registry date
	 * @param rowQuantity the row quantity
	 * @param fileSource the file source
	 */
	public ValuatorMarketfactFileTO(Long idValuatorFilePk, String fileName,
			Integer fileState, Integer processType, byte[] marketfactFile,
			Integer marketfactType, Date registryDate,Long rowQuantity,Integer fileSource) {
		super();
		this.idValuatorFilePk = idValuatorFilePk;
		this.fileName = fileName;
		this.fileState = fileState;
		this.processType = processType;
		this.marketfactFile = marketfactFile;
		this.marketfactType = marketfactType;
		this.registryDate = registryDate;
		this.rowQuantity=rowQuantity;
		this.fileSource=fileSource;
	}

	/**
	 * Gets the id valuator file pk.
	 *
	 * @return the id valuator file pk
	 */
	public Long getIdValuatorFilePk() {
		return idValuatorFilePk;
	}

	/**
	 * Sets the id valuator file pk.
	 *
	 * @param idValuatorFilePk the new id valuator file pk
	 */
	public void setIdValuatorFilePk(Long idValuatorFilePk) {
		this.idValuatorFilePk = idValuatorFilePk;
	}

	/**
	 * Gets the file name.
	 *
	 * @return the file name
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Sets the file name.
	 *
	 * @param fileName the new file name
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Gets the file state.
	 *
	 * @return the file state
	 */
	public Integer getFileState() {
		return fileState;
	}

	/**
	 * Sets the file state.
	 *
	 * @param fileState the new file state
	 */
	public void setFileState(Integer fileState) {
		this.fileState = fileState;
	}

	/**
	 * Gets the process type.
	 *
	 * @return the process type
	 */
	public Integer getProcessType() {
		return processType;
	}

	/**
	 * Sets the process type.
	 *
	 * @param processType the new process type
	 */
	public void setProcessType(Integer processType) {
		this.processType = processType;
	}

	/**
	 * Gets the marketfact file.
	 *
	 * @return the marketfact file
	 */
	public byte[] getMarketfactFile() {
		return marketfactFile;
	}

	/**
	 * Sets the marketfact file.
	 *
	 * @param marketfactFile the new marketfact file
	 */
	public void setMarketfactFile(byte[] marketfactFile) {
		this.marketfactFile = marketfactFile;
	}

	/**
	 * Gets the marketfact type.
	 *
	 * @return the marketfact type
	 */
	public Integer getMarketfactType() {
		return marketfactType;
	}

	/**
	 * Sets the marketfact type.
	 *
	 * @param marketfactType the new marketfact type
	 */
	public void setMarketfactType(Integer marketfactType) {
		this.marketfactType = marketfactType;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}
	
	/**
	 * Gets the temp marketfact file.
	 *
	 * @return the temp marketfact file
	 */
	public File getTempMarketfactFile() {
		return tempMarketfactFile;
	}

	/**
	 * Sets the temp marketfact file.
	 *
	 * @param tempMarketfactFile the new temp marketfact file
	 */
	public void setTempMarketfactFile(File tempMarketfactFile) {
		this.tempMarketfactFile = tempMarketfactFile;
	}

	/**
	 * Gets the inital date.
	 *
	 * @return the inital date
	 */
	public Date getInitalDate() {
		return initalDate;
	}

	/**
	 * Sets the inital date.
	 *
	 * @param initalDate the new inital date
	 */
	public void setInitalDate(Date initalDate) {
		this.initalDate = initalDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * Gets the row quantity.
	 *
	 * @return the row quantity
	 */
	public Long getRowQuantity() {
		return rowQuantity;
	}

	/**
	 * Sets the row quantity.
	 *
	 * @param rowQuantity the new row quantity
	 */
	public void setRowQuantity(Long rowQuantity) {
		this.rowQuantity = rowQuantity;
	}

	/**
	 * Gets the file source.
	 *
	 * @return the file source
	 */
	public Integer getFileSource() {
		return fileSource;
	}

	/**
	 * Sets the file source.
	 *
	 * @param fileSource the new file source
	 */
	public void setFileSource(Integer fileSource) {
		this.fileSource = fileSource;
	}

	/**
	 * Gets the root tag.
	 *
	 * @return the root tag
	 */
	public String getRootTag() {
		return rootTag;
	}

	/**
	 * Sets the root tag.
	 *
	 * @param rootTag the new root tag
	 */
	public void setRootTag(String rootTag) {
		this.rootTag = rootTag;
	}

	/**
	 * Gets the stream file dir.
	 *
	 * @return the stream file dir
	 */
	public String getStreamFileDir() {
		return streamFileDir;
	}

	/**
	 * Sets the stream file dir.
	 *
	 * @param streamFileDir the new stream file dir
	 */
	public void setStreamFileDir(String streamFileDir) {
		this.streamFileDir = streamFileDir;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the process date.
	 *
	 * @return the process date
	 */
	public Date getProcessDate() {
		return processDate;
	}

	/**
	 * Sets the process date.
	 *
	 * @param processDate the new process date
	 */
	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}
	
	
}
