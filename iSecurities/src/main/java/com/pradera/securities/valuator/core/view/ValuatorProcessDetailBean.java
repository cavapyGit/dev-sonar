package com.pradera.securities.valuator.core.view;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.valuatorprocess.ValuatorProcessExecution;
import com.pradera.model.valuatorprocess.type.ValuatorDetailType;
import com.pradera.securities.valuator.core.facade.ValuatorProcessDetailFacade;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorProcessDetailBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@DepositaryWebBean
public class ValuatorProcessDetailBean extends GenericBaseBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant VIEW_PROCESS_DETAIL. */
	private static final String VIEW_PROCESS_DETAIL = "viewProcessDetail";
	
	/** The Constant SEARCH_PROCESS_DETAIL. */
	private static final String SEARCH_PROCESS_DETAIL = "searchProcessDetail";
	
	/** The valuator process detail facade. */
	@EJB private ValuatorProcessDetailFacade valuatorProcessDetailFacade;
	
	/** The general parameters facade. */
	@EJB private GeneralParametersFacade generalParametersFacade;
	
	/** The valuator process execution. */
	private ValuatorProcessExecution valuatorProcessExecution;
	
	/** The execution monitoring. */
//	private ValuatorNotificationTrack executionMonitoring;
	
	/** The valuator process execution to. */
	private ValuatorProcessExecutionTO valuatorProcessExecutionTO;
	
	/** The valuator process execution selected. */
	private ValuatorProcessExecutionTO valuatorProcessExecutionSelected;
	
	/** The parameter execution type. */
	private Map<Integer, String> parameterExecutionType;
	
	/** The parameter process state. */
	private Map<Integer, String> parameterProcessState;
	
	/** The parameter valuator type. */
	private Map<Integer, String> parameterValuatorType;
	
	/** The parameter detail valuator. */
	private Map<Integer, String> parameterDetailValuator;
	
	/** The parameter motive. */
	private Map<Integer, String> parameterMotive;
	
	/** The date types list search. */
	private List<ParameterTable> dateTypesListSearch;
	
	/** The execution types list search. */
	private List<ParameterTable> executionTypesListSearch;
	
	/** The execution types list view. */
	private List<ParameterTable> executionTypesListView;
	
	/** The process states list search. */
	private List<ParameterTable> processStatesListSearch;
	
	/** The process states list view. */
	private List<ParameterTable> processStatesListView;
	
	/** The valuator types list search. */
	private List<ParameterTable> valuatorTypesListSearch;
	
	/** The valuator types list view. */
	private List<ParameterTable> valuatorTypesListView;
	
	/** The valuator data model. */
	private GenericDataModel<ValuatorProcessExecutionTO> valuatorDataModel;
	
	/** The valuator data model detail. */
	private GenericDataModel<ValuatorExecutionDetailTO> valuatorDataModelDetail;
	
	/** The valuator data model failed. */
	private GenericDataModel<ValuatorExecutionFailedTO> valuatorDataModelFailed;

	/** The user info. */
	@Inject private UserInfo userInfo;

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		loadPriviligies();
		cleanSearchForm();
	}
	
	/**
	 * Load priviligies.
	 */
	private void loadPriviligies() {
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		
		userInfo.setPrivilegeComponent(privilegeComponent);
	}
	
	/**
	 * Clean view.
	 */
	public void cleanView(){
		JSFUtilities.resetViewRoot();
		try {
			createObjectValuatorProcessExecution();
			loadAllExecutionTypes();
			loadAllProcessStates();
			loadAllValuatorTypes();
			loadAllDetailValuators();
			loadAllMotive();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Creates the object valuator process execution.
	 */
	public void createObjectValuatorProcessExecution(){
		valuatorProcessExecution = new ValuatorProcessExecution();
//		executionMonitoring = new ValuatorNotificationTrack();
		
		valuatorDataModelDetail = null;
		valuatorDataModelFailed = null;
	}

	/**
	 * Load all date type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadAllDateType() throws ServiceException {
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.VALUATOR_PROCESS_EXECUTION_DATE_TYPES.getCode());
		List<ParameterTable> list = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		this.setDateTypesListSearch(list);
		if(list.size() == ComponentConstant.ONE){
			valuatorProcessExecutionTO.setDateType(list.get(ComponentConstant.ZERO).getParameterTablePk());
		}
	}
	
	/**
	 * Load all execution types.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadAllExecutionTypes() throws ServiceException{
		parameterExecutionType = new HashMap<>();
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.VALUATOR_PROCESS_EXECUTION_TYPES.getCode());
		List<ParameterTable> list = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		for (ParameterTable parameterTable : list) {
			parameterExecutionType.put(parameterTable.getParameterTablePk(), parameterTable.getDescription());
		}
		if(getViewOperationType() == null)
			this.setExecutionTypesListSearch(list);
		else if(getViewOperationType().equals(ViewOperationsType.DETAIL.getCode()) )
			this.setExecutionTypesListView(list);
	}
	
	/**
	 * Load all process states.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadAllProcessStates() throws ServiceException{
		parameterProcessState = new HashMap<>();
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.VALUATOR_PROCESS_EXECUTION_STATES.getCode());
		List<ParameterTable> list = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		for (ParameterTable parameterTable : list) {
			parameterProcessState.put(parameterTable.getParameterTablePk(), parameterTable.getDescription());
		}
		if(getViewOperationType() == null)
			this.setProcessStatesListSearch(list);
		else if(getViewOperationType().equals(ViewOperationsType.DETAIL.getCode()) )
			this.setProcessStatesListView(list);
	}

	/**
	 * Load all valuator types.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadAllValuatorTypes() throws ServiceException{
		parameterValuatorType = new HashMap<>();
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.VALUATOR_PROCESS_EXECUTION_VALUATOR_TYPES.getCode());
		List<ParameterTable> list = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		for (ParameterTable parameterTable : list) {
			parameterValuatorType.put(parameterTable.getParameterTablePk(), parameterTable.getDescription());
		}
		if(getViewOperationType() == null)
			this.setValuatorTypesListSearch(list);
		else if(getViewOperationType().equals(ViewOperationsType.DETAIL.getCode()) )
			this.setValuatorTypesListView(list);
	}

	/**
	 * Load all detail valuators.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadAllDetailValuators() throws ServiceException{
		parameterDetailValuator = new HashMap<>();
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.VALUATOR_PROCESS_EXECUTION_DETAIL_OBJECT.getCode());
		List<ParameterTable> list = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		for (ParameterTable parameterTable : list) {
			parameterDetailValuator.put(parameterTable.getParameterTablePk(), parameterTable.getDescription());
		}
	}
	
	/**
	 * Load all motive.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadAllMotive() throws ServiceException {
		parameterMotive = new HashMap<>();
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.VALUATOR_EXECUTION_FAILED_MOTIVE.getCode());
		List<ParameterTable> list = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		for (ParameterTable parameterTable : list) {
			parameterMotive.put(parameterTable.getParameterTablePk(), parameterTable.getDescription());
		}
	}
	
	/**
	 * Clean search form.
	 */
	public void cleanSearchForm() {
		JSFUtilities.resetViewRoot();
		try {
			setViewOperationType(null);
			createObjectValuatorExecutionTO();
			loadAllDateType();
			loadAllExecutionTypes();
			loadAllProcessStates();
			loadAllValuatorTypes();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Creates the object valuator execution to.
	 */
	public void createObjectValuatorExecutionTO(){
		valuatorProcessExecutionTO = new ValuatorProcessExecutionTO();
		valuatorProcessExecutionTO.setInitialDate(CommonsUtilities.currentDateTime());
		valuatorProcessExecutionTO.setFinalDate(CommonsUtilities.currentDateTime());
		
		valuatorDataModel = null;
		valuatorProcessExecutionSelected = null;
	}
	
	/**
	 * Back form.
	 *
	 * @return the string
	 */
	public String backForm(){
		setViewOperationType(null);
		return SEARCH_PROCESS_DETAIL;
	}

	/**
	 * View process execution.
	 *
	 * @param selectedTO the selected to
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String viewProcessExecution(ValuatorProcessExecutionTO selectedTO) throws ServiceException {
		if(selectedTO!=null){
			setViewOperationType(ViewOperationsType.DETAIL.getCode());
			cleanView();
			valuatorProcessExecution = valuatorProcessDetailFacade.viewProcessExecution(selectedTO.getIdProcessExecutionPk());
//			executionMonitoring = valuatorProcessDetailFacade.getExecutionMonitor(selectedTO.getIdProcessExecutionPk());
			
			searchValuatorExecutionDetail();
			if(valuatorDataModelDetail.getDataList() != null && !valuatorDataModelDetail.getDataList().isEmpty()){
				Long idExecutionDetailPk = null;
				for (ValuatorExecutionDetailTO to : valuatorDataModelDetail.getDataList()) {
					if(to.getDetailType().equals(ValuatorDetailType.BALANCE.getCode())){
						idExecutionDetailPk = to.getIdExecutionDetailPk();
						searchValuatorExecutionFailed(idExecutionDetailPk);
						break;
					}
				}
			}
			
			return VIEW_PROCESS_DETAIL;
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getExceptionMessage(
							ErrorServiceType.VALUATOR_SIMULATION_UNSELECTED.getMessage()));
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}
	}
	
	/**
	 * Search process executions.
	 */
	@LoggerAuditWeb
	public void searchProcessExecutions(){
		List<ValuatorProcessExecutionTO> list = null;
		try {
			list = valuatorProcessDetailFacade.getValuatorProcessFacade(valuatorProcessExecutionTO);
		} catch (ServiceException ex) {
			if(ex.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(
						GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getExceptionMessage(ex.getMessage()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
		if(list==null){
			valuatorDataModel = new GenericDataModel<ValuatorProcessExecutionTO>();
		}else{
			valuatorDataModel = new GenericDataModel<ValuatorProcessExecutionTO>(list);
		}
	}
	
	/**
	 * Search valuator execution detail.
	 */
	public void searchValuatorExecutionDetail(){
		List<ValuatorExecutionDetailTO> list = null;
		try {
			list = valuatorProcessDetailFacade.getValuatorExecutionDetail(valuatorProcessExecution.getIdProcessExecutionPk());
		} catch (ServiceException ex) {
			if(ex.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(
						GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getExceptionMessage(ex.getMessage()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
		if(list==null){
			valuatorDataModelDetail = new GenericDataModel<ValuatorExecutionDetailTO>();
		}else{
			valuatorDataModelDetail = new GenericDataModel<ValuatorExecutionDetailTO>(list);
		}
	}
	
	/**
	 * Search valuator execution failed.
	 *
	 * @param idExecutionDetailPk the id execution detail pk
	 */
	public void searchValuatorExecutionFailed(Long idExecutionDetailPk){
		List<ValuatorExecutionFailedTO> list = null;
		try {
			list = valuatorProcessDetailFacade.getValuatorExecutionFailed(idExecutionDetailPk);
		} catch (ServiceException ex) {
			if(ex.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(
						GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getExceptionMessage(ex.getMessage()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
		if(list==null){
			valuatorDataModelFailed = new GenericDataModel<ValuatorExecutionFailedTO>();
		}else{
			valuatorDataModelFailed = new GenericDataModel<ValuatorExecutionFailedTO>(list);
		}
	}

	/**
	 * Gets the valuator process detail facade.
	 *
	 * @return the valuator process detail facade
	 */
	public ValuatorProcessDetailFacade getValuatorProcessDetailFacade() {
		return valuatorProcessDetailFacade;
	}

	/**
	 * Sets the valuator process detail facade.
	 *
	 * @param valuatorProcessDetailFacade the new valuator process detail facade
	 */
	public void setValuatorProcessDetailFacade(
			ValuatorProcessDetailFacade valuatorProcessDetailFacade) {
		this.valuatorProcessDetailFacade = valuatorProcessDetailFacade;
	}

	/**
	 * Gets the general parameters facade.
	 *
	 * @return the general parameters facade
	 */
	public GeneralParametersFacade getGeneralParametersFacade() {
		return generalParametersFacade;
	}

	/**
	 * Sets the general parameters facade.
	 *
	 * @param generalParametersFacade the new general parameters facade
	 */
	public void setGeneralParametersFacade(
			GeneralParametersFacade generalParametersFacade) {
		this.generalParametersFacade = generalParametersFacade;
	}

	/**
	 * Gets the valuator process execution.
	 *
	 * @return the valuator process execution
	 */
	public ValuatorProcessExecution getValuatorProcessExecution() {
		return valuatorProcessExecution;
	}

	/**
	 * Sets the valuator process execution.
	 *
	 * @param valuatorProcessExecution the new valuator process execution
	 */
	public void setValuatorProcessExecution(
			ValuatorProcessExecution valuatorProcessExecution) {
		this.valuatorProcessExecution = valuatorProcessExecution;
	}

	/**
	 * Gets the valuator process execution to.
	 *
	 * @return the valuator process execution to
	 */
	public ValuatorProcessExecutionTO getValuatorProcessExecutionTO() {
		return valuatorProcessExecutionTO;
	}

	/**
	 * Sets the valuator process execution to.
	 *
	 * @param valuatorProcessExecutionTO the new valuator process execution to
	 */
	public void setValuatorProcessExecutionTO(
			ValuatorProcessExecutionTO valuatorProcessExecutionTO) {
		this.valuatorProcessExecutionTO = valuatorProcessExecutionTO;
	}

	/**
	 * Gets the valuator process execution selected.
	 *
	 * @return the valuator process execution selected
	 */
	public ValuatorProcessExecutionTO getValuatorProcessExecutionSelected() {
		return valuatorProcessExecutionSelected;
	}

	/**
	 * Sets the valuator process execution selected.
	 *
	 * @param valuatorProcessExecutionSelected the new valuator process execution selected
	 */
	public void setValuatorProcessExecutionSelected(
			ValuatorProcessExecutionTO valuatorProcessExecutionSelected) {
		this.valuatorProcessExecutionSelected = valuatorProcessExecutionSelected;
	}

	/**
	 * Gets the parameter execution type.
	 *
	 * @return the parameter execution type
	 */
	public Map<Integer, String> getParameterExecutionType() {
		return parameterExecutionType;
	}

	/**
	 * Sets the parameter execution type.
	 *
	 * @param parameterExecutionType the parameter execution type
	 */
	public void setParameterExecutionType(
			Map<Integer, String> parameterExecutionType) {
		this.parameterExecutionType = parameterExecutionType;
	}

	/**
	 * Gets the parameter process state.
	 *
	 * @return the parameter process state
	 */
	public Map<Integer, String> getParameterProcessState() {
		return parameterProcessState;
	}

	/**
	 * Sets the parameter process state.
	 *
	 * @param parameterProcessState the parameter process state
	 */
	public void setParameterProcessState(Map<Integer, String> parameterProcessState) {
		this.parameterProcessState = parameterProcessState;
	}

	/**
	 * Gets the parameter valuator type.
	 *
	 * @return the parameter valuator type
	 */
	public Map<Integer, String> getParameterValuatorType() {
		return parameterValuatorType;
	}

	/**
	 * Sets the parameter valuator type.
	 *
	 * @param parameterValuatorType the parameter valuator type
	 */
	public void setParameterValuatorType(Map<Integer, String> parameterValuatorType) {
		this.parameterValuatorType = parameterValuatorType;
	}

	/**
	 * Gets the date types list search.
	 *
	 * @return the date types list search
	 */
	public List<ParameterTable> getDateTypesListSearch() {
		return dateTypesListSearch;
	}

	/**
	 * Sets the date types list search.
	 *
	 * @param dateTypesListSearch the new date types list search
	 */
	public void setDateTypesListSearch(List<ParameterTable> dateTypesListSearch) {
		this.dateTypesListSearch = dateTypesListSearch;
	}

	/**
	 * Gets the execution types list search.
	 *
	 * @return the execution types list search
	 */
	public List<ParameterTable> getExecutionTypesListSearch() {
		return executionTypesListSearch;
	}

	/**
	 * Sets the execution types list search.
	 *
	 * @param executionTypesListSearch the new execution types list search
	 */
	public void setExecutionTypesListSearch(
			List<ParameterTable> executionTypesListSearch) {
		this.executionTypesListSearch = executionTypesListSearch;
	}

	/**
	 * Gets the execution types list view.
	 *
	 * @return the execution types list view
	 */
	public List<ParameterTable> getExecutionTypesListView() {
		return executionTypesListView;
	}

	/**
	 * Sets the execution types list view.
	 *
	 * @param executionTypesListView the new execution types list view
	 */
	public void setExecutionTypesListView(
			List<ParameterTable> executionTypesListView) {
		this.executionTypesListView = executionTypesListView;
	}

	/**
	 * Gets the process states list search.
	 *
	 * @return the process states list search
	 */
	public List<ParameterTable> getProcessStatesListSearch() {
		return processStatesListSearch;
	}

	/**
	 * Sets the process states list search.
	 *
	 * @param processStatesListSearch the new process states list search
	 */
	public void setProcessStatesListSearch(
			List<ParameterTable> processStatesListSearch) {
		this.processStatesListSearch = processStatesListSearch;
	}

	/**
	 * Gets the process states list view.
	 *
	 * @return the process states list view
	 */
	public List<ParameterTable> getProcessStatesListView() {
		return processStatesListView;
	}

	/**
	 * Sets the process states list view.
	 *
	 * @param processStatesListView the new process states list view
	 */
	public void setProcessStatesListView(List<ParameterTable> processStatesListView) {
		this.processStatesListView = processStatesListView;
	}

	/**
	 * Gets the valuator types list search.
	 *
	 * @return the valuator types list search
	 */
	public List<ParameterTable> getValuatorTypesListSearch() {
		return valuatorTypesListSearch;
	}

	/**
	 * Sets the valuator types list search.
	 *
	 * @param valuatorTypesListSearch the new valuator types list search
	 */
	public void setValuatorTypesListSearch(
			List<ParameterTable> valuatorTypesListSearch) {
		this.valuatorTypesListSearch = valuatorTypesListSearch;
	}

	/**
	 * Gets the valuator types list view.
	 *
	 * @return the valuator types list view
	 */
	public List<ParameterTable> getValuatorTypesListView() {
		return valuatorTypesListView;
	}

	/**
	 * Sets the valuator types list view.
	 *
	 * @param valuatorTypesListView the new valuator types list view
	 */
	public void setValuatorTypesListView(List<ParameterTable> valuatorTypesListView) {
		this.valuatorTypesListView = valuatorTypesListView;
	}

	/**
	 * Gets the valuator data model.
	 *
	 * @return the valuator data model
	 */
	public GenericDataModel<ValuatorProcessExecutionTO> getValuatorDataModel() {
		return valuatorDataModel;
	}

	/**
	 * Sets the valuator data model.
	 *
	 * @param valuatorDataModel the new valuator data model
	 */
	public void setValuatorDataModel(
			GenericDataModel<ValuatorProcessExecutionTO> valuatorDataModel) {
		this.valuatorDataModel = valuatorDataModel;
	}

	/**
	 * Gets the valuator data model detail.
	 *
	 * @return the valuator data model detail
	 */
	public GenericDataModel<ValuatorExecutionDetailTO> getValuatorDataModelDetail() {
		return valuatorDataModelDetail;
	}

	/**
	 * Sets the valuator data model detail.
	 *
	 * @param valuatorDataModelDetail the new valuator data model detail
	 */
	public void setValuatorDataModelDetail(
			GenericDataModel<ValuatorExecutionDetailTO> valuatorDataModelDetail) {
		this.valuatorDataModelDetail = valuatorDataModelDetail;
	}

	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * Gets the parameter detail valuator.
	 *
	 * @return the parameter detail valuator
	 */
	public Map<Integer, String> getParameterDetailValuator() {
		return parameterDetailValuator;
	}

	/**
	 * Sets the parameter detail valuator.
	 *
	 * @param parameterDetailValuator the parameter detail valuator
	 */
	public void setParameterDetailValuator(
			Map<Integer, String> parameterDetailValuator) {
		this.parameterDetailValuator = parameterDetailValuator;
	}

	/**
	 * Gets the valuator data model failed.
	 *
	 * @return the valuator data model failed
	 */
	public GenericDataModel<ValuatorExecutionFailedTO> getValuatorDataModelFailed() {
		return valuatorDataModelFailed;
	}

	/**
	 * Sets the valuator data model failed.
	 *
	 * @param valuatorDataModelFailed the new valuator data model failed
	 */
	public void setValuatorDataModelFailed(
			GenericDataModel<ValuatorExecutionFailedTO> valuatorDataModelFailed) {
		this.valuatorDataModelFailed = valuatorDataModelFailed;
	}

	/**
	 * Gets the parameter motive.
	 *
	 * @return the parameter motive
	 */
	public Map<Integer, String> getParameterMotive() {
		return parameterMotive;
	}

	/**
	 * Sets the parameter motive.
	 *
	 * @param parameterMotive the parameter motive
	 */
	public void setParameterMotive(Map<Integer, String> parameterMotive) {
		this.parameterMotive = parameterMotive;
	}

	/**
	 * Gets the execution monitoring.
	 *
	 * @return the execution monitoring
	 */
//	public ValuatorNotificationTrack getExecutionMonitoring() {
//		return executionMonitoring;
//	}
//
//	/**
//	 * Sets the execution monitoring.
//	 *
//	 * @param executionMonitoring the new execution monitoring
//	 */
//	public void setExecutionMonitoring(ValuatorNotificationTrack executionMonitoring) {
//		this.executionMonitoring = executionMonitoring;
//	}
}