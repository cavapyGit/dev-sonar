package com.pradera.securities.valuator.core.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.configuration.ValuatorSetup;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.valuatorprocess.ValuatorProcessExecution;
import com.pradera.model.valuatorprocess.ValuatorProcessSetup;
import com.pradera.securities.issuancesecuritie.util.PropertiesConstants;
import com.pradera.securities.valuator.core.facade.ValuatorSecuritiesFacade;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorProcessBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@DepositaryWebBean
public class ValuatorProcessBean extends GenericBaseBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The Constant SEARCH_PROCESS_DETAIL. */
	private static final String SEARCH_PROCESS_DETAIL = "searchProcessDetail";
	
	/** The valuator setup. */
	@Inject @ValuatorSetup
	private ValuatorProcessSetup valuatorSetup;
	
	/** The valuator execution. */
	private ValuatorCoreProcessTO valuatorExecution;
	
	/** The valuator execution test. */
	private ValuatorCoreProcessTO valuatorExecutionTest;
	
	/** The valuator facade. */
	@EJB private ValuatorSecuritiesFacade valuatorFacade;
	
	/** The batch process service facade. */
	@EJB private BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The accounts facade. */
	@EJB private AccountsFacade accountsFacade;
	
	/** The user info. */
	@Inject private UserInfo userInfo;
	
	/** The lst participants. */
	private List<Participant> lstParticipants;
	
	/** The partic selected. */
	private List<Participant> particSelected;
	
	/** The last execution. */
	private ValuatorProcessExecution lastExecution;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		valuatorExecution = new ValuatorCoreProcessTO();
		valuatorExecution.setExecutionHour(valuatorSetup.getExecutionHour());
		try {
			loadParticipants();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Load participants.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadParticipants() throws ServiceException{
		// TODO Auto-generated method stub
		lstParticipants = accountsFacade.getParticipantsOnPortFolly();
	}

	/**
	 * Gets the countdown hour.
	 *
	 * @return the countdown hour
	 */
	public String getCountdownHour(){
		Date hour = CommonsUtilities.currentDateTime();
		Date executionHour = valuatorExecution.getExecutionHour();
		return CommonsUtilities.getHoursMinutSecondsBetween(executionHour, hour);
	}
	
	/**
	 * New execution valuator.
	 */
	public void newExecutionValuator(){
		valuatorExecutionTest = new ValuatorCoreProcessTO();
		valuatorExecutionTest.setEmail(userInfo.getUserAccountSession().getEmail());
		valuatorExecutionTest.setHolder(new Holder());
		valuatorExecutionTest.setValuatorDate(CommonsUtilities.currentDateTime());
		valuatorExecutionTest.setIpAddress(JSFUtilities.getRemoteIpAddress());
	}
	
	/**
	 * Clean entities.
	 */
	public void cleanEntities(){
		valuatorExecutionTest.setHolder(new Holder());
		valuatorExecutionTest.setParticipantPk(null);
		valuatorExecutionTest.setSecurityCode(null);
		valuatorExecutionTest.setExecutionDetailType(null);
		valuatorExecutionTest.setExecutionPk(null);
	}
	
	/**
	 * Before valuator execution.
	 */
	public void beforeValuatorExecution(){
		
		if(valuatorExecutionTest.getExecutionCriteria().equals(2)){
			if(valuatorExecutionTest.getHolder().getIdHolderPk()==null){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage("lbl.header.alert.warning"), 
						 						 PropertiesUtilities.getMessage("msg.valuator.simulation.validation.cui.not.found"));
				JSFUtilities.showRequiredValidationDialog();
				return;
			}
		}
		JSFUtilities.executeJavascriptFunction("PF('cnfwValuatorExecution').show()");
	}
	
	/**
	 * Run valuator execution.
	 */
	@LoggerAuditWeb
	public void runValuatorExecution(){
		ValuatorCoreProcessTO valuatorTO = valuatorExecutionTest; 
		String securityCode = valuatorTO.getSecurityCode();
		List<Long> participantPk  = valuatorTO.getParticSelected();
		Long holderPk		= valuatorTO.getHolder().getIdHolderPk();
		Long executionPk = valuatorTO.getExecutionPk();
		Integer valDetaiType=valuatorTO.getExecutionDetailType();
		Integer instrumentType = valuatorTO.getInstrumentType();
		String email = valuatorTO.getEmail();
		String userName = userInfo.getUserAccountSession().getUserName();
		
		Map<String, Object> details = new HashMap<String, Object>();
//		if(instrumentType!=null){
//			details.put(ValuatorConstants.PARAM_VALUATOR_INSTRUMENT, instrumentType);
//		}
//		details.put(ValuatorConstants.PARAM_VALUATOR_CURRENCY, CurrencyType.PYG.getCode());
//		details.put(ValuatorConstants.PARAM_VALUATOR_DATE, CommonsUtilities.convertDatetoStringForTemplate(valuatorTO.getValuatorDate(),CommonsUtilities.DATETIME_PATTERN));
//		details.put(ValuatorConstants.PARAM_USER_EMAIL, email);
//		details.put(ValuatorConstants.PARAM_USER_NAME, userName);
//		details.put(ValuatorConstants.PARAM_VALUATOR_EXECUTION_TYPE, ValuatorExecutionType.MANUAL.getCode());

		String keyMessage = null;
		Object[] parameter = null;
		
		if(securityCode!=null){
//			details.put(ValuatorConstants.PARAM_SECURITY_CODE, securityCode);
			keyMessage = PropertiesConstants.VALUATOR_EXECUTOR_PROCESS_MESSAGE_OK;
			parameter = new Object[]{securityCode};
		}else if(participantPk!=null && !participantPk.isEmpty()){
//			details.put(ValuatorConstants.PARAM_PARTICIPANT_CODE, StringUtils.join(participantPk,","));
			keyMessage = PropertiesConstants.VALUATOR_EXECUTOR_PROCESS_MESSAGE_OK_PARTICIPANT;
			parameter = new Object[]{StringUtils.join(participantPk,",")}; 
		}else if(holderPk!=null){
//			details.put(ValuatorConstants.PARAM_HOLDER_CODE, holderPk);
			keyMessage = PropertiesConstants.VALUATOR_EXECUTOR_PROCESS_MESSAGE_OK_CUI;
			parameter = new Object[]{holderPk};
		}else{
//			details.put(ValuatorConstants.PARAM_INCONSISTENCE_CODE, BooleanType.YES.getCode());
//			details.put(ValuatorConstants.PARAM_VALUATOR_PROCESS_PK, executionPk);
//			details.put(ValuatorConstants.PARAM_VALUATOR_DETAIL_TYPE, valDetaiType);
			keyMessage = PropertiesConstants.VALUATOR_EXECUTOR_PROCESS_MESSAGE_OK_INCONSISTENCE;
			parameter = new Object[]{executionPk};
		}
		
		try {
			valuatorFacade.registerBatch(details);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(keyMessage,parameter));
		JSFUtilities.showSimpleEndTransactionDialog("searchProcess");
	}
	
	/**
	 * Redirect to search process detail.
	 *
	 * @return the string
	 */
	public String redirectToSearchProcessDetail(){		
		return SEARCH_PROCESS_DETAIL;
	}
	
	/**
	 * Select all participant.
	 */
	public void selectAllParticipant(){
		valuatorExecutionTest.setParticSelected(new ArrayList<Long>());
		if(valuatorExecutionTest.isAllParticipant()){
			for(Participant participant: lstParticipants){
				valuatorExecutionTest.getParticSelected().add(participant.getIdParticipantPk());
			}
		}
	}
	
	/**
	 * Grabar proceso.
	 */
	public void grabarProceso(){
//		try {
////			valuatorFacade.generateValuatorCode(null, null, null);
//		} catch (ServiceException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} finally {
//		}
	}

	/**
	 * Gets the valuator execution.
	 *
	 * @return the valuator execution
	 */
	public ValuatorCoreProcessTO getValuatorExecution() {
		return valuatorExecution;
	}

	/**
	 * Sets the valuator execution.
	 *
	 * @param valuatorExecution the new valuator execution
	 */
	public void setValuatorExecution(ValuatorCoreProcessTO valuatorExecution) {
		this.valuatorExecution = valuatorExecution;
	}

	/**
	 * Gets the valuator execution test.
	 *
	 * @return the valuator execution test
	 */
	public ValuatorCoreProcessTO getValuatorExecutionTest() {
		return valuatorExecutionTest;
	}

	/**
	 * Sets the valuator execution test.
	 *
	 * @param valuatorExecutionTest the new valuator execution test
	 */
	public void setValuatorExecutionTest(ValuatorCoreProcessTO valuatorExecutionTest) {
		this.valuatorExecutionTest = valuatorExecutionTest;
	}

	/**
	 * Gets the lst participants.
	 *
	 * @return the lst participants
	 */
	public List<Participant> getLstParticipants() {
		return lstParticipants;
	}

	/**
	 * Sets the lst participants.
	 *
	 * @param lstParticipants the new lst participants
	 */
	public void setLstParticipants(List<Participant> lstParticipants) {
		this.lstParticipants = lstParticipants;
	}

	/**
	 * Gets the partic selected.
	 *
	 * @return the partic selected
	 */
	public List<Participant> getParticSelected() {
		return particSelected;
	}

	/**
	 * Sets the partic selected.
	 *
	 * @param particSelected the new partic selected
	 */
	public void setParticSelected(List<Participant> particSelected) {
		this.particSelected = particSelected;
	}

	/**
	 * Gets the last execution.
	 *
	 * @return the last execution
	 */
	public ValuatorProcessExecution getLastExecution() {
		return lastExecution;
	}

	/**
	 * Sets the last execution.
	 *
	 * @param lastExecution the new last execution
	 */
	public void setLastExecution(ValuatorProcessExecution lastExecution) {
		this.lastExecution = lastExecution;
	}
}