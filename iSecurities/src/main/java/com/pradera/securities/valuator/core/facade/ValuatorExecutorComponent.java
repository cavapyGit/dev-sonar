package com.pradera.securities.valuator.core.facade;

import javax.ejb.Singleton;
import javax.interceptor.Interceptors;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class ValuatorSecuritiesFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/12/2014
 */
@Singleton
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ValuatorExecutorComponent {

//	/**
//	 * 
//	 */
//	@EJB private ValuatorSecuritiesFacade valuatorSecuritiesFacade;
//	@EJB private NotificationServiceFacade notificationFacade;
//	@EJB private ValuatorLiveOperationService valuatorLiveOperation;
//	/**USER INFO*/
//	private String userSession = "ADMIN";
//
//	@SuppressWarnings("unchecked")
//	public void executeValuatorProcess(ProcessLogger processLogger) {
//		// TODO Auto-generated method stub
//		/**PARAMETERS TO EXECUTE VALUATOR PROCESS*/
//		List<String> securities = null; 
//		List<Long> participants = null, accounts = null;
//		/**CURRENCY EXECUTION BY DEFAULT IT'S BOB*/
//		Integer currency = null;
//		/**VALUATOR DATE BY DEFAULT IT'S THE CURRENT DATE*/
//		Date valuatorDate = null;
//		/**INSTRUMENT TYPE*/
//		Integer instrumentType = null;
//		/**USER EMAIL*/
//		String userEmail = null, userName = null;		
//		
//		List<ProcessLoggerDetail> lstProcessLoggerDetails = processLogger.getProcessLoggerDetails();
//		for (ProcessLoggerDetail objDetail: lstProcessLoggerDetails){
//			if (StringUtils.equalsIgnoreCase(ValuatorConstants.PARAM_SECURITY_CODE, objDetail.getParameterName())) {
//				securities = (List<String>) populateParametersValue(objDetail.getParameterValue());
//			}else if(StringUtils.equalsIgnoreCase(ValuatorConstants.PARAM_PARTICIPANT, objDetail.getParameterName())) {
//				participants = (List<Long>) populateParametersValue(objDetail.getParameterValue());
//			}else if(StringUtils.equalsIgnoreCase(ValuatorConstants.PARAM_HOLDER_ACCOUNT, objDetail.getParameterName())) {
//				accounts = (List<Long>) populateParametersValue(objDetail.getParameterValue());
//			}else if(StringUtils.equalsIgnoreCase(ValuatorConstants.PARAM_VALUATOR_CURRENCY, objDetail.getParameterName())) {
//				currency = new Integer(objDetail.getParameterValue());
//			}else if(StringUtils.equalsIgnoreCase(ValuatorConstants.PARAM_VALUATOR_INSTRUMENT, objDetail.getParameterName())) {
//				instrumentType = new Integer(objDetail.getParameterValue());
//			}else if(StringUtils.equalsIgnoreCase(ValuatorConstants.PARAM_VALUATOR_DATE, objDetail.getParameterName())) {
//				valuatorDate = CommonsUtilities.convertStringtoDate(objDetail.getParameterValue());
//			}else if(StringUtils.equalsIgnoreCase(ValuatorConstants.PARAM_USER_EMAIL, objDetail.getParameterName())) {
//				userEmail = objDetail.getParameterValue();
//			}else if(StringUtils.equalsIgnoreCase(ValuatorConstants.PARAM_USER_NAME, objDetail.getParameterName())) {
//				userName = objDetail.getParameterValue();
//			}
//		}
//
//		try {
//			/**FIRST WE NEED TO CREATE VALUATOR_PROCESS_EXECUTION*/
//			ValuatorProcessExecution processExecution = getProcessExecution(valuatorDate, ValuatorExecutionType.MANUAL.getCode(),
//																			participants, accounts,securities);
//			processExecution.setValuatorExecutionParameters(getExecutionParameters(processExecution, participants, accounts, securities));
//			/**PERSIST OBJECT ON DATABASE*/
//			valuatorSecuritiesFacade.registerProcessExecution(processExecution);
//			
//			/**SECOND INVOCATE VALUATOR BY SECURITY*/
//			executeValuatorProcess(processExecution,securities, participants, accounts, currency, valuatorDate, instrumentType, 
//								   ValuatorProcessDetailType.SECURITY.getCode());
//			
//			/**THIRD INVOCATE VALUATOR BY OPERATIONS, LIKE REPORTING, BLOCK_OPERATION*/
//			executeValuatorProcess(processExecution,securities, participants, accounts, currency, valuatorDate, instrumentType, 
//					               ValuatorProcessDetailType.OPERATIONS.getCode());
//			
//			/**FOURT INVOCATE VALUATOR BY BALANCES*/
//			executeValuatorProcess(processExecution,securities, participants, accounts, currency, valuatorDate, instrumentType, 
//					               ValuatorProcessDetailType.BALANCE.getCode());
//			
//			
//		} catch (ServiceException e) {
//			e.printStackTrace();
//			
//			/**Config notification to send detail of error at user who send the process*/
//			BusinessProcessType businesProcessType = BusinessProcessType.VALUATOR_EXECUTION_CORE_PROCESS;
//			String message = null;
//			if(e.getMessage()!=null){
//				message = PropertiesUtilities.getExceptionMessage(e.getMessage(),e.getParams(),new Locale("es"));
//			}else{
//				message = "ERROR AL EJECUTAR EL VALORADOR";
//			}
//			BusinessProcess bp = new BusinessProcess();
//			bp.setIdBusinessProcessPk(businesProcessType.getCode());
//			UserAccountSession userSession = new UserAccountSession();
//			userSession.setUserName(userName);
//			userSession.setEmail(userEmail);
//			userSession.setFullName(userName);
//			userSession.setPhoneNumber(userName);			
//			notificationFacade.sendNotificationManual(userName, bp, Arrays.asList(userSession), businesProcessType.getDescription(), message, NotificationType.EMAIL);
//		}
//	}
//	
//	@SuppressWarnings("incomplete-switch")
//	private void executeValuatorProcess(ValuatorProcessExecution procesExecution, List<String> securityCodes, List<Long> participants, 
//										List<Long> accounts, Integer valuatorCurrency, 
//										Date valuatorDate, Integer _instrumentType, Integer execDetType) throws ServiceException{
//		
//		ValuatorExecutionDetail executionDetail = getExecutionDetail(procesExecution,execDetType);
//		valuatorSecuritiesFacade.registerProcessExecutionDetail(executionDetail, participants, securityCodes, accounts);
//		Long executionId = procesExecution.getIdProcessExecutionPk();
//		
//		/**SI LA VALORACION ES POR VALORES*/
//		if(execDetType.equals(ValuatorProcessDetailType.SECURITY.getCode())){
//			for(InstrumentType instrumenType: InstrumentType.list){
//				if(_instrumentType!=null && !_instrumentType.equals(instrumenType.getCode())){
//					continue;
//				}
//				switch(instrumenType){
//					case FIXED_INCOME: 
//							valuatorSecuritiesFacade.executeFixedIncomeValuator(executionId,execDetType,securityCodes, valuatorCurrency, valuatorDate);break;
//					case VARIABLE_INCOME: 
//							valuatorSecuritiesFacade.executeVariableIncomeValuator(executionId,execDetType,securityCodes, valuatorCurrency, valuatorDate);break;
//				}
//			}
//		}else if(execDetType.equals(ValuatorProcessDetailType.BALANCE.getCode())){
//			Integer valuatorType = procesExecution.getValuatorType();
//			valuatorSecuritiesFacade.executeBalanceValuator(executionId,valuatorType, participants, securityCodes, accounts,valuatorDate);
//		}else if(execDetType.equals(ValuatorProcessDetailType.OPERATIONS.getCode())){
//			valuatorLiveOperation.updateReportingMarketPrice(securityCodes, valuatorDate);
//		}
//	}
//	
//	private Object populateParametersValue(String value){
//		List<Object> dataValue = new ArrayList<Object>();
//		if(StringUtils.containsAny(value, ValuatorConstants.STR_COMMA)){
//			for(String valueData: Arrays.asList(value.split(ValuatorConstants.STR_COMMA))){
//				dataValue.add(valueData.trim());
//			}
//		}else{
//			dataValue.addAll(Arrays.asList(value.trim()));
//		}
//		return dataValue;
//	}
//	
//	/**METHOD TO INICIALIZATE VALUATOR_PROCESS_EXECUTION OBJECT*/
//	private ValuatorProcessExecution getProcessExecution(Date processDate ,Integer executorType, 
//														 List<Long> participants, List<Long> accounts, List<String> securities) throws ServiceException{
//		ValuatorProcessOperation processOperation = new ValuatorProcessOperation();
//		processOperation.setRegistryDate(CommonsUtilities.currentDateTime());
//		processOperation.setRegistryUser(userSession);
//		
//		Long operationType = BusinessProcessType.VALUATOR_EXECUTION_CORE_PROCESS.getCode();
//		processOperation.setOperationType(operationType);
//		processOperation.setOperationNumber(valuatorSecuritiesFacade.findValuatorProcessNumber(operationType));
//		
//		/**PUT RELATION OF EXECUTION WITH PROCESS OPERATION*/
//		ValuatorProcessExecution processExecution = new ValuatorProcessExecution();
//		processExecution.setBeginTime(CommonsUtilities.currentDateTime());
//		processExecution.setExecutionType(executorType);
//		processExecution.setProcessDate(processDate);
//		processExecution.setRegistryDate(CommonsUtilities.currentDateTime());
//		processExecution.setRegistryUser(userSession);
//		
//		if((participants==null || participants.isEmpty()) && (accounts==null || accounts.isEmpty()) && (securities==null || securities.isEmpty())){
//			processExecution.setValuatorType(ValuatorType.ENTIRE_PORTFOLIO.getCode());
//		}else{
//			if(participants!=null && !participants.isEmpty()){
//				processExecution.setValuatorType(ValuatorType.BY_PARTICIPANT.getCode());
//			}else if(accounts!=null && !accounts.isEmpty()){
//				processExecution.setValuatorType(ValuatorType.BY_INVESTOR.getCode());
//			}else if(securities!=null && !securities.isEmpty()){
//				processExecution.setValuatorType(ValuatorType.BY_SECURITY.getCode());
//			}
//		}
//		
//		processExecution.setProcessState(ValuatorExecutionStateType.REGISTERED.getCode());
//		processExecution.setValuatorProcessOperation(processOperation);
//		processExecution.setBeginTime(CommonsUtilities.currentDateTime());
//		return processExecution;
//	}
//	
//	/**METHOD TO CREATE VALUATOR_EXECUTION_DETAIL LIKE JAVA OBJECT*/
//	private ValuatorExecutionDetail getExecutionDetail(ValuatorProcessExecution processExecution, Integer processDetailType) throws ServiceException{
//		ValuatorExecutionDetail executionDetail = new ValuatorExecutionDetail();
//		executionDetail.setDetailType(processDetailType);
//		executionDetail.setQuantityFinished(GeneralConstants.ZERO_VALUE_LONG);
//		executionDetail.setQuantityInconsistent(GeneralConstants.ZERO_VALUE_LONG);
//		executionDetail.setQuantityProcess(GeneralConstants.ZERO_VALUE_LONG);
//		executionDetail.setBeginTime(CommonsUtilities.currentDateTime());
//		
//		switch(ValuatorProcessDetailType.get(processDetailType)){
//			case SECURITY: 	 executionDetail.setOrderExecution(1);break;
//			case BALANCE: 	 executionDetail.setOrderExecution(2);break;
//			case OPERATIONS: executionDetail.setOrderExecution(3);break;
//		}
//		
//		executionDetail.setValuatorProcessExecution(processExecution);
//		executionDetail.setRegistryUser(userSession);
//		executionDetail.setRegistryDate(CommonsUtilities.currentDateTime());
//		executionDetail.setProcessState(ValuatorExecutionStateType.REGISTERED.getCode());
//		
//		return executionDetail;
//	}
//	
//	/**METHOD TO INICIALIZATE LIST OF VALUATOR_EXECUTION_PARAMETERS*/
//	private List<ValuatorExecutionParameters> getExecutionParameters(ValuatorProcessExecution valuatorExecutor ,List<Long> participants, 
//																	 List<Long> accounts, List<String> securities){
//		/**CREATE LIST TO HANDLE ALL PARAMETERS FOR THE PROCESS*/
//		List<ValuatorExecutionParameters> parameterList = new ArrayList<ValuatorExecutionParameters>();
//		
//		/**VERIFIED THE PARAMETERS FOR ALL THE PARTICIPANTS*/
//		if(participants!=null && !participants.isEmpty()){
//			for(Long participantPk : participants){
//				ValuatorExecutionParameters parameter = getValuatorExecutorParameter(valuatorExecutor);
//				parameter.setParticipant(new Participant(participantPk));
//				parameterList.add(parameter);
//			}
//		}
//		
//		/**VERIFIED THE PARAMETERS FOR ALL THE HOLDER_ACCOUNTS*/
//		if(accounts!=null && !accounts.isEmpty()){
//			for(Long accountPk : accounts){
//				ValuatorExecutionParameters parameter = getValuatorExecutorParameter(valuatorExecutor);
//				parameter.setHolderAccount(new HolderAccount(accountPk));
//				parameterList.add(parameter);
//			}
//		}
//		
//		/**VERIFIED THE PARAMETERS FOR ALL THE SECURITIES*/
//		if(securities!=null && !securities.isEmpty()){
//			for(String securityPk : securities){
//				ValuatorExecutionParameters parameter = getValuatorExecutorParameter(valuatorExecutor);
////				parameter.setSecurity(new Security(securityPk));
//				parameter.setSecurityCode(securityPk);
//				parameterList.add(parameter);
//			}
//		}		
//		return parameterList;
//	}
//
//	/**METHOD TO INICIALIZATE VALUATOR_EXECUTION_PARAMETER LIKE JAVA OBJECT*/
//	private ValuatorExecutionParameters getValuatorExecutorParameter(ValuatorProcessExecution valuatorExecutor){
//		ValuatorExecutionParameters parameter = new ValuatorExecutionParameters();
//		parameter.setRegistryUser(userSession);
//		parameter.setRegistryDate(CommonsUtilities.currentDateTime());
//		parameter.setValuatorProcessExecution(valuatorExecutor);
//		return parameter;
//	}
}