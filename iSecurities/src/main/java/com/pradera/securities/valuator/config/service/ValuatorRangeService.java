package com.pradera.securities.valuator.config.service; 

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.valuatorprocess.ValuatorTermRange;
import com.pradera.securities.valuator.config.view.ValuatorRangeTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorRangeService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@Stateless
public class ValuatorRangeService extends CrudDaoServiceBean{
	
	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup IdepositarySetup idepositarySetup;
	
	/**
	 * Creates the valuator term range.
	 *
	 * @param valuatorTermRange the valuator term range
	 * @return the valuator term range
	 * @throws ServiceException the service exception
	 */
	public ValuatorTermRange saveNewTermRange(ValuatorTermRange valuatorTermRange) throws ServiceException {
		return create(valuatorTermRange);
	}

	/**
	 * Update valuatorTermRange.
	 * Method to update the ValuatorTermRange
	 * @param valuatorTermRange the valuator term range
	 * @return the valuator term range
	 * @throws ServiceException the service exception
	 */
	public ValuatorTermRange modifyTermRange(ValuatorTermRange valuatorTermRange) throws ServiceException {
		return update(valuatorTermRange);
	}

	/**
	 * Delete valuatorTermRange.
	 * Method to delete the ValuatorTermRange
	 * @param valuatorTermRange the valuator term range
	 * @throws ServiceException the service exception
	 */
	public void deleteTermRange(ValuatorTermRange valuatorTermRange) throws ServiceException {
		delete(ValuatorTermRange.class, valuatorTermRange.getIdTermRangePk());
	}

	/***
	 * Method to find a valuator term range by Id.
	 * @param idTermRangePk the id term range
	 * @return a object ValuatorTermRange type
	 * @throws ServiceException the service exception
	 */
	public ValuatorTermRange viewTermRange(Long idTermRangePk) throws ServiceException {
		return find(ValuatorTermRange.class, idTermRangePk);
	}

	/***
	 * Method to get a list of ValuatorRangeTO by ValuatorRangeTO.
	 * @param ValuatorRangeTO the TO valuator range
	 * @return a list of ValuatorRangeTO
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ValuatorRangeTO> getValuatorRangeServiceBean(ValuatorRangeTO ValuatorRangeTO) throws ServiceException {
		StringBuilder selectSQL = new StringBuilder();
		StringBuilder whereSQL = new StringBuilder();
		StringBuilder orderBySQL = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();

		selectSQL.append("Select new com.pradera.securities.valuator.config.view.ValuatorRangeTO(				");
		selectSQL.append("vtr.idTermRangePk, vtr.rangeCode, vtr.maxRange, vtr.minRange,	vtr.registryDate )	");
		selectSQL.append("From ValuatorTermRange vtr														");
		
		whereSQL.append("Where 1 = 1 ");
		if (ValuatorRangeTO.getRangeCode() != null) {
			whereSQL.append("And vtr.rangeCode = :rangeCode ");
			parameters.put("rangeCode",ValuatorRangeTO.getRangeCode());
		}
		if (ValuatorRangeTO.getMinRange() != null) {
			whereSQL.append("And vtr.minRange >= :minRange ");
			parameters.put("minRange",ValuatorRangeTO.getMinRange());
		}
		if (ValuatorRangeTO.getMaxRange() != null) {
			whereSQL.append("And vtr.maxRange <= :maxRange ");
			parameters.put("maxRange",ValuatorRangeTO.getMaxRange());
		}

		orderBySQL.append(" Order By vtr.minRange, vtr.maxRange ");
		try {
			return findListByQueryString(selectSQL.toString().concat(whereSQL.toString()).concat(orderBySQL.toString()),parameters);
		} catch (NoResultException ex) {
			return null;
		}
	}

	/**
	 * Method to validate is the ValuatorTermRange exists.
	 * @param valuatorTermRange the valuator term range
	 * @return boolean value
	 * @throws ServiceException the service exception
	 */
	public Boolean valuatorTermRangeExists(ValuatorTermRange valuatorTermRange) throws ServiceException {
		
		Boolean exists = Boolean.FALSE;
		StringBuilder selectSQL = new StringBuilder();
		StringBuilder whereSQL = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();

		selectSQL.append("Select vtr.idTermRangePk					");
		selectSQL.append("From ValuatorTermRange vtr				");
		
		whereSQL.append("Where vtr.rangeCode = :rangeCode ");
		parameters.put("rangeCode",valuatorTermRange.getRangeCode());

		try {
			if (findObjectByQueryString(selectSQL.toString().concat(whereSQL.toString()),parameters) != null){
				exists =  Boolean.TRUE;
			}
		} catch (NoResultException ex) {
			exists = Boolean.FALSE;
		}
		return exists;
	}

	/***
	 * Method to find a valuator term range by securityDays.
	 * @param securityDays the security days
	 * @return a object ValuatorTermRange type
	 * @throws ServiceException the service exception
	 */
	public ValuatorTermRange getValuatorRangeBySecurityDaysService(Integer securityDays) throws ServiceException{
		StringBuilder selectSQL = new StringBuilder();
		selectSQL.append("Select vtr from ValuatorTermRange vtr");
		selectSQL.append(" where :securityDays >= vtr.minRange and :securityDays <= vtr.maxRange");
		Query query = this.em.createQuery(selectSQL.toString());
		query.setParameter("securityDays", BigDecimal.valueOf(securityDays));
		ValuatorTermRange valuatorTermRange=null;
		try {
			valuatorTermRange=(ValuatorTermRange)query.getSingleResult();
		} catch (NoResultException ex) {
			valuatorTermRange=null;
		} catch (NonUniqueResultException nu) {
			valuatorTermRange=null;
		}
		return valuatorTermRange;
	}
	
}