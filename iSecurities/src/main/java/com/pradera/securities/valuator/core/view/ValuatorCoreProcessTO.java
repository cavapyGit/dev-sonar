package com.pradera.securities.valuator.core.view;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.model.accounts.Holder;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorCoreProcessTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class ValuatorCoreProcessTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -9039073338453940980L;
	
	/** The execution hour. */
	private Date executionHour;
	
	/** The execution criteria. */
	private Integer executionCriteria;
	
	/** The security code. */
	private String securityCode;
	
	/** The participant pk. */
	private Long participantPk;
	
	/** The holder. */
	private Holder holder;
	
	/** The execution type. */
	private Integer executionType;
	
	/** The instrument type. */
	private Integer instrumentType;
	
	/** The email. */
	private String email;
	
	/** The valuator date. */
	private Date valuatorDate;
	
	/** The ip address. */
	private String ipAddress;
	
	/** The partic selected. */
	private List<Long> particSelected;
	
	/** The is all participant. */
	public boolean isAllParticipant;
	
	/** The execution pk. */
	public Long executionPk;
	
	/** The execution detail type. */
	public Integer executionDetailType;

	/**
	 * Gets the execution hour.
	 *
	 * @return the execution hour
	 */
	public Date getExecutionHour() {
		return executionHour;
	}

	/**
	 * Sets the execution hour.
	 *
	 * @param executionHour the new execution hour
	 */
	public void setExecutionHour(Date executionHour) {
		this.executionHour = executionHour;
	}

	/**
	 * Gets the security code.
	 *
	 * @return the security code
	 */
	public String getSecurityCode() {
		return securityCode;
	}

	/**
	 * Sets the security code.
	 *
	 * @param securityCode the new security code
	 */
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	/**
	 * Gets the execution type.
	 *
	 * @return the execution type
	 */
	public Integer getExecutionType() {
		return executionType;
	}

	/**
	 * Sets the execution type.
	 *
	 * @param executionType the new execution type
	 */
	public void setExecutionType(Integer executionType) {
		this.executionType = executionType;
	}

	/**
	 * Gets the instrument type.
	 *
	 * @return the instrument type
	 */
	public Integer getInstrumentType() {
		return instrumentType;
	}

	/**
	 * Sets the instrument type.
	 *
	 * @param instrumentType the new instrument type
	 */
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the execution criteria.
	 *
	 * @return the execution criteria
	 */
	public Integer getExecutionCriteria() {
		return executionCriteria;
	}

	/**
	 * Sets the execution criteria.
	 *
	 * @param executionCriteria the new execution criteria
	 */
	public void setExecutionCriteria(Integer executionCriteria) {
		this.executionCriteria = executionCriteria;
	}

	/**
	 * Gets the participant pk.
	 *
	 * @return the participant pk
	 */
	public Long getParticipantPk() {
		return participantPk;
	}

	/**
	 * Sets the participant pk.
	 *
	 * @param participantPk the new participant pk
	 */
	public void setParticipantPk(Long participantPk) {
		this.participantPk = participantPk;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Gets the valuator date.
	 *
	 * @return the valuator date
	 */
	public Date getValuatorDate() {
		return valuatorDate;
	}

	/**
	 * Sets the valuator date.
	 *
	 * @param valuatorDate the new valuator date
	 */
	public void setValuatorDate(Date valuatorDate) {
		this.valuatorDate = valuatorDate;
	}

	/**
	 * Gets the ip address.
	 *
	 * @return the ip address
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * Sets the ip address.
	 *
	 * @param ipAddress the new ip address
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * Gets the partic selected.
	 *
	 * @return the partic selected
	 */
	public List<Long> getParticSelected() {
		return particSelected;
	}

	/**
	 * Sets the partic selected.
	 *
	 * @param particSelected the new partic selected
	 */
	public void setParticSelected(List<Long> particSelected) {
		this.particSelected = particSelected;
	}

	/**
	 * Checks if is all participant.
	 *
	 * @return true, if is all participant
	 */
	public boolean isAllParticipant() {
		return isAllParticipant;
	}

	/**
	 * Sets the all participant.
	 *
	 * @param isAllParticipant the new all participant
	 */
	public void setAllParticipant(boolean isAllParticipant) {
		this.isAllParticipant = isAllParticipant;
	}

	/**
	 * Gets the execution pk.
	 *
	 * @return the execution pk
	 */
	public Long getExecutionPk() {
		return executionPk;
	}

	/**
	 * Sets the execution pk.
	 *
	 * @param executionPk the new execution pk
	 */
	public void setExecutionPk(Long executionPk) {
		this.executionPk = executionPk;
	}

	/**
	 * Gets the execution detail type.
	 *
	 * @return the execution detail type
	 */
	public Integer getExecutionDetailType() {
		return executionDetailType;
	}

	/**
	 * Sets the execution detail type.
	 *
	 * @param executionDetailType the new execution detail type
	 */
	public void setExecutionDetailType(Integer executionDetailType) {
		this.executionDetailType = executionDetailType;
	}
}