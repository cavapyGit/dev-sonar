package com.pradera.securities.valuator.core.files.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.pradera.integration.common.validation.to.RecordValidationType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MarketfactValidationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class MarketfactValidationTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The lst marketfact f iles. */
	private List<ValuatorMarketfactMechanismType> lstMarketfactFIles;
	
	/** The lst errors. */
	private List<RecordValidationType> lstErrors;
	
	/** The rejected marketfactfiles. */
	private int rejectedMarketfactfiles ;
	
	/** The has header error. */
	private boolean hasHeaderError;
	
	/** The total count. */
	private int totalCount;
	
	
	/**
	 * Instantiates a new marketfact validation to.
	 */
	public MarketfactValidationTO() {
		super();
		lstMarketfactFIles=new ArrayList<>(0);
		lstErrors = new ArrayList<RecordValidationType>(0);
	}
	
	
	/**
	 * Gets the lst errors.
	 *
	 * @return the lstErrors
	 */
	public List<RecordValidationType> getLstErrors() {
		return lstErrors;
	}
	
	/**
	 * Sets the lst errors.
	 *
	 * @param lstErrors the lstErrors to set
	 */
	public void setLstErrors(List<RecordValidationType> lstErrors) {
		this.lstErrors = lstErrors;
	}

	/**
	 * Gets the rejected marketfactfiles.
	 *
	 * @return the rejected marketfactfiles
	 */
	public int getRejectedMarketfactfiles() {
		return rejectedMarketfactfiles;
	}

	/**
	 * Sets the rejected marketfactfiles.
	 *
	 * @param rejectedMarketfactfiles the new rejected marketfactfiles
	 */
	public void setRejectedMarketfactfiles(int rejectedMarketfactfiles) {
		this.rejectedMarketfactfiles = rejectedMarketfactfiles;
	}


	/**
	 * Checks if is checks for header error.
	 *
	 * @return the hasHeaderError
	 */
	public boolean isHasHeaderError() {
		return hasHeaderError;
	}

	/**
	 * Sets the checks for header error.
	 *
	 * @param hasHeaderError the hasHeaderError to set
	 */
	public void setHasHeaderError(boolean hasHeaderError) {
		this.hasHeaderError = hasHeaderError;
	}

	/**
	 * Gets the total count.
	 *
	 * @return the totalCount
	 */
	public int getTotalCount() {
		return totalCount;
	}

	/**
	 * Sets the total count.
	 *
	 * @param totalCount the totalCount to set
	 */
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	
	/**
	 * Gets the lst marketfact f iles.
	 *
	 * @return the lst marketfact f iles
	 */
	public List<ValuatorMarketfactMechanismType> getLstMarketfactFIles() {
		return lstMarketfactFIles;
	}
	
	/**
	 * Sets the lst marketfact f iles.
	 *
	 * @param lstMarketfactFIles the new lst marketfact f iles
	 */
	public void setLstMarketfactFIles(
			List<ValuatorMarketfactMechanismType> lstMarketfactFIles) {
		this.lstMarketfactFIles = lstMarketfactFIles;
	}
	


}
