package com.pradera.securities.valuator.core.view;

import java.io.Serializable;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorProcessExecutionTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class ValuatorProcessExecutionTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id process execution pk. */
	private Long idProcessExecutionPk;
	
 	/** The operation number. */
	 private Long operationNumber;

	/** The begin time. */
	private Date beginTime;

	/** The execution type. */
	private Integer executionType;
	
	/** The date type. */
	private Integer dateType;

	/** The finish time. */
	private Date finishTime;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;

	/** The process date. */
	private Date processDate;

	/** The process state. */
	private Integer processState;

	/** The registry date. */
	private Date registryDate;

	/** The registry user. */
	private String registryUser;
	
	/** The valuator type. */
	private Integer valuatorType;

	/**
	 * Instantiates a new valuator process execution to.
	 */
	public ValuatorProcessExecutionTO() {}

	/**
	 * Instantiates a new valuator process execution to.
	 *
	 * @param idProcessExecutionPk the id process execution pk
	 * @param operationNumber the operation number
	 * @param beginTime the begin time
	 * @param executionType the execution type
	 * @param finishTime the finish time
	 * @param processState the process state
	 * @param registryDate the registry date
	 * @param valuatorType the valuator type
	 */
	public ValuatorProcessExecutionTO(Long idProcessExecutionPk,
			Long operationNumber, Date beginTime, Integer executionType,
			Date finishTime, Integer processState, Date registryDate,
			Integer valuatorType) {
		super();
		this.idProcessExecutionPk = idProcessExecutionPk;
		this.operationNumber = operationNumber;
		this.beginTime = beginTime;
		this.executionType = executionType;
		this.finishTime = finishTime;
		this.processState = processState;
		this.registryDate = registryDate;
		this.valuatorType = valuatorType;
	}

	/**
	 * Gets the id process execution pk.
	 *
	 * @return the id process execution pk
	 */
	public Long getIdProcessExecutionPk() {
		return idProcessExecutionPk;
	}

	/**
	 * Sets the id process execution pk.
	 *
	 * @param idProcessExecutionPk the new id process execution pk
	 */
	public void setIdProcessExecutionPk(Long idProcessExecutionPk) {
		this.idProcessExecutionPk = idProcessExecutionPk;
	}

	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}

	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}

	/**
	 * Gets the begin time.
	 *
	 * @return the begin time
	 */
	public Date getBeginTime() {
		return beginTime;
	}

	/**
	 * Sets the begin time.
	 *
	 * @param beginTime the new begin time
	 */
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	/**
	 * Gets the execution type.
	 *
	 * @return the execution type
	 */
	public Integer getExecutionType() {
		return executionType;
	}

	/**
	 * Sets the execution type.
	 *
	 * @param executionType the new execution type
	 */
	public void setExecutionType(Integer executionType) {
		this.executionType = executionType;
	}

	/**
	 * Gets the date type.
	 *
	 * @return the date type
	 */
	public Integer getDateType() {
		return dateType;
	}

	/**
	 * Sets the date type.
	 *
	 * @param dateType the new date type
	 */
	public void setDateType(Integer dateType) {
		this.dateType = dateType;
	}

	/**
	 * Gets the finish time.
	 *
	 * @return the finish time
	 */
	public Date getFinishTime() {
		return finishTime;
	}

	/**
	 * Sets the finish time.
	 *
	 * @param finishTime the new finish time
	 */
	public void setFinishTime(Date finishTime) {
		this.finishTime = finishTime;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	/**
	 * Gets the process date.
	 *
	 * @return the process date
	 */
	public Date getProcessDate() {
		return processDate;
	}

	/**
	 * Sets the process date.
	 *
	 * @param processDate the new process date
	 */
	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	/**
	 * Gets the process state.
	 *
	 * @return the process state
	 */
	public Integer getProcessState() {
		return processState;
	}

	/**
	 * Sets the process state.
	 *
	 * @param processState the new process state
	 */
	public void setProcessState(Integer processState) {
		this.processState = processState;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the valuator type.
	 *
	 * @return the valuator type
	 */
	public Integer getValuatorType() {
		return valuatorType;
	}

	/**
	 * Sets the valuator type.
	 *
	 * @param valuatorType the new valuator type
	 */
	public void setValuatorType(Integer valuatorType) {
		this.valuatorType = valuatorType;
	}
}