package com.pradera.securities.valuator.simulator.view;

import java.math.BigDecimal;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class DetailSimulationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class DetailSimulationTO {

	/** The market rate. */
	private BigDecimal marketRate;
	
	/** The market price. */
	private BigDecimal marketPrice;
	
	/** The total balance. */
	private BigDecimal totalBalance;
	
	/** The total available. */
	private BigDecimal totalAvailable;
	
	/** The total valorized. */
	private BigDecimal totalValorized;
	
	/** The available available. */
	private BigDecimal availableAvailable;
	
	/** The security code. */
	private String securityCode;
	

	/**
	 * Instantiates a new detail simulation to.
	 */
	public DetailSimulationTO() {}


	/**
	 * Gets the market rate.
	 *
	 * @return the market rate
	 */
	public BigDecimal getMarketRate() {
		return marketRate;
	}


	/**
	 * Sets the market rate.
	 *
	 * @param marketRate the new market rate
	 */
	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}


	/**
	 * Gets the market price.
	 *
	 * @return the market price
	 */
	public BigDecimal getMarketPrice() {
		return marketPrice;
	}


	/**
	 * Sets the market price.
	 *
	 * @param marketPrice the new market price
	 */
	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}


	/**
	 * Gets the total balance.
	 *
	 * @return the total balance
	 */
	public BigDecimal getTotalBalance() {
		return totalBalance;
	}


	/**
	 * Sets the total balance.
	 *
	 * @param totalBalance the new total balance
	 */
	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}


	/**
	 * Gets the total available.
	 *
	 * @return the total available
	 */
	public BigDecimal getTotalAvailable() {
		return totalAvailable;
	}


	/**
	 * Sets the total available.
	 *
	 * @param totalAvailable the new total available
	 */
	public void setTotalAvailable(BigDecimal totalAvailable) {
		this.totalAvailable = totalAvailable;
	}


	/**
	 * Gets the total valorized.
	 *
	 * @return the total valorized
	 */
	public BigDecimal getTotalValorized() {
		return totalValorized;
	}


	/**
	 * Sets the total valorized.
	 *
	 * @param totalValorized the new total valorized
	 */
	public void setTotalValorized(BigDecimal totalValorized) {
		this.totalValorized = totalValorized;
	}


	/**
	 * Gets the available available.
	 *
	 * @return the available available
	 */
	public BigDecimal getAvailableAvailable() {
		return availableAvailable;
	}


	/**
	 * Sets the available available.
	 *
	 * @param availableAvailable the new available available
	 */
	public void setAvailableAvailable(BigDecimal availableAvailable) {
		this.availableAvailable = availableAvailable;
	}


	/**
	 * Gets the security code.
	 *
	 * @return the security code
	 */
	public String getSecurityCode() {
		return securityCode;
	}


	/**
	 * Sets the security code.
	 *
	 * @param securityCode the new security code
	 */
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
	
	


}