package com.pradera.securities.valuator.core.facade;

import static com.pradera.model.valuatorprocess.type.ValuatorDetailType.SECURITY;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.valuatorprocess.ValuatorPriceReport;
import com.pradera.securities.query.to.SecurityReportResultTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorExecutionBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@BatchProcess(name = "ValuatorReportRepBatch")
@RequestScoped
public class ValuatorReportRepBatch implements JobExecution, Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The valuator securities facade. */
	@EJB private ValuatorSecuritiesFacade valuatorSecuritiesFacade;
	
    @Inject 
    ClientRestService clientRestService;
	
	@Override
	public void startJob(ProcessLogger processLogger) {
		
		//List<ProcessLoggerDetail> lstProcessLoggerDetails = processLogger.getProcessLoggerDetails();
		/*for (ProcessLoggerDetail objDetail: lstProcessLoggerDetails){
			if(StringUtils.equalsIgnoreCase(ValuatorConstants.PARAM_USER_EMAIL, objDetail.getParameterName())) {
				userEmail = objDetail.getParameterValue();
			}else if(StringUtils.equalsIgnoreCase(ValuatorConstants.PARAM_USER_NAME, objDetail.getParameterName())) {
				userName = objDetail.getParameterValue();
			}else if(StringUtils.equalsIgnoreCase(ValuatorConstants.PARAM_VALUATOR_EXECUTION_TYPE, objDetail.getParameterName())){
				executionType = new Integer(objDetail.getParameterValue());
			}
		}*/
		
		//Traemos el estado del indicador para mandar el historico o el diario
		Integer indHistoric = valuatorSecuritiesFacade.indHistoricPrice();
		
		//fecha actual
		Date valuatorDate = CommonsUtilities.truncateDateTime(CommonsUtilities.currentDate());
		
		//fecha de inicio de calculo historico
		Date historicInitialDate = CommonsUtilities.convertStringtoDate("01/01/2018");
		
		//Si el indicador HISTORICO de la bbdd esta en 1 quiere decir que enviaremos a calcular desde el historicInitialDate	
		if(indHistoric.equals(GeneralConstants.ONE_VALUE_INTEGER)) {
				
			//borramos toda la tabla ya que el historico inicia desde 0
			valuatorSecuritiesFacade.restartDataValuatorPricesReportHist();
				
			//procesamos los reportos de la fecha 1
			listSecurityProcess(historicInitialDate,indHistoric);
				
			//aumentamos 1 dia al anterior
			historicInitialDate = CommonsUtilities.addDate(historicInitialDate, 1);
			
			while (historicInitialDate.before(valuatorDate)) {
					
				//procesamos los reportos de la fecha
				listSecurityProcess(historicInitialDate,indHistoric);
					
				//aumentamos 1 dia al anterior
				historicInitialDate = CommonsUtilities.addDate(historicInitialDate, 1);
			}
			
//			if(indHistoric.equals(GeneralConstants.ONE_VALUE_INTEGER)) {
//			
//				List<Date> listFec = fechasfaltantes();
//				
//				for(Date fecha : listFec) {
//					
//					listSecurityProcess(fecha,indHistoric);
//					
//				}
//			}
				
		}else {
			//Verificando que el proceso no se haya ejecutado con anterioridad en el dia
			List<ValuatorPriceReport> listValuatorPriceReport = valuatorSecuritiesFacade.searchValuatorPriceReportByCurrenDate(valuatorDate);
			if(Validations.validateListIsNullOrEmpty(listValuatorPriceReport)){
				listSecurityProcess(valuatorDate,indHistoric);
				//TODO controlar EXCEPCION
			}
		}
	}
	
	/**
	 * Metodo para procesar los datos de la fecha
	 * @param valuatorDate
	 */
	public void listSecurityProcess (Date valuatorDate, Integer indHistoric) {
		try {
			
				//Listamos los valores en reporto AGBolsa
				List<SecurityReportResultTO> securitiesListAGB = valuatorSecuritiesFacade.searchSecurityReportForPriceCurveAGB(valuatorDate); //new ArrayList<SecurityReportResultTO>();
				
				//Listamos los valores en reporto ASFI
				List<SecurityReportResultTO> securitiesListAsfi = valuatorSecuritiesFacade.searchSecurityReportForPriceCurveASFI(valuatorDate);
				
				Map<String,Security> mapSecurities = new HashMap<String,Security>();
				
				//Recorremos el listado para generar el precio AGBolsa 
				for(SecurityReportResultTO listSecValuator : securitiesListAGB) {
					
					Security sec = valuatorSecuritiesFacade.searchSecurity(listSecValuator.getSecurityCode());
					mapSecurities.put(sec.getIdSecurityCodePk(), sec);
					
					executeValuatorProcessAGB(
							sec, valuatorDate, listSecValuator.getInstrumentType(), SECURITY.getCode(),listSecValuator.getMarketRate(),listSecValuator.getMarketDate(),indHistoric,listSecValuator.getOperationPart());
				}
				
				//Recorremos el listado para generar el precio ASFI
				for(SecurityReportResultTO listSecValuator : securitiesListAsfi) {
					
					Security sec = mapSecurities.get(listSecValuator.getSecurityCode()); 
					
					if(!Validations.validateIsNotNullAndNotEmpty(sec)) {
						sec = valuatorSecuritiesFacade.searchSecurity(listSecValuator.getSecurityCode());
					}
					executeValuatorProcessASFI(
							sec, valuatorDate, listSecValuator.getInstrumentType(), SECURITY.getCode(),listSecValuator.getMarketRate(),listSecValuator.getMarketDate(),indHistoric,listSecValuator.getOperationPart());
				}
			
		}catch (Exception e2) {
			// TODO: handle exception
		}
	}
	
	
	/**
	 * Metodo para calcular el precio curva para AGBolsa
	 * @param security
	 * @param processDate
	 * @param instrumentType
	 * @param valuatorType
	 * @param marketRate
	 * @param marketDate
	 */
	@LoggerAuditWeb
	private void executeValuatorProcessAGB(Security security, Date processDate, Integer instrumentType, Integer valuatorType, BigDecimal marketRate, Date marketDate, Integer indHistoric, Integer operationPart){
		
		try {
				switch(instrumentType){
					case 124: 
							BigDecimal priceReportF = valuatorSecuritiesFacade.valuatorPriceReport(
									security,processDate,valuatorType,marketRate,marketDate,instrumentType,indHistoric, GeneralConstants.ZERO_VALUE_INTEGER).get(0);
							BigDecimal economicTermF = valuatorSecuritiesFacade.valuatorPriceReport(
									security,processDate,valuatorType,marketRate,marketDate,instrumentType,indHistoric, GeneralConstants.ZERO_VALUE_INTEGER).get(1);
							
							ValuatorPriceReport valuatorPriceReportF = new ValuatorPriceReport();
							
							valuatorPriceReportF.setMarketPrice(priceReportF);
							valuatorPriceReportF.setEconomicTerm(economicTermF);
							valuatorPriceReportF.setSecurity(security);
							valuatorPriceReportF.setMarketRate(marketRate);
							valuatorPriceReportF.setMarketDate(marketDate);
							valuatorPriceReportF.setProcessDate(processDate);
							valuatorPriceReportF.setIndAsfi(GeneralConstants.ZERO_VALUE_INTEGER);
							valuatorPriceReportF.setSecurityCode(security.getIdSecurityCodePk());
							valuatorPriceReportF.setOperationPart(operationPart);
							valuatorSecuritiesFacade.registerValuatorPriceReport(valuatorPriceReportF);
							break;
					case 399: 
							BigDecimal priceReportV = valuatorSecuritiesFacade.valuatorPriceReport(
									security,processDate,valuatorType,marketRate,marketDate,instrumentType,indHistoric, GeneralConstants.ZERO_VALUE_INTEGER).get(0);
							
							ValuatorPriceReport valuatorPriceReportV = new ValuatorPriceReport();
							
							valuatorPriceReportV.setMarketPrice(priceReportV);
							valuatorPriceReportV.setSecurity(security);
							valuatorPriceReportV.setMarketRate(marketRate);
							valuatorPriceReportV.setMarketDate(marketDate);
							valuatorPriceReportV.setProcessDate(processDate);
							valuatorPriceReportV.setIndAsfi(GeneralConstants.ZERO_VALUE_INTEGER);
							valuatorPriceReportV.setSecurityCode(security.getIdSecurityCodePk());
							valuatorPriceReportV.setOperationPart(operationPart);
							
							valuatorSecuritiesFacade.registerValuatorPriceReport(valuatorPriceReportV);
							break;
				}
				
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	

	/**
	 * Metodo para calcular el precio curva para ASFI
	 * @param security
	 * @param processDate
	 * @param instrumentType
	 * @param valuatorType
	 * @param marketRate
	 * @param marketDate
	 */
	@LoggerAuditWeb
	private void executeValuatorProcessASFI(Security security, Date processDate, Integer instrumentType, Integer valuatorType, BigDecimal marketRate, Date marketDate,Integer indHistoric, Integer operationPart){
		
		try {
				switch(instrumentType){
					case 124: 
							BigDecimal priceReportF = valuatorSecuritiesFacade.valuatorPriceReport(
									security,processDate,valuatorType,marketRate,marketDate,instrumentType,indHistoric, GeneralConstants.ONE_VALUE_INTEGER).get(0);
							BigDecimal economicTermF = valuatorSecuritiesFacade.valuatorPriceReport(
									security,processDate,valuatorType,marketRate,marketDate,instrumentType,indHistoric, GeneralConstants.ONE_VALUE_INTEGER).get(1);
							
							ValuatorPriceReport valuatorPriceReportF = new ValuatorPriceReport();
							
							valuatorPriceReportF.setMarketPrice(priceReportF);
							valuatorPriceReportF.setEconomicTerm(economicTermF);
							valuatorPriceReportF.setSecurity(security);
							valuatorPriceReportF.setMarketRate(marketRate);
							valuatorPriceReportF.setMarketDate(marketDate);
							valuatorPriceReportF.setProcessDate(processDate);
							valuatorPriceReportF.setIndAsfi(GeneralConstants.ONE_VALUE_INTEGER);
							valuatorPriceReportF.setSecurityCode(security.getIdSecurityCodePk());
							valuatorPriceReportF.setOperationPart(operationPart);
							
							valuatorSecuritiesFacade.registerValuatorPriceReport(valuatorPriceReportF);
							break;
					case 399: 
							BigDecimal priceReportV = valuatorSecuritiesFacade.valuatorPriceReport(
									security,processDate,valuatorType,marketRate,marketDate,instrumentType,indHistoric, GeneralConstants.ONE_VALUE_INTEGER).get(0);
							
							ValuatorPriceReport valuatorPriceReportV = new ValuatorPriceReport();
							
							valuatorPriceReportV.setMarketPrice(priceReportV);
							valuatorPriceReportV.setSecurity(security);
							valuatorPriceReportV.setMarketRate(marketRate);
							valuatorPriceReportV.setMarketDate(marketDate);
							valuatorPriceReportV.setProcessDate(processDate);
							valuatorPriceReportV.setIndAsfi(GeneralConstants.ONE_VALUE_INTEGER);
							valuatorPriceReportV.setSecurityCode(security.getIdSecurityCodePk());
							valuatorPriceReportV.setOperationPart(operationPart);
							
							valuatorSecuritiesFacade.registerValuatorPriceReport(valuatorPriceReportV);
							break;
				}
				
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
//	public List<Date> fechasfaltantes (){
//		List<Date> fFaltantes = new ArrayList<>();
//		
//		fFaltantes.add(CommonsUtilities.convertStringtoDate("31/07/2020"));
//		fFaltantes.add(CommonsUtilities.convertStringtoDate("03/08/2020"));
//		
//		return fFaltantes;	
//	}
	
	
		
//	/**
//	 * Metodo para verificar la reversion de saldos en operaciones selid
//	 * @return
//	 */
//	public Boolean verifyUnblockBalanceSelidBatch () {
//	
//		Boolean result = valuatorSecuritiesFacade.verifyUnblockBalanceSelidBatchBoolean();
//		
//		return result;
//	}
	
//	/**
//	 * Metodo para registrar el batch de reversion de bloqueo de saldos en operaciones SELID. issue 1040
//	 */
//	@LoggerAuditWeb
//	private void registerUnblockBalanceSelidBatch(ProcessLogger processLogger){
//		
//		Schedulebatch scheduleBatch = new Schedulebatch();
//        scheduleBatch.setIdBusinessProcess(BusinessProcessType.VALUATOR_BALANCE_UNBLOCK_BUY_SELL_SELID.getCode());
//        scheduleBatch.setUserName(processLogger.getIdUserAccount());
//        scheduleBatch.setPrivilegeCode(processLogger.getLastModifyApp());
//        scheduleBatch.setParameters(new HashMap<String, String>());
//    	clientRestService.excuteRemoteBatchProcess(scheduleBatch,"iSettlements");
//
//	}
	
//	/**
//	 * Metodo para registrar el batch de bloqueo de saldos en operaciones SELID. issue 1040
//	 */
//	@LoggerAuditWeb
//	private void registerBlockBalanceSelidBatch(ProcessLogger processLogger){
//		
//		Schedulebatch scheduleBatch = new Schedulebatch();
//        scheduleBatch.setIdBusinessProcess(BusinessProcessType.VALUATOR_BALANCE_UPDATE_BUY_SELL_SELID.getCode());
//        scheduleBatch.setUserName(processLogger.getIdUserAccount());
//        scheduleBatch.setPrivilegeCode(processLogger.getLastModifyApp());
//        scheduleBatch.setParameters(new HashMap<String, String>());
//    	clientRestService.excuteRemoteBatchProcess(scheduleBatch,"iSettlements");
//		
//	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}
	

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}
}