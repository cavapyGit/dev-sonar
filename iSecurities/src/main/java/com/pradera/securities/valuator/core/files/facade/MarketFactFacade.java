package com.pradera.securities.valuator.core.files.facade;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.valuatorprocess.ValuatorMarketfactConsolidat;
import com.pradera.model.valuatorprocess.ValuatorMarketfactFile;
import com.pradera.model.valuatorprocess.ValuatorMarketfactHistory;
import com.pradera.model.valuatorprocess.ValuatorMarketfactMechanism;
import com.pradera.securities.valuator.core.files.service.MarketFactService;
import com.pradera.securities.valuator.core.files.to.ValuatorMarketfactConsolidatTo;
import com.pradera.securities.valuator.core.files.to.ValuatorMarketfactFileTO;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MarketFactFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , Aug 25, 2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class MarketFactFacade {

	/** The transaction registry. */
	@Resource TransactionSynchronizationRegistry transactionRegistry;
	
	/** The holiday query service bean. */
	@EJB HolidayQueryServiceBean holidayQueryServiceBean;
	
	/** The market fact service. */
	@EJB
	MarketFactService marketFactService;
	
	/**
	 * Search valuator marketfact file facade.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.VALUATOR_EXECUTION_MARKETFACT_FILE_QUERY)
	public List<ValuatorMarketfactFileTO> searchValuatorMarketfactFileFacade(ValuatorMarketfactFileTO filter)throws ServiceException{
		return marketFactService.searchValuatorMarketfactFileService(filter);
	}
	
	/**
	 * Save valuator marketfact files facade.
	 *
	 * @param lstMarketfactFiles the lst marketfact files
	 * @param workingDay the working day
	 * @param processDate the process date
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.VALUATOR_EXECUTION_MARKETFACT_FILE_REGISTER)
	public void saveValuatorMarketfactFilesFacade(List<ValuatorMarketfactFile> lstMarketfactFiles, boolean workingDay, Date processDate)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		if(!workingDay){
			marketFactService.replaceHolidaysFile(processDate, loggerUser);
		}
		marketFactService.saveMarketfactFilesService(lstMarketfactFiles);
	}
	
	/**
	 * Change file state facade.
	 *
	 * @param filter the filter
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.VALUATOR_EXECUTION_MARKETFACT_FILE_PROCESS)
	public void changeFileStateFacade(ValuatorMarketfactFileTO filter)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		marketFactService.changeFileStateService(filter, loggerUser);
	}
	
	/**
	 * Change file statefacade.
	 *
	 * @param lstRecepcionado the lst recepcionado
	 * @param fileState the file state
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.VALUATOR_EXECUTION_MARKETFACT_FILE_PROCESS)
	public void changeFileStatefacade(List<ValuatorMarketfactFileTO> lstRecepcionado,Integer fileState)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		for(ValuatorMarketfactFileTO filter : lstRecepcionado){
			filter.setFileState(fileState);
			marketFactService.changeFileStateService(filter, loggerUser);
		}
	}
	
	/**
	 * Save mechanism to historical.
	 *
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.VALUATOR_EXECUTION_MARKETFACT_FILE_PROCESS)
	public void saveMechanismToHistorical()throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		List<ValuatorMarketfactMechanism>  lstMarketFactMechanism=marketFactService.searchValuatorMarketfactMechanismService();
		List<ValuatorMarketfactHistory> lstMarketFactHistory=new ArrayList<ValuatorMarketfactHistory>();
		for(ValuatorMarketfactMechanism Mechanism : lstMarketFactMechanism){
			ValuatorMarketfactHistory History= new ValuatorMarketfactHistory();
			History.setAveragePrice(Mechanism.getAveragePrice());
			History.setAverageRate(Mechanism.getAverageRate());
			History.setInformationDate(Mechanism.getInformationDate());
			History.setMarketfactDate(Mechanism.getMarketfactDate());
			History.setMarketfactActive(Mechanism.getMarketfactActive());
			History.setMarketfactType(Mechanism.getMarketfactType());
			History.setMechanismCode(Mechanism.getMechanismCode());
			History.setMinimunAmount(Mechanism.getMinimunAmount());
			History.setNegotiationAmount(Mechanism.getNegotiationAmount());
			History.setRegistryDate(CommonsUtilities.currentDateTime());
			History.setRegistryUser(loggerUser.getUserName());
			History.setSecurityClass(Mechanism.getSecurityClass());
			History.setSecurityCode(Mechanism.getSecurityCode());
			History.setValuatorCode(Mechanism.getValuatorCode());
			History.setValuatorType(Mechanism.getValuatorType());
			History.setValuatorMarketfactFile(Mechanism.getValuatorMarketfactFile());
			lstMarketFactHistory.add(History);
		}
		marketFactService.saveMarketFactHistoryService(lstMarketFactHistory);
		marketFactService.deleteAllMarketFactFileMechanismService();
	}
	
	
	/**
	 * Gets the consolidate to mechanism facade.
	 *
	 * @param marketfactType the marketfact type
	 * @param processDate the process date
	 * @return the consolidate to mechanism facade
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.VALUATOR_EXECUTION_MARKETFACT_FILE_PROCESS)
	public List<ValuatorMarketfactConsolidatTo>  getConsolidateToMechanismFacade(Integer marketfactType, Date processDate)throws ServiceException{
		return marketFactService.getConsolidateToMechanismService(marketfactType,processDate);
	}
	
	/**
	 * Search valuator marketfact mechanism facade.
	 *
	 * @param marketfactType the marketfact type
	 * @param processDate the process date
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.VALUATOR_EXECUTION_MARKETFACT_FILE_QUERY)
	public List<ValuatorMarketfactMechanism> searchValuatorMarketfactMechanismFacade(Integer marketfactType, Date processDate)throws ServiceException{
		return marketFactService.searchValuatorMarketfactMechanismService(marketfactType,processDate);
	}
	
	/**
	 * Search valuator marketfact mechanism facade no equals.
	 *
	 * @param marketfactType the marketfact type
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.VALUATOR_EXECUTION_MARKETFACT_FILE_PROCESS)
	public List<ValuatorMarketfactMechanism> searchValuatorMarketfactMechanismFacadeNoEquals(Integer marketfactType)throws ServiceException{
		return marketFactService.searchValuatorMarketfactMechanismServiceNoEquals(marketfactType);
	}
	
	/**
	 * Delete market fact file mechanism facade.
	 *
	 * @param lstRecepcionado the lst recepcionado
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.VALUATOR_EXECUTION_MARKETFACT_FILE_PROCESS)
	public void deleteMarketFactFileMechanismFacade(List<ValuatorMarketfactFileTO> lstRecepcionado)throws ServiceException{
		for(ValuatorMarketfactFileTO filter : lstRecepcionado){
			marketFactService.deleteMarketFactFileMechanismService(filter);
		}
	}

	/**
	 * Exist market fact mechanism today facade.
	 *
	 * @param filter the filter
	 * @return the boolean
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.VALUATOR_EXECUTION_MARKETFACT_FILE_QUERY)
	public Boolean existMarketFactMechanismTodayFacade(ValuatorMarketfactFileTO filter)throws ServiceException{
		return marketFactService.existMarketFactMechanismTodayService(filter);
	}
	
	/**
	 * Exist market fact mechanism beforfacade.
	 *
	 * @param filter the filter
	 * @return the boolean
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.VALUATOR_EXECUTION_MARKETFACT_FILE_QUERY)
	public Boolean existMarketFactMechanismBeforfacade(ValuatorMarketfactFileTO filter)throws ServiceException{
		return marketFactService.existMarketFactMechanismBeforeService(filter);
	}
	
	/**
	 * Save valuator market fact mechanism facade.
	 *
	 * @param lstMechanism the lst mechanism
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.VALUATOR_EXECUTION_MARKETFACT_FILE_PROCESS)
	public void saveValuatorMarketFactMechanismFacade(List<ValuatorMarketfactMechanism> lstMechanism)throws ServiceException{
		marketFactService.saveValuatorMarketFactMechanismService(lstMechanism);
	}
	
	/**
	 * Save valuator marketfact consolidat.
	 *
	 * @param lstConsolidat the lst consolidat
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.VALUATOR_EXECUTION_MARKETFACT_FILE_PROCESS)
	public void saveValuatorMarketfactConsolidat(List<ValuatorMarketfactConsolidat> lstConsolidat)throws ServiceException{
		marketFactService.saveValuatorMarketfactConsolidat(lstConsolidat);
	}
	
	/**
	 * Gets the next holidate date.
	 *
	 * @param date the date
	 * @return the next holidate date
	 * @throws ServiceException the service exception
	 */
	public Date getNextHolidateDate(Date date) throws ServiceException{
		Date holidateDate = null;
		Boolean validateWknd = true;
		if(holidayQueryServiceBean.isNonWorkingDayServiceBean(date,validateWknd)){
			holidateDate = date;
		}else{
			holidateDate = holidayQueryServiceBean.getNextHolidateDate(date);		
		}
		return holidateDate;
	}
	
	/**
	 * Exist marketfact consolidat facade.
	 *
	 * @return the boolean
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.VALUATOR_EXECUTION_MARKETFACT_FILE_QUERY)
	public Boolean existMarketfactConsolidatFacade()throws ServiceException{
		return marketFactService.existMarketfactConsolidatService();
	}
	
	/**
	 * Delete all market fact consolidat facade.
	 *
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.VALUATOR_EXECUTION_MARKETFACT_FILE_PROCESS)
	public void deleteAllMarketFactConsolidatFacade()throws ServiceException{
		marketFactService.deleteAllMarketFactConsolidatService();
	}
}
