package com.pradera.securities.valuator.classes.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.commons.configuration.ValuatorSetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.issuancesecuritie.InterestPaymentSchedule;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.SecurityInvestor;
import com.pradera.model.issuancesecuritie.type.InterestType;
import com.pradera.model.valuatorprocess.SecurityClassValuator;
import com.pradera.model.valuatorprocess.ValuatorProcessClass;
import com.pradera.model.valuatorprocess.ValuatorProcessSetup;
import com.pradera.model.valuatorprocess.ValuatorSecurityCode;
import com.pradera.securities.valuator.classes.to.SecurityClassValuatorResultTO;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SecuritiesClassValuatorService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@Stateless
@Interceptors(ContextHolderInterceptor.class)
public class SecuritiesClassValuatorService extends CrudDaoServiceBean{

	/** The valuator setup. */
	@Inject @ValuatorSetup private ValuatorProcessSetup valuatorSetup;
	
	/**
	 * Gets the list valuator process class.
	 *
	 * @param instrumentType the instrument type
	 * @return the list valuator process class
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")	
	public List<ValuatorProcessClass> getListValuatorProcessClass(Integer instrumentType) throws ServiceException	{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select new ValuatorProcessClass ("+
						"	vpc.idValuatorClassPk,		"+
						"	vpc.classDescription,		"+
						"	vpc.currency,				"+
						"	vpc.minRelevantAmount,		"+
						"	vpc.topRelevantAmount,		"+
						"	vpc.formuleType ,			"+
						"	vpc.instrumentType )		"+
						"	From ValuatorProcessClass vpc Where 1 = 1 ");
		if(Validations.validateIsNotNull(instrumentType)){
			sbQuery.append(" and vpc.instrumentType = :instrumentType");
		}
		sbQuery.append(" order by vpc.idValuatorClassPk");
		Query query = this.em.createQuery(sbQuery.toString());
		if(Validations.validateIsNotNull(instrumentType)){
			query.setParameter("instrumentType", instrumentType);
		}
		return (List<ValuatorProcessClass>)query.getResultList();
	}	
	/**
	 * Find securyty Class.
	 *
	 * @param securityClassFk the ParameterTablePK
	 * @return Long
	 * @throws ServiceException the service exception
	 */
	
	public Long existSecurityClass(Integer securityClassFk)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select count(*)");
		sbQuery.append("FROM SecurityClassValuator scv ");
		sbQuery.append("WHERE scv.idSecurityClassFk = :securityClassFk");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("securityClassFk", securityClassFk);
		Long count= (Long) query.getSingleResult();
		return count;
	}
	

	/**
	 * Serch security class valuator result.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SecurityClassValuatorResultTO> serchSecurityClassValuatorResult(SecurityClassValuatorResultTO filter)throws ServiceException{
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select new com.pradera.securities.valuator.classes.to.SecurityClassValuatorResultTO(");
		sbQuery.append("	scv.idSecurityClassFk,");
		sbQuery.append("	max(p.text1),");
		sbQuery.append("	max(p.description),");
		sbQuery.append("	max(p.shortInteger))");
		sbQuery.append("	FROM SecurityClassValuator scv,");
		sbQuery.append("	ParameterTable p");
		sbQuery.append("	where p.parameterTablePk=scv.idSecurityClassFk");
		if(Validations.validateIsNotNull(filter.getDescription())){
			sbQuery.append("	and (p.text1 like :text1");
			sbQuery.append("	or p.description like :description)");
			parameters.put("text1", filter.getDescription());
			parameters.put("description", filter.getDescription());
		}
		if(Validations.validateIsNotNull(filter.getInstrumentType())){
			sbQuery.append("	and p.shortInteger = :instrumentType");
			parameters.put("instrumentType", filter.getInstrumentType());
		}
		sbQuery.append("	group by scv.idSecurityClassFk");
		return findListByQueryString(sbQuery.toString(), parameters);
	}
	
	
	/**
	 * Gets the list security class valuator result to service.
	 *
	 * @param filter the filter
	 * @return the list security class valuator result to service
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SecurityClassValuatorResultTO> getListSecurityClassValuatorResultTOService(SecurityClassValuatorResultTO filter)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select new  com.pradera.securities.valuator.classes.to.SecurityClassValuatorResultTO("+
						" scv.idSecurityValuatorPk,"+
						" scv.idSecurityClassFk,"+
						" vpc.idValuatorClassPk,"+
						" scv.formuleType) "+
						" From SecurityClassValuator scv ");
		sbQuery.append(" Inner Join  scv.valuatorProcessClass vpc ");
		sbQuery.append(" where 1=1 ");
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdSecurityClassFk())){
			sbQuery.append("	AND scv.idSecurityClassFk = :securityClassFk");
		}
		Query query = this.em.createQuery(sbQuery.toString());
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdSecurityClassFk())){
			query.setParameter("securityClassFk", filter.getIdSecurityClassFk());
		}
		return (List<SecurityClassValuatorResultTO>)query.getResultList();	
		
	}
	
	/**
	 * Gets the valuator classes by sec class service.
	 *
	 * @param secClassValuator the sec class valuator
	 * @return the valuator classes by sec class service
	 */
	@SuppressWarnings("unchecked")
	public List<SecurityClassValuator> getValuatorClassesBySecClassService(SecurityClassValuator secClassValuator) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select scv from SecurityClassValuator scv join fetch scv.valuatorProcessClass vpc");
		sbQuery.append(" where 1=1 ");
		if(Validations.validateIsNotNullAndNotEmpty(secClassValuator.getIdSecurityClassFk())){
			sbQuery.append("	AND scv.idSecurityClassFk = :securityClassFk");
		}
		Query query = this.em.createQuery(sbQuery.toString());
		
		if(Validations.validateIsNotNullAndNotEmpty(secClassValuator.getIdSecurityClassFk())){
			query.setParameter("securityClassFk", secClassValuator.getIdSecurityClassFk());
		}
		return (List<SecurityClassValuator>)query.getResultList();	
	}
	
	/**
	 * Gets the valuator process class by pk service.
	 *
	 * @param idValuatorClassPk the id valuator class pk
	 * @return the valuator process class by pk service
	 * @throws ServiceException the service exception
	 */
	public ValuatorProcessClass getValuatorProcessClassByPkService(String idValuatorClassPk) throws ServiceException{
		Map<String,Object> param=new HashMap<String,Object>();
		param.put("idValuatorClassPrm", idValuatorClassPk);
		ValuatorProcessClass valuatorProcessClass=null;
		try{
			valuatorProcessClass=findObjectByNamedQuery(ValuatorProcessClass.SEARCH_VALUATOR_PROCESS_CLASS,param);
		} catch (NoResultException nre) {
			valuatorProcessClass=null;
		}
		return valuatorProcessClass;
	}
	
	/**
	 * Register list security class.
	 *
	 * @param lstSecurityClassValuator the lst security class valuator
	 * @throws ServiceException the service exception
	 */
	public void registerListSecurityClass(List<SecurityClassValuator> lstSecurityClassValuator)throws ServiceException{
		for(SecurityClassValuator obj : lstSecurityClassValuator){
			create(obj);			
		}
	}
	
	/**
	 * Update securities class valuator service.
	 *
	 * @param lstScv the lst scv
	 * @param filter the filter
	 * @throws ServiceException the service exception
	 */
	public void updateSecuritiesClassValuatorService(List<SecurityClassValuator> lstScv,SecurityClassValuatorResultTO filter)throws ServiceException{
		List<SecurityClassValuatorResultTO> listTO = getListSecurityClassValuatorResultTOService(filter);
		Map<Long, SecurityClassValuatorResultTO> mapTO=new HashMap<Long,SecurityClassValuatorResultTO>();
		for(SecurityClassValuatorResultTO objTO : listTO){
			mapTO.put(objTO.getIdSecurityValuatorPk(), objTO);
		}
		
		for(int i=0;i<lstScv.size();i++){
			SecurityClassValuator scv =lstScv.get(i);
			if(Validations.validateIsNotNull(mapTO.get(scv.getIdSecurityValuatorPk()))){ //elimino de las listas los repetidos( los que no sufrieron cambios)
				listTO.remove(mapTO.get(scv.getIdSecurityValuatorPk()));
				mapTO.remove(scv.getIdSecurityValuatorPk());
				lstScv.remove(i);	
				i--;
			}
		}
		
		if(lstScv.size()>GeneralConstants.ZERO_VALUE_INT){
			for(SecurityClassValuator scv : lstScv){
				create(scv);
			}
		}
		if(listTO.size()>GeneralConstants.ZERO_VALUE_INT){
			for(SecurityClassValuatorResultTO objTO : listTO){
				delete(SecurityClassValuator.class, objTO.getIdSecurityValuatorPk());
			}
		}
	}
	
	/**
	 * Delete securities class valuator service.
	 *
	 * @param filter the filter
	 * @throws ServiceException the service exception
	 */
	public void deleteSecuritiesClassValuatorService(SecurityClassValuatorResultTO filter)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("delete from SecurityClassValuator scv ");
		if(Validations.validateIsNotNull(filter.getIdSecurityClassFk())){
			sbQuery.append("where scv.idSecurityClassFk = :securityClassFk");
			Query query = this.em.createQuery(sbQuery.toString());
			query.setParameter("securityClassFk", filter.getIdSecurityClassFk());
			query.executeUpdate();
		}
	}
	
	/**
	 * Gets the valuator process class.
	 *
	 * @param security the security
	 * @param lstCboSecurityClassValuator the lst cbo security class valuator
	 * @return the valuator process class
	 */
	public ValuatorProcessClass getValuatorProcessClass(Security security,List<SecurityClassValuator> lstCboSecurityClassValuator) {
		
		if(security.getIdSecurityCodePk()!=null && security.getValuatorProcessClass()!=null){
			return security.getValuatorProcessClass();
		}
		
		if(lstCboSecurityClassValuator== null){
			SecurityClassValuator sec=new SecurityClassValuator();
			sec.setIdSecurityClassFk(security.getSecurityClass());
			lstCboSecurityClassValuator = getValuatorClassesBySecClassService(sec);
		}
		
		if(lstCboSecurityClassValuator.size()==GeneralConstants.ONE_VALUE_INTEGER.intValue()){
			SecurityClassValuator secClassVal=lstCboSecurityClassValuator.get(0);
			return secClassVal.getValuatorProcessClass();
		}else{
			return findValuatorProcessClass(security);
		}
	}
	
	/**
	 * Find valuator process class.
	 *
	 * @param security the security
	 * @return the valuator process class
	 */
	public ValuatorProcessClass findValuatorProcessClass(Security security) {
		Integer interestType = security.getInterestType();
		SecurityInvestor securityInvestor = security.getSecurityInvestor();
		Integer indFci = securityInvestor.getIndCfi();
		Integer indZeroCupon = BooleanType.NO.getCode();
		Integer indFixedInterest = BooleanType.NO.getCode();
		Integer indVariableInterst = BooleanType.NO.getCode();
		Integer secClassTrg = security.getIdSecurityClassTrg();
		Integer indConvertibleStock = security.getIndConvertibleStock();
		Integer secClass = security.getSecurityClass();
		Integer convStockType = security.getConvertibleStockType();
		
		if(interestType!=null &&
				interestType.equals(InterestType.FIXED.getCode())){
			indFixedInterest = BooleanType.YES.getCode();
		}else if(interestType!=null &&
				interestType.equals(InterestType.VARIABLE.getCode())){
			indVariableInterst = BooleanType.YES.getCode();
		}
		
		InterestPaymentSchedule interestSchedule = security.getInterestPaymentSchedule();
		if(interestSchedule!=null && interestSchedule.getProgramInterestCoupons()!=null){
			if(interestSchedule.getProgramInterestCoupons().size() <= BooleanType.YES.getCode()){
				indZeroCupon = BooleanType.YES.getCode();
			}
		}
		
		if(convStockType==null){
			convStockType = BooleanType.NO.getCode();
		}
		
		if(secClassTrg==null){
			secClassTrg = BooleanType.NO.getCode();
		}
		
		if(indConvertibleStock.equals(BooleanType.NO.getCode())){
			convStockType = BooleanType.NO.getCode();
			secClassTrg   = BooleanType.NO.getCode();
		}
		
		return automaticValuatorClass(indFixedInterest, indVariableInterst, indFci, secClass, convStockType,  indZeroCupon, secClassTrg);
	}
	
	/**
	 * *
	 * 	Method to get ValuatorProcessClass according with the distinct parameters .
	 *
	 * @param indFixed the ind fixed
	 * @param indVariable the ind variable
	 * @param indFci the ind fci
	 * @param secClass the sec class
	 * @param convType the conv type
	 * @param indZeroCoupon the ind zero coupon
	 * @param secClassTrg the sec class trg
	 * @return the valuator process class
	 */
	public ValuatorProcessClass automaticValuatorClass(Integer indFixed, Integer indVariable, Integer indFci, Integer secClass, Integer convType,
													   Integer indZeroCoupon, Integer secClassTrg) {
		
		/**Find valuatorClass according with the parameters one by one*/
		List<ValuatorProcessClass> lstValuatorClass = getValuatorClassFromSecurityClass(secClass);
		lstValuatorClass = getValuatorClass(indFixed, null, null, null, null, null, lstValuatorClass);
		lstValuatorClass = getValuatorClass(null, indVariable, null, null, null, null, lstValuatorClass);
		lstValuatorClass = getValuatorClass(null, null, indFci, null, null, null, lstValuatorClass);
		lstValuatorClass = getValuatorClass(null, null, null, secClassTrg, null, null, lstValuatorClass);
		lstValuatorClass = getValuatorClass(null, null, null, null, convType, null, lstValuatorClass);
		lstValuatorClass = getValuatorClass(null, null, null, null, null, indZeroCoupon, lstValuatorClass);
		
		if(lstValuatorClass.size() == BigDecimal.ONE.intValue()){
			return lstValuatorClass.get(0);
		}
		
		return new ValuatorProcessClass();
	}
	
	/**
	 * *
	 * Method to get All valuatorProcessClass according with the parameters.
	 *
	 * @param securityClass the security class
	 * @return the valuator class from security class
	 */
	public List<ValuatorProcessClass> getValuatorClassFromSecurityClass(Integer securityClass) {
		Set<ValuatorProcessClass> lstSecurityClass = new HashSet<>();
		
		for(SecurityClassValuator secClassValuator: valuatorSetup.getSecurityClassValuators()){
			if(securityClass!=null){
				if(secClassValuator.getIdSecurityClassFk().equals(securityClass)){
					lstSecurityClass.add(secClassValuator.getValuatorProcessClass());
				}
			}else{
				lstSecurityClass.add(secClassValuator.getValuatorProcessClass());
			}
		}
		return new ArrayList<>(lstSecurityClass);
	}
	
	/**
	 * *
	 * Method to get ValuatorProcessClass according with the paremeters entered.
	 *
	 * @param indFixed the ind fixed
	 * @param indVariable the ind variable
	 * @param indFci the ind fci
	 * @param secClassTrg the sec class trg
	 * @param convType the conv type
	 * @param indZeroCoupon the ind zero coupon
	 * @param lstClass the lst class
	 * @return the valuator class
	 */
	public List<ValuatorProcessClass> getValuatorClass(Integer indFixed, Integer indVariable, Integer indFci, Integer secClassTrg,
													   Integer convType, Integer indZeroCoupon, List<ValuatorProcessClass> lstClass){
		Set<ValuatorProcessClass> lstSecurityClass = new HashSet<>();
		
		for(ValuatorProcessClass valuatorClass: lstClass){
			/**Verified if class is fixed instrument*/
			if(indFixed!=null){
				if(valuatorClass.getIndFixedCoupon().equals(indFixed)){
					lstSecurityClass.add(valuatorClass);
				}
			}
			
			/**Verified if class is variable instrument*/
			if(indVariable!=null){
				if(valuatorClass.getIndVariableCoupon().equals(indVariable)){
					lstSecurityClass.add(valuatorClass);
				}
			}
			
			/**Verified if class is FCI*/
			if(indFci!=null){
				if(valuatorClass.getIndCfi().equals(indFci)){
					lstSecurityClass.add(valuatorClass);
				}
			}
			
			/**Verified if class is CONVERTIBLE IS STOCK*/
			if(convType!=null){
				if(valuatorClass.getIdConvertibleType().equals(convType)){
					lstSecurityClass.add(valuatorClass);
				}
			}
			
			/**Verified if securityClass Target it's the same than valuatorClass*/
			if(secClassTrg!=null){
				if(secClassTrg.equals(valuatorClass.getIdSecurityClassTrg())){
					lstSecurityClass.add(valuatorClass);
				}
			}
			
			/**Verified if have zero coupon*/
			if(indZeroCoupon!=null){
				if(indZeroCoupon.equals(valuatorClass.getIndZeroCoupon())){
					lstSecurityClass.add(valuatorClass);
				}
			}
		}
		return new ArrayList<>(lstSecurityClass);
	}
	
	/**
	 * Gets the valuator security code by security.
	 *
	 * @param security the security
	 * @return the valuator security code by security
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ValuatorSecurityCode> getValuatorSecurityCodeBySecurity(Security security) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select Distinct(vsc) from ValuatorSecurityCode vsc");
		sbQuery.append(" join fetch vsc.valuatorCode vc");
		sbQuery.append(" Left join fetch vsc.valuatorSecurityMarketfacts vsm");
		sbQuery.append(" Left join fetch vsc.valuatorTermRange vtr");
		sbQuery.append(" where vsc.security.idSecurityCodePk=:idSecurityCodePrm ");
		sbQuery.append(" Order By vsc.idValuatorSecurityPk	Desc"); 
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSecurityCodePrm", security.getIdSecurityCodePk());
		List<ValuatorSecurityCode> lstResul= (List<ValuatorSecurityCode>)query.getResultList();
		return lstResul;
	}
}


