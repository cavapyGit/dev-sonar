package com.pradera.securities.valuator.core.files.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.pradera.model.valuatorprocess.ValuatorMarketfactConsolidat;
import com.pradera.model.valuatorprocess.ValuatorMarketfactMechanism;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorMarketfactConsolidatTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class ValuatorMarketfactConsolidatTo implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id valuator consolidat pk. */
	private Long idValuatorConsolidatPk;
	
	/** The average price. */
	private BigDecimal averagePrice;
	
	/** The average rate. */
	private BigDecimal averageRate;
	
	/** The information date. */
	private Date informationDate;
	
	/** The marketfact date. */
	private Date marketfactDate;
	
	/** The marketfact active. */
	private String marketfactActive;
	
	/** The minimun amount. */
	private BigDecimal minimunAmount;
	
	/** The negotiation amount. */
	private BigDecimal negotiationAmount;
	
	/** The security class. */
	private String securityClass;
	
	/** The security code. */
	private String securityCode;
	
	/** The valuator code. */
	private String valuatorCode;
	
	/** The valuator type. */
	private String valuatorType;
	
	/** The marketfact type. */
	private Integer marketfactType;
	
	/** The registry date. */
	private Date registryDate;
	
	/** The registry user. */
	private String registryUser;
	
	/** The valuator marketfact mechanisms. */
	private List<ValuatorMarketfactMechanism> valuatorMarketfactMechanisms;
	
	/** The valuator marketfact consolidat. */
	private ValuatorMarketfactConsolidat valuatorMarketfactConsolidat;

	
	/**
	 * Instantiates a new valuator marketfact consolidat to.
	 *
	 * @param averagePrice the average price
	 * @param averageRate the average rate
	 * @param negotiationAmount the negotiation amount
	 * @param informationDate the information date
	 * @param marketfactDate the marketfact date
	 * @param minimunAmount the minimun amount
	 * @param valuatorType the valuator type
	 * @param marketfactActive the marketfact active
	 * @param securityClass the security class
	 * @param securityCode the security code
	 * @param valuatorCode the valuator code
	 */
	public ValuatorMarketfactConsolidatTo(
			double averagePrice,
			double averageRate,
			double negotiationAmount, 
			Date informationDate,
			Date marketfactDate,	
			BigDecimal minimunAmount,
			String valuatorType,  
			String marketfactActive, 
			String securityClass, 
			String securityCode,
			String valuatorCode) {
		super();
		this.averagePrice = BigDecimal.valueOf(averagePrice);
		this.averageRate = BigDecimal.valueOf(averageRate);
		this.negotiationAmount = BigDecimal.valueOf(negotiationAmount);
		this.informationDate = informationDate;
		this.marketfactDate = marketfactDate;
		this.minimunAmount = minimunAmount;
		this.valuatorType = valuatorType;
		this.marketfactActive = marketfactActive;
		this.securityClass = securityClass;
		this.securityCode = securityCode;
		this.valuatorCode = valuatorCode;

	}
	
	/**
	 * Instantiates a new valuator marketfact consolidat to.
	 */
	public ValuatorMarketfactConsolidatTo() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Gets the valuator marketfact consolidat.
	 *
	 * @return the valuator marketfact consolidat
	 */
	public ValuatorMarketfactConsolidat getValuatorMarketfactConsolidat() {
		valuatorMarketfactConsolidat= new ValuatorMarketfactConsolidat();
		valuatorMarketfactConsolidat.setAveragePrice(averagePrice);
		valuatorMarketfactConsolidat.setAverageRate(averageRate);
		valuatorMarketfactConsolidat.setNegotiationAmount(negotiationAmount);
		valuatorMarketfactConsolidat.setInformationDate(informationDate);
		valuatorMarketfactConsolidat.setMarketfactDate(marketfactDate);
		valuatorMarketfactConsolidat.setMinimunAmount(minimunAmount);
		valuatorMarketfactConsolidat.setValuatorType(valuatorType);
		valuatorMarketfactConsolidat.setMarketfactActive(marketfactActive);
		valuatorMarketfactConsolidat.setSecurityClass(securityClass);
		valuatorMarketfactConsolidat.setSecurityCode(securityCode);
		valuatorMarketfactConsolidat.setValuatorCode(valuatorCode);

		return valuatorMarketfactConsolidat;
	}
	

	/**
	 * Gets the id valuator consolidat pk.
	 *
	 * @return the id valuator consolidat pk
	 */
	public Long getIdValuatorConsolidatPk() {
		return idValuatorConsolidatPk;
	}

	/**
	 * Sets the id valuator consolidat pk.
	 *
	 * @param idValuatorConsolidatPk the new id valuator consolidat pk
	 */
	public void setIdValuatorConsolidatPk(Long idValuatorConsolidatPk) {
		this.idValuatorConsolidatPk = idValuatorConsolidatPk;
	}

	/**
	 * Gets the average price.
	 *
	 * @return the average price
	 */
	public BigDecimal getAveragePrice() {
		return averagePrice;
	}

	/**
	 * Sets the average price.
	 *
	 * @param averagePrice the new average price
	 */
	public void setAveragePrice(BigDecimal averagePrice) {
		this.averagePrice = averagePrice;
	}

	/**
	 * Gets the average rate.
	 *
	 * @return the average rate
	 */
	public BigDecimal getAverageRate() {
		return averageRate;
	}

	/**
	 * Sets the average rate.
	 *
	 * @param averageRate the new average rate
	 */
	public void setAverageRate(BigDecimal averageRate) {
		this.averageRate = averageRate;
	}

	/**
	 * Gets the information date.
	 *
	 * @return the information date
	 */
	public Date getInformationDate() {
		return informationDate;
	}

	/**
	 * Sets the information date.
	 *
	 * @param informationDate the new information date
	 */
	public void setInformationDate(Date informationDate) {
		this.informationDate = informationDate;
	}

	/**
	 * Gets the marketfact date.
	 *
	 * @return the marketfact date
	 */
	public Date getMarketfactDate() {
		return marketfactDate;
	}

	/**
	 * Sets the marketfact date.
	 *
	 * @param marketfactDate the new marketfact date
	 */
	public void setMarketfactDate(Date marketfactDate) {
		this.marketfactDate = marketfactDate;
	}

	/**
	 * Gets the marketfact active.
	 *
	 * @return the marketfact active
	 */
	public String getMarketfactActive() {
		return marketfactActive;
	}

	/**
	 * Sets the marketfact active.
	 *
	 * @param marketfactActive the new marketfact active
	 */
	public void setMarketfactActive(String marketfactActive) {
		this.marketfactActive = marketfactActive;
	}

	/**
	 * Gets the marketfact type.
	 *
	 * @return the marketfact type
	 */
	public Integer getMarketfactType() {
		return marketfactType;
	}

	/**
	 * Sets the marketfact type.
	 *
	 * @param marketfactType the new marketfact type
	 */
	public void setMarketfactType(Integer marketfactType) {
		this.marketfactType = marketfactType;
	}

	/**
	 * Gets the minimun amount.
	 *
	 * @return the minimun amount
	 */
	public BigDecimal getMinimunAmount() {
		return minimunAmount;
	}

	/**
	 * Sets the minimun amount.
	 *
	 * @param minimunAmount the new minimun amount
	 */
	public void setMinimunAmount(BigDecimal minimunAmount) {
		this.minimunAmount = minimunAmount;
	}

	/**
	 * Gets the negotiation amount.
	 *
	 * @return the negotiation amount
	 */
	public BigDecimal getNegotiationAmount() {
		return negotiationAmount;
	}

	/**
	 * Sets the negotiation amount.
	 *
	 * @param negotiationAmount the new negotiation amount
	 */
	public void setNegotiationAmount(BigDecimal negotiationAmount) {
		this.negotiationAmount = negotiationAmount;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the security class.
	 *
	 * @return the security class
	 */
	public String getSecurityClass() {
		return securityClass;
	}

	/**
	 * Sets the security class.
	 *
	 * @param securityClass the new security class
	 */
	public void setSecurityClass(String securityClass) {
		this.securityClass = securityClass;
	}

	/**
	 * Gets the security code.
	 *
	 * @return the security code
	 */
	public String getSecurityCode() {
		return securityCode;
	}

	/**
	 * Sets the security code.
	 *
	 * @param securityCode the new security code
	 */
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	/**
	 * Gets the valuator code.
	 *
	 * @return the valuator code
	 */
	public String getValuatorCode() {
		return valuatorCode;
	}

	/**
	 * Sets the valuator code.
	 *
	 * @param valuatorCode the new valuator code
	 */
	public void setValuatorCode(String valuatorCode) {
		this.valuatorCode = valuatorCode;
	}

	/**
	 * Gets the valuator type.
	 *
	 * @return the valuator type
	 */
	public String getValuatorType() {
		return valuatorType;
	}

	/**
	 * Sets the valuator type.
	 *
	 * @param valuatorType the new valuator type
	 */
	public void setValuatorType(String valuatorType) {
		this.valuatorType = valuatorType;
	}

	/**
	 * Gets the valuator marketfact mechanisms.
	 *
	 * @return the valuator marketfact mechanisms
	 */
	public List<ValuatorMarketfactMechanism> getValuatorMarketfactMechanisms() {
		return valuatorMarketfactMechanisms;
	}

	/**
	 * Sets the valuator marketfact mechanisms.
	 *
	 * @param valuatorMarketfactMechanisms the new valuator marketfact mechanisms
	 */
	public void setValuatorMarketfactMechanisms(
			List<ValuatorMarketfactMechanism> valuatorMarketfactMechanisms) {
		this.valuatorMarketfactMechanisms = valuatorMarketfactMechanisms;
	}
	
}
