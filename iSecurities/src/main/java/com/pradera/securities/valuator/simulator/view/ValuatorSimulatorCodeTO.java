package com.pradera.securities.valuator.simulator.view;

import java.math.BigDecimal;

import com.pradera.commons.utils.GenericDataModel;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorSimulatorCodeTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class ValuatorSimulatorCodeTO {

	/** The id simulator code pk. */
	private Long idSimulatorCodePk;
	
	/** The id valuator code pk. */
	private Long idValuatorCodePk;

	/** The id valuator code. */
	private String idValuatorCode;

	/** The average rate. */
	private BigDecimal averageRate;

	/** The security to. */
	private GenericDataModel<ValuatorSimulatorSecurityTO> securityTO;

	/**
	 * Instantiates a new valuator simulator code to.
	 */
	public ValuatorSimulatorCodeTO() {}

	/**
	 * Instantiates a new valuator simulator code to.
	 *
	 * @param idValuatorCodePk the id valuator code pk
	 * @param idValuatorCode the id valuator code
	 */
	public ValuatorSimulatorCodeTO(Long idValuatorCodePk, String idValuatorCode) {
		super();
		this.idValuatorCodePk = idValuatorCodePk;
		this.idValuatorCode = idValuatorCode;
	}
		
	/**
	 * Instantiates a new valuator simulator code to.
	 *
	 * @param idSimulatorCodePk the id simulator code pk
	 * @param idValuatorCode the id valuator code
	 * @param averageRate the average rate
	 */
	public ValuatorSimulatorCodeTO(Long idSimulatorCodePk, String idValuatorCode, BigDecimal averageRate) {
		this.idSimulatorCodePk = idSimulatorCodePk;
		this.idValuatorCode = idValuatorCode;
		this.averageRate = averageRate;
	}

	/**
	 * Gets the id simulator code pk.
	 *
	 * @return the id simulator code pk
	 */
	public Long getIdSimulatorCodePk() {
		return idSimulatorCodePk;
	}

	/**
	 * Sets the id simulator code pk.
	 *
	 * @param idSimulatorCodePk the new id simulator code pk
	 */
	public void setIdSimulatorCodePk(Long idSimulatorCodePk) {
		this.idSimulatorCodePk = idSimulatorCodePk;
	}

	/**
	 * Gets the id valuator code pk.
	 *
	 * @return the id valuator code pk
	 */
	public Long getIdValuatorCodePk() {
		return idValuatorCodePk;
	}

	/**
	 * Sets the id valuator code pk.
	 *
	 * @param idValuatorCodePk the new id valuator code pk
	 */
	public void setIdValuatorCodePk(Long idValuatorCodePk) {
		this.idValuatorCodePk = idValuatorCodePk;
	}

	/**
	 * Gets the id valuator code.
	 *
	 * @return the id valuator code
	 */
	public String getIdValuatorCode() {
		return idValuatorCode;
	}

	/**
	 * Sets the id valuator code.
	 *
	 * @param idValuatorCode the new id valuator code
	 */
	public void setIdValuatorCode(String idValuatorCode) {
		this.idValuatorCode = idValuatorCode;
	}

	/**
	 * Gets the average rate.
	 *
	 * @return the average rate
	 */
	public BigDecimal getAverageRate() {
		return averageRate;
	}

	/**
	 * Sets the average rate.
	 *
	 * @param averageRate the new average rate
	 */
	public void setAverageRate(BigDecimal averageRate) {
		this.averageRate = averageRate;
	}

	/**
	 * Gets the security to.
	 *
	 * @return the security to
	 */
	public GenericDataModel<ValuatorSimulatorSecurityTO> getSecurityTO() {
		return securityTO;
	}

	/**
	 * Sets the security to.
	 *
	 * @param securityTO the new security to
	 */
	public void setSecurityTO(
			GenericDataModel<ValuatorSimulatorSecurityTO> securityTO) {
		this.securityTO = securityTO;
	}
}