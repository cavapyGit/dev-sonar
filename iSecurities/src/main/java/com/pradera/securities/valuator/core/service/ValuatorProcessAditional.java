package com.pradera.securities.valuator.core.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.SessionContext;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;

import com.pradera.commons.configuration.ValuatorSetup;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.custody.affectation.type.AffectationStateType;
import com.pradera.model.custody.affectation.type.LevelAffectationType;
import com.pradera.model.custody.type.AccreditationOperationStateType;
import com.pradera.model.funds.BeginEndDay;
import com.pradera.model.issuancesecuritie.AmortizationPaymentSchedule;
import com.pradera.model.issuancesecuritie.InterestPaymentSchedule;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.ProgramAmortizationCoupon;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.valuatorprocess.SecurityClassValuator;
import com.pradera.model.valuatorprocess.ValuatorCode;
import com.pradera.model.valuatorprocess.ValuatorExecutionDetail;
import com.pradera.model.valuatorprocess.ValuatorExecutionParameters;
import com.pradera.model.valuatorprocess.ValuatorMarketfactFile;
import com.pradera.model.valuatorprocess.ValuatorPriceResult;
import com.pradera.model.valuatorprocess.ValuatorProcessClass;
import com.pradera.model.valuatorprocess.ValuatorProcessExecution;
import com.pradera.model.valuatorprocess.ValuatorProcessSetup;
import com.pradera.model.valuatorprocess.ValuatorSecurityCode;
import com.pradera.model.valuatorprocess.ValuatorSecurityMarketfact;
import com.pradera.model.valuatorprocess.type.ValuatorDetailType;
import com.pradera.model.valuatorprocess.type.ValuatorExecutionStateType;
import com.pradera.model.valuatorprocess.type.ValuatorFormuleType;
import com.pradera.model.valuatorprocess.type.ValuatorMarkFactFileType;
import com.pradera.model.valuatorprocess.type.ValuatorType;
import com.pradera.securities.valuator.core.view.ValuatorInconsistenciesTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorProcessAditional.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
public class ValuatorProcessAditional extends CrudDaoServiceBean {

	/** The valuator setup. */
	@Inject @ValuatorSetup
	private ValuatorProcessSetup valuatorSetup;
	
	/** The ctx. */
	@Resource
	protected SessionContext ctx;
	
	@Inject
	private PraderaLogger log;

	/**
	 * Find market fact balance.
	 *
	 * @param securityCode the security code
	 * @param participant the participant
	 * @param holderAccount the holder account
	 * @return the holder market fact balance
	 * @throws ServiceException the service exception
	 */
	public HolderMarketFactBalance findMarketFactBalance(String securityCode,
			Long participant, Long holderAccount) throws ServiceException {
		return new HolderMarketFactBalance();
	}
	
	/**
	 * Creates the valuator security code.
	 *
	 * @param valuatorSecurityCode the valuator security code
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.WRITE)
	public void createValuatorSecurityCode(ValuatorSecurityCode valuatorSecurityCode, LoggerUser loggerUser) throws ServiceException{
		create(valuatorSecurityCode,loggerUser);
	}
	
	/**
	 * Creates the valuator security code.
	 *
	 * @param valuatorSecurityMarketfact the valuator security marketfact
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.WRITE)
	public void createValuatorSecurityCode(ValuatorSecurityMarketfact valuatorSecurityMarketfact, LoggerUser loggerUser) throws ServiceException{
		create(valuatorSecurityMarketfact,loggerUser);
	}

	/**
	 * Find security for valuator code.
	 *
	 * @param securityCode the security code
	 * @param securityClass the security class
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	@Lock(LockType.READ)
	public List<Security> findSecurityForValuatorCode(List<String> securityCode, Integer securityClass) throws ServiceException {
		StringBuilder querySQL = new StringBuilder();
		querySQL.append("Select sec.issuer.mnemonic, sec.expirationDate, sec.currency,			");
		querySQL.append("		sec.indSubordinated, sec.indEarlyRedemption,					");
		querySQL.append("		sec.interestRate, sec.circulationBalance, sec.instrumentType,	");
		querySQL.append("		sec.valuatorProcessClass.idValuatorClassPk,						");
		querySQL.append("		sec.idSecurityCodePk, sec.issuanceDate, 						");
		querySQL.append("		sec.currentNominalValue, sec.securitySerial,					");
		querySQL.append("		sec.indPrepaid													");
		querySQL.append("From Security sec														");
		querySQL.append("Where sec.idSecurityCodePk IN (:securityCode)							");

		Query query = em.createQuery(querySQL.toString());
		query.setParameter("securityCode", securityCode);
		query.setHint("org.hibernate.cacheable", Boolean.TRUE);
		return populateSecurityForValuatorCode(query.getResultList(),securityCode,securityClass);
	}

	/**
	 * Populate security for valuator code.
	 *
	 * @param data the data
	 * @param securityCode the security code
	 * @param securityClass the security class
	 * @return the list
	 */
	public List<Security> populateSecurityForValuatorCode(List<Object[]> data, List<String> securityCode, Integer securityClass) {
		List<Security> securities = new ArrayList<Security>();
		for (int i = 0; i < data.size(); i++) {
			Object[] valueData = data.get(i);

			Issuer issuer = new Issuer();
			issuer.setMnemonic((String) valueData[0]);

			Security security = new Security();
			security.setExpirationDate((Date) valueData[1]);
			security.setCurrency((Integer) valueData[2]);
			security.setIndSubordinated((Integer) valueData[3]);
			security.setIndEarlyRedemption((Integer) valueData[4]);
			security.setInterestRate((BigDecimal) valueData[5]);
			security.setCirculationBalance((BigDecimal) valueData[6]);
			security.setInstrumentType((Integer) valueData[7]);
			security.setIssuer(issuer);
			security.setAmortizationPaymentSchedule(new AmortizationPaymentSchedule());
			security.setInterestPaymentSchedule(new InterestPaymentSchedule());
			security.setValuatorProcessClass(new ValuatorProcessClass((String) valueData[8]));
			security.setIdSecurityCodePk((String) valueData[9]);
			security.setIssuanceDate((Date) valueData[10]);
			security.setCurrentNominalValue((BigDecimal) valueData[11]);
			security.setSecuritySerial((String) valueData[12]);
			security.setIndPrepaid((Integer) valueData[13]);
			security.setSecurityClass(securityClass);
			securities.add(security);
		}
		return securities;
	}

	/**
	 * Gets the fixed valuator code.
	 *
	 * @param securityCodePk the security code pk
	 * @param valuatorType the valuator type
	 * @param mnemonic the mnemonic
	 * @param currency the currency
	 * @param range the range
	 * @param prepaid the prepaid
	 * @param subOrdinate the sub ordinate
	 * @return the fixed valuator code
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public String getFixedValuatorCode(String securityCodePk,String valuatorType, String mnemonic, String currency, String range, String prepaid, 
									   String subOrdinate) throws ServiceException {
		StringBuilder valuatorCode = new StringBuilder();
		/** Implementando la Circular ASFI 625 */
		if(mnemonic.equals("CLA")){
			mnemonic = "BME";
		}
		/** CONCAT ALL ELEMENTS TO GENERATE VALUATOR CODE */						
		valuatorCode.append(valuatorType).append(mnemonic).append(currency).append(range).append(prepaid).append(subOrdinate).toString();
		/** VALIDATE IF VALUATOR CODE GENERATED IT'S THE SAME LENGTH SETTED */
		if (valuatorSetup.getLengthCodeFixed().equals(valuatorCode.length())) {
			return valuatorCode.toString();
		}
		throw new ServiceException(ErrorServiceType.VALUATOR_CORE_PROCESS_ERROR_VALUATOR_CODE,new String[]{securityCodePk,valuatorCode.toString()});
	}

	/**
	 * Gets the variable valuator code.
	 *
	 * @param securityCodePk the security code pk
	 * @param valuatorType the valuator type
	 * @param mnemonic the mnemonic
	 * @param currency the currency
	 * @param serie the serie
	 * @return the variable valuator code
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public String getVariableValuatorCode(String securityCodePk, String valuatorType, String mnemonic, String currency, String serie)
			throws ServiceException {
		StringBuilder valuatorCode = new StringBuilder();
		/** CONCAT ALL ELEMENTS TO GENERATE VALUATOR CODE */
		valuatorCode.append(valuatorType).append(mnemonic).append(currency).append(serie).toString();
		/** VALIDATE IF VALUATOR CODE GENERATED IT'S THE SAME LENGTH SETTED */
		if (valuatorSetup.getLengthCodeVariable().equals(valuatorCode.length())) {
			return valuatorCode.toString();
		}
		throw new ServiceException(ErrorServiceType.VALUATOR_CORE_PROCESS_ERROR_VALUATOR_CODE,new String[]{securityCodePk,valuatorCode.toString()});
	}

	/**
	 * Exist valuator code.
	 *
	 * @param valuatorCode the valuator code
	 * @return the valuator code
	 * @throws ServiceException the service exception
	 */
	public ValuatorCode existValuatorCode(String valuatorCode) throws ServiceException {
		StringBuilder querySQL = new StringBuilder();
		querySQL.append("Select new com.pradera.model.valuatorprocess.ValuatorCode(				");
		querySQL.append("			valCod.idValuatorCodePk, valCod.idValuatorCode)				");
		querySQL.append("From ValuatorCode valCod												");
		querySQL.append("Where valCod.idValuatorCode = :valuatorCode							");

		Query query = em.createQuery(querySQL.toString());
		query.setParameter("valuatorCode", valuatorCode);

		try {
			return (ValuatorCode) query.getSingleResult();
		} catch (NoResultException ex) {
			return null;
		}
	}

	/**
	 * Find valuator process.
	 *
	 * @param valuatorClassCode the valuator class code
	 * @return the valuator process class
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public ValuatorProcessClass findValuatorProcess(String valuatorClassCode) throws ServiceException {
		if(valuatorClassCode!=null){
			Map<String,ValuatorProcessClass> valuatorClassList = valuatorSetup.getValuatorProcessClass();
			return valuatorClassList.get(valuatorClassCode);
		}
		return null;
	}

	/**
	 * Gets the valuator code on map.
	 *
	 * @param instrumentType the instrument type
	 * @return the valuator code on map
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	@Lock(LockType.READ)
	public ConcurrentMap<Long, String> getValuatorCodeOnMap(Integer instrumentType) throws ServiceException {
		StringBuilder querySQL = new StringBuilder();
		querySQL.append("Select Distinct valCod.idValuatorCodePk, 				");
		querySQL.append("				 valCod.idValuatorCode	 				");
		querySQL.append("From ValuatorCode valCod								");
		querySQL.append("Where Length(valCod.idValuatorCode) = :valCodeSize		");
		
		Query query = em.createQuery(querySQL.toString());
		if(instrumentType.equals(InstrumentType.FIXED_INCOME.getCode())){
			query.setParameter("valCodeSize", valuatorSetup.getLengthCodeFixed());
		}else if(instrumentType.equals(InstrumentType.VARIABLE_INCOME.getCode())){
			query.setParameter("valCodeSize", valuatorSetup.getLengthCodeVariable());
		}
		
		List<Object[]> resultData = null;
		try {
			resultData = query.getResultList();
			for (Object[] arraData : resultData) {
				Long valuatorCodePk = (Long) arraData[0];
				String valuatorCode = (String) arraData[1];
				
				if(valuatorSetup.getValuatorCodeMap()==null){
					valuatorSetup.setValuatorCodeMap(new ConcurrentHashMap<Long,String>());
				}
				
				if(valuatorSetup.getValuatorCodeMap().get(valuatorCodePk)==null){
					valuatorSetup.getValuatorCodeMap().put(valuatorCodePk,valuatorCode);
				}
			}
		} catch (NoResultException ex) {
			
		}

		return valuatorSetup.getValuatorCodeMap();
	}

	/**
	 * Gets the valuator security code on map.
	 *
	 * @param securtyCodes the securty codes
	 * @param processPk the process pk
	 * @param instrumentType the instrument type
	 * @return the valuator security code on map
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public ConcurrentMap<String, Long> getValuatorSecurityCodeOnMap(List<String> securtyCodes,Long processPk, Integer instrumentType)throws ServiceException {
		Map<String,Object> parameters = new HashMap<>();
		StringBuilder subQuerySQL = new StringBuilder();
		subQuerySQL.append("Select  Max(_valSec.idValuatorSecurityPk) 					");
		subQuerySQL.append("From ValuatorSecurityCode _valSec							");
		subQuerySQL.append("Where _valSec.security.idSecurityCodePk In(					");		
		
		/****************/
		
		ValuatorProcessExecution valuatorProcess = find(ValuatorProcessExecution.class, processPk);
		Integer valuatorType = valuatorProcess.getValuatorType();
		
		StringBuilder _querySQL = new StringBuilder();
		StringBuilder _whereSQL = new StringBuilder();
		
		_querySQL.append("SELECT DISTINCT	sec.idSecurityCodePk 									");
		_querySQL.append("FROM HolderAccountBalance				hab									");
		_querySQL.append("INNER JOIN hab.security 				sec									");
		_whereSQL.append("Where 1 = 1																");
		_whereSQL.append("AND sec.instrumentType = :instrumentType									");
//		_whereSQL.append("AND sec.idSecurityCodePk Like 'DPF-FIEE00%'								");
		parameters.put("instrumentType", instrumentType);
		
		if(valuatorType.equals(ValuatorType.BY_SECURITY.getCode())){
			_whereSQL.append("AND sec.idSecurityCodePk IN (											");
			_whereSQL.append("Select par.security.idSecurityCodePk									");
			_whereSQL.append("From ValuatorExecutionParameters par									");
			_whereSQL.append("Where par.valuatorProcessExecution.idProcessExecutionPk = :processPk)	");
			parameters.put("processPk", processPk);
		}
		
		if(valuatorType.equals(ValuatorType.BY_INVESTOR.getCode())){
			_querySQL.append("INNER JOIN hab.holderAccount			ha								");
			
			_whereSQL.append("AND ha.idHolderAccountPk IN (											");
			_whereSQL.append("Select par.holderAccount.idHolderAccountPk							");
			_whereSQL.append("From ValuatorExecutionParameters par									");
			_whereSQL.append("Where par.valuatorProcessExecution.idProcessExecutionPk = :processPk)	");
			parameters.put("processPk", processPk);
		}
		
		if(valuatorType.equals(ValuatorType.BY_PARTICIPANT.getCode())){
			_whereSQL.append("AND hab.participant.idParticipantPk IN (								");
			_whereSQL.append("Select par.participant.idParticipantPk								");
			_whereSQL.append("From ValuatorExecutionParameters par									");
			_whereSQL.append("Where par.valuatorProcessExecution.idProcessExecutionPk = :processPk)	");
			parameters.put("processPk", processPk);
		}
			
		subQuerySQL.append(_querySQL.append(_whereSQL));
		/***************/
		
		subQuerySQL.append(")	Group By _valSec.security.idSecurityCodePk							");
		
		StringBuilder querySQL = new StringBuilder();
		querySQL.append("Select valSec.idValuatorSecurityPk, 				 			");
		querySQL.append("		valSec.security.idSecurityCodePk,	 					");
		querySQL.append("		valSec.valuatorCode.idValuatorCodePk	 				");
		
		/**If process is fixed income*/
		if(instrumentType.equals(InstrumentType.FIXED_INCOME.getCode())){
			querySQL.append("	,valSec.valuatorTermRange.idTermRangePk					");
		}
		
		querySQL.append("From ValuatorSecurityCode valSec								");
		querySQL.append("Where   valSec.idValuatorSecurityPk IN (						");
		querySQL.append(subQuerySQL.toString()).append(			")						");

		List<Object[]> resultData = findListByQueryString(querySQL.toString(),parameters);
		ConcurrentMap<String, Long> mapData = new ConcurrentHashMap<String, Long>();

		for (Object[] arraData : resultData) {
			Long value = (Long) arraData[0];
			String key = arraData[1].toString() + arraData[2].toString() ;
			
			if(instrumentType.equals(InstrumentType.FIXED_INCOME.getCode())){
				key+=arraData[3].toString();
			}
			mapData.put(key, value);
		}
		return mapData;
	}
	
	/**
	 * Calculate equivalent rate.
	 *
	 * @param security the security
	 * @param marketRate the market rate
	 * @param marketDate the market date
	 * @param valuatorDate the valuator date
	 * @return the big decimal
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public BigDecimal calculateEquivalentRate(Security security, BigDecimal marketRate, Date marketDate, Date valuatorDate) throws ServiceException{
		
		if(security==null || security.getValuatorProcessClass()==null){
			return BigDecimal.ZERO;
		}
		
		String valuatorClass = security.getValuatorProcessClass().getIdValuatorClassPk();		
		Integer formuleType = findValuatorProcess(valuatorClass).getFormuleType();
		
		if(formuleType.equals(ValuatorFormuleType.WITHOUT_COUPONS.getCode())){
			Date expirationDate = security.getExpirationDate();
			BigDecimal poq = new BigDecimal(CommonsUtilities.getDaysBetween(marketDate, expirationDate));
			BigDecimal peq = new BigDecimal(CommonsUtilities.getDaysBetween(valuatorDate, expirationDate));
			BigDecimal trPorcent = CommonsUtilities.getPercentOnDecimals(marketRate);
			return marketRate;//ValuatorFormuls.getTRE(poq, peq, trPorcent).multiply(new BigDecimal(100));
		}else{
			return marketRate;
		}
	}

	/**
	 * Calculate economic term.
	 *
	 * @param security the security
	 * @param marketRate the market rate
	 * @param marketDate the market date
	 * @param valuatorDate the valuator date
	 * @return the big decimal
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public BigDecimal calculateEconomicTerm(Security security, BigDecimal marketRate, Date marketDate, Date valuatorDate) throws ServiceException {
		
		if(security==null || security.getValuatorProcessClass()==null){
			return BigDecimal.ZERO;
		}
		
		String valuatorClass = security.getValuatorProcessClass().getIdValuatorClassPk();		
		Integer formuleType = findValuatorProcess(valuatorClass).getFormuleType();
		
		if(formuleType.equals(ValuatorFormuleType.WITH_COUPONS.getCode())){			
			List<ProgramInterestCoupon> interestCouponList = security.getInterestPaymentSchedule().getProgramInterestCoupons();
			List<ProgramAmortizationCoupon> amortizationCouponList = security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons();
			
//			log.info("VALOR: "+security.getIdSecurityCodePk());
//			log.info("interestCouponList: "+interestCouponList.size() + ", amortizationCouponList: "+amortizationCouponList.size());
			if(interestCouponList.isEmpty() || amortizationCouponList.isEmpty()){
				throw new ServiceException(ErrorServiceType.VALUATOR_CORE_PROCESS_EMPTY_COUPONS, new String[]{security.getIdSecurityCodePk()});
			}
			
			List<BigDecimal> fcList = new ArrayList<BigDecimal>();
			List<BigDecimal> plList = new ArrayList<BigDecimal>();
			List<BigDecimal> fcPlcList = new ArrayList<BigDecimal>();
			
			Collections.sort(interestCouponList,COMPARE_BY_COUPON_NUMBER);
			for(ProgramInterestCoupon interesCoupon: interestCouponList){
				ProgramAmortizationCoupon amortizationCoupon = getAmortizationFromDate(interesCoupon, amortizationCouponList);
				
				Date expDate  = interesCoupon.getExperitationDate();
				Date begDate  = interesCoupon.getBeginingDate();
				Date expADate = amortizationCoupon.getExpirationDate();
				Integer days  = CommonsUtilities.getDaysBetween(begDate, expDate);
				BigDecimal amAmount = amortizationCoupon.getCouponAmount();
				BigDecimal intAmount = interesCoupon.getCouponAmount();
				Integer devengementDays = CommonsUtilities.getDaysBetween(valuatorDate, expDate);
				
				if(begDate.before(valuatorDate)){
					days = CommonsUtilities.getDaysBetween(valuatorDate, expDate);
				}
				
				BigDecimal vci = BigDecimal.ZERO;
				/**If interest coupon have pending state it's okay to use in formula's price*/
				if(interesCoupon.getStateProgramInterest().equals(ProgramScheduleStateType.PENDING.getCode())){
					vci = vci.add(intAmount);
				}
				/**If interest coupon is the last coupon, we add nominal value of capital amortization*/
				if(expADate.equals(expDate)){
					vci = vci.add(amAmount);
				}
				
				plList.add(new BigDecimal(days));
				fcList.add(vci);
				fcPlcList.add(new BigDecimal(devengementDays).multiply(vci));
			}
			
			BigDecimal economicTerm = null;
			try{
				BigDecimal tr = CommonsUtilities.getPercentOnDecimals(marketRate);
//				economicTerm = ValuatorFormuls.getPEit_V_2(fcList, tr, plList, fcPlcList); 
//				economicTerm = ValuatorFormuls.getPEit(fcList, tr, plList, fcPlcList);
			}catch(ArithmeticException ex){
				throw new ServiceException(ErrorServiceType.VALUATOR_CORE_PROCESS_ERROR_TE,new String[]{security.getIdSecurityCodePk()});
			}
			return economicTerm;
		}else{
			return new BigDecimal(CommonsUtilities.getDaysBetween(valuatorDate, security.getExpirationDate()));
		}
	}

	/**
	 * Method to calculate price according with marketFact rate and date.
	 *
	 * @param security the security
	 * @param operation the operation
	 * @param formuleType the formule type
	 * @param marketRate the market rate
	 * @param marketDate the market date
	 * @param valuatorDate the valuator date
	 * @param execDetailType the exec detail type
	 * @return the big decimal
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public BigDecimal calculateMarketPrice(Security security, MechanismOperation operation, Integer formuleType, BigDecimal marketRate, 
									 	   Date marketDate, Date valuatorDate, Integer execDetailType) throws ServiceException {
		BigDecimal price = BigDecimal.ZERO;

		if(execDetailType.equals(ValuatorDetailType.SECURITY.getCode())){
			/**Validate if market rate is null*/
			if(marketRate == null){
				String securityCodePk = security!=null?security.getIdSecurityCodePk():null;
				throw new ServiceException(ErrorServiceType.VALUATOR_CORE_PROCESS_MARKET_RATE_INCONSISTENTE,new String[]{securityCodePk});
			}
			/**Begin process*/
			if (formuleType.equals(ValuatorFormuleType.WITHOUT_COUPONS.getCode())) {
				Date expirationDate = security.getExpirationDate();
				Date issuanceDate	= security.getIssuanceDate();
				BigDecimal poq = new BigDecimal(CommonsUtilities.getDaysBetween(marketDate, expirationDate));
				BigDecimal pl  = new BigDecimal(CommonsUtilities.getDaysBetween(issuanceDate, expirationDate));
				BigDecimal peq = new BigDecimal(CommonsUtilities.getDaysBetween(valuatorDate, expirationDate));
				BigDecimal trPorcent = CommonsUtilities.getPercentOnDecimals(marketRate);
//				BigDecimal tre = ValuatorFormuls.getTRE(poq, peq, trPorcent);
				BigDecimal vfi = CommonsUtilities.roundAmount(security.getCurrentNominalValue());
				BigDecimal tr = security.getInterestRate();

				if (tr != null && !tr.equals(BigDecimal.ZERO)) {
					tr = CommonsUtilities.getPercentOnDecimals(tr);
//					vfi = ValuatorFormuls.getVFi(vfi, tr, pl);
					vfi = CommonsUtilities.roundAmount(vfi);
				}
//				price = ValuatorFormuls.getPit(vfi, tre, peq);
			} else if (formuleType.equals(ValuatorFormuleType.WITH_COUPONS.getCode())) {
				/***/
				List<ProgramInterestCoupon> interestCouponList = security.getInterestPaymentSchedule().getProgramInterestCoupons();
				List<ProgramAmortizationCoupon> amortizationCouponList = security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons();
				
				List<BigDecimal> fcList = new ArrayList<BigDecimal>();
				List<BigDecimal> plList = new ArrayList<BigDecimal>();
				
//				log.info("VALOR: "+security.getIdSecurityCodePk());
//				log.info("interestCouponList: "+interestCouponList.size() + ", amortizationCouponList: "+amortizationCouponList.size());
				if(interestCouponList.isEmpty() || amortizationCouponList.isEmpty()){
					throw new ServiceException(ErrorServiceType.VALUATOR_CORE_PROCESS_EMPTY_COUPONS, new String[]{security.getIdSecurityCodePk()});
				}
				
				Collections.sort(interestCouponList,COMPARE_BY_COUPON_NUMBER);
				for(ProgramInterestCoupon interesCoupon: interestCouponList){
					ProgramAmortizationCoupon amortizationCoupon = getAmortizationFromDate(interesCoupon, amortizationCouponList);
					
					Date expDate  = interesCoupon.getExperitationDate();
					Date begDate  = interesCoupon.getBeginingDate();
					Date expADate = amortizationCoupon.getExpirationDate();
					Integer days  = CommonsUtilities.getDaysBetween(begDate, expDate);
					BigDecimal amAmount  = amortizationCoupon.getCouponAmount();
					BigDecimal intAmount = interesCoupon.getCouponAmount();
//					BigDecimal nomValue = amortizationCoupon.getNominalValue();
					
//					BigDecimal tr = CommonsUtilities.getPercentOnDecimals(security.getInterestRate());
					BigDecimal vci = BigDecimal.ZERO;
					
					/**If interest coupon have pending state it's okay to use in formula's price*/
					if(interesCoupon.getStateProgramInterest().equals(ProgramScheduleStateType.PENDING.getCode())){
						vci = vci.add(intAmount);
					}
					/**If interest coupon is the last coupon, we add nominal value of capital amortization*/
					if(expADate.equals(expDate)){
						vci = vci.add(amAmount);
					}
					
					if(begDate.before(valuatorDate)){
						days = CommonsUtilities.getDaysBetween(valuatorDate, expDate);
					}
					plList.add(new BigDecimal(days));
					fcList.add(vci);
				}
				try{
					BigDecimal tr = CommonsUtilities.getPercentOnDecimals(marketRate);
//					ValuatorFormuls.getPitFormule(fcList, tr, plList);
//					price = ValuatorFormuls.getPitFormuleOther(fcList, tr, plList);
				}catch(ArithmeticException ex){
					throw new ServiceException(ErrorServiceType.VALUATOR_CORE_PROCESS_ERROR_PRICE,new String[]{security.getIdSecurityCodePk()});
				}
			}
		}else if(execDetailType.equals(ValuatorDetailType.OPERATIONS.getCode())){
			BigDecimal vfo 			= operation.getCashPrice();
			
			//issue 617, no existia tasa para la valoracion
			//ssue 1035, multiples hechos de mercado para la valoracion, si existe mas de uno se toma el primero es todo. 
			if(operation.getOriginRequest().equals(NegotiationMechanismType.SIRTEX.getCode())){
				
				List<Object []> test = amountRateCashPriceToSirtex(operation.getIdMechanismOperationPk());
				operation.setAmountRate((BigDecimal) (test.get(0)[0]));
				vfo = (BigDecimal) (test.get(0)[1]);
			}
			
			BigDecimal tre 			= CommonsUtilities.getPercentOnDecimals(operation.getAmountRate());
			Date issuancDate		= operation.getOperationDate();
			Integer operationTerm	= CommonsUtilities.getDaysBetween(issuancDate,valuatorDate);
//			price 		   			= ValuatorFormuls.getVFi(vfo, tre, new BigDecimal(operationTerm));
		}
		return price;
	}
	
	/**
	 * Metodo para extraer el valor nominal a una fecha determinada
	 * @return
	 */
	public BigDecimal historicNominalValue(Date valuatorDate, String security) {
		
			StringBuilder querySql = new StringBuilder();
			
			querySql.append(" select DISTINCT NOMINAL_VALUE                  ");
			querySql.append(" from NOMINAL_VALUE_HISTORY                     ");
			querySql.append(" where ID_SECURITY_CODE_FK = :security  ");
			querySql.append(" and CUT_DATE = :valuatorDate                    ");
	
			Query query = em.createNativeQuery(querySql.toString());
			
			query.setParameter("valuatorDate", valuatorDate);
			query.setParameter("security", security);
			
			BigDecimal bdResult = null;
			
			try {
				bdResult = (BigDecimal)query.getSingleResult();
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			return bdResult;
	}
	
	/**
	 * Method to calculate price according with marketFact rate and date.
	 *
	 * @param security the security
	 * @param operation the operation
	 * @param formuleType the formule type
	 * @param marketRate the market rate
	 * @param marketDate the market date
	 * @param valuatorDate the valuator date
	 * @param execDetailType the exec detail type
	 * @return the big decimal
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public BigDecimal calculateMarketPriceReport(Security security, MechanismOperation operation, Integer formuleType, BigDecimal marketRate, 
									 	   Date marketDate, Date valuatorDate, Integer execDetailType, Integer indHistoric, Integer indAsfi) throws ServiceException {
		BigDecimal price = BigDecimal.ZERO;

		if(execDetailType.equals(ValuatorDetailType.SECURITY.getCode())){
			/**Validate if market rate is null*/
			if(marketRate == null){
				String securityCodePk = security!=null?security.getIdSecurityCodePk():null;
				throw new ServiceException(ErrorServiceType.VALUATOR_CORE_PROCESS_MARKET_RATE_INCONSISTENTE,new String[]{securityCodePk});
			}
			/**Begin process*/
			if (formuleType.equals(ValuatorFormuleType.WITHOUT_COUPONS.getCode())) {
				Date expirationDate = security.getExpirationDate();
				Date issuanceDate	= security.getIssuanceDate();
				
				BigDecimal poq = null;
				//Depende si se calcula precio curva Asfi o precio subyacente
				if(indAsfi.equals(GeneralConstants.ONE_VALUE_INTEGER)) {
					poq = new BigDecimal(CommonsUtilities.getDaysBetween(issuanceDate, expirationDate));
				}else {
					poq = new BigDecimal(CommonsUtilities.getDaysBetween(marketDate, expirationDate));
				}
				
				BigDecimal pl  = new BigDecimal(CommonsUtilities.getDaysBetween(issuanceDate, expirationDate));
				BigDecimal peq = new BigDecimal(CommonsUtilities.getDaysBetween(valuatorDate, expirationDate));
				BigDecimal trPorcent = CommonsUtilities.getPercentOnDecimals(marketRate);
				
				BigDecimal tre = null;
				Integer peqInt = peq.intValue();
				//Depende si se calcula precio curva Asfi o precio subyacente
				if(indAsfi.equals(GeneralConstants.ONE_VALUE_INTEGER)) {
					tre = trPorcent;
				}else {
					//Si es historico extraemos el tre se calcula dependiendo del peq
					if(peqInt.equals(GeneralConstants.ZERO_VALUE_INTEGER)) {
						tre = trPorcent;
					}else {
//						tre = ValuatorFormuls.getTRE(poq, peq, trPorcent);
					}
				}
				
				BigDecimal vfi = null;
				BigDecimal nominalHistory = null;
				//Si es historico extraemos el valor nominal de esa fecha
				if(indHistoric.equals(GeneralConstants.ONE_VALUE_INTEGER)) {
					if(peqInt.equals(GeneralConstants.ZERO_VALUE_INTEGER)) {
						nominalHistory = historicNominalValue(CommonsUtilities.addDate(valuatorDate, -1),security.getIdSecurityCodePk());
						 if(!Validations.validateIsNotNullAndNotEmpty(nominalHistory)) {
							 nominalHistory = historicNominalValue(CommonsUtilities.addDate(valuatorDate, -2),security.getIdSecurityCodePk());
						 }
					}else {
						nominalHistory = historicNominalValue(valuatorDate,security.getIdSecurityCodePk());
						if(!Validations.validateIsNotNullAndNotEmpty(nominalHistory)) {
							 nominalHistory = historicNominalValue(CommonsUtilities.addDate(valuatorDate, -1),security.getIdSecurityCodePk());
						 }
					}
					vfi = CommonsUtilities.roundAmount(nominalHistory);
				}else {
					if(peqInt.equals(GeneralConstants.ZERO_VALUE_INTEGER)) {
						nominalHistory = historicNominalValue(CommonsUtilities.addDate(valuatorDate, -1),security.getIdSecurityCodePk());
						if(!Validations.validateIsNotNullAndNotEmpty(nominalHistory)) {
							 nominalHistory = historicNominalValue(CommonsUtilities.addDate(valuatorDate, -2),security.getIdSecurityCodePk());
						 }
						vfi = CommonsUtilities.roundAmount(nominalHistory);
					}else {
						vfi = CommonsUtilities.roundAmount(security.getCurrentNominalValue());
					}
				}
				
				BigDecimal tr = security.getInterestRate();

				if (tr != null && !tr.equals(BigDecimal.ZERO)) {
					tr = CommonsUtilities.getPercentOnDecimals(tr);
//					vfi = ValuatorFormuls.getVFi(vfi, tr, pl);
					vfi = CommonsUtilities.roundAmount(vfi);
				}
//				price = ValuatorFormuls.getPit(vfi, tre, peq);
			} else if (formuleType.equals(ValuatorFormuleType.WITH_COUPONS.getCode())) {
				/***/
				List<ProgramInterestCoupon> interestCouponList = security.getInterestPaymentSchedule().getProgramInterestCoupons();
				List<ProgramAmortizationCoupon> amortizationCouponList = security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons();
				
				List<BigDecimal> fcList = new ArrayList<BigDecimal>();
				List<BigDecimal> plList = new ArrayList<BigDecimal>();
				
//				log.info("VALOR: "+security.getIdSecurityCodePk());
//				log.info("interestCouponList: "+interestCouponList.size() + ", amortizationCouponList: "+amortizationCouponList.size());
				if(interestCouponList.isEmpty() || amortizationCouponList.isEmpty()){
					throw new ServiceException(ErrorServiceType.VALUATOR_CORE_PROCESS_EMPTY_COUPONS, new String[]{security.getIdSecurityCodePk()});
				}
				
				Collections.sort(interestCouponList,COMPARE_BY_COUPON_NUMBER);
				
				//Si el calculo del precio sera historico
				if (indHistoric.equals(GeneralConstants.ONE_VALUE_INTEGER)) {
					
					Integer indExpired = 0;
					
					for(ProgramInterestCoupon interesCoupon: interestCouponList){
						ProgramAmortizationCoupon amortizationCoupon = getAmortizationFromDate(interesCoupon, amortizationCouponList);
						
						Date expDate  = interesCoupon.getExperitationDate();
						Date begDate  = interesCoupon.getBeginingDate();
						Date expADate = amortizationCoupon.getExpirationDate();
						Integer days  = CommonsUtilities.getDaysBetween(begDate, expDate);
						BigDecimal amAmount  = amortizationCoupon.getCouponAmount();
						BigDecimal intAmount = interesCoupon.getCouponAmount();
//						BigDecimal nomValue = amortizationCoupon.getNominalValue();
						
//						BigDecimal tr = CommonsUtilities.getPercentOnDecimals(security.getInterestRate());
						BigDecimal vci = BigDecimal.ZERO;
						
						/**If interest coupon have pending state it's okay to use in formula's price*/
//						if(interesCoupon.getStateProgramInterest().equals(ProgramScheduleStateType.PENDING.getCode())){
//							
//						}
						
						//En caso de ser historico los cupones vencidos deben ser tomados en cuenta desde la fecha del calculo
						if (indExpired.equals(GeneralConstants.ZERO_VALUE_INTEGER)) {
							if(interesCoupon.getStateProgramInterest().equals(ProgramScheduleStateType.EXPIRED.getCode())){
								if(CommonsUtilities.addDate(valuatorDate, 1).after(begDate)
										&& CommonsUtilities.addDate(valuatorDate, -1).before(expDate)) {
									vci = vci.add(intAmount);
									indExpired = 1;
								}
							}else {
								vci = vci.add(intAmount);
							}
						}else {
							vci = vci.add(intAmount);
						}
						
						/**If interest coupon is the last coupon, we add nominal value of capital amortization*/
						if(expADate.equals(expDate)){
							vci = vci.add(amAmount);
						}
						
						if(begDate.before(valuatorDate)){
							days = CommonsUtilities.getDaysBetween(valuatorDate, expDate);
						}
						plList.add(new BigDecimal(days));
						fcList.add(vci);
					}
				//Si el calculo de precio es diario	
				}else {
					for(ProgramInterestCoupon interesCoupon: interestCouponList){
						ProgramAmortizationCoupon amortizationCoupon = getAmortizationFromDate(interesCoupon, amortizationCouponList);
						
						Date expDate  = interesCoupon.getExperitationDate();
						Date begDate  = interesCoupon.getBeginingDate();
						Date expADate = amortizationCoupon.getExpirationDate();
						Integer days  = CommonsUtilities.getDaysBetween(begDate, expDate);
						BigDecimal amAmount  = amortizationCoupon.getCouponAmount();
						BigDecimal intAmount = interesCoupon.getCouponAmount();
//						BigDecimal nomValue = amortizationCoupon.getNominalValue();
						
//						BigDecimal tr = CommonsUtilities.getPercentOnDecimals(security.getInterestRate());
						BigDecimal vci = BigDecimal.ZERO;
						
						/**If interest coupon have pending state it's okay to use in formula's price*/
						if(interesCoupon.getStateProgramInterest().equals(ProgramScheduleStateType.PENDING.getCode())){
							vci = vci.add(intAmount);
						}
						/**If interest coupon is the last coupon, we add nominal value of capital amortization*/
						if(expADate.equals(expDate)){
							vci = vci.add(amAmount);
						}
						
						if(begDate.before(valuatorDate)){
							days = CommonsUtilities.getDaysBetween(valuatorDate, expDate);
						}
						plList.add(new BigDecimal(days));
						fcList.add(vci);
					}
				}
				
				try{
					BigDecimal tr = CommonsUtilities.getPercentOnDecimals(marketRate);
//					ValuatorFormuls.getPitFormule(fcList, tr, plList);
//					price = ValuatorFormuls.getPitFormuleOther(fcList, tr, plList);
				}catch(ArithmeticException ex){
					throw new ServiceException(ErrorServiceType.VALUATOR_CORE_PROCESS_ERROR_PRICE,new String[]{security.getIdSecurityCodePk()});
				}
			}
		}else if(execDetailType.equals(ValuatorDetailType.OPERATIONS.getCode())){
			BigDecimal vfo 			= operation.getCashPrice();
			
			//issue 617, no existia tasa para la valoracion
			//ssue 1035, multiples hechos de mercado para la valoracion, si existe mas de uno se toma el primero es todo. 
			if(operation.getOriginRequest().equals(NegotiationMechanismType.SIRTEX.getCode())){
				
				List<Object []> test = amountRateCashPriceToSirtex(operation.getIdMechanismOperationPk());
				operation.setAmountRate((BigDecimal) (test.get(0)[0]));
				vfo = (BigDecimal) (test.get(0)[1]);
			}
			
			BigDecimal tre 			= CommonsUtilities.getPercentOnDecimals(operation.getAmountRate());
			Date issuancDate		= operation.getOperationDate();
			Integer operationTerm	= CommonsUtilities.getDaysBetween(issuancDate,valuatorDate);
//			price 		   			= ValuatorFormuls.getVFi(vfo, tre, new BigDecimal(operationTerm));
		}
		return price;
	}
	
	/**
	 * Calculate economic term.
	 *
	 * @param security the security
	 * @param marketRate the market rate
	 * @param marketDate the market date
	 * @param valuatorDate the valuator date
	 * @return the big decimal
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public BigDecimal calculateEconomicTermReport (Security security, BigDecimal marketRate, Date marketDate, Date valuatorDate, Integer indHistoric) throws ServiceException {
		
		if(security==null || security.getValuatorProcessClass()==null){
			return BigDecimal.ZERO;
		}
		
		String valuatorClass = security.getValuatorProcessClass().getIdValuatorClassPk();		
		Integer formuleType = findValuatorProcess(valuatorClass).getFormuleType();
		
		if(formuleType.equals(ValuatorFormuleType.WITH_COUPONS.getCode())){			
			List<ProgramInterestCoupon> interestCouponList = security.getInterestPaymentSchedule().getProgramInterestCoupons();
			List<ProgramAmortizationCoupon> amortizationCouponList = security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons();
			
//			log.info("VALOR: "+security.getIdSecurityCodePk());
//			log.info("interestCouponList: "+interestCouponList.size() + ", amortizationCouponList: "+amortizationCouponList.size());
			if(interestCouponList.isEmpty() || amortizationCouponList.isEmpty()){
				throw new ServiceException(ErrorServiceType.VALUATOR_CORE_PROCESS_EMPTY_COUPONS, new String[]{security.getIdSecurityCodePk()});
			}
			
			List<BigDecimal> fcList = new ArrayList<BigDecimal>();
			List<BigDecimal> plList = new ArrayList<BigDecimal>();
			List<BigDecimal> fcPlcList = new ArrayList<BigDecimal>();
			
			Collections.sort(interestCouponList,COMPARE_BY_COUPON_NUMBER);
			
			//Si el calculo del precio sera historico
			if (indHistoric.equals(GeneralConstants.ONE_VALUE_INTEGER)) {
				
				Integer indExpired = 0;
				
				for(ProgramInterestCoupon interesCoupon: interestCouponList){
					ProgramAmortizationCoupon amortizationCoupon = getAmortizationFromDate(interesCoupon, amortizationCouponList);
					
					Date expDate  = interesCoupon.getExperitationDate();
					Date begDate  = interesCoupon.getBeginingDate();
					Date expADate = amortizationCoupon.getExpirationDate();
					Integer days  = CommonsUtilities.getDaysBetween(begDate, expDate);
					BigDecimal amAmount = amortizationCoupon.getCouponAmount();
					BigDecimal intAmount = interesCoupon.getCouponAmount();
					Integer devengementDays = CommonsUtilities.getDaysBetween(valuatorDate, expDate);
					
					if(begDate.before(valuatorDate)){
						days = CommonsUtilities.getDaysBetween(valuatorDate, expDate);
					}
					
					BigDecimal vci = BigDecimal.ZERO;
					/**If interest coupon have pending state it's okay to use in formula's price*/
//					if(interesCoupon.getStateProgramInterest().equals(ProgramScheduleStateType.PENDING.getCode())){
//						vci = vci.add(intAmount);
//					}
					
					//En caso de ser historico los cupones vencidos deben ser tomados en cuenta desde la fecha del calculo
					if (indExpired.equals(GeneralConstants.ZERO_VALUE_INTEGER)) {
						if(interesCoupon.getStateProgramInterest().equals(ProgramScheduleStateType.EXPIRED.getCode())){
							if(valuatorDate.after(begDate)
									&& valuatorDate.before(expDate)) {
								vci = vci.add(intAmount);
								indExpired = 1;
							}
						}else {
							vci = vci.add(intAmount);
						}
					}else {
						vci = vci.add(intAmount);
					}
					
					
					/**If interest coupon is the last coupon, we add nominal value of capital amortization*/
					if(expADate.equals(expDate)){
						vci = vci.add(amAmount);
					}
					
					plList.add(new BigDecimal(days));
					fcList.add(vci);
					fcPlcList.add(new BigDecimal(devengementDays).multiply(vci));
				}
				
			//Si el calculo de precio es diario	
			}else {
				for(ProgramInterestCoupon interesCoupon: interestCouponList){
					ProgramAmortizationCoupon amortizationCoupon = getAmortizationFromDate(interesCoupon, amortizationCouponList);
					
					Date expDate  = interesCoupon.getExperitationDate();
					Date begDate  = interesCoupon.getBeginingDate();
					Date expADate = amortizationCoupon.getExpirationDate();
					Integer days  = CommonsUtilities.getDaysBetween(begDate, expDate);
					BigDecimal amAmount = amortizationCoupon.getCouponAmount();
					BigDecimal intAmount = interesCoupon.getCouponAmount();
					Integer devengementDays = CommonsUtilities.getDaysBetween(valuatorDate, expDate);
					
					if(begDate.before(valuatorDate)){
						days = CommonsUtilities.getDaysBetween(valuatorDate, expDate);
					}
					
					BigDecimal vci = BigDecimal.ZERO;
					/**If interest coupon have pending state it's okay to use in formula's price*/
					if(interesCoupon.getStateProgramInterest().equals(ProgramScheduleStateType.PENDING.getCode())){
						vci = vci.add(intAmount);
					}
					/**If interest coupon is the last coupon, we add nominal value of capital amortization*/
					if(expADate.equals(expDate)){
						vci = vci.add(amAmount);
					}
					
					plList.add(new BigDecimal(days));
					fcList.add(vci);
					fcPlcList.add(new BigDecimal(devengementDays).multiply(vci));
				}
			}
			
			
			
			
			BigDecimal economicTerm = null;
			try{
				BigDecimal tr = CommonsUtilities.getPercentOnDecimals(marketRate);
//				economicTerm = ValuatorFormuls.getPEit_V_2(fcList, tr, plList, fcPlcList); 
//				economicTerm = ValuatorFormuls.getPEit(fcList, tr, plList, fcPlcList);
			}catch(ArithmeticException ex){
				throw new ServiceException(ErrorServiceType.VALUATOR_CORE_PROCESS_ERROR_TE,new String[]{security.getIdSecurityCodePk()});
			}
			return economicTerm;
		}else{
			return new BigDecimal(CommonsUtilities.getDaysBetween(valuatorDate, security.getExpirationDate()));
		}
	}
	
	
	/**
	 * Metodo para recuperar la tasa con la que se registro el HM de la operacion
	 * @param idMechanismOperation
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> amountRateCashPriceToSirtex (Long idMechanismOperation){
		
		StringBuilder querySql = new StringBuilder();
		
		querySql.append("  	select  																											 ");
		querySql.append("  	distinct sam.MARKET_RATE, sam.MARKET_PRICE 				                                                       		 ");
		querySql.append("  	from SETTLEMENT_ACCOUNT_MARKETFACT sam                                                                               ");
		querySql.append("  	inner join SETTLEMENT_ACCOUNT_OPERATION sao on sam.ID_SETTLEMENT_ACCOUNT_FK = sao.ID_SETTLEMENT_ACCOUNT_PK           ");
		querySql.append("  	inner join HOLDER_ACCOUNT_OPERATION hao on sao.ID_HOLDER_ACCOUNT_OPERATION_FK = hao.ID_HOLDER_ACCOUNT_OPERATION_PK   ");
		querySql.append("  	inner join MECHANISM_OPERATION mo on hao.ID_MECHANISM_OPERATION_FK = mo.ID_MECHANISM_OPERATION_PK                    ");
		querySql.append("  	where mo.ID_MECHANISM_OPERATION_PK = :idMechanismOperation                                       		             ");
		querySql.append("  	and mo.ID_NEGOTIATION_MECHANISM_FK = 5                                                                               ");
		querySql.append("  	and sam.IND_ACTIVE = 1                                                                                               ");
		querySql.append("  	and hao.OPERATION_PART = 1                                                                                           ");
		querySql.append("  	and sao.role = 1                                                                                                     ");
		querySql.append("  	and hao.HOLDER_ACCOUNT_STATE = 625                                                                                   ");
		querySql.append("  	and mo.OPERATION_STATE = 615                                                                                         ");
		
		Query query = em.createNativeQuery(querySql.toString());
		query.setParameter("idMechanismOperation", idMechanismOperation);
		List<Object[]> marketRatesSirtex = query.getResultList();
		
		return marketRatesSirtex;
	}
	
	/** The compare by coupon number. */
	public static Comparator<ProgramInterestCoupon> COMPARE_BY_COUPON_NUMBER = new Comparator<ProgramInterestCoupon>() {
	 public int compare(ProgramInterestCoupon o1, ProgramInterestCoupon o2) {
			/**ORDENAMOS LOS CUPONES POR NUMERO DE CUPON*/
			int resultado = o1.getCouponNumber()-o2.getCouponNumber();
	        if ( resultado != 0 ) { return resultado; }
			return resultado;
		}
    };
	
    /**
     * Gets the amortization from date.
     *
     * @param interesCoupon the interes coupon
     * @param amortizationList the amortization list
     * @return the amortization from date
     * @throws ServiceException the service exception
     */
    @Lock(LockType.READ)
	public ProgramAmortizationCoupon getAmortizationFromDate(ProgramInterestCoupon interesCoupon, List<ProgramAmortizationCoupon> amortizationList)
																		throws ServiceException{
		Date expirationInteres = interesCoupon.getExperitationDate();
		for(ProgramAmortizationCoupon amortizationCoupon: amortizationList){
			Date expirationAmortization = amortizationCoupon.getExpirationDate();
			if(expirationAmortization.after(expirationInteres) || expirationAmortization.equals(expirationInteres)){
				return amortizationCoupon;
			}
		}
		return null;
	}
	
	/**
	 * *
	 * Method to catch event from marketFactSecurity that generate price by security and rate.
	 *
	 * @param marketFactForEvent the market fact for event
	 */
//	@Asynchronous
//	@Lock(LockType.READ)
//	public void registerMarketFactEvent(@Observes  MarketFactForEvent marketFactForEvent){
//		Integer valuatorType    = marketFactForEvent.getMechanismOperationId()==null?ValuatorDetailType.SECURITY.getCode():ValuatorDetailType.OPERATIONS.getCode();
//		marketFactForEvent.setValuatorOperationType(valuatorType);
//
//		/**CONFIG MAP TO HANDLE KEY VALUE WITH SECURITY AND RATE PRICE BUT TEMPORALLY*/
//		if(valuatorSetup.getSecuritiesRatePriceTmp().contains(marketFactForEvent)) return;
//		
//		valuatorSetup.getSecuritiesRatePriceTmp().add(marketFactForEvent);
//	}
//	
	/**
	 * Validate valuator parameters.
	 *
	 * @param parameters the parameters
	 * @param blValidate the bl validate
	 * @throws ServiceException the service exception
	 */
	public void validateValuatorParameters(List<ValuatorExecutionParameters> parameters, boolean blValidate) throws ServiceException{
		List<String> securities=new ArrayList<String>();
		for(ValuatorExecutionParameters _p: parameters){
			securities.add(_p.getSecurityCode()); 
		}
		
		/**Verified if securities exists in bd*/
		if(blValidate){
			StringBuilder sb = new StringBuilder();
			sb.append("Select Nvl(Count(*),0) From Security Where idSecurityCodePk In (:secCodes)");
			Query query = em.createQuery(sb.toString());	
			query.setParameter("secCodes", securities);
			
			if(securities.size() != ((Long)query.getSingleResult()).intValue()){
				throw new ServiceException(ErrorServiceType.VALUATOR_CORE_PROCESS_INCORRECT_SECURITY,securities.toArray());
			}
		}
		/**Save all securities like parameters on database*/
		saveAll(parameters);
	}
	
	/**
	 * Execute sp.
	 *
	 * @param processExecutionID the process execution id
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void executeSp(Long processExecutionID) throws ServiceException{
		try{
			StringBuilder sb = new StringBuilder();
			sb.append("BEGIN PKG_VALUATOR_PROCESS.SP_EXECUTE_VALUATOR_PROCESS(:pExecutionId);END;");
			Query query = em.createNativeQuery(sb.toString());
	    	query.setParameter("pExecutionId", processExecutionID);
	    	query.executeUpdate();
		}catch(Exception ex){
			ctx.setRollbackOnly();
			throw ex;
		}
	}

	/**
	 * Method to generate Price by rate and date with security.
	 *
	 * @param valuatorExecutionId the valuator execution id
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void processPriceBySecurityRate(Long valuatorExecutionId, LoggerUser loggerUser) throws ServiceException{
		ArrayBlockingQueue<Object> marketFactFromEvent = valuatorSetup.getSecuritiesRatePriceTmp();
		List<Object> marketFactEventToRemove = new ArrayList<>();
		
		List<ValuatorPriceResult> marketFactPriceList = new ArrayList<>();
		for(Object marketFactObject: marketFactFromEvent.toArray()){
//			MarketFactForEvent marketFactForEvent = (MarketFactForEvent)marketFactObject;
//			if(marketFactForEvent.getValuatorExecutionId()!=null && marketFactForEvent.getValuatorExecutionId().equals(valuatorExecutionId)){
//				String securityCode = marketFactForEvent.getSecurityCode();
//				BigDecimal marketRate = marketFactForEvent.getMarketRate();
//				BigDecimal marketPrice = marketFactForEvent.getMarketPrice();
//				Date marketDate = marketFactForEvent.getMarketDate();
//				Integer indHomolog = marketFactForEvent.getIndHomolog();
//				Long operationId = marketFactForEvent.getMechanismOperationId();
//				Date processDate = marketFactForEvent.getProcessDate();
//				BigDecimal valuatorRate = marketFactForEvent.getValuatorRate();
//				BigDecimal economTerm = marketFactForEvent.getEconomicTerm();
//				Long valuatorCodePk   = marketFactForEvent.getValuatorCodPk();
//				
//				marketFactPriceList.add(getValuatorPriceResult(securityCode, marketRate, marketDate, marketPrice, valuatorExecutionId,
//															   indHomolog,operationId,processDate,valuatorRate,economTerm,valuatorCodePk));
//				marketFactEventToRemove.add(marketFactObject);
//			}
		}
//		saveAll(marketFactPriceList, loggerUser);
//		persistForBatch(marketFactPriceList, ValuatorConstants.SIZE_OF_BATCH);
		marketFactFromEvent.removeAll(marketFactEventToRemove);
	}
	
	/**
	 * Exist security in process.
	 *
	 * @param securityCode the security code
	 * @return the boolean
	 */
	@Lock(LockType.READ)
	public Boolean existSecurityInProcess(String securityCode){
		for(Object marketFactEvent: valuatorSetup.getSecuritiesRatePriceTmp()){
//			if(((MarketFactForEvent)marketFactEvent).getSecurityCode().equals(securityCode)) return Boolean.TRUE;
		}
		return Boolean.TRUE;
	}
	
	/**
	 * Exist market price.
	 *
	 * @param securityCode the security code
	 * @param markRate the mark rate
	 * @param operationId the operation id
	 * @param processDate the process date
	 * @param valDetailType the val detail type
	 * @param markDate the mark date
	 * @return the boolean
	 */
	@Lock(LockType.READ)
	public Boolean existMarketPrice(String securityCode, BigDecimal markRate, Long operationId, Date processDate, Integer valDetailType, Date markDate){
//		MarketFactForEvent marketFactEvent = new MarketFactForEvent();
//		marketFactEvent.setSecurityCode(securityCode);
//		marketFactEvent.setMarketRate(markRate);
//		marketFactEvent.setMechanismOperationId(operationId);
//		marketFactEvent.setProcessDate(processDate);
//		marketFactEvent.setValuatorOperationType(valDetailType);
//		marketFactEvent.setMarketDate(markDate);
		return true;//valuatorSetup.getSecuritiesRatePriceTmp().contains(marketFactEvent);
	}
	
	/**
	 * *
	 * Method to populate ValuatorPriceResult and after insert on database.
	 *
	 * @param secCode the sec code
	 * @param markRate the mark rate
	 * @param markDate the mark date
	 * @param markPrice the mark price
	 * @param execId the exec id
	 * @param indHomolog the ind homolog
	 * @param operationId the operation id
	 * @param processDate the process date
	 * @param valuatRate the valuat rate
	 * @param economTerm the econom term
	 * @param valCod the val cod
	 * @return the valuator price result
	 */
	@Lock(LockType.READ)
	private ValuatorPriceResult getValuatorPriceResult(String secCode, BigDecimal markRate, Date markDate, BigDecimal markPrice, Long execId, 
													   Integer indHomolog, Long operationId, Date processDate, BigDecimal valuatRate, BigDecimal economTerm, Long valCod){
		ValuatorPriceResult valuatorPriceResult = new ValuatorPriceResult();
		valuatorPriceResult.setValuatorProcessExecution(new ValuatorProcessExecution(execId));
		valuatorPriceResult.setSecurityCode(secCode);
		valuatorPriceResult.setMarketDate(markDate);
		valuatorPriceResult.setMarketPrice(markPrice);
		valuatorPriceResult.setProcessDate(processDate);
		valuatorPriceResult.setValuatorRate(valuatRate);
		valuatorPriceResult.setEconomicTerm(economTerm);
		valuatorPriceResult.setValuatorCodeFk(valCod);
		
		if(operationId!=null){
			valuatorPriceResult.setMechanismOperationPk(operationId);
			valuatorPriceResult.setMarketRate(BigDecimal.ZERO);
		}else{
			valuatorPriceResult.setMarketRate(markRate);
		}
		valuatorPriceResult.setIndHomologated(indHomolog);
		return valuatorPriceResult;
	}

	/**
	 * Join rate from phys and desmaterial.
	 *
	 * @param physicalRatesMap the physical rates map
	 * @param desmaterializatMap the desmaterializat map
	 * @return the map
	 */
	@Lock(LockType.READ)
	public Map<String, Map<Date,List<BigDecimal>>> joinRateFromPhysAndDesmaterial(Map<String, Map<Date,List<BigDecimal>>> physicalRatesMap, 
																				  Map<String, Map<Date,List<BigDecimal>>> desmaterializatMap) {
		// TODO Auto-generated method stub
		Map<String, Map<Date,List<BigDecimal>>> securityWithRateMap = new HashMap<>();
		
		for(String securityCode: desmaterializatMap.keySet()){
			if(securityWithRateMap.get(securityCode)==null){
				securityWithRateMap.put(securityCode, new HashMap<Date,List<BigDecimal>>());
			}
			for(Date date: desmaterializatMap.get(securityCode).keySet()){
				if(securityWithRateMap.get(securityCode).get(date)==null){
					securityWithRateMap.get(securityCode).put(date, new ArrayList<BigDecimal>());
				}
				securityWithRateMap.get(securityCode).get(date).addAll(desmaterializatMap.get(securityCode).get(date));
			}
		}
		desmaterializatMap.clear();
		
		for(String securityCode: physicalRatesMap.keySet()){
			if(securityWithRateMap.get(securityCode)==null){
				securityWithRateMap.put(securityCode, new HashMap<Date,List<BigDecimal>>());
			}
			for(Date date: physicalRatesMap.get(securityCode).keySet()){
				if(securityWithRateMap.get(securityCode).get(date)==null){
					securityWithRateMap.get(securityCode).put(date, new ArrayList<BigDecimal>());
				}
				securityWithRateMap.get(securityCode).get(date).addAll(physicalRatesMap.get(securityCode).get(date));
			}
		}
		physicalRatesMap.clear();
		
		for(String securityCode: securityWithRateMap.keySet()){
			for(Date date: securityWithRateMap.get(securityCode).keySet()){
				List<BigDecimal> rates = securityWithRateMap.get(securityCode).get(date);
				Set<BigDecimal> rateSet = new HashSet<>();
				for(BigDecimal rate: rates){
					rateSet.add(rate);
				}
				securityWithRateMap.get(securityCode).get(date).addAll(rateSet);
			}
		}
		return securityWithRateMap;
	}
	
	/**
	 * Find formule type.
	 *
	 * @param securityClass the security class
	 * @param valuatorCode the valuator code
	 * @return the integer
	 */
	@Lock(LockType.READ)
	public Integer findFormuleType(Integer securityClass, String valuatorCode){
		try {
			for(SecurityClassValuator classValuator: findValuatorProcess(valuatorCode).getSecurityClassValuators()){
				if(classValuator.getIdSecurityClassFk().equals(securityClass)){
					return classValuator.getFormuleType(); 
				}
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * *
	 * Method to get formuleType from SecurityClassValuator.
	 *
	 * @param securityClass the security class
	 * @return the integer
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public Integer findUniqueFormuleType(Integer securityClass) throws ServiceException{
		List<SecurityClassValuator> securityClassValuator = valuatorSetup.getSecurityClassValuators();
		Integer uniqueFormule = null;
		for(SecurityClassValuator classValuator: securityClassValuator){
			if(securityClass.equals(classValuator.getIdSecurityClassFk())){
				Integer formuleType = classValuator.getFormuleType();
				if(uniqueFormule!=null && !formuleType.equals(uniqueFormule)){
					return null;
				}
				uniqueFormule = formuleType;
			}
		}
		return uniqueFormule;
	}
	
	/**
	 * METHOD TO SAVE VALUATOR_PROCESS_EXECUTION.
	 *
	 * @param _processExecution the _process execution
	 * @param logger the logger
	 * @param blValidate the bl validate
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public void registerProcessExecution(ValuatorProcessExecution _processExecution, LoggerUser logger, boolean blValidate) throws ServiceException{
		ValuatorProcessExecution processExecution = _processExecution;
		
		/**PERSIST JAVA OBJECT ON DATABASE*/
		create(processExecution.getValuatorProcessOperation(),logger);
		create(processExecution,logger);
		
		/**ITERATE VALUATOR_PARAMETERS ON DATABASE*/	
		if(!blValidate)
			saveAll(processExecution.getValuatorExecutionParameters(),logger);
	}
	
	/**
	 * Gets the execution detail.
	 *
	 * @param executionId the execution id
	 * @param eventBehaviorId the event behavior id
	 * @param detailType the detail type
	 * @param secClass the sec class
	 * @return the execution detail
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public ValuatorExecutionDetail getExecutionDetail(Long executionId, Long eventBehaviorId, Integer detailType, Integer secClass) throws ServiceException{
		StringBuilder querySQL = new StringBuilder();
		querySQL.append("Select detail												");
		querySQL.append("From ValuatorExecutionDetail detail						");
		querySQL.append("Inner Join detail.valuatorProcessExecution execution		");
		querySQL.append("Inner Join detail.valuatorEventBehavior behavior			");
		querySQL.append("Where detail.detailType = :detailType	And					");
		querySQL.append("	   execution.idProcessExecutionPk = :executionId And	");
		querySQL.append("	   behavior.idValuatorEventPk = :eventBehaviorId		");
		
		if(secClass!=null){
			querySQL.append("And detail.securityClass = :secClass					");
		}else{
			querySQL.append("And detail.securityClass Is null						");
		}
		

		Query query = em.createQuery(querySQL.toString());
		query.setParameter("detailType", detailType);
		query.setParameter("executionId", executionId);
		query.setParameter("eventBehaviorId", eventBehaviorId);
		
		if(secClass!=null){
			query.setParameter("secClass", secClass);
		}
		
		try{
			return (ValuatorExecutionDetail) query.getSingleResult();
		}catch(NoResultException e){
			return null;
		}
	}
	

	/**
	 * Creates the security class event.
	 *
	 * @param eventCode the event code
	 * @param processPk the process pk
	 * @param detailType the detail type
	 * @param securityClass the security class
	 * @param indBegin the ind begin
	 * @param rowQuantity the row quantity
	 * @param hasError the has error
	 */
	@Asynchronous
	@Lock(LockType.READ)
	public void createSecurityClassEvent(Long eventCode, Long processPk, Integer detailType, Integer securityClass, Integer indBegin, 
																														Integer rowQuantity, Integer hasError){
		
		
		
		try {
			/**Getting object from bd*/
//			ValuatorExecutionDetail executionDetail = getExecutionDetail(processPk, eventCode, detailType, null);
//			/**Getting object of execution detail*/
//			if(executionDetail == null){
				ValuatorExecutionDetail executionDetail = null;
				/**If indicator is begin of process*/
				if(indBegin.equals(BooleanType.YES.getCode())){
//					if(executionDetail == null){
						/**Getting object according with parameters*/
						executionDetail = getExecutionDetail(processPk, eventCode, detailType, null);
						ValuatorExecutionDetail executionSecurityClass = executionDetail.clone();
						executionSecurityClass.setExecutionDetail(executionDetail);
						executionSecurityClass.setSecurityClass(securityClass);
						executionSecurityClass.setIdExecutionDetailPk(null);
						executionSecurityClass.setValuatorExecutionDetails(null);
						executionSecurityClass.setValuatorExecutionFaileds(null);
						executionSecurityClass.setValuatorProcessExecution(new ValuatorProcessExecution(processPk));
						executionSecurityClass.setQuantityProcess(rowQuantity.longValue());
						executionSecurityClass.setQuantityInconsistent(ComponentConstant.ZERO.longValue());
						executionSecurityClass.setQuantityFinished(rowQuantity.longValue());
						executionSecurityClass.setOrderExecution(executionDetail.getValuatorExecutionDetails().size()+1);
						executionSecurityClass.setBeginTime(CommonsUtilities.currentDateTime());
						executionDetail.setProcessState(ValuatorExecutionStateType.VALUATING.getCode());
						/**Create event of security class*/
						create(executionSecurityClass);
//					}
				}else{
					/**Getting object from bd*/
					executionDetail = getExecutionDetail(processPk, eventCode, detailType, securityClass);
					/**Update state and hour of finish*/
					executionDetail.setFinishTime(CommonsUtilities.currentDateTime());
					/**If process has error*/
					if(hasError.equals(BooleanType.YES.getCode())){
						executionDetail.setProcessState(ValuatorExecutionStateType.FINISHED_WITH_ERROR.getCode());
					}else{
						executionDetail.setProcessState(ValuatorExecutionStateType.FINISHED.getCode());
					}
					update(executionDetail);
				}
//			}
		} catch (ServiceException | CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * *
	 * Method to return marketFact files in one day.
	 *
	 * @param processExecution the process execution
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	@SuppressWarnings("unchecked")
	public List<ValuatorMarketfactFile> verifyMarketFile(ValuatorProcessExecution processExecution) throws ServiceException{
		
		/**Getting date from process*/
		Date processDate = processExecution.getProcessDate();
		/**Verify state of process execution*/
		Integer processState = ValuatorMarkFactFileType.RECEPCIONADO.getCode();
		/**Build query to execute*/
		StringBuilder querySql = new StringBuilder();
		querySql.append("Select fil From ValuatorMarketfactFile fil			");
		querySql.append("Where 	fil.fileState 	= :recepted		And			");
		querySql.append("		Trunc(fil.processDate) = Trunc(:processDate)");
		
		Query query = em.createQuery(querySql.toString());
		query.setParameter("recepted", processState);
		query.setParameter("processDate", processDate);
		
		try{
			return query.getResultList();
		}catch(NoResultException ex){
			return new ArrayList<ValuatorMarketfactFile>();
		}
	}
	
	/**
	 * *
	 * Method to verified if begin end day exists in database.
	 *
	 * @param date the date
	 * @return the begin end day
	 * @throws ServiceException the service exception
	 */
	public BeginEndDay searchBeginEnd(Date date) throws ServiceException{
		BeginEndDay beginEndDay = null;
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("SELECT BED 									");
			sbQuery.append("FROM BeginEndDay BED 						");
			sbQuery.append("WHERE trunc(BED.processDay) = trunc(:date) 	");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("date", date);
			beginEndDay = (BeginEndDay) query.getSingleResult();
		}catch(NoResultException | NonUniqueResultException x){
			return null;
		}
		return beginEndDay;
		
	}
	
	/**
	 * *
	 * Method to get all inconsistencies in reporting.
	 *
	 * @param role the role
	 * @return the reporting inconsist
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public List<ValuatorInconsistenciesTO> getReportingInconsist(Integer role) throws ServiceException{
		/**List to handle all inconsistencies on reporting*/
		List<ValuatorInconsistenciesTO> inconsistenciesList = new ArrayList<ValuatorInconsistenciesTO>();
		
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append("Select hao.id_incharge_stock_participant, mo.id_security_code_fk, hao.id_holder_account_fk,							");
		sbQuery.append("	   hmb.market_date As m_date, hmb.market_rate As m_rate, hmb.market_price As m_price,                           ");
		sbQuery.append("       sam.market_date, sam.market_rate, sam.market_price, ha.alternate_code, pa.mnemonic,				            ");
		sbQuery.append("       null as affectation_pk, null as accreditation_pk, null as document_number, 									");
		sbQuery.append("       mo.id_mechanism_operation_pk, mo.ballot_number, mo.sequential, mo.operation_number         					");
		sbQuery.append("From settlement_account_marketfact sam                                                                              ");
		sbQuery.append("Inner Join settlement_account_operation sao on sao.id_settlement_account_pk = sam.id_settlement_account_fk          ");
		sbQuery.append("Inner Join holder_account_operation hao on hao.id_holder_account_operation_pk = sao.id_holder_account_operation_fk  ");
		sbQuery.append("Inner Join holder_account ha on ha.id_holder_account_pk = hao.id_holder_account_fk                                  ");
		sbQuery.append("Inner Join participant pa on pa.id_participant_pk = hao.id_incharge_stock_participant                               ");
		sbQuery.append("Inner Join settlement_operation so on so.id_settlement_operation_pk = sao.id_settlement_operation_fk                ");
		sbQuery.append("Inner Join mechanism_operation mo on mo.id_mechanism_operation_pk = so.id_mechanism_operation_fk                    ");
		sbQuery.append("Inner Join holder_marketfact_balance  hmb on (hmb.id_security_code_fk = mo.id_security_code_fk And                  ");
		sbQuery.append("                                              hmb.id_holder_account_fk = hao.id_holder_account_fk)                  ");
		sbQuery.append("where                                                                                                               ");
		sbQuery.append("      so.operation_part = :report And  mo.ind_reporting_balance = :oneParam And                                     ");
		sbQuery.append("      sao.role = :role  And sam.ind_active = :oneParam And so.operation_state = :opeState And                  		");
		sbQuery.append("      sao.operation_state = :soState  And hmb.ind_active = :oneParam And                                            ");
		sbQuery.append("      Not Exists (Select * From Holder_MarketFact_Balance market							 	                    ");
		sbQuery.append("      			  Where hmb.id_holder_account_fk = market.id_holder_account_fk 	And									");
		sbQuery.append("      			        hmb.id_security_code_fk = market.id_security_code_fk 	And									");
		sbQuery.append("      			  		NVL(sam.market_rate,0) = NVL(market.market_rate,0) 		And									"); 
		sbQuery.append("      			  		NVL(sam.market_price,0) = NVL(market.market_price,0)	And									");
		sbQuery.append("      			  		sam.market_date = market.market_date 														");
		sbQuery.append("      			  )																									");
		
		Query query = null;
		try{
			query = em.createNativeQuery(sbQuery.toString());
		}catch(NoResultException ex){
			return inconsistenciesList;
		}
		
		query.setParameter("role", role);
		query.setParameter("oneParam", ComponentConstant.ONE);
		query.setParameter("report", ComponentConstant.TERM_PART);
		query.setParameter("soState", HolderAccountOperationStateType.CONFIRMED.getCode());
		query.setParameter("opeState", MechanismOperationStateType.CASH_SETTLED.getCode());
		
		@SuppressWarnings("unchecked")
		List<Object[]> objectDataList = query.getResultList();
		for(Object[] data: objectDataList){
			inconsistenciesList.add(populateValuatorInconsist(data,role));
		}
		
		return inconsistenciesList;
	}
	
	/**
	 * *
	 * Method to get all inconsistencies in accreditation.
	 *
	 * @return the accreditation inconsist
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public List<ValuatorInconsistenciesTO> getAccreditationInconsist() throws ServiceException{
		/**List to handle all inconsistencies on accreditation*/
		List<ValuatorInconsistenciesTO> inconsistenciesList = new ArrayList<ValuatorInconsistenciesTO>();
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select ade.id_participant_fk, ade.id_security_code_fk, ade.id_holder_account_fk,								");
		sbQuery.append("	   hmb.market_date As m_date, hmb.market_rate As m_rate, hmb.market_price As m_price,                       ");
		sbQuery.append("	   mrk.market_date, mrk.market_rate, mrk.market_price, ha.alternate_code, pa.mnemonic,				        ");
		sbQuery.append("	   null As affectation_pk, aco.id_accreditation_operation_pk, aco.certificate_number, 						");
		sbQuery.append("	   null as operation_pk, null as ballot, null as sequential, null as operation_number						");
		sbQuery.append("From accreditation_operation aco                                                                                ");
		sbQuery.append("Inner join accreditation_detail ade On ade.id_accreditation_operation_fk = aco.id_accreditation_operation_pk    ");
		sbQuery.append("Inner join holder_account ha On ha.id_holder_account_pk = ade.id_holder_account_fk                              ");
		sbQuery.append("Inner join participant pa On pa.id_participant_pk = ade.id_participant_fk                                       ");
		sbQuery.append("Inner join accreditation_marketfact mrk On mrk.id_accreditation_detail_fk = ade.id_accreditation_detail_pk      ");
		sbQuery.append("Inner join holder_marketfact_balance hmb On (hmb.id_security_code_fk = ade.id_security_code_fk And              ");
		sbQuery.append("											 hmb.id_holder_account_fk = ade.id_holder_account_fk)               ");
		sbQuery.append("Where mrk.ind_active = :oneParam and aco.accreditation_state = :stat  And 									 	");
		sbQuery.append("	  hmb.ind_active = :oneParam And ade.available_balance > :zeroParam											"); 
//		sbQuery.append("	  hmb.ind_active = :oneParam and (hmb.market_date <> mrk.market_date Or hmb.market_rate <> mrk.market_rate) ");
		
		sbQuery.append("	  And  Not Exists ( Select * From Holder_MarketFact_Balance market                                        	");
		sbQuery.append("	      				Where hmb.id_holder_account_fk = market.id_holder_account_fk  							");
		sbQuery.append("	    						And   hmb.id_security_code_fk = market.id_security_code_fk 						"); 
		sbQuery.append("	  							And   NVL(mrk.market_rate,0) = NVL(market.market_rate,0) 						");   
		sbQuery.append("	  							And   NVL(mrk.market_price,0) = NVL(market.market_price,0) 						"); 
		sbQuery.append("	  							And   mrk.market_date = market.market_date )									");
		
		Query query = null;
		try{
			query = em.createNativeQuery(sbQuery.toString());
		}catch(NoResultException ex){
			return inconsistenciesList;
		}
		
		query.setParameter("oneParam", ComponentConstant.ONE);
		query.setParameter("zeroParam", new BigDecimal(ComponentConstant.ZERO));
		query.setParameter("stat", AccreditationOperationStateType.CONFIRMED.getCode());
		
		@SuppressWarnings("unchecked")
		List<Object[]> objectDataList = query.getResultList();
		for(Object[] data: objectDataList){
			inconsistenciesList.add(populateValuatorInconsist(data,null));
		}
		
		return inconsistenciesList;
	}
	
	/**
	 * Gets the rebloking inconsist.
	 *
	 * @return the rebloking inconsist
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	@Lock(LockType.READ)
	public List<ValuatorInconsistenciesTO> getReblokingInconsist() throws ServiceException{
		List<ValuatorInconsistenciesTO> inconsistenciesList = new ArrayList<ValuatorInconsistenciesTO>();
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append("SELECT BO.ID_PARTICIPANT_FK, BO.ID_SECURITY_CODE_FK, BO.ID_HOLDER_ACCOUNT_FK,						"); 
		sbQuery.append("HMB.MARKET_DATE AS M_DATE, HMB.MARKET_RATE AS M_RATE, HMB.MARKET_PRICE AS M_PRICE,                  ");
		sbQuery.append("       MRK.MARKET_DATE, MRK.MARKET_RATE, MRK.MARKET_PRICE,                                          ");
		sbQuery.append("       HA.ALTERNATE_CODE, PA.MNEMONIC,                                                              ");
		sbQuery.append("       BO.ID_BLOCK_OPERATION_PK, NULL AS ACC_PK, BO.DOCUMENT_NUMBER,                                ");
		sbQuery.append("       null as operation_pk, null as ballot, null as sequential, null as operation_number           ");
		sbQuery.append("FROM BLOCK_OPERATION BO                                                                             ");
		sbQuery.append("INNER JOIN HOLDER_MARKETFACT_BALANCE HMB ON HMB.ID_SECURITY_CODE_FK = BO.ID_SECURITY_CODE_FK        ");
		sbQuery.append("INNER JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK = BO.ID_HOLDER_ACCOUNT_FK                   ");
		sbQuery.append("INNER JOIN PARTICIPANT PA ON PA.ID_PARTICIPANT_PK = BO.ID_PARTICIPANT_FK                            ");
		sbQuery.append("INNER JOIN BLOCK_MARKETFACT_OPERATION MRK ON MRK.ID_BLOCK_OPERATION_FK = BO.ID_BLOCK_OPERATION_PK   ");
		sbQuery.append("WHERE BO.BLOCK_LEVEL = :level AND BO.ACTUAL_BLOCK_BALANCE > :zeroParam AND 							");
		sbQuery.append("	  BO.BLOCK_STATE = :state AND HMB.ID_HOLDER_ACCOUNT_FK = BO.ID_HOLDER_ACCOUNT_FK AND 			");
		sbQuery.append("      HMB.AVAILABLE_BALANCE > :zeroParam AND MRK.IND_ACTIVE = :oneParam								");
		
		Query query = null;
		try{
			query = em.createNativeQuery(sbQuery.toString());
		}catch(NoResultException ex){
			return inconsistenciesList;
		}
		query.setParameter("oneParam", ComponentConstant.ONE);
		query.setParameter("zeroParam", ComponentConstant.ZERO);
		query.setParameter("state", AffectationStateType.BLOCKED.getCode());
		query.setParameter("level", LevelAffectationType.REBLOCK.getCode());
		
		List<Object[]> objectDataList = query.getResultList();
		for(Object[] data: objectDataList){
			inconsistenciesList.add(populateValuatorInconsist(data,null));
		}
		
		return inconsistenciesList;
	}
	
	/**
	 * *
	 * Method to get all inconsistencies in affectation.
	 *
	 * @return the affectation inconsist
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public List<ValuatorInconsistenciesTO> getAffectationInconsist() throws ServiceException{
		List<ValuatorInconsistenciesTO> inconsistenciesList = new ArrayList<ValuatorInconsistenciesTO>();
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select bop.id_participant_fk, bop.id_security_code_fk, 	bop.id_holder_account_fk,							");
		sbQuery.append("	   hmb.market_date As m_date, hmb.market_rate As m_rate, hmb.market_price As m_price,                   ");
		sbQuery.append("	   mrk.market_date, mrk.market_rate, mrk.market_price, ha.alternate_Code, pa.mnemonic,				 	");
		sbQuery.append("	   bop.id_Block_Operation_Pk, null as acc_pk, bop.document_number as document_number, 					");
		sbQuery.append("	   null as operation_pk, null as ballot, null as sequential, null as operation_number					");
		sbQuery.append("From Block_Operation	bop 																			 	");
		sbQuery.append("Inner Join Block_MarketFact_Operation mrk ON (mrk.id_Block_Operation_FK = bop.id_block_operation_pk)	 	");
		sbQuery.append("Inner Join Holder_account ha ON (ha.id_holder_account_pk = bop.id_holder_account_fk)	 				 	");
		sbQuery.append("Inner Join Participant pa ON (pa.id_participant_pk = bop.id_participant_fk)	 							 	");
		sbQuery.append("Inner Join Holder_MarketFact_Balance hmb ON (hmb.id_security_code_Fk = bop.id_security_code_Fk And		 	");
		sbQuery.append("											 hmb.id_holder_account_fk = bop.id_holder_account_fk 	)	 	");
		sbQuery.append("Where 	mrk.ind_active = :oneParam 		And hmb.ind_active = :oneParam And								 	");
		sbQuery.append("		bop.block_state = :blocked  	And bop.block_level = :blockLev And 						 	 	");
		sbQuery.append("		bop.actual_block_balance >= :bOneParam																");
		sbQuery.append("	  And  Not Exists ( Select * From Holder_MarketFact_Balance market                                      ");
		sbQuery.append("	      				Where hmb.id_holder_account_fk = market.id_holder_account_fk  						");
		sbQuery.append("	    						And   hmb.id_security_code_fk = market.id_security_code_fk 					"); 
		sbQuery.append("	  							And   NVL(mrk.market_rate,0) = NVL(market.market_rate,0) 					");   
		sbQuery.append("	  							And   NVL(mrk.market_price,0) = NVL(market.market_price,0) 					"); 
		sbQuery.append("	  							And   mrk.market_date = market.market_date )								");
		
		Query query = null;
		try{
			query = em.createNativeQuery(sbQuery.toString());
		}catch(NoResultException ex){
			return inconsistenciesList;
		}
		query.setParameter("oneParam", ComponentConstant.ONE);
		query.setParameter("bOneParam", new BigDecimal(ComponentConstant.ONE));
		query.setParameter("blocked", AffectationStateType.BLOCKED.getCode());
		query.setParameter("blockLev", LevelAffectationType.BLOCK.getCode());
		
		@SuppressWarnings("unchecked")
		List<Object[]> objectDataList = query.getResultList();
		for(Object[] data: objectDataList){
			inconsistenciesList.add(populateValuatorInconsist(data,null));
		}
		
		return inconsistenciesList;
	}
	
	/**
	 * *
	 * Method to cast ValuatorInconsistencies from object array [].
	 *
	 * @param dataObject the data object
	 * @param role the role
	 * @return the valuator inconsistencies to
	 */
	public ValuatorInconsistenciesTO populateValuatorInconsist(Object[] dataObject,Integer role){
		ValuatorInconsistenciesTO inconsistent = new ValuatorInconsistenciesTO();
		
		inconsistent.setParticipantPk(((BigDecimal) dataObject[0]).longValue());
		inconsistent.setSecurityCode((String) dataObject[1]);
		inconsistent.setHolderAccountPk(((BigDecimal) dataObject[2]).longValue());
		inconsistent.setMarketDate((Date) dataObject[3]);
		inconsistent.setMarketRate((BigDecimal) dataObject[4]);
		inconsistent.setMarketPrice((BigDecimal) dataObject[5]);
		inconsistent.setMarketDateInconsist((Date) dataObject[6]);
		inconsistent.setMarketRateInconsist((BigDecimal) dataObject[7]);
		inconsistent.setMarketPriceInconsist((BigDecimal) dataObject[8]);
		
		inconsistent.setAccountNumber(dataObject[9]!=null?(String) dataObject[9].toString():null);
		inconsistent.setInstitutionMnemonic(dataObject[10]!=null?(String) dataObject[10].toString():null);
		inconsistent.setAffectationPk(dataObject[11]!=null?((BigDecimal) dataObject[11]).longValue():null);
		inconsistent.setAccreditationPk(dataObject[12]!=null?((BigDecimal) dataObject[12]).longValue():null);
		inconsistent.setDocumentNumber(dataObject[13]!=null?(String) dataObject[13].toString():null);
		inconsistent.setMechanismOperationPk(dataObject[14]!=null?((BigDecimal) dataObject[14]).longValue():null);
		inconsistent.setOperationBallot(dataObject[15]!=null?(String) dataObject[15].toString():null);
		inconsistent.setOperationSequential(dataObject[16]!=null?(String) dataObject[16].toString():null);
		inconsistent.setOperationNumber(dataObject[17]!=null?(String) dataObject[17].toString():null);
		
		inconsistent.setRole(role);
		
		return inconsistent;
	}
}