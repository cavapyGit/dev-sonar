//package com.pradera.securities.valuator.core.files.to;
//
//import java.io.Serializable;
//
//import com.pradera.commons.utils.GeneralConstants;
//
//public class RecordValidationType implements Comparable<RecordValidationType>, Serializable {
//	
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 1L;
//
//	private Integer lineNumber;
//	private Integer recordNumber;
//	private String field;
//	private String errorType;
//	private String errorDescription;
//	private String fieldText;
//	private Integer fieldNum;
//	private String emptyProp = GeneralConstants.EMPTY_STRING;
//	
//	public RecordValidationType() {
//		super();
//	}
//	/**
//	 * @return the field
//	 */
//	public String getField() {
//		return field;
//	}
//	/**
//	 * @param field the field to set
//	 */
//	public void setField(String field) {
//		this.field = field;
//	}
//	/**
//	 * @return the errorType
//	 */
//	public String getErrorType() {
//		return errorType;
//	}
//	/**
//	 * @param errorType the errorType to set
//	 */
//	public void setErrorType(String errorType) {
//		this.errorType = errorType;
//	}
//	/**
//	 * @return the errorDescription
//	 */
//	public String getErrorDescription() {
//		return errorDescription;
//	}
//	/**
//	 * @param errorDescription the errorDescription to set
//	 */
//	public void setErrorDescription(String errorDescription) {
//		this.errorDescription = errorDescription;
//	}
//	/**
//	 * @return the lineNumber
//	 */
//	public Integer getLineNumber() {
//		return lineNumber;
//	}
//	/**
//	 * @param lineNumber the lineNumber to set
//	 */
//	public void setLineNumber(Integer lineNumber) {
//		this.lineNumber = lineNumber;
//	}
//	/**
//	 * @return the recordNumber
//	 */
//	public Integer getRecordNumber() {
//		return recordNumber;
//	}
//	/**
//	 * @param recordNumber the recordNumber to set
//	 */
//	public void setRecordNumber(Integer recordNumber) {
//		this.recordNumber = recordNumber;
//	}
//	
//	public String getFieldText() {
//		return fieldText;
//	}
//	public void setFieldText(String fieldText) {
//		this.fieldText = fieldText;
//	}
//	/**
//	 * @return the emptyProp
//	 */
//	public String getEmptyProp() {
//		return emptyProp;
//	}
//	/**
//	 * @param emptyProp the emptyProp to set
//	 */
//	public void setEmptyProp(String emptyProp) {
//		this.emptyProp = emptyProp;
//	}
//	
//	@Override
//	public int compareTo(RecordValidationType o) {
//		return lineNumber - o.getLineNumber();
//	}
//	public Integer getFieldNum() {
//		return fieldNum;
//	}
//	public void setFieldNum(Integer fieldNum) {
//		this.fieldNum = fieldNum;
//	}
//	
//	
//	
//}
