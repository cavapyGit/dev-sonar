package com.pradera.securities.valuator.classes.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.valuatorprocess.SecurityClassValuator;
import com.pradera.model.valuatorprocess.ValuatorProcessClass;
import com.pradera.securities.issuancesecuritie.util.PropertiesConstants;
import com.pradera.securities.valuator.classes.facade.SecuritiesClassValuatorFacade;
import com.pradera.securities.valuator.classes.to.SecurityClassValuatorResultTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SecuritiesClassValuatorBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@DepositaryWebBean
public class SecuritiesClassValuatorBean extends GenericBaseBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant REGISTER_CLASS. */
	private static final String REGISTER_CLASS="registerClass";
	
	/** The Constant SEARCH_CLASS. */
	private static final String SEARCH_CLASS="searchClass";
	
	/** The Constant REGISTERED. */
	private static final Integer REGISTERED = GeneralConstants.ONE_VALUE_INTEGER;
	
	/** The general parameter facade. */
	@EJB private GeneralParametersFacade generalParameterFacade;
	
	/** The securities class valuator facade. */
	@EJB private SecuritiesClassValuatorFacade securitiesClassValuatorFacade;
	
	/** The user info. */
	@Inject private UserInfo userInfo;
	
	/** The depositary setup. */
	@Inject @DepositarySetup IdepositarySetup depositarySetup;
	
	/** The securities class valuator to. */
	private SecuritiesClassValuatorTO securitiesClassValuatorTO;
	
	/** The lst security class valuator. */
	private List<SecurityClassValuator> lstSecurityClassValuator;  //lista que registro
	
	/** The map valuator process class. */
	private Map<String, ValuatorProcessClass> mapValuatorProcessClass=new HashMap<String,ValuatorProcessClass>();
	
	/** The parameter description. */
	private Map<Integer,String> parameterDescription = new HashMap<Integer,String>();
	
	/** The is selected class valuator. */
	private boolean isSelectedClassValuator;
	
	/** The is empty. */
	private boolean isEmpty;
	
	/** The tmp instrument. */
	private Integer tmpInstrument;
	
	/** The search instrument. */
	private Integer searchInstrument;
	
	/** List to find Data Tables*. */
	private List<ValuatorProcessClass> lstVpClassShow ;
	
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		securitiesClassValuatorTO = new SecuritiesClassValuatorTO();
		loadPriviligies();
		loadViewSearch();
		loadCurrency();
		loadInstrument();
	}
	
	/**
	 * Load priviligies.
	 */
	private void loadPriviligies() {
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnDeleteView(Boolean.TRUE);
		privilegeComponent.setBtnModifyView(Boolean.TRUE);
		userInfo.setPrivilegeComponent(privilegeComponent);
	}
	
	/**
	 * Load view search.
	 */
	public void loadViewSearch(){
		securitiesClassValuatorTO.setSecurityClassValuatorResult(new SecurityClassValuatorResultTO());
		securitiesClassValuatorTO.getSecurityClassValuatorResult().setInitialDate(getCurrentSystemDate());
		securitiesClassValuatorTO.getSecurityClassValuatorResult().setFinalDate(getCurrentSystemDate());
		setSearchInstrument(null);
	}
	
	/**
	 * Load view register.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String loadViewRegister(){
		setViewOperationType(ViewOperationsType.REGISTER.getCode());
		securitiesClassValuatorTO.setRegisterDate(getCurrentSystemDate());
		securitiesClassValuatorTO.setValuatorClassPk(null);
		securitiesClassValuatorTO.setIntrument(null);
		lstSecurityClassValuator= new ArrayList<SecurityClassValuator>();
		lstVpClassShow =new ArrayList<ValuatorProcessClass>();
		mapValuatorProcessClass.clear();
		setSelectedClassValuator(Boolean.FALSE);
		loadInstrument();
		fillDataTables();
		loadFormulasDescription();			
		return REGISTER_CLASS;
	}
	
	/**
	 * Load view update.
	 *
	 * @return the string
	 */
	public String loadViewUpdate(){
		if(Validations.validateIsNotNull(securitiesClassValuatorTO.getSecurityToSelected())){
			SecurityClassValuatorResultTO filter=securitiesClassValuatorTO.getSecurityToSelected();
			securitiesClassValuatorTO.setRegisterDate(getCurrentSystemDate());
			securitiesClassValuatorTO.setValuatorClassPk(filter.getIdSecurityClassFk());
			securitiesClassValuatorTO.setIntrument(filter.getInstrumentType());
			setViewOperationType(ViewOperationsType.MODIFY.getCode());
			setSelectedClassValuator(Boolean.TRUE);
			loadCboValuatorClass();
			loadValuatorProcessClass();
			loadFormulasDescription();
			loadInstrument();			
			loadSecuritiesClassValuator(filter);
			fillDataTables();
		return REGISTER_CLASS;
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.VALUATOR_PROCESS_MSG_NO_SELECTED));
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}
	}
	
	/**
	 * Load view detail.
	 *
	 * @param to the to
	 * @return the string
	 */
	public String loadViewDetail(SecurityClassValuatorResultTO to){
		SecurityClassValuatorResultTO filter=to;
		securitiesClassValuatorTO.setRegisterDate(getCurrentSystemDate());
		securitiesClassValuatorTO.setIntrument(filter.getInstrumentType());
		securitiesClassValuatorTO.setValuatorClassPk(filter.getIdSecurityClassFk());			
		setViewOperationType(ViewOperationsType.DETAIL.getCode());
		setSelectedClassValuator(Boolean.TRUE);
		loadCboValuatorClass();
		loadFormulasDescription();
		loadInstrument();
		loadValuatorProcessClass();
		loadSecuritiesClassValuator(filter);
		fillDataTables();
		return REGISTER_CLASS;
	}
	
	/**
	 * Load view delete.
	 *
	 * @return the string
	 */
	public String loadViewDelete(){
		if(Validations.validateIsNotNull(securitiesClassValuatorTO.getSecurityToSelected())){
			SecurityClassValuatorResultTO filter=securitiesClassValuatorTO.getSecurityToSelected();
			securitiesClassValuatorTO.setRegisterDate(getCurrentSystemDate());
			securitiesClassValuatorTO.setIntrument(filter.getInstrumentType());
			securitiesClassValuatorTO.setValuatorClassPk(filter.getIdSecurityClassFk());			
			setViewOperationType(ViewOperationsType.DELETE.getCode());
			setSelectedClassValuator(Boolean.TRUE);
			loadCboValuatorClass();
			loadFormulasDescription();
			loadInstrument();
			loadValuatorProcessClass();
			loadSecuritiesClassValuator(filter);
			fillDataTables();
			return REGISTER_CLASS;
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.VALUATOR_PROCESS_MSG_NO_SELECTED));
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}
	}
	
	/**
	 * Clean form register.
	 */
	public void cleanFormRegister(){
			securitiesClassValuatorTO.setRegisterDate(getCurrentSystemDate());
			lstSecurityClassValuator=new ArrayList<SecurityClassValuator>();
			lstVpClassShow=new ArrayList<ValuatorProcessClass>();
			setSelectedClassValuator(Boolean.FALSE);
			securitiesClassValuatorTO.setValuatorClassPk(null);
			securitiesClassValuatorTO.setIntrument(null);
			mapValuatorProcessClass.clear();
			securitiesClassValuatorTO.setLstDataModelSecurityClassValuator(new GenericDataModel<SecurityClassValuator>());
			securitiesClassValuatorTO.setLstValuatorProcessClass(new GenericDataModel<ValuatorProcessClass>());
	}
	
	/**
	 * Clean form search.
	 */
	public void cleanFormSearch(){
		setEmpty(Boolean.FALSE);
		securitiesClassValuatorTO.setLstDMSecurityClassValuatorResult(new GenericDataModel<SecurityClassValuatorResultTO>());
		loadViewSearch();
	}
	
	/**
	 * Cancel form update.
	 */
	public void cancelFormUpdate(){
		securitiesClassValuatorTO.setRegisterDate(CommonsUtilities.currentDate());
		SecurityClassValuatorResultTO filter=securitiesClassValuatorTO.getSecurityToSelected();
		lstSecurityClassValuator=new ArrayList<SecurityClassValuator>();
		lstVpClassShow = new ArrayList<ValuatorProcessClass>();
		setSelectedClassValuator(Boolean.TRUE);
		loadValuatorProcessClass();
		loadSecuritiesClassValuator(filter);
		fillDataTables();
	}
	
	/**
	 * Search security class valuator.
	 */
	@LoggerAuditWeb
	public void searchSecurityClassValuator(){ 
		try{
			SecurityClassValuatorResultTO filter=new SecurityClassValuatorResultTO();
			if(Validations.validateIsNotNull(getSearchInstrument()))
				filter.setInstrumentType(getSearchInstrument());
			List<SecurityClassValuatorResultTO> lstResult= securitiesClassValuatorFacade.serchSecurityClassValuatorResultFacade(filter);
			if(Validations.validateIsNotNull(lstResult)){
				securitiesClassValuatorTO.setLstDMSecurityClassValuatorResult(new GenericDataModel<SecurityClassValuatorResultTO>(lstResult));
			}
			else{
				securitiesClassValuatorTO.setLstDMSecurityClassValuatorResult(new GenericDataModel<SecurityClassValuatorResultTO>());
			}			
		}catch(ServiceException ex){	
			if(ex.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getExceptionMessage(ex.getMessage()));
			}
			JSFUtilities.showSimpleEndTransactionDialog("SEARCH_CLASS");
		}
	}
	
	/**
	 * Load securities class valuator.
	 *
	 * @param filter the filter
	 */
	@LoggerAuditWeb
	public void loadSecuritiesClassValuator(SecurityClassValuatorResultTO filter){	//load form update	
		try{
			List<SecurityClassValuatorResultTO> listTO=securitiesClassValuatorFacade.getListSecurityClassValuatorResultTOFacade(filter);
			if(Validations.validateIsNotNull(listTO)){
				this.removeValuatorProcessClass(listTO);
				this.orderLstSecuritiesClassValuator(lstSecurityClassValuator);
				this.fillMapValuatorProcessClass();
			}
		}catch(ServiceException ex){
			if(ex.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getExceptionMessage(ex.getMessage()));
			}
			JSFUtilities.showSimpleEndTransactionDialog("SEARCH_CLASS");
		}
	}
	
	/**
	 * remueve las clases de valoracion (VALUATOR_PROCESS_CLASS) que ya se encuentran agregadas a una clase de valor.
	 *
	 * @param listTO the list to
	 */
	public void removeValuatorProcessClass(List<SecurityClassValuatorResultTO> listTO){
		Map<String, ValuatorProcessClass> mapVpc=new HashMap<String,ValuatorProcessClass>();
		for(ValuatorProcessClass vpc : lstVpClassShow){			
			mapVpc.put(vpc.getIdValuatorClassPk(), vpc);
		}
		lstSecurityClassValuator=new ArrayList<SecurityClassValuator>();
		for(SecurityClassValuatorResultTO objTO : listTO){
			if(Validations.validateIsNotNullAndNotEmpty(mapVpc.get(objTO.getIdValuatorClass()))){
				ValuatorProcessClass vpc=mapVpc.get(objTO.getIdValuatorClass());
				SecurityClassValuator scv=new SecurityClassValuator();
				scv.setIdSecurityValuatorPk(objTO.getIdSecurityValuatorPk());
				scv.setIdSecurityClassFk(objTO.getIdSecurityClassFk());
				scv.setValuatorProcessClass(vpc);
				scv.setFormuleType(objTO.getFormuleType());
				lstSecurityClassValuator.add(scv);
				lstVpClassShow.remove(vpc);
			}
		}
	}
	
	/**
	 * Load cbo valuator class.
	 */
	@LoggerAuditWeb
	private void loadCboValuatorClass(){//Cargamos los valores de clases
		try{
		ParameterTableTO parameterTableTO = new ParameterTableTO();		
		parameterTableTO.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		parameterTableTO.setState(REGISTERED);
		parameterTableTO.setShortInteger(securitiesClassValuatorTO.getIntrument());		
		securitiesClassValuatorTO.setLstValorClass(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO));
		for(ParameterTable obj: securitiesClassValuatorTO.getLstValorClass()){
			parameterDescription.put(obj.getParameterTablePk(), obj.getText1());
		}
		}catch(ServiceException ex){
			ex.printStackTrace();
		}
	}
	
	/**
	 * Load formulas description.
	 */
	@LoggerAuditWeb
	private void loadFormulasDescription()  {
		try{
		ParameterTableTO parameterTableTO = new ParameterTableTO();		
		parameterTableTO.setMasterTableFk(MasterTableType.FORMULA_TYPE.getCode());
		parameterTableTO.setState(REGISTERED);
		securitiesClassValuatorTO.setLstFormulas(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO));
		for(ParameterTable obj: securitiesClassValuatorTO.getLstFormulas()){
			parameterDescription.put(obj.getParameterTablePk(), obj.getDescription());
		}
		}catch(ServiceException ex){
			ex.printStackTrace();
		}
	}
	
	/**
	 * Load instrument.
	 */
	@LoggerAuditWeb
	private void loadInstrument()  {
		try{
		ParameterTableTO parameterTableTO = new ParameterTableTO();		
		parameterTableTO.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
		parameterTableTO.setState(REGISTERED);
		securitiesClassValuatorTO.setLstIntrument(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO));
		for(ParameterTable obj: securitiesClassValuatorTO.getLstIntrument()){
			parameterDescription.put(obj.getParameterTablePk(), obj.getParameterName());
		}
		}catch(ServiceException ex){
			ex.printStackTrace();
		}
	}
	
	/**
	 * Load currency.
	 */
	private void loadCurrency()  {
		try{
		ParameterTableTO parameterTableTO = new ParameterTableTO();		
		parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		parameterTableTO.setState(REGISTERED);
		securitiesClassValuatorTO.setLstCurrency(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO));
		for(ParameterTable obj: securitiesClassValuatorTO.getLstCurrency()){
			parameterDescription.put(obj.getParameterTablePk(), obj.getDescription());
		}
		}catch(ServiceException ex){
			ex.printStackTrace();
		}
	}
	
	/**
	 * Load valuator process class.
	 */
	@LoggerAuditWeb
	public void loadValuatorProcessClass(){		
		try {
			Integer intrument = null;
			if(Validations.validateIsNotNull(securitiesClassValuatorTO.getIntrument()))
				intrument = securitiesClassValuatorTO.getIntrument();
			lstVpClassShow=new ArrayList<ValuatorProcessClass>();
			lstVpClassShow=securitiesClassValuatorFacade.getSecuritiesClassValuatorFacade(intrument);				
		} catch (ServiceException ex) {
			if(ex.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getExceptionMessage(ex.getMessage()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}	
	
	/**
	 * Fill data tables.
	 */
	public void fillDataTables(){
		if(Validations.validateIsNullOrEmpty(lstVpClassShow)){
			securitiesClassValuatorTO.setLstValuatorProcessClass( new GenericDataModel<ValuatorProcessClass>());
		}else{
		  securitiesClassValuatorTO.setLstValuatorProcessClass( new GenericDataModel<ValuatorProcessClass>(lstVpClassShow));
		}
		
		if(Validations.validateIsNullOrEmpty(lstSecurityClassValuator)){
			securitiesClassValuatorTO.setLstDataModelSecurityClassValuator( new GenericDataModel<SecurityClassValuator>());
		}else{
			securitiesClassValuatorTO.setLstDataModelSecurityClassValuator( new GenericDataModel<SecurityClassValuator>(lstSecurityClassValuator));
		}
	}
	
	/**
	 * Fill map valuator process class.
	 */
	public void fillMapValuatorProcessClass(){
		if(lstSecurityClassValuator!=null){
			mapValuatorProcessClass.clear();
			for(SecurityClassValuator scv : lstSecurityClassValuator){
				ValuatorProcessClass vpc=scv.getValuatorProcessClass();
				if(Validations.validateIsNotNull(vpc.getIdValuatorClassPk())){
				mapValuatorProcessClass.put(vpc.getIdValuatorClassPk(), vpc);
				}
			}
		}
	}
	
	/**
	 * Adds the detail security class valuator.
	 *
	 * @param valuatorProcessClass the valuator process class
	 */
	public void addDetailSecurityClassValuator(ValuatorProcessClass valuatorProcessClass){
		if(isSelectedClassValuator()){
			if(!Validations.validateIsNotNull(mapValuatorProcessClass.get(valuatorProcessClass.getIdValuatorClassPk()))){			
				SecurityClassValuator securityClassValuator = new SecurityClassValuator();
				securityClassValuator.setValuatorProcessClass(valuatorProcessClass);
				securityClassValuator.setIdSecurityClassFk(securitiesClassValuatorTO.getValuatorClassPk());
				securityClassValuator.setFormuleType(valuatorProcessClass.getFormuleType());
				securityClassValuator.setRegistryDate(securitiesClassValuatorTO.getRegisterDate());
				securityClassValuator.setRegistryUser(userInfo.getUserAccountSession().getUserName());
				securityClassValuator.setClassState(REGISTERED);
				this.lstSecurityClassValuator.add(securityClassValuator);
				this.mapValuatorProcessClass.put(valuatorProcessClass.getIdValuatorClassPk(), valuatorProcessClass);
				this.lstVpClassShow.remove(valuatorProcessClass);
				this.orderLstSecuritiesClassValuator(lstSecurityClassValuator);
				this.fillDataTables();
				tmpInstrument=securitiesClassValuatorTO.getIntrument();
				
			}
		}	
	}
	
	/**
	 * Removes the detail security class valuator.
	 *
	 * @param securityClassValuator the security class valuator
	 */
	public void removeDetailSecurityClassValuator(SecurityClassValuator securityClassValuator){
		this.lstSecurityClassValuator.remove(securityClassValuator);
		this.mapValuatorProcessClass.remove(securityClassValuator.getValuatorProcessClass().getIdValuatorClassPk());
		this.lstVpClassShow.add(securityClassValuator.getValuatorProcessClass());
		this.orderLstValuatorProcessClass(lstVpClassShow);
	}
	
	/**
	 * Choose valuator class.
	 */
	@LoggerAuditWeb
	public void chooseValuatorClass(){
		
		try{
			validateExistSecurityClass(securitiesClassValuatorTO.getValuatorClassPk());
			setSelectedClassValuator(Boolean.TRUE);
		}catch(ServiceException e){
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
			}
			JSFUtilities.showSimpleValidationDialog();
			setSelectedClassValuator(Boolean.FALSE);			
			securitiesClassValuatorTO.setValuatorClassPk(null);
		}	
	}
	
	/**
	 * Validate exist security class.
	 *
	 * @param classValaor the class valaor
	 * @throws ServiceException the service exception
	 */
	public void validateExistSecurityClass(Integer classValaor) throws ServiceException{
		Long countSecurityClass=securitiesClassValuatorFacade.existSecurityClass(classValaor);
		if(!countSecurityClass.equals(GeneralConstants.ZERO_VALUE_LONG)){
			throw new ServiceException(ErrorServiceType.VALUATOR_SECURITY_CLASS_EXIST);
		}
	}
	
	/**
	 * Before save security class valuator.
	 */
	public void beforeSaveSecurityClassValuator(){
		String messageproperties=GeneralConstants.EMPTY_STRING;
		Boolean haveValidation = Boolean.FALSE;
		switch(ViewOperationsType.get(getViewOperationType())){
			case REGISTER:	messageproperties=PropertiesConstants.VALUATOR_PROCESS_REGISTER_CONFIRM;
							haveValidation=validateBeforeRegister();break;
			case UPDATE  :	messageproperties=PropertiesConstants.VALUATOR_PROCESS_UPDATE_CONFIRM;
							haveValidation=validateBeforeRegister();break;
			case DELETE  :  messageproperties=PropertiesConstants.VALUATOR_PROCESS_DELETE_CONFIRM;
							haveValidation=Boolean.TRUE;break;
			default:break;
		}
		if(haveValidation){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),PropertiesUtilities.getMessage(messageproperties));
			JSFUtilities.executeJavascriptFunction("PF('cnfwSecurityClass').show()");			
		}		
	}
	
	/**
	 * Save security class valuator.
	 */
	@LoggerAuditWeb
	public void saveSecurityClassValuator(){
		switch(ViewOperationsType.get(getViewOperationType())){
			case REGISTER:	registerSecuritiesClassValuadtor();break;
			case UPDATE  :	updateSecuritiesClassValuator();break;
			case DELETE  :  deleteSecuritiesClassValuator();break;
			default:break;
		}
		if(Validations.validateIsNotNull(securitiesClassValuatorTO.getLstDMSecurityClassValuatorResult())){
			searchSecurityClassValuator();
		}
	}
	
	/**
	 * Update securities class valuator.
	 */
	@LoggerAuditWeb
	private void updateSecuritiesClassValuator(){
		try{
			securitiesClassValuatorFacade.updateSecuritiesClassValuatorFacade(lstSecurityClassValuator,
					securitiesClassValuatorTO.getSecurityToSelected());
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
								PropertiesUtilities.getMessage(PropertiesConstants.VALUATOR_PROCESS_UPDATE_CONFIRM_OK,
																new Object[]{securitiesClassValuatorTO.getValuatorClassPk().toString()}));
			JSFUtilities.showSimpleEndTransactionDialog(SEARCH_CLASS);
			}catch(ServiceException e){
				if(e.getErrorService()!=null){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
										PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				}
				JSFUtilities.showSimpleEndTransactionDialog(SEARCH_CLASS);
			}
	}
	
	/**
	 * Register securities class valuadtor.
	 */
	@LoggerAuditWeb
	private void registerSecuritiesClassValuadtor() {
		try{
		securitiesClassValuatorFacade.registerListSecurityClass(lstSecurityClassValuator);
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
							PropertiesUtilities.getMessage(PropertiesConstants.VALUATOR_PROCESS_REGISTER_CONFIRM_OK,
															new Object[]{securitiesClassValuatorTO.getValuatorClassPk().toString()}));
		JSFUtilities.showSimpleEndTransactionDialog(SEARCH_CLASS);
		}catch(ServiceException e){
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
			}
			JSFUtilities.showSimpleEndTransactionDialog(SEARCH_CLASS);
		}
	}
	
	/**
	 * Delete securities class valuator.
	 */
	@LoggerAuditWeb
	public void deleteSecuritiesClassValuator(){
		try{
			SecurityClassValuatorResultTO filter=securitiesClassValuatorTO.getSecurityToSelected();
			securitiesClassValuatorFacade.deleteSecuritiesClassValuatorService(filter);
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
			PropertiesUtilities.getMessage(PropertiesConstants.VALUATOR_PROCESS_DELETE_CONFIRM_OK,
											new Object[]{filter.getIdSecurityClassFk().toString()+"-"+filter.getText1()}));
			JSFUtilities.showSimpleEndTransactionDialog(SEARCH_CLASS);
		}catch(ServiceException e){
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
			}
			JSFUtilities.showSimpleEndTransactionDialog(SEARCH_CLASS);
		}
	}
	
	/**
	 * Validate before register.
	 *
	 * @return the boolean
	 */
	public Boolean validateBeforeRegister(){
		try{
			if(!isSelectedClassValuator())return Boolean.FALSE;
			if(lstSecurityClassValuator.size()==0){	
				throw new ServiceException(ErrorServiceType.VALUATOR_SECURITIES_CLASS_EMPTY);				
			}
			return Boolean.TRUE;
		}catch(ServiceException e){
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
			}
			JSFUtilities.showSimpleValidationDialog();
			return Boolean.FALSE;
		}		
	}
	
	/**
	 * Before change instrument type.
	 */
	public void beforeChangeInstrumentType(){
		if(lstSecurityClassValuator.size()>GeneralConstants.ZERO_VALUE_INT){
			securitiesClassValuatorTO.setRegisterDate(getCurrentSystemDate());
			lstSecurityClassValuator=new ArrayList<SecurityClassValuator>();
			setSelectedClassValuator(Boolean.FALSE);
			securitiesClassValuatorTO.setValuatorClassPk(null);
			mapValuatorProcessClass.clear();
			securitiesClassValuatorTO.setLstDataModelSecurityClassValuator(new GenericDataModel<SecurityClassValuator>());

			String messageproperties=PropertiesConstants.VALUATOR_PROCESS_CLASS_CHANGE_INSTRUMENT_TYPE_CONFIRM;
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),PropertiesUtilities.getMessage(messageproperties));
				JSFUtilities.executeJavascriptFunction("PF('cnfwChangeInstrumentType').show()");
		}
		else{
			loadValuatorProcessClass();
			loadCboValuatorClass();
			fillDataTables();
		}
	}
	
	/**
	 * Change instrument type.
	 */
	public void changeInstrumentType(){
		loadValuatorProcessClass();
		loadCboValuatorClass();
		fillDataTables();
	}
	
	/**
	 * No change instrument type.
	 */
	public void noChangeInstrumentType(){
		securitiesClassValuatorTO.setIntrument(tmpInstrument);
	}
	
	/**
	 * Order lst valuator process class.
	 *
	 * @param lstVpClassShow the lst vp class show
	 */
	public void orderLstValuatorProcessClass(List<ValuatorProcessClass> lstVpClassShow){
		Collections.sort(lstVpClassShow, new Comparator<ValuatorProcessClass>(){
			@Override
			public int compare(ValuatorProcessClass obj1, ValuatorProcessClass obj2) {
				return obj1.getIdValuatorClassPk().compareTo(obj2.getIdValuatorClassPk());
			}
		});
	}
	
	/**
	 * Order lst securities class valuator.
	 *
	 * @param lstSecurityClassValuator the lst security class valuator
	 */
	public void orderLstSecuritiesClassValuator(List<SecurityClassValuator> lstSecurityClassValuator){
		Collections.sort(lstSecurityClassValuator, new Comparator<SecurityClassValuator>(){
			@Override
			public int compare(SecurityClassValuator obj1, SecurityClassValuator obj2) {
				return (obj1.getValuatorProcessClass().getIdValuatorClassPk()).compareTo(obj2.getValuatorProcessClass().getIdValuatorClassPk());
			}
		});
	}
	
	/**
	 * Gets the securities class valuator to.
	 *
	 * @return the securities class valuator to
	 */
	public SecuritiesClassValuatorTO getSecuritiesClassValuatorTO() {
		return securitiesClassValuatorTO;
	}
	
	/**
	 * Sets the securities class valuator to.
	 *
	 * @param securitiesClassValuatorTO the new securities class valuator to
	 */
	public void setSecuritiesClassValuatorTO(SecuritiesClassValuatorTO securitiesClassValuatorTO) {
		this.securitiesClassValuatorTO = securitiesClassValuatorTO;
	}
	
	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}
	
	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	
	/**
	 * Gets the lst security class valuator.
	 *
	 * @return the lst security class valuator
	 */
	public List<SecurityClassValuator> getLstSecurityClassValuator() {
		return lstSecurityClassValuator;
	}
	
	/**
	 * Sets the lst security class valuator.
	 *
	 * @param lstSecurityClassValuator the new lst security class valuator
	 */
	public void setLstSecurityClassValuator(
			List<SecurityClassValuator> lstSecurityClassValuator) {
		this.lstSecurityClassValuator = lstSecurityClassValuator;
	}
	
	/**
	 * Checks if is selected class valuator.
	 *
	 * @return true, if is selected class valuator
	 */
	public boolean isSelectedClassValuator() {
		return isSelectedClassValuator;
	}
	
	/**
	 * Sets the selected class valuator.
	 *
	 * @param isSelectedClassValuator the new selected class valuator
	 */
	public void setSelectedClassValuator(boolean isSelectedClassValuator) {
		this.isSelectedClassValuator = isSelectedClassValuator;
	}
	
	/**
	 * Gets the parameter description.
	 *
	 * @return the parameter description
	 */
	public Map<Integer, String> getParameterDescription() {
		return parameterDescription;
	}
	
	/**
	 * Sets the parameter description.
	 *
	 * @param parameterDescription the parameter description
	 */
	public void setParameterDescription(Map<Integer, String> parameterDescription) {
		this.parameterDescription = parameterDescription;
	}
	
	/**
	 * Checks if is empty.
	 *
	 * @return true, if is empty
	 */
	public boolean isEmpty() {
		return isEmpty;
	}
	
	/**
	 * Sets the empty.
	 *
	 * @param isEmpty the new empty
	 */
	public void setEmpty(boolean isEmpty) {
		this.isEmpty = isEmpty;
	}
	
	/**
	 * Gets the search instrument.
	 *
	 * @return the search instrument
	 */
	public Integer getSearchInstrument() {
		return searchInstrument;
	}
	
	/**
	 * Sets the search instrument.
	 *
	 * @param searchInstrument the new search instrument
	 */
	public void setSearchInstrument(Integer searchInstrument) {
		this.searchInstrument = searchInstrument;
	}
	
	
}
