package com.pradera.securities.valuator.core.files.facade;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
import com.pradera.securities.valuator.core.files.to.ValuatorMarketfactFileTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorMarketFactFileBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@BatchProcess(name="ValuatorMarketFactFileBatch")
@RequestScoped
public class ValuatorMarketFactFileBatch implements Serializable,JobExecution{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The market fact facade. */
	@EJB MarketFactFacade marketFactFacade;
	
	@EJB private MarketFactFileProcesingFacade processingFileFacade;
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {

		Integer fileSource=null;
		Date registerDate=null;
		Date processDate=null;
		String user=null;
		
		for(ProcessLoggerDetail detail : processLogger.getProcessLoggerDetails()){
			if(detail.getParameterName().equals(GeneralConstants.FILE_SOURCE)){
				fileSource = Integer.valueOf(detail.getParameterValue());
			}else if(detail.getParameterName().equals(GeneralConstants.REGISTER_DATE)){
				registerDate=CommonsUtilities.convertStringtoDate(detail.getParameterValue());
			}else if(detail.getParameterName().equals(GeneralConstants.PROCESS_DATE)){
				processDate = CommonsUtilities.convertStringtoDate(detail.getParameterValue(),CommonsUtilities.DATETIME_PATTERN);
			}
		}

		ValuatorMarketfactFileTO filter= new ValuatorMarketfactFileTO();
		filter.setFileSource(fileSource);
		filter.setInitalDate(registerDate);
		filter.setEndDate(registerDate);
		filter.setRegistryDate(registerDate);
		filter.setRegistryUser(user);
		filter.setProcessDate(processDate);
		
		processingFileFacade.startProcess(filter);
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}
}