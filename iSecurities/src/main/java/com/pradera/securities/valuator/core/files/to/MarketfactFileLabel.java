package com.pradera.securities.valuator.core.files.to;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Enum MarketfactFileLabel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public enum MarketfactFileLabel {

	/** The mechanism code. */
	MECHANISM_CODE("mechanismCode"),					
	
	/** The information date. */
	INFORMATION_DATE("informationDate"),	 
	
	/** The valuator code. */
	VALUATOR_CODE("valuatorCode"),							
	
	/** The security class. */
	SECURITY_CLASS("securityClass"),							
	
	/** The security code. */
	SECURITY_CODE("securityCode"),									
	
	/** The average rate. */
	AVERAGE_RATE("averageRate"),							
	
	/** The negotiation amount. */
	NEGOTIATION_AMOUNT("negotiationAmount"),						
	
	/** The minimun amount. */
	MINIMUN_AMOUNT("minimunAmount"),							
	
	/** The marketfact active. */
	MARKETFACT_ACTIVE("marketfactActive"),		
	
	/** The marketfact date. */
	MARKETFACT_DATE("marketfactDate"),
	
	/** The average price. */
	AVERAGE_PRICE("averagePrice"),
	
	/** The valuator type. */
	VALUATOR_TYPE("valuatorType"),
;
	
	/** The name. */
	String name;
	
	/**
	 * Instantiates a new marketfact file label.
	 *
	 * @param name the name
	 */
	private MarketfactFileLabel(String name) {
		// Tthis.name=name;ODO Auto-generated constructor stub
	}
		
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/** The Constant list. */
	public static final List<MarketfactFileLabel> list = new ArrayList<MarketfactFileLabel>();
	
	/** The Constant lookup. */
	public static final Map<String, MarketfactFileLabel> lookup = new HashMap<String, MarketfactFileLabel>();
	static {
		for (MarketfactFileLabel s : EnumSet.allOf(MarketfactFileLabel.class)) {
			list.add(s);
			lookup.put(s.getName(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param name the name
	 * @return the marketfact file label
	 */
	public static MarketfactFileLabel get(String name) {
		return lookup.get(name);
	}
}
