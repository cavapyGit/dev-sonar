package com.pradera.securities.valuator.core.facade;

import static com.pradera.model.valuatorprocess.type.ValuatorDetailType.BALANCE;
import static com.pradera.model.valuatorprocess.type.ValuatorDetailType.OPERATIONS;
import static com.pradera.model.valuatorprocess.type.ValuatorDetailType.SECURITY;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.configuration.ValuatorSetup;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.processes.scheduler.Schedulebatch;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
import com.pradera.model.valuatorprocess.ValuatorEventBehavior;
import com.pradera.model.valuatorprocess.ValuatorExecutionDetail;
import com.pradera.model.valuatorprocess.ValuatorExecutionParameters;
import com.pradera.model.valuatorprocess.ValuatorProcessExecution;
import com.pradera.model.valuatorprocess.ValuatorProcessOperation;
import com.pradera.model.valuatorprocess.ValuatorProcessSetup;
import com.pradera.model.valuatorprocess.type.ValuatorDetailType;
import com.pradera.model.valuatorprocess.type.ValuatorExecutionStateType;
import com.pradera.model.valuatorprocess.type.ValuatorExecutionType;
import com.pradera.model.valuatorprocess.type.ValuatorType;
import com.pradera.securities.valuator.core.files.facade.BalancePsyFacade;
import com.pradera.securities.valuator.core.files.facade.ValuatorMarketFactFileBatch;
import com.pradera.securities.valuator.core.service.ValuatorProcessHistory;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorExecutionBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@BatchProcess(name = "ValuatorExecutionBatch")
@RequestScoped
public class ValuatorExecutionBatch implements JobExecution, Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The notification facade. */
	@EJB private NotificationServiceFacade notificationFacade;
	
	/** The valuator event behaviors. */
	private List<ValuatorEventBehavior> valuatorEventBehaviors;
	
	/** The valuator process history. */
	@EJB private ValuatorProcessHistory valuatorProcessHistory;
	
	/** The valuator live operation. */
	@EJB private ValuatorLiveOperationFacade valuatorLiveOperation;
	
	/** The valuator securities facade. */
	@EJB private ValuatorSecuritiesFacade valuatorSecuritiesFacade;
	
	/** The valuator process validation. */
	@EJB private ValuatorProcessValidation valuatorProcessValidation;
	
	/** The holiday query service bean. */
	@EJB HolidayQueryServiceBean holidayQueryServiceBean;
	
	/** The parameter service bean. */
	@EJB private ParameterServiceBean parameterServiceBean;
	
    @Inject 
    ClientRestService clientRestService;
	
	@EJB
	private BalancePsyFacade balancePsyFacade;
	
	/** USER INFO. */
	private String userSession = "ADMIN";
	
	/** The valuator setup. */
	@Inject @ValuatorSetup ValuatorProcessSetup valuatorSetup;
	
	@Inject @BatchProcess(name = "ValuatorMarketFactFileBatch")
	private ValuatorMarketFactFileBatch valuatorMarketFactFileBatch;
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void startJob(ProcessLogger processLogger) {
		// TODO Auto-generated method stub
		/**PARAMETERS TO EXECUTE VALUATOR PROCESS*/
		List<String> securities = null;
		List<Long> participants = null, accounts = null, holders = null;
		/**CURRENCY EXECUTION BY DEFAULT IT'S BOB*/
		Integer currency = CurrencyType.PYG.getCode();
		/**VALUATOR DATE BY DEFAULT IT'S THE CURRENT DATE*/
		Date valuatorDate = CommonsUtilities.truncateDateTime(CommonsUtilities.currentDate());
		/**INSTRUMENT TYPE*/
		Integer instrumentType = null;
		/**USER EMAIL*/
		String userEmail = null, userName = null;
		/**Execution Type*/
		Integer executionType = ValuatorExecutionType.AUTOMATIC.getCode();
		/**Indicator to know if valuatorProcess is all portFolly*/
		Integer indAllPortfolly = BooleanType.NO.getCode();
		/**Id of process when is throwing again*/
		Long valuatorProcessPk = null;
		Integer executionDetail = null;
		Integer indInconsistence = BooleanType.NO.getCode();
		/**Variable to handle execution Process*/
		ValuatorProcessExecution mainExecution = null;
		/**String to handle detail of error*/
		String errorMessage = null, notifMessage = null;
		/**Begin the process to get parameters*/		
		List<ProcessLoggerDetail> lstProcessLoggerDetails = processLogger.getProcessLoggerDetails();
		for (ProcessLoggerDetail objDetail: lstProcessLoggerDetails){
//			if (StringUtils.equalsIgnoreCase(ValuatorConstants.PARAM_SECURITY_CODE, objDetail.getParameterName())) {
//				securities = (List<String>) populateParametersValue(objDetail.getParameterValue());
//			}else if(StringUtils.equalsIgnoreCase(ValuatorConstants.PARAM_PARTICIPANT_CODE, objDetail.getParameterName())) {
//				participants = (List<Long>) populateParametersLongValue(objDetail.getParameterValue());
//			}else if(StringUtils.equalsIgnoreCase(ValuatorConstants.PARAM_INCONSISTENCE_CODE, objDetail.getParameterName())) {
//				indInconsistence = BooleanType.YES.getCode();
//			}else if(StringUtils.equalsIgnoreCase(ValuatorConstants.PARAM_VALUATOR_DETAIL_TYPE, objDetail.getParameterName())) {
//				executionDetail = new Integer(objDetail.getParameterValue()); 
//			}else if(StringUtils.equalsIgnoreCase(ValuatorConstants.PARAM_VALUATOR_PROCESS_PK, objDetail.getParameterName())) {
//				valuatorProcessPk = new Long(objDetail.getParameterValue());
//			}else if(StringUtils.equalsIgnoreCase(ValuatorConstants.PARAM_HOLDER_CODE, objDetail.getParameterName())) {
//				holders = (List<Long>) populateParametersLongValue(objDetail.getParameterValue());
//			}else if(StringUtils.equalsIgnoreCase(ValuatorConstants.PARAM_HOLDER_ACCOUNT_CODE, objDetail.getParameterName())) {
//				accounts = (List<Long>) populateParametersLongValue(objDetail.getParameterValue());
//			}else if(StringUtils.equalsIgnoreCase(ValuatorConstants.PARAM_VALUATOR_CURRENCY, objDetail.getParameterName())) {
//				currency = new Integer(objDetail.getParameterValue());
//			}else if(StringUtils.equalsIgnoreCase(ValuatorConstants.PARAM_VALUATOR_INSTRUMENT, objDetail.getParameterName())) {
//				instrumentType = new Integer(objDetail.getParameterValue());
//			}else if(StringUtils.equalsIgnoreCase(ValuatorConstants.PARAM_VALUATOR_DATE, objDetail.getParameterName())) {
//				valuatorDate = CommonsUtilities.convertStringtoDate(objDetail.getParameterValue(),CommonsUtilities.DATETIME_PATTERN);
//				/**Truncate hours in date*/
//				valuatorDate = CommonsUtilities.truncateDateTime(valuatorDate);
//			}else if(StringUtils.equalsIgnoreCase(ValuatorConstants.PARAM_USER_EMAIL, objDetail.getParameterName())) {
//				userEmail = objDetail.getParameterValue();
//			}else if(StringUtils.equalsIgnoreCase(ValuatorConstants.PARAM_USER_NAME, objDetail.getParameterName())) {
//				userName = objDetail.getParameterValue();
//			}else if(StringUtils.equalsIgnoreCase(ValuatorConstants.PARAM_VALUATOR_EXECUTION_TYPE, objDetail.getParameterName())){
//				executionType = new Integer(objDetail.getParameterValue());
//			}
		}
		 
		/**If prices is null, we must load prices in memory**/
		if(valuatorSetup.getSecuritiesRatePriceTmp()==null){
			valuatorSecuritiesFacade.reloadMarketPrices(valuatorDate);
		}
//		if(valuatorSetup.getSecuritiesRatePriceMap()==null){
//			valuatorSecuritiesFacade.reloadMarketPrices(valuatorDate);
//		}
		/**Exception to know if process had some errors*/
		ServiceException e = null; Exception ex=null;
		try {
			/**Read consolidate */
			valuatorSetup.setMarketFactConsolidat(parameterServiceBean.getMarketFactConsolidat(null, null, valuatorDate));
			
			/**Verify if valuator date is holiday, this process is automatic*/
			if(holidayQueryServiceBean.isNonWorkingDayServiceBean(valuatorDate,true)){
				/**Setup logger detail and add at main process logger*/
				ProcessLoggerDetail loggerDetail = new ProcessLoggerDetail();
				loggerDetail.setParameterName(GeneralConstants.PROCESS_DATE);
				loggerDetail.setParameterValue(CommonsUtilities.convertDatetoStringForTemplate(valuatorDate,CommonsUtilities.DATETIME_PATTERN));
				processLogger.getProcessLoggerDetails().add(loggerDetail);
				
				/**If market fact file has not been loaded*/
				if(valuatorSetup.getMarketFactConsolidat()==null || valuatorSetup.getMarketFactConsolidat().isEmpty()){
					/**Start process file*/
					valuatorMarketFactFileBatch.startJob(processLogger);
					/**Read consolidate once again*/
					valuatorSetup.setMarketFactConsolidat(parameterServiceBean.getMarketFactConsolidat(null, null, valuatorDate));
				}
			}
			
			/**get behaviors from database*/
			valuatorEventBehaviors = valuatorSecuritiesFacade.getValuatorEventBehavior();

			if(indInconsistence.equals(BooleanType.YES.getCode()) && valuatorProcessPk!=null && executionDetail!=null){
				/**Get process execution object*/
				mainExecution = valuatorSecuritiesFacade.getProcessExecution(valuatorProcessPk);
				/**If execution process doesn't exist*/
				if(mainExecution==null){
					throw new ServiceException(ErrorServiceType.VALUATOR_EXECUTION_INCORRECT,new Object[]{valuatorProcessPk});
				}
				/**Get all securities associated at the execution process*/
				securities = valuatorSecuritiesFacade.getSecurityCodeByExecution(valuatorProcessPk);
				/**getting process Date*/
				Date processDat = mainExecution.getProcessDate();
				/**if execution is for balances, then execute balances and operation*/
				if(executionDetail.equals(BALANCE.getCode())){
					executeValuatorProcess(mainExecution,securities, participants, accounts, currency, processDat, instrumentType,BALANCE.getCode());
					executeValuatorProcess(mainExecution,securities, participants, accounts, currency, processDat, instrumentType,OPERATIONS.getCode());
					balancePsyFacade.processDailyBalancePsy(); //issue 507
					valuatorSecuritiesFacade.updateHolderMarketfactBalance(); //issue 238			
					registerBlockBalanceSelidBatch(processLogger);//issue 1040	
				}
				/**if this is only operation, execute update of price on operations*/
				if(executionDetail.equals(OPERATIONS.getCode())){
					executeValuatorProcess(mainExecution,securities, participants, accounts, currency, processDat, instrumentType,OPERATIONS.getCode());
					balancePsyFacade.processDailyBalancePsy(); //issue 507
					valuatorSecuritiesFacade.updateHolderMarketfactBalance(); //issue 238			
					registerBlockBalanceSelidBatch(processLogger);//issue 1040
				}
			}else{
				/**Getting all participants on portFolly*/
				List<Long> allParticipants = valuatorProcessValidation.getParticipantOnPortfolly(instrumentType);
				/**If not exist any criteria we must to get all participant*/
				if(securities==null && participants == null && accounts==null && holders==null){
					participants = allParticipants;
					indAllPortfolly = BooleanType.YES.getCode();
				}else if(participants!=null){
					if(participants.containsAll(allParticipants) && allParticipants.containsAll(participants)){
						indAllPortfolly = BooleanType.YES.getCode();
					}
				}
				
				/**FIRST WE NEED TO CREATE VALUATOR_PROCESS_EXECUTION*/
				mainExecution = getProcessExecution(valuatorDate, executionType, participants, accounts,securities,holders,indAllPortfolly);
				
				/**CONFIG PARAMETERS OF PROCESS*/
				mainExecution.setValuatorExecutionParameters(getExecutionParameters(mainExecution, participants, accounts, securities,holders));
				
				/**1.-	PERSIST PROCESS ON DATABASE*/
				valuatorSecuritiesFacade.registerProcessExecution(mainExecution,(securities!=null && !securities.isEmpty()));
				
				/**REVERTIMOS SALDOS BLOQUEADOS EN OPERACIONES SELID*///issue 1040	
				registerUnblockBalanceSelidBatch(processLogger);
				
				Integer count = 0;
				/** VERIFICAMOS QUE NO EXISTAN SALDOS EN TRANSITO DE OPERACIONES SELID*///issue 1040
				while(verifyUnblockBalanceSelidBatch().equals(true)) {
					count++;
					System.out.println("============ REVIRTIENDO SALDOS SELID contador: " + count);
					Thread.sleep(20000); // 20 seconds waiting to continue
				}
				
				/**2.-  SECOND VALIDATE SECURITIES*/
				securities = validateValuatorProcess(securities, instrumentType, accounts, holders, participants,mainExecution);
				
				/**3.-	THIRD INVOCATE VALUATOR BY SECURITY*/
				executeValuatorProcess(mainExecution,securities, participants, accounts, currency, valuatorDate, instrumentType, SECURITY.getCode());
				
				/**4.-	FOUR INVOCATE VALUATOR BY BALANCES*/
				executeValuatorProcess(mainExecution,securities, participants, accounts, currency, valuatorDate, instrumentType, BALANCE.getCode());
				
				/**5.-	FIVE INVOCATE VALUATOR BY OPERATIONS, LIKE REPORTING, BLOCK_OPERATION*/
				executeValuatorProcess(mainExecution,securities, participants, accounts, currency, valuatorDate, instrumentType, OPERATIONS.getCode());
				
				/**6.-	SIX PERSIST PHYSICAL BALANCE */
				balancePsyFacade.processDailyBalancePsy(); //issue 507
				
				/**7.-	UPDATE HMB BALANCE 0 IND 1 */
				valuatorSecuritiesFacade.updateHolderMarketfactBalance(); //issue 238			
				
				/**8.-  BLOQUEAMOS SALDOS OPERACIONES SELID */ //issue 1040	
				registerBlockBalanceSelidBatch(processLogger);
				 
			}
		} catch (ServiceException _ex  ) {
			e = _ex;
		} catch (Exception _ex) {
			ex = _ex;
		}
		
		if(ex!=null || e!=null){
			/**Config notification to send detail of error at user who send the process*/
			BusinessProcessType businesProcessType = BusinessProcessType.VALUATOR_EXECUTION_CORE_PROCESS;
			if(e!=null && e.getMessage()!=null){
				errorMessage = PropertiesUtilities.getExceptionMessage(e.getMessage(),e.getParams(),new Locale("es"));
//				if(errorMessage.trim().length()>ValuatorConstants.SIZE_OF_MAX_STRING){
//					notifMessage = (errorMessage.substring(0, ValuatorConstants.SIZE_OF_MAX_STRING)).trim();
//				}else{
//					notifMessage = errorMessage;
//				}
				
			}else{
				errorMessage = "ERROR AL EJECUTAR EL VALORADOR"+" "+ex.getMessage()!=null?ex.getMessage():"";
			}
			
			BusinessProcess bp = new BusinessProcess();
			bp.setIdBusinessProcessPk(businesProcessType.getCode());
			UserAccountSession userSession = new UserAccountSession();
			userSession.setUserName(userName);
			userSession.setEmail(userEmail);
			userSession.setFullName(userName);
			userSession.setPhoneNumber(userName);
			notificationFacade.sendNotificationManual(userName, bp, Arrays.asList(userSession), businesProcessType.getDescription(), notifMessage, NotificationType.EMAIL);
			
			/***Invoke method to finish event*/
			valuatorSecuritiesFacade.finishValuatorEvent();
			valuatorSecuritiesFacade.reloadMarketPrices(valuatorDate);
		}

		try {
			/**Save final state of process*/
			valuatorSecuritiesFacade.finishValuatorExecutionProcess(mainExecution.getIdProcessExecutionPk(), errorMessage);
		} catch (ServiceException e1) {
			e1.printStackTrace();
		}
	}
	
	/**
	 * Execute valuator process.
	 *
	 * @param procesExecution the proces execution
	 * @param securityCodes the security codes
	 * @param participants the participants
	 * @param accounts the accounts
	 * @param valuatorCurrency the valuator currency
	 * @param valuatorDate the valuator date
	 * @param _instrumentType the _instrument type
	 * @param execDetType the exec det type
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("incomplete-switch")
	private void executeValuatorProcess(ValuatorProcessExecution procesExecution, List<String> securityCodes, List<Long> participants, 
										List<Long> accounts, Integer valuatorCurrency,Date valuatorDate, Integer _instrumentType, Integer execDetType) 
																																		throws ServiceException{
		List<ValuatorExecutionDetail> lstExecutionDetail = getExecutionDetail(procesExecution,execDetType);
		valuatorSecuritiesFacade.registerProcessExecutionDetail(lstExecutionDetail, participants, securityCodes, accounts);
		Long processPk = procesExecution.getIdProcessExecutionPk();
		/**SI LA VALORACION ES POR VALORES*/
		if(execDetType.equals(ValuatorDetailType.SECURITY.getCode())){
			for(InstrumentType instrumenType: InstrumentType.list){
				if(_instrumentType!=null && !_instrumentType.equals(instrumenType.getCode())){
					continue;
				}
				switch(instrumenType){
					case FIXED_INCOME: 
							valuatorSecuritiesFacade.executeFixedIncomeValuator(processPk,execDetType, valuatorCurrency, valuatorDate);break;
					case VARIABLE_INCOME: 
							valuatorSecuritiesFacade.executeVariableIncomeValuator(processPk,execDetType,valuatorCurrency, valuatorDate);break;
				}
			}
		}else if(execDetType.equals(ValuatorDetailType.BALANCE.getCode())){
			Integer valuatorType = procesExecution.getValuatorType();
			valuatorSecuritiesFacade.executeBalanceValuator(processPk,valuatorType);
		}else if(execDetType.equals(ValuatorDetailType.OPERATIONS.getCode())){
//			valuatorLiveOperation.updateLiveOperation(securityCodes, valuatorDate,processPk);
		}
	}
		
	/**
	 * Metodo para verificar la reversion de saldos en operaciones selid
	 * @return
	 */
	public Boolean verifyUnblockBalanceSelidBatch () {
	
		Boolean result = valuatorSecuritiesFacade.verifyUnblockBalanceSelidBatchBoolean();
		
		return result;
	}
	
	/**
	 * Metodo para registrar el batch de reversion de bloqueo de saldos en operaciones SELID. issue 1040
	 */
	@LoggerAuditWeb
	private void registerUnblockBalanceSelidBatch(ProcessLogger processLogger){
		
		Schedulebatch scheduleBatch = new Schedulebatch();
        scheduleBatch.setIdBusinessProcess(BusinessProcessType.VALUATOR_BALANCE_UNBLOCK_BUY_SELL_SELID.getCode());
        scheduleBatch.setUserName(processLogger.getIdUserAccount());
        scheduleBatch.setPrivilegeCode(processLogger.getLastModifyApp());
        scheduleBatch.setParameters(new HashMap<String, String>());
    	clientRestService.excuteRemoteBatchProcess(scheduleBatch,"iSettlements");

	}
	
	/**
	 * Metodo para registrar el batch de bloqueo de saldos en operaciones SELID. issue 1040
	 */
	@LoggerAuditWeb
	private void registerBlockBalanceSelidBatch(ProcessLogger processLogger){
		
		Schedulebatch scheduleBatch = new Schedulebatch();
        scheduleBatch.setIdBusinessProcess(BusinessProcessType.VALUATOR_BALANCE_UPDATE_BUY_SELL_SELID.getCode());
        scheduleBatch.setUserName(processLogger.getIdUserAccount());
        scheduleBatch.setPrivilegeCode(processLogger.getLastModifyApp());
        scheduleBatch.setParameters(new HashMap<String, String>());
    	clientRestService.excuteRemoteBatchProcess(scheduleBatch,"iSettlements");
		
	}
	
	/**
	 * *
	 * Main Method to begin validations.
	 *
	 * @param securities the securities
	 * @param instrument the instrument
	 * @param accounts the accounts
	 * @param holders the holders
	 * @param participants the participants
	 * @param procesExecution the proces execution
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<String> validateValuatorProcess(List<String> securities, Integer instrument, List<Long> accounts, List<Long> holders, List<Long> participants,
												ValuatorProcessExecution procesExecution)throws ServiceException{
		
		List<ValuatorExecutionDetail> lstExecutionDetail = getExecutionDetail(procesExecution,ValuatorDetailType.VALIDATIONS.getCode());
		valuatorSecuritiesFacade.registerProcessExecutionDetail(lstExecutionDetail, participants, securities, accounts);
		
		/**If valuator process is by securities, validate securities exist's*/
		if(securities!=null && !securities.isEmpty()){
			valuatorSecuritiesFacade.validateSecuritiesInPortfolly(procesExecution.getValuatorExecutionParameters());
		}
		
		/**Methods to validate before to begin valuator process*/
		valuatorProcessValidation.validateSecurityInBalances(securities, instrument, accounts, holders, participants,procesExecution.getIdProcessExecutionPk());
		valuatorProcessValidation.validateMarketFactFile(procesExecution);
		valuatorProcessValidation.validateMarketFactInconsistencies(procesExecution);
		valuatorProcessValidation.validateBegindEndDay(procesExecution);
		
		return null;
	}
	
	/**
	 * Populate parameters value.
	 *
	 * @param value the value
	 * @return the object
	 */
	private Object populateParametersValue(String value){
		List<Object> dataValue = new ArrayList<Object>();
//		if(StringUtils.containsAny(value, ValuatorConstants.STR_COMMA)){
//			for(String valueData: Arrays.asList(value.split(ValuatorConstants.STR_COMMA))){
//				dataValue.add(valueData.trim());
//			}
//		}else{
//			dataValue.addAll(Arrays.asList(value.trim()));
//		}
		return dataValue;
	}
	
	/**
	 * Populate parameters long value.
	 *
	 * @param value the value
	 * @return the object
	 */
	private Object populateParametersLongValue(String value){
		List<Long> dataValue = new ArrayList<Long>();
//		if(StringUtils.containsAny(value, ValuatorConstants.STR_COMMA)){
//			for(String valueData: Arrays.asList(value.split(ValuatorConstants.STR_COMMA))){
//				dataValue.add(new Long(valueData.trim()));
//			}
//		}else{
//			dataValue.addAll(Arrays.asList(new Long(value.trim())));
//		}
		return dataValue;
	}
	
	/**
	 * METHOD TO INICIALIZATE VALUATOR_PROCESS_EXECUTION OBJECT.
	 *
	 * @param processDate the process date
	 * @param executorType the executor type
	 * @param participants the participants
	 * @param accounts the accounts
	 * @param securities the securities
	 * @param holders the holders
	 * @param indAllPortfolly the ind all portfolly
	 * @return the process execution
	 * @throws ServiceException the service exception
	 */
	private ValuatorProcessExecution getProcessExecution(Date processDate ,Integer executorType, List<Long> participants, List<Long> accounts, 
														 List<String> securities, List<Long> holders, Integer indAllPortfolly) throws ServiceException{
		ValuatorProcessOperation processOperation = new ValuatorProcessOperation();
		processOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		processOperation.setRegistryUser(userSession);
		
		Long operationType = BusinessProcessType.VALUATOR_EXECUTION_CORE_PROCESS.getCode();
		processOperation.setOperationType(operationType);
		processOperation.setOperationNumber(valuatorSecuritiesFacade.findValuatorProcessNumber(operationType));
		
		/**PUT RELATION OF EXECUTION WITH PROCESS OPERATION*/
		ValuatorProcessExecution processExecution = new ValuatorProcessExecution();
		processExecution.setBeginTime(CommonsUtilities.currentDateTime());
		processExecution.setExecutionType(executorType);
		processExecution.setProcessDate(processDate);
		processExecution.setRegistryDate(CommonsUtilities.currentDateTime());
		processExecution.setRegistryUser(userSession);
		processExecution.setIndAllPortFolly(indAllPortfolly);
		
		if((participants==null || participants.isEmpty()) && (accounts==null || accounts.isEmpty()) && (securities==null || securities.isEmpty()) && 
		   (holders == null || holders.isEmpty())){
			processExecution.setValuatorType(ValuatorType.ENTIRE_PORTFOLIO.getCode());
		}else{
			if(participants!=null && !participants.isEmpty()){
				processExecution.setValuatorType(ValuatorType.BY_PARTICIPANT.getCode());
			}else if(accounts!=null && !accounts.isEmpty()){
				processExecution.setValuatorType(ValuatorType.BY_INVESTOR.getCode());
			}else if(securities!=null && !securities.isEmpty()){
				processExecution.setValuatorType(ValuatorType.BY_SECURITY.getCode());
			}else if(holders!=null && !holders.isEmpty()){
				processExecution.setValuatorType(ValuatorType.BY_INVESTOR.getCode());
			}
		}
		
		processExecution.setProcessState(ValuatorExecutionStateType.VALUATING.getCode());
		processExecution.setValuatorProcessOperation(processOperation);
		processExecution.setBeginTime(CommonsUtilities.currentDateTime());
		return processExecution;
	}
	
	/**
	 * METHOD TO CREATE VALUATOR_EXECUTION_DETAIL LIKE JAVA OBJECT.
	 *
	 * @param execution the execution
	 * @param executionDetailt the execution detailt
	 * @return the execution detail
	 * @throws ServiceException the service exception
	 */
	private List<ValuatorExecutionDetail> getExecutionDetail(ValuatorProcessExecution execution, Integer executionDetailt) throws ServiceException{
		List<ValuatorExecutionDetail> lstExecutionDetail = new ArrayList<>();
		
		List<ValuatorEventBehavior> lstValuatorEvent = valuatorEventBehaviors;
		
		if(lstValuatorEvent!=null){
			/**Variable to handle orderExecution in detail*/
			Integer orderOnDetail = ComponentConstant.ZERO;
			/**Object to handle main execution detail and put like father of it*/
			ValuatorExecutionDetail mainExecutionDetail = null;
			/**Iterate execution detail, and put values*/
			for(ValuatorEventBehavior eventBehavior: lstValuatorEvent){
				
				if(eventBehavior.getValuatorDetailType().equals(executionDetailt)){
					ValuatorExecutionDetail processExecutionDetail = new ValuatorExecutionDetail();
					processExecutionDetail.setDetailType(executionDetailt);
					processExecutionDetail.setQuantityFinished(GeneralConstants.ZERO_VALUE_LONG);
					processExecutionDetail.setQuantityInconsistent(GeneralConstants.ZERO_VALUE_LONG);
					processExecutionDetail.setQuantityProcess(GeneralConstants.ZERO_VALUE_LONG);
					processExecutionDetail.setBeginTime(CommonsUtilities.currentDateTime());
					processExecutionDetail.setValuatorEventBehavior(eventBehavior);
					
					/**If the main event, then the detail must be associated  at execution detail*/
					if(eventBehavior.getExecutionSequence().toString().length() == ComponentConstant.ONE){
						/**Assignment this execution detail like main*/
						mainExecutionDetail = processExecutionDetail;
						switch(ValuatorDetailType.get(executionDetailt)){
							case VALIDATIONS: 	processExecutionDetail.setOrderExecution(1);break;
							case SECURITY: 	 	processExecutionDetail.setOrderExecution(2);break;
							case BALANCE: 	 	processExecutionDetail.setOrderExecution(3);break;
							case OPERATIONS: 	processExecutionDetail.setOrderExecution(4);break;
						}
					}else{
						/**Adding orderExecution and executionDetail*/
						processExecutionDetail.setOrderExecution(++orderOnDetail);
						processExecutionDetail.setExecutionDetail(mainExecutionDetail);
					}
					
					processExecutionDetail.setValuatorProcessExecution(execution);
					processExecutionDetail.setRegistryUser(userSession);
					processExecutionDetail.setRegistryDate(CommonsUtilities.currentDateTime());
					processExecutionDetail.setProcessState(ValuatorExecutionStateType.REGISTERED.getCode());
					
					lstExecutionDetail.add(processExecutionDetail);
				}
			}
		}
		return lstExecutionDetail;
	}
	
	/**
	 * METHOD TO INICIALIZATE LIST OF VALUATOR_EXECUTION_PARAMETERS.
	 *
	 * @param valuatorExecutor the valuator executor
	 * @param participants the participants
	 * @param accounts the accounts
	 * @param securities the securities
	 * @param holders the holders
	 * @return the execution parameters
	 */
	private List<ValuatorExecutionParameters> getExecutionParameters(ValuatorProcessExecution valuatorExecutor ,List<Long> participants, 
																	 List<Long> accounts, List<String> securities, List<Long> holders){
		/**CREATE LIST TO HANDLE ALL PARAMETERS FOR THE PROCESS*/
		List<ValuatorExecutionParameters> parameterList = new ArrayList<ValuatorExecutionParameters>();
		
		/**VERIFIED THE PARAMETERS FOR ALL THE PARTICIPANTS*/
		if(participants!=null && !participants.isEmpty()){
			for(Long participantPk : participants){
				ValuatorExecutionParameters parameter = getValuatorExecutorParameter(valuatorExecutor);
				parameter.setParticipant(new Participant(participantPk));
				parameterList.add(parameter);
			}
		}
		
		/**VERIFIED THE PARAMETERS FOR ALL THE HOLDER_ACCOUNTS*/
		if(accounts!=null && !accounts.isEmpty()){
			for(Long accountPk : accounts){
				ValuatorExecutionParameters parameter = getValuatorExecutorParameter(valuatorExecutor);
				parameter.setHolderAccount(new HolderAccount(accountPk));
				parameterList.add(parameter);
			}
		}
		
		if(holders!=null && !holders.isEmpty()){
			try {
				for(Long accountPk : valuatorSecuritiesFacade.getAccountsFromHolder(holders)){
					ValuatorExecutionParameters parameter = getValuatorExecutorParameter(valuatorExecutor);
					parameter.setHolderAccount(new HolderAccount(accountPk));
					parameterList.add(parameter);
				}
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		/**VERIFIED THE PARAMETERS FOR ALL THE SECURITIES*/
		if(securities!=null && !securities.isEmpty()){
			for(String securityPk : securities){
				ValuatorExecutionParameters parameter = getValuatorExecutorParameter(valuatorExecutor);
				parameter.setSecurityCode(securityPk);
				parameterList.add(parameter);
			}
		}		
		return parameterList;
	}

	/**
	 * METHOD TO INICIALIZATE VALUATOR_EXECUTION_PARAMETER LIKE JAVA OBJECT.
	 *
	 * @param valuatorExecutor the valuator executor
	 * @return the valuator executor parameter
	 */
	private ValuatorExecutionParameters getValuatorExecutorParameter(ValuatorProcessExecution valuatorExecutor){
		ValuatorExecutionParameters parameter = new ValuatorExecutionParameters();
		parameter.setRegistryUser(userSession);
		parameter.setRegistryDate(CommonsUtilities.currentDateTime());
		parameter.setValuatorProcessExecution(valuatorExecutor);
		return parameter;
	}


	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}
	

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}
}