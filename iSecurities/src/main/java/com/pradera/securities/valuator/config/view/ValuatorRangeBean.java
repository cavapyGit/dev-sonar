
package com.pradera.securities.valuator.config.view;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.valuatorprocess.ValuatorTermRange;
import com.pradera.securities.issuancesecuritie.util.PropertiesConstants;
import com.pradera.securities.valuator.config.facade.ValuatorRangeFacade;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorRangeBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@DepositaryWebBean
public class ValuatorRangeBean extends GenericBaseBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant REGISTER_RANGE. */
	private static final String REGISTER_RANGE = "registerValuatorRange";
	
	/** The Constant SEARCH_RANGE. */
	private static final String SEARCH_RANGE = "searchRange";
	
	/** The valuator range facade. */
	@EJB private ValuatorRangeFacade valuatorRangeFacade;
	
	/** The valuator range to. */
	private ValuatorRangeTO valuatorRangeTO;
	
	/** The valuator term range. */
	private ValuatorTermRange valuatorTermRange;
	
	/** The valuator data model. */
	private GenericDataModel<ValuatorRangeTO> valuatorDataModel;
	
	/** The valuator range selected. */
	private ValuatorRangeTO valuatorRangeSelected;
	
	/** The user info. */
	@Inject private UserInfo userInfo;
	
	/** The depositary setup. */
	@Inject @DepositarySetup IdepositarySetup depositarySetup;

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		valuatorTermRange = new ValuatorTermRange();
		loadPriviligies();
		cleanSearchForm();
	}

	/**
	 * Load priviligies.
	 */
	private void loadPriviligies() {
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnModifyView(Boolean.TRUE);
		privilegeComponent.setBtnDeleteView(Boolean.TRUE);
		userInfo.setPrivilegeComponent(privilegeComponent);
	}
	
	/**
	 * Clean search form.
	 */
	public void cleanSearchForm() {
		JSFUtilities.resetViewRoot();
		valuatorRangeTO = new ValuatorRangeTO();
		valuatorRangeTO.setInitialDate(CommonsUtilities.currentDateTime());
		valuatorRangeTO.setFinalDate(CommonsUtilities.currentDateTime());
		valuatorDataModel = null;
		valuatorRangeSelected = null;
	}
	
	/**
	 * Before save term range.
	 */
	public void beforeSaveTermRange(){
		String messageProperties = "";
		Boolean haveValidation = Boolean.FALSE;
		switch(ViewOperationsType.get(getViewOperationType())){
			case REGISTER: 	messageProperties = PropertiesConstants.VALUATOR_PROCESS_CONFIG_RANGE_REGISTER_CONFIRM;
							haveValidation 	  = valuatorTermRangeExists();
							break;
			case MODIFY:	messageProperties = PropertiesConstants.VALUATOR_PROCESS_CONFIG_RANGE_MODIFY_CONFIRM;
							break;
			case DELETE:	messageProperties = PropertiesConstants.VALUATOR_PROCESS_CONFIG_RANGE_DELETE_CONFIRM;
							break;
			default:		break;
		}
		if(!ViewOperationsType.get(getViewOperationType()).equals(ViewOperationsType.DELETE)){
			if(!haveValidation)
				haveValidation = validateRangeDif(valuatorTermRange);
			if(!haveValidation)
				haveValidation = validateRange(valuatorTermRange);
		}
		
		if(!haveValidation){
			Long idTermRangePk = valuatorTermRange.getIdTermRangePk();
			String valuatorTermRange = null;
			if(idTermRangePk!=null){
				valuatorTermRange = idTermRangePk.toString();
			}
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(
					GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(messageProperties,valuatorTermRange));
			JSFUtilities.executeJavascriptFunction("PF('cnfwValuatorTermRange').show()");
		}
	}
	
	/**
	 * Save new term range.
	 */
	@LoggerAuditWeb
	public void saveNewTermRange(){
		
		try{
			switch(ViewOperationsType.get(getViewOperationType())){
				case REGISTER: 	registerTermRange();break;
				case MODIFY:  	modifyTermRange();break;
				case DELETE:  	deleteTermRange();break;
				default:		break;
			}
		}catch(ServiceException ex){
			if(ex.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(
						GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getExceptionMessage(
								ex.getErrorService().getMessage(),
								ex.getParams()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
		JSFUtilities.showSimpleEndTransactionDialog(SEARCH_RANGE);
		searchTermRanges();
		valuatorRangeSelected = null;
	}
	
	/**
	 * Clean register.
	 */
	public void cleanRegister(){
		JSFUtilities.resetViewRoot();
		valuatorTermRange = new ValuatorTermRange();
		valuatorTermRange.setRegistryDate(CommonsUtilities.currentDateTime());
	}
	
	/**
	 * New valuator term range.
	 *
	 * @return the string
	 */
	public String newValuatorTermRange(){
		setViewOperationType(ViewOperationsType.REGISTER.getCode());
		valuatorTermRange = new ValuatorTermRange();
		valuatorTermRange.setRegistryDate(CommonsUtilities.currentDateTime());
		return REGISTER_RANGE;
	}
	
	/**
	 * View term range.
	 *
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String viewTermRange() throws ServiceException {
		if(valuatorRangeSelected!=null){
			setViewOperationType(ViewOperationsType.MODIFY.getCode());
			valuatorTermRange = valuatorRangeFacade.viewTermRange(valuatorRangeSelected.getIdTermRangePk());
			return REGISTER_RANGE;
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getExceptionMessage(
							ErrorServiceType.VALUATOR_CONFIG_RANGE_UNSELECTED.getMessage()));
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}
	}
	
	/**
	 * Before delete term range.
	 *
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String beforeDeleteTermRange() throws ServiceException {
		if(valuatorRangeSelected!=null){
			setViewOperationType(ViewOperationsType.DELETE.getCode());
			valuatorTermRange = valuatorRangeFacade.viewTermRange(valuatorRangeSelected.getIdTermRangePk());
			return REGISTER_RANGE;
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getExceptionMessage(
							ErrorServiceType.VALUATOR_CONFIG_RANGE_UNSELECTED.getMessage()));
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}
	}
	
	/**
	 * Delete term range.
	 *
	 * @throws ServiceException the service exception
	 */
	public void deleteTermRange() throws ServiceException {
		valuatorRangeFacade.deleteTermRange(valuatorTermRange);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(
				GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities.getMessage(
						PropertiesConstants.VALUATOR_PROCESS_CONFIG_RANGE_DELETE_SUCCESS,
						new Object[]{valuatorTermRange.getIdTermRangePk().toString()}));
		JSFUtilities.showSimpleValidationDialog();
	}
	
	/**
	 * Register term range.
	 *
	 * @throws ServiceException the service exception
	 */
	public void registerTermRange() throws ServiceException {
		
		valuatorTermRange.setRegistryUser(userInfo.getUserAccountSession().getUserName());
		valuatorTermRange.setRegistryDate(CommonsUtilities.currentDateTime());
		ValuatorTermRange termRange = valuatorRangeFacade.registerTermRange(valuatorTermRange);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(
				GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities.getMessage(
						PropertiesConstants.VALUATOR_PROCESS_CONFIG_RANGE_REGISTER_SUCCES,
						new Object[]{termRange.getIdTermRangePk().toString()}));
	}
	
	/**
	 * Modify term range.
	 *
	 * @throws ServiceException the service exception
	 */
	public void modifyTermRange() throws ServiceException {
		
		ValuatorTermRange termRange = valuatorRangeFacade.modifyTermRange(valuatorTermRange);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(
				GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(
						PropertiesConstants.VALUATOR_PROCESS_CONFIG_RANGE_MODIFY_SUCCESS,
						new Object[]{termRange.getIdTermRangePk().toString()}));
	}
	
	/**
	 * Search term ranges.
	 */
	@LoggerAuditWeb
	public void searchTermRanges(){
		if(!validateDiference()){
			List<ValuatorRangeTO> valuatorRangeTOList = getValuatorRangeTOList(valuatorRangeTO);
			if(valuatorRangeTOList==null){
				valuatorDataModel = new GenericDataModel<ValuatorRangeTO>();
			}else{
				valuatorDataModel = new GenericDataModel<ValuatorRangeTO>(valuatorRangeTOList);
			}
		}
	}
	
	/**
	 * Gets the valuator range to list.
	 *
	 * @param to the to
	 * @return the valuator range to list
	 */
	public List<ValuatorRangeTO> getValuatorRangeTOList(ValuatorRangeTO to){
		List<ValuatorRangeTO> list = null;
		try {
			list = valuatorRangeFacade.getValuatorRangeFacade(to);
		} catch (ServiceException ex) {
			if(ex.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(
						GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getExceptionMessage(ex.getMessage()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
		return list;
	}
	
	/**
	 * Convert code range to string format to.
	 */
	public void convertCodeRangeToStringFormatTO(){
		if(valuatorRangeTO.getRangeCode() != null
				&& valuatorRangeTO.getRangeCode().length()==GeneralConstants.ONE_VALUE_INTEGER)
			valuatorRangeTO.setRangeCode(GeneralConstants.ZERO_VALUE_STRING+valuatorRangeTO.getRangeCode());
	}
	
	/**
	 * Validate diference.
	 *
	 * @return the boolean
	 */
	public Boolean validateDiference(){
		if(valuatorRangeTO.getMinRange() != null && valuatorRangeTO.getMaxRange() != null){
			if(valuatorRangeTO.getMinRange().intValue() >= valuatorRangeTO.getMaxRange().intValue()){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getExceptionMessage(ErrorServiceType.VALUATOR_CONFIG_RANGE_LESS_THAN.getMessage()));
				JSFUtilities.showSimpleValidationDialog();
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Convert code range to string format object.
	 */
	public void convertCodeRangeToStringFormatObject(){
		if(valuatorTermRange.getRangeCode().length()==GeneralConstants.ONE_VALUE_INTEGER)
			valuatorTermRange.setRangeCode(GeneralConstants.ZERO_VALUE_STRING+valuatorTermRange.getRangeCode());
	}
	
	/**
	 * Valuator term range exists.
	 *
	 * @return the boolean
	 */
	public Boolean valuatorTermRangeExists() {
		if(valuatorTermRange.getRangeCode()!=null){
			Boolean exists = Boolean.FALSE;
			String propertieKey = null;
			Object[] parameters = null;
			
			try {
				convertCodeRangeToStringFormatObject();
				exists = valuatorRangeFacade.valuatorTermRangeExists(valuatorTermRange);
				propertieKey = ErrorServiceType.VALUATOR_CONFIG_RANGE_EXISTS.getMessage();
				parameters = new Object[]{valuatorTermRange.getRangeCode()};
			} catch (ServiceException e) {
				propertieKey = e.getMessage();
				parameters = e.getParams();
			} finally {
				if(exists){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(
							GeneralConstants.LBL_HEADER_ALERT_ERROR),
							PropertiesUtilities.getExceptionMessage(propertieKey,parameters));
					JSFUtilities.showSimpleValidationAltDialog();
					valuatorTermRange.setRangeCode(null);
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Validate range.
	 *
	 * @param valuatorTermRange the valuator term range
	 * @return the boolean
	 */
	public Boolean validateRange(ValuatorTermRange valuatorTermRange) {
		ValuatorRangeTO to = new ValuatorRangeTO();
		to.setIdTermRangePk(valuatorTermRange.getIdTermRangePk());
		to.setMinRange(valuatorTermRange.getMinRange());
		to.setMaxRange(valuatorTermRange.getMaxRange());
		
		Boolean validatingValues = validateRangeValues(
				to,
				ErrorServiceType.VALUATOR_CONFIG_RANGE_MIN.getMessage(),
				new Object[]{Integer.parseInt(to.getMinRange().toString())},
				Boolean.TRUE);
		
		if(validatingValues){
			return true;
		}else if(validateRangeDif(valuatorTermRange)){
			return true;
		}
		return false;
	}

	/**
	 * Validate range values.
	 *
	 * @param to the to
	 * @param propertieKeyValue the propertie key value
	 * @param parametersValue the parameters value
	 * @param validatingMax the validating max
	 * @return the boolean
	 */
	public Boolean validateRangeValues(ValuatorRangeTO to, String propertieKeyValue, Object[] parametersValue, Boolean validatingMax){
		
		List<ValuatorRangeTO> list = getValuatorRangeTOList(new ValuatorRangeTO());
		Integer minValue = null;
		Integer maxValue = null;
		Integer newMaxRange = null;
		String propertieKey = null;
		Object[] parameters = null;
		
		if(to.getMinRange() != null)
			minValue = to.getMinRange().intValue();
		if(to.getMaxRange() != null)
			maxValue = to.getMaxRange().intValue();
		
		if(ViewOperationsType.get(getViewOperationType()).equals(ViewOperationsType.MODIFY)){
			for (ValuatorRangeTO rangeTO : list) {
				if(rangeTO.getIdTermRangePk().equals(valuatorTermRange.getIdTermRangePk())){
					list.remove(rangeTO);
					break;
				}
			}
		}
		
		for (int i = 0; i < list.size(); i++) {
			ValuatorRangeTO range = list.get(i);
			
			if(minValue != null && minValue <= range.getMaxRange().intValue()){
				if(minValue >= range.getMinRange().intValue()){
					propertieKey = propertieKeyValue;
					parameters = parametersValue;
					valuatorTermRange.setMinRange(null);
					if(!validatingMax)
						valuatorTermRange.setMaxRange(null);
					break;
				}else{
					if(i < list.size() - ComponentConstant.ONE ){
						newMaxRange = (list.get(i)).getMinRange().intValue() - ComponentConstant.ONE;
						break;
					}
				}
			}
		}
		
		if(validatingMax && maxValue != null && newMaxRange != null){
			if(maxValue > newMaxRange){
				propertieKey = ErrorServiceType.VALUATOR_CONFIG_RANGE_MAX.getMessage();
				parameters = new Object[]{maxValue.toString()};
				valuatorTermRange.setMaxRange(null);
			}
		}
		
		if(propertieKey!=null){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(propertieKey,parameters));
			JSFUtilities.showSimpleValidationDialog();
			return true;
		}
		return false;
	}
	
	/**
	 * Validate min range.
	 */
	@LoggerAuditWeb
	public void validateMinRange(){
		if(valuatorTermRange.getMinRange() != null){
			ValuatorRangeTO to = new ValuatorRangeTO();
			to.setMinRange(valuatorTermRange.getMinRange());
			to.setMaxRange(valuatorTermRange.getMaxRange());
			Integer minValue = to.getMinRange().intValue();
			Boolean validating = validateRangeDif(valuatorTermRange);
			if(!validating)
				validateRangeValues(
						to,
						ErrorServiceType.VALUATOR_CONFIG_RANGE_MIN.getMessage(),
						new Object[]{minValue.toString()},
						Boolean.TRUE);
		}
	}
	
	/**
	 * Validate max range.
	 */
	@LoggerAuditWeb
	public void validateMaxRange(){
		if(valuatorTermRange.getMaxRange()!=null){
			ValuatorRangeTO to = new ValuatorRangeTO();
			to.setMinRange(valuatorTermRange.getMaxRange());
			Integer minValue = to.getMinRange().intValue();
			
			Boolean validating = validateRangeDif(valuatorTermRange);
			if(!validating)
				validateRangeValues(
						to,
						ErrorServiceType.VALUATOR_CONFIG_RANGE_MAX.getMessage(),
						new Object[]{minValue.toString()},
						Boolean.FALSE);
		}
	}
	
	/**
	 * Validate range dif.
	 *
	 * @param valuatorTermRange the valuator term range
	 * @return the boolean
	 */
	public Boolean validateRangeDif(ValuatorTermRange valuatorTermRange){
		
		String propertieKey = null;
		Object[] parameters = null;
		Integer minValue = null;
		Integer maxValue = null;
		
		if(valuatorTermRange.getMinRange() != null)
			minValue = valuatorTermRange.getMinRange().intValue();
		if(valuatorTermRange.getMaxRange() != null)
			maxValue = valuatorTermRange.getMaxRange().intValue();
		
		if(minValue != null && maxValue != null){
			if(minValue.equals(maxValue)){
				propertieKey = ErrorServiceType.VALUATOR_CONFIG_RANGE_DIF.getMessage();
				parameters = new Object[]{maxValue.toString()};
			}
			else if(minValue > maxValue){
				propertieKey = ErrorServiceType.VALUATOR_CONFIG_RANGE_LESS_THAN.getMessage();
			}
		}
		
		if(propertieKey!=null){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(propertieKey,parameters));
			JSFUtilities.showSimpleValidationDialog();
			return true;
		}
		return false;
	}

	/**
	 * Gets the valuator range facade.
	 *
	 * @return the valuator range facade
	 */
	public ValuatorRangeFacade getValuatorRangeFacade() {
		return valuatorRangeFacade;
	}

	/**
	 * Sets the valuator range facade.
	 *
	 * @param valuatorRangeFacade the new valuator range facade
	 */
	public void setValuatorRangeFacade(ValuatorRangeFacade valuatorRangeFacade) {
		this.valuatorRangeFacade = valuatorRangeFacade;
	}

	/**
	 * Gets the valuator range to.
	 *
	 * @return the valuator range to
	 */
	public ValuatorRangeTO getValuatorRangeTO() {
		return valuatorRangeTO;
	}

	/**
	 * Sets the valuator range to.
	 *
	 * @param valuatorRangeTO the new valuator range to
	 */
	public void setValuatorRangeTO(ValuatorRangeTO valuatorRangeTO) {
		this.valuatorRangeTO = valuatorRangeTO;
	}

	/**
	 * Gets the valuator term range.
	 *
	 * @return the valuator term range
	 */
	public ValuatorTermRange getValuatorTermRange() {
		return valuatorTermRange;
	}

	/**
	 * Sets the valuator term range.
	 *
	 * @param valuatorTermRange the new valuator term range
	 */
	public void setValuatorTermRange(ValuatorTermRange valuatorTermRange) {
		this.valuatorTermRange = valuatorTermRange;
	}

	/**
	 * Gets the valuator data model.
	 *
	 * @return the valuator data model
	 */
	public GenericDataModel<ValuatorRangeTO> getValuatorDataModel() {
		return valuatorDataModel;
	}

	/**
	 * Sets the valuator data model.
	 *
	 * @param valuatorDataModel the new valuator data model
	 */
	public void setValuatorDataModel(
			GenericDataModel<ValuatorRangeTO> valuatorDataModel) {
		this.valuatorDataModel = valuatorDataModel;
	}

	/**
	 * Gets the valuator range selected.
	 *
	 * @return the valuator range selected
	 */
	public ValuatorRangeTO getValuatorRangeSelected() {
		return valuatorRangeSelected;
	}

	/**
	 * Sets the valuator range selected.
	 *
	 * @param valuatorRangeSelected the new valuator range selected
	 */
	public void setValuatorRangeSelected(ValuatorRangeTO valuatorRangeSelected) {
		this.valuatorRangeSelected = valuatorRangeSelected;
	}

	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * Gets the depositary setup.
	 *
	 * @return the depositary setup
	 */
	public IdepositarySetup getDepositarySetup() {
		return depositarySetup;
	}

	/**
	 * Sets the depositary setup.
	 *
	 * @param depositarySetup the new depositary setup
	 */
	public void setDepositarySetup(IdepositarySetup depositarySetup) {
		this.depositarySetup = depositarySetup;
	}
}