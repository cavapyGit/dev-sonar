package com.pradera.securities.valuator.core.files.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.beanio.BeanReaderErrorHandler;
import org.beanio.BeanReaderException;
import org.beanio.RecordContext;

import com.pradera.integration.common.validation.to.RecordValidationType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MarketfactFileErrorHandler.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class MarketfactFileErrorHandler implements BeanReaderErrorHandler, Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The total errors. */
	private List<RecordValidationType> totalErrors;
	
	/** The rejected marketfact. */
	private int rejectedMarketfact = 0;
	
	/** The Marketfact type. */
	private Integer MarketfactType; 
	
	/**
	 * Instantiates a new marketfact file error handler.
	 */
	public MarketfactFileErrorHandler() {
		super();
		totalErrors = new ArrayList<RecordValidationType>();
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.beanio.BeanReaderErrorHandler#handleError(org.beanio.BeanReaderException)
	 */
	@Override
	public void handleError(BeanReaderException ex) throws Exception {
		rejectedMarketfact++;
		RecordContext contex=ex.getRecordContext();
		if(contex.hasRecordErrors()){
			for(String error : contex.getRecordErrors()){
				RecordValidationType ref= new RecordValidationType();
				ref.setErrorDescription(error);
				ref.setLineNumber(contex.getLineNumber());
				totalErrors.add(ref);
			}
		}
		if(contex.hasFieldErrors()){
			for(String field : contex.getFieldErrors().keySet()){
				for(String error : contex.getFieldErrors(field)){
					RecordValidationType ref = new RecordValidationType();
					ref.setErrorDescription(error);
					ref.setLineNumber(contex.getLineNumber());
					ref.setFieldText(contex.getFieldText(field));
					ref.setField(field);
					totalErrors.add(ref);
				}
			}
		}
		
	}
	
	/**
	 * Gets the total errors.
	 *
	 * @return the total errors
	 */
	public List<RecordValidationType> getTotalErrors() {
		return totalErrors;
	}

	/**
	 * Sets the total errors.
	 *
	 * @param totalErrors the new total errors
	 */
	public void setTotalErrors(List<RecordValidationType> totalErrors) {
		this.totalErrors = totalErrors;
	}

	/**
	 * Gets the rejected marketfact.
	 *
	 * @return the rejected marketfact
	 */
	public int getRejectedMarketfact() {
		return rejectedMarketfact;
	}

	/**
	 * Sets the rejected marketfact.
	 *
	 * @param rejectedMarketfact the new rejected marketfact
	 */
	public void setRejectedMarketfact(int rejectedMarketfact) {
		this.rejectedMarketfact = rejectedMarketfact;
	}

	/**
	 * Gets the marketfact type.
	 *
	 * @return the marketfact type
	 */
	public Integer getMarketfactType() {
		return MarketfactType;
	}

	/**
	 * Sets the marketfact type.
	 *
	 * @param marketfactType the new marketfact type
	 */
	public void setMarketfactType(Integer marketfactType) {
		MarketfactType = marketfactType;
	}

	
}
