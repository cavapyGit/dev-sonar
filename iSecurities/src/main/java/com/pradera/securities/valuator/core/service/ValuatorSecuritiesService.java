package com.pradera.securities.valuator.core.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.ejb.Asynchronous;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJB;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.commons.configuration.ValuatorSetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.accreditationcertificates.AccreditationOperation;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.type.PhysicalCertificateStateType;
import com.pradera.model.custody.type.SituationType;
import com.pradera.model.custody.type.StateType;
import com.pradera.model.issuancesecuritie.AmortizationPaymentSchedule;
import com.pradera.model.issuancesecuritie.InterestPaymentSchedule;
import com.pradera.model.issuancesecuritie.ProgramAmortizationCoupon;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.settlement.SettlementUnfulfillment;
import com.pradera.model.valuatorprocess.ValuatorCode;
import com.pradera.model.valuatorprocess.ValuatorEventBehavior;
import com.pradera.model.valuatorprocess.ValuatorExecutionDetail;
import com.pradera.model.valuatorprocess.ValuatorInconsistencies;
import com.pradera.model.valuatorprocess.ValuatorMarketfactConsolidat;
import com.pradera.model.valuatorprocess.ValuatorPriceReport;
import com.pradera.model.valuatorprocess.ValuatorProcessExecution;
import com.pradera.model.valuatorprocess.ValuatorProcessSetup;
import com.pradera.model.valuatorprocess.ValuatorSecurityMarketfact;
import com.pradera.model.valuatorprocess.type.ValuatorExecutionStateType;
import com.pradera.model.valuatorprocess.type.ValuatorFormuleType;
import com.pradera.model.valuatorprocess.type.ValuatorType;
import com.pradera.securities.query.to.SecurityReportResultTO;
import com.pradera.securities.valuator.core.view.ValuatorInconsistenciesTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorSecuritiesService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ValuatorSecuritiesService extends CrudDaoServiceBean{
	
	/** The valuator aditional. */
	@EJB private ValuatorProcessAditional valuatorAditional;
	
	/** The range service. */
	@EJB private ValuatorRangesService rangeService;
	
	/** The valuator setup. */
	@Inject @ValuatorSetup ValuatorProcessSetup valuatorSetup;
	
	/** The free memory. */
	private Long freeMemory;
	
	/** The user session. */
	private String USER_SESSION = "ADMIN";
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		long allocatedMemory      = (Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory());
		freeMemory = Runtime.getRuntime().maxMemory() - allocatedMemory;
	}
	
	/**
	 * Creates the valuator code.
	 *
	 * @param valuatorCode the valuator code
	 * @param mapValuatorCodes the map valuator codes
	 * @param loggerUser the logger user
	 * @return the valuator code
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ValuatorCode createValuatorCode(String valuatorCode, ConcurrentMap<Long,String> mapValuatorCodes, LoggerUser loggerUser) throws ServiceException{
		/**CREATE OBJECT TO HANDLE VALUATOR CODE*/
		ValuatorCode valuatorObject = new ValuatorCode();
		valuatorObject.setIdValuatorCode(valuatorCode);
		
		/**PERSIST OBJECT ON BD*/
		create(valuatorObject,loggerUser);
		
		mapValuatorCodes.put(valuatorObject.getIdValuatorCodePk(), valuatorCode);
		
		/**FINISH OF METHOD*/
		return valuatorObject;
	}

	/**
	 * *
	 * Method to get securities and class according with valuator process.
	 *
	 * @param instrumentType the instrument type
	 * @param processPk the process pk
	 * @return the map
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public Map<Integer,List<String>> findSecurityList(Integer instrumentType, Long processPk) throws ServiceException{
		/**Find the valuator execution to know all parameters received*/
		ValuatorProcessExecution valuatorProcess = find(ValuatorProcessExecution.class, processPk);
		/**getting valuator type*/
		Integer valuatorType = valuatorProcess.getValuatorType();
		
		/**Query for getting all securities in HAB*/
		StringBuilder querySQL = new StringBuilder();
		StringBuilder whereSQL = new StringBuilder();
		/**Query for getting all securities in physical certificate*/
		StringBuilder queryForPhysical = new StringBuilder();
		StringBuilder whereForPhysical = new StringBuilder();
		/**Map to handle parameters*/
		Map<String,Object> parameters = new HashMap<>();
		/**Map to handle parameters for physical*/
		Map<String,Object> parametersForPhysical = new HashMap<>();
		/**List to save temporally all securities and return in method*/
		Map<Integer,List<String>> securityByClass = new HashMap<>();
		
		/**Main query on HAB*/
		querySQL.append("Select Distinct sec.securityClass, sec.idSecurityCodePk							");
		querySQL.append("From HolderAccountBalance				hab											");
		querySQL.append("Inner Join hab.security 				sec											");
		whereSQL.append("Where hab.totalBalance > 0	And sec.shareBalance > 0								");
		whereSQL.append("And   sec.stateSecurity IN :registeredState										");
		/**Assignment some parameters at map*/
		parameters.put("registeredState", Arrays.asList(SecurityStateType.REGISTERED.getCode(),SecurityStateType.BLOCKED.getCode()));
		
		/**Main query on physical certificate*/
		queryForPhysical.append("Select Distinct sec.securityClass, sec.idSecurityCodePk					");
		queryForPhysical.append("From PhysicalCertMarketfact mark											");
		queryForPhysical.append("Inner Join mark.physicalCertificate phys									");
		queryForPhysical.append("Inner Join phys.securities sec												");
		queryForPhysical.append("Inner Join phys.physicalBalanceDetails bal									");
		queryForPhysical.append("Where phys.state IN (:stateList)	And phys.situation	In(:situationList)	");
		queryForPhysical.append("And   mark.indActive = :oneParam											");
		whereForPhysical.append("And   sec.stateSecurity IN :registeredState								");
		
		/**Assignment some parameters at map*/
		parametersForPhysical.put("oneParam", BooleanType.YES.getCode());
		parametersForPhysical.put("situationList", Arrays.asList(SituationType.CUSTODY_VAULT.getCode(),
													  			 SituationType.DEPOSITARY_OPERATIONS.getCode(),
													  			 SituationType.DEPOSITARY_VAULT.getCode()));
		
		parametersForPhysical.put("stateList", Arrays.asList(PhysicalCertificateStateType.CONFIRMED.getCode(),
												  			 PhysicalCertificateStateType.AUTHORIZED.getCode(),
												  			 StateType.DEPOSITED.getCode()));
		parametersForPhysical.put("registeredState", Arrays.asList(SecurityStateType.REGISTERED.getCode(),SecurityStateType.BLOCKED.getCode()));

//		whereSQL.append("And sec.idSecurityCodePk LIKE 'DPF%'										");
		/**For query on HAB*/
		whereSQL.append("AND sec.instrumentType = :instrumentType										");
		parameters.put("instrumentType", instrumentType);
		
		/**For query on physical certificate*/
		whereForPhysical.append("AND sec.instrumentType = :instrumentType								");
		parametersForPhysical.put("instrumentType", instrumentType);
		
		if(valuatorType.equals(ValuatorType.BY_SECURITY.getCode())){
			/**For query on HAB*/
			whereSQL.append("AND sec.idSecurityCodePk IN (													");
			whereSQL.append("Select par.security.idSecurityCodePk											");
			whereSQL.append("From ValuatorExecutionParameters par											");
			whereSQL.append("Where par.valuatorProcessExecution.idProcessExecutionPk = :processPk)			");
			parameters.put("processPk", processPk);
			
			/**For query on physical certificate*/
			whereForPhysical.append("AND sec.idSecurityCodePk IN (											");
			whereForPhysical.append("Select par.security.idSecurityCodePk									");
			whereForPhysical.append("From ValuatorExecutionParameters par									");
			whereForPhysical.append("Where par.valuatorProcessExecution.idProcessExecutionPk = :processPk)	");
			parametersForPhysical.put("processPk", processPk);
		}
		
		if(valuatorType.equals(ValuatorType.BY_INVESTOR.getCode())){
			/**For query on HAB*/
			whereSQL.append("AND hab.holderAccount.idHolderAccountPk IN (									");
			whereSQL.append("Select par.holderAccount.idHolderAccountPk										");
			whereSQL.append("From ValuatorExecutionParameters par											");
			whereSQL.append("Where par.valuatorProcessExecution.idProcessExecutionPk = :processPk)			");
			parameters.put("processPk", processPk);
			
			/**For query on physical certificate*/
			whereForPhysical.append("AND bal.holderAccount.idHolderAccountPk IN (							");
			whereForPhysical.append("Select par.holderAccount.idHolderAccountPk								");
			whereForPhysical.append("From ValuatorExecutionParameters par									");
			whereForPhysical.append("Where par.valuatorProcessExecution.idProcessExecutionPk = :processPk)	");
			parametersForPhysical.put("processPk", processPk);
		}
		
		if(valuatorType.equals(ValuatorType.BY_PARTICIPANT.getCode())){
			/**For query on HAB*/
			whereSQL.append("AND hab.participant.idParticipantPk IN (										");
			whereSQL.append("Select par.participant.idParticipantPk											");
			whereSQL.append("From ValuatorExecutionParameters par											");
			whereSQL.append("Where par.valuatorProcessExecution.idProcessExecutionPk = :processPk)			");
			parameters.put("processPk", processPk);
			
			/**For query on physical certificate*/
			whereForPhysical.append("AND bal.participant.idParticipantPk IN (								");
			whereForPhysical.append("Select par.participant.idParticipantPk									");
			whereForPhysical.append("From ValuatorExecutionParameters par									");
			whereForPhysical.append("Where par.valuatorProcessExecution.idProcessExecutionPk = :processPk)	");
			parametersForPhysical.put("processPk", processPk);
		}
		
		/**Only for fixed income*/
		if(instrumentType.equals(InstrumentType.FIXED_INCOME.getCode())){
			/**For query on HAB*/
			whereSQL.append("And   sec.expirationDate > Trunc(:processDate)									");
			parameters.put("processDate", valuatorProcess.getProcessDate());
			
			/**For query on physical certificate*/
			whereForPhysical.append("And   sec.expirationDate > Trunc(:processDate)							");
			parametersForPhysical.put("processDate", valuatorProcess.getProcessDate());
		}
		
		/**For query on HAB*/
		whereSQL.append("Order By sec.securityClass		Desc												");
		/**For query on physical certificate*/
		whereForPhysical.append("Order By sec.securityClass		Desc										");
		
		/**Getting data from HAB*/
		for(Object[] security: (List<Object[]>)findListByQueryString(querySQL.toString().concat(whereSQL.toString()), parameters)){
			Integer securityClass = (Integer) security[0];
			
			if(securityByClass.get(securityClass)==null){
				securityByClass.put(securityClass, new ArrayList<String>());
			}
			securityByClass.get(securityClass).add((String) security[1]);
		}
		
		/**Getting data from physical certificate*/
		String fullPhysicalQuery = queryForPhysical.toString().concat(whereForPhysical.toString());
		List<Object[]> physicalSecurities = (List<Object[]>)findListByQueryString(fullPhysicalQuery, parametersForPhysical);
		if(physicalSecurities!=null && !physicalSecurities.isEmpty()){
			for(Object[] security: physicalSecurities){
				Integer securityClass = (Integer) security[0];
				
				if(securityByClass.get(securityClass)==null){
					securityByClass.put(securityClass, new ArrayList<String>());
				}
				String securityCode = (String) security[1];
				/**If security code doesn't exist*/
				if(!securityByClass.get(securityClass).contains(securityCode)){
					/**Add security code at map*/
					securityByClass.get(securityClass).add(securityCode);
				}
			}
		}
		
		return securityByClass;
	}
	
	/**
	 * Metodo para buscar el valor en REPORTO
	 * @param processDate
	 * @return
	 */
	public List<SecurityReportResultTO> searchSecurityReport(Date processDate){

		StringBuilder querySql = new StringBuilder();
		
		querySql.append("  select DISTINCT HMB.ID_SECURITY_CODE_PK,sec.INSTRUMENT_TYPE, HMB.MARKET_RATE, HMB.MARKET_DATE        ");
		querySql.append("  from MECHANISM_OPERATION MO                                                                          ");
		querySql.append("  inner join security sec on sec.ID_SECURITY_CODE_PK = MO.ID_SECURITY_CODE_FK                          ");
		querySql.append("  inner join SETTLEMENT_OPERATION so on so.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK    ");
		querySql.append("  inner join MARKET_FACT_VIEW HMB on HMB.ID_SECURITY_CODE_PK = MO.ID_SECURITY_CODE_FK                  ");
		querySql.append("  where 1 = 1                                                                                          ");
		querySql.append("  and MO.ID_NEGOTIATION_MODALITY_FK in (3,4)                                                           ");
		querySql.append("  and SO.SETTLEMENT_DATE = :processDate                                                                ");
		querySql.append("  AND HMB.CUT_DATE = :processDate                                                              		");
		querySql.append("  and so.OPERATION_PART = 1                                                                          	");
		querySql.append("  and HMB.REPORTED_BALANCE > 0                                                                         ");
		querySql.append("  order by 1 desc                                                                                      ");
		
		Query query = em.createNativeQuery(querySql.toString());
		query.setParameter("processDate", processDate);
		
		List<Object[]> lstRestul = query.getResultList();
		
		List<SecurityReportResultTO> searchTOList = new ArrayList<SecurityReportResultTO>();
		
		if (lstRestul.size()>0){
			
			for (Object[] lstFiltr : lstRestul){
				
				SecurityReportResultTO searchTO = new SecurityReportResultTO();
				
				searchTO.setSecurityCode(lstFiltr[0].toString());
				searchTO.setInstrumentType(Integer.valueOf(lstFiltr[1].toString()));
				if(Validations.validateIsNotNullAndNotEmpty(lstFiltr[2].toString())){
					searchTO.setMarketRate((BigDecimal)lstFiltr[2]);
				}
				searchTO.setMarketDate((Date)lstFiltr[3]);
				searchTOList.add(searchTO);
			}
		}
		
		return searchTOList;
	}
	
	
	/**
	 * Metodo para buscar el valor en REPORTO
	 * @param processDate
	 * @return
	 */
	public Security searchSecurity(String idSecurityCode) throws ServiceException{
		
		StringBuilder selectSQL = new StringBuilder();
		
		selectSQL.append("SELECT sec			");
		selectSQL.append("FROM Security sec						");
		selectSQL.append("inner join fetch sec.valuatorProcessClass vpc						");
		//selectSQL.append("inner join fetch sec.interestPaymentSchedule ips						");
		//selectSQL.append("inner join fetch sec.amortizationPaymentSchedule aps						");
		//selectSQL.append("join fetch ips.programInterestCoupons pic						");
		//selectSQL.append("join fetch aps.programAmortizationCoupons pac						");
		
		selectSQL.append("WHERE sec.idSecurityCodePk  = :idSecurityCode				");
		
		Map<String,Object> parameters = new HashMap<String,Object>();
		
		parameters.put("idSecurityCode", idSecurityCode);
		
		Security result = (Security)findObjectByQueryString(selectSQL.toString(), parameters);
		
		AmortizationPaymentSchedule aps = findAmortizationPaymentSchedule(idSecurityCode);
		InterestPaymentSchedule ips = findInterestPaymentSchedule(idSecurityCode);
		
		result.setAmortizationPaymentSchedule(aps);
		result.setInterestPaymentSchedule(ips);
		
		return (result) ;
	}
	
	/**
	 * Cuponera de Amortizacion
	 */
	public AmortizationPaymentSchedule findAmortizationPaymentSchedule(String idSecurityCodePk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT aps FROM AmortizationPaymentSchedule aps");
		//sbQuery.append("	INNER JOIN FETCH aps.programAmortizationCoupons pac");
		sbQuery.append("	WHERE aps.security.idSecurityCodePk = :idSecurityCodePk");
		//sbQuery.append("	AND pac.stateProgramAmortization = :state");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		//query.setParameter("state", ProgramScheduleStateType.PENDING.getCode());
		
		AmortizationPaymentSchedule result = null;
		try {
			result = (AmortizationPaymentSchedule) query.getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return result;
	}

	/**
	 * Cuponera de interes
	 */
	public InterestPaymentSchedule findInterestPaymentSchedule(String idSecurityCodePk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ips FROM InterestPaymentSchedule ips");
		//sbQuery.append("	INNER JOIN FETCH ips.programInterestCoupons pic");
		sbQuery.append("	WHERE ips.security.idSecurityCodePk = :idSecurityCodePk");
		//sbQuery.append("	AND pic.stateProgramInterest = :state");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		//query.setParameter("state", ProgramScheduleStateType.PENDING.getCode());
		
		InterestPaymentSchedule result = null;
		try {
			result = (InterestPaymentSchedule) query.getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return result;
	}
	
	
	
	/**
	 * Register execution detail.
	 *
	 * @param executionDetail the execution detail
	 * @return the valuator execution detail
	 * @throws ServiceException the service exception
	 */
	public ValuatorExecutionDetail registerExecutionDetail(ValuatorExecutionDetail executionDetail) throws ServiceException{
		return create(executionDetail);
	}
	
	/**
	 * 
	 * @param valuatorPriceReport
	 * @return
	 * @throws ServiceException
	 */
	public ValuatorPriceReport registerValuatorPriceReport(ValuatorPriceReport valuatorPriceReport) throws ServiceException{
		return create(valuatorPriceReport);
	}
	
	/**
	 * *
	 * Method to update state of execution detail.
	 *
	 * @param processPk the process pk
	 * @param detailTypePk the detail type pk
	 * @param valuatorEventPk the valuator event pk
	 * @param hasError the has error
	 * @throws ServiceException the service exception
	 */
	@Asynchronous
	@Lock(LockType.READ)
	public void finishValuatorExecutionDetail(Long processPk, Integer detailTypePk, Long valuatorEventPk, boolean hasError) throws ServiceException{
		/**Getting execution detail from database*/
		ValuatorExecutionDetail executionDetail = valuatorAditional.getExecutionDetail(processPk, valuatorEventPk, detailTypePk, null);
		/**Update finish time and date at process*/
		executionDetail.setFinishTime(CommonsUtilities.currentDateTime());
		/**If has errors update state whether or not*/
		if(hasError){
			executionDetail.setProcessState(ValuatorExecutionStateType.FINISHED_WITH_ERROR.getCode());
		}else{
			executionDetail.setProcessState(ValuatorExecutionStateType.FINISHED.getCode());
		}
		
		/**Update finish time at parent*/
		if(executionDetail.getExecutionDetail()!=null){
			ValuatorExecutionDetail parent = executionDetail.getExecutionDetail();
			parent.setProcessState(executionDetail.getProcessState());
			parent.setFinishTime(CommonsUtilities.currentDateTime());
		}
		/**Update object*/
		update(executionDetail);
	}
	
	/**
	 * *
	 * Method to save inconsistencies.
	 *
	 * @param valuatorInconsistList the valuator inconsist list
	 * @param execution the execution
	 * @param logUsser the log usser
	 * @throws ServiceException the service exception
	 */
	@Asynchronous
	@Lock(LockType.READ)
	public void insertValuatorInconsistencies(List<ValuatorInconsistenciesTO> valuatorInconsistList, ValuatorProcessExecution execution, LoggerUser logUsser) 
																																		throws ServiceException{
		List<ValuatorInconsistencies> inconsistenciesList = new ArrayList<>();
		
		/**Iterate all inconsistencies like TO*/
		for(ValuatorInconsistenciesTO inconsistenceTO: valuatorInconsistList){
			/**Create inconsistencies objects like jpa*/
			ValuatorInconsistencies inconsistencie = new ValuatorInconsistencies();
			/**Begin on setting data on object*/
			inconsistencie.setValuatorProcessExecution(execution);
			/**Adding participant, account and security*/
			inconsistencie.setParticipant(new Participant(inconsistenceTO.getParticipantPk()));
			inconsistencie.setHolderAccount(new HolderAccount(inconsistenceTO.getHolderAccountPk()));
			inconsistencie.setSecurityCode(inconsistenceTO.getSecurityCode());
			/**Setting marketFact from balances*/
			inconsistencie.setMarketDate(inconsistenceTO.getMarketDate());
			inconsistencie.setMarketRate(inconsistenceTO.getMarketRate());
			inconsistencie.setMarketPrice(inconsistenceTO.getMarketPrice());
			/**Setting marketFact from operation*/
			inconsistencie.setMarketDateOperation(inconsistenceTO.getMarketDateInconsist());
			inconsistencie.setMarketRateOperation(inconsistenceTO.getMarketRateInconsist());
			inconsistencie.setMarketPriceOperation(inconsistenceTO.getMarketPriceInconsist());
			/**If is mechanism Operation*/
			inconsistencie.setRole(inconsistenceTO.getRole());
			if(inconsistenceTO.getMechanismOperationPk()!=null){
				inconsistencie.setMechanismOperation(new MechanismOperation(inconsistenceTO.getMechanismOperationPk()));
			}
			/**If is affectation*/
			if(inconsistenceTO.getAffectationPk()!=null){
				BlockOperation blockOperation = new BlockOperation();
				blockOperation.setIdBlockOperationPk(inconsistenceTO.getAffectationPk());
				inconsistencie.setBlockOperation(blockOperation);
			}
			/**If is accreditation*/
			if(inconsistenceTO.getAccreditationPk()!=null){
				AccreditationOperation accreditationOperation = new AccreditationOperation();
				accreditationOperation.setIdAccreditationOperationPk(inconsistenceTO.getAccreditationPk());
				inconsistencie.setAccreditationOperation(accreditationOperation);
			}
			/**Add inconsistencies at list to save*/
			inconsistenciesList.add(inconsistencie);
		}
		/**Save masivelly*/
		saveAll(inconsistenciesList,logUsser);
	}
	
	/**
	 * *
	 * Method t finish valuatorExecution process.
	 *
	 * @param processPk the process pk
	 * @param errorMessage the error message
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public void finishValuatorExecutionProcess(Long processPk, String errorMessage) throws ServiceException{
		/**Getting object from database with id*/
		ValuatorProcessExecution processExecution = find(ValuatorProcessExecution.class, processPk);
		/**Set finish time and date*/
		processExecution.setFinishTime(CommonsUtilities.currentDateTime());
		/**Save error message of process*/
		processExecution.setErrorMessage(errorMessage);
		/**If has errors update state whether or not*/
		if(errorMessage!=null){
			processExecution.setProcessState(ValuatorExecutionStateType.FINISHED_WITH_ERROR.getCode());
		}else{
			processExecution.setProcessState(ValuatorExecutionStateType.FINISHED.getCode());
			/**If process has been finished sucessfuly*/
			if(processExecution.getIndAllPortFolly().equals(BooleanType.YES.getCode())){
				valuatorSetup.getMarketFactConsolidat().clear();
			}
		}
		/**Update object*/
		update(processExecution);
	}

	/**
	 * *.
	 *
	 * @return the long
	 */
	@Lock(LockType.READ)
	public Long findQuantitySecurities(){
		StringBuilder selectSQL = new StringBuilder();
		selectSQL.append("SELECT NVL(COUNT(sec),0)							");
		selectSQL.append("FROM Security sec									");
		selectSQL.append("WHERE sec.stateSecurity IN (:registeredState)		");
		
		Query query = em.createQuery(selectSQL.toString());
		query.setParameter("registeredState", Arrays.asList(SecurityStateType.REGISTERED.getCode(),
															SecurityStateType.BLOCKED.getCode(),
															SecurityStateType.SUSPENDED.getCode()));
		try{
			return (Long) query.getSingleResult();
		}catch(NoResultException ex){
			return GeneralConstants.ZERO_VALUE_LONG;
		}
	}
	
	/**
	 * Find valuator process number.
	 *
	 * @param operationType the operation type
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public Long findValuatorProcessNumber(Long operationType) throws ServiceException{
		StringBuilder selectSQL = new StringBuilder();
		selectSQL.append("SELECT NVL(MAX(vpo.operationNumber),0) + 1			");
		selectSQL.append("FROM ValuatorProcessOperation vpo						");
		selectSQL.append("WHERE vpo.operationType  = :operationType				");
		selectSQL.append("		AND TRUNC(vpo.registryDate)  = TRUNC(:currDate)	");
		
		Map<String,Object> parameters = new HashMap<String,Object>();
		parameters.put("operationType", operationType);
		parameters.put("currDate", CommonsUtilities.currentDate());
		
		try{
			return (Long) findObjectByQueryString(selectSQL.toString(), parameters);
		}catch(NoResultException ex){
			return GeneralConstants.ONE_VALUE_LONG;
		}
	}
	
	/**
	 * Metodo para verificar la reversion de saldos en operaciones selid
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Boolean  verifyUnblockBalanceSelidBatchBoolean() {
		
		Boolean result = false;
		
		try {
			
			StringBuilder querySql = new StringBuilder();
			
			querySql.append(" 	SELECT                                                                                                          ");
			querySql.append(" 	    SDO.SETTLEMENT_DATE, SO.OPERATION_STATE, MO.ID_MECHANISM_OPERATION_PK,                                      ");
			querySql.append(" 	    SAM.MARKET_DATE, SAM.MARKET_PRICE, SAM.MARKET_RATE, HAO.ID_HOLDER_ACCOUNT_FK,                               ");
			querySql.append(" 	    HAO.ID_INCHARGE_STOCK_PARTICIPANT, MO.ID_SECURITY_CODE_FK , HMB.IND_ACTIVE,                                 ");
			querySql.append(" 	    SAO.STOCK_REFERENCE , HAO.ROLE                                                                              ");
			querySql.append(" 	FROM SETTLEMENT_DATE_OPERATION SDO                                                                              ");
			querySql.append(" 	INNER JOIN SETTLEMENT_REQUEST SR ON SR.ID_SETTLEMENT_REQUEST_PK = SDO.ID_SETTLEMENT_REQUEST_FK                  ");
			querySql.append(" 	INNER JOIN SETTLEMENT_OPERATION SO ON SO.ID_SETTLEMENT_OPERATION_PK = SDO.ID_SETTLEMENT_OPERATION_FK            ");
			querySql.append(" 	INNER JOIN MECHANISM_OPERATION MO ON MO.ID_MECHANISM_OPERATION_PK = SO.ID_MECHANISM_OPERATION_FK                ");
			querySql.append(" 	INNER JOIN HOLDER_ACCOUNT_OPERATION HAO on HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK         ");
			querySql.append(" 	INNER JOIN SETTLEMENT_ACCOUNT_OPERATION SAO ON SAO.ID_SETTLEMENT_OPERATION_FK = SO.ID_SETTLEMENT_OPERATION_PK   ");
			querySql.append(" 	INNER JOIN SETTLEMENT_ACCOUNT_MARKETFACT SAM ON SAO.ID_SETTLEMENT_ACCOUNT_PK = SAM.ID_SETTLEMENT_ACCOUNT_FK     ");
			querySql.append(" 	INNER JOIN HOLDER_MARKETFACT_BALANCE HMB ON HMB.ID_SECURITY_CODE_FK = MO.ID_SECURITY_CODE_FK                    ");
			querySql.append(" 	WHERE 1 = 1                                                                                                     ");
			querySql.append(" 	AND SR.REQUEST_TYPE = 2018                                                                                      ");
			querySql.append(" 	AND SR.REQUEST_STATE = 1543                                                                                     ");
			querySql.append(" 	AND SO.IND_EXTENDED = 1                                                                                         ");
			querySql.append(" 	AND SO.IND_UNFULFILLED <> 1                                                                                     ");
			querySql.append(" 	AND SO.OPERATION_PART = 1                                                                                       ");
			querySql.append(" 	AND HAO.OPERATION_PART = 1                                                                                      ");
			querySql.append(" 	AND HAO.HOLDER_ACCOUNT_STATE = 625                                                                              ");
			querySql.append(" 	AND SAO.OPERATION_STATE = 625                                                                                   ");
			querySql.append(" 	AND SAM.IND_ACTIVE = 1                                                                                          ");
			querySql.append(" 	AND TRUNC(HMB.MARKET_DATE) = TRUNC(SAM.MARKET_DATE)                                                             ");
			querySql.append(" 	AND HMB.ID_PARTICIPANT_FK = HAO.ID_INCHARGE_STOCK_PARTICIPANT                                                   ");
			querySql.append(" 	AND HMB.IND_ACTIVE = 1                                                                                          ");
			querySql.append(" 	AND (HMB.SALE_BALANCE > 0 or HMB.PURCHASE_BALANCE > 0)                                                          ");
			querySql.append(" 	AND SO.OPERATION_STATE = 614                                                                                    ");
			querySql.append(" 	AND SAO.STOCK_REFERENCE IN (1484,1109)                                                                          ");
		
			Query querySec = em.createNativeQuery(querySql.toString());
			//querySec.setParameter("oneParam", BooleanType.YES.getCode());
			
			List<Object[]>  objectData = (List<Object[]>)querySec.getResultList();
				
			if(objectData.size() > 0) {
				result = true;
			}
			
		} catch (Exception e) {
			//Message Error
		}
		
		return result;
	}
	
	
	/**
	 * Gets the accounts from holder.
	 *
	 * @param holders the holders
	 * @return the accounts from holder
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Long> getAccountsFromHolder(List<Long> holders) throws ServiceException{
		StringBuilder selectSQL = new StringBuilder();
		selectSQL.append("SELECT Distinct ha.idHolderAccountPk					");
		selectSQL.append("FROM HolderAccountBalance hab							");
		selectSQL.append("INNER JOIN hab.holderAccount ha						");
		selectSQL.append("INNER JOIN ha.holderAccountDetails had				");
		selectSQL.append("INNER JOIN had.holder ho								");
		selectSQL.append("WHERE ho.idHolderPk  IN (:holders)					");
		
		Map<String,Object> parameters = new HashMap<String,Object>();
		parameters.put("holders", holders);
		return findListByQueryString(selectSQL.toString(), parameters);
	}

	/**
	 * Gets the security code by execution.
	 *
	 * @param processPk the process pk
	 * @return the security code by execution
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	@SuppressWarnings("unchecked")
	public List<String> getSecurityCodeByExecution(Long processPk) throws ServiceException{
		StringBuilder selectSQL = new StringBuilder();
		selectSQL.append("SELECT Distinct pric.securityCode									");
		selectSQL.append("FROM ValuatorPriceResult pric										");
		selectSQL.append("WHERE pric.valuatorProcessExecution.idProcessExecutionPk = :pk	");
		
		Map<String,Object> parameters = new HashMap<String,Object>();
		parameters.put("pk", processPk);
		return findListByQueryString(selectSQL.toString(), parameters);
	}
	
	/**
	 * *
	 * Method to get MarketFactConsolidate on HashMap by ValuatorCode.
	 *
	 * @param marketFactType the market fact type
	 * @param marketFactActive the market fact active
	 * @return the market fact consolidat
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public Map<String,List<ValuatorMarketfactConsolidat>> getMarketFactConsolidat(List<Integer> marketFactType, List<String> marketFactActive) 
																					throws ServiceException{
		return valuatorSetup.getMarketFactConsolidat();
	}
	
	/**
	 * Gets the valuator market fact security.
	 *
	 * @return the valuator market fact security
	 */
	@Lock(LockType.READ)
	public ValuatorSecurityMarketfact getValuatorMarketFactSecurity(){
		ValuatorSecurityMarketfact securityMarketFact = new ValuatorSecurityMarketfact();
		securityMarketFact.setRegistryDate(CommonsUtilities.currentDateTime());
		securityMarketFact.setRegistryUser(USER_SESSION);
		return new ValuatorSecurityMarketfact();
	}
	
	/**
	 * Populate security with coupons like amortization && coupons.
	 *
	 * @param securities the securities
	 * @param interestMap the interest map
	 * @param amortizationMap the amortization map
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@Lock(LockType.READ)
	public List<Security> populateSecurityWithCoupons(List<Security> securities, Map<String,List<ProgramInterestCoupon>> interestMap, 
													  Map<String,List<ProgramAmortizationCoupon>> amortizationMap) throws ServiceException{
		
		/**Iterate all securities on list*/
		for(Security security: securities){
			String securityCode = security.getIdSecurityCodePk();
			List<ProgramAmortizationCoupon> amortizationList = amortizationMap.get(securityCode);
			List<ProgramInterestCoupon> 	interestList 	 = interestMap.get(securityCode);
			/**Verified if amortizationsList have at least one element*/
			if(amortizationList!=null && !amortizationList.isEmpty()){
				security.getAmortizationPaymentSchedule().setProgramAmortizationCoupons(amortizationList);
			}
			/**Verified if interestList have at least one element*/
			if(interestList!=null && !interestList.isEmpty()){
				security.getInterestPaymentSchedule().setProgramInterestCoupons(interestList);
			}
		}
		
		return securities;
	}
	
	/**
	 * *
	 * Method to get All interest coupon from list of security code.
	 *
	 * @param securityCode the security code
	 * @param valuatorDate the valuator date
	 * @param securityClass the security class
	 * @return the program interes coupon map
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	@Lock(LockType.READ)
	public Map<String,List<ProgramInterestCoupon>> getProgramInteresCouponMap(List<String> securityCode, Date valuatorDate, Integer securityClass)
																								    throws ServiceException {
		
		Integer uniqueFormuleType = valuatorAditional.findUniqueFormuleType(securityClass);
		
		StringBuilder querySQL = new StringBuilder();
		querySQL.append("SELECT SEC.ID_SECURITY_CODE_PK , CUP.COUPON_NUMBER , CUP.BEGINING_DATE, 														");
		querySQL.append("       CUP.EXPERITATION_DATE, CUP.STATE_PROGRAM_INTEREST, CUP.COUPON_AMOUNT													");
		querySQL.append("FROM PROGRAM_INTEREST_COUPON CUP																								");
		querySQL.append("INNER JOIN INTEREST_PAYMENT_SCHEDULE SCHE ON SCHE.ID_INT_PAYMENT_SCHEDULE_PK = CUP.ID_INT_PAYMENT_SCHEDULE_FK			  		");
		querySQL.append("INNER JOIN SECURITY SEC ON SEC.ID_SECURITY_CODE_PK = SCHE.ID_SECURITY_CODE_FK											 		");
		querySQL.append("WHERE TRUNC(CUP.EXPERITATION_DATE) > TRUNC(:valuatorDate) AND SEC.ID_SECURITY_CODE_PK IN (:securityCodes)						");
		
		if(uniqueFormuleType==null || uniqueFormuleType.equals(ValuatorFormuleType.WITH_COUPONS.getCode())){
			querySQL.append("																															");
			querySQL.append("UNION ALL																													");
			querySQL.append("																															");
			querySQL.append("SELECT PROD_.ID_SECURITY_CODE_PK , CUP_.COUPON_NUMBER , CUP_.BEGINING_DATE, 												");
			querySQL.append("       CUP_.EXPERITATION_DATE, 0	,0																						");
			querySQL.append("FROM PROGRAM_INTEREST_COUPON CUP_																							");
			querySQL.append("INNER JOIN INTEREST_PAYMENT_SCHEDULE SCHE_ ON SCHE_.ID_INT_PAYMENT_SCHEDULE_PK = CUP_.ID_INT_PAYMENT_SCHEDULE_FK			");
			querySQL.append("INNER JOIN SECURITY SEC_ ON SEC_.ID_SECURITY_CODE_PK = SCHE_.ID_SECURITY_CODE_FK											");
			querySQL.append("INNER JOIN SECURITY PROD_ ON PROD_.ID_REF_SECURITY_CODE_FK = SEC_.ID_SECURITY_CODE_PK 										");
			querySQL.append("WHERE TRUNC(CUP_.EXPERITATION_DATE) > TRUNC(:valuatorDate) AND PROD_.ID_SECURITY_CODE_PK IN (:securityCodes)				");
			querySQL.append("AND CUP_.COUPON_NUMBER NOT IN (																							");
			querySQL.append("   SELECT SB_CUP.COUPON_NUMBER																								");
			querySQL.append("   FROM PROGRAM_INTEREST_COUPON SB_CUP																						");
			querySQL.append("   INNER JOIN INTEREST_PAYMENT_SCHEDULE SB_SCHE ON SB_SCHE.ID_INT_PAYMENT_SCHEDULE_PK = SB_CUP.ID_INT_PAYMENT_SCHEDULE_FK	");
			querySQL.append("   INNER JOIN SECURITY SB_SEC ON SB_SEC.ID_SECURITY_CODE_PK = SB_SCHE.ID_SECURITY_CODE_FK 									");
			querySQL.append("   WHERE SB_SEC.ID_SECURITY_CODE_PK = PROD_.ID_SECURITY_CODE_PK)															");
		}
		
		Query query = em.createNativeQuery(querySQL.toString());
		query.setParameter("valuatorDate", valuatorDate);
		query.setParameter("securityCodes", securityCode); 
		
		return populateInteresBySecurityCode(query.getResultList());
	}
	
	/**
	 * *
	 * Method to populate data from list of object[].
	 *
	 * @param objectData the object data
	 * @return the map
	 */
	@Lock(LockType.READ)
	private Map<String,List<ProgramInterestCoupon>> populateInteresBySecurityCode(List<Object[]> objectData){
		Map<String, List<ProgramInterestCoupon>> mapData = new HashMap<String, List<ProgramInterestCoupon>>();
		for(Object[] data: objectData){
			String securityCode = (String) data[0];
			if(mapData.get(securityCode)==null){
				mapData.put(securityCode, new ArrayList<ProgramInterestCoupon>());
			}
			ProgramInterestCoupon interestObject = new ProgramInterestCoupon();
			interestObject.setCouponNumber(((BigDecimal) data[1]).intValue());
			interestObject.setBeginingDate((Date) data[2]);
			interestObject.setExperitationDate((Date) data[3]);
			interestObject.setStateProgramInterest(((BigDecimal) data[4]).intValue());
			interestObject.setCouponAmount((BigDecimal) data[5]);
			mapData.get(securityCode).add(interestObject);
		}
		return mapData;
	}
	
	/**
	 * *
	 * Method to get All Amortization coupon from list of security code.
	 *
	 * @param securityCode the security code
	 * @param valuatorDate the valuator date
	 * @return the program amortization coupon map
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	@Lock(LockType.READ)
	public Map<String, List<ProgramAmortizationCoupon>> getProgramAmortizationCouponMap(List<String> securityCode, Date valuatorDate) 
																									throws ServiceException {
		StringBuilder querySQL = new StringBuilder();
		querySQL.append("Select schedul.security.idSecurityCodePk, amortCup.couponNumber, 					");
		querySQL.append("		amortCup.beginingDate, amortCup.expirationDate, 							");
		querySQL.append("		amortCup.couponAmount, corporat.oldNominalValue  							");
		querySQL.append("From ProgramAmortizationCoupon amortCup											");
		querySQL.append("Inner Join amortCup.amortizationPaymentSchedule schedul							");
		querySQL.append("Left  Join amortCup.corporativeOperation 		 corporat							");
		querySQL.append("Where TRUNC(amortCup.expirationDate) > TRUNC(:valuatorDate)						");
		querySQL.append("	   And 	schedul.security.idSecurityCodePk IN (:securityCodes)					");
		querySQL.append("Order By schedul.security.idSecurityCodePk Asc, amortCup.couponNumber Asc			");
		
		Query query = em.createQuery(querySQL.toString());
		query.setParameter("valuatorDate", valuatorDate);
		query.setParameter("securityCodes", securityCode);
		query.setHint("org.hibernate.cacheable", Boolean.TRUE);
		
		return populateAmortizationBySecurityCode(query.getResultList());
	}
	
	/**
	 * *
	 * Method to populate all amortization coupon from list of object[].
	 *
	 * @param objectData the object data
	 * @return the map
	 */
	@Lock(LockType.READ)
	private Map<String, List<ProgramAmortizationCoupon>> populateAmortizationBySecurityCode(List<Object[]> objectData){
		Map<String, List<ProgramAmortizationCoupon>> mapData = new HashMap<String, List<ProgramAmortizationCoupon>>();
		for(Object[] data: objectData){
			String securityCode = (String) data[0];
			if(mapData.get(securityCode)==null){
				mapData.put(securityCode, new ArrayList<ProgramAmortizationCoupon>());
			}
			ProgramAmortizationCoupon amortizationCoupon = new ProgramAmortizationCoupon();
			amortizationCoupon.setCouponNumber((Integer) data[1]);
			amortizationCoupon.setBeginingDate((Date) data[2]);
			amortizationCoupon.setExpirationDate((Date) data[3]);
			amortizationCoupon.setCouponAmount((BigDecimal) data[4]);
			amortizationCoupon.setNominalValue((BigDecimal) data[5]);
			mapData.get(securityCode).add(amortizationCoupon);
		}
		return mapData;
	}
	
	/**
	 * Execute balance valuator.
	 *
	 * @param processExecutionID the process execution id
	 * @throws ServiceException the service exception
	 */
	@TransactionTimeout(unit=TimeUnit.HOURS,value=1)
	public void executeBalanceValuator(Long processExecutionID )throws ServiceException {
		StringBuilder sb = new StringBuilder();
		sb.append("BEGIN PKG_VALUATOR_PROCESS.SP_EXECUTE_VALUATOR_PROCESS(:pExecutionId);END;");
		Query query = em.createNativeQuery(sb.toString());
    	query.setParameter("pExecutionId", processExecutionID);
    	query.executeUpdate();
	}
	
	/**
	 * Gets the security with adquisition rate.
	 *
	 * @param security the security
	 * @param instrumentType the instrument type
	 * @return the security with adquisition rate
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	@Lock(LockType.READ)
	public Map<String,Map<Date,List<BigDecimal>>> getSecurityWithAdquisitionRate(List<String> security, Integer instrumentType) throws ServiceException{
		
		StringBuilder querySQL = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String,Object>();
		querySQL.append("Select Distinct sec.idSecurityCodePk, hmba.marketDate, hmba.marketRate, hmba.marketPrice 	");
		querySQL.append("From HolderMarketFactBalance hmba															");
		querySQL.append("Inner Join hmba.security sec																");
		querySQL.append("Where hmba.indActive = :oneParam 		And 												");
		querySQL.append("     ((hmba.totalBalance - hmba.reportingBalance) >= :oneParam								");
		querySQL.append("     		Or hmba.reportedBalance >= :oneParam)											");
		if(security!=null && !security.isEmpty()){
			querySQL.append("And sec.idSecurityCodePk IN (:securityList)											");
			parameters.put("securityList", security);
		}
		querySQL.append("Group By sec.idSecurityCodePk, hmba.marketDate, hmba.marketRate, hmba.marketPrice			");
		querySQL.append("Order By Count(hmba.marketRate) Desc														");
		parameters.put("oneParam", BooleanType.YES.getCode());
		
		/*
		StringBuilder querySql = new StringBuilder();
																																																
		querySql.append("    SELECT ID_SECURITY_CODE_PK,                                                                                                                                          ");
		querySql.append("           MARKET_DATE,                                                                                                                                                  ");
		querySql.append("           MARKET_RATE,                                                                                                                                                  ");
		querySql.append("           MARKET_PRICE                                                                                                                                                  ");
		querySql.append("    FROM (                                                                                                                                                               ");
		querySql.append("    SELECT                                                                                                                                                               ");
		querySql.append("        distinct SEC.ID_SECURITY_CODE_PK,                                                                                                                                ");
		querySql.append("        HMB.MARKET_DATE,                                                                                                                                                 ");
		querySql.append("        HMB.MARKET_RATE,                                                                                                                                                 ");
		querySql.append("        HMB.MARKET_PRICE                                                                                                                                                 ");
		querySql.append("    from HOLDER_MARKETFACT_BALANCE HMB                                                                                                                                   ");
		querySql.append("    inner join SECURITY SEC on HMB.ID_SECURITY_CODE_FK=SEC.ID_SECURITY_CODE_PK                                                                                           ");
		querySql.append("    where HMB.IND_ACTIVE=1                                                                                                                                               ");
		querySql.append("    and ((HMB.TOTAL_BALANCE - HMB.REPORTING_BALANCE)>=:oneParam or HMB.REPORTED_BALANCE>=:oneParam )                                                                                       ");
		if(security!=null && !security.isEmpty()){
			querySql.append("    and (SEC.ID_SECURITY_CODE_PK in (:securityList))                                                                           ");
		}
		querySql.append("    AND SEC.ID_SECURITY_CODE_PK NOT IN (SELECT MO_SUB.ID_SECURITY_CODE_FK                                                                                                ");
		querySql.append("                                    FROM SETTLEMENT_DATE_OPERATION SDO_SUB                                                                                               ");
		querySql.append("                                    INNER JOIN SETTLEMENT_REQUEST SR_SUB ON SR_SUB.ID_SETTLEMENT_REQUEST_PK = SDO_SUB.ID_SETTLEMENT_REQUEST_FK                           ");
		querySql.append("                                    INNER JOIN SETTLEMENT_OPERATION SO_SUB ON SO_SUB.ID_SETTLEMENT_OPERATION_PK = SDO_SUB.ID_SETTLEMENT_OPERATION_FK                     ");
		querySql.append("                                    INNER JOIN MECHANISM_OPERATION MO_SUB ON MO_SUB.ID_MECHANISM_OPERATION_PK = SO_SUB.ID_MECHANISM_OPERATION_FK                         ");
		querySql.append("                                    INNER JOIN HOLDER_ACCOUNT_OPERATION HAO_SUB on HAO_SUB.ID_MECHANISM_OPERATION_FK = MO_SUB.ID_MECHANISM_OPERATION_PK                  ");
		querySql.append("                                    INNER JOIN SETTLEMENT_ACCOUNT_OPERATION SAO_SUB on SAO_SUB.ID_HOLDER_ACCOUNT_OPERATION_FK = HAO_SUB.ID_HOLDER_ACCOUNT_OPERATION_PK   ");
		querySql.append("                                    INNER JOIN SETTLEMENT_ACCOUNT_MARKETFACT SAM_SUB ON SAO_SUB.ID_SETTLEMENT_ACCOUNT_PK = SAM_SUB.ID_SETTLEMENT_ACCOUNT_FK              ");
		querySql.append("                                    WHERE 1 = 1                                                                                                                          ");
		querySql.append("                                    AND SO_SUB.OPERATION_PART = 1                                                                                                        ");
		querySql.append("                                    AND HAO_SUB.OPERATION_PART = 1                                                                                                       ");
		querySql.append("                                    AND HAO_SUB.ROLE = 1                                                                                                                 ");
		querySql.append("                                    AND HAO_SUB.HOLDER_ACCOUNT_STATE = 625                                                                                               ");
		querySql.append("                                    AND SAO_SUB.OPERATION_STATE = 625                                                                                                    ");
		querySql.append("                                    AND SR_SUB.REQUEST_TYPE = 2018                                                                                                       ");
		querySql.append("                                    AND SR_SUB.REQUEST_STATE = 1543                                                                                                      ");
		querySql.append("                                    AND SO_SUB.IND_EXTENDED = 1                                                                                                          ");
		querySql.append("       							 AND SAM_SUB.IND_ACTIVE = 1                                                                                             			  ");
		querySql.append("                                    AND SO_SUB.IND_UNFULFILLED <> 1                                                                                                      ");
		querySql.append("                                    AND (   SO_SUB.OPERATION_PART = 1 and SO_SUB.OPERATION_STATE = 614                                                                   ");
		querySql.append("                                         or SO_SUB.OPERATION_PART = 2 and SO_SUB.OPERATION_STATE = 615 ))                                                                ");
		querySql.append("    group by                                                                                                                                                             ");
		querySql.append("        SEC.ID_SECURITY_CODE_PK ,                                                                                                                                        ");
		querySql.append("        HMB.MARKET_DATE ,                                                                                                                                                ");
		querySql.append("        HMB.MARKET_RATE ,                                                                                                                                                ");
		querySql.append("        HMB.MARKET_PRICE                                                                                                                                                 ");
		querySql.append("                                                                                                                                                                         ");
		querySql.append("    UNION ALL                                                                                                                                                            ");
		querySql.append("                                                                                                                                                                         ");
		querySql.append("    SELECT                                                                                                                                                               ");
		querySql.append("        MO.ID_SECURITY_CODE_FK ID_SECURITY_CODE_PK,                                                                                                                      ");
		querySql.append("        SAM.MARKET_DATE,                                                                                                                                                 ");
		querySql.append("        SAM.MARKET_RATE,                                                                                                                                                 ");
		querySql.append("        SAM.MARKET_PRICE                                                                                                                                                 ");
		querySql.append("    FROM SETTLEMENT_DATE_OPERATION SDO                                                                                                                                   ");
		querySql.append("    INNER JOIN SETTLEMENT_REQUEST SR ON SR.ID_SETTLEMENT_REQUEST_PK = SDO.ID_SETTLEMENT_REQUEST_FK                                                                       ");
		querySql.append("    INNER JOIN SETTLEMENT_OPERATION SO ON SO.ID_SETTLEMENT_OPERATION_PK = SDO.ID_SETTLEMENT_OPERATION_FK                                                                 ");
		querySql.append("    INNER JOIN MECHANISM_OPERATION MO ON MO.ID_MECHANISM_OPERATION_PK = SO.ID_MECHANISM_OPERATION_FK                                                                     ");
		querySql.append("    INNER JOIN HOLDER_ACCOUNT_OPERATION HAO on HAO.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK                                                              ");
		querySql.append("    INNER JOIN SETTLEMENT_ACCOUNT_OPERATION SAO on SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = HAO.ID_HOLDER_ACCOUNT_OPERATION_PK                                               ");
		querySql.append("    INNER JOIN SETTLEMENT_ACCOUNT_MARKETFACT SAM ON SAO.ID_SETTLEMENT_ACCOUNT_PK = SAM.ID_SETTLEMENT_ACCOUNT_FK                                                          ");
		querySql.append("    WHERE 1 = 1                                                                                                                                                          ");
		querySql.append("    AND SO.OPERATION_PART = 1                                                                                                                                            ");
		querySql.append("    AND HAO.OPERATION_PART = 1                                                                                                                                           ");
		querySql.append("    AND HAO.ROLE = 1                                                                                                                                                     ");
		querySql.append("    AND HAO.HOLDER_ACCOUNT_STATE = 625                                                                                                                                   ");
		querySql.append("    AND SAO.OPERATION_STATE = 625                                                                                                                                        ");
		querySql.append("    AND SR.REQUEST_TYPE = 2018                                                                                                                                           ");
		querySql.append("    AND SR.REQUEST_STATE = 1543                                                                                                                                          ");
		querySql.append("    AND SO.IND_EXTENDED = 1                                                                                                                                              ");
		querySql.append("    AND SAM.IND_ACTIVE = 1                                                                                             			  									  ");
		querySql.append("    AND SO.IND_UNFULFILLED <> 1                                                                                                                                          ");
		querySql.append("    AND (   SO.OPERATION_PART = 1 and SO.OPERATION_STATE = 614                                                                                                           ");
		querySql.append("         or SO.OPERATION_PART = 2 and SO.OPERATION_STATE = 615 )                                                                                                         ");
		
		if(security!=null && !security.isEmpty()){
			querySql.append("    and (MO.ID_SECURITY_CODE_FK in (:securityList))                                                                                           ");
		}
		querySql.append("    group by                                                                                                                                                             ");
		querySql.append("        MO.ID_SECURITY_CODE_FK ,                                                                                                                                         ");
		querySql.append("        SAM.MARKET_DATE ,                                                                                                                                                ");
		querySql.append("        SAM.MARKET_RATE ,                                                                                                                                                ");
		querySql.append("        SAM.MARKET_PRICE )                                                                                                                                               ");
		querySql.append("    group by                                                                                                                                                             ");
		querySql.append("        ID_SECURITY_CODE_PK ,                                                                                                                                            ");
		querySql.append("        MARKET_DATE ,                                                                                                                                                    ");
		querySql.append("        MARKET_RATE ,                                                                                                                                                    ");
		querySql.append("        MARKET_PRICE                                                                                                                                                     ");
		querySql.append("    order by count(MARKET_RATE) Desc                                                                                                                                     ");
		
		Query querySec = em.createNativeQuery(querySql.toString());
		querySec.setParameter("oneParam", BooleanType.YES.getCode());
		if(security!=null && !security.isEmpty()){
			querySec.setParameter("securityList", security);
		}
		
		List<Object[]>  objectData = (List<Object[]>)querySec.getResultList();*/
				
		List<Object[]> objectData = findListByQueryString(querySQL.toString(), parameters);
		Map<String,Map<Date,List<BigDecimal>>> mapData = new HashMap<String,Map<Date,List<BigDecimal>>>();
		
		for(Object[] data: objectData){
			String securityCode   = (String) data[0];
			Date marketDate 	  = (Date) data[1];
			BigDecimal marketRate = (BigDecimal) data[2];
			BigDecimal marketPrice = (BigDecimal) data[3];
			if(mapData.get(securityCode)==null){
				Map<Date,List<BigDecimal>> listData = new  HashMap<>();
				mapData.put(securityCode, listData);
			}
			if(mapData.get(securityCode).get(marketDate)==null){
				mapData.get(securityCode).put(marketDate, new ArrayList<BigDecimal>());
			}
			
			/**Verified instrumentType and assignment enough data*/
			if(instrumentType.equals(InstrumentType.FIXED_INCOME.getCode())){
				mapData.get(securityCode).get(marketDate).add(marketRate);
			}else if(instrumentType.equals(InstrumentType.VARIABLE_INCOME.getCode())){
				mapData.get(securityCode).get(marketDate).add(marketPrice);
			}
		}
		return mapData;
	}
	
	/**
	 * Gets the physical security rate.
	 *
	 * @param securityList the security list
	 * @param instrumentType the instrument type
	 * @return the physical security rate
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	@Lock(LockType.READ)
	public Map<String,Map<Date,List<BigDecimal>>> getPhysicalSecurityRate(List<String> securityList, Integer instrumentType) throws ServiceException{
		StringBuilder querySQL = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String,Object>();
		querySQL.append("Select Distinct phys.securities.idSecurityCodePk, 							");
		querySQL.append("				 mark.marketDate, mark.marketRate, mark.marketPrice			");
		querySQL.append("From PhysicalCertMarketfact mark											");
		querySQL.append("Inner Join mark.physicalCertificate phys									");
		querySQL.append("Where phys.state IN (:stateList)	And phys.situation	In(:situationList)	");
		querySQL.append("And   mark.indActive = :oneParam											");
		
		parameters.put("oneParam", BooleanType.YES.getCode());
		parameters.put("situationList", Arrays.asList(SituationType.CUSTODY_VAULT.getCode(),
													  SituationType.DEPOSITARY_OPERATIONS.getCode(),
													  SituationType.DEPOSITARY_VAULT.getCode()));
		
		parameters.put("stateList", Arrays.asList(PhysicalCertificateStateType.CONFIRMED.getCode(),
												  PhysicalCertificateStateType.AUTHORIZED.getCode(),
												  StateType.DEPOSITED.getCode()));
		
		if(securityList!=null && !securityList.isEmpty()){
			querySQL.append("And   phys.securities.idSecurityCodePk In (:securityList)				");
			parameters.put("securityList", securityList);
		}
		
		List<Object[]> objectData = findListByQueryString(querySQL.toString(), parameters);
		Map<String,Map<Date,List<BigDecimal>>> mapData = new HashMap<>();

		/**Iterate all rows found*/
		for(Object[] data: objectData){
			String securityCode 	= (String) data[0];
			Date marketDate 		= (Date) data[1];
			BigDecimal marketRate 	= (BigDecimal) data[2];
			BigDecimal marketPrice 	= (BigDecimal) data[3];
			if(mapData.get(securityCode)==null){
				Map<Date,List<BigDecimal>> listData = new  HashMap<>();
				mapData.put(securityCode, listData);
			}
			if(mapData.get(securityCode).get(marketDate)==null){
				mapData.get(securityCode).put(marketDate, new ArrayList<BigDecimal>());
			}
			/**Verified instrumentType and assignment enough data*/
			if(instrumentType.equals(InstrumentType.FIXED_INCOME.getCode())){
				mapData.get(securityCode).get(marketDate).add(marketRate);
			}else if(instrumentType.equals(InstrumentType.VARIABLE_INCOME.getCode())){
				mapData.get(securityCode).get(marketDate).add(marketPrice);
			}
		}
		return mapData;
	}
	
	/**
	 * Gets the operation by security code.	
	 *
	 * @param processPk the process pk
	 * @param instrumentType the instrument type
	 * @return the operation by security code
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	@Lock(LockType.READ)
	public List<MechanismOperation> getOperationBySecurityCode(Long processPk, Integer instrumentType) throws ServiceException{
		StringBuilder querySQL = new StringBuilder();
		querySQL.append("Select mech.idMechanismOperationPk, mech.stockQuantity, sett.settledQuantity,						");
		querySQL.append("		mech.cashPrice, mech.amountRate, mech.operationDate, 										");
		querySQL.append("		sec.idSecurityCodePk 																		");
		//issue 617
		querySQL.append("		,mech.mechanisnModality.negotiationMechanism.idNegotiationMechanismPk 						");
		querySQL.append("From SettlementOperation sett																		");
		querySQL.append("Inner Join sett.mechanismOperation mech															");
		querySQL.append("Inner Join mech.securities sec																		");
		querySQL.append("Where mech.indReportingBalance = :oneParam															");
		querySQL.append("  And mech.operationState IN( :settledState	)														");
		querySQL.append("  And mech.mechanisnModality.negotiationMechanism.idNegotiationMechanismPk IN (:idMec)				");
		querySQL.append("  And sett.operationPart = :termPart	And sett.indPartial = :zeroParam							");
		querySQL.append("  And sec.instrumentType = :instrumentType															");
		querySQL.append("  And sec.stateSecurity In (:stateSecurities)														");
		
		
		// Traemos todos los que estan autorizados
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT SU FROM SettlementUnfulfillment SU");
		sbQuery.append("	inner join fetch SU.operationUnfulfillment OU ");
		sbQuery.append("	inner join fetch OU.settlementOperation SO ");
		sbQuery.append("	WHERE SU.requestState = 1543 ");

		Query queryV = em.createQuery(sbQuery.toString());
		@SuppressWarnings("unchecked")
		List <SettlementUnfulfillment> tets = (List <SettlementUnfulfillment>)queryV.getResultList();
		List<Long> lstSettperation = new ArrayList<Long>();
		for (SettlementUnfulfillment oepartion: tets) {
			lstSettperation.add(oepartion.getOperationUnfulfillment().getSettlementOperation().getIdSettlementOperationPk());
		}
		if(tets!=null&&tets.size()>0)
			querySQL.append("  And sett.idSettlementOperationPk not In (:lstSettlementOperation)														");
		
		querySQL.append("  And sec.idSecurityCodePk In 																		");
//		querySQL.append("  And sec.idSecurityCodePk Like 'DPF-BECN%' 														");
		
		/**SubQuery to associate parameters executed in the process*/
		StringBuilder subQuery = new StringBuilder();
		ValuatorProcessExecution execution = find(ValuatorProcessExecution.class, processPk);
		Integer valuatorType = execution.getValuatorType();
		
		subQuery.append("(Select Distinct hab.security.idSecurityCodePk 							");
		subQuery.append(" From HolderAccountBalance hab			Where 1 = 1 						");
		
		if(valuatorType.equals(ValuatorType.BY_PARTICIPANT.getCode())){
			subQuery.append("AND hab.participant.idParticipantPk IN (								");
			subQuery.append("Select par.participant.idParticipantPk									");
		}
		if(valuatorType.equals(ValuatorType.BY_SECURITY.getCode())){
			subQuery.append("AND hab.security.idSecurityCodePk IN (									");
			subQuery.append("Select par.security.idSecurityCodePk									");
		}
		if(valuatorType.equals(ValuatorType.BY_INVESTOR.getCode())){
			subQuery.append("AND hab.holderAccount.idHolderAccountPk IN (							");
			subQuery.append("Select par.holderAccount.idHolderAccountPk								");
		}
		
		subQuery.append("From ValuatorExecutionParameters par										");
		subQuery.append("Where par.valuatorProcessExecution.idProcessExecutionPk = :processPk))		");
		
		/**Adding subQuery with main query*/
		querySQL.append(subQuery.toString());
		
		/**Verify instrument type, should get only securities with expirationDate greater than processDate*/
		if(instrumentType.equals(InstrumentType.FIXED_INCOME.getCode())){
			querySQL.append("And Trunc(sec.expirationDate) > Trunc(:processDate)					");
		}

		Query query = em.createQuery(querySQL.toString());
		query.setParameter("processPk", processPk);
		query.setParameter("instrumentType", instrumentType);
		query.setParameter("oneParam", ComponentConstant.ONE);
		query.setParameter("zeroParam", ComponentConstant.ZERO);
		query.setParameter("termPart", ComponentConstant.TERM_PART);
		if(instrumentType.equals(InstrumentType.FIXED_INCOME.getCode())){
			query.setParameter("processDate", execution.getProcessDate());
		}
		//query.setParameter("settledState", MechanismOperationStateType.CASH_SETTLED.getCode());
		
		List<Integer> states = new ArrayList<Integer>();
		states.add(MechanismOperationStateType.CASH_SETTLED.getCode());
		states.add(MechanismOperationStateType.CANCELED_TERM_STATE.getCode());
		query.setParameter("settledState", states);
		
		
		if(tets!=null&&tets.size()>0)
			query.setParameter("lstSettlementOperation", lstSettperation);		
		
		
		query.setParameter("stateSecurities", Arrays.asList(SecurityStateType.REGISTERED.getCode(),SecurityStateType.BLOCKED.getCode()));
		query.setParameter("idMec", Arrays.asList(
				NegotiationMechanismType.BOLSA.getCode(),
				NegotiationMechanismType.OTC.getCode(),
				NegotiationMechanismType.SIRTEX.getCode()));
		List<MechanismOperation> operationList = new ArrayList<MechanismOperation>(); 
		List<Object[]> objectDataList = query.getResultList();
		for(Object[] data: objectDataList){
			MechanismOperation operation = new MechanismOperation();
			operation.setIdMechanismOperationPk((Long) data[0]);
			operation.setStockQuantity((BigDecimal) data[1]);
			operation.setQuantity((BigDecimal) data[2]);
			operation.setCashPrice((BigDecimal) data[3]);
			operation.setAmountRate((BigDecimal) data[4]);
			operation.setOperationDate((Date) data[5]);
			operation.setOriginRequest((Long) data[7]);
			Security security = new Security((String)data[6]);
			operation.setSecurities(security);
			operationList.add(operation);
		}
		return operationList;
	}
	
	/**
	 * Gets the operation by security code.	 issue 1040
	 *
	 * @param processPk the process pk
	 * @param instrumentType the instrument type
	 * @return the operation by security code
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	@Lock(LockType.READ)
	public List<MechanismOperation> getSelidOperationBySecurityCode(Long processPk, Integer instrumentType) throws ServiceException{
		
		StringBuilder querySQL = new StringBuilder();
		querySQL.append("Select mech.idMechanismOperationPk, mech.stockQuantity, sett.settledQuantity,						");
		querySQL.append("		mech.cashPrice, mech.amountRate, mech.operationDate, 										");
		querySQL.append("		sec.idSecurityCodePk 																		");
		querySQL.append("		,mech.mechanisnModality.negotiationMechanism.idNegotiationMechanismPk 						");
		querySQL.append("From SettlementRequest setrr																		");
		querySQL.append("Inner Join setrr.lstSettlementDateOperations setdo													");		
		querySQL.append("inner join setdo.settlementOperation sett															");
		querySQL.append("Inner Join sett.mechanismOperation mech															");
		querySQL.append("Inner Join mech.securities sec																		");
		//querySQL.append("Where mech.indReportingBalance = :oneParam														");
		querySQL.append("Where 1 = 1																						");
		querySQL.append("  And mech.operationState = :settledState															");
		querySQL.append("  And mech.mechanisnModality.negotiationMechanism.idNegotiationMechanismPk IN (:idMec)				");
		querySQL.append("  And sett.operationPart = :termPart	And sett.indPartial = :zeroParam							");
		querySQL.append("  And sec.instrumentType = :instrumentType															");
		querySQL.append("  And setrr.requestType = 2018																		");
		querySQL.append("  And setrr.requestState = 1543																	");
		querySQL.append("  And sett.indExtended = 1																		");
		querySQL.append("  And sett.indUnfulfilled <> 1																	");
		querySQL.append("  And sec.stateSecurity In (:stateSecurities)														");
		querySQL.append("  And sec.idSecurityCodePk In 																		");
		
		/**SubQuery to associate parameters executed in the process*/
		StringBuilder subQuery = new StringBuilder();
		ValuatorProcessExecution execution = find(ValuatorProcessExecution.class, processPk);
		Integer valuatorType = execution.getValuatorType();
		
		subQuery.append("(Select Distinct hab.security.idSecurityCodePk 							");
		subQuery.append(" From HolderAccountBalance hab			Where 1 = 1 						");
		
		if(valuatorType.equals(ValuatorType.BY_PARTICIPANT.getCode())){
			subQuery.append("AND hab.participant.idParticipantPk IN (								");
			subQuery.append("Select par.participant.idParticipantPk									");
		}
		if(valuatorType.equals(ValuatorType.BY_SECURITY.getCode())){
			subQuery.append("AND hab.security.idSecurityCodePk IN (									");
			subQuery.append("Select par.security.idSecurityCodePk									");
		}
		if(valuatorType.equals(ValuatorType.BY_INVESTOR.getCode())){
			subQuery.append("AND hab.holderAccount.idHolderAccountPk IN (							");
			subQuery.append("Select par.holderAccount.idHolderAccountPk								");
		}
		
		subQuery.append("From ValuatorExecutionParameters par										");
		subQuery.append("Where par.valuatorProcessExecution.idProcessExecutionPk = :processPk))		");
		
		/**Adding subQuery with main query*/
		querySQL.append(subQuery.toString());
		
		/**Verify instrument type, should get only securities with expirationDate greater than processDate*/
		if(instrumentType.equals(InstrumentType.FIXED_INCOME.getCode())){
			querySQL.append("And Trunc(sec.expirationDate) > Trunc(:processDate)					");
		}

		Query query = em.createQuery(querySQL.toString());
		query.setParameter("processPk", processPk);
		query.setParameter("instrumentType", instrumentType);
		//query.setParameter("oneParam", ComponentConstant.ONE);
		query.setParameter("zeroParam", ComponentConstant.ZERO);
		query.setParameter("termPart", ComponentConstant.CASH_PART);
		if(instrumentType.equals(InstrumentType.FIXED_INCOME.getCode())){
			query.setParameter("processDate", execution.getProcessDate());
		}
		query.setParameter("settledState", MechanismOperationStateType.ASSIGNED_STATE.getCode());
		query.setParameter("stateSecurities", Arrays.asList(SecurityStateType.REGISTERED.getCode(),SecurityStateType.BLOCKED.getCode()));
		query.setParameter("idMec", Arrays.asList(
				NegotiationMechanismType.BOLSA.getCode()
				//NegotiationMechanismType.OTC.getCode(),
				//NegotiationMechanismType.SIRTEX.getCode()
				));
		List<MechanismOperation> operationList = new ArrayList<MechanismOperation>(); 
		List<Object[]> objectDataList = query.getResultList();
		for(Object[] data: objectDataList){
			MechanismOperation operation = new MechanismOperation();
			operation.setIdMechanismOperationPk((Long) data[0]);
			operation.setStockQuantity((BigDecimal) data[1]);
			operation.setQuantity((BigDecimal) data[2]);
			operation.setCashPrice((BigDecimal) data[3]);
			operation.setAmountRate((BigDecimal) data[4]);
			operation.setOperationDate((Date) data[5]);
			operation.setOriginRequest((Long) data[7]);
			Security security = new Security((String)data[6]);
			operation.setSecurities(security);
			operationList.add(operation);
		}
		return operationList;
	}
	
	/**
	 * Row security code quantity.
	 *
	 * @return the integer
	 */
	public Integer rowSecurityCodeQuantity(){
		return (int) (freeMemory / 23);
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		long allocatedMemory      = (Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory());
		System.out.println("MAX MEMORY-> "+Runtime.getRuntime().maxMemory());
		System.out.println("TOTAL MEMORY-> "+Runtime.getRuntime().totalMemory());
		System.out.println("FREE MEMORY-> "+Runtime.getRuntime().freeMemory());
		System.out.println("AVAILABLE MEMORY-> "+ (Runtime.getRuntime().maxMemory() - allocatedMemory));
		System.out.println(Runtime.getRuntime().availableProcessors());
		String securityCode = "BTS-3194712924731892372";
		System.out.println("NRO BYTES-> "+securityCode.getBytes().length);
	}
	
	/**
	 * Gets the valuator event behavior.
	 *
	 * @return the valuator event behavior
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	@Lock(LockType.READ)
	public List<ValuatorEventBehavior> getValuatorEventBehavior() throws ServiceException{
		StringBuilder querySQL = new StringBuilder();
		querySQL.append("Select event							");
		querySQL.append("From ValuatorEventBehavior event		");
		querySQL.append("Where event.indActive = :oneParam		");
		querySQL.append("Order By event.idValuatorEventPk		");
		
		Query query = em.createQuery(querySQL.toString());
		query.setParameter("oneParam", ComponentConstant.ONE);
		
		return query.getResultList();
	}
	
	/**
	 * Metodo para actualizar el indicador ind_active en holderMarketfactBalance
	 * cuando el registro tiene todos los saldos en 0
	 */
	public void updateHolderMarketfactBalance(){
		
		StringBuilder queryUpdate = new StringBuilder();
		
		queryUpdate.append("  UPDATE HOLDER_MARKETFACT_BALANCE                             ");
		queryUpdate.append("  SET IND_ACTIVE =:indActive ,                                 ");
		queryUpdate.append("  	LAST_MODIFY_DATE = :currentDateTime                        ");
		queryUpdate.append("  WHERE ID_MARKETFACT_BALANCE_PK IN (                          ");
		queryUpdate.append("                                                               ");
		queryUpdate.append("    SELECT HMB.ID_MARKETFACT_BALANCE_PK                        ");
		queryUpdate.append("    FROM HOLDER_MARKETFACT_BALANCE HMB                         ");
		queryUpdate.append("    INNER JOIN SECURITY SEC                                    ");
		queryUpdate.append("  		ON HMB.ID_SECURITY_CODE_FK = SEC.ID_SECURITY_CODE_PK   ");
		queryUpdate.append("    WHERE 1 = 1                                                ");
		//queryUpdate.append("    AND SEC.SECURITY_CLASS IN (420,1976)                     ");
		queryUpdate.append("    AND HMB.TOTAL_BALANCE         = 0                          ");
		queryUpdate.append("    AND HMB.AVAILABLE_BALANCE     = 0                          ");
		queryUpdate.append("    AND HMB.TRANSIT_BALANCE       = 0                          ");
		queryUpdate.append("    AND HMB.PAWN_BALANCE          = 0                          ");
		queryUpdate.append("    AND HMB.BAN_BALANCE           = 0                          ");
		queryUpdate.append("    AND HMB.OTHER_BLOCK_BALANCE   = 0                          ");
		queryUpdate.append("    AND HMB.RESERVE_BALANCE       = 0                          ");
		queryUpdate.append("    AND HMB.ACCREDITATION_BALANCE = 0                          ");
		queryUpdate.append("    AND HMB.PURCHASE_BALANCE      = 0                          ");
		queryUpdate.append("    AND HMB.SALE_BALANCE          = 0                          ");
		queryUpdate.append("    AND HMB.REPORTING_BALANCE     = 0                          ");
		queryUpdate.append("    AND HMB.REPORTED_BALANCE      = 0                          ");
		queryUpdate.append("    AND HMB.MARGIN_BALANCE        = 0                          ");
		queryUpdate.append("    AND HMB.BORROWER_BALANCE      = 0                          ");
		queryUpdate.append("    AND HMB.LENDER_BALANCE        = 0                          ");
		queryUpdate.append("    AND HMB.LOANABLE_BALANCE      = 0                          ");
		queryUpdate.append("    AND HMB.OPPOSITION_BALANCE    = 0                          ");
		queryUpdate.append("    AND HMB.IND_ACTIVE = 1                                     ");
		queryUpdate.append("    )                                                          ");
		
		Query queryToUpdate = em.createNativeQuery(queryUpdate.toString());
		queryToUpdate.setParameter("indActive", GeneralConstants.ZERO_VALUE_INT);
		queryToUpdate.setParameter("currentDateTime", CommonsUtilities.currentDateTime());
		
		queryToUpdate.executeUpdate();
	}
	
	/**
	 * Cuponera de Amortizacion vigentes
	 */
	public AmortizationPaymentSchedule findAmortizationPaymentScheduleForPending(String idSecurityCodePk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT aps FROM AmortizationPaymentSchedule aps");
		sbQuery.append("	INNER JOIN FETCH aps.programAmortizationCoupons pac");
		sbQuery.append("	WHERE aps.security.idSecurityCodePk = :idSecurityCodePk");
		sbQuery.append("	AND pac.stateProgramAmortization = :state");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		query.setParameter("state", ProgramScheduleStateType.PENDING.getCode());
		
		AmortizationPaymentSchedule result = null;
		try {
			result = (AmortizationPaymentSchedule) query.getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return result;
	}
	
	/**
	 * Cuponera de interes vigentes
	 */
	public InterestPaymentSchedule findInterestPaymentScheduleForPending(String idSecurityCodePk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ips FROM InterestPaymentSchedule ips");
		sbQuery.append("	INNER JOIN FETCH ips.programInterestCoupons pic");
		sbQuery.append("	WHERE ips.security.idSecurityCodePk = :idSecurityCodePk");
		sbQuery.append("	AND pic.stateProgramInterest = :state");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		query.setParameter("state", ProgramScheduleStateType.PENDING.getCode());
		
		InterestPaymentSchedule result = null;
		try {
			result = (InterestPaymentSchedule) query.getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return result;
	}
	
	/**
	 * Metodo para buscar el valor en REPORTO para calculo de precio curva issue 1290
	 * @param processDate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<SecurityReportResultTO> searchSecurityReportForPriceCurveAGB(Date processDate){

		StringBuilder querySql = new StringBuilder();
		
		querySql.append(" SELECT valor,                                                                 ");
		querySql.append("   (SELECT s.INSTRUMENT_TYPE FROM security s WHERE s.ID_SECURITY_CODE_PK=valor ");
		querySql.append("   ) INSTRUMENT_TYPE,                                                          ");
		querySql.append("   tasamer,                                                                    ");
		querySql.append("   fechamer,                                                                    ");
		querySql.append("   1 as operationpart                                                                    ");
		querySql.append(" FROM                                                                          ");
		querySql.append("   (SELECT ID_SECURITY_CODE_PK valor ,                                         ");
		querySql.append("     MARKET_DATE fechamer,                                                     ");
		querySql.append("     MARKET_RATE tasamer                                                       ");
		querySql.append("   FROM                                                                        ");
		querySql.append("     ( SELECT DISTINCT V.ID_SECURITY_CODE_PK,                                  ");
		querySql.append("       V.MARKET_DATE,                                                          ");
		querySql.append("       V.MARKET_RATE                                                           ");
		querySql.append("     FROM MARKET_FACT_VIEW v                                                   ");
		querySql.append("     WHERE V.ID_SECURITY_CODE_PK IN                                            ");
		querySql.append("       ( SELECT DISTINCT sec.ID_SECURITY_CODE_PK                               ");
		querySql.append("       FROM MECHANISM_OPERATION MO                                             ");
		querySql.append("       INNER JOIN security sec                                                 ");
		querySql.append("       ON sec.ID_SECURITY_CODE_PK = MO.ID_SECURITY_CODE_FK                     ");
		querySql.append("       INNER JOIN SETTLEMENT_OPERATION so                                      ");
		querySql.append("       ON so.ID_MECHANISM_OPERATION_FK    = MO.ID_MECHANISM_OPERATION_PK       ");
		querySql.append("       WHERE 1                            = 1                                  ");
		querySql.append("       AND MO.ID_NEGOTIATION_MODALITY_FK IN (3,4)                              ");
		querySql.append("       AND SO.SETTLEMENT_DATE             = :processDate                       ");
		querySql.append("       AND so.OPERATION_PART              = 1                                  ");
		querySql.append("       )                                                                       ");
		querySql.append("     AND V.REPORTED_BALANCE >0                                                 ");
		querySql.append("     AND V.CUT_DATE         = :processCutDate                                  ");
		
		//querySql.append("     AND V.ID_SECURITY_CODE_PK         = 'BTS-NC52001538'                                  ");
		
		querySql.append("     UNION                                                                     ");
		querySql.append("     SELECT DISTINCT V.ID_SECURITY_CODE_PK,                                    ");
		querySql.append("       V.MARKET_DATE,                                                          ");
		querySql.append("       V.MARKET_RATE                                                           ");
		querySql.append("     FROM MARKET_FACT_VIEW v                                                   ");
		querySql.append("     WHERE V.ID_SECURITY_CODE_PK IN                                            ");
		querySql.append("       ( SELECT DISTINCT sec.ID_SECURITY_CODE_PK                               ");
		querySql.append("       FROM MECHANISM_OPERATION MO                                             ");
		querySql.append("       INNER JOIN security sec                                                 ");
		querySql.append("       ON sec.ID_SECURITY_CODE_PK = MO.ID_SECURITY_CODE_FK                     ");
		querySql.append("       INNER JOIN SETTLEMENT_OPERATION so                                      ");
		querySql.append("       ON so.ID_MECHANISM_OPERATION_FK    = MO.ID_MECHANISM_OPERATION_PK       ");
		querySql.append("       WHERE 1                            = 1                                  ");
		querySql.append("       AND MO.ID_NEGOTIATION_MODALITY_FK IN (3,4)                              ");
		querySql.append("       AND SO.SETTLEMENT_DATE             = :processDate                       ");
		querySql.append("       AND so.OPERATION_PART              = 1                                  ");
		querySql.append("       )                                                                       ");
		querySql.append("     AND V.total_balance     >0                                                ");
		querySql.append("     AND V.AVAILABLE_BALANCE >0                                                ");
		querySql.append("     AND V.CUT_DATE          = :processCutDate                                 ");
		
		//querySql.append("     AND V.ID_SECURITY_CODE_PK         = 'BTS-NC52001538'                                  ");
		
		querySql.append("     UNION                                                                     ");
		querySql.append("     SELECT DISTINCT sec.ID_SECURITY_CODE_PK,                                  ");
		querySql.append("       SO.SETTLEMENT_DATE,                                                     ");
		querySql.append("       sec.INTEREST_RATE                                                       ");
		querySql.append("     FROM MECHANISM_OPERATION MO                                               ");
		querySql.append("     INNER JOIN security sec                                                   ");
		querySql.append("     ON sec.ID_SECURITY_CODE_PK = MO.ID_SECURITY_CODE_FK                       ");
		querySql.append("     INNER JOIN SETTLEMENT_OPERATION so                                        ");
		querySql.append("     ON so.ID_MECHANISM_OPERATION_FK    = MO.ID_MECHANISM_OPERATION_PK         ");
		querySql.append("     WHERE 1                            = 1                                    ");
		querySql.append("     AND MO.ID_NEGOTIATION_MODALITY_FK IN (3,4)                                ");
		querySql.append("     AND MO.ID_SECURITY_CODE_FK NOT    IN                                      ");
		querySql.append("       ( SELECT DISTINCT V.ID_SECURITY_CODE_PK                                 ");
		querySql.append("       FROM MARKET_FACT_VIEW v                                                 ");
		querySql.append("       WHERE V.ID_SECURITY_CODE_PK IN                                          ");
		querySql.append("         ( SELECT DISTINCT sec.ID_SECURITY_CODE_PK                             ");
		querySql.append("         FROM MECHANISM_OPERATION MO                                           ");
		querySql.append("         INNER JOIN security sec                                               ");
		querySql.append("         ON sec.ID_SECURITY_CODE_PK = MO.ID_SECURITY_CODE_FK                   ");
		querySql.append("         INNER JOIN SETTLEMENT_OPERATION so                                    ");
		querySql.append("         ON so.ID_MECHANISM_OPERATION_FK    = MO.ID_MECHANISM_OPERATION_PK     ");
		querySql.append("         WHERE 1                            = 1                                ");
		querySql.append("         AND MO.ID_NEGOTIATION_MODALITY_FK IN (3,4)                            ");
		querySql.append("         AND SO.SETTLEMENT_DATE             = :processDate                     ");
		querySql.append("         AND so.OPERATION_PART              = 1                                ");
		querySql.append("         )                                                                     ");
		querySql.append("       AND V.REPORTED_BALANCE >0                                               ");
		querySql.append("       AND V.CUT_DATE         = :processCutDate                                ");
		querySql.append("       UNION                                                                   ");
		querySql.append("       SELECT DISTINCT V.ID_SECURITY_CODE_PK                                   ");
		querySql.append("       FROM MARKET_FACT_VIEW v                                                 ");
		querySql.append("       WHERE V.ID_SECURITY_CODE_PK IN                                          ");
		querySql.append("         ( SELECT DISTINCT sec.ID_SECURITY_CODE_PK                             ");
		querySql.append("         FROM MECHANISM_OPERATION MO                                           ");
		querySql.append("         INNER JOIN security sec                                               ");
		querySql.append("         ON sec.ID_SECURITY_CODE_PK = MO.ID_SECURITY_CODE_FK                   ");
		querySql.append("         INNER JOIN SETTLEMENT_OPERATION so                                    ");
		querySql.append("         ON so.ID_MECHANISM_OPERATION_FK    = MO.ID_MECHANISM_OPERATION_PK     ");
		querySql.append("         WHERE 1                            = 1                                ");
		querySql.append("         AND MO.ID_NEGOTIATION_MODALITY_FK IN (3,4)                            ");
		querySql.append("         AND SO.SETTLEMENT_DATE             = :processDate                     ");
		querySql.append("         AND so.OPERATION_PART              = 1                                ");
		querySql.append("         )                                                                     ");
		querySql.append("       AND V.total_balance     >0                                              ");
		querySql.append("       AND V.AVAILABLE_BALANCE >0                                              ");
		querySql.append("       AND V.CUT_DATE          = :processCutDate                               ");
		querySql.append("       )                                                                       ");
		querySql.append("     AND SO.SETTLEMENT_DATE = :processDate                                     ");
		querySql.append("     AND so.OPERATION_PART  = 1                                                ");
		
		//querySql.append("     AND sec.ID_SECURITY_CODE_PK        = 'BTS-NC52001538'                                  ");
		
		querySql.append("     )                                                                         ");
		querySql.append("   )                                                                           ");
		
		Query query = em.createNativeQuery(querySql.toString());
		query.setParameter("processDate", processDate);
		query.setParameter("processCutDate", CommonsUtilities.addDate(processDate, -1));
		
		List<Object[]> lstRestul = query.getResultList();
		
		List<SecurityReportResultTO> searchTOList = new ArrayList<SecurityReportResultTO>();
		
		if (lstRestul.size()>0){
			
			for (Object[] lstFiltr : lstRestul){
				
				SecurityReportResultTO searchTO = new SecurityReportResultTO();
				
				searchTO.setSecurityCode(lstFiltr[0].toString());
				searchTO.setInstrumentType(Integer.valueOf(lstFiltr[1].toString()));
				if(Validations.validateIsNotNullAndNotEmpty(lstFiltr[2])){
					searchTO.setMarketRate((BigDecimal)lstFiltr[2]);
				}
				searchTO.setMarketDate((Date)lstFiltr[3]);
				BigDecimal operationPart = (BigDecimal)lstFiltr[4];
				searchTO.setOperationPart(operationPart.intValue());
				searchTOList.add(searchTO);
			}
		}
		
		return searchTOList;
	}
	
	/**
	 * Metodo para buscar el valor en REPORTO para calculo de precio curva issue 1290
	 * @param processDate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<SecurityReportResultTO> searchSecurityReportForPriceCurveASFI(Date processDate){

		StringBuilder querySql = new StringBuilder();
		
		querySql.append(" SELECT valor,                                                                 ");
		querySql.append("   (SELECT s.INSTRUMENT_TYPE FROM security s WHERE s.ID_SECURITY_CODE_PK=valor ");
		querySql.append("   ) INSTRUMENT_TYPE,                                                          ");
		querySql.append("   tasamer,                                                                    ");
		querySql.append("   fechamer,                                                                   ");
		querySql.append("   operationpart                                                               ");
		querySql.append(" FROM  (                                                                       ");
		querySql.append("   SELECT ID_SECURITY_CODE_PK valor ,                                          ");
		querySql.append("     MARKET_DATE fechamer,                                                     ");
		querySql.append("     MARKET_RATE tasamer,                                                      ");
		querySql.append("     OPERATION_PART operationpart                                              ");
		querySql.append("   FROM (                                                                      ");
		
		querySql.append("     SELECT DISTINCT sec.ID_SECURITY_CODE_PK,                                  ");
		querySql.append("       SO.SETTLEMENT_DATE as market_date,                                      ");
		querySql.append("       sec.INTEREST_RATE as market_rate,                                       ");
		querySql.append("       so.OPERATION_PART as OPERATION_PART                                     ");
		querySql.append("     FROM MECHANISM_OPERATION MO                                               ");
		querySql.append("     INNER JOIN security sec                                                   ");
		querySql.append("     ON sec.ID_SECURITY_CODE_PK = MO.ID_SECURITY_CODE_FK                       ");
		querySql.append("     INNER JOIN SETTLEMENT_OPERATION so                                        ");
		querySql.append("     ON so.ID_MECHANISM_OPERATION_FK    = MO.ID_MECHANISM_OPERATION_PK         ");
		querySql.append("     WHERE 1 = 1                                							    ");
		querySql.append("     AND MO.ID_NEGOTIATION_MODALITY_FK IN (3,4,1,2)                            ");
		querySql.append("     AND MO.ID_NEGOTIATION_MECHANISM_FK in ( 1,3 )                             ");
		querySql.append("     AND CASE  WHEN SO.IND_EXTENDED = 1                                                                                        "); 
		querySql.append("             	 AND SO.IND_PREPAID= 1                                                                                          "); 
		querySql.append("             THEN (select DISTINCT TRUNC(SR.AUTHORIZE_DATE)                                                                    "); 
		querySql.append("                 from SETTLEMENT_DATE_OPERATION SDO                                                                            "); 
		querySql.append("                 inner join SETTLEMENT_REQUEST SR on SDO.ID_SETTLEMENT_REQUEST_FK=SR.ID_SETTLEMENT_REQUEST_PK                  "); 
		querySql.append("                 inner join SETTLEMENT_OPERATION SO_SUB on SDO.ID_SETTLEMENT_OPERATION_FK=SO_SUB.ID_SETTLEMENT_OPERATION_PK    "); 
		querySql.append("                 inner join MECHANISM_OPERATION MO_SUB on SO_SUB.ID_MECHANISM_OPERATION_FK=MO_SUB.ID_MECHANISM_OPERATION_PK    "); 
		querySql.append("                 where SR.REQUEST_TYPE=2019                                                                                    "); 
		querySql.append("                 and MO_SUB.ID_MECHANISM_OPERATION_PK = MO.ID_MECHANISM_OPERATION_PK                                           "); 
		querySql.append("                 and SR.REQUEST_STATE = 1543 )                                                                                 ");
		querySql.append("     			WHEN SO.IND_PREPAID = 1 THEN TRUNC(SO.SETTLEMENT_DATE)          "); 
		querySql.append("     	     	WHEN SO.OPERATION_PART = 1 THEN TRUNC(MO.OPERATION_DATE)        "); 
		querySql.append("               WHEN SO.OPERATION_PART = 2 THEN TRUNC(MO.TERM_SETTLEMENT_DATE)  ");
		querySql.append("     END = :processDate                                                        ");
		querySql.append("     AND so.OPERATION_PART  in (1,2)                                           ");
		//querySql.append("     AND sec.ID_SECURITY_CODE_PK        = 'BBB-BSO-1-N2U-13'                 ");
		querySql.append("     )                                                                         ");
		querySql.append("   )                                                                           ");
		
		Query query = em.createNativeQuery(querySql.toString());
		query.setParameter("processDate", processDate);
		//query.setParameter("processCutDate", CommonsUtilities.addDate(processDate, -1));
		
		List<Object[]> lstRestul = query.getResultList();
		
		List<SecurityReportResultTO> searchTOList = new ArrayList<SecurityReportResultTO>();
		
		if (lstRestul.size()>0){
			
			for (Object[] lstFiltr : lstRestul){
				
				SecurityReportResultTO searchTO = new SecurityReportResultTO();
				
				searchTO.setSecurityCode(lstFiltr[0].toString());
				searchTO.setInstrumentType(Integer.valueOf(lstFiltr[1].toString()));
				if(Validations.validateIsNotNullAndNotEmpty(lstFiltr[2])){
					searchTO.setMarketRate((BigDecimal)lstFiltr[2]);
				}
				searchTO.setMarketDate((Date)lstFiltr[3]);
				BigDecimal operationPart = (BigDecimal)lstFiltr[4];
				searchTO.setOperationPart(operationPart.intValue());
				searchTOList.add(searchTO);
			}
		}
		
		return searchTOList;
	}
	
	
	/**
	 * Metodo para buscar registros de valuator price report en la fecha
	 * @param processDate
	 * @return
	 */
	public List<ValuatorPriceReport> searchValuatorPriceReportByCurrenDate(Date processDate){

		StringBuilder querySql = new StringBuilder();
		
		querySql.append(" SELECT *        						");
		querySql.append(" FROM VALUATOR_PRICE_REPORT  			");
		querySql.append(" WHERE PROCESS_DATE  = :processDate    ");

		Query query = em.createNativeQuery(querySql.toString());
		query.setParameter("processDate", processDate);
		
		List<ValuatorPriceReport> lstRestul = (List<ValuatorPriceReport>)query.getResultList();
				
		return lstRestul;
	}
	
	/**
	 * Metodo para ver si es historico o no
	 */
	public Integer indHistoricPrice() {
		
		StringBuilder querySql = new StringBuilder();
		
		querySql.append(" SELECT INDICATOR4     				      ");
		querySql.append(" FROM PARAMETER_TABLE  					  ");
		querySql.append(" WHERE PARAMETER_TABLE_PK  = :processDate    ");

		Query query = em.createNativeQuery(querySql.toString());
		query.setParameter("processDate", 2567);
		
		BigDecimal bdResult = (BigDecimal)query.getSingleResult();
		
		Integer indResult = bdResult.intValue();
		
		/*Integer rutePt = 2567;
		ParameterTable objParameterTable=new ParameterTable();
		objParameterTable = find(ParameterTable.class, rutePt);*/
		//return objParameterTable.getIndicator4();
		return indResult;
		
		
	}
	
	
	/**
	 * Metodo para borrar registros de valuator price report en la fecha
	 * @param processDate
	 * @return
	 */
	public void restartDataValuatorPricesReportHist(){

		StringBuilder queryUpdateOpSet = new StringBuilder();
		
		queryUpdateOpSet.append(" DELETE FROM  VALUATOR_PRICE_REPORT   							");
		//queryUpdateOpSet.append(" WHERE  ID_SETTLMENT_PROCESS_FK = :idSettlementProcessPk		");
		
		Query queryToUpdateOpSet = em.createNativeQuery(queryUpdateOpSet.toString());
		//queryToUpdateOpSet.setParameter("idSettlementProcessPk", idSettlementProcessPk);
		queryToUpdateOpSet.executeUpdate();
				
	}
	
	
	
}