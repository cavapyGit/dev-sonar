package com.pradera.securities.valuator.core.files.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.model.generalparameter.ParameterTable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MarketFactMgmtTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , Aug 25, 2015
 */
public class MarketFactMgmtTO  implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The lst marketfact type. */
	private List<ParameterTable>	lstMarketfactType;
	
	/** The lst file source. */
	private List<ParameterTable>	lstFileSource;
	
	/** The lst file loading. */
	private List<ParameterTable>	lstFileLoading;
	
	/** The lst marketfact state. */
	private List<ParameterTable>	lstMarketfactState;
	
	/** The lst marketfact process. */
	private List<ParameterTable> 	lstMarketfactProcess;
	
	/** The lst valuator marketfact file to. */
	private GenericDataModel<ValuatorMarketfactFileTO> lstValuatorMarketfactFileTO;
	
	/** The lst register v marketfact file to. */
	private GenericDataModel<ValuatorMarketfactFileTO> lstRegisterVMarketfactFileTO;
	
	/** The lst errors. */
	private GenericDataModel<RecordValidationType> lstErrors;
	
	/** The process file to. */
	private ProcessFileTO processFileTO;
	
	/** The marketfact type pk. */
	private Integer marketfactTypePk;
	
	/** The file source. */
	private Integer fileSource;
	
	/** The file loading type. */
	private Integer fileLoadingType;
	
	/** The registry date. */
	private Date registryDate;
	
	/** The ind historic. */
	private Integer indHistoric;
	
	/** The process date. */
	private Date processDate;
	
	/** The ind tomorrow holliday. */
	private Integer indTomorrowHolliday;
	
	/** The valuator marketfact file to. */
	private ValuatorMarketfactFileTO valuatorMarketfactFileTO;
	
	/**
	 * Instantiates a new market fact mgmt to.
	 */
	public MarketFactMgmtTO(){
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the lst marketfact type.
	 *
	 * @return the lst marketfact type
	 */
	public List<ParameterTable> getLstMarketfactType() {
		return lstMarketfactType;
	}

	/**
	 * Sets the lst marketfact type.
	 *
	 * @param lstMarketfactType the new lst marketfact type
	 */
	public void setLstMarketfactType(List<ParameterTable> lstMarketfactType) {
		this.lstMarketfactType = lstMarketfactType;
	}
	
	/**
	 * Gets the lst marketfact state.
	 *
	 * @return the lst marketfact state
	 */
	public List<ParameterTable> getLstMarketfactState() {
		return lstMarketfactState;
	}

	/**
	 * Sets the lst marketfact state.
	 *
	 * @param lstMarketfactState the new lst marketfact state
	 */
	public void setLstMarketfactState(List<ParameterTable> lstMarketfactState) {
		this.lstMarketfactState = lstMarketfactState;
	}

	/**
	 * Gets the lst marketfact process.
	 *
	 * @return the lst marketfact process
	 */
	public List<ParameterTable> getLstMarketfactProcess() {
		return lstMarketfactProcess;
	}

	/**
	 * Sets the lst marketfact process.
	 *
	 * @param lstMarketfactProcess the new lst marketfact process
	 */
	public void setLstMarketfactProcess(List<ParameterTable> lstMarketfactProcess) {
		this.lstMarketfactProcess = lstMarketfactProcess;
	}

	/**
	 * Gets the lst valuator marketfact file to.
	 *
	 * @return the lst valuator marketfact file to
	 */
	public GenericDataModel<ValuatorMarketfactFileTO> getLstValuatorMarketfactFileTO() {
		return lstValuatorMarketfactFileTO;
	}

	/**
	 * Sets the lst valuator marketfact file to.
	 *
	 * @param lstValuatorMarketfactFileTO the new lst valuator marketfact file to
	 */
	public void setLstValuatorMarketfactFileTO(
			GenericDataModel<ValuatorMarketfactFileTO> lstValuatorMarketfactFileTO) {
		this.lstValuatorMarketfactFileTO = lstValuatorMarketfactFileTO;
	}
	
	 
	/**
	 * Gets the marketfact type pk.
	 *
	 * @return the marketfact type pk
	 */
	public Integer getMarketfactTypePk() {
		return marketfactTypePk;
	}

	/**
	 * Sets the marketfact type pk.
	 *
	 * @param marketfactTypePk the new marketfact type pk
	 */
	public void setMarketfactTypePk(Integer marketfactTypePk) {
		this.marketfactTypePk = marketfactTypePk;
	}
	
	/**
	 * Gets the file source.
	 *
	 * @return the file source
	 */
	public Integer getFileSource() {
		return fileSource;
	}

	/**
	 * Sets the file source.
	 *
	 * @param fileSource the new file source
	 */
	public void setFileSource(Integer fileSource) {
		this.fileSource = fileSource;
	}

	/**
	 * Gets the valuator marketfact file to.
	 *
	 * @return the valuator marketfact file to
	 */
	public ValuatorMarketfactFileTO getValuatorMarketfactFileTO() {
		return valuatorMarketfactFileTO;
	}

	/**
	 * Sets the valuator marketfact file to.
	 *
	 * @param valuatorMarketfactFileTO the new valuator marketfact file to
	 */
	public void setValuatorMarketfactFileTO(
			ValuatorMarketfactFileTO valuatorMarketfactFileTO) {
		this.valuatorMarketfactFileTO = valuatorMarketfactFileTO;
	}

	/**
	 * Gets the lst register v marketfact file to.
	 *
	 * @return the lst register v marketfact file to
	 */
	public GenericDataModel<ValuatorMarketfactFileTO> getLstRegisterVMarketfactFileTO() {
		return lstRegisterVMarketfactFileTO;
	}

	/**
	 * Sets the lst register v marketfact file to.
	 *
	 * @param lstRegisterVMarketfactFileTO the new lst register v marketfact file to
	 */
	public void setLstRegisterVMarketfactFileTO(
			GenericDataModel<ValuatorMarketfactFileTO> lstRegisterVMarketfactFileTO) {
		this.lstRegisterVMarketfactFileTO = lstRegisterVMarketfactFileTO;
	}

	/**
	 * Gets the lst errors.
	 *
	 * @return the lst errors
	 */
	public GenericDataModel<RecordValidationType> getLstErrors() {
		return lstErrors;
	}

	/**
	 * Sets the lst errors.
	 *
	 * @param lstErrors the new lst errors
	 */
	public void setLstErrors(GenericDataModel<RecordValidationType> lstErrors) {
		this.lstErrors = lstErrors;
	}

	/**
	 * Gets the lst file source.
	 *
	 * @return the lst file source
	 */
	public List<ParameterTable> getLstFileSource() {
		return lstFileSource;
	}

	/**
	 * Sets the lst file source.
	 *
	 * @param lstFileSource the new lst file source
	 */
	public void setLstFileSource(List<ParameterTable> lstFileSource) {
		this.lstFileSource = lstFileSource;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the process file to.
	 *
	 * @return the process file to
	 */
	public ProcessFileTO getProcessFileTO() {
		return processFileTO;
	}

	/**
	 * Sets the process file to.
	 *
	 * @param processFileTO the new process file to
	 */
	public void setProcessFileTO(ProcessFileTO processFileTO) {
		this.processFileTO = processFileTO;
	}

	/**
	 * Gets the ind historic.
	 *
	 * @return the ind historic
	 */
	public Integer getIndHistoric() {
		return indHistoric;
	}

	/**
	 * Sets the ind historic.
	 *
	 * @param indHistoric the new ind historic
	 */
	public void setIndHistoric(Integer indHistoric) {
		this.indHistoric = indHistoric;
	}

	/**
	 * Gets the process date.
	 *
	 * @return the process date
	 */
	public Date getProcessDate() {
		return processDate;
	}

	/**
	 * Sets the process date.
	 *
	 * @param processDate the new process date
	 */
	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	/**
	 * Gets the ind tomorrow holliday.
	 *
	 * @return the ind tomorrow holliday
	 */
	public Integer getIndTomorrowHolliday() {
		return indTomorrowHolliday;
	}

	/**
	 * Sets the ind tomorrow holliday.
	 *
	 * @param indTomorrowHolliday the new ind tomorrow holliday
	 */
	public void setIndTomorrowHolliday(Integer indTomorrowHolliday) {
		this.indTomorrowHolliday = indTomorrowHolliday;
	}

	/**
	 * Gets the file loading type.
	 *
	 * @return the file loading type
	 */
	public Integer getFileLoadingType() {
		return fileLoadingType;
	}

	/**
	 * Sets the file loading type.
	 *
	 * @param fileLoadingType the new file loading type
	 */
	public void setFileLoadingType(Integer fileLoadingType) {
		this.fileLoadingType = fileLoadingType;
	}

	/**
	 * Gets the lst file loading.
	 *
	 * @return the lst file loading
	 */
	public List<ParameterTable> getLstFileLoading() {
		return lstFileLoading;
	}

	/**
	 * Sets the lst file loading.
	 *
	 * @param lstFileLoading the new lst file loading
	 */
	public void setLstFileLoading(List<ParameterTable> lstFileLoading) {
		this.lstFileLoading = lstFileLoading;
	}
}