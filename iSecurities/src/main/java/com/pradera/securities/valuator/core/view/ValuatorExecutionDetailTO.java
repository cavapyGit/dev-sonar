package com.pradera.securities.valuator.core.view;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.model.valuatorprocess.ValuatorProcessExecution;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ValuatorExecutionDetailTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class ValuatorExecutionDetailTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id execution detail pk. */
	private Long idExecutionDetailPk;
	
	/** The detail type. */
	private Integer detailType;
	
	/** The begin time. */
	private Date beginTime;
	
	/** The finish time. */
	private Date finishTime;
	
	/** The order execution. */
	private Integer orderExecution;
	
	/** The quantity process. */
	private Long quantityProcess;
	
	/** The quantity inconsistent. */
	private Long quantityInconsistent;
	
	/** The quantity finished. */
	private Long quantityFinished;
	
	/** The process state. */
	private Integer processState;
	
	/** The registry date. */
	private Date registryDate;
	
	/** The registry user. */
	private String registryUser;
	
	/** The valuator process execution. */
	private ValuatorProcessExecution valuatorProcessExecution;
	
	/** The valuator execution faileds. */
	private List<ValuatorExecutionFailedTO> valuatorExecutionFaileds;

	/**
	 * Instantiates a new valuator execution detail to.
	 */
	public ValuatorExecutionDetailTO() {
	}

	/**
	 * Instantiates a new valuator execution detail to.
	 *
	 * @param idExecutionDetailPk the id execution detail pk
	 * @param detailType the detail type
	 * @param beginTime the begin time
	 * @param finishTime the finish time
	 * @param orderExecution the order execution
	 * @param quantityProcess the quantity process
	 * @param quantityInconsistent the quantity inconsistent
	 * @param quantityFinished the quantity finished
	 * @param processState the process state
	 * @param idProcessExecutionPk the id process execution pk
	 */
	public ValuatorExecutionDetailTO(Long idExecutionDetailPk,
			Integer detailType, Date beginTime, Date finishTime,
			Integer orderExecution, Long quantityProcess,
			Long quantityInconsistent, Long quantityFinished,
			Integer processState, Long idProcessExecutionPk ) {
		super();
		this.idExecutionDetailPk = idExecutionDetailPk;
		this.detailType = detailType;
		this.beginTime = beginTime;
		this.finishTime = finishTime;
		this.orderExecution = orderExecution;
		this.quantityProcess = quantityProcess;
		this.quantityInconsistent = quantityInconsistent;
		this.quantityFinished = quantityFinished;
		this.processState = processState;
		ValuatorProcessExecution valuatorProcessExecution = new ValuatorProcessExecution();
		valuatorProcessExecution.setIdProcessExecutionPk(idProcessExecutionPk);
		this.valuatorProcessExecution = valuatorProcessExecution;
	}

	/**
	 * Gets the id execution detail pk.
	 *
	 * @return the id execution detail pk
	 */
	public Long getIdExecutionDetailPk() {
		return this.idExecutionDetailPk;
	}

	/**
	 * Sets the id execution detail pk.
	 *
	 * @param idExecutionDetailPk the new id execution detail pk
	 */
	public void setIdExecutionDetailPk(Long idExecutionDetailPk) {
		this.idExecutionDetailPk = idExecutionDetailPk;
	}

	/**
	 * Gets the begin time.
	 *
	 * @return the begin time
	 */
	public Date getBeginTime() {
		return this.beginTime;
	}

	/**
	 * Sets the begin time.
	 *
	 * @param beginTime the new begin time
	 */
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	/**
	 * Gets the finish time.
	 *
	 * @return the finish time
	 */
	public Date getFinishTime() {
		return this.finishTime;
	}

	/**
	 * Sets the finish time.
	 *
	 * @param finishTime the new finish time
	 */
	public void setFinishTime(Date finishTime) {
		this.finishTime = finishTime;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the valuator process execution.
	 *
	 * @return the valuator process execution
	 */
	public ValuatorProcessExecution getValuatorProcessExecution() {
		return this.valuatorProcessExecution;
	}

	/**
	 * Sets the valuator process execution.
	 *
	 * @param valuatorProcessExecution the new valuator process execution
	 */
	public void setValuatorProcessExecution(ValuatorProcessExecution valuatorProcessExecution) {
		this.valuatorProcessExecution = valuatorProcessExecution;
	}

	/**
	 * Gets the valuator execution faileds.
	 *
	 * @return the valuator execution faileds
	 */
	public List<ValuatorExecutionFailedTO> getValuatorExecutionFaileds() {
		return this.valuatorExecutionFaileds;
	}

	/**
	 * Sets the valuator execution faileds.
	 *
	 * @param valuatorExecutionFaileds the new valuator execution faileds
	 */
	public void setValuatorExecutionFaileds(List<ValuatorExecutionFailedTO> valuatorExecutionFaileds) {
		this.valuatorExecutionFaileds = valuatorExecutionFaileds;
	}

	/**
	 * Adds the valuator execution failed.
	 *
	 * @param valuatorExecutionFailed the valuator execution failed
	 * @return the valuator execution failed to
	 */
	public ValuatorExecutionFailedTO addValuatorExecutionFailed(ValuatorExecutionFailedTO valuatorExecutionFailed) {
		getValuatorExecutionFaileds().add(valuatorExecutionFailed);
		valuatorExecutionFailed.setValuatorExecutionDetail(this);

		return valuatorExecutionFailed;
	}

	/**
	 * Removes the valuator execution failed.
	 *
	 * @param valuatorExecutionFailed the valuator execution failed
	 * @return the valuator execution failed to
	 */
	public ValuatorExecutionFailedTO removeValuatorExecutionFailed(ValuatorExecutionFailedTO valuatorExecutionFailed) {
		getValuatorExecutionFaileds().remove(valuatorExecutionFailed);
		valuatorExecutionFailed.setValuatorExecutionDetail(null);

		return valuatorExecutionFailed;
	}

	/**
	 * Gets the detail type.
	 *
	 * @return the detail type
	 */
	public Integer getDetailType() {
		return detailType;
	}

	/**
	 * Sets the detail type.
	 *
	 * @param detailType the new detail type
	 */
	public void setDetailType(Integer detailType) {
		this.detailType = detailType;
	}
	
	/**
	 * Gets the order execution.
	 *
	 * @return the order execution
	 */
	public Integer getOrderExecution() {
		return orderExecution;
	}

	/**
	 * Sets the order execution.
	 *
	 * @param orderExecution the new order execution
	 */
	public void setOrderExecution(Integer orderExecution) {
		this.orderExecution = orderExecution;
	}

	/**
	 * Gets the process state.
	 *
	 * @return the process state
	 */
	public Integer getProcessState() {
		return processState;
	}

	/**
	 * Sets the process state.
	 *
	 * @param processState the new process state
	 */
	public void setProcessState(Integer processState) {
		this.processState = processState;
	}

	/**
	 * Gets the quantity finished.
	 *
	 * @return the quantity finished
	 */
	public Long getQuantityFinished() {
		return quantityFinished;
	}

	/**
	 * Sets the quantity finished.
	 *
	 * @param quantityFinished the new quantity finished
	 */
	public void setQuantityFinished(Long quantityFinished) {
		this.quantityFinished = quantityFinished;
	}

	/**
	 * Gets the quantity inconsistent.
	 *
	 * @return the quantity inconsistent
	 */
	public Long getQuantityInconsistent() {
		return quantityInconsistent;
	}

	/**
	 * Sets the quantity inconsistent.
	 *
	 * @param quantityInconsistent the new quantity inconsistent
	 */
	public void setQuantityInconsistent(Long quantityInconsistent) {
		this.quantityInconsistent = quantityInconsistent;
	}

	/**
	 * Gets the quantity process.
	 *
	 * @return the quantity process
	 */
	public Long getQuantityProcess() {
		return quantityProcess;
	}

	/**
	 * Sets the quantity process.
	 *
	 * @param quantityProcess the new quantity process
	 */
	public void setQuantityProcess(Long quantityProcess) {
		this.quantityProcess = quantityProcess;
	}

}