package com.pradera.securities.placementsegment.view;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.primefaces.component.picklist.PickList;
import org.primefaces.model.DualListModel;

import com.pradera.model.accounts.Participant;

// TODO: Auto-generated Javadoc
//public class PrimeFacesPickListConverter {
//
//}

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PrimeFacesPickListConverter.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@FacesConverter(value = "primeFacesPickListConverter")
public class PrimeFacesPickListConverter implements Converter {
	
	/* (non-Javadoc)
	 * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.String)
	 */
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		Object ret = null;
		if (arg1 instanceof PickList) {
			Object dualList = ((PickList) arg1).getValue();
			DualListModel dl = (DualListModel) dualList;
			for (Object o : dl.getSource()) {
				String id = "" + ((Participant) o).getIdParticipantPk();
				if (arg2.equals(id)) {
					ret = o;
					break;
				}
			}
			if (ret == null){
				for (Object o : dl.getTarget()) {
					String id = "" + ((Participant) o).getIdParticipantPk();
					if (arg2.equals(id)) {
						ret = o;
						break;
					}
				}
			}
		}
		return ret;
	}

	/* (non-Javadoc)
	 * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
	 */
	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		String str = "";
		if (arg2 instanceof Participant) {
			str = "" + ((Participant) arg2).getIdParticipantPk();
		}
		return str;
	}
}