package com.pradera.securities.placementsegment.view;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DualListModel;
import org.primefaces.model.StreamedContent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.utils.view.GeneralPropertiesConstants;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.view.IssuanceHelperBean;
import com.pradera.core.component.helperui.view.IssuerHelperBean;
import com.pradera.core.component.helperui.view.SecurityHelpBean;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegParticipaStruct;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegment;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegmentDetail;
import com.pradera.model.issuancesecuritie.placementsegment.type.PlacementSegmentMotiveRejectType;
import com.pradera.model.issuancesecuritie.placementsegment.type.PlacementSegmentStateType;
import com.pradera.model.issuancesecuritie.placementsegment.type.PlacementSegmentStructStateType;
import com.pradera.model.issuancesecuritie.type.IssuanceStateType;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.SecurityPlacementType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.securities.issuancesecuritie.facade.IssuanceSecuritiesServiceFacade;
import com.pradera.securities.issuancesecuritie.facade.SecurityServiceFacade;
import com.pradera.securities.issuancesecuritie.to.IssuanceTO;
import com.pradera.securities.issuancesecuritie.util.PropertiesConstants;
import com.pradera.securities.issuer.to.IssuerTO;
import com.pradera.securities.placementsegment.facade.PlacementSegmentServiceFacade;
import com.pradera.securities.placementsegment.to.MotiveController;
import com.pradera.securities.placementsegment.to.PlacementSegmentTO;
// TODO: Auto-generated Javadoc

/**
 * The Class PlacementSegmentMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@DepositaryWebBean
public class PlacementSegmentMgmtBean extends GenericBaseBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The country residence. */
	@Inject @Configurable Integer countryResidence;
	
	/** The max days of custody request. */
	@Inject @Configurable Integer maxDaysOfCustodyRequest;

	/** The placement segment to. */
	private PlacementSegmentTO placementSegmentTO;
	
	/** The placement segment session. */
	private PlacementSegment placementSegmentSession;
	
	/** The issuer request. */
	private Issuer issuerRequest;
	
	/** The issuer request search. */
	private Issuer issuerRequestSearch;
	
	/** The issuance request. */
	private Issuance issuanceRequest;
	
	/** The issuance request search. */
	private Issuance issuanceRequestSearch;
	
	/** The participant list. */
	private List<Participant> participantList;
	
	private List<Participant> participantListTotal;
	
	/** The placement segment participan struc. */
	private List<PlacementSegParticipaStruct> placementSegmentParticipanStruc;
	
	/** The participant list model. */
	private DualListModel<Participant> participantListModel;
	
	/** The lst cbo placement segment state. */
	private List<ParameterTable> lstCboPlacementSegmentState;
	
	/** The parameter table map. */
	private Map<Integer,String> parameterTableMap = new HashMap<>();
	
	/** The parameter table to. */
	private ParameterTableTO parameterTableTO;
	
	/** The last plac segment. */
	private PlacementSegment lastPlacSegment;
	
	/** The posterior plac segment. */
	private PlacementSegment posteriorPlacSegment;
	
	/** The issuance securities service facade. */
	@EJB
	private IssuanceSecuritiesServiceFacade issuanceSecuritiesServiceFacade;
	
	/** The participant service facade. */
	@EJB
	AccountsFacade accountsFacade;
	
	/** The security service facade. */
	@EJB
	SecurityServiceFacade securityServiceFacade;
	
	/** The placement segment service facade. */
	@EJB
	PlacementSegmentServiceFacade placementSegmentServiceFacade;
	
	/** The placement segment list. */
	private GenericDataModel<PlacementSegment> placementSegmentDataModel;
	
	/** The security list. */
	private List<Security> securityList;
	
	/** The general parameter facade. */
	@EJB
	GeneralParametersFacade generalParameterFacade;
	
	/** The motive controller. */
	private MotiveController motiveController;
	
	/** The issuer helper bean. */
	@Inject
	IssuerHelperBean issuerHelperBean;
	
	/** The issuance helper bean. */
	@Inject
	IssuanceHelperBean issuanceHelperBean;
	
	/** The batch process service facade. */
	@EJB
    private BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject UserPrivilege userPrivilege;
	
	/** The issuer code. */
	private String issuerCode = null; 
	
	/** The placement seg participant. */
	private Participant placementSegParticipant;
	
	/** The opening amount. */
	private BigDecimal openingAmount;


	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		Participant participantTO = new Participant();
		participantTO.setState(ParticipantStateType.REGISTERED.getCode());
		motiveController = new MotiveController();
		try {
			participantList = placementSegmentServiceFacade.getParticipantSecundaryReport(participantTO);
			participantListTotal = new ArrayList<>(participantList);
			participantListModel = new DualListModel<Participant>(this.getParticipantList(),new ArrayList<Participant>());
			loadCboPlacementSegmentStateType();
			loadPlacementSegmentMotive();
			loadHolidays();
			loadUserValidation();
		//Load current date
//		placementSegmentTO = new PlacementSegmentTO(CommonsUtilities.currentDate(),CommonsUtilities.currentDate());
			placementSegmentTO = new PlacementSegmentTO();
			//End - Load current date 
			placementSegmentSession = new PlacementSegment();
			placementSegmentSession.setIssuance(new Issuance());
			issuanceRequest = new Issuance();
			issuerRequest = new Issuer();
			issuerRequestSearch= new Issuer();
			issuanceRequestSearch = new Issuance();
			if(issuerCode != null){
				placementSegmentTO.setIdIssuerPk(issuerCode);
				IssuerTO issuerTO= new IssuerTO();
				issuerTO.setIdIssuerPk(issuerCode);
				issuerRequestSearch= issuanceSecuritiesServiceFacade.findIssuerServiceFacade(issuerTO);
				searchIssuerRequestSearch();
			}
			placementSegmentTO.setSecurity(new Security());
			placementSegmentTO.setParticipant(new Participant());
			placementSegmentTO.setListParticipant(placementSegmentServiceFacade.getParticipantRegistered());
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * Load user validation.
	 */
	private void loadUserValidation() {
		// TODO Auto-generated method stub
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnModifyView(Boolean.TRUE);
		privilegeComponent.setBtnConfirmView(Boolean.TRUE);
		privilegeComponent.setBtnRejectView(Boolean.TRUE);
		privilegeComponent.setBtnBlockView(Boolean.TRUE);
		privilegeComponent.setBtnUnblockView(Boolean.TRUE);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
		
		if(userInfo.getUserAccountSession().isIssuerInstitucion()){
			issuerCode = userInfo.getUserAccountSession().getIssuerCode(); 
		}
	}



	/**
	 * Load holidays.
	 */
	private void loadHolidays() {
		// TODO Auto-generated method stub
		 Holiday holidayFilter = new Holiday();
		 holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		 GeographicLocation countryFilter = new GeographicLocation();
		 countryFilter.setIdGeographicLocationPk(countryResidence);
		 holidayFilter.setGeographicLocation(countryFilter);
		 try {
			allHolidays = CommonsUtilities.convertListDateToString(generalParameterFacade.getListHolidayDateServiceFacade(holidayFilter));
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Instantiates a new placement segment mgmt bean.
	 */
	public PlacementSegmentMgmtBean() {
		super();
	}
	
	/**
	 * Registry placementent segment request handler.
	 */
	public void registryPlacementSegmentRequestHandler(){
		executeAction();
		hideLocalComponents();
		participantList =new ArrayList<>(participantListTotal);
		participantListModel = new DualListModel<Participant>(this.getParticipantList(),new ArrayList<Participant>());
		setViewOperationType(ViewOperationsType.REGISTER.getCode());
		loadPlacementSession();
		issuanceRequest = new Issuance();
		issuerRequest.setHolder(new Holder());
		placementSegParticipant=new Participant();
	}
	
	/**
	 * Test batch process.
	 */
	@LoggerAuditWeb
	public void testBatchProcess(){
		hideLocalComponents();
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.PLACEMENT_SEGMENT_OPEN.getCode());
		try {
			batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(), businessProcess, new HashMap<String, Object>());
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		JSFUtilities.executeJavascriptFunction("PF('batchWOk').show()");
	}
	
	/**
	 * Load placement session.
	 */
	private void loadPlacementSession(){
		placementSegmentSession = new PlacementSegment();
		placementSegmentSession.setBeginingDate(CommonsUtilities.currentDateTime());
//		placementSegmentSession.setClosingDate(CommonsUtilities.currentDateTime());
		placementSegmentSession.setIssuance(new Issuance());
		placementSegmentSession.getIssuance().setIssuer(new Issuer());
		placementSegmentSession.setBalanceToConfirm(BigDecimal.ZERO);
		issuerRequest = new Issuer();
		if(issuerCode!=null){
			issuerRequest.setIdIssuerPk(issuerCode);
		}
		lastPlacSegment = null;
	}
	
	/**
	 * Load placement after issuer.
	 */
	public void loadPlacementAfterIssuer(){
		Issuer issuer = placementSegmentSession.getIssuance().getIssuer();
		placementSegmentSession = new PlacementSegment();
		placementSegmentSession.setBeginingDate(CommonsUtilities.currentDateTime());
//		placementSegmentSession.setClosingDate(CommonsUtilities.currentDateTime());
		placementSegmentSession.setIssuance(new Issuance());
		placementSegmentSession.getIssuance().setIssuer(issuer);
		lastPlacSegment = null;
	}
	
	/**
	 * Search issuer request search.
	 */
	public void searchIssuerRequestSearch(){
		if(Validations.validateIsNotNullAndNotEmpty(issuerRequestSearch.getIdIssuerPk()))
			placementSegmentTO.setIdIssuerPk(issuerRequestSearch.getIdIssuerPk());			
		else placementSegmentTO.setIdIssuerPk(null);
		cleanDataModel();			
	}
	/**
	 * Search issuer request handler.
	 */
	public void searchIssuerRequestHandler(){
		executeAction();	
		hideLocalComponents();
		if(Validations.validateIsNotNullAndNotEmpty(issuerRequest.getIdIssuerPk()))	
		{			
			String idIssuerCode = this.issuerRequest.getIdIssuerPk();
			IssuerTO issuerTO = new IssuerTO();
			issuerTO.setIdIssuerPk(idIssuerCode);
			Issuer issuerTemp = null;
			
			if(issuerTO.getIdIssuerPk()!=null && issuerTO.getIdIssuerPk().length()!=0){
				try {
					issuerTemp = issuanceSecuritiesServiceFacade.findIssuerServiceFacade(issuerTO);
				} catch (ServiceException e) {
					e.printStackTrace();
				}
			}
			
			if(issuerTemp == null){
				return;
			}
			if(!issuerTemp.getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					      , PropertiesUtilities.getMessage(PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_ISSUER_STATE,
					    		  new Object[]{issuerTemp.getMnemonic()}) );						
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgValidation').show()");
				loadPlacementSession();
				return;
			}
			Holder holderTemp = issuerTemp.getHolder();
			if(Validations.validateIsNull(holderTemp)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					      , PropertiesUtilities.getMessage(PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_HOLDER_NOT_EXIST) );					
				placementSegmentTO.setIdIssuancePk(null);				
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgValidation').show()");
				loadPlacementSession();		
				return;
			}else if(holderTemp!=null && !holderTemp.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					      , PropertiesUtilities.getMessage(PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_HOLDER,
					    		  new Object[]{holderTemp.getIdHolderPk().toString()}) );					
				placementSegmentTO.setIdIssuancePk(null);				
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgValidation').show()");
				loadPlacementSession();		
				return;
			}
			placementSegmentSession.getIssuance().setIssuer(issuerTemp);
			issuerRequest = issuerTemp;
		}
	}
	
	/**
	 * Search issuance request handler.
	 *
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void searchIssuanceRequestHandler() throws ServiceException{
		if(placementSegmentSession.getIssuance()==null){
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_ISSUANCE_EMPTY,new Object[]{this.getPlacementSegmentTO().getIdIssuancePk()});
			loadPlacementAfterIssuer();
			JSFUtilities.showRequiredDialog();
			return;
		}else if(placementSegmentSession.getIssuance().getIdIssuanceCodePk()==null){
			if(isViewOperationRegister()){//Si se esta registrando el tramo
				loadPlacementAfterIssuer();
				JSFUtilities.resetComponent(":frmSegmentPlacementSearch:pnlIssuanceData");
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_ISSUANCE_EMPTY,new Object[]{this.getPlacementSegmentTO().getIdIssuancePk()});
				JSFUtilities.showSimpleValidationDialog();
				return ;//Cuando se ejecuta el onblur en la caja de texto, sin ningun valor
			}
		}
		IssuanceTO issuanceTO = IssuanceTO.getInstanceFromIssuancePK(placementSegmentSession.getIssuance().getIdIssuanceCodePk());//Traemos la instancia desde el Pk
		issuanceTO.setNeedSecurities(BooleanType.YES.getBooleanValue());
		issuanceTO.setNeedPlaceSegments(BooleanType.YES.getBooleanValue());
		Issuance issuanceTemp = null;
		try {
			issuanceTemp = issuanceSecuritiesServiceFacade.findIssuanceServiceFacade(issuanceTO);
		//	issuanceTemp.setIssuer(issuerRequest);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		if(issuanceTemp == null){
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_ISSUANCE_EMPTY,new Object[]{this.getPlacementSegmentTO().getIdIssuancePk()});
			loadPlacementAfterIssuer();
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		if(!issuanceTemp.isPrimaryPlacementNegotiation()){
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_ISSUANCE_NOT_PLACEMENT,null);
			loadPlacementAfterIssuer();
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		if(issuanceTemp.getRegisteredTranches().compareTo( issuanceTemp.getNumberTotalTranches() )==0 ||
		   issuanceTemp.getConfirmedTranches().compareTo( issuanceTemp.getNumberTotalTranches() )==0){
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_PLACEMENTS_TRANCES_COMPLETE,null);
			loadPlacementAfterIssuer();
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		//Si la emision tiene data inconsistente, mostrar mensaje
		if(issuanceTemp.getNumberTotalTranches()==null || issuanceTemp.getAmountConfirmedSegments()==null || issuanceTemp.getAmountOpenedSegments()==null){
			showMessageOnDialogOnlyKey(PropertiesConstants.LBL_HEADER_ALERT, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_ISSUANCE_INCONSITENT_DATA);
			loadPlacementAfterIssuer();
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		//Validate state of Issuance
		if(!issuanceTemp.getStateIssuance().equals(IssuanceStateType.AUTHORIZED.getCode())){
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_ISSUANCE_EXPIRED,null);
			loadPlacementAfterIssuer();
			JSFUtilities.showSimpleValidationDialog();
			return;
		}else if(issuanceTemp.getStateIssuance().equals(IssuanceStateType.SUSPENDED.getCode())){
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_ISSUANCE_SUSPEND,new Object[]{placementSegmentTO.getIdIssuancePk()});
			loadPlacementAfterIssuer();
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		//END - Validate state of Issuance
		

		if(issuanceTemp.getStateIssuance().equals(IssuanceStateType.AUTHORIZED.getCode()) && isViewOperationRegister()){
			PlacementSegmentTO placementSegmentTO = new PlacementSegmentTO();
			placementSegmentTO.setIdIssuancePk(issuanceTemp.getIdIssuanceCodePk());
			
			IssuerTO issuerTO=new IssuerTO();
			issuerTO.setIdIssuerPk( issuanceTemp.getIssuer().getIdIssuerPk() );
			Issuer issuerTemp = issuanceSecuritiesServiceFacade.findIssuerServiceFacade(issuerTO);
			setIssuerRequest(issuerTemp);
//			PlacementSegment lastPlacementSegment = null;
			
			placementSegmentSession.getIssuance().setIssuer(issuerTemp);
			
			List<PlacementSegment> placSegmentOnIssuance = null;
			try {
				placSegmentOnIssuance = placementSegmentServiceFacade.getLastPlacementSegmentServiceFacade(placementSegmentTO);
				if(placSegmentOnIssuance!=null && !placSegmentOnIssuance.isEmpty()){
					for(PlacementSegment placSeg : placSegmentOnIssuance){
						if(placSeg.getTrancheNumber()!=null){
							this.lastPlacSegment = placSeg;
							break;
						}
					}					
				}
			} catch (ServiceException e1) {
				e1.printStackTrace();
			}
			/*
			List<PlacementSegmentStateType> placStateTypeList = PlacementSegmentStateType.listSomeElements(REGISTERED,CONFIRMED);
			if(lastPlacSegment!=null && placStateTypeList.contains(lookup.get(lastPlacSegment.getPlacementSegmentState()))){
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_NOT_OPENED,null);
				loadPlacementAfterIssuer();
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			 */
			if(lastPlacSegment!=null && lastPlacSegment.getTrancheNumber().equals(placementSegmentSession.getIssuance().getNumberTotalTranches())){
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_PLACEMENTNUMBER,null);
				loadPlacementAfterIssuer();
				JSFUtilities.showSimpleValidationDialog();
				return;
			}

			SecurityTO securityTO = new SecurityTO();
			securityTO.setIdIssuanceCodePk(issuanceTemp.getIdIssuanceCodePk());
			securityTO.setSecurityState(SecurityStateType.REGISTERED.getCode().toString());
			try {
				securityList = securityServiceFacade.findSecuritiesServiceFacade(securityTO);
			} catch (ServiceException e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
				e.printStackTrace();
			}
			
			if(securityList==null || securityList.isEmpty()){
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_ISSUANCE_SECURITIES_EMPTY,new Object[]{issuanceTemp.getIdIssuanceCodePk()});
				loadPlacementAfterIssuer();
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			//Issue 891 validando que los valores esten autorizados
			for(Security security: securityList){
				if (Validations.validateIsNotNull(security.getIndAuthorized())) {
					if (security.getIndAuthorized().equals(BooleanType.NO.getCode())) {
						showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null,
								PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_SECURITIES_AUTHORIZED_SECURITY,
								new Object[]{security.getIdSecurityCodePk()});
						loadPlacementAfterIssuer();
						JSFUtilities.showSimpleValidationDialog();
						return;
					}
				}
			}
			
			BigDecimal sum=BigDecimal.ZERO;
			for(Security sec : securityList){
				sum=sum.add( sec.getDesmaterializedBalance() );
			}
			issuanceTemp.setPlacedSecuritiesBalance( sum.intValue() );
			try {
				placementSegmentSession = placementSegmentServiceFacade.setPlacementDetailsFromSecuritiesServiceFacade(placementSegmentSession, securityList);
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		placementSegmentSession.setIssuance(issuanceTemp);//put issuance searched from database
		placementSegmentSession.setBeginingDate(getInitialDate());//Put initial date of PlacSegment according with the issuance entered
//		placementSegmentSession.setClosingDate(CommonsUtilities.addDate(this.getInitialDate(), 1));
		placementSegmentSession.setPlacementSegmentType(issuanceTemp.getPlacementType());;//Heredamos el tipo de colocacion de la emision, siempre es el mismo
		
		Integer numberPlacements = issuanceTemp.getNumberTotalTranches();
		BigDecimal issuanceAmount= issuanceTemp.getIssuanceAmount();
		if(numberPlacements!=null && numberPlacements.equals(BigDecimal.ONE.intValue())){
			placementSegmentSession.setAmountToPlace(issuanceAmount);
			evaluateAmountOnPlacementHanlder(issuanceAmount);
		}
		
		/**
		 *  Issue 2503
		 */
		List<PlacementSegment> listPlacement=null;
		PlacementSegmentTO placementSegmentTO = new PlacementSegmentTO();
		placementSegmentTO.setIdIssuancePk(placementSegmentSession.getIssuance().getIdIssuanceCodePk());
		placementSegmentTO.setIdPlacementeSegmentRefPk(null);
		try{
			/**Get List To Placement Segment To Data Base*/
			listPlacement=placementSegmentServiceFacade.getListPlacementSegmentServiceFacade(placementSegmentTO, parameterTableMap);	
		}catch(Exception e){
			e.printStackTrace();
		}
		
		openingAmount =BigDecimal.ZERO;
		BigDecimal amountPlaceToSegments=BigDecimal.ZERO;
		if(Validations.validateListIsNotNullAndNotEmpty(listPlacement)){
			for (PlacementSegment placementSegment : listPlacement) {
				amountPlaceToSegments=CommonsUtilities.addTwoDecimal(amountPlaceToSegments, placementSegment.getAmountToPlace(), 4);
			}
		}
		/**Opening Amount is Amount Issuance sub Amount To Placement Segment*/
		openingAmount=CommonsUtilities.subtractTwoDecimal(issuanceAmount, amountPlaceToSegments, 8);
		
	}
	
	/**
	 * Evaluate amount on placement hanlder.
	 *
	 * @param amountPlaced the amount placed
	 */
	public void evaluateAmountOnPlacementHanlder(BigDecimal amountPlaced){
		Integer lastPlacementSegmentNumber = lastPlacSegment!=null?lastPlacSegment.getTrancheNumber()!=null?lastPlacSegment.getTrancheNumber():0:0;
		Integer numberTotalPlacSegment = placementSegmentSession.getIssuance().getNumberTotalTranches();
		boolean isLastPlacSegment = ((numberTotalPlacSegment - 1) == lastPlacementSegmentNumber);
		if(amountPlaced==null || amountPlaced.intValue() == BigDecimal.ZERO.intValue()){
			this.placementSegmentSession.setAmountToPlace(null);
			return;
		}
		BigDecimal amountAvailable = placementSegmentSession.getIssuance().getAmountAvailable();
		if(amountAvailable.subtract(amountPlaced).equals(BigDecimal.ZERO) && !isLastPlacSegment){
			this.placementSegmentSession.setAmountToPlace(null);
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_PLACEMENT_AMOUNT_ALL,null);
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		if(amountAvailable.compareTo(amountPlaced) == BigDecimal.ONE.negate().intValue()){
			this.placementSegmentSession.setAmountToPlace(null);
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_PLACEMENT_AMOUNT,null);
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		BigDecimal amountFixed = placementSegmentSession.getPlacedAmountFixedFromDetail();
		if(amountFixed!=null && amountPlaced.compareTo(amountFixed) == BigDecimal.ONE.negate().intValue()){
			BigDecimal amountPlacedTemp = placementSegmentSession.getAmountToPlaceTemp();
			placementSegmentSession.setAmountToPlace(amountPlacedTemp);
			placementSegmentSession.setAmountToPlaceTemp(amountPlaced);
			JSFUtilities.executeJavascriptFunction("PF('cnfwFormCleanData').show()");
 			return;
		}
		placementSegmentSession.setAmountToPlaceTemp(amountPlaced);
	}
	
	/**
	 * Clean amounts on plac segm detail hanlder.
	 */
	public void cleanAmountsOnPlacSegmDetailHanlder(){
		try {
			placementSegmentSession.setPlacementSegmentDetails(null);//clean detail, and then values will be set
			placementSegmentSession = placementSegmentServiceFacade.setPlacementDetailsFromSecuritiesServiceFacade(placementSegmentSession, securityList);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		BigDecimal amountToPlaced = placementSegmentSession.getAmountToPlaceTemp();
		placementSegmentSession.setAmountToPlace(amountToPlaced);
		evaluateAmountOnPlacementHanlder(amountToPlaced);
		JSFUtilities.executeJavascriptFunction("PF('cnfwFormCleanData').hide()");
	}
	
	/**
	 * Not clean amount on plac seg det handler.
	 */
	public void notCleanAmountOnPlacSegDetHandler(){
		BigDecimal amountToPlaced = placementSegmentSession.getAmountToPlace();
		placementSegmentSession.setAmountToPlaceTemp(amountToPlaced);
	}
	
	/**
	 * Evaluate placed amount handler.
	 *
	 * @param placementSegmentDetail the placement segment detail
	 * @param lstPlacementSegmentDetail the lst placement segment detail
	 */
	public void evaluatePlacedAmountHandler(PlacementSegmentDetail placementSegmentDetail,List <PlacementSegmentDetail> lstPlacementSegmentDetail){
		executeAction();
		if (Validations.validateIsNotNull(placementSegmentDetail.getPlacedAmount())) {
			placementSegmentDetail.setPlacedAmount(placementSegmentDetail.getPlacedAmount().setScale(2, RoundingMode.HALF_UP));
			BigDecimal placedAmount = placementSegmentDetail.getPlacedAmount();//get placed amount on placement segment detail
			if(placedAmount!=null){
				BigDecimal amountAvailableOnPlacSeg = this.getPlacementSegmentSession().getAmountToPlace().subtract(this.getPlacementSegmentSession().getPlacedAmountFixedFromDetail().subtract(placementSegmentDetail.getPlacedAmount()));
				BigDecimal valNominalSecurity = placementSegmentDetail.getSecurity().getCurrentNominalValue();//get nominal value on security
				if(placedAmount.equals(BigDecimal.ZERO)){
					showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_SECURITIES_AMMOUNT_ZERO,null);
					placementSegmentDetail.setPlacedAmount(null);
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
				if(amountAvailableOnPlacSeg.compareTo(placementSegmentDetail.getPlacedAmount()) == BigDecimal.ONE.negate().intValue()){
					showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_SECURITIES_AMMOUNT,null);
					placementSegmentDetail.setPlacedAmount(null);
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
				if(!CommonsUtilities.isMultipleOf(placedAmount,valNominalSecurity)){
					showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_SECURITIES_AMMOUNT_NOMINALVAL,null);
					placementSegmentDetail.setPlacedAmount(null);
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
				if(amountAvailableOnPlacSeg.subtract(placedAmount).equals(BigDecimal.ZERO) && placSegmHasDetailSelected(placementSegmentSession)){
					showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_SECURITIES_AMMOUNT,null);
					placementSegmentDetail.setPlacedAmount(null);
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
				evaluatePlacedAmountHandlerFloating(placementSegmentDetail,lstPlacementSegmentDetail);
				placementSegmentDetail.setSecurityBalance( placementSegmentDetail.getPlacedAmount().divide( placementSegmentDetail.getSecurity().getCurrentNominalValue() )  );
			}
		}
	}
	
	/**
	 * Insert security balance.
	 *
	 * @param placementSegmentDetail the placement segment detail
	 * @param lstPlacementSegmentDetail the lst placement segment detail
	 */
	public void insertSecurityBalance(PlacementSegmentDetail placementSegmentDetail,List <PlacementSegmentDetail> lstPlacementSegmentDetail){
		try {
			if(Validations.validateIsNotNull(placementSegmentDetail.getSecurityBalance())){
				BigDecimal placedAmount=placementSegmentDetail.getSecurity().getCurrentNominalValue().multiply( placementSegmentDetail.getSecurityBalance() );
				placementSegmentDetail.setPlacedAmount(placedAmount);
				evaluatePlacedAmountHandler(placementSegmentDetail,lstPlacementSegmentDetail);
			}

		} catch (Exception e) {
			excepcion.fire( new ExceptionToCatchEvent(e) );
		}
	}
	
	/**
	 * Evaluate placed amount handler type floating.
	 *
	 * @param placementSegmentDetailAux the placement segment detail aux
	 * @param lstPlacementSegmentDetail the lst placement segment detail
	 * @return the boolean
	 */
	public Boolean evaluatePlacedAmountHandlerFloating(PlacementSegmentDetail placementSegmentDetailAux,List <PlacementSegmentDetail> lstPlacementSegmentDetail)
	{
		executeAction();
		BigDecimal amountAvailablefloating = this.getPlacementSegmentSession().getAmountToPlace().subtract(this.getPlacementSegmentSession().getPlacedAmountFixedFromDetail());
		BigDecimal amountMinForValNominal = new BigDecimal(0);
		for(PlacementSegmentDetail placementSegmentDetail :  lstPlacementSegmentDetail)
		{
			if(placementSegmentDetail.isSelected() && Validations.validateIsNotNullAndNotEmpty(placementSegmentDetail.getPlacementSegmentDetType())
					&& placementSegmentDetail.getPlacementSegmentDetType().equals(codeFloatingSecurityPlacementType()))
			{
				amountMinForValNominal=amountMinForValNominal.add(placementSegmentDetail.getSecurity().getCurrentNominalValue());
			}
		}
		if(amountMinForValNominal.compareTo(amountAvailablefloating)==1)
		{
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_SECURITIES_AMMOUNT,null);
			if(placementSegmentDetailAux.getPlacementSegmentDetType().equals(codeFloatingSecurityPlacementType())){
				placementSegmentDetailAux.setPlacementSegmentDetType(codeFixedSecurityPlacementType());
			}
			if(placementSegmentDetailAux.getPlacementSegmentDetType().equals(codeFixedSecurityPlacementType())){
				placementSegmentDetailAux.setPlacedAmount(null);
			}
			JSFUtilities.showSimpleValidationDialog();
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Change period dates.
	 */
	public void changePeriodDates(){
		try {
			placementSegmentSession.setPlacementePeriod(null);
			if(placementSegmentSession.getBeginingDate()!=null && placementSegmentSession.getClosingDate()!=null){
				placementSegmentSession.setPlacementePeriod( 
						CommonsUtilities.getDaysBetween(placementSegmentSession.getBeginingDate(), placementSegmentSession.getClosingDate()) );
			}
		} catch (Exception e) {
			excepcion.fire( new ExceptionToCatchEvent(e) );
		}
	}
	
	/**
	 * Insert placement period days.
	 */
	public void insertPlacementPeriodDays(){
		try {
			if(placementSegmentSession.getBeginingDate()!=null && placementSegmentSession.getPlacementePeriod()!=null){
				placementSegmentSession.setClosingDate( 
						CommonsUtilities.addDaysToDate(placementSegmentSession.getBeginingDate(), placementSegmentSession.getPlacementePeriod() ) );
			}
		} catch (Exception e) {
			excepcion.fire( new ExceptionToCatchEvent(e) );
		}
	}
	
	/**
	 * Checks if is have plac seg detail not selected.
	 * method to know if placementSegment has Details selected and not placed Amount entered
	 * @param placementSegment the placement segment
	 * @return the boolean
	 */
	public Boolean placSegmHasDetailSelected(PlacementSegment placementSegment){
		for(PlacementSegmentDetail placementSegmentDetail: placementSegment.getPlacementSegmentDetails()){
			if(placementSegmentDetail.isSelected() && placementSegmentDetail.getPlacedAmount()==null){
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Gets the some security placement type list.
	 *
	 * @return the some security placement type list
	 */
	public List<SecurityPlacementType> getSomeSecurityPlacementTypeList(){
		executeAction();
		return SecurityPlacementType.listSomeElements(SecurityPlacementType.FIXED,SecurityPlacementType.FLOATING);
	}
	
	/**
	 * Select placement segment detail handler.
	 *
	 * @param placementSegmentDetail the placement segment detail
	 */
	public void selectPlacementSegmentDetailHandler(PlacementSegmentDetail placementSegmentDetail){
//		try {
			if(placementSegmentDetail.isSelected()){
				
			if(Validations.validateListIsNullOrEmpty(getParticipantListModel().getTarget())) {
				placementSegmentDetail.setSelected(false);
				return;
			}	
//			if(!Validations.validateIsNotNullAndPositive(placementSegParticipant.getIdParticipantPk())){
//				placementSegParticipant.setIdParticipantPk(null);
//				Object[] parameters = {PropertiesUtilities.getMessage("placementsegment.lbl.participant.structing")};
//				String strMsg =  PropertiesUtilities.getMessage(GeneralPropertiesConstants.PROPERTYFILE,
//						GeneralPropertiesConstants.ERROR_COMBO_REQUIRED, parameters);
//				
//				JSFUtilities.addContextMessage("frmSegmentPlacementSearch:cboParticipant", 
//						FacesMessage.SEVERITY_ERROR, strMsg,strMsg);
//				placementSegmentDetail.setSelected(false);
//				return;
//			}
			
			List<Integer> lstStates=new ArrayList<Integer>();
			lstStates.add( PlacementSegmentStateType.OPENED.getCode() );
			lstStates.add( PlacementSegmentStateType.CONFIRMED.getCode() );
			lstStates.add( PlacementSegmentStateType.REGISTERED.getCode() );
			lstStates.add( PlacementSegmentStateType.SUSPENDED.getCode() );
			
			Map<String,Object> params = new HashMap<String, Object>();
			params.put("idIssuancePkPrm", placementSegmentSession.getIssuance().getIdIssuanceCodePk());
			params.put("idSecurityCodePrm", placementSegmentDetail.getSecurity().getIdSecurityCodePk());
			params.put("statePrm", lstStates);
			
			for(Participant participantModel: getParticipantListModel().getTarget()){
				params.put("idParticipantPrm", participantModel.getIdParticipantPk());
//				if(placementSegmentServiceFacade.isExistsSegmentPaticipantSecurities(params)){
//					placementSegmentDetail.setSelected(false);
//					Object[] arg={placementSegParticipant.getIdParticipantPk(),placementSegmentDetail.getSecurity().getIdSecurityCodePk()};
//					String message=PropertiesUtilities.getExceptionMessage(ErrorServiceType.PLACEMENT_SEGMENT_PART_SEC_OPENED.getMessage(), arg);
//					String header=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT);
//					JSFUtilities.showMessageOnDialog(header, message);
//					JSFUtilities.showSimpleValidationDialog();
//					return;
//				}else{			
					placementSegmentDetail.setPlacedAmount(null);
					placementSegmentDetail.setRequestAmount(BigDecimal.ZERO);
					if(placementSegmentDetail.getPlacementSegmentDetType()!=null && this.placementSegmentSession.getIssuance().getPlacementType().equals(SecurityPlacementType.MIXED.getCode())){
						placementSegmentDetail.setPlacementSegmentDetType(null);
						
					}	
//				}
			}
		}	
//		} catch (ServiceException e) {
//			excepcion.fire( new ExceptionToCatchEvent(e) );
//		}
	}
	
	/**
	 * On transfer.
	 *
	 * @param event the event
	 */
	public void onTransfer(TransferEvent event) {
		System.out.print("---- >");
	}
	
	/**
	 * Before save placement segment hanlder.
	 */
	public void beforeSavePlacementSegmentHanlder(){
		Boolean haveSecuritiesSelected = Boolean.FALSE;
		executeAction();
		/*
		if(this.getParticipantListModel().getTarget().isEmpty()){
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_PARTICIPANT_STRUCTING,null);
			JSFUtilities.executeJavascriptFunction("cnfwMsgCustomValidationSec.show()");
			return;
		}
		*/
		if(this.placementSegmentSession.getAmountToPlace().equals(BigDecimal.ZERO)){
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_AMOUNTTOPLACE_ZERO,null);
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		for(PlacementSegmentDetail placementSegmentDetail: this.getPlacementSegmentSession().getPlacementSegmentDetails()){
			if(placementSegmentDetail.isSelected()){
				if(placementSegmentDetail.getPlacementSegmentDetType() == null){
					showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_SECURITY_ISIN,new Object[]{placementSegmentDetail.getSecurity().getIdIsinCode()});
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
				SecurityPlacementType placSegType = SecurityPlacementType.lookup.get(placementSegmentDetail.getPlacementSegmentDetType());
				BigDecimal placedAmount = placementSegmentDetail.getPlacedAmount();
				if(placSegType.equals(SecurityPlacementType.FIXED) && (placedAmount==null || placedAmount.equals(BigDecimal.ZERO))){
					showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_SECURITY_ISIN_NOTSELECTED,null);
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
				if (IssuanceType.MIXED.getCode().equals(placementSegmentSession.getIssuance().getIssuanceType())) {
					BigDecimal ResultAmount= placementSegmentDetail.getSecurity().getCirculationAmount().subtract(placedAmount);
					if (BigDecimal.ZERO.compareTo(ResultAmount) > 0) {
						Object[] params= new Object[2];
						params[0]= placementSegmentDetail.getSecurity().getIdSecurityCodePk();
						params[1]= placementSegmentDetail.getSecurity().getCirculationAmount();
						showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_NEGATIVE_CIRCULATION_AMOUNT, params);
						JSFUtilities.showSimpleValidationDialog();
						return;
					}
				}
				
				haveSecuritiesSelected = Boolean.TRUE;
			}
		}
		if(!haveSecuritiesSelected){
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_SECURITIES_EMPTY,null);
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		if(placementSegmentSession.getIssuance().getPlacementType().equals(SecurityPlacementType.FIXED.getCode())){
			BigDecimal amountFixedBussy = placementSegmentSession.getPlacedAmountFixedFromDetail();
			BigDecimal amountAvailable = placementSegmentSession.getAmountToPlace().subtract(amountFixedBussy);
			if(amountAvailable.compareTo(BigDecimal.ZERO) != 0){
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_AMOUNTAVAILABLE,null);
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
		}
		if(placementSegmentSession.getIssuance().getPlacementType().equals(SecurityPlacementType.MIXED.getCode())){
			BigDecimal amountFloatingBussy = placementSegmentSession.getPlacedAmountFloatingFromDetail()==null?BigDecimal.ZERO:placementSegmentSession.getPlacedAmountFloatingFromDetail();
			BigDecimal amountFixedBussy = placementSegmentSession.getPlacedAmountFixedFromDetail();
			BigDecimal amountBussy = amountFixedBussy.add(amountFloatingBussy);
			BigDecimal amountAvailable = placementSegmentSession.getAmountToPlace().subtract(amountBussy); 
			if(placementSegmentSession.getAmountToPlace().compareTo(amountBussy)!= BigDecimal.ZERO.intValue()){
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_AMOUNTAVAILABLE,new Object[]{amountAvailable});
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			Integer lastPlacementSegmentNumber = lastPlacSegment!=null?lastPlacSegment.getTrancheNumber()!=null?lastPlacSegment.getTrancheNumber():0:0;
			Integer numberTotalPlacSegment = placementSegmentSession.getIssuance().getNumberTotalTranches();
			boolean isLastPlacSegment = ((numberTotalPlacSegment - 1) == lastPlacementSegmentNumber);
			if(lastPlacSegment!=null && isLastPlacSegment){
				Boolean haveSecFloat = Boolean.FALSE, haveSecFixed = Boolean.FALSE;
				PlacementSegmentTO placementSegmentTO = new PlacementSegmentTO();
				placementSegmentTO.setIdIssuancePk(placementSegmentSession.getIssuance().getIdIssuanceCodePk());
				try {
					placementSegmentTO.setPlacSegDetailType(SecurityPlacementType.FLOATING.getCode());
					haveSecFloat = placementSegmentServiceFacade.verifiedIssuanceMixteServiceFacade(placementSegmentTO,placementSegmentSession);
					placementSegmentTO.setPlacSegDetailType(SecurityPlacementType.FIXED.getCode());
					haveSecFixed = placementSegmentServiceFacade.verifiedIssuanceMixteServiceFacade(placementSegmentTO,placementSegmentSession);
				} catch (ServiceException e) {
					e.printStackTrace();
				}
				if(!haveSecFloat && (amountFloatingBussy==null || amountFloatingBussy.equals(BigDecimal.ZERO))){
					showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_SECURITIES_AMMOUNT_FLOATING,null);
					JSFUtilities.showSimpleValidationDialog();
					return;
				}else if(!haveSecFixed && (amountFixedBussy==null || amountFixedBussy.equals(BigDecimal.ZERO))){
					showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_SECURITIES_AMMOUNT_FIXED,null);
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
			}
		}
		if (Validations.validateIsNotNull(placementSegmentSession.getClosingDate())) {
			if(Validations.validateIsNotNullAndNotEmpty(placementSegmentSession.getPlacementePeriod())){
				placementSegmentSession.setPlacementTerm(new BigDecimal(placementSegmentSession.getPlacementePeriod()));
			}
		} else {
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_CLOSING_DATE_NO_ENTERED,null);
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		if(isViewOperationRegister()){
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_REGISTER, null, PropertiesConstants.PLACEMENTESEGMENT_MESSAGE_REGISTER, null);
		}else{
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_REGISTER, null, PropertiesConstants.PLACEMENTESEGMENT_MESSAGE_MODIFY, new Object[]{placementSegmentSession.getIssuance().getIdIssuanceCodePk()});
		}
		JSFUtilities.executeJavascriptFunction("PF('cnfwFormMgmtRegister').show()");
	}
	
	/**
	 * Save placement segment handler.
	 */
	@LoggerAuditWeb
	public void savePlacementSegmentHandler(){
		placementSegmentSession.setRegistryDate(CommonsUtilities.currentDate());
		placementSegmentSession.setRegistryUser(userInfo.getUserAccountSession().getUserName());
		placementSegmentSession.setPlacementSegParticipaStructs(new ArrayList<PlacementSegParticipaStruct>());
		placementSegmentSession.setPlacementSegmentState(PlacementSegmentStateType.REGISTERED.getCode());
		
		for(Participant participantModel: getParticipantListModel().getTarget()){
		
			PlacementSegParticipaStruct placementSegParticipaStruct = new PlacementSegParticipaStruct();
			placementSegParticipaStruct.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			placementSegParticipaStruct.setRegistryDate(CommonsUtilities.currentDate());
			placementSegParticipaStruct.setParticipant(participantModel);
			placementSegParticipaStruct.setStatePlacSegParticipant(PlacementSegmentStructStateType.REGISTERED.getCode());
			placementSegParticipaStruct.setPlacementSegment(placementSegmentSession);
			placementSegmentSession.getPlacementSegParticipaStructs().add(placementSegParticipaStruct);
		}
		List<PlacementSegmentDetail> placementSegmentDetailListTemp = new ArrayList<PlacementSegmentDetail>();
		for(PlacementSegmentDetail placementSegmentDetail: getPlacementSegmentSession().getPlacementSegmentDetails()){
			placementSegmentDetail.setBalanceToConfirm(BigDecimal.ZERO);
			placementSegmentDetail.setRequestBalance(BigDecimal.ZERO);
			if(!placementSegmentDetail.isSelected()){
				placementSegmentDetailListTemp.add(placementSegmentDetail);
			}else if(placementSegmentDetail.getPlacementSegmentDetType().equals(SecurityPlacementType.FLOATING.getCode())){
				placementSegmentDetail.setPlacedAmount(BigDecimal.ZERO);//This value can't be Null
			}
		}
		placementSegmentSession.getPlacementSegmentDetails().removeAll(placementSegmentDetailListTemp);
		
		PlacementSegment placementSegment = null ;
		try {
			if(isViewOperationRegister()) {
				placementSegment = placementSegmentServiceFacade.registryPlacementSegmentServiceFacade(placementSegmentSession);
			} else if(isViewOperationModify())
				placementSegment = placementSegmentServiceFacade.modifyPlacementSegmentServiceFacade(placementSegmentSession);
		} catch (ServiceException e) {
			e.printStackTrace();
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
				JSFUtilities.executeJavascriptFunction("PF('formWEndConversationtOk').show()");
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		if(placementSegment!=null){
			if(isViewOperationRegister()){
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_SUCCESS, null, 
						PropertiesConstants.PLACEMENTESEGMENT_MESSAGE_REGISTER_OK,new Object[]{placementSegment.getIssuance().getIdIssuanceCodePk()});
			}else if(isViewOperationModify()){
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_SUCCESS, null, PropertiesConstants.PLACEMENTESEGMENT_MESSAGE_MODIFY_OK,new Object[]{placementSegment.getIssuance().getIdIssuanceCodePk()});
			}
			JSFUtilities.executeJavascriptFunction("PF('formWMgmtOk').show()");
		}
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
	}
	/**
	 * Before confirm handler.
	 */
	public void beforeConfirmHandler(){
		executeAction();
		if(this.getPlacementSegmentSession()==null){
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.ERROR_RECORD_REQUIRED, null);	
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		if(this.getPlacementSegmentSession().getPlacementSegmentState().equals(PlacementSegmentStateType.REGISTERED.getCode())){
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_REGISTER, null, PropertiesConstants.PLACEMENTESEGMENT_MESSAGE_CONFIRM, new Object[]{placementSegmentSession.getIssuance().getIdIssuanceCodePk()});
			this.setViewOperationType(ViewOperationsType.CONFIRM.getCode());
			JSFUtilities.executeJavascriptFunction("PF('dialogwConfirmAccount').show()");
			return;
		}
		showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_CONFIRM, null);
		JSFUtilities.showSimpleValidationDialog();
	}
	
	/**
	 * Before confirm action.
	 *
	 * @return the string
	 */
	public String beforeConfirmAction(){
		PlacementSegment placementSegment = this.getPlacementSegmentSession(); 
		executeAction();
		if(placementSegment==null){
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.ERROR_RECORD_REQUIRED, null);	
			JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		Holder holderTemp = placementSegment.getIssuance().getIssuer().getHolder();
		if(Validations.validateIsNull(holderTemp)){				
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_HOLDER_NOT_EXIST, null);	
			JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		if(placementSegment.getPlacementSegmentState().equals(PlacementSegmentStateType.REGISTERED.getCode())){			
			selectPlacementSegmentHandler(placementSegment);
			this.setViewOperationType(ViewOperationsType.CONFIRM.getCode());
			return "placementSegmentDetailMgmtRule";
		}
		showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_CONFIRM, null);
		JSFUtilities.showSimpleValidationDialog();
		return "";
	}
	/**
	 * Before suspend action.
	 *
	 * @return the string
	 */
	public String beforeSuspendAction(){
		PlacementSegment placementSegment = this.getPlacementSegmentSession(); 
		executeAction();
		if(placementSegment==null){
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.ERROR_RECORD_REQUIRED, null);	
			JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		if(placementSegment.getPlacementSegmentState().equals(PlacementSegmentStateType.OPENED.getCode())){			
			selectPlacementSegmentHandler(placementSegment);
			this.setViewOperationType(ViewOperationsType.BLOCK.getCode());
			return "placementSegmentDetailMgmtRule";
		}
		showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_SUSPEND, null);
		JSFUtilities.showSimpleValidationDialog();
		return "";
	}
	/**
	 * Before suspend up action.
	 *
	 * @return the string
	 */
	public String beforeSuspendUpAction(){
		PlacementSegment placementSegment = this.getPlacementSegmentSession(); 
		executeAction();
		if(placementSegment==null){
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.ERROR_RECORD_REQUIRED, null);	
			JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		if(placementSegment.getPlacementSegmentState().equals(PlacementSegmentStateType.SUSPENDED.getCode())){			
			selectPlacementSegmentHandler(placementSegment);
			this.setViewOperationType(ViewOperationsType.UNBLOCK.getCode());
			return "placementSegmentDetailMgmtRule";
		}
		showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_SUSPENDUP, null);
		JSFUtilities.showSimpleValidationDialog();
		return "";
	}
	/**
	 * Before suspend handler.
	 */
	public void beforeSuspendHandler(){
		if(this.placementSegmentSession==null){
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.ERROR_RECORD_REQUIRED, null);	
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		if(this.placementSegmentSession.getPlacementSegmentState().equals(PlacementSegmentStateType.OPENED.getCode())){
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_MESSAGE_SUSPEND, new Object[]{placementSegmentSession.getIssuance().getIdIssuanceCodePk()});
			this.setViewOperationType(ViewOperationsType.BLOCK.getCode());
			JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').show()");
			return;
		}
		showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_SUSPEND, null);
		JSFUtilities.showSimpleValidationDialog();
	}
	/**
	 * Before suspend up handler.
	 */
	public void beforeSuspendUpHandler(){
		if(this.placementSegmentSession==null){
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.ERROR_RECORD_REQUIRED, null);	
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		if(this.placementSegmentSession.getPlacementSegmentState().equals(PlacementSegmentStateType.SUSPENDED.getCode())){
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_REGISTER, null, PropertiesConstants.PLACEMENTESEGMENT_MESSAGE_SUSPEND_UP, new Object[]{placementSegmentSession.getIssuance().getIdIssuanceCodePk()});
			this.setViewOperationType(ViewOperationsType.UNBLOCK.getCode());
			JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').show()");
			return;
		}
		showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_SUSPENDUP,null);
		JSFUtilities.showSimpleValidationDialog();
	}
	
	/**
	 * Before reject handler.
	 */
	public void beforeRejectHandler(){
		executeAction();
		if(this.getPlacementSegmentSession()==null){
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.ERROR_RECORD_REQUIRED, null);	
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		if(placementSegmentSession.getPlacementSegmentState().equals(PlacementSegmentStateType.REGISTERED.getCode())){
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_REGISTER, null, PropertiesConstants.PLACEMENTESEGMENT_MESSAGE_REJECT, new Object[]{placementSegmentSession.getIssuance().getIdIssuanceCodePk()});
			setViewOperationType(ViewOperationsType.REJECT.getCode());
			JSFUtilities.executeJavascriptFunction("PF('dialogWMotive').show()");
			return;
		}
		showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_DO_ACTION, new Object[]{getPlacementSegmentSession().getPlacementSegmentStateDescription()});
		JSFUtilities.showSimpleValidationDialog();
	}
	
	/**
	 * Select placement segment detail handler.
	 *
	 * @param placementSegment the placement segment
	 */
	@LoggerAuditWeb
	public void selectPlacementSegmentHandler(PlacementSegment placementSegment){
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		IssuerTO issuerTO = new IssuerTO();
		PlacementSegmentTO placementSegmentTO = new PlacementSegmentTO();
		placementSegmentTO.setIdPlacementeSegmentPk(placementSegment.getIdPlacementSegmentPk());
		placementSegmentTO.setNeedIssuance(Boolean.TRUE);
		placementSegmentTO.setNeedParticipants(Boolean.TRUE);
		placementSegmentTO.setNeedSecurities(Boolean.TRUE);
		placementSegmentTO.setIdIssuancePk( placementSegment.getIssuance().getIdIssuanceCodePk() );
		PlacementSegment placementTemp = null;
		try {
			BigDecimal amountConfirm=BigDecimal.ZERO;
			BigDecimal amountDetailConfirm=BigDecimal.ZERO;
			placementTemp = placementSegmentServiceFacade.getPlacementSegmentServiceBean(placementSegmentTO);
			if(placementTemp!=null){
				// validate if the securities belong to share banking
				Object[] issuerDetail= issuanceSecuritiesServiceFacade.getIssuerDetails(placementSegment.getIssuance().getIdIssuanceCodePk());
				Integer objEconomicActivity = (Integer)(issuerDetail[2] == null ? ComponentConstant.ZERO : issuerDetail[2]);
				Integer objSecurityClass = (Integer)(issuerDetail[3] == null ? ComponentConstant.ZERO : issuerDetail[3]);
				if(ComponentConstant.ONE.equals(objEconomicActivity) && ComponentConstant.ONE.equals(objSecurityClass)){
					placementTemp.setShareBanking(true);
				}
				List<PlacementSegmentDetail> placementSegmentDetails=new ArrayList<PlacementSegmentDetail>();
				for(PlacementSegmentDetail placementSegmentDetail: placementTemp.getPlacementSegmentDetails()){
					if(!placementSegmentDetail.getSecurity().isCoupon() &&
					   !placementSegmentDetail.getSecurity().isDetached()){
						//issue 101
						if( placementSegmentDetail.getSecurity().getCurrentNominalValue()!=BigDecimal.ZERO ){
							//placementSegmentDetail.setSecurityBalance( placementSegmentDetail.getPlacedAmount().divide( placementSegmentDetail.getSecurity().getCurrentNominalValue(),2,RoundingMode.HALF_UP )  );
							//issue 1034
							placementSegmentDetail.setSecurityBalance(CommonsUtilities.divideTwoDecimal(placementSegmentDetail.getPlacedAmount(), placementSegmentDetail.getSecurity().getCurrentNominalValue(), 0));
						}else{
							placementSegmentDetail.setSecurityBalance( placementSegmentDetail.getPlacedAmount().divide( placementSegmentDetail.getSecurity().getInitialNominalValue() )  );
						}
						
						amountDetailConfirm=CommonsUtilities.multiplyDecimals(placementSegmentDetail.getSecurity().getCurrentNominalValue(),placementSegmentDetail.getRequestBalance(), 4);
						amountConfirm=CommonsUtilities.addTwoDecimal(amountConfirm, amountDetailConfirm, 4);
						placementSegmentDetail.setPendingPlacement(placementSegmentDetail.getPlacedAmount().subtract(placementSegmentDetail.getRequestAmount()));
						placementSegmentDetails.add(placementSegmentDetail);
					}
				}
				placementTemp.setPlacementSegmentDetails(placementSegmentDetails);
				placementTemp.setAmountConfirm(amountConfirm);
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(placementTemp)){
				issuerTO.setIdIssuerPk(placementTemp.getIssuance().getIssuer().getIdIssuerPk());
				issuerRequest = issuanceSecuritiesServiceFacade.findIssuerServiceFacade(issuerTO);			
				for(PlacementSegmentDetail placementSegmentDetail: placementTemp.getPlacementSegmentDetails()){
					placementSegmentDetail.setSelected(Boolean.TRUE);
				}
			}
			placementSegParticipant=new Participant();
			
			/**
			 * Issue 2503
			 */
			PlacementSegmentTO placementSegmentTOTmp = new PlacementSegmentTO();
			placementSegmentTOTmp.setIdIssuancePk(placementSegment.getIssuance().getIdIssuanceCodePk() );
			placementSegmentTOTmp.setIdPlacementeSegmentRefPk(placementSegment.getIdPlacementSegmentPk());
			List<PlacementSegment> listPlacement;

			/**Get List To Placement Segment To Data Base*/
			listPlacement = placementSegmentServiceFacade.getListPlacementSegmentServiceFacade(placementSegmentTOTmp, parameterTableMap);

			openingAmount =BigDecimal.ZERO;
			BigDecimal amountPlaceToSegments=BigDecimal.ZERO;
			
			if(Validations.validateListIsNotNullAndNotEmpty(listPlacement)){
				for (PlacementSegment placementSegmentTemp : listPlacement) {
					amountPlaceToSegments=CommonsUtilities.addTwoDecimal(amountPlaceToSegments, placementSegmentTemp.getAmountToPlace(), 4);
				}
			}

			/**Opening Amount is Amount Issuance sub Amount To Placement Segment*/
			openingAmount=CommonsUtilities.subtractTwoDecimal(placementSegment.getIssuance().getIssuanceAmount(), amountPlaceToSegments, 8);
			
			
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		setPlacementSegmentSession(placementTemp);
		getParticipantListModel().setTarget(new ArrayList<>());
//		placementSegParticipant=placementSegmentSession.getPlacementSegParticipaStructs().get(0).getParticipant();
		for(PlacementSegParticipaStruct p:placementSegmentSession.getPlacementSegParticipaStructs()) {
			getParticipantListModel().getTarget().add(p.getParticipant());
		}
		
		placementSegmentSession.setPlacementePeriod( CommonsUtilities.getDaysBetween(placementSegmentSession.getBeginingDate(), placementSegmentSession.getClosingDate()) );
		
		//obteniendo los documentos en caso de la reapertura
		try {
			placementSegmentSession.setListPlacementSegmentHys(placementSegmentServiceFacade.getReopenPlacementSegmentDocumentsServiceFacade(placementSegmentSession));
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Before modify handler.
	 *
	 * @return the string
	 */
	public String beforeModifyHandler(){
		if(placementSegmentSession==null){
			showMessageOnDialogOnlyKey(null, PropertiesConstants.ERROR_RECORD_REQUIRED);	
			JSFUtilities.showSimpleValidationDialog();
			return StringUtils.EMPTY;
		}
		if(placementSegmentSession.getPlacementSegmentState().equals(PlacementSegmentStateType.REGISTERED.getCode())){
			PlacementSegmentTO placementSegmentTO = new PlacementSegmentTO();
			placementSegmentTO.setIdPlacementeSegmentPk(placementSegmentSession.getIdPlacementSegmentPk());
			placementSegmentTO.setNeedIssuance(Boolean.TRUE);
			placementSegmentTO.setNeedParticipants(Boolean.TRUE);
			placementSegmentTO.setNeedSecurities(Boolean.TRUE);
			PlacementSegment placementTemp = null;
			try {
				placementTemp = placementSegmentServiceFacade.getPlacementSegmentServiceBean(placementSegmentTO);
			} catch (Exception e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
			
			SecurityTO securityTO = new SecurityTO();
			securityTO.setIdIssuanceCodePk(placementTemp.getIssuance().getIdIssuanceCodePk());
			List<Security> securityListTemp = null;
			try {
				securityListTemp = securityServiceFacade.findSecuritiesServiceFacade(securityTO);
				placementTemp = placementSegmentServiceFacade.setPlacementDetailsFromSecuritiesServiceFacade(placementTemp,securityListTemp);
			} catch (Exception e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
			setPlacementSegmentSession(placementTemp);
			securityList = securityListTemp; 
			participantListModel.setTarget(placementTemp.getPartListFromPartStruct());
			participantListModel.getSource().removeAll(placementTemp.getPartListFromPartStruct());
			
			jump:
			for(Participant participantTarget: participantListModel.getTarget()){
				for(Participant participant: participantListModel.getSource()){
					if(participantTarget.getIdParticipantPk().equals(participant.getIdParticipantPk())){
						participantListModel.getSource().remove(participant);
						continue jump;
					}
				}
			}
			
			setViewOperationType(ViewOperationsType.MODIFY.getCode());
			return "placementSegmentModifyMgmtRule";
		}else{
			showMessageOnDialogOnlyKey(null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_MODIFY_STATE);
			JSFUtilities.showSimpleValidationDialog();
			return StringUtils.EMPTY;
		}
	}
	
	/**
	 * Change action holder account request handler.
	 */
	@LoggerAuditWeb
	public void processOperationHandler(){
		switch(ViewOperationsType.lookup.get(getViewOperationType())){
			case CONFIRM : confirmPlacementRequest();break;
			case REJECT: rejectPlacementRequest();break;
			case BLOCK: suspendPlacementRequest();break;
			case UNBLOCK: suspendUpPlacementRequest();break;
		}
		searchPlacementSegmentHandler();
	}
	
	/**
	 * Suspend up placement request.
	 */
	@LoggerAuditWeb
	private void suspendUpPlacementRequest() {
		PlacementSegment placementSegment = null;
		try {
			placementSegment = placementSegmentServiceFacade.suspendUpPlacementServiceFacade(placementSegmentSession);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
				JSFUtilities.executeJavascriptFunction("PF('formWEndConversationtOk').show()");
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		if(placementSegment!=null){
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_SUCCESS, null, PropertiesConstants.PLACEMENTESEGMENT_MESSAGE_SUSPEND_UP_OK,new Object[]{placementSegment.getIssuance().getIdIssuanceCodePk()});
			JSFUtilities.executeJavascriptFunction("PF('dialogWConfirmOk').show()");
		}
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
	}

	/**
	 * Suspend placement request.
	 */
	@LoggerAuditWeb
	private void suspendPlacementRequest() {
		PlacementSegment placementSegment = null;
		try {
			placementSegment = placementSegmentServiceFacade.suspendPlacementServiceFacade(placementSegmentSession);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
				JSFUtilities.executeJavascriptFunction("PF('formWEndConversationtOk').show()");
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		if(placementSegment!=null){
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_SUCCESS, null, PropertiesConstants.PLACEMENTESEGMENT_MESSAGE_SUSPEND_OK,new Object[]{placementSegment.getIssuance().getIdIssuanceCodePk()});
			JSFUtilities.executeJavascriptFunction("PF('dialogWConfirmOk').show()");
		}
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
	}

	/**
	 * Reject placement request.
	 */
	@LoggerAuditWeb
	private void rejectPlacementRequest() {
		placementSegmentSession.setPlacementSegmentState(PlacementSegmentStateType.REJECT.getCode());
		placementSegmentSession.setActionMotive(Integer.parseInt(""+motiveController.getIdMotivePK()));
		placementSegmentSession.setOtherActionMotive(motiveController.getMotiveText());
		
		PlacementSegment placementSegment = null;
		try {
			placementSegment = placementSegmentServiceFacade.rejectPlacementSegmentServiceFacade(placementSegmentSession);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
				JSFUtilities.executeJavascriptFunction("PF('formWEndConversationtOk').show()");
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		if(placementSegment!=null){
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_SUCCESS, null, PropertiesConstants.PLACEMENTESEGMENT_MESSAGE_REJECT_OK,new Object[]{placementSegment.getIssuance().getIdIssuanceCodePk()});
			JSFUtilities.executeJavascriptFunction("PF('dialogWConfirmOk').show()");
		}
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
	}

	/**
	 * Confirm placement request.
	 */
	@LoggerAuditWeb
	private void confirmPlacementRequest() {
		PlacementSegment placementSegment = null;
		placementSegmentParticipanStruc = null;
		try {
			placementSegment = placementSegmentServiceFacade.confirmPlacementSegmentServiceFacade(placementSegmentSession);
			placementSegmentParticipanStruc = placementSegment.getPlacementSegParticipaStructs();
			
			Iterator<PlacementSegParticipaStruct> iterator = placementSegmentParticipanStruc.iterator();
			while (iterator.hasNext()) {
				PlacementSegParticipaStruct placementSegParticipaStruct = iterator.next();
				if(Validations.validateIsNullOrEmpty(placementSegParticipaStruct.getDescription())){
					iterator.remove();
				}
			}

		} catch (ServiceException e) {
			e.printStackTrace();
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
				JSFUtilities.executeJavascriptFunction("PF('formWEndConversationtOk').show()");
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		if(placementSegment!=null){
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_SUCCESS,null, PropertiesConstants.PLACEMENTESEGMENT_MESSAGE_CONFIRM_OK,
					new Object[]{placementSegment.getTrancheNumber(),placementSegment.getIssuance().getIdIssuanceCodePk()});
			JSFUtilities.executeJavascriptFunction("PF('dialogWConfirmAccountOk').show()");
		}
		this.placementSegmentSession = placementSegment;
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
	}
	
	/**
	 * Reject placement request.
	 */
	@LoggerAuditWeb
	public void reopenRequestPlacementSegment() {
		PlacementSegment placementSegment = null;
		try {
			placementSegment = placementSegmentServiceFacade.reopenRequestPlacementSegmentServiceFacade(placementSegmentSession);
		} catch (ServiceException e) {
			e.printStackTrace();
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
				JSFUtilities.executeJavascriptFunction("PF('formWEndConversationtOk').show()");
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		if(placementSegment!=null){
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_SUCCESS, null, PropertiesConstants.PLACEMENTESEGMENT_MESSAGE_REOPEN_REQUEST_OK,new Object[]{placementSegment.getIssuance().getIdIssuanceCodePk()});
			JSFUtilities.executeJavascriptFunction("PF('dialogWConfirmOk').show()");
		}
		searchPlacementSegmentHandler();
	}
	
	/**
	 * Confirm Reopen Request.
	 */
	@LoggerAuditWeb
	public void reopenConfirmPlacementSegment() {
		PlacementSegment placementSegment = null;
		try {
			placementSegment = placementSegmentServiceFacade.reopenPlacementSegmentServiceFacade(placementSegmentSession);
		} catch (ServiceException e) {
			e.printStackTrace();
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.MESSAGES_ALERT, e.getErrorService().getMessage());
				JSFUtilities.executeJavascriptFunction("PF('formWEndConversationtOk').show()");
			}else{
				excepcion.fire( new ExceptionToCatchEvent(e));
			}
			return;
		}
		if(placementSegment!=null){
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_SUCCESS, null, PropertiesConstants.PLACEMENTESEGMENT_MESSAGE_REOPEN_OK,new Object[]{placementSegment.getIssuance().getIdIssuanceCodePk()});
			JSFUtilities.executeJavascriptFunction("PF('dialogWConfirmOk').show()");
		}
		searchPlacementSegmentHandler();
	}
	
	/**
	 * Hide local components.
	 */
	public void hideLocalComponents(){
		JSFUtilities.hideGeneralDialogues();	
	}
	
	/**
	 * Search placement segment handler.
	 */
	@LoggerAuditWeb
	public void searchPlacementSegmentHandler(){
		if(issuerRequestSearch==null || issuerRequestSearch.getIdIssuerPk()==null || issuerRequestSearch.getIdIssuerPk().length() == 0){
			showMessageOnDialogOnlyKey(PropertiesConstants.LBL_HEADER_ALERT_ERROR, PropertiesConstants.ISSUER_VALIDATION_ISSUER);
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		Date initialDate = placementSegmentTO.getInitialDate();
		Date finalDate = placementSegmentTO.getFinalDate();
		
		executeAction();
		PlacementSegmentTO placementSegmentTO = new PlacementSegmentTO();
		placementSegmentTO.setIdIssuerPk(issuerRequestSearch.getIdIssuerPk());
		if(issuanceRequestSearch!=null && issuanceRequestSearch.getIdIssuanceCodePk()!=null && !issuanceRequestSearch.getIdIssuanceCodePk().isEmpty()){
			placementSegmentTO.setIdIssuancePk(issuanceRequestSearch.getIdIssuanceCodePk());
		}
		placementSegmentTO.setInitialDate(initialDate);
		placementSegmentTO.setFinalDate(finalDate);
		placementSegmentTO.setPlacementeSegmentState(this.placementSegmentTO.getPlacementeSegmentState());
		placementSegmentTO.setPlacementSegmentNumber(this.placementSegmentTO.getPlacementSegmentNumber());
		placementSegmentTO.setParticipant(this.placementSegmentTO.getParticipant());
		placementSegmentTO.setSecurity(this.placementSegmentTO.getSecurity());
		
		List<PlacementSegment> placementSegmentTemp = null;
		try{
			//Get from database
			placementSegmentTemp = placementSegmentServiceFacade.getListPlacementSegmentServiceFacade(placementSegmentTO,parameterTableMap);
		}catch(ServiceException ex){
			ex.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		placementSegmentDataModel = new GenericDataModel<PlacementSegment>(placementSegmentTemp);
		placementSegmentSession = new PlacementSegment();
	}	
		
	/**
	 * Clean placement segment handler.
	 */
	public void cleanPlacementSegmentHandler(){
		executeAction();
		participantList =new ArrayList<>(participantListTotal);
		participantListModel = new DualListModel<Participant>(this.getParticipantList(),new ArrayList<Participant>());
		placementSegmentSession = new PlacementSegment();
		placementSegmentSession.setBeginingDate(CommonsUtilities.currentDateTime());
	//	placementSegmentSession.setClosingDate(CommonsUtilities.currentDateTime());
		placementSegmentSession.setIssuance(new Issuance());
		placementSegParticipant.setIdParticipantPk(null);
		issuanceRequest = new Issuance();
		issuerRequest = new Issuer();
		if(issuerCode!=null){
			issuerRequest.setIdIssuerPk(issuerCode);
		}
	}
	
	/**
	 * Clean placement segment search handler.
	 *
	 * @param e the e
	 */
	public void cleanPlacementSegmentSearchHandler(ActionEvent e){
		executeAction();
		placementSegmentTO = new PlacementSegmentTO();
		placementSegmentTO.setParticipant(new Participant());
		placementSegmentTO.setSecurity(new Security());
		issuanceRequestSearch = new Issuance();
		if(!userInfo.getUserAccountSession().isIssuerInstitucion()){
			issuerRequestSearch = new Issuer();
		}		
		placementSegmentDataModel = null;
		try {
			placementSegmentTO.setListParticipant(placementSegmentServiceFacade.getParticipantRegistered());
		} catch (ServiceException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if(issuerCode!=null){
			placementSegmentTO.setIdIssuerPk(issuerCode);
			searchIssuerRequestSearch();
		}
	}
	
	/**
	 * Load cbo placement segment state type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCboPlacementSegmentStateType() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.PLACEMENT_SEGMENT_STATE.getCode());
		lstCboPlacementSegmentState = generalParameterFacade.getListParameterTableServiceBean(parameterTableTO);
		for(ParameterTable parameterTable: lstCboPlacementSegmentState){
			parameterTableMap.put(parameterTable.getParameterTablePk(), parameterTable.getParameterName());
		}
	}
	
	/**
	 * Load placement segment motive.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadPlacementSegmentMotive() throws ServiceException{
		parameterTableTO=new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.PLACEMENT_SEGMENT_REJECT_MOTIVE.getCode());
		motiveController.setMotiveList(generalParameterFacade.getListParameterTableServiceBean(parameterTableTO), MasterTableType.PLACEMENT_SEGMENT_REJECT_MOTIVE);
	}
	
	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate(){
		return CommonsUtilities.currentDate();
	}
	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate(){
		if(this.posteriorPlacSegment==null){//
			return placementSegmentSession.getIssuance().getExpirationDate();
		}
		return CommonsUtilities.addDate(posteriorPlacSegment.getBeginingDate(),-1);
	}
	/**
	 * Change motive handler.
	 */
	public void changeMotiveHandler(){
		if(this.getMotiveController().getIdMotivePK()==Long.parseLong(PlacementSegmentMotiveRejectType.OTHER.getCode().toString())){
			this.getMotiveController().setShowMotiveText(Boolean.TRUE);
		}else{
			this.getMotiveController().setShowMotiveText(Boolean.FALSE);
		}
	}
	
	/**
	 * Confirm motive handler.
	 */
	public void confirmMotiveHandler(){
		if(motiveController.getIdMotivePK()==Long.parseLong(PlacementSegmentMotiveRejectType.OTHER.getCode().toString())){
			if(Validations.validateIsNullOrEmpty(this.getMotiveController().getMotiveText())){
				JSFUtilities.executeJavascriptFunction("PF('dialogWMotive').show()");
				return;
			}
		}
		JSFUtilities.executeJavascriptFunction("PF('dialogWMotive').hide();");
		showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_REGISTER,null, PropertiesConstants.PLACEMENTESEGMENT_MESSAGE_REJECT,new Object[]{placementSegmentSession.getIssuance().getIdIssuanceCodePk()});
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').show()");
	}
	
	/**
	 * Confirm reopen handler.
	 */
	public void confirmReopenHandler(){
		if(placementSegmentSession.getReopenFileName() == null){
			if(Validations.validateIsNullOrEmpty(this.placementSegmentSession.getReopenFileName())){
				//JSFUtilities.executeJavascriptFunction("dialogWReopen.show()");
				showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT,null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_REOPEN_NOFILE,null);
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
		}
		JSFUtilities.executeJavascriptFunction("PF('dialogWReopen').hide();");
		showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_MODIFY,null, PropertiesConstants.PLACEMENTESEGMENT_MESSAGE_REOPEN_REQUEST,new Object[]{placementSegmentSession.getIssuance().getIdIssuanceCodePk()});
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirmReopenRequest').show();");
	}
	
	/**
	 * Before modify action.
	 *
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String beforeModifyAction() throws ServiceException{
		executeAction();
		if(placementSegmentSession==null){
			showMessageOnDialogOnlyKey(PropertiesConstants.LBL_HEADER_ALERT, PropertiesConstants.ERROR_RECORD_REQUIRED);	
			JSFUtilities.showSimpleValidationDialog();
			return StringUtils.EMPTY;
		}
		if(placementSegmentSession.getPlacementSegmentState().equals(PlacementSegmentStateType.REGISTERED.getCode()) 
				||placementSegmentSession.getPlacementSegmentState().equals(PlacementSegmentStateType.OPENED.getCode())){
			
			participantList =new ArrayList<>(participantListTotal);
			participantListModel = new DualListModel<Participant>(this.getParticipantList(),new ArrayList<Participant>());
			PlacementSegment PlacementSegmentAux=null;
			PlacementSegmentTO placementSegmentAuxTO = new PlacementSegmentTO();
			placementSegmentAuxTO.setIdIssuancePk(placementSegmentSession.getIssuance().getIdIssuanceCodePk());
			List<PlacementSegment> placSegmentOnIssuance = null;
			
			try {
				placSegmentOnIssuance = placementSegmentServiceFacade.getLastPlacementSegmentServiceFacade(placementSegmentAuxTO);
				if(placSegmentOnIssuance!=null && !placSegmentOnIssuance.isEmpty()){
					PlacementSegmentAux = placSegmentOnIssuance.get(0);
				}
			} catch (ServiceException e1) {
				e1.printStackTrace();
			}
			
			if(	Validations.validateIsNotNullAndNotEmpty(PlacementSegmentAux) &&
					PlacementSegmentAux.getPlacementSegmentState().equals(PlacementSegmentStateType.REGISTERED.getCode()) && 
					placementSegmentSession.getPlacementSegmentState().equals(PlacementSegmentStateType.OPENED.getCode()))
			{
				 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.PLACEMENTESEGMENT_ERROR_MODIFY_FOR_STATE
									,placementSegmentSession.getIssuance().getIdIssuanceCodePk()));		  
				 JSFUtilities.showSimpleValidationDialog();
				return StringUtils.EMPTY;
			}
			else
			{
				IssuerTO issuerTO = new IssuerTO();
				PlacementSegmentTO placementSegmentTO = new PlacementSegmentTO();
				placementSegmentTO.setIdPlacementeSegmentPk(placementSegmentSession.getIdPlacementSegmentPk());
				placementSegmentTO.setNeedIssuance(Boolean.TRUE);
				placementSegmentTO.setNeedParticipants(Boolean.TRUE);
				placementSegmentTO.setNeedSecurities(Boolean.TRUE);
				placementSegmentTO.setIdIssuancePk( placementSegmentSession.getIssuance().getIdIssuanceCodePk() );
				
				PlacementSegment placementTemp = placementSegmentServiceFacade.getPlacementSegmentServiceBean(placementSegmentTO);
				List<PlacementSegmentDetail> placementSegmentDetails=new ArrayList<PlacementSegmentDetail>();
				for(PlacementSegmentDetail placementSegmentDetail: placementTemp.getPlacementSegmentDetails()){
					if(!placementSegmentDetail.getSecurity().isCoupon() &&
					   !placementSegmentDetail.getSecurity().isDetached()){
						//issue 1034
						//placementSegmentDetail.setSecurityBalance(  placementSegmentDetail.getPlacedAmount().divide( placementSegmentDetail.getSecurity().getCurrentNominalValue() )  );
						placementSegmentDetail.setSecurityBalance(CommonsUtilities.divideTwoDecimal(placementSegmentDetail.getPlacedAmount(), placementSegmentDetail.getSecurity().getCurrentNominalValue(), 0));
						placementSegmentDetails.add(placementSegmentDetail);
					}
				}
				placementTemp.setPlacementSegmentDetails(placementSegmentDetails);
				
				issuerTO.setIdIssuerPk(placementTemp.getIssuance().getIssuer().getIdIssuerPk());
				issuerRequest = issuanceSecuritiesServiceFacade.findIssuerServiceFacade(issuerTO);	
				
				SecurityTO securityTO = new SecurityTO();
				securityTO.setIdIssuanceCodePk(placementTemp.getIssuance().getIdIssuanceCodePk());
				securityTO.setSecurityState(SecurityStateType.REGISTERED.getCode().toString());
				securityTO.setIndIsCoupon(BooleanType.NO.getCode());
				securityTO.setIndIsDetached(BooleanType.NO.getCode());
				List<Security> securityListTemp = null;
				try {
					securityListTemp = securityServiceFacade.findSecuritiesServiceFacade(securityTO);
					placementTemp = placementSegmentServiceFacade.setPlacementDetailsFromSecuritiesServiceFacade(placementTemp,securityListTemp);
					if(Validations.validateIsNotNullAndNotEmpty(placementTemp.getTrancheNumber()))
						posteriorPlacSegment = placementSegmentServiceFacade.getPosteriorPlacementSegmentServiceFacade(placementTemp.getIssuance().getIdIssuanceCodePk(),placementTemp.getTrancheNumber());
				} catch (Exception e) {
					excepcion.fire(new ExceptionToCatchEvent(e));
				}
				setPlacementSegmentSession(placementTemp);
				
				securityList = securityListTemp;
				placementSegParticipant=placementTemp.getPlacementSegParticipaStructs().get(0).getParticipant();
				
				BigDecimal sum=BigDecimal.ZERO;
				for(Security sec:securityListTemp){
					sum=sum.add(sec.getDesmaterializedBalance());
				}
				placementSegmentSession.getIssuance().setPlacedSecuritiesBalance( sum.intValue() );
				
				IssuanceTO issuanceTO = IssuanceTO.getInstanceFromIssuancePK(placementSegmentSession.getIssuance().getIdIssuanceCodePk());//Traemos la instancia desde el Pk
				issuanceTO.setNeedPlaceSegments(BooleanType.YES.getBooleanValue());
				Issuance issuanceTemp  = issuanceSecuritiesServiceFacade.findIssuanceServiceFacade(issuanceTO);
				
				placementSegmentSession.getIssuance().setRegisteredTranches( issuanceTemp.getRegisteredTranches() );
				placementSegmentSession.getIssuance().setConfirmedTranches( issuanceTemp.getConfirmedTranches() );
				placementSegmentSession.getIssuance().setOpenedTranches( issuanceTemp.getOpenedTranches() );
				placementSegmentSession.getIssuance().setClosedTranches( issuanceTemp.getClosedTranches() );
				
				placementSegmentSession.getIssuance().setAmountRegisteredSegment( issuanceTemp.getAmountRegisteredSegment() );
				placementSegmentSession.getIssuance().setAmountConfirmedSegment( issuanceTemp.getAmountConfirmedSegment() );
				placementSegmentSession.getIssuance().setAmountOpenedSegment( issuanceTemp.getAmountOpenedSegment() );
				
				
				
				getParticipantListModel().setTarget(new ArrayList<>());
				for(PlacementSegParticipaStruct p:placementSegmentSession.getPlacementSegParticipaStructs()) {
					getParticipantListModel().getTarget().add(p.getParticipant());
				}
				
				jump:
					for(Participant participantTarget: participantListModel.getTarget()){
						for(Participant participant: participantListModel.getSource()){
							if(participantTarget.getIdParticipantPk().equals(participant.getIdParticipantPk())){
								participantListModel.getSource().remove(participant);
								continue jump;
							}
						}
					}
				/**
				 * Issue 2503
				 */
				PlacementSegmentTO placementSegmentTOTmp = new PlacementSegmentTO();
				placementSegmentTOTmp.setIdIssuancePk(placementSegmentSession.getIssuance().getIdIssuanceCodePk() );
				placementSegmentTOTmp.setIdPlacementeSegmentRefPk(placementSegmentSession.getIdPlacementSegmentPk());
				List<PlacementSegment> listPlacement;

				/**Get List To Placement Segment To Data Base*/
				listPlacement = placementSegmentServiceFacade.getListPlacementSegmentServiceFacade(placementSegmentTOTmp, parameterTableMap);

				openingAmount =BigDecimal.ZERO;
				BigDecimal amountPlaceToSegments=BigDecimal.ZERO;
				
				if(Validations.validateListIsNotNullAndNotEmpty(listPlacement)){
					for (PlacementSegment placementSegmentTemp : listPlacement) {
						amountPlaceToSegments=CommonsUtilities.addTwoDecimal(amountPlaceToSegments, placementSegmentTemp.getAmountToPlace(), 4);
					}
				}

				/**Opening Amount is Amount Issuance sub Amount To Placement Segment*/
				openingAmount=CommonsUtilities.subtractTwoDecimal(placementSegmentSession.getIssuance().getIssuanceAmount(), amountPlaceToSegments, 8);
				
				
				
				setViewOperationType(ViewOperationsType.MODIFY.getCode());
				return "placementSegmentModifyMgmtRule";
			}			
		}else{
			showMessageOnDialogOnlyKey(null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_MODIFY_STATE);
			JSFUtilities.showSimpleValidationDialog();
			return StringUtils.EMPTY;
		}
	}
	
	/**
	 * Before reopen action. ISSUE 1273
	 *
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String beforeReopenRequest() throws ServiceException{
		executeAction();
		if(placementSegmentSession == null){
			showMessageOnDialogOnlyKey(PropertiesConstants.LBL_HEADER_ALERT, PropertiesConstants.ERROR_RECORD_REQUIRED);	
			JSFUtilities.showSimpleValidationDialog();
			return StringUtils.EMPTY;
		}
		
		PlacementSegmentTO placementSegmentTO = new PlacementSegmentTO();
		placementSegmentTO.setIdPlacementeSegmentPk(placementSegmentSession.getIdPlacementSegmentPk());
		placementSegmentTO.setNeedIssuance(Boolean.TRUE);
		placementSegmentTO.setNeedParticipants(Boolean.TRUE);
		placementSegmentTO.setNeedSecurities(Boolean.TRUE);
		placementSegmentTO.setIdIssuancePk( placementSegmentSession.getIssuance().getIdIssuanceCodePk() );
		
		placementSegmentSession = placementSegmentServiceFacade.getPlacementSegmentServiceBean(placementSegmentTO);
		
		if(!placementSegmentSession.getPlacementSegmentState().equals(PlacementSegmentStateType.CLOSED.getCode())){
			showMessageOnDialogOnlyKey(null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_REOPEN_STATE);
			JSFUtilities.showSimpleValidationDialog();
			return StringUtils.EMPTY;
		}
		if(Validations.validateIsNotNull(placementSegmentSession.getReopenRequest()) && placementSegmentSession.getReopenRequest()==1){
			showMessageOnDialogOnlyKey(null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_REOPEN_REQUEST);
			JSFUtilities.showSimpleValidationDialog();
			return StringUtils.EMPTY;
		}
		
		JSFUtilities.executeJavascriptFunction("PF('dialogWReopen').show()");
		
		return StringUtils.EMPTY;
	}
	
	/**
	 * Before reopen action. iSSUE 1273
	 *
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String beforeReopenConfirm() throws ServiceException{
		executeAction();
		if(placementSegmentSession == null){
			showMessageOnDialogOnlyKey(PropertiesConstants.LBL_HEADER_ALERT, PropertiesConstants.ERROR_RECORD_REQUIRED);	
			JSFUtilities.showSimpleValidationDialog();
			return StringUtils.EMPTY;
		}
		if(Validations.validateIsNull(placementSegmentSession.getReopenRequest()) || placementSegmentSession.getReopenRequest()!=1){
			showMessageOnDialogOnlyKey(PropertiesConstants.LBL_HEADER_ALERT, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_REOPEN_APROVE);	
			JSFUtilities.showSimpleValidationDialog();
			return StringUtils.EMPTY;
		}
		
		PlacementSegmentTO placementSegmentTO = new PlacementSegmentTO();
		placementSegmentTO.setIdPlacementeSegmentPk(placementSegmentSession.getIdPlacementSegmentPk());
		placementSegmentTO.setNeedIssuance(Boolean.TRUE);
		placementSegmentTO.setNeedParticipants(Boolean.TRUE);
		placementSegmentTO.setNeedSecurities(Boolean.TRUE);
		placementSegmentTO.setIdIssuancePk( placementSegmentSession.getIssuance().getIdIssuanceCodePk() );
		
		placementSegmentSession = placementSegmentServiceFacade.getPlacementSegmentServiceBean(placementSegmentTO);
		
		if(!placementSegmentSession.getPlacementSegmentState().equals(PlacementSegmentStateType.CLOSED.getCode())){
			showMessageOnDialogOnlyKey(null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_REOPEN_STATE);
			JSFUtilities.showSimpleValidationDialog();
			return StringUtils.EMPTY;
		}
		
		showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT_MODIFY,null, PropertiesConstants.PLACEMENTESEGMENT_MESSAGE_REOPEN,new Object[]{placementSegmentSession.getIssuance().getIdIssuanceCodePk()});
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirmReopen').show();");
		
		return StringUtils.EMPTY;
	}
	
	/**
	 * Reopen file handler.
	 * 
	 * @param event the event
	 */
	public void reopenFileHandler(FileUploadEvent event) {
		try {
			 String fDisplayName = event.getFile().getFileName();
			 
			 if(fDisplayName != null){
				 
				 placementSegmentSession.setReopenFileName(fDisplayName);
				 placementSegmentSession.setReopenFile(event.getFile().getContents());

				 //si el nombre del archivo es demasiado grande se limita el tamanio
				 String fileName = event.getFile().getFileName();
				 if(fileName.length()<50){
					 placementSegmentSession.setReopenFileName(fileName);	 
				 }else{
					 placementSegmentSession.setReopenFileName(fileName.substring(0,48));
				 }
			 }

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Removes the uploaded file handler.
	 *
	 * @param event the event
	 */
	public void removeUploadedFileHandler(ActionEvent event){
		try {
			placementSegmentSession.setReopenFileName(null);
			placementSegmentSession.setReopenFile(null);
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Gets the download file.
	 *
	 * @return the download certificate file
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public StreamedContent getDownloadReopenFile(String name, byte[] file ) throws IOException {

		InputStream inputStream = new ByteArrayInputStream(file);
		StreamedContent streamedContentFile = new DefaultStreamedContent(inputStream, null, name);
		inputStream.close();

		return streamedContentFile;
	}
	
	/**
	 * Before reject handler.
	 *
	 * @return the string
	 */
	public String beforeRejectAction(){
		executeAction();
		PlacementSegment placementSegment = this.getPlacementSegmentSession();
		if(placementSegment==null){
			showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.ERROR_RECORD_REQUIRED, null);
			JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		if(placementSegment.getPlacementSegmentState().equals(PlacementSegmentStateType.REGISTERED.getCode())){			
			selectPlacementSegmentHandler(placementSegment);
			setViewOperationType(ViewOperationsType.REJECT.getCode());
			return "placementSegmentDetailMgmtRule";
		}
		showMessageOnDialog(PropertiesConstants.LBL_HEADER_ALERT, null, PropertiesConstants.PLACEMENTESEGMENT_VALIDATION_DO_ACTION, new Object[]{getPlacementSegmentSession().getPlacementSegmentStateDescription()});
		JSFUtilities.showSimpleValidationDialog();
		return "";
	}
	/**
	 * Gets the placement segment session.
	 *
	 * @return the placement segment session
	 */
	public PlacementSegment getPlacementSegmentSession() {
		return placementSegmentSession;
	}

	/**
	 * Sets the placement segment session.
	 *
	 * @param placementSegmentSession the placement segment session
	 */
	public void setPlacementSegmentSession(PlacementSegment placementSegmentSession) {
		this.placementSegmentSession = placementSegmentSession;
	}

	/**
	 * Gets the issuer request.
	 *
	 * @return the issuer request
	 */
	public Issuer getIssuerRequest() {
		return issuerRequest;
	}

	/**
	 * Sets the issuer request.
	 *
	 * @param issuerRequest the issuer request
	 */
	public void setIssuerRequest(Issuer issuerRequest) {
		this.issuerRequest = issuerRequest;
	}

	/**
	 * Gets the placement segment to.
	 *
	 * @return the placement segment to
	 */
	public PlacementSegmentTO getPlacementSegmentTO() {
		return placementSegmentTO;
	}

	/**
	 * Sets the placement segment to.
	 *
	 * @param placementSegmentTO the placement segment to
	 */
	public void setPlacementSegmentTO(PlacementSegmentTO placementSegmentTO) {
		this.placementSegmentTO = placementSegmentTO;
	}

	/**
	 * Gets the issuance request.
	 *
	 * @return the issuance request
	 */
	public Issuance getIssuanceRequest() {
		return issuanceRequest;
	}

	/**
	 * Sets the issuance request.
	 *
	 * @param issuanceRequest the issuance request
	 */
	public void setIssuanceRequest(Issuance issuanceRequest) {
		this.issuanceRequest = issuanceRequest;
	}
	
	/**
	 * Gets the current date.
	 *
	 * @return the current date
	 */
	public Date getCurrentDate(){
		return CommonsUtilities.currentDate();
	}

	/**
	 * Gets the participant list.
	 *
	 * @return the participant list
	 */
	public List<Participant> getParticipantList() {
		return participantList;
	}

	/**
	 * Sets the participant list.
	 *
	 * @param participantList the new participant list
	 */
	public void setParticipantList(List<Participant> participantList) {
		this.participantList = participantList;
	}

	/**
	 * Gets the participant list model.
	 *
	 * @return the participant list model
	 */
	public DualListModel<Participant> getParticipantListModel() {
		return participantListModel;
	}

	/**
	 * Sets the participant list model.
	 *
	 * @param participantListModel the new participant list model
	 */
	public void setParticipantListModel(DualListModel<Participant> participantListModel) {
		this.participantListModel = participantListModel;
	}

	
	
	/**
	 * Gets the lst cbo placement segment state.
	 *
	 * @return the lst cbo placement segment state
	 */
	public List<ParameterTable> getLstCboPlacementSegmentState() {
		return lstCboPlacementSegmentState;
	}

	/**
	 * Sets the lst cbo placement segment state.
	 *
	 * @param lstCboPlacementSegmentState the new lst cbo placement segment state
	 */
	public void setLstCboPlacementSegmentState(List<ParameterTable> lstCboPlacementSegmentState) {
		this.lstCboPlacementSegmentState = lstCboPlacementSegmentState;
	}

	/**
	 * Gets the placement segment data model.
	 *
	 * @return the placement segment data model
	 */
	public GenericDataModel<PlacementSegment> getPlacementSegmentDataModel() {
		return placementSegmentDataModel;
	}

	/**
	 * Sets the placement segment data model.
	 *
	 * @param placementSegmentDataModel the new placement segment data model
	 */
	public void setPlacementSegmentDataModel(GenericDataModel<PlacementSegment> placementSegmentDataModel) {
		this.placementSegmentDataModel = placementSegmentDataModel;
	}

	/**
	 * Gets the motive controller.
	 *
	 * @return the motive controller
	 */
	public MotiveController getMotiveController() {
		return motiveController;
	}

	/**
	 * Sets the motive controller.
	 *
	 * @param motiveController the new motive controller
	 */
	public void setMotiveController(MotiveController motiveController) {
		this.motiveController = motiveController;
	}

	/**
	 * Gets the last plac segment.
	 *
	 * @return the last plac segment
	 */
	public PlacementSegment getLastPlacSegment() {
		return lastPlacSegment;
	}

	/**
	 * Sets the last plac segment.
	 *
	 * @param lastPlacSegment the new last plac segment
	 */
	public void setLastPlacSegment(PlacementSegment lastPlacSegment) {
		this.lastPlacSegment = lastPlacSegment;
	}
	
	/**
	 * Exist last placem segment.
	 *
	 * @return true, if successful
	 */
	public boolean isExistLastPlacemSegment(){
		return (placementSegmentSession.getIssuance()!=null && lastPlacSegment!=null);
	}

	/**
	 * Gets the security list.
	 *
	 * @return the security list
	 */
	public List<Security> getSecurityList() {
		return securityList;
	}

	/**
	 * Sets the security list.
	 *
	 * @param securityList the new security list
	 */
	public void setSecurityList(List<Security> securityList) {
		this.securityList = securityList;
	}

	/**
	 * Gets the placement segment participan struc.
	 *
	 * @return the placement segment participan struc
	 */
	public List<PlacementSegParticipaStruct> getPlacementSegmentParticipanStruc() {
		return placementSegmentParticipanStruc;
	}

	/**
	 * Sets the placement segment participan struc.
	 *
	 * @param placementSegmentParticipanStruc the new placement segment participan struc
	 */
	public void setPlacementSegmentParticipanStruc(
			List<PlacementSegParticipaStruct> placementSegmentParticipanStruc) {
		this.placementSegmentParticipanStruc = placementSegmentParticipanStruc;
	}
	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}
	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	
	/** The security help bean. */
	@Inject
	SecurityHelpBean securityHelpBean;
	/**
	 * Show isin detail.
	 *
	 * @param isinCode the isin code
	 */
	public void showIsinDetail(String isinCode){
		securityHelpBean.setSecurityCode(isinCode);
		securityHelpBean.setName("helpSecurity");
		securityHelpBean.searchSecurityHandler();
	}
	/**
	 * Gets the issuer request search.
	 *
	 * @return the issuer request search
	 */
	public Issuer getIssuerRequestSearch() {
		return issuerRequestSearch;
	}

	/**
	 * Sets the issuer request search.
	 *
	 * @param issuerRequestSearch the new issuer request search
	 */
	public void setIssuerRequestSearch(Issuer issuerRequestSearch) {
		this.issuerRequestSearch = issuerRequestSearch;
	}
	/**
	 * Gets the issuance request search.
	 *
	 * @return the issuance request search
	 */
	public Issuance getIssuanceRequestSearch() {
		return issuanceRequestSearch;
	}
	/**
	 * Sets the issuance request search.
	 *
	 * @param issuanceRequestSearch the new issuance request search
	 */
	public void setIssuanceRequestSearch(Issuance issuanceRequestSearch) {
		this.issuanceRequestSearch = issuanceRequestSearch;
	}
	
	
	
	/**
	 * Gets the placement seg participant.
	 *
	 * @return the placement seg participant
	 */
	public Participant getPlacementSegParticipant() {
		return placementSegParticipant;
	}



	/**
	 * Sets the placement seg participant.
	 *
	 * @param placementSegParticipant the new placement seg participant
	 */
	public void setPlacementSegParticipant(Participant placementSegParticipant) {
		this.placementSegParticipant = placementSegParticipant;
	}



	/**
	 * Gets the minimun date.
	 *
	 * @param date the date
	 * @return the minimun date
	 */
	public Date getMinimunDate(Date date){
		return this.getAlterDateDays(date, maxDaysOfCustodyRequest*-1);
	}
	
	/**
	 * Gets the maximun date.
	 *
	 * @param date the date
	 * @return the maximun date
	 */
	public Date getMaximunDate(Date date){
		return this.getAlterDateDays(date, maxDaysOfCustodyRequest);
	}
	
	/**
	 * Clean Data Model.
	 */
	public void cleanDataModel()
	{
		placementSegmentDataModel = null;
	}
	/**
	 * Checks if is mixed Security Placement Type.
	 * 
	 * @return true, if is mixed Security Placement Type
	 *         isMixedSecurityPlacementType
	 */
	public boolean isMixedSecurityPlacementType() {
		if (placementSegmentSession.getIssuance().getPlacementType() != null
				&& placementSegmentSession.getIssuance().getPlacementType().equals(
						SecurityPlacementType.MIXED.getCode())) {
			return true;
		}
		return false;
	}
	/**
	 * Checks if is mixed Security Placement Type.
	 * 
	 * @return true, if is fixed Security Placement Type
	 *         isFixedSecurityPlacementType
	 */
	public boolean isFixedSecurityPlacementType() {
		if (placementSegmentSession.getIssuance().getPlacementType() != null
				&& placementSegmentSession.getIssuance().getPlacementType().equals(
						SecurityPlacementType.FIXED.getCode())) {
			return true;
		}
		return false;
	}
	/**
	 * Checks if is mixed Security Placement Type.
	 * 
	 * @return true, if is floating Security Placement Type
	 *         isFloatingSecurityPlacementType
	 */
	public boolean isFloatingSecurityPlacementType() {
		if (placementSegmentSession.getIssuance().getPlacementType() != null
				&& placementSegmentSession.getIssuance().getPlacementType().equals(
						SecurityPlacementType.FLOATING.getCode())) {
			return true;
		}
		return false;
	}
	
	/**
	 * get Mixed Security Placement Type.
	 *
	 * @return code Mixed Security Placement Type
	 */
	public Integer codeMixedSecurityPlacementType()
	{
		return SecurityPlacementType.MIXED.getCode();
	}
	
	/**
	 * get Fixed Security Placement Type.
	 *
	 * @return code Fixed Security Placement Type
	 */
	public Integer codeFixedSecurityPlacementType()
	{
		return SecurityPlacementType.FIXED.getCode();
	}
	
	/**
	 * get Floating Security Placement Type.
	 *
	 * @return code Floating Security Placement Type
	 */
	public Integer codeFloatingSecurityPlacementType()
	{
		return SecurityPlacementType.FLOATING.getCode();
	}



	/**
	 * Gets the opening amount.
	 *
	 * @return the openingAmount
	 */
	public BigDecimal getOpeningAmount() {
		return openingAmount;
	}



	/**
	 * Sets the opening amount.
	 *
	 * @param openingAmount the openingAmount to set
	 */
	public void setOpeningAmount(BigDecimal openingAmount) {
		this.openingAmount = openingAmount;
	}
	
	
}