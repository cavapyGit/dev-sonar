package com.pradera.securities.placementsegment.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.model.accounts.Participant;
import com.pradera.model.issuancesecuritie.Security;

// TODO: Auto-generated Javadoc
/**
 * The Class PlacementSegmentTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/04/2013
 */
public class PlacementSegmentTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id issuer pk. */
	private String idIssuerPk;
	
	/** The id issuance pk. */
	private String idIssuancePk;
	
	/** The id placemente segment pk. */
	private Long idPlacementeSegmentPk;
	
	/** The id placemente segment Reference pk. */
	private Long idPlacementeSegmentRefPk;
	
	/** The state placemente segment. */
	private Integer placementeSegmentState;
	
	/** The placement segment number. */
	private Integer placementSegmentNumber;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/** The begin date. */
	private Date beginPlacementDate;
	
	/** The close placement date. */
	private Date closePlacementDate;
	
	/** The plac seg detail type. */
	private Integer placSegDetailType;
	
	/** The need placement part struct. */
	private Boolean needParticipants = Boolean.FALSE;
	
	/** The need securities. */
	private Boolean needSecurities = Boolean.FALSE;
	
	/** The need issuance. */
	private Boolean needIssuance = Boolean.FALSE;
	
	/** The security. */
	private Security security;
	
	/** The participant. */
	private Participant participant;
	
	/** The list participant. */
	List<Participant> listParticipant;

	/**
	 * The Constructor.
	 */
	public PlacementSegmentTO() {
		super();
	}

	/**
	 * Instantiates a new placement segment to.
	 *
	 * @param initialDate the initial date
	 * @param finalDate the final date
	 */
	public PlacementSegmentTO(Date initialDate, Date finalDate) {
		super();
		this.initialDate = initialDate;
		this.finalDate = finalDate;
	}



	/**
	 * Gets the id issuer pk.
	 *
	 * @return the id issuer pk
	 */
	public String getIdIssuerPk() {
		return idIssuerPk;
	}

	/**
	 * Sets the id issuer pk.
	 *
	 * @param idIssuerPk the id issuer pk
	 */
	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}

	/**
	 * Gets the id issuance pk.
	 *
	 * @return the id issuance pk
	 */
	public String getIdIssuancePk() {
		return idIssuancePk;
	}

	/**
	 * Sets the id issuance pk.
	 *
	 * @param idIssuancePk the id issuance pk
	 */
	public void setIdIssuancePk(String idIssuancePk) {
		this.idIssuancePk = idIssuancePk;
	}

	/**
	 * Gets the id placemente segment pk.
	 *
	 * @return the id placemente segment pk
	 */
	public Long getIdPlacementeSegmentPk() {
		return idPlacementeSegmentPk;
	}

	/**
	 * Sets the id placemente segment pk.
	 *
	 * @param idPlacementeSegmentPk the id placemente segment pk
	 */
	public void setIdPlacementeSegmentPk(Long idPlacementeSegmentPk) {
		this.idPlacementeSegmentPk = idPlacementeSegmentPk;
	}
	
	

	/**
	 * Gets the id placemente segment ref pk.
	 *
	 * @return the idPlacementeSegmentRefPk
	 */
	public Long getIdPlacementeSegmentRefPk() {
		return idPlacementeSegmentRefPk;
	}

	/**
	 * Sets the id placemente segment ref pk.
	 *
	 * @param idPlacementeSegmentRefPk the idPlacementeSegmentRefPk to set
	 */
	public void setIdPlacementeSegmentRefPk(Long idPlacementeSegmentRefPk) {
		this.idPlacementeSegmentRefPk = idPlacementeSegmentRefPk;
	}

	/**
	 * Gets the state placemente segment.
	 *
	 * @return the state placemente segment
	 */
	public Integer getPlacementeSegmentState() {
		return placementeSegmentState;
	}

	/**
	 * Sets the state placemente segment.
	 *
	 * @param statePlacementeSegment the state placemente segment
	 */
	public void setPlacementeSegmentState(Integer statePlacementeSegment) {
		this.placementeSegmentState = statePlacementeSegment;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	/**
	 * Gets the need participants.
	 *
	 * @return the need participants
	 */
	public Boolean getNeedParticipants() {
		return needParticipants;
	}

	/**
	 * Sets the need participants.
	 *
	 * @param needParticipants the new need participants
	 */
	public void setNeedParticipants(Boolean needParticipants) {
		this.needParticipants = needParticipants;
	}

	/**
	 * Gets the need securities.
	 *
	 * @return the need securities
	 */
	public Boolean getNeedSecurities() {
		return needSecurities;
	}

	/**
	 * Sets the need securities.
	 *
	 * @param needSecurities the new need securities
	 */
	public void setNeedSecurities(Boolean needSecurities) {
		this.needSecurities = needSecurities;
	}

	/**
	 * Gets the need issuance.
	 *
	 * @return the need issuance
	 */
	public Boolean getNeedIssuance() {
		return needIssuance;
	}

	/**
	 * Sets the need issuance.
	 *
	 * @param needIssuance the new need issuance
	 */
	public void setNeedIssuance(Boolean needIssuance) {
		this.needIssuance = needIssuance;
	}

	/**
	 * Gets the begin placement date.
	 *
	 * @return the begin placement date
	 */
	public Date getBeginPlacementDate() {
		return beginPlacementDate;
	}

	/**
	 * Sets the begin placement date.
	 *
	 * @param beginPlacementDate the new begin placement date
	 */
	public void setBeginPlacementDate(Date beginPlacementDate) {
		this.beginPlacementDate = beginPlacementDate;
	}

	/**
	 * Gets the close placement date.
	 *
	 * @return the close placement date
	 */
	public Date getClosePlacementDate() {
		return closePlacementDate;
	}

	/**
	 * Sets the close placement date.
	 *
	 * @param closePlacementDate the new close placement date
	 */
	public void setClosePlacementDate(Date closePlacementDate) {
		this.closePlacementDate = closePlacementDate;
	}

	/**
	 * Gets the plac seg detail type.
	 *
	 * @return the plac seg detail type
	 */
	public Integer getPlacSegDetailType() {
		return placSegDetailType;
	}

	/**
	 * Sets the plac seg detail type.
	 *
	 * @param placSegDetailType the new plac seg detail type
	 */
	public void setPlacSegDetailType(Integer placSegDetailType) {
		this.placSegDetailType = placSegDetailType;
	}

	/**
	 * Gets the placement segment number.
	 *
	 * @return the placement segment number
	 */
	public Integer getPlacementSegmentNumber() {
		return placementSegmentNumber;
	}

	/**
	 * Sets the placement segment number.
	 *
	 * @param placementSegmentNumber the new placement segment number
	 */
	public void setPlacementSegmentNumber(Integer placementSegmentNumber) {
		this.placementSegmentNumber = placementSegmentNumber;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * Gets the list participant.
	 *
	 * @return the list participant
	 */
	public List<Participant> getListParticipant() {
		return listParticipant;
	}

	/**
	 * Sets the list participant.
	 *
	 * @param listParticipant the new list participant
	 */
	public void setListParticipant(List<Participant> listParticipant) {
		this.listParticipant = listParticipant;
	}
}