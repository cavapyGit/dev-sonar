package com.pradera.securities.placementsegment.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountGroupType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStateDetailType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantMechanismStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.accounts.type.ParticipantType;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegParticStrucHy;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegParticipaStruct;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegment;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegmentDetail;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegmentDetailHy;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegmentHy;
import com.pradera.model.issuancesecuritie.placementsegment.type.PlacementSegmentHistoryType;
import com.pradera.model.issuancesecuritie.placementsegment.type.PlacementSegmentStateType;
import com.pradera.model.issuancesecuritie.type.IssuanceStateType;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.SecurityPlacementType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.negotiation.type.NegotiationModalityType;
import com.pradera.securities.issuancesecuritie.service.IssuanceServiceBean;
import com.pradera.securities.issuancesecuritie.to.IssuanceTO;
import com.pradera.securities.issuer.service.IssuerServiceBean;
import com.pradera.securities.placementsegment.to.PlacementSegmentTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PlacementSegmentServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class PlacementSegmentServiceBean extends CrudDaoServiceBean{
	
	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The issuance service bean. */
	@EJB
	IssuanceServiceBean issuanceServiceBean;
	
	/** The issuer service bean. */
	@EJB
	IssuerServiceBean issuerServiceBean;

	/**
	 * Registry placement segment service bean.
	 *
	 * @param placementSegmentParam the placement segment param
	 * @return the placement segment
	 * @throws ServiceException the service exception
	 */
	public PlacementSegment registryPlacementSegmentServiceBean(PlacementSegment placementSegmentParam) throws ServiceException{
		PlacementSegment placementSegment = placementSegmentParam;
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		if(placementSegment.getPlacementSegmentState().equals(PlacementSegmentStateType.REGISTERED.getCode())){//Si el tramo es registrado
			if(placementSegment.getPlacementSegmentType().equals(SecurityPlacementType.FIXED.getCode())){//Si es fijo, el monto fijo sera el mismo del tramo
				placementSegment.setFixedAmount(placementSegment.getAmountToPlace());
				placementSegment.setFloatingAmount(BigDecimal.ZERO);
			}else if(placementSegment.getPlacementSegmentType().equals(SecurityPlacementType.FLOATING.getCode())){//Si es flotante, el monto floatante sera el mismo del tramo
				placementSegment.setFixedAmount(BigDecimal.ZERO);
				placementSegment.setFloatingAmount(placementSegment.getAmountToPlace());
			}else if(placementSegment.getPlacementSegmentType().equals(SecurityPlacementType.MIXED.getCode())){
				placementSegment.setFixedAmount(placementSegment.getPlacedAmountFixedFromDetail());//El monto fijo sera la sumatoria de los valores ingresados
				BigDecimal floatingAmount = placementSegment.getPlacedAmountFloatingFromDetail();
				placementSegment.setFloatingAmount(floatingAmount);
				
				if(floatingAmount==null){//Si es null, el tramo aun no tiene valores flotantes
					placementSegment.setPlacementSegmentType(SecurityPlacementType.FIXED.getCode());
					placementSegment.setFloatingAmount(BigDecimal.ZERO);
				}else if(placementSegment.getFixedAmount()==null || placementSegment.getFixedAmount().equals(BigDecimal.ZERO)){
					placementSegment.setPlacementSegmentType(SecurityPlacementType.FLOATING.getCode());
				}
			}
			if(!(placementSegment.getFixedAmount().add(placementSegment.getFloatingAmount())).equals(placementSegment.getAmountToPlace())){//Validamos la sumatoria
				throw new ServiceException();//la sumatoria no coinciden, error de validaciones
			}
			placementSegment.setRequestFixedAmount(BigDecimal.ZERO);//El monto en solicitudes es 0
			placementSegment.setRequestFloatingAmount(BigDecimal.ZERO);//El monto en solicitudes es 0
		}
		placementSegment.setBalanceToConfirm(BigDecimal.ZERO);
		placementSegment=create(placementSegment);
		
		return placementSegment;
	}
	
	
	
	/**
	 * Validate placement segment entities.
	 *
	 * @param placementSegment the placement segment
	 * @throws ServiceException the service exception
	 */
	private void validatePlacementSegmentEntities(PlacementSegment placementSegment) throws ServiceException{
		Map<String,Object> mapParams = new HashMap<String, Object>();
		for(PlacementSegmentDetail placementDetail: placementSegment.getPlacementSegmentDetails()){
			mapParams.put("idSecurityCodePkParam", placementDetail.getSecurity().getIdSecurityCodePk());
			Security security = findObjectByNamedQuery(Security.SECURITY_STATE, mapParams);
			if(!security.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.SECURITY_BLOCK);
			}
		}
		
		mapParams = new HashMap<String, Object>();
		for(PlacementSegParticipaStruct participantPlac: placementSegment.getPlacementSegParticipaStructs()){
			mapParams.put("idParticipantPkParam", participantPlac.getParticipant().getIdParticipantPk());
			Participant participantBuyer = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, mapParams);
			if(!participantBuyer.getState().equals(ParticipantStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
			}
		}
		
		mapParams = new HashMap<String, Object>();
		mapParams.put("idIssuanceCodePkParam", placementSegment.getIssuance().getIdIssuanceCodePk());
		Issuance issuace = findObjectByNamedQuery(Issuance.ISSUANCE_STATE, mapParams);
		if(!issuace.getStateIssuance().equals(IssuanceStateType.AUTHORIZED.getCode())){
			throw new ServiceException(ErrorServiceType.ISSUANCE_BLOCK);
		}
		
		mapParams = new HashMap<String, Object>();
		mapParams.put("idIssuerPkParam", placementSegment.getIssuance().getIssuer().getIdIssuerPk());
		Issuer issuer = findObjectByNamedQuery(Issuer.ISSUER_STATE, mapParams);
		if(!issuer.getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.ISSUER_BLOCK);
		}
		
		mapParams = new HashMap<String, Object>();
		Holder holderTemp = placementSegment.getIssuance().getIssuer().getHolder();
		if(Validations.validateIsNull(holderTemp)){				
			throw new ServiceException(ErrorServiceType.HOLDER_NOT_EXIST_FOR_EMISOR);
		}else if(holderTemp!=null && !holderTemp.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
			mapParams.put("idIssuerPkParam", placementSegment.getIssuance().getIssuer().getHolder().getIdHolderPk());
			Holder holder = findObjectByNamedQuery(Holder.HOLDER_STATE, mapParams);
			if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.HOLDER_BLOCK);
			}
		}
	}
	
	/**
	 * Confirm placement segment service bean.
	 *
	 * @param placementSegmentParam the placement segment param
	 * @param loggerUser the logger user
	 * @return the placement segment
	 * @throws ServiceException the service exception
	 */
	public PlacementSegment confirmPlacementSegmentServiceBean(PlacementSegment placementSegmentParam, LoggerUser loggerUser) throws ServiceException{
		validatePlacementSegmentEntities(placementSegmentParam);
		
		PlacementSegmentTO placementSegmentTO = new PlacementSegmentTO();
		placementSegmentTO.setNeedIssuance(Boolean.TRUE);
		placementSegmentTO.setIdPlacementeSegmentPk(placementSegmentParam.getIdPlacementSegmentPk());
		placementSegmentTO.setNeedIssuance(false);
		Holder issuerLikeHolder = null;
		PlacementSegment placementSegment = null;
		try {
			placementSegment = getPlacementSegmentServiceBean(placementSegmentTO);
			issuerLikeHolder = placementSegment.getIssuance().getIssuer().getHolder();
		} catch (ServiceException e) {
			e.printStackTrace();
			return placementSegment;
		}
		Integer placementNumberAvailable = placementSegment.getIssuance().getCurrentTranche() + 1;
		BigDecimal amountOnPlacementConfirmed, amountConfirmedIssuance = placementSegment.getIssuance().getAmountConfirmedSegments();
		amountOnPlacementConfirmed = placementSegment.getAmountToPlace().add(amountConfirmedIssuance==null?BigDecimal.ZERO:amountConfirmedIssuance);
		
		placementSegment.setConfirmUser(loggerUser.getUserName());
		placementSegment.setConfirmDate(CommonsUtilities.currentDateTime());
		if(placementSegment.getConfirmDate().compareTo(placementSegment.getBeginingDate()) >= 0){
			placementSegment.setPlacementSegmentState(PlacementSegmentStateType.OPENED.getCode());//Aperturamos el tramo
			placementSegment.setOpenDate(CommonsUtilities.currentDateTime());
		}else{
			placementSegment.setPlacementSegmentState(PlacementSegmentStateType.CONFIRMED.getCode());
		}
		
		placementSegment.setTrancheNumber(placementNumberAvailable);
		placementSegment.setPendingOperation(BigDecimal.ZERO);
		placementSegment.setPendingAnnotation(BigDecimal.ZERO);
		
		HolderAccountTO holderAccountTO = null;
//		StringBuilder participantsDecription = new StringBuilder();
		for(PlacementSegParticipaStruct placementSegParticipaStruct: placementSegment.getPlacementSegParticipaStructs()){
			Participant participant = placementSegParticipaStruct.getParticipant();
			
			holderAccountTO = new HolderAccountTO();
			holderAccountTO.setParticipantTO(participant.getIdParticipantPk());
			holderAccountTO.setHolderaccountGroupType(HolderAccountGroupType.ISSUER.getCode());
			holderAccountTO.setIdHolderPk(issuerLikeHolder.getIdHolderPk());
			
			HolderAccount holderAccount = accountsFacade.getHolderAccountComponentServiceFacade(holderAccountTO);
			
			if(holderAccount !=null){
				placementSegParticipaStruct.setHolderAccount(holderAccount);
				update(placementSegParticipaStruct);
				continue;
			}

			//Need to get correlative participant's accounts
			Query query = em.createQuery("SELECT MAX(ha.accountNumber) + 1 FROM HolderAccount ha WHERE ha.participant.idParticipantPk = :idParticipantPkParameter");
			query.setParameter("idParticipantPkParameter", participant.getIdParticipantPk());
			Integer correlative = 0;
			try{
				correlative = (Integer) query.getSingleResult();
				if(correlative == null){
					correlative = 0;
				}
				correlative ++ ;
			}catch(NoResultException ex){
				ex.toString();
			}
			//END - Need to get correlative participant's accounts
			

			//Get Correlative Code
			StringBuilder alternateCode = new StringBuilder();
			alternateCode.append(participant.getIdParticipantPk());
			alternateCode.append("-(");
			alternateCode.append(issuerLikeHolder.getIdHolderPk());
			alternateCode.append(")-");
			alternateCode.append(correlative);
			//END - Get Correlative Code
			
			//We need create HolderAccount
			holderAccount = new HolderAccount();
			holderAccount.setAccountGroup(HolderAccountGroupType.ISSUER.getCode());
			holderAccount.setAccountType(HolderAccountType.JURIDIC.getCode());
			holderAccount.setStateAccount(HolderAccountStatusType.ACTIVE.getCode());
			holderAccount.setRegistryDate(new Date());
			holderAccount.setRegistryUser(loggerUser.getUserName());
			holderAccount.setParticipant(participant);
			holderAccount.setAccountNumber(correlative);
			holderAccount.setAlternateCode(alternateCode.toString());
			holderAccount.setOpeningDate(CommonsUtilities.currentDate());
			
			List<HolderAccountDetail> holderAccountDetailList = new ArrayList<HolderAccountDetail>();
				HolderAccountDetail holderAccountDetail = new HolderAccountDetail();
				holderAccountDetail.setHolder(issuerLikeHolder);
				holderAccountDetail.setStateAccountDetail(HolderAccountStateDetailType.REGISTERED.getCode());
				holderAccountDetail.setRegistryUser(loggerUser.getUserName());
				holderAccountDetail.setRegistryDate(new Date());
				holderAccountDetail.setHolderAccount(holderAccount);
				holderAccountDetail.setIndRepresentative(ComponentConstant.ONE);
			holderAccountDetailList.add(holderAccountDetail);
			
			holderAccount.setHolderAccountDetails(holderAccountDetailList);
			
			//Save HolderAccount
			create(holderAccount);
			//End - Save HolderAccount
			//END - We need create HolderAccount
			placementSegParticipaStruct.setHolderAccount(holderAccount);//Asignamos la cuenta al Participante Colocador
			placementSegParticipaStruct.setDescription(holderAccount.getAlternateCode());//Asignamos el codigo alterno solo a las cuentas que han sido creadas en este proceso
			update(placementSegParticipaStruct);
		}
		for(PlacementSegmentDetail detail: placementSegment.getPlacementSegmentDetails()){
			detail.setPendingOperation(BigDecimal.ZERO);
			detail.setPendingAnnotation(BigDecimal.ZERO);
			update(detail);
		}
		update(placementSegment);
		
		Query query = em.createQuery("UPDATE Issuance issu SET issu.currentTranche = :currentTrancheParameter, " +
									 "issu.amountConfirmedSegments = :amounConfirmParam WHERE issu.idIssuanceCodePk = :idIssuanceCodePkParameter");
		query.setParameter("idIssuanceCodePkParameter", placementSegment.getIssuance().getIdIssuanceCodePk());
		query.setParameter("currentTrancheParameter", placementNumberAvailable);
		query.setParameter("amounConfirmParam", amountOnPlacementConfirmed);
		query.executeUpdate();
		
		/**
		 * Issue 2433
		 * If Issuance Type MIXED Update Security , subtract Physical Amount, and  
		 */
		if(IssuanceType.MIXED.getCode().equals(placementSegment.getIssuance().getIssuanceType())){
			updateSecurityAmountForSegment(placementSegment);
		}
		return placementSegment;
	}
	
	/**
	 * Gets the list placement segment service.
	 *
	 * @param placementSegmentTO the placement segment to
	 * @return the list placement segment service
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<PlacementSegment> getListPlacementSegmentService(PlacementSegmentTO placementSegmentTO) throws ServiceException{
		Map<String,Object> parameters = new HashMap<>();
		StringBuilder querySQL = new StringBuilder();
		StringBuilder whereSQL = new StringBuilder();
		querySQL.append("Select plac From PlacementSegment plac							");
		querySQL.append("Inner Join Fetch plac.issuance issu							");
		querySQL.append("Inner Join Fetch issu.issuer iss								");
		querySQL.append("Inner Join Fetch iss.holder hol								");
		whereSQL.append("Where 1 = 1													");
		
		if(placementSegmentTO.getIdIssuerPk()!=null){
			whereSQL.append("And	issu.issuer.idIssuerPk = :issuerParam					");
			parameters.put("issuerParam", placementSegmentTO.getIdIssuerPk());
		}
		
		if(placementSegmentTO.getIdIssuancePk()!=null){
			whereSQL.append("And	issu.idIssuanceCodePk = :issuanceParam					");
			parameters.put("issuanceParam", placementSegmentTO.getIdIssuancePk());
		}
		
		if(placementSegmentTO.getInitialDate()!=null){
			whereSQL.append("And	Trunc(plac.beginingDate) >= Trunc(:initDate)			");
			parameters.put("initDate", placementSegmentTO.getInitialDate());
		}
		
		if(placementSegmentTO.getFinalDate()!=null){
			whereSQL.append("And	Trunc(plac.closingDate) <= Trunc(:finalDate)			");
			parameters.put("finalDate", placementSegmentTO.getFinalDate());
		}
		
		if(placementSegmentTO.getPlacementSegmentNumber()!=null){
			whereSQL.append("And	plac.trancheNumber = :placNumber						");
			parameters.put("placNumber", placementSegmentTO.getPlacementSegmentNumber());
		}
		
		if(placementSegmentTO.getPlacementeSegmentState()!=null){
			whereSQL.append("And	plac.placementSegmentState = :placState					");
			parameters.put("placState", placementSegmentTO.getPlacementeSegmentState());
		}
		if(placementSegmentTO.getSecurity()!=null){
			if(placementSegmentTO.getSecurity().getIdSecurityCodePk()!=null){
				querySQL.append("Inner Join plac.placementSegmentDetails det					");
				querySQL.append("Inner Join det.security sec									");
				whereSQL.append("And	sec.idSecurityCodePk = :securityCode					");
				parameters.put("securityCode", placementSegmentTO.getSecurity().getIdSecurityCodePk());
			}
		}
		if(placementSegmentTO.getParticipant()!=null){
			if(placementSegmentTO.getParticipant().getIdParticipantPk()!=null){
				querySQL.append("Inner Join plac.placementSegParticipaStructs part				");
				querySQL.append("Inner Join part.participant partDet							");
				whereSQL.append("And	partDet.idParticipantPk = :participantPk				");
				parameters.put("participantPk", placementSegmentTO.getParticipant().getIdParticipantPk());
			}
		}
		if(placementSegmentTO.getIdPlacementeSegmentRefPk()!=null){
			whereSQL.append("And	plac.idPlacementSegmentPk < :idPlacementSegmentPk					");
			parameters.put("idPlacementSegmentPk", placementSegmentTO.getIdPlacementeSegmentRefPk());
		}
		return findListByQueryString(querySQL.toString().concat(whereSQL.toString()), parameters);
	}
	
	/**
	 * Gets the list placement segment service bean.
	 *
	 * @param placementSegmentTO the placement segment to
	 * @return the list placement segment service bean
	 * @throws ServiceException the service exception
	 */
	public List<PlacementSegment> getListPlacementSegmentServiceBean(PlacementSegmentTO placementSegmentTO) throws ServiceException{
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<PlacementSegment> criteriaQuery = criteriaBuilder.createQuery(PlacementSegment.class);
		Root<PlacementSegment> placementSegmentRoot = criteriaQuery.from(PlacementSegment.class);
		List<Predicate> criteriaParameters = new ArrayList<Predicate>();
		
		if(placementSegmentTO.getIdIssuerPk()!=null && Validations.validateIsPositiveNumber(placementSegmentTO.getIdIssuerPk().length())){
			Join<PlacementSegment,Issuance> issuanceJoin = placementSegmentRoot.join("issuance");
			Join<Issuance,Issuer> issuerJoin = issuanceJoin.join("issuer");
			criteriaParameters.add(criteriaBuilder.equal(issuerJoin.get("idIssuerPk"),placementSegmentTO.getIdIssuerPk()));
		}
		
		placementSegmentRoot.fetch("issuance");
		if(placementSegmentTO.getIdIssuancePk()!=null && Validations.validateIsPositiveNumber(placementSegmentTO.getIdIssuancePk().length())){
			Join<PlacementSegment,Issuance> issuanceJoin = placementSegmentRoot.join("issuance");
			criteriaParameters.add(criteriaBuilder.equal(issuanceJoin.get("idIssuanceCodePk"),placementSegmentTO.getIdIssuancePk()));
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(placementSegmentTO.getInitialDate()) && Validations.validateIsNotNullAndNotEmpty(placementSegmentTO.getFinalDate())){
			criteriaParameters.add(criteriaBuilder.greaterThanOrEqualTo(placementSegmentRoot.<Date>get("beginingDate"),placementSegmentTO.getInitialDate()));
			criteriaParameters.add(criteriaBuilder.lessThanOrEqualTo(placementSegmentRoot.<Date>get("closingDate"),placementSegmentTO.getFinalDate()));
		}
		
		if(placementSegmentTO.getPlacementSegmentNumber()!=null){
			criteriaParameters.add(criteriaBuilder.equal(placementSegmentRoot.get("trancheNumber"),placementSegmentTO.getPlacementSegmentNumber()));
		}
		
		if(placementSegmentTO.getBeginPlacementDate()!=null){
			criteriaParameters.add(criteriaBuilder.equal(placementSegmentRoot.<Date>get("beginingDate"),placementSegmentTO.getBeginPlacementDate()));
		}
		
		if(placementSegmentTO.getPlacementeSegmentState()!=null && Validations.validateIsPositiveNumber(placementSegmentTO.getPlacementeSegmentState())){
			criteriaParameters.add(criteriaBuilder.equal(placementSegmentRoot.get("placementSegmentState"),placementSegmentTO.getPlacementeSegmentState()));
		}
		criteriaQuery.where(criteriaBuilder.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()]))); 
		TypedQuery<PlacementSegment> placementSegmentCriteria = em.createQuery(criteriaQuery);
		
		try{
			return placementSegmentCriteria.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}
	
	/**
	 * Gets the placement segment service bean.
	 *
	 * @param placementSegmentTO the placement segment to
	 * @return the placement segment service bean
	 * @throws ServiceException the service exception
	 */
	public PlacementSegment getPlacementSegmentServiceBean(PlacementSegmentTO placementSegmentTO) throws ServiceException{
		
		StringBuilder querySQL = new StringBuilder();
		querySQL.append("Select plac From PlacementSegment plac						");
		querySQL.append("Inner Join Fetch plac.placementSegmentDetails psd			");
		querySQL.append("Inner Join Fetch plac.issuance issu						");
		querySQL.append("Inner Join Fetch issu.issuer iss							");
		querySQL.append("Inner Join Fetch iss.holder hol							");
		querySQL.append("where plac.idPlacementSegmentPk = :idPlacementSegmentPk	");
		querySQL.append("order by psd.security.idSecurityCodePk asc					");
		
		Query query = em.createQuery(querySQL.toString());		
		query.setParameter("idPlacementSegmentPk",placementSegmentTO.getIdPlacementeSegmentPk());
						
		PlacementSegment placementSegmentTemp = null;

		try{
			placementSegmentTemp = (PlacementSegment) query.getSingleResult();//  placementSegmentCriteria.getSingleResult();
			
			if(placementSegmentTO.getNeedIssuance()){
				IssuanceTO issuanceTO=new IssuanceTO();
				issuanceTO.setIdIssuanceCodePk(placementSegmentTO.getIdIssuancePk() );
				issuanceTO.setNeedPlaceSegments(BooleanType.YES.getBooleanValue());
				Issuance isu=issuanceServiceBean.findIssuanceServiceBean(issuanceTO);
				placementSegmentTemp.setIssuance(isu);
			}			
		} catch (NonUniqueResultException nure) {
			return null;
		}
		
		if(placementSegmentTO.getNeedIssuance()){
			placementSegmentTemp.getIssuance().getIssuer().getIdIssuerPk();
		}
		
		if(placementSegmentTO.getNeedParticipants()){
			StringBuilder partAccountDescription = new StringBuilder();
			for(PlacementSegParticipaStruct placementSegParticipaStruct: placementSegmentTemp.getPlacementSegParticipaStructs()){
				Long partCode = placementSegParticipaStruct.getParticipant().getIdParticipantPk();
				Long rntHolder = placementSegmentTemp.getIssuance().getIssuer().getHolder().getIdHolderPk();
				
				HolderAccountTO holderAccountTO = new HolderAccountTO();
				holderAccountTO.setParticipantTO(partCode);
				holderAccountTO.setIdHolderPk(rntHolder);
				holderAccountTO.setNeedBanks(Boolean.FALSE);
				holderAccountTO.setNeedParticipant(Boolean.FALSE);
				holderAccountTO.setHolderaccountGroupType(HolderAccountGroupType.ISSUER.getCode());
				HolderAccount issAccount = accountsFacade.getHolderAccountComponentServiceFacade(holderAccountTO);
				
				placementSegParticipaStruct.setIssuerAccount(issAccount);
				partAccountDescription.append(GeneralConstants.STR_BR);
				partAccountDescription.append(partCode).append(GeneralConstants.BLANK_SPACE).append(placementSegParticipaStruct.getParticipant().getDescription());
				if(issAccount!=null){
					partAccountDescription.append(GeneralConstants.DASH).append(issAccount.getAlternateCode());
				}
				placementSegmentTemp.setDescriptionParticipantStruc(partAccountDescription.toString());
				placementSegParticipaStruct.getParticipant().getIdParticipantPk();
				Participant objPart = this.find(Participant.class, placementSegParticipaStruct.getParticipant().getIdParticipantPk());
				Participant part=new Participant();
				part.setIdParticipantPk( placementSegParticipaStruct.getParticipant().getIdParticipantPk() );
				part.setMnemonic(objPart.getMnemonic());
				part.setDescription(objPart.getDescription());
				placementSegParticipaStruct.setParticipant( part );
			}
		}
		
		
		if(placementSegmentTO.getNeedSecurities()){
			BigDecimal placedSecuritiesBalance=BigDecimal.ZERO;
			for(PlacementSegmentDetail placementSegmentDetail: placementSegmentTemp.getPlacementSegmentDetails()){
				placementSegmentDetail.getSecurity().getIdSecurityCodePk();
				placedSecuritiesBalance=placedSecuritiesBalance.add( placementSegmentDetail.getSecurity().getDesmaterializedBalance() );
			}
			placementSegmentTemp.getIssuance().setPlacedSecuritiesBalance(placedSecuritiesBalance.intValue());
		}
		return placementSegmentTemp;
	}

	/**
	 * Modify placement segment service bean.
	 *
	 * @param placementSegment the placement segment
	 * @return the placement segment
	 * @throws ServiceException the service exception
	 */
	public PlacementSegment modifyPlacementSegmentServiceBean(PlacementSegment placementSegment) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		
		//issue 1034
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());		
		
		PlacementSegmentHy placementSegmentHistory = new PlacementSegmentHy();
		placementSegmentHistory.setPlacementSegment(placementSegment);
		if(Validations.validateIsNotNullAndNotEmpty(placementSegment.getTrancheNumber()))
			placementSegmentHistory.setTrancheNumber(BigDecimal.valueOf(placementSegment.getTrancheNumber()));
		if(Validations.validateIsNotNullAndNotEmpty(placementSegment.getPlacementSegmentState()))
			placementSegmentHistory.setStatePlacementSegment(BigDecimal.valueOf(placementSegment.getPlacementSegmentState()));
		placementSegmentHistory.setRegistryUser(placementSegment.getRegistryUser());
		placementSegmentHistory.setRegistryDate(placementSegment.getRegistryDate());
		placementSegmentHistory.setActionDate(placementSegment.getActionDate());
		if(Validations.validateIsNotNullAndNotEmpty(placementSegment.getActionMotive()))
			placementSegmentHistory.setActionMotive(BigDecimal.valueOf(placementSegment.getActionMotive()));
		placementSegmentHistory.setOtherActionMotive(placementSegment.getOtherActionMotive());
		placementSegmentHistory.setRequestType(BigDecimal.valueOf(PlacementSegmentHistoryType.MODIFIED.getCode()));
		placementSegmentHistory.setBeginingDate(placementSegment.getBeginingDate());
		placementSegmentHistory.setClosingDate(placementSegment.getClosingDate());
		placementSegmentHistory.setPlacementTerm(placementSegment.getPlacementTerm());
		placementSegmentHistory.setAmountToPlace(placementSegment.getAmountToPlace());
		placementSegmentHistory.setPlacedAmount(placementSegment.getPlacedAmount());
		placementSegmentHistory.setIdIssuanceCodeFk(placementSegment.getIssuance().getIdIssuanceCodePk());
		placementSegmentHistory.setFixedAmount(placementSegment.getFixedAmount());
		placementSegmentHistory.setRequestFixedAmount(placementSegment.getRequestFixedAmount());
		placementSegmentHistory.setFloatingAmount(placementSegment.getFloatingAmount());
		placementSegmentHistory.setRequestFloatingAmount(placementSegment.getRequestFloatingAmount());
		placementSegmentHistory.setPlacementSegmentType(placementSegment.getPlacementSegmentType());
		placementSegmentHistory.setLastModifyApp(loggerUser.getUserAction().getIdPrivilegeModify());
		placementSegmentHistory.setLastModifyDate(loggerUser.getAuditTime());
		placementSegmentHistory.setLastModifyIp(loggerUser.getIpAddress());
		placementSegmentHistory.setLastModifyUser(loggerUser.getUserName());		
		
		create(placementSegmentHistory);
		
		if(Validations.validateListIsNotNullAndNotEmpty(placementSegment.getPlacementSegmentDetails()))
		{
			PlacementSegmentDetail objPlacementSegmentDetail = null;
			for(int i=0;i<placementSegment.getPlacementSegmentDetails().size();i++)
			{
				objPlacementSegmentDetail = placementSegment.getPlacementSegmentDetails().get(i);
				PlacementSegmentDetailHy placementSegmentDetailHistory = new PlacementSegmentDetailHy();
				placementSegmentDetailHistory.setPlacementSegmentHy(placementSegmentHistory);
				placementSegmentDetailHistory.setIdSecurityCodeFk(objPlacementSegmentDetail.getSecurity().getIdSecurityCodePk());
				if(Validations.validateIsNotNullAndNotEmpty(objPlacementSegmentDetail.getStatePlacementSegmentDet()))
					placementSegmentDetailHistory.setStatePlacementSegmentDet(BigDecimal.valueOf(objPlacementSegmentDetail.getStatePlacementSegmentDet()));
				placementSegmentDetailHistory.setDescription(objPlacementSegmentDetail.getDescription());
				placementSegmentDetailHistory.setPlacedAmount(objPlacementSegmentDetail.getPlacedAmount());
				placementSegmentDetailHistory.setRegistryUser(objPlacementSegmentDetail.getRegistryUser());
				placementSegmentDetailHistory.setRegistryDate(objPlacementSegmentDetail.getRegistryDate());
				placementSegmentDetailHistory.setPlacementSegmentTypeHy(objPlacementSegmentDetail.getPlacementSegmentDetType());
				placementSegmentDetailHistory.setRequestAmount(objPlacementSegmentDetail.getRequestAmount());
				placementSegmentDetailHistory.setLastModifyApp(loggerUser.getUserAction().getIdPrivilegeModify());
				placementSegmentDetailHistory.setLastModifyDate(loggerUser.getAuditTime());
				placementSegmentDetailHistory.setLastModifyIp(loggerUser.getIpAddress());
				placementSegmentDetailHistory.setLastModifyUser(loggerUser.getUserName());	
				create(placementSegmentDetailHistory);
			}
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(placementSegment.getPlacementSegParticipaStructs()))
		{
			PlacementSegParticipaStruct objPlacementSegParticipaStruct=null;
			for(int i=0;i<placementSegment.getPlacementSegParticipaStructs().size();i++)
			{
				objPlacementSegParticipaStruct= placementSegment.getPlacementSegParticipaStructs().get(i);
				PlacementSegParticStrucHy placementSegParticStrucHistory = new PlacementSegParticStrucHy();
				placementSegParticStrucHistory.setPlacementSegmentHy(placementSegmentHistory);
				placementSegParticStrucHistory.setDescription(objPlacementSegParticipaStruct.getDescription());
				placementSegParticStrucHistory.setRegistryUser(objPlacementSegParticipaStruct.getRegistryUser());
				placementSegParticStrucHistory.setRegistryDate(objPlacementSegParticipaStruct.getRegistryDate());
				if(Validations.validateIsNotNullAndNotEmpty(objPlacementSegParticipaStruct.getStatePlacSegParticipant()))
					placementSegParticStrucHistory.setStatePlacSegParticipant(BigDecimal.valueOf(objPlacementSegParticipaStruct.getStatePlacSegParticipant()));
				placementSegParticStrucHistory.setHolderAccount(objPlacementSegParticipaStruct.getHolderAccount());
				placementSegParticStrucHistory.setLastModifyApp(loggerUser.getUserAction().getIdPrivilegeModify());
				placementSegParticStrucHistory.setLastModifyDate(loggerUser.getAuditTime());
				placementSegParticStrucHistory.setLastModifyIp(loggerUser.getIpAddress());
				placementSegParticStrucHistory.setLastModifyUser(loggerUser.getUserName());
				create(placementSegParticStrucHistory);
			}
		}
				
		PlacementSegmentTO placementSegmentTO = new PlacementSegmentTO();
		placementSegmentTO.setIdPlacementeSegmentPk(placementSegment.getIdPlacementSegmentPk());
		
		PlacementSegment placementSegmentToModified = this.getPlacementSegmentServiceBean(placementSegmentTO);
		placementSegmentToModified.getPlacementSegmentDetails().removeAll(placementSegmentToModified.getPlacementSegmentDetails());
		placementSegmentToModified.getPlacementSegParticipaStructs().removeAll(placementSegmentToModified.getPlacementSegParticipaStructs());
		
		placementSegmentToModified.getPlacementSegmentDetails().addAll(placementSegment.getPlacementSegmentDetails());
		placementSegmentToModified.getPlacementSegParticipaStructs().addAll(placementSegment.getPlacementSegParticipaStructs());
		placementSegmentToModified.setAmountToPlace(placementSegment.getAmountToPlace());
		placementSegmentToModified.setBeginingDate(placementSegment.getBeginingDate());
		placementSegmentToModified.setClosingDate(placementSegment.getClosingDate());
		
		if(placementSegment.getPlacementSegmentState().equals(PlacementSegmentStateType.REGISTERED.getCode())
				|| placementSegment.getPlacementSegmentState().equals(PlacementSegmentStateType.OPENED.getCode())){//Si el tramo es registrado
			if(placementSegment.getIssuance().getPlacementType().equals(SecurityPlacementType.FIXED.getCode())){//Si es fijo, el monto fijo sera el mismo del tramo
				placementSegmentToModified.setFixedAmount(placementSegment.getAmountToPlace());
				placementSegmentToModified.setFloatingAmount(BigDecimal.ZERO);
			}else if(placementSegment.getIssuance().getPlacementType().equals(SecurityPlacementType.FLOATING.getCode())){//Si es flotante, el monto floatante sera el mismo del tramo
				placementSegmentToModified.setFixedAmount(BigDecimal.ZERO);
				placementSegmentToModified.setFloatingAmount(placementSegment.getAmountToPlace());
			}else if(placementSegment.getIssuance().getPlacementType().equals(SecurityPlacementType.MIXED.getCode())){
				placementSegment.setFixedAmount(placementSegment.getPlacedAmountFixedFromDetail());//El monto fijo sera la sumatoria de los valores ingresados
				BigDecimal floatingAmount = placementSegment.getPlacedAmountFloatingFromDetail();
				placementSegmentToModified.setFloatingAmount(floatingAmount);
				if(floatingAmount==null){//Si es null, el tramo aun no tiene valores flotantes
					placementSegmentToModified.setPlacementSegmentType(SecurityPlacementType.FIXED.getCode());
					placementSegmentToModified.setFloatingAmount(BigDecimal.ZERO);
					placementSegmentToModified.setFixedAmount(placementSegment.getFixedAmount());
				}else if(placementSegment.getFixedAmount()!=null && !placementSegment.getFixedAmount().equals(BigDecimal.ZERO)){
					placementSegmentToModified.setPlacementSegmentType(SecurityPlacementType.FLOATING.getCode());
					placementSegmentToModified.setFixedAmount(placementSegment.getFixedAmount());
				}
			}
			if(!(placementSegmentToModified.getFixedAmount().add(placementSegmentToModified.getFloatingAmount())).equals(placementSegmentToModified.getAmountToPlace())){//Validamos la sumatoria
				throw new ServiceException();//la sumatoria no coinciden, error de validaciones
			}
			placementSegmentToModified.setRequestFixedAmount(BigDecimal.ZERO);//El monto en solicitudes es 0
			placementSegmentToModified.setRequestFloatingAmount(BigDecimal.ZERO);//El monto en solicitudes es 0
		}
		
		update(placementSegmentToModified);
		return placementSegment;
	}
	
	/**
	 * Can register placement segment.
	 *
	 * @param placementSegmentTO the placement segment to
	 * @return the placement segment
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<PlacementSegment> getLastPlacementSegmentServiceBean(PlacementSegmentTO placementSegmentTO) throws ServiceException{
		String query = "SELECT placSeg FROM PlacementSegment placSeg " +
						"WHERE placSeg.placementSegmentState IN (:stateRegisterParam, :stateConfirmParam, :stateOpenParam, :stateSuspendParam, :stateCloseParam) AND " +
						"	   placSeg.issuance.idIssuanceCodePk = :issuerCodeParam " +
						"ORDER BY  placSeg.idPlacementSegmentPk DESC";
		Query queryJpa = em.createQuery(query);
		queryJpa.setParameter("stateRegisterParam", PlacementSegmentStateType.REGISTERED.getCode());
		queryJpa.setParameter("stateConfirmParam", PlacementSegmentStateType.CONFIRMED.getCode());
		queryJpa.setParameter("stateOpenParam", PlacementSegmentStateType.OPENED.getCode());
		queryJpa.setParameter("stateSuspendParam", PlacementSegmentStateType.SUSPENDED.getCode());
		queryJpa.setParameter("stateCloseParam", PlacementSegmentStateType.CLOSED.getCode());
		queryJpa.setParameter("issuerCodeParam", placementSegmentTO.getIdIssuancePk());
		
		List<PlacementSegment> placementSegmentList = null;
		try{
			placementSegmentList = queryJpa.getResultList();
		}catch(NoResultException ex){
			return null;
		}
		if(placementSegmentList==null || placementSegmentList.isEmpty()){
			return null;
		}
		return placementSegmentList;
	}
	
	/**
	 * Get Posterior Placement Segment Service.
	 *
	 * @param IdIssuanceCode Id Issuance Code
	 * @param trancheNumber tranche Number
	 * @return object Placement Segment
	 * @throws ServiceException the service exception
	 */
	public PlacementSegment getPosteriorPlacementSegmentService(String IdIssuanceCode,Integer trancheNumber)throws ServiceException{
		String query = "SELECT placSeg FROM PlacementSegment placSeg   " +
				"WHERE (placSeg.placementSegmentState = :stateRegisterParam or placSeg.placementSegmentState = :stateConfirmParam)"
				+ "  AND placSeg.trancheNumber = :trancheNumber   " +
				"	AND     placSeg.issuance.idIssuanceCodePk = :issuerCodeParam " ;
				Query queryJpa = em.createQuery(query);
			queryJpa.setParameter("stateRegisterParam", PlacementSegmentStateType.REGISTERED.getCode());
			queryJpa.setParameter("stateConfirmParam", PlacementSegmentStateType.CONFIRMED.getCode());
			queryJpa.setParameter("trancheNumber", Integer.valueOf(trancheNumber.intValue()+1));
			queryJpa.setParameter("issuerCodeParam", IdIssuanceCode);
			
			PlacementSegment placementSegmentList = null;
			try{
				placementSegmentList =(PlacementSegment) queryJpa.getSingleResult();
			}catch(NoResultException ex){
				return null;
			}
			if(Validations.validateIsNullOrEmpty(placementSegmentList)){
				return null;
			}
			return placementSegmentList;
	}
	/**
	 * Open placement segment service bean.
	 * this method is invocated by Batch Process
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public void openPlacementSegmentServiceBean() throws ServiceException{
		String query = "SELECT DISTINCT(placSeg.issuance) FROM PlacementSegment placSeg " +
					   "WHERE " +
					   "	  placSeg.placementSegmentState IN (:placSegmentConfirmParam)";
		Query queryIssuance = em.createQuery(query);
		queryIssuance.setParameter("placSegmentConfirmParam", PlacementSegmentStateType.CONFIRMED.getCode());
		List<Issuance> issuanceList= (List<Issuance>) queryIssuance.getResultList();
	
		jumpIssuance:
		for(Issuance issuance: issuanceList){
			
			String queryPlacement = "SELECT placSeg FROM PlacementSegment placSeg " +
									 "WHERE placSeg.issuance.idIssuanceCodePk = :idIssuanceParam AND " +
									 "		placSeg.placementSegmentState IN (:placSegmentConfirmParam)" ;
			Query queryPlacementJpa = em.createQuery(queryPlacement);
			queryPlacementJpa.setParameter("idIssuanceParam", issuance.getIdIssuanceCodePk());
			queryPlacementJpa.setParameter("placSegmentConfirmParam", PlacementSegmentStateType.CONFIRMED.getCode());
			
			List<PlacementSegment> placementSegmentList = (List<PlacementSegment>) queryPlacementJpa.getResultList();
			for(PlacementSegment placSegmentEvaluate: placementSegmentList){
				PlacementSegment placSegmentUpdate = null;
				if(placSegmentEvaluate.getPlacementSegmentState().equals(PlacementSegmentStateType.OPENED.getCode())){
					continue jumpIssuance;
				}else if(placSegmentEvaluate.getPlacementSegmentState().equals(PlacementSegmentStateType.SUSPENDED.getCode())){
					//Do something in the future
					continue;
				}else if(placSegmentEvaluate.getPlacementSegmentState().equals(PlacementSegmentStateType.CONFIRMED.getCode())){
					if((CommonsUtilities.currentDate().after(placSegmentEvaluate.getBeginingDate()) || CommonsUtilities.currentDate().equals(placSegmentEvaluate.getBeginingDate())) && 
					  (CommonsUtilities.currentDate().before(placSegmentEvaluate.getClosingDate()) || CommonsUtilities.currentDate().equals(placSegmentEvaluate.getClosingDate()))){//Si la fecha actual es antes que la fecha de cierre de tramo.
						placSegmentEvaluate.setPlacementSegmentState(PlacementSegmentStateType.OPENED.getCode());//Aperturamos el tramo
						placSegmentEvaluate.setOpenDate(CommonsUtilities.currentDateTime());
						placSegmentUpdate = placSegmentEvaluate;
						update(placSegmentUpdate);
					}
				}
			}
		}
	}
	
	/**
	 * Close placement segment service bean.
	 * this method is invocated by Batch Process
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public void closePlacementSegmentServiceBean() throws ServiceException{
		String query = "SELECT DISTINCT(placSeg.issuance) FROM PlacementSegment placSeg ";
		Query queryJpaIssuance = em.createQuery(query);
		List<Issuance> issuanceList= (List<Issuance>) queryJpaIssuance.getResultList();
	
		for(Issuance issuance: issuanceList){
			String queryPlacement = "SELECT placSeg FROM PlacementSegment placSeg " +
									 "WHERE placSeg.issuance.idIssuanceCodePk = :idIssuanceParam AND " +
									 "		placSeg.placementSegmentState IN (:placSegmentOpenParam, :placSegmentSuspendParam, :placSecmentConfirmParam)" ;
			Query queryJpa = em.createQuery(queryPlacement);
			queryJpa.setParameter("idIssuanceParam", issuance.getIdIssuanceCodePk());
			queryJpa.setParameter("placSegmentOpenParam", PlacementSegmentStateType.OPENED.getCode());
			queryJpa.setParameter("placSegmentSuspendParam", PlacementSegmentStateType.SUSPENDED.getCode());
			queryJpa.setParameter("placSecmentConfirmParam", PlacementSegmentStateType.CONFIRMED.getCode());
			
			List<PlacementSegment> placementSegmentList = (List<PlacementSegment>) queryJpa.getResultList();
			for(PlacementSegment placSegmentEvaluate : placementSegmentList){
				if(placSegmentEvaluate.getClosingDate()!=null && CommonsUtilities.currentDate().after(placSegmentEvaluate.getClosingDate())){
					placSegmentEvaluate.setPlacementSegmentState(PlacementSegmentStateType.CLOSED.getCode());
					update(placSegmentEvaluate);
				}
			}
		}
	}
	
	/**
	 * Reject placement segment service bean.
	 * this method is invocated by Batch Process
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings({ "unchecked"})
	public void rejectPlacementSegmentServiceBean() throws ServiceException{
		String queryPlacement = "SELECT placSeg FROM PlacementSegment placSeg where placSeg.placementSegmentState IN (:placSegmentRegisteredParam)" ;
		Query queryJpa = em.createQuery(queryPlacement);
		queryJpa.setParameter("placSegmentRegisteredParam", PlacementSegmentStateType.REGISTERED.getCode());
		
		List<PlacementSegment> placementSegmentList = (List<PlacementSegment>) queryJpa.getResultList();
		for(PlacementSegment placSegmentEvaluate : placementSegmentList){
			if(CommonsUtilities.currentDate().after(placSegmentEvaluate.getBeginingDate())){//Si la fecha de inicio es antes que la actual
				placSegmentEvaluate.setPlacementSegmentState(PlacementSegmentStateType.REJECT.getCode());
				update(placSegmentEvaluate);
			}
		}
	}
	
	/**
	 * Gets the participant secundary report.
	 *
	 * @param filter the filter
	 * @return the participant secundary report
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Participant> getParticipantSecundaryReport(Participant filter) throws ServiceException{
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select DISTINCT new com.pradera.model.accounts.Participant(P.idParticipantPk," +//0
							   "P.description," +//1
							   "P.mnemonic) " +//2
					   " 	From Participant P " +
					   "	Inner join P.participantMechanisms PMEC" +
					   "	WHERE PMEC.mechanismModality.id.idNegotiationModalityPk IN :modalityParam AND" +
					   "		  P.state = :stateParam AND" +
					   "		  P.accountType = :accountType AND" +
					   "		  PMEC.stateParticipantMechanism = :stateMechanParam" +
					   "	ORDER BY P.idParticipantPk ASC");
		Query query = em.createQuery(sbQuery.toString());
		List<Long> modalityPrimaryPlac = Arrays.asList(NegotiationModalityType.PRIMARY_PLACEMENT_EQUITIES.getCode(),NegotiationModalityType.PRIMARY_PLACEMENT_FIXED_INCOME.getCode());
		query.setParameter("modalityParam", modalityPrimaryPlac);
		query.setParameter("accountType", ParticipantType.DIRECT.getCode());
		query.setParameter("stateParam", filter.getState());
		query.setParameter("stateMechanParam", ParticipantMechanismStateType.REGISTERED.getCode());
		
		try{
			return query.getResultList();
		}catch(NoResultException ex){
			return new ArrayList<Participant>();
		}
	}
	
	/**
	 * Issue 2433
	 * In the Issuance Type MIXED  
	 * Update The Physical Amount the Security, after Confirm Placement Segment.
	 *
	 * @param placementSegment the placement segment
	 */
	public void updateSecurityAmountForSegment(PlacementSegment placementSegment) throws ServiceException{
		
		
		for (PlacementSegmentDetail placSegDetail : placementSegment.getPlacementSegmentDetails()) {
			Security security=placSegDetail.getSecurity();
			BigDecimal balanceAdd=CommonsUtilities.divideTwoDecimal(placSegDetail.getPlacedAmount(), security.getCurrentNominalValue(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
			BigDecimal physicalBalance = CommonsUtilities.subtractTwoDecimal(security.getPhysicalBalance(), balanceAdd, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
			BigDecimal circulationBalance = CommonsUtilities.subtractTwoDecimal(security.getCirculationBalance(), balanceAdd, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
			BigDecimal circulationAmount = CommonsUtilities.subtractTwoDecimal(security.getCirculationAmount(), placSegDetail.getPlacedAmount(), GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
			
			Object[] params= new Object[2];
			params[0]= security.getIdSecurityCodePk();
			if (BigDecimal.ZERO.compareTo(physicalBalance) > 0 ) {
				params[1]= physicalBalance;
				throw new ServiceException(ErrorServiceType.PLACEMENT_SECURITES_NEGATIVE_PHYSICAL_BALANCE, params);
			}
			
			StringBuilder sb=new StringBuilder();
			sb.append("UPDATE Security sec ").
				append("	SET sec.physicalBalance = :physicalBalance, ").
				append("	sec.circulationAmount= :circulationAmount, ").
				append("	sec.circulationBalance= :circulationBalance").
				append(" WHERE sec.idSecurityCodePk= :idSecurityCodePk");
			Query query = em.createQuery(sb.toString());
			query.setParameter("physicalBalance", physicalBalance);
			query.setParameter("circulationAmount", circulationAmount);
			query.setParameter("circulationBalance", circulationBalance);
			query.setParameter("idSecurityCodePk", security.getIdSecurityCodePk());
			query.executeUpdate();

		}
				
	}
	
	/**
	 * Gets the placement from participant or security.
	 *
	 * @param participantCode the participant code
	 * @param securityCode the security code
	 * @return the placement from participant or security
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public PlacementSegment getPlacementFromParticipantOrSecurity(Long participantCode, String securityCode) throws ServiceException{
		Map<String,Object> parameters = new HashMap<>();
		StringBuilder querySql = new StringBuilder();
		querySql.append("Select plac.idPlacementSegmentPk, plac.placementSegmentState,	");
		querySql.append("		plac.pendingOperation,plac.pendingAnnotation,			");
		querySql.append("		sec.idPlacementSegmentDetPk,sec.pendingOperation,		");
		querySql.append("		sec.pendingAnnotation,sec.security.idSecurityCodePk		");
		querySql.append("From PlacementSegment plac										");
		querySql.append("Inner Join plac.placementSegmentDetails sec					");
		querySql.append("Inner Join plac.placementSegParticipaStructs part				");
		querySql.append("Where part.participant.idParticipantPk = :participantCode		");
		querySql.append("	   And sec.security.idSecurityCodePk = :securityCode		");
		querySql.append("	   And plac.placementSegmentState In(:placStates)			");

		parameters.put("securityCode", securityCode);
		parameters.put("participantCode", participantCode);
		parameters.put("placStates", Arrays.asList(PlacementSegmentStateType.OPENED.getCode(),
												   PlacementSegmentStateType.SUSPENDED.getCode(),
												   PlacementSegmentStateType.CLOSED.getCode()));
		
		/**Adding some validation whenever the list has more than one element*/
		try{
			List<Object[]> placementList = findListByQueryString(querySql.toString(), parameters);
			if(placementList!=null && !placementList.isEmpty()){
				if(placementList.size() != ComponentConstant.ONE){
					for(Object[] placement: placementList){
						PlacementSegment placement_ = populatePlacementSegment(placement);
						if(placement_.getPlacementSegmentState().equals(PlacementSegmentStateType.OPENED.getCode())){
							return placement_;
						}
					}
				}
				return populatePlacementSegment(placementList.get(ComponentConstant.ZERO));
			}
		}catch(NoResultException ex){
			throw new ServiceException();
		}
		throw new ServiceException();
	}
	
	/**
	 * Populate placement segment.
	 *
	 * @param dataToCollect the data to collect
	 * @return the placement segment
	 */
	public PlacementSegment populatePlacementSegment(Object[] dataToCollect){
		/**Creating new object of placement segment*/
		PlacementSegment placement = new PlacementSegment();
		placement.setIdPlacementSegmentPk((Long) dataToCollect[0]);
		placement.setPlacementSegmentState((Integer) dataToCollect[1]);
		placement.setPendingOperation((BigDecimal) dataToCollect[2]);
		placement.setPendingAnnotation((BigDecimal) dataToCollect[3]);
		
		/**Creating new object of placement detail*/
		PlacementSegmentDetail placementDetail = new PlacementSegmentDetail();
		placementDetail.setIdPlacementSegmentDetPk((Long) dataToCollect[4]);
		placementDetail.setPendingOperation((BigDecimal) dataToCollect[5]);
		placementDetail.setPendingAnnotation((BigDecimal) dataToCollect[6]);
		placementDetail.setSecurity(new Security((String) dataToCollect[7]));
		
		/**relating objects*/
		placement.setPlacementSegmentDetails(new ArrayList<PlacementSegmentDetail>());
		placement.getPlacementSegmentDetails().add(placementDetail);
		
		return placement;
	}
	
	/**
	 * Update pending operation.
	 *
	 * @param placementSegment the placement segment
	 * @param operation the operation
	 * @param quantity the quantity
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updatePendingOperation(PlacementSegment placementSegment, long operation, BigDecimal quantity, LoggerUser loggerUser) throws ServiceException{
		BigDecimal placementOperation = placementSegment.getPendingOperation();
		/**Iterate all placement segment details*/
		for(PlacementSegmentDetail detail: placementSegment.getPlacementSegmentDetails()){
			BigDecimal detailOperation = detail.getPendingOperation();
			if(operation == ComponentConstant.SUM){
				placementOperation = placementOperation.add(quantity);
				detailOperation	   = detailOperation.add(quantity);
			}else if(operation == ComponentConstant.SUBTRACTION){
				placementOperation = placementOperation.subtract(quantity);
				detailOperation	   = detailOperation.subtract(quantity);
			}
			
			/**Update placement segment detail*/
			updatePlacementSegmentDetail(detailOperation, null, detail.getIdPlacementSegmentDetPk(), loggerUser);
		}
		
		/**Update placement segment detail*/
		updatePlacementSegment(placementOperation, null, placementSegment.getIdPlacementSegmentPk(), loggerUser);
	}
	
	/**
	 * Update pending annotation.
	 *
	 * @param placementSegment the placement segment
	 * @param operation the operation
	 * @param quantity the quantity
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updatePendingAnnotation(PlacementSegment placementSegment, long operation, BigDecimal quantity, LoggerUser loggerUser) throws ServiceException{
		BigDecimal placementOperation = placementSegment.getPendingAnnotation();
		/**Iterate all placement segment details*/
		for(PlacementSegmentDetail detail: placementSegment.getPlacementSegmentDetails()){
			BigDecimal detailOperation = detail.getPendingAnnotation();
			if(operation == ComponentConstant.SUM){
				placementOperation = placementOperation.add(quantity);
				detailOperation	   = detailOperation.add(quantity);
			}else if(operation == ComponentConstant.SUBTRACTION){
				placementOperation = placementOperation.subtract(quantity);
				detailOperation	   = detailOperation.subtract(quantity);
			}
			
			/**Update placement segment detail*/
			updatePlacementSegmentDetail(null, detailOperation, detail.getIdPlacementSegmentDetPk(), loggerUser);
		}
		
		/**Update placement segment detail*/
		updatePlacementSegment(null, placementOperation, placementSegment.getIdPlacementSegmentPk(), loggerUser);
	}
	
	/**
	 * Update placement segment detail.
	 *
	 * @param pendingOperation the pending operation
	 * @param pendingAnnotation the pending annotation
	 * @param detailPk the detail pk
	 * @param logUsser the log usser
	 * @throws ServiceException the service exception
	 */
	public void updatePlacementSegmentDetail(BigDecimal pendingOperation, BigDecimal pendingAnnotation, Long detailPk, LoggerUser logUsser) throws ServiceException{
		/**Adding some validations*/
		if(pendingAnnotation!=null && pendingAnnotation.compareTo(BigDecimal.ZERO)<ComponentConstant.ZERO){
			throw new ServiceException(ErrorServiceType.NEGATIVE_BALANCE);
		}
		if(pendingOperation!=null && pendingOperation.compareTo(BigDecimal.ZERO)<ComponentConstant.ZERO){
			throw new ServiceException(ErrorServiceType.NEGATIVE_BALANCE);
		}
		
		/**Begin of build the query to do update*/
		Map<String,Object> parameters = new HashMap<>();
		StringBuilder updateQuery = new StringBuilder();
		updateQuery.append("Update PlacementSegmentDetail 						");
		
		if(pendingOperation!=null){
			updateQuery.append("Set pendingOperation =	:pendOperation			");
			parameters.put("pendOperation", pendingOperation);
		}
		if(pendingAnnotation!=null){
			updateQuery.append("Set pendingAnnotation =	:pendAnnotation			");
			parameters.put("pendAnnotation", pendingAnnotation);
		}
		
		updateQuery.append(" ,lastModifyApp = :lastModifyApp 					");
		updateQuery.append(" ,lastModifyDate = :lastModifyDate 					");
		updateQuery.append(" ,lastModifyIp = :lastModifyIp 						");
		updateQuery.append(" ,lastModifyUser = :lastModifyUser 					");
		updateQuery.append("Where idPlacementSegmentDetPk = :detailPk			");
		
		parameters.put("lastModifyApp", logUsser.getIdPrivilegeOfSystem());
		parameters.put("lastModifyDate", logUsser.getAuditTime());
		parameters.put("lastModifyIp", logUsser.getIpAddress());
		parameters.put("lastModifyUser", logUsser.getUserName());
		parameters.put("detailPk", detailPk);
		
		updateByQuery(updateQuery.toString(), parameters);
		flushTransaction();
	}
	
	public void updatePlacementRequestBalance(BigDecimal stockQuantity, Long detailPk, LoggerUser logUsser) throws ServiceException{
		/**Begin of build the query to do update*/
		Map<String,Object> parameters = new HashMap<>();
		StringBuilder updateQuery = new StringBuilder();
		updateQuery.append("Update PlacementSegmentDetail 							");
		
		updateQuery.append("Set requestBalance = requestBalance + :stockQuantitty	");
		parameters.put("stockQuantitty", stockQuantity);
		
		updateQuery.append(" ,lastModifyApp = :lastModifyApp 						");
		updateQuery.append(" ,lastModifyDate = :lastModifyDate 						");
		updateQuery.append(" ,lastModifyIp = :lastModifyIp 							");
		updateQuery.append(" ,lastModifyUser = :lastModifyUser 						");
		updateQuery.append("Where idPlacementSegmentDetPk = :detailPk				");
		
		parameters.put("lastModifyApp", logUsser.getIdPrivilegeOfSystem());
		parameters.put("lastModifyDate", logUsser.getAuditTime());
		parameters.put("lastModifyIp", logUsser.getIpAddress());
		parameters.put("lastModifyUser", logUsser.getUserName());
		parameters.put("detailPk", detailPk);
		
		updateByQuery(updateQuery.toString(), parameters);
		flushTransaction();
	}
	
	/**
	 * Update placement segment.
	 *
	 * @param pendingOperation the pending operation
	 * @param pendingAnnotation the pending annotation
	 * @param placementPk the placement pk
	 * @param logUsser the log usser
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void updatePlacementSegment(BigDecimal pendingOperation, BigDecimal pendingAnnotation, Long placementPk, LoggerUser logUsser) throws ServiceException{
		/**Adding some validations*/
		if(pendingAnnotation!=null && pendingAnnotation.compareTo(BigDecimal.ZERO)<ComponentConstant.ZERO){
			throw new ServiceException(ErrorServiceType.NEGATIVE_BALANCE);
		}
		if(pendingOperation!=null && pendingOperation.compareTo(BigDecimal.ZERO)<ComponentConstant.ZERO){
			throw new ServiceException(ErrorServiceType.NEGATIVE_BALANCE);
		}
		
		/**Begin of build the query to do update*/
		Map<String,Object> parameters = new HashMap<>();
		StringBuilder updateQuery = new StringBuilder();
		updateQuery.append("Update PlacementSegment 							");
		
		if(pendingOperation!=null){
			updateQuery.append("Set pendingOperation =	:pendOperation			");
			parameters.put("pendOperation", pendingOperation);
		}
		if(pendingAnnotation!=null){
			updateQuery.append("Set pendingAnnotation =	:pendAnnotation			");
			parameters.put("pendAnnotation", pendingAnnotation);
		}
		
		updateQuery.append(" ,lastModifyApp = :lastModifyApp 					");
		updateQuery.append(" ,lastModifyDate = :lastModifyDate 					");
		updateQuery.append(" ,lastModifyIp = :lastModifyIp 						");
		updateQuery.append(" ,lastModifyUser = :lastModifyUser 					");
		updateQuery.append("Where idPlacementSegmentPk = :placementPk			");

		parameters.put("placementPk", placementPk);
		parameters.put("lastModifyDate", logUsser.getAuditTime());
		parameters.put("lastModifyIp", logUsser.getIpAddress());
		parameters.put("lastModifyUser", logUsser.getUserName());
		parameters.put("lastModifyApp", logUsser.getIdPrivilegeOfSystem());
		
		updateByQuery(updateQuery.toString(), parameters);
		flushTransaction();
	}
	
	/**
	 * Reopen Request
	 * Issue 1273
	 * @param placementSegment the placement segment
	 * @return the placement segment
	 * @throws ServiceException the service exception
	 */
	public PlacementSegment reopenRequestPlacementSegmentServiceBean(PlacementSegment placementSegment) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		
		PlacementSegmentTO placementSegmentTO = new PlacementSegmentTO();
		placementSegmentTO.setIdPlacementeSegmentPk(placementSegment.getIdPlacementSegmentPk());
		
		PlacementSegment placementSegmentToModified = this.getPlacementSegmentServiceBean(placementSegmentTO);
		placementSegmentToModified.setReopenRequest(1);
		placementSegmentToModified.setLastModifyApp(loggerUser.getUserAction().getIdPrivilegeRegister());
		placementSegmentToModified.setLastModifyDate(loggerUser.getAuditTime());
		placementSegmentToModified.setLastModifyIp(loggerUser.getIpAddress());
		placementSegmentToModified.setLastModifyUser(loggerUser.getUserName());
		
		em.merge(placementSegmentToModified);
		return placementSegment;
	}
	/**
	 * Reopen
	 * Issue 1273
	 * @param placementSegment the placement segment
	 * @return the placement segment
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public PlacementSegment reopenPlacementSegmentServiceBean(PlacementSegment placementSegment) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		
		PlacementSegmentHy placementSegmentHistory = new PlacementSegmentHy();
		placementSegmentHistory.setPlacementSegment(placementSegment);
		if(Validations.validateIsNotNullAndNotEmpty(placementSegment.getTrancheNumber()))
			placementSegmentHistory.setTrancheNumber(BigDecimal.valueOf(placementSegment.getTrancheNumber()));
		if(Validations.validateIsNotNullAndNotEmpty(placementSegment.getPlacementSegmentState()))
			placementSegmentHistory.setStatePlacementSegment(BigDecimal.valueOf(placementSegment.getPlacementSegmentState()));
		placementSegmentHistory.setRegistryUser(placementSegment.getRegistryUser());
		placementSegmentHistory.setRegistryDate(placementSegment.getRegistryDate());
		placementSegmentHistory.setActionDate(placementSegment.getActionDate());
		if(Validations.validateIsNotNullAndNotEmpty(placementSegment.getActionMotive()))
			placementSegmentHistory.setActionMotive(BigDecimal.valueOf(placementSegment.getActionMotive()));
		placementSegmentHistory.setOtherActionMotive(placementSegment.getOtherActionMotive());
		placementSegmentHistory.setRequestType(BigDecimal.valueOf(PlacementSegmentHistoryType.MODIFIED.getCode()));
		placementSegmentHistory.setBeginingDate(placementSegment.getBeginingDate());
		placementSegmentHistory.setClosingDate(placementSegment.getClosingDate());
		placementSegmentHistory.setPlacementTerm(placementSegment.getPlacementTerm());
		placementSegmentHistory.setAmountToPlace(placementSegment.getAmountToPlace());
		placementSegmentHistory.setPlacedAmount(placementSegment.getPlacedAmount());
		placementSegmentHistory.setIdIssuanceCodeFk(placementSegment.getIssuance().getIdIssuanceCodePk());
		placementSegmentHistory.setFixedAmount(placementSegment.getFixedAmount());
		placementSegmentHistory.setRequestFixedAmount(placementSegment.getRequestFixedAmount());
		placementSegmentHistory.setFloatingAmount(placementSegment.getFloatingAmount());
		placementSegmentHistory.setRequestFloatingAmount(placementSegment.getRequestFloatingAmount());
		placementSegmentHistory.setPlacementSegmentType(placementSegment.getPlacementSegmentType());
		placementSegmentHistory.setReopenFileName(placementSegment.getReopenFileName());
		placementSegmentHistory.setReopenFile(placementSegment.getReopenFile());
		placementSegmentHistory.setLastModifyApp(loggerUser.getUserAction().getIdPrivilegeRegister());
		placementSegmentHistory.setLastModifyDate(loggerUser.getAuditTime());
		placementSegmentHistory.setLastModifyIp(loggerUser.getIpAddress());
		placementSegmentHistory.setLastModifyUser(loggerUser.getUserName());
		
		em.persist(placementSegmentHistory);
		
		PlacementSegmentTO placementSegmentTO = new PlacementSegmentTO();
		placementSegmentTO.setIdPlacementeSegmentPk(placementSegment.getIdPlacementSegmentPk());
		
		PlacementSegment placementSegmentToModified = this.getPlacementSegmentServiceBean(placementSegmentTO);
		placementSegmentToModified.setPlacementSegmentState(PlacementSegmentStateType.OPENED.getCode());
		placementSegmentToModified.setReopenRequest(0);
		placementSegmentToModified.setLastModifyApp(loggerUser.getUserAction().getIdPrivilegeRegister());
		placementSegmentToModified.setLastModifyDate(loggerUser.getAuditTime());
		placementSegmentToModified.setLastModifyIp(loggerUser.getIpAddress());
		placementSegmentToModified.setLastModifyUser(loggerUser.getUserName());
		
		em.merge(placementSegmentToModified);
		
		return placementSegment;
	}
	
	@SuppressWarnings("unchecked")
	public List<PlacementSegmentHy> getListPlacementSegmentHysReopenService(PlacementSegment placementSegment) throws ServiceException{
		Map<String,Object> parameters = new HashMap<>();
		StringBuilder querySQL = new StringBuilder();
		querySQL.append(" Select psh ");
		querySQL.append(" From PlacementSegmentHy psh ");
		querySQL.append(" Inner Join Fetch psh.placementSegment ps ");
		querySQL.append(" Where 1 = 1 ");
		querySQL.append(" And ps.idPlacementSegmentPk = :idPlacementSegmentPk ");
		querySQL.append(" And psh.reopenFileName != null ");
		
		parameters.put("idPlacementSegmentPk", placementSegment.getIdPlacementSegmentPk());
		
		return findListByQueryString(querySQL.toString(), parameters);
	}
}