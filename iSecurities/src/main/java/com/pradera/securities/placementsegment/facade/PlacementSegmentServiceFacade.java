package com.pradera.securities.placementsegment.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegParticipaStruct;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegment;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegmentDetail;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegmentHy;
import com.pradera.model.issuancesecuritie.placementsegment.type.PlacementSegmentDetailStateType;
import com.pradera.model.issuancesecuritie.placementsegment.type.PlacementSegmentStateType;
import com.pradera.model.issuancesecuritie.type.IssuanceStateType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.SecurityPlacementType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.securities.placementsegment.service.PlacementSegmentServiceBean;
import com.pradera.securities.placementsegment.to.PlacementSegmentTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PlacementSegmentServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class PlacementSegmentServiceFacade extends CrudDaoServiceBean{

	/** The placement segment service bean. */
	@EJB
	PlacementSegmentServiceBean placementSegmentServiceBean;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	/**
	 * Validate placement segment entities.
	 *
	 * @param placementSegment the placement segment
	 * @throws ServiceException the service exception
	 */
	private void validatePlacementSegmentEntities(PlacementSegment placementSegment) throws ServiceException{
		Map<String,Object> mapParams = new HashMap<String, Object>();
		for(PlacementSegmentDetail placementDetail: placementSegment.getPlacementSegmentDetails()){
			mapParams.put("idSecurityCodePkParam", placementDetail.getSecurity().getIdSecurityCodePk());
			Security security = findObjectByNamedQuery(Security.SECURITY_STATE, mapParams);
			if(!security.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.SECURITY_BLOCK);
			}
		}
		
		mapParams = new HashMap<String, Object>();
		for(PlacementSegParticipaStruct participantPlac: placementSegment.getPlacementSegParticipaStructs()){
			mapParams.put("idParticipantPkParam", participantPlac.getParticipant().getIdParticipantPk());
			Participant participantBuyer = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, mapParams);
			if(!participantBuyer.getState().equals(ParticipantStateType.REGISTERED.getCode())){
				throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK);
			}
		}
		
		mapParams = new HashMap<String, Object>();
		mapParams.put("idIssuanceCodePkParam", placementSegment.getIssuance().getIdIssuanceCodePk());
		Issuance issuace = findObjectByNamedQuery(Issuance.ISSUANCE_STATE, mapParams);
		if(!issuace.getStateIssuance().equals(IssuanceStateType.AUTHORIZED.getCode())){
			throw new ServiceException(ErrorServiceType.ISSUANCE_BLOCK);
		}
		
		mapParams = new HashMap<String, Object>();
		mapParams.put("idIssuerPkParam", placementSegment.getIssuance().getIssuer().getIdIssuerPk());
		Issuer issuer = findObjectByNamedQuery(Issuer.ISSUER_STATE, mapParams);
		if(!issuer.getStateIssuer().equals(IssuerStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.ISSUER_BLOCK);
		}
	}
	
	/**
	 * Validate placement segment state.
	 *
	 * @param placementSeglemnt the placement seglemnt
	 * @param stateValidate the state validate
	 * @throws ServiceException the service exception
	 */
	private void validatePlacementSegmentState(PlacementSegment placementSeglemnt,List<Integer> stateValidate) throws ServiceException{
		Map<String,Object> mapParams = new HashMap<String, Object>();
		mapParams.put("idPlacementPkParam", placementSeglemnt.getIdPlacementSegmentPk());
		PlacementSegment placementSegmentP = findObjectByNamedQuery(PlacementSegment.PLAC_STATE, mapParams);
		if(!stateValidate.contains(placementSegmentP.getPlacementSegmentState())){
			throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
		}
	}
	
	/**
	 * Registry placement segment service facade.
	 *
	 * @param placementSegment the placement segment
	 * @return the placement segment
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.PLACEMENT_SEGMENT_REGISTER)
	public PlacementSegment registryPlacementSegmentServiceFacade(PlacementSegment placementSegment) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		validatePlacementSegmentEntities(placementSegment);
		placementSegment= placementSegmentServiceBean.registryPlacementSegmentServiceBean(placementSegment);
		return placementSegment;
	}
	
	/**
	 * Gets the list placement segment service facade.
	 *
	 * @param placementSegmentTO the placement segment to
	 * @param parameters the parameters
	 * @return the list placement segment service facade
	 * @throws ServiceException the service exception
	 */
	//@ProcessAuditLogger (process = BusinessProcessType.PLACEMENT_SEGMENT_QUERY)
	public List<PlacementSegment> getListPlacementSegmentServiceFacade(PlacementSegmentTO placementSegmentTO, Map<Integer,String> parameters) throws ServiceException{
//		return placementSegmentServiceBean.getListPlacementSegmentServiceBean(placementSegmentTO);
		List<PlacementSegment> placementList = placementSegmentServiceBean.getListPlacementSegmentService(placementSegmentTO);
		for(PlacementSegment placSegment: placementList){
			placSegment.setStateDescription(parameters.get(placSegment.getPlacementSegmentState()));
		}
		return placementSegmentServiceBean.getListPlacementSegmentService(placementSegmentTO);
	}
	
	/**
	 * Gets the placement segment service bean.
	 *
	 * @param placementSegmentTO the placement segment to
	 * @return the placement segment service bean
	 * @throws ServiceException the service exception
	 */
	public PlacementSegment getPlacementSegmentServiceBean(PlacementSegmentTO placementSegmentTO) throws ServiceException{
		return placementSegmentServiceBean.getPlacementSegmentServiceBean(placementSegmentTO);
	}
	
	/**
	 * Confirm placement segment service bean.
	 *
	 * @param placementSegment the placement segment
	 * @return the placement segment
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.PLACEMENT_SEGMENT_CONFIRM)
	public PlacementSegment confirmPlacementSegmentServiceFacade(PlacementSegment placementSegment) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());

		return placementSegmentServiceBean.confirmPlacementSegmentServiceBean(placementSegment, loggerUser);
	}
	
	
	/**
	 * Update placement segment service facade.
	 *
	 * @param placementSegment the placement segment
	 * @return the placement segment
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.PLACEMENT_SEGMENT_REJECT)
	public PlacementSegment rejectPlacementSegmentServiceFacade(PlacementSegment placementSegment) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		
		List<Integer> stateValidate = Arrays.asList(PlacementSegmentStateType.REGISTERED.getCode());
		validatePlacementSegmentState(placementSegment, stateValidate);
		
		update(placementSegment);
		return placementSegment;
	}
	
	/**
	 * Suspend placement service facade.
	 *
	 * @param placementSegment the placement segment
	 * @return the placement segment
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.PLACEMENT_SEGMENT_SUSPEND)
	public PlacementSegment suspendPlacementServiceFacade(PlacementSegment placementSegment) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		placementSegment.setPlacementSegmentState(PlacementSegmentStateType.SUSPENDED.getCode());
		
		List<Integer> stateValidate = Arrays.asList(PlacementSegmentStateType.OPENED.getCode());
		validatePlacementSegmentState(placementSegment, stateValidate);
		
		update(placementSegment);
		return placementSegment;
	}
	
	/**
	 * Suspend up placement service facade.
	 *
	 * @param placementSegment the placement segment
	 * 
	 * @return the placement segment
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.PLACEMENT_SEGMENT_UNSUSPEND)
	public PlacementSegment suspendUpPlacementServiceFacade(PlacementSegment placementSegment) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		
		if(placementSegment.getOpenDate()==null){
			placementSegment.setPlacementSegmentState(PlacementSegmentStateType.CONFIRMED.getCode());
		}else{
			placementSegment.setPlacementSegmentState(PlacementSegmentStateType.OPENED.getCode());
		}
		
		List<Integer> stateValidate = Arrays.asList(PlacementSegmentStateType.SUSPENDED.getCode());
		validatePlacementSegmentState(placementSegment, stateValidate);
		
		update(placementSegment);
		return placementSegment;
	}
	
	/**
	 * Modify placement segment service facade.
	 *
	 * @param placementSegment the placement segment
	 * @return the placement segment
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger (process = BusinessProcessType.PLACEMENT_SEGMENT_MODIFICATION)
	public PlacementSegment modifyPlacementSegmentServiceFacade(PlacementSegment placementSegment) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		
		//issue 1034
		if(loggerUser.getUserAction().getIdPrivilegeModify()!=null){
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		}else{
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeExecute());
		}
		
		validatePlacementSegmentEntities(placementSegment);
		return placementSegmentServiceBean.modifyPlacementSegmentServiceBean(placementSegment);
	}
	
	/**
	 * Modify placement segment reopen Request service facade.
	 * Issue 1273
	 *
	 * @param placementSegment the placement segment
	 * @return the placement segment
	 * @throws ServiceException the service exception
	 */
//	@ProcessAuditLogger (process = BusinessProcessType.PLACEMENT_SEGMENT_MODIFICATION)
	public PlacementSegment reopenRequestPlacementSegmentServiceFacade(PlacementSegment placementSegment) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser.getUserAction().getIdPrivilegeModify()!=null){
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		}else{
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeExecute());
		}
		return placementSegmentServiceBean.reopenRequestPlacementSegmentServiceBean(placementSegment);
	}
	
	/**
	 * Modify placement segment service facade.
	 * Issue 1273
	 *
	 * @param placementSegment the placement segment
	 * @return the placement segment
	 * @throws ServiceException the service exception
	 */
//	@ProcessAuditLogger (process = BusinessProcessType.PLACEMENT_SEGMENT_MODIFICATION)
	@LoggerAuditWeb
	public PlacementSegment reopenPlacementSegmentServiceFacade(PlacementSegment placementSegment) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		
		if(loggerUser.getUserAction().getIdPrivilegeModify()!=null){
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		}else{
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeExecute());
		}
		
		return placementSegmentServiceBean.reopenPlacementSegmentServiceBean(placementSegment);
	}
	
	public List<PlacementSegmentHy> getReopenPlacementSegmentDocumentsServiceFacade(PlacementSegment placementSegment) throws ServiceException{
		
		return placementSegmentServiceBean.getListPlacementSegmentHysReopenService(placementSegment);
	}
	
	/**
	 * Gets the last placement segment service facade.
	 *
	 * @param placementSegmentTO the placement segment to
	 * @return the last placement segment service facade
	 * @throws ServiceException the service exception
	 */
	public List<PlacementSegment> getLastPlacementSegmentServiceFacade(PlacementSegmentTO placementSegmentTO) throws ServiceException{
		return placementSegmentServiceBean.getLastPlacementSegmentServiceBean(placementSegmentTO);
	}
	
	/**
	 * Get Posterior Placement Segment Service.
	 *
	 * @param IdIssuanceCode Id Issuance Code
	 * @param trancheNumber tranche Number
	 * @return object Placement Segment
	 * @throws ServiceException the service exception
	 */
	public PlacementSegment getPosteriorPlacementSegmentServiceFacade(String IdIssuanceCode,Integer trancheNumber)throws ServiceException{
		return placementSegmentServiceBean.getPosteriorPlacementSegmentService(IdIssuanceCode, trancheNumber);
	}
	/**
	 * Sets the placement details from securities service facade.
	 *
	 * @param placementSegment the placement segment
	 * @param securityList the security list
	 * @return the placement segment
	 * @throws ServiceException the service exception
	 */
	public PlacementSegment setPlacementDetailsFromSecuritiesServiceFacade(PlacementSegment placementSegment, List<Security> securityList) throws ServiceException{
		
		if(placementSegment.getPlacementSegmentDetails()==null){
			placementSegment.setPlacementSegmentDetails(new ArrayList<PlacementSegmentDetail>());
		}
		
		List<PlacementSegmentDetail> placementSegmentDetailTemp = new ArrayList<PlacementSegmentDetail>();
		jump:
		for(Security security: securityList){
			
			if(!placementSegment.getPlacementSegmentDetails().isEmpty() && placementSegment.getIdPlacementSegmentPk()!=null){
				for(PlacementSegmentDetail placementSegmentDetail: placementSegment.getPlacementSegmentDetails()){
					if(placementSegmentDetail.getSecurity().getIdSecurityCodePk().equals(security.getIdSecurityCodePk())){
						placementSegmentDetail.setSelected(Boolean.TRUE);
						placementSegmentDetailTemp.add(placementSegmentDetail);
						continue jump;
					}
				}
			}
			
			PlacementSegmentDetail placementSegmentDetail = new PlacementSegmentDetail();
			
			placementSegmentDetail.setRegistryDate(CommonsUtilities.currentDate());
			placementSegmentDetail.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			placementSegmentDetail.setStatePlacementSegmentDet(PlacementSegmentDetailStateType.REGISTERED.getCode());
			placementSegmentDetail.setSecurity(security);
			placementSegmentDetail.setPlacementSegment(placementSegment);
			if(!(placementSegment.getIssuance().getPlacementType().equals(SecurityPlacementType.MIXED.getCode()))){
				placementSegmentDetail.setPlacementSegmentDetType(placementSegment.getIssuance().getPlacementType());
			}
			placementSegmentDetailTemp.add(placementSegmentDetail);
		}
		placementSegment.setPlacementSegmentDetails(placementSegmentDetailTemp);
		return placementSegment;
	}
	
	/**
	 * Verified issuance mixte have floating service facade.
	 *
	 * @param placementSegmentTO the placement segment to
	 * @param placementSegmentParam the placement segment param
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean verifiedIssuanceMixteServiceFacade(PlacementSegmentTO placementSegmentTO, PlacementSegment placementSegmentParam) throws ServiceException{
		List<PlacementSegment> placementSegmentList = null;
		
		placementSegmentList = placementSegmentServiceBean.getLastPlacementSegmentServiceBean(placementSegmentTO);
		if(placementSegmentList==null){
			placementSegmentList = new ArrayList<PlacementSegment>();
		}
		placementSegmentList.add(placementSegmentParam);
		for(PlacementSegment placementSegment: placementSegmentList){
			for(PlacementSegmentDetail placSegDetail: placementSegment.getPlacementSegmentDetails()){
				SecurityPlacementType placSegType = SecurityPlacementType.lookup.get(placSegDetail.getPlacementSegmentDetType());
				if(placSegType.getCode().equals(placementSegmentTO.getPlacSegDetailType())){
					return Boolean.TRUE;
				}
			}
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Gets the participant secundary report.
	 *
	 * @param filter the filter
	 * @return the participant secundary report
	 * @throws ServiceException the service exception
	 */
	public List<Participant> getParticipantSecundaryReport(Participant filter) throws ServiceException{
		return placementSegmentServiceBean.getParticipantSecundaryReport(filter);
	}
	
	/**
	 * Gets the participant registered.
	 *
	 * @return the participant registered
	 * @throws ServiceException the service exception
	 */
	public List<Participant> getParticipantRegistered() throws ServiceException{
		Participant filter = new Participant();
		filter.setState(ParticipantStateType.REGISTERED.getCode());
		return accountsFacade.getLisParticipantServiceBean(filter);
	}
	
	/**
	 * Checks if is exists segment paticipant securities.
	 *
	 * @param params the params
	 * @return true, if is exists segment paticipant securities
	 * @throws ServiceException the service exception
	 */
	public boolean isExistsSegmentPaticipantSecurities(Map<String,Object> params) throws ServiceException{
		Integer valPartSegSec =Integer.parseInt( placementSegmentServiceBean.findObjectByNamedQuery(PlacementSegment.PLAC_SECURITIES_BY_PARTICIPANT, params).toString());
		return valPartSegSec > 0;
	}
	


	/**
	 * Add operation placement.
	 *
	 * @param participantCode the participant code
	 * @param securityCode the security code
	 * @param operationPk the operation pk
	 * @param stockQuantity the stock quantity
	 * @throws ServiceException the service exception
	 */
	public void addOperationPlacement(Long participantCode, String securityCode, Long operationPk, BigDecimal stockQuantity) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser.getUserAction().getIdPrivilegeRegister()!=null){
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		}else if (loggerUser.getUserAction().getIdPrivilegeConfirm()!=null){
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		}
		
		PlacementSegment placement = placementSegmentServiceBean.getPlacementFromParticipantOrSecurity(participantCode, securityCode);
		//placementSegmentServiceBean.updatePendingOperation(placement, ComponentConstant.SUM, BigDecimal.ONE, loggerUser);
		placementSegmentServiceBean.updatePendingOperation(placement, ComponentConstant.SUM, stockQuantity, loggerUser);
	}
	
	/**
	 * Substract operation placement.
	 *
	 * @param participantCode the participant code
	 * @param securityCode the security code
	 * @param operationPk the operation pk
	 * @param stockQuantity the stock quantity
	 * @throws ServiceException the service exception
	 */
	public void substractOperationPlacement(Long participantCode, String securityCode, Long operationPk, BigDecimal stockQuantity) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSettlement());
		PlacementSegment placement = placementSegmentServiceBean.getPlacementFromParticipantOrSecurity(participantCode, securityCode);
		//placementSegmentServiceBean.updatePendingOperation(placement, ComponentConstant.SUBTRACTION, BigDecimal.ONE, loggerUser);
		placementSegmentServiceBean.updatePendingOperation(placement, ComponentConstant.SUBTRACTION, stockQuantity, loggerUser);
		placementSegmentServiceBean.updatePlacementRequestBalance(stockQuantity, operationPk, loggerUser);
	}
	
	/**
	 * Add annotation placement.
	 *
	 * @param participantCode the participant code
	 * @param securityCode the security code
	 * @param operationPk the operation pk
	 * @param stockQuantity the stock quantity
	 * @throws ServiceException the service exception
	 */
	public void addAnnotationPlacement(Long participantCode, String securityCode, Long operationPk, BigDecimal stockQuantity) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSettlement());
		PlacementSegment placement = placementSegmentServiceBean.getPlacementFromParticipantOrSecurity(participantCode, securityCode);
		//placementSegmentServiceBean.updatePendingAnnotation(placement, ComponentConstant.SUM, BigDecimal.ONE, loggerUser);
		placementSegmentServiceBean.updatePendingAnnotation(placement, ComponentConstant.SUM, stockQuantity, loggerUser);
	}
	
	/**
	 * Substract annotation placement.
	 *
	 * @param participantCode the participant code
	 * @param securityCode the security code
	 * @param operationPk the operation pk
	 * @param stockQuantity the stock quantity
	 * @throws ServiceException the service exception
	 */
	public void substractAnnotationPlacement(Long participantCode, String securityCode, Long operationPk, BigDecimal stockQuantity) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		PlacementSegment placement = placementSegmentServiceBean.getPlacementFromParticipantOrSecurity(participantCode, securityCode);
		//placementSegmentServiceBean.updatePendingAnnotation(placement, ComponentConstant.SUBTRACTION, BigDecimal.ONE, loggerUser);
		placementSegmentServiceBean.updatePendingAnnotation(placement, ComponentConstant.SUBTRACTION, stockQuantity, loggerUser);
	}
}