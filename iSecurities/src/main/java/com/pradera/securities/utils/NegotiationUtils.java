package com.pradera.securities.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.beanio.BeanIOConfigurationException;
import org.beanio.BeanReader;

import com.pradera.commons.utils.BeanIOUtils;
import com.pradera.integration.exception.ServiceException;
import com.pradera.securities.valuator.core.files.to.MarketfactFileErrorHandler;
import com.pradera.securities.valuator.core.files.to.MarketfactValidationTO;
import com.pradera.securities.valuator.core.files.to.ValuatorMarketfactFileTO;
import com.pradera.securities.valuator.core.files.to.ValuatorMarketfactMechanismType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class NegotiationUtils.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-ago-2015
 */
public class NegotiationUtils {

	/**
	 * Instantiates a new negotiation utils.
	 */
	public NegotiationUtils() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Validate marketfact file.
	 *
	 * @param tmpMarketfactFile the tmp marketfact file
	 * @return the marketfact validation to
	 * @throws ParserConfigurationException the parser configuration exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws BeanIOConfigurationException the bean io configuration exception
	 * @throws IllegalArgumentException the illegal argument exception
	 * @throws ServiceException the service exception
	 */
	public static MarketfactValidationTO validateMarketfactFile(ValuatorMarketfactFileTO tmpMarketfactFile) 
			throws ParserConfigurationException, IOException,BeanIOConfigurationException, IllegalArgumentException, ServiceException {
		MarketfactValidationTO validate = new MarketfactValidationTO();
		validate=validateFileOperationStruct(validate,tmpMarketfactFile);
		return validate;
	}
	
	/**
	 * Validate file operation struct.
	 *
	 * @param validate the validate
	 * @param tmpMarketfactFile the tmp marketfact file
	 * @return the marketfact validation to
	 * @throws ServiceException the service exception
	 * @throws BeanIOConfigurationException the bean io configuration exception
	 * @throws IllegalArgumentException the illegal argument exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static MarketfactValidationTO validateFileOperationStruct(MarketfactValidationTO validate,ValuatorMarketfactFileTO tmpMarketfactFile) 
			throws ServiceException, BeanIOConfigurationException, IllegalArgumentException, IOException {
		InputStream inputFile=Thread.currentThread().getContextClassLoader().getResourceAsStream(tmpMarketfactFile.getStreamFileDir());
		String fileString=FileUtils.readFileToString(tmpMarketfactFile.getTempMarketfactFile());
		StringReader stringReader = new StringReader(fileString);
		BeanReader beanReader= BeanIOUtils.getReader(tmpMarketfactFile.getRootTag(), inputFile, stringReader, new Locale("es"));
		MarketfactFileErrorHandler handler= new MarketfactFileErrorHandler();
		beanReader.setErrorHandler(handler);
		//aqui puedo capurar errores
		ValuatorMarketfactMechanismType marketfactfile;
		while((marketfactfile=(ValuatorMarketfactMechanismType)beanReader.read()) != null){
			validate.getLstMarketfactFIles().add(marketfactfile);
		}
		beanReader.close();
		stringReader.close();
		inputFile.close();
		validate.setTotalCount(validate.getLstMarketfactFIles().size());
		validate.setLstErrors(handler.getTotalErrors());
		validate.setRejectedMarketfactfiles(handler.getRejectedMarketfact());
		return validate;
	}
	
}
