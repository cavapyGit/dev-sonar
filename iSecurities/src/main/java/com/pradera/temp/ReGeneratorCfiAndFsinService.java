package com.pradera.temp;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.ejb.AccessTimeout;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.Query;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.securities.service.SecuritiesServiceBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.issuancesecuritie.Security;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ReGeneratorCfiAndFsinService extends CrudDaoServiceBean {

	private List<Security> securities;

	@EJB
	private SecuritiesServiceBean serviceBean;

	public void startCFI() {
	}

	
	public int getCount(){
		StringBuilder queryStr = new StringBuilder();
		queryStr.append(" select se.idSecurityCodePk from Security se ");
		queryStr.append(" where 1 = 1 ");
		queryStr.append(" and se.fsinCode is not null ");
		queryStr.append(" and se.stateSecurity = 131 ");
		queryStr.append(" and se.securityClass in (126,406,408,409,410,411,412,413,416,418,419,421,426,1936,1937,1962,2352,2353,2366) ");
		Query query = em.createQuery(queryStr.toString());
		return query.getResultList().size();
	}  
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	@TransactionTimeout(unit = TimeUnit.HOURS, value = 10)
	@AccessTimeout(unit = TimeUnit.HOURS, value = 10)
	@Performance
	public void startFSIN() {
		StringBuilder queryStr = new StringBuilder();
		queryStr.append(" select se from Security se ");
		queryStr.append(" where 1 = 1 ");
		queryStr.append(" and se.fsinCode is null ");
		queryStr.append(" and se.stateSecurity = 131 ");
		queryStr.append(" and se.securityClass in (126,406,408,409,410,411,412,413,416,417,418,419,421,426,1936,1937,1962,2352,2353,2366) ");
		queryStr.append(" and rownum between 1 and 1000 ");

		/* PRIMERA PARTE */
		Query query = em.createQuery(queryStr.toString());
		securities = new ArrayList<>();
		securities = query.getResultList();
		String fsin = null;
		for (Security security : securities) {
			try {
				fsin = serviceBean.getShortCfiCode(security);
				if (fsin != null) {
					security.setFsinCode(fsin);
					update(security);
					System.out.println("FSIN GENERADO :" + fsin);
				} else {
					System.out.println("FSIN GENERADO : No se genero --> "
							+ security.getIdSecurityCodePk());
				}

			} catch (ServiceException e) {
				System.out.println("FSIN GENERADO : Error al generar --> "
						+ security.getIdSecurityCodePk());
				e.printStackTrace();
			}
		}
		/* SEGUNDA PARTE */
		em.createQuery(queryStr.toString());
		securities = new ArrayList<>();
		securities = query.getResultList();
		fsin = null;
		for (Security security : securities) {
			try {
				fsin = serviceBean.getShortCfiCode(security);
				if (fsin != null) {
					security.setFsinCode(fsin);
					update(security);
					System.out.println("FSIN GENERADO :" + fsin);
				} else {
					System.out.println("FSIN GENERADO : No se genero --> "
							+ security.getIdSecurityCodePk());
				}

			} catch (ServiceException e) {
				System.out.println("FSIN GENERADO : Error al generar --> "
						+ security.getIdSecurityCodePk());
				e.printStackTrace();
			}
		}
	}
}
