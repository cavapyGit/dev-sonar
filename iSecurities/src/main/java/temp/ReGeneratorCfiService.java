package temp;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.ejb.AccessTimeout;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.Query;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.securities.service.SecuritiesServiceBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.issuancesecuritie.Security;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ReGeneratorCfiService extends CrudDaoServiceBean {
	
	private List<Security> securities;
	
	@EJB
	private SecuritiesServiceBean serviceBean;
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	@TransactionTimeout(unit=TimeUnit.HOURS,value=10)
	@AccessTimeout(unit=TimeUnit.HOURS,value=10)
	@Performance 
	public void startCFI(){
		//reGenerateIdIsinCode();
		StringBuilder queryStr = new StringBuilder();
		queryStr.append(" select se from Security se ");
		queryStr.append(" where 1 = 1 ");
		queryStr.append(" and se.cfiCode is null ");
		queryStr.append(" and se.stateSecurity = 131 ");
		queryStr.append(" and se.securityClass in (126,406,408,409,410,411,412,413,416,418,419,421,426,1936,1937,1962,2352,2353,2366) ");
		queryStr.append(" and rownum between 1 and 500 ");
		Query query = em.createQuery(queryStr.toString());
		securities = new ArrayList<>();
		securities = query.getResultList();
		String cfiCode=null;
		for (Security security : securities) {
			cfiCode = serviceBean.generateCFICode(security);
			System.out.println("CFI GENERADO : "+cfiCode);
			if(cfiCode== null){
				System.out.println("CFI NO GENERADO :"+security.getIdSecurityCodePk());
				security.setCfiCode("NOCFI");
			}
			security.setCfiCode(cfiCode);
			updateSecurityCfi(security);
		}
	}
	
	
	public void reGenerateIdIsinCode(){
		StringBuilder queryStr = new StringBuilder();
		queryStr.append(" select se from Security se where se.idSecurityCodePk in "
	    + "('CFC-CPL-E1-07','CFC-AGP-N1U-12','')");	
//		+ "('CFC-CPL-E1-07','CFC-CPL-E1-07','ACC-CRU2U','ACC-TCO1U','ACC-BISALEAS1B','ACC-BME1U'"
//      + ",'ACC-BSC1U','ACC-BIS1U','ACC-BISASEG1B','LRS-NR00521736Q')");
		Security security = new Security();
		try {
			Query query = em.createQuery(queryStr.toString());
			securities = new ArrayList<>();
			securities = query.getResultList();
			String isin = null;
			for (Security sec : securities) {
				isin = serviceBean.securityIsinGenerate(sec);
				if(isin!=null){
					System.out.println("VALOR : "+sec.getIdSecurityCodePk()+ "-->"+isin);
				}else{
					System.out.println("NO se pudos generar codigo ISIN");
				}
			}
		} catch (ServiceException e) {
			//e.printStackTrace();
		}
	}
	public int getCount(){
		StringBuilder queryStr = new StringBuilder();
		queryStr.append(" select se.idSecurityCodePk from Security se ");
		queryStr.append(" where 1 = 1 ");
		queryStr.append(" and se.cfiCode is not null ");
		queryStr.append(" and se.stateSecurity = 131 ");
		queryStr.append(" and se.securityClass in (126,406,408,409,410,411,412,413,416,418,419,421,426,1936,1962,2352,2353,2366) ");
		Query query = em.createQuery(queryStr.toString());
		return query.getResultList().size();
	}
	//@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private synchronized  void updateSecurityCfi(Security security) {
		String secu = security.getIdSecurityCodePk();
		String cfi_code = security.getCfiCode();
		if(cfi_code!=null&&secu!=null){
			String  up = "update security set Cfi_Code = '"+secu+"' where 1 =1 and Id_Security_Code_Pk = '"+cfi_code+"'";
			try {
				em.createNativeQuery(up).executeUpdate();
				//em.flush();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			System.out.println("CFI NULO");
		}
	}
//	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
//	@Interceptors(ContextHolderInterceptor.class)
//	@TransactionTimeout(unit=TimeUnit.HOURS,value=1)
//	@AccessTimeout(unit=TimeUnit.HOURS,value=1)
//	@Performance 
//	public void startFSIN(){
//		StringBuilder queryStr = new StringBuilder();
//		queryStr.append(" select se from Security se ");
//		queryStr.append(" where 1 = 1 ");
//		queryStr.append(" and se.fsinCode is null ");
//		queryStr.append(" and se.securityClass in (126,406,408,409,410,411,412,413,416,418,419,421,426,1936,1937,1962,2352,2353,2366) ");
//		queryStr.append(" and rownum between 1 and 8000 ");
//		
//		/*PRIMERA PARTE*/		
//		Query query = em.createQuery(queryStr.toString());
//		securities = new ArrayList<>();
//		securities = query.getResultList();
//		String fsin=null;
//		for (Security security : securities) {
//			try {
//				fsin = serviceBean.getShortCfiCode(security);
//				security.setFsinCode(fsin);
//				update(security);
//				System.out.println("CFI GENERADO :"+fsin);
//			} catch (ServiceException e) {
//				System.out.println("No se pudo generar CFI CODE PARA EL VALOR :"+security.getIdSecurityCodePk());
//				e.printStackTrace();
//			}
//		}
//		/*SEGUNDA PARTE*/
//		em.createQuery(queryStr.toString());
//		securities = new ArrayList<>();
//		securities = query.getResultList();
//		fsin=null;
//		for (Security security : securities) {
//			try {
//				fsin = serviceBean.getShortCfiCode(security);
//				security.setFsinCode(fsin);
//				update(security);
//				System.out.println("CFI GENERADO :"+fsin);
//			} catch (ServiceException e) {
//				System.out.println("No se pudo generar CFI CODE PARA EL VALOR :"+security.getIdSecurityCodePk());
//				e.printStackTrace();
//			}
//		}
//	}
}

