package temp;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;

import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;

@DepositaryWebBean
@LoggerCreateBean
public class ReGeneratorCfiMgmt  extends GenericBaseBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ReGeneratorCfiFacade facade;
	
	
	private int count;
	
	
	@PostConstruct
	public void init(){
		count = facade.getConunt();
	}
	
	public void reGenerateDataCFI(){
		System.out.println("INICIANDO LA ACTUALIZACION DE CFI");
		facade.startReGenerateCFI();
		System.out.println("SE FINALIZO LA GENERACION DE CFI");
	}
	public void reGenerateDataFSIN(){
		System.out.println("INICIANDO LA ACTUALIZACION DE FSIN");
		facade.startReGenerateFSIN();
		System.out.println("SE FINALIZO LA GENERACION DE FSIN");
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
}
