package temp;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;

@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ReGeneratorCfiFacade {
	@EJB 
	private ReGeneratorCfiService service;
	
	public void startReGenerateCFI(){
		service.startCFI();
	}
	public void startReGenerateFSIN(){
		//service.startFSIN();
	}
	
	public int getConunt(){
		return service.getCount();
	}
}
