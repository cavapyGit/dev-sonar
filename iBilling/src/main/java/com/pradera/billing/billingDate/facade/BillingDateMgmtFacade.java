package com.pradera.billing.billingDate.facade;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.BusinessProcess;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ServiceCalculationServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Interceptors(ContextHolderInterceptor.class)
public class BillingDateMgmtFacade  {
	
	/** The user info. */
	@Inject
	UserInfo userInfo;	
	
	/** The transaction registry. */
	@Resource
	TransactionSynchronizationRegistry 		transactionRegistry;

	/** The batch process service facade. */
	@EJB 
	private BatchProcessServiceFacade batchProcessServiceFacade;
	
	/**
	 * Instantiates a new batch process service facade.
	 */
	public BillingDateMgmtFacade() {
		// TODO Auto-generated constructor stub
	}
	
	
    /**
     * Metodo para ejecutar el proceso batch para el cambio de fecha de los servicios con indicador BCB
     * @param participantPk
     * @throws ServiceException
     */
	@LoggerAuditWeb
    public void registerValuatorBatch(Date date, String userNameInfo) throws ServiceException{
    	
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		// issue 1182: en caso de que los privilegios no sean agarrados se procede a setearlos manualmente
		String userName = "";
		if(Validations.validateIsNotNull(loggerUser) 
				&& Validations.validateIsNotNull(loggerUser.getUserAction()) 
				&& Validations.validateIsNotNull(loggerUser.getUserAction().getIdPrivilegeRegister())){
			System.out.println("::: obtencion automatica del privilegio");
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
			userName = loggerUser.getUserName();
		}else{
			/* Consultar en seguridad:
			 * select * from SYSTEM_OPTION where ID_SYSTEM_OPTION_PK = 141326 --> deberia arrojar la sig url a: /iBilling/pages/billingDate/billingDate.xhtml
			 * select * from OPTION_PRIVILEGE where ID_SYSTEM_OPTION_FK = 141326 --> arroja los siguientes privilegios 2720=Registrar, 2719=Modificar
			 */
			// segun la BBDD de seguridad, el id 2720 corresponde al privilegio de registro
			loggerUser.setIdPrivilegeOfSystem(2720);
			System.out.println(":: obtencion manual del privilegio");
			userName = userNameInfo;
		}
		
		Map<String, Object> details = new HashMap<String, Object>();
		details.put("date", date.getDate());
		
		BusinessProcess businessProcess = new BusinessProcess();
		//businessProcess.setIdBusinessProcessPk(BusinessProcessType.VALUATOR_EXECUTION_CORE_PROCESS.getCode());
		businessProcess.setIdBusinessProcessPk(40809L);
		
		try {
			batchProcessServiceFacade.registerBatch(userName,businessProcess,details);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
}
