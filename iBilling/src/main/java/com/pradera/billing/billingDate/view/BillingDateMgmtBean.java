package com.pradera.billing.billingDate.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.billing.billingDate.facade.BillingDateMgmtFacade;
import com.pradera.billing.billingexception.facade.InvoiceExceptionServiceFacade;
import com.pradera.billing.billingexception.invoice.to.ExceptionInvoiceGenerationCutTo;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.integration.exception.ServiceException;

/**
 * 
 * @author jquino
 */
@DepositaryWebBean
@LoggerCreateBean
public class BillingDateMgmtBean extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The user info. */
	@Inject
	UserInfo userInfo;

	@Inject
	UserPrivilege userPrivilege;

	/** The batch process service facade. */
	@EJB
	BillingDateMgmtFacade billingDateMgmtFacade;
	
	/** The batch process service facade. */
	@EJB
	InvoiceExceptionServiceFacade invoiceExceptionServiceFacade;

	/** The register date. */
	private Date dateBcb;
	
	List<ExceptionInvoiceGenerationCutTo> listExceptionInvoiceGenerationsCutTos;
	

	/**
	 * Inits the.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException {

		dateBcb = CommonsUtilities.currentDate();

	}

	/**
	 * Metodo para ejecutar el batch con la nueva fecha
	 * 
	 * @throws ServiceException
	 */
	public void executeChangeDate() throws ServiceException {
		try {
			billingDateMgmtFacade.registerValuatorBatch(dateBcb, userInfo.getUserAccountSession().getUserName());
		} catch (Exception e) {
			billingDateMgmtFacade.registerValuatorBatch(dateBcb, "-");
		}

		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesUtilities.getMessage("billing.date.popup"));
		JSFUtilities.showSimpleValidationDialog();
		
		listExceptionInvoiceGenerationsCutTos = new ArrayList<ExceptionInvoiceGenerationCutTo>();
	}
	
	/**
	 * Metodo 
	 * @throws ServiceException
	 */
	public void search() throws ServiceException {
		// Obteniendo las excepciones
		//List<Long> listBillingSchedulePk = invoiceExceptionServiceFacade.getIdBillingSckedule();
		listExceptionInvoiceGenerationsCutTos = invoiceExceptionServiceFacade.getExceptionInvoiceGenerationsCutTo(dateBcb);
		
	}

	public Date getDateBcb() {
		return dateBcb;
	}

	public void setDateBcb(Date dateBcb) {
		this.dateBcb = dateBcb;
	}

	public List<ExceptionInvoiceGenerationCutTo> getListExceptionInvoiceGenerationsCutTos() {
		return listExceptionInvoiceGenerationsCutTos;
	}

	public void setListExceptionInvoiceGenerationsCutTos(List<ExceptionInvoiceGenerationCutTo> listExceptionInvoiceGenerationsCutTos) {
		this.listExceptionInvoiceGenerationsCutTos = listExceptionInvoiceGenerationsCutTos;
	}
	
}