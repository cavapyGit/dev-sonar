package com.pradera.billing.process.batch;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

import com.pradera.billing.billing.facade.BillingProcessedServiceFacade;
import com.pradera.billing.billing.to.BillingProcessedFilter;
import com.pradera.billing.common.facade.BusinessLayerCommonFacade;
import com.pradera.billing.common.utils.BillingUtils;
import com.pradera.billing.process.service.BillingCalculationService;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.billing.webservices.ServiceAPIClients;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.billing.BillingCalculationProcess;
import com.pradera.model.billing.type.BillingCalculationType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingPreliminaryBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@BatchProcess(name = "BillingPreliminaryBatch")
@RequestScoped
public class BillingPreliminaryBatch implements JobExecution, Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1119270984853234705L;

	/** The billing calculation executor. */
	@EJB
	BillingCalculationService billingCalculationExecutor;

	/** The billing processed service facade. */
	@EJB
	BillingProcessedServiceFacade billingProcessedServiceFacade;

	/** The business layer common facade. */
	@EJB
	BusinessLayerCommonFacade businessLayerCommonFacade;

	/** The message configuration. */
	@Inject
	@InjectableResource(location = "Messages_es.properties")
	private Properties messageConfiguration;
	
	@EJB
	ServiceAPIClients serviceAPIClients;

	/** The log. */
	@Inject
	private PraderaLogger log;

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {

		/** getting details ProcessLoggerDetails */
		List<ProcessLoggerDetail> processLoggerDetails = processLogger.getProcessLoggerDetails();

		BillingProcessedFilter billingProcessedFilter = new BillingProcessedFilter();
		List<BillingProcessedFilter> listbillingProcessedFilter = new ArrayList<BillingProcessedFilter>();

		try {

			if (Validations.validateListIsNotNullAndNotEmpty(processLoggerDetails)) {

				for (ProcessLoggerDetail processLoggerDetail : processLoggerDetails) {

					/**
					 * If Batch Execution For Execute Process
					 */
					if (processLoggerDetail.getParameterName().equals(GeneralConstants.PARAMETER_DATE)) {
						Date calculationDate = CommonsUtilities.convertStringtoDate(processLoggerDetail.getParameterValue());
						billingProcessedFilter.setBillingDateStart(calculationDate);
						billingProcessedFilter.setBillingDateEnd(calculationDate);
						billingProcessedFilter = null;
						break;
					}
					if(processLoggerDetail.getParameterName().equals(GeneralConstants.PARAMETER_SCHEDULE_ID)){
						billingProcessedFilter=null;
						break;
					}

					/** GETTING PARAMETERS FOR ENTITY COLLECTION **/

					if (processLoggerDetail.getParameterName().equals(GeneralConstants.FIELD_ENTITY_COLLECTION)) {
						billingProcessedFilter.setEntityCollection(Integer.parseInt(processLoggerDetail.getParameterValue()));
					}

					if (processLoggerDetail.getParameterName().equals("documentCodeOther")) {
						billingProcessedFilter.setDocumentCodeOther(processLoggerDetail.getParameterValue());
					}

					if (processLoggerDetail.getParameterName().equals("documentType")) {
						billingProcessedFilter.setDocumentTypeOther(Integer.parseInt((String) processLoggerDetail.getParameterValue()));
					}

					if (processLoggerDetail.getParameterName().equals("participantPk")) {
						billingProcessedFilter.setParticipantPk(Long.parseLong(processLoggerDetail.getParameterValue()));
					}

					if (processLoggerDetail.getParameterName().equals("issuerPk")) {
						billingProcessedFilter.setIssuerPk(processLoggerDetail.getParameterValue());
					}

					if (processLoggerDetail.getParameterName().equals("holderPk")) {
						billingProcessedFilter.setHolderPk(Long.parseLong(processLoggerDetail.getParameterValue()));
					}

					/** END ***/

					if (processLoggerDetail.getParameterName().equals("processedState")) {
						billingProcessedFilter.setProcessedState(Integer.parseInt(processLoggerDetail.getParameterValue()));
					}

					if (processLoggerDetail.getParameterName().equals(GeneralConstants.FIELD_COLLECTION_PERIOD)) {
						billingProcessedFilter.setCollectionPeriod(Integer.parseInt(processLoggerDetail.getParameterValue()));
					}

					if (processLoggerDetail.getParameterName().equals("startDate")) {
						Date startDate = CommonsUtilities.convertStringtoDate(processLoggerDetail.getParameterValue());
						billingProcessedFilter.setBillingDateStart(startDate);
					}

					if (processLoggerDetail.getParameterName().equals("endDate")) {
						Date endDate = CommonsUtilities.convertStringtoDate(processLoggerDetail.getParameterValue());
						billingProcessedFilter.setBillingDateEnd(endDate);
					}

					if (processLoggerDetail.getParameterName().equals("billingDate")) {
						Date billingDate = CommonsUtilities.convertStringtoDate(processLoggerDetail.getParameterValue());
						billingProcessedFilter.setBillingDate(billingDate);
					}

					if (processLoggerDetail.getParameterName().equals("idBillingServicePk")) {
						billingProcessedFilter.setBillingServicePk(Long.parseLong(processLoggerDetail.getParameterValue()));
					}

					if (processLoggerDetail.getParameterName().equals("inIntegratedBill")) {
						billingProcessedFilter.setIndIntegratedBill(Integer.parseInt(processLoggerDetail.getParameterValue()));
					}
					
					if (processLoggerDetail.getParameterName().equals("idHolderByParticipant")) {
						billingProcessedFilter.setIdHolderByParticipant(Long.parseLong(processLoggerDetail.getParameterValue()));
					}

				}

				if (Validations.validateIsNotNull(billingProcessedFilter)) {
					listbillingProcessedFilter.add(billingProcessedFilter);
				} else {

					/**
					 * Create Filter To Send Billing for each Collection Period
					 * with Initial Date and Final Date by Period
					 */
					listbillingProcessedFilter = billingProcessedServiceFacade.findFilterBillingProcessedBatch();
				}

			} else {
				/**
				 * Create Filter To Send Billing for each Collection Period with
				 * Initial Date and Final Date by Period
				 */
				listbillingProcessedFilter = billingProcessedServiceFacade.findFilterBillingProcessedBatch();

			}

			for (Iterator iterator = listbillingProcessedFilter.iterator(); iterator.hasNext();){
				BillingProcessedFilter billingProcessedFilterSend = (BillingProcessedFilter) iterator.next();
				billingProcessedFilter=billingProcessedFilterSend;
				/*** SEND PRELIMINARY PROCESS OF BILLING **/
				billingCalculationExecutor.executePreliminaryProcessBilling(billingProcessedFilterSend);
			}

		} catch (Exception e) {

			log.error("Error in execute PreliminaryProcessBilling in Billing Preliminary Batch");
			log.error("Save BillingCalculationProcess With state Error");
			BillingCalculationProcess billingCalculationProcess = new BillingCalculationProcess();
			billingCalculationProcess.setOperationDate(CommonsUtilities.currentDateTime());
			billingCalculationProcess.setBillingDate(billingProcessedFilter.getBillingDateEnd());

			if (billingProcessedFilter.getCollectionPeriod().equals(PropertiesConstants.COLLECTION_PERIOD_MONTHLY_PK)) {

				String strDate = CommonsUtilities.convertDateToString(billingProcessedFilter.getBillingDateEnd(),BillingUtils.DATE_PATTERN_yyyy_MM_dd);
				String strYearMonth = BillingUtils.getPatternYearMonth(strDate,BillingUtils.DATE_PATTERN_yyyy_MM_dd);
				billingCalculationProcess.setBillingMonth(BillingUtils.getPatternMonth(strYearMonth,BillingUtils.DATE_PATTERN_yyyy_MM));
				billingCalculationProcess.setBillingYear(BillingUtils.getPatternYear(strYearMonth,BillingUtils.DATE_PATTERN_yyyy_MM));
			}

			billingCalculationProcess.setEntityCollection(billingProcessedFilter.getEntityCollection());
			billingCalculationProcess.setBillingPeriod(billingProcessedFilter.getCollectionPeriod());
			billingCalculationProcess.setProcessedState(BillingCalculationType.ERROR.getCode());
			billingCalculationProcess.setFileXml(new byte[0]);
			billingProcessedServiceFacade.saveAllBillingProcessedService(billingCalculationProcess);

			/** Send Notification **/
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.PRELIMINARY_PROCESS_BILLING.getCode());
			String subject = messageConfiguration.getProperty("header.message.confirmation");

			String message = messageConfiguration.getProperty("body.message.warning.preliminary");
			message = MessageFormat.format(message,
									BusinessProcessType.PRELIMINARY_PROCESS_BILLING.getDescription(), 
									CommonsUtilities.currentDateTime().toString());

			List<UserAccountSession> listUser = this.businessLayerCommonFacade.getListUsserByBusinessProcess(BusinessProcessType.PRELIMINARY_PROCESS_BILLING.getCode());
			businessLayerCommonFacade.sendNotificationManual(businessProcess,listUser, subject, message, NotificationType.EMAIL);
			businessLayerCommonFacade.sendNotificationManual(businessProcess,listUser, subject, message, NotificationType.MESSAGE);

			e.printStackTrace();
			throw new RuntimeException();
		}

		/***
		 * SEND REPORT AND NOTIFICATION OF PROCESS PRELIMINARY BILLING
		 * 
		 */
		try {

			BusinessProcess businessProcess = new BusinessProcess();

			businessProcess.setIdBusinessProcessPk(BusinessProcessType.PRELIMINARY_PROCESS_BILLING.getCode());

			Object[] parameters = {
					BusinessProcessType.PRELIMINARY_PROCESS_BILLING.getDescription(),
					CommonsUtilities.currentDateTime().toString() };
			businessLayerCommonFacade.sendNotificationAutomatic(businessProcess, null, parameters);

			if (Validations.validateIsNotNull(billingProcessedFilter.getProcessBillingCalculationPk())) {
				
				this.serviceAPIClients.generateBillingReport(billingProcessedFilter, 
						ReportIdType.CONSOLIDATED_BILLING_PRELIMINARY_REPORT.getCode());
				
				//billingProcessedServiceFacade.sendingReportOnBilling(billingProcessedFilter,
					//	ReportIdType.CONSOLIDATED_BILLING_PRELIMINARY_REPORT.getCode()); //mdz

			}

		} catch (ServiceException e) {

			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}

}