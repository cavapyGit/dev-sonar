package com.pradera.billing.process.service;

import javax.ejb.Local;

import com.pradera.billing.billing.to.BillingProcessedFilter;
import com.pradera.integration.exception.ServiceException;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Interface BillingCalculationService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Local
public interface BillingCalculationService {

	/**
	 * Execute preliminary process billing.
	 *
	 * @param billingProcessedFilter the billing processed filter
	 * @throws ServiceException the service exception
	 */
	public void executePreliminaryProcessBilling(BillingProcessedFilter billingProcessedFilter) throws ServiceException;
	
	/**
	 * Execute definitive process billing.
	 *
	 * @param billingProcessedFilter the billing processed filter
	 * @throws ServiceException the service exception
	 */
	public void executeDefinitiveProcessBilling(BillingProcessedFilter billingProcessedFilter) throws ServiceException;

	
}
