package com.pradera.billing.process.service.executor;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.ejb.AccessTimeout;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.billing.billing.facade.BillingProcessedServiceFacade;
import com.pradera.billing.billing.to.BillingProcessedFilter;
import com.pradera.billing.billingcollection.facade.CollectionServiceMgmtServiceFacade;
import com.pradera.billing.billingcollection.to.CollectionRecordTo;
import com.pradera.billing.billingcollection.to.DailyExchangeRateFilter;
import com.pradera.billing.billingcollection.to.ProcessedServiceTo;
import com.pradera.billing.billingexception.to.IssuerTo;
import com.pradera.billing.billingservice.to.BillingServiceTo;
import com.pradera.billing.common.facade.BusinessLayerCommonFacade;
import com.pradera.billing.common.utils.BillingUtils;
import com.pradera.billing.common.utils.SequenceGeneratorCode;
import com.pradera.billing.process.service.BillingCalculationService;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.billing.BillingCalculation;
import com.pradera.model.billing.BillingCalculationDetail;
import com.pradera.model.billing.BillingCalculationProcess;
import com.pradera.model.billing.BillingCorrelative;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.CollectionRecord;
import com.pradera.model.billing.ProcessedService;
import com.pradera.model.billing.type.BillingCalculationType;
import com.pradera.model.billing.type.EntityCollectionType;
import com.pradera.model.billing.type.ProcessedServiceType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.BillingCorrelativeType;
import com.pradera.model.generalparameter.type.DailyExchangeRoleType;
import com.pradera.model.generalparameter.type.InformationSourceType;
import com.pradera.model.issuancesecuritie.Issuer;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingCalculationExecutor.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Singleton
public class BillingCalculationExecutor implements BillingCalculationService {

	/** The log. */
	@Inject
	PraderaLogger log;

	/** The billing processed service facade. */
	@EJB
	BillingProcessedServiceFacade 		billingProcessedServiceFacade;

	/** The business layer common facade. */
	@EJB
	BusinessLayerCommonFacade	  		businessLayerCommonFacade;

	/** The sequence generator code. */
	@EJB
	SequenceGeneratorCode  				sequenceGeneratorCode;
	
	
	/** The general parameters facade. */
	@EJB GeneralParametersFacade 		generalParametersFacade;
	
	/** The helper component facade. */
	@EJB HelperComponentFacade 			helperComponentFacade;
	
	/** The collection service mgmt service facade. */
	@EJB
	CollectionServiceMgmtServiceFacade collectionServiceMgmtServiceFacade;
	
	/** The Constant TIME. */
	private static final long TIME	= 5;

	private boolean isDetail;
	
	/**
	 * 
	 *  PRELIMINARY BILLING PROCESS
	 *  
	 *  1.-  	Obtener todos los BillingSchedule relacionados con el BillingService y que coincidan con 
	 *   	  	la Periodo de Facturacion y la fecha de Corte (Fecha Final) 
	 * 	2.-		Con el ExceptionInvoiceGenerations 	que se Obtuvo obtener las Entidades (Entidad de Cobro) y 
	 * 			a quien se le va a cobrar que se encuentra en la tabla BillingEntityException
	 * 	3.- 	Obtener el listado de las entidades y consultar en la tabla CollectionRecord, segun el proceso 
	 * 			de Calculo
	 * 	4.- 	Por cada CollectionRecord,  se agrupan en facturas y se van sumando el collectionAmountBilled
	 * 			y a cada cobro se le coloca su EntityKeyCollection que es unico conformado por
	 * 	  		type entity | code entity | currency type | indicator integrated | collection period | collection date end
	 *	5.- 	Crear los BillingCalculation  
	 */	
	@SuppressWarnings("rawtypes")
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	@TransactionTimeout(unit=TimeUnit.HOURS,value=1)
	@AccessTimeout(unit=TimeUnit.MINUTES,value=60)
	@Performance 
	public void executePreliminaryProcessBilling(BillingProcessedFilter billingProcessedFilter) throws ServiceException {


		log.info("::::::::::::::::::::: INICIO DEL PROCESO PRELIMINAR DE FACTURACION ---> ENTIDAD DE COBRO : "+ billingProcessedFilter.getEntityCollection()
				+" - HORA : "+ CommonsUtilities.currentDateTime() +" :::::::::::::::::::::::::::::::::");


		BillingCalculationProcess	billingCalculationProcess	= null;	
		List<BillingCalculation> 	billingCalculationList = null;
		List <CollectionRecord> 	collectionRecordListToModify=new ArrayList<>();
		Integer indMinimumAmount= null;
		BigDecimal minimumAmount= null;
		Integer currencyMinimumAmount= null;
		
		Map<String, Object> parameters = new HashMap<String, Object>();

		DailyExchangeRateFilter dailyExchangeRateFilter = new DailyExchangeRateFilter();			
		dailyExchangeRateFilter.setIdSourceinformation(InformationSourceType.BCRD.getCode());	
		dailyExchangeRateFilter.setInitialDate((billingProcessedFilter.getBillingDateEnd()==null?null:CommonsUtilities.truncateDateTime(billingProcessedFilter.getBillingDateEnd()) ));
		dailyExchangeRateFilter.setFinalDate((billingProcessedFilter.getBillingDateEnd()==null?null:CommonsUtilities.truncateDateTime(billingProcessedFilter.getBillingDateEnd()) ));


		Map<Integer,Map> exchangeSell= new HashMap<Integer,Map>();
		exchangeSell=businessLayerCommonFacade.chargeExchangeMemorySelllBs(dailyExchangeRateFilter, DailyExchangeRoleType.SELL.getCode());
		parameters.put(GeneralConstants.MATRIX_EXCHANGE_RATE_SELL,exchangeSell);

		if(Validations.validateIsNotNullAndPositive(billingProcessedFilter.getIdHolderByParticipant())){
			isDetail=Boolean.TRUE;
		}
		/**
		 *  FIND ALL LOTS (PROCESSD SERVICES) ACCORDING TO PERIOD
		 */
		List<ProcessedService> processedServiceList  = billingProcessedServiceFacade.getProcessedServiceForCalculateBillingWithException(billingProcessedFilter);

		if (Validations.validateListIsNotNullAndNotEmpty(processedServiceList)){
			
			billingCalculationList	= new ArrayList<BillingCalculation>();
			
			/*** MAKING FILTER OF PROCESSED SERVICE **/
			ProcessedServiceTo 		processedServiceTo	= new ProcessedServiceTo();
			processedServiceTo.getCollectionRecordToSearch().setIndBilled(PropertiesConstants.NOT_BILLED);
			processedServiceTo.getCollectionRecordToSearch().setRecordState(ProcessedServiceType.PROCESADO.getCode());
			
			if ( Validations.validateIsNotNullAndPositive(billingProcessedFilter.getIndIntegratedBill())){
				BillingServiceTo billingServiceTo = new BillingServiceTo();
				billingServiceTo.setInIntegratedBill(billingProcessedFilter.getIndIntegratedBill());
				
				if ( billingProcessedFilter.getIndIntegratedBill().equals(PropertiesConstants.NOT_IND_INTEGRATED_BILL)){						
					if ( Validations.validateIsNotNull(billingProcessedFilter.getBillingServicePk())){
						billingServiceTo.setIdBillingServicePk(billingProcessedFilter.getBillingServicePk());
					}						
				}
				
				processedServiceTo.setBillingServiceTo(billingServiceTo);
			}
			
			List<Holder> listHolder=Validations.validateListIsNotNullAndNotEmpty(billingProcessedFilter.getHolderList())
							?billingProcessedFilter.getHolderList()
							:collectionServiceMgmtServiceFacade.findAllHolder();
			List<Participant> listParticipant = helperComponentFacade.getLisParticipantServiceFacade(new Participant());
			List<Issuer> listIssuer=collectionServiceMgmtServiceFacade.findAllIssuer();
			
			if (Validations.validateIsNotNull(billingProcessedFilter.getEntityCollection()) ){
				
				if (billingProcessedFilter.getEntityCollection().equals(EntityCollectionType.HOLDERS.getCode()) &&
						Validations.validateIsNotNull(billingProcessedFilter.getHolderPk()) ){
					
					processedServiceTo.getCollectionRecordToSearch().setHolder(new Holder(billingProcessedFilter.getHolderPk()));
					
				}else if ( billingProcessedFilter.getEntityCollection().equals(EntityCollectionType.ISSUERS.getCode()) &&
						Validations.validateIsNotNull(billingProcessedFilter.getIssuerPk()) ){
					
					processedServiceTo.getCollectionRecordToSearch().setIssuer( new Issuer(billingProcessedFilter.getIssuerPk()));
					
				}else if ( billingProcessedFilter.getEntityCollection().equals(EntityCollectionType.PARTICIPANTS.getCode()) &&	
						Validations.validateIsNotNull(billingProcessedFilter.getParticipantPk()) ){
					
					processedServiceTo.getCollectionRecordToSearch().setParticipant(new Participant(billingProcessedFilter.getParticipantPk()));
					if(isDetail){
						processedServiceTo.getCollectionRecordToSearch().setIdHolderByParticipant(billingProcessedFilter.getIdHolderByParticipant());
					}
					
					
				}else if ( billingProcessedFilter.getEntityCollection().equals(EntityCollectionType.AFP.getCode()) &&	
						Validations.validateIsNotNull(billingProcessedFilter.getParticipantPk()) ){
					
					processedServiceTo.getCollectionRecordToSearch().setParticipant(new Participant(billingProcessedFilter.getParticipantPk()));
					
				}else if ( billingProcessedFilter.getEntityCollection().equals(EntityCollectionType.OTHERS.getCode())  ){
					
					if (Validations.validateIsNotNullAndNotEmpty(billingProcessedFilter.getDocumentCodeOther())){
						processedServiceTo.getCollectionRecordToSearch().setOtherEntityDocument(billingProcessedFilter.getDocumentCodeOther());
					}
					
					if (Validations.validateIsNotNullAndPositive(billingProcessedFilter.getDocumentTypeOther()) ){
						processedServiceTo.getCollectionRecordToSearch().setDocumentType(billingProcessedFilter.getDocumentTypeOther());
					}
					
				}
				
				
				/** APPLY FOR ALL ENTITIES COLLECIONS**/
				processedServiceTo.getCollectionRecordToSearch().setEntityCollection(billingProcessedFilter.getEntityCollection());
				
			}
			/***END**/

			/***
			 *     FIND THE COLLECTION FOR EACH LOT ( PROCESSED SERVICE)
			 */
			for (ProcessedService processedService : processedServiceList) {

				processedServiceTo.setIdProcessedServicePk(processedService.getIdProcessedServicePk());
				processedServiceTo.setListHolder(listHolder);
				processedServiceTo.setListIssuer(listIssuer);
				processedServiceTo.setListParticipant(listParticipant);
				List <CollectionRecord> collectionRecordList = collectionServiceMgmtServiceFacade.findCollectionRecordByParametersException(processedService,processedServiceTo);
				collectionRecordListToModify.addAll(collectionRecordList);

				String 	keyEntityCollection  =	"";
				Integer indIntegratedBill	 = 	processedService.getBillingService().getInIntegratedBill();

				if(Validations.validateListIsNotNullAndNotEmpty(collectionRecordList)){
					
					for (CollectionRecord collectionRecord : collectionRecordList) {

						indIntegratedBill	=	processedService.getBillingService().getInIntegratedBill();
						//If Not Integrated Bill: Service For Service
						if(indIntegratedBill!=null && PropertiesConstants.NOT_IND_INTEGRATED_BILL.equals(indIntegratedBill)&&
								billingProcessedFilter.getBillingServicePk()==null){
							billingProcessedFilter.setBillingServicePk(processedService.getBillingService().getIdBillingServicePk());
						}
						
						/*** Getting key generated for entity collection , currency type , indicator integrate bill , period and date calculation**/
						keyEntityCollection =	businessLayerCommonFacade.getKeyEntityCollection(collectionRecord,indIntegratedBill,billingProcessedFilter); 
						
						collectionRecord.setKeyEntityCollection(keyEntityCollection);
						
						Boolean  findKeyEntityCollection	= Boolean.FALSE;
						
						for (BillingCalculation billingCalculationNext : billingCalculationList) {

							/**IF FOR MATCH  KEY : ENTITY , INDICATOR INTEGRATED , CURRENCY TYPE **/
							if (billingCalculationNext.getKeyEntityCollection().equalsIgnoreCase(keyEntityCollection)){
								/*** BILL INTEGRATED **/

								findKeyEntityCollection	= Boolean.TRUE;

								if (indIntegratedBill.equals(PropertiesConstants.YES_IND_INTEGRATED_BILL)){

									/** Add Amount at Bill**/
									addAmountOnBill(billingCalculationNext, collectionRecord);
									
									/** Concatenate Number Request*/
									addConcatenatedRequest(billingCalculationNext, collectionRecord);
									/**Concatenate Mnemonic Holder */
									addConcatenatedMnemonic(billingCalculationNext, collectionRecord);

									BillingService service	= processedService.getBillingService();
									/** GETTING DETAILS OF BILL***/
									List<BillingCalculationDetail> details =billingCalculationNext.getBillingCalculationDetails();	

									BillingCalculationDetail billCalculationDetail = this.findBillingCalculationDetail(details, service);

									if (Validations.validateIsNull(billCalculationDetail)){
										/***
										 * Add the first BillingCalculationDetail
										 * **/
										BillingCalculationDetail billingCalculationDetailNew = new  BillingCalculationDetail();
										billingCalculationDetailNew.setCollectionAmount(collectionRecord.getCollectionAmountBilled());
										billingCalculationDetailNew.setGrossAmount(collectionRecord.getGrossAmountBilled());
										billingCalculationDetailNew.setTaxApplied(collectionRecord.getTaxAppliedBilled());
										billingCalculationDetailNew.setBillingService(processedService.getBillingService());

										billingCalculationNext.getBillingCalculationDetails().add(billingCalculationDetailNew);
									}else{

										addAmountOnBillDetail(billCalculationDetail, collectionRecord, service);
										
									}

									/** DON'T LOOK FOR MORE BILLS WHEN THE SERVICE IS INTEGRATED***/

									break;


								}else { /** BILL NOT INTEGRATED ***/

									BillingService service	= processedService.getBillingService();
									List<BillingCalculationDetail> details =billingCalculationNext.getBillingCalculationDetails();	

									BillingCalculationDetail billCalculationDetail = this.findBillingCalculationDetail(details, service);

									if (Validations.validateIsNotNull(billCalculationDetail)){

										/** Add Amount at Bill**/
										addAmountOnBill(billingCalculationNext, collectionRecord);
										
										/** Add Amount at Bill Detail**/
										addAmountOnBillDetail(billCalculationDetail, collectionRecord, processedService.getBillingService());

										/** Concatenate Number Request*/
										addConcatenatedRequest(billingCalculationNext, collectionRecord);
										
										/**Concatenate Mnemonic Holder */
										addConcatenatedMnemonic(billingCalculationNext, collectionRecord);
										
										/** Add to Minimum Amount to Compare*/
										indMinimumAmount= collectionRecord.getIndMinimumAmount();
										minimumAmount= collectionRecord.getMinimumAmount();
										currencyMinimumAmount= collectionRecord.getCurrencyMinimumAmount();

									}else{

										findKeyEntityCollection	= Boolean.FALSE;
										continue;
									}


								}

							} /**END IF MATCH **/


						}


						if (!findKeyEntityCollection  ){

							BillingCalculation	billingCalculation = new BillingCalculation();
							this.settingEntityCollection(billingCalculation, collectionRecord);

							billingCalculation.setBillingDateStart(billingProcessedFilter.getBillingDateStart());
							billingCalculation.setBillingCourtDate(billingProcessedFilter.getBillingDateEnd());
							billingCalculation.setBillingDate(billingProcessedFilter.getBillingDate());
							
							billingCalculation.setCurrencyType(collectionRecord.getCurrencyBilling());/*****/
							billingCalculation.setGrossAmount(collectionRecord.getGrossAmountBilled());
							billingCalculation.setNetAmount(collectionRecord.getCollectionAmountBilled());							
							billingCalculation.setNetTax(collectionRecord.getTaxAppliedBilled());

							/** Number To Request */
							billingCalculation.setCountRequest(collectionRecord.getCountRequest());
							billingCalculation.setRequestConcatenated(collectionRecord.getRequestConcatenated());
							billingCalculation.setMnemonicConcatenated(collectionRecord.getMnemonicConcatenated());
							
							/** Add to Minimum Amount to Compare*/
							indMinimumAmount= collectionRecord.getIndMinimumAmount();
							minimumAmount= collectionRecord.getMinimumAmount();
							currencyMinimumAmount= collectionRecord.getCurrencyMinimumAmount();

							/** SETTING ATRIBUTTES DISTINTIVOS**/
							billingCalculation.setKeyEntityCollection(keyEntityCollection);
							billingCalculation.setIndIntegratedBill(processedService.getBillingService().getInIntegratedBill());
							
							
							/***  Add the first BillingCalculationDetail
							 * **/
							BillingCalculationDetail billingCalculationDetailNew = new  BillingCalculationDetail();
							billingCalculationDetailNew.setCollectionAmount(collectionRecord.getCollectionAmountBilled());
							billingCalculationDetailNew.setGrossAmount(collectionRecord.getGrossAmountBilled());
							billingCalculationDetailNew.setTaxApplied(collectionRecord.getTaxAppliedBilled());
							billingCalculationDetailNew.setBillingService(processedService.getBillingService());

							billingCalculation.getBillingCalculationDetails().add(billingCalculationDetailNew);


							/*** Add BillingCalculation on List */
							billingCalculationList.add(billingCalculation);

						}
						
					}
				}


			}

			updateCollectionRecordByBillingCalculation(collectionRecordListToModify);

		}

		appliedMinimumAmount(billingCalculationList,indMinimumAmount,minimumAmount,currencyMinimumAmount,parameters);

		/** Add Bills to **/
		if (Validations.validateIsNotNullAndNotEmpty(billingCalculationList)){
			
			/**** ACA DEBE ELIMINAR TODAS LAS FACTURAS Q TIENEN EL MISMO KEY BILL*/
			
			if (billingProcessedFilter.getCollectionPeriod().equals(PropertiesConstants.COLLECTION_PERIOD_MONTHLY_PK)){

				String strDate       	=CommonsUtilities.convertDateToString(billingProcessedFilter.getBillingDateEnd(),BillingUtils.DATE_PATTERN_yyyy_MM_dd);
				String strYearMonth  	=BillingUtils.getPatternYearMonth(strDate, BillingUtils.DATE_PATTERN_yyyy_MM_dd);							
				billingProcessedFilter.setMonthBilling(BillingUtils.getPatternMonth(strYearMonth,BillingUtils.DATE_PATTERN_yyyy_MM));
				billingProcessedFilter.setYearBilling(BillingUtils.getPatternYear(strYearMonth,BillingUtils.DATE_PATTERN_yyyy_MM));
				
			}
			
			billingProcessedServiceFacade.deleteBillingCalculationToProcessPreliminary(billingCalculationList, billingProcessedFilter);
			
			
			billingCalculationProcess = new BillingCalculationProcess();
			billingCalculationProcess.setOperationDate(CommonsUtilities.currentDateTime());
			//La Fecha de Facturacion es a Fecha de Corte
			billingCalculationProcess.setBillingDate(billingProcessedFilter.getBillingDateEnd());
			
			if (billingProcessedFilter.getCollectionPeriod().equals(PropertiesConstants.COLLECTION_PERIOD_MONTHLY_PK)){
				
				billingCalculationProcess.setBillingMonth(billingProcessedFilter.getMonthBilling());
				billingCalculationProcess.setBillingYear(billingProcessedFilter.getYearBilling());
			}
			
			billingCalculationProcess.setEntityCollection(billingProcessedFilter.getEntityCollection());
			billingCalculationProcess.setBillingPeriod(billingProcessedFilter.getCollectionPeriod());
			billingCalculationProcess.setProcessedState(BillingCalculationType.PRELIMINAR.getCode());

			billingCalculationProcess.setBillingCalculations(billingCalculationList);
			billingCalculationProcess.setFileXml(new byte[0]);
			billingProcessedServiceFacade.saveAllBillingProcessedService(billingCalculationProcess);
			billingProcessedFilter.setProcessBillingCalculationPk(billingCalculationProcess.getIdBillingCalcProcessPk());
			billingProcessedFilter.setNumberBilling(billingCalculationList!=null?billingCalculationList.size():0);
		}
		

		try {
			billingProcessedServiceFacade.flushTransaction();
			if(Validations.validateIsNotNull(billingCalculationProcess)) {
				billingProcessedServiceFacade.generateInterfaceXml(billingCalculationProcess.getIdBillingCalcProcessPk());
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}
	
	/**
	 * Applied minimum amount.
	 *
	 * @param billingCalculationList the billing calculation list
	 * @param indMinimumAmount the ind minimum amount
	 * @param minimumAmount the minimum amount
	 * @param currencyMinimumAmount the currency minimum amount
	 * @param parameters the parameters
	 * @throws ServiceException the service exception
	 */
	public void appliedMinimumAmount(List<BillingCalculation> billingCalculationList, Integer indMinimumAmount,
			BigDecimal minimumAmount , Integer currencyMinimumAmount, Map<String, Object> parameters ) throws ServiceException{

		if(Validations.validateIsNotNullAndPositive(indMinimumAmount)&&
				Validations.validateListIsNotNullAndNotEmpty(billingCalculationList)){
			for (BillingCalculation billingCalculation : billingCalculationList) {
				
				//Solo las Facturas que tienen Indicador de Individuales
				if(PropertiesConstants.NOT_IND_INTEGRATED_BILL.equals(billingCalculation.getIndIntegratedBill())){
					BigDecimal amountNew	=BigDecimal.ZERO;
					BigDecimal netTax		=BigDecimal.ZERO;
					BigDecimal netAmount	=BigDecimal.ZERO;
					amountNew=	businessLayerCommonFacade.appliedMinimumAmountBilling(billingCalculation.getGrossAmount(),
										indMinimumAmount,
										minimumAmount, currencyMinimumAmount, parameters,
										billingCalculation.getCurrencyType());

					netAmount=	businessLayerCommonFacade.appliedMinimumAmountBilling(billingCalculation.getNetAmount(),
							indMinimumAmount,
							minimumAmount, currencyMinimumAmount, parameters,
							billingCalculation.getCurrencyType());
					
					if(Validations.validateIsNotNullAndPositive(billingCalculation.getNetTax())){
						netTax=	businessLayerCommonFacade.appliedMinimumAmountBilling(billingCalculation.getNetTax(),
									indMinimumAmount,
									minimumAmount, currencyMinimumAmount, parameters,
									billingCalculation.getCurrencyType());
						}
					billingCalculation.setGrossAmount(amountNew);
					billingCalculation.setNetAmount(netAmount);
					billingCalculation.setNetTax(netTax);
				}
				
			}	
		}
		
	}
	

	/**
	 * Adds the amount on bill detail.
	 *
	 * @param billingCalculationDetail the billing calculation detail
	 * @param collectionRecord the collection record
	 * @param billingService the billing service
	 */
	protected void 	addAmountOnBillDetail(BillingCalculationDetail billingCalculationDetail, CollectionRecord collectionRecord, BillingService billingService){


		BigDecimal grossAmount				= BillingUtils.addTwoDecimal(collectionRecord.getGrossAmountBilled(), billingCalculationDetail.getGrossAmount(), GeneralConstants.ROUNDING_DIGIT_NUMBER);
		billingCalculationDetail.setGrossAmount(grossAmount);

		BigDecimal collectionAmount			= BillingUtils.addTwoDecimal(collectionRecord.getCollectionAmountBilled(),billingCalculationDetail.getCollectionAmount(), GeneralConstants.ROUNDING_DIGIT_NUMBER); 
		billingCalculationDetail.setCollectionAmount(collectionAmount);

		BigDecimal taxAmount 				= BillingUtils.addTwoDecimal(collectionRecord.getTaxAppliedBilled(), billingCalculationDetail.getTaxApplied(), GeneralConstants.ROUNDING_DIGIT_NUMBER);
		billingCalculationDetail.setTaxApplied(taxAmount);

		billingCalculationDetail.setBillingService(billingService);
	}


	/**
	 * Adds the amount on bill.
	 *
	 * @param billingCalculation the billing calculation
	 * @param collectionRecord the collection record
	 */
	protected void 	addAmountOnBill(BillingCalculation billingCalculation , CollectionRecord collectionRecord ){

		BigDecimal grossAmount				= BillingUtils.addTwoDecimal(collectionRecord.getGrossAmountBilled(), billingCalculation.getGrossAmount(), GeneralConstants.ROUNDING_DIGIT_NUMBER);
		billingCalculation.setGrossAmount(grossAmount);

		BigDecimal netAmount				= BillingUtils.addTwoDecimal(collectionRecord.getCollectionAmountBilled(), billingCalculation.getNetAmount(), GeneralConstants.ROUNDING_DIGIT_NUMBER); 
		billingCalculation.setNetAmount(netAmount);

		BigDecimal netTax 					= BillingUtils.addTwoDecimal(collectionRecord.getTaxAppliedBilled(), billingCalculation.getNetTax(), GeneralConstants.ROUNDING_DIGIT_NUMBER);
		billingCalculation.setNetTax(netTax);
		
		Integer numberRequest				= BillingUtils.addTwoInteger(billingCalculation.getCountRequest(), collectionRecord.getCountRequest());
		billingCalculation.setCountRequest(numberRequest);
	}


	/**
	 * Adds the concatenated request.
	 *
	 * @param billingCalculation the billing calculation
	 * @param collectionRecord the collection record
	 */
	protected void addConcatenatedRequest(BillingCalculation billingCalculation , CollectionRecord collectionRecord ){
		
		StringBuilder request=new StringBuilder();
		request.append(billingCalculation.getRequestConcatenated());
		if(Validations.validateIsNotNullAndNotEmpty(collectionRecord.getRequestConcatenated())){
			billingCalculation.getRequestConcatenated();
			request.append(GeneralConstants.STR_COMMA).append(collectionRecord.getRequestConcatenated());
		}
		
		billingCalculation.setRequestConcatenated(request.toString());
		
	}

	/**
	 * Adds the concatenated request.
	 *
	 * @param billingCalculation the billing calculation
	 * @param collectionRecord the collection record
	 */
	protected void addConcatenatedMnemonic(BillingCalculation billingCalculation , CollectionRecord collectionRecord ){
		
		StringBuilder request=new StringBuilder();
		billingCalculation.getMnemonicConcatenated();
		//Si MnemonicCollectionRecord ==null === MnemonicBilling no hace nada
		//Si MnemonicBilling == null === MnemoninBilling=MnemonicCollectionRecord
		//Si MnemonicBilling !=MnemonicCollectionRecord === Concatenar
		//Else MnemonicBilling=MnemonicCollectionRecord
		if(Validations.validateIsNotNullAndNotEmpty(collectionRecord.getMnemonicConcatenated())) {
			if(Validations.validateIsNullOrEmpty(billingCalculation.getMnemonicConcatenated())) {
				billingCalculation.setMnemonicConcatenated(collectionRecord.getMnemonicConcatenated());
			}else if(!(collectionRecord.getMnemonicConcatenated().trim().equals(billingCalculation.getMnemonicConcatenated().trim()))) {
				request.append(billingCalculation.getMnemonicConcatenated());
				request.append(GeneralConstants.STR_COMMA).append(collectionRecord.getMnemonicConcatenated());
				billingCalculation.setMnemonicConcatenated(request.toString());
			}else {
				billingCalculation.setMnemonicConcatenated(collectionRecord.getMnemonicConcatenated());
			}
		}
		
		
		
		
	}
	
	

	/**
	 * *
	 * Method allows you to find services on memory.
	 *
	 * @param billingCalculationDetailList the billing calculation detail list
	 * @param service the service
	 * @return BillingCalculationDetail
	 */
	protected BillingCalculationDetail	findBillingCalculationDetail(List<BillingCalculationDetail> billingCalculationDetailList, BillingService service){

		for (BillingCalculationDetail billingCalculationDetail : billingCalculationDetailList) {
			if (billingCalculationDetail.getBillingService().getIdBillingServicePk().equals(service.getIdBillingServicePk())){
				return billingCalculationDetail;
			}
		}

		return null;
	}



	/**
	 * *.
	 *
	 * @param billingCalculation the billing calculation
	 * @param collectionRecord the collection record
	 */
	protected void settingEntityCollection(BillingCalculation billingCalculation , CollectionRecord collectionRecord){

		if(collectionRecord.getEntityCollectionId().equals(EntityCollectionType.HOLDERS.getCode())){			
			billingCalculation.setEntityCollection(EntityCollectionType.HOLDERS.getCode());
			billingCalculation.setHolder(collectionRecord.getHolder());			
			billingCalculation.setEntityCode(collectionRecord.getHolder().getIdHolderPk().toString());
			billingCalculation.setEntityDescription(collectionRecord.getHolder().getFullName());

		}else if(collectionRecord.getEntityCollectionId().equals(EntityCollectionType.ISSUERS.getCode())){			
			billingCalculation.setEntityCollection(EntityCollectionType.ISSUERS.getCode());
			billingCalculation.setIssuer(collectionRecord.getIssuer());			
			billingCalculation.setEntityCode(collectionRecord.getIssuer().getIdIssuerPk());
			billingCalculation.setEntityDescription(collectionRecord.getIssuer().getBusinessName());

		}else if(collectionRecord.getEntityCollectionId().equals(EntityCollectionType.PARTICIPANTS.getCode())){			
			billingCalculation.setEntityCollection(EntityCollectionType.PARTICIPANTS.getCode());
			billingCalculation.setParticipant(collectionRecord.getParticipant());			
			billingCalculation.setEntityCode(collectionRecord.getParticipant().getIdParticipantPk().toString());
			billingCalculation.setEntityDescription(collectionRecord.getParticipant().getDescription());

		}else if(collectionRecord.getEntityCollectionId().equals(EntityCollectionType.OTHERS.getCode())){	
			billingCalculation.setEntityCollection(EntityCollectionType.OTHERS.getCode());			
			billingCalculation.setEntityCode(collectionRecord.getOtherEntityDocument());
			billingCalculation.setEntityDescription(collectionRecord.getOtherEntityDescription());	
			billingCalculation.setIndentifyType(collectionRecord.getDocumentType());
			
			billingCalculation.setOtherEntityAddress(collectionRecord.getOtherEntityAddress());
			billingCalculation.setOtherEntityCountry(collectionRecord.getOtherEntityCountry());
			billingCalculation.setOtherEntityDepartment(collectionRecord.getOtherEntityDepartment());
			billingCalculation.setOtherEntityDistrict(collectionRecord.getOtherEntityDistrict());
			billingCalculation.setOtherEntityProvince(collectionRecord.getOtherEntityProvince());
			
			
		}else if(collectionRecord.getEntityCollectionId().equals(EntityCollectionType.AFP.getCode())){
			billingCalculation.setEntityCollection(EntityCollectionType.AFP.getCode());
			billingCalculation.setParticipant(collectionRecord.getParticipant());			
			billingCalculation.setEntityCode(collectionRecord.getParticipant().getIdParticipantPk().toString());
			billingCalculation.setEntityDescription(collectionRecord.getParticipant().getDescription());
			
		}else if(collectionRecord.getEntityCollectionId().equals(EntityCollectionType.HACIENDA.getCode())){
			billingCalculation.setEntityCollection(EntityCollectionType.HACIENDA.getCode());
			billingCalculation.setEntityCode(EntityCollectionType.HACIENDA.getCode().toString());
			billingCalculation.setEntityDescription(EntityCollectionType.HACIENDA.getValue());
			
		}else if(collectionRecord.getEntityCollectionId().equals(EntityCollectionType.CENTRAL_BANK.getCode())){
			billingCalculation.setEntityCollection(EntityCollectionType.CENTRAL_BANK.getCode());
			billingCalculation.setEntityCode(EntityCollectionType.CENTRAL_BANK.getCode().toString());
			billingCalculation.setEntityDescription(EntityCollectionType.CENTRAL_BANK.getValue());
			
		}else if(collectionRecord.getEntityCollectionId().equals(EntityCollectionType.STOCK_EXCHANGE.getCode())){
			billingCalculation.setEntityCollection(EntityCollectionType.STOCK_EXCHANGE.getCode());
			billingCalculation.setEntityCode(EntityCollectionType.STOCK_EXCHANGE.getCode().toString());
			billingCalculation.setEntityDescription(EntityCollectionType.STOCK_EXCHANGE.getValue());
			
		}else if(collectionRecord.getEntityCollectionId().equals(EntityCollectionType.REGULATOR_ENTITY.getCode())){
			billingCalculation.setEntityCollection(EntityCollectionType.REGULATOR_ENTITY.getCode());
			billingCalculation.setEntityCode(EntityCollectionType.REGULATOR_ENTITY.getCode().toString());
			billingCalculation.setEntityDescription(EntityCollectionType.REGULATOR_ENTITY.getValue());
			
		}
		
		
	}

	
	
	
	

	/**
	 *  DEFINITIVE BILLING PROCESS .
	 *
	 * @param billingProcessedFilter the billing processed filter
	 * @throws ServiceException the service exception
	 */
	

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	@TransactionTimeout(unit=TimeUnit.HOURS,value=1)
	@AccessTimeout(unit=TimeUnit.MINUTES,value=60)
	@Performance 
	public void executeDefinitiveProcessBilling(BillingProcessedFilter billingProcessedFilter) throws ServiceException {
		

		log.info("::::::::::::::::::::: INICIO DEL PROCESO DEFINITIVO DE FACTURACION " +
				 " - HORA : "+ CommonsUtilities.currentDateTime() +" :::::::::::::::::::::::::::::::::");
		
		
		
		BillingCalculationProcess	billingCalculationProcess	= billingProcessedServiceFacade.findBillingCalculationProcessById(billingProcessedFilter.getProcessBillingCalculationPk());
		/** If mark date Billing by screen Then Get Billing Date Becoming*/
		if(Validations.validateIsNotNull(billingProcessedFilter.getBillingDateBecoming())){
			billingCalculationProcess.setBillingDate(billingProcessedFilter.getBillingDateBecoming());
		}else{
			billingCalculationProcess.setBillingDate(CommonsUtilities.currentDateTime());
		}
		
		List<BillingCalculation> billingCalculations	= billingCalculationProcess.getBillingCalculations();	
		
		if (Validations.validateListIsNotNullAndNotEmpty(billingCalculations)){
			
			CollectionRecordTo	collectionRecordToSearch	= new CollectionRecordTo();
			ProcessedServiceTo processedServiceTo			= new ProcessedServiceTo() ;
			processedServiceTo.setCollectionRecordToSearch(collectionRecordToSearch);
			
			BillingCorrelative billingCorrelative	= new BillingCorrelative();
			
			
		   for (Iterator<BillingCalculation> iterBillingCalculation=billingCalculations.iterator(); iterBillingCalculation.hasNext();) {
				BillingCalculation billingCalculation = (BillingCalculation) iterBillingCalculation.next();
				
				processedServiceTo.setIdProcessedServicePk(null);				
				String keyEntityCollection	= billingCalculation.getKeyEntityCollection(); 				
				processedServiceTo.getCollectionRecordToSearch().setKeyEntityCollection(keyEntityCollection);
				
				
				/**
				 *  SETEAR DIRECCIONES Y GEOGRAFIA
				 */
				
				this.settingGeography(billingCalculation);
				
				
				/****  
				 * 
				 * GETTING CORRELATIVES :::>   NCF AND  INTERNAL SEQUENCE
				 * SequenceGeneratorCode
				 * 
				 * 
				 * ***/						
				
				billingCorrelative.setCorrelativeCD(BillingCorrelativeType.BILL_CODE_COR.getValue());	
				billingCorrelative.setBillingSerie(null);
				billingCorrelative.setBillingTCF(null);				
				Long 	numericSequence		=	sequenceGeneratorCode.getLastSequenceGeneratorCode(billingCorrelative);
				billingCalculation.setBillingNumber(numericSequence);
				
				if(Validations.validateIsNotNull(billingProcessedFilter.getBillingDateBecoming())){
					billingCalculation.setBillingDate(billingProcessedFilter.getBillingDateBecoming());
				}else{
					billingCalculation.setBillingDate(CommonsUtilities.currentDateTime());
				}
				
				// This key is for getting all collection Same 
				List<CollectionRecord> listCollectionRecord	=	collectionServiceMgmtServiceFacade.findCollectionRecordByParameters(processedServiceTo);
				
				// recorrer cada cobro y setear su estado a facturado y el indicador a  1
				for (Iterator<CollectionRecord> iter=listCollectionRecord.iterator(); iter.hasNext();) {
					CollectionRecord collectionRecord = (CollectionRecord) iter.next();
					collectionRecord.setRecordState(ProcessedServiceType.FACTURADO.getCode());
					collectionRecord.setIndBilled(PropertiesConstants.YES_BILLED);
					
					collectionServiceMgmtServiceFacade.updateCollectionRecordByQuery(collectionRecord);
					// luego obtener la pk del lote de cobro y actualizar el estado segun el query update creado
					processedServiceTo.setIdProcessedServicePk(collectionRecord.getProcessedService().getIdProcessedServicePk());
					processedServiceTo.setProcessedState(ProcessedServiceType.FACTURADO.getCode());
					collectionServiceMgmtServiceFacade.updateProcessedServiceOnBilling(processedServiceTo);
									
					iter.remove();
				}
				
								
				billingProcessedServiceFacade.updateBillingCalculation(billingCalculation);
				iterBillingCalculation.remove();

				
			}
			
			/*** SETTING  ESTATUS FOR BILLING CALCULATION PROCESS WITH : DEFINITIVE**/
			billingCalculationProcess.setProcessedState(BillingCalculationType.DEFINITIVO.getCode());
			billingProcessedServiceFacade.updateBillingCalculationProcess(billingCalculationProcess);
			billingProcessedFilter.setBillingProcessedState(BillingCalculationType.DEFINITIVO.getCode());
			
		}
		
		try {
			billingProcessedServiceFacade.flushTransaction();
			if(Validations.validateIsNotNull(billingCalculationProcess)) {
				billingProcessedServiceFacade.generateInterfaceXml(billingCalculationProcess.getIdBillingCalcProcessPk());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	
	
	
	/**
	 * Sets the ting geography.
	 *
	 * @param billingCalculation the new ting geography
	 * @throws ServiceException the service exception
	 */
	protected void settingGeography(BillingCalculation billingCalculation ) throws ServiceException{

		if(billingCalculation.getEntityCollection().equals(EntityCollectionType.HOLDERS.getCode())){
			
			Holder holder	=	helperComponentFacade.findHolderByCode(billingCalculation.getHolder().getIdHolderPk());
			billingCalculation.setOtherEntityAddress(holder.getLegalAddress());
			billingCalculation.setOtherEntityCountry(holder.getLegalResidenceCountry());
			billingCalculation.setOtherEntityDepartment(holder.getLegalDepartment());
			billingCalculation.setOtherEntityProvince(holder.getLegalProvince());
			billingCalculation.setOtherEntityDistrict(holder.getLegalDistrict());
			
		}else if(billingCalculation.getEntityCollection().equals(EntityCollectionType.ISSUERS.getCode())){
			
			IssuerTo issuerTo=new IssuerTo();
			issuerTo=businessLayerCommonFacade.getGeographicLocationForIssuer(billingCalculation.getIssuer().getIdIssuerPk());
			billingCalculation.setOtherEntityAddress(issuerTo.getAddressIssuer());
			billingCalculation.setOtherEntityCountry(issuerTo.getCountry());
			billingCalculation.setOtherEntityDepartment(issuerTo.getDepartament());
			billingCalculation.setOtherEntityProvince(issuerTo.getProvince());
			billingCalculation.setOtherEntityDistrict(issuerTo.getDistrict());
			
		}else if(billingCalculation.getEntityCollection().equals(EntityCollectionType.PARTICIPANTS.getCode())){
			Participant participant = helperComponentFacade.findParticipantsByCode(billingCalculation.getParticipant().getIdParticipantPk());
			billingCalculation.setOtherEntityAddress(participant.getAddress());
			billingCalculation.setOtherEntityCountry(participant.getResidenceCountry());
			billingCalculation.setOtherEntityDepartment(participant.getDepartment());
			billingCalculation.setOtherEntityProvince(participant.getProvince());
			billingCalculation.setOtherEntityDistrict(participant.getDistrict());
			

		}else if(billingCalculation.getEntityCollection().equals(EntityCollectionType.AFP.getCode())){
			Participant participant = helperComponentFacade.findParticipantsByCode(billingCalculation.getParticipant().getIdParticipantPk());
			billingCalculation.setOtherEntityAddress(participant.getAddress());
			billingCalculation.setOtherEntityCountry(participant.getResidenceCountry());
			billingCalculation.setOtherEntityDepartment(participant.getDepartment());
			billingCalculation.setOtherEntityProvince(participant.getProvince());
			billingCalculation.setOtherEntityDistrict(participant.getDistrict());
			
		}else if(billingCalculation.getEntityCollection().equals(EntityCollectionType.HACIENDA.getCode())){
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setParameterTablePk(billingCalculation.getEntityCollection());
			List<ParameterTable>  list	= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			ParameterTable parameterTable	= list.get(0);
			billingCalculation.setOtherEntityAddress(parameterTable.getObservation());
			billingCalculation.setOtherEntityCountry(parameterTable.getIndicator4());
			billingCalculation.setOtherEntityDepartment(parameterTable.getIndicator4());
			billingCalculation.setOtherEntityProvince(parameterTable.getIndicator5());
			billingCalculation.setOtherEntityDistrict(parameterTable.getIndicator6());
			
		}else if(billingCalculation.getEntityCollection().equals(EntityCollectionType.CENTRAL_BANK.getCode())){
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setParameterTablePk(billingCalculation.getEntityCollection());
			List<ParameterTable>  list	= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			ParameterTable parameterTable	= list.get(0);
			
			billingCalculation.setOtherEntityAddress(parameterTable.getObservation());
			billingCalculation.setOtherEntityCountry(parameterTable.getIndicator4());
			billingCalculation.setOtherEntityDepartment(parameterTable.getIndicator4());
			billingCalculation.setOtherEntityProvince(parameterTable.getIndicator5());
			billingCalculation.setOtherEntityDistrict(parameterTable.getIndicator6());
			
			
		}else if(billingCalculation.getEntityCollection().equals(EntityCollectionType.STOCK_EXCHANGE.getCode())){
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setParameterTablePk(billingCalculation.getEntityCollection());
			List<ParameterTable>  list	= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			ParameterTable parameterTable	= list.get(0);
			
			billingCalculation.setOtherEntityAddress(parameterTable.getObservation());
			billingCalculation.setOtherEntityCountry(parameterTable.getIndicator4());
			billingCalculation.setOtherEntityDepartment(parameterTable.getIndicator4());
			billingCalculation.setOtherEntityProvince(parameterTable.getIndicator5());
			billingCalculation.setOtherEntityDistrict(parameterTable.getIndicator6());
			
			
		}else if(billingCalculation.getEntityCollection().equals(EntityCollectionType.REGULATOR_ENTITY.getCode())){
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setParameterTablePk(billingCalculation.getEntityCollection());
			List<ParameterTable>  list	= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			ParameterTable parameterTable	= list.get(0);
			
			billingCalculation.setOtherEntityAddress(parameterTable.getObservation());
			billingCalculation.setOtherEntityCountry(parameterTable.getIndicator4());
			billingCalculation.setOtherEntityDepartment(parameterTable.getIndicator4());
			billingCalculation.setOtherEntityProvince(parameterTable.getIndicator5());
			billingCalculation.setOtherEntityDistrict(parameterTable.getIndicator6());
			
		}
		
		
	}
	
	
	/**
	 * Only Modify .
	 *
	 * @param listCollectionRecord the list collection record
	 */
	public void updateCollectionRecordByBillingCalculation(List<CollectionRecord> listCollectionRecord){
		
		List<CollectionRecord> listCollectionRecordForModify=new ArrayList<>();
		CollectionRecord collectionRecord=null;
		for (CollectionRecord collectionRecordModify : listCollectionRecord) {
			collectionRecord=businessLayerCommonFacade.findEntityByCode(CollectionRecord.class, collectionRecordModify.getIdCollectionRecordPk());
			collectionRecord.setKeyEntityCollection(collectionRecordModify.getKeyEntityCollection());
			listCollectionRecordForModify.add(collectionRecord);
		}
		
		
	}
}
