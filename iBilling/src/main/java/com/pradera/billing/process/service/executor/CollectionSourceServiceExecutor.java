package com.pradera.billing.process.service.executor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.billing.billingcollection.facade.BaseCollectionMgmtServiceFacade;
import com.pradera.billing.billingcollection.facade.CollectionServiceMgmtServiceFacade;
import com.pradera.billing.billingcollection.to.CollectionRecordDetailTo;
import com.pradera.billing.billingcollection.to.CollectionRecordTo;
import com.pradera.billing.billingcollection.to.DailyExchangeRateFilter;
import com.pradera.billing.billingcollection.to.ProcessedServiceTo;
import com.pradera.billing.billingrate.facade.BillingRateMgmtServiceFacade;
import com.pradera.billing.billingservice.facade.BillingServiceMgmtServiceFacade;
import com.pradera.billing.billingsource.facade.SourceBillingServiceFacade;
import com.pradera.billing.billingsource.to.BillingServiceSourceInfo;
import com.pradera.billing.billingsource.to.BillingServiceSourceLoggerTo;
import com.pradera.billing.common.facade.BusinessLayerCommonFacade;
import com.pradera.billing.common.utils.BillingUtils;
import com.pradera.billing.process.service.CollectionSourceService;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.entity.producer.DepositaryDataBase;
import com.pradera.core.framework.entity.service.LoaderEntityServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.CollectionRecord;
import com.pradera.model.billing.CollectionRecordDetail;
import com.pradera.model.billing.ProcessedService;
import com.pradera.model.billing.RateMovement;
import com.pradera.model.billing.ServiceRate;
import com.pradera.model.billing.ServiceRateScale;
import com.pradera.model.billing.type.BaseCollectionType;
import com.pradera.model.billing.type.EntityCollectionType;
import com.pradera.model.billing.type.ProcessedServiceType;
import com.pradera.model.billing.type.RateStateType;
import com.pradera.model.billing.type.RateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.DailyExchangeRoleType;
import com.pradera.model.generalparameter.type.InformationSourceType;
import com.pradera.model.issuancesecuritie.Issuer;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CollectionSourceServiceExecutor.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Singleton
public class CollectionSourceServiceExecutor implements CollectionSourceService{
	

	/** The log. */
	@Inject
	PraderaLogger log;
	 
	/** The load entity service. */
	@EJB
	LoaderEntityServiceBean loadEntityService;
	 	
	/** The em. */
	@Inject @DepositaryDataBase
	private EntityManager em;
	 
	/** The collection service mgmt service facade. */
	@EJB 
	CollectionServiceMgmtServiceFacade collectionServiceMgmtServiceFacade;
	 
	/** The base collection mgmt service facade. */
	@EJB
	BaseCollectionMgmtServiceFacade baseCollectionMgmtServiceFacade;
	
	/** The billing service mgmt service facade. */
	@EJB
	BillingServiceMgmtServiceFacade billingServiceMgmtServiceFacade;
	
	/** The billing rate mgmt service facade. */
	@EJB
	BillingRateMgmtServiceFacade billingRateMgmtServiceFacade;
	
	/** The source billing service facade. */
	@EJB
	SourceBillingServiceFacade sourceBillingServiceFacade;
	
	//private static final long TIME	= 1;
	
	/** The business layer common facade. */
	@EJB
	BusinessLayerCommonFacade businessLayerCommonFacade;
	 
	
	
	
	/* (non-Javadoc)
	 * @see com.pradera.billing.process.service.CollectionSourceService#startCalculateCollectionTransaction(com.pradera.billing.billingcollection.to.ProcessedServiceTo)
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	@Performance
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=60)
	@Override
	public void startCalculateCollectionTransaction(ProcessedServiceTo processedServiceTo) throws ServiceException  {
			
		BillingServiceSourceInfo	billingServiceSourceInfo=new BillingServiceSourceInfo();
		List<BillingServiceSourceLoggerTo> listBillingServiceSourceLoggerTo=null;
		
		if (processedServiceTo.getErrorType()!=null){
			throw new ServiceException(processedServiceTo.getErrorType());
		}
		else if(processedServiceTo.getOtherErrorType()){
			throw new ServiceException();
		}
		
		ProcessedService processedService = null;
		
 		collectionServiceMgmtServiceFacade.verifyProcessedServiceUniqueByPeriod(processedServiceTo);							
 		collectionServiceMgmtServiceFacade.verifyRateActives(processedServiceTo);

		processedService = collectionServiceMgmtServiceFacade.verifyCollectionUnbilled(processedServiceTo);
		
		loadEntityService.detachmentEntity(processedService);
		

		/**::::::::::::::::::::::::::::: 1 --  getting billing service ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
		BillingService billingService = processedService.getBillingService();
		  
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put(GeneralConstants.BILLING_SERVICE, billingService);	
		parameters.put(GeneralConstants.OPERATION_DATE,processedService.getOperationDate());		
		parameters.put(GeneralConstants.CALCULATION_DATE,processedService.getCalculationDate());
		
		

		List<ServiceRate> listServiceRates = billingService.getServiceRates();
		
		/**this list will be filled */
		List<CollectionRecord> collectionRecordList	=	processedService.getCollectionRecords(); 
		List<CollectionRecordTo> collectionRecordToList=null;
		
		/**
		 * Aqui voy a poner la carga del archivo XMl en una clase
		 */
		try {
			log.debug("::::::::::::::::::::::::: INICIO DE GENERACION DE COBRO MEDIANTE BASE DE COBRO EXTERNA ::::::::::::::::::");
			
			log.debug("::::::::::::::::::::LECTURA DEL ARCHIVO XML::::::::::::::::::::");
			billingServiceSourceInfo=sourceBillingServiceFacade.loadAccountingSourceInfoById(processedServiceTo.getBillingServiceTo().getIdSourcePk());
			listBillingServiceSourceLoggerTo=sourceBillingServiceFacade.fillBillingServiceSourceLogger(parameters,billingServiceSourceInfo);
			collectionRecordToList=sourceBillingServiceFacade.processForBillingSource(parameters,listBillingServiceSourceLoggerTo);
		} catch (ServiceException e) {
			log.error("::::::::::::::::::::::ERROR EN GENERACION DE COBRO MEDIANTE BASE DE COBRO EXTERNA:::::::::::::::::::::::::::");
			e.printStackTrace();
		}
		
		/***
		 * 
		 * Recordar el indicador de renta fija o renta variable EN SHORT_INTEGER
		 */

			
			if (Validations.validateListIsNotNullAndNotEmpty(listServiceRates)){
				
				collectionRecordList = new ArrayList<CollectionRecord>();
				
				for (ServiceRate serviceRate : listServiceRates) {
					
													
						if (serviceRate.getRateState().equals(RateStateType.ACTIVED.getCode())){
							
							
						    /** ::::::::::::::::::::::::::::: 2 -- getting movement type only billed ::::::::::::::::::::::::::::::::::::::::::::::::*/
						
						
							
									
						    parameters.put(GeneralConstants.SERVICE_RATE, serviceRate);

							/** :::::::::::::::::::::::::::::: 3    GETTING DATA BY BASE COLLECTION ::::::::::::::::::::::::::::::::::::::::::::::: */
					
							if (serviceRate.getRateType().equals(RateType.PORCENTUAL.getCode())){
						    	
								collectionRecordToList	= gettingDataForCalculatingPercentage(parameters);		
								
						    }
								
							if (serviceRate.getRateType().equals(RateType.FIJO.getCode())){
						    	
								collectionRecordToList	= gettingDataForCalculatingFixed(parameters);		
								
						    }
							
							/**
							 * Only Staggered
							 */
							if (serviceRate.getRateType().equals(RateType.ESCALONADO_FIJO.getCode())){
								/****
								 * VERIFY IND ACCUMULATIVE
								 */
								/** ::::::::::::::::::::::::::::: 2 -- getting movement type only billed ::::::::::::::::::::::::::::::::::::::::::::::::*/
							    List<ServiceRateScale> serviceRateScaleList = billingRateMgmtServiceFacade.findServiceRateScaleByServiceRate(serviceRate.getIdServiceRatePk());
							
							    
								if (Validations.validateListIsNotNullAndNotEmpty(serviceRateScaleList)){
									
									serviceRate.setServiceRateScales(serviceRateScaleList);						
									collectionRecordToList	= gettingDataForCalculatingEstaggeredFixed(parameters);		
									
							    }
							
						    /** :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
							
							}
							
							if (serviceRate.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode())){
								
								/****
								 * VERIFY IND ACCUMULATIVE
								 */
								/** ::::::::::::::::::::::::::::: 2 -- getting movement type only billed ::::::::::::::::::::::::::::::::::::::::::::::::*/
							    List<ServiceRateScale> serviceRateScaleList = billingRateMgmtServiceFacade.findServiceRateScaleByServiceRate(serviceRate.getIdServiceRatePk());
							
								if (Validations.validateListIsNotNullAndNotEmpty(serviceRateScaleList)){
									
									serviceRate.setServiceRateScales(serviceRateScaleList);						
									collectionRecordToList	= gettingDataForCalculatingEstaggeredFixed(parameters);		
									
							    }
							
						    /** :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
							
							}
							/**
							 * TARIFA ESCALONADA MIXTA
							 */
							
							if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordToList)){
							
						    log.info(" NUMBER OF REGISTER FOR CALCULATE  : "+collectionRecordToList.size()+"");
								
							parameters.put(GeneralConstants.COLLECTION_RECORD_TO_LIST,collectionRecordToList );
							
				            /** :::::::::::::::::::::::::::::: 4°   ADDING NEW AMOUNT BY EACH  MOVEMENT RATE ::::::::::::::::::::::::::::::::::::::::::::::: */					
							
							
							processForServiceRate(parameters, collectionRecordList);

							/** :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
							
							
							/** after than  add mount for calculation , setting memory to null */
							parameters.remove(GeneralConstants.COLLECTION_RECORD_TO_LIST);
							collectionRecordToList=null;// setting values
							
							}else
							{
								log.info(" NUMBER OF REGISTER FOR CALCULATE  : 0 ");
							}
							
							
							if (parameters.containsKey(GeneralConstants.SERVICE_RATE)){
								parameters.remove(GeneralConstants.SERVICE_RATE);
							}
																		
						}// end each service rate	
					
					}

			
				if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordList)){
					
					
					for (CollectionRecord collectionRecord : collectionRecordList) {
						collectionRecord.setProcessedService(processedService);
					}
					processedService.setCollectionRecords(collectionRecordList);
					
	
	
					/** :::::::::::::::  FILTRAR ENTITIES BILLED  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::**/
							
					collectionServiceMgmtServiceFacade.filterEntitiesBilled(processedService);
					
					/** :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */			
		
					if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordList) ){
	
						/** persist calculation on BD */
						collectionServiceMgmtServiceFacade.saveProcessedServiceByBatch(processedService);								
					
					}
					
	
				}
				
				ProcessedService processedServiceFinish=
						collectionServiceMgmtServiceFacade.getProcessedServiceByCode(processedService.getIdProcessedServicePk());
						processedServiceFinish.setOperationDate(CommonsUtilities.currentDate());
						processedServiceFinish.setProcessedState(ProcessedServiceType.PROCESADO.getCode());
				log.info(" ::::::::::::::::::::::::::::::::  FIN   Proceso de Transaccion de Tarifas Porcentuales ::::::::::::::::::::::::::::::::");
				log.info(" ::::::::::::::::::  Codigo de Servicio =  " +billingService.getServiceCode()+" ::::::::::::::::::::::::::");	
				
				em.flush();
		
				//log.info(" :::::::::::::::::::::::::::::::: ERROR AL ACTUALIZAR  EL SERVICIO PROCESADO  = " +billingService.getServiceCode()+"  :::");
			
			
				if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordList)){	
					log.info(" ::::::::::::::::::::::::::::::::  REGISTRO COBROS" +
							" EN EL SERVICIO = " +collectionRecordList.size()+"  :::");
				}else{
					log.info(" ::::::::::::::::::::::::::::::::  NO REGISTRO COBROS EN EL SERVICIO = " +billingService.getServiceCode()+"  :::");
				}
				
				
			}
				
	}
	
	
	
	/* (non-Javadoc)
	 * @see com.pradera.billing.process.service.CollectionSourceService#processForServiceRate(java.util.Map, java.util.List)
	 */
	@Override
	public void processForServiceRate(Map<String, Object> parameters,
			List<CollectionRecord> collectionRecordList) throws ServiceException {
		

		BillingService 	billingService 	= (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);					
		ServiceRate 	serviceRate 	= (ServiceRate) parameters.get(GeneralConstants.SERVICE_RATE);
		Date 			calculateDate	= (Date) parameters.get(GeneralConstants.CALCULATION_DATE);
		Date 			operationDate	= (Date) parameters.get(GeneralConstants.OPERATION_DATE);
		
		
		
		/** Getting currency change factor***/
		 
		BigDecimal amountDetail   = BigDecimal.ZERO;
		BigDecimal referencePrice = BigDecimal.ZERO;		
		DailyExchangeRateFilter dailyExchangeRateFilter = new DailyExchangeRateFilter();			
		dailyExchangeRateFilter.setIdSourceinformation(InformationSourceType.BCRD.getCode());	
		dailyExchangeRateFilter.setInitialDate((calculateDate==null?null:CommonsUtilities.truncateDateTime(calculateDate) ));
		dailyExchangeRateFilter.setFinalDate((calculateDate==null?null:CommonsUtilities.truncateDateTime(calculateDate) ));
		
		
		if ( !billingService.getCurrencyBilling().equals(CurrencyType.PYG.getCode())) {
			
			dailyExchangeRateFilter.setIdCurrency(billingService.getCurrencyBilling());
			referencePrice = businessLayerCommonFacade.gettingReferencePrice(dailyExchangeRateFilter);

		}else{
		 referencePrice =new BigDecimal(1) ; /** FACTOR 1**/
		}
		
		
		/*** GETTING NEW LIST COLLECTION RECORD WITH NEW AMOUNT FOR RATE , IF THERE IS AN ENTITY ON LIST COLLECTION RECORD  ONLY ADDS BY AMOUNT RATE,  
		     IF NOT CREATE A NEW COLLECTION RECORD FOR A ENTITY COLLECTION AND AFTER ADD A  LIST OF COLLECTION RECORDS  **/			
		List<CollectionRecordTo> collectionRecordToList = (List<CollectionRecordTo>) parameters.get(GeneralConstants.COLLECTION_RECORD_TO_LIST);
		
		
		for (CollectionRecordTo collectionRecordTo : collectionRecordToList) {
		
			List<CollectionRecordDetail> newCollectionRecordDetailList	=  loadCollectionRecordDetail( collectionRecordTo.getCollectionRecordDetailToList(), serviceRate,parameters);
			
			/**getting entity for add news amount **/
			Object objEntity = collectionRecordTo.getEntityCollection(collectionRecordTo.getEntityCollection());	
			CollectionRecord collectionRecord=findEntityAtList(collectionRecordList, objEntity, collectionRecordTo.getEntityCollection());
			
			/** THE GOAL IS  CREATE A RECORD FOR EACH ENTITY ONLY IF DON´T EXIST**/
			if (collectionRecord==null){
				collectionRecord = new CollectionRecord();
				collectionRecord.setEntityCollectionId(billingService.getEntityCollection());				
				collectionRecord.setGrossAmount(new BigDecimal(0));
				collectionRecord.setCollectionAmount(new BigDecimal(0));
				collectionRecord.setCalculationDate(calculateDate);				
				collectionRecord.setOperationDate(operationDate);	
				collectionRecord.setCurrencyType(billingService.getCurrencyCalculation());
				collectionRecord.setCurrencyBilling(billingService.getCurrencyBilling());
				collectionRecord.setRecordState(ProcessedServiceType.PROCESADO.getCode());
				collectionRecord.setIndBilled(PropertiesConstants.NOT_BILLED);/** default , not  billed**/ 
				addEntityCollection(collectionRecord, collectionRecordTo); /** CAN BE : HOLDER , PARTICIPANT OR ISSUER **/
				
				
				/**  aggregate new collection record to collectionRecordList*/
				collectionRecordList.add(collectionRecord);
			}
			
			
			
			/** ADD ENTITI'S AMOUNT OF EACH DETAIL  **/
			if (Validations.validateListIsNullOrEmpty(collectionRecord.getCollectionRecordDetail())){
				collectionRecord.setCollectionRecordDetail(new ArrayList<CollectionRecordDetail>());
			}
								
			
			/** ADD TO COLLECTION RECORD**/			
			for (CollectionRecordDetail collectionRecordDetail: newCollectionRecordDetailList) {	
				
				collectionRecord.getCollectionRecordDetail().add(collectionRecordDetail);
				BigDecimal tempAmount = BigDecimal.ZERO;
				
				if (serviceRate.getRateType().equals(RateType.PORCENTUAL.getCode())){
					 
					 /** CONVERT TO CURRENCY OF BILLING SERVICE**/
					 amountDetail	= BillingUtils.divideTwoDecimal( collectionRecordDetail.getRatePercent() , referencePrice , GeneralConstants.ROUNDING_DIGIT_NUMBER);					 
					 collectionRecordDetail.setRateAmount(amountDetail);
					 
					 tempAmount		= BillingUtils.addTwoDecimal(collectionRecord.getCollectionAmount(), amountDetail, GeneralConstants.ROUNDING_DIGIT_NUMBER); 
					 
				}else if ( (serviceRate.getRateType().equals(RateType.FIJO.getCode())) ||
					      	(serviceRate.getRateType().equals(RateType.ESCALONADO_FIJO.getCode())) ){		
					
					 tempAmount		= BillingUtils.addTwoDecimal(collectionRecord.getCollectionAmount(), collectionRecordDetail.getRateAmount(), GeneralConstants.ROUNDING_DIGIT_NUMBER);	
					 
				}
								
				collectionRecord.setCollectionAmount(tempAmount);
			}
			
			
			/*** APPLLIED ACCORDING TO EXCHANGE CURRENCY SERVICE **/				
			/*convertAmount = BillingUtils.multiplyDecimals( referencePrice, collectionRecord.getCollectionAmount(), 3);	
			collectionRecord.setCollectionAmount(convertAmount);*/	
					

			/** ONLY IF SERVICE APPLY   TAXED **/
			if (billingService.getTaxApplied().equals(PropertiesConstants.YES_APPLY_TAXED)){
				
				ParameterTable parameterTableTax= billingServiceMgmtServiceFacade.getParameterTableById(PropertiesConstants.TAX_PK);				
				Double taxed=(parameterTableTax!=null)?parameterTableTax.getDecimalAmount1():new Double(GeneralConstants.ZERO_VALUE_STRING);
				
				BigDecimal taxedAmount=BillingUtils.multiplyDecimals(collectionRecord.getCollectionAmount(), new BigDecimal(taxed.toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
				collectionRecord.setTaxApplied(taxedAmount);
						
				BigDecimal grossAmount = BillingUtils.addTwoDecimal(collectionRecord.getCollectionAmount(), taxedAmount, GeneralConstants.ROUNDING_DIGIT_NUMBER);
				
				/** SETTING FINALLY COLLECTION AMOUNT **/
				collectionRecord.setGrossAmount(grossAmount);
				
			}else{
				collectionRecord.setTaxApplied(new BigDecimal(GeneralConstants.ZERO_VALUE_STRING));
				collectionRecord.setGrossAmount(collectionRecord.getCollectionAmount());
			}
						
			
		}
		
	}
	
	/**
	 * Adds the entity collection.
	 *
	 * @param collectionRecord the collection record
	 * @param collectionRecordTo the collection record to
	 */
	protected void addEntityCollection(CollectionRecord collectionRecord,CollectionRecordTo collectionRecordTo){
		
		Integer typeEntity = collectionRecordTo.getEntityCollection();
		if(typeEntity.equals(EntityCollectionType.HOLDERS.getCode())){
			Holder holder=new Holder();
			holder.setIdHolderPk((Long) collectionRecordTo.getEntityCollection(typeEntity));	
			collectionRecord.setHolder(holder);
		}else if (typeEntity.equals(EntityCollectionType.ISSUERS.getCode())){
			Issuer issuer= new Issuer();
			issuer.setIdIssuerPk((String) collectionRecordTo.getEntityCollection(typeEntity));
			collectionRecord.setIssuer(issuer);
		}else if(typeEntity.equals(EntityCollectionType.PARTICIPANTS.getCode())){
			Participant participant= new Participant();
			participant.setIdParticipantPk((Long) collectionRecordTo.getEntityCollection(typeEntity));
			collectionRecord.setParticipant(participant);
		}else if(typeEntity.equals(EntityCollectionType.AFP.getCode())){
			Participant participant= new Participant();
			participant.setIdParticipantPk((Long) collectionRecordTo.getEntityCollection(typeEntity));
			collectionRecord.setParticipant(participant);
		}else if(typeEntity.equals(EntityCollectionType.CENTRAL_BANK.getCode())){
			collectionRecord.setOtherEntityDocument(EntityCollectionType.CENTRAL_BANK.getCode().toString());
			collectionRecord.setOtherEntityDescription(EntityCollectionType.CENTRAL_BANK.getValue());
		}else if(typeEntity.equals(EntityCollectionType.HACIENDA.getCode())){
			collectionRecord.setOtherEntityDocument(EntityCollectionType.HACIENDA.getCode().toString());
			collectionRecord.setOtherEntityDescription(EntityCollectionType.HACIENDA.getValue());
		}else if(typeEntity.equals(EntityCollectionType.STOCK_EXCHANGE.getCode())){
			collectionRecord.setOtherEntityDocument(EntityCollectionType.STOCK_EXCHANGE.getCode().toString());
			collectionRecord.setOtherEntityDescription(EntityCollectionType.STOCK_EXCHANGE.getValue());
		}else if(typeEntity.equals(EntityCollectionType.OTHERS.getCode())){
			collectionRecord.setOtherEntityDocument(null);
			collectionRecord.setOtherEntityDescription(null);
		}
		
				
	}
	
	/**
	 * Find entity at list.
	 *
	 * @param collectionRecordList the collection record list
	 * @param entity the entity
	 * @param typeEntity the type entity
	 * @return the collection record
	 */
	protected  CollectionRecord findEntityAtList(List<CollectionRecord> collectionRecordList,Object entity,Integer typeEntity){
		
		if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordList)){
				
			for (CollectionRecord collectionRecord : collectionRecordList) {
				
				Boolean exist=false;
				if(typeEntity.equals(EntityCollectionType.HOLDERS.getCode())){
					exist= collectionRecord.getHolder().getIdHolderPk().equals((Long)entity);
				}else if (typeEntity.equals(EntityCollectionType.ISSUERS.getCode())){
					exist= collectionRecord.getIssuer().getIdIssuerPk().equals((String)entity);
				}else if(typeEntity.equals(EntityCollectionType.PARTICIPANTS.getCode())){
					exist= collectionRecord.getParticipant().getIdParticipantPk().equals((Long)entity);
				}
				
				if (exist){
					return collectionRecord;
				}
			}
		}
		
		return null;
	}
	
	/**
	 * Find entity at list temporal to.
	 *
	 * @param collectionRecordToList the collection record to list
	 * @param entity the entity
	 * @param typeEntity the type entity
	 * @return the collection record to
	 */
	protected  CollectionRecordTo findEntityAtListTemporalTo(List<CollectionRecordTo> collectionRecordToList,Object entity,Integer typeEntity){
		
		if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordToList)){
				
			for (CollectionRecordTo collectionRecordTo : collectionRecordToList) {
				
				if (collectionRecordTo.existEntityCollection(entity, typeEntity)){
					return collectionRecordTo;
				}
			}
		}
		
	 return null;
	
   }
	
	
	/**
	 * Load collection record detail.
	 *
	 * @param collectionRecordDetailToList the collection record detail to list
	 * @param serviceRate the service rate
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	protected List<CollectionRecordDetail>  loadCollectionRecordDetail(List<CollectionRecordDetailTo> collectionRecordDetailToList,
			 						ServiceRate serviceRate, Map<String, Object> parameters) throws ServiceException{
		
		List<CollectionRecordDetail> newCollectionRecordDetailList  = new ArrayList<CollectionRecordDetail>();		
		CollectionRecordDetail newCollectionRecordDetail = new CollectionRecordDetail();
				
		if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordDetailToList)){
		
			for (CollectionRecordDetailTo collectionRecordDetailTo : collectionRecordDetailToList) {
			
				BigDecimal buyPrice= null;
				/** getting RateMovement*/
				RateMovement rateMovement = null;
				if(Validations.validateIsNotNullAndPositive(collectionRecordDetailTo.getIdMovementTypePk())){
					rateMovement = serviceRate.getRateMovement(collectionRecordDetailTo.getIdMovementTypePk());
					newCollectionRecordDetail.setIdMovementTypePk(collectionRecordDetailTo.getIdMovementTypePk());	
				}
				newCollectionRecordDetail = new CollectionRecordDetail();														
				newCollectionRecordDetail.setServiceRate(serviceRate);
				
				Date movementDate 					= collectionRecordDetailTo.getMovementDate();
				Date operationDate					= collectionRecordDetailTo.getOperationDate();
				Long operationNumber 				= collectionRecordDetailTo.getOperationNumber();
				Long idSourceParticipant 			= collectionRecordDetailTo.getIdSourceParticipant();
				Long idTargetParticipant 			= collectionRecordDetailTo.getIdTargetParticipant();
				BigDecimal operationPrice 			= collectionRecordDetailTo.getOperationPrice();
				Long holderAccount 	  				= collectionRecordDetailTo.getHolderAccount();
				Long idHolderPk 		  			= collectionRecordDetailTo.getIdHolderPk();
				Long movementQuantity				= collectionRecordDetailTo.getMovementQuantity();
				Integer securityClass				= collectionRecordDetailTo.getSecurityClass();
				Integer modality					= collectionRecordDetailTo.getModality();
				String ballotSequence				= collectionRecordDetailTo.getBallotSequential();
				BigDecimal negotiatedAmountOrigin 	= collectionRecordDetailTo.getNegotiatedAmountOrigin();
				BigDecimal negotiatedAmount 		= collectionRecordDetailTo.getNegotiatedAmount();
				newCollectionRecordDetail.setServiceRate(serviceRate);
				
				if (serviceRate.getRateType().equals(RateType.PORCENTUAL.getCode())){
					
					BigDecimal percentAmount 		=  BigDecimal.ZERO;
					BigDecimal grossAmount			=  collectionRecordDetailTo.getPercentage();/** getting amount for apply factor**/
					BigDecimal ratePercent			=  serviceRate.getRatePercent();
					
					/** Applied Percentage on Amount Gross**/
					percentAmount= BillingUtils.multiplyDecimals(grossAmount, ratePercent, GeneralConstants.ROUNDING_DIGIT_NUMBER);
					/** Applied Billing Currency */
					businessLayerCommonFacade.convertCalculationToBilled(percentAmount, 
							serviceRate.getCurrencyRate(),newCollectionRecordDetail,
							parameters, DailyExchangeRoleType.SELL.getCode());
					newCollectionRecordDetail.setRatePercent(percentAmount);
					newCollectionRecordDetail.setCalculationDate(collectionRecordDetailTo.getCalculationDate());
				
				}else if (serviceRate.getRateType().equals(RateType.FIJO.getCode())){
					
					BigDecimal rateAmount			=  serviceRate.getRateAmount();
					BigDecimal temporalAmount		=  BillingUtils.addTwoDecimal(rateAmount, newCollectionRecordDetail.getRateAmount(), GeneralConstants.ROUNDING_DIGIT_NUMBER);
					
					/** Applied Billing Currency */
					businessLayerCommonFacade.convertCalculationToBilled(temporalAmount, 
							serviceRate.getCurrencyRate(), newCollectionRecordDetail,
							parameters, DailyExchangeRoleType.SELL.getCode());
					
					newCollectionRecordDetail.setRateAmount(temporalAmount);
					
				}else if (serviceRate.getRateType().equals(RateType.ESCALONADO_FIJO.getCode())){
					BigDecimal temporalAmount		=  BillingUtils.addTwoDecimal(collectionRecordDetailTo.getAmount(), newCollectionRecordDetail.getRateAmount(), GeneralConstants.ROUNDING_DIGIT_NUMBER);
					
					/** Applied Billing Currency */
					businessLayerCommonFacade.convertCalculationToBilled(temporalAmount, 
							serviceRate.getCurrencyRate(),newCollectionRecordDetail,
							parameters, DailyExchangeRoleType.SELL.getCode());
					newCollectionRecordDetail.setRateAmount(temporalAmount);
					
				}else if (serviceRate.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode())){
					BigDecimal temporalAmount		=  BillingUtils.addTwoDecimal(collectionRecordDetailTo.getAmount(), newCollectionRecordDetail.getRateAmount(), GeneralConstants.ROUNDING_DIGIT_NUMBER);
					
					/** Applied Billing Currency */
					businessLayerCommonFacade.convertCalculationToBilled(temporalAmount, 
							serviceRate.getCurrencyRate(), newCollectionRecordDetail,
							parameters, DailyExchangeRoleType.SELL.getCode());
					newCollectionRecordDetail.setRateAmount(temporalAmount);
					
				}
				else if (serviceRate.getRateType().equals(RateType.ESCALONADO_MIXTO.getCode())){
					BigDecimal temporalAmount		=  BillingUtils.addTwoDecimal(collectionRecordDetailTo.getAmount(), newCollectionRecordDetail.getRateAmount(), GeneralConstants.ROUNDING_DIGIT_NUMBER);
					
					/** Applied Billing Currency */
					businessLayerCommonFacade.convertCalculationToBilled(temporalAmount, 
							serviceRate.getCurrencyRate(), newCollectionRecordDetail,
							parameters, DailyExchangeRoleType.SELL.getCode());
					newCollectionRecordDetail.setRateAmount(temporalAmount);
					
				}else if (serviceRate.getRateType().equals(RateType.TIPO_MOVIMIENTO_PORCENTUAL.getCode())){
			    	
					
					/** 1° calculate for number movements**/
					BigDecimal grossAmount 		       			=  new BigDecimal(0.0);
					BigDecimal percentAmount 		       		=  new BigDecimal(0.0);
					BigDecimal ratePercent						=  rateMovement.getRatePercent();
					/** calculate with amount */
					
					/***Este PriceByQuantity cambiar a la moneda de Calculo*/
					buyPrice=businessLayerCommonFacade.processExchange(parameters,
								collectionRecordDetailTo.getCurrencyRate(),
								serviceRate.getCurrencyRate(),DailyExchangeRoleType.BUY.getCode());
					
					/***ALL EXCHANGE RATE  -----> CONVERSION TO CALCULATION CURRENCY  */
					
					grossAmount= BillingUtils.multiplyDecimalLong(operationPrice, movementQuantity, GeneralConstants.ROUNDING_DIGIT_NUMBER);
					grossAmount =BillingUtils.multiplyDecimals(buyPrice,grossAmount , GeneralConstants.ROUNDING_DIGIT_NUMBER);
					
					
					/** Applied Percentage on Amount Gross**/
					percentAmount= BillingUtils.multiplyDecimals(grossAmount,ratePercent , GeneralConstants.ROUNDING_DIGIT_NUMBER);
					
					if(BillingUtils.isBaseSettlementTrading(serviceRate.getBillingService().getBaseCollection())){
						percentAmount=businessLayerCommonFacade.appliedMinimumAmountCalculation(serviceRate, 
								percentAmount, parameters);
					}
					/** Applied Billing Currency */
					businessLayerCommonFacade.convertCalculationToBilled(percentAmount, 
							serviceRate.getCurrencyRate(),newCollectionRecordDetail,
							parameters, DailyExchangeRoleType.SELL.getCode());
					
					newCollectionRecordDetail.setRatePercent(percentAmount);
					newCollectionRecordDetail.setCurrencyRate(collectionRecordDetailTo.getCurrencyRate());
					newCollectionRecordDetail.setMovementQuantity(movementQuantity);
					newCollectionRecordDetail.setCalculationDate(collectionRecordDetailTo.getCalculationDate());
					newCollectionRecordDetail.setMovementDate(movementDate);
					newCollectionRecordDetail.setOperationDate(operationDate);
					newCollectionRecordDetail.setOperationPrice(operationPrice);
					newCollectionRecordDetail.setOperationNumber(operationNumber);
					newCollectionRecordDetail.setIdSourceParticipant(idSourceParticipant);
					newCollectionRecordDetail.setIdTargetParticipant(idTargetParticipant);
					newCollectionRecordDetail.setHolderAccount(holderAccount);
					newCollectionRecordDetail.setIdHolder(idHolderPk);
					newCollectionRecordDetail.setSecurityClass(securityClass);
					newCollectionRecordDetail.setModality(modality);
					newCollectionRecordDetail.setBallotSequential(ballotSequence);
					newCollectionRecordDetail.setNegotiatedAmount(negotiatedAmount);
					newCollectionRecordDetail.setNegotiatedAmountOrigin(negotiatedAmountOrigin);
					
			    }else if (serviceRate.getRateType().equals(RateType.TIPO_MOVIMIENTO_FIJO.getCode())){
			    															
					/** 1° calculate for number movements**/
					BigDecimal amount 			= new BigDecimal(0.0);
					Integer numMovement 		= collectionRecordDetailTo.getMovementCount();
					BigDecimal rateAmount		= rateMovement.getRateAmount();
					
					/** calculate with amount */
					amount= BillingUtils.multiplyDecimalInteger(rateAmount, numMovement, GeneralConstants.ROUNDING_DIGIT_NUMBER);
					
					/** Applied Billing Currency */
					businessLayerCommonFacade.convertCalculationToBilled(amount, 
							serviceRate.getCurrencyRate(),newCollectionRecordDetail,
							parameters, DailyExchangeRoleType.SELL.getCode());
					
					newCollectionRecordDetail.setRateAmount(amount);
					newCollectionRecordDetail.setMovementCount(numMovement);
					newCollectionRecordDetail.setMovementDate(movementDate);
					newCollectionRecordDetail.setOperationDate(operationDate);
					newCollectionRecordDetail.setOperationNumber(operationNumber);
					newCollectionRecordDetail.setIdSourceParticipant(idSourceParticipant);
					newCollectionRecordDetail.setIdTargetParticipant(idTargetParticipant);
					newCollectionRecordDetail.setOperationPrice(operationPrice);
					newCollectionRecordDetail.setHolderAccount(holderAccount); 
					newCollectionRecordDetail.setIdHolder(idHolderPk);  
					newCollectionRecordDetail.setMovementQuantity(movementQuantity);
					newCollectionRecordDetail.setCurrencyRate(serviceRate.getCurrencyRate());
					newCollectionRecordDetail.setSecurityClass(securityClass);
					newCollectionRecordDetail.setModality(modality);
					newCollectionRecordDetail.setBallotSequential(ballotSequence);
					newCollectionRecordDetail.setNegotiatedAmount(negotiatedAmount);
					newCollectionRecordDetail.setNegotiatedAmountOrigin(negotiatedAmountOrigin);
					
			    }
			    else if ( (serviceRate.getRateType().equals(RateType.ESCALONADO_FIJO.getCode()) ||
			    		serviceRate.getRateType().equals(RateType.ESCALONADO_MIXTO.getCode()))    &&
			    		serviceRate.getIndScaleMovement().equals(PropertiesConstants.YES_VALUE_INTEGER)
			    		){
			    		/**Aplicar la logica de escalones con movimientos*/
			    	

				    	/** 1° calculate for number movements**/
						BigDecimal amount 			= new BigDecimal(0.0);
						Integer numMovement 		= collectionRecordDetailTo.getMovementCount();
						
						/** calculate with amount */
						amount= collectionRecordDetailTo.getAmount();
						/** Applied Billing Currency */
						businessLayerCommonFacade.convertCalculationToBilled(amount, serviceRate.getCurrencyRate(),
											newCollectionRecordDetail,
											parameters, DailyExchangeRoleType.SELL.getCode());
						
						newCollectionRecordDetail.setRateAmount(amount);
						newCollectionRecordDetail.setMovementCount(numMovement);
						newCollectionRecordDetail.setMovementDate(movementDate);
						newCollectionRecordDetail.setOperationDate(operationDate);
						newCollectionRecordDetail.setOperationNumber(operationNumber);
						newCollectionRecordDetail.setIdSourceParticipant(idSourceParticipant);
						newCollectionRecordDetail.setIdTargetParticipant(idTargetParticipant);
						newCollectionRecordDetail.setOperationPrice(operationPrice);
						newCollectionRecordDetail.setHolderAccount(holderAccount); 
						newCollectionRecordDetail.setIdHolder(idHolderPk);  
						newCollectionRecordDetail.setMovementQuantity(movementQuantity);
						newCollectionRecordDetail.setCurrencyRate(serviceRate.getCurrencyRate());
						newCollectionRecordDetail.setSecurityClass(securityClass);
						newCollectionRecordDetail.setModality(modality);
						newCollectionRecordDetail.setBallotSequential(ballotSequence);
						newCollectionRecordDetail.setNegotiatedAmount(negotiatedAmount);
						newCollectionRecordDetail.setNegotiatedAmountOrigin(negotiatedAmountOrigin);
			    	
			    	
			    	
			    }else if(serviceRate.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode())     &&
			    		serviceRate.getIndScaleMovement().equals(PropertiesConstants.YES_VALUE_INTEGER)){
			    	
			    	/**YES_VALUE_INTEGER =ONE; si hay movimientos
			    	 * =zero; no hay movimientos
			    	 * */
			    		/** 1° calculate for number movements**/
						BigDecimal percentAmount 		=  new BigDecimal(0.0);
						
						/** Applied Percentage on Amount Gross**/
						percentAmount= collectionRecordDetailTo.getAmount();
						
						/** Applied Billing Currency */
						businessLayerCommonFacade.convertCalculationToBilled(percentAmount, 
								serviceRate.getCurrencyRate(), newCollectionRecordDetail,
								parameters, DailyExchangeRoleType.SELL.getCode());
						
						newCollectionRecordDetail.setRatePercent(percentAmount);
						newCollectionRecordDetail.setCurrencyRate(collectionRecordDetailTo.getCurrencyRate());
						newCollectionRecordDetail.setMovementQuantity(movementQuantity);
						newCollectionRecordDetail.setCalculationDate(collectionRecordDetailTo.getCalculationDate());
						newCollectionRecordDetail.setMovementDate(movementDate);
						newCollectionRecordDetail.setOperationDate(operationDate);
						newCollectionRecordDetail.setOperationPrice(operationPrice);
						newCollectionRecordDetail.setOperationNumber(operationNumber);
						newCollectionRecordDetail.setIdSourceParticipant(idSourceParticipant);
						newCollectionRecordDetail.setIdTargetParticipant(idTargetParticipant);
						newCollectionRecordDetail.setHolderAccount(holderAccount);
						newCollectionRecordDetail.setIdHolder(idHolderPk);
						newCollectionRecordDetail.setSecurityClass(securityClass);
						newCollectionRecordDetail.setModality(modality);
						newCollectionRecordDetail.setBallotSequential(ballotSequence);
						newCollectionRecordDetail.setNegotiatedAmount(negotiatedAmount);
						newCollectionRecordDetail.setNegotiatedAmountOrigin(negotiatedAmountOrigin);
			    	
			    }
				
				/**
				 * More Detail
				 */
				if(Validations.validateIsNotNullAndNotEmpty(collectionRecordDetailTo.getIdSecurityCodeFk())){
					newCollectionRecordDetail.setIdSecurityCodeFk(collectionRecordDetailTo.getIdSecurityCodeFk());
				}
				if(Validations.validateIsNotNullAndNotEmpty(collectionRecordDetailTo.getSecurityClass())){
					newCollectionRecordDetail.setSecurityClass(collectionRecordDetailTo.getSecurityClass());
				}
				if(Validations.validateIsNotNullAndNotEmpty(collectionRecordDetailTo.getIdHolderPk())){
					newCollectionRecordDetail.setIdHolder(collectionRecordDetailTo.getIdHolderPk());
				}
				if(Validations.validateIsNotNullAndNotEmpty(collectionRecordDetailTo.getCurrencyOrigin())){
					newCollectionRecordDetail.setCurrencyOrigin(collectionRecordDetailTo.getCurrencyOrigin());
				}
				if(Validations.validateIsNotNullAndNotEmpty(negotiatedAmountOrigin)){
					newCollectionRecordDetail.setNegotiatedAmountOrigin(negotiatedAmountOrigin);
				}
				if(Validations.validateIsNotNullAndNotEmpty(negotiatedAmount)){
					newCollectionRecordDetail.setNegotiatedAmount(negotiatedAmount);
				}
				if(Validations.validateIsNotNullAndPositive(collectionRecordDetailTo.getNominalValue())){
					newCollectionRecordDetail.setNominalValue(collectionRecordDetailTo.getNominalValue());
				}
				if(Validations.validateIsNotNullAndPositive(collectionRecordDetailTo.getMovementQuantity())){
					newCollectionRecordDetail.setMovementQuantity(collectionRecordDetailTo.getMovementQuantity());
				}
				if(Validations.validateIsNotNullAndPositive(collectionRecordDetailTo.getRole())){
					newCollectionRecordDetail.setRole(collectionRecordDetailTo.getRole());
				}
				if(Validations.validateIsNotNullAndNotEmpty(collectionRecordDetailTo.getIdIssuanceCodePk())){
					newCollectionRecordDetail.setIdIssuanceCodeFk(collectionRecordDetailTo.getIdIssuanceCodePk());
				}
				if(Validations.validateIsNotNullAndNotEmpty(collectionRecordDetailTo.getOperationNumber())){
					newCollectionRecordDetail.setOperationNumber(collectionRecordDetailTo.getOperationNumber());
				}
				if(Validations.validateIsNotNullAndNotEmpty(collectionRecordDetailTo.getIdIssuerFk())){
					newCollectionRecordDetail.setIdIssuer(collectionRecordDetailTo.getIdIssuerFk());
				}
				if(Validations.validateIsNotNullAndNotEmpty(collectionRecordDetailTo.getHolderAccount())){
					newCollectionRecordDetail.setHolderAccount(collectionRecordDetailTo.getHolderAccount());
				}
												
			}
		
		newCollectionRecordDetailList.add(newCollectionRecordDetail);
		}
		return newCollectionRecordDetailList;
	}


	/* (non-Javadoc)
	 * @see com.pradera.billing.process.service.CollectionSourceService#gettingDataForCalculatingFixed(java.util.Map)
	 */
	@Override
	public List<CollectionRecordTo> gettingDataForCalculatingFixed(Map<String, Object> parameters) throws ServiceException {
		
		
		List<CollectionRecordTo> collectionRecordToList=null;	
		

		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);	
		
		/** getting data with or without exceptions*/
		if ( billingService.getBaseCollection().equals(BaseCollectionType.ACCOUNT_MAINTENANCE_ISSUER.getCode()) ||
			 billingService.getBaseCollection().equals(BaseCollectionType.DIRECT_PARTICIPANT.getCode())			 
				){
				
			collectionRecordToList = baseCollectionMgmtServiceFacade.findEntitiesForQuotaAnnual(parameters)	;
		}
		if(billingService.getBaseCollection().equals(BaseCollectionType.ACCOUNT_MODIFICATION.getCode()) ){
			collectionRecordToList = baseCollectionMgmtServiceFacade.findHolderForQuota(parameters);
		}
		if(billingService.getBaseCollection().equals(BaseCollectionType.CREATION_ISIN_CODE.getCode()) ){
			collectionRecordToList = baseCollectionMgmtServiceFacade.findSecurityForQuota(parameters);
		}
	
		return collectionRecordToList;
	}
	
	
	
/* (non-Javadoc)
 * @see com.pradera.billing.process.service.CollectionSourceService#gettingDataForCalculatingPercentage(java.util.Map)
 */
//	 
	@Override
	public List<CollectionRecordTo> gettingDataForCalculatingPercentage (Map<String, Object> parameters) throws ServiceException {
		
		List<CollectionRecordTo> collectionRecordToList=null;
	

		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);	
		Integer baseCollection=billingService.getBaseCollection();
		
		/** getting data with or without exceptions*/
		if (BaseCollectionType.CUSTODY_SECURITIES_PHYSICAL.getCode().equals(baseCollection)||
				BaseCollectionType.CUSTODY_SECURITIES_PHYSICAL_AFP.getCode().equals(baseCollection)
				){
			ParameterTable instrumentType	= billingServiceMgmtServiceFacade.getParameterTableById(baseCollection);
			Integer idInstrumentType = instrumentType.getShortInteger();
			collectionRecordToList = baseCollectionMgmtServiceFacade.findPhysicalCustodyOfSecurities(parameters,idInstrumentType);				
		}
		else if (BaseCollectionType.RECORD_NOT_PLACED_SECURITIES_FIXED_INCOME.getCode().equals(baseCollection)||
				BaseCollectionType.RECORD_NOT_PLACED_SECURITIES_VARIABLE_INCOME.getCode().equals(baseCollection)
				){
			ParameterTable instrumentType	= billingServiceMgmtServiceFacade.getParameterTableById(baseCollection);
			Integer idInstrumentType = instrumentType.getShortInteger();
			collectionRecordToList = baseCollectionMgmtServiceFacade.findBalanceSecurityNotPlaced(parameters,idInstrumentType);
		}
			
			
				
			
		return collectionRecordToList;
	}
	
	
/* (non-Javadoc)
 * @see com.pradera.billing.process.service.CollectionSourceService#gettingDataForCalculatingEstaggeredFixed(java.util.Map)
 */
//	 
	@Override
	public List<CollectionRecordTo> gettingDataForCalculatingEstaggeredFixed (Map<String, Object> parameters) throws ServiceException {
		
		List<CollectionRecordTo> collectionRecordToList=null;
	
		/**Aqui hacer la verificacion**/

		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);	
		/** getting data with or without exceptions*/
		if (billingService.getBaseCollection().equals(BaseCollectionType.TRANSFERS_SECURITIES_SIRTEX_FIXED_INCOME.getCode())){
				
			collectionRecordToList = baseCollectionMgmtServiceFacade.securitiesTransferStaggeredAccumulative(parameters);
		}
				
			
		return collectionRecordToList;
	}
	
	
		
}
