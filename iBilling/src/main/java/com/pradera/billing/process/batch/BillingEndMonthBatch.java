package com.pradera.billing.process.batch;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

import com.pradera.billing.billing.facade.BillingProcessedServiceFacade;
import com.pradera.billing.billing.to.BillingProcessedFilter;
import com.pradera.billing.common.facade.BusinessLayerCommonFacade;
import com.pradera.billing.common.utils.BillingUtils;
import com.pradera.billing.process.service.BillingCalculationService;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.billing.webservices.ServiceAPIClients;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.billing.BillingCalculationProcess;
import com.pradera.model.billing.type.BillingCalculationType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessLogger;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingEndMonthBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 08-Octubre-2015
 */
@BatchProcess(name="BillingEndMonthBatch")
@RequestScoped
public class BillingEndMonthBatch implements JobExecution,Serializable {


	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1119270984853234705L;



	/** The billing calculation executor. */
	@EJB
	BillingCalculationService  		billingCalculationExecutor;
	
	/** The billing processed service facade. */
	@EJB
	BillingProcessedServiceFacade 	billingProcessedServiceFacade;
	
	/** The business layer common facade. */
	@EJB
	BusinessLayerCommonFacade		businessLayerCommonFacade;

	/** The message configuration. */
	@Inject
	@InjectableResource(location = "Messages_es.properties")
	private Properties messageConfiguration;
	

	/** The log. */
	@Inject
	private PraderaLogger log;
	
	@EJB
	ServiceAPIClients serviceAPIClients;
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		/**
		 * 
		 * Solo se ejecutara por Automatico, no hay pantalla
		 * Proceso que factura los lotes de cobro semanales a Fin del Mes
		 * 
		 * 1.- Buscar los lotes de Calculos que tengan periodo facturacion semanal
		 * 
		 *      Los Collection Records que tengan el Indicador No Facturado
		 *      
		 * 2.- Si el Indicador de BillingSchedule indEndMonth = 1
		 * 
		 * 		Considerar todo el mes     
		 * 
		 * Generar una Factura
		 * 
		 * 
		 */


		BillingProcessedFilter billingProcessedFilter = new BillingProcessedFilter();
		List<BillingProcessedFilter> listbillingProcessedFilter = new ArrayList<BillingProcessedFilter>();

		try {

			
			Date calculationDate = CommonsUtilities.currentDate();
			Date endOfMonth=BillingUtils.getLastDayOfMonth( CommonsUtilities.currentDate() );

			if(CommonsUtilities.isEqualDate(endOfMonth, calculationDate)){
				listbillingProcessedFilter = billingProcessedServiceFacade.findFilterBillingEndMonthProcessedBatch(calculationDate);
			}

			for (Iterator iterator = listbillingProcessedFilter.iterator(); iterator.hasNext();){
				BillingProcessedFilter billingProcessedFilterSend = (BillingProcessedFilter) iterator.next();
				billingProcessedFilter=billingProcessedFilterSend;
				/*** SEND PRELIMINARY PROCESS OF BILLING **/
				billingCalculationExecutor.executePreliminaryProcessBilling(billingProcessedFilterSend);
			}

		} catch (Exception e) {

			log.error("Error in execute PreliminaryProcessBilling in Billing Preliminary Batch");
			log.error("Save BillingCalculationProcess With state Error");
			BillingCalculationProcess billingCalculationProcess = new BillingCalculationProcess();
			billingCalculationProcess.setOperationDate(CommonsUtilities.currentDateTime());
			billingCalculationProcess.setBillingDate(billingProcessedFilter.getBillingDateEnd());

			if (billingProcessedFilter.getCollectionPeriod().equals(PropertiesConstants.COLLECTION_PERIOD_MONTHLY_PK)) {

				String strDate = CommonsUtilities.convertDateToString(billingProcessedFilter.getBillingDateEnd(),BillingUtils.DATE_PATTERN_yyyy_MM_dd);
				String strYearMonth = BillingUtils.getPatternYearMonth(strDate,BillingUtils.DATE_PATTERN_yyyy_MM_dd);
				billingCalculationProcess.setBillingMonth(BillingUtils.getPatternMonth(strYearMonth,BillingUtils.DATE_PATTERN_yyyy_MM));
				billingCalculationProcess.setBillingYear(BillingUtils.getPatternYear(strYearMonth,BillingUtils.DATE_PATTERN_yyyy_MM));
			}

			billingCalculationProcess.setEntityCollection(billingProcessedFilter.getEntityCollection());
			billingCalculationProcess.setBillingPeriod(billingProcessedFilter.getCollectionPeriod());
			billingCalculationProcess.setProcessedState(BillingCalculationType.ERROR.getCode());
			billingCalculationProcess.setFileXml(new byte[0]);
			billingProcessedServiceFacade.saveAllBillingProcessedService(billingCalculationProcess);

			/** Send Notification **/
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.PRELIMINARY_END_MONTH_PROCESS_BILLING.getCode());
			String subject = messageConfiguration.getProperty("header.message.confirmation");

			String message = messageConfiguration.getProperty("body.message.warning.preliminary");
			message = MessageFormat.format(message,
									BusinessProcessType.PRELIMINARY_END_MONTH_PROCESS_BILLING.getDescription(), 
									CommonsUtilities.currentDateTime().toString());

			List<UserAccountSession> listUser = this.businessLayerCommonFacade.getListUsserByBusinessProcess(BusinessProcessType.PRELIMINARY_END_MONTH_PROCESS_BILLING.getCode());
			businessLayerCommonFacade.sendNotificationManual(businessProcess,listUser, subject, message, NotificationType.EMAIL);
			businessLayerCommonFacade.sendNotificationManual(businessProcess,listUser, subject, message, NotificationType.MESSAGE);

			e.printStackTrace();
			throw new RuntimeException();
		}

		/***
		 * SEND REPORT AND NOTIFICATION OF PROCESS PRELIMINARY BILLING
		 * 
		 */
		try {

			BusinessProcess businessProcess = new BusinessProcess();

			businessProcess.setIdBusinessProcessPk(BusinessProcessType.PRELIMINARY_END_MONTH_PROCESS_BILLING.getCode());

			Object[] parameters = {
					BusinessProcessType.PRELIMINARY_END_MONTH_PROCESS_BILLING.getDescription(),
					CommonsUtilities.currentDateTime().toString() };
			businessLayerCommonFacade.sendNotificationAutomatic(businessProcess, null, parameters);

			if (Validations.validateIsNotNull(billingProcessedFilter.getProcessBillingCalculationPk())) {

				this.serviceAPIClients.generateBillingReport(billingProcessedFilter, 
						ReportIdType.CONSOLIDATED_BILLING_PRELIMINARY_REPORT.getCode());
				
				//billingProcessedServiceFacade.sendingReportOnBilling(billingProcessedFilter,
						//ReportIdType.CONSOLIDATED_BILLING_PRELIMINARY_REPORT.getCode());

			}

		} catch (ServiceException e) {

			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}

	
}