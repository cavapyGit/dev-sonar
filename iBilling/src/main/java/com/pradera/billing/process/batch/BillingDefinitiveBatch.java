package com.pradera.billing.process.batch;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

import com.pradera.billing.billing.facade.BillingProcessedServiceFacade;
import com.pradera.billing.billing.to.BillingProcessedFilter;
import com.pradera.billing.common.facade.BusinessLayerCommonFacade;
import com.pradera.billing.process.service.BillingCalculationService;
import com.pradera.billing.webservices.ServiceAPIClients;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.billing.BillingCalculationProcess;
import com.pradera.model.billing.type.BillingCalculationType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingDefinitiveBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@BatchProcess(name="BillingDefinitiveBatch")
@RequestScoped
public class BillingDefinitiveBatch implements JobExecution,Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	

	/** The billing calculation executor. */
	@EJB
	BillingCalculationService  		billingCalculationExecutor;
	
	/** The billing processed service facade. */
	@EJB
	BillingProcessedServiceFacade 	billingProcessedServiceFacade;
	
	/** The business layer common facade. */
	@EJB
	BusinessLayerCommonFacade		businessLayerCommonFacade;

	/** The message configuration. */
	@Inject
	@InjectableResource(location = "Messages_es.properties")
	private Properties messageConfiguration;
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	@EJB
	ServiceAPIClients serviceAPIClients;
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		
		
		/**getting details ProcessLoggerDetails */
		List<ProcessLoggerDetail> processLoggerDetails = processLogger.getProcessLoggerDetails();

		BillingProcessedFilter billingProcessedFilter  = new BillingProcessedFilter();;
		if (Validations.validateListIsNotNullAndNotEmpty(processLoggerDetails)){

			

			for(ProcessLoggerDetail processLoggerDetail: processLoggerDetails){



				/** GETTING PARAMETERS FOR ENTITY COLLECTION **/

				if(processLoggerDetail.getParameterName().equals(GeneralConstants.FIELD_CALCULATION_PROCESS)){
					billingProcessedFilter.setProcessBillingCalculationPk(Long.parseLong(processLoggerDetail.getParameterValue()));					
				}
				if(processLoggerDetail.getParameterName().equals(GeneralConstants.FIELD_DATE_BECOMING)){
					Date dateBecoming=CommonsUtilities.convertStringtoDate(processLoggerDetail.getParameterValue());
					billingProcessedFilter.setBillingDateBecoming(dateBecoming);
				}

				
			}
			
		}
		
		try {
			billingCalculationExecutor.executeDefinitiveProcessBilling(billingProcessedFilter);
		} catch (Exception e) {
					
			BillingCalculationProcess	billingCalculationProcess	= billingProcessedServiceFacade.findBillingCalculationProcessById(billingProcessedFilter.getProcessBillingCalculationPk());
			billingCalculationProcess.setProcessedState(BillingCalculationType.ERROR.getCode());
			
			try {
				billingProcessedServiceFacade.updateBillingCalculationProcess(billingCalculationProcess);
			} catch (ServiceException e1) {

				log.error("Error in execute updateBillingCalculationProcess in Billing Definitive Batch");
				e1.printStackTrace();
			}
			log.error(e.getMessage());
			e.printStackTrace();
			throw  new RuntimeException( );
		}
		

		/***
		*  SEND REPORT AND NOTIFICATION OF PROCESS PRELIMINARY BILLING
		*  	
		*/ 
		try {
			  BusinessProcess businessProcess = new BusinessProcess();

			  businessProcess.setIdBusinessProcessPk(BusinessProcessType.DEFINITIVE_PROCESS_BILLING.getCode());
			 String subject = messageConfiguration.getProperty("header.message.confirmation");
			
			if (Validations.validateIsNotNull(billingProcessedFilter.getBillingProcessedState()) && 
					BillingCalculationType.DEFINITIVO.getCode().equals(billingProcessedFilter.getBillingProcessedState())){
					
					Object[] parameters={BusinessProcessType.DEFINITIVE_PROCESS_BILLING.getDescription(),CommonsUtilities.currentDateTime().toString()};
					businessLayerCommonFacade.sendNotificationAutomatic(businessProcess, null, parameters);
					
					/**Send Report*/
					this.serviceAPIClients.generateBillingReport(billingProcessedFilter, 
							ReportIdType.CONSOLIDATED_BILLING_DEFINITIVE_REPORT.getCode());
					
					//billingProcessedServiceFacade.sendingReportOnBilling(billingProcessedFilter,ReportIdType.CONSOLIDATED_BILLING_DEFINITIVE_REPORT.getCode());
					
					/**Create File XML  */

				
			}else{
					String message = messageConfiguration.getProperty("body.message.warning.definitive");					 
					message = MessageFormat.format(message, BusinessProcessType.DEFINITIVE_PROCESS_BILLING.getDescription(),CommonsUtilities.currentDateTime().toString()); 
					List<UserAccountSession> listUser
					=this.businessLayerCommonFacade.getListUsserByBusinessProcess(BusinessProcessType.DEACTIVE_SERVICE_BILLING.getCode());
					
					businessLayerCommonFacade.sendNotificationManual(businessProcess, listUser, subject, message, NotificationType.MESSAGE);
					businessLayerCommonFacade.sendNotificationManual(businessProcess, listUser, subject, message, NotificationType.EMAIL);
			}
			} catch (ServiceException e) {
				log.error(e.getMessage());
				e.printStackTrace();
			}
		 
	}	

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		
		return false;
	}	


}
