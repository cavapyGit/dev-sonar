package com.pradera.billing.process.service;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import com.pradera.billing.billingcollection.to.CollectionRecordTo;
import com.pradera.billing.billingcollection.to.ProcessedServiceTo;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.billing.CollectionRecord;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Project iDepositary. Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Interface CollectionBasicService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Local
public interface CollectionBasicService {

	/**
	 * Start calculate collection transaction.
	 *
	 * @param processedServiceTo the processed service to
	 * @throws ServiceException the service exception
	 */
	public void startCalculateCollectionTransaction(ProcessedServiceTo processedServiceTo) throws ServiceException;

	/**
	 * Gets the ting data for calculating percentage.
	 *
	 * @param parameters the parameters
	 * @return the ting data for calculating percentage
	 * @throws Exception the exception
	 */
	public List<CollectionRecordTo> gettingDataForCalculatingPercentage(Map<String, Object> parameters)
			throws Exception;

	/**
	 * Gets the ting data for calculating fixed.
	 *
	 * @param parameters the parameters
	 * @return the ting data for calculating fixed
	 * @throws ServiceException the service exception
	 */
	public List<CollectionRecordTo> gettingDataForCalculatingFixed(Map<String, Object> parameters)
			throws ServiceException;

	/**
	 * Gets the ting data for calculating estaggered fixed.
	 *
	 * @param parameters the parameters
	 * @return the ting data for calculating estaggered fixed
	 * @throws ServiceException the service exception
	 */
	public List<CollectionRecordTo> gettingDataForCalculatingEstaggeredFixed(Map<String, Object> parameters)
			throws ServiceException;

	/**
	 * Process for service rate.
	 *
	 * @param parameters           the parameters
	 * @param collectionRecordList the collection record list
	 * @throws ServiceException the service exception
	 */
	public void processForServiceRate(Map<String, Object> parameters, List<CollectionRecord> collectionRecordList)
			throws ServiceException;

}
