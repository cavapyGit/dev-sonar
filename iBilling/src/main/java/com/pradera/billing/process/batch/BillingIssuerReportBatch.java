package com.pradera.billing.process.batch;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.services.remote.reports.ReportGenerationService;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.report.type.ReportFormatType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingIssuerReportBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@BatchProcess(name="BillingIssuerReportBatch")
@RequestScoped
public class BillingIssuerReportBatch implements JobExecution,Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The report id. */
	private final Long REPORT_ID = 188L;
	
	/** The report service. */
	@Inject 
	Instance<ReportGenerationService> reportService;
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());
		try{
			this.sendReport(loggerUser);
		}catch(ServiceException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Send report.
	 *
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void sendReport(LoggerUser loggerUser) throws ServiceException{
		ReportUser user = new ReportUser();
		user.setUserName(loggerUser.getUserName());
		user.setReportFormat(ReportFormatType.PDF.getCode());
		Map<String,String> parameters = new HashMap<String, String>();
		parameters.put("initial_date",CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));
		parameters.put("end_date",CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));
		reportService.get().saveReportExecution(REPORT_ID,parameters , null, user, loggerUser);
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}

}
