package com.pradera.billing.process.batch;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

import com.pradera.billing.billingservice.facade.BillingServiceMgmtServiceFacade;
import com.pradera.billing.common.facade.BusinessLayerCommonFacade;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessLogger;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingDeactiveBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@BatchProcess(name="BillingDeactiveBatch")
@RequestScoped
public class BillingDeactiveBatch implements JobExecution,Serializable {

	/** The billing service mgmt service facade. */
	@EJB
	BillingServiceMgmtServiceFacade billingServiceMgmtServiceFacade;
	
	/** The message configuration. */
	@Inject
	@InjectableResource(location = "Messages_es.properties")
	Properties messageConfiguration;
	
	/** The business layer common facade. */
	@EJB
	BusinessLayerCommonFacade		businessLayerCommonFacade;
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		// TODO Auto-generated method stub
		Date date = CommonsUtilities.currentDate();
		try {
			billingServiceMgmtServiceFacade.expiredBillingServicesActivedLock(date);
		} catch (Exception ex) {
		 
				BusinessProcess businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.DEACTIVE_SERVICE_BILLING.getCode());

				Object[] parameters={CommonsUtilities.currentDateTime().toString()};
				businessLayerCommonFacade.sendNotificationAutomatic(businessProcess, null, parameters);
		}
		
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}

}
