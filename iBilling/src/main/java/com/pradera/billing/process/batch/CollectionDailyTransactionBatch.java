package com.pradera.billing.process.batch;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.apache.commons.lang3.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.billing.billingcollection.facade.CollectionServiceMgmtServiceFacade;
import com.pradera.billing.billingcollection.service.SenderBillingService;
import com.pradera.billing.billingcollection.to.ProcessedServiceTo;
import com.pradera.billing.billingservice.to.BillingServiceTo;
import com.pradera.billing.common.facade.BusinessLayerCommonFacade;
import com.pradera.billing.common.utils.BillingUtils;
import com.pradera.billing.process.service.CollectionBasicService;
import com.pradera.billing.process.service.CollectionTransactionMovement;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.extension.transaction.Transactional;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CollectionDailyTransactionBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@BatchProcess(name="CollectionDailyTransactionBatch")
@RequestScoped
public class CollectionDailyTransactionBatch implements JobExecution,Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The collection transaction movement. */
	@EJB
	CollectionTransactionMovement collectionTransactionMovement;
	
	/** The collection basic service executor. */
	@EJB
	CollectionBasicService collectionBasicServiceExecutor;
	
	/** The crud dao service bean. */
	@EJB
	CrudDaoServiceBean crudDaoServiceBean;
	

	/** The collection service mgmt service facade. */
	@EJB
	CollectionServiceMgmtServiceFacade	collectionServiceMgmtServiceFacade;	
	 
	/** The business layer common facade. */
	@EJB
	BusinessLayerCommonFacade businessLayerCommonFacade;
	 
	/** The sender billing service. */
	@EJB
    SenderBillingService senderBillingService;
	
	
	/** The log. */
	@Inject
	PraderaLogger 			logger;
	/** The Constant log. */
	private static final Logger log = LoggerFactory
			.getLogger(CollectionDailyTransactionBatch.class);
	

	
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Transactional
	@Override
	public void startJob(ProcessLogger processLogger) {
		
		/**getting details ProcessLoggerDetails */
		List<ProcessLoggerDetail> processLoggerDetails = processLogger.getProcessLoggerDetails();
		
		List<ProcessedServiceTo>  listProcessedServiceTo=null;
		
		if (Validations.validateListIsNotNullAndNotEmpty(processLoggerDetails)){
		/** Enter here where the calculation automatic are executed manually**/
		ProcessedServiceTo processedServiceTo= new ProcessedServiceTo();
			for(ProcessLoggerDetail processLoggerDetail: processLoggerDetails){
				
				/**
				 * If Batch Execution For Execute Process
				 */
				if(processLoggerDetail.getParameterName().equals(GeneralConstants.PARAMETER_DATE)){
					Date calculationDate =CommonsUtilities.convertStringtoDate(processLoggerDetail.getParameterValue());
					processedServiceTo.setCalculationDate(calculationDate);
					processedServiceTo.setOperationDate(CommonsUtilities.currentDate());
					processedServiceTo=null;
					break;
				}
				if(processLoggerDetail.getParameterName().equals(GeneralConstants.PARAMETER_SCHEDULE_ID)){
					processedServiceTo=null;
					break;
				}
				
				
				if(processLoggerDetail.getParameterName().equals(GeneralConstants.FIELD_ID_BILLING_SERVICE_PK)){
					Long idBillingServicePk = Long.parseLong(processLoggerDetail.getParameterValue());
					
					if (Validations.validateIsNull(processedServiceTo.getBillingServiceTo())){
						BillingServiceTo billingServiceTo = new BillingServiceTo();
						processedServiceTo.setBillingServiceTo(billingServiceTo);
					}
					
					processedServiceTo.getBillingServiceTo().setIdBillingServicePk(idBillingServicePk);	
					
				}
				
				if(processLoggerDetail.getParameterName().equals(GeneralConstants.FIELD_ID_PROCESSED_SERVICE_PK)){	
					processedServiceTo.setIdProcessedServicePk(Long.parseLong(processLoggerDetail.getParameterValue()));
				}
				
				if(processLoggerDetail.getParameterName().equals(GeneralConstants.FIELD_CALCULATION_DATE)){	
					Date calculationDate =CommonsUtilities.convertStringtoDate(processLoggerDetail.getParameterValue());
					processedServiceTo.setCalculationDate(calculationDate);
					processedServiceTo.setOperationDate(CommonsUtilities.currentDate());
				}
				
				
				if(processLoggerDetail.getParameterName().equals(GeneralConstants.FIELD_SERVICE_CODE)){	
					if (Validations.validateIsNull(processedServiceTo.getBillingServiceTo())){
						BillingServiceTo billingServiceTo = new BillingServiceTo();
						processedServiceTo.setBillingServiceTo(billingServiceTo);
					}
					
					String serviceCode=  processLoggerDetail.getParameterValue().toString();
					processedServiceTo.getBillingServiceTo().setServiceCode(serviceCode);
				}
				
				if(processLoggerDetail.getParameterName().equals(GeneralConstants.FIELD_SERVICE_NAME)){	
					if (Validations.validateIsNull(processedServiceTo.getBillingServiceTo())){
						BillingServiceTo billingServiceTo = new BillingServiceTo();
						processedServiceTo.setBillingServiceTo(billingServiceTo);
					}
					
					String serviceName=  processLoggerDetail.getParameterValue().toString();
					processedServiceTo.getBillingServiceTo().setServiceName(serviceName);
				}
				if(processLoggerDetail.getParameterName().equals(GeneralConstants.FIELD_BASE_COLLECTION)){
					
					if (Validations.validateIsNull(processedServiceTo.getBillingServiceTo())){
						BillingServiceTo billingServiceTo = new BillingServiceTo();
						processedServiceTo.setBillingServiceTo(billingServiceTo);
					}
						
					Integer baseCollection =Integer.parseInt(processLoggerDetail.getParameterValue());
					processedServiceTo.getBillingServiceTo().setBaseCollection(baseCollection);
					
				}
				
				if(processLoggerDetail.getParameterName().equals(GeneralConstants.FIELD_CALCULATION_PERIOD)){
					
					if (Validations.validateIsNull(processedServiceTo.getBillingServiceTo())){
						BillingServiceTo billingServiceTo = new BillingServiceTo();
						processedServiceTo.setBillingServiceTo(billingServiceTo);
					}
						
					Integer calculationPeriod =Integer.parseInt(processLoggerDetail.getParameterValue());
					processedServiceTo.getBillingServiceTo().setCalculationPeriod(calculationPeriod);
					
				}
							
			}
			
			if (Validations.validateIsNotNull(processedServiceTo)){
				listProcessedServiceTo= new ArrayList<ProcessedServiceTo>();			
				listProcessedServiceTo.add(processedServiceTo);
			}else{
				listProcessedServiceTo=collectionServiceMgmtServiceFacade.findProcessAutomaticDailyBatch();
			}
		}
		else{
		
		/**
		 * Enter here where the calculation automatic are executed by batch
		 * getting all services related to collection base Transaction Movement**/
			/** ACA SE DEBE LISTAR TODOS LOS DIARIOS   **/
			listProcessedServiceTo=collectionServiceMgmtServiceFacade.findProcessAutomaticDailyBatch();

			
		}
		
		List<ProcessedServiceTo> listProcessedServiceTmp=new ArrayList<ProcessedServiceTo>();
		for (Iterator iterator = listProcessedServiceTo.iterator(); iterator.hasNext();) {
			
			ProcessedServiceTo processedServiceTo = (ProcessedServiceTo) iterator.next();
			
			/**
			 * If Before Calculation Day is Holiday, replicar
			 */
			Date calculationDate= processedServiceTo.getCalculationDate();
			Date afterCalculationDate=CommonsUtilities.addDate(calculationDate, 1);
			try {
				while(businessLayerCommonFacade.ifDayHoliday(afterCalculationDate)){
					ProcessedServiceTo processedServiceNext=SerializationUtils.clone(processedServiceTo);
					processedServiceNext.setCalculationDate(afterCalculationDate);
					listProcessedServiceTmp.add(processedServiceNext);
					
					afterCalculationDate=CommonsUtilities.addDate(afterCalculationDate, 1);
				}
			} catch (ServiceException e) {
				log.error(e.getMessage());
				logger.error(e.getMessage());
			}
			
		}
		
		if (Validations.validateListIsNotNullAndNotEmpty(listProcessedServiceTo)){
			List<Object> listCalculationServices=new ArrayList<Object>();
			Object calculationServices=null;

			LoggerUser loggerUser = businessLayerCommonFacade.getLoggerUser();
			if(loggerUser.getIdPrivilegeOfSystem()== null){
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
			}
			for (ProcessedServiceTo processedServiceTo : listProcessedServiceTo) {
				
					/** Notification. list to Billing*/
					calculationServices=new Object();
					calculationServices=BillingUtils.BREAK_LINE.concat(processedServiceTo.getBillingServiceTo().getServiceCode()).
										concat(GeneralConstants.DASH).
										concat(processedServiceTo.getBillingServiceTo().getServiceName());
					listCalculationServices.add(calculationServices);
					
					processedServiceTo.setLoggerUser(loggerUser);

					/** begin process transactional **/
					
					/** EL SINGLETON DEBE ENCOLAR LAS LLAMADAS POR BASE DE COBRO  */								
					try {						
						senderBillingService.senderServiceForCalculate(processedServiceTo);					
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						log.error(e.getMessage());
						logger.error(e.getMessage());
					}	
					/** end process transactional **/
			}
				BusinessProcess businessProcess = new BusinessProcess();				
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.DAILY_COLLECT_SERVICE_CALCULATION.getCode());
				Object[] parameters={BillingUtils.convertListObjectToString(listCalculationServices,GeneralConstants.STR_COMMA_WITHOUT_SPACE)};
				businessLayerCommonFacade.sendNotificationAutomatic(businessProcess, null, parameters);
		}
		else{
			log.info("DONT EXISTS SERVICE BILLINGS AVAILABLES FOR CALCULATIONS ");
			log.error("DONT EXISTS SERVICE BILLINGS AVAILABLES FOR CALCULATIONS ");
			logger.error("DONT EXISTS SERVICE BILLINGS AVAILABLES FOR CALCULATIONS ");
		}
	}
	
	
	
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}


}