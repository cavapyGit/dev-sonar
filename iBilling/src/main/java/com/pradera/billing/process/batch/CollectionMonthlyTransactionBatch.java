package com.pradera.billing.process.batch;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.AsyncResult;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.billing.billingcollection.facade.CollectionServiceMgmtServiceFacade;
import com.pradera.billing.billingcollection.service.SenderBillingService;
import com.pradera.billing.billingcollection.to.ProcessedServiceTo;
import com.pradera.billing.billingservice.to.BillingServiceTo;
import com.pradera.billing.common.facade.BusinessLayerCommonFacade;
import com.pradera.billing.common.utils.BillingUtils;
import com.pradera.billing.process.service.CollectionBasicService;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CollectionMonthlyTransactionBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@BatchProcess(name="CollectionMonthlyTransactionBatch")
@RequestScoped
public class CollectionMonthlyTransactionBatch  implements JobExecution,Serializable{
	
	
	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	
	/** The collection basic service executor. */
	@EJB
	CollectionBasicService collectionBasicServiceExecutor;
	
	 
	/** The collection service mgmt service facade. */
	@EJB
	CollectionServiceMgmtServiceFacade collectionServiceMgmtServiceFacade;
	
	/** The business layer common facade. */
	@EJB
	BusinessLayerCommonFacade businessLayerCommonFacade;
	 
	/** The sender billing service. */
	@EJB
    SenderBillingService senderBillingService;
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		
		

		
		/**getting details ProcessLoggerDetails */
		List<ProcessLoggerDetail> processLoggerDetails = processLogger.getProcessLoggerDetails();
		
		List<ProcessedServiceTo>  listProcessedServiceTo=null;
		
		
		
		if (Validations.validateListIsNotNullAndNotEmpty(processLoggerDetails)){
		/** Enter here where the calculation automatic are executed manually**/
		ProcessedServiceTo processedServiceTo= new ProcessedServiceTo();	
			for(ProcessLoggerDetail processLoggerDetail: processLoggerDetails){
				
				/**
				 * If Batch Execution For Execute Process
				 */
				if(processLoggerDetail.getParameterName().equals(GeneralConstants.PARAMETER_DATE)){
					Date calculationDate =CommonsUtilities.convertStringtoDate(processLoggerDetail.getParameterValue());
					processedServiceTo.setCalculationDate(calculationDate);
					processedServiceTo.setOperationDate(CommonsUtilities.currentDate());
					processedServiceTo=null;
					break;
				}
				if(processLoggerDetail.getParameterName().equals(GeneralConstants.PARAMETER_SCHEDULE_ID)){
					processedServiceTo=null;
					break;
				}
				
				if(processLoggerDetail.getParameterName().equals(GeneralConstants.FIELD_ID_BILLING_SERVICE_PK)){
					Long idBillingServicePk = Long.parseLong(processLoggerDetail.getParameterValue());
					
					if (Validations.validateIsNull(processedServiceTo.getBillingServiceTo())){
					BillingServiceTo billingServiceTo = new BillingServiceTo();
					processedServiceTo.setBillingServiceTo(billingServiceTo);
					}
					
					processedServiceTo.getBillingServiceTo().setIdBillingServicePk(idBillingServicePk);	
					
				}
				
				
				if(processLoggerDetail.getParameterName().equals(GeneralConstants.FIELD_ID_PROCESSED_SERVICE_PK)){	
					processedServiceTo.setIdProcessedServicePk(Long.parseLong(processLoggerDetail.getParameterValue()));
				}
				
				
				
				if(processLoggerDetail.getParameterName().equals(GeneralConstants.FIELD_CALCULATION_DATE)){	
					Date calculationDate =CommonsUtilities.convertStringtoDate(processLoggerDetail.getParameterValue());
					processedServiceTo.setCalculationDate(calculationDate);
					processedServiceTo.setOperationDate(CommonsUtilities.currentDate());
				}
				
				if(processLoggerDetail.getParameterName().equals(GeneralConstants.FIELD_YEAR)){
					Integer year= Integer.parseInt(processLoggerDetail.getParameterValue());
					processedServiceTo.setYearProcess(year);
				}
				
				if(processLoggerDetail.getParameterName().equals(GeneralConstants.FIELD_MONTH)){	
					Integer month= Integer.parseInt(processLoggerDetail.getParameterValue());
					processedServiceTo.setMonthProcess(month);
				}
				if(processLoggerDetail.getParameterName().equals(GeneralConstants.FIELD_INITIAL_DATE)){	
					Date initialDate =CommonsUtilities.convertStringtoDate(processLoggerDetail.getParameterValue());
					
					processedServiceTo.setInitialDate(initialDate);
				}
				if(processLoggerDetail.getParameterName().equals(GeneralConstants.FIELD_FINAL_DATE)){	
					Date finalDate =CommonsUtilities.convertStringtoDate(processLoggerDetail.getParameterValue());
					
					processedServiceTo.setFinalDate(finalDate);
				}
				
				
				
				if(processLoggerDetail.getParameterName().equals(GeneralConstants.FIELD_SERVICE_CODE)){	
					if (Validations.validateIsNull(processedServiceTo.getBillingServiceTo())){
						BillingServiceTo billingServiceTo = new BillingServiceTo();
						processedServiceTo.setBillingServiceTo(billingServiceTo);
					}
					
					String serviceCode=  processLoggerDetail.getParameterValue().toString();
					processedServiceTo.getBillingServiceTo().setServiceCode(serviceCode);
				}
				
				if(processLoggerDetail.getParameterName().equals(GeneralConstants.FIELD_SERVICE_NAME)){	
					if (Validations.validateIsNull(processedServiceTo.getBillingServiceTo())){
						BillingServiceTo billingServiceTo = new BillingServiceTo();
						processedServiceTo.setBillingServiceTo(billingServiceTo);
					}
					
					String serviceName=  processLoggerDetail.getParameterValue().toString();
					processedServiceTo.getBillingServiceTo().setServiceName(serviceName);
				}
				
				if(processLoggerDetail.getParameterName().equals(GeneralConstants.FIELD_BASE_COLLECTION)){
					
					if (Validations.validateIsNull(processedServiceTo.getBillingServiceTo())){
						BillingServiceTo billingServiceTo = new BillingServiceTo();
						processedServiceTo.setBillingServiceTo(billingServiceTo);
					}
						
					Integer baseCollection =Integer.parseInt(processLoggerDetail.getParameterValue());
					processedServiceTo.getBillingServiceTo().setBaseCollection(baseCollection);
					
				}
				
				if(processLoggerDetail.getParameterName().equals(GeneralConstants.FIELD_CALCULATION_PERIOD)){	
					
					if (Validations.validateIsNull(processedServiceTo.getBillingServiceTo())){
						BillingServiceTo billingServiceTo = new BillingServiceTo();
						processedServiceTo.setBillingServiceTo(billingServiceTo);
					}
						
					Integer calculationPeriod =Integer.parseInt(processLoggerDetail.getParameterValue());
					processedServiceTo.getBillingServiceTo().setCalculationPeriod(calculationPeriod);
					
				}
				
			}
			
			if (Validations.validateIsNotNull(processedServiceTo)){
				listProcessedServiceTo= new ArrayList<ProcessedServiceTo>();			
				listProcessedServiceTo.add(processedServiceTo);
			}else{
				listProcessedServiceTo=collectionServiceMgmtServiceFacade.findProcessAutomaticMonthlyBatch();
			}
		}
		else{
		
		/**
		 * Enter here where the calculation automatic are executed by batch
		 * getting all services related to collection base **/
			
			/** ACA SE DEBE LISTAR TODOS LOS MENSUALES  **/
			listProcessedServiceTo=collectionServiceMgmtServiceFacade.findProcessAutomaticMonthlyBatch();
		}
		
		
		
		if (Validations.validateListIsNotNullAndNotEmpty(listProcessedServiceTo)){
			List<Object> listCalculationServices=new ArrayList<Object>();
			Object calculationServices=null;
			LoggerUser loggerUser = businessLayerCommonFacade.getLoggerUser();
			
			
			
			for (ProcessedServiceTo processedServiceTo : listProcessedServiceTo) {
				
				
				/** Notification. list to Billing*/
				calculationServices=new Object();
				calculationServices=BillingUtils.BREAK_LINE.concat(processedServiceTo.getBillingServiceTo().getServiceCode()).
									concat(BillingUtils.HORIZONTAL_LINE).
									concat(processedServiceTo.getBillingServiceTo().getServiceName());
				listCalculationServices.add(calculationServices);
				
				processedServiceTo.setLoggerUser(loggerUser);
				
				try {
					AsyncResult att=new AsyncResult<Integer>(1);
					senderBillingService.senderServiceForCalculate(processedServiceTo);
					
					
				
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					log.error(e.getMessage());
				}
			
			}
				BusinessProcess businessProcess = new BusinessProcess();
				
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.MONTHLY_COLLECT_SERVICE_CALCULATION.getCode());
				Object[] parameters={BillingUtils.convertListObjectToString(listCalculationServices,GeneralConstants.STR_COMMA_WITHOUT_SPACE)};
				businessLayerCommonFacade.sendNotificationAutomatic(businessProcess, null, parameters);
			
			
		}
		else{
			log.info("DONT EXISTS SERVICE BILLINGS AVAILABLES FOR CALCULATIONS ");
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}

	
}