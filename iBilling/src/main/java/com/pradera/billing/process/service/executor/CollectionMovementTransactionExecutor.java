package com.pradera.billing.process.service.executor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.ejb.AccessTimeout;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.billing.billingcollection.facade.BaseCollectionMgmtServiceFacade;
import com.pradera.billing.billingcollection.facade.CollectionServiceMgmtServiceFacade;
import com.pradera.billing.billingcollection.service.BaseCollectionMgmtServiceBean;
import com.pradera.billing.billingcollection.to.CollectionRecordDetailTo;
import com.pradera.billing.billingcollection.to.CollectionRecordTo;
import com.pradera.billing.billingcollection.to.DailyExchangeRateFilter;
import com.pradera.billing.billingcollection.to.ProcessedServiceTo;
import com.pradera.billing.billingrate.facade.BillingRateMgmtServiceFacade;
import com.pradera.billing.billingservice.facade.BillingServiceMgmtServiceFacade;
import com.pradera.billing.billingservice.service.BillingServiceMgmtServiceBean;
import com.pradera.billing.billingservice.to.BillingServiceTo;
import com.pradera.billing.common.facade.BusinessLayerCommonFacade;
import com.pradera.billing.common.utils.BillingUtils;
import com.pradera.billing.process.service.CollectionTransactionMovement;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.entity.producer.DepositaryDataBase;
import com.pradera.core.framework.entity.service.LoaderEntityServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.billing.BillingEconomicActivity;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.CollectionRecord;
import com.pradera.model.billing.CollectionRecordDetail;
import com.pradera.model.billing.ProcessedService;
import com.pradera.model.billing.RateMovement;
import com.pradera.model.billing.ServiceRate;
import com.pradera.model.billing.ServiceRateScale;
import com.pradera.model.billing.type.BaseCollectionType;
import com.pradera.model.billing.type.EntityCollectionType;
import com.pradera.model.billing.type.ProcessedServiceType;
import com.pradera.model.billing.type.RateStateType;
import com.pradera.model.billing.type.RateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.DailyExchangeRoleType;
import com.pradera.model.issuancesecuritie.Issuer;




// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CollectionMovementTransactionExecutor.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Singleton
public class CollectionMovementTransactionExecutor implements CollectionTransactionMovement{

	/** The log. */
	@Inject
	PraderaLogger log;
		
	/** The base collection mgmt service facade. */
	@EJB
	BaseCollectionMgmtServiceFacade baseCollectionMgmtServiceFacade;
	
	/** The billing service mgmt service facade. */
	@EJB
	BillingServiceMgmtServiceFacade billingServiceMgmtServiceFacade;
	
	/** The billing rate mgmt service facade. */
	@EJB
	BillingRateMgmtServiceFacade billingRateMgmtServiceFacade;
	
	/** The collection service mgmt service facade. */
	@EJB 
	CollectionServiceMgmtServiceFacade collectionServiceMgmtServiceFacade;
	

	/** The billing service mgmt service bean. */
	@EJB
	private BillingServiceMgmtServiceBean billingServiceMgmtServiceBean;
	
	/** The business layer common facade. */
	@EJB
	BusinessLayerCommonFacade businessLayerCommonFacade;
	
	@EJB
	 BaseCollectionMgmtServiceBean 		baseCollectionMgmtServiceBean;
	
	/** The load entity service. */
	@EJB
	LoaderEntityServiceBean loadEntityService;	
	
	/** The em. */
	@Inject @DepositaryDataBase
	private EntityManager em;
	 
	/**
	 * Gets the ting list billing services.
	 *
	 * @return the ting list billing services
	 */
	public void gettingListBillingServices(){
		
	}
	
	/** Processing Collection Movement Transaction
	 * @param processedServiceTo Filter Processed Service
	 * @see com.pradera.billing.process.service.CollectionTransactionMovement#startCalculateCollectionTransaction(com.pradera.billing.billingcollection.to.ProcessedServiceTo)
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=60)
	@AccessTimeout(unit=TimeUnit.MINUTES,value=60)
	@Performance
	@Override
	public void startCalculateCollectionTransaction(ProcessedServiceTo processedServiceTo) throws ServiceException   {
				
		if (processedServiceTo.getErrorType()!=null){
			throw new ServiceException(processedServiceTo.getErrorType());
		}
		else if(processedServiceTo.getOtherErrorType()){
			throw new ServiceException();
		}
		
		ProcessedService processedService = null;
		
     	collectionServiceMgmtServiceFacade.verifyProcessedServiceUniqueByPeriod(processedServiceTo);							
		
     	collectionServiceMgmtServiceFacade.verifyRateActives(processedServiceTo);
								
		processedService = collectionServiceMgmtServiceFacade.verifyCollectionUnbilled(processedServiceTo);
		processedService.setInitialDate(processedServiceTo.getInitialDate());
		processedService.setFinalDate(processedServiceTo.getFinalDate());
		loadEntityService.detachmentEntity(processedService);
		
		
		
		/**::::::::::::::::::::::::::::: 1. --  getting billing service ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
		BillingService billingService = processedService.getBillingService();
		List<BillingEconomicActivity> listEconActBd=billingServiceMgmtServiceBean.getBillingEconomicActivityByBillingServicePk(billingService.getIdBillingServicePk());		
		List<Integer> listEconomicAct=BillingUtils.getBillingEconomicActivityListInteger(listEconActBd);

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put(GeneralConstants.BILLING_SERVICE, billingService);
		parameters.put(GeneralConstants.ECONOMIC_ACTIVITY_LIST_INT, listEconomicAct);
		parameters.put(GeneralConstants.OPERATION_DATE,processedService.getOperationDate());
		parameters.put(GeneralConstants.CALCULATION_DATE,processedService.getCalculationDate());
		
		if (PropertiesConstants.PERIOD_CALCULATION_DAILY_PK.equals(billingService.getCalculationPeriod())){
			Date calculateDate= processedService.getCalculationDate();
			Date date1=BillingUtils.getDateInitialDay(calculateDate);
			Date date2=BillingUtils.getDateFinalDay(calculateDate);
			parameters.put(GeneralConstants.FIELD_INITIAL_DATE, date1);
			parameters.put(GeneralConstants.FIELD_FINAL_DATE, date2);
		}else if (PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK.equals(billingService.getCalculationPeriod())){
			parameters.put(GeneralConstants.FIELD_INITIAL_DATE, processedService.getInitialDate());
			parameters.put(GeneralConstants.FIELD_FINAL_DATE, processedService.getFinalDate());
		}
		
		DailyExchangeRateFilter dailyExchangeRateFilter = new DailyExchangeRateFilter();			
		dailyExchangeRateFilter.setIdSourceinformation(billingService.getSourceInformation());	
		dailyExchangeRateFilter.setInitialDate((processedService.getCalculationDate()==null?null:CommonsUtilities.truncateDateTime(processedService.getCalculationDate()) ));
		dailyExchangeRateFilter.setFinalDate((processedService.getCalculationDate()==null?null:CommonsUtilities.truncateDateTime(processedService.getCalculationDate()) ));

		Map<Integer,Map> exchangeCalculation= new HashMap<Integer,Map>();
		exchangeCalculation=businessLayerCommonFacade.chargeExchangeMemoryBuyBs(dailyExchangeRateFilter, DailyExchangeRoleType.BUY.getCode());
		parameters.put(GeneralConstants.MATRIX_EXCHANGE_RATE_BUY,exchangeCalculation);
		
		Map<Integer,Map> exchangeSell= new HashMap<Integer,Map>();
		exchangeSell=businessLayerCommonFacade.chargeExchangeMemorySelllBs(dailyExchangeRateFilter, DailyExchangeRoleType.SELL.getCode());
		parameters.put(GeneralConstants.MATRIX_EXCHANGE_RATE_SELL,exchangeSell);
		
		
		List<ServiceRate> listServiceRates = billingService.getServiceRates();
		
		/**this list will be filled */
		List<CollectionRecord> collectionRecordList=null; 
		
		
		
		if (Validations.validateListIsNotNullAndNotEmpty(listServiceRates)){
			
			collectionRecordList = new ArrayList<CollectionRecord>();
			
			for (ServiceRate serviceRate : listServiceRates) {
				
				if (serviceRate.getRateState().equals(RateStateType.ACTIVED.getCode())){
						
					Integer indMovement=serviceRate.getIndScaleMovement();
						
			           /** ::::::::::::::::::::::::::::: 2 -- getting movement type only billed ::::::::::::::::::::::::::::::::::::::::::::::::*/
					    List<RateMovement> rateMovementList = null;
				
						rateMovementList= billingRateMgmtServiceFacade.findRateMovementByServiceRate(serviceRate.getIdServiceRatePk());
						
						
						List<CollectionRecordTo> collectionRecordToList=null;
						if (Validations.validateListIsNotNullAndNotEmpty(rateMovementList)){
							
					        parameters.put(GeneralConstants.SERVICE_RATE, serviceRate);
							serviceRate.setRateMovements(rateMovementList);
							/** :::::::::::::::::::::::::::::: 3    GETTING DATA BY EACH  MOVEMENT RATE ::::::::::::::::::::::::::::::::::::::::::::::: */
								
								/***
								 * Rate only Movement percentage
								 */
								if (serviceRate.getRateType().equals(RateType.TIPO_MOVIMIENTO_PORCENTUAL.getCode())){
									collectionRecordToList= gettingDataForCalculatingByMovement(parameters);
							    }else if (serviceRate.getRateType().equals(RateType.TIPO_MOVIMIENTO_FIJO.getCode())){
							    	collectionRecordToList= gettingDataForCalculatingByQuantity(parameters);
							    }else if (serviceRate.getRateType().equals(RateType.FIJO.getCode())){	
							    	collectionRecordToList= gettingDataForCalculatingByQuantity(parameters);
							    }
								
								/**
								 * ESCALONADA FIJO CON MOVIMIENTO 
								 */
							    else if (serviceRate.getRateType().equals(RateType.ESCALONADO_FIJO.getCode())){
									/****
									 * VERIFY IND ACCUMULATIVE
									 */
									/** ::::::::::::::::::::::::::::: 2 -- getting movement type only billed ::::::::::::::::::::::::::::::::::::::::::::::::*/
								    List<ServiceRateScale> serviceRateScaleList = billingRateMgmtServiceFacade.findServiceRateScaleByServiceRate(serviceRate.getIdServiceRatePk());
								    serviceRate.setServiceRateScales(serviceRateScaleList);
								    
								    if(indMovement.equals(GeneralConstants.ONE_VALUE_INTEGER)){
								    	if (Validations.validateListIsNotNullAndNotEmpty(serviceRateScaleList)){
								    		/**
								    		 * Getting movement
								    		 * Applied Scale
								    		 * */
								    		collectionRecordToList	= gettingDataForCalculatingEstaggeredFixed(parameters);	
											
									    }
								    }
									else{
										
										collectionRecordToList	= gettingDataForCalculatingEstaggeredFixed(parameters);	
									}
								
							    /** :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
								
								}
								/**
								 * ESCALONADA PORCENTUAL CON MOVIMIENTO
								 * 
								 * REMATERIALIZACION_REVERSION_ANOTACIONES_CUENTA_R_FIJA
								 * REMATERIALIZACION_REVERSION_ANOTACIONES_CUENTA_R_VARIABLE
								 * 
								 */
							    else if (serviceRate.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode())){
									
									/****
									 * VERIFY IND ACCUMULATIVE
									 */
									/** ::::::::::::::::::::::::::::: 2. -- getting movement type only billed ::::::::::::::::::::::::::::::::::::::::::::::::*/
								    List<ServiceRateScale> serviceRateScaleList = billingRateMgmtServiceFacade.findServiceRateScaleByServiceRate(serviceRate.getIdServiceRatePk());
								    serviceRate.setServiceRateScales(serviceRateScaleList);
								    
								    if(indMovement.equals(GeneralConstants.ONE_VALUE_INTEGER)){
								    	if (Validations.validateListIsNotNullAndNotEmpty(serviceRateScaleList)){
								    		
								    		/**
								    		 * Getting movement
								    		 * Applied Scale
								    		 * */
								    		collectionRecordToList	= gettingDataForCalculatingEstaggeredFixed(parameters);
									    }
								    }
									else{
										
										collectionRecordToList	= gettingDataForCalculatingEstaggeredFixed(parameters);	
									}
								    
								
							    /** :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
								
								}
								/**
								 * ESCALONADA MIXTA CON MOVIMIENTO
								 */
							    else if (serviceRate.getRateType().equals(RateType.ESCALONADO_MIXTO.getCode())){
									
									/****
									 * VERIFY IND ACCUMULATIVE
									 */
									/** ::::::::::::::::::::::::::::: 2. -- getting movement type only billed ::::::::::::::::::::::::::::::::::::::::::::::::*/
								    List<ServiceRateScale> serviceRateScaleList = billingRateMgmtServiceFacade.findServiceRateScaleByServiceRate(serviceRate.getIdServiceRatePk());
								    serviceRate.setServiceRateScales(serviceRateScaleList);
								    
								    if(indMovement.equals(GeneralConstants.ONE_VALUE_INTEGER)){
								    	if (Validations.validateListIsNotNullAndNotEmpty(serviceRateScaleList)){
								    		/**
								    		 * Getting movement
								    		 * Applied Scale
								    		 * */
								    		collectionRecordToList	= gettingDataForCalculatingEstaggeredFixed(parameters);	
											
									    }
								    }
									else{
										
										collectionRecordToList	= gettingDataForCalculatingEstaggeredFixed(parameters);	
									}
								    
								
							    /** :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
								
								}			
										
						    /** :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
						
								
						}
						
						
				
						if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordToList)){
						
							 
					    log.info(" NUMBER OF REGISTER FOR CALCULATE  : "+collectionRecordToList.size()+"");
							
						parameters.put(GeneralConstants.COLLECTION_RECORD_TO_LIST,collectionRecordToList );
						
			            /** :::::::::::::::::::::::::::::: 4.   ADDING NEW AMOUNT BY EACH  MOVEMENT RATE ::::::::::::::::::::::::::::::::::::::::::::::: */					
						
						processForServiceRate(parameters, collectionRecordList);
						
						
						/** :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
						
						
						/** after than  add mount for calculation , setting memory to null */
						parameters.remove(GeneralConstants.COLLECTION_RECORD_TO_LIST);
						parameters.remove(GeneralConstants.MINIMUM_BILLING);
						collectionRecordToList=null;// setting values
						
						}else
						{
							log.info(" NUMBER OF REGISTER FOR CALCULATE  : 0 ");
						}
						
						
						if (parameters.containsKey(GeneralConstants.SERVICE_RATE)){
							parameters.remove(GeneralConstants.SERVICE_RATE);
						}
						
				}								
		}// end each service rate
			
			
		if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordList)){
			
			
			for (CollectionRecord collectionRecord : collectionRecordList) {
				
				collectionRecord.setProcessedService(processedService);
			}
			processedService.setCollectionRecords(collectionRecordList);
			

			/** :::::::::::::::  FILTRAR ENTITIES BILLED  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::**/
					
			collectionServiceMgmtServiceFacade.filterEntitiesBilled(processedService);
			
			/** :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */		

			if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordList) ){
				
				/** persist calculation on BD */
				collectionServiceMgmtServiceFacade.saveProcessedServiceByBatch(processedService);							
				
			}
			

		}
		
		ProcessedService processedServiceFinish=
				collectionServiceMgmtServiceFacade.getProcessedServiceByCode(processedService.getIdProcessedServicePk());
				processedServiceFinish.setOperationDate(CommonsUtilities.currentDate());
				processedServiceFinish.setProcessedState(ProcessedServiceType.PROCESADO.getCode());
		log.info(" ::::::::::::::::::::::::::::::::  FIN   Proceso de Transaccion de Movimientos ::::::::::::::::::::::::::::::::");
		log.info(" ::::::::::::::::::  Codigo de Servicio =  " +billingService.getServiceCode()+" ::::::::::::::::::::::::::");	
		
		
		if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordList)){	
			log.info(" ::::::::::::::::::::::::::::::::  REGISTRO COBROS EN EL SERVICIO = " +collectionRecordList.size()+"  :::");
		}else{
			log.info(" ::::::::::::::::::::::::::::::::  NO REGISTRO COBROS EN EL SERVICIO = " +billingService.getServiceCode()+"  :::");
		}
				
		}

		em.flush();		
		
	}
	
	

	
	
	/* (non-Javadoc)
	 * @see com.pradera.billing.process.service.CollectionTransactionMovement#processForServiceRate(java.util.Map, java.util.List)
	 */
	@Override
	public void processForServiceRate(Map<String, Object> parameters, List<CollectionRecord> collectionRecordList) throws ServiceException{
		
			BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
			List<Integer> listEconomicAct=(List<Integer>)parameters.get(GeneralConstants.ECONOMIC_ACTIVITY_LIST_INT);
			Integer portFolio=billingService.getPortfolio();
			

			/**
			 * Cambio: si es SAFI con cartera de clientes, crear un listado de CollectionRecord
			 */
			if(BillingUtils.isSafiPortfolioClient(listEconomicAct, portFolio)) {
				generateCollectionRecordSafi(parameters,collectionRecordList);
			}else {
				generateCollectionRecord(parameters,collectionRecordList);
			}

		
	}
	
	public List<CollectionRecord>  generateCollectionRecord(Map<String, Object> parameters, List<CollectionRecord> collectionRecordList) throws ServiceException {
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		ServiceRate serviceRate = (ServiceRate) parameters.get(GeneralConstants.SERVICE_RATE);
		Date calculateDate= (Date) parameters.get(GeneralConstants.CALCULATION_DATE);
		Date operationDate= (Date) parameters.get(GeneralConstants.OPERATION_DATE);

		/*** GETTING NEW LIST COLLECTION RECORD WITH NEW AMOUNT FOR RATE , IF THERE IS AN ENTITY ON LIST COLLECTION RECORD  ONLY ADDS BY AMOUNT RATE,  
		     IF NOT CREATE A NEW COLLECTION RECORD FOR A ENTITY COLLECTION AND AFTER ADD A  LIST OF COLLECTION RECORDS  **/			
		List<CollectionRecordTo> collectionRecordToList = (List<CollectionRecordTo>) parameters.get(GeneralConstants.COLLECTION_RECORD_TO_LIST);
		

		
		for (CollectionRecordTo collectionRecordTo : collectionRecordToList) {
		
			List<CollectionRecordDetail> newCollectionRecordDetailList=loadCollectionRecordDetail(collectionRecordTo.getCollectionRecordDetailToList(), serviceRate,parameters);
			
			/**getting entity for add news amount **/
			Object objEntity = collectionRecordTo.getEntityCollection(collectionRecordTo.getEntityCollection());	
			CollectionRecord collectionRecord=findEntityAtList(collectionRecordList, objEntity, collectionRecordTo.getEntityCollection());
			
			/** THE GOAL IS  CREATE A RECORD FOR EACH ENTITY ONLY IF DON'T EXIST**/
			if (collectionRecord==null){
				collectionRecord = new CollectionRecord();
				collectionRecord.setEntityCollectionId(billingService.getEntityCollection());						
				collectionRecord.setGrossAmount(new BigDecimal(0));
				collectionRecord.setCollectionAmount(new BigDecimal(0));
				collectionRecord.setCalculationDate(calculateDate);
				collectionRecord.setOperationDate(operationDate);
				collectionRecord.setCurrencyType(billingService.getCurrencyCalculation());
				collectionRecord.setCurrencyBilling(billingService.getCurrencyBilling());
				collectionRecord.setRecordState(ProcessedServiceType.PROCESADO.getCode());
				collectionRecord.setIndBilled(PropertiesConstants.NOT_BILLED);/** default , not  billed**/ 	
				addEntityCollection(collectionRecord, collectionRecordTo); /** CAN BE : HOLDER , PARTICIPANT OR ISSUER **/
											
				/**  aggregate new collection record to collectionRecordList*/
				collectionRecordList.add(collectionRecord);
			}
			
			
			businessLayerCommonFacade.addNumberRequestToCollection(collectionRecord, collectionRecordTo);
			businessLayerCommonFacade.addMnemonicHolderToCollection(collectionRecord, collectionRecordTo);
			
			/** ADD ENTITI'S AMOUNT OF EACH DETAIL  **/
			if (serviceRate.getRateType().equals(RateType.TIPO_MOVIMIENTO_PORCENTUAL.getCode())){
				  
				addAmountForServiceRatePercentage(collectionRecord, newCollectionRecordDetailList , billingService, parameters);
				  
			}else if (serviceRate.getRateType().equals(RateType.TIPO_MOVIMIENTO_FIJO.getCode())){	
			    	
			    addAmountForServiceRateFixed(collectionRecord, newCollectionRecordDetailList , billingService);
			    	
			}
			
			/**STAGGERED WITH MOVEMENT**/
			if ( (serviceRate.getRateType().equals(RateType.ESCALONADO_FIJO.getCode()) ||
		    		serviceRate.getRateType().equals(RateType.ESCALONADO_MIXTO.getCode()))    &&
		    		serviceRate.getIndScaleMovement().equals(PropertiesConstants.YES_VALUE_INTEGER)){
				
		    	addAmountForServiceRateFixed(collectionRecord, newCollectionRecordDetailList , billingService);
		    	
			}else if (serviceRate.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode()) &&
					serviceRate.getIndScaleMovement().equals(PropertiesConstants.YES_VALUE_INTEGER)){

		    	addAmountForServiceRatePercentage(collectionRecord, newCollectionRecordDetailList , billingService, parameters);
		    	
			}
		

			if(!BillingUtils.isBaseSettlementTrading(billingService.getBaseCollection())){
				/**
				 * APPLLIED RATE MINIMUM
				 */
				BigDecimal amountNew=businessLayerCommonFacade.appliedMinimumAmountCalculation(serviceRate, 
								collectionRecord.getCollectionAmount(), parameters);
				businessLayerCommonFacade.collectionToMinimumBilling(collectionRecord,parameters);
				collectionRecord.setCollectionAmount(amountNew);
			}
			

			
			/** ONLY IF SERVICE APPLY   TAXED **/
			if (billingService.getTaxApplied().equals(PropertiesConstants.YES_APPLY_TAXED)){
				
				ParameterTable parameterTableTax= billingServiceMgmtServiceFacade.getParameterTableById(PropertiesConstants.TAX_PK);				
				Double taxed=(parameterTableTax!=null)?parameterTableTax.getDecimalAmount1():new Double(GeneralConstants.ZERO_VALUE_STRING);
				
				BigDecimal taxedAmount=BillingUtils.multiplyDecimals(collectionRecord.getCollectionAmount(), new BigDecimal(taxed.toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
				collectionRecord.setTaxApplied(taxedAmount);
						
				BigDecimal grossAmount = BillingUtils.addTwoDecimal(collectionRecord.getCollectionAmount(), taxedAmount, GeneralConstants.ROUNDING_DIGIT_NUMBER);
				
				/** SETTING FINALLY COLLECTION AMOUNT **/
				collectionRecord.setGrossAmount(grossAmount);
				
			}else{
				collectionRecord.setTaxApplied(new BigDecimal(GeneralConstants.ZERO_VALUE_STRING));
				collectionRecord.setGrossAmount(collectionRecord.getCollectionAmount());
			}

			/**
			 * Save To Billing Calculation 
			 */
			businessLayerCommonFacade.billingChargesToConvertCurrency(collectionRecord, parameters);
		
		}
		
		return collectionRecordList;
	}
	

	public List<CollectionRecord>  generateCollectionRecordSafi(Map<String, Object> parameters, List<CollectionRecord> collectionRecordList)  throws ServiceException{
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		ServiceRate serviceRate = (ServiceRate) parameters.get(GeneralConstants.SERVICE_RATE);
		Date calculateDate= (Date) parameters.get(GeneralConstants.CALCULATION_DATE);
		Date operationDate= (Date) parameters.get(GeneralConstants.OPERATION_DATE);

		/*** GETTING NEW LIST COLLECTION RECORD WITH NEW AMOUNT FOR RATE , IF THERE IS AN ENTITY ON LIST COLLECTION RECORD  ONLY ADDS BY AMOUNT RATE,  
		     IF NOT CREATE A NEW COLLECTION RECORD FOR A ENTITY COLLECTION AND AFTER ADD A  LIST OF COLLECTION RECORDS  **/			
		List<CollectionRecordTo> collectionRecordToList = (List<CollectionRecordTo>) parameters.get(GeneralConstants.COLLECTION_RECORD_TO_LIST);
		

		
		for (CollectionRecordTo collectionRecordTo : collectionRecordToList) {
						
			/**getting entity for add news amount **/
			Object objEntity = collectionRecordTo.getEntityCollection(collectionRecordTo.getEntityCollection());
			List<CollectionRecordTo> listForSafi=businessLayerCommonFacade.generateListForSafi(collectionRecordTo);
			
			for (CollectionRecordTo collectionSafi : listForSafi) {
				List<CollectionRecordDetail> newCollectionRecordDetailList=loadCollectionRecordDetail(collectionSafi.getCollectionRecordDetailToList(), serviceRate,parameters);
				CollectionRecord collectionRecord=findEntityAtListSafi(collectionRecordList, objEntity, collectionSafi.getEntityCollection(), collectionSafi.getMnemonicHolder());
				
				/** THE GOAL IS  CREATE A RECORD FOR EACH ENTITY ONLY IF DON'T EXIST**/
				if (collectionRecord==null){
					collectionRecord = new CollectionRecord();
					collectionRecord.setEntityCollectionId(billingService.getEntityCollection());						
					collectionRecord.setGrossAmount(new BigDecimal(0));
					collectionRecord.setCollectionAmount(new BigDecimal(0));
					collectionRecord.setCalculationDate(calculateDate);
					collectionRecord.setOperationDate(operationDate);
					collectionRecord.setCurrencyType(billingService.getCurrencyCalculation());
					collectionRecord.setCurrencyBilling(billingService.getCurrencyBilling());
					collectionRecord.setRecordState(ProcessedServiceType.PROCESADO.getCode());
					collectionRecord.setIndBilled(PropertiesConstants.NOT_BILLED);/** default , not  billed**/
					collectionRecord.setMnemonicConcatenated(collectionSafi.getMnemonicHolder());
					addEntityCollection(collectionRecord, collectionSafi); /** CAN BE : HOLDER , PARTICIPANT OR ISSUER **/
												
					/**  aggregate new collection record to collectionRecordList*/
					collectionRecordList.add(collectionRecord);
				}
				
				
				businessLayerCommonFacade.addNumberRequestToCollection(collectionRecord, collectionSafi);
				
				
				/** ADD ENTITI'S AMOUNT OF EACH DETAIL  **/
				if (serviceRate.getRateType().equals(RateType.TIPO_MOVIMIENTO_PORCENTUAL.getCode())){
					  
					addAmountForServiceRatePercentage(collectionRecord, newCollectionRecordDetailList , billingService, parameters);
					  
				}else if (serviceRate.getRateType().equals(RateType.TIPO_MOVIMIENTO_FIJO.getCode())){	
				    	
				    addAmountForServiceRateFixed(collectionRecord, newCollectionRecordDetailList , billingService);
				    	
				}
				
				/**STAGGERED WITH MOVEMENT**/
				if ( (serviceRate.getRateType().equals(RateType.ESCALONADO_FIJO.getCode()) ||
			    		serviceRate.getRateType().equals(RateType.ESCALONADO_MIXTO.getCode()))    &&
			    		serviceRate.getIndScaleMovement().equals(PropertiesConstants.YES_VALUE_INTEGER)){
					
			    	addAmountForServiceRateFixed(collectionRecord, newCollectionRecordDetailList , billingService);
			    	
				}else if (serviceRate.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode()) &&
						serviceRate.getIndScaleMovement().equals(PropertiesConstants.YES_VALUE_INTEGER)){

			    	addAmountForServiceRatePercentage(collectionRecord, newCollectionRecordDetailList , billingService, parameters);
			    	
				}
			

				if(!BillingUtils.isBaseSettlementTrading(billingService.getBaseCollection())){
					/**
					 * APPLLIED RATE MINIMUM
					 */
					BigDecimal amountNew=businessLayerCommonFacade.appliedMinimumAmountCalculation(serviceRate, 
									collectionRecord.getCollectionAmount(), parameters);
					businessLayerCommonFacade.collectionToMinimumBilling(collectionRecord,parameters);
					collectionRecord.setCollectionAmount(amountNew);
				}
				

				
				/** ONLY IF SERVICE APPLY   TAXED **/
				if (billingService.getTaxApplied().equals(PropertiesConstants.YES_APPLY_TAXED)){
					
					ParameterTable parameterTableTax= billingServiceMgmtServiceFacade.getParameterTableById(PropertiesConstants.TAX_PK);				
					Double taxed=(parameterTableTax!=null)?parameterTableTax.getDecimalAmount1():new Double(GeneralConstants.ZERO_VALUE_STRING);
					
					BigDecimal taxedAmount=BillingUtils.multiplyDecimals(collectionRecord.getCollectionAmount(), new BigDecimal(taxed.toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
					collectionRecord.setTaxApplied(taxedAmount);
							
					BigDecimal grossAmount = BillingUtils.addTwoDecimal(collectionRecord.getCollectionAmount(), taxedAmount, GeneralConstants.ROUNDING_DIGIT_NUMBER);
					
					/** SETTING FINALLY COLLECTION AMOUNT **/
					collectionRecord.setGrossAmount(grossAmount);
					
				}else{
					collectionRecord.setTaxApplied(new BigDecimal(GeneralConstants.ZERO_VALUE_STRING));
					collectionRecord.setGrossAmount(collectionRecord.getCollectionAmount());
				}

				/**
				 * Save To Billing Calculation 
				 */
				businessLayerCommonFacade.billingChargesToConvertCurrency(collectionRecord, parameters);
			}
			
		
		}
		
		return collectionRecordList;
	}
	

	/**
	 * For movements.
	 *
	 * @param parameters the parameters
	 * @return the ting data for calculating by quantity
	 */
	public List<CollectionRecordTo> gettingDataForCalculatingByQuantity(Map<String, Object> parameters){
	
		List<CollectionRecordTo> collectionRecordToList=null;

		Date calculateDate= (Date) parameters.get(GeneralConstants.CALCULATION_DATE);

		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		
		List<Object> dataMaps=new ArrayList<>();
		try {
			

				

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		if (Validations.validateListIsNotNullAndNotEmpty(dataMaps)){
			collectionRecordToList = new ArrayList<CollectionRecordTo>();
				
			
				for (Object object : dataMaps) {
					
				
				
						Object    entity         = ((Object[])object)[0];
						String    security       = (String)((Object[])object)[1];
						Date       movementDate  =  (Date)(( (Object[])object )[2]);
						Long   operationNumber   = Long.parseLong(((BigDecimal)((Object[])object)[3]).toString());
						Long 	typeMovement	 = Long.parseLong(((BigDecimal)((Object[])object)[4]).toString());;
						Integer    numMovement          	= Integer.parseInt((((Object[])object)[5]).toString());
						
//						Long      typeMovement   = Long.parseLong(((BigDecimal)((Object[])object)[2]).toString());
//						Date       operationDate   		 =  (Date)(( (Object[])object )[5]);
//						Long       operationNumber       =  Long.parseLong(((BigDecimal)((Object[])object)[6]).toString());  
//						Long       idSourceParticipant   =  Long.parseLong(((BigDecimal)((Object[])object)[7]).toString());
//						Long       idTargetParticipant   =  Long.parseLong(((BigDecimal)((Object[])object)[8]).toString()); 
//						BigDecimal operationPrice 		 = new BigDecimal((((Object[])object)[9]).toString());	   	 	   
//						Long       holderAccount	     =  Long.parseLong(((BigDecimal)((Object[])object)[10]).toString());
//						Long       idHolderPk	     	 =  Long.parseLong(((BigDecimal)((Object[])object)[11]).toString()); 
//						Long	   movementQuantity      =  Long.parseLong(((BigDecimal)((Object[])object)[12]).toString());
						
						CollectionRecordDetailTo collectionRecordDetailTo = new CollectionRecordDetailTo();
						collectionRecordDetailTo.setIdSecurityCodeFk(security);
						collectionRecordDetailTo.setIdMovementTypePk(typeMovement);
						collectionRecordDetailTo.setMovementCount(numMovement);
						collectionRecordDetailTo.setCalculationDate(calculateDate);
						collectionRecordDetailTo.setOperationNumber(operationNumber);
						collectionRecordDetailTo.setMovementDate(movementDate);
//						collectionRecordDetailTo.setOperationDate(operationDate);
//						collectionRecordDetailTo.setOperationNumber(operationNumber);
//						collectionRecordDetailTo.setIdSourceParticipant(idSourceParticipant);
//						collectionRecordDetailTo.setIdTargetParticipant(idTargetParticipant);
//						collectionRecordDetailTo.setOperationPrice(operationPrice);
//						collectionRecordDetailTo.setHolderAccount(holderAccount); 
//						collectionRecordDetailTo.setIdHolderPk(idHolderPk);
//						collectionRecordDetailTo.setMovementQuantity(movementQuantity);					
							

					//if !existe entity en collectionRecordToList then agregar y crear un detalle
					// else agregar detalle	
						CollectionRecordTo collectionRecordTo = findEntityAtListTemporalTo(collectionRecordToList, entity, billingService);
						if (collectionRecordTo==null){
							collectionRecordTo = new CollectionRecordTo();
							if(baseCollectionMgmtServiceBean.isPaymentByparticipant(billingService)){
								entity = baseCollectionMgmtServiceBean.getIdParticipantPkByIdIssuerPk(entity.toString());
								collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
							}
							else{
								collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
							}
							
							List<CollectionRecordDetailTo> collectionRecordDetailToList = new  ArrayList<CollectionRecordDetailTo>();
							collectionRecordDetailToList.add(collectionRecordDetailTo);	
							collectionRecordToList.add(collectionRecordTo);	
							collectionRecordTo.setCollectionRecordDetailToList(collectionRecordDetailToList);
						}else{
							collectionRecordTo.getCollectionRecordDetailToList().add(collectionRecordDetailTo);
						}
						
						
				 }
				
		}
		
		
		return collectionRecordToList;
	}
	
	

	/**
	 * Aplicarle el escalon despues de obtener los movimientos.
	 *
	 * @param parameters the parameters
	 * @return the ting data for calculating estaggered fixed
	 * @throws ServiceException the service exception
	 */
	public List<CollectionRecordTo> gettingDataForCalculatingEstaggeredFixed (Map<String, Object> parameters) throws ServiceException {
		
		List<CollectionRecordTo> collectionRecordToList=null;
	
		/**Aqui hacer la verificacion**/

		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);	
		/** getting data with or without exceptions*/
		if (billingService.getBaseCollection().equals(BaseCollectionType.TRANSFERS_SECURITIES_SIRTEX_FIXED_INCOME.getCode())||
				billingService.getBaseCollection().equals(BaseCollectionType.TRANSFERS_SECURITIES_SIRTEX_VARIABLE_INCOME.getCode())||
				billingService.getBaseCollection().equals(BaseCollectionType.INDIRECT_PARTICIPANT.getCode())||
				billingService.getBaseCollection().equals(BaseCollectionType.SETTLEMENT_OTC_TRANSACTIONS_VARIABLE_INCOME.getCode())||
				billingService.getBaseCollection().equals(BaseCollectionType.RECORD_SECURITIES_DEMATERIALIZED_FIXED_INCOME.getCode())||
				billingService.getBaseCollection().equals(BaseCollectionType.RECORD_SECURITIES_DEMATERIALIZED_VARIABLE_INCOME.getCode())||
				billingService.getBaseCollection().equals(BaseCollectionType.REQUEST_CHANGE_OWNERSHIP_FIXED_INCOME.getCode())||
				billingService.getBaseCollection().equals(BaseCollectionType.REQUEST_CHANGE_OWNERSHIP_VARIABLE_INCOME.getCode())||
				billingService.getBaseCollection().equals(BaseCollectionType.PAYING_AGENT.getCode())||
				billingService.getBaseCollection().equals(BaseCollectionType.REGISTRATION_PRECAUTIONARY_MEASURES.getCode())||
				billingService.getBaseCollection().equals(BaseCollectionType.LIFTING_PRECAUTIONARY_MEASURES.getCode())||
				billingService.getBaseCollection().equals(BaseCollectionType.TRANSACTION_CHARGE.getCode())||
				billingService.getBaseCollection().equals(BaseCollectionType.REMATERIALIZATION_OR_REVERSAL_ACCOUNT_ENTRIES_VARIABLE_INCOME.getCode())
				){
			/**
			 * 
			 * OBTENGO LOS COBROS PARA LOS MOVIEMIENTOS
			 */
			collectionRecordToList=	gettingDataForCalculatingByMovement(parameters);
			parameters.put(GeneralConstants.COLLECTION_RECORD_MOVEMENT, collectionRecordToList);
			/***
			 * Aplico Los escalones
			 */
			collectionRecordToList = baseCollectionMgmtServiceFacade.securitiesTransferStaggeredAccumulative(parameters);
		}
				
			
		return collectionRecordToList;
	}
	
	/**
	 * Calculation For Rates With Movements .
	 *
	 * @param parameters the parameters
	 * @return the ting data for calculating by movement
	 * @throws ServiceException the service exception
	 */
	public List<CollectionRecordTo> gettingDataForCalculatingByMovement(Map<String, Object> parameters) throws ServiceException{

		List<CollectionRecordTo> collectionRecordToList=null;

		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);	
		Integer baseCollection=billingService.getBaseCollection();

		/** getting data with or without exceptions*/
				if (
						BaseCollectionType.SETTLEMENT_SECURITIES_TRANSACTIONS_FIXED_INCOME.getCode().equals(baseCollection)||
						BaseCollectionType.SETTLEMENT_SECURITIES_TRANSACTIONS_VARIABLE_INCOME.getCode().equals(baseCollection)||
						BaseCollectionType.TRANSFERS_SECURITIES_SIRTEX_FIXED_INCOME.getCode().equals(baseCollection)||
						BaseCollectionType.TRANSFERS_SECURITIES_SIRTEX_VARIABLE_INCOME.getCode().equals(baseCollection)||
						BaseCollectionType.INDIRECT_PARTICIPANT.getCode().equals(baseCollection)||
						BaseCollectionType.SETTLEMENT_OTC_TRANSACTIONS_VARIABLE_INCOME.getCode().equals(baseCollection)||
						BaseCollectionType.RECORD_SECURITIES_DEMATERIALIZED_FIXED_INCOME.getCode().equals(baseCollection)||
						BaseCollectionType.RECORD_SECURITIES_DEMATERIALIZED_VARIABLE_INCOME.getCode().equals(baseCollection)||
						BaseCollectionType.TRANSACTION_CHARGE.getCode().equals(baseCollection)||
						BaseCollectionType.REMATERIALIZATION_OR_REVERSAL_ACCOUNT_ENTRIES_VARIABLE_INCOME.getCode().equals(baseCollection)
						){
					/*** GETTING COLLECTION FOR MOVEMENT */
					collectionRecordToList=	gettingDataForCalculatingBySettlement(parameters);					
				}else if(
						BaseCollectionType.REQUEST_CHANGE_OWNERSHIP_FIXED_INCOME.getCode().equals(baseCollection)||
						BaseCollectionType.REQUEST_CHANGE_OWNERSHIP_VARIABLE_INCOME.getCode().equals(baseCollection)||
						BaseCollectionType.PAYING_AGENT.getCode().equals(baseCollection)||
						BaseCollectionType.REGISTRATION_PRECAUTIONARY_MEASURES.getCode().equals(baseCollection)||
						BaseCollectionType.LIFTING_PRECAUTIONARY_MEASURES.getCode().equals(baseCollection)
						){
					
					collectionRecordToList= gettingDataForCalculatingByCustody(parameters);
				}

		return collectionRecordToList;
	} 
	
	/**
	 * Gets the ting data for calculating by custody.
	 *
	 * @param parameters the parameters
	 * @return the ting data for calculating by custody
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<CollectionRecordTo> gettingDataForCalculatingByCustody(Map<String, Object> parameters) throws ServiceException{
		
		List<CollectionRecordTo> collectionRecordToList=null;

		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		Integer baseCollection=billingService.getBaseCollection();
		
		
		List<Object> dataMaps=null;
		
		/**11  SOLICITUDES_CAMBIO_TITULARIDAD */
		if (baseCollection.equals(BaseCollectionType.REQUEST_CHANGE_OWNERSHIP_FIXED_INCOME.getCode()) ||
				baseCollection.equals(BaseCollectionType.REQUEST_CHANGE_OWNERSHIP_VARIABLE_INCOME.getCode())){
			/** CONSIDERED THE NEGOTIATION MECHANISM**/
			
			
			ParameterTable parameterTable	= billingServiceMgmtServiceFacade.getParameterTableById(baseCollection);
			Integer idInstrumentType = parameterTable.getShortInteger();
//			Long idNegotiationMechanism = parameterTable.getLongInteger();
			 
			
			dataMaps = baseCollectionMgmtServiceFacade.findMovementSecuritiesChangeOwnerShip(parameters);
			
		}

		/**16 SERVICIO_AGENTE_PAGADOR **/
		else if (baseCollection.equals(BaseCollectionType.PAYING_AGENT.getCode()) ){
			/** CONSIDERED THE NEGOTIATION MECHANISM**/
			
			
//			ParameterTable parameterTable	= billingServiceMgmtServiceFacade.getParameterTableById(baseCollection);
//			Integer idInstrumentType = parameterTable.getShortInteger();
//			Long idNegotiationMechanism = parameterTable.getLongInteger();
//			dataMaps = baseCollectionMgmtServiceFacade.findMovementSecuritiesTransferSirtex(parameters ,idInstrumentType, idNegotiationMechanism);

		}
		/**17.1  INSCRIPCION_MEDIDAS_PRECAUTORIAS **/
		else if (baseCollection.equals(BaseCollectionType.REGISTRATION_PRECAUTIONARY_MEASURES.getCode()) ){
			/** CONSIDERED THE NEGOTIATION MECHANISM**/
			
			
			ParameterTable parameterTable	= billingServiceMgmtServiceFacade.getParameterTableById(baseCollection);
			Integer idInstrumentType = parameterTable.getShortInteger();
			Long idNegotiationMechanism = parameterTable.getLongInteger();
			dataMaps = baseCollectionMgmtServiceFacade.findMovementPawnBlockAffectation(parameters ,idInstrumentType, idNegotiationMechanism);
			
		}
		/**17.2  LEVANTAMIENTO_MEDIDAS_PRECAUTORIAS **/
		else if (baseCollection.equals(BaseCollectionType.LIFTING_PRECAUTIONARY_MEASURES.getCode()) ){
			/** CONSIDERED THE NEGOTIATION MECHANISM**/
			
			
			ParameterTable parameterTable	= billingServiceMgmtServiceFacade.getParameterTableById(baseCollection);
			Integer idInstrumentType = parameterTable.getShortInteger();
			Long idNegotiationMechanism = parameterTable.getLongInteger();
			dataMaps = baseCollectionMgmtServiceFacade.findMovementReversalAffectation(parameters ,idInstrumentType, idNegotiationMechanism);
			
		}
		
		BigDecimal buyPrice = BigDecimal.ZERO;
		
		if (Validations.validateListIsNotNullAndNotEmpty(dataMaps)){
			collectionRecordToList = new ArrayList<CollectionRecordTo>();
			String requestLast="";
			Long operationNumberLast=null;	
				for (Object object : dataMaps) {
					Long numberRequest=null;
					String issuance=null;
	
						Object     entity         		= ((Object[])object)[0];
						String     security       		= (String)((Object[])object)[1].toString();
						Long       typeMovement   		= Long.parseLong((((Object[])object)[2]).toString());
						Integer    currency          	= Integer.parseInt((((Object[])object)[3]).toString());
						Date       movementDate   		= (Date)(( (Object[])object )[4]);					
						BigDecimal operationPrice 		= new BigDecimal((((Object[])object)[5]).toString());
						Long	   movementQuantity  	= Long.parseLong((((Object[])object)[6]).toString());
						Date operationDate		 		= (Date)(( (Object[])object )[7]);
						Long operationNumber 	 		= Long.parseLong((((Object[])object)[8]).toString());
						Long idSourceParticipant 		= ((Object[])object)[9]==null ? null :Long.parseLong((((Object[])object)[9]).toString());
						Long idTargetParticipant 		= ((Object[])object)[10]==null ? null :Long.parseLong((((Object[])object)[10]).toString());
						Long holderAccount 	     		= Long.parseLong((((Object[])object)[11]).toString());
						Long idHolderPk 		 		= Long.parseLong((((Object[])object)[12]).toString());
						
						Integer    	securityClass=null ;
						Integer    	modality=null ;
						String 		bulletSequence=null;
						String 		request=null;
						String 		mnemonicHolder		=null;
						Integer 	economicActivity	=null;
						Integer 	portfolio			=null;


						if(BillingUtils.isRequestAffectations(baseCollection)){
							numberRequest		= Long.parseLong((((Object[])object)[13]).toString());
							issuance       		= (String)((Object[])object)[14].toString();
							request		= numberRequest.toString().concat(issuance);
							securityClass=Integer.parseInt((((Object[])object)[15]).toString());
							mnemonicHolder		= ((Object[])object)[16]!=null?((Object[])object)[16].toString():null;
							economicActivity	= (BigDecimal)((Object[])object)[17]!=null?Integer.parseInt(((BigDecimal)((Object[])object)[17]).toString()):null;
							portfolio			= (BigDecimal)((Object[])object)[18]!=null?Integer.parseInt(((BigDecimal)((Object[])object)[18]).toString()):null;

						}
						if(BillingUtils.isRegisterOwnerChange(baseCollection)) {
							mnemonicHolder		= ((Object[])object)[13]!=null?((Object[])object)[13].toString():null;
							economicActivity	= (BigDecimal)((Object[])object)[14]!=null?Integer.parseInt(((BigDecimal)((Object[])object)[14]).toString()):null;
							portfolio			= (BigDecimal)((Object[])object)[15]!=null?Integer.parseInt(((BigDecimal)((Object[])object)[15]).toString()):null;
						}
						/**
						 * Monto Negociado Original
						 * Valor Nominal * Cantidad de Valores
						 * 			VN*Q
						 * 
						 * **/
						BigDecimal negotiatedAmountOrigin=BillingUtils.multiplyDecimalLong(operationPrice, movementQuantity, GeneralConstants.ROUNDING_DIGIT_NUMBER);

						/***Este PriceByQuantity cambiar a la moneda de Calculo*/
						buyPrice=businessLayerCommonFacade.processExchange(parameters,currency,
								billingService.getCurrencyCalculation(),DailyExchangeRoleType.BUY.getCode());
						
						/***ALL EXCHANGE RATE  -----> CONVERSION TO CALCULATION CURRENCY  */
						BigDecimal priceByQuantity =BillingUtils.multiplyDecimals(buyPrice,negotiatedAmountOrigin , GeneralConstants.ROUNDING_DIGIT_NUMBER);
						
						
						CollectionRecordDetailTo collectionRecordDetailTo = new CollectionRecordDetailTo();
						collectionRecordDetailTo.setIdSecurityCodeFk(security);
						collectionRecordDetailTo.setIdMovementTypePk(typeMovement);
						collectionRecordDetailTo.setCurrencyRate(currency);
						collectionRecordDetailTo.setCalculationDate(movementDate);
						collectionRecordDetailTo.setOperationPrice(operationPrice);
						collectionRecordDetailTo.setMovementQuantity(movementQuantity);
						collectionRecordDetailTo.setOperationDate(operationDate);
						collectionRecordDetailTo.setIdSourceParticipant(idSourceParticipant);
						collectionRecordDetailTo.setIdTargetParticipant(idTargetParticipant);
						collectionRecordDetailTo.setHolderAccount(holderAccount);
						collectionRecordDetailTo.setIdHolderPk(idHolderPk);
						collectionRecordDetailTo.setPriceByQuantity(priceByQuantity);//P*Q a moneda de Calculo
						collectionRecordDetailTo.setNegotiatedAmountOrigin(negotiatedAmountOrigin);//Monto negociado Original
						collectionRecordDetailTo.setNegotiatedAmount(priceByQuantity);
						collectionRecordDetailTo.setCurrencyOrigin(currency);
						collectionRecordDetailTo.setSecurityClass(securityClass);
						collectionRecordDetailTo.setModality(modality);
						collectionRecordDetailTo.setBallotSequential(bulletSequence);
						collectionRecordDetailTo.setMnemonicHolder(mnemonicHolder);
						collectionRecordDetailTo.setEconomicActivityParticipant(economicActivity);
						collectionRecordDetailTo.setPortfolio(portfolio);
												
					//if !existe entity en collectionRecordToList then agregar y crear un detalle
					// else agregar detalle	findAffectationsRequest
						CollectionRecordTo collectionRecordTo =null;
						if(BillingUtils.isRequestAffectations(baseCollection)){
							collectionRecordDetailTo.setOperationNumber(numberRequest);
							collectionRecordDetailTo.setTmpRequest(numberRequest.toString());
							if(findAffectationsRequest(request, requestLast)){
								/* Solo para Inscripcion de medidas precautorias se calcula
								 * en base a la [Solicitud+Emision] */
								collectionRecordTo= findEntityAtListTemporalToByRequest(collectionRecordToList, entity, billingService,numberRequest.toString());
							}
							requestLast=request;
						}
						else if(BillingUtils.isRegisterOwnerChange(baseCollection)){
							collectionRecordDetailTo.setOperationNumber(operationNumber);
							collectionRecordDetailTo.setTmpRequest(operationNumber.toString());
							if(findOperation(operationNumber, operationNumberLast)){
								collectionRecordTo= findEntityAtListTemporalToByRequest(collectionRecordToList, entity, billingService,operationNumber.toString());
							}
							operationNumberLast=operationNumber;
						}
						else{
							collectionRecordTo= findEntityAtListTemporalTo(collectionRecordToList, entity, billingService);
						}
						
						
						
						if (collectionRecordTo==null){
							collectionRecordTo = new CollectionRecordTo();
							if(baseCollectionMgmtServiceBean.isPaymentByparticipant(billingService)){
								entity = baseCollectionMgmtServiceBean.getIdParticipantPkByIdIssuerPk(entity.toString());
								collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
							}
							else{
								collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
							}
							
							List<CollectionRecordDetailTo> collectionRecordDetailToList = new  ArrayList<CollectionRecordDetailTo>();
							collectionRecordDetailToList.add(collectionRecordDetailTo);	
							collectionRecordToList.add(collectionRecordTo);	
							collectionRecordTo.setCollectionRecordDetailToList(collectionRecordDetailToList);
						}else{
							collectionRecordTo.getCollectionRecordDetailToList().add(collectionRecordDetailTo);
						}
						
						
				 }
				
		}
		
		
		return collectionRecordToList;
	}
	
	/**
	 * *
	 * TRANSFERENCIAS_VALORES_SIRTEX_R_FIJA
	 * TRANSFERENCIAS_VALORES_SIRTEX_R_VARIABLE.
	 *
	 * @param parameters the parameters
	 * @return the ting data for calculating by settlement
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<CollectionRecordTo> gettingDataForCalculatingBySettlement(Map<String, Object> parameters) throws ServiceException{
		
		List<CollectionRecordTo> collectionRecordToList=null;

		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		Integer baseCollection=billingService.getBaseCollection();

		
		
		List<Object> dataMaps=null;
		try {
			
			/**
			 * Si existe Fuente, se procesa el archivo XML
			 */
			if(Validations.validateIsNotNullAndPositive(billingService.getIndSourceBaseCollection())){
				
				
				return businessLayerCommonFacade.executeFileBaseCollection(new BillingServiceTo(billingService),parameters);
				
			}else{
				// getting data with or without exceptions

				/**4 **/
				if (baseCollection.equals(BaseCollectionType.SETTLEMENT_SECURITIES_TRANSACTIONS_FIXED_INCOME.getCode()) ||
						baseCollection.equals(BaseCollectionType.SETTLEMENT_SECURITIES_TRANSACTIONS_VARIABLE_INCOME.getCode()) ){
					/** CONSIDERED THE NEGOTIATION MECHANISM**/
					
					
					ParameterTable parameterTable	= billingServiceMgmtServiceFacade.getParameterTableById(baseCollection);
					Integer idInstrumentType = parameterTable.getShortInteger();
					Long idNegotiationMechanism = parameterTable.getLongInteger();
					
					dataMaps = baseCollectionMgmtServiceFacade.findMovementSettlementWithMechanismByEntityCollection(parameters ,idInstrumentType, idNegotiationMechanism);

				}

				
				/** 9 OTC  MECHANISM 3*/
				else if (baseCollection.equals(BaseCollectionType.INDIRECT_PARTICIPANT.getCode()) ||
						baseCollection.equals(BaseCollectionType.SETTLEMENT_OTC_TRANSACTIONS_VARIABLE_INCOME.getCode()) ){
					/** CONSIDERED THE NEGOTIATION MECHANISM**/
					
					
					ParameterTable parameterTable	= billingServiceMgmtServiceFacade.getParameterTableById(baseCollection);
					Integer idInstrumentType = parameterTable.getShortInteger();
					Long idNegotiationMechanism = parameterTable.getLongInteger();
					 
					
					dataMaps = baseCollectionMgmtServiceFacade.findMovementPrivateSecuritiesTransferOperationsOtc(parameters ,idInstrumentType, idNegotiationMechanism);

				}

				
				
				/**18*/
				else if (baseCollection.equals(BaseCollectionType.TRANSACTION_CHARGE.getCode()) ||
						baseCollection.equals(BaseCollectionType.REMATERIALIZATION_OR_REVERSAL_ACCOUNT_ENTRIES_VARIABLE_INCOME.getCode()) ){
					/** CONSIDERED THE NEGOTIATION MECHANISM**/
					
					
					ParameterTable parameterTable	= billingServiceMgmtServiceFacade.getParameterTableById(baseCollection);
					Integer idInstrumentType = parameterTable.getShortInteger();
					Long idNegotiationMechanism = parameterTable.getLongInteger();
					dataMaps = baseCollectionMgmtServiceFacade.findMovementReversionAnnotationIntoAccount(parameters ,idInstrumentType, idNegotiationMechanism);

				}
			}

						
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
 
		
		BigDecimal buyPrice = BigDecimal.ZERO;
		
		if (Validations.validateListIsNotNullAndNotEmpty(dataMaps)){
			collectionRecordToList = new ArrayList<CollectionRecordTo>();
				
				
				for (Object object : dataMaps) {
	
						Object     entity         		= ((Object[])object)[0];
						String     security       		= (String)((Object[])object)[1].toString();
						Long       typeMovement   		= Long.parseLong((((Object[])object)[2]).toString());
						Integer    currency          	= Integer.parseInt((((Object[])object)[3]).toString());
						Date       movementDate   		= (Date)(( (Object[])object )[4]);					
						BigDecimal operationPrice 		= new BigDecimal((((Object[])object)[5]).toString());
						Long	   movementQuantity  	= Long.parseLong((((Object[])object)[6]).toString());
						Date operationDate		 		= (Date)(( (Object[])object )[7]);
						Long operationNumber 	 		= Long.parseLong((((Object[])object)[8]).toString());
						Long idSourceParticipant 		= ((Object[])object)[9]==null ? null :Long.parseLong((((Object[])object)[9]).toString());
						Long idTargetParticipant 		= ((Object[])object)[10]==null ? null :Long.parseLong((((Object[])object)[10]).toString());
						Long holderAccount 	     		= Long.parseLong((((Object[])object)[11]).toString());
						Long idHolderPk 		 		= Long.parseLong((((Object[])object)[12]).toString());
						
						Integer    securityClass=null ;
						Integer    modality=null ;
						String bulletSequence=null;
						Integer role=null;
						String 	mnemonicHolder		= null;
						Integer 	economicActivity	= null;
						Integer 	portfolio			= null;
						
						/**4 BURSATILES*/
						if(BillingUtils.isBaseSettlementTrading(baseCollection)){
							
							securityClass       = ((Object[])object)[13]==null ? null : Integer.parseInt((((Object[])object)[13]).toString());
							modality          	= ((Object[])object)[14]==null ? null :Integer.parseInt((((Object[])object)[14]).toString());
							bulletSequence		= ((Object[])object)[15]==null ? "" : (String)((Object[])object)[15].toString();
							role				= ((Object[])object)[16]==null ? null : Integer.parseInt((((Object[])object)[16]).toString());
							mnemonicHolder		= ((Object[])object)[17]!=null?((Object[])object)[17].toString():null;
							economicActivity	= (BigDecimal)((Object[])object)[18]!=null?Integer.parseInt(((BigDecimal)((Object[])object)[18]).toString()):null;
							portfolio			= (BigDecimal)((Object[])object)[19]!=null?Integer.parseInt(((BigDecimal)((Object[])object)[19]).toString()):null;

						}/**9 OTC*/
						else if(BillingUtils.isTransfersOtcTransactions(baseCollection)){
							securityClass        = ((Object[])object)[13]==null ? null : Integer.parseInt((((Object[])object)[13]).toString());
							modality          	= ((Object[])object)[14]==null ? null :Integer.parseInt((((Object[])object)[14]).toString());
							mnemonicHolder		= ((Object[])object)[15]!=null?((Object[])object)[15].toString():null;
							economicActivity	= (BigDecimal)((Object[])object)[16]!=null?Integer.parseInt(((BigDecimal)((Object[])object)[16]).toString()):null;
							portfolio			= (BigDecimal)((Object[])object)[17]!=null?Integer.parseInt(((BigDecimal)((Object[])object)[17]).toString()):null;

						}/** 18*/
						else if(BillingUtils.isRematerializationAnnotations(baseCollection)) {
							mnemonicHolder		= ((Object[])object)[14]!=null?((Object[])object)[14].toString():null;
							economicActivity	= (BigDecimal)((Object[])object)[15]!=null?Integer.parseInt(((BigDecimal)((Object[])object)[15]).toString()):null;
							portfolio			= (BigDecimal)((Object[])object)[16]!=null?Integer.parseInt(((BigDecimal)((Object[])object)[16]).toString()):null;
						}
						
						
						/**
						 * Monto Negociado Original
						 * Valor Nominal * Cantidad de Valores
						 * 			VN*Q
						 * 
						 * **/
						BigDecimal negotiatedAmountOrigin=BillingUtils.multiplyDecimalLong(operationPrice, movementQuantity, GeneralConstants.ROUNDING_DIGIT_NUMBER);

						/***Este PriceByQuantity cambiar a la moneda de Calculo*/
						buyPrice=businessLayerCommonFacade.processExchange(parameters,currency,
								billingService.getCurrencyCalculation(),DailyExchangeRoleType.BUY.getCode());
						
						/***ALL EXCHANGE RATE  -----> CONVERSION TO CALCULATION CURRENCY  */
						BigDecimal priceByQuantity =BillingUtils.multiplyDecimals(buyPrice,negotiatedAmountOrigin , GeneralConstants.ROUNDING_DIGIT_NUMBER);
						
						
						CollectionRecordDetailTo collectionRecordDetailTo = new CollectionRecordDetailTo();
						collectionRecordDetailTo.setIdSecurityCodeFk(security);
						collectionRecordDetailTo.setIdMovementTypePk(typeMovement);
						collectionRecordDetailTo.setCurrencyRate(currency);
						collectionRecordDetailTo.setCalculationDate(movementDate);
						
						if (baseCollection.equals(BaseCollectionType.INDIRECT_PARTICIPANT.getCode()) || //issue 224
								baseCollection.equals(BaseCollectionType.SETTLEMENT_OTC_TRANSACTIONS_VARIABLE_INCOME.getCode()) ){
							operationPrice = operationPrice.multiply(BigDecimal.valueOf(movementQuantity)) ;
						}
						
						collectionRecordDetailTo.setOperationPrice(operationPrice);
						collectionRecordDetailTo.setMovementQuantity(movementQuantity);
						collectionRecordDetailTo.setOperationDate(operationDate);
						collectionRecordDetailTo.setOperationNumber(operationNumber);
						collectionRecordDetailTo.setTmpRequest(operationNumber.toString());
						collectionRecordDetailTo.setIdSourceParticipant(idSourceParticipant);
						collectionRecordDetailTo.setIdTargetParticipant(idTargetParticipant);
						collectionRecordDetailTo.setHolderAccount(holderAccount);
						collectionRecordDetailTo.setIdHolderPk(idHolderPk);
						collectionRecordDetailTo.setPriceByQuantity(priceByQuantity);//P*Q a moneda de Calculo
						collectionRecordDetailTo.setNegotiatedAmountOrigin(negotiatedAmountOrigin);//Monto negociado Original
						collectionRecordDetailTo.setNegotiatedAmount(priceByQuantity);
						collectionRecordDetailTo.setCurrencyOrigin(currency);
						collectionRecordDetailTo.setSecurityClass(securityClass);
						collectionRecordDetailTo.setModality(modality);
						collectionRecordDetailTo.setBallotSequential(bulletSequence);
						collectionRecordDetailTo.setRole(role);
						collectionRecordDetailTo.setMnemonicHolder(mnemonicHolder);
						collectionRecordDetailTo.setEconomicActivityParticipant(economicActivity);
						collectionRecordDetailTo.setPortfolio(portfolio);
												
					//if !existe entity en collectionRecordToList then agregar y crear un detalle
					// else agregar detalle	
						CollectionRecordTo collectionRecordTo = findEntityAtListTemporalTo(collectionRecordToList, entity, billingService);
						if (collectionRecordTo==null){
							collectionRecordTo = new CollectionRecordTo();
							if(baseCollectionMgmtServiceBean.isPaymentByparticipant(billingService)){
								entity = baseCollectionMgmtServiceBean.getIdParticipantPkByIdIssuerPk(entity.toString());
								collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
							}
							else{
								collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
							}
							
							List<CollectionRecordDetailTo> collectionRecordDetailToList = new  ArrayList<CollectionRecordDetailTo>();
							collectionRecordDetailToList.add(collectionRecordDetailTo);	
							collectionRecordToList.add(collectionRecordTo);	
							collectionRecordTo.setCollectionRecordDetailToList(collectionRecordDetailToList);
						}else{
							collectionRecordTo.getCollectionRecordDetailToList().add(collectionRecordDetailTo);
						}
						
						
				 }
				
		}
		
		
		return collectionRecordToList;
	}
	
	/**
	 * *
	 * loadCollectionRecordDetail.
	 *
	 * @param collectionRecordDetailToList the collection record detail to list
	 * @param serviceRate the service rate
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	protected List<CollectionRecordDetail>  loadCollectionRecordDetail(List<CollectionRecordDetailTo> collectionRecordDetailToList,
											 ServiceRate serviceRate, Map<String, Object> parameters) throws ServiceException{
		
		List<CollectionRecordDetail> newCollectionRecordDetailList  = new ArrayList<CollectionRecordDetail>();
		
				CollectionRecordDetail newCollectionRecordDetail = null;
				for (CollectionRecordDetailTo collectionRecordDetailTo : collectionRecordDetailToList) {


					BigDecimal buyPrice;
						
			    	/** getting RateMovement*/
					RateMovement rateMovement = serviceRate.getRateMovement(collectionRecordDetailTo.getIdMovementTypePk());
					newCollectionRecordDetail = new CollectionRecordDetail();
					newCollectionRecordDetail.setIdSecurityCodeFk(collectionRecordDetailTo.getIdSecurityCodeFk());
					newCollectionRecordDetail.setIdMovementTypePk(collectionRecordDetailTo.getIdMovementTypePk());															
					newCollectionRecordDetail.setServiceRate(serviceRate);
					
					
					 Date movementDate 			= collectionRecordDetailTo.getMovementDate();
					 Date operationDate			= collectionRecordDetailTo.getOperationDate();
					 Long operationNumber 		= collectionRecordDetailTo.getOperationNumber();
					 Long idSourceParticipant 	= collectionRecordDetailTo.getIdSourceParticipant();
					 Long idTargetParticipant 	= collectionRecordDetailTo.getIdTargetParticipant();
					 BigDecimal operationPrice 	= collectionRecordDetailTo.getOperationPrice();
					 Long holderAccount 	  	= collectionRecordDetailTo.getHolderAccount();
					 Long idHolderPk 		  	= collectionRecordDetailTo.getIdHolderPk();
					 Long movementQuantity		= collectionRecordDetailTo.getMovementQuantity();
					 Integer securityClass		= collectionRecordDetailTo.getSecurityClass();
					 Integer modality			= collectionRecordDetailTo.getModality();
					 String ballotSequence		= collectionRecordDetailTo.getBallotSequential();
					 BigDecimal negotiatedAmountOrigin 	= collectionRecordDetailTo.getNegotiatedAmountOrigin();
					 BigDecimal negotiatedAmount 	= collectionRecordDetailTo.getNegotiatedAmount();
						 
					if (serviceRate.getRateType().equals(RateType.TIPO_MOVIMIENTO_PORCENTUAL.getCode())){
				    	
			
						/** 1. calculate for number movements**/
						BigDecimal grossAmount 		       			=  new BigDecimal(0.0);
						BigDecimal percentAmount 		       		=  new BigDecimal(0.0);
						BigDecimal ratePercent						=  rateMovement.getRatePercent();
						/** calculate with amount */
						
						/***Este PriceByQuantity cambiar a la moneda de Calculo*/
						buyPrice=businessLayerCommonFacade.processExchange(parameters,
									collectionRecordDetailTo.getCurrencyRate(),
									serviceRate.getCurrencyRate(),DailyExchangeRoleType.BUY.getCode());
						
						/***ALL EXCHANGE RATE  -----> CONVERSION TO CALCULATION CURRENCY  */
						
						grossAmount= BillingUtils.multiplyDecimalLong(operationPrice, movementQuantity, GeneralConstants.ROUNDING_DIGIT_NUMBER);
						grossAmount =BillingUtils.multiplyDecimals(buyPrice,grossAmount , GeneralConstants.ROUNDING_DIGIT_NUMBER);
						
						
						/** Applied Percentage on Amount Gross**/
						percentAmount= BillingUtils.multiplyDecimals(grossAmount,ratePercent , GeneralConstants.ROUNDING_DIGIT_NUMBER);
						
						if(BillingUtils.isBaseSettlementTrading(serviceRate.getBillingService().getBaseCollection())){
							percentAmount=businessLayerCommonFacade.appliedMinimumAmountCalculation(serviceRate, 
									percentAmount, parameters);
						}
						/** Applied Billing Currency */
						businessLayerCommonFacade.convertCalculationToBilled(percentAmount, 
								serviceRate.getCurrencyRate(),newCollectionRecordDetail,
								parameters, DailyExchangeRoleType.SELL.getCode());
						
						newCollectionRecordDetail.setRatePercent(percentAmount);
						newCollectionRecordDetail.setCurrencyRate(collectionRecordDetailTo.getCurrencyRate());
						newCollectionRecordDetail.setMovementQuantity(movementQuantity);
						newCollectionRecordDetail.setCalculationDate(collectionRecordDetailTo.getCalculationDate());
						newCollectionRecordDetail.setMovementDate(movementDate);
						newCollectionRecordDetail.setOperationDate(operationDate);
						newCollectionRecordDetail.setOperationPrice(operationPrice);
						newCollectionRecordDetail.setOperationNumber(operationNumber);
						newCollectionRecordDetail.setIdSourceParticipant(idSourceParticipant);
						newCollectionRecordDetail.setIdTargetParticipant(idTargetParticipant);
						newCollectionRecordDetail.setHolderAccount(holderAccount);
						newCollectionRecordDetail.setIdHolder(idHolderPk);
						newCollectionRecordDetail.setSecurityClass(securityClass);
						newCollectionRecordDetail.setModality(modality);
						newCollectionRecordDetail.setBallotSequential(ballotSequence);
						newCollectionRecordDetail.setNegotiatedAmount(negotiatedAmount);
						newCollectionRecordDetail.setNegotiatedAmountOrigin(negotiatedAmountOrigin);
						
				    }else if (serviceRate.getRateType().equals(RateType.TIPO_MOVIMIENTO_FIJO.getCode())){
				    															
						/** 1. calculate for number movements**/
						BigDecimal amount 			= new BigDecimal(0.0);
						Integer numMovement 		= collectionRecordDetailTo.getMovementCount();
						BigDecimal rateAmount		= rateMovement.getRateAmount();
						
						/** calculate with amount */
						amount= BillingUtils.multiplyDecimalInteger(rateAmount, numMovement, GeneralConstants.ROUNDING_DIGIT_NUMBER);
						
						/** Applied Billing Currency */
						businessLayerCommonFacade.convertCalculationToBilled(amount, 
								serviceRate.getCurrencyRate(),newCollectionRecordDetail,
								parameters, DailyExchangeRoleType.SELL.getCode());
						
						newCollectionRecordDetail.setRateAmount(amount);
						newCollectionRecordDetail.setMovementCount(numMovement);
						newCollectionRecordDetail.setMovementDate(movementDate);
						newCollectionRecordDetail.setOperationDate(operationDate);
						newCollectionRecordDetail.setOperationNumber(operationNumber);
						newCollectionRecordDetail.setIdSourceParticipant(idSourceParticipant);
						newCollectionRecordDetail.setIdTargetParticipant(idTargetParticipant);
						newCollectionRecordDetail.setOperationPrice(operationPrice);
						newCollectionRecordDetail.setHolderAccount(holderAccount); 
						newCollectionRecordDetail.setIdHolder(idHolderPk);  
						newCollectionRecordDetail.setMovementQuantity(movementQuantity);
						newCollectionRecordDetail.setCurrencyRate(serviceRate.getCurrencyRate());
						newCollectionRecordDetail.setSecurityClass(securityClass);
						newCollectionRecordDetail.setModality(modality);
						newCollectionRecordDetail.setBallotSequential(ballotSequence);
						newCollectionRecordDetail.setNegotiatedAmount(negotiatedAmount);
						newCollectionRecordDetail.setNegotiatedAmountOrigin(negotiatedAmountOrigin);
						
				    }
				    else if ( (serviceRate.getRateType().equals(RateType.ESCALONADO_FIJO.getCode()) ||
				    		serviceRate.getRateType().equals(RateType.ESCALONADO_MIXTO.getCode()))    &&
				    		serviceRate.getIndScaleMovement().equals(PropertiesConstants.YES_VALUE_INTEGER)
				    		){
				    		/**Aplicar la logica de escalones con movimientos*/
				    	

					    	/** 1. calculate for number movements**/
							BigDecimal amount 			= new BigDecimal(0.0);
							Integer numMovement 		= collectionRecordDetailTo.getMovementCount();
							
							/** calculate with amount */
							amount= collectionRecordDetailTo.getAmount();
							/** Applied Billing Currency */
							businessLayerCommonFacade.convertCalculationToBilled(amount, serviceRate.getCurrencyRate(),
												newCollectionRecordDetail,
												parameters, DailyExchangeRoleType.SELL.getCode());
							
							newCollectionRecordDetail.setRateAmount(amount);
							newCollectionRecordDetail.setMovementCount(numMovement);
							newCollectionRecordDetail.setMovementDate(movementDate);
							newCollectionRecordDetail.setOperationDate(operationDate);
							newCollectionRecordDetail.setOperationNumber(operationNumber);
							newCollectionRecordDetail.setIdSourceParticipant(idSourceParticipant);
							newCollectionRecordDetail.setIdTargetParticipant(idTargetParticipant);
							newCollectionRecordDetail.setOperationPrice(operationPrice);
							newCollectionRecordDetail.setHolderAccount(holderAccount); 
							newCollectionRecordDetail.setIdHolder(idHolderPk);  
							newCollectionRecordDetail.setMovementQuantity(movementQuantity);
							newCollectionRecordDetail.setCurrencyRate(serviceRate.getCurrencyRate());
							newCollectionRecordDetail.setSecurityClass(securityClass);
							newCollectionRecordDetail.setModality(modality);
							newCollectionRecordDetail.setBallotSequential(ballotSequence);
							newCollectionRecordDetail.setNegotiatedAmount(negotiatedAmount);
							newCollectionRecordDetail.setNegotiatedAmountOrigin(negotiatedAmountOrigin);
				    	
				    	
				    	
				    }else if(serviceRate.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode())     &&
				    		serviceRate.getIndScaleMovement().equals(PropertiesConstants.YES_VALUE_INTEGER)){
				    	
				    	/**YES_VALUE_INTEGER =ONE; si hay movimientos
				    	 * =zero; no hay movimientos
				    	 * */
				    		/** 1. calculate for number movements**/
							BigDecimal percentAmount 		=  new BigDecimal(0.0);
							
							/** Applied Percentage on Amount Gross**/
							percentAmount= collectionRecordDetailTo.getAmount();
							
							/** Applied Billing Currency */
							businessLayerCommonFacade.convertCalculationToBilled(percentAmount, 
									serviceRate.getCurrencyRate(), newCollectionRecordDetail,
									parameters, DailyExchangeRoleType.SELL.getCode());
							
							newCollectionRecordDetail.setRatePercent(percentAmount);
							newCollectionRecordDetail.setCurrencyRate(collectionRecordDetailTo.getCurrencyRate());
							newCollectionRecordDetail.setMovementQuantity(movementQuantity);
							newCollectionRecordDetail.setCalculationDate(collectionRecordDetailTo.getCalculationDate());
							newCollectionRecordDetail.setMovementDate(movementDate);
							newCollectionRecordDetail.setOperationDate(operationDate);
							newCollectionRecordDetail.setOperationPrice(operationPrice);
							newCollectionRecordDetail.setOperationNumber(operationNumber);
							newCollectionRecordDetail.setIdSourceParticipant(idSourceParticipant);
							newCollectionRecordDetail.setIdTargetParticipant(idTargetParticipant);
							newCollectionRecordDetail.setHolderAccount(holderAccount);
							newCollectionRecordDetail.setIdHolder(idHolderPk);
							newCollectionRecordDetail.setSecurityClass(securityClass);
							newCollectionRecordDetail.setModality(modality);
							newCollectionRecordDetail.setBallotSequential(ballotSequence);
							newCollectionRecordDetail.setNegotiatedAmount(negotiatedAmount);
							newCollectionRecordDetail.setNegotiatedAmountOrigin(negotiatedAmountOrigin);
				    	
				    }
					
					/**
					 * More Details
					 */
					if(Validations.validateIsNotNullAndNotEmpty(collectionRecordDetailTo.getSecurityClass())){
						newCollectionRecordDetail.setSecurityClass(collectionRecordDetailTo.getSecurityClass());
					}
					if(Validations.validateIsNotNullAndNotEmpty(collectionRecordDetailTo.getCurrencyOrigin())){
						newCollectionRecordDetail.setCurrencyOrigin(collectionRecordDetailTo.getCurrencyOrigin());
					}
					if(Validations.validateIsNotNullAndPositive(collectionRecordDetailTo.getRole())){
						newCollectionRecordDetail.setRole(collectionRecordDetailTo.getRole());
					}
					if(Validations.validateIsNotNullAndNotEmpty(collectionRecordDetailTo.getMnemonicHolder())){
						newCollectionRecordDetail.setMnemonicHolder(collectionRecordDetailTo.getMnemonicHolder());
					}
					if(Validations.validateIsNotNullAndNotEmpty(collectionRecordDetailTo.getEconomicActivityParticipant())){
						newCollectionRecordDetail.setEconomicActivityParticipant(collectionRecordDetailTo.getEconomicActivityParticipant());
					}
					if(Validations.validateIsNotNullAndNotEmpty(collectionRecordDetailTo.getPortfolio())){
						newCollectionRecordDetail.setPortfolio(collectionRecordDetailTo.getPortfolio());
					}
					
					newCollectionRecordDetailList.add(newCollectionRecordDetail);
				}	
						
		return newCollectionRecordDetailList;
	}
	
	/**
	 * Adds the amount for service rate fixed.
	 *
	 * @param collectionRecord the collection record
	 * @param collectionRecordDetailList the collection record detail list
	 * @param billingService the billing service
	 * @throws ServiceException the service exception
	 */
	protected  void addAmountForServiceRateFixed(CollectionRecord collectionRecord , List<CollectionRecordDetail> collectionRecordDetailList,
												BillingService billingService) throws ServiceException{
		
			if (Validations.validateListIsNullOrEmpty(collectionRecord.getCollectionRecordDetail())){
				collectionRecord.setCollectionRecordDetail(new ArrayList<CollectionRecordDetail>());
			}
			 
			/** ADD TO COLLECTION RECORD**/			
			for (CollectionRecordDetail collectionRecordDetail: collectionRecordDetailList) {	
						
				BigDecimal tempAmount=BillingUtils.addTwoDecimal(collectionRecord.getCollectionAmount(), collectionRecordDetail.getRateAmount(), GeneralConstants.ROUNDING_DIGIT_NUMBER);
				collectionRecord.setCollectionAmount(tempAmount);
				collectionRecord.getCollectionRecordDetail().add(collectionRecordDetail);
			}

			
	}
	
	/**
	 * Adds the amount for service rate percentage.
	 *
	 * @param collectionRecord the collection record
	 * @param collectionRecordDetailList the collection record detail list
	 * @param billingService the billing service
	 * @param parameters the parameters
	 * @throws ServiceException the service exception
	 */
	protected  void addAmountForServiceRatePercentage(CollectionRecord collectionRecord , List<CollectionRecordDetail> collectionRecordDetailList,
													BillingService billingService, Map<String, Object> parameters) throws ServiceException{
		
		if (Validations.validateListIsNullOrEmpty(collectionRecord.getCollectionRecordDetail())){
			collectionRecord.setCollectionRecordDetail(new ArrayList<CollectionRecordDetail>());
		}
		
		 BigDecimal amountDetail   = BigDecimal.ZERO;
		 
		for (CollectionRecordDetail collectionRecordDetail: collectionRecordDetailList) {
			

			amountDetail = collectionRecordDetail.getRatePercent();

			
			/** ADD TO COLLECTION RECORD**/
			BigDecimal tempAmount=BillingUtils.addTwoDecimal(collectionRecord.getCollectionAmount(), amountDetail, GeneralConstants.ROUNDING_DIGIT_NUMBER);					
			collectionRecord.setCollectionAmount(tempAmount);
			
			collectionRecord.getCollectionRecordDetail().add(collectionRecordDetail);
		}
		
		
		
    }
	
	
	/**
	 * Find operation.
	 *
	 * @param operation the operation
	 * @param operationLast the operation last
	 * @return the boolean
	 */
	protected Boolean findOperation(Long operation, Long operationLast){

		return operation.equals(operationLast);
	}
	
	/**
	 * Find affectations request.
	 *
	 * @param request the request
	 * @param requestLast the request last
	 * @return the boolean
	 */
	protected Boolean findAffectationsRequest(String request, String requestLast){

		return request.equals(requestLast);
	}
	
	/**
	 * Find entity at list temporal to by request.
	 *
	 * @param collectionRecordToList the collection record to list
	 * @param entity the entity
	 * @param typeEntity the type entity
	 * @param request the request
	 * @return the collection record to
	 */
	protected  CollectionRecordTo findEntityAtListTemporalToByRequest(List<CollectionRecordTo> collectionRecordToList,
									Object entity,BillingService billingService,String request){
		
			if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordToList)){
					
				for (CollectionRecordTo collectionRecordTo : collectionRecordToList) {
					if(baseCollectionMgmtServiceBean.isPaymentByparticipant(billingService)){
						entity = baseCollectionMgmtServiceBean.getIdParticipantPkByIdIssuerPk(entity.toString());
					}
					if (collectionRecordTo.existEntityCollection(entity, billingService.getEntityCollection())){

						for (CollectionRecordDetailTo collectionRecordDetTo : collectionRecordTo.getCollectionRecordDetailToList()) {
							if(request.equals(collectionRecordDetTo.getOperationNumber().toString())){
								return collectionRecordTo;
							}
						}
						
					}
				}
			}
		return null;
	}
	
	/**
	 * Find entity at list temporal to.
	 *
	 * @param collectionRecordToList the collection record to list
	 * @param entity the entity
	 * @param typeEntity the type entity
	 * @return the collection record to
	 */
	protected  CollectionRecordTo findEntityAtListTemporalTo(List<CollectionRecordTo> collectionRecordToList,
									Object entity,BillingService billingService){
		
			if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordToList)){
					
				for (CollectionRecordTo collectionRecordTo : collectionRecordToList) {
					/*COBRO A EMISORES Y FACTURACION A PARTICIPANTES*/
					if(baseCollectionMgmtServiceBean.isPaymentByparticipant(billingService)){
						entity = baseCollectionMgmtServiceBean.getIdParticipantPkByIdIssuerPk(entity.toString());
					}
					if (collectionRecordTo.existEntityCollection(entity, billingService.getEntityCollection())){
						return collectionRecordTo;
					}
				}
			}
		return null;
	}
	
	/**
	 * Find entity at list.
	 *
	 * @param collectionRecordList the collection record list
	 * @param entity the entity
	 * @param typeEntity the type entity
	 * @return the collection record
	 */
	protected  CollectionRecord findEntityAtList(List<CollectionRecord> collectionRecordList,Object entity,Integer typeEntity){
		
		if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordList)){
				
			for (CollectionRecord collectionRecord : collectionRecordList) {
				
				Boolean exist=false;
				if(typeEntity.equals(EntityCollectionType.HOLDERS.getCode())){
					exist= collectionRecord.getHolder().getIdHolderPk().equals((Long)entity);
				}else if (typeEntity.equals(EntityCollectionType.ISSUERS.getCode())){
					exist= collectionRecord.getIssuer().getIdIssuerPk().equals((String)entity);
				}else if(typeEntity.equals(EntityCollectionType.PARTICIPANTS.getCode())){
					exist= collectionRecord.getParticipant().getIdParticipantPk().equals((Long)entity);
				}
				
				if (exist){
					return collectionRecord;
				}
			}
		}
		
		return null;
	}
	

	/**
	 * Find entity at list for Safi.
	 *
	 * @param collectionRecordList the collection record list
	 * @param entity the entity
	 * @param typeEntity the type entity
	 * @return the collection record
	 */
	protected  CollectionRecord findEntityAtListSafi(List<CollectionRecord> collectionRecordList,Object entity,Integer typeEntity, String safi){
		
		if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordList)){
				
			for (CollectionRecord collectionRecord : collectionRecordList) {
				
				Boolean exist=false;
				if(typeEntity.equals(EntityCollectionType.HOLDERS.getCode())){
					exist= collectionRecord.getHolder().getIdHolderPk().equals((Long)entity)
							&&collectionRecord.getMnemonicConcatenated().equals(safi);
				}else if (typeEntity.equals(EntityCollectionType.ISSUERS.getCode())){
					exist= collectionRecord.getIssuer().getIdIssuerPk().equals((String)entity)
							&&collectionRecord.getMnemonicConcatenated().equals(safi);
				}else if(typeEntity.equals(EntityCollectionType.PARTICIPANTS.getCode())){
					exist= collectionRecord.getParticipant().getIdParticipantPk().equals((Long)entity)
							&&collectionRecord.getMnemonicConcatenated().equals(safi);
				}
				
				if (exist){
					return collectionRecord;
				}
			}
		}
		
		return null;
	}
	
	/**
	 * Adds the entity collection.
	 *
	 * @param collectionRecord the collection record
	 * @param collectionRecordTo the collection record to
	 */
	protected void addEntityCollection(CollectionRecord collectionRecord,CollectionRecordTo collectionRecordTo){
			
		Integer typeEntity = collectionRecordTo.getEntityCollection();
		if(typeEntity.equals(EntityCollectionType.HOLDERS.getCode())){
			Holder holder=new Holder();
			holder.setIdHolderPk((Long) collectionRecordTo.getEntityCollection(typeEntity));	
			collectionRecord.setHolder(holder);
		}else if (typeEntity.equals(EntityCollectionType.ISSUERS.getCode())){
			Issuer issuer= new Issuer();
			issuer.setIdIssuerPk((String) collectionRecordTo.getEntityCollection(typeEntity));
			collectionRecord.setIssuer(issuer);
		}else if(typeEntity.equals(EntityCollectionType.PARTICIPANTS.getCode())){
			Participant participant= new Participant();
			participant.setIdParticipantPk((Long) collectionRecordTo.getEntityCollection(typeEntity));
			collectionRecord.setParticipant(participant);
		}
				
	}
	
	
	
	
	
}
