package com.pradera.billing.process.batch.interceptor;

import java.io.Serializable;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class RegisterThreadLocalInterceptor.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/03/2013
 */
@LoggerAuditBatch
@Interceptor
public class RegisterThreadBatchInterceptor implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5114899327070760847L;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	@Resource(lookup="java:comp/TransactionSynchronizationRegistry")
	TransactionSynchronizationRegistry registry;
	
	/**
	 * Instantiates a new register thread local interceptor.
	 */
	public RegisterThreadBatchInterceptor() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Around invoke.
	 *
	 * @param ic the ic
	 * @return the object
	 * @throws Exception the exception
	 */
	@AroundInvoke
	public Object aroundInvoke(InvocationContext ic) throws Exception {
		
		LoggerUser loggerUser = (LoggerUser)registry.getResource(RegistryContextHolderType.LOGGER_USER);		
		loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
		return ic.proceed();
	}
	
	

}
