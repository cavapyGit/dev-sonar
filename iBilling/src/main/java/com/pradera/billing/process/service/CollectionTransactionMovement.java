package com.pradera.billing.process.service;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import com.pradera.billing.billingcollection.to.CollectionRecordTo;
import com.pradera.billing.billingcollection.to.ProcessedServiceTo;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.billing.CollectionRecord;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Interface CollectionTransactionMovement.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Local
public interface CollectionTransactionMovement {
	
	
	/**
	 * Start calculate collection transaction.
	 *
	 * @param processedServiceTo the processed service to
	 * @throws ServiceException the service exception
	 */
	public void startCalculateCollectionTransaction(ProcessedServiceTo processedServiceTo)throws ServiceException;
	
	/**
	 * Gets the ting data for calculating by quantity.
	 *
	 * @param parameters the parameters
	 * @return the ting data for calculating by quantity
	 */
	public List<CollectionRecordTo> gettingDataForCalculatingByQuantity(Map<String, Object> parameters);
	
	/**
	 * Gets the ting data for calculating by settlement.
	 *
	 * @param parameters the parameters
	 * @return the ting data for calculating by settlement
	 * @throws ServiceException the service exception
	 */
	public List<CollectionRecordTo> gettingDataForCalculatingBySettlement(Map<String, Object> parameters) throws ServiceException;
	
	/**
	 * Process for service rate.
	 *
	 * @param parameters the parameters
	 * @param collectionRecordList the collection record list
	 * @throws ServiceException the service exception
	 */
	public void                     processForServiceRate(Map<String, Object> parameters ,List<CollectionRecord> collectionRecordList) throws ServiceException;

}
