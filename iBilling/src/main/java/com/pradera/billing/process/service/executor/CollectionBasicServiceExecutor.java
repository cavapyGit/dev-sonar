package com.pradera.billing.process.service.executor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.ejb.AccessTimeout;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.billing.billingcollection.facade.BaseCollectionMgmtServiceFacade;
import com.pradera.billing.billingcollection.facade.CollectionServiceMgmtServiceFacade;
import com.pradera.billing.billingcollection.service.BaseCollectionMgmtServiceBean;
import com.pradera.billing.billingcollection.to.CollectionRecordDetailTo;
import com.pradera.billing.billingcollection.to.CollectionRecordTo;
import com.pradera.billing.billingcollection.to.DailyExchangeRateFilter;
import com.pradera.billing.billingcollection.to.ProcessedServiceTo;
import com.pradera.billing.billingrate.facade.BillingRateMgmtServiceFacade;
import com.pradera.billing.billingservice.facade.BillingServiceMgmtServiceFacade;
import com.pradera.billing.billingservice.service.BillingServiceMgmtServiceBean;
import com.pradera.billing.common.facade.BusinessLayerCommonFacade;
import com.pradera.billing.common.utils.BillingUtils;
import com.pradera.billing.process.service.CollectionBasicService;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.entity.producer.DepositaryDataBase;
import com.pradera.core.framework.entity.service.LoaderEntityServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantType;
import com.pradera.model.billing.BillingEconomicActivity;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.CollectionRecord;
import com.pradera.model.billing.CollectionRecordDetail;
import com.pradera.model.billing.ProcessedService;
import com.pradera.model.billing.ServiceRate;
import com.pradera.model.billing.ServiceRateScale;
import com.pradera.model.billing.type.BaseCollectionType;
import com.pradera.model.billing.type.EntityCollectionType;
import com.pradera.model.billing.type.ProcessedServiceType;
import com.pradera.model.billing.type.RateStateType;
import com.pradera.model.billing.type.RateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.DailyExchangeRoleType;
import com.pradera.model.issuancesecuritie.Issuer;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Project iDepositary. Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CollectionBasicServiceExecutor.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Singleton
public class CollectionBasicServiceExecutor implements CollectionBasicService {

	/** The log. */
	@Inject
	PraderaLogger log;

	/** The load entity service. */
	@EJB
	LoaderEntityServiceBean loadEntityService;

	/** The em. */
	@Inject
	@DepositaryDataBase
	private EntityManager em;

	/** The collection service mgmt service facade. */
	@EJB
	CollectionServiceMgmtServiceFacade collectionServiceMgmtServiceFacade;

	/** The base collection mgmt service facade. */
	@EJB
	BaseCollectionMgmtServiceFacade baseCollectionMgmtServiceFacade;

	/** The billing service mgmt service facade. */
	@EJB
	BillingServiceMgmtServiceFacade billingServiceMgmtServiceFacade;

	@EJB
	BaseCollectionMgmtServiceBean baseCollectionMgmtServiceBean;

	/** The billing rate mgmt service facade. */
	@EJB
	BillingRateMgmtServiceFacade billingRateMgmtServiceFacade;

	/** The billing service mgmt service bean. */
	@EJB
	private BillingServiceMgmtServiceBean billingServiceMgmtServiceBean;

	// private static final long TIME = 1;

	/** The business layer common facade. */
	@EJB
	BusinessLayerCommonFacade businessLayerCommonFacade;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pradera.billing.process.service.CollectionBasicService#
	 * startCalculateCollectionTransaction(com.pradera.billing.billingcollection.to.
	 * ProcessedServiceTo)
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	@Performance
	@TransactionTimeout(unit = TimeUnit.MINUTES, value = 60)
	@AccessTimeout(unit = TimeUnit.MINUTES, value = 60)
	@Override
	/***
	 * Processing Collection Basic Transaction
	 */
	public void startCalculateCollectionTransaction(ProcessedServiceTo processedServiceTo) throws ServiceException {

		/***
		 * 
		 * Recordar el indicador de renta fija o renta variable EN SHORT_INTEGER
		 */

		if (processedServiceTo.getErrorType() != null) {
			throw new ServiceException(processedServiceTo.getErrorType());
		} else if (processedServiceTo.getOtherErrorType()) {
			throw new ServiceException();
		}

		ProcessedService processedService = null;

		// Verificar que No servicios repetidos en el mismo periodo
		collectionServiceMgmtServiceFacade.verifyProcessedServiceUniqueByPeriod(processedServiceTo);
		// Verificamos que las tarifas esten activas
		collectionServiceMgmtServiceFacade.verifyRateActives(processedServiceTo);

		processedService = collectionServiceMgmtServiceFacade.verifyCollectionUnbilled(processedServiceTo);
		processedService.setInitialDate(processedServiceTo.getInitialDate());
		processedService.setFinalDate(processedServiceTo.getFinalDate());
		loadEntityService.detachmentEntity(processedService);

		/**
		 * ::::::::::::::::::::::::::::: 1 -- getting billing service
		 * ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		 */
		BillingService billingService = processedService.getBillingService();
		List<BillingEconomicActivity> listEconActBd = billingServiceMgmtServiceBean
				.getBillingEconomicActivityByBillingServicePk(billingService.getIdBillingServicePk());
		List<Integer> listEconomicAct = BillingUtils.getBillingEconomicActivityListInteger(listEconActBd);
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put(GeneralConstants.BILLING_SERVICE, billingService);
		parameters.put(GeneralConstants.ECONOMIC_ACTIVITY_LIST_INT, listEconomicAct);
		parameters.put(GeneralConstants.OPERATION_DATE, processedService.getOperationDate());
		parameters.put(GeneralConstants.CALCULATION_DATE, processedService.getCalculationDate());

		if (PropertiesConstants.PERIOD_CALCULATION_DAILY_PK.equals(billingService.getCalculationPeriod())) {
			Date calculateDate = processedService.getCalculationDate();
			Date date1 = BillingUtils.getDateInitialDay(calculateDate);
			Date date2 = BillingUtils.getDateFinalDay(calculateDate);
			parameters.put(GeneralConstants.FIELD_INITIAL_DATE, date1);
			parameters.put(GeneralConstants.FIELD_FINAL_DATE, date2);
		} else if (PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK.equals(billingService.getCalculationPeriod())) {
			parameters.put(GeneralConstants.FIELD_INITIAL_DATE, processedService.getInitialDate());
			parameters.put(GeneralConstants.FIELD_FINAL_DATE, processedService.getFinalDate());
		}

		DailyExchangeRateFilter dailyExchangeRateFilter = new DailyExchangeRateFilter();
		dailyExchangeRateFilter.setIdSourceinformation(billingService.getSourceInformation());
		dailyExchangeRateFilter.setInitialDate((processedService.getCalculationDate() == null ? null
				: CommonsUtilities.truncateDateTime(processedService.getCalculationDate())));
		dailyExchangeRateFilter.setFinalDate((processedService.getCalculationDate() == null ? null
				: CommonsUtilities.truncateDateTime(processedService.getCalculationDate())));

		Map<Integer, Map> exchangeCalculation = new HashMap<Integer, Map>();
		exchangeCalculation = businessLayerCommonFacade.chargeExchangeMemoryBuyBs(dailyExchangeRateFilter,
				DailyExchangeRoleType.BUY.getCode());
		parameters.put(GeneralConstants.MATRIX_EXCHANGE_RATE_BUY, exchangeCalculation);

		Map<Integer, Map> exchangeSell = new HashMap<Integer, Map>();
		exchangeSell = businessLayerCommonFacade.chargeExchangeMemorySelllBs(dailyExchangeRateFilter,
				DailyExchangeRoleType.SELL.getCode());
		parameters.put(GeneralConstants.MATRIX_EXCHANGE_RATE_SELL, exchangeSell);

		List<ServiceRate> listServiceRates = billingService.getServiceRates();

		/** this list will be filled */
		List<CollectionRecord> collectionRecordList = processedService.getCollectionRecords();
		if (Validations.validateListIsNotNullAndNotEmpty(listServiceRates)) {
			collectionRecordList = new ArrayList<CollectionRecord>();
			/** RECORRO TARIFA POR TARIFA */
			for (ServiceRate serviceRate : listServiceRates) {
				if (RateStateType.ACTIVED.getCode().equals(serviceRate.getRateState())) {
					/**
					 *  2 -- getting movement type only billed
					 */
					List<CollectionRecordTo> collectionRecordToList = null;
					parameters.put(GeneralConstants.SERVICE_RATE, serviceRate);
					/**
					 *  3 GETTING DATA BY BASE COLLECTION
					 */
					// 9. PARTICIPANTE DIRECTO
					if (serviceRate.getRateType().equals(RateType.TIPO_MOVIMIENTO_FIJO.getCode())){
						collectionRecordToList = gettingDataForCalculatingByQuantity(parameters);
					} else if (serviceRate.getRateType().equals(RateType.FIJO.getCode())) {
						collectionRecordToList = gettingDataForCalculatingFixed(parameters);
					} else {
						// 1. CUSTODIA FIJA - 2.CUSTODIA ELECTRONICA
						List<ServiceRateScale> serviceRateScaleList = billingRateMgmtServiceFacade
								.findServiceRateScaleByServiceRate(serviceRate.getIdServiceRatePk());
						if (Validations.validateListIsNotNullAndNotEmpty(serviceRateScaleList)) {
							serviceRate.setServiceRateScales(serviceRateScaleList);
							collectionRecordToList = gettingDataForCalculatingEstaggeredFixed(parameters);
						}
					}
					
					
					
					/*
					if (serviceRate.getRateType().equals(RateType.PORCENTUAL.getCode())) {
						collectionRecordToList = gettingDataForCalculatingPercentage(parameters);
					}
					if (serviceRate.getRateType().equals(RateType.FIJO.getCode())) {
						collectionRecordToList = gettingDataForCalculatingFixed(parameters);
					}
					/**
					 * Only Staggered
					 *if (serviceRate.getRateType().equals(RateType.ESCALONADO_FIJO.getCode())) {

						/****
						 * VERIFY IND ACCUMULATIVE
						 */
						/**
						 * 2 -- getting movement type only billed
						 */
						/*
						 * List<ServiceRateScale> serviceRateScaleList = billingRateMgmtServiceFacade
						 * .findServiceRateScaleByServiceRate(serviceRate.getIdServiceRatePk()); if
						 * (Validations.validateListIsNotNullAndNotEmpty(serviceRateScaleList)) {
						 * serviceRate.setServiceRateScales(serviceRateScaleList);
						 * collectionRecordToList =
						 * gettingDataForCalculatingEstaggeredFixed(parameters); }
						 
					}
					if (serviceRate.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode())) {
						/****
						 * VERIFY IND ACCUMULATIVE
						/**
						 *  2 -- getting movement type only billed
						 
						List<ServiceRateScale> serviceRateScaleList = billingRateMgmtServiceFacade
								.findServiceRateScaleByServiceRate(serviceRate.getIdServiceRatePk());
						if (Validations.validateListIsNotNullAndNotEmpty(serviceRateScaleList)) {
							serviceRate.setServiceRateScales(serviceRateScaleList);
							collectionRecordToList = gettingDataForCalculatingEstaggeredFixed(parameters);
						}
					}
					/**
					 * TARIFA ESCALONADA MIXTA
					
					if (serviceRate.getRateType().equals(RateType.ESCALONADO_MIXTO.getCode())) {
						/****
						 * VERIFY IND ACCUMULATIVE
						 */
						/**
						 *  2. -- getting movement type only billed
						 
						List<ServiceRateScale> serviceRateScaleList = billingRateMgmtServiceFacade
								.findServiceRateScaleByServiceRate(serviceRate.getIdServiceRatePk());
						serviceRate.setServiceRateScales(serviceRateScaleList);
						if (Validations.validateListIsNotNullAndNotEmpty(serviceRateScaleList)) {
							/**
							 * 1- Obtener Data
							 * 
							 * 2- Aplicarle el escalon
							 * 
							 * NegotiationMechanismType
							 
							Getting movement
							collectionRecordToList = gettingDataForCalculatingEstaggeredFixed(parameters);
						}
					}
					*/
					if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordToList)) {
						log.info(" NUMBER OF REGISTER FOR CALCULATE  : " + collectionRecordToList.size() + "");
						parameters.put(GeneralConstants.COLLECTION_RECORD_TO_LIST, collectionRecordToList);
						/**
						 *  4. ADDING NEW AMOUNT BY EACH MOVEMENT RATE
						 */
						processForServiceRate(parameters, collectionRecordList);
						/** after than add mount for calculation , setting memory to null */
						parameters.remove(GeneralConstants.COLLECTION_RECORD_TO_LIST);
						parameters.remove(GeneralConstants.MINIMUM_BILLING);
						collectionRecordToList = null;// setting values
					} else {
						log.info(" NUMBER OF REGISTER FOR CALCULATE  : 0 ");
					}
					if (parameters.containsKey(GeneralConstants.SERVICE_RATE)) {
						parameters.remove(GeneralConstants.SERVICE_RATE);
					}
				} // end each service rate
			}
			if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordList)) {
				for (CollectionRecord collectionRecord : collectionRecordList) {
					collectionRecord.setProcessedService(processedService);
				}
				processedService.setCollectionRecords(collectionRecordList);
				/**
				 * ::::::::::::::: FILTRAR ENTITIES BILLED
				 * ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
				 **/
				collectionServiceMgmtServiceFacade.filterEntitiesBilled(processedService);
				/**
				 * ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
				 */
				if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordList)) {
					/** persist calculation on BD */
					collectionServiceMgmtServiceFacade.saveProcessedServiceByBatch(processedService);
				}
			}
			ProcessedService processedServiceFinish = collectionServiceMgmtServiceFacade
					.getProcessedServiceByCode(processedService.getIdProcessedServicePk());
			processedServiceFinish.setOperationDate(CommonsUtilities.currentDate());
			processedServiceFinish.setProcessedState(ProcessedServiceType.PROCESADO.getCode());
			log.info(
					" ::::::::::::::::::::::::::::::::  FIN   Proceso de Transaccion de Tarifas Porcentuales ::::::::::::::::::::::::::::::::");
			log.info(" ::::::::::::::::::  Codigo de Servicio =  " + billingService.getServiceCode()
					+ " ::::::::::::::::::::::::::");
			em.flush();
			// log.info(" :::::::::::::::::::::::::::::::: ERROR AL ACTUALIZAR EL SERVICIO
			// PROCESADO = " +billingService.getServiceCode()+" :::");
			if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordList)) {
				log.info(" ::::::::::::::::::::::::::::::::  REGISTRO COBROS" + " EN EL SERVICIO = "
						+ collectionRecordList.size() + "  :::");
			} else {
				log.info(" ::::::::::::::::::::::::::::::::  NO REGISTRO COBROS EN EL SERVICIO = "
						+ billingService.getServiceCode() + "  :::");
			}
		}
	}

	/*
	 * (non-Javadoc) APLICA LA COMISION
	 * 
	 * @see com.pradera.billing.process.service.CollectionBasicService#
	 * processForServiceRate(java.util.Map, java.util.List)
	 */
	@Override
	public void processForServiceRate(Map<String, Object> parameters, List<CollectionRecord> collectionRecordList)
			throws ServiceException {
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		List<Integer> listEconomicAct = (List<Integer>) parameters.get(GeneralConstants.ECONOMIC_ACTIVITY_LIST_INT);
		Integer portFolio = billingService.getPortfolio();

		/**
		 * Cambio: si es SAFI con cartera de clientes, crear un listado de
		 * CollectionRecord
		 */
		if (BillingUtils.isSafiPortfolioClient(listEconomicAct, portFolio)) {
			generateCollectionRecordSafi(parameters, collectionRecordList);
		} else {
			generateCollectionRecord(parameters, collectionRecordList);
		}

	}

	public List<CollectionRecord> generateCollectionRecord(Map<String, Object> parameters,
			List<CollectionRecord> collectionRecordList) throws ServiceException {
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		ServiceRate serviceRate = (ServiceRate) parameters.get(GeneralConstants.SERVICE_RATE);
		Date calculateDate = (Date) parameters.get(GeneralConstants.CALCULATION_DATE);
		Date operationDate = (Date) parameters.get(GeneralConstants.OPERATION_DATE);
		/** Getting currency change factor ***/

		BigDecimal amountDetail = BigDecimal.ZERO;

		/***
		 * GETTING NEW LIST COLLECTION RECORD WITH NEW AMOUNT FOR RATE , IF THERE IS AN
		 * ENTITY ON LIST COLLECTION RECORD ONLY ADDS BY AMOUNT RATE, IF NOT CREATE A
		 * NEW COLLECTION RECORD FOR A ENTITY COLLECTION AND AFTER ADD A LIST OF
		 * COLLECTION RECORDS
		 **/
		List<CollectionRecordTo> collectionRecordToList = (List<CollectionRecordTo>) parameters
				.get(GeneralConstants.COLLECTION_RECORD_TO_LIST);

		// Map<Date,Map> matrixExchangeRange=
		// businessLayerCommonFacade.gettingExchangeByRangeDate(collectionRecordToList,
		// parameters, DailyExchangeRoleType.SELL.getCode());

		for (CollectionRecordTo collectionRecordTo : collectionRecordToList) {

			List<CollectionRecordDetail> newCollectionRecordDetailList = loadCollectionRecordDetail(
					collectionRecordTo.getCollectionRecordDetailToList(), serviceRate, parameters);

			/** getting entity for add news amount **/
			Object objEntity = collectionRecordTo.getEntityCollection(collectionRecordTo.getEntityCollection());
			CollectionRecord collectionRecord = findEntityAtList(collectionRecordList, objEntity,
					collectionRecordTo.getEntityCollection());

			/** THE GOAL IS CREATE A RECORD FOR EACH ENTITY ONLY IF DON'T EXIST **/
			if (collectionRecord == null) {
				collectionRecord = new CollectionRecord();
				collectionRecord.setEntityCollectionId(billingService.getEntityCollection());
				collectionRecord.setGrossAmount(new BigDecimal(0));
				collectionRecord.setCollectionAmount(new BigDecimal(0));
				collectionRecord.setCalculationDate(calculateDate);
				collectionRecord.setOperationDate(operationDate);
				collectionRecord.setCurrencyType(billingService.getCurrencyCalculation());
				collectionRecord.setCurrencyBilling(billingService.getCurrencyBilling());
				collectionRecord.setRecordState(ProcessedServiceType.PROCESADO.getCode());
				collectionRecord.setIndBilled(PropertiesConstants.NOT_BILLED);/** default , not billed **/
				addEntityCollection(collectionRecord,
						collectionRecordTo); /** CAN BE : HOLDER , PARTICIPANT OR ISSUER **/

				/** aggregate new collection record to collectionRecordList */
				collectionRecordList.add(collectionRecord);
			}

			/**
			 * Exclude DPF's in Number Request
			 */
			if (!BaseCollectionType.RECORD_SECURITIES_DEMATERIALIZED_DPF.getCode()
					.equals(billingService.getBaseCollection())) {
				businessLayerCommonFacade.addNumberRequestToCollection(collectionRecord, collectionRecordTo);
			}
			businessLayerCommonFacade.addMnemonicHolderToCollection(collectionRecord, collectionRecordTo);

			/** ADD ENTITI'S AMOUNT OF EACH DETAIL **/
			if (Validations.validateListIsNullOrEmpty(collectionRecord.getCollectionRecordDetail())) {
				collectionRecord.setCollectionRecordDetail(new ArrayList<CollectionRecordDetail>());
			}

			/** ADD TO COLLECTION RECORD **/
			for (CollectionRecordDetail collectionRecordDetail : newCollectionRecordDetailList) {
				collectionRecord.getCollectionRecordDetail().add(collectionRecordDetail);
				BigDecimal tempAmount = BigDecimal.ZERO;

				if (serviceRate.getRateType().equals(RateType.PORCENTUAL.getCode())) {

					amountDetail = collectionRecordDetail.getRatePercent();

					collectionRecordDetail.setRateAmount(amountDetail);

					tempAmount = BillingUtils.addTwoDecimal(collectionRecord.getCollectionAmount(), amountDetail,
							GeneralConstants.ROUNDING_DIGIT_NUMBER);

				} else if ((serviceRate.getRateType().equals(RateType.FIJO.getCode()))
						|| (serviceRate.getRateType().equals(RateType.ESCALONADO_FIJO.getCode()))
						|| (serviceRate.getRateType().equals(RateType.ESCALONADO_MIXTO.getCode()))
						|| (serviceRate.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode()))) {

					tempAmount = BillingUtils.addTwoDecimal(collectionRecord.getCollectionAmount(),
							collectionRecordDetail.getRateAmount(), GeneralConstants.ROUNDING_DIGIT_NUMBER);

				}

				collectionRecord.setCollectionAmount(tempAmount);
			}
			// Caso especial para Emision de DPF
			if (BaseCollectionType.RECORD_SECURITIES_DEMATERIALIZED_DPF.getCode()
					.equals(billingService.getBaseCollection())) {
				collectionRecord.setCollectionAmount(collectionRecordTo.getCollectionAmount());

			}

			/**
			 * APPLLIED RATE MINIMUM
			 */
			BigDecimal amountNew = businessLayerCommonFacade.appliedMinimumAmountCalculation(serviceRate,
					collectionRecord.getCollectionAmount(), parameters);

			businessLayerCommonFacade.collectionToMinimumBilling(collectionRecord, parameters);
			collectionRecord.setCollectionAmount(amountNew);

			/** ONLY IF SERVICE APPLY TAXED **/
			if (billingService.getTaxApplied().equals(PropertiesConstants.YES_APPLY_TAXED)) {

				ParameterTable parameterTableTax = billingServiceMgmtServiceFacade
						.getParameterTableById(PropertiesConstants.TAX_PK);
				Double taxed = (parameterTableTax != null) ? parameterTableTax.getDecimalAmount1()
						: new Double(GeneralConstants.ZERO_VALUE_STRING);

				BigDecimal taxedAmount = BillingUtils.multiplyDecimals(collectionRecord.getCollectionAmount(),
						new BigDecimal(taxed.toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
				collectionRecord.setTaxApplied(taxedAmount);

				BigDecimal grossAmount = BillingUtils.addTwoDecimal(collectionRecord.getCollectionAmount(), taxedAmount,
						GeneralConstants.ROUNDING_DIGIT_NUMBER);

				/** SETTING FINALLY COLLECTION AMOUNT **/
				collectionRecord.setGrossAmount(grossAmount);

			} else {
				collectionRecord.setTaxApplied(new BigDecimal(GeneralConstants.ZERO_VALUE_STRING));
				collectionRecord.setGrossAmount(collectionRecord.getCollectionAmount());
			}

			/**
			 * Save To Billing Calculation
			 */
			businessLayerCommonFacade.billingChargesToConvertCurrency(collectionRecord, parameters);
		}
		return collectionRecordList;
	}

	public List<CollectionRecord> generateCollectionRecordSafi(Map<String, Object> parameters,
			List<CollectionRecord> collectionRecordList) throws ServiceException {
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		ServiceRate serviceRate = (ServiceRate) parameters.get(GeneralConstants.SERVICE_RATE);
		Date calculateDate = (Date) parameters.get(GeneralConstants.CALCULATION_DATE);
		Date operationDate = (Date) parameters.get(GeneralConstants.OPERATION_DATE);
		/** Getting currency change factor ***/

		BigDecimal amountDetail = BigDecimal.ZERO;

		/***
		 * GETTING NEW LIST COLLECTION RECORD WITH NEW AMOUNT FOR RATE , IF THERE IS AN
		 * ENTITY ON LIST COLLECTION RECORD ONLY ADDS BY AMOUNT RATE, IF NOT CREATE A
		 * NEW COLLECTION RECORD FOR A ENTITY COLLECTION AND AFTER ADD A LIST OF
		 * COLLECTION RECORDS
		 **/
		List<CollectionRecordTo> collectionRecordToList = (List<CollectionRecordTo>) parameters
				.get(GeneralConstants.COLLECTION_RECORD_TO_LIST);

		// Map<Date,Map> matrixExchangeRange=
		// businessLayerCommonFacade.gettingExchangeByRangeDate(collectionRecordToList,
		// parameters, DailyExchangeRoleType.SELL.getCode());

		for (CollectionRecordTo collectionRecordTo : collectionRecordToList) {

			/** getting entity for add news amount **/
			Object objEntity = collectionRecordTo.getEntityCollection(collectionRecordTo.getEntityCollection());
			List<CollectionRecordTo> listForSafi = businessLayerCommonFacade.generateListForSafi(collectionRecordTo);

			for (CollectionRecordTo collectionSafi : listForSafi) {
				List<CollectionRecordDetail> newCollectionRecordDetailList = loadCollectionRecordDetail(
						collectionSafi.getCollectionRecordDetailToList(), serviceRate, parameters);
				CollectionRecord collectionRecord = findEntityAtListSafi(collectionRecordList, objEntity,
						collectionSafi.getEntityCollection(), collectionSafi.getMnemonicHolder());

				/** THE GOAL IS CREATE A RECORD FOR EACH ENTITY ONLY IF DON'T EXIST **/
				if (collectionRecord == null) {
					collectionRecord = new CollectionRecord();
					collectionRecord.setEntityCollectionId(billingService.getEntityCollection());
					collectionRecord.setGrossAmount(new BigDecimal(0));
					collectionRecord.setCollectionAmount(new BigDecimal(0));
					collectionRecord.setCalculationDate(calculateDate);
					collectionRecord.setOperationDate(operationDate);
					collectionRecord.setCurrencyType(billingService.getCurrencyCalculation());
					collectionRecord.setCurrencyBilling(billingService.getCurrencyBilling());
					collectionRecord.setRecordState(ProcessedServiceType.PROCESADO.getCode());
					collectionRecord.setIndBilled(PropertiesConstants.NOT_BILLED);/** default , not billed **/
					collectionRecord.setMnemonicConcatenated(collectionSafi.getMnemonicHolder());
					addEntityCollection(collectionRecord,
							collectionSafi); /** CAN BE : HOLDER , PARTICIPANT OR ISSUER **/

					/** aggregate new collection record to collectionRecordList */
					collectionRecordList.add(collectionRecord);
				}

				/**
				 * Exclude DPF's in Number Request
				 */
				if (!BaseCollectionType.RECORD_SECURITIES_DEMATERIALIZED_DPF.getCode()
						.equals(billingService.getBaseCollection())) {
					businessLayerCommonFacade.addNumberRequestToCollection(collectionRecord, collectionSafi);
				}
				// businessLayerCommonFacade.addMnemonicHolderToCollection(collectionRecord,
				// collectionSafi);

				/** ADD ENTITI'S AMOUNT OF EACH DETAIL **/
				if (Validations.validateListIsNullOrEmpty(collectionRecord.getCollectionRecordDetail())) {
					collectionRecord.setCollectionRecordDetail(new ArrayList<CollectionRecordDetail>());
				}

				/** ADD TO COLLECTION RECORD **/
				for (CollectionRecordDetail collectionRecordDetail : newCollectionRecordDetailList) {
					collectionRecord.getCollectionRecordDetail().add(collectionRecordDetail);
					BigDecimal tempAmount = BigDecimal.ZERO;

					if (serviceRate.getRateType().equals(RateType.PORCENTUAL.getCode())) {

						amountDetail = collectionRecordDetail.getRatePercent();

						collectionRecordDetail.setRateAmount(amountDetail);

						tempAmount = BillingUtils.addTwoDecimal(collectionRecord.getCollectionAmount(), amountDetail,
								GeneralConstants.ROUNDING_DIGIT_NUMBER);

					} else if ((serviceRate.getRateType().equals(RateType.FIJO.getCode()))
							|| (serviceRate.getRateType().equals(RateType.ESCALONADO_FIJO.getCode()))
							|| (serviceRate.getRateType().equals(RateType.ESCALONADO_MIXTO.getCode()))
							|| (serviceRate.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode()))) {

						tempAmount = BillingUtils.addTwoDecimal(collectionRecord.getCollectionAmount(),
								collectionRecordDetail.getRateAmount(), GeneralConstants.ROUNDING_DIGIT_NUMBER);

					}

					collectionRecord.setCollectionAmount(tempAmount);
				}
				// Caso especial para Emision de DPF
				if (BaseCollectionType.RECORD_SECURITIES_DEMATERIALIZED_DPF.getCode()
						.equals(billingService.getBaseCollection())) {
					collectionRecord.setCollectionAmount(collectionSafi.getCollectionAmount());

				}

				/**
				 * APPLLIED RATE MINIMUM
				 */
				BigDecimal amountNew = businessLayerCommonFacade.appliedMinimumAmountCalculation(serviceRate,
						collectionRecord.getCollectionAmount(), parameters);

				businessLayerCommonFacade.collectionToMinimumBilling(collectionRecord, parameters);
				collectionRecord.setCollectionAmount(amountNew);

				/** ONLY IF SERVICE APPLY TAXED **/
				if (billingService.getTaxApplied().equals(PropertiesConstants.YES_APPLY_TAXED)) {

					ParameterTable parameterTableTax = billingServiceMgmtServiceFacade
							.getParameterTableById(PropertiesConstants.TAX_PK);
					Double taxed = (parameterTableTax != null) ? parameterTableTax.getDecimalAmount1()
							: new Double(GeneralConstants.ZERO_VALUE_STRING);

					BigDecimal taxedAmount = BillingUtils.multiplyDecimals(collectionRecord.getCollectionAmount(),
							new BigDecimal(taxed.toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
					collectionRecord.setTaxApplied(taxedAmount);

					BigDecimal grossAmount = BillingUtils.addTwoDecimal(collectionRecord.getCollectionAmount(),
							taxedAmount, GeneralConstants.ROUNDING_DIGIT_NUMBER);

					/** SETTING FINALLY COLLECTION AMOUNT **/
					collectionRecord.setGrossAmount(grossAmount);

				} else {
					collectionRecord.setTaxApplied(new BigDecimal(GeneralConstants.ZERO_VALUE_STRING));
					collectionRecord.setGrossAmount(collectionRecord.getCollectionAmount());
				}

				/**
				 * Save To Billing Calculation
				 */
				businessLayerCommonFacade.billingChargesToConvertCurrency(collectionRecord, parameters);
			}

		}
		return collectionRecordList;
	}

	/**
	 * Adds the entity collection.
	 *
	 * @param collectionRecord   the collection record
	 * @param collectionRecordTo the collection record to
	 */
	protected void addEntityCollection(CollectionRecord collectionRecord, CollectionRecordTo collectionRecordTo) {

		Integer typeEntity = collectionRecordTo.getEntityCollection();
		if (typeEntity.equals(EntityCollectionType.HOLDERS.getCode())) {
			Holder holder = new Holder();
			holder.setIdHolderPk((Long) collectionRecordTo.getEntityCollection(typeEntity));
			collectionRecord.setHolder(holder);
		} else if (typeEntity.equals(EntityCollectionType.ISSUERS.getCode())) {
			Issuer issuer = new Issuer();
			issuer.setIdIssuerPk((String) collectionRecordTo.getEntityCollection(typeEntity));
			collectionRecord.setIssuer(issuer);
		} else if (typeEntity.equals(EntityCollectionType.PARTICIPANTS.getCode())) {
			Participant participant = new Participant();
			participant.setIdParticipantPk((Long) collectionRecordTo.getEntityCollection(typeEntity));
			collectionRecord.setParticipant(participant);
		} else if (typeEntity.equals(EntityCollectionType.AFP.getCode())) {
			Participant participant = new Participant();
			participant.setIdParticipantPk((Long) collectionRecordTo.getEntityCollection(typeEntity));
			collectionRecord.setParticipant(participant);
		} else if (typeEntity.equals(EntityCollectionType.CENTRAL_BANK.getCode())) {
			collectionRecord.setOtherEntityDocument(EntityCollectionType.CENTRAL_BANK.getCode().toString());
			collectionRecord.setOtherEntityDescription(EntityCollectionType.CENTRAL_BANK.getValue());
		} else if (typeEntity.equals(EntityCollectionType.HACIENDA.getCode())) {
			collectionRecord.setOtherEntityDocument(EntityCollectionType.HACIENDA.getCode().toString());
			collectionRecord.setOtherEntityDescription(EntityCollectionType.HACIENDA.getValue());
		} else if (typeEntity.equals(EntityCollectionType.STOCK_EXCHANGE.getCode())) {
			collectionRecord.setOtherEntityDocument(EntityCollectionType.STOCK_EXCHANGE.getCode().toString());
			collectionRecord.setOtherEntityDescription(EntityCollectionType.STOCK_EXCHANGE.getValue());
		} else if (typeEntity.equals(EntityCollectionType.OTHERS.getCode())) {
			collectionRecord.setOtherEntityDocument(null);
			collectionRecord.setOtherEntityDescription(null);
		}

	}

	/**
	 * Find entity at list.
	 *
	 * @param collectionRecordList the collection record list
	 * @param entity               the entity
	 * @param typeEntity           the type entity
	 * @return the collection record
	 */
	protected CollectionRecord findEntityAtList(List<CollectionRecord> collectionRecordList, Object entity,
			Integer typeEntity) {

		if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordList)) {

			for (CollectionRecord collectionRecord : collectionRecordList) {

				Boolean exist = false;
				if (typeEntity.equals(EntityCollectionType.HOLDERS.getCode())) {
					exist = collectionRecord.getHolder().getIdHolderPk().equals((Long) entity);
				} else if (typeEntity.equals(EntityCollectionType.ISSUERS.getCode())) {
					exist = collectionRecord.getIssuer().getIdIssuerPk().equals((String) entity);
				} else if (typeEntity.equals(EntityCollectionType.PARTICIPANTS.getCode())) {
					exist = collectionRecord.getParticipant().getIdParticipantPk().equals((Long) entity);
				}

				if (exist) {
					return collectionRecord;
				}
			}
		}

		return null;
	}

	/**
	 * Find entity at list for Safi.
	 *
	 * @param collectionRecordList the collection record list
	 * @param entity               the entity
	 * @param typeEntity           the type entity
	 * @return the collection record
	 */
	protected CollectionRecord findEntityAtListSafi(List<CollectionRecord> collectionRecordList, Object entity,
			Integer typeEntity, String safi) {

		if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordList)) {

			for (CollectionRecord collectionRecord : collectionRecordList) {

				Boolean exist = false;
				if (typeEntity.equals(EntityCollectionType.HOLDERS.getCode())) {
					exist = collectionRecord.getHolder().getIdHolderPk().equals((Long) entity)
							&& collectionRecord.getMnemonicConcatenated().equals(safi);
				} else if (typeEntity.equals(EntityCollectionType.ISSUERS.getCode())) {
					exist = collectionRecord.getIssuer().getIdIssuerPk().equals((String) entity)
							&& collectionRecord.getMnemonicConcatenated().equals(safi);
				} else if (typeEntity.equals(EntityCollectionType.PARTICIPANTS.getCode())) {
					exist = collectionRecord.getParticipant().getIdParticipantPk().equals((Long) entity)
							&& collectionRecord.getMnemonicConcatenated().equals(safi);
				}

				if (exist) {
					return collectionRecord;
				}
			}
		}

		return null;
	}

	/**
	 * Find entity at list temporal to.
	 *
	 * @param collectionRecordToList the collection record to list
	 * @param entity                 the entity
	 * @param typeEntity             the type entity
	 * @return the collection record to
	 */
	protected CollectionRecordTo findEntityAtListTemporalTo(List<CollectionRecordTo> collectionRecordToList,
			Object entity, BillingService billingService) {

		if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordToList)) {

			for (CollectionRecordTo collectionRecordTo : collectionRecordToList) {
				if (baseCollectionMgmtServiceBean.isPaymentByparticipant(billingService)) {
					entity = baseCollectionMgmtServiceBean.getIdParticipantPkByIdIssuerPk(entity.toString());
				}
				if (collectionRecordTo.existEntityCollection(entity, billingService.getEntityCollection())) {
					return collectionRecordTo;
				}
			}
		}

		return null;

	}

	/**
	 * Load collection record detail.
	 *
	 * @param collectionRecordDetailToList the collection record detail to list
	 * @param serviceRate                  the service rate
	 * @param parameters                   the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	protected List<CollectionRecordDetail> loadCollectionRecordDetail(
			List<CollectionRecordDetailTo> collectionRecordDetailToList, ServiceRate serviceRate,
			Map<String, Object> parameters) throws ServiceException {

		List<CollectionRecordDetail> newCollectionRecordDetailList = new ArrayList<CollectionRecordDetail>();

		if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordDetailToList)) {

			for (CollectionRecordDetailTo collectionRecordDetailTo : collectionRecordDetailToList) {

				CollectionRecordDetail newCollectionRecordDetail = new CollectionRecordDetail();
				newCollectionRecordDetail.setServiceRate(serviceRate);
				BigDecimal negotiatedAmountOrigin = collectionRecordDetailTo.getNegotiatedAmountOrigin();
				BigDecimal negotiatedAmount = collectionRecordDetailTo.getNegotiatedAmount();

				if (serviceRate.getRateType().equals(RateType.PORCENTUAL.getCode())) {

					BigDecimal percentAmount = BigDecimal.ZERO;
					BigDecimal grossAmount = collectionRecordDetailTo
							.getPercentage();/** getting amount for apply factor **/
					BigDecimal ratePercent = serviceRate.getRatePercent();

					/** Applied Percentage on Amount Gross **/
					percentAmount = BillingUtils.multiplyDecimals(grossAmount, ratePercent,
							GeneralConstants.ROUNDING_DIGIT_NUMBER_FIVE);
					/** Applied Billing Currency */
					businessLayerCommonFacade.convertCalculationToBilled(percentAmount, serviceRate.getCurrencyRate(),
							newCollectionRecordDetail, parameters, DailyExchangeRoleType.SELL.getCode());
					newCollectionRecordDetail.setRatePercent(percentAmount);
					newCollectionRecordDetail.setCalculationDate(collectionRecordDetailTo.getCalculationDate());

				} else if (serviceRate.getRateType().equals(RateType.FIJO.getCode())) {

					BigDecimal rateAmount = serviceRate.getRateAmount();
					BigDecimal temporalAmount = BillingUtils.addTwoDecimal(rateAmount,
							newCollectionRecordDetail.getRateAmount(), GeneralConstants.ROUNDING_DIGIT_NUMBER);

					/** Applied Billing Currency */
					businessLayerCommonFacade.convertCalculationToBilled(temporalAmount, serviceRate.getCurrencyRate(),
							newCollectionRecordDetail, parameters, DailyExchangeRoleType.SELL.getCode());

					newCollectionRecordDetail.setRateAmount(temporalAmount);

				} else if (serviceRate.getRateType().equals(RateType.ESCALONADO_FIJO.getCode())) {
					BigDecimal temporalAmount = BillingUtils.addTwoDecimal(collectionRecordDetailTo.getAmount(),
							newCollectionRecordDetail.getRateAmount(), GeneralConstants.ROUNDING_DIGIT_NUMBER);

					/** Applied Billing Currency */
					businessLayerCommonFacade.convertCalculationToBilled(temporalAmount, serviceRate.getCurrencyRate(),
							newCollectionRecordDetail, parameters, DailyExchangeRoleType.SELL.getCode());
					newCollectionRecordDetail.setRateAmount(temporalAmount);

				} else if (serviceRate.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode())) {
					BigDecimal temporalAmount = BillingUtils.addTwoDecimal(collectionRecordDetailTo.getAmount(),
							newCollectionRecordDetail.getRateAmount(), GeneralConstants.ROUNDING_DIGIT_NUMBER);

					/** Applied Billing Currency */
					businessLayerCommonFacade.convertCalculationToBilled(temporalAmount, serviceRate.getCurrencyRate(),
							newCollectionRecordDetail, parameters, DailyExchangeRoleType.SELL.getCode());
					newCollectionRecordDetail.setRateAmount(temporalAmount);

				} else if (serviceRate.getRateType().equals(RateType.ESCALONADO_MIXTO.getCode())) {
					BigDecimal temporalAmount = BillingUtils.addTwoDecimal(collectionRecordDetailTo.getAmount(),
							newCollectionRecordDetail.getRateAmount(), GeneralConstants.ROUNDING_DIGIT_NUMBER);

					/** Applied Billing Currency */
					businessLayerCommonFacade.convertCalculationToBilled(temporalAmount, serviceRate.getCurrencyRate(),
							newCollectionRecordDetail, parameters, DailyExchangeRoleType.SELL.getCode());
					newCollectionRecordDetail.setRateAmount(temporalAmount);

				}

				/**
				 * More Detail
				 */
				if (Validations.validateIsNotNullAndNotEmpty(collectionRecordDetailTo.getIdSecurityCodeFk())) {
					newCollectionRecordDetail.setIdSecurityCodeFk(collectionRecordDetailTo.getIdSecurityCodeFk());
				}
				if (Validations.validateIsNotNullAndNotEmpty(collectionRecordDetailTo.getSecurityClass())) {
					newCollectionRecordDetail.setSecurityClass(collectionRecordDetailTo.getSecurityClass());
				}
				if (Validations.validateIsNotNullAndNotEmpty(collectionRecordDetailTo.getIdHolderPk())) {
					newCollectionRecordDetail.setIdHolder(collectionRecordDetailTo.getIdHolderPk());
				}
				if (Validations.validateIsNotNullAndNotEmpty(collectionRecordDetailTo.getCurrencyOrigin())) {
					newCollectionRecordDetail.setCurrencyOrigin(collectionRecordDetailTo.getCurrencyOrigin());
				}
				if (Validations.validateIsNotNullAndNotEmpty(negotiatedAmountOrigin)) {
					newCollectionRecordDetail.setNegotiatedAmountOrigin(negotiatedAmountOrigin);
				}
				if (Validations.validateIsNotNullAndNotEmpty(negotiatedAmount)) {
					newCollectionRecordDetail.setNegotiatedAmount(negotiatedAmount);
				}
				if (Validations.validateIsNotNullAndPositive(collectionRecordDetailTo.getNominalValue())) {
					newCollectionRecordDetail.setNominalValue(collectionRecordDetailTo.getNominalValue());
				}
				if (Validations.validateIsNotNullAndPositive(collectionRecordDetailTo.getMovementQuantity())) {
					newCollectionRecordDetail.setMovementQuantity(collectionRecordDetailTo.getMovementQuantity());
				}
				if (Validations.validateIsNotNullAndPositive(collectionRecordDetailTo.getRole())) {
					newCollectionRecordDetail.setRole(collectionRecordDetailTo.getRole());
				}
				if (Validations.validateIsNotNullAndNotEmpty(collectionRecordDetailTo.getIdIssuanceCodePk())) {
					newCollectionRecordDetail.setIdIssuanceCodeFk(collectionRecordDetailTo.getIdIssuanceCodePk());
				}
				if (Validations.validateIsNotNullAndNotEmpty(collectionRecordDetailTo.getOperationNumber())) {
					newCollectionRecordDetail.setOperationNumber(collectionRecordDetailTo.getOperationNumber());
				}
				if (Validations.validateIsNotNullAndNotEmpty(collectionRecordDetailTo.getIdIssuerFk())) {
					newCollectionRecordDetail.setIdIssuer(collectionRecordDetailTo.getIdIssuerFk());
				}
				if (Validations.validateIsNotNullAndNotEmpty(collectionRecordDetailTo.getHolderAccount())) {
					newCollectionRecordDetail.setHolderAccount(collectionRecordDetailTo.getHolderAccount());
				}
				if (Validations.validateIsNotNullAndNotEmpty(collectionRecordDetailTo.getMnemonicHolder())) {
					newCollectionRecordDetail.setMnemonicHolder(collectionRecordDetailTo.getMnemonicHolder());
				}
				if (Validations
						.validateIsNotNullAndNotEmpty(collectionRecordDetailTo.getEconomicActivityParticipant())) {
					newCollectionRecordDetail
							.setEconomicActivityParticipant(collectionRecordDetailTo.getEconomicActivityParticipant());
				}
				if (Validations.validateIsNotNullAndNotEmpty(collectionRecordDetailTo.getPortfolio())) {
					newCollectionRecordDetail.setPortfolio(collectionRecordDetailTo.getPortfolio());
				}
				newCollectionRecordDetailList.add(newCollectionRecordDetail);
			}
		} else {
			CollectionRecordDetail newCollectionRecordDetail = new CollectionRecordDetail();

			if (serviceRate.getRateType().equals(RateType.FIJO.getCode())) {
				newCollectionRecordDetail.setServiceRate(serviceRate);

				BigDecimal rateAmount = serviceRate.getRateAmount();
				BigDecimal temporalAmount = BigDecimal.ZERO;
				temporalAmount = BillingUtils.addTwoDecimal(rateAmount, newCollectionRecordDetail.getRateAmount(),
						GeneralConstants.ROUNDING_DIGIT_NUMBER);

				newCollectionRecordDetail.setRateAmount(temporalAmount);

			}
			newCollectionRecordDetailList.add(newCollectionRecordDetail);
		}

		return newCollectionRecordDetailList;
	}
	
	
	public List<CollectionRecordTo> gettingDataForCalculatingByQuantity(Map<String, Object> parameters) {

		List<CollectionRecordTo> collectionRecordToList = null;

		Date calculateDate = (Date) parameters.get(GeneralConstants.CALCULATION_DATE);

		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);

		List<Object> dataMaps = new ArrayList<>();
		
		try {
			dataMaps = baseCollectionMgmtServiceFacade.processExceptionBaseRemote(parameters);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (Validations.validateListIsNotNullAndNotEmpty(dataMaps)) {
			collectionRecordToList = new ArrayList<CollectionRecordTo>();
			for (Object object : dataMaps) {
				Object entity = ((Object[]) object)[0];
				String security = (String) ((Object[]) object)[1];
				Date movementDate = (Date) (((Object[]) object)[2]);
				Long operationNumber = Long.parseLong(((BigDecimal) ((Object[]) object)[3]).toString());
				Long typeMovement = Long.parseLong(((BigDecimal) ((Object[]) object)[4]).toString());
				Integer numMovement = Integer.parseInt((((Object[]) object)[5]).toString());
				
				CollectionRecordDetailTo collectionRecordDetailTo = new CollectionRecordDetailTo();
				collectionRecordDetailTo.setIdSecurityCodeFk(security);
				collectionRecordDetailTo.setIdMovementTypePk(typeMovement);
				collectionRecordDetailTo.setMovementCount(numMovement);
				collectionRecordDetailTo.setCalculationDate(calculateDate);
				collectionRecordDetailTo.setOperationNumber(operationNumber);
				collectionRecordDetailTo.setMovementDate(movementDate);

				CollectionRecordTo collectionRecordTo = findEntityAtListTemporalTo(collectionRecordToList, entity, billingService);
				if (collectionRecordTo == null) {
					collectionRecordTo = new CollectionRecordTo();
					if (baseCollectionMgmtServiceBean.isPaymentByparticipant(billingService)) {
						entity = baseCollectionMgmtServiceBean.getIdParticipantPkByIdIssuerPk(entity.toString());
						collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());
					} else {
						collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());
					}

					List<CollectionRecordDetailTo> collectionRecordDetailToList = new ArrayList<CollectionRecordDetailTo>();
					collectionRecordDetailToList.add(collectionRecordDetailTo);
					collectionRecordToList.add(collectionRecordTo);
					collectionRecordTo.setCollectionRecordDetailToList(collectionRecordDetailToList);
				} else {
					collectionRecordTo.getCollectionRecordDetailToList().add(collectionRecordDetailTo);
				}

			}

		}
		return collectionRecordToList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pradera.billing.process.service.CollectionBasicService#
	 * gettingDataForCalculatingFixed(java.util.Map)
	 */
	@Override
	public List<CollectionRecordTo> gettingDataForCalculatingFixed(Map<String, Object> parameters)
			throws ServiceException {

		List<CollectionRecordTo> collectionRecordToList = null;

		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);

		if (BaseCollectionType.DIRECT_PARTICIPANT.getCode().equals(billingService.getBaseCollection())
				|| BaseCollectionType.INDIRECT_PARTICIPANT.getCode().equals(billingService.getBaseCollection())) {
			collectionRecordToList = baseCollectionMgmtServiceFacade.findRegisteredAccount(parameters);
		} else if (BaseCollectionType.ACCOUNT_MODIFICATION.getCode().equals(billingService.getBaseCollection())) {
			collectionRecordToList = baseCollectionMgmtServiceFacade.findHolderForQuota(parameters);
		}
		/** getting data with or without exceptions 
		if (BaseCollectionType.ACCOUNT_MAINTENANCE_ISSUER.getCode().equals(billingService.getBaseCollection())
				|| BaseCollectionType.DIRECT_PARTICIPANT.getCode().equals(billingService.getBaseCollection())) {
			collectionRecordToList = baseCollectionMgmtServiceFacade.findEntitiesForQuotaAnnual(parameters);
		} else if (BaseCollectionType.CREATION_CUI.getCode().equals(billingService.getBaseCollection())) {
			collectionRecordToList = baseCollectionMgmtServiceFacade.findHolderForQuota(parameters);
		} else if (BaseCollectionType.CREATION_ISIN_CODE.getCode().equals(billingService.getBaseCollection())) {
			collectionRecordToList = baseCollectionMgmtServiceFacade.findSecurityForQuota(parameters);
		} else if (BaseCollectionType.ISSUANCE_CAT.getCode().equals(billingService.getBaseCollection())) {
			collectionRecordToList = baseCollectionMgmtServiceFacade.findAccreditationCertificate(parameters);
		}
*/
		else if (BaseCollectionType.PARTICIPANT_REGISTER_DIRECT_THIRD_AND_SELF.getCode().equals(billingService.getBaseCollection())) {
			collectionRecordToList = baseCollectionMgmtServiceFacade.findMonthlyRegisterParticipantForQuota(parameters, ParticipantType.DIRECT.getCode(), BooleanType.YES.getCode());
		}
		else if (BaseCollectionType.PARTICIPANT_REGISTER_INDIRECT_THIRD_AND_SELF.getCode().equals(billingService.getBaseCollection())) {
			collectionRecordToList = baseCollectionMgmtServiceFacade.findMonthlyRegisterParticipantForQuota(parameters, ParticipantType.INDIRECT.getCode(), BooleanType.YES.getCode());
		}
		else if (BaseCollectionType.PARTICIPANT_REGISTER_DIRECT_SELF.getCode().equals(billingService.getBaseCollection())) {
			collectionRecordToList = baseCollectionMgmtServiceFacade.findMonthlyRegisterParticipantForQuota(parameters, ParticipantType.DIRECT.getCode(), BooleanType.NO.getCode());
		}
		else if (BaseCollectionType.PARTICIPANT_REGISTER_INDIRECT_SELF.getCode().equals(billingService.getBaseCollection())) {
			collectionRecordToList = baseCollectionMgmtServiceFacade.findMonthlyRegisterParticipantForQuota(parameters, ParticipantType.INDIRECT.getCode(), BooleanType.NO.getCode());
		}
		return collectionRecordToList;
	}

//	 
	/**
	 * Calculos para las tarifas porcentuales.
	 *
	 * @param parameters the parameters
	 * @return the ting data for calculating percentage
	 * @throws ServiceException the service exception
	 */
	@Override
	public List<CollectionRecordTo> gettingDataForCalculatingPercentage(Map<String, Object> parameters)
			throws ServiceException {
		List<CollectionRecordTo> collectionRecordToList = null;
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		Integer baseCollection = billingService.getBaseCollection();
		/** getting data with or without exceptions */
		/*** 1 **/
		if (BaseCollectionType.RECORD_BOOK_ENTRY_PHYSICAL_CUSTODY_USD.getCode().equals(baseCollection)
				|| BaseCollectionType.RECORD_BOOK_ENTRY_PHYSICAL_CUSTODY_LOCAL.getCode().equals(baseCollection)
				|| BaseCollectionType.RECORD_BOOK_ENTRY_DESMATERIALIZED_CUSTODY.getCode().equals(baseCollection))
				//|| BaseCollectionType.RECORD_BOOK_ENTRY_SECURITIES_VARIABLE_INCOME.getCode().equals(baseCollection)
				//|| BaseCollectionType.RECORD_BOOK_ENTRY_SECURITIES_VARIABLE_INCOME_AFP.getCode()
				//		.equals(baseCollection)) 
				{
			ParameterTable instrumentType = billingServiceMgmtServiceFacade.getParameterTableById(baseCollection);		
			collectionRecordToList = baseCollectionMgmtServiceFacade.findBalanceSecurityAnnotationOnAccount(parameters,
					null);
		} else if (BaseCollectionType.DIRECT_PARTICIPANT.getCode().equals(baseCollection)) {
			collectionRecordToList = baseCollectionMgmtServiceFacade.findRegisteredAccount(parameters);
		}
		
		//TODO REMOVER TODO ABAJO

		/*** 2 */
		else if (BaseCollectionType.RECORD_NOT_PLACED_SECURITIES_FIXED_INCOME.getCode().equals(baseCollection)
				|| BaseCollectionType.RECORD_NOT_PLACED_SECURITIES_VARIABLE_INCOME.getCode().equals(baseCollection)) {
			ParameterTable instrumentType = billingServiceMgmtServiceFacade.getParameterTableById(baseCollection);
			Integer idInstrumentType = instrumentType.getShortInteger();
			collectionRecordToList = baseCollectionMgmtServiceFacade.findBalanceSecurityNotPlaced(parameters,
					idInstrumentType);
		}

		/*** 3 */
		else if (BaseCollectionType.CUSTODY_SECURITIES_PHYSICAL.getCode().equals(baseCollection)
				|| BaseCollectionType.CUSTODY_SECURITIES_PHYSICAL_AFP.getCode().equals(baseCollection)) {
			ParameterTable instrumentType = billingServiceMgmtServiceFacade.getParameterTableById(baseCollection);
			Integer idInstrumentType = instrumentType.getShortInteger();
			collectionRecordToList = baseCollectionMgmtServiceFacade.findPhysicalCustodyOfSecurities(parameters,
					idInstrumentType);
		}

		/*** 24 */
		else if (BaseCollectionType.RECORD_BOOK_ENTRY_DESMATERIALIZED_CUSTODY.getCode()
				.equals(baseCollection)
				|| BaseCollectionType.CUSTODY_SECURITIES_PHYSICAL_PRIVATE_OFFERT_VARIABLE_INCOME.getCode()
						.equals(baseCollection)) {
			ParameterTable instrumentType = billingServiceMgmtServiceFacade.getParameterTableById(baseCollection);
			Integer idInstrumentType = instrumentType.getShortInteger();
			collectionRecordToList = baseCollectionMgmtServiceFacade.findPhysicalCustodyOfSecuritiesPrivate(parameters,
					idInstrumentType);
		}
		return collectionRecordToList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pradera.billing.process.service.CollectionBasicService#
	 * gettingDataForCalculatingEstaggeredFixed(java.util.Map)
	 */
	@Override
	public List<CollectionRecordTo> gettingDataForCalculatingEstaggeredFixed(Map<String, Object> parameters)
			throws ServiceException {

		List<CollectionRecordTo> collectionRecordToList = null;

		/** Aqui hacer la verificacion **/

		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		/** getting data with or without exceptions */
		if (BaseCollectionType.TRANSFERS_SECURITIES_SIRTEX_FIXED_INCOME.getCode()
					.equals(billingService.getBaseCollection())
				|| BaseCollectionType.TRANSFERS_SECURITIES_SIRTEX_VARIABLE_INCOME.getCode()
					.equals(billingService.getBaseCollection()) 
				|| BaseCollectionType.RECORD_BOOK_ENTRY_PHYSICAL_CUSTODY_USD.getCode()
					.equals(billingService.getBaseCollection())
				|| BaseCollectionType.RECORD_BOOK_ENTRY_PHYSICAL_CUSTODY_LOCAL.getCode()
					.equals(billingService.getBaseCollection())
				|| BaseCollectionType.RECORD_BOOK_ENTRY_DESMATERIALIZED_CUSTODY.getCode()
					.equals(billingService.getBaseCollection())		
				) {
			/** 8 TRANSFERENCIAS VALORES SIRTEX **/
			collectionRecordToList = gettingDataForCalculatingForStaggered(parameters);
			parameters.put(GeneralConstants.COLLECTION_RECORD_MOVEMENT, collectionRecordToList);
			collectionRecordToList = baseCollectionMgmtServiceFacade
					.securitiesTransferStaggeredAccumulative(parameters);

		} else if (/*** 10 REGISTRO VALORES DESMATERIALIZADOS */
		BaseCollectionType.RECORD_SECURITIES_DEMATERIALIZED_DPF.getCode().equals(billingService.getBaseCollection())
				|| BaseCollectionType.RECORD_SECURITIES_DEMATERIALIZED_FIXED_INCOME.getCode()
						.equals(billingService.getBaseCollection())
				|| BaseCollectionType.RECORD_SECURITIES_DEMATERIALIZED_VARIABLE_INCOME.getCode()
						.equals(billingService.getBaseCollection())) {

			collectionRecordToList = gettingDataForCalculatingForStaggered(parameters);
			parameters.put(GeneralConstants.COLLECTION_RECORD_MOVEMENT, collectionRecordToList);
			collectionRecordToList = baseCollectionMgmtServiceFacade
					.securitiesTransferStaggeredAccumulative(parameters);

		} else if (/** 16 SERVICIO AGENTE PAGADOR */
		BaseCollectionType.PAYING_AGENT.getCode().equals(billingService.getBaseCollection())) {
			collectionRecordToList = gettingDataForCalculatingForStaggered(parameters);
			parameters.put(GeneralConstants.COLLECTION_RECORD_MOVEMENT, collectionRecordToList);
			collectionRecordToList = baseCollectionMgmtServiceFacade
					.securitiesTransferStaggeredAccumulative(parameters);
		}

		return collectionRecordToList;
	}

	/**
	 * Gets the ting data for calculating for staggered.
	 *
	 * @param parameters the parameters
	 * @return the ting data for calculating for staggered
	 * @throws ServiceException the service exception
	 */
	public List<CollectionRecordTo> gettingDataForCalculatingForStaggered(Map<String, Object> parameters)
			throws ServiceException {

		List<CollectionRecordTo> collectionRecordToList = null;

		Date calculateDate = (Date) parameters.get(GeneralConstants.CALCULATION_DATE);
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		List<Object> dataMaps = null;
		try {

			Integer baseCollection = billingService.getBaseCollection();
			
			// CUSTODIA FISICA
			if (BaseCollectionType.RECORD_BOOK_ENTRY_PHYSICAL_CUSTODY_USD.getCode().equals(billingService.getBaseCollection())) {
				ParameterTable parameterTable = billingServiceMgmtServiceFacade.getParameterTableById(baseCollection);
				dataMaps = baseCollectionMgmtServiceFacade.findBalancePhysicalCustody(parameters);
			}
			
			if (BaseCollectionType.RECORD_BOOK_ENTRY_PHYSICAL_CUSTODY_LOCAL.getCode().equals(billingService.getBaseCollection())) {
				ParameterTable parameterTable = billingServiceMgmtServiceFacade.getParameterTableById(baseCollection);
				dataMaps = baseCollectionMgmtServiceFacade.findBalancePhysicalCustody(parameters);
			}

			// CUSTODIA ELECTRONICA
			if (BaseCollectionType.RECORD_BOOK_ENTRY_DESMATERIALIZED_CUSTODY.getCode()
					.equals(billingService.getBaseCollection())) {
				ParameterTable parameterTable = billingServiceMgmtServiceFacade.getParameterTableById(baseCollection);
				dataMaps = baseCollectionMgmtServiceFacade.findBalanceSecurityAnnotationOnAccountDesmaterialized(parameters);
			}
			
			

			/** 10 EMISIONES RENTA FIJA Y VARIABLE *   1. CUSTODIA FISICA*/
			if (BaseCollectionType.RECORD_SECURITIES_DEMATERIALIZED_VARIABLE_INCOME.getCode()
					.equals(billingService.getBaseCollection())
					|| BaseCollectionType.RECORD_SECURITIES_DEMATERIALIZED_FIXED_INCOME.getCode()
							.equals(billingService.getBaseCollection())) {
				/** CONSIDERED THE NEGOTIATION MECHANISM **/

				ParameterTable parameterTable = billingServiceMgmtServiceFacade.getParameterTableById(baseCollection);
				Integer idInstrumentType = parameterTable.getShortInteger();

				dataMaps = baseCollectionMgmtServiceFacade.findIssuanceDematerializedAnnotationOnAccount(parameters,
						idInstrumentType, null);

				/** 10.3 EMISIONES DPF **/
			} else if (BaseCollectionType.RECORD_SECURITIES_DEMATERIALIZED_DPF.getCode()
					.equals(billingService.getBaseCollection())) {
				ParameterTable parameterTable = billingServiceMgmtServiceFacade.getParameterTableById(baseCollection);
				Integer idInstrumentType = parameterTable.getShortInteger();

				dataMaps = baseCollectionMgmtServiceFacade.findIssuanceDematerializedAnnotationDpf(parameters,
						idInstrumentType, null);
			}

			/** 16 **/
			else if (billingService.getBaseCollection().equals(BaseCollectionType.PAYING_AGENT.getCode())) {
				/** CONSIDERED THE NEGOTIATION MECHANISM **/
				ParameterTable parameterTable = billingServiceMgmtServiceFacade.getParameterTableById(baseCollection);
				Integer idInstrumentType = parameterTable.getShortInteger();

				dataMaps = baseCollectionMgmtServiceFacade.findPayingAgent(parameters, idInstrumentType, null);

			}
			/** 17 **/
			else if (billingService.getBaseCollection()
					.equals(BaseCollectionType.REGISTRATION_PRECAUTIONARY_MEASURES.getCode())) {

			}

		} catch (Exception e) {

			e.printStackTrace();
		}

		DailyExchangeRateFilter dailyExchangeRateFilter = new DailyExchangeRateFilter();
		dailyExchangeRateFilter
				.setFinalDate((calculateDate == null ? null : CommonsUtilities.truncateDateTime(calculateDate)));
		dailyExchangeRateFilter
				.setInitialDate((calculateDate == null ? null : CommonsUtilities.truncateDateTime(calculateDate)));
		dailyExchangeRateFilter.setIdSourceinformation(billingService.getSourceInformation());

		BigDecimal buyPrice = BigDecimal.ZERO;

		if (Validations.validateListIsNotNullAndNotEmpty(dataMaps)) {
			collectionRecordToList = new ArrayList<CollectionRecordTo>();
			//Map<Date, Map> listRangeDate = businessLayerCommonFacade.gettingExchangeByRangeDate(null, dataMaps, 4,
			//		parameters, DailyExchangeRoleType.BUY.getCode());
			//parameters.put(GeneralConstants.MATRIX_EXCHANGE_RATE_RANGE, listRangeDate);
			BigDecimal referencePrice = BigDecimal.ZERO;
			BigDecimal realAmount = BigDecimal.ZERO;
			 
			for (Object object : dataMaps) {
				Object     	entity 				= null;
				BigDecimal 	amount	   			= null;
				Integer    	currency          	= null;
			    Long 	   	holder				= null;
			    String 		security			= null;
			    Integer    	securityClass		= null;
			    Long 	   	holderAccount		= null;
			    Integer 	portfolio			= null;
			    
				if(BaseCollectionType.RECORD_BOOK_ENTRY_PHYSICAL_CUSTODY_USD.getCode().equals(billingService.getBaseCollection())){
					entity         		= ((Object[])object)[0];
					amount	   			= ((BigDecimal)((Object[])object)[1]);
					currency          	= (Integer)((Object[])object)[2];					
				} else {
					entity         		= ((Object[])object)[0];
					amount	   			= ((BigDecimal)((Object[])object)[1]);
					currency          	= Integer.parseInt(((BigDecimal)((Object[])object)[2]).toString());
				    holder				= Long.parseLong(((Object[])object)[3].toString());
				    security			= ((Object[])object)[4].toString();
				    securityClass		= Integer.parseInt(((BigDecimal)((Object[])object)[5]).toString());
				    holderAccount		= Long.parseLong(((Object[])object)[6].toString());
				    portfolio			= (BigDecimal)((Object[])object)[7]!=null? (((BigDecimal)((Object[])object)[7])).intValue():null;
				}
				

			    CollectionRecordDetailTo collectionRecordDetailTo = new CollectionRecordDetailTo();
			    
			    referencePrice=businessLayerCommonFacade.processExchange(parameters,currency,
			    		billingService.getCurrencyCalculation(),DailyExchangeRoleType.BUY.getCode());				    				
				/***ALL EXCHANGE RATE  -----> CONVERSION TO DOP  */
				realAmount =BillingUtils.multiplyDecimals(referencePrice, amount , GeneralConstants.ROUNDING_DIGIT_NUMBER);

				collectionRecordDetailTo = new CollectionRecordDetailTo();						
				collectionRecordDetailTo.setPercentage(realAmount);
				collectionRecordDetailTo.setIdSecurityCodeFk(security);
				collectionRecordDetailTo.setSecurityClass(securityClass);
				collectionRecordDetailTo.setIdHolderPk(holder);
				collectionRecordDetailTo.setCurrencyOrigin(currency);
				collectionRecordDetailTo.setNegotiatedAmount(realAmount);
				collectionRecordDetailTo.setNegotiatedAmountOrigin(amount);
				collectionRecordDetailTo.setCalculationDate(calculateDate);
				collectionRecordDetailTo.setHolderAccount(holderAccount);
				collectionRecordDetailTo.setMnemonicHolder(null);
				collectionRecordDetailTo.setEconomicActivityParticipant(null);
				collectionRecordDetailTo.setPortfolio(portfolio);
				/*
				Object entity = ((Object[]) object)[0];
				String security = ((Object[]) object)[1] == null ? "" : (String) ((Object[]) object)[1];
				Integer currency = Integer.parseInt((((Object[]) object)[2]).toString());
				BigDecimal operationPrice = new BigDecimal((((Object[]) object)[3]).toString());
				Date operationDate = (Date) (((Object[]) object)[4]);
				Long idSourceParticipant = ((Object[]) object)[5] == null ? 0
						: Long.parseLong((((Object[]) object)[5]).toString());
				Long idTargetParticipant = ((Object[]) object)[6] == null ? 0
						: Long.parseLong((((Object[]) object)[6]).toString());
				Integer securityClass = Integer.parseInt((((Object[]) object)[7]).toString());
				

			

				BigDecimal priceByQuantity = null;

				if (BaseCollectionType.RECORD_SECURITIES_DEMATERIALIZED_DPF.getCode()
						.equals(billingService.getBaseCollection())) {
					buyPrice = businessLayerCommonFacade.processExchangeToRange(parameters,
							CommonsUtilities.truncateDateTime(operationDate), currency,
							billingService.getCurrencyCalculation(), DailyExchangeRoleType.BUY.getCode());
				} else {
					/*** Este PriceByQuantity cambiar a la moneda de Calculo 
					buyPrice = businessLayerCommonFacade.processExchange(parameters, currency,
							billingService.getCurrencyCalculation(), DailyExchangeRoleType.BUY.getCode());
				}*/

				/*** ALL EXCHANGE RATE -----> CONVERSION TO BOB 
			
				priceByQuantity = BillingUtils.multiplyDecimals(buyPrice, operationPrice,
						GeneralConstants.ROUNDING_DIGIT_NUMBER);

				collectionRecordDetailTo.setIdSecurityCodeFk(security);
				collectionRecordDetailTo.setCurrencyOrigin(currency);
				collectionRecordDetailTo.setCurrencyRate(currency);
				collectionRecordDetailTo.setOperationPrice(operationPrice);
				collectionRecordDetailTo.setOperationDate(operationDate);
				collectionRecordDetailTo.setIdSourceParticipant(idSourceParticipant);
				collectionRecordDetailTo.setIdTargetParticipant(idTargetParticipant);
				collectionRecordDetailTo.setPriceByQuantity(priceByQuantity);
				collectionRecordDetailTo.setNegotiatedAmount(priceByQuantity);
				collectionRecordDetailTo.setNegotiatedAmountOrigin(operationPrice);
				collectionRecordDetailTo.setCalculationDate(operationDate);
				collectionRecordDetailTo.setIdIssuanceCodePk(emision);
				collectionRecordDetailTo.setSecurityClass(securityClass);
				collectionRecordDetailTo.setNominalValue(nominalValue);
				collectionRecordDetailTo.setMovementQuantity(movementQuantity);
				collectionRecordDetailTo.setRole(role);
				collectionRecordDetailTo.setOperationNumber(operationNumber);
				collectionRecordDetailTo.setIdHolderPk(idHolderPk);
				collectionRecordDetailTo.setMnemonicHolder(mnemonicHolder);
				collectionRecordDetailTo.setEconomicActivityParticipant(economicActivity);
				collectionRecordDetailTo.setPortfolio(portfolio);
				*/
				// if !exists entity to collectionRecordToList then add y create detail
				// else add detail
				CollectionRecordTo collectionRecordTo = findEntityAtListTemporalTo(collectionRecordToList, entity,
						billingService);
				if (collectionRecordTo == null) {
					collectionRecordTo = new CollectionRecordTo();

					if (baseCollectionMgmtServiceBean.isPaymentByparticipant(billingService)) {
						entity = baseCollectionMgmtServiceBean.getIdParticipantPkByIdIssuerPk(entity.toString());
						collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());
					} else {
						collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());
					}

					List<CollectionRecordDetailTo> collectionRecordDetailToList = new ArrayList<CollectionRecordDetailTo>();
					collectionRecordDetailToList.add(collectionRecordDetailTo);
					collectionRecordToList.add(collectionRecordTo);
					collectionRecordTo.setCollectionRecordDetailToList(collectionRecordDetailToList);
				} else {
					collectionRecordTo.getCollectionRecordDetailToList().add(collectionRecordDetailTo);
				}

			}

		}

		return collectionRecordToList;
	}

}
