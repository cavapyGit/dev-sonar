package com.pradera.billing.billingexception.invoice.to;
 

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.pradera.billing.billingservice.to.BillingServiceTo;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ExceptionInvoiceGenerationTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class ExceptionInvoiceGenerationTo implements Serializable {
	
	/** ExceptionInvoiceGenerationTo. */
	private static final long serialVersionUID = 1L;

	/** The id exception invoice pk. */
	private Long 						idExceptionInvoicePk;

	/** The exception date. */
	private Date 						exceptionDate;
	
	/** The exception state. */
	private Integer 					exceptionState;
	
	/** The type exception. */
	private Integer 					typeException;
	
	/** The ind select all. */
	private Integer 					indSelectAll;
	
	/** The billing service. */
	private BillingServiceTo 			billingService;
	
	/** The billing schedule. */
	private BillingScheduleTo 			billingSchedule;

	/** The description exception state. */
	private String 						descriptionExceptionState;
	
	/** The description type exception. */
	private String 						descriptionTypeException;
	
	/** The description entity collection. */
	private String 						descriptionEntityCollection;
	
	/** One or All. */
	private String 						descriptionIndSelection;

	/** The billing entity exception to selected. */
	private BillingEntityExceptionTo 	billingEntityExceptionToSelected;
	
	/** The billing entity exception. */
	private List<BillingEntityExceptionTo> billingEntityException;

	/**
	 * Instantiates a new exception invoice generation to.
	 */
	public ExceptionInvoiceGenerationTo() {
		billingEntityException= new ArrayList<BillingEntityExceptionTo>();
		billingSchedule=new BillingScheduleTo();
	}

	/**
	 * Instantiates a new exception invoice generation to.
	 *
	 * @param exceptionDate the exception date
	 * @param exceptionState the exception state
	 * @param typeException the type exception
	 * @param indSelectAll the ind select all
	 * @param billingService the billing service
	 * @param billingSchedule the billing schedule
	 * @param billingEntityException the billing entity exception
	 */
	public ExceptionInvoiceGenerationTo(Date exceptionDate,
			Integer exceptionState, Integer typeException,
			Integer indSelectAll, BillingServiceTo billingService,
			BillingScheduleTo billingSchedule,
			List<BillingEntityExceptionTo> billingEntityException) {
		this.exceptionDate = exceptionDate;
		this.exceptionState = exceptionState;
		this.typeException = typeException;
		this.indSelectAll = indSelectAll;
		this.billingService = billingService;
		this.billingSchedule = billingSchedule;
		this.billingEntityException = billingEntityException;
	}

	/**
	 * Instantiates a new exception invoice generation to.
	 *
	 * @param otherException the other exception
	 */
	public ExceptionInvoiceGenerationTo(ExceptionInvoiceGenerationTo otherException){
		this.exceptionDate = otherException.getExceptionDate();
		this.exceptionState = otherException.getExceptionState();
		this.typeException = otherException.getTypeException();
		this.indSelectAll = otherException.getIndSelectAll();
		this.billingService = otherException.getBillingService();
		this.billingSchedule = otherException.getBillingSchedule();
		this.billingEntityException = otherException.getBillingEntityException();
	}


	/**
	 * Gets the description exception state.
	 *
	 * @return the descriptionExceptionState
	 */
	public String getDescriptionExceptionState() {
		return descriptionExceptionState;
	}

	/**
	 * Sets the description exception state.
	 *
	 * @param descriptionExceptionState the descriptionExceptionState to set
	 */
	public void setDescriptionExceptionState(String descriptionExceptionState) {
		this.descriptionExceptionState = descriptionExceptionState;
	}

	/**
	 * Gets the description type exception.
	 *
	 * @return the descriptionTypeException
	 */
	public String getDescriptionTypeException() {
		return descriptionTypeException;
	}

	/**
	 * Sets the description type exception.
	 *
	 * @param descriptionTypeException the descriptionTypeException to set
	 */
	public void setDescriptionTypeException(String descriptionTypeException) {
		this.descriptionTypeException = descriptionTypeException;
	}

	/**
	 * Gets the id exception invoice pk.
	 *
	 * @return the idExceptionInvoicePk
	 */
	public Long getIdExceptionInvoicePk() {
		return idExceptionInvoicePk;
	}

	/**
	 * Sets the id exception invoice pk.
	 *
	 * @param idExceptionInvoicePk the idExceptionInvoicePk to set
	 */
	public void setIdExceptionInvoicePk(Long idExceptionInvoicePk) {
		this.idExceptionInvoicePk = idExceptionInvoicePk;
	}

	/**
	 * Gets the exception date.
	 *
	 * @return the exceptionDate
	 */
	public Date getExceptionDate() {
		return exceptionDate;
	}

	/**
	 * Sets the exception date.
	 *
	 * @param exceptionDate the exceptionDate to set
	 */
	public void setExceptionDate(Date exceptionDate) {
		this.exceptionDate = exceptionDate;
	}

	/**
	 * Gets the exception state.
	 *
	 * @return the exceptionState
	 */
	public Integer getExceptionState() {
		return exceptionState;
	}

	/**
	 * Sets the exception state.
	 *
	 * @param exceptionState the exceptionState to set
	 */
	public void setExceptionState(Integer exceptionState) {
		this.exceptionState = exceptionState;
	}

	/**
	 * Gets the type exception.
	 *
	 * @return the typeException
	 */
	public Integer getTypeException() {
		return typeException;
	}

	/**
	 * Sets the type exception.
	 *
	 * @param typeException the typeException to set
	 */
	public void setTypeException(Integer typeException) {
		this.typeException = typeException;
	}

	/**
	 * Gets the ind select all.
	 *
	 * @return the indSelectAll
	 */
	public Integer getIndSelectAll() {
		return indSelectAll;
	}

	/**
	 * Sets the ind select all.
	 *
	 * @param indSelectAll the indSelectAll to set
	 */
	public void setIndSelectAll(Integer indSelectAll) {
		this.indSelectAll = indSelectAll;
	}

	/**
	 * Gets the billing service.
	 *
	 * @return the billingService
	 */
	public BillingServiceTo getBillingService() {
		return billingService;
	}

	/**
	 * Sets the billing service.
	 *
	 * @param billingService the billingService to set
	 */
	public void setBillingService(BillingServiceTo billingService) {
		this.billingService = billingService;
	}

	/**
	 * Gets the billing schedule.
	 *
	 * @return the billingSchedule
	 */
	public BillingScheduleTo getBillingSchedule() {
		return billingSchedule;
	}

	/**
	 * Sets the billing schedule.
	 *
	 * @param billingSchedule the billingSchedule to set
	 */
	public void setBillingSchedule(BillingScheduleTo billingSchedule) {
		this.billingSchedule = billingSchedule;
	}

	/**
	 * Gets the billing entity exception.
	 *
	 * @return the billingEntityException
	 */
	public List<BillingEntityExceptionTo> getBillingEntityException() {
		return billingEntityException;
	}

	/**
	 * Sets the billing entity exception.
	 *
	 * @param billingEntityException the billingEntityException to set
	 */
	public void setBillingEntityException(
			List<BillingEntityExceptionTo> billingEntityException) {
		this.billingEntityException = billingEntityException;
	}

	/**
	 * Gets the billing entity exception to selected.
	 *
	 * @return the billingEntityExceptionToSelected
	 */
	public BillingEntityExceptionTo getBillingEntityExceptionToSelected() {
		return billingEntityExceptionToSelected;
	}

	/**
	 * Sets the billing entity exception to selected.
	 *
	 * @param billingEntityExceptionToSelected the billingEntityExceptionToSelected to set
	 */
	public void setBillingEntityExceptionToSelected(
			BillingEntityExceptionTo billingEntityExceptionToSelected) {
		this.billingEntityExceptionToSelected = billingEntityExceptionToSelected;
	}

	/**
	 * Gets the description entity collection.
	 *
	 * @return the descriptionEntityCollection
	 */
	public String getDescriptionEntityCollection() {
		return descriptionEntityCollection;
	}

	/**
	 * Sets the description entity collection.
	 *
	 * @param descriptionEntityCollection the descriptionEntityCollection to set
	 */
	public void setDescriptionEntityCollection(String descriptionEntityCollection) {
		this.descriptionEntityCollection = descriptionEntityCollection;
	}

	/**
	 * Gets the description ind selection.
	 *
	 * @return the descriptionIndSelection
	 */
	public String getDescriptionIndSelection() {
		return descriptionIndSelection;
	}

	/**
	 * Sets the description ind selection.
	 *
	 * @param descriptionIndSelection the descriptionIndSelection to set
	 */
	public void setDescriptionIndSelection(String descriptionIndSelection) {
		this.descriptionIndSelection = descriptionIndSelection;
	}

	
}