package com.pradera.billing.billingexception.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.Query;

import com.pradera.billing.billingexception.to.HolderAccountTo;
import com.pradera.billing.billingexception.to.HolderTo;
import com.pradera.billing.billingexception.to.IssuanceTo;
import com.pradera.billing.billingexception.to.RateExceptionTo;
import com.pradera.billing.billingrate.to.ServiceRateTo;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.billing.RateExclusion;
import com.pradera.model.billing.RateInclusion;
import com.pradera.model.billing.ServiceRate;
import com.pradera.model.billing.type.IndexExceptionType;
import com.pradera.model.billing.type.RateExceptionStatus;
import com.pradera.model.billing.type.RulerExceptionType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RateExceptionServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class RateExceptionServiceBean  extends CrudDaoServiceBean{
	
	/** The log. */
	@Inject
	private PraderaLogger 	log;
	
	/** The general parameters service. */
	@EJB
	ParameterServiceBean 	generalParametersService;
	
	/** The parameter service. */
	@EJB
	ParameterServiceBean 	parameterService;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade 	helperComponentFacade;

	/**
	 * Find rate exclusion list by id service rate pk.
	 *
	 * @param serviceRatePk the service rate pk
	 * @return the list
	 */
	public List<RateExclusion>   findRateExclusionListByIdServiceRatePk(Long serviceRatePk){
		
		StringBuilder stringBuilderSql = new StringBuilder();	
		stringBuilderSql.append("SELECT re FROM  RateExclusion re  ");
		
		
		stringBuilderSql.append(" WHERE 1=1   ");	
		
		if (Validations.validateIsNotNull(serviceRatePk)){
			 
			stringBuilderSql.append(" and re.serviceRate.idServiceRatePk = :serviceRatePk  ");
			stringBuilderSql.append(" and re.exclusionState = :exclusionState   ");
		}
		
		Query query = em.createQuery(stringBuilderSql.toString());
		
		if (Validations.validateIsNotNull(serviceRatePk)){
			 
			query.setParameter("serviceRatePk",serviceRatePk );
			query.setParameter("exclusionState",RateExceptionStatus.REGISTER.getCode() );
		}
		
		List<RateExclusion> listObject=null;
		try {
			listObject=query.getResultList();
		
		} catch (Exception e) {
			log.error(("findRateExclusionListByIdServiceRatePk"));
			log.debug(e.getMessage());
		}
		return listObject;
	}
	
	/**
	 * Find rate inclusion list by id service rate pk.
	 *
	 * @param serviceRatePk the service rate pk
	 * @return the list
	 */
	public List<RateInclusion>   findRateInclusionListByIdServiceRatePk(Long serviceRatePk){
		
		StringBuilder stringBuilderSql = new StringBuilder();	
		stringBuilderSql.append("SELECT ri FROM  RateInclusion ri  ");
		stringBuilderSql.append(" WHERE 1=1   ");	
		
		if (Validations.validateIsNotNull(serviceRatePk)){
			 
			stringBuilderSql.append(" and ri.serviceRate.idServiceRatePk = :serviceRatePk  ");
			stringBuilderSql.append(" and ri.inclusionState =:inclusionState  ");
		}
		
		Query query = em.createQuery(stringBuilderSql.toString());
		
		if (Validations.validateIsNotNull(serviceRatePk)){
			 
			query.setParameter("serviceRatePk",serviceRatePk );
			query.setParameter("inclusionState",RateExceptionStatus.REGISTER.getCode() );
		}
		
		List<RateInclusion> listObject=null;
		try {
			listObject=query.getResultList();
		
		} catch (Exception e) {
			log.error(("findRateInclusionListByIdServiceRatePk"));
			log.debug(e.getMessage());
		}
		return listObject;
	}
	
	/**
	 * Find rate exclusion list by id service rate pk.
	 *
	 * @param rateExceptionTo the rate exception to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<RateExclusion>   findRateExclusionListByIdServiceRatePk(RateExceptionTo rateExceptionTo)throws ServiceException{
		
		StringBuilder stringBuilderSql = new StringBuilder();	
		stringBuilderSql.append("SELECT re FROM  RateExclusion re  ");
		stringBuilderSql.append(" JOIN  Fetch re.serviceRate  ");
		  stringBuilderSql.append(" LEFT JOIN  Fetch re.securities  ");
		  stringBuilderSql.append(" LEFT JOIN  Fetch re.issuer  ");
		  stringBuilderSql.append(" LEFT JOIN  Fetch re.holder  ");
		  stringBuilderSql.append(" LEFT JOIN  Fetch re.holderAccount  ");
		  stringBuilderSql.append(" LEFT JOIN  Fetch re.participant  ");
		  stringBuilderSql.append(" LEFT JOIN  Fetch re.issuance  ");
		stringBuilderSql.append(" WHERE 1=1   ");	
		
		if (Validations.validateIsNotNull(rateExceptionTo.getIdServiceRate())){
			 
			stringBuilderSql.append(" and re.serviceRate.idServiceRatePk = :serviceRatePk  ");
		}
		
		if (Validations.validateIsNotNull(rateExceptionTo.getIdRateException())){
			 
			stringBuilderSql.append(" and re.idRateExclusionPk = :rateExclusionPk  ");
		}
		
		if ( Validations.validateIsNotNullAndPositive(rateExceptionTo.getTypeRule()) ){
			
			stringBuilderSql.append(" and re.exceptionRule = :typeRulePrm  ");
		}
		
		if (Validations.validateIsNotNull(rateExceptionTo.getDateRegister()) && Validations.validateIsNotNull(rateExceptionTo.getTodayDate()) ){
			 
			stringBuilderSql.append(" and re.exclusionDate BETWEEN :dateRegisterPrm and :todayDatePrm ");
		}
		
		Query query = em.createQuery(stringBuilderSql.toString());
		
		if (Validations.validateIsNotNull(rateExceptionTo.getIdServiceRate())){
			 
			query.setParameter("serviceRatePk",rateExceptionTo.getIdServiceRate() );
		}
		
		if (Validations.validateIsNotNull(rateExceptionTo.getIdRateException())){
			 
			query.setParameter("rateExclusionPk",rateExceptionTo.getIdRateException() );
		}
		
		if ( Validations.validateIsNotNullAndPositive(rateExceptionTo.getTypeRule()) ){
			
			query.setParameter("typeRulePrm",rateExceptionTo.getTypeRule() );
		}
		
		if (Validations.validateIsNotNull(rateExceptionTo.getDateRegister()) && Validations.validateIsNotNull(rateExceptionTo.getTodayDate()) ){
			 
			query.setParameter("dateRegisterPrm",rateExceptionTo.getDateRegister() );
			query.setParameter("todayDatePrm",rateExceptionTo.getTodayDate() );
		}
		
		List<RateExclusion> listObject=null;
		try {
			listObject=query.getResultList();
		
		} catch (Exception e) {
			log.error(("findRateExclusionListByIdServiceRatePk"));
			log.debug(e.getMessage());
		}
		return listObject;
	}
	
	/**
	 * Find rate inclusion list by id service rate pk.
	 *
	 * @param rateExceptionTo the rate exception to
	 * @return the list
	 */
	public List<RateInclusion>   findRateInclusionListByIdServiceRatePk(RateExceptionTo rateExceptionTo){
		
		StringBuilder stringBuilderSql = new StringBuilder();	
		stringBuilderSql.append("SELECT ri FROM  RateInclusion ri  ");
		  stringBuilderSql.append(" JOIN  Fetch ri.serviceRate  ");
		  stringBuilderSql.append(" LEFT JOIN  Fetch ri.securities  ");
		  stringBuilderSql.append(" LEFT JOIN  Fetch ri.issuer  ");
		  stringBuilderSql.append(" LEFT JOIN  Fetch ri.holder  ");
		  stringBuilderSql.append(" LEFT JOIN  Fetch ri.holderAccount  ");
		  stringBuilderSql.append(" LEFT JOIN  Fetch ri.participant  ");
		  stringBuilderSql.append(" LEFT JOIN  Fetch ri.issuance  ");
		  
		stringBuilderSql.append(" WHERE 1=1   ");	
		
		if (Validations.validateIsNotNull(rateExceptionTo.getIdServiceRate())){
			 
			stringBuilderSql.append(" and ri.serviceRate.idServiceRatePk = :serviceRatePk  ");
		}
		
		if (Validations.validateIsNotNull(rateExceptionTo.getIdRateException())){
			 
			stringBuilderSql.append(" and ri.idRateInclusionPk = :rateInclusionPk  ");
		}
		
		if ( Validations.validateIsNotNullAndPositive(rateExceptionTo.getTypeRule()) ){
			
			stringBuilderSql.append(" and ri.exceptionRule = :typeRulePrm  ");
		}
		
		if (Validations.validateIsNotNull(rateExceptionTo.getDateRegister()) && Validations.validateIsNotNull(rateExceptionTo.getTodayDate()) ){ 
			 
			stringBuilderSql.append(" and ri.inclusionDate BETWEEN :dateRegisterPrm and :todayDatePrm ");
		}
		
		Query query = em.createQuery(stringBuilderSql.toString());
		
		if (Validations.validateIsNotNull(rateExceptionTo.getIdServiceRate())){
			 
			query.setParameter("serviceRatePk",rateExceptionTo.getIdServiceRate() );
		}
		
		if (Validations.validateIsNotNull(rateExceptionTo.getIdRateException())){
			 
			query.setParameter("rateInclusionPk",rateExceptionTo.getIdRateException() );
		}
		
		if ( Validations.validateIsNotNullAndPositive(rateExceptionTo.getTypeRule()) ){
			
			query.setParameter("typeRulePrm",rateExceptionTo.getTypeRule() );
		}
		
		if (Validations.validateIsNotNull(rateExceptionTo.getDateRegister()) && Validations.validateIsNotNull(rateExceptionTo.getTodayDate()) ){
			 
			query.setParameter("dateRegisterPrm",rateExceptionTo.getDateRegister() );
			query.setParameter("todayDatePrm",rateExceptionTo.getTodayDate() );
		}
		List<RateInclusion> listObject=null;
		try {
			listObject=query.getResultList();
		
		} catch (Exception e) {
			log.error(("findRateInclusionListByIdServiceRatePk"));
			log.debug(e.getMessage());
		}
		
		return listObject;
	}
	

	/**
	 * Gets the atributtes rate inclusion.
	 *
	 * @param table the table
	 * @param stringBuilderBeginTableInclusion the string builder begin table inclusion
	 * @param rateInclusion the rate inclusion
	 * @param index the index
	 * @param parameters the parameters
	 * @return the atributtes rate inclusion
	 * @throws ServiceException the service exception
	 */
	public String getAtributtesRateInclusion(String table, StringBuilder stringBuilderBeginTableInclusion, 
											RateInclusion rateInclusion, int index, Map parameters) throws ServiceException{

		/** Count Exception Overflow*/
		int counter=(Integer)parameters.get(GeneralConstants.COUNTER_EXCEPTIONS);
		
		/** CLASS_VALOR **/
		if (rateInclusion.getExceptionRule().equals(RulerExceptionType.SECURITY_CLASS_EXCEPTION_RATE.getCode())){
			if(rateInclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ONE)){
				if (Validations.validateIsNotNull(rateInclusion.getSecurityClass())){
					
					if( index == 0 ){
						stringBuilderBeginTableInclusion.append(" and ( ");
					}
					else if( index > 0 ){
						stringBuilderBeginTableInclusion.append(" or ( ");
					}
						
					
					stringBuilderBeginTableInclusion.append("nvl ( ");
					stringBuilderBeginTableInclusion.append(table).
								append(".SECURITY_CLASS,0)=").
								append(rateInclusion.getSecurityClass());
					stringBuilderBeginTableInclusion.append(" ) ");
					counter++;
				}				
			}
			
			else if(rateInclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ALL)){
				
				/** GETS FROM DATABASE ALL SECURITY CLASSES CODES  */
				List<ParameterTable> paramTableSecurityClasses ;
				
				ParameterTableTO parameterTableTO = new ParameterTableTO();
				parameterTableTO.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());		
				paramTableSecurityClasses = generalParametersService.getListParameterTableServiceBean(parameterTableTO);
				
				
				int i=0;
				stringBuilderBeginTableInclusion.append(" and ( ");
				for (ParameterTable parameterTableTemp : paramTableSecurityClasses) {
					
					String condicional = (i==0?" ":" or ");
					
					stringBuilderBeginTableInclusion.append(condicional).append("  ( ");
					stringBuilderBeginTableInclusion.append("nvl ( ");
					stringBuilderBeginTableInclusion.append(table).
							append(".SECURITY_CLASS,0)=").
							append(parameterTableTemp.getParameterTablePk());
					stringBuilderBeginTableInclusion.append(" ) ");
					
					i++;
					counter++;
				}
				
				stringBuilderBeginTableInclusion.append(" ) ");				
							
			}
			
		/** VALOR **/	
		}else if (rateInclusion.getExceptionRule().equals(RulerExceptionType.SECURITY_EXCEPTION_RATE.getCode())){
			if(rateInclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ONE)){
				if (Validations.validateIsNotNull(rateInclusion.getSecurities())){
					
					if( index == 0 )
						stringBuilderBeginTableInclusion.append(" and ( ");
					else if( index > 0 )
						stringBuilderBeginTableInclusion.append(" or ( ");
					
					stringBuilderBeginTableInclusion.append("nvl ( ");
					stringBuilderBeginTableInclusion.append(table).
							append(".ID_SECURITY_CODE_FK,' ')='").
							append(rateInclusion.getSecurities().getIdSecurityCodePk()).
							append("'");
					stringBuilderBeginTableInclusion.append(" ) ");
					counter++;
				}
			}
			
			else if(rateInclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ALL)){
		
				List<Security> securityList = findAllSecurity();			
				
				int i=0;
				stringBuilderBeginTableInclusion.append(" and ( ");
				for (Security security : securityList) {
					
					String condicional = (i==0?" ":" or ");
					
					stringBuilderBeginTableInclusion.append(condicional).append("  ( ");
					stringBuilderBeginTableInclusion.append("nvl ( ");
					stringBuilderBeginTableInclusion.append(table).
							append(".ID_SECURITY_CODE_FK,' ')='").
							append(security.getIdSecurityCodePk()).
							append("'");
					stringBuilderBeginTableInclusion.append(" ) ");
					
					i++;
					counter++;
				}
				
				stringBuilderBeginTableInclusion.append(" ) ");	
				
			}	
			
		/** PARTICIPANT **/
		}else if(rateInclusion.getExceptionRule().equals(RulerExceptionType.PARTICIPANT_EXCEPTION_RATE.getCode())){
			if(rateInclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ONE)){				

				if (Validations.validateIsNotNull(rateInclusion.getParticipant())){
					
					if( index == 0 ){
						stringBuilderBeginTableInclusion.append(" and ( ");
					}
					else if( index > 0 ){
						stringBuilderBeginTableInclusion.append(" or ( ");
					}
						
					
					stringBuilderBeginTableInclusion.append(" ( ");
					stringBuilderBeginTableInclusion.append("nvl ( ");
					stringBuilderBeginTableInclusion.append(table).
							append(".ID_PARTICIPANT_FK,0)=").
							append(rateInclusion.getParticipant().getIdParticipantPk());
					stringBuilderBeginTableInclusion.append(" ) ");
					counter++;
				}
				
				if (Validations.validateIsNotNull(rateInclusion.getHolderAccount())){     
					stringBuilderBeginTableInclusion.append(" AND ( nvl (").
							append(table).append(".ID_HOLDER_ACCOUNT_FK,0)=").
							append(rateInclusion.getHolderAccount().getIdHolderAccountPk());
					stringBuilderBeginTableInclusion.append(" ) ");
					counter++;
				}
				
				if (Validations.validateIsNotNull(rateInclusion.getHolder())){
					stringBuilderBeginTableInclusion.append(" AND ( nvl (").
							append(table).
							append(".ID_HOLDER_FK,0)=").
							append(rateInclusion.getHolder().getIdHolderPk());
					stringBuilderBeginTableInclusion.append(" ) ");
					counter++;
				}
				
				if (Validations.validateIsNotNull(rateInclusion.getSecurities())){ 
					stringBuilderBeginTableInclusion.append(" AND ( nvl (").
							append(table).
							append(".ID_SECURITY_CODE_FK,' ')='").
							append(rateInclusion.getSecurities().getIdSecurityCodePk()).
							append("'");
					stringBuilderBeginTableInclusion.append(" ) ");
					counter++;
				}
				
				stringBuilderBeginTableInclusion.append(" ) ");	
				
			}else if(rateInclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ALL)){
				List<Participant>    listClientParticipant;
				
				Participant participantTemp=new Participant();						 
				listClientParticipant = helperComponentFacade.getLisParticipantServiceFacade(participantTemp);
			 				
				int i=0;
				stringBuilderBeginTableInclusion.append(" and ( ");
				for (Participant participant : listClientParticipant) {
					
					String condicional = (i==0?" ":" or ");
					
					stringBuilderBeginTableInclusion.append(condicional).append("  ( ");
					stringBuilderBeginTableInclusion.append("nvl ( ");
					stringBuilderBeginTableInclusion.
							append(table).
							append(".ID_PARTICIPANT_FK,0)=").
							append(participant.getIdParticipantPk());
					stringBuilderBeginTableInclusion.append(" ) ");
					
					i++;
					counter++;
				}
				
				stringBuilderBeginTableInclusion.append(" ) ");	
				
				
			}
			
		/** ISSUER **/
		}else if(rateInclusion.getExceptionRule().equals(RulerExceptionType.ISSUER_EXCEPTION_RATE.getCode())){
			if(rateInclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ONE)){
				
				if (Validations.validateIsNotNull(rateInclusion.getIssuer())){
					
					if( index == 0 ){
						stringBuilderBeginTableInclusion.append(" and ( ");
					}
					else if( index > 0 ){
						stringBuilderBeginTableInclusion.append(" or ( ");
					}
						
					
					stringBuilderBeginTableInclusion.append(" ( ");
					stringBuilderBeginTableInclusion.append("nvl ( ");
					stringBuilderBeginTableInclusion.
						append(table).
						append(".ID_ISSUER_FK,' ')='").
						append(rateInclusion.getIssuer().getIdIssuerPk()).
						append("'");
					stringBuilderBeginTableInclusion.append(" ) ");
					counter++;
				}
				
				if (Validations.validateIsNotNull(rateInclusion.getIssuance())){ 							
					stringBuilderBeginTableInclusion.append(" AND ( nvl (").
						append(table).
						append(".ID_ISSUANCE_CODE_FK,' ')='").
						append(rateInclusion.getIssuance().getIdIssuanceCodePk()).
						append("'");
					stringBuilderBeginTableInclusion.append(" ) ");
					counter++;
				}
				
				if (Validations.validateIsNotNull(rateInclusion.getSecurities())){ 
					stringBuilderBeginTableInclusion.append(" AND ( nvl (").
						append(table).
						append(".ID_SECURITY_CODE_FK,' ')='").
						append(rateInclusion.getSecurities().getIdSecurityCodePk()).
						append("'");
					stringBuilderBeginTableInclusion.append(" ) ");
					counter++;
				}
				
				stringBuilderBeginTableInclusion.append(" ) ");	
				
			}else if(rateInclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ALL)){
								
				List<Issuer> issuerList = this.findAllIssuer();

				int i=0;
				stringBuilderBeginTableInclusion.append(" and ( ");
				for (Issuer issuer : issuerList) {
					
					String condicional = (i==0?" ":" or ");
					
					stringBuilderBeginTableInclusion.append(condicional).append("  ( ");
					stringBuilderBeginTableInclusion.append("nvl ( ");
					stringBuilderBeginTableInclusion.append(table).
							append(".ID_ISSUER_FK,' ')='").
							append(issuer.getIdIssuerPk()).
							append("'");	
					stringBuilderBeginTableInclusion.append(" ) ");
					
					i++;
					counter++;
				}
				
				stringBuilderBeginTableInclusion.append(" ) ");	
				
				
			}
			
		/** HOLDER **/	
		}else if(rateInclusion.getExceptionRule().equals(RulerExceptionType.HOLDER_EXCEPTION_RATE.getCode())){
			if(rateInclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ONE)){
				
				if (Validations.validateIsNotNull(rateInclusion.getHolder())){
					
					if( index == 0 ){
						stringBuilderBeginTableInclusion.append(" and ( ");
					}
					else if( index > 0 ){
						stringBuilderBeginTableInclusion.append(" or ( ");
					}
						
					
					stringBuilderBeginTableInclusion.append(" ( ");
					stringBuilderBeginTableInclusion.append("nvl ( ");
					stringBuilderBeginTableInclusion.append(table).
							append(".ID_HOLDER_FK,0)=").
							append(rateInclusion.getHolder().getIdHolderPk());
					stringBuilderBeginTableInclusion.append(" ) ");
					counter++;
				}
				
				if (Validations.validateIsNotNull(rateInclusion.getHolderAccount())){					
					stringBuilderBeginTableInclusion.append(" AND ( nvl (").
							append(table).
							append(".ID_HOLDER_ACCOUNT_FK,0)=").
							append(rateInclusion.getHolderAccount().getIdHolderAccountPk());
					stringBuilderBeginTableInclusion.append(" ) ");
					counter++;
				}	
				
				  stringBuilderBeginTableInclusion.append(" ) ");	
				
			}else if(rateInclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ALL)){

				List<Holder> holderList = this.findAllHolder();

				int i=0;
				stringBuilderBeginTableInclusion.append(" and ( ");
				for (Holder holder : holderList) {
					
					String condicional = (i==0?" ":" or ");
					
					stringBuilderBeginTableInclusion.append(condicional).append("  ( ");
					stringBuilderBeginTableInclusion.append("nvl ( ").
							append(table).
							append(".ID_HOLDER_FK,0)=").
							append(holder.getIdHolderPk());		
					stringBuilderBeginTableInclusion.append(" ) ");
					
					i++;
					counter++;
				}
				
				stringBuilderBeginTableInclusion.append(" ) ");	
			}
				
		}
		
		parameters.put(GeneralConstants.COUNTER_EXCEPTIONS, counter);
		return stringBuilderBeginTableInclusion.toString();
	}
	
	
	/**
	 * Make Query To Rate Exclusion By Parameters.
	 *
	 * @param table the table
	 * @param stringBuilderBeginTableExclusion the string builder begin table exclusion
	 * @param rateExclusion the rate exclusion
	 * @param parameters the parameters
	 * @return the atributtes rate exclusion
	 * @throws ServiceException the service exception
	 */
	public String getAtributtesRateExclusion(String table,StringBuilder stringBuilderBeginTableExclusion  ,
											RateExclusion rateExclusion,
											Map parameters) throws ServiceException{

		
//		StringBuilder stringBuilder = new StringBuilder("");
		
		/** Count Exception Overflow*/
		int counter=(Integer)parameters.get(GeneralConstants.COUNTER_EXCEPTIONS);
		
		
		/** CLASS_VALOR **/
		if (rateExclusion.getExceptionRule().equals(RulerExceptionType.SECURITY_CLASS_EXCEPTION_RATE.getCode())){
			if(rateExclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ONE)){
				
				if (Validations.validateIsNotNull(rateExclusion.getSecurityClass())){
					stringBuilderBeginTableExclusion.append("AND NOT ( ");
					stringBuilderBeginTableExclusion.append("nvl ( ");
					stringBuilderBeginTableExclusion.
							append(table).
							append(".SECURITY_CLASS,0)=").
							append(rateExclusion.getSecurityClass());
					stringBuilderBeginTableExclusion.append(" ) ");
					counter++;
				}				
			}
			
			else if(rateExclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ALL)){
				
				/** GETS FROM DATABASE ALL SECURITY CLASSES CODES  */
				List<ParameterTable> paramTableSecurityClasses ;
				
				ParameterTableTO parameterTableTO = new ParameterTableTO();
				parameterTableTO.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());		
				paramTableSecurityClasses = generalParametersService.getListParameterTableServiceBean(parameterTableTO);
				
				for (ParameterTable parameterTableTemp : paramTableSecurityClasses) {
					stringBuilderBeginTableExclusion.append("AND NOT ( ").
								append("nvl ( ").
								append(table).
								append(".SECURITY_CLASS,0)=").
								append(parameterTableTemp.getParameterTablePk()).
								append(" ) ");
					counter++;
					
				}
				
			}
			
		/** VALOR **/	
		}else if (rateExclusion.getExceptionRule().equals(RulerExceptionType.SECURITY_EXCEPTION_RATE.getCode())){
			if(rateExclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ONE)){
				
				if (Validations.validateIsNotNull(rateExclusion.getSecurities())){
					stringBuilderBeginTableExclusion.append("AND NOT ( ").
						append("nvl ( ").
						append(table).
						append(".ID_SECURITY_CODE_FK,' ')='").
						append(rateExclusion.getSecurities().getIdSecurityCodePk()).
						append("'").
						append(" ) ");
					counter++;
				}
			}
			
			else if(rateExclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ALL)){
		
				List<Security> securityList = this.findAllSecurity();			
			 
				for (Security security : securityList) {
					stringBuilderBeginTableExclusion.append("AND NOT ( ").
						append("nvl ( ").
						append(table).
						append(".ID_SECURITY_CODE_FK,' ')='").
						append(security.getIdSecurityCodePk()).
						append("'");
					stringBuilderBeginTableExclusion.append(" ) ");
					counter++;
				}
				
			}	
			
		/** PARTICIPANT **/
		}else if(rateExclusion.getExceptionRule().equals(RulerExceptionType.PARTICIPANT_EXCEPTION_RATE.getCode())){
			if(rateExclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ONE)){				

				if (Validations.validateIsNotNull(rateExclusion.getParticipant())){
					stringBuilderBeginTableExclusion.append("AND NOT ( ").
						append("nvl ( ").
						append(table).
						append(".ID_PARTICIPANT_FK,0)=").
						append(rateExclusion.getParticipant().getIdParticipantPk());
					counter++;
				}
				
				if (Validations.validateIsNotNull(rateExclusion.getHolderAccount())){
					stringBuilderBeginTableExclusion.
						append(" AND nvl (").
						append(table).
						append(".ID_HOLDER_ACCOUNT_FK,0)=").
						append(rateExclusion.getHolderAccount().getIdHolderAccountPk());
					counter++;
				}
				
				if (Validations.validateIsNotNull(rateExclusion.getHolder())){
					stringBuilderBeginTableExclusion.
						append(" AND nvl (").
						append(table).
						append(".ID_HOLDER_FK,0)=").
						append(rateExclusion.getHolder().getIdHolderPk());
					counter++;
				}
				
				if (Validations.validateIsNotNull(rateExclusion.getSecurities())){
					stringBuilderBeginTableExclusion.
						append(" AND nvl (").
						append(table).
						append(".ID_SECURITY_CODE_FK,' ')='").
						append(rateExclusion.getSecurities().getIdSecurityCodePk()).
						append("'");
					counter++;
				}
				stringBuilderBeginTableExclusion.append(" ) ");
				
				
			}else if(rateExclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ALL)){
				List<Participant>    listClientParticipant;
				
				Participant participantTemp=new Participant();								 
				listClientParticipant = helperComponentFacade.getLisParticipantServiceFacade(participantTemp);
				 
				for (Participant participant : listClientParticipant) {
					
					stringBuilderBeginTableExclusion.append("AND NOT ( ").
						append("nvl ( ").
						append(table).
						append(".ID_PARTICIPANT_FK,0)=").
						append(participant.getIdParticipantPk()).
						append(" ) ");
					counter++;
						 
				}
			}
			
		/** ISSUER **/
		}else if(rateExclusion.getExceptionRule().equals(RulerExceptionType.ISSUER_EXCEPTION_RATE.getCode())){
			if(rateExclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ONE)){
				
				if (Validations.validateIsNotNull(rateExclusion.getIssuer())){
					
					stringBuilderBeginTableExclusion.append("AND NOT ( ").
						append("nvl ( ").
						append(table).
						append(".ID_ISSUER_FK,' ')='").
						append(rateExclusion.getIssuer().getIdIssuerPk()).
						append("'");
					counter++;
				}
				
				if (Validations.validateIsNotNull(rateExclusion.getIssuance())){
					
					stringBuilderBeginTableExclusion.append(" AND nvl (").
								append(table).
								append(".ID_ISSUANCE_CODE_FK,' ')='").
								append(rateExclusion.getIssuance().getIdIssuanceCodePk()).
								append("'");
					counter++;
				}
				
				if (Validations.validateIsNotNull(rateExclusion.getSecurities())){
					
					stringBuilderBeginTableExclusion.append(" AND nvl (").
								append(table).
								append(".ID_SECURITY_CODE_FK,' ')='").
								append(rateExclusion.getSecurities().getIdSecurityCodePk()).
								append("'");
					counter++;
				}
				stringBuilderBeginTableExclusion.append(" ) ");
				
				
			}else if(rateExclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ALL)){
								
				List<Issuer> issuerList = this.findAllIssuer();
 
				for (Issuer issuer : issuerList) {
					stringBuilderBeginTableExclusion.append("AND NOT ( ").
								append("nvl ( ").
								append(table).append(".ID_ISSUER_FK,' ')='").
								append(issuer.getIdIssuerPk()).
								append("'").
								append(" ) ");
					counter++;
				}				
			}
			
		/** HOLDER **/	
		}else if(rateExclusion.getExceptionRule().equals(RulerExceptionType.HOLDER_EXCEPTION_RATE.getCode())){
			if(rateExclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ONE)){
				
				if (Validations.validateIsNotNull(rateExclusion.getHolder())){
					stringBuilderBeginTableExclusion.append("AND NOT ( ").
								append("nvl ( ").
								append(table).
								append(".ID_HOLDER_FK,0)=").
								append(rateExclusion.getHolder().getIdHolderPk());
					counter++;
				}
				
				if (Validations.validateIsNotNull(rateExclusion.getHolderAccount())){
					stringBuilderBeginTableExclusion.append(" AND nvl ( ").
								append(table).
								append(".ID_HOLDER_ACCOUNT_FK,0)=").
								append(rateExclusion.getHolderAccount().getIdHolderAccountPk());
					counter++;
				}	
				
				 stringBuilderBeginTableExclusion.append(" ) ");
				
			}else if(rateExclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ALL)){

				List<Holder> holderList = this.findAllHolder();
				parameters.put(GeneralConstants.HOLDER_EXCEPTION_ALL_LIST, holderList);
				for (Holder holder : holderList) {
					stringBuilderBeginTableExclusion.append("AND NOT ( ").
								append("NVL ( ").
								append(table).
								append(".ID_HOLDER_FK,0)=").
								append(holder.getIdHolderPk());
					stringBuilderBeginTableExclusion.append(" ) ");
					counter++;
				}	
			}
				
		}

		/**
		 * Consult Counter Exception To use Component Exception 
		 */
		parameters.put(GeneralConstants.COUNTER_EXCEPTIONS, counter);
		return stringBuilderBeginTableExclusion.toString();
	
	}	
	
	/**
	 * Method create a new Exception.
	 *
	 * @param rateExclusion the rate exclusion
	 * @return the rate exclusion
	 * @throws ServiceException the service exception
	 */
	public RateExclusion saveExceptionExclusion(RateExclusion rateExclusion) throws ServiceException{
		this.create(rateExclusion);
		return rateExclusion;
	}
	
	/**
	 * Save All Exception Rate Exclusion.
	 *
	 * @param rateExclusionList the rate exclusion list
	 */
	public void saveAllExceptionRateExclusion(List<RateExclusion> rateExclusionList) {
		this.saveAll(rateExclusionList);
	
	}
	
	/**
	 * Save All Exception Rate Inclusion.
	 *
	 * @param rateInclusionList the rate inclusion list
	 */
	public void saveAllExceptionRateInclusion(List<RateInclusion> rateInclusionList){
		this.saveAll(rateInclusionList);
	
	}
	
	
	/**
	 * *
	 * Save Exception Inclusion.
	 *
	 * @param rateInclusion the rate inclusion
	 * @return the rate inclusion
	 * @throws ServiceException the service exception
	 */
	public RateInclusion saveExceptionInclusion(RateInclusion rateInclusion) throws ServiceException{
		this.create(rateInclusion);
		return rateInclusion;
	}

	/**
	 * Remove Exception Exclusion.
	 *
	 * @param idRateException the id rate exception
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int removeExceptionExclusion(Long idRateException) throws ServiceException{
		StringBuilder sb = new StringBuilder();
		sb.append("delete from rate_exclusion where id_rate_exclusion_pk=:rateExclusionPk ");
		
		int i=0;
		if (Validations.validateIsNotNull(idRateException)){
			
			Query query= em.createNativeQuery(sb.toString());	
			
			query.setParameter("rateExclusionPk", idRateException);
			i=query.executeUpdate();			
		}
		return i;
	}

	/**
	 * Remove Exception Inclusion.
	 *
	 * @param idRateException the id rate exception
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int removeExceptionInclusion(Long idRateException) throws ServiceException{
		StringBuilder sb = new StringBuilder();
		sb.append("delete from rate_inclusion where id_rate_inclusion_pk=:rateInclusionPk ");
		
		int i=0;
		if (Validations.validateIsNotNull(idRateException)){
			
		Query query= em.createNativeQuery(sb.toString());	
			
			query.setParameter("rateInclusionPk", idRateException);
			i=query.executeUpdate();			
		}
		return i;
	}
	 
	/**
	 * Get Service Rate By Rate Exception.
	 *
	 * @param rateExceptionId the rate exception id
	 * @return the services rate by rate exception
	 * @throws ServiceException the service exception
	 */
	public List<ServiceRate> getServicesRateByRateException(Long rateExceptionId) throws ServiceException {
		 	 
		StringBuilder stringBuilderSql = new StringBuilder();	
		stringBuilderSql.append("SELECT sr FROM  ServiceRate sr  ");
		stringBuilderSql.append(" WHERE 1=1   ");	
		
		if ( Validations.validateIsNotNull(rateExceptionId) ){
			 
			stringBuilderSql.append(" and sr.idServiceRatePk = :serviceRatePk  ");
		}
				
		Query query = em.createQuery(stringBuilderSql.toString());
		
		if ( Validations.validateIsNotNull(rateExceptionId) ){
			 
			query.setParameter("serviceRatePk",rateExceptionId );
		}		
		
		List<ServiceRate> listObject=null;
		try {
			listObject=query.getResultList();
		
		} catch (Exception e) {
			log.error(("findServiceRateListByIdServiceRatePk"));
			log.debug(e.getMessage());
		}
		return listObject;
	}
	
	/**
	 * Get Service Rate By Rate Exceptions .
	 *
	 * @param rateExceptionId the rate exception id
	 * @return the services rate by rate exceptions
	 * @throws ServiceException the service exception
	 */
	public List<ServiceRateTo> getServicesRateByRateExceptions(Long rateExceptionId) throws ServiceException { 
		StringBuilder stringBuilderSql = new StringBuilder();	
		stringBuilderSql.append(" select ");
		stringBuilderSql.append(" 	ID_SERVICE_RATE_PK, ");
		stringBuilderSql.append(" 	CURRENCY_RATE, ");
		stringBuilderSql.append(" 	RATE_CODE, ");
		stringBuilderSql.append(" 	RATE_TYPE, ");
		stringBuilderSql.append(" 	RATE_AMOUNT, ");
		stringBuilderSql.append(" 	RATE_PERCENT, ");
		stringBuilderSql.append(" 	RATE_STATE, ");
		stringBuilderSql.append(" 	IND_TAXED, ");
		stringBuilderSql.append(" 	RATE_NAME, ");
		stringBuilderSql.append(" 	INITIAL_EFFECTIVE_DATE, ");
		stringBuilderSql.append("	END_EFFECTIVE_DATE, ");
		stringBuilderSql.append(" 	COMMENTS, ");
		stringBuilderSql.append(" 	ID_BILLING_SERVICE_FK  ");
		stringBuilderSql.append(" from service_rate  "); 
		stringBuilderSql.append(" where 1=1 ");
		
		if(Validations.validateIsNotNull(rateExceptionId))
		{
			stringBuilderSql.append(" and ID_SERVICE_RATE_PK=:idServiceRatePk "); 
		}
		 Query query = em.createNativeQuery(stringBuilderSql.toString());
		
		 if(Validations.validateIsNotNull(rateExceptionId))
			{
			 query.setParameter("idServiceRatePk",rateExceptionId);
			}
	
		 List<Object[]>  objectList = query.getResultList();
		 List<ServiceRateTo> serviceRateToList=null;


		 
		 for(int i=0;i<objectList.size();++i){ 
			 
				 
				 
			 serviceRateToList=new ArrayList<ServiceRateTo>();
			 Object[] sResults = (Object[])objectList.get(i);
			 ServiceRateTo serviceRateToView = new ServiceRateTo();		
			 serviceRateToView.setRateCode(sResults[2]==null?0:Integer.parseInt(sResults[2].toString()));
			 serviceRateToView.setRateName(sResults[8].toString() );
			 serviceRateToView.setRateType(sResults[3]==null?0:Integer.parseInt(sResults[3].toString()));
			 serviceRateToView.setIdBillingService(sResults[12].toString());
			 serviceRateToList.add(serviceRateToView);
		 }
		 
		return serviceRateToList;
	}
	
	/**
	 * Find Data Holder To.
	 *
	 * @param participantPk the participant pk
	 * @return the list
	 */
	public List<HolderTo> findDataHolderTo(Long participantPk){
	
	 StringBuilder sbQuery = new StringBuilder();
	 sbQuery.append(" SELECT DISTINCT  ");
	 sbQuery.append("  HAB.ID_PARTICIPANT_PK AS ID_PARTICIPANT_FK ,  ");
	 sbQuery.append("  HO.ID_HOLDER_PK AS ID_HOLDER_FK ,");
	 sbQuery.append("  HO.FULL_NAME, ");
	 sbQuery.append("  HAB.ID_HOLDER_ACCOUNT_PK AS ID_HOLDER_ACCOUNT_FK, ");
	 sbQuery.append("  HA.ACCOUNT_NUMBER as ACCOUNT_NUMBER , ");
	 sbQuery.append("  HAB.ID_SECURITY_CODE_PK AS ID_SECURITY_CODE_FK , ");
	 sbQuery.append("  CASE  ");
	 sbQuery.append("   WHEN HAD.IND_REPRESENTATIVE=1 THEN 'SI' ");
	 sbQuery.append("   WHEN HAD.IND_REPRESENTATIVE=0 THEN 'NO' ");
	 sbQuery.append("  END  AS REPRESENTANTE,   ");
	 sbQuery.append("  HAD.IND_REPRESENTATIVE ");
	 sbQuery.append(" FROM HOLDER_ACCOUNT_BALANCE HAB   ");
	 sbQuery.append("   INNER JOIN HOLDER_ACCOUNT HA ON ( HA.ID_HOLDER_ACCOUNT_PK = HAB.ID_HOLDER_ACCOUNT_PK )   ");
	 sbQuery.append("   INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON (HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK )   ");
	 sbQuery.append("   INNER JOIN HOLDER HO ON (HO.ID_HOLDER_PK=HAD.ID_HOLDER_FK)    ");
	 sbQuery.append(" WHERE  HAB.ID_PARTICIPANT_PK=:participantPk ");
	 sbQuery.append(" AND HAD.IND_REPRESENTATIVE=1  ");
	 sbQuery.append(" ORDER BY HAB.ID_PARTICIPANT_PK,HO.ID_HOLDER_PK, HAB.ID_HOLDER_ACCOUNT_PK  ");
	 
	 sbQuery.append("   ");
	 Query query = em.createNativeQuery(sbQuery.toString());
	 query.setParameter("participantPk",participantPk);
    
	   List<Object[]>  objectList = query.getResultList();
	   List<HolderTo> holderToList = null;
	   if (Validations.validateListIsNotNullAndNotEmpty(objectList)){
		   holderToList = new ArrayList<HolderTo>();
		   
	   for(int i=0;i<objectList.size();++i){ 
		   Object[] sResults = (Object[])objectList.get(i);
		   HolderTo holderTo=new HolderTo();
		   
		   holderTo.setIdParticipantPk(Long.parseLong(sResults[0]==null?"":sResults[0].toString()));
		   holderTo.setIdHolderPk(Long.parseLong(sResults[1]==null?"":sResults[1].toString()));
		   holderTo.setFullNameHolder(sResults[2]==null?"":sResults[2].toString());
		   holderTo.setHolderAccountPk(Long.parseLong(sResults[3]==null?"":sResults[3].toString()));
		   holderTo.setAccountNumberPk(Long.parseLong(sResults[4]==null?"":sResults[4].toString()));
		   holderTo.setIdSecurityCodePk(sResults[5]==null?"":sResults[5].toString());
		   holderTo.setDescriptionRepresentative(sResults[6]==null?"":sResults[6].toString());
		   holderTo.setIsRepresentative(Integer.parseInt(sResults[7]==null?"":sResults[7].toString()));
		   holderToList.add(holderTo);
	   }
    
	   }
    return holderToList;
	}


	/**
	 * FInd Data Issuance To.
	 *
	 * @param issuerCode the issuer code
	 * @return the list
	 */
	public List<IssuanceTo> findDataIssuanceTo(String issuerCode){
		
		 StringBuilder sbQuery = new StringBuilder();
		 sbQuery.append(" SELECT DISTINCT  ");
		 sbQuery.append("  ISR.ID_ISSUER_PK AS ID_ISSUER_PK , ");
		 sbQuery.append("  ISR.BUSINESS_NAME AS BUSINESS_NAME, ");
		 sbQuery.append("  ISC.ID_ISSUANCE_CODE_PK AS ID_ISSUANCE_CODE_PK, ");
		 sbQuery.append("  ISC.DESCRIPTION AS DESCRIPTION_ISSUANCE, ");
		 sbQuery.append("  SCT.ID_SECURITY_CODE_PK AS ID_SECURITY_CODE_PK, ");
		 sbQuery.append("  SCT.DESCRIPTION AS DESCRIPTION_ISIN_CODE ");
		 
		 sbQuery.append(" FROM ISSUER ISR  ");
		 sbQuery.append(" INNER JOIN ISSUANCE ISC ON ( ISC.ID_ISSUER_FK = ISR.ID_ISSUER_PK)  ");
		 sbQuery.append(" LEFT OUTER JOIN SECURITY SCT ON ( SCT.ID_ISSUANCE_CODE_FK = ISC.ID_ISSUANCE_CODE_PK)   ");
		 
		 sbQuery.append(" WHERE  trunc(ISC.expiration_date) > trunc(:dateSystem) and ISC.state_issuance = 128 " );
		 sbQuery.append(" AND ISR.ID_ISSUER_PK = :issuerPk ");
		 sbQuery.append(" ORDER BY ID_ISSUANCE_CODE_PK, SCT.ID_SECURITY_CODE_PK ");
		
     	 sbQuery.append("   ");
		 Query query = em.createNativeQuery(sbQuery.toString());
		 query.setParameter("issuerPk",issuerCode);
		 query.setParameter("dateSystem",CommonsUtilities.currentDate());
		
		 List<Object[]>  objectList = query.getResultList();
		 List<IssuanceTo> issuanceToList = null;
		 
		 if (Validations.validateListIsNotNullAndNotEmpty(objectList)){
			 issuanceToList = new ArrayList<IssuanceTo>();
			   
		   for(int i=0;i<objectList.size();++i){ 
			   Object[] sResults = (Object[])objectList.get(i);
			   IssuanceTo issuanceTo=new IssuanceTo();
		 
			   issuanceTo.setIdIssuanceCodePk(sResults[2]==null?"":sResults[2].toString());
			   issuanceTo.setDescriptionIssuance((sResults[3]==null?"":sResults[3].toString()));
			   issuanceTo.setIdSecurityCodePk(sResults[4]==null?"":sResults[4].toString());
		 
			   issuanceToList.add(issuanceTo);
		   }
		 }
		return issuanceToList;
	}

	/**
	 * Find Holder Account.
	 *
	 * @param idHolder the id holder
	 * @return the list
	 */
	public List<HolderAccountTo> findHolderAccount(Long idHolder){
		StringBuilder sbQuery = new StringBuilder();
		 sbQuery.append(" SELECT  ");
		 sbQuery.append("  HO.ID_HOLDER_PK, ");
		 sbQuery.append("  HA.ACCOUNT_NUMBER, ");
		 sbQuery.append("  HA.ID_HOLDER_ACCOUNT_PK, HAD.ind_representative ");
		 
		 sbQuery.append(" FROM HOLDER HO  ");
		 sbQuery.append(" INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON ( HAD.ID_HOLDER_FK = HO.ID_HOLDER_PK ) ");
		 sbQuery.append(" INNER JOIN HOLDER_ACCOUNT        HA ON ( HA.ID_HOLDER_ACCOUNT_PK =  HAD.ID_HOLDER_ACCOUNT_FK )  ");
		 
		 sbQuery.append(" WHERE HAD.ind_representative=1 AND  HO.ID_HOLDER_PK=:holderPk ");
		 sbQuery.append(" ORDER BY HA.ACCOUNT_NUMBER ");
		 
		
		 sbQuery.append("   ");
		 Query query = em.createNativeQuery(sbQuery.toString());
		 query.setParameter("holderPk",idHolder);
		
		 List<Object[]>  objectList = query.getResultList();
		 List<HolderAccountTo>holderAccountToList = null;
		 
		 if (Validations.validateListIsNotNullAndNotEmpty(objectList)){
			 holderAccountToList = new ArrayList<HolderAccountTo>();
			   
		   for(int i=0;i<objectList.size();++i){ 
			   Object[] sResults = (Object[])objectList.get(i);
			   HolderAccountTo holderAccountTo = new HolderAccountTo();

			   holderAccountTo.setAccountNumber(sResults[1]==null?0:Integer.parseInt(sResults[1].toString()));
			   holderAccountTo.setIdHolderAccountPk(Long.parseLong(sResults[2]==null?"":sResults[2].toString()));
			   
			   holderAccountToList.add(holderAccountTo);
		   }
		 }
		return holderAccountToList;
		
	}

	
	
	/**
	 *  get list of Security   .
	 *
	 * @return the list
	 */
	public List<Security> findAllSecurity(){
		StringBuilder sbQuery = new StringBuilder();
		 sbQuery.append(" select            ").
		 		 append("  ID_SECURITY_CODE_PK  ").
		 		 append("  from SECURITY    ");		 
		
		 sbQuery.append("   ");		
		 Query query = em.createNativeQuery(sbQuery.toString());
		 
		 List<Object[]>  objectList = query.getResultList();
		 List<Security>  securityList = null;
		 
		 if (Validations.validateListIsNotNullAndNotEmpty(objectList)){
			 securityList = new ArrayList<Security>();
			   
		   for(int i=0;i<objectList.size();++i){ 
			   Object sResults = (Object)objectList.get(i);
			   Security security = new Security();

			   security.setIdSecurityCodePk(sResults==null?"":sResults.toString());
			   
			   securityList.add(security);
		   }
		 }
		return securityList;
		
	}
	
	
	/**
	 *  Get list of Issuer .
	 *
	 * @return the list
	 */
	public List<Issuer> findAllIssuer(){
 		
		 StringBuilder sbQuery = new StringBuilder();
		 sbQuery.append(" Select ID_ISSUER_PK        ")
		 		.append("  from ISSUER               ");
		 
    	 sbQuery.append("   ");
		 Query query = em.createNativeQuery(sbQuery.toString());
		
		 List<Object[]>  objectList = query.getResultList();
		 List<Issuer> issuerList = null;
		 
		 if (Validations.validateListIsNotNullAndNotEmpty(objectList)){
			 issuerList = new ArrayList<Issuer>();
			   
		   for(int i=0;i<objectList.size();++i){ 
			   Object sResults = (Object)objectList.get(i);
			   Issuer issuer=new Issuer();
		 
			   issuer.setIdIssuerPk(sResults==null?"":sResults.toString());
		 
			   issuerList.add(issuer);
		   }
		 }
		return issuerList;
	}
		
	/**
	 * get list of Holder .
	 *
	 * @return the list
	 */
	public List<Holder> findAllHolder(){

		StringBuilder sbQuery = new StringBuilder();
		 sbQuery.append(" SELECT             ").
		 		 append("  ID_HOLDER_PK      ").
		 		 append("  FROM HOLDER       ");
 
		
		 sbQuery.append("   ");
		 Query query = em.createNativeQuery(sbQuery.toString());
		 List<Object[]>  objectList = query.getResultList();
		 List<Holder>holderList = null;
		 
		 if (Validations.validateListIsNotNullAndNotEmpty(objectList)){
			 holderList = new ArrayList<Holder>();
			   
		   for(int i=0;i<objectList.size();++i){ 
			   Object sResults = (Object)objectList.get(i);
			   Holder holder = new Holder();

			   holder.setIdHolderPk(Long.parseLong(sResults==null?"":sResults.toString()));
			   
			   holderList.add(holder);
		   }
		 }
		return holderList;
		
	}
	

	/**
	 * *
	 * Mark Object with Ind. included
	 *
	 * @param parameters the parameters
	 * @param objectList the object list
	 * @param objectTarget the object target
	 * @param rateInclusion the rate inclusion
	 * @param index the index
	 * @return the list to inclusion
	 * @throws ServiceException the service exception
	 */
	public List<Object>  getListToInclusion(Map parameters, List<Object> objectList,  List<Object> objectTarget, 
											RateInclusion rateInclusion, int[] index ) throws ServiceException{
		

		Boolean isInclusion=Boolean.FALSE;
		int indexInd=index[IndexExceptionType.LENGTH_LIST.getCode()];
		
		if(Validations.validateListIsNotNullAndNotEmpty(objectTarget)){
			objectList=objectTarget;
			
			for (Iterator iterator = objectList.iterator(); iterator.hasNext();){
				Object[] objects = (Object[]) iterator.next();
				//Si no es la primera vez que entra consultar si el objeto ya tiene el indicador
				// de Inclusion
				if((boolean)objects[indexInd]==true){
					continue;
				}
				isInclusion=isObjectInclusion(rateInclusion, parameters, objects, index);
				((Object[])objects)[indexInd] =isInclusion;
			}
			
			 return objectTarget;
		}else{
			List<Object> listInclusion	=new ArrayList<>();
			if(Validations.validateListIsNotNullAndNotEmpty(objectList)){

				for (Iterator iterator = objectList.iterator(); iterator.hasNext();) {
					Object[] objects = (Object[]) iterator.next();
					Object[] objInclusion= new Object[indexInd+1];
					
					
					isInclusion=isObjectInclusion(rateInclusion, parameters, objects, index);

					((Object[])objInclusion)[indexInd] =isInclusion;
					
					for (int i = 0; i < objects.length; i++) {
						Object object = objects[i];
						objInclusion[i]=object;
					}
					listInclusion.add(objInclusion);
				}
				
			}
			return listInclusion;
		}

	}
	
	

	/**
	 *  
	 * Verify Object to Market Included.
	 *
	 * @param parameters the parameters
	 * @param objectList the object list
	 * @param rateExclusion the rate exclusion
	 * @param index the index
	 * @return the list to exclusion
	 */
	public List<Object>  getListToExclusion(Map parameters, List<Object> objectList, RateExclusion rateExclusion, int[] index ) {
		

		int indexParticipant=index[IndexExceptionType.PARTICIPANT.getCode()];//0;
		int indexHolder=index[IndexExceptionType.HOLDER.getCode()];//3;
		int indexSecurity=index[IndexExceptionType.SECURITY.getCode()];//4;
		int indexSecurityClass=index[IndexExceptionType.SECURITY_CLASS.getCode()];//5;
		int indexHolderAccount=index[IndexExceptionType.HOLDER_ACCOUNT.getCode()];//8;
		int indexIssuer=index[IndexExceptionType.ISSUER.getCode()];//9;
		int indexIsssuance=index[IndexExceptionType.ISSUANCE.getCode()];//10;
		
		if(Validations.validateListIsNotNullAndNotEmpty(objectList)){
			List<Object[]> toRemove=new ArrayList<>();
			for (Iterator iterator = objectList.iterator(); iterator.hasNext();) {
				Object[] objects = (Object[]) iterator.next();
				/***
				 * Tarifa 1: Anotaciones en Cuenta
				 * 
				 * 0 Participant
				 * 1 Nominal Value
				 * 2 Currency
				 * 3 Holder
				 * 4 Security
				 * 5 Security Class
				 * 6 Total Balance
				 * 7 Nominal Value
				 * 8 Account
				 * 9 Issuer
				 * 10 Issuance
				 * 11 isInclusion
				 */
				
				Long 	idParticipantPk		=	Long.parseLong(((Object[])objects)[indexParticipant].toString());
				Long 	idHolderPk 			=	Long.parseLong(((Object[])objects)[indexHolder].toString());
				String 	idSecurityPk		= 	((Object[])objects)[indexSecurity].toString();
				Integer idSecurityClass		= 	Integer.parseInt(((BigDecimal)((Object[])objects)[indexSecurityClass]).toString());
				Long 	idHolderAccountPk 	=	Long.parseLong(((Object[])objects)[indexHolderAccount].toString());
				String 	idIssuerPk			= 	((Object[])objects)[indexIssuer].toString();
				String 	idIssuancePk		= 	((Object[])objects)[indexIsssuance].toString();
				Boolean isInclusion			=	(Boolean) ((Object[])objects)[11];
				
				if(isInclusion){
					continue;
				}
				/** CLASS_VALOR **/
				else if (rateExclusion.getExceptionRule().equals(RulerExceptionType.SECURITY_CLASS_EXCEPTION_RATE.getCode())){
					
					
					if(rateExclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ONE)){
						if (Validations.validateIsNotNull(rateExclusion.getSecurityClass())){
							
							
							if(rateExclusion.getSecurityClass().equals(idSecurityClass)){
								toRemove.add(objects);
								
							}
							
						}				
					}
					
					else if(rateExclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ALL)){
						
						/** GETS FROM DATABASE ALL SECURITY CLASSES CODES  */
						List<ParameterTable> paramTableSecurityClasses ;
	
						paramTableSecurityClasses =(List<ParameterTable>) parameters.get(GeneralConstants.SECURITY_CLASS_EXCEPTION_ALL_LIST); 

						for (ParameterTable parameterTableTemp : paramTableSecurityClasses) {
							
							if(parameterTableTemp.getParameterTablePk().equals(idSecurityClass)){
								toRemove.add(objects);
								break;
							}
							
						}
									
									
					}
					
				/** VALOR **/	
				}else if (rateExclusion.getExceptionRule().equals(RulerExceptionType.SECURITY_EXCEPTION_RATE.getCode())){
					
					if(rateExclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ONE)){
						if (Validations.validateIsNotNull(rateExclusion.getSecurities())){
							
							if(rateExclusion.getSecurities().getIdSecurityCodePk().equalsIgnoreCase(idSecurityPk)){
								toRemove.add(objects);
							}
							
						}
					}
					
					else if(rateExclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ALL)){
				
						List<Security> securityList = (List<Security>) parameters.get(GeneralConstants.SECURITY_EXCEPTION_ALL_LIST);
						
						for (Security securitys : securityList) {
							
							if(securitys.getIdSecurityCodePk().equalsIgnoreCase(idSecurityPk)){
								toRemove.add(objects);
								break;
							}
						
							
						}
						
						
					}	
					
				/** PARTICIPANT **/
				}else if(rateExclusion.getExceptionRule().equals(RulerExceptionType.PARTICIPANT_EXCEPTION_RATE.getCode())){
					
					if(rateExclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ONE)){

						if (Validations.validateIsNotNull(rateExclusion.getParticipant())){
							if(rateExclusion.getParticipant().getIdParticipantPk().equals(idParticipantPk)){
								toRemove.add(objects);
							}
							
						}
						
						if (Validations.validateIsNotNull(rateExclusion.getHolderAccount())){
							if(rateExclusion.getHolderAccount().getIdHolderAccountPk().equals(idHolderAccountPk)){
								toRemove.add(objects);
								
							}
							
						}
						
						if (Validations.validateIsNotNull(rateExclusion.getHolder())){
							if(rateExclusion.getHolder().getIdHolderPk().equals(idHolderPk)){
								toRemove.add(objects);
								
							}
						}
						
						if (Validations.validateIsNotNull(rateExclusion.getSecurities())){ 
							if(rateExclusion.getSecurities().getIdSecurityCodePk().equals(idSecurityPk)){
								toRemove.add(objects);
							}
						}
						
						
					}else if(rateExclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ALL)){
						List<Participant>    listClientParticipant;
											 
						listClientParticipant = (List<Participant> ) parameters.get(GeneralConstants.PARTICIPANT_EXCEPTION_ALL_LIST);
					 	
						for (Participant participant : listClientParticipant) {
							if(participant.getIdParticipantPk().equals(idParticipantPk)){
								toRemove.add(objects);
								break;
							}
							
						}
						
						
						
					}
					
				/** ISSUER **/
				}else if(rateExclusion.getExceptionRule().equals(RulerExceptionType.ISSUER_EXCEPTION_RATE.getCode())){
					if(rateExclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ONE)){
						
						if (Validations.validateIsNotNull(rateExclusion.getIssuer())){
							
							if(rateExclusion.getIssuer().getIdIssuerPk().equals(idIssuerPk)){
								toRemove.add(objects);
							}
						}
						
						if (Validations.validateIsNotNull(rateExclusion.getIssuance())){ 							
							if(rateExclusion.getIssuance().getIdIssuanceCodePk().equals(idIssuancePk)){
								toRemove.add(objects);
							}
						}
						
						if (Validations.validateIsNotNull(rateExclusion.getSecurities())){ 
							if(rateExclusion.getSecurities().getIdSecurityCodePk().equals(idSecurityPk)){
								toRemove.add(objects);
							}
						}
						
						
					}else if(rateExclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ALL)){
										
						List<Issuer> issuerList = (List<Issuer> )parameters.get(GeneralConstants.ISSUER_EXCEPTION_ALL_LIST);

						for (Issuer issuer : issuerList) {
							if(issuer.getIdIssuerPk().equals(idIssuerPk)){
								toRemove.add(objects);
								break;
							}
							
						}
						
						
						
					}
					
				/** HOLDER **/	
				}else if(rateExclusion.getExceptionRule().equals(RulerExceptionType.HOLDER_EXCEPTION_RATE.getCode())){
					if(rateExclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ONE)){
						
						if (Validations.validateIsNotNull(rateExclusion.getHolder())){
							if(rateExclusion.getHolder().getIdHolderPk().equals(idHolderPk)){
								toRemove.add(objects);
							}
							
						}
						
						if (Validations.validateIsNotNull(rateExclusion.getHolderAccount())){					
							if(rateExclusion.getHolderAccount().getIdHolderAccountPk().equals(idHolderAccountPk)){
								toRemove.add(objects);
							}
						}	
						
						
					}else if(rateExclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ALL)){

						List<Holder> holderList = (List<Holder>)parameters.get(GeneralConstants.HOLDER_EXCEPTION_ALL_LIST);

						for (Holder holder : holderList) {
							if(holder.getIdHolderPk().equals(idHolderPk)){
								toRemove.add(objects);
								break;
							}
							
						}
						
					}
						
				}
				
	
			}
			objectList.removeAll(toRemove);
			
		}
		return objectList;
	
	}
	
	/**
	 * Return is Object have Inclusion.
	 *
	 * @param rateInclusion the rate inclusion
	 * @param parameters the parameters
	 * @param objects the objects
	 * @param index the index
	 * @return true, if is object inclusion
	 */
	public boolean isObjectInclusion(RateInclusion rateInclusion, Map parameters, Object[] objects, int[] index){
		
		int indexParticipant=index[IndexExceptionType.PARTICIPANT.getCode()];//0;
		int indexHolder=index[IndexExceptionType.HOLDER.getCode()];//3;
		int indexSecurity=index[IndexExceptionType.SECURITY.getCode()];//4;
		int indexSecurityClass=index[IndexExceptionType.SECURITY_CLASS.getCode()];//5;
		int indexHolderAccount=index[IndexExceptionType.HOLDER_ACCOUNT.getCode()];//8;
		int indexIssuer=index[IndexExceptionType.ISSUER.getCode()];//9;
		int indexIsssuance=index[IndexExceptionType.ISSUANCE.getCode()];//10;
		/***
		 * Tarifa 1: Anotaciones en Cuenta
		 * 
		 * 0  Participant
		 * 1  Nominal Value
		 * 2  Currency
		 * 3  Holder
		 * 4  Security
		 * 5  Security Class
		 * 6  Total Balance
		 * 7  Nominal Value
		 * 8  Account
		 * 9  Issuer
		 * 10 Issuance
		 * 
		 */
		
		Long 	idParticipantPk		=	Long.parseLong(((Object[])objects)[indexParticipant].toString());
		Long 	idHolderPk 			=	Long.parseLong(((Object[])objects)[indexHolder].toString());
		String 	idSecurityPk		= 	((Object[])objects)[indexSecurity].toString();
		Integer idSecurityClass		= 	Integer.parseInt(((BigDecimal)((Object[])objects)[indexSecurityClass]).toString());
		Long 	idHolderAccountPk 	=	Long.parseLong(((Object[])objects)[indexHolderAccount].toString());
		String 	idIssuerPk			= 	((Object[])objects)[indexIssuer].toString();
		String 	idIssuancePk		= 	((Object[])objects)[indexIsssuance].toString();
		
		
		/** CLASS_VALOR **/
		if (rateInclusion.getExceptionRule().equals(RulerExceptionType.SECURITY_CLASS_EXCEPTION_RATE.getCode())){
			
			
			if(rateInclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ONE)){
				if (Validations.validateIsNotNull(rateInclusion.getSecurityClass())){
					
					if(rateInclusion.getSecurityClass().equals(idSecurityClass)){
						return Boolean.TRUE;
					}
					
				}				
			}
			
			else if(rateInclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ALL)){
				
				/** GETS FROM DATABASE ALL SECURITY CLASSES CODES  */
				List<ParameterTable> paramTableSecurityClasses ;
		
				paramTableSecurityClasses = (List<ParameterTable>)parameters.get(GeneralConstants.SECURITY_CLASS_EXCEPTION_ALL_LIST) ;
				
				
				for (ParameterTable parameterTableTemp : paramTableSecurityClasses) {
					
					if(parameterTableTemp.getParameterTablePk().equals(idSecurityClass)){
						return Boolean.TRUE;
					}
					
				}
							
							
			}
			
		/** VALOR **/	
		}else if (rateInclusion.getExceptionRule().equals(RulerExceptionType.SECURITY_EXCEPTION_RATE.getCode())){
			
			if(rateInclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ONE)){
				if (Validations.validateIsNotNull(rateInclusion.getSecurities())){
					
					if(rateInclusion.getSecurities().getIdSecurityCodePk().equalsIgnoreCase(idSecurityPk)){
						return Boolean.TRUE;
					}
					
				}
			}
			
			else if(rateInclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ALL)){
		
				List<Security> securityList =(List<Security>)parameters.get(GeneralConstants.SECURITY_EXCEPTION_ALL_LIST); 

				for (Security securitys : securityList) {
					
					if(securitys.getIdSecurityCodePk().equalsIgnoreCase(idSecurityPk)){
						return Boolean.TRUE;
					}
					
				}
				
				
			}	
			
		/** PARTICIPANT **/
		}else if(rateInclusion.getExceptionRule().equals(RulerExceptionType.PARTICIPANT_EXCEPTION_RATE.getCode())){
			
			if(rateInclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ONE)){				

				if (Validations.validateIsNotNull(rateInclusion.getParticipant())){
					if(rateInclusion.getParticipant().getIdParticipantPk().equals(idParticipantPk)){
						return Boolean.TRUE;
					}
					
				}
				
				if (Validations.validateIsNotNull(rateInclusion.getHolderAccount())){
					if(rateInclusion.getHolderAccount().getIdHolderAccountPk().equals(idHolderAccountPk)){
						return Boolean.TRUE;
					}
					
				}
				
				if (Validations.validateIsNotNull(rateInclusion.getHolder())){
					if(rateInclusion.getHolder().getIdHolderPk().equals(idHolderPk)){
						return Boolean.TRUE;
					}
				}
				
				if (Validations.validateIsNotNull(rateInclusion.getSecurities())){ 
					if(rateInclusion.getSecurities().getIdSecurityCodePk().equals(idSecurityPk)){
						return Boolean.TRUE;
					}
				}
				
				
			}else if(rateInclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ALL)){
				List<Participant>    listClientParticipant;
									 
				listClientParticipant = (List<Participant>)parameters.get(GeneralConstants.PARTICIPANT_EXCEPTION_ALL_LIST) ;
						
			 	
				for (Participant participant : listClientParticipant) {
					if(participant.getIdParticipantPk().equals(idParticipantPk)){
						return Boolean.TRUE;
					}
					
				}
				
				
				
			}
			
		/** ISSUER **/
		}else if(rateInclusion.getExceptionRule().equals(RulerExceptionType.ISSUER_EXCEPTION_RATE.getCode())){
			if(rateInclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ONE)){
				
				if (Validations.validateIsNotNull(rateInclusion.getIssuer())){
					
					if(rateInclusion.getIssuer().getIdIssuerPk().equals(idIssuerPk)){
						return Boolean.TRUE;
					}
				}
				
				if (Validations.validateIsNotNull(rateInclusion.getIssuance())){ 							
					if(rateInclusion.getIssuance().getIdIssuanceCodePk().equals(idIssuancePk)){
						return Boolean.TRUE;
					}
				}
				
				if (Validations.validateIsNotNull(rateInclusion.getSecurities())){ 
					if(rateInclusion.getSecurities().getIdSecurityCodePk().equals(idSecurityPk)){
						return Boolean.TRUE;
					}
				}
				
				
			}else if(rateInclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ALL)){
								
				List<Issuer> issuerList = (List<Issuer>)parameters.get(GeneralConstants.ISSUER_EXCEPTION_ALL_LIST); 

				for (Issuer issuer : issuerList) {
					if(issuer.getIdIssuerPk().equals(idIssuerPk)){
						return Boolean.TRUE;
					}
					
				}
				
				
				
			}
			
		/** HOLDER **/	
		}else if(rateInclusion.getExceptionRule().equals(RulerExceptionType.HOLDER_EXCEPTION_RATE.getCode())){
			if(rateInclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ONE)){
				
				if (Validations.validateIsNotNull(rateInclusion.getHolder())){
					if(rateInclusion.getHolder().getIdHolderPk().equals(idHolderPk)){
						return Boolean.TRUE;
					}
					
				}
				
				if (Validations.validateIsNotNull(rateInclusion.getHolderAccount())){					
					if(rateInclusion.getHolderAccount().getIdHolderAccountPk().equals(idHolderAccountPk)){
						return Boolean.TRUE;
					}
				}	
				
				
			}else if(rateInclusion.getIndSelectAll().equals(PropertiesConstants.RDB_ALL)){

				List<Holder> holderList = (List<Holder>)parameters.get(GeneralConstants.HOLDER_EXCEPTION_ALL_LIST);
				
				for (Holder holder : holderList) {
					if(holder.getIdHolderPk().equals(idHolderPk)){
						return Boolean.TRUE;
					}
					
				}
				
			}
				
		}
		return Boolean.FALSE;
	}

}

