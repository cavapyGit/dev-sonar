package com.pradera.billing.billingexception.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.billing.billingrate.to.ServiceRateExceptionsTo;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RateExceptionTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class RateExceptionTo implements Serializable {


	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id rate exception. */

	private Long 		idRateException;
	
	/** The id service rate. */
	private Long 		idServiceRate;
	
	/** The type exception. */
	private Integer 	typeException;/**  Exclusion or Inclusion  **. */
	
	private String 		descriptionTypeException;
	
	/** The type rule. */
	private Integer 	typeRule;
	
	/** The description type rule. */
	private String 		descriptionTypeRule;
	
	/** The key id rate exception type exception. */
	private String 		keyIdRateExceptionTypeException;

	/** The security class. */
	private Integer		securityClass;
	
	/** The security class consult. */
	private Integer		securityClassConsult;
	
	/** The description security class. */
	private String		descriptionSecurityClass;
	
	/** The comments. */
	private String 		comments;
	
	/** The comments consult. */
	private String 		commentsConsult;
	
	/** The security. */
	private String		security;
	
	/** The security consult. */
	private String		securityConsult;
	
	/** The security mnemonic consult. */
	private String		securityMnemonicConsult;
	
	/** The security description consult. */
	private String		securityDescriptionConsult;
	
	/** The security code. */
	private String 		securityCode;
	
	/** The security code consult. */
	private String 		securityCodeConsult;
	
	/** The state description. */
	private String 		stateDescription;
	
	/** The date register. */
	private Date		dateRegister;
	
	/** The today date. */
	private Date		todayDate;
	
	
	/** The issuer. */
	private String		issuer;
	
	/** The participant. */
	private Long		participant;
	
	/** The holder. */
	private Long		holder;
	
	/** The holder account. */
	private Long		holderAccount;
	
 	/** The service rate to. */
	 private ServiceRateExceptionsTo 		serviceRateTo;
 	
	/** The service rate to list. */
	private List<ServiceRateExceptionsTo> serviceRateToList; 
	
	/**  USED TO CONSULT BEFORE SAVE THE EXCEPTION *. */
	private List<IssuanceTo>       issuanceToList;
	
	/** The holder account to list. */
	private List<HolderAccountTo>  holderAccountToList;
	

//	private List<RateExclusion> 	rateExclusionToList;
//	
//	private List<RateInclusion> 	rateInclusionToList;
	
//	private List<RuleParticipantTo> ruleParticipantToList;//
//	
//	private List<RuleIssuerTo> 		ruleIssuerToList;
	
	/** The exception state. */
private Integer 				exceptionState;	

	/**  flag for visibility on page. */
	private boolean 			flagRuleTypeClassValor;
	
	/** The flag rule type valor. */
	private boolean 			flagRuleTypeValor;
	
	/** The flag rule type participant. */
	private boolean 			flagRuleTypeParticipant;
	
	/** The flag rule type issuer. */
	private boolean 			flagRuleTypeIssuer;
	
	/** The flag rule type holder. */
	private boolean 			flagRuleTypeHolder;
	
	/** The flag rate consult. */
	private boolean 			flagRateConsult;
	

	/**  RULES OF EXCEPTIONS *. */
	private RuleSecurityClassTo ruleSecurityClassTo;
	
	/** The rule security to. */
	private RuleSecurityTo 		ruleSecurityTo;
	
	/** The rule participant to. */
	private RuleParticipantTo 	ruleParticipantTo;//
	
	/** The rule issuer to. */
	private RuleIssuerTo 		ruleIssuerTo;
	
	/** The rule holder to. */
	private RuleHolderTo		ruleHolderTo;
	

	
	/**
	 * Instantiates a new rate exception to.
	 */
	public RateExceptionTo(){
		
		ruleSecurityClassTo 	= 	new 	RuleSecurityClassTo();
		ruleSecurityTo 			= 	new 	RuleSecurityTo();
		ruleParticipantTo 		= 	new		RuleParticipantTo();
		ruleIssuerTo 			= 	new 	RuleIssuerTo();
		ruleHolderTo			=   new     RuleHolderTo();
	}
	
	/**   INDICATOR - ALL OR ONE * / 0 : ALL 1 : ONE. */
	private Integer indSelectAll;

	// AGREGAR TODAS LAS REGLAS

	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription() {
		return stateDescription;
	}

	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the new state description
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}

	/**
	 * Gets the security mnemonic consult.
	 *
	 * @return the security mnemonic consult
	 */
	public String getSecurityMnemonicConsult() {
		return securityMnemonicConsult;
	}

	/**
	 * Sets the security mnemonic consult.
	 *
	 * @param securityMnemonicConsult the new security mnemonic consult
	 */
	public void setSecurityMnemonicConsult(String securityMnemonicConsult) {
		this.securityMnemonicConsult = securityMnemonicConsult;
	}

	/**
	 * Gets the security description consult.
	 *
	 * @return the security description consult
	 */
	public String getSecurityDescriptionConsult() {
		return securityDescriptionConsult;
	}

	/**
	 * Sets the security description consult.
	 *
	 * @param securityDescriptionConsult the new security description consult
	 */
	public void setSecurityDescriptionConsult(String securityDescriptionConsult) {
		this.securityDescriptionConsult = securityDescriptionConsult;
	}
	
	/**
	 * Checks if is flag rate consult.
	 *
	 * @return true, if is flag rate consult
	 */
	public boolean isFlagRateConsult() {
		return flagRateConsult;
	}

	/**
	 * Sets the flag rate consult.
	 *
	 * @param flagRateConsult the new flag rate consult
	 */
	public void setFlagRateConsult(boolean flagRateConsult) {
		this.flagRateConsult = flagRateConsult;
	}

	/**
	 * Gets the security consult.
	 *
	 * @return the security consult
	 */
	public String getSecurityConsult() {
		return securityConsult;
	}

	/**
	 * Sets the security consult.
	 *
	 * @param securityConsult the new security consult
	 */
	public void setSecurityConsult(String securityConsult) {
		this.securityConsult = securityConsult;
	}

	/**
	 * Gets the security class consult.
	 *
	 * @return the security class consult
	 */
	public Integer getSecurityClassConsult() {
		return securityClassConsult;
	}

	/**
	 * Sets the security class consult.
	 *
	 * @param securityClassConsult the new security class consult
	 */
	public void setSecurityClassConsult(Integer securityClassConsult) {
		this.securityClassConsult = securityClassConsult;
	}

	

	/**
	 * Gets the security code.
	 *
	 * @return the securityCode
	 */
	public String getSecurityCode() {
		return securityCode;
	}

	/**
	 * Sets the security code.
	 *
	 * @param securityCode the securityCode to set
	 */
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	/**
	 * Gets the security code consult.
	 *
	 * @return the securityCodeConsult
	 */
	public String getSecurityCodeConsult() {
		return securityCodeConsult;
	}

	/**
	 * Sets the security code consult.
	 *
	 * @param securityCodeConsult the securityCodeConsult to set
	 */
	public void setSecurityCodeConsult(String securityCodeConsult) {
		this.securityCodeConsult = securityCodeConsult;
	}

	/**
	 * Checks if is flag rule type class valor.
	 *
	 * @return true, if is flag rule type class valor
	 */
	public boolean isFlagRuleTypeClassValor() {
		return flagRuleTypeClassValor;
	}

	/**
	 * Sets the flag rule type class valor.
	 *
	 * @param flagRuleTypeClassValor the new flag rule type class valor
	 */
	public void setFlagRuleTypeClassValor(boolean flagRuleTypeClassValor) {
		this.flagRuleTypeClassValor = flagRuleTypeClassValor;
	}

	/**
	 * Checks if is flag rule type valor.
	 *
	 * @return true, if is flag rule type valor
	 */
	public boolean isFlagRuleTypeValor() {
		return flagRuleTypeValor;
	}

	/**
	 * Sets the flag rule type valor.
	 *
	 * @param flagRuleTypeValor the new flag rule type valor
	 */
	public void setFlagRuleTypeValor(boolean flagRuleTypeValor) {
		this.flagRuleTypeValor = flagRuleTypeValor;
	}

	/**
	 * Checks if is flag rule type participant.
	 *
	 * @return true, if is flag rule type participant
	 */
	public boolean isFlagRuleTypeParticipant() {
		return flagRuleTypeParticipant;
	}

	/**
	 * Sets the flag rule type participant.
	 *
	 * @param flagRuleTypeParticipant the new flag rule type participant
	 */
	public void setFlagRuleTypeParticipant(boolean flagRuleTypeParticipant) {
		this.flagRuleTypeParticipant = flagRuleTypeParticipant;
	}

	/**
	 * Checks if is flag rule type issuer.
	 *
	 * @return true, if is flag rule type issuer
	 */
	public boolean isFlagRuleTypeIssuer() {
		return flagRuleTypeIssuer;
	}

	/**
	 * Sets the flag rule type issuer.
	 *
	 * @param flagRuleTypeIssuer the new flag rule type issuer
	 */
	public void setFlagRuleTypeIssuer(boolean flagRuleTypeIssuer) {
		this.flagRuleTypeIssuer = flagRuleTypeIssuer;
	}

	/**
	 * Checks if is flag rule type holder.
	 *
	 * @return true, if is flag rule type holder
	 */
	public boolean isFlagRuleTypeHolder() {
		return flagRuleTypeHolder;
	}

	/**
	 * Sets the flag rule type holder.
	 *
	 * @param flagRuleTypeHolder the new flag rule type holder
	 */
	public void setFlagRuleTypeHolder(boolean flagRuleTypeHolder) {
		this.flagRuleTypeHolder = flagRuleTypeHolder;
	}

	/**
	 * Gets the date register.
	 *
	 * @return the date register
	 */
	public Date getDateRegister() {
		return dateRegister;
	}

	/**
	 * Sets the date register.
	 *
	 * @param dateRegister the new date register
	 */
	public void setDateRegister(Date dateRegister) {
		this.dateRegister = dateRegister;
	}

	/**
	 * Gets the id rate exception.
	 *
	 * @return the id rate exception
	 */
	public Long getIdRateException() {
		return idRateException;
	}

	/**
	 * Sets the id rate exception.
	 *
	 * @param idRateException the new id rate exception
	 */
	public void setIdRateException(Long idRateException) {
		this.idRateException = idRateException;
	}


	/**
	 * Gets the today date.
	 *
	 * @return the today date
	 */
	public Date getTodayDate() {
		return todayDate;
	}

	/**
	 * Sets the today date.
	 *
	 * @param todayDate the new today date
	 */
	public void setTodayDate(Date todayDate) {
		this.todayDate = todayDate;
	}

	/**
	 * Gets the id service rate.
	 *
	 * @return the id service rate
	 */
	public Long getIdServiceRate() {
		return idServiceRate;
	}

	/**
	 * Sets the id service rate.
	 *
	 * @param idServiceRate the new id service rate
	 */
	public void setIdServiceRate(Long idServiceRate) {
		this.idServiceRate = idServiceRate;
	}

	/**
	 * Gets the type exception.
	 *
	 * @return the type exception
	 */
	public Integer getTypeException() {
		return typeException;
	}

	/**
	 * Sets the type exception.
	 *
	 * @param typeException the new type exception
	 */
	public void setTypeException(Integer typeException) {
		this.typeException = typeException;
	}

	/**
	 * Gets the type rule.
	 *
	 * @return the type rule
	 */
	public Integer getTypeRule() {
		return typeRule;
	}

	/**
	 * Sets the type rule.
	 *
	 * @param typeRule the new type rule
	 */
	public void setTypeRule(Integer typeRule) {
		this.typeRule = typeRule;
	}

	/**
	 * Gets the security class.
	 *
	 * @return the security class
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}

	/**
	 * Sets the security class.
	 *
	 * @param securityClass the new security class
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	/**
	 * Gets the comments.
	 *
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * Sets the comments.
	 *
	 * @param comments the new comments
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * Gets the comments consult.
	 *
	 * @return the comments consult
	 */
	public String getCommentsConsult() {
		return commentsConsult;
	}

	/**
	 * Sets the comments consult.
	 *
	 * @param commentsConsult the new comments consult
	 */
	public void setCommentsConsult(String commentsConsult) {
		this.commentsConsult = commentsConsult;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public String getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(String security) {
		this.security = security;
	}

	/**
	 * Gets the description type exception.
	 *
	 * @return the description type exception
	 */
	public String getDescriptionTypeException() {
		return descriptionTypeException;
	}

	/**
	 * Sets the description type exception.
	 *
	 * @param descriptionTypeException the new description type exception
	 */
	public void setDescriptionTypeException(String descriptionTypeException) {
		this.descriptionTypeException = descriptionTypeException;
	}

	/**
	 * Gets the description type rule.
	 *
	 * @return the description type rule
	 */
	public String getDescriptionTypeRule() {
		return descriptionTypeRule;
	}

	/**
	 * Sets the description type rule.
	 *
	 * @param descriptionTypeRule the new description type rule
	 */
	public void setDescriptionTypeRule(String descriptionTypeRule) {
		this.descriptionTypeRule = descriptionTypeRule;
	}

	/**
	 * Gets the descriptio security class.
	 *
	 * @return the descriptio security class
	 */
	public String getDescriptioSecurityClass() {
		return descriptionSecurityClass;
	}

	/**
	 * Sets the descriptio security class.
	 *
	 * @param descriptioSecurityClass the new descriptio security class
	 */
	public void setDescriptioSecurityClass(String descriptioSecurityClass) {
		this.descriptionSecurityClass = descriptioSecurityClass;
	}



	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public String getIssuer() {
		return issuer;
	}

	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Long getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Long participant) {
		this.participant = participant;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Long getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Long holder) {
		this.holder = holder;
	}

	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public Long getHolderAccount() {
		return holderAccount;
	}

	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(Long holderAccount) {
		this.holderAccount = holderAccount;
	}
//
//	public List<RateExclusion> getRateExclusionToList() {
//		return rateExclusionToList;
//	}
//
//	public void setRateExclusionToList(List<RateExclusion> rateExclusionToList) {
//		this.rateExclusionToList = rateExclusionToList;
//	}
//
//	public List<RateInclusion> getRateInclusionToList() {
//		return rateInclusionToList;
//	}
//
//	public void setRateInclusionToList(List<RateInclusion> rateInclusionToList) {
//		this.rateInclusionToList = rateInclusionToList;
//	}



	/**
 * Gets the issuance to list.
 *
 * @return the issuance to list
 */
public List<IssuanceTo> getIssuanceToList() {
		return issuanceToList;
	}

	/**
	 * Sets the issuance to list.
	 *
	 * @param issuanceToList the new issuance to list
	 */
	public void setIssuanceToList(List<IssuanceTo> issuanceToList) {
		this.issuanceToList = issuanceToList;
	}

	/**
	 * Gets the holder account to list.
	 *
	 * @return the holder account to list
	 */
	public List<HolderAccountTo> getHolderAccountToList() {
		return holderAccountToList;
	}

	/**
	 * Sets the holder account to list.
	 *
	 * @param holderAccountToList the new holder account to list
	 */
	public void setHolderAccountToList(List<HolderAccountTo> holderAccountToList) {
		this.holderAccountToList = holderAccountToList;
	}

	/**
	 * Disable flag rule type exception.
	 */
	public void disableFlagRuleTypeException() {
		this.flagRuleTypeClassValor = Boolean.FALSE;
		this.flagRuleTypeValor = Boolean.FALSE;
		this.flagRuleTypeParticipant = Boolean.FALSE;
		this.flagRuleTypeIssuer = Boolean.FALSE;
		this.flagRuleTypeHolder = Boolean.FALSE;
		this.flagRateConsult = Boolean.FALSE;
	}
//
//	public List<RuleParticipantTo> getRuleParticipantToList() {
//		return ruleParticipantToList;
//	}
//
//	public void setRuleParticipantToList(
//			List<RuleParticipantTo> ruleParticipantToList) {
//		this.ruleParticipantToList = ruleParticipantToList;
//	}

	/**
 * Gets the rule participant to.
 *
 * @return the rule participant to
 */
public RuleParticipantTo getRuleParticipantTo() {
		return ruleParticipantTo;
	}

	/**
	 * Sets the rule participant to.
	 *
	 * @param ruleParticipantTo the new rule participant to
	 */
	public void setRuleParticipantTo(RuleParticipantTo ruleParticipantTo) {
		this.ruleParticipantTo = ruleParticipantTo;
	}

	/**
	 * Gets the exception state.
	 *
	 * @return the exception state
	 */
	public Integer getExceptionState() {
		return exceptionState;
	}

	/**
	 * Sets the exception state.
	 *
	 * @param exceptionState the new exception state
	 */
	public void setExceptionState(Integer exceptionState) {
		this.exceptionState = exceptionState;
	}

	/**
	 * Gets the rule security class to.
	 *
	 * @return the rule security class to
	 */
	public RuleSecurityClassTo getRuleSecurityClassTo() {
		return ruleSecurityClassTo;
	}

	/**
	 * Sets the rule security class to.
	 *
	 * @param ruleSecurityClassTo the new rule security class to
	 */
	public void setRuleSecurityClassTo(RuleSecurityClassTo ruleSecurityClassTo) {
		this.ruleSecurityClassTo = ruleSecurityClassTo;
	}

	/**
	 * Gets the rule security to.
	 *
	 * @return the rule security to
	 */
	public RuleSecurityTo getRuleSecurityTo() {
		return ruleSecurityTo;
	}

	/**
	 * Sets the rule security to.
	 *
	 * @param ruleSecurityTo the new rule security to
	 */
	public void setRuleSecurityTo(RuleSecurityTo ruleSecurityTo) {
		this.ruleSecurityTo = ruleSecurityTo;
	}

	/**
	 * Gets the ind select all.
	 *
	 * @return the ind select all
	 */
	public Integer getIndSelectAll() {
		return indSelectAll;
	}

	/**
	 * Sets the ind select all.
	 *
	 * @param indSelectAll the new ind select all
	 */
	public void setIndSelectAll(Integer indSelectAll) {
		this.indSelectAll = indSelectAll;
	}
//
//	public List<RuleIssuerTo> getRuleIssuerToList() {
//		return ruleIssuerToList;
//	}
//
//	public void setRuleIssuerToList(List<RuleIssuerTo> ruleIssuerToList) {
//		this.ruleIssuerToList = ruleIssuerToList;
//	}

	/**
 * Gets the rule issuer to.
 *
 * @return the rule issuer to
 */
public RuleIssuerTo getRuleIssuerTo() {
		return ruleIssuerTo;
	}

	/**
	 * Sets the rule issuer to.
	 *
	 * @param ruleIssuerTo the new rule issuer to
	 */
	public void setRuleIssuerTo(RuleIssuerTo ruleIssuerTo) {
		this.ruleIssuerTo = ruleIssuerTo;
	}

	/**
	 * Gets the rule holder to.
	 *
	 * @return the rule holder to
	 */
	public RuleHolderTo getRuleHolderTo() {
		return ruleHolderTo;
	}

	/**
	 * Sets the rule holder to.
	 *
	 * @param ruleHolderTo the new rule holder to
	 */
	public void setRuleHolderTo(RuleHolderTo ruleHolderTo) {
		this.ruleHolderTo = ruleHolderTo;
	}

	/**
	 * Gets the key id rate exception type exception.
	 *
	 * @return the key id rate exception type exception
	 */
	public String getKeyIdRateExceptionTypeException() {
		keyIdRateExceptionTypeException = getIdRateException()+"-"+getTypeException();
		return keyIdRateExceptionTypeException;
	}

	/**
	 * Sets the key id rate exception type exception.
	 *
	 * @param keyIdRateExceptionTypeException the new key id rate exception type exception
	 */
	public void setKeyIdRateExceptionTypeException(
			String keyIdRateExceptionTypeException) {
		this.keyIdRateExceptionTypeException = keyIdRateExceptionTypeException;
	}


	/**
	 * Gets the description security class.
	 *
	 * @return the descriptionSecurityClass
	 */
	public String getDescriptionSecurityClass() {
		return descriptionSecurityClass;
	}

	/**
	 * Sets the description security class.
	 *
	 * @param descriptionSecurityClass the descriptionSecurityClass to set
	 */
	public void setDescriptionSecurityClass(String descriptionSecurityClass) {
		this.descriptionSecurityClass = descriptionSecurityClass;
	}

	/**
	 * Gets the service rate to.
	 *
	 * @return the serviceRateTo
	 */
	public ServiceRateExceptionsTo getServiceRateTo() {
		return serviceRateTo;
	}

	/**
	 * Sets the service rate to.
	 *
	 * @param serviceRateTo the serviceRateTo to set
	 */
	public void setServiceRateTo(ServiceRateExceptionsTo serviceRateTo) {
		this.serviceRateTo = serviceRateTo;
	}

	/**
	 * Gets the service rate to list.
	 *
	 * @return the serviceRateToList
	 */
	public List<ServiceRateExceptionsTo> getServiceRateToList() {
		return serviceRateToList;
	}

	/**
	 * Sets the service rate to list.
	 *
	 * @param serviceRateToList the serviceRateToList to set
	 */
	public void setServiceRateToList(List<ServiceRateExceptionsTo> serviceRateToList) {
		this.serviceRateToList = serviceRateToList;
	}


	
	
	
}
