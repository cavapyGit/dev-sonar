package com.pradera.billing.billingexception.to;

import java.io.Serializable;
import java.util.List;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RuleHolderTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class RuleHolderTo  implements Serializable{
		
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2995711057778534353L;
	
	/** The id holder pk. */
	private Long 					idHolderPk;
	
	/** The account number pk. */
	private Long 					accountNumberPk;
	
/** The holder selected. */
//	private Holder 					holderSelected;
	private HolderTo 					holderSelected;
	
	/** The holder account to list. */
	private List<HolderAccountTo>   holderAccountToList;  /**  list of account number`s *. */
	

	private String 				    descriptionConsult;
	
	/** The account number. */
	private Integer					accountNumber;
	
	/**  0 : All Holder  1 : One Holder. */
	private Integer				indicatorOneOrAll;
	
	
	/**
	 * Instantiates a new rule holder to.
	 */
	public RuleHolderTo(){
		holderSelected = new HolderTo();
	}


	/**
	 * Gets the id holder pk.
	 *
	 * @return the idHolderPk
	 */
	public Long getIdHolderPk() {
		return idHolderPk;
	}


	/**
	 * Sets the id holder pk.
	 *
	 * @param idHolderPk the idHolderPk to set
	 */
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}


	/**
	 * Gets the account number pk.
	 *
	 * @return the accountNumberPk
	 */
	public Long getAccountNumberPk() {
		return accountNumberPk;
	}


	/**
	 * Sets the account number pk.
	 *
	 * @param accountNumberPk the accountNumberPk to set
	 */
	public void setAccountNumberPk(Long accountNumberPk) {
		this.accountNumberPk = accountNumberPk;
	}


	/**
	 * Gets the holder selected.
	 *
	 * @return the holderSelected
	 */
	public HolderTo getHolderSelected() {
		return holderSelected;
	}


	/**
	 * Sets the holder selected.
	 *
	 * @param holderSelected the holderSelected to set
	 */
	public void setHolderSelected(HolderTo holderSelected) {
		this.holderSelected = holderSelected;
	}


	/**
	 * Gets the holder account to list.
	 *
	 * @return the holderAccountToList
	 */
	public List<HolderAccountTo> getHolderAccountToList() {
		return holderAccountToList;
	}


	/**
	 * Sets the holder account to list.
	 *
	 * @param holderAccountToList the holderAccountToList to set
	 */
	public void setHolderAccountToList(List<HolderAccountTo> holderAccountToList) {
		this.holderAccountToList = holderAccountToList;
	}


	/**
	 * Gets the description consult.
	 *
	 * @return the descriptionConsult
	 */
	public String getDescriptionConsult() {
		return descriptionConsult;
	}


	/**
	 * Sets the description consult.
	 *
	 * @param descriptionConsult the descriptionConsult to set
	 */
	public void setDescriptionConsult(String descriptionConsult) {
		this.descriptionConsult = descriptionConsult;
	}


	/**
	 * Gets the account number.
	 *
	 * @return the accountNumber
	 */
	public Integer getAccountNumber() {
		return accountNumber;
	}


	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}


	/**
	 * Gets the indicator one or all.
	 *
	 * @return the indicatorOneOrAll
	 */
	public Integer getIndicatorOneOrAll() {
		return indicatorOneOrAll;
	}


	/**
	 * Sets the indicator one or all.
	 *
	 * @param indicatorOneOrAll the indicatorOneOrAll to set
	 */
	public void setIndicatorOneOrAll(Integer indicatorOneOrAll) {
		this.indicatorOneOrAll = indicatorOneOrAll;
	}
	
	
}
