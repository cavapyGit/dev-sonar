package com.pradera.billing.billingexception.to;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ParticipantTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class ParticipantTo implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2616239928682825978L;
	
	/** The id participant pk. */
	private Long 		idParticipantPk;
	
	/** The full name participant. */
	private String 		fullNameParticipant;
	
	
	
	/**
	 * Instantiates a new participant to.
	 */
	public ParticipantTo() {
		// TODO Auto-generated constructor stub
	
	}
	
	
	/**
	 * Instantiates a new participant to.
	 *
	 * @param idParticipantPk the id participant pk
	 */
	public ParticipantTo(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}


	/**
	 * Instantiates a new participant to.
	 *
	 * @param idParticipantPk the id participant pk
	 * @param fullNameParticipant the full name participant
	 */
	public ParticipantTo(Long idParticipantPk, String fullNameParticipant) {
		this.idParticipantPk = idParticipantPk;
		this.fullNameParticipant = fullNameParticipant;
	}


	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	
	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	
	/**
	 * Gets the full name participant.
	 *
	 * @return the full name participant
	 */
	public String getFullNameParticipant() {
		return fullNameParticipant;
	}
	
	/**
	 * Sets the full name participant.
	 *
	 * @param fullNameParticipant the new full name participant
	 */
	public void setFullNameParticipant(String fullNameParticipant) {
		this.fullNameParticipant = fullNameParticipant;
	}
	
	

}
