package com.pradera.billing.billingexception.invoice.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.pradera.billing.billingexception.to.HolderTo;
import com.pradera.billing.billingexception.to.IssuerTo;
import com.pradera.billing.billingexception.to.ParticipantTo;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingEntityExceptionTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class BillingEntityExceptionTo implements Serializable{



	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id billing entity exception pk. */
	private Long 							idBillingEntityExceptionPk;

	/** The entity collection id. */
	private Integer 						entityCollectionId;

	/** The issuer. */
	private IssuerTo 						issuer;

	/** The participant. */
	private ParticipantTo 					participant;
	
	/** The holder. */
	private HolderTo 						holder;
	
	/** The id participant pk. */
	private Long 							idParticipantPk;
	
	/** The id holder pk. */
	private Long 							idHolderPk;
	
	/** The id issuer pk. */
	private String 							idIssuerPk;
	
	/** The description entity collection. */
	private String 							descriptionEntityCollection;
	
	/** The description entity code. */
	private String 							descriptionEntityCode;

	/** The exception invoice generations. */
	private ExceptionInvoiceGenerationTo 	exceptionInvoiceGenerations;
	
	/** The list id participant. */
	private List<Long> 						listIdParticipant=new ArrayList<>();
	
	/** The list id holder. */
	private List<Long> 						listIdHolder=new ArrayList<>();
	
	/** The list id issuer. */
	private List<String> 					listIdIssuer=new ArrayList<>();

	/**
	 * Instantiates a new billing entity exception to.
	 */
	public BillingEntityExceptionTo() {

	}

	/**
	 * Instantiates a new billing entity exception to.
	 *
	 * @param idBillingEntityExceptionPk the id billing entity exception pk
	 * @param entityCollectionId the entity collection id
	 * @param idParticipantPk the id participant pk
	 * @param idHolderPk the id holder pk
	 * @param idIssuerPk the id issuer pk
	 */
	public BillingEntityExceptionTo(Long idBillingEntityExceptionPk,
			Integer entityCollectionId, Long idParticipantPk, Long idHolderPk,
			String idIssuerPk) {
		this.idBillingEntityExceptionPk = idBillingEntityExceptionPk;
		this.entityCollectionId = entityCollectionId;
		this.idParticipantPk = idParticipantPk;
		this.idHolderPk = idHolderPk;
		this.idIssuerPk = idIssuerPk;
	}

	/**
	 * Gets the id billing entity exception pk.
	 *
	 * @return the idBillingEntityExceptionPk
	 */
	public Long getIdBillingEntityExceptionPk() {
		return idBillingEntityExceptionPk;
	}

	/**
	 * Sets the id billing entity exception pk.
	 *
	 * @param idBillingEntityExceptionPk the idBillingEntityExceptionPk to set
	 */
	public void setIdBillingEntityExceptionPk(Long idBillingEntityExceptionPk) {
		this.idBillingEntityExceptionPk = idBillingEntityExceptionPk;
	}

	/**
	 * Gets the entity collection id.
	 *
	 * @return the entityCollectionId
	 */
	public Integer getEntityCollectionId() {
		return entityCollectionId;
	}

	/**
	 * Sets the entity collection id.
	 *
	 * @param entityCollectionId the entityCollectionId to set
	 */
	public void setEntityCollectionId(Integer entityCollectionId) {
		this.entityCollectionId = entityCollectionId;
	}


	/**
	 * Gets the id participant pk.
	 *
	 * @return the idParticipantPk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the idParticipantPk to set
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * Gets the id holder pk.
	 *
	 * @return the idHolderPk
	 */
	public Long getIdHolderPk() {
		return idHolderPk;
	}

	/**
	 * Sets the id holder pk.
	 *
	 * @param idHolderPk the idHolderPk to set
	 */
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}

	/**
	 * Gets the id issuer pk.
	 *
	 * @return the idIssuerPk
	 */
	public String getIdIssuerPk() {
		return idIssuerPk;
	}

	/**
	 * Sets the id issuer pk.
	 *
	 * @param idIssuerPk the idIssuerPk to set
	 */
	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}

	/**
	 * Gets the exception invoice generations.
	 *
	 * @return the exceptionInvoiceGenerations
	 */
	public ExceptionInvoiceGenerationTo getExceptionInvoiceGenerations() {
		return exceptionInvoiceGenerations;
	}

	/**
	 * Sets the exception invoice generations.
	 *
	 * @param exceptionInvoiceGenerations the exceptionInvoiceGenerations to set
	 */
	public void setExceptionInvoiceGenerations(
			ExceptionInvoiceGenerationTo exceptionInvoiceGenerations) {
		this.exceptionInvoiceGenerations = exceptionInvoiceGenerations;
	}

	/**
	 * Gets the description entity collection.
	 *
	 * @return the descriptionEntityCollection
	 */
	public String getDescriptionEntityCollection() {
		return descriptionEntityCollection;
	}

	/**
	 * Sets the description entity collection.
	 *
	 * @param descriptionEntityCollection the descriptionEntityCollection to set
	 */
	public void setDescriptionEntityCollection(String descriptionEntityCollection) {
		this.descriptionEntityCollection = descriptionEntityCollection;
	}

	/**
	 * Gets the description entity code.
	 *
	 * @return the descriptionEntityCode
	 */
	public String getDescriptionEntityCode() {
		return descriptionEntityCode;
	}

	/**
	 * Sets the description entity code.
	 *
	 * @param descriptionEntityCode the descriptionEntityCode to set
	 */
	public void setDescriptionEntityCode(String descriptionEntityCode) {
		this.descriptionEntityCode = descriptionEntityCode;
	}

	/**
	 * Gets the list id participant.
	 *
	 * @return the listIdParticipant
	 */
	public List<Long> getListIdParticipant() {
		return listIdParticipant;
	}

	/**
	 * Sets the list id participant.
	 *
	 * @param listIdParticipant the listIdParticipant to set
	 */
	public void setListIdParticipant(List<Long> listIdParticipant) {
		this.listIdParticipant = listIdParticipant;
	}

	/**
	 * Gets the list id holder.
	 *
	 * @return the listIdHolder
	 */
	public List<Long> getListIdHolder() {
		return listIdHolder;
	}

	/**
	 * Sets the list id holder.
	 *
	 * @param listIdHolder the listIdHolder to set
	 */
	public void setListIdHolder(List<Long> listIdHolder) {
		this.listIdHolder = listIdHolder;
	}

	/**
	 * Gets the list id issuer.
	 *
	 * @return the listIdIssuer
	 */
	public List<String> getListIdIssuer() {
		return listIdIssuer;
	}

	/**
	 * Sets the list id issuer.
	 *
	 * @param listIdIssuer the listIdIssuer to set
	 */
	public void setListIdIssuer(List<String> listIdIssuer) {
		this.listIdIssuer = listIdIssuer;
	}

	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public IssuerTo getIssuer() {
		return issuer;
	}

	/**
	 * Sets the issuer.
	 *
	 * @param issuer the issuer to set
	 */
	public void setIssuer(IssuerTo issuer) {
		this.issuer = issuer;
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public ParticipantTo getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the participant to set
	 */
	public void setParticipant(ParticipantTo participant) {
		this.participant = participant;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public HolderTo getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the holder to set
	 */
	public void setHolder(HolderTo holder) {
		this.holder = holder;
	}
	
	

	
	
}