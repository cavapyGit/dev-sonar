package com.pradera.billing.billingexception.to;

import java.io.Serializable;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class IssuanceTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class IssuanceTo implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	
	/** The id issuance code pk. */
	private String 	idIssuanceCodePk;
	
	/** The description issuance. */
	private String 	descriptionIssuance;
	
	/** The id security code pk. */
	private String 	idSecurityCodePk;

	/** The selected. */
	private boolean selected;	
	 
	
	/**
	 * Instantiates a new issuance to.
	 */
	public IssuanceTo() {
	}

	

	/**
	 * Instantiates a new issuance to.
	 *
	 * @param idIssuanceCodePk the id issuance code pk
	 */
	public IssuanceTo(String idIssuanceCodePk) {
		this.idIssuanceCodePk = idIssuanceCodePk;
	}



	/**
	 * Gets the id issuance code pk.
	 *
	 * @return the id issuance code pk
	 */
	public String getIdIssuanceCodePk() {
		return idIssuanceCodePk;
	}

	/**
	 * Sets the id issuance code pk.
	 *
	 * @param idIssuanceCodePk the new id issuance code pk
	 */
	public void setIdIssuanceCodePk(String idIssuanceCodePk) {
		this.idIssuanceCodePk = idIssuanceCodePk;
	}

	
	/**
	 * Gets the description issuance.
	 *
	 * @return the description issuance
	 */
	public String getDescriptionIssuance() {
		return descriptionIssuance;
	}

	/**
	 * Sets the description issuance.
	 *
	 * @param descriptionIssuance the new description issuance
	 */
	public void setDescriptionIssuance(String descriptionIssuance) {
		this.descriptionIssuance = descriptionIssuance;
	}

	/**
	 * Gets the id security code pk.
	 *
	 * @return the id security code pk
	 */
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}
	
	/**
	 * Sets the id security code pk.
	 *
	 * @param idSecurityCodePk the new id security code pk
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	/**
	 * Gets the serialversionuid.
	 *
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	/**
	 * Checks if is selected.
	 *
	 * @return true, if is selected
	 */
	public boolean isSelected() {
		return selected;
	}
	
	/**
	 * Sets the selected.
	 *
	 * @param selected the new selected
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}


	
}
