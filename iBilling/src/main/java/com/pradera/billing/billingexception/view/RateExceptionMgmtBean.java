package com.pradera.billing.billingexception.view;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;
import org.primefaces.event.SelectEvent;

import com.pradera.billing.billingexception.facade.RateExceptionServiceFacade;
import com.pradera.billing.billingexception.to.HolderAccountTo;
import com.pradera.billing.billingexception.to.HolderTo;
import com.pradera.billing.billingexception.to.IssuanceTo;
import com.pradera.billing.billingexception.to.IssuerTo;
import com.pradera.billing.billingexception.to.RateExceptionTo;
import com.pradera.billing.billingexception.to.RuleHolderTo;
import com.pradera.billing.billingexception.to.RuleIssuerTo;
import com.pradera.billing.billingexception.to.RuleParticipantTo;
import com.pradera.billing.billingexception.to.RuleSecurityClassTo;
import com.pradera.billing.billingexception.to.RuleSecurityTo;
import com.pradera.billing.billingexception.to.SecurityTo;
import com.pradera.billing.billingrate.facade.BillingRateMgmtServiceFacade;
import com.pradera.billing.billingrate.to.ServiceRateExceptionsTo;
import com.pradera.billing.billingrate.to.ServiceRateTo;
import com.pradera.billing.billingservice.facade.BillingServiceMgmtServiceFacade;
import com.pradera.billing.billingservice.to.BillingServiceTo;
import com.pradera.billing.billingservice.view.BillingServiceMgmtBean;
import com.pradera.billing.common.utils.BillingUtils;
import com.pradera.billing.helper.view.BillingServiceHelperBean;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.view.HolderHelperBean;
import com.pradera.core.component.helperui.view.IssuanceHelperBean;
import com.pradera.core.component.helperui.view.IssuerHelperBean;
import com.pradera.core.component.helperui.view.SecurityHelpBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.RateExclusion;
import com.pradera.model.billing.RateInclusion;
import com.pradera.model.billing.ServiceRate;
import com.pradera.model.billing.type.RateExceptionStatus;
import com.pradera.model.billing.type.RulerExceptionType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RateExceptionMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class RateExceptionMgmtBean extends GenericBaseBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;

	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade 			helperComponentFacade;
	
	/** The billing rate mgmt service facade. */
	@EJB
	BillingRateMgmtServiceFacade 	billingRateMgmtServiceFacade ;
	
	/** The general parameters service. */
	@EJB
	ParameterServiceBean 			generalParametersService;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade 		generalParametersFacade;	
	
	/** The rate exception service facade. */
	@EJB
	RateExceptionServiceFacade 		rateExceptionServiceFacade;
	
	/** The billing service mgmt service facade. */
	@EJB
	BillingServiceMgmtServiceFacade billingServiceMgmtServiceFacade;
	
	/** The billing service helper bean. */
	@Inject
	BillingServiceHelperBean 		billingServiceHelperBean;
	
	/** The issuer helper bean. */
	@Inject
	IssuerHelperBean 				issuerHelperBean;
	
//	@Inject
//	SecuritiesHelperBean 			securitiesHelperBean;
	
	/** The issuance helper bean. */
@Inject
	IssuanceHelperBean 				issuanceHelperBean;

	/** The holder helper bean. */
	@Inject
	HolderHelperBean 				holderHelperBean;
	
	/** The Constant log. */
	private static final Logger log = Logger.getLogger(BillingServiceMgmtBean.class);
	
	/**  parameters for add  exception rules. */
	private BillingService 			billingService ;
	
	/** The rule participant to. */
	private RuleParticipantTo	 	ruleParticipantTo;
	
	/** The rule security class to. */
	private RuleSecurityClassTo  	ruleSecurityClassTo;
	
	/** The rule security to. */
	private RuleSecurityTo 		 	ruleSecurityTo;
	
	/** The rule issuer to. */
	private RuleIssuerTo 		 	ruleIssuerTo;
	
	/** The rule holder to. */
	private RuleHolderTo 	     	ruleHolderTo;
	
	/** The issuance to. */
	private IssuanceTo 			 	issuanceTo;	

		

	/** The rate exception to. */
	private RateExceptionTo 		rateExceptionTo;
	
	/** The rate exception consult to. */
	private RateExceptionTo 		rateExceptionConsultTo;
	
	/** The rate exception selection. */
	private RateExceptionTo 		rateExceptionSelection;
	
	/** The security mnemonic consult. */
	private String 					securityMnemonicConsult;
	
	/** The security description consult. */
	private String 					securityDescriptionConsult;
	
	
	/** The rule selected. */
	private Integer 				ruleSelected;
	
	/** The security class pk. */
	private Integer 				securityClassPk;
	
	
	/** new Variables*. */
	private	Issuer					issuerView;
	
	/** The today date. */
	private Date 					todayDate;
	
	/** The rdb one. */
	private Integer 				rdbOne=PropertiesConstants.RDB_ONE;
	
	/** The rdb all. */
	private Integer 				rdbAll=PropertiesConstants.RDB_ALL;
	
	/** The show rdb num. */
	private Boolean 				showRdbNum;
	
	/** The ind num rule. */
	private Integer 				indNumRule;

	/** The security help bean. */
	@Inject
	private SecurityHelpBean 		securityHelpBean;
	
	/** The show rule class. */	

	private Boolean 	showRuleClass;
	
	/** The show rule issuer. */
	private Boolean 	showRuleIssuer;
	
	/** The show rule security. */
	private Boolean 	showRuleSecurity;
	
	/** The show rule participant. */
	private Boolean 	showRuleParticipant;
	
	/** The show rule holder. */
	private Boolean 	showRuleHolder;
	
	/** The show list exception. */
	private Boolean 	showListException;
	
    /** The disable issuer. */
    private Boolean 	disableIssuer;
    
    /** The disable security. */
    private Boolean 	disableSecurity;
    
    /** The disable class security. */
    private Boolean 	disableClassSecurity;

    /** The flag mopdify. */
    private Boolean 	flagMopdify=Boolean.FALSE;
    
    /** The flag field exeption. */
    private Boolean 	flagFieldExeption=Boolean.FALSE;
    
    /** The flag rules. */
    private Boolean 	flagRules=Boolean.FALSE;
    
    /** The register frm. */
    private Boolean 	registerFrm=Boolean.FALSE;
    
    /** The cbm save. */
    private Boolean 	cbmSave=Boolean.FALSE;
    
    /** The cbm modify. */
    private Boolean 	cbmModify=Boolean.FALSE;      
    
    
    /** *   Attribute for Rule Participants. */
    
	private Boolean 		showCmbParticipant;
	
	/** The show pnl billing service. */
	private Boolean 		showPnlBillingService;
	
	/** The participant selected. */
	private Long 			participantSelected;
	
	/** The holder to list. */
	private	List<HolderTo> 	holderToList;
	
	/** Helpers. */
	private Holder			holderHelp;
	
	/** The security help. */
	private Security 		securityHelp;
	
	/** The security view. */
	private Security 		securityView;


	/** *   Attribute for Rule Issuer. */
	private	List<IssuanceTo> 								issuanceToList;

	/** The service rate to selelected. */
	private ServiceRateTo 					serviceRateToSelelected;
	
	/** The service rate to last select. */
	private ServiceRateTo 					serviceRateToLastSelect;
	
	/** The service rate to. */
	private ServiceRateTo 					serviceRateTo = new ServiceRateTo();
	
	/** *   Attribute for Rule Holder. */	
    private List<HolderAccountTo> 							holderAccountToList;

	/** The param table security classes. */
	private List<ParameterTable> 							paramTableSecurityClasses = new ArrayList<ParameterTable>();
	
	/** The list rules type. */
	private List<ParameterTable> 							listRulesType;
	
	/** The list exception. */
	private List<ParameterTable> 							listException;
	
	/** The list rate type. */
	private List<ParameterTable> 							listRateType;
	
	/** The list exception state. */
	private List<ParameterTable> 							listExceptionState;
	
	/** The list client participant. */
	private List<Participant>    							listClientParticipant;	

	/** The service rate to data model. */
	private GenericDataModel<ServiceRateTo>   serviceRateToDataModel;
	
	/** The service rate to consult data model. */
	private GenericDataModel<ServiceRateTo>   serviceRateToConsultDataModel;
	
	/** The rate exception to data model. */
	private GenericDataModel<RateExceptionTo> 				rateExceptionToDataModel;
	
	/** The service rate consult data model. */
	private GenericDataModel<ServiceRate>     				serviceRateConsultDataModel;
	
	/** The holder to data model. */
	private GenericDataModel<HolderTo>        				holderToDataModel;
	
	/** The holder to consult data model. */
	private GenericDataModel<HolderTo>        				holderToConsultDataModel;
	
	/** The issuance to data model. */
	private GenericDataModel<IssuanceTo>      				issuanceToDataModel;
	
	/** The issuance to consult data model. */
	private GenericDataModel<IssuanceTo>      				issuanceToConsultDataModel;
	
	/** The holder account to data model. */
	private GenericDataModel<HolderAccountTo> 				holderAccountToDataModel;
	
	/** The holder account to consult data model. */
	private GenericDataModel<HolderAccountTo> 				holderAccountToConsultDataModel;
	
	/** The list rate exception to. */
	private List<RateExceptionTo> 							listRateExceptionTo; 	// used to deleted
	
	/** The list rate exception to modify. */
	private List<RateExceptionTo> 							listRateExceptionToModify;   

	/** The list service rate to. */
	private List<ServiceRateTo>   							listServiceRateTo;
	
	/** The rate exception to list original. */
	private List<RateExceptionTo> 							rateExceptionToListOriginal;  // used to compare before delete
	
	/** The lst rate exception remove. */
	private List<RateExceptionTo> 							lstRateExceptionRemove = new ArrayList<RateExceptionTo>();
	
	
	
	


	/**
	 * Gets the issuer view.
	 *
	 * @return the issuerView
	 */
	public Issuer getIssuerView() {
		return issuerView;
	}

	/**
	 * Sets the issuer view.
	 *
	 * @param issuerView the issuerView to set
	 */
	public void setIssuerView(Issuer issuerView) {
		this.issuerView = issuerView;
	}

	/**
	 * Gets the security view.
	 *
	 * @return the securityView
	 */
	public Security getSecurityView() {
		return securityView;
	}

	/**
	 * Sets the security view.
	 *
	 * @param securityView the securityView to set
	 */
	public void setSecurityView(Security securityView) {
		this.securityView = securityView;
	}

	/**
	 * Gets the holder help.
	 *
	 * @return the holderHelp
	 */
	public Holder getHolderHelp() {
		return holderHelp;
	}

	/**
	 * Sets the holder help.
	 *
	 * @param holderHelp the holderHelp to set
	 */
	public void setHolderHelp(Holder holderHelp) {
		this.holderHelp = holderHelp;
	}

	/**
	 * Gets the security help.
	 *
	 * @return the securityHelp
	 */
	public Security getSecurityHelp() {
		return securityHelp;
	}

	/**
	 * Sets the security help.
	 *
	 * @param securityHelp the securityHelp to set
	 */
	public void setSecurityHelp(Security securityHelp) {
		this.securityHelp = securityHelp;
	}
	

	/**
	 * Gets the rate exception to.
	 *
	 * @return the rateExceptionTo
	 */
	public RateExceptionTo getRateExceptionTo() {
		return rateExceptionTo;
	}

	/**
	 * Sets the rate exception to.
	 *
	 * @param rateExceptionTo the rateExceptionTo to set
	 */
	public void setRateExceptionTo(RateExceptionTo rateExceptionTo) {
		this.rateExceptionTo = rateExceptionTo;
	}

	/**
	 * Gets the rate exception consult to.
	 *
	 * @return the rateExceptionConsultTo
	 */
	public RateExceptionTo getRateExceptionConsultTo() {
		return rateExceptionConsultTo;
	}

	/**
	 * Sets the rate exception consult to.
	 *
	 * @param rateExceptionConsultTo the rateExceptionConsultTo to set
	 */
	public void setRateExceptionConsultTo(RateExceptionTo rateExceptionConsultTo) {
		this.rateExceptionConsultTo = rateExceptionConsultTo;
	}

	/**
	 * Gets the rate exception selection.
	 *
	 * @return the rateExceptionSelection
	 */
	public RateExceptionTo getRateExceptionSelection() {
		return rateExceptionSelection;
	}

	/**
	 * Sets the rate exception selection.
	 *
	 * @param rateExceptionSelection the rateExceptionSelection to set
	 */
	public void setRateExceptionSelection(RateExceptionTo rateExceptionSelection) {
		this.rateExceptionSelection = rateExceptionSelection;
	}

	/**
	 * Gets the security mnemonic consult.
	 *
	 * @return the securityMnemonicConsult
	 */
	public String getSecurityMnemonicConsult() {
		return securityMnemonicConsult;
	}

	/**
	 * Sets the security mnemonic consult.
	 *
	 * @param securityMnemonicConsult the securityMnemonicConsult to set
	 */
	public void setSecurityMnemonicConsult(String securityMnemonicConsult) {
		this.securityMnemonicConsult = securityMnemonicConsult;
	}

	/**
	 * Gets the security description consult.
	 *
	 * @return the securityDescriptionConsult
	 */
	public String getSecurityDescriptionConsult() {
		return securityDescriptionConsult;
	}

	/**
	 * Sets the security description consult.
	 *
	 * @param securityDescriptionConsult the securityDescriptionConsult to set
	 */
	public void setSecurityDescriptionConsult(String securityDescriptionConsult) {
		this.securityDescriptionConsult = securityDescriptionConsult;
	}

	/**
	 * Gets the rule selected.
	 *
	 * @return the ruleSelected
	 */
	public Integer getRuleSelected() {
		return ruleSelected;
	}

	/**
	 * Sets the rule selected.
	 *
	 * @param ruleSelected the ruleSelected to set
	 */
	public void setRuleSelected(Integer ruleSelected) {
		this.ruleSelected = ruleSelected;
	}

	/**
	 * Gets the security class pk.
	 *
	 * @return the securityClassPk
	 */
	public Integer getSecurityClassPk() {
		return securityClassPk;
	}

	/**
	 * Sets the security class pk.
	 *
	 * @param securityClassPk the securityClassPk to set
	 */
	public void setSecurityClassPk(Integer securityClassPk) {
		this.securityClassPk = securityClassPk;
	}

	/**
	 * Gets the today date.
	 *
	 * @return the todayDate
	 */
	public Date getTodayDate() {
		return todayDate;
	}

	/**
	 * Sets the today date.
	 *
	 * @param todayDate the todayDate to set
	 */
	public void setTodayDate(Date todayDate) {
		this.todayDate = todayDate;
	}

	/**
	 * Gets the rdb one.
	 *
	 * @return the rdbOne
	 */
	public Integer getRdbOne() {
		return rdbOne;
	}

	/**
	 * Sets the rdb one.
	 *
	 * @param rdbOne the rdbOne to set
	 */
	public void setRdbOne(Integer rdbOne) {
		this.rdbOne = rdbOne;
	}

	/**
	 * Gets the rdb all.
	 *
	 * @return the rdbAll
	 */
	public Integer getRdbAll() {
		return rdbAll;
	}

	/**
	 * Sets the rdb all.
	 *
	 * @param rdbAll the rdbAll to set
	 */
	public void setRdbAll(Integer rdbAll) {
		this.rdbAll = rdbAll;
	}

	/**
	 * Gets the security help bean.
	 *
	 * @return the securityHelpBean
	 */
	public SecurityHelpBean getSecurityHelpBean() {
		return securityHelpBean;
	}

	/**
	 * Sets the security help bean.
	 *
	 * @param securityHelpBean the securityHelpBean to set
	 */
	public void setSecurityHelpBean(SecurityHelpBean securityHelpBean) {
		this.securityHelpBean = securityHelpBean;
	}

	/**
	 * Gets the show rule class.
	 *
	 * @return the showRuleClass
	 */
	public Boolean getShowRuleClass() {
		return showRuleClass;
	}

	/**
	 * Sets the show rule class.
	 *
	 * @param showRuleClass the showRuleClass to set
	 */
	public void setShowRuleClass(Boolean showRuleClass) {
		this.showRuleClass = showRuleClass;
	}

	/**
	 * Gets the show rule issuer.
	 *
	 * @return the showRuleIssuer
	 */
	public Boolean getShowRuleIssuer() {
		return showRuleIssuer;
	}

	/**
	 * Sets the show rule issuer.
	 *
	 * @param showRuleIssuer the showRuleIssuer to set
	 */
	public void setShowRuleIssuer(Boolean showRuleIssuer) {
		this.showRuleIssuer = showRuleIssuer;
	}

	/**
	 * Gets the show rule security.
	 *
	 * @return the showRuleSecurity
	 */
	public Boolean getShowRuleSecurity() {
		return showRuleSecurity;
	}

	/**
	 * Sets the show rule security.
	 *
	 * @param showRuleSecurity the showRuleSecurity to set
	 */
	public void setShowRuleSecurity(Boolean showRuleSecurity) {
		this.showRuleSecurity = showRuleSecurity;
	}

	/**
	 * Gets the show rule participant.
	 *
	 * @return the showRuleParticipant
	 */
	public Boolean getShowRuleParticipant() {
		return showRuleParticipant;
	}

	/**
	 * Sets the show rule participant.
	 *
	 * @param showRuleParticipant the showRuleParticipant to set
	 */
	public void setShowRuleParticipant(Boolean showRuleParticipant) {
		this.showRuleParticipant = showRuleParticipant;
	}

	/**
	 * Gets the show rule holder.
	 *
	 * @return the showRuleHolder
	 */
	public Boolean getShowRuleHolder() {
		return showRuleHolder;
	}

	/**
	 * Sets the show rule holder.
	 *
	 * @param showRuleHolder the showRuleHolder to set
	 */
	public void setShowRuleHolder(Boolean showRuleHolder) {
		this.showRuleHolder = showRuleHolder;
	}

	/**
	 * Gets the show list exception.
	 *
	 * @return the showListException
	 */
	public Boolean getShowListException() {
		return showListException;
	}

	/**
	 * Sets the show list exception.
	 *
	 * @param showListException the showListException to set
	 */
	public void setShowListException(Boolean showListException) {
		this.showListException = showListException;
	}

	/**
	 * Gets the disable issuer.
	 *
	 * @return the disableIssuer
	 */
	public Boolean getDisableIssuer() {
		return disableIssuer;
	}

	/**
	 * Sets the disable issuer.
	 *
	 * @param disableIssuer the disableIssuer to set
	 */
	public void setDisableIssuer(Boolean disableIssuer) {
		this.disableIssuer = disableIssuer;
	}

	/**
	 * Gets the disable security.
	 *
	 * @return the disableSecurity
	 */
	public Boolean getDisableSecurity() {
		return disableSecurity;
	}

	/**
	 * Sets the disable security.
	 *
	 * @param disableSecurity the disableSecurity to set
	 */
	public void setDisableSecurity(Boolean disableSecurity) {
		this.disableSecurity = disableSecurity;
	}

	/**
	 * Gets the disable class security.
	 *
	 * @return the disableClassSecurity
	 */
	public Boolean getDisableClassSecurity() {
		return disableClassSecurity;
	}

	/**
	 * Sets the disable class security.
	 *
	 * @param disableClassSecurity the disableClassSecurity to set
	 */
	public void setDisableClassSecurity(Boolean disableClassSecurity) {
		this.disableClassSecurity = disableClassSecurity;
	}

	/**
	 * Gets the flag mopdify.
	 *
	 * @return the flagMopdify
	 */
	public Boolean getFlagMopdify() {
		return flagMopdify;
	}

	/**
	 * Sets the flag mopdify.
	 *
	 * @param flagMopdify the flagMopdify to set
	 */
	public void setFlagMopdify(Boolean flagMopdify) {
		this.flagMopdify = flagMopdify;
	}

	/**
	 * Gets the flag field exeption.
	 *
	 * @return the flagFieldExeption
	 */
	public Boolean getFlagFieldExeption() {
		return flagFieldExeption;
	}

	/**
	 * Sets the flag field exeption.
	 *
	 * @param flagFieldExeption the flagFieldExeption to set
	 */
	public void setFlagFieldExeption(Boolean flagFieldExeption) {
		this.flagFieldExeption = flagFieldExeption;
	}

	/**
	 * Gets the flag rules.
	 *
	 * @return the flagRules
	 */
	public Boolean getFlagRules() {
		return flagRules;
	}

	/**
	 * Sets the flag rules.
	 *
	 * @param flagRules the flagRules to set
	 */
	public void setFlagRules(Boolean flagRules) {
		this.flagRules = flagRules;
	}

	/**
	 * Gets the register frm.
	 *
	 * @return the registerFrm
	 */
	public Boolean getRegisterFrm() {
		return registerFrm;
	}

	/**
	 * Sets the register frm.
	 *
	 * @param registerFrm the registerFrm to set
	 */
	public void setRegisterFrm(Boolean registerFrm) {
		this.registerFrm = registerFrm;
	}

	/**
	 * Gets the cbm save.
	 *
	 * @return the cbmSave
	 */
	public Boolean getCbmSave() {
		return cbmSave;
	}

	/**
	 * Sets the cbm save.
	 *
	 * @param cbmSave the cbmSave to set
	 */
	public void setCbmSave(Boolean cbmSave) {
		this.cbmSave = cbmSave;
	}

	/**
	 * Gets the cbm modify.
	 *
	 * @return the cbmModify
	 */
	public Boolean getCbmModify() {
		return cbmModify;
	}

	/**
	 * Sets the cbm modify.
	 *
	 * @param cbmModify the cbmModify to set
	 */
	public void setCbmModify(Boolean cbmModify) {
		this.cbmModify = cbmModify;
	}

	/**
	 * Gets the holder to list.
	 *
	 * @return the holderToList
	 */
	public List<HolderTo> getHolderToList() {
		return holderToList;
	}

	/**
	 * Sets the holder to list.
	 *
	 * @param holderToList the holderToList to set
	 */
	public void setHolderToList(List<HolderTo> holderToList) {
		this.holderToList = holderToList;
	}

	/**
	 * Gets the issuance to list.
	 *
	 * @return the issuanceToList
	 */
	public List<IssuanceTo> getIssuanceToList() {
		return issuanceToList;
	}

	/**
	 * Sets the issuance to list.
	 *
	 * @param issuanceToList the issuanceToList to set
	 */
	public void setIssuanceToList(List<IssuanceTo> issuanceToList) {
		this.issuanceToList = issuanceToList;
	}

	/**
	 * Gets the service rate to selelected.
	 *
	 * @return the serviceRateToSelelected
	 */
	public ServiceRateTo getServiceRateToSelelected() {
		return serviceRateToSelelected;
	}

	/**
	 * Sets the service rate to selelected.
	 *
	 * @param serviceRateToSelelected the serviceRateToSelelected to set
	 */
	public void setServiceRateToSelelected(ServiceRateTo serviceRateToSelelected) {
		this.serviceRateToSelelected = serviceRateToSelelected;
	}

	/**
	 * Gets the service rate to last select.
	 *
	 * @return the serviceRateToLastSelect
	 */
	public ServiceRateTo getServiceRateToLastSelect() {
		return serviceRateToLastSelect;
	}

	/**
	 * Sets the service rate to last select.
	 *
	 * @param serviceRateToLastSelect the serviceRateToLastSelect to set
	 */
	public void setServiceRateToLastSelect(ServiceRateTo serviceRateToLastSelect) {
		this.serviceRateToLastSelect = serviceRateToLastSelect;
	}

	/**
	 * Gets the service rate to.
	 *
	 * @return the serviceRateTo
	 */
	public ServiceRateTo getServiceRateTo() {
		return serviceRateTo;
	}

	/**
	 * Sets the service rate to.
	 *
	 * @param serviceRateTo the serviceRateTo to set
	 */
	public void setServiceRateTo(ServiceRateTo serviceRateTo) {
		this.serviceRateTo = serviceRateTo;
	}

	/**
	 * Gets the param table security classes.
	 *
	 * @return the paramTableSecurityClasses
	 */
	public List<ParameterTable> getParamTableSecurityClasses() {
		return paramTableSecurityClasses;
	}

	/**
	 * Sets the param table security classes.
	 *
	 * @param paramTableSecurityClasses the paramTableSecurityClasses to set
	 */
	public void setParamTableSecurityClasses(
			List<ParameterTable> paramTableSecurityClasses) {
		this.paramTableSecurityClasses = paramTableSecurityClasses;
	}

	/**
	 * Gets the list rules type.
	 *
	 * @return the listRulesType
	 */
	public List<ParameterTable> getListRulesType() {
		return listRulesType;
	}

	/**
	 * Sets the list rules type.
	 *
	 * @param listRulesType the listRulesType to set
	 */
	public void setListRulesType(List<ParameterTable> listRulesType) {
		this.listRulesType = listRulesType;
	}

	/**
	 * Gets the list exception.
	 *
	 * @return the listException
	 */
	public List<ParameterTable> getListException() {
		return listException;
	}

	/**
	 * Sets the list exception.
	 *
	 * @param listException the listException to set
	 */
	public void setListException(List<ParameterTable> listException) {
		this.listException = listException;
	}

	/**
	 * Gets the list rate type.
	 *
	 * @return the listRateType
	 */
	public List<ParameterTable> getListRateType() {
		return listRateType;
	}

	/**
	 * Sets the list rate type.
	 *
	 * @param listRateType the listRateType to set
	 */
	public void setListRateType(List<ParameterTable> listRateType) {
		this.listRateType = listRateType;
	}

	/**
	 * Gets the list exception state.
	 *
	 * @return the listExceptionState
	 */
	public List<ParameterTable> getListExceptionState() {
		return listExceptionState;
	}

	/**
	 * Sets the list exception state.
	 *
	 * @param listExceptionState the listExceptionState to set
	 */
	public void setListExceptionState(List<ParameterTable> listExceptionState) {
		this.listExceptionState = listExceptionState;
	}

	/**
	 * Gets the service rate to data model.
	 *
	 * @return the serviceRateToDataModel
	 */
	public GenericDataModel<ServiceRateTo> getServiceRateToDataModel() {
		return serviceRateToDataModel;
	}

	/**
	 * Sets the service rate to data model.
	 *
	 * @param serviceRateToDataModel the serviceRateToDataModel to set
	 */
	public void setServiceRateToDataModel(
			GenericDataModel<ServiceRateTo> serviceRateToDataModel) {
		this.serviceRateToDataModel = serviceRateToDataModel;
	}

	/**
	 * Gets the service rate to consult data model.
	 *
	 * @return the serviceRateToConsultDataModel
	 */
	public GenericDataModel<ServiceRateTo> getServiceRateToConsultDataModel() {
		return serviceRateToConsultDataModel;
	}

	/**
	 * Sets the service rate to consult data model.
	 *
	 * @param serviceRateToConsultDataModel the serviceRateToConsultDataModel to set
	 */
	public void setServiceRateToConsultDataModel(
			GenericDataModel<ServiceRateTo> serviceRateToConsultDataModel) {
		this.serviceRateToConsultDataModel = serviceRateToConsultDataModel;
	}

	/**
	 * Gets the rate exception to data model.
	 *
	 * @return the rateExceptionToDataModel
	 */
	public GenericDataModel<RateExceptionTo> getRateExceptionToDataModel() {
		return rateExceptionToDataModel;
	}

	/**
	 * Sets the rate exception to data model.
	 *
	 * @param rateExceptionToDataModel the rateExceptionToDataModel to set
	 */
	public void setRateExceptionToDataModel(
			GenericDataModel<RateExceptionTo> rateExceptionToDataModel) {
		this.rateExceptionToDataModel = rateExceptionToDataModel;
	}

	/**
	 * Gets the service rate consult data model.
	 *
	 * @return the serviceRateConsultDataModel
	 */
	public GenericDataModel<ServiceRate> getServiceRateConsultDataModel() {
		return serviceRateConsultDataModel;
	}

	/**
	 * Sets the service rate consult data model.
	 *
	 * @param serviceRateConsultDataModel the serviceRateConsultDataModel to set
	 */
	public void setServiceRateConsultDataModel(
			GenericDataModel<ServiceRate> serviceRateConsultDataModel) {
		this.serviceRateConsultDataModel = serviceRateConsultDataModel;
	}

	/**
	 * Gets the list rate exception to.
	 *
	 * @return the listRateExceptionTo
	 */
	public List<RateExceptionTo> getListRateExceptionTo() {
		return listRateExceptionTo;
	}

	/**
	 * Sets the list rate exception to.
	 *
	 * @param listRateExceptionTo the listRateExceptionTo to set
	 */
	public void setListRateExceptionTo(List<RateExceptionTo> listRateExceptionTo) {
		this.listRateExceptionTo = listRateExceptionTo;
	}

	/**
	 * Gets the list rate exception to modify.
	 *
	 * @return the listRateExceptionToModify
	 */
	public List<RateExceptionTo> getListRateExceptionToModify() {
		return listRateExceptionToModify;
	}

	/**
	 * Sets the list rate exception to modify.
	 *
	 * @param listRateExceptionToModify the listRateExceptionToModify to set
	 */
	public void setListRateExceptionToModify(
			List<RateExceptionTo> listRateExceptionToModify) {
		this.listRateExceptionToModify = listRateExceptionToModify;
	}

	/**
	 * Gets the list service rate to.
	 *
	 * @return the listServiceRateTo
	 */
	public List<ServiceRateTo> getListServiceRateTo() {
		return listServiceRateTo;
	}

	/**
	 * Sets the list service rate to.
	 *
	 * @param listServiceRateTo the listServiceRateTo to set
	 */
	public void setListServiceRateTo(List<ServiceRateTo> listServiceRateTo) {
		this.listServiceRateTo = listServiceRateTo;
	}

	/**
	 * Gets the rate exception to list original.
	 *
	 * @return the rateExceptionToListOriginal
	 */
	public List<RateExceptionTo> getRateExceptionToListOriginal() {
		return rateExceptionToListOriginal;
	}

	/**
	 * Sets the rate exception to list original.
	 *
	 * @param rateExceptionToListOriginal the rateExceptionToListOriginal to set
	 */
	public void setRateExceptionToListOriginal(
			List<RateExceptionTo> rateExceptionToListOriginal) {
		this.rateExceptionToListOriginal = rateExceptionToListOriginal;
	}

	/**
	 * Gets the lst rate exception remove.
	 *
	 * @return the lstRateExceptionRemove
	 */
	public List<RateExceptionTo> getLstRateExceptionRemove() {
		return lstRateExceptionRemove;
	}

	/**
	 * Sets the lst rate exception remove.
	 *
	 * @param lstRateExceptionRemove the lstRateExceptionRemove to set
	 */
	public void setLstRateExceptionRemove(
			List<RateExceptionTo> lstRateExceptionRemove) {
		this.lstRateExceptionRemove = lstRateExceptionRemove;
	}

	/**
	 * Gets the ind num rule.
	 *
	 * @return the ind num rule
	 */
	public Integer getIndNumRule() {
		return indNumRule;
	}

	/**
	 * Set Ind Num Rule.
	 *
	 * @param indNumRule the new ind num rule
	 */
	public void setIndNumRule(Integer indNumRule) {
		this.indNumRule = indNumRule;
	}

	/**
	 * Gets the rule participant to.
	 *
	 * @return the rule participant to
	 */
	public RuleParticipantTo getRuleParticipantTo() {
		return ruleParticipantTo;
	}

	/**
	 * Sets the rule participant to.
	 *
	 * @param ruleParticipantTo the new rule participant to
	 */
	public void setRuleParticipantTo(RuleParticipantTo ruleParticipantTo) {
		this.ruleParticipantTo = ruleParticipantTo;
	}

	/**
	 * Gets the show cmb participant.
	 *
	 * @return the show cmb participant
	 */
	public Boolean getShowCmbParticipant() {
		return showCmbParticipant;
	}

	/**
	 * Sets the show cmb participant.
	 *
	 * @param showCmbParticipant the new show cmb participant
	 */
	public void setShowCmbParticipant(Boolean showCmbParticipant) {
		this.showCmbParticipant = showCmbParticipant;
	}

	/**
	 * Gets the list client participant.
	 *
	 * @return the list client participant
	 */
	public List<Participant> getListClientParticipant() {
		return listClientParticipant;
	}

	/**
	 * Sets the list client participant.
	 *
	 * @param listClientParticipant the new list client participant
	 */
	public void setListClientParticipant(List<Participant> listClientParticipant) {
		this.listClientParticipant = listClientParticipant;
	}

	/**
	 * Gets the participant selected.
	 *
	 * @return the participant selected
	 */
	public Long getParticipantSelected() {
		return participantSelected;
	}

	/**
	 * Sets the participant selected.
	 *
	 * @param participantSelected the new participant selected
	 */
	public void setParticipantSelected(Long participantSelected) {
		this.participantSelected = participantSelected;
	}


	/**
	 * Gets the issuance to consult data model.
	 *
	 * @return the issuance to consult data model
	 */
	public GenericDataModel<IssuanceTo> getIssuanceToConsultDataModel() {
		return issuanceToConsultDataModel;
	}

	/**
	 * Sets the issuance to consult data model.
	 *
	 * @param issuanceToConsultDataModel the new issuance to consult data model
	 */
	public void setIssuanceToConsultDataModel(
			GenericDataModel<IssuanceTo> issuanceToConsultDataModel) {
		this.issuanceToConsultDataModel = issuanceToConsultDataModel;
	}

	/**
	 * Gets the show rdb num.
	 *
	 * @return the show rdb num
	 */
	public Boolean getShowRdbNum() {
		return showRdbNum;
	}

	/**
	 * Sets the show rdb num.
	 *
	 * @param showRdbNum the new show rdb num
	 */
	public void setShowRdbNum(Boolean showRdbNum) {
		this.showRdbNum = showRdbNum;
	}

	
	/**
	 * Gets the holder to data model.
	 *
	 * @return the holder to data model
	 */
	public GenericDataModel<HolderTo> getHolderToDataModel() {
		return holderToDataModel;
	}

	/**
	 * Sets the holder to data model.
	 *
	 * @param holderToDataModel the new holder to data model
	 */
	public void setHolderToDataModel(GenericDataModel<HolderTo> holderToDataModel) {
		this.holderToDataModel = holderToDataModel;
	}

	/**
	 * Gets the show pnl billing service.
	 *
	 * @return the show pnl billing service
	 */
	public Boolean getShowPnlBillingService() {
		return showPnlBillingService;
	}

	/**
	 * Sets the show pnl billing service.
	 *
	 * @param showPnlBillingService the new show pnl billing service
	 */
	public void setShowPnlBillingService(Boolean showPnlBillingService) {
		this.showPnlBillingService = showPnlBillingService;
	}
	

	/**
	 * Gets the rule security class to.
	 *
	 * @return the rule security class to
	 */
	public RuleSecurityClassTo getRuleSecurityClassTo() {
		return ruleSecurityClassTo;
	}

	/**
	 * Sets the rule security class to.
	 *
	 * @param ruleSecurityClassTo the new rule security class to
	 */
	public void setRuleSecurityClassTo(RuleSecurityClassTo ruleSecurityClassTo) {
		this.ruleSecurityClassTo = ruleSecurityClassTo;
	}

	/**
	 * Gets the rule security to.
	 *
	 * @return the rule security to
	 */
	public RuleSecurityTo getRuleSecurityTo() {
		return ruleSecurityTo;
	}

	/**
	 * Sets the rule security to.
	 *
	 * @param ruleSecurityTo the new rule security to
	 */
	public void setRuleSecurityTo(RuleSecurityTo ruleSecurityTo) {
		this.ruleSecurityTo = ruleSecurityTo;
	}
	

	/**
	 * Gets the issuance to data model.
	 *
	 * @return the issuance to data model
	 */
	public GenericDataModel<IssuanceTo> getIssuanceToDataModel() {
		return issuanceToDataModel;
	}

	/**
	 * Sets the issuance to data model.
	 *
	 * @param issuanceToDataModel the new issuance to data model
	 */
	public void setIssuanceToDataModel(
			GenericDataModel<IssuanceTo> issuanceToDataModel) {
		this.issuanceToDataModel = issuanceToDataModel;
	}
	
	/**
	 * Gets the holder account to data model.
	 *
	 * @return the holder account to data model
	 */
	public GenericDataModel<HolderAccountTo> getHolderAccountToDataModel() {
		return holderAccountToDataModel;
	}

	/**
	 * Sets the holder account to data model.
	 *
	 * @param holderAccountToDataModel the new holder account to data model
	 */
	public void setHolderAccountToDataModel(
			GenericDataModel<HolderAccountTo> holderAccountToDataModel) {
		this.holderAccountToDataModel = holderAccountToDataModel;
	}

	/**
	 * Gets the rule issuer to.
	 *
	 * @return the rule issuer to
	 */
	public RuleIssuerTo getRuleIssuerTo() {
		return ruleIssuerTo;
	}

	/**
	 * Sets the rule issuer to.
	 *
	 * @param ruleIssuerTo the new rule issuer to
	 */
	public void setRuleIssuerTo(RuleIssuerTo ruleIssuerTo) {
		this.ruleIssuerTo = ruleIssuerTo;
	}

	/**
	 * Gets the issuance to.
	 *
	 * @return the issuance to
	 */
	public IssuanceTo getIssuanceTo() {
		return issuanceTo;
	}

	/**
	 * Sets the issuance to.
	 *
	 * @param issuanceTo the new issuance to
	 */
	public void setIssuanceTo(IssuanceTo issuanceTo) {
		this.issuanceTo = issuanceTo;
	}

	/**
	 * *.
	 *
	 * @return the rule holder to
	 */
	public RuleHolderTo getRuleHolderTo() {
		return ruleHolderTo;
	}

	/**
	 * Sets the rule holder to.
	 *
	 * @param ruleHolderTo the new rule holder to
	 */
	public void setRuleHolderTo(RuleHolderTo ruleHolderTo) {
		this.ruleHolderTo = ruleHolderTo;
	}

	/**
	 * Gets the holder account to list.
	 *
	 * @return the holder account to list
	 */
	public List<HolderAccountTo> getHolderAccountToList() {
		return holderAccountToList;
	}

	/**
	 * Sets the holder account to list.
	 *
	 * @param holderAccountToList the new holder account to list
	 */
	public void setHolderAccountToList(List<HolderAccountTo> holderAccountToList) {
		this.holderAccountToList = holderAccountToList;
	}

	/**
	 * Gets the holder account to consult data model.
	 *
	 * @return the holder account to consult data model
	 */
	public GenericDataModel<HolderAccountTo> getHolderAccountToConsultDataModel() {
		return holderAccountToConsultDataModel;
	}

	/**
	 * Sets the holder account to consult data model.
	 *
	 * @param holderAccountToConsultDataModel the new holder account to consult data model
	 */
	public void setHolderAccountToConsultDataModel(
			GenericDataModel<HolderAccountTo> holderAccountToConsultDataModel) {
		this.holderAccountToConsultDataModel = holderAccountToConsultDataModel;
	}

	/**
	 * Gets the holder to consult data model.
	 *
	 * @return the holder to consult data model
	 */
	public GenericDataModel<HolderTo> getHolderToConsultDataModel() {
		return holderToConsultDataModel;
	}

	/**
	 * Sets the holder to consult data model.
	 *
	 * @param holderToConsultDataModel the new holder to consult data model
	 */
	public void setHolderToConsultDataModel(
			GenericDataModel<HolderTo> holderToConsultDataModel) {
		this.holderToConsultDataModel = holderToConsultDataModel;
	}

	/**
	 * Gets the billing service.
	 *
	 * @return the billingService
	 */
	public BillingService getBillingService() {
		return billingService;
	}

	/**
	 * Sets the billing service.
	 *
	 * @param billingService the billingService to set
	 */
	public void setBillingService(BillingService billingService) {
		this.billingService = billingService;
	}

	/**
	 * Post Construct.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException  {
		
		if (!FacesContext.getCurrentInstance().isPostback()){
			this.serviceRateToDataModel = new GenericDataModel<ServiceRateTo>();
			
			this.rateExceptionToDataModel=null;
			this.billingService= new BillingService();
			issuerView=			new Issuer();
			ruleHolderTo = 		new RuleHolderTo();
			securityView=		new Security();
			rateExceptionTo= 	new RateExceptionTo();
			listRateExceptionTo=new ArrayList<RateExceptionTo>();
			/** getting values for exception rules */
			
			/** GETS FROM DATABASE ALL SECURITY CLASSES CODES  */
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());			
			paramTableSecurityClasses = generalParametersService.getListParameterTableServiceBean(parameterTableTO);
			
			registerFrm=Boolean.TRUE;				
			loadCmb();
		}
		
	}
	
	/**
	 *  
	 * LOAD THE 1ST DATAMODEL - RATE.
	 */
	public void implListenerHelperBS(){
		if (Validations.validateIsNotNull(this.billingService)&&
				(Validations.validateIsNotNull(this.billingService.getIdBillingServicePk()) )){
				
				try {
				List<ServiceRate> listServiceRate=billingRateMgmtServiceFacade.getServicesRateByBillingService
												 (this.billingService.getIdBillingServicePk()); 
				if (Validations.validateListIsNotNullAndNotEmpty(listServiceRate)){
					listServiceRateTo = new ArrayList<ServiceRateTo>();
					ServiceRateTo serviceRateToTemp;  
					for( ServiceRate serviceRate : listServiceRate ){
							 serviceRateToTemp = new ServiceRateTo();
							 serviceRateToTemp.setIdServiceRatePk(serviceRate.getIdServiceRatePk());
							 serviceRateToTemp.setRateCode(serviceRate.getRateCode());
							 serviceRateToTemp.setRateName(serviceRate.getRateName()); 
							 serviceRateToTemp.setDescriptionType(serviceRate.getDescriptionType());
							 serviceRateToTemp.setInitialEffectiveDate(serviceRate.getInitialEffectiveDate());
							 serviceRateToTemp.setEndEffectiveDate(serviceRate.getEndEffectiveDate());
							 serviceRateToTemp.setDescriptionStatus(serviceRate.getDescriptionStatus()); 
							 listServiceRateTo.add(serviceRateToTemp);					 
						} 
					
					this.serviceRateToDataModel.setWrappedData(listServiceRateTo);
				}
					
				
				} catch (ServiceException e) {
					log.debug(e.getMessage());
					e.printStackTrace();
				}
			} 
	}
	
	/**
	 * Impl listener helper issuer.
	 */
	public void implListenerHelperIssuer(){
		if(Validations.validateIsNotNull(issuerView)){ 
				if (showRuleIssuer){
					this.disableSecurity=Boolean.FALSE;
					this.disableClassSecurity=Boolean.FALSE;
//					this.security= new Security();
				}
		}
	}
	
	/**
	 * *		
	 * 
	 * LOAD SERVICE RATE 1ST DATAMODEL		
	 * ***.
	 */
	public void addExceptionOnMemory(){
		
		if ( (Validations.validateIsNotNull(this.billingService)) &&
				(Validations.validateIsNotNull(this.billingService.getIdBillingServicePk())) ){
			this.showListException=Boolean.TRUE;
			showRdbNum=Boolean.TRUE;
		}else{
			this.showListException=Boolean.FALSE;
		}	
				
		if( this.showListException==Boolean.TRUE && Validations.validateListIsNotNullAndNotEmpty(this.rateExceptionToListOriginal)
				&& this.listRateExceptionTo.size() != this.rateExceptionToListOriginal.size() ){		
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.WARNING_DATA_SAVE,""));
					JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
						serviceRateTo = serviceRateToLastSelect;
			return;
		}
		/** VALIDATE IS EXIST DATA ON RATE*/
		if( !(Validations.validateListIsNotNullAndNotEmpty(this.listServiceRateTo))
				&& Validations.validateIsNotNullAndNotEmpty(billingService)
				&& Validations.validateIsNotNullAndNotEmpty(billingService.getServiceCode()) ){			
		implListenerHelperBS();
		}

		
	  flagMopdify=Boolean.FALSE;
	  flagFieldExeption=Boolean.FALSE;	
		
	  this.rateExceptionToDataModel =null;	  
	  this.showListException=Boolean.FALSE;
	  this.cbmSave=Boolean.TRUE;
	  this.cbmModify=Boolean.FALSE;
	  
	  if(Validations.validateListIsNotNullAndNotEmpty(this.listRateExceptionTo)){
		  this.listRateExceptionTo.clear();
	  }
	}
	
	/**
	 * Change rule.
	 */
	public void changeRule(){
		this.hiddenAllRules();	
		cleanConsult();
		/**if Selected the combo*/
		if(Validations.validateIsNotNull(rateExceptionTo)&&Validations.validateIsNotNullAndPositive(rateExceptionTo.getTypeRule())){
			showRdbNum=Boolean.TRUE;
			indNumRule=null;
		}

	} 
	
	/**
	 * Hidden all rules.
	 */
	public void hiddenAllRules(){
		showRuleClass=Boolean.FALSE;
		showRuleIssuer=Boolean.FALSE;		
		showRuleSecurity=Boolean.FALSE;
		showRuleParticipant=Boolean.FALSE;
		showRuleHolder=Boolean.FALSE;	
		flagRules=Boolean.FALSE;
	}
	

	/**
	 * Change state flag.
	 */
	public void changeStateFlag(){  
		serviceRateTo = null ;
		changeRule();
		if( Validations.validateIsNotNull(rateExceptionTo) && Validations.validateIsNotNullAndNotEmpty(rateExceptionTo.getIdServiceRate()) ){
			rateExceptionTo.setIdServiceRate(null);
		}
		
		if( Validations.validateListIsNotNullAndNotEmpty(this.listServiceRateTo) ){
			this.listServiceRateTo.clear();	
		}	
		
		if( Validations.validateListIsNotNullAndNotEmpty(this.listRateExceptionTo) ){
			this.listRateExceptionTo.clear();	
		}			
		
		if( !registerFrm && Validations.validateListIsNotNullAndNotEmpty(this.listRateExceptionTo) ){

			executeAction();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_DATA_SAVE,""));
			JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");  
			return;
		}
		
		
		if( !(Validations.validateListIsNotNullAndNotEmpty(this.listServiceRateTo))
				&& Validations.validateIsNotNullAndNotEmpty(billingService)
					&& Validations.validateIsNotNullAndNotEmpty(billingService.getServiceCode()) ){			
			implListenerHelperBS();
		}
	
		if (Validations.validateIsNotNull(this.billingService)&&
				!(Validations.validateIsNotNull(this.billingService.getServiceName()) )){
			if( Validations.validateListIsNotNullAndNotEmpty(this.listServiceRateTo) ){
				this.listServiceRateTo.clear();	
			}			
		}
		flagMopdify=Boolean.TRUE;
		flagFieldExeption=Boolean.TRUE;	
		
		this.rateExceptionToDataModel =null;
	    this.cbmSave=Boolean.FALSE;
	    this.cbmModify=Boolean.TRUE;
	    
	    this.rateExceptionTo.setDateRegister(null);
	    this.rateExceptionTo.setTypeException(null);
	 
	    showListException=Boolean.FALSE;
	   
	}

	/**
	 *   
	 * Clean the 1ST panel List .
	 */
	public void cleanPanelList(){
		/**Information Display Panel of Billing Service*/
		if(Validations.validateIsNotNull(this.billingService)
				&&Validations.validateIsNotNull(this.billingService.getServiceCode())
				&&!billingService.getServiceCode().equals(GeneralConstants.EMPTY_STRING)){
			showPnlBillingService=Boolean.TRUE;
		}
		else {
			showPnlBillingService=Boolean.FALSE;
		}
		if( Validations.validateListIsNotNullAndNotEmpty(this.listServiceRateTo) ){
			this.listServiceRateTo.clear();			
		}
		
		if( Validations.validateListIsNotNullAndNotEmpty(this.rateExceptionToListOriginal) ){
			this.rateExceptionToListOriginal.clear();
		}
		
		if( Validations.validateIsNotNull(this.serviceRateToDataModel) 
				&& !( Validations.validateIsNullOrNotPositive(this.serviceRateToDataModel.getSize()) ) ){
			this.serviceRateToDataModel = null; 
		} 
		
		if( Validations.validateListIsNotNullAndNotEmpty(this.listRateExceptionTo) ){
			this.rateExceptionToDataModel=null;
			this.listRateExceptionTo.clear();
			showListException=Boolean.FALSE;
		}
	}
	
	/**
	 * Remove Rate EXCEPTION.
	 *
	 * @param event the event
	 */
	public void removeException(ActionEvent event){
		RateExceptionTo rateExceptionTo= (RateExceptionTo) event .getComponent().getAttributes().get("removeName");
    	lstRateExceptionRemove.add(rateExceptionTo);
		this.getListRateExceptionTo().remove(rateExceptionTo); 
	}
	
	/**
	 *  ADD EXCEPCION ON MEMORY 2ND DATA MODEL.
	 */
	public void beforeAddExceptionOnTemporal(){
				
		Boolean correctData = Boolean.FALSE;
		Boolean selectServiceRate = Boolean.FALSE; 
		if(Validations.validateIsNotNullAndNotEmpty(listServiceRateTo)){
			for(ServiceRateTo serviceRateToTem : listServiceRateTo){
				if( serviceRateToTem.isSelected() && this.rateExceptionTo.getTypeRule() > 0){
					selectServiceRate=Boolean.TRUE;
					break;
				}
			}
		}
		
		/**VALIDATE ONE ON CLASS VALOR */
			if( rateExceptionTo.getTypeRule().equals(RulerExceptionType.SECURITY_CLASS_EXCEPTION_RATE.getCode()) 
				&& Validations.validateIsNotNullAndPositive(this.rateExceptionTo.getRuleSecurityClassTo().getSecurityClass()) 
				&& this.getIndNumRule()==PropertiesConstants.RDB_ONE
				&& Validations.validateIsNotNullAndPositive(this.rateExceptionTo.getTypeException()) ) {
				correctData=Boolean.TRUE;
				ruleSecurityClassTo = new RuleSecurityClassTo();
				ruleSecurityClassTo.setSecurityClass(this.rateExceptionTo.getRuleSecurityClassTo().getSecurityClass());
				ruleSecurityClassTo.setIndicatorOneOrAll(indNumRule);
			
		/**VALIDATE ALL ON CLASS VALOR */
			}else if( rateExceptionTo.getTypeRule().equals(RulerExceptionType.SECURITY_CLASS_EXCEPTION_RATE.getCode()) 
					&& !(Validations.validateIsNotNullAndPositive(this.rateExceptionTo.getRuleSecurityClassTo().getSecurityClass())) 
					&& this.getIndNumRule()==PropertiesConstants.RDB_ALL
					&& Validations.validateIsNotNullAndPositive(this.rateExceptionTo.getTypeException()) ){
					correctData=Boolean.TRUE;
					ruleSecurityClassTo = new RuleSecurityClassTo();
					ruleSecurityClassTo.setIndicatorOneOrAll(indNumRule);
			}
			
		/**VALIDATE ONE ON VALOR */	
			else if( rateExceptionTo.getTypeRule().equals(RulerExceptionType.SECURITY_EXCEPTION_RATE.getCode())
					&& Validations.validateIsNotNull(this.securityView) 
					&& Validations.validateIsNotNullAndNotEmpty(this.securityView.getIdSecurityCodePk()) 
					&& this.getIndNumRule()==PropertiesConstants.RDB_ONE 
					&& Validations.validateIsNotNullAndPositive(this.rateExceptionTo.getTypeException()) ){
				correctData=Boolean.TRUE;
				ruleSecurityTo = new RuleSecurityTo();
				SecurityTo securityTo= new SecurityTo();
				securityTo.setIdSecurityCodePk(securityView.getIdSecurityCodePk());
				securityTo.setDescription(securityView.getDescription());
				securityTo.setMnemonic(securityView.getMnemonic());
				
				ruleSecurityTo.setSecuritySelected(securityTo);
				ruleSecurityTo.setSecurityDescriptionConsult(this.securityView.getDescription());
				ruleSecurityTo.setSecurityMnemonicConsult(this.securityView.getMnemonic());
				ruleSecurityTo.setIndicatorOneOrAll(indNumRule);
				
				rateExceptionTo.getRuleSecurityTo().setSecuritySelected(securityTo);
				
		/**VALIDATE ALL ON VALOR */	
			}else if( rateExceptionTo.getTypeRule().equals(RulerExceptionType.SECURITY_EXCEPTION_RATE.getCode()) 
					&& !(Validations.validateIsNotNull(this.securityView) ) 
					&& !(Validations.validateIsNotNullAndNotEmpty(this.securityView.getIdSecurityCodePk()) ) 
					&& this.getIndNumRule()==PropertiesConstants.RDB_ALL 
					&& Validations.validateIsNotNullAndPositive(this.rateExceptionTo.getTypeException()) ){
				correctData=Boolean.TRUE;
				ruleSecurityTo = new RuleSecurityTo();
				ruleSecurityTo.setIndicatorOneOrAll(indNumRule);
				
			}
			/**VALIDATE ONE ON PARTICIPANT*/
			else if(rateExceptionTo.getTypeRule().equals(RulerExceptionType.PARTICIPANT_EXCEPTION_RATE.getCode()) 
					&& Validations.validateIsNotNullAndPositive(this.participantSelected) 
					&& this.getIndNumRule()==PropertiesConstants.RDB_ONE
					&& Validations.validateIsNotNullAndPositive(this.rateExceptionTo.getTypeException()) ){
				correctData=Boolean.TRUE;
				ruleParticipantTo = new RuleParticipantTo();
				ruleParticipantTo.setIndicatorOneOrAll(indNumRule);	
			
			}
			/**VALIDATE ALL ON PARTICIPANT*/
			else if( rateExceptionTo.getTypeRule().equals(RulerExceptionType.PARTICIPANT_EXCEPTION_RATE.getCode()) 
					&& this.getIndNumRule()==PropertiesConstants.RDB_ALL 
					&& Validations.validateIsNotNullAndPositive(this.rateExceptionTo.getTypeException()) ){
				correctData=Boolean.TRUE;
				ruleParticipantTo = new RuleParticipantTo();
				ruleParticipantTo.setIndicatorOneOrAll(indNumRule);	
				
	   /**VALIDATE ONE ON ISSUER */			
			}else if( rateExceptionTo.getTypeRule().equals(RulerExceptionType.ISSUER_EXCEPTION_RATE.getCode()) 
					&& Validations.validateIsNotNullAndNotEmpty(this.issuerView.getIdIssuerPk()) 
					&& this.getIndNumRule()==PropertiesConstants.RDB_ONE 
					&& Validations.validateIsNotNullAndPositive(this.rateExceptionTo.getTypeException()) ){
				correctData=Boolean.TRUE;
				ruleIssuerTo = new RuleIssuerTo();
				IssuerTo issuerTo=new IssuerTo();
				issuerTo.setIdIssuerPk(this.issuerView.getIdIssuerPk());
				issuerTo.setMnemonic(this.issuerView.getMnemonic());
				issuerTo.setBusinessName(this.issuerView.getBusinessName());
				ruleIssuerTo.setIssuerSelected(issuerTo);
				ruleIssuerTo.setIndicatorOneOrAll(indNumRule);
				
	   /**VALIDATE ALL ON ISSUER */			
			}else if( rateExceptionTo.getTypeRule().equals(RulerExceptionType.ISSUER_EXCEPTION_RATE.getCode()) 
					&& !(Validations.validateIsNotNullAndNotEmpty(this.issuerView.getIdIssuerPk()) ) 
					&& this.getIndNumRule()==PropertiesConstants.RDB_ALL 
					&& Validations.validateIsNotNullAndPositive(this.rateExceptionTo.getTypeException()) ){
				correctData=Boolean.TRUE;
				ruleIssuerTo = new RuleIssuerTo();
				ruleIssuerTo.setIndicatorOneOrAll(indNumRule);
				
	   /**VALIDATE ONE ON HOLDER */				
			}else if( rateExceptionTo.getTypeRule().equals(RulerExceptionType.HOLDER_EXCEPTION_RATE.getCode()) 
					&& Validations.validateIsNotNullAndNotEmpty(this.rateExceptionTo.getRuleHolderTo().getHolderSelected().getIdHolderPk()) 
					&& this.getIndNumRule()==PropertiesConstants.RDB_ONE 
					&& Validations.validateIsNotNullAndPositive(this.rateExceptionTo.getTypeException()) ){
				correctData=Boolean.TRUE;
			    ruleHolderTo = new RuleHolderTo();
			    ruleHolderTo.setHolderSelected(this.rateExceptionTo.getRuleHolderTo().getHolderSelected());
			    ruleHolderTo.setIdHolderPk(this.rateExceptionTo.getRuleHolderTo().getHolderSelected().getIdHolderPk());
			    ruleHolderTo.setIndicatorOneOrAll(indNumRule);
			    
	   /**VALIDATE ALL ON HOLDER */				
			}else if( rateExceptionTo.getTypeRule().equals(RulerExceptionType.HOLDER_EXCEPTION_RATE.getCode()) 
					&& !(Validations.validateIsNotNullAndNotEmpty(this.rateExceptionTo.getRuleHolderTo().getHolderSelected().getIdHolderPk()) ) 
					&& this.getIndNumRule()==PropertiesConstants.RDB_ALL 
					&& Validations.validateIsNotNullAndPositive(this.rateExceptionTo.getTypeException()) ){
				correctData=Boolean.TRUE;
				ruleHolderTo = new RuleHolderTo();
				ruleHolderTo.setIndicatorOneOrAll(indNumRule);
			}	 
			
				if(correctData && selectServiceRate)
				{
					showListException=Boolean.TRUE;
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_ADD_EXCEPTION));
					JSFUtilities.executeJavascriptFunction("cnfwTypeAddException.show();");
					return ;
				}else{
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
							, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_COMPLETE_DATA_EXCEPTION,""));
					JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
				}
	}


	/**
	 * Add New Exception.
	 */
	public void addNewException(){
		
 		List<ServiceRateExceptionsTo> serviceRateToListTemp = new ArrayList<ServiceRateExceptionsTo>();
		for(ServiceRateTo serviceRateTo : listServiceRateTo){
			ServiceRateExceptionsTo serviceRateExceptionsTo=new ServiceRateExceptionsTo();
			if(serviceRateTo.isSelected()){
				serviceRateExceptionsTo.setIdServiceRatePk(serviceRateTo.getIdServiceRatePk());
				serviceRateExceptionsTo.setRateName(serviceRateTo.getRateName());
				serviceRateExceptionsTo.setDescriptionStatus(serviceRateTo.getDescriptionStatus());
				serviceRateExceptionsTo.setDescriptionType(serviceRateTo.getDescriptionType());
				serviceRateExceptionsTo.setEndEffectiveDate(serviceRateTo.getEndEffectiveDate());
				serviceRateExceptionsTo.setIndScaleMovement(serviceRateTo.getIndScaleMovement());
				serviceRateExceptionsTo.setInitialEffectiveDate(serviceRateTo.getInitialEffectiveDate());
				serviceRateExceptionsTo.setRateCode(serviceRateTo.getRateCode());
				serviceRateToListTemp.add(serviceRateExceptionsTo);
			}
		}
		
		
		
		/** add issuance list **/
		 if(Validations.validateListIsNotNullAndNotEmpty(issuanceToList))
			 rateExceptionTo.setIssuanceToList(issuanceToList);
	
		/** add holder list **/
		 if(Validations.validateListIsNotNullAndNotEmpty(holderAccountToList))  
			 rateExceptionTo.setHolderAccountToList(holderAccountToList);
			 
		 
		/** PARTICIPANT RULE */
		if(holderToList!=null){
			ruleParticipantTo.setHolderToList(holderToList);
		}
				
		if(ruleParticipantTo!=null){
			rateExceptionTo.setRuleParticipantTo(ruleParticipantTo);
		}
			
			
			/** CLASS VALOR RULE */
		if(ruleSecurityClassTo!=null){
			rateExceptionTo.setRuleSecurityClassTo(ruleSecurityClassTo);
		}
			
		
		/** 	SECURITY RULE 	*/
		if(ruleSecurityTo!=null){
			rateExceptionTo.setRuleSecurityTo(ruleSecurityTo);
		}
			
		
		/**		ISSUER		*/
		if(issuanceToList!=null){
			ruleIssuerTo.setIssuanceTolist(issuanceToList);	
		}
			
		
		if(ruleIssuerTo!=null){
			rateExceptionTo.setRuleIssuerTo(ruleIssuerTo);
		}
			
		
		/**     HOLDER      */
		if(holderAccountToList!=null){
			ruleHolderTo.setHolderAccountToList(holderAccountToList);
		}
			
			
		if(ruleHolderTo!=null){
			rateExceptionTo.setRuleHolderTo(ruleHolderTo);
		}
							
		
		RateExceptionTo rateExceptionToTemp=SerializationUtils.clone(this.rateExceptionTo);
		rateExceptionToTemp.setServiceRateToList(serviceRateToListTemp);
				
		for(ParameterTable parameterTableTo : listException ){
			if( parameterTableTo.getParameterTablePk().equals(rateExceptionTo.getTypeException()) ){
				rateExceptionToTemp.setDescriptionTypeException(parameterTableTo.getParameterName());
			}
		}
		
		for( ParameterTable parameterTable : listRulesType ){
			if( parameterTable.getParameterTablePk().equals(rateExceptionTo.getTypeRule()) ){
				rateExceptionToTemp.setDescriptionTypeRule(parameterTable.getParameterName());
			}
		}
		
		if( Validations.validateIsNotNull(rateExceptionTo.getRuleSecurityTo().getSecuritySelected())){
			
			RuleSecurityTo ruleSecurityToTemp  =  new RuleSecurityTo();
			ruleSecurityToTemp.setSecuritySelected(this.rateExceptionTo.getRuleSecurityTo().getSecuritySelected());
			ruleSecurityToTemp.setIndicatorOneOrAll(indNumRule);
			rateExceptionToTemp.setRuleSecurityTo(ruleSecurityToTemp);
		}
		
		if(Validations.validateIsNotNullAndPositive(this.participantSelected)){
			Participant participantTemp = new Participant();
				participantTemp.setIdParticipantPk(this.participantSelected);
				rateExceptionToTemp.setParticipant(participantTemp.getIdParticipantPk());
		}
				
		
		
		listRateExceptionTo.add(rateExceptionToTemp);
		this.rateExceptionToDataModel= new GenericDataModel<RateExceptionTo>(listRateExceptionTo);

		this.indNumRule=null;
		/** clean rule's */
		this.rateExceptionTo.setSecurityClass(-1);
		this.rateExceptionTo.setTypeRule(-1);
		this.rateExceptionTo.setSecurityCode(null);
		this.rateExceptionTo.setComments(null);
		this.showListException=Boolean.TRUE;
		this.showRuleClass=Boolean.FALSE;
		this.flagRules=Boolean.FALSE;
		this.rateExceptionTo.setTypeException(null);
		for (int i = 0; i < listServiceRateTo.size(); i++) {
			listServiceRateTo.get(i).setSelected(false); 
		}
		serviceRateToDataModel = new GenericDataModel<ServiceRateTo>(listServiceRateTo);

		securityView=new Security();
		holderHelp=new Holder();
		issuerView=new Issuer();
			
		/**	 CLEAN RULE CLASS SECURITY	**/		
		if(Validations.validateIsNotNullAndNotEmpty(ruleSecurityClassTo)){
			ruleSecurityClassTo.setSecurityClass(-1);
		}
		
		
		/**	 CLEAN RULE SECURITY	**/		
		if(Validations.validateIsNotNullAndNotEmpty(ruleSecurityTo)){
			ruleSecurityTo.setSecuritySelected(null);
		}
		

		this.securityView=new Security();
		
		/**  CLEAN PARTICIPANT		**/
		this.participantSelected = null;
		if(Validations.validateIsNotNull(holderToList))
			holderToList.clear();

		
		
		
		
		/**  CLEAN RULE ISSUER  	**/
		RuleIssuerTo ruleIssuerToTemp = new RuleIssuerTo();
	    this.rateExceptionTo.setRuleIssuerTo(ruleIssuerToTemp);
		this.issuanceToDataModel=null;
		if(Validations.validateListIsNotNullAndNotEmpty(issuanceToList)){
			issuanceToList.clear();
		}
		ruleIssuerTo=new RuleIssuerTo();
		
		
		
		
		
		
		/**  CLEAN RULE HOLDER      **/
		if(Validations.validateIsNotNullAndNotEmpty(ruleHolderTo)){
			holderAccountToDataModel = null;
			ruleHolderTo.setIdHolderPk(null);
			ruleHolderTo.setAccountNumberPk(null);		
			
			HolderTo holderTo = new HolderTo();
			ruleHolderTo.setHolderSelected(holderTo); 
		}
		if(Validations.validateListIsNotNullAndNotEmpty(holderAccountToList)){
			holderAccountToList.clear();
		}
		    
		
		    
	}
	
	
	/**
	 * Load cmb.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCmb() throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		if(Validations.validateListIsNullOrEmpty(listRulesType)){
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO.setMasterTableFk(PropertiesConstants.RULER_TYPE_PK);
			listRulesType = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}
		
		if(Validations.validateListIsNullOrEmpty(listException)){
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO.setMasterTableFk(PropertiesConstants.EXCEPTION_PK);
			listException = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}  
		if(Validations.validateListIsNullOrEmpty(listExceptionState)){
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO.setMasterTableFk(PropertiesConstants.EXCEPTION_STATUS_PK);
			listExceptionState = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} 
	}
	
	/**
	 * Before save exception.
	 */
	@LoggerAuditWeb
	public void beforeSaveException(){		
		if( Validations.validateIsNotNull(rateExceptionToDataModel) 
				&& Validations.validateIsNotNullAndPositive(rateExceptionToDataModel.getSize()) ) {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
						PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_SAVE_EXCEPTION));
				JSFUtilities.executeJavascriptFunction("cnfwTypeSaveException.show();");
		}else {
			executeAction();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_EMPTY_EXCEPTION,""));
				JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();"); 
			}
	}
	
	/**
	 * Save new exception.
	 */
	@LoggerAuditWeb
	public void saveNewException(){
		
		/**   TO SAVE   */
		if( Validations.validateIsNotNullAndPositive(rateExceptionToDataModel.getSize()) && flagMopdify==Boolean.FALSE ){
			try {
				rateExceptionServiceFacade.preparateRateException(listRateExceptionTo);
				
			} catch (ServiceException e) {

				e.printStackTrace();
			}
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_EXCEPTION_SAVE,""));
			JSFUtilities.executeJavascriptFunction("cnfWConfirmSuccessDialog.show();");
			registerFrm=Boolean.TRUE;	
			cleanException();
			
		} else if( rateExceptionToDataModel.getSize()>1 && flagMopdify==Boolean.TRUE ){
			
			
			
			
		} else {			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_EMPTY_EXCEPTION,""));
			JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
		}
		
	}
	
	/**
	 * Clean exception.
	 */
	public void cleanException(){
		
		this.rateExceptionTo.setTypeException(null);
		this.rateExceptionTo.setTypeRule(-1);		
		this.rateExceptionTo.setDescriptionTypeException(null);		
		this.rateExceptionTo.setDescriptionTypeRule(null);  
		this.rateExceptionTo.setSecurityClass(-1);	
		//this.rateExceptionTo.setDescriptionSecurityClass(null);	
		this.rateExceptionTo.setComments(null);
		this.rateExceptionTo.setSecurity(null); 
		this.rateExceptionTo.setSecurityCode(null);
		this.rateExceptionTo.setIdServiceRate(null);
		this.rateExceptionTo.setDateRegister(null);
		this.rateExceptionTo.setIdRateException(null);
		this.rateExceptionTo.setStateDescription(null);
//		securitiesHelperBean.setSecurityMnemonic(null);
//		securitiesHelperBean.setSecurityDescription(null);
		
		this.rateExceptionSelection = null;
		this.setParticipantSelected(null);
		
		listRateExceptionTo = new ArrayList<RateExceptionTo>();
		listRateExceptionTo.clear();
		this.rateExceptionToDataModel=null;
		
		this.indNumRule=null;
		
		hiddenAllRules();
		if( Validations.validateListIsNotNullAndNotEmpty(listServiceRateTo) ){
			for (int i = 0; i < listServiceRateTo.size(); i++) {
				listServiceRateTo.get(i).setSelected(false); 
			}	
		} 
		if(Validations.validateIsNotNull(this.listServiceRateTo)){
			serviceRateToDataModel = new GenericDataModel<ServiceRateTo>(listServiceRateTo);
		}
		flagMopdify = Boolean.FALSE;
		flagRules = Boolean.FALSE;
		showListException = Boolean.FALSE;
		showPnlBillingService=Boolean.FALSE;
		
//		securityHelpBean.setIsinCode(null);
		securityHelpBean.setName(null);
		securityHelpBean.setSecurity(new Security());
		
		if(this.cbmModify==Boolean.TRUE){
			this.flagMopdify=Boolean.TRUE;
		}else{
			this.flagMopdify=Boolean.FALSE;
		}
		this.serviceRateTo = null;		
		if ( (Validations.validateIsNotNull(this.billingService)) &&
				(Validations.validateIsNotNull(this.billingService.getIdBillingServicePk())) ){
			this.billingService = new BillingService();
		}
		if( Validations.validateListIsNotNullAndNotEmpty(this.listServiceRateTo) ){
			this.listServiceRateTo.clear();			
		}
		if( Validations.validateListIsNotNullAndNotEmpty(this.listRateExceptionTo) ){
			this.listRateExceptionTo.clear();
		}
		if( Validations.validateListIsNotNullAndNotEmpty(this.rateExceptionToListOriginal) ){
			this.rateExceptionToListOriginal.clear();
		}
		
		/**	 CLEAN RULE CLASS SECURITY	**/		
		if(Validations.validateIsNotNullAndNotEmpty(ruleSecurityClassTo))
		ruleSecurityClassTo.setSecurityClass(-1);
		
		/**	 CLEAN RULE SECURITY	**/		
		if(Validations.validateIsNotNullAndNotEmpty(ruleSecurityTo))
		ruleSecurityTo.setSecuritySelected(null);
		
		/**	 CLEAN RULE ISSUER	**/		
		this.issuanceToDataModel= new GenericDataModel<IssuanceTo>();
		if(Validations.validateListIsNotNullAndNotEmpty(this.issuanceToList)){
			this.issuanceToList.clear();
		}


		/**  CLEAN RULE HOLDER      **/
		if(Validations.validateIsNotNullAndNotEmpty(ruleHolderTo)){
			holderAccountToDataModel = null;
			ruleHolderTo.setIdHolderPk(null);
			ruleHolderTo.setAccountNumberPk(null);
		}
		if(Validations.validateListIsNotNullAndNotEmpty(holderAccountToList)){
			holderAccountToList.clear();
		}
		
		/** CLEAN HELP HOLDER*/
		holderHelp=new Holder();
		
	}

	/**
	 * Clean to register.
	 */
	public void cleanToRegister(){
		if(registerFrm){
			registerFrm=Boolean.FALSE;		
		}
		cleanException();
	}
	
	/**
	 * Go to search exception.
	 *
	 * @return the string
	 */
	public String goToSearchException() {
		if(!registerFrm){
			registerFrm=Boolean.TRUE;
			cleanException();
			return "searchException";
		}else{
			return "";
		}
	}
	
	/**
	 * Before clean exception all.
	 */
	public void beforeCleanExceptionAll(){
		if ( ((Validations.validateIsNotNull(this.billingService)) &&
				(Validations.validateIsNotNull(this.billingService.getIdBillingServicePk()))) || Validations.validateListIsNotNullAndNotEmpty(this.listRateExceptionTo) ){
	 	
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_DATA_CLEAN));
			JSFUtilities.executeJavascriptFunction("cnfwTypeCleanPageException.show();");
		}
	}
	
	/**
	 * Before back exception.
	 *
	 * @return the string
	 */
	public String beforeBackException(){
		if ( cbmSave && Validations.validateIsNotNull(this.billingService) 
				&& Validations.validateIsNotNull( this.billingService.getIdBillingServicePk() ) ){
	 	
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BACK_EXCEPTION_SEARCH));
			JSFUtilities.executeJavascriptFunction("cnfwTypeCleanException.show();");
			return "";
		}else if( cbmModify && Validations.validateIsNotNullAndPositive(this.listRateExceptionTo.size()) 
						    && this.listRateExceptionTo.size() != this.rateExceptionToListOriginal.size() ){
			
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BACK_EXCEPTION_SEARCH_DELETE));
				JSFUtilities.executeJavascriptFunction("cnfwTypeCleanException.show();");
				return "";
		}else if ( (Validations.validateIsNotNull(this.billingService)) &&
				(Validations.validateIsNotNull(this.billingService.getIdBillingServicePk())) ){
			cleanException();
			registerFrm=Boolean.TRUE;
			return "searchException";
		}else {
			registerFrm=Boolean.TRUE;
			return "searchException";
		}
	}
	
	/**
	 * Clean exception all.
	 */
	public void cleanExceptionAll(){
		cleanException();
		this.rateExceptionTo.setSecurityCode(null);
	}
	
	/**
	 * Refresh show list exception.
	 */
	public void refreshShowListException(){
		if(this.showListException == Boolean.FALSE){
			this.showListException = Boolean.TRUE;
		}else{
			this.showListException = Boolean.FALSE;
		}
	}
	
	/**
	 *  LOAD EXCEPTION BY RATE .
	 *
	 * @param serviceRateToTemp the service rate to temp
	 */
	@LoggerAuditWeb
	public void loadExceptionByServiceRate(ServiceRateTo serviceRateToTemp){
		try {
			Date todayD = new Date();	
			this.rateExceptionSelection = null;			
			 List<RateExclusion> listRateExceptionExclusionTemp = new ArrayList<RateExclusion>();	
			 List<RateInclusion> listRateExceptionInclusionTemp = new ArrayList<RateInclusion>();
			 
			if(this.showListException==Boolean.TRUE
					&& Validations.validateListIsNotNullAndNotEmpty(rateExceptionToListOriginal) 
//					&& this.rateExceptionToListOriginal.size()>=0
					&& Validations.validateListIsNotNullAndNotEmpty(listRateExceptionTo)
					&& this.listRateExceptionTo.size() != this.rateExceptionToListOriginal.size() ){
				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_DATA_SAVE,""));
				JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
				serviceRateTo = serviceRateToLastSelect;
				return;
			}
			
			serviceRateToLastSelect = serviceRateToTemp;
			ServiceRateTo serviceRateTo = serviceRateToTemp; 
			
			listRateExceptionTo.clear();
			log.debug(" busqueda de excepciones");
			
			if( registerFrm 
					&& Validations.validateIsNotNull(this.serviceRateTo) 
						&& Validations.validateIsNotNull(this.serviceRateTo.getIdServiceRatePk())  ){
				rateExceptionTo.setIdServiceRate(serviceRateTo.getIdServiceRatePk());
			}
			if( Validations.validateIsNotNull(rateExceptionTo.getDateRegister()) ){
				rateExceptionTo.setTodayDate(todayD);				
			}
				
			
			/** EXCLUSION */
			 if( registerFrm ){
				 if( (PropertiesConstants.EXCLUSION_PK.equals(rateExceptionTo.getTypeException())) 
					 && Validations.validateIsNotNull(this.serviceRateTo)) {
				 		listRateExceptionExclusionTemp = rateExceptionServiceFacade.getRateExceptionExclusionList(rateExceptionTo);				
					 }	 
				 else if( PropertiesConstants.EXCLUSION_PK.equals(rateExceptionTo.getTypeException())
						 && !(Validations.validateIsNotNull(this.serviceRateTo)) ){
					 listRateExceptionExclusionTemp = rateExceptionServiceFacade.getRateExceptionExclusionList(rateExceptionTo);
				 }
				  
							
				 /** INCLUSION */
				 else if( ( PropertiesConstants.INCLUSION_PK.equals(rateExceptionTo.getTypeException())) 
						 && Validations.validateIsNotNull(this.serviceRateTo))   {
					 listRateExceptionInclusionTemp = rateExceptionServiceFacade.getRateExceptionInclusionList(rateExceptionTo);
				 }
				 else if( PropertiesConstants.INCLUSION_PK.equals(rateExceptionTo.getTypeException())
						 && !(Validations.validateIsNotNull(this.serviceRateTo)) ){
					 listRateExceptionInclusionTemp = rateExceptionServiceFacade.getRateExceptionInclusionList(rateExceptionTo);
				 } 
				 else if( (Validations.validateIsNotNull(this.serviceRateTo)) ){
					 listRateExceptionInclusionTemp = rateExceptionServiceFacade.getRateExceptionInclusionList(rateExceptionTo);
					 listRateExceptionExclusionTemp = rateExceptionServiceFacade.getRateExceptionExclusionList(rateExceptionTo);
				 }else {
					 listRateExceptionInclusionTemp = rateExceptionServiceFacade.getRateExceptionInclusionList(rateExceptionTo);
					 listRateExceptionExclusionTemp = rateExceptionServiceFacade.getRateExceptionExclusionList(rateExceptionTo);
				 }
			 } 
			 else if(!registerFrm){
				 rateExceptionTo.setIdServiceRate(serviceRateTo.getIdServiceRatePk());
				 listRateExceptionExclusionTemp = rateExceptionServiceFacade.getRateExceptionExclusionList(rateExceptionTo);
				 listRateExceptionInclusionTemp = rateExceptionServiceFacade.getRateExceptionInclusionList(rateExceptionTo);
			 }

				RateExceptionTo rateExceptionToTmp = new RateExceptionTo(); 
			/**	SETTING EXCLUSION */
			for(RateExclusion rateExclusion : listRateExceptionExclusionTemp){
				rateExceptionToTmp = new RateExceptionTo();
				
				for( ParameterTable parameterTable : listRulesType ){
					if( parameterTable.getParameterTablePk().equals(rateExclusion.getExceptionRule()) ){
						rateExceptionToTmp.setDescriptionTypeRule(parameterTable.getParameterName());		
						break;
					}
				}
				for(ParameterTable parameterTableTo : listException ){
					if( PropertiesConstants.EXCLUSION_PK.equals(parameterTableTo.getParameterTablePk()) ){					 
					  rateExceptionToTmp.setDescriptionTypeException(parameterTableTo.getParameterName());
					  rateExceptionToTmp.setTypeException(parameterTableTo.getParameterTablePk());
					  break;
					}
				}
				/** SETTING THE STATE  */
				for( ParameterTable parameterTableToTemp : listExceptionState ){
					if (parameterTableToTemp.getParameterTablePk().equals(rateExclusion.getExclusionState()) ){
						rateExceptionToTmp.setStateDescription(parameterTableToTemp.getParameterName());
						break;
					}
				}
				 
				rateExceptionToTmp.setIdRateException(rateExclusion.getIdRateExclusionPk());
				rateExceptionToTmp.setIdServiceRate(rateExclusion.getServiceRate().getIdServiceRatePk());
				rateExceptionToTmp.setDateRegister(rateExclusion.getExclusionDate());
				rateExceptionToTmp.setComments(rateExclusion.getComments());
				rateExceptionToTmp.setTypeRule(rateExclusion.getExceptionRule());				
				rateExceptionToTmp.setExceptionState(rateExclusion.getExclusionState());
				rateExceptionToTmp.setIndSelectAll(rateExclusion.getIndSelectAll());
				
				 
				
				
							
				if(Validations.validateIsNotNullAndPositive(rateExclusion.getSecurityClass())){
					RuleSecurityClassTo ruleSecurityClassToTemp = new RuleSecurityClassTo();
						ruleSecurityClassToTemp.setSecurityClass(rateExclusion.getSecurityClass());	
						rateExceptionToTmp.setRuleSecurityClassTo(ruleSecurityClassToTemp);
				}
								
				if(Validations.validateIsNotNull(rateExclusion.getSecurities())){

					RuleSecurityTo ruleSecurityToTemp = new RuleSecurityTo();
					SecurityTo securityTo=new SecurityTo();
					securityHelp = new Security();

					/**LAZY**/
					rateExclusion.getSecurities().setIssuanceCountry(null);
					rateExclusion.getSecurities().setIssuance(null);
					rateExclusion.getSecurities().setIssuer(null);
					rateExclusion.getSecurities().setSecurity(null);
					
					securityTo.setIdSecurityCodePk(rateExclusion.getSecurities().getIdSecurityCodePk());
					securityTo.setDescription(rateExclusion.getSecurities().getDescription());
					securityTo.setMnemonic(rateExclusion.getSecurities().getMnemonic());
					
					securityHelp.setIdSecurityCodePk(rateExclusion.getSecurities().getIdSecurityCodePk());
					securityHelp.setDescription(rateExclusion.getSecurities().getDescription());
					securityHelp.setIdIsinCode(rateExclusion.getSecurities().getIdIsinCode());
					securityHelp.setCfiCode(rateExclusion.getSecurities().getCfiCode());
					
					ruleSecurityToTemp.setSecuritySelected(securityTo);
					rateExceptionToTmp.setRuleSecurityTo(ruleSecurityToTemp);
				}
				
				if(Validations.validateIsNotNull(rateExclusion.getParticipant())){
					rateExceptionToTmp.setParticipant(rateExclusion.getParticipant().getIdParticipantPk());
				}
				 
				
				if(Validations.validateIsNotNull(rateExclusion.getIssuer())){
					RuleIssuerTo ruleIssuerToTemp = new RuleIssuerTo();
					IssuanceTo         issuanceTo = new IssuanceTo(); 						
					List<IssuanceTo> issuanceToList = new ArrayList<IssuanceTo>(); 
					
					/**LAZY**/
					if(Validations.validateIsNotNullAndNotEmpty(rateExclusion.getIssuance())){
						rateExclusion.getIssuance().setGeographicLocation(null);
					}
					rateExclusion.getIssuer().setGeographicLocation(null);
					rateExclusion.getIssuer().setHolder(null);

					ruleIssuerToTemp.setIssuerSelected(rateExclusion.getIssuer()!=null?new IssuerTo(rateExclusion.getIssuer().getIdIssuerPk()):new IssuerTo());
					ruleIssuerToTemp.setIssuanceConsult(rateExclusion.getIssuance()!=null?new IssuanceTo(rateExclusion.getIssuance().getIdIssuanceCodePk()):new IssuanceTo());
					ruleIssuerToTemp.setBusinessName(rateExclusion.getIssuer().getBusinessName());

					if(Validations.validateIsNotNullAndNotEmpty(rateExclusion.getIssuance())){
						issuanceTo.setIdIssuanceCodePk(rateExclusion.getIssuance().getIdIssuanceCodePk());
						issuanceTo.setDescriptionIssuance(rateExclusion.getIssuance().getDescription());
					}
					if(Validations.validateIsNotNullAndNotEmpty(rateExclusion.getSecurities()))
						issuanceTo.setIdSecurityCodePk(rateExclusion.getSecurities().getIdSecurityCodePk());
					
					if( (Validations.validateIsNotNull(issuanceTo) && Validations.validateIsNotNullAndNotEmpty(rateExclusion.getIssuance()) 
							|| (Validations.validateIsNotNull(issuanceTo) && Validations.validateIsNotNullAndNotEmpty(rateExclusion.getSecurities())) )	
							){
						issuanceToList.add(issuanceTo);	
					}
						
					
					ruleIssuerToTemp.setIssuanceTolist(issuanceToList); 
					rateExceptionToTmp.setRuleIssuerTo(ruleIssuerToTemp);
				}
				
				if(Validations.validateIsNotNull(rateExclusion.getHolder())){

					RuleHolderTo ruleHolderToTemp = new RuleHolderTo(); 
					HolderTo		   holderTemp     = new HolderTo();
					
					/**LAZY**/
					rateExclusion.getHolder().setParticipant(null);
					if( Validations.validateIsNotNullAndNotEmpty(rateExclusion.getHolder()) ){
						holderTemp.setDescriptionRepresentative(rateExclusion.getHolder().getDescriptionHolder());
						holderTemp.setIdHolderPk(rateExclusion.getHolder().getIdHolderPk());
						holderTemp.setFullNameHolder(rateExclusion.getHolder().getFullName());
						ruleHolderToTemp.setHolderSelected(holderTemp);	
						
						ruleHolderToTemp.setIdHolderPk(rateExclusion.getHolder().getIdHolderPk());
						ruleHolderToTemp.setDescriptionConsult(rateExclusion.getHolder().getDescriptionHolder());	
					}
	
					if( Validations.validateIsNotNullAndNotEmpty(rateExclusion.getHolderAccount()) ){
						ruleHolderToTemp.setAccountNumber(rateExclusion.getHolderAccount().getAccountNumber());
					}		
					rateExceptionToTmp.setRuleHolderTo(ruleHolderToTemp);
				}
				
				 listRateExceptionTo.add(rateExceptionToTmp);		
			}
			/**	SETTING INCLUSION */
			for(RateInclusion rateInclusion : listRateExceptionInclusionTemp){
				rateExceptionToTmp = new RateExceptionTo();
				
				for( ParameterTable parameterTable : listRulesType ){
					if( parameterTable.getParameterTablePk().equals(rateInclusion.getExceptionRule()) ){
						rateExceptionToTmp.setDescriptionTypeRule(parameterTable.getParameterName());
						break;
					}
				}
				for(ParameterTable parameterTableTo : listException ){
					if( PropertiesConstants.INCLUSION_PK.equals(parameterTableTo.getParameterTablePk()) ){
					  rateExceptionToTmp.setDescriptionTypeException(parameterTableTo.getParameterName());
					  rateExceptionToTmp.setTypeException(parameterTableTo.getParameterTablePk());
					  break;
					}
				}
				/** SETTING THE STATE  */
				for( ParameterTable parameterTableToTemp : listExceptionState ){
					if( parameterTableToTemp.getParameterTablePk().equals(rateInclusion.getInclusionState()) ){
						rateExceptionToTmp.setStateDescription(parameterTableToTemp.getParameterName());
						break;
					}
				} 
				
				 rateExceptionToTmp.setIdRateException(rateInclusion.getIdRateInclusionPk());
				 rateExceptionToTmp.setIdServiceRate(rateInclusion.getServiceRate().getIdServiceRatePk());
				 rateExceptionToTmp.setDateRegister(rateInclusion.getInclusionDate());
				 rateExceptionToTmp.setComments(rateInclusion.getComments());
				 rateExceptionToTmp.setTypeRule(rateInclusion.getExceptionRule());
				 rateExceptionToTmp.setExceptionState(rateInclusion.getInclusionState());
				 rateExceptionToTmp.setIndSelectAll(rateInclusion.getIndSelectAll());
				 
				 
				 
				 
				    if(Validations.validateIsNotNullAndNotEmpty(rateInclusion.getSecurityClass())){
						RuleSecurityClassTo ruleSecurityClassToTemp = new RuleSecurityClassTo();
							ruleSecurityClassToTemp.setSecurityClass(rateInclusion.getSecurityClass());	
							rateExceptionToTmp.setRuleSecurityClassTo(ruleSecurityClassToTemp);
					}
									
					if(Validations.validateIsNotNull(rateInclusion.getSecurities())){
						
						RuleSecurityTo ruleSecurityToTemp = new RuleSecurityTo();
						SecurityTo securityTo=new SecurityTo();
						securityTo.setIdSecurityCodePk(rateInclusion.getSecurities().getIdSecurityCodePk());
						securityTo.setDescription(rateInclusion.getSecurities().getDescription());
						securityTo.setMnemonic(rateInclusion.getSecurities().getMnemonic());

						/**LAZY**/
						rateInclusion.getSecurities().setIssuanceCountry(null);
						rateInclusion.getSecurities().setIssuance(null);
						rateInclusion.getSecurities().setIssuer(null);
						rateInclusion.getSecurities().setSecurity(null);
						
						securityHelp.setIdSecurityCodePk(rateInclusion.getSecurities().getIdSecurityCodePk());
						securityHelp.setDescription(rateInclusion.getSecurities().getDescription());
						securityHelp.setIdIsinCode(rateInclusion.getSecurities().getIdIsinCode());
						securityHelp.setCfiCode(rateInclusion.getSecurities().getCfiCode());
						
						ruleSecurityToTemp.setSecuritySelected(securityTo);
						rateExceptionToTmp.setRuleSecurityTo(ruleSecurityToTemp);
					}
					
					if(Validations.validateIsNotNull(rateInclusion.getParticipant()))
					 rateExceptionToTmp.setParticipant(rateInclusion.getParticipant().getIdParticipantPk());
					
					
					
					if(Validations.validateIsNotNull(rateInclusion.getIssuer())){
						
						RuleIssuerTo ruleIssuerToTemp = new RuleIssuerTo();
						IssuanceTo         issuanceTo = new IssuanceTo(); 						
						List<IssuanceTo> issuanceToList = new ArrayList<IssuanceTo>(); 
						
						/**LAZY**/
						if(Validations.validateIsNotNullAndNotEmpty(rateInclusion.getIssuance())){
							rateInclusion.getIssuance().setGeographicLocation(null);
						}
						rateInclusion.getIssuer().setGeographicLocation(null);
						rateInclusion.getIssuer().setHolder(null);
						
						ruleIssuerToTemp.setIssuerSelected(rateInclusion.getIssuer()!=null?new IssuerTo(rateInclusion.getIssuer().getIdIssuerPk()):new IssuerTo());
						ruleIssuerToTemp.setIssuanceConsult(rateInclusion.getIssuance()!=null?new IssuanceTo(rateInclusion.getIssuance().getIdIssuanceCodePk()):new IssuanceTo());
						ruleIssuerToTemp.setBusinessName(rateInclusion.getIssuer().getBusinessName());
						
						if(Validations.validateIsNotNullAndNotEmpty(rateInclusion.getIssuance())){
							issuanceTo.setIdIssuanceCodePk(rateInclusion.getIssuance().getIdIssuanceCodePk());
							issuanceTo.setDescriptionIssuance(rateInclusion.getIssuance().getDescription());	
						}
						if(Validations.validateIsNotNullAndNotEmpty(rateInclusion.getSecurities()))
							issuanceTo.setIdSecurityCodePk(rateInclusion.getSecurities().getIdSecurityCodePk());

						if( (Validations.validateIsNotNull(issuanceTo) && Validations.validateIsNotNullAndNotEmpty(rateInclusion.getIssuance()) 
								|| (Validations.validateIsNotNull(issuanceTo) && Validations.validateIsNotNullAndNotEmpty(rateInclusion.getSecurities())) )	
								)
							issuanceToList.add(issuanceTo);						
						
						ruleIssuerToTemp.setIssuanceTolist(issuanceToList); 
						rateExceptionToTmp.setRuleIssuerTo(ruleIssuerToTemp);
					}
					
					
					
					if(Validations.validateIsNotNull(rateInclusion.getHolder())){

						RuleHolderTo ruleHolderToTemp = new RuleHolderTo(); 
						HolderTo		   holderTemp     = new HolderTo();
						
						/**LAZY**/
						rateInclusion.getHolder().setParticipant(null);
						
						if( Validations.validateIsNotNullAndNotEmpty(rateInclusion.getHolder()) ){
							
								holderTemp.setDescriptionRepresentative(rateInclusion.getHolder().getDescriptionHolder());;
								holderTemp.setIdHolderPk(rateInclusion.getHolder().getIdHolderPk());
								holderTemp.setFullNameHolder(rateInclusion.getHolder().getFullName());;
								ruleHolderToTemp.setHolderSelected(holderTemp);	
								
								ruleHolderToTemp.setIdHolderPk(rateInclusion.getHolder().getIdHolderPk());
								ruleHolderToTemp.setDescriptionConsult(rateInclusion.getHolder().getDescriptionHolder());	
						}

						if( Validations.validateIsNotNullAndNotEmpty(rateInclusion.getHolderAccount()) ){
							
							ruleHolderToTemp.setAccountNumber(rateInclusion.getHolderAccount().getAccountNumber());
							
						}						
						rateExceptionToTmp.setRuleHolderTo(ruleHolderToTemp);
					}
					
				 listRateExceptionTo.add(rateExceptionToTmp);
				
			}
			
			if( Validations.validateListIsNotNullAndNotEmpty(listRateExceptionTo) ) {
				this.showListException=Boolean.TRUE;
				this.rateExceptionToListOriginal =BillingUtils.cloneRateExceptionToList(this.getListRateExceptionTo());
				rateExceptionToDataModel = new GenericDataModel<RateExceptionTo>(listRateExceptionTo);
				
				/***	Apply privileges to Exception Security	***/
				
				setTrueValidButtons();
				
				
			}else{
				this.showListException=Boolean.FALSE;
			}
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	/**
	 * 	SEARCH FROM BUTTON	-------.
	 */
	@LoggerAuditWeb
	public void loadExceptionBySearch(){	
		
		if(!registerFrm && Validations.validateIsNotNull(serviceRateTo) && Validations.validateIsNotNull(serviceRateTo.getIdServiceRatePk())){
			loadExceptionByServiceRate(serviceRateTo);
		}	
		
		/** Search Windows **/
		if(registerFrm ){
			
			if(Validations.validateIsNotNullAndNotEmpty(serviceRateTo) ){
				loadExceptionByServiceRate(serviceRateTo);
			}
			else{
				showMessageOnDialog(PropertiesUtilities
						.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.MODIFY_RATE_NO_SELECTED));
				JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
			}				
		}
	}
	
	/**
	 * 	SEARCH FROM DATA MODEL.
	 *
	 * @param selectEvent the select event
	 */
	@LoggerAuditWeb
	public void loadExceptionByServiceRate(SelectEvent selectEvent){
		loadExceptionByServiceRate((ServiceRateTo) selectEvent.getObject());
	}
	

	/**
	 * Before delete.
	 */
	public void beforeDelete(){
				
		if (Validations.validateIsNotNull(this.rateExceptionSelection))  	
		{
			
			if (rateExceptionSelection.getStateDescription().equals(RateExceptionStatus.DELETED.getValue()))
			{				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_RATE_EXCEPTION_DELETED, 
								new Object[]{rateExceptionSelection.getIdRateException().toString()}));
				JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
				return;
			} 
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
					, PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_DELETE_EXCEPTION,
							new Object[]{rateExceptionSelection.getIdRateException().toString()}));
			JSFUtilities.executeJavascriptFunction("cnfwTypeDeleteException.show();");	
				
		}else {			
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_NO_EXCEPTION_DELETE,""));
				JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
			}
		
	}
	
	/**
	 * Delete exception.
	 */
	@LoggerAuditWeb
	public void deleteException(){
		
		log.info("deleteException - change state");
			if( PropertiesConstants.EXCLUSION_PK.equals(rateExceptionSelection.getTypeException()) ){
				try {
					RateExclusion rateExclusion=rateExceptionServiceFacade.getRateExceptionExclusionByPk(rateExceptionSelection.getIdRateException());				
					rateExclusion.setExclusionState(RateExceptionStatus.DELETED.getCode());
					rateExceptionServiceFacade.deletedExclusion(rateExclusion);
				} catch (ServiceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				log.debug(" se actualizó el estado de la exclusion ");
				executeAction();	
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
						, PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_EXCEPTION_DELETE,
								new Object[]{rateExceptionSelection.getIdRateException().toString()}));
				JSFUtilities.executeJavascriptFunction("cnfWConfirmDialogDelete.show();");
				loadExceptionBySearch();
			}else if( PropertiesConstants.INCLUSION_PK.equals(rateExceptionSelection.getTypeException()) ){
				try {
					RateInclusion rateInclusion=rateExceptionServiceFacade.getRateExceptionInclusionByPk(rateExceptionSelection.getIdRateException());
					rateInclusion.setInclusionState(RateExceptionStatus.DELETED.getCode());
					rateExceptionServiceFacade.deletedInclusion(rateInclusion);
				} catch (ServiceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				log.debug(" se actualizó el estado de la inclusion ");
				executeAction();	
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
						, PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_EXCEPTION_DELETE,
								new Object[]{rateExceptionSelection.getIdRateException().toString()}));
				JSFUtilities.executeJavascriptFunction("cnfWConfirmDialogDelete.show();");
				loadExceptionBySearch();
			}
			
		executeAction();		
		showMessageOnDialog(PropertiesUtilities
				.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_EXCEPTION_DELETE,"") );
		JSFUtilities.executeJavascriptFunction("cnfWConfirmDialogDelete.show();"); 
	}
	
	/**
	 * Load id service rate.
	 *
	 * @param e the e
	 */
	public void loadIdServiceRate(SelectEvent e){
		this.serviceRateTo=(ServiceRateTo)e.getObject();				
	}

	/**
	 * Load detail exception.
	 *
	 * @param rateExceptionConsultTo the rate exception consult to
	 */
	public void loadDetailException(RateExceptionTo rateExceptionConsultTo){
		
		List<ServiceRateExceptionsTo> listServiceRateExceptionsTo=rateExceptionConsultTo.getServiceRateToList();
		List<ServiceRateTo> listServiceRateTo=new ArrayList<>();
		for (ServiceRateExceptionsTo serviceRateExceptionsTo : listServiceRateExceptionsTo) {
			ServiceRateTo serviceRateTo=new ServiceRateTo();
			serviceRateTo.setRateName(serviceRateExceptionsTo.getRateName());
			serviceRateTo.setIdServiceRatePk(serviceRateExceptionsTo.getIdServiceRatePk());
			serviceRateTo.setDescriptionStatus(serviceRateExceptionsTo.getDescriptionStatus());
			serviceRateTo.setDescriptionType(serviceRateExceptionsTo.getDescriptionType());
			serviceRateTo.setEndEffectiveDate(serviceRateExceptionsTo.getEndEffectiveDate());
			serviceRateTo.setIndScaleMovement(serviceRateExceptionsTo.getIndScaleMovement());
			serviceRateTo.setInitialEffectiveDate(serviceRateExceptionsTo.getInitialEffectiveDate());
			serviceRateTo.setRateCode(serviceRateExceptionsTo.getRateCode());
			listServiceRateTo.add(serviceRateTo);
		
		}
		
		
		
		cleanConsult();
		rateExceptionTo.setCommentsConsult(rateExceptionConsultTo.getComments());
		
		if( Validations.validateIsNotNull(issuanceToConsultDataModel) 
					&& 	issuanceToConsultDataModel.getSize() >= 1 ) 
			issuanceToConsultDataModel = null;
		
		if( Validations.validateIsNotNullAndNotEmpty(ruleHolderTo.getHolderSelected()) ){
			ruleHolderTo.setHolderSelected(null);
		}
		
		if( Validations.validateIsNotNull(holderAccountToConsultDataModel)
					&& holderAccountToConsultDataModel.getSize() >= 1 )
			holderAccountToConsultDataModel = null; 
		
						
		this.rateExceptionTo.disableFlagRuleTypeException();		
		
		serviceRateToConsultDataModel = new GenericDataModel<ServiceRateTo>(listServiceRateTo);
				
		this.rateExceptionTo.setFlagRateConsult(Boolean.TRUE);
		if( rateExceptionConsultTo.getTypeRule().equals(RulerExceptionType.SECURITY_CLASS_EXCEPTION_RATE.getCode()) ){
			JSFUtilities.executeJavascriptFunction("cnfwFlagConsultMemory.show()");				
			rateExceptionTo.setSecurityClassConsult(rateExceptionConsultTo.getRuleSecurityClassTo().getSecurityClass());
			this.rateExceptionTo.setFlagRuleTypeClassValor(Boolean.TRUE);
			
		}else if( rateExceptionConsultTo.getTypeRule().equals(RulerExceptionType.SECURITY_EXCEPTION_RATE.getCode()) ){
			
			RuleSecurityTo  ruleSecurityToTemp = new RuleSecurityTo();
			securityHelp 	=new Security();
			ruleSecurityToTemp.setSecuritySelected(rateExceptionConsultTo.getRuleSecurityTo().getSecuritySelected());
			rateExceptionTo.setRuleSecurityTo(ruleSecurityToTemp);
			securityHelp.setIdSecurityCodePk(rateExceptionConsultTo.getRuleSecurityTo().getSecuritySelected().getIdSecurityCodePk());
			securityHelp.setDescription(rateExceptionConsultTo.getRuleSecurityTo().getSecuritySelected().getDescription());
//			securitiesHelperBean.setSecurityMnemonic(rateExceptionConsultTo.getRuleSecurityTo().getSecurityMnemonicConsult());
//			securitiesHelperBean.setSecurityDescription(rateExceptionConsultTo.getRuleSecurityTo().getSecurityDescriptionConsult());			
			JSFUtilities.executeJavascriptFunction("cnfwFlagConsultMemory.show()");

			this.rateExceptionTo.setFlagRuleTypeValor(Boolean.TRUE);
		}else if( rateExceptionConsultTo.getTypeRule().equals(RulerExceptionType.PARTICIPANT_EXCEPTION_RATE.getCode()) ){
				this.setParticipantSelected(rateExceptionConsultTo.getParticipant());
			if(Validations.validateListIsNotNullAndNotEmpty(rateExceptionConsultTo.getRuleParticipantTo().getHolderToList())){
				this.setParticipantSelected(rateExceptionConsultTo.getRuleParticipantTo().getHolderToList().get(0).getIdParticipantPk());			
				holderToConsultDataModel = new GenericDataModel<HolderTo>(rateExceptionConsultTo.getRuleParticipantTo().getHolderToList());
			}
			JSFUtilities.executeJavascriptFunction("cnfwFlagConsultMemory.show()");
			
			this.rateExceptionTo.setFlagRuleTypeParticipant(Boolean.TRUE);			
		}else if( rateExceptionConsultTo.getTypeRule().equals(RulerExceptionType.ISSUER_EXCEPTION_RATE.getCode()) ){			
			ruleIssuerTo.setIssuerSelected(rateExceptionConsultTo.getRuleIssuerTo().getIssuerSelected());
			ruleIssuerTo.setBusinessName(rateExceptionConsultTo.getRuleIssuerTo().getBusinessName());
			rateExceptionTo.setRuleIssuerTo(ruleIssuerTo);			
			
			if( Validations.validateListIsNotNullAndNotEmpty(rateExceptionConsultTo.getIssuanceToList())) {
				issuanceToConsultDataModel = new GenericDataModel<IssuanceTo>(rateExceptionConsultTo.getIssuanceToList());
			}
						
			JSFUtilities.executeJavascriptFunction("cnfwFlagConsultMemory.show()");
			
			this.rateExceptionTo.setFlagRuleTypeIssuer(Boolean.TRUE);			
		}else if( rateExceptionConsultTo.getTypeRule().equals(RulerExceptionType.HOLDER_EXCEPTION_RATE.getCode()) ){
			holderHelp = new Holder(rateExceptionConsultTo.getRuleHolderTo().getHolderSelected().getIdHolderPk());
			holderHelp.setFullName(rateExceptionConsultTo.getRuleHolderTo().getHolderSelected().getFullNameHolder());
			holderHelp.setDescriptionHolder(rateExceptionConsultTo.getRuleHolderTo().getHolderSelected().getFullNameHolder());
			ruleHolderTo.setHolderSelected(rateExceptionConsultTo.getRuleHolderTo().getHolderSelected());
			
			if( Validations.validateListIsNotNullAndNotEmpty(rateExceptionConsultTo.getHolderAccountToList()) ){
				holderAccountToConsultDataModel = new GenericDataModel<HolderAccountTo>(rateExceptionConsultTo.getHolderAccountToList());
			}
			
 
			JSFUtilities.executeJavascriptFunction("cnfwFlagConsultMemory.show()");
			this.rateExceptionTo.setFlagRuleTypeHolder(Boolean.TRUE);
		}			
		
		
	}
	
	/**
	 * Clean consult.
	 */
	private void cleanConsult() {
		rateExceptionTo.setCommentsConsult(null);
		rateExceptionTo.setSecurityClassConsult(null);
		
		
		
		
		/**Rule Class Valor*/
			rateExceptionTo.setSecurityClass(null);
			if(Validations.validateIsNotNullAndNotEmpty(ruleSecurityTo)){
//				ruleSecurityTo.setIdIsinCodePk(null);
				ruleSecurityTo.setSecuritySelected(null);
				rateExceptionTo.setRuleSecurityTo(ruleSecurityTo);
			}
			if(Validations.validateIsNotNullAndNotEmpty(ruleSecurityClassTo) 
					&& Validations.validateIsNotNullAndNotEmpty(ruleSecurityClassTo.getSecurityClass())){
				ruleSecurityClassTo.setSecurityClass(null);
				rateExceptionTo.setRuleSecurityClassTo(ruleSecurityClassTo);
			}
			
			
		/**Rule Valor*/			
			this.securityView=new Security();

			
		/**Rule participant*/						
			participantSelected=null;
			holderToDataModel=null;
			holderToConsultDataModel=null;
			rateExceptionTo.setSecurityDescriptionConsult(null);
			rateExceptionTo.setSecurityMnemonicConsult(null);			
			
		/**Rule Holder*/
			
			ruleHolderTo.setHolderSelected(new HolderTo());		
			rateExceptionTo.setRuleHolderTo(ruleHolderTo);
			this.holderAccountToDataModel=null;
			
		/**Rule Issuer*/
			
			this.issuerView=new Issuer();
			this.issuanceToDataModel = null;
			if(Validations.validateListIsNotNullAndNotEmpty(issuanceToList)){
				issuanceToList.clear();
			}
			

	}

	/**
	 * Load detail service rate.
	 *
	 * @param rateExceptionConsultTo the rate exception consult to
	 */
	@LoggerAuditWeb
	public void loadDetailServiceRate(RateExceptionTo rateExceptionConsultTo) {

		try {
			rateExceptionTo.setCommentsConsult(null);
			rateExceptionTo.setCommentsConsult(rateExceptionConsultTo.getComments());
			rateExceptionTo.setFlagRateConsult(Boolean.FALSE);
			setParticipantSelected(null);
			holderToConsultDataModel = new GenericDataModel<HolderTo>();
			
			this.rateExceptionTo.disableFlagRuleTypeException();	
			
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setMasterTableFk(PropertiesConstants.RATE_TYPE); 
			listRateType = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO); 
						 
	  /***  CONSULT SERVICE RATE`S ***/
			List<ServiceRateTo> listServiceRateTemp = new ArrayList<ServiceRateTo>();
			listServiceRateTemp = rateExceptionServiceFacade.getServicesRateByExceptionIdTwo(rateExceptionConsultTo.getIdServiceRate());
			
			if( Validations.validateListIsNotNullAndNotEmpty(listServiceRateTemp)){
				for( ParameterTable parameterTableTemp : listRateType ){
					if( parameterTableTemp.getParameterTablePk().equals(listServiceRateTemp.get(0).getRateType()) ){
						listServiceRateTemp.get(0).setDescriptionType(parameterTableTemp.getParameterName());
						break;
					}
				}
			}
			
	 /***  CONSULT BILLING SERVICE TO GET THE BILLING CODE ***/		
			BillingServiceTo billingServiceTo = new BillingServiceTo();			
			List<BillingService> listBillingService = billingServiceMgmtServiceFacade.getBillingServiceListAutomaticAndEventual(billingServiceTo);
			
			if( Validations.validateListIsNotNullAndNotEmpty(listBillingService)){
				for( BillingService billingServiceTemp : listBillingService){
					if( billingServiceTemp.getIdBillingServicePk().toString().equals(listServiceRateTemp.get(0).getIdBillingService())){
						listServiceRateTemp.get(0).setServiceCode(billingServiceTemp.getServiceCode());
						break;
					}
				}
			}
	
       /*** SHOW SERVICE RATE LIST ***/
			JSFUtilities.executeJavascriptFunction("cnfwFlagConsultMemory.show()");			
			serviceRateToConsultDataModel = new GenericDataModel<ServiceRateTo>(listServiceRateTemp);
			this.rateExceptionTo.setFlagRateConsult(Boolean.TRUE);
			
	/**		CONSULT RULES		**/
			
								/**		CLASS VALOR		**/			
			if( rateExceptionConsultTo.getTypeRule().equals(RulerExceptionType.SECURITY_CLASS_EXCEPTION_RATE.getCode()) ){
				JSFUtilities.executeJavascriptFunction("cnfwFlagConsultMemory.show()");	
				rateExceptionTo.setRuleSecurityClassTo(rateExceptionConsultTo.getRuleSecurityClassTo());
				this.rateExceptionTo.setFlagRuleTypeClassValor(Boolean.TRUE);
			}
								/**		   VALOR    	**/
			else if( rateExceptionConsultTo.getTypeRule().equals(RulerExceptionType.SECURITY_EXCEPTION_RATE.getCode()) ){
				
				rateExceptionTo.setRuleSecurityTo(rateExceptionConsultTo.getRuleSecurityTo());
				
				if( rateExceptionConsultTo.getIndSelectAll().equals(PropertiesConstants.RDB_ONE) 
						&& Validations.validateIsNotNullAndNotEmpty(rateExceptionConsultTo.getRuleSecurityTo()) ){					
					
//						securitiesHelperBean.setSecurityMnemonic(rateExceptionConsultTo.getRuleSecurityTo().getSecuritySelected().getMnemonic());
//						securitiesHelperBean.setSecurityDescription(rateExceptionConsultTo.getRuleSecurityTo().getSecuritySelected().getDescription());
					
					JSFUtilities.executeJavascriptFunction("cnfwFlagConsultMemory.show()");
					this.rateExceptionTo.setFlagRuleTypeValor(Boolean.TRUE);
					
				}else if( rateExceptionConsultTo.getIndSelectAll().equals(PropertiesConstants.RDB_ALL) ){
//						securitiesHelperBean.setSecurityMnemonic(null);
//						securitiesHelperBean.setSecurityDescription(null);
						JSFUtilities.executeJavascriptFunction("cnfwFlagConsultMemory.show()");
						this.rateExceptionTo.setFlagRuleTypeValor(Boolean.TRUE);
					}
			}
								/**		PARTICIPANT		**/	
			else if( rateExceptionConsultTo.getTypeRule().equals(RulerExceptionType.PARTICIPANT_EXCEPTION_RATE.getCode()) ){
				
					HolderTo holderToTemp = new HolderTo();
					List<HolderTo> holderToList = new ArrayList<HolderTo>();
					
					if( Validations.validateIsNotNullAndNotEmpty(rateExceptionConsultTo.getParticipant()) ){
						this.setParticipantSelected(rateExceptionConsultTo.getParticipant());
						getParticipantListToCombo();
						
						if(Validations.validateIsNotNullAndNotEmpty(rateExceptionConsultTo.getRuleHolderTo().getAccountNumber())){
							holderToTemp.setIdHolderPk(rateExceptionConsultTo.getRuleHolderTo().getHolderSelected().getIdHolderPk());
							holderToTemp.setFullNameHolder(rateExceptionConsultTo.getRuleHolderTo().getHolderSelected().getFullNameHolder());
							holderToTemp.setAccountNumberConsult(rateExceptionConsultTo.getRuleHolderTo().getAccountNumber());
							holderToTemp.setIdSecurityCodePk(rateExceptionConsultTo.getRuleSecurityTo().getSecuritySelected().getIdSecurityCodePk());

							holderToList.add(holderToTemp);
						}
						
						holderToConsultDataModel = new GenericDataModel<HolderTo>(holderToList);
					}
					
				
				
				JSFUtilities.executeJavascriptFunction("cnfwFlagConsultMemory.show()");
				this.rateExceptionTo.setFlagRuleTypeParticipant(Boolean.TRUE);
			}
								/**		   ISSUER	    **/	
			else if( rateExceptionConsultTo.getTypeRule().equals(RulerExceptionType.ISSUER_EXCEPTION_RATE.getCode()) ){
				rateExceptionTo.setRuleIssuerTo(rateExceptionConsultTo.getRuleIssuerTo());
				RuleIssuerTo ruleIssuerTo = new RuleIssuerTo();
				if(Validations.validateIsNotNullAndNotEmpty(rateExceptionConsultTo.getRuleIssuerTo())){					
					ruleIssuerTo.setBusinessName(rateExceptionConsultTo.getRuleIssuerTo().getBusinessName());
					issuanceToConsultDataModel = new GenericDataModel<IssuanceTo>(rateExceptionConsultTo.getRuleIssuerTo().getIssuanceTolist());
				}		
								
				JSFUtilities.executeJavascriptFunction("cnfwFlagConsultMemory.show()");
				this.rateExceptionTo.setFlagRuleTypeIssuer(Boolean.TRUE);
			}
								 /**		HOLDER		**/	
			else if( rateExceptionConsultTo.getTypeRule().equals(RulerExceptionType.HOLDER_EXCEPTION_RATE.getCode()) ){
				
				RuleHolderTo ruleHolderTo = new RuleHolderTo();
				if(Validations.validateIsNotNullAndNotEmpty(rateExceptionConsultTo.getRuleHolderTo())){
					ruleHolderTo.setHolderSelected(rateExceptionConsultTo.getRuleHolderTo().getHolderSelected());
					ruleHolderTo.setDescriptionConsult(rateExceptionConsultTo.getRuleHolderTo().getDescriptionConsult());
					ruleHolderTo.setAccountNumber(rateExceptionConsultTo.getRuleHolderTo().getAccountNumber());
				}
				
				rateExceptionTo.setRuleHolderTo(ruleHolderTo);
			 
				JSFUtilities.executeJavascriptFunction("cnfwFlagConsultMemory.show()");
				this.rateExceptionTo.setFlagRuleTypeHolder(Boolean.TRUE);				
			}	
			
			
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		
	}
	

	/**
	 * Update rules.
	 */
	@LoggerAuditWeb
	public void updateRules(){
		if(Validations.validateIsNotNullAndPositive(indNumRule)){
			this.hiddenAllRules();			
			if( this.rateExceptionTo.getTypeRule().equals(RulerExceptionType.SECURITY_CLASS_EXCEPTION_RATE.getCode()) ){
				showRuleClass=Boolean.TRUE; flagRules=Boolean.TRUE;
			}else if( this.rateExceptionTo.getTypeRule().equals(RulerExceptionType.SECURITY_EXCEPTION_RATE.getCode()) ){
				showRuleSecurity=Boolean.TRUE; flagRules=Boolean.TRUE; 
			}else if( this.rateExceptionTo.getTypeRule().equals(RulerExceptionType.PARTICIPANT_EXCEPTION_RATE.getCode()) ){
				showRuleParticipant=Boolean.TRUE; flagRules=Boolean.TRUE;
				getParticipantListToCombo();
				showCmbParticipant=Boolean.TRUE;
			}else if( this.rateExceptionTo.getTypeRule().equals(RulerExceptionType.ISSUER_EXCEPTION_RATE.getCode()) ){
				showRuleIssuer=Boolean.TRUE; flagRules=Boolean.TRUE;
			}else if( this.rateExceptionTo.getTypeRule().equals(RulerExceptionType.HOLDER_EXCEPTION_RATE.getCode()) ){
				showRuleHolder=Boolean.TRUE; flagRules=Boolean.TRUE;
			}	
		}
		else{
			cleanConsult();
			if( this.rateExceptionTo.getTypeRule().equals(RulerExceptionType.SECURITY_CLASS_EXCEPTION_RATE.getCode()) ){
				showRuleClass=Boolean.FALSE; flagRules=Boolean.TRUE;
			}else if( this.rateExceptionTo.getTypeRule().equals(RulerExceptionType.SECURITY_EXCEPTION_RATE.getCode()) ){
				showRuleSecurity=Boolean.FALSE; flagRules=Boolean.TRUE; 
			}else if( this.rateExceptionTo.getTypeRule().equals(RulerExceptionType.PARTICIPANT_EXCEPTION_RATE.getCode()) ){
				showRuleParticipant=Boolean.FALSE; flagRules=Boolean.TRUE;
				showCmbParticipant=Boolean.TRUE;
			}else if( this.rateExceptionTo.getTypeRule().equals(RulerExceptionType.ISSUER_EXCEPTION_RATE.getCode()) ){
				showRuleIssuer=Boolean.FALSE; flagRules=Boolean.TRUE;
			}else if( this.rateExceptionTo.getTypeRule().equals(RulerExceptionType.HOLDER_EXCEPTION_RATE.getCode()) ){
				showRuleHolder=Boolean.FALSE; flagRules=Boolean.TRUE;
			}	
		}		
	}
	
	/**
	 * Update data table participant.
	 */
	@LoggerAuditWeb
	public void updateDataTableParticipant(){
		List<HolderTo> holderToList;
		holderToDataModel=null;
		
		if(Validations.validateIsNotNullAndPositive(participantSelected)){
			holderToList=rateExceptionServiceFacade.findDataParticipantTo(participantSelected);
			this.holderToDataModel=new GenericDataModel<HolderTo>(holderToList);
		}
	}
	
	
	/**
	 * Load issuance and security.
	 */
	@LoggerAuditWeb
	public void loadIssuanceAndSecurity(){

		try {
			List<IssuanceTo> issuanceToList; 
			issuanceToDataModel=null;
			if(Validations.validateIsNotNull(this.issuerView)||
					Validations.validateIsNotNullAndNotEmpty(this.issuerView.getIdIssuerPk())){
				issuanceToList=rateExceptionServiceFacade.findIssuanceTo(this.issuerView.getIdIssuerPk());
				this.issuanceToDataModel=new GenericDataModel<IssuanceTo>(issuanceToList);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * getParticipantListToCombo.
	 *
	 * @return the participant list to combo
	 */
	public void getParticipantListToCombo() {
		
		if(Validations.validateListIsNullOrEmpty(listClientParticipant)){
			Participant participantTemp=new Participant();
			
			try {
				listClientParticipant=helperComponentFacade.getLisParticipantServiceFacade(participantTemp);
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Show security.
	 */
	public void showSecurity(){
//		security = new Security();
//		security.setIdIsinCode(rateExceptionTo.getIsinCode());
//		security.setMnemonic(securitiesHelperBean.getSecurityMnemonic());
//		security.setDescription(securitiesHelperBean.getSecurityDescription());
		
//		rateExceptionTo.setSecurityMnemonicConsult(securitiesHelperBean.getSecurityMnemonic());
//		rateExceptionTo.setSecurityDescriptionConsult(securitiesHelperBean.getSecurityDescription());		
	}
	
	/**
	 * Load holder account.
	 */
	@LoggerAuditWeb
	public void loadHolderAccount(){
		
		if( Validations.validateIsNotNullAndNotEmpty(this.holderHelp.getIdHolderPk()) ){
			try {
				List<HolderAccountTo> holderAccountToList;
				holderToDataModel   = null;
				holderAccountToList = rateExceptionServiceFacade.findHolderAccount(this.holderHelp.getIdHolderPk());
				this.holderAccountToDataModel = new GenericDataModel<HolderAccountTo>(holderAccountToList);
				this.rateExceptionTo.getRuleHolderTo().getHolderSelected().setIdHolderPk(this.holderHelp.getIdHolderPk());
				this.rateExceptionTo.getRuleHolderTo().getHolderSelected().setFullNameHolder(this.holderHelp.getFullName());
				
			} catch (ServiceException e) {
				e.printStackTrace();
			}
			
		}else if(Validations.validateIsNotNullAndNotEmpty(this.holderAccountToDataModel) ){
			holderAccountToDataModel = null;
		}
	}

	/**
	 * Back from consult to search.
	 */
	public void backFromConsultToSearch(){
		
		/**  CLOSED MODAL CONSULT  **/
		
		JSFUtilities.executeJavascriptFunction("PF('cnfwFlagConsultMemory').hide();");
		
	}
	
	
	/**
	 * *	Apply privileges to Exception Security	**.
	 */
	public void setTrueValidButtons(){        

		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		
		if(userPrivilege.getUserAcctions().isDelete()){
			privilegeComponent.setBtnDeleteView(true);
		}

		userPrivilege.setPrivilegeComponent(privilegeComponent);


	}
	
	
	/**
	 * Clean results exceptions.
	 */
	// Clean rateExceptionToDataModel
	public void cleanResultsExceptions(){
		
		if(Validations.validateListIsNotNullAndNotEmpty(listRateExceptionTo)){
			
			this.rateExceptionToDataModel = null ;	
			
		}
	}
	
	
}
