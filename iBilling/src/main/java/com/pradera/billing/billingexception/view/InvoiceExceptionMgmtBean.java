package com.pradera.billing.billingexception.view;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;

import com.pradera.billing.billingexception.facade.InvoiceExceptionServiceFacade;
import com.pradera.billing.billingexception.invoice.to.BillingEntityExceptionTo;
import com.pradera.billing.billingexception.invoice.to.BillingScheduleTo;
import com.pradera.billing.billingexception.invoice.to.ExceptionInvoiceGenerationTo;
import com.pradera.billing.billingexception.invoice.to.ProcessedExceptionInvoiceGenerationTo;
import com.pradera.billing.billingexception.to.HolderTo;
import com.pradera.billing.billingexception.to.IssuerTo;
import com.pradera.billing.billingrate.facade.BillingRateMgmtServiceFacade;
import com.pradera.billing.billingservice.facade.BillingServiceMgmtServiceFacade;
import com.pradera.billing.billingservice.to.BillingServiceTo;
import com.pradera.billing.billingservice.view.BillingServiceMgmtBean;
import com.pradera.billing.common.utils.BillingUtils;
import com.pradera.billing.helper.view.BillingServiceHelperBean;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.view.HolderHelperBean;
import com.pradera.core.component.helperui.view.IssuanceHelperBean;
import com.pradera.core.component.helperui.view.IssuerHelperBean;
import com.pradera.core.component.helperui.view.SecuritiesHelperBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.ExceptionInvoiceGenerations;
import com.pradera.model.billing.type.BillingServiceFilter;
import com.pradera.model.billing.type.EntityCollectionType;
import com.pradera.model.billing.type.InvoiceExceptionStatus;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class InvoiceExceptionMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class InvoiceExceptionMgmtBean extends GenericBaseBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;
	
	/** The Constant log. */
	private static final Logger log = Logger.getLogger(BillingServiceMgmtBean.class);
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade 				helperComponentFacade;
	
	/** The billing rate mgmt service facade. */
	@EJB
	BillingRateMgmtServiceFacade 		billingRateMgmtServiceFacade ;
	
	/** The general parameters service. */
	@EJB
	ParameterServiceBean 				generalParametersService;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade 			generalParametersFacade;	
	
	/** The invoice exception service facade. */
	@EJB
	InvoiceExceptionServiceFacade 		invoiceExceptionServiceFacade;
	
	/** The billing service mgmt service facade. */
	@EJB
	BillingServiceMgmtServiceFacade 	billingServiceMgmtServiceFacade;
	
	/** The billing service helper bean. */
	@Inject
	BillingServiceHelperBean 			billingServiceHelperBean;
	
	/** The issuer helper bean. */
	@Inject
	IssuerHelperBean 					issuerHelperBean;
	
	/** The securities helper bean. */
	@Inject
	SecuritiesHelperBean 				securitiesHelperBean;
	
	/** The issuance helper bean. */
	@Inject
	IssuanceHelperBean 					issuanceHelperBean;

	/** The holder helper bean. */
	@Inject
	HolderHelperBean					 holderHelperBean;

	
	/**  parameters for add  exception rules. */
	private BillingService 					billingService;
	
	/** The exception invoice generation to. */
	private ExceptionInvoiceGenerationTo 	exceptionInvoiceGenerationTo;
	
	/** The exception invoice selection. */
	private ExceptionInvoiceGenerationTo 	exceptionInvoiceSelection;
	
	/** The billing entity exception to. */
	private BillingEntityExceptionTo 		billingEntityExceptionTo;
	
	/** The billing schedule to. */
	private BillingScheduleTo 				billingScheduleTo;
	
	/** The collection period selected. */
	private Integer 						collectionPeriodSelected;
	
	/** The list exception status. */
	private List<ParameterTable> 			listExceptionStatus;
	
	/** The list exception. */
	private List<ParameterTable> 			listException;
	
	/** The list collection period. */
	private List<ParameterTable> 			listCollectionPeriod;
	
	/** The list entity collection. */
	private List<ParameterTable> 			listEntityCollection;
	
	
	/** Participant list. */
	private List<Participant> 				listClientParticipant;


	/** The in integrated bill selected. */
	private Integer 						inIntegratedBillSelected;
	
	/** The ind no integrated. */
	private Integer 						indNoIntegrated=PropertiesConstants.NOT_IND_INTEGRATED_BILL;
	
	/** The ind integrated. */
	private Integer 						indIntegrated=PropertiesConstants.YES_IND_INTEGRATED_BILL;
	
	/** The initial date. */
	private Date 							initialDate;
	
	/** The today date. */
	private Date 							todayDate;
	
	/** The rdb one. */
	private Integer 						rdbOne=PropertiesConstants.RDB_ONE;
	
	/** The rdb all. */
	private Integer 						rdbAll=PropertiesConstants.RDB_ALL;
	
	/** The show rdb num. */
	private Boolean 						showRdbNum;
	
	/** The ind num rule. */
	private Integer 						indNumRule;

	/** The holder selected. */
	private Holder 							holderSelected;
	
	/** The issuer. */
	private Issuer 							issuer;
	
	/** The participant selected. */
	private Long 							participantSelected;
    
    /** The cbm save. */
    private Boolean 						cbmSave=Boolean.FALSE;
    
    /** The cbm modify. */
    private Boolean 						cbmModify=Boolean.FALSE;      
    

    /** variables para entityCollection. */
    private Boolean 						disableEntityCollectionPanelVisible;
    
    /** The disable admin ec visible. */
    private Boolean 						disableAdminECVisible;
    
	/** The disable stock exchange ec visible. */
	private Boolean 						disableStockExchangeECVisible;
	
	/** The disable holder ec visible. */
	private Boolean 						disableHolderECVisible;
	
	/** The disable issuer ec visible. */
	private Boolean 						disableIssuerECVisible;
	
	/** The disable participant ec visible. */
	private Boolean 						disableParticipantECVisible;
	
	/** The disable other ec visible. */
	private Boolean 						disableOtherECVisible;
	
	/** The collection period disable. */
	private Boolean 						collectionPeriodDisable=Boolean.FALSE;
	
	/** The disable collection period panel visible. */
	private Boolean							disableCollectionPeriodPanelVisible;
	
	/** The disable daily cp visible. */
	private Boolean 						disableDailyCPVisible;
	
	/** The disable weekly cp visible. */
	private Boolean 						disableWeeklyCPVisible;
	
	/** The disable monthly cp visible. */
	private Boolean 						disableMonthlyCPVisible;
	
	/** The disable quarterly cp visible. */
	private Boolean 						disableQuarterlyCPVisible;
	
	/** The disable yearly cp visible. */
	private Boolean 						disableYearlyCPVisible;
    
    /** The bool end of month. */
    private Boolean							boolEndOfMonth;

    /** Agrupamiento de Servicios. */
    private Integer 										entityCollectionGroup;
    
    /** The billing service group. */
    private List<BillingService> 							billingServiceGroup;
    
    /** The billing service data model. */
    private GenericDataModel<BillingService> 				billingServiceDataModel;
    
    /** The list processed exception invoice generation to. */
    private List<ProcessedExceptionInvoiceGenerationTo> 	listProcessedExceptionInvoiceGenerationTo;
	
	/** The exception invoice data model. */
	private GenericDataModel<ExceptionInvoiceGenerationTo> 	exceptionInvoiceDataModel;
	
	/** The list exception invoice generation to. */
	private List<ExceptionInvoiceGenerationTo> 				listExceptionInvoiceGenerationTo=new ArrayList<ExceptionInvoiceGenerationTo>();
	
	/** The list excep table. */
	private List<ExceptionInvoiceGenerationTo> 				listExcepTable=new ArrayList<ExceptionInvoiceGenerationTo>();
	
	/** The data model billing entity exception to. */
	private GenericDataModel<BillingEntityExceptionTo> 		dataModelBillingEntityExceptionTo;
	
    /** Consult. */
    private List<BillingEntityExceptionTo> 					listBillingEntityExceptionTo;
    
    /** The billing entity data model. */
    private GenericDataModel<BillingEntityExceptionTo> 		billingEntityDataModel;

    
	/**
	 * Gets the data model billing entity exception to.
	 *
	 * @return the dataModelBillingEntityExceptionTo
	 */
	public GenericDataModel<BillingEntityExceptionTo> getDataModelBillingEntityExceptionTo() {
		return dataModelBillingEntityExceptionTo;
	}

	/**
	 * Sets the data model billing entity exception to.
	 *
	 * @param dataModelBillingEntityExceptionTo the dataModelBillingEntityExceptionTo to set
	 */
	public void setDataModelBillingEntityExceptionTo(
			GenericDataModel<BillingEntityExceptionTo> dataModelBillingEntityExceptionTo) {
		this.dataModelBillingEntityExceptionTo = dataModelBillingEntityExceptionTo;
	}

	/**
	 * Gets the disable collection period panel visible.
	 *
	 * @return the disableCollectionPeriodPanelVisible
	 */
	public Boolean getDisableCollectionPeriodPanelVisible() {
		return disableCollectionPeriodPanelVisible;
	}

	/**
	 * Sets the disable collection period panel visible.
	 *
	 * @param disableCollectionPeriodPanelVisible the disableCollectionPeriodPanelVisible to set
	 */
	public void setDisableCollectionPeriodPanelVisible(
			Boolean disableCollectionPeriodPanelVisible) {
		this.disableCollectionPeriodPanelVisible = disableCollectionPeriodPanelVisible;
	}

	/**
	 * Gets the disable entity collection panel visible.
	 *
	 * @return the disableEntityCollectionPanelVisible
	 */
	public Boolean getDisableEntityCollectionPanelVisible() {
		return disableEntityCollectionPanelVisible;
	}

	/**
	 * Sets the disable entity collection panel visible.
	 *
	 * @param disableEntityCollectionPanelVisible the disableEntityCollectionPanelVisible to set
	 */
	public void setDisableEntityCollectionPanelVisible(
			Boolean disableEntityCollectionPanelVisible) {
		this.disableEntityCollectionPanelVisible = disableEntityCollectionPanelVisible;
	}

	/**
	 * Gets the disable daily cp visible.
	 *
	 * @return the disableDailyCPVisible
	 */
	public Boolean getDisableDailyCPVisible() {
		return disableDailyCPVisible;
	}

	/**
	 * Sets the disable daily cp visible.
	 *
	 * @param disableDailyCPVisible the disableDailyCPVisible to set
	 */
	public void setDisableDailyCPVisible(Boolean disableDailyCPVisible) {
		this.disableDailyCPVisible = disableDailyCPVisible;
	}

	/**
	 * Gets the disable weekly cp visible.
	 *
	 * @return the disableWeeklyCPVisible
	 */
	public Boolean getDisableWeeklyCPVisible() {
		return disableWeeklyCPVisible;
	}

	/**
	 * Sets the disable weekly cp visible.
	 *
	 * @param disableWeeklyCPVisible the disableWeeklyCPVisible to set
	 */
	public void setDisableWeeklyCPVisible(Boolean disableWeeklyCPVisible) {
		this.disableWeeklyCPVisible = disableWeeklyCPVisible;
	}

	/**
	 * Gets the disable monthly cp visible.
	 *
	 * @return the disableMonthlyCPVisible
	 */
	public Boolean getDisableMonthlyCPVisible() {
		return disableMonthlyCPVisible;
	}

	/**
	 * Sets the disable monthly cp visible.
	 *
	 * @param disableMonthlyCPVisible the disableMonthlyCPVisible to set
	 */
	public void setDisableMonthlyCPVisible(Boolean disableMonthlyCPVisible) {
		this.disableMonthlyCPVisible = disableMonthlyCPVisible;
	}

	/**
	 * Gets the disable quarterly cp visible.
	 *
	 * @return the disableQuarterlyCPVisible
	 */
	public Boolean getDisableQuarterlyCPVisible() {
		return disableQuarterlyCPVisible;
	}

	/**
	 * Sets the disable quarterly cp visible.
	 *
	 * @param disableQuarterlyCPVisible the disableQuarterlyCPVisible to set
	 */
	public void setDisableQuarterlyCPVisible(Boolean disableQuarterlyCPVisible) {
		this.disableQuarterlyCPVisible = disableQuarterlyCPVisible;
	}

	
	/**
	 * Gets the disable yearly cp visible.
	 *
	 * @return the disableYearlyCPVisible
	 */
	public Boolean getDisableYearlyCPVisible() {
		return disableYearlyCPVisible;
	}

	/**
	 * Sets the disable yearly cp visible.
	 *
	 * @param disableYearlyCPVisible the disableYearlyCPVisible to set
	 */
	public void setDisableYearlyCPVisible(Boolean disableYearlyCPVisible) {
		this.disableYearlyCPVisible = disableYearlyCPVisible;
	}

	/**
	 * Gets the bool end of month.
	 *
	 * @return the boolEndOfMonth
	 */
	public Boolean getBoolEndOfMonth() {
		return boolEndOfMonth;
	}

	/**
	 * Sets the bool end of month.
	 *
	 * @param boolEndOfMonth the boolEndOfMonth to set
	 */
	public void setBoolEndOfMonth(Boolean boolEndOfMonth) {
		this.boolEndOfMonth = boolEndOfMonth;
	}

	/**
	 * Gets the collection period selected.
	 *
	 * @return the collectionPeriodSelected
	 */
	public Integer getCollectionPeriodSelected() {
		return collectionPeriodSelected;
	}

	/**
	 * Sets the collection period selected.
	 *
	 * @param collectionPeriodSelected the collectionPeriodSelected to set
	 */
	public void setCollectionPeriodSelected(Integer collectionPeriodSelected) {
		this.collectionPeriodSelected = collectionPeriodSelected;
	}

	/**
	 * Gets the list collection period.
	 *
	 * @return the listCollectionPeriod
	 */
	public List<ParameterTable> getListCollectionPeriod() {
		return listCollectionPeriod;
	}

	/**
	 * Sets the list collection period.
	 *
	 * @param listCollectionPeriod the listCollectionPeriod to set
	 */
	public void setListCollectionPeriod(List<ParameterTable> listCollectionPeriod) {
		this.listCollectionPeriod = listCollectionPeriod;
	}

	/**
	 * Gets the collection period disable.
	 *
	 * @return the collectionPeriodDisable
	 */
	public Boolean getCollectionPeriodDisable() {
		return collectionPeriodDisable;
	}

	/**
	 * Sets the collection period disable.
	 *
	 * @param collectionPeriodDisable the collectionPeriodDisable to set
	 */
	public void setCollectionPeriodDisable(Boolean collectionPeriodDisable) {
		this.collectionPeriodDisable = collectionPeriodDisable;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initialDate
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the initialDate to set
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the holder selected.
	 *
	 * @return the holderSelected
	 */
	public Holder getHolderSelected() {
		return holderSelected;
	}

	/**
	 * Sets the holder selected.
	 *
	 * @param holderSelected the holderSelected to set
	 */
	public void setHolderSelected(Holder holderSelected) {
		this.holderSelected = holderSelected;
	}

	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}

	/**
	 * Sets the issuer.
	 *
	 * @param issuer the issuer to set
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	/**
	 * Gets the participant selected.
	 *
	 * @return the participantSelected
	 */
	public Long getParticipantSelected() {
		return participantSelected;
	}

	/**
	 * Sets the participant selected.
	 *
	 * @param participantSelected the participantSelected to set
	 */
	public void setParticipantSelected(Long participantSelected) {
		this.participantSelected = participantSelected;
	}

	/**
	 * Gets the disable admin ec visible.
	 *
	 * @return the disableAdminECVisible
	 */
	public Boolean getDisableAdminECVisible() {
		return disableAdminECVisible;
	}

	/**
	 * Sets the disable admin ec visible.
	 *
	 * @param disableAdminECVisible the disableAdminECVisible to set
	 */
	public void setDisableAdminECVisible(Boolean disableAdminECVisible) {
		this.disableAdminECVisible = disableAdminECVisible;
	}

	/**
	 * Gets the disable stock exchange ec visible.
	 *
	 * @return the disableStockExchangeECVisible
	 */
	public Boolean getDisableStockExchangeECVisible() {
		return disableStockExchangeECVisible;
	}

	/**
	 * Sets the disable stock exchange ec visible.
	 *
	 * @param disableStockExchangeECVisible the disableStockExchangeECVisible to set
	 */
	public void setDisableStockExchangeECVisible(
			Boolean disableStockExchangeECVisible) {
		this.disableStockExchangeECVisible = disableStockExchangeECVisible;
	}

	/**
	 * Gets the disable holder ec visible.
	 *
	 * @return the disableHolderECVisible
	 */
	public Boolean getDisableHolderECVisible() {
		return disableHolderECVisible;
	}

	/**
	 * Sets the disable holder ec visible.
	 *
	 * @param disableHolderECVisible the disableHolderECVisible to set
	 */
	public void setDisableHolderECVisible(Boolean disableHolderECVisible) {
		this.disableHolderECVisible = disableHolderECVisible;
	}

	/**
	 * Gets the disable issuer ec visible.
	 *
	 * @return the disableIssuerECVisible
	 */
	public Boolean getDisableIssuerECVisible() {
		return disableIssuerECVisible;
	}

	/**
	 * Sets the disable issuer ec visible.
	 *
	 * @param disableIssuerECVisible the disableIssuerECVisible to set
	 */
	public void setDisableIssuerECVisible(Boolean disableIssuerECVisible) {
		this.disableIssuerECVisible = disableIssuerECVisible;
	}

	/**
	 * Gets the disable participant ec visible.
	 *
	 * @return the disableParticipantECVisible
	 */
	public Boolean getDisableParticipantECVisible() {
		return disableParticipantECVisible;
	}

	/**
	 * Sets the disable participant ec visible.
	 *
	 * @param disableParticipantECVisible the disableParticipantECVisible to set
	 */
	public void setDisableParticipantECVisible(Boolean disableParticipantECVisible) {
		this.disableParticipantECVisible = disableParticipantECVisible;
	}

	/**
	 * Gets the disable other ec visible.
	 *
	 * @return the disableOtherECVisible
	 */
	public Boolean getDisableOtherECVisible() {
		return disableOtherECVisible;
	}

	/**
	 * Sets the disable other ec visible.
	 *
	 * @param disableOtherECVisible the disableOtherECVisible to set
	 */
	public void setDisableOtherECVisible(Boolean disableOtherECVisible) {
		this.disableOtherECVisible = disableOtherECVisible;
	}

	/**
	 * Gets the show rdb num.
	 *
	 * @return the showRdbNum
	 */
	public Boolean getShowRdbNum() {
		return showRdbNum;
	}

	/**
	 * Sets the show rdb num.
	 *
	 * @param showRdbNum the showRdbNum to set
	 */
	public void setShowRdbNum(Boolean showRdbNum) {
		this.showRdbNum = showRdbNum;
	}

	/**
	 * Gets the list client participant.
	 *
	 * @return the listClientParticipant
	 */
	public List<Participant> getListClientParticipant() {
		return listClientParticipant;
	}

	/**
	 * Sets the list client participant.
	 *
	 * @param listClientParticipant the listClientParticipant to set
	 */
	public void setListClientParticipant(List<Participant> listClientParticipant) {
		this.listClientParticipant = listClientParticipant;
	}

	/**
	 * Gets the exception invoice selection.
	 *
	 * @return the exceptionInvoiceSelection
	 */
	public ExceptionInvoiceGenerationTo getExceptionInvoiceSelection() {
		return exceptionInvoiceSelection;
	}

	/**
	 * Sets the exception invoice selection.
	 *
	 * @param exceptionInvoiceSelection the exceptionInvoiceSelection to set
	 */
	public void setExceptionInvoiceSelection(
			ExceptionInvoiceGenerationTo exceptionInvoiceSelection) {
		this.exceptionInvoiceSelection = exceptionInvoiceSelection;
	}

	/**
	 * Gets the exception invoice generation to.
	 *
	 * @return the exceptionInvoiceGenerationTo
	 */
	public ExceptionInvoiceGenerationTo getExceptionInvoiceGenerationTo() {
		return exceptionInvoiceGenerationTo;
	}

	/**
	 * Sets the exception invoice generation to.
	 *
	 * @param exceptionInvoiceGenerationTo the exceptionInvoiceGenerationTo to set
	 */
	public void setExceptionInvoiceGenerationTo(
			ExceptionInvoiceGenerationTo exceptionInvoiceGenerationTo) {
		this.exceptionInvoiceGenerationTo = exceptionInvoiceGenerationTo;
	}

	/**
	 * Gets the billing entity exception to.
	 *
	 * @return the billingEntityExceptionTo
	 */
	public BillingEntityExceptionTo getBillingEntityExceptionTo() {
		return billingEntityExceptionTo;
	}

	/**
	 * Sets the billing entity exception to.
	 *
	 * @param billingEntityExceptionTo the billingEntityExceptionTo to set
	 */
	public void setBillingEntityExceptionTo(
			BillingEntityExceptionTo billingEntityExceptionTo) {
		this.billingEntityExceptionTo = billingEntityExceptionTo;
	}

	/**
	 * Gets the billing schedule to.
	 *
	 * @return the billingScheduleTo
	 */
	public BillingScheduleTo getBillingScheduleTo() {
		return billingScheduleTo;
	}

	/**
	 * Sets the billing schedule to.
	 *
	 * @param billingScheduleTo the billingScheduleTo to set
	 */
	public void setBillingScheduleTo(BillingScheduleTo billingScheduleTo) {
		this.billingScheduleTo = billingScheduleTo;
	}

	/**
	 * Gets the list exception status.
	 *
	 * @return the listExceptionStatus
	 */
	public List<ParameterTable> getListExceptionStatus() {
		return listExceptionStatus;
	}

	/**
	 * Sets the list exception status.
	 *
	 * @param listExceptionStatus the listExceptionStatus to set
	 */
	public void setListExceptionStatus(List<ParameterTable> listExceptionStatus) {
		this.listExceptionStatus = listExceptionStatus;
	}

	/**
	 * Gets the list exception.
	 *
	 * @return the listException
	 */
	public List<ParameterTable> getListException() {
		return listException;
	}

	/**
	 * Sets the list exception.
	 *
	 * @param listException the listException to set
	 */
	public void setListException(List<ParameterTable> listException) {
		this.listException = listException;
	}

	/**
	 * Gets the list entity collection.
	 *
	 * @return the listEntityCollection
	 */
	public List<ParameterTable> getListEntityCollection() {
		return listEntityCollection;
	}

	/**
	 * Sets the list entity collection.
	 *
	 * @param listEntityCollection the listEntityCollection to set
	 */
	public void setListEntityCollection(List<ParameterTable> listEntityCollection) {
		this.listEntityCollection = listEntityCollection;
	}

	/**
	 * Gets the exception invoice data model.
	 *
	 * @return the exceptionInvoiceDataModel
	 */
	public GenericDataModel<ExceptionInvoiceGenerationTo> getExceptionInvoiceDataModel() {
		return exceptionInvoiceDataModel;
	}

	/**
	 * Sets the exception invoice data model.
	 *
	 * @param exceptionInvoiceDataModel the exceptionInvoiceDataModel to set
	 */
	public void setExceptionInvoiceDataModel(
			GenericDataModel<ExceptionInvoiceGenerationTo> exceptionInvoiceDataModel) {
		this.exceptionInvoiceDataModel = exceptionInvoiceDataModel;
	}

	/**
	 * Gets the today date.
	 *
	 * @return the todayDate
	 */
	public Date getTodayDate() {
		return todayDate;
	}

	/**
	 * Sets the today date.
	 *
	 * @param todayDate the todayDate to set
	 */
	public void setTodayDate(Date todayDate) {
		this.todayDate = todayDate;
	}

	/**
	 * Gets the rdb one.
	 *
	 * @return the rdbOne
	 */
	public Integer getRdbOne() {
		return rdbOne;
	}

	/**
	 * Sets the rdb one.
	 *
	 * @param rdbOne the rdbOne to set
	 */
	public void setRdbOne(Integer rdbOne) {
		this.rdbOne = rdbOne;
	}

	/**
	 * Gets the rdb all.
	 *
	 * @return the rdbAll
	 */
	public Integer getRdbAll() {
		return rdbAll;
	}

	/**
	 * Sets the rdb all.
	 *
	 * @param rdbAll the rdbAll to set
	 */
	public void setRdbAll(Integer rdbAll) {
		this.rdbAll = rdbAll;
	}

	/**
	 * Gets the ind num rule.
	 *
	 * @return the indNumRule
	 */
	public Integer getIndNumRule() {
		return indNumRule;
	}

	/**
	 * Sets the ind num rule.
	 *
	 * @param indNumRule the indNumRule to set
	 */
	public void setIndNumRule(Integer indNumRule) {
		this.indNumRule = indNumRule;
	}

	/**
	 * Gets the billing service.
	 *
	 * @return the billingService
	 */
	public BillingService getBillingService() {
		return billingService;
	}

	/**
	 * Sets the billing service.
	 *
	 * @param billingService the billingService to set
	 */
	public void setBillingService(BillingService billingService) {
		this.billingService = billingService;
	}

	/**
	 * Gets the list exception invoice generation to.
	 *
	 * @return the listExceptionInvoiceGenerationTo
	 */
	public List<ExceptionInvoiceGenerationTo> getListExceptionInvoiceGenerationTo() {
		return listExceptionInvoiceGenerationTo;
	}

	/**
	 * Sets the list exception invoice generation to.
	 *
	 * @param listExceptionInvoiceGenerationTo the listExceptionInvoiceGenerationTo to set
	 */
	public void setListExceptionInvoiceGenerationTo(
			List<ExceptionInvoiceGenerationTo> listExceptionInvoiceGenerationTo) {
		this.listExceptionInvoiceGenerationTo = listExceptionInvoiceGenerationTo;
	}

	/**
	 * Gets the list billing entity exception to.
	 *
	 * @return the listBillingEntityExceptionTo
	 */
	public List<BillingEntityExceptionTo> getListBillingEntityExceptionTo() {
		return listBillingEntityExceptionTo;
	}

	/**
	 * Sets the list billing entity exception to.
	 *
	 * @param listBillingEntityExceptionTo the listBillingEntityExceptionTo to set
	 */
	public void setListBillingEntityExceptionTo(
			List<BillingEntityExceptionTo> listBillingEntityExceptionTo) {
		this.listBillingEntityExceptionTo = listBillingEntityExceptionTo;
	}
	
	/**
	 * Gets the billing entity data model.
	 *
	 * @return the billingEntityDataModel
	 */
	public GenericDataModel<BillingEntityExceptionTo> getBillingEntityDataModel() {
		return billingEntityDataModel;
	}

	/**
	 * Sets the billing entity data model.
	 *
	 * @param billingEntityDataModel the billingEntityDataModel to set
	 */
	public void setBillingEntityDataModel(
			GenericDataModel<BillingEntityExceptionTo> billingEntityDataModel) {
		this.billingEntityDataModel = billingEntityDataModel;
	}
	
	/**
	 * Gets the in integrated bill selected.
	 *
	 * @return the inIntegratedBillSelected
	 */
	public Integer getInIntegratedBillSelected() {
		return inIntegratedBillSelected;
	}

	/**
	 * Sets the in integrated bill selected.
	 *
	 * @param inIntegratedBillSelected the inIntegratedBillSelected to set
	 */
	public void setInIntegratedBillSelected(Integer inIntegratedBillSelected) {
		this.inIntegratedBillSelected = inIntegratedBillSelected;
	}

	/**
	 * Gets the ind no integrated.
	 *
	 * @return the indNoIntegrated
	 */
	public Integer getIndNoIntegrated() {
		return indNoIntegrated;
	}

	/**
	 * Sets the ind no integrated.
	 *
	 * @param indNoIntegrated the indNoIntegrated to set
	 */
	public void setIndNoIntegrated(Integer indNoIntegrated) {
		this.indNoIntegrated = indNoIntegrated;
	}

	/**
	 * Gets the ind integrated.
	 *
	 * @return the indIntegrated
	 */
	public Integer getIndIntegrated() {
		return indIntegrated;
	}

	/**
	 * Sets the ind integrated.
	 *
	 * @param indIntegrated the indIntegrated to set
	 */
	public void setIndIntegrated(Integer indIntegrated) {
		this.indIntegrated = indIntegrated;
	}

	/**
	 * Gets the entity collection group.
	 *
	 * @return the entityCollectionGroup
	 */
	public Integer getEntityCollectionGroup() {
		return entityCollectionGroup;
	}

	/**
	 * Sets the entity collection group.
	 *
	 * @param entityCollectionGroup the entityCollectionGroup to set
	 */
	public void setEntityCollectionGroup(Integer entityCollectionGroup) {
		this.entityCollectionGroup = entityCollectionGroup;
	}

	/**
	 * Gets the billing service group.
	 *
	 * @return the billingServiceGroup
	 */
	public List<BillingService> getBillingServiceGroup() {
		return billingServiceGroup;
	}

	/**
	 * Sets the billing service group.
	 *
	 * @param billingServiceGroup the billingServiceGroup to set
	 */
	public void setBillingServiceGroup(List<BillingService> billingServiceGroup) {
		this.billingServiceGroup = billingServiceGroup;
	}

	/**
	 * Gets the billing service data model.
	 *
	 * @return the billingServiceDataModel
	 */
	public GenericDataModel<BillingService> getBillingServiceDataModel() {
		return billingServiceDataModel;
	}

	/**
	 * Sets the billing service data model.
	 *
	 * @param billingServiceDataModel the billingServiceDataModel to set
	 */
	public void setBillingServiceDataModel(
			GenericDataModel<BillingService> billingServiceDataModel) {
		this.billingServiceDataModel = billingServiceDataModel;
	}

	/**
	 * Gets the list processed exception invoice generation to.
	 *
	 * @return the listProcessedExceptionInvoiceGenerationTo
	 */
	public List<ProcessedExceptionInvoiceGenerationTo> getListProcessedExceptionInvoiceGenerationTo() {
		return listProcessedExceptionInvoiceGenerationTo;
	}

	/**
	 * Sets the list processed exception invoice generation to.
	 *
	 * @param listProcessedExceptionInvoiceGenerationTo the listProcessedExceptionInvoiceGenerationTo to set
	 */
	public void setListProcessedExceptionInvoiceGenerationTo(
			List<ProcessedExceptionInvoiceGenerationTo> listProcessedExceptionInvoiceGenerationTo) {
		this.listProcessedExceptionInvoiceGenerationTo = listProcessedExceptionInvoiceGenerationTo;
	}
	

	/**
	 * Inits the.
	 */


	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	

	


	/**
	 *PostConstructor 
	 */
	@PostConstruct
	public void init()  {
		
		if (!FacesContext.getCurrentInstance().isPostback()){
			this.billingService= new BillingService();
			cleanHelperEntityCollection();
			todayDate=CommonsUtilities.currentDate();
			loadExceptionObject();
			loadCmbData();
			

			exceptionInvoiceGenerationTo.setExceptionDate(todayDate);
		}
		
	}
	
	/**
	 * *
	 * Action to Button New Excepction Invoice.
	 */
	public void loadRegistrationPage(){
		/**Cargar Combos loadCmbData*/
		loadCmbData();
		getParticipantListToCombo();
		cleanRegisterExcpInvoice();
		
		showRdbNum=Boolean.TRUE;
		/**Limpiar pantalla clear**/
		this.setViewOperationType(ViewOperationsType.REGISTER.getCode());
		/**
		 * Por Default Factura Individual 
		 */
		inIntegratedBillSelected=PropertiesConstants.NOT_IND_INTEGRATED_BILL;
		initialDate =CommonsUtilities.currentDate();
		
		
	}
	
	/**
	 * *	Apply privileges to Invoice Exception Security	**.
	 */
	public void setTrueValidButtons(){        

		PrivilegeComponent privilegeComponent = new PrivilegeComponent();

//		if(userPrivilege.getUserAcctions().isModify()){
//			privilegeComponent.setBtnModifyView(true);
//		}

		if(userPrivilege.getUserAcctions().isDelete()){
			privilegeComponent.setBtnDeleteView(true);
		}


		userPrivilege.setPrivilegeComponent(privilegeComponent);

	}
	
	/**
	 * Load exception object.
	 */
	public void loadExceptionObject(){
		exceptionInvoiceGenerationTo=new ExceptionInvoiceGenerationTo();
		exceptionInvoiceGenerationTo.setExceptionDate(todayDate);
		exceptionInvoiceSelection=new ExceptionInvoiceGenerationTo();
		billingEntityExceptionTo=new BillingEntityExceptionTo();
		billingScheduleTo=new BillingScheduleTo();
		
		billingEntityDataModel=null;
	}
	
	/**
	 * Load cmb data.
	 */
	public void loadCmbData() {
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		/** GETS FROM DATABASE State  */
		try {
			if(Validations.validateListIsNullOrEmpty(listExceptionStatus)){
				log.debug("lista de estados de las Excepciones de Facturacion: ");
				parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
				parameterTableTO.setMasterTableFk(BillingServiceFilter.EXCEPTION_INVOICE_STATUS_PK.getCode());
				listExceptionStatus= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			}
			
			if(Validations.validateListIsNullOrEmpty(listException)){
				log.debug("lista de tipo de Excepcion:");
				parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
				parameterTableTO.setMasterTableFk(PropertiesConstants.EXCEPTION_PK);
				listException=generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			}
			
			if (Validations.validateListIsNullOrEmpty(listCollectionPeriod)){
				log.debug("lista de periodos de cobro:");
				parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
				parameterTableTO.setOrderbyParameterTableCd(GeneralConstants.ONE_VALUE_INTEGER);
				parameterTableTO.setMasterTableFk(BillingServiceFilter.COLLECTION_PERIOD_PK.getCode());
				listCollectionPeriod= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			}
			
			if(Validations.validateListIsNullOrEmpty(listEntityCollection)){
				log.debug("lista de Entidad de Cobro :");
				parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
				parameterTableTO.setMasterTableFk(BillingServiceFilter.ENTITY_COLLECTION_PK.getCode());
				listEntityCollection=generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			}
			
		} catch (ServiceException e) {
			log.debug(e.getMessage());
			e.printStackTrace();
		}
	
	}
	
	/**
	 * Search exception invoice.
	 */
	@LoggerAuditWeb
	public void searchExceptionInvoice(){
		ExceptionInvoiceGenerationTo exceptionInvoiceFilter=new ExceptionInvoiceGenerationTo();
		exceptionInvoiceFilter.setBillingService(new BillingServiceTo(billingService.getIdBillingServicePk()));
		exceptionInvoiceFilter.setExceptionState(exceptionInvoiceGenerationTo.getExceptionState());
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put(GeneralConstants.COLLECTION_PERIOD, 	listCollectionPeriod);
		parameters.put(GeneralConstants.ENTITY_COLLECTION, 	listEntityCollection);
		parameters.put(GeneralConstants.EXCEPTION, 			listException);
		parameters.put(GeneralConstants.EXCEPTION_STATUS, 	listExceptionStatus);
		
		listExceptionInvoiceGenerationTo=invoiceExceptionServiceFacade.searchInvoiceException(parameters, exceptionInvoiceFilter);
		
		exceptionInvoiceDataModel=new GenericDataModel<>();
		if(Validations.validateListIsNotNullAndNotEmpty(listExceptionInvoiceGenerationTo)){
			
			exceptionInvoiceDataModel.setWrappedData(listExceptionInvoiceGenerationTo);
			
			/**Deshabilitar Opciones visibles **/
			setTrueValidButtons();
		}
			
	}
		
		/**
		 * Clean exception invoice.
		 */
		public void cleanExceptionInvoice(){
			billingService=new BillingService();
			exceptionInvoiceGenerationTo=new ExceptionInvoiceGenerationTo();
			exceptionInvoiceGenerationTo.setExceptionDate(todayDate);
			exceptionInvoiceDataModel=null;
		}
		
		/**
		 * Clean helper entity collection.
		 */
		public void cleanHelperEntityCollection(){
			this.issuer=new Issuer();
			this.holderSelected=new Holder();
			this.participantSelected=null;
		}
		
		/**
		 * Delete exception.
		 */
		@LoggerAuditWeb
		public void deleteException(){
			
			
			try {
				ExceptionInvoiceGenerations exceptionInvoice=invoiceExceptionServiceFacade.getExceptionInvoiceGenerations(exceptionInvoiceSelection.getIdExceptionInvoicePk());
				exceptionInvoice.setExceptionState(InvoiceExceptionStatus.DELETED.getCode());
				invoiceExceptionServiceFacade.deletedInvoiceGeneration(exceptionInvoice);
			} catch (ServiceException e) {
				e.printStackTrace();
			}
			executeAction();	
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
					, PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_EXCEPTION_DELETE,
							new Object[]{exceptionInvoiceSelection.getIdExceptionInvoicePk().toString()}));
			JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialogDelete').show();");
		}
		
		
		/**
		 * Before deleted exception.
		 *
		 * @param event the event
		 */
		public void beforeDeletedException(ActionEvent event){
			
			if(InvoiceExceptionStatus.REGISTER.getCode().equals(exceptionInvoiceSelection.getExceptionState())){
			
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
						, PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_DELETE_EXCEPTION_INVOICE,
								new Object[]{exceptionInvoiceSelection.getIdExceptionInvoicePk().toString()}));
				JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmSourceDeleted').show();");
			}
		}
		
		/**
		 * Validatebefore modify exception.
		 *
		 * @param event the event
		 */
		@LoggerAuditWeb
		public void validatebeforeModifyException(ActionEvent event){
			
			
		}
		
		/**
		 * getParticipantListToCombo.
		 *
		 * @return the participant list to combo
		 */
		public void getParticipantListToCombo() {
			if(Validations.validateListIsNullOrEmpty(listClientParticipant)){
			Participant participantTemp=new Participant();
			
			participantTemp.setState(ParticipantStateType.REGISTERED.getCode());
			try {
				listClientParticipant=helperComponentFacade.getLisParticipantServiceFacade(participantTemp);
			} catch (ServiceException e) {
				e.printStackTrace();
			}
			}
			
		}

	
	/**
	 * Update rules.
	 */
	@LoggerAuditWeb
	public void updateRules(){
		
		Integer valueEntityCollection= null;
		/**Dependiendo la entidad de cobro actualizo los paneles**/
		if(PropertiesConstants.RDB_ONE.equals(exceptionInvoiceGenerationTo.getIndSelectAll())){
			
			valueEntityCollection=getEntityCollectionByInd();

			if(valueEntityCollection!=null){
				if(EntityCollectionType.ISSUERS.getCode().equals(valueEntityCollection)){
					resetRenderedEntityCollection();
					disableIssuerECVisible=Boolean.TRUE;
				}else if(EntityCollectionType.PARTICIPANTS.getCode().equals(valueEntityCollection)){
					resetRenderedEntityCollection();
					disableParticipantECVisible=Boolean.TRUE;
				}else if(EntityCollectionType.HOLDERS.getCode().equals(valueEntityCollection)){
					resetRenderedEntityCollection();
					disableHolderECVisible=Boolean.TRUE;
				}else if(EntityCollectionType.OTHERS.getCode().equals(valueEntityCollection)){
					resetRenderedEntityCollection();
					disableOtherECVisible=Boolean.TRUE;
				}else if(EntityCollectionType.MANAGER.getCode().equals(valueEntityCollection)){
					resetRenderedEntityCollection();
					disableAdminECVisible=Boolean.TRUE;
				}else if(EntityCollectionType.STOCK_EXCHANGE.getCode().equals(valueEntityCollection)){
					resetRenderedEntityCollection();
					disableStockExchangeECVisible=Boolean.TRUE;
				}
				disableEntityCollectionPanelVisible=Boolean.TRUE;
			}else{
				resetRenderedEntityCollection();
			}
						
		}else if(PropertiesConstants.RDB_ALL.equals(exceptionInvoiceGenerationTo.getIndSelectAll())){
			resetRenderedEntityCollection();
		}
		
		disableCollectionPeriodPanelVisible=Boolean.TRUE;
		cleanHelperEntityCollection();
		exceptionInvoiceGenerationTo.getBillingEntityException().clear();
	}
	
	/**
	 * Update billing schedule.
	 */
	@LoggerAuditWeb
	public void updateBillingSchedule(){
		
		if(PropertiesConstants.COLLECTION_PERIOD_DAILY_PK.equals(collectionPeriodSelected)){
			resetRenderedCollectionPeriod();
		}else if(PropertiesConstants.COLLECTION_PERIOD_WEEKLY_PK.equals(collectionPeriodSelected)){
			resetRenderedCollectionPeriod();
			disableWeeklyCPVisible=Boolean.TRUE;
		}else if(PropertiesConstants.COLLECTION_PERIOD_MONTHLY_PK.equals(collectionPeriodSelected)){
			resetRenderedCollectionPeriod();
			disableMonthlyCPVisible=Boolean.TRUE;
		}else if(PropertiesConstants.COLLECTION_PERIOD_QUARTERLY_PK.equals(collectionPeriodSelected)){
			resetRenderedCollectionPeriod();
			disableQuarterlyCPVisible=Boolean.TRUE;
		}else if(PropertiesConstants.COLLECTION_PERIOD_YEARLY_PK.equals(collectionPeriodSelected)){
			resetRenderedCollectionPeriod();
			disableYearlyCPVisible=Boolean.TRUE;
		}else{
			resetRenderedCollectionPeriod();
		}
		
		initialDate =CommonsUtilities.currentDate();
		boolEndOfMonth=Boolean.FALSE;
		
	}
	
	/**
	 * Hide OutPutPanel EntityCollection.
	 */
	public void resetRenderedEntityCollection(){
		disableAdminECVisible=Boolean.FALSE;
		disableStockExchangeECVisible=Boolean.FALSE;
		disableHolderECVisible=Boolean.FALSE;
		disableIssuerECVisible=Boolean.FALSE;
		disableParticipantECVisible=Boolean.FALSE;
		disableOtherECVisible=Boolean.FALSE;
		
		disableEntityCollectionPanelVisible=Boolean.FALSE;
		disableCollectionPeriodPanelVisible=Boolean.FALSE;
	}
	
	/**
	 * Hide OutPutPanel CollectionPeriod.
	 */
	public void resetRenderedCollectionPeriod(){
		disableDailyCPVisible=Boolean.FALSE;
		disableWeeklyCPVisible=Boolean.FALSE;
		disableMonthlyCPVisible=Boolean.FALSE;
		disableQuarterlyCPVisible=Boolean.FALSE;
		disableYearlyCPVisible=Boolean.FALSE;
	}
	
	/**
	 * Before registration listener excp invoice.
	 */
	public void beforeRegistrationListenerExcpInvoice() {	
		log.debug("beforeRegistrationListener :");
		Boolean flagSave=Boolean.FALSE;
		/**Validaciones que si es Agrupada el grupo que pertenezca sea el mismo**/
		
		if(Validations.validateListIsNullOrEmpty(listExcepTable)){
			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_INVOICE_EXCEPTION_REQUIRED));
			JSFUtilities.executeJavascriptFunction("PF('warningDialog').show()");  
			return;
		}else{
			flagSave=Boolean.TRUE;
		}
		
		if(flagSave){
			Object codigo=billingService.getServiceCode();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
					, PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_EXCP_INVOICE_ADD,codigo));
			JSFUtilities.executeJavascriptFunction("PF('cnfwExcpInvoiceSave').show();");

		}
		
		
	}
	
	/**
	 * Save excp invoice.
	 */
	@LoggerAuditWeb
	public void saveExcpInvoice() {
		log.debug("saveExcpInvoice  :");
		
		try {
			
			invoiceExceptionServiceFacade.saveProcessedInvoiceException(listProcessedExceptionInvoiceGenerationTo);
			
		} catch (ServiceException e) {
			executeAction();
			showMessageOnDialog(PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_ROLL_BACK_BILLING_SERVICE_REGISTER,""));
			JSFUtilities.executeJavascriptFunction("PF('cnfWCollectionDialogRollBack').show();");
//			/**Limpiar los campos**/
		}

		
		JSFUtilities.executeJavascriptFunction("PF('cnfwExcpInvoiceSave').hide();");
		executeAction();
		Object codigo=billingService.getServiceCode();
		
		showMessageOnDialog(PropertiesUtilities
				.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(PropertiesConstants.SUCCES_INVOICE_EXCEPTION_SAVE,codigo));
		JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		cleanRegisterExcpInvoice();
	}
	
	/**
	 * Limpia la seccion del helper.
	 */
	public void cleanBillingHelper(){
		this.billingService=new BillingService();
	}
	
	/**
	 * Limpia todo.
	 */
	public void cleanExceptionInvoiceBox(){
		this.billingService=new BillingService();
		loadExceptionObject();
		cleanHelperEntityCollection();
		resetRenderedEntityCollection();
		resetRenderedCollectionPeriod();
		collectionPeriodDisable=Boolean.TRUE;
		exceptionInvoiceGenerationTo= new ExceptionInvoiceGenerationTo();
		collectionPeriodSelected=null;
		if(Validations.validateListIsNotNullAndNotEmpty(listExceptionInvoiceGenerationTo)){
			listExceptionInvoiceGenerationTo.clear();
		}
		if(Validations.validateListIsNotNullAndNotEmpty(listExcepTable)){
			listExcepTable.clear();
		}
		
		exceptionInvoiceDataModel=new GenericDataModel<>(listExcepTable);
		listProcessedExceptionInvoiceGenerationTo= new ArrayList<ProcessedExceptionInvoiceGenerationTo>();
		JSFUtilities.resetComponent("frmRegisterExcpInvoice");
	}
	
	/**
	 * Limpia las cajas de registerExceptionInvoice.
	 */
	public void cleanRegisterExcpInvoice(){
		
		cleanExceptionInvoiceBox();
		cleanBillingServiceClustered();
		listExcepTable=new ArrayList<>();
		dataModelBillingEntityExceptionTo=new GenericDataModel<BillingEntityExceptionTo>();
		/**
		 * Facturacion Individual por Defecto
		 */
		inIntegratedBillSelected=PropertiesConstants.NOT_IND_INTEGRATED_BILL;
		
	}

	/**
	 * Clean Panel involved Billing Service Clustered.
	 */
	public void cleanBillingServiceClustered(){
		billingServiceGroup=new ArrayList<BillingService>();
		entityCollectionGroup=null;
		billingServiceDataModel=new GenericDataModel<BillingService>(billingServiceGroup);
		listExceptionInvoiceGenerationTo=new ArrayList<>();
	}
	
	/**
	 * Method to action to Billing Service Helper.
	 *
	 * @param event the event
	 */
	public void updateSelectedCombo(ActionEvent event){
		Boolean flagClean=Boolean.FALSE;
		if(Validations.validateIsNotNullAndNotEmpty(this.billingService)){
			Object codigo=billingService.getServiceCode();
			if(Validations.validateIsNotNull(codigo))
			{
				if(codigo.equals(GeneralConstants.EMPTY_STRING)){
					flagClean=Boolean.TRUE;
				}
				else if(Validations.validateIsNotNull(billingService.getServiceState())){
					if(billingService.getServiceState().compareTo(PropertiesConstants.SERVICE_STATUS_ACTIVE_PK)==0){

							showPanelException();

					}
					else{
						billingService=new BillingService();
						executeAction();
						showMessageOnDialog(PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_STATUS_ACTIVE,codigo));
						JSFUtilities.executeJavascriptFunction("PF('cnfWCollectionDialog').show();");
						return;
						}
				}
				else {
					flagClean=Boolean.TRUE;
				}
				
				if(Validations.validateIsNotNull(billingService.getEntityCollection())){
					if(billingService.getEntityCollection().compareTo(EntityCollectionType.MANAGER.getCode())==0)
					{
						billingService=new BillingService();
						executeAction();
						showMessageOnDialog(PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_ENTITY_ADMIN,codigo));
						JSFUtilities.executeJavascriptFunction("PF('cnfWCollectionDialog').show();");
						return;
						
					}
				}
				else{
					flagClean=Boolean.TRUE;
				}
				

				
			}
			else {
				flagClean=Boolean.TRUE;
			}
			
		}
		else{
			flagClean=Boolean.TRUE;
		}
		if(flagClean){
			cleanBillingHelper();
		}
	}
	
	/**
	 * *
	 * If BillingService can be used.
	 */
	public void showPanelException(){
		collectionPeriodSelected=null;
		collectionPeriodDisable=Boolean.FALSE;
		resetRenderedEntityCollection();
		resetRenderedCollectionPeriod();
		exceptionInvoiceGenerationTo=new ExceptionInvoiceGenerationTo();
	}
	
	/**
	 * Go to.
	 *
	 * @param viewToGo the view to go
	 * @return the string
	 */
	public String goTo(String viewToGo) {
		
		if  (viewToGo.equals("editExcpInvoice")){
			if (Validations.validateIsNotNull(billingService))
			{				 
				viewToGo="newExceptionInvoiceMgmt";
			}
		}
		return viewToGo;
		}
	
	/**
	 *  Method that open dialog to confirm or cancel when this operation go to register one service.
	 */	
	public void hideDialogsFromNew(){
//		JSFUtilities.hideComponent("frmSearchManualCollection:cnfIdProcessedServiceSave");
//		JSFUtilities.hideGeneralDialogues();
		this.searchExceptionInvoice();
	}
	
	/**
	 * Before add exception invoice.
	 */
	@LoggerAuditWeb
	public void beforeAddExceptionInvoice(){
		
		if(PropertiesConstants.EXCLUSION_PK.equals(exceptionInvoiceGenerationTo.getTypeException())&&
				!isBillingServiceIncluded()){
			/**Si la excepcion es de tipo Exclusion, Consultar
			 * Si es servicio tiene alguna Inclusion
			 * **/
			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_EXCLUSION_REQUIRED));
			JSFUtilities.executeJavascriptFunction("PF('warningDialog').show()");  
			return;
		}
		if(Validations.validateIsNullOrNotPositive(exceptionInvoiceGenerationTo.getTypeException())){
			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_TIPE_EXCEPTION_REQUIRED));
			JSFUtilities.executeJavascriptFunction("PF('warningDialog').show()");  
			return;
		}
		
		if(Validations.validateIsNull(exceptionInvoiceGenerationTo.getIndSelectAll())){
			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_INDICATOR_ONE_ALL_REQUIRED));
			JSFUtilities.executeJavascriptFunction("PF('warningDialog').show()");  
			return;
		}
		
		/**Si una entidad se encuentra en una Inclusion, no se puede volver a incluir*/
		
		Object codigo=this.billingService.getServiceCode();
		executeAction();
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
				, PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_EXCP_INVOICE_ADD,codigo));
		JSFUtilities.executeJavascriptFunction("PF('cnfwAddExcpInvoiceSave').show();");
		
	}
	
	/**
	 * Adds the exception invoice.
	 */
	public void addExceptionInvoice(){
		billingScheduleTo.setCollectionPeriod(collectionPeriodSelected);
		ProcessedExceptionInvoiceGenerationTo processedExceptionInvoiceGenerationTo= new ProcessedExceptionInvoiceGenerationTo();
		/**
		 * Solo para la facturacion Agrupada, agregar un listado de servicios
		 */
		ExceptionInvoiceGenerationTo exceptionInvoiceGenerationTo=null;
		if(PropertiesConstants.YES_IND_INTEGRATED_BILL.equals(inIntegratedBillSelected)){

			if(!Validations.validateListIsNotNullAndNotEmpty(billingServiceGroup)){
				//Debe Existir al menos un servicio				
				showMessageOnDialog(
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_AT_LEAST_ONE));
				JSFUtilities.executeJavascriptFunction("PF('warningDialog').show()");  
				return;
			}
			
			for (BillingService itBilling : this.billingServiceGroup) {
				exceptionInvoiceGenerationTo=createExceptionInvoice(itBilling);
				listExceptionInvoiceGenerationTo.add(exceptionInvoiceGenerationTo);
			}
		}else{
			exceptionInvoiceGenerationTo=createExceptionInvoice(this.billingService);
			listExceptionInvoiceGenerationTo.add(exceptionInvoiceGenerationTo);
		}
		listExcepTable.addAll(listExceptionInvoiceGenerationTo);
		exceptionInvoiceDataModel=new GenericDataModel<ExceptionInvoiceGenerationTo>(listExcepTable);
		processedExceptionInvoiceGenerationTo.setListExceptionInvoiceGenerationTo(listExceptionInvoiceGenerationTo);
		processedExceptionInvoiceGenerationTo.setIndIntegratedBill(this.inIntegratedBillSelected);
		processedExceptionInvoiceGenerationTo.setExceptionState(InvoiceExceptionStatus.REGISTER.getCode());
		
		
		/**Agrupar por lotes de Excepcion de Facturacion*/
		
		listProcessedExceptionInvoiceGenerationTo.add(processedExceptionInvoiceGenerationTo);
		
		billingScheduleTo=new BillingScheduleTo();
		billingService=new BillingService();
		collectionPeriodSelected=null;

		cleanBillingServiceClustered();
		disableEntityCollectionPanelVisible=Boolean.FALSE;
		disableCollectionPeriodPanelVisible=Boolean.FALSE;
		
		
	}
	
	/**
	 * Creates the exception invoice.
	 *
	 * @param billingService the billing service
	 * @return the exception invoice generation to
	 */
	public ExceptionInvoiceGenerationTo createExceptionInvoice(BillingService billingService){
		
		ExceptionInvoiceGenerationTo exceptionInvoiceGenerationTo= 
				SerializationUtils.clone(this.exceptionInvoiceGenerationTo);
		
		billingScheduleTo.setIndBcbException(GeneralConstants.ZERO_VALUE_INTEGER);
		billingScheduleTo.setDescriptionCollectionPeriod(BillingUtils.
							getParameterDescription(listCollectionPeriod, billingScheduleTo.getCollectionPeriod()));
		setDateToBillingSchedule();
		exceptionInvoiceGenerationTo.setBillingService(new BillingServiceTo(billingService.getIdBillingServicePk()));
		exceptionInvoiceGenerationTo.setBillingSchedule(billingScheduleTo);
		exceptionInvoiceGenerationTo.setExceptionDate(CommonsUtilities.currentDate());
		exceptionInvoiceGenerationTo.setExceptionState(InvoiceExceptionStatus.REGISTER.getCode());
		
		/**Add Description Type Exception*/
		exceptionInvoiceGenerationTo.setDescriptionTypeException(BillingUtils.
							getParameterDescription(listException, exceptionInvoiceGenerationTo.getTypeException()));

		return exceptionInvoiceGenerationTo;
	}
	
	/**
	 * Sets the date to billing schedule.
	 */
	public void setDateToBillingSchedule(){
		
		if(PropertiesConstants.COLLECTION_PERIOD_DAILY_PK.equals(collectionPeriodSelected)){
			billingScheduleTo.setDayOfMonth("*");
		}else if(PropertiesConstants.COLLECTION_PERIOD_WEEKLY_PK.equals(collectionPeriodSelected)){
			
		}else if(PropertiesConstants.COLLECTION_PERIOD_MONTHLY_PK.equals(collectionPeriodSelected)){
			
		}else if(PropertiesConstants.COLLECTION_PERIOD_QUARTERLY_PK.equals(collectionPeriodSelected)){
			
			billingScheduleTo.setDayOfMonth(BillingUtils.getDayToDate(initialDate).toString());
			billingScheduleTo.setMonth(BillingUtils.getMonthToDate(initialDate).toString());
			
		}else if(PropertiesConstants.COLLECTION_PERIOD_YEARLY_PK.equals(collectionPeriodSelected)){
			
			billingScheduleTo.setDayOfMonth(BillingUtils.getDayToDate(initialDate).toString());
			billingScheduleTo.setMonth(BillingUtils.getMonthToDate(initialDate).toString());
			
		}
	}
	
	/**
	 * Si el servicio de facturacion tiene al menos una excepcion, 
	 * solo para los servicios individuales.
	 *
	 * @return the boolean
	 */
	public Boolean isBillingServiceIncluded(){
		
		Boolean haveInclusion=Boolean.FALSE;
		List<ExceptionInvoiceGenerations> listExceptions=null;
		ExceptionInvoiceGenerationTo exceptionInvoiceGenerationTo=new ExceptionInvoiceGenerationTo(this.exceptionInvoiceGenerationTo);
		exceptionInvoiceGenerationTo.setTypeException(PropertiesConstants.INCLUSION_PK);
		/**Facturacion Agrupada*/
		if(PropertiesConstants.YES_IND_INTEGRATED_BILL.equals(inIntegratedBillSelected)){
			haveInclusion=Boolean.TRUE;
		}else{
			exceptionInvoiceGenerationTo.setBillingService(new BillingServiceTo(billingService.getIdBillingServicePk()));
			listExceptions=invoiceExceptionServiceFacade.findInvoiceExceptionByFilter(exceptionInvoiceGenerationTo);
			if(Validations.validateListIsNotNullAndNotEmpty(listExceptions)){
				haveInclusion=Boolean.TRUE;
			}
			
		}
		
		return haveInclusion;
	}
	
	/**
	 * Before add entity collection.
	 */
	@LoggerAuditWeb
	public void beforeAddEntityCollection(){
		Object codigo=this.billingService.getServiceCode();
		
		/**
		 * Verificar que la entidad no se encuentre en la lista temporal
		 */
		if(isRepeatEntityTemp()){
			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_ENTITY_ALREADY_LIST));
			JSFUtilities.executeJavascriptFunction("PF('warningDialog').show()");  
			return;
		}
		executeAction();
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
				, PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_EXCP_INVOICE_ADD,codigo));
		JSFUtilities.executeJavascriptFunction("PF('cnfwAddEntityCollection').show();");
	}
	
	/**
	 * Adds the entity collection.
	 */
	public void addEntityCollection(){
		
		billingEntityExceptionTo=new BillingEntityExceptionTo();
		Integer entityCollection=getEntityCollectionByInd();
				
		billingEntityExceptionTo.setEntityCollectionId(entityCollection);
		
		if(EntityCollectionType.ISSUERS.getCode().equals(entityCollection)){
//			billingEntityExceptionTo.setIssuer(issuer);
			billingEntityExceptionTo.setIssuer(new IssuerTo(issuer.getIdIssuerPk()));
			billingEntityExceptionTo.setDescriptionEntityCode(issuer.getCodeMnemonic());
			billingEntityExceptionTo.setDescriptionEntityCollection(issuer.getBusinessName());
		}else if(EntityCollectionType.PARTICIPANTS.getCode().equals(entityCollection)){
			billingEntityExceptionTo.setIdParticipantPk(participantSelected);
			billingEntityExceptionTo.setDescriptionEntityCode(participantSelected.toString());
			for (Iterator iterator = listClientParticipant.iterator(); iterator.hasNext();) {
				Participant participant = (Participant) iterator.next();
				if(participant.getIdParticipantPk().equals(participantSelected)){
					
					billingEntityExceptionTo.setDescriptionEntityCollection(participant.getDescription());
					break;
				}
				
			}

		}else if(EntityCollectionType.HOLDERS.getCode().equals(entityCollection)){
//			billingEntityExceptionTo.setHolder(holderSelected);
			billingEntityExceptionTo.setHolder(new HolderTo(holderSelected.getIdHolderPk()));
			billingEntityExceptionTo.setDescriptionEntityCode(holderSelected.getDocumentNumber());
			billingEntityExceptionTo.setDescriptionEntityCollection(holderSelected.getFullName());
		}else if(EntityCollectionType.OTHERS.getCode().equals(entityCollection)){
			billingEntityExceptionTo.setDescriptionEntityCode("");
			billingEntityExceptionTo.setDescriptionEntityCollection("");
		}
		exceptionInvoiceGenerationTo.getBillingEntityException().add(billingEntityExceptionTo);
		
		dataModelBillingEntityExceptionTo=new GenericDataModel<>(exceptionInvoiceGenerationTo.getBillingEntityException());
		
		cleanHelperEntityCollection();
	}
	
	
	/**
	 * Gets the entity collection by ind.
	 *
	 * @return the entity collection by ind
	 */
	public Integer getEntityCollectionByInd(){
		
		Integer entityCollection=null;
		
		if(PropertiesConstants.YES_IND_INTEGRATED_BILL.equals(inIntegratedBillSelected)&&
				Validations.validateListIsNotNullAndNotEmpty(billingServiceGroup)){
			
			entityCollection= this.billingServiceGroup.get(0).getEntityCollection();
			
		}else if(Validations.validateIsNotNull(billingService)&&
				Validations.validateIsNotNullAndPositive(billingService.getEntityCollection())){

				entityCollection=billingService.getEntityCollection();

			
		}
		return entityCollection;
	}
	
	/**
	 * Validate type exception.
	 */
	public void validateTypeException(){
		exceptionInvoiceGenerationTo.setIndSelectAll(null);
		collectionPeriodSelected=null;
		collectionPeriodDisable=Boolean.FALSE;
		resetRenderedEntityCollection();
		resetRenderedCollectionPeriod();		
	}
	
	/**
	 * If the entiy Repeat in List.
	 *
	 * @return true, if is repeat entity temp
	 */
	public boolean isRepeatEntityTemp(){
		boolean isRepeat=Boolean.FALSE; 
		
		Integer entityCollection=billingService.getEntityCollection();
		if(EntityCollectionType.ISSUERS.getCode().equals(entityCollection)){
			
			for (BillingEntityExceptionTo entiyException : exceptionInvoiceGenerationTo.getBillingEntityException()) {
				if(entiyException.getIssuer().getIdIssuerPk().equals(issuer.getIdIssuerPk())){
					isRepeat=Boolean.TRUE;
					break;
				}
			}
			
		}else if(EntityCollectionType.PARTICIPANTS.getCode().equals(entityCollection)){
			for (BillingEntityExceptionTo entiyException : exceptionInvoiceGenerationTo.getBillingEntityException()) {
				if(entiyException.getIdParticipantPk().equals(participantSelected)){
					isRepeat=Boolean.TRUE;
					break;
				}
			}
			

		}else if(EntityCollectionType.HOLDERS.getCode().equals(entityCollection)){
			for (BillingEntityExceptionTo entiyException : exceptionInvoiceGenerationTo.getBillingEntityException()) {
				if(entiyException.getHolder().getIdHolderPk().equals(holderSelected.getIdHolderPk())){
					isRepeat=Boolean.TRUE;
					break;
				}
			}
			
		}else if(EntityCollectionType.HOLDERS.getCode().equals(entityCollection)){
			
		}
		
		return isRepeat;
	}
	
	/**
	 * Remove To entity Temporal.
	 *
	 * @param event the event
	 */
	public void entityRemoveTmp(ActionEvent event){
		BillingEntityExceptionTo billingEntityExceptionTo = (BillingEntityExceptionTo) event.getComponent().getAttributes().get("removeName");
		exceptionInvoiceGenerationTo.getBillingEntityException().remove(billingEntityExceptionTo); 
	}
	
	
	/**
	 * Search invoice exception.
	 */
	public void searchInvoiceException(){
		
	}


	
	/**
	 * Load detail exception.
	 *
	 * @param exceptionInvoiceGenerationTo the exception invoice generation to
	 */
	@LoggerAuditWeb
	public void loadDetailException(ExceptionInvoiceGenerationTo exceptionInvoiceGenerationTo) {
		
		exceptionInvoiceSelection=exceptionInvoiceGenerationTo;
		if(PropertiesConstants.INCLUSION_PK.equals(exceptionInvoiceGenerationTo.getTypeException())){
			/**
			 * Verificar o preguntar que si elimina la exclusion no generara facturas para el servicio
			 */
		}
		
		if(PropertiesConstants.RDB_ALL.equals(exceptionInvoiceSelection.getIndSelectAll())){
			
		}else if(PropertiesConstants.RDB_ONE.equals(exceptionInvoiceSelection.getIndSelectAll())){
			listBillingEntityExceptionTo= exceptionInvoiceSelection.getBillingEntityException();
			billingEntityDataModel=new GenericDataModel<>(listBillingEntityExceptionTo);
		}
		
		/*** SHOW ENTITY LIST ***/
		JSFUtilities.executeJavascriptFunction("PF('cnfwFlagConsultMemory').show();");
	}
	
	/**
	 * Back from consult to search.
	 */
	public void backFromConsultToSearch(){
		
		/**  CLOSED MODAL CONSULT  **/
		
		JSFUtilities.executeJavascriptFunction("PF('cnfwFlagConsultMemory').hide();");
		
	}
	
	/**
	 * Show Helper BIlling Service if inIntegratedBillSelected is single and
	 * Hide if grouping.
	 */
	public void updateComponentIndIntegratedBill() {
		
		Integer inIntegratedBill = this.inIntegratedBillSelected;
		this.billingService.setInIntegratedBill(inIntegratedBill);
		
		/**
		 * 
		 */
		cleanExceptionInvoiceBox();
		cleanBillingServiceClustered();
		if (Validations.validateIsNotNull(inIntegratedBill)) {
			if (inIntegratedBill.equals(PropertiesConstants.YES_IND_INTEGRATED_BILL)) {
				
			} else if (inIntegratedBill.equals(PropertiesConstants.NOT_IND_INTEGRATED_BILL)) {
				
			}
		} else {
			
		}

	}
	
	/**
	 * Adds the billing to clustered.
	 */
	public void addBillingToClustered(){
		
		/**
		 * El primer servicio de la lista es mandatorio por la entidad de cobro.
		 * No pueden agruparse servicios con diferente entidad de cobro
		 */
		BillingService billingService=SerializationUtils.clone(this.billingService);
		if(Validations.validateListIsNullOrEmpty(billingServiceGroup)){
								
			billingServiceGroup=new ArrayList<BillingService>();
			entityCollectionGroup=billingService.getEntityCollection();
			billingServiceGroup.add(billingService);
			
			
		}else{
			if(Validations.validateIsNotNullAndPositive(entityCollectionGroup)){
				
				if(isRepeatBillingServiceClustered()){
					//El servicio no puede estar repetido
					showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_ALREADY_LIST));
					JSFUtilities.executeJavascriptFunction("PF('warningDialog').show()");  
					return;
				}
				
				if(!billingService.getEntityCollection().equals(entityCollectionGroup)){
					//Mostrar una alerta que indique que los servicios
					//deben tener la misma entidad de cobro.
					showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SAME_ENTITY_BY_SERVICE));
					JSFUtilities.executeJavascriptFunction("PF('warningDialog').show()");  
					return;
				}
				entityCollectionGroup=billingService.getEntityCollection();
				billingServiceGroup.add(billingService);
			}
		}
		billingServiceDataModel=new GenericDataModel<BillingService>(billingServiceGroup);
		cleanBillingHelper();
	}
	
	/**
	 * Validate If Billing Service Clustered Repeat in List.
	 *
	 * @return true, if is repeat billing service clustered
	 */
	public boolean isRepeatBillingServiceClustered(){
		boolean isRepeat=Boolean.FALSE;
		
		for (BillingService iterService : billingServiceGroup) {
			if(billingService.getServiceCode().equals(iterService.getServiceCode())){
				isRepeat=Boolean.TRUE;
				break;
			}
		}
		
		return isRepeat;
		
	}
	
	
	/**
	 * Remove Service To Clustered.
	 *
	 * @param event the event
	 */
	public void serviceRemoveTmp(ActionEvent event){
		BillingService billingServiceRemove = (BillingService) event.getComponent().getAttributes().get("removeName");
		billingServiceGroup.remove(billingServiceRemove);
	}
	
	/**
	 * Check end month.
	 */
	public void checkEndMonth(){
		
		log.debug("checkEndMonth");
		/*Si esta marcado el check de fin de mes*/
		if(boolEndOfMonth){
			billingScheduleTo.setIndEndOfMonth(BooleanType.YES.getCode());
		}else{
			billingScheduleTo.setIndEndOfMonth(BooleanType.NO.getCode());
		}
		
		
		
	}
	
}
