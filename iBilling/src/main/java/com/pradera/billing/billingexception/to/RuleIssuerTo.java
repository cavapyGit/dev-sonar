package com.pradera.billing.billingexception.to;

import java.io.Serializable;
import java.util.List;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RuleIssuerTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class RuleIssuerTo  implements Serializable{
	
	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2995711057778534353L;
	
	
	/** The business name. */
	private String 				businessName;
	
	/** The issuance tolist. */
	private List<IssuanceTo>    issuanceTolist;
	
//	private Holder				holderSelected;
	
/** The issuance consult. */
//	private Issuance            issuanceConsult; /** use to consult on search */
	private IssuanceTo            issuanceConsult; 
 /**  use to consult on search. */
	
//	private Issuer				issuerSelected;
	private IssuerTo			issuerSelected;
	
	/**  0 : All Issuer  1 : One Issuer. */
	private Integer				indicatorOneOrAll;
	
	
	/**
	 * Instantiates a new rule issuer to.
	 */
	public RuleIssuerTo(){
//		holderSelected = 	new Holder();
		issuerSelected=		new IssuerTo();
	}
	

	/**
	 * Gets the issuance tolist.
	 *
	 * @return the issuance tolist
	 */
	public List<IssuanceTo> getIssuanceTolist() {
		return issuanceTolist;
	}
	
	/**
	 * Sets the issuance tolist.
	 *
	 * @param issuanceTolist the new issuance tolist
	 */
	public void setIssuanceTolist(List<IssuanceTo> issuanceTolist) {
		this.issuanceTolist = issuanceTolist;
	}
	

	/**
	 * Gets the business name.
	 *
	 * @return the business name
	 */
	public String getBusinessName() {
		return businessName;
	}

	/**
	 * Sets the business name.
	 *
	 * @param businessName the new business name
	 */
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}


	/**
	 * Gets the indicator one or all.
	 *
	 * @return the indicator one or all
	 */
	public Integer getIndicatorOneOrAll() {
		return indicatorOneOrAll;
	}


	/**
	 * Sets the indicator one or all.
	 *
	 * @param indicatorOneOrAll the new indicator one or all
	 */
	public void setIndicatorOneOrAll(Integer indicatorOneOrAll) {
		this.indicatorOneOrAll = indicatorOneOrAll;
	}


	/**
	 * Gets the issuance consult.
	 *
	 * @return the issuanceConsult
	 */
	public IssuanceTo getIssuanceConsult() {
		return issuanceConsult;
	}


	/**
	 * Sets the issuance consult.
	 *
	 * @param issuanceConsult the issuanceConsult to set
	 */
	public void setIssuanceConsult(IssuanceTo issuanceConsult) {
		this.issuanceConsult = issuanceConsult;
	}


	/**
	 * Gets the issuer selected.
	 *
	 * @return the issuerSelected
	 */
	public IssuerTo getIssuerSelected() {
		return issuerSelected;
	}


	/**
	 * Sets the issuer selected.
	 *
	 * @param issuerSelected the issuerSelected to set
	 */
	public void setIssuerSelected(IssuerTo issuerSelected) {
		this.issuerSelected = issuerSelected;
	}

//
//	public Holder getHolderSelected() {
//		return holderSelected;
//	}
//
//
//	public void setHolderSelected(Holder holderSelected) {
//		this.holderSelected = holderSelected;
//	}
//
//
//	public Issuance getIssuanceConsult() {
//		return issuanceConsult;
//	}
//
//
//	public void setIssuanceConsult(Issuance issuanceConsult) {
//		this.issuanceConsult = issuanceConsult;
//	}
//
//
//	/**
//	 * @return the issuerSelected
//	 */
//	public Issuer getIssuerSelected() {
//		return issuerSelected;
//	}
//
//
//	/**
//	 * @param issuerSelected the issuerSelected to set
//	 */
//	public void setIssuerSelected(Issuer issuerSelected) {
//		this.issuerSelected = issuerSelected;
//	}
	

 

	
	
	
}
