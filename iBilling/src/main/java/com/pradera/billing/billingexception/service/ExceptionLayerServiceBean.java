package com.pradera.billing.billingexception.service;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.Query;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;
import org.hibernate.exception.SQLGrammarException;

import com.pradera.billing.billingcollection.to.CollectionRecordTo;
import com.pradera.billing.common.facade.BusinessLayerCommonFacade;
import com.pradera.billing.common.utils.BillingUtils;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.security.model.type.UserAccountStateType;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.RateExclusion;
import com.pradera.model.billing.RateInclusion;
import com.pradera.model.billing.ServiceRate;
import com.pradera.model.billing.type.BaseCollectionType;
import com.pradera.model.billing.type.EntityCollectionType;
import com.pradera.model.billing.type.IndexExceptionType;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.process.BusinessProcess;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ExceptionLayerServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ExceptionLayerServiceBean   extends CrudDaoServiceBean{

	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The rate exception service bean. */
	@EJB
	RateExceptionServiceBean 	rateExceptionServiceBean;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade 	generalParametersFacade;

	/** The client rest service. */
	@Inject
	ClientRestService     		clientRestService;

	/** The business layer common facade. */
	@EJB
	BusinessLayerCommonFacade 	businessLayerCommonFacade;
	
	/** The exception layer service bean. */
	@EJB
	ExceptionLayerServiceBean  	exceptionLayerServiceBean;
	
	/** The message configuration. */
	@Inject
	@InjectableResource(location = "Messages_es.properties")
	
	private Properties messageConfiguration;
	
	/**
	 * Filter exception by base collection.
	 *
	 * @param parameters the parameters
	 * @param stringBuilderSqlBase the string builder sql base
	 * @return the string builder
	 * @throws ServiceException the service exception
	 */
	public StringBuilder filterExceptionByBaseCollection(Map parameters, StringBuilder stringBuilderSqlBase) throws ServiceException {
		
		/** getting data for do merging into dynamic query**/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		
		/** getting data for do merging into dynamic query**/
		ServiceRate serviceRate=(ServiceRate) parameters.get(GeneralConstants.SERVICE_RATE);
		 
		/**		Build tringBuilder to return	**/
		StringBuilder strTableComponentBegin = new StringBuilder();
		
		StringBuilder stringBuilderBeginTableExclusion=new StringBuilder();
		/** END Exclusion**/
		StringBuilder stringBuilderEndTableExclusion=new StringBuilder();
		
		/** Declaration dynamics Table Inclusion**/
		StringBuilder stringBuilderBeginTableInclusion=new StringBuilder();
		
		/**	 END Inclusion	**/
		StringBuilder stringBuilderEndTableInclusion=new StringBuilder();
		
		/**	 END strTableComponent	**/
		
		
		/***1*/
		StringBuilder strTableComponentEnd = new StringBuilder();
		if(billingService.getBaseCollection().equals(BaseCollectionType.RECORD_BOOK_ENTRY_PHYSICAL_CUSTODY_USD.getCode())||
				billingService.getBaseCollection().equals(BaseCollectionType.RECORD_BOOK_ENTRY_PHYSICAL_CUSTODY_LOCAL.getCode())||
				billingService.getBaseCollection().equals(BaseCollectionType.RECORD_BOOK_ENTRY_DESMATERIALIZED_CUSTODY.getCode())
//				billingService.getBaseCollection().equals(BaseCollectionType.RECORD_BOOK_ENTRY_SECURITIES_FIXED_INCOME_AFP.getCode())||
		//		billingService.getBaseCollection().equals(BaseCollectionType.RECORD_BOOK_ENTRY_SECURITIES_VARIABLE_INCOME.getCode())||
			//	billingService.getBaseCollection().equals(BaseCollectionType.RECORD_BOOK_ENTRY_SECURITIES_VARIABLE_INCOME_AFP.getCode())
				){
			
			filterAccountAnnotations(stringBuilderBeginTableExclusion, stringBuilderEndTableExclusion,
					parameters, strTableComponentBegin,
					stringBuilderBeginTableInclusion, stringBuilderEndTableInclusion,
					strTableComponentEnd);
			
		/***2*/
		}else if(billingService.getBaseCollection().equals(BaseCollectionType.RECORD_NOT_PLACED_SECURITIES_FIXED_INCOME.getCode())||
				billingService.getBaseCollection().equals(BaseCollectionType.RECORD_NOT_PLACED_SECURITIES_VARIABLE_INCOME.getCode())){
			
			filterNotPlaced(stringBuilderBeginTableExclusion, stringBuilderEndTableExclusion,
					parameters, strTableComponentBegin,
					stringBuilderBeginTableInclusion, stringBuilderEndTableInclusion,
					strTableComponentEnd);

		/***3*/
		}else if(billingService.getBaseCollection().equals(BaseCollectionType.CUSTODY_SECURITIES_PHYSICAL.getCode())||
				billingService.getBaseCollection().equals(BaseCollectionType.CUSTODY_SECURITIES_PHYSICAL_AFP.getCode())||
				billingService.getBaseCollection().equals(BaseCollectionType.RECORD_BOOK_ENTRY_DESMATERIALIZED_CUSTODY.getCode())||
				billingService.getBaseCollection().equals(BaseCollectionType.CUSTODY_SECURITIES_PHYSICAL_PRIVATE_OFFERT_VARIABLE_INCOME.getCode())){
			
			filterCertificatesDeposit(stringBuilderBeginTableExclusion, stringBuilderEndTableExclusion,
					parameters, strTableComponentBegin,
					stringBuilderBeginTableInclusion, stringBuilderEndTableInclusion,
					strTableComponentEnd);
		/***4*/
		}else if(billingService.getBaseCollection().equals(BaseCollectionType.SETTLEMENT_SECURITIES_TRANSACTIONS_FIXED_INCOME.getCode())||
				billingService.getBaseCollection().equals(BaseCollectionType.SETTLEMENT_SECURITIES_TRANSACTIONS_VARIABLE_INCOME.getCode())
				){
			
			filterMovementSettlementWithMechanismByEntityCollection(stringBuilderBeginTableExclusion, stringBuilderEndTableExclusion,
										parameters, strTableComponentBegin,
										stringBuilderBeginTableInclusion, stringBuilderEndTableInclusion,
										strTableComponentEnd);
		// 9 Y 10
		}else if(billingService.getBaseCollection().equals(BaseCollectionType.DIRECT_PARTICIPANT.getCode())
				|| billingService.getBaseCollection().equals(BaseCollectionType.INDIRECT_PARTICIPANT.getCode())){			
			filterRegisteredMatrixAccount(stringBuilderBeginTableExclusion, stringBuilderEndTableExclusion,
										strTableComponentBegin,
										stringBuilderBeginTableInclusion, stringBuilderEndTableInclusion,
										strTableComponentEnd);		
		}
		/***6  22*/
		else if(billingService.getBaseCollection().equals(BaseCollectionType.ACCOUNT_MAINTENANCE_ISSUER.getCode())	||
				billingService.getBaseCollection().equals(BaseCollectionType.CREATION_ISIN_CODE.getCode())){
			
			filterRegisteredMatrixIssuer(stringBuilderBeginTableExclusion, stringBuilderEndTableExclusion,
					parameters, strTableComponentBegin,
					stringBuilderBeginTableInclusion, stringBuilderEndTableInclusion,
					strTableComponentEnd);
		}
		/**7 *
		else if(billingService.getBaseCollection().equals(BaseCollectionType.CREACION_CUI.getCode())){
			
			filterRegisteredHolder(stringBuilderBeginTableExclusion, stringBuilderEndTableExclusion,
					strTableComponentBegin,
					stringBuilderBeginTableInclusion, stringBuilderEndTableInclusion,
					strTableComponentEnd);
		}
		
		/**   8   */
		else if(billingService.getBaseCollection().equals(BaseCollectionType.TRANSFERS_SECURITIES_SIRTEX_FIXED_INCOME.getCode())||
				billingService.getBaseCollection().equals(BaseCollectionType.TRANSFERS_SECURITIES_SIRTEX_VARIABLE_INCOME.getCode())){
			
			filterSecuritiesSirtex(stringBuilderBeginTableExclusion, stringBuilderEndTableExclusion,
					parameters, strTableComponentBegin,
					stringBuilderBeginTableInclusion, stringBuilderEndTableInclusion,
					strTableComponentEnd);
		}
		/**	MOVEMENT*/
		/**  	9	*/
		else if(
				billingService.getBaseCollection().equals(BaseCollectionType.INDIRECT_PARTICIPANT.getCode())||
				billingService.getBaseCollection().equals(BaseCollectionType.SETTLEMENT_OTC_TRANSACTIONS_VARIABLE_INCOME.getCode())){			
			
			filterSecuritiesMovement(stringBuilderBeginTableExclusion, stringBuilderEndTableExclusion,
					parameters, strTableComponentBegin,
					stringBuilderBeginTableInclusion, stringBuilderEndTableInclusion,
					strTableComponentEnd);

		}
		
		/**10 EMISONES DESMATERIALIZADAS*/
		else if(BaseCollectionType.RECORD_SECURITIES_DEMATERIALIZED_DPF.getCode().equals(billingService.getBaseCollection())||
				billingService.getBaseCollection().equals(BaseCollectionType.RECORD_SECURITIES_DEMATERIALIZED_FIXED_INCOME.getCode())||
				billingService.getBaseCollection().equals(BaseCollectionType.RECORD_SECURITIES_DEMATERIALIZED_VARIABLE_INCOME.getCode())){			
			
			filterRegisteredIssuance(stringBuilderBeginTableExclusion, stringBuilderEndTableExclusion,
					strTableComponentBegin,
					stringBuilderBeginTableInclusion, stringBuilderEndTableInclusion,
					strTableComponentEnd);

		}
		
		/**11 CAMBIO DE TITULARIDAD 
		 * SOLICITUDES_CAMBIO_TITULARIDAD
		 * */
		else if(
				billingService.getBaseCollection().equals(BaseCollectionType.REQUEST_CHANGE_OWNERSHIP_FIXED_INCOME.getCode())||
				billingService.getBaseCollection().equals(BaseCollectionType.REQUEST_CHANGE_OWNERSHIP_VARIABLE_INCOME.getCode())){			
			
			filterChangeOwnerShip(stringBuilderBeginTableExclusion, stringBuilderEndTableExclusion,
					parameters, strTableComponentBegin,
					stringBuilderBeginTableInclusion, stringBuilderEndTableInclusion,
					strTableComponentEnd);

		}
		
		/**  12		REGISTRO_EVENTOS_CORPORATIVOS	**/
		else if(
				billingService.getBaseCollection().equals(BaseCollectionType.RECORD_CORPORATIVE_EVENTS.getCode())){

			filterCorporativeIssuer(stringBuilderBeginTableExclusion, stringBuilderEndTableExclusion,
													strTableComponentBegin,
													stringBuilderBeginTableInclusion, stringBuilderEndTableInclusion,
													strTableComponentEnd);
			
		}
		
		/** 15 EMISION_CERTIFICADOS_ACREDITACION  */
		else if(billingService.getBaseCollection().equals(BaseCollectionType.ISSUANCE_CAT.getCode())){
			
			findCertificateAccreditation(stringBuilderBeginTableExclusion, stringBuilderEndTableExclusion,
					parameters,strTableComponentBegin,
					stringBuilderBeginTableInclusion, stringBuilderEndTableInclusion,
					strTableComponentEnd);
		}
		
		/** 16 SERVICIO_AGENTE_PAGADOR  */
		else if(billingService.getBaseCollection().equals(BaseCollectionType.PAYING_AGENT.getCode())){
			
			findPayingAgent(stringBuilderBeginTableExclusion, stringBuilderEndTableExclusion,
					parameters,strTableComponentBegin,
					stringBuilderBeginTableInclusion, stringBuilderEndTableInclusion,
					strTableComponentEnd);
		}
		
		/** 17 INSCRIPCION_LEVANTAMIENTO_MEDIDAS_PRECAUTORIAS */
		else if(BaseCollectionType.REGISTRATION_PRECAUTIONARY_MEASURES.getCode().equals(billingService.getBaseCollection())||
				BaseCollectionType.LIFTING_PRECAUTIONARY_MEASURES.getCode().equals(billingService.getBaseCollection())){
			
			findMovementPawnBlockAffectation(stringBuilderBeginTableExclusion, stringBuilderEndTableExclusion,
					parameters,strTableComponentBegin,
					stringBuilderBeginTableInclusion, stringBuilderEndTableInclusion,
					strTableComponentEnd);
		}
		/** 18  */
		else if(
				billingService.getBaseCollection().equals(BaseCollectionType.TRANSACTION_CHARGE.getCode())||
				billingService.getBaseCollection().equals(BaseCollectionType.REMATERIALIZATION_OR_REVERSAL_ACCOUNT_ENTRIES_VARIABLE_INCOME.getCode())){
			
			filterRematerializationAnnotations(stringBuilderBeginTableExclusion, stringBuilderEndTableExclusion,
					parameters,strTableComponentBegin,
					stringBuilderBeginTableInclusion, stringBuilderEndTableInclusion,
					strTableComponentEnd);
		}

		
		
		/**	CALL EXCLUSION	**/ 
		bodyExclusion(stringBuilderBeginTableExclusion, stringBuilderSqlBase, 
					  stringBuilderEndTableExclusion, strTableComponentBegin,
					  parameters);
		
		
		/**	CALL INCLUSION	**/ 
		bodyInclusion(stringBuilderBeginTableInclusion, stringBuilderSqlBase ,
				      stringBuilderEndTableInclusion, strTableComponentBegin,
				      parameters);
		
		
		strTableComponentBegin.append(strTableComponentEnd.toString());
		
		return strTableComponentBegin;
		
	}
	
	
	
	
	
	
	/**
	 * EDV 1
	 * REGISTRO_VALORES_ANOTACIONES_CUENTA_R_FIJA
	 * REGISTRO_VALORES_ANOTACIONES_CUENTA_R_VARIABLE
	 *  REGISTRO DE VALORES DE ANOTACIONES EN CUENTA *.
	 *
	 * @param stringBuilderBeginTableExclusion the string builder begin table exclusion
	 * @param stringBuilderEndTableExclusion the string builder end table exclusion
	 * @param parameters the parameters
	 * @param strTableComponentBegin the str table component begin
	 * @param stringBuilderBeginTableInclusion the string builder begin table inclusion
	 * @param stringBuilderEndTableInclusion the string builder end table inclusion
	 * @param strTableComponentEnd the str table component end
	 * @return the string builder
	 * @throws ServiceException the service exception
	 */
	public StringBuilder filterAccountAnnotations(StringBuilder stringBuilderBeginTableExclusion, StringBuilder stringBuilderEndTableExclusion,
				 Map parameters, StringBuilder strTableComponentBegin,
				 StringBuilder stringBuilderBeginTableInclusion,
				 StringBuilder stringBuilderEndTableInclusion,
				 StringBuilder strTableComponentEnd) throws ServiceException{		
		/** Declaration dynamics Table Component**/
		/** getting data for do merging into dynamic query**/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		
		String strEntityCollection="";
		if(Validations.validateIsNotNullAndNotEmpty(billingService.getEntityCollection()) ){
			Integer entityCollection = billingService.getEntityCollection();
			if (entityCollection.equals(EntityCollectionType.HOLDERS.getCode())){
				strEntityCollection= " T.ID_HOLDER_FK  ";
			}else if (entityCollection.equals(EntityCollectionType.ISSUERS.getCode())){
				strEntityCollection= " T.ID_ISSUER_FK  ";
			}else if(entityCollection.equals(EntityCollectionType.PARTICIPANTS.getCode())){
				strEntityCollection= " T.ID_PARTICIPANT_FK  ";
			}
		}

		strTableComponentBegin.append("  SELECT ").append(strEntityCollection).append(" AS ENTITY ,    ")
							   .append(" 	SUM(T.TOTAL_BALANCE*T.NOMINAL_VALUE), ")
							   .append("     T.CURRENCY, T.ID_HOLDER_FK, T.ID_SECURITY_CODE_FK,   ")
							   .append("		T.SECURITY_CLASS, T.ID_HOLDER_ACCOUNT_FK, 	  ")
							   .append("  T.CARTERA_PROPIA")
						   	   .append("    FROM 	( ");
		
		strTableComponentEnd.append(" ) T   ");
		strTableComponentEnd.append(" GROUP BY").append(strEntityCollection).append(" ,    ").
							 append("	T.CURRENCY,  T.ID_HOLDER_FK, 	T.ID_SECURITY_CODE_FK, ").
							 append("   T.SECURITY_CLASS, T.ID_HOLDER_ACCOUNT_FK,   " )
							 .append("  T.CARTERA_PROPIA");
		strTableComponentEnd.append("  ORDER BY ").append(strEntityCollection).append(", T.CURRENCY,  T.ID_HOLDER_FK, ").
							 append("  T.ID_HOLDER_FK,	T.SECURITY_CLASS ");
		
		/** Declaration dynamics Table Exclusion**/
		stringBuilderBeginTableExclusion.append(" SELECT TE.ID_PARTICIPANT_FK ,       ");
		stringBuilderBeginTableExclusion.append("        TE.ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.SECURITY_CLASS , ");
		stringBuilderBeginTableExclusion.append(" 		 TE.ID_HOLDER_ACCOUNT_FK , ");
		stringBuilderBeginTableExclusion.append("        TE.ID_HOLDER_FK , ");
		stringBuilderBeginTableExclusion.append("        TE.ID_ISSUER_FK ,");
		stringBuilderBeginTableExclusion.append(" 		 '' ID_ISSUANCE_CODE_FK, ");
		stringBuilderBeginTableExclusion.append(" 		 TE.CURRENCY ,   ");
		stringBuilderBeginTableExclusion.append(" 		 TE.CARTERA_PROPIA ,   ");
		stringBuilderBeginTableExclusion.append(" 		 TE.TOTAL_BALANCE, ");
		stringBuilderBeginTableExclusion.append(" 		 TE.NOMINAL_VALUE ");
		stringBuilderBeginTableExclusion.append("        FROM ( ");		
		
		stringBuilderEndTableExclusion.append(" ) TE  WHERE  (1=1)     ");
		
		
		/** Declaration dynamics Table Inclusion**/
		stringBuilderBeginTableInclusion.append(" SELECT TI.ID_PARTICIPANT_FK ,  ");
		stringBuilderBeginTableInclusion.append("        TI.ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.SECURITY_CLASS , ");
		stringBuilderBeginTableInclusion.append(" 		 TI.ID_HOLDER_ACCOUNT_FK , ");
		stringBuilderBeginTableInclusion.append("        TI.ID_HOLDER_FK , ");
		stringBuilderBeginTableInclusion.append("        TI.ID_ISSUER_FK , ");
		stringBuilderBeginTableInclusion.append(" 		 '' ID_ISSUANCE_CODE_FK, ");
		stringBuilderBeginTableInclusion.append(" 		 TI.CURRENCY ,   ");
		stringBuilderBeginTableInclusion.append(" 		 TI.CARTERA_PROPIA ,   ");
		stringBuilderBeginTableInclusion.append(" 		 TI.TOTAL_BALANCE, ");
		stringBuilderBeginTableInclusion.append(" 		 TI.NOMINAL_VALUE ");
		stringBuilderBeginTableInclusion.append("        FROM ( ");		
		
		stringBuilderEndTableInclusion.append(" ) TI  WHERE  (1=1)     "); 
		
		
		return strTableComponentBegin;		
		/**		FIN		**/	
	}
	
	
	/**
	 * EDV 2 
	 * 	REGISTRO_VALORES_NO_COLOCADOS_R_FIJA
	 * 	REGISTRO_VALORES_NO_COLOCADOS_R_VARIABLE
	 * MANTENIMIENTO DE VALORES NO COLOCADOS *.
	 *
	 * @param stringBuilderBeginTableExclusion the string builder begin table exclusion
	 * @param stringBuilderEndTableExclusion the string builder end table exclusion
	 * @param parameters the parameters
	 * @param strTableComponentBegin the str table component begin
	 * @param stringBuilderBeginTableInclusion the string builder begin table inclusion
	 * @param stringBuilderEndTableInclusion the string builder end table inclusion
	 * @param strTableComponentEnd the str table component end
	 * @return the string builder
	 * @throws ServiceException the service exception
	 */
	public StringBuilder filterNotPlaced(StringBuilder stringBuilderBeginTableExclusion, StringBuilder stringBuilderEndTableExclusion,
				 Map parameters,  StringBuilder strTableComponentBegin,
				 StringBuilder stringBuilderBeginTableInclusion,
				 StringBuilder stringBuilderEndTableInclusion,
				 StringBuilder strTableComponentEnd) throws ServiceException{
		
		/** getting data for do merging into dynamic query**/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		
		String strEntityCollection="";
		if(Validations.validateIsNotNullAndNotEmpty(billingService.getEntityCollection()) ){
			Integer entityCollection = billingService.getEntityCollection();
			if (entityCollection.equals(EntityCollectionType.HOLDERS.getCode())){
				strEntityCollection= " T.ID_HOLDER_FK ";
			}else if (entityCollection.equals(EntityCollectionType.ISSUERS.getCode())){
				strEntityCollection= " T.ID_ISSUER_FK ";
			}else if(entityCollection.equals(EntityCollectionType.PARTICIPANTS.getCode())){
				strEntityCollection= " T.ID_PARTICIPANT_FK ";
			}
		}
		
		
		/** Declaration dynamics Table Component**/
		strTableComponentBegin.append("  SELECT ").append(strEntityCollection).append(" ,T.NOT_PLACED, T.CURRENCY,     ");
		strTableComponentBegin.append("    		 T.SECURITY_CLASS,   T.SECURITY_CODE, ");
		strTableComponentBegin.append("    		 T.ID_ISSUANCE_CODE_FK, T.ID_ISSUER_FK ");
		strTableComponentBegin.append("    FROM 	( ");
		
		strTableComponentEnd.append(" ) T  ");
		strTableComponentEnd.append("ORDER BY ").append(strEntityCollection);
		
		
		/** Declaration dynamics Table Exclusion**/
		stringBuilderBeginTableExclusion.append(" SELECT    ");
		stringBuilderBeginTableExclusion.append("        TE.SECURITY_CLASS , ");
		stringBuilderBeginTableExclusion.append("        TE.ID_ISSUER_FK ,");
		stringBuilderBeginTableExclusion.append(" 		 TE.ID_ISSUANCE_CODE_FK, ");
		stringBuilderBeginTableExclusion.append(" 		 TE.CURRENCY ,   ");
		stringBuilderBeginTableExclusion.append("        TE.SECURITY_CODE ,");
		stringBuilderBeginTableExclusion.append(" 		 TE.ID_PARTICIPANT_FK ,   ");
		stringBuilderBeginTableExclusion.append(" 		 TE.NOT_PLACED ");
		stringBuilderBeginTableExclusion.append("        FROM ( ");		
		
		stringBuilderEndTableExclusion.append(" ) TE  WHERE  (1=1)     ");
		
		
		/** Declaration dynamics Table Inclusion**/
		stringBuilderBeginTableInclusion.append(" SELECT   ");
		stringBuilderBeginTableInclusion.append("        TI.SECURITY_CLASS , ");
		stringBuilderBeginTableInclusion.append("        TI.ID_ISSUER_FK , ");
		stringBuilderBeginTableInclusion.append(" 		 TI.ID_ISSUANCE_CODE_FK, ");
		stringBuilderBeginTableInclusion.append(" 		 TI.CURRENCY ,   ");
		stringBuilderBeginTableInclusion.append("        TI.SECURITY_CODE ,");
		stringBuilderBeginTableInclusion.append(" 		 TI.ID_PARTICIPANT_FK ,   ");
		stringBuilderBeginTableInclusion.append(" 		 TI.NOT_PLACED ");
		stringBuilderBeginTableInclusion.append("        FROM ( ");		
		
		stringBuilderEndTableInclusion.append(" ) TI  WHERE  (1=1)     "); 
		
		
		return strTableComponentBegin;		
		/**		FIN		**/	
	}
	
	
	
	/**
	 * EDV 3 
	 * GUARDA_Y_CUSTODIA_DE_VALORES_FISICOS_R_FIJA
	 * GUARDA_Y_CUSTODIA_DE_VALORES_FISICOS_R_VARIABLE
	 * EDV 24
	 * CUSTODY_SECURITIES_PHYSICAL_PRIVATE_OFFERT_FIXED_INCOME
	 * CUSTODY_SECURITIES_PHYSICAL_PRIVATE_OFFERT_VARIABLE_INCOME
	 * CUSTODIA DE VALORES FISICOS *.
	 *
	 * @param stringBuilderBeginTableExclusion the string builder begin table exclusion
	 * @param stringBuilderEndTableExclusion the string builder end table exclusion
	 * @param parameters the parameters
	 * @param strTableComponentBegin the str table component begin
	 * @param stringBuilderBeginTableInclusion the string builder begin table inclusion
	 * @param stringBuilderEndTableInclusion the string builder end table inclusion
	 * @param strTableComponentEnd the str table component end
	 * @return the string builder
	 * @throws ServiceException the service exception
	 */
	public StringBuilder filterCertificatesDeposit(StringBuilder stringBuilderBeginTableExclusion, StringBuilder stringBuilderEndTableExclusion,
			 	 Map parameters,  StringBuilder strTableComponentBegin,
				 StringBuilder stringBuilderBeginTableInclusion,
				 StringBuilder stringBuilderEndTableInclusion,
				 StringBuilder strTableComponentEnd) throws ServiceException{
		
		/** getting data for do merging into dynamic query**/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		
		String strEntityCollection="";
		if(Validations.validateIsNotNullAndNotEmpty(billingService.getEntityCollection()) ){
			Integer entityCollection = billingService.getEntityCollection();
			if (entityCollection.equals(EntityCollectionType.HOLDERS.getCode())){
				strEntityCollection= " T.ID_HOLDER_FK ";
			}else if (entityCollection.equals(EntityCollectionType.ISSUERS.getCode())){
				strEntityCollection= " T.ID_ISSUER_FK ";
			}else if(entityCollection.equals(EntityCollectionType.PARTICIPANTS.getCode())){
				strEntityCollection= " T.ID_PARTICIPANT_FK ";
			}
		}
		
		/** Declaration dynamics Table Component**/  
		strTableComponentBegin.append("  SELECT ").append(strEntityCollection).append(" AS ENTITY,   ").
							   append("  SUM(T.TOTAL_BALANCE*T.NOMINAL_VALUE), ").
							   append("  T.CURRENCY, ").
							   append("  T.SECURITY_CLASS, ").
							   append("  T.ID_SECURITY_CODE_FK,  ").
							   append("  T.ID_HOLDER_FK, T.ID_HOLDER_ACCOUNT_FK, ")
							   .append("  T.NEMONICO_HOLDER, T.ACT_ECO_PAR, T.CARTERA_PROPIA")
							   .append("    FROM 	( ");
		
		strTableComponentEnd.append("	) T");
		strTableComponentEnd.append(" GROUP BY  ").append(strEntityCollection).append(" ,  T.CURRENCY,  ").
							 append(" 	T.SECURITY_CLASS, T.ID_SECURITY_CODE_FK,  ").
							 append("   T.ID_HOLDER_FK, T.ID_HOLDER_ACCOUNT_FK, ")
							 .append("  T.NEMONICO_HOLDER, T.ACT_ECO_PAR, T.CARTERA_PROPIA ");
		strTableComponentEnd.append(" ORDER BY  ").append(strEntityCollection).append(" ,  T.CURRENCY,  ").
							 append(" 	T.SECURITY_CLASS, T.ID_SECURITY_CODE_FK ");
		
		
		/** Declaration dynamics Table Exclusion**/
		stringBuilderBeginTableExclusion.append(" SELECT TE.ID_PARTICIPANT_FK ,       ");
		stringBuilderBeginTableExclusion.append("        TE.ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.SECURITY_CLASS , ");
		stringBuilderBeginTableExclusion.append(" 		 TE.ID_HOLDER_ACCOUNT_FK , ");
		stringBuilderBeginTableExclusion.append("        TE.ID_HOLDER_FK , ");
		stringBuilderBeginTableExclusion.append("        TE.ID_ISSUER_FK ,");
		stringBuilderBeginTableExclusion.append(" 		 '' ID_ISSUANCE_CODE_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.NEMONICO_HOLDER , ");
		stringBuilderBeginTableExclusion.append(" 		 TE.CARTERA_PROPIA ,   ");
		stringBuilderBeginTableExclusion.append(" 		 TE.ACT_ECO_PAR ,   ");
		stringBuilderBeginTableExclusion.append(" 		 TE.CURRENCY ,   ");
		stringBuilderBeginTableExclusion.append(" 		 TE.NOMINAL_VALUE, ");
		stringBuilderBeginTableExclusion.append(" 		 TE.TOTAL_BALANCE ");
		stringBuilderBeginTableExclusion.append("        FROM ( ");		
		
		stringBuilderEndTableExclusion.append(" ) TE  WHERE  (1=1)     ");
		
		
		/** Declaration dynamics Table Inclusion**/
		stringBuilderBeginTableInclusion.append(" SELECT TI.ID_PARTICIPANT_FK ,  ");
		stringBuilderBeginTableInclusion.append("        TI.ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.SECURITY_CLASS , ");
		stringBuilderBeginTableInclusion.append(" 		 TI. ID_HOLDER_ACCOUNT_FK , ");
		stringBuilderBeginTableInclusion.append("        TI.ID_HOLDER_FK , ");
		stringBuilderBeginTableInclusion.append("        TI.ID_ISSUER_FK , ");
		stringBuilderBeginTableInclusion.append(" 		 '' ID_ISSUANCE_CODE_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.NEMONICO_HOLDER , ");
		stringBuilderBeginTableInclusion.append(" 		 TI.CARTERA_PROPIA ,   ");
		stringBuilderBeginTableInclusion.append(" 		 TI.ACT_ECO_PAR ,   ");
		stringBuilderBeginTableInclusion.append(" 		 TI.CURRENCY ,   ");
		stringBuilderBeginTableInclusion.append(" 		 TI.NOMINAL_VALUE, ");
		stringBuilderBeginTableInclusion.append(" 		 TI.TOTAL_BALANCE ");
		stringBuilderBeginTableInclusion.append("        FROM ( ");		
		
		stringBuilderEndTableInclusion.append(" ) TI  WHERE  (1=1)     "); 
		
		
		return strTableComponentBegin;		
		/**		FIN		**/	
		}
	
	
	/**
	 *  4 LIQUIDACION_OPERACIONES_BURSATILES_R_FIJA*.
	 *
	 * @param stringBuilderBeginTableExclusion the string builder begin table exclusion
	 * @param stringBuilderEndTableExclusion the string builder end table exclusion
	 * @param parameters the parameters
	 * @param strTableComponentBegin the str table component begin
	 * @param stringBuilderBeginTableInclusion the string builder begin table inclusion
	 * @param stringBuilderEndTableInclusion the string builder end table inclusion
	 * @param strTableComponentEnd the str table component end
	 * @return the string builder
	 */
	public StringBuilder filterMovementSettlementWithMechanismByEntityCollection(StringBuilder stringBuilderBeginTableExclusion, StringBuilder stringBuilderEndTableExclusion,
																					Map parameters, StringBuilder strTableComponentBegin,
																					StringBuilder stringBuilderBeginTableInclusion,
																					StringBuilder stringBuilderEndTableInclusion,
																					StringBuilder strTableComponentEnd){
																		
		
		/** getting data for do merging into dynamic query**/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		
		/** getting data for do merging into dynamic query**/
		ServiceRate serviceRate=(ServiceRate) parameters.get(GeneralConstants.SERVICE_RATE);
		
		/** Declaration dynamics Table Component**/
		String strEntityCollection="";
		if(Validations.validateIsNotNullAndNotEmpty(billingService.getEntityCollection()) ){
			Integer entityCollection = billingService.getEntityCollection();
			if (entityCollection.equals(EntityCollectionType.HOLDERS.getCode())){
				strEntityCollection= " T.ID_HOLDER_FK ";
			}else if (entityCollection.equals(EntityCollectionType.ISSUERS.getCode())){
				strEntityCollection= " T.ID_ISSUER_FK ";
			}else if(entityCollection.equals(EntityCollectionType.PARTICIPANTS.getCode())){
				strEntityCollection= " T.ID_PARTICIPANT_FK ";
			}
		}
		
 		
		strTableComponentBegin.append(" SELECT ").append(strEntityCollection).append(" ,T.ID_SECURITY_CODE_FK , ");
		strTableComponentBegin.append(" T.ID_MOVEMENT_TYPE_FK, ");
		strTableComponentBegin.append(" T.CURRENCY, ");
		strTableComponentBegin.append(" T.MOVEMENT_DATE, ");
		strTableComponentBegin.append(" T.OPERATION_PRICE, ");
		strTableComponentBegin.append(" T.MOVEMENT_QUANTITY, ");
		strTableComponentBegin.append(" T.OPERATION_DATE, T.OPERATION_NUMBER, ");
		strTableComponentBegin.append(" T.ID_SOURCE_PARTICIPANT_FK, T.ID_TARGET_PARTICIPANT_FK, ");
		strTableComponentBegin.append(" T.ID_HOLDER_ACCOUNT_FK , T.ID_HOLDER_FK, T.SECURITY_CLASS, ");
		strTableComponentBegin.append(" T.ID_NEGOTIATION_MODALITY_FK,");
		strTableComponentBegin.append(" T.BALLOT_SEQUENTIAL AS BALLOT_SEQUENTIAL,");
		strTableComponentBegin.append(" T.ROLE, ")
								.append("  T.NEMONICO_HOLDER, T.ACT_ECO_PAR, T.CARTERA_PROPIA");
		strTableComponentBegin.append("  FROM 	(");
		strTableComponentEnd.append(" ) T ORDER BY ").append(strEntityCollection).append(" , T.ID_SECURITY_CODE_FK , T.ID_MOVEMENT_TYPE_FK   ");
	
		
		/** Declaration dynamics Table Exclusion**/
		stringBuilderBeginTableExclusion.append(" SELECT TE.ID_HOLDER_ACCOUNT_FK ,  ");
		stringBuilderBeginTableExclusion.append("       TE.ID_HOLDER_FK , ");
		stringBuilderBeginTableExclusion.append("       TE.ID_PARTICIPANT_FK , ");
		stringBuilderBeginTableExclusion.append(" 		TE.ID_SECURITY_CODE_FK , ");
		stringBuilderBeginTableExclusion.append("       TE.ID_ISSUER_FK, ");
		stringBuilderBeginTableExclusion.append("       TE.SECURITY_CLASS,");
		stringBuilderBeginTableExclusion.append(" 		TE.ID_MOVEMENT_TYPE_FK ,");
		stringBuilderBeginTableExclusion.append(" 		TE.CURRENCY , ");
		stringBuilderBeginTableExclusion.append(" 		TE.MOVEMENT_DATE , ");
		stringBuilderBeginTableExclusion.append(" 		TE.OPERATION_PRICE , ");
		stringBuilderBeginTableExclusion.append(" 		TE.MOVEMENT_QUANTITY,  ");		
		stringBuilderBeginTableExclusion.append(" 		TE.ID_ISSUANCE_CODE_FK, ");
		stringBuilderBeginTableExclusion.append(" 		TE.OPERATION_DATE  ,  TE.OPERATION_NUMBER,  ");
		stringBuilderBeginTableExclusion.append(" 		TE.ID_SOURCE_PARTICIPANT_FK , TE.ID_TARGET_PARTICIPANT_FK, ");
		stringBuilderBeginTableExclusion.append("		TE.ID_NEGOTIATION_MODALITY_FK,	");
		stringBuilderBeginTableExclusion.append("		TE.BALLOT_SEQUENTIAL, ");
		stringBuilderBeginTableExclusion.append("		TE.ROLE, ");
		stringBuilderBeginTableExclusion.append("        TE.NEMONICO_HOLDER , ");
		stringBuilderBeginTableExclusion.append(" 		 TE.CARTERA_PROPIA ,   ");
		stringBuilderBeginTableExclusion.append(" 		 TE.ACT_ECO_PAR ,   ");
		stringBuilderBeginTableExclusion.append("		TE.ID_HOLDER_ACCOUNT_MOVEMENT_PK ");
		stringBuilderBeginTableExclusion.append("        from ( ");		
		
		stringBuilderEndTableExclusion.append(" ) TE  where  (1=1)     ");
		
		
		/** Declaration dynamics Table Inclusion**/
		stringBuilderBeginTableInclusion.append(" SELECT TI.ID_HOLDER_ACCOUNT_FK ,  ");
		stringBuilderBeginTableInclusion.append("       TI.ID_HOLDER_FK , ");
		stringBuilderBeginTableInclusion.append("       TI.ID_PARTICIPANT_FK , ");
		stringBuilderBeginTableInclusion.append(" 		TI.ID_SECURITY_CODE_FK , ");
		stringBuilderBeginTableInclusion.append("       TI.ID_ISSUER_FK, ");
		stringBuilderBeginTableInclusion.append("       TI.SECURITY_CLASS,");
		stringBuilderBeginTableInclusion.append(" 		TI.ID_MOVEMENT_TYPE_FK ,");
		stringBuilderBeginTableInclusion.append(" 		TI.CURRENCY , ");
		stringBuilderBeginTableInclusion.append(" 		TI.MOVEMENT_DATE , ");
		stringBuilderBeginTableInclusion.append(" 		TI.OPERATION_PRICE , ");
		stringBuilderBeginTableInclusion.append(" 		TI.MOVEMENT_QUANTITY,  ");	
		stringBuilderBeginTableInclusion.append(" 		TI.ID_ISSUANCE_CODE_FK, ");		
		stringBuilderBeginTableInclusion.append(" 		TI.OPERATION_DATE  ,  TI.OPERATION_NUMBER,  ");
		stringBuilderBeginTableInclusion.append(" 		TI.ID_SOURCE_PARTICIPANT_FK , TI.ID_TARGET_PARTICIPANT_FK, ");
		stringBuilderBeginTableInclusion.append("       TI.ID_NEGOTIATION_MODALITY_FK, ");
		stringBuilderBeginTableInclusion.append("		TI.BALLOT_SEQUENTIAL, ");
		stringBuilderBeginTableInclusion.append("		TI.ROLE,");
		stringBuilderBeginTableInclusion.append("        TI.NEMONICO_HOLDER , ");
		stringBuilderBeginTableInclusion.append(" 		 TI.CARTERA_PROPIA ,   ");
		stringBuilderBeginTableInclusion.append(" 		 TI.ACT_ECO_PAR ,   ");
		stringBuilderBeginTableInclusion.append("		TI.ID_HOLDER_ACCOUNT_MOVEMENT_PK");
		stringBuilderBeginTableInclusion.append("        from ( ");		

		stringBuilderEndTableInclusion.append(" ) TI  where  (1=1)     ");
		
		
		return strTableComponentBegin;
		/**		FIN		**/	
	}
	
	/**
	 *  EDV 5 MATRIX *.
	 *
	 * @param stringBuilderBeginTableExclusion the string builder begin table exclusion
	 * @param stringBuilderEndTableExclusion the string builder end table exclusion
	 * @param strTableComponentBegin the str table component begin
	 * @param stringBuilderBeginTableInclusion the string builder begin table inclusion
	 * @param stringBuilderEndTableInclusion the string builder end table inclusion
	 * @param strTableComponentEnd the str table component end
	 * @return the string builder
	 * @throws ServiceException the service exception
	 */
	public StringBuilder filterRegisteredMatrixAccount(StringBuilder stringBuilderBeginTableExclusion, StringBuilder stringBuilderEndTableExclusion,
													 StringBuilder strTableComponentBegin,
													 StringBuilder stringBuilderBeginTableInclusion,
													 StringBuilder stringBuilderEndTableInclusion,
													 StringBuilder strTableComponentEnd) throws ServiceException{		
		
		/** Declaration dynamics Table Component**/
		strTableComponentBegin.append(" SELECT T.ID_PARTICIPANT_FK    FROM 	( ");
		
		strTableComponentEnd.append(" ) T  ");
		
		
		/** Declaration dynamics Table Exclusion**/
		stringBuilderBeginTableExclusion.append(" SELECT TE.ID_PARTICIPANT_FK ,       ");
		stringBuilderBeginTableExclusion.append("        '' ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableExclusion.append("        '' SECURITY_CLASS , ");
		stringBuilderBeginTableExclusion.append(" 		  '' ID_HOLDER_ACCOUNT_FK , ");
		stringBuilderBeginTableExclusion.append("        '' ID_HOLDER_FK , ");
		stringBuilderBeginTableExclusion.append("        '' ID_ISSUER_FK ,");
		stringBuilderBeginTableExclusion.append(" 		  '' ID_ISSUANCE_CODE_FK ");
		stringBuilderBeginTableExclusion.append("        FROM ( ");		

		stringBuilderEndTableExclusion.append(" ) TE  WHERE  (1=1)     ");
		
		
		/** Declaration dynamics Table Inclusion**/
		stringBuilderBeginTableInclusion.append(" SELECT TI.ID_PARTICIPANT_FK ,  ");
		stringBuilderBeginTableInclusion.append("        '' ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableInclusion.append("        '' SECURITY_CLASS , ");
		stringBuilderBeginTableInclusion.append(" 		 '' ID_HOLDER_ACCOUNT_FK , ");
		stringBuilderBeginTableInclusion.append("        '' ID_HOLDER_FK , ");
		stringBuilderBeginTableInclusion.append("        '' ID_ISSUER_FK , ");
		stringBuilderBeginTableInclusion.append(" 		 '' ID_ISSUANCE_CODE_FK ");
		stringBuilderBeginTableInclusion.append("        FROM ( ");		
		
		stringBuilderEndTableInclusion.append(" ) TI  WHERE  (1=1)     "); 
		
		
		return strTableComponentBegin;		
		/**		FIN		**/	
	}
	
	/**
	 * EDV 6 EMISOR*.
	 *
	 * @param stringBuilderBeginTableExclusion the string builder begin table exclusion
	 * @param stringBuilderEndTableExclusion the string builder end table exclusion
	 * @param parameters the parameters
	 * @param strTableComponentBegin the str table component begin
	 * @param stringBuilderBeginTableInclusion the string builder begin table inclusion
	 * @param stringBuilderEndTableInclusion the string builder end table inclusion
	 * @param strTableComponentEnd the str table component end
	 * @return the string builder
	 * @throws ServiceException the service exception
	 */
	public StringBuilder filterRegisteredMatrixIssuer(StringBuilder stringBuilderBeginTableExclusion, StringBuilder stringBuilderEndTableExclusion,
				 Map parameters,StringBuilder strTableComponentBegin,
				 StringBuilder stringBuilderBeginTableInclusion,
				 StringBuilder stringBuilderEndTableInclusion,
				 StringBuilder strTableComponentEnd) throws ServiceException{		
		
		/** getting data for do merging into dynamic query**/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);

		/** Declaration dynamics Table Component**/
		String strEntityCollection="";
		if(Validations.validateIsNotNullAndNotEmpty(billingService.getEntityCollection()) ){
			Integer entityCollection = billingService.getEntityCollection();
			if (entityCollection.equals(EntityCollectionType.HOLDERS.getCode())){
				strEntityCollection= " T.ID_HOLDER_FK ";
			}else if (entityCollection.equals(EntityCollectionType.ISSUERS.getCode())){
				strEntityCollection= " T.ID_ISSUER_FK ";
			}else if(entityCollection.equals(EntityCollectionType.PARTICIPANTS.getCode())){
				strEntityCollection= " T.ID_PARTICIPANT_FK ";
			}
		}
		
		/** Declaration dynamics Table Component**/
		strTableComponentBegin.append(" SELECT T.ID_ISSUER_FK    FROM 	( ");
		
		strTableComponentEnd.append(" ) T  ");
		
		
		/** Declaration dynamics Table Exclusion**/
		stringBuilderBeginTableExclusion.append(" SELECT '' ID_PARTICIPANT_FK ,       ");
		stringBuilderBeginTableExclusion.append("        '' ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableExclusion.append("        '' SECURITY_CLASS , ");
		stringBuilderBeginTableExclusion.append(" 		  '' ID_HOLDER_ACCOUNT_FK , ");
		stringBuilderBeginTableExclusion.append("        '' ID_HOLDER_FK , ");
		stringBuilderBeginTableExclusion.append("        TE.ID_ISSUER_FK ,");
		stringBuilderBeginTableExclusion.append(" 		  '' ID_ISSUANCE_CODE_FK ");
		stringBuilderBeginTableExclusion.append("        FROM ( ");		
		
		stringBuilderEndTableExclusion.append(" ) TE  WHERE  (1=1)     ");
		
		
		/** Declaration dynamics Table Inclusion**/
		stringBuilderBeginTableInclusion.append(" SELECT '' ID_PARTICIPANT_FK ,  ");
		stringBuilderBeginTableInclusion.append("        '' ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableInclusion.append("        '' SECURITY_CLASS , ");
		stringBuilderBeginTableInclusion.append(" 		 '' ID_HOLDER_ACCOUNT_FK , ");
		stringBuilderBeginTableInclusion.append("        '' ID_HOLDER_FK , ");
		stringBuilderBeginTableInclusion.append("        TI.ID_ISSUER_FK , ");
		stringBuilderBeginTableInclusion.append(" 		 '' ID_ISSUANCE_CODE_FK ");
		stringBuilderBeginTableInclusion.append("        FROM ( ");		
		
		stringBuilderEndTableInclusion.append(" ) TI  WHERE  (1=1)     "); 
		
		
		return strTableComponentBegin;		
		/**		FIN		**/	
		}

	
	
	/**
	 * EDV 7 TITULAR*.
	 *
	 * @param stringBuilderBeginTableExclusion the string builder begin table exclusion
	 * @param stringBuilderEndTableExclusion the string builder end table exclusion
	 * @param strTableComponentBegin the str table component begin
	 * @param stringBuilderBeginTableInclusion the string builder begin table inclusion
	 * @param stringBuilderEndTableInclusion the string builder end table inclusion
	 * @param strTableComponentEnd the str table component end
	 * @return the string builder
	 * @throws ServiceException the service exception
	 */
	public StringBuilder filterRegisteredHolder(StringBuilder stringBuilderBeginTableExclusion, StringBuilder stringBuilderEndTableExclusion,
				 StringBuilder strTableComponentBegin,
				 StringBuilder stringBuilderBeginTableInclusion,
				 StringBuilder stringBuilderEndTableInclusion,
				 StringBuilder strTableComponentEnd) throws ServiceException{		
		/** Declaration dynamics Table Component**/
		strTableComponentBegin.append(" SELECT T.ID_HOLDER_FK   FROM 	( ");
		
		strTableComponentEnd.append(" ) T  ");
		
		
		/** Declaration dynamics Table Exclusion**/
		stringBuilderBeginTableExclusion.append(" SELECT '' ID_PARTICIPANT_FK ,       ");
		stringBuilderBeginTableExclusion.append("        '' ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableExclusion.append("        '' SECURITY_CLASS , ");
		stringBuilderBeginTableExclusion.append(" 		  '' ID_HOLDER_ACCOUNT_FK , ");
		stringBuilderBeginTableExclusion.append("        	TE.ID_HOLDER_FK , ");
		stringBuilderBeginTableExclusion.append("        '' ID_ISSUER_FK ,");
		stringBuilderBeginTableExclusion.append(" 		  '' ID_ISSUANCE_CODE_FK ");
		stringBuilderBeginTableExclusion.append("        FROM ( ");		
		
		stringBuilderEndTableExclusion.append(" ) TE  WHERE  (1=1)     ");
		
		
		/** Declaration dynamics Table Inclusion**/
		stringBuilderBeginTableInclusion.append(" SELECT '' ID_PARTICIPANT_FK ,  ");
		stringBuilderBeginTableInclusion.append("        '' ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableInclusion.append("        '' SECURITY_CLASS , ");
		stringBuilderBeginTableInclusion.append(" 		 '' ID_HOLDER_ACCOUNT_FK , ");
		stringBuilderBeginTableInclusion.append("        TI.ID_HOLDER_FK , ");
		stringBuilderBeginTableInclusion.append("        '' ID_ISSUER_FK , ");
		stringBuilderBeginTableInclusion.append(" 		 '' ID_ISSUANCE_CODE_FK ");
		stringBuilderBeginTableInclusion.append("        FROM ( ");		
		
		stringBuilderEndTableInclusion.append(" ) TI  WHERE  (1=1)     "); 
		
		
		return strTableComponentBegin;		
		/**		FIN		**/	
		}
	
	
	/**
	 * 8 
	 * <br>Exceptions : PARTICIPANT, ID_SECURITY_CODE_FK, SECURITY_CLASS, ID_ISSUER_FK, ID_ISSUANCE_CODE_FK.
	 *
	 * @param stringBuilderBeginTableExclusion the string builder begin table exclusion
	 * @param stringBuilderEndTableExclusion the string builder end table exclusion
	 * @param parameters the parameters
	 * @param strTableComponentBegin the str table component begin
	 * @param stringBuilderBeginTableInclusion the string builder begin table inclusion
	 * @param stringBuilderEndTableInclusion the string builder end table inclusion
	 * @param strTableComponentEnd the str table component end
	 * @return the string builder
	 * @throws ServiceException the service exception
	 */
	public StringBuilder filterSecuritiesSirtex(StringBuilder stringBuilderBeginTableExclusion, StringBuilder stringBuilderEndTableExclusion,
			 Map parameters, StringBuilder strTableComponentBegin,
			 StringBuilder stringBuilderBeginTableInclusion,
			 StringBuilder stringBuilderEndTableInclusion,
			 StringBuilder strTableComponentEnd) throws ServiceException{
	
	/** getting data for do merging into dynamic query**/
	BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
	
	/** getting data for do merging into dynamic query**/
	ServiceRate serviceRate=(ServiceRate) parameters.get(GeneralConstants.SERVICE_RATE);
	
	
	/** Start Query Base **/
	/** Declaration dynamics Table Component**/
	String strEntityCollection="";
	if(Validations.validateIsNotNullAndNotEmpty(billingService.getEntityCollection()) ){
		Integer entityCollection = billingService.getEntityCollection();
		if (entityCollection.equals(EntityCollectionType.ISSUERS.getCode())){
			strEntityCollection= " T.ID_ISSUER_FK ";
		}else if(entityCollection.equals(EntityCollectionType.PARTICIPANTS.getCode())){
			strEntityCollection= " T.ID_PARTICIPANT_FK ";
		}
	}
	
	strTableComponentBegin.append(" SELECT ").append(strEntityCollection).append(" ,T.ID_SECURITY_CODE_FK ,  ");
	strTableComponentBegin.append(" T.CURRENCY, T.NOMINAL_VALUE * T.TOTAL_BALANCE ,   ");
	strTableComponentBegin.append(" TRUNC(T.OPERATION_DATE), ");
	strTableComponentBegin.append(" '' AS PART_ORIG, '' AS PART_DEST, T.SECURITY_CLASS,  ");
	strTableComponentBegin.append(" T.NOMINAL_VALUE , T.TOTAL_BALANCE, T.ROLE, ");
	strTableComponentBegin.append(" T.OPERATION_NUMBER, T.ID_HOLDER_FK,")
							.append("  T.NEMONICO_HOLDER, T.ACT_ECO_PAR, T.CARTERA_PROPIA");
	strTableComponentBegin.append(" FROM ( ");
	
	strTableComponentEnd.append(" ) T  ");
	
	strTableComponentEnd.append(" ORDER BY ").append(strEntityCollection).append(" , T.ID_SECURITY_CODE_FK ,    ");
	strTableComponentEnd.append(" T.CURRENCY,  TRUNC(T.OPERATION_DATE)   ");
	
	
	/** Declaration dynamics Table Exclusion**/
	stringBuilderBeginTableExclusion.append(" SELECT TE.ID_PARTICIPANT_FK ,       ");
	stringBuilderBeginTableExclusion.append("        TE.TOTAL_BALANCE, ");
	stringBuilderBeginTableExclusion.append("        TE.ID_SECURITY_CODE_FK, ");
	stringBuilderBeginTableExclusion.append("        TE.CURRENCY, ");
	stringBuilderBeginTableExclusion.append("        TE.NOMINAL_VALUE, ");
	stringBuilderBeginTableExclusion.append("        TE.SECURITY_CLASS, ");
	stringBuilderBeginTableExclusion.append("        TE.ID_ISSUER_FK, ");
	stringBuilderBeginTableExclusion.append("        TE.OPERATION_DATE, ");
	stringBuilderBeginTableExclusion.append("        TE.ID_ISSUANCE_CODE_FK, ");
	stringBuilderBeginTableExclusion.append("        TE.ROLE, ");
	stringBuilderBeginTableExclusion.append("        TE.ID_HOLDER_FK , ");
	stringBuilderBeginTableExclusion.append("        TE.NEMONICO_HOLDER , ");
	stringBuilderBeginTableExclusion.append(" 		 TE.CARTERA_PROPIA ,   ");
	stringBuilderBeginTableExclusion.append(" 		 TE.ACT_ECO_PAR ,   ");
	stringBuilderBeginTableExclusion.append(" 		 TE.OPERATION_NUMBER");
	
	stringBuilderBeginTableExclusion.append("        FROM ( ");		
	
	stringBuilderEndTableExclusion.append(" ) TE  WHERE  (1=1)     ");
	
	
	/** Declaration dynamics Table Inclusion**/
	stringBuilderBeginTableInclusion.append(" SELECT TI.ID_PARTICIPANT_FK,       ");
	stringBuilderBeginTableInclusion.append("        TI.TOTAL_BALANCE, ");
	stringBuilderBeginTableInclusion.append("        TI.ID_SECURITY_CODE_FK, ");
	stringBuilderBeginTableInclusion.append("        TI.CURRENCY, ");
	stringBuilderBeginTableInclusion.append("        TI.NOMINAL_VALUE, ");
	stringBuilderBeginTableInclusion.append("        TI.SECURITY_CLASS, ");
	stringBuilderBeginTableInclusion.append("        TI.ID_ISSUER_FK, ");
	stringBuilderBeginTableInclusion.append("        TI.OPERATION_DATE, ");
	stringBuilderBeginTableInclusion.append("        TI.ID_ISSUANCE_CODE_FK, ");
	stringBuilderBeginTableInclusion.append("        TI.ROLE, ");
	stringBuilderBeginTableInclusion.append("        TI.ID_HOLDER_FK , ");
	stringBuilderBeginTableInclusion.append("        TI.NEMONICO_HOLDER , ");
	stringBuilderBeginTableInclusion.append(" 		 TI.CARTERA_PROPIA ,   ");
	stringBuilderBeginTableInclusion.append(" 		 TI.ACT_ECO_PAR ,   ");
	stringBuilderBeginTableInclusion.append("        TI.OPERATION_NUMBER ");
	
	stringBuilderBeginTableInclusion.append("        FROM ( ");		
	
	stringBuilderEndTableInclusion.append(" ) TI  WHERE  (1=1)     "); 
	
	
	return strTableComponentBegin;		
	/**		FIN		**/	
	}
	
	/**
	 * 9
	 * <br>TRANSFERENCIAS_VALORES_OPERACIONES_EXTRABURSATILES_PRIVADAS_R_FIJA.
	 *
	 * @param stringBuilderBeginTableExclusion the string builder begin table exclusion
	 * @param stringBuilderEndTableExclusion the string builder end table exclusion
	 * @param parameters the parameters
	 * @param strTableComponentBegin the str table component begin
	 * @param stringBuilderBeginTableInclusion the string builder begin table inclusion
	 * @param stringBuilderEndTableInclusion the string builder end table inclusion
	 * @param strTableComponentEnd the str table component end
	 * @return the string builder
	 * @throws ServiceException the service exception
	 */
	public StringBuilder filterSecuritiesMovement(StringBuilder stringBuilderBeginTableExclusion, StringBuilder stringBuilderEndTableExclusion,
				 Map parameters, StringBuilder strTableComponentBegin,
				 StringBuilder stringBuilderBeginTableInclusion,
				 StringBuilder stringBuilderEndTableInclusion,
				 StringBuilder strTableComponentEnd) throws ServiceException{
		
		/** getting data for do merging into dynamic query**/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		
		/** getting data for do merging into dynamic query**/
		ServiceRate serviceRate=(ServiceRate) parameters.get(GeneralConstants.SERVICE_RATE);
		
		
		/** Start Query Base **/
		/** Declaration dynamics Table Component**/
		String strEntityCollection="";
		if(Validations.validateIsNotNullAndNotEmpty(billingService.getEntityCollection()) ){
			Integer entityCollection = billingService.getEntityCollection();
			if (entityCollection.equals(EntityCollectionType.HOLDERS.getCode())){
				strEntityCollection= " T.ID_HOLDER_FK ";
			}else if (entityCollection.equals(EntityCollectionType.ISSUERS.getCode())){
				strEntityCollection= " T.ID_ISSUER_FK ";
			}else if(entityCollection.equals(EntityCollectionType.PARTICIPANTS.getCode())){
				strEntityCollection= " T.ID_PARTICIPANT_FK ";
			}
		}
		
		strTableComponentBegin.append(" SELECT ").append(strEntityCollection).append(" ,T.ID_SECURITY_CODE_FK , T.ID_MOVEMENT_TYPE_FK, ");
		strTableComponentBegin.append(" T.CURRENCY,  TRUNC(T.MOVEMENT_DATE), sum(T.NOMINAL_VALUE) ,T.MOVEMENT_QUANTITY , ");
		strTableComponentBegin.append(" TRUNC(T.OPERATION_DATE), T.OPERATION_NUMBER, ");
		strTableComponentBegin.append(" T.ID_SOURCE_PARTICIPANT_FK, T.ID_TARGET_PARTICIPANT_FK, T.ID_HOLDER_ACCOUNT_FK , T.ID_HOLDER_FK, ");
		strTableComponentBegin.append(" T.SECURITY_CLASS, T.ID_NEGOTIATION_MODALITY_FK, ")
								.append("  T.NEMONICO_HOLDER, T.ACT_ECO_PAR, T.CARTERA_PROPIA");
		strTableComponentBegin.append(" from ( ");
		
		
		strTableComponentEnd.append(" ) T group by").append(strEntityCollection).append(" , T.ID_SECURITY_CODE_FK , T.ID_MOVEMENT_TYPE_FK,   ");
		strTableComponentEnd.append(" T.CURRENCY,  TRUNC(T.MOVEMENT_DATE), T.MOVEMENT_QUANTITY ,TRUNC(T.OPERATION_DATE), T.OPERATION_NUMBER,  "); 
		strTableComponentEnd.append(" T.ID_SOURCE_PARTICIPANT_FK, T.ID_TARGET_PARTICIPANT_FK, T.ID_HOLDER_ACCOUNT_FK , T.ID_HOLDER_FK, ");
		strTableComponentEnd.append(" T.SECURITY_CLASS, T.ID_NEGOTIATION_MODALITY_FK, ")
							.append("  T.NEMONICO_HOLDER, T.ACT_ECO_PAR, T.CARTERA_PROPIA");
		
		strTableComponentEnd.append(" order by ").append(strEntityCollection).append(" , T.ID_SECURITY_CODE_FK , T.ID_MOVEMENT_TYPE_FK,   ");
		strTableComponentEnd.append(" T.CURRENCY,  TRUNC(T.MOVEMENT_DATE), T.MOVEMENT_QUANTITY ,TRUNC(T.OPERATION_DATE), T.OPERATION_NUMBER,  "); 
		strTableComponentEnd.append(" T.ID_SOURCE_PARTICIPANT_FK, T.ID_TARGET_PARTICIPANT_FK, T.ID_HOLDER_ACCOUNT_FK , T.ID_HOLDER_FK, ");
		strTableComponentEnd.append(" T.SECURITY_CLASS, T.ID_NEGOTIATION_MODALITY_FK ");
		
		
		
		
		
		/** Declaration dynamics Table Exclusion**/
		stringBuilderBeginTableExclusion.append(" SELECT TE.ID_HOLDER_ACCOUNT_FK ,       ");
		stringBuilderBeginTableExclusion.append("        TE.ID_HOLDER_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_PARTICIPANT_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_ISSUER_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.SECURITY_CLASS, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_MOVEMENT_TYPE_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.CURRENCY, ");
		stringBuilderBeginTableExclusion.append("        TE.NEMONICO_HOLDER , ");
		stringBuilderBeginTableExclusion.append(" 		 TE.CARTERA_PROPIA ,   ");
		stringBuilderBeginTableExclusion.append(" 		 TE.ACT_ECO_PAR ,   ");
		stringBuilderBeginTableExclusion.append("        TE.MOVEMENT_DATE, ");
		stringBuilderBeginTableExclusion.append("        TE.NOMINAL_VALUE, ");
		stringBuilderBeginTableExclusion.append("        TE.OPERATION_PRICE, ");
		stringBuilderBeginTableExclusion.append("        TE.MOVEMENT_QUANTITY, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_ISSUANCE_CODE_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.OPERATION_DATE, ");
		stringBuilderBeginTableExclusion.append("        TE.OPERATION_NUMBER, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_SOURCE_PARTICIPANT_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_TARGET_PARTICIPANT_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_NEGOTIATION_MODALITY_FK ");
		
		stringBuilderBeginTableExclusion.append("        FROM ( ");		
		
		stringBuilderEndTableExclusion.append(" ) TE  WHERE  (1=1)     ");
		
		
		/** Declaration dynamics Table Inclusion**/
		stringBuilderBeginTableInclusion.append(" SELECT TI.ID_HOLDER_ACCOUNT_FK ,       ");
		stringBuilderBeginTableInclusion.append("        TI.ID_HOLDER_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_PARTICIPANT_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_ISSUER_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.SECURITY_CLASS, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_MOVEMENT_TYPE_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.CURRENCY, ");
		stringBuilderBeginTableInclusion.append("        TI.NEMONICO_HOLDER , ");
		stringBuilderBeginTableInclusion.append(" 		 TI.CARTERA_PROPIA ,   ");
		stringBuilderBeginTableInclusion.append(" 		 TI.ACT_ECO_PAR ,   ");
		stringBuilderBeginTableInclusion.append("        TI.MOVEMENT_DATE, ");
		stringBuilderBeginTableInclusion.append("        TI.NOMINAL_VALUE, ");
		stringBuilderBeginTableInclusion.append("        TI.OPERATION_PRICE, ");
		stringBuilderBeginTableInclusion.append("        TI.MOVEMENT_QUANTITY, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_ISSUANCE_CODE_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.OPERATION_DATE, ");
		stringBuilderBeginTableInclusion.append("        TI.OPERATION_NUMBER, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_SOURCE_PARTICIPANT_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_TARGET_PARTICIPANT_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_NEGOTIATION_MODALITY_FK ");
		
		stringBuilderBeginTableInclusion.append("        FROM ( ");		
		
		stringBuilderEndTableInclusion.append(" ) TI  WHERE  (1=1)     "); 
		
		
		return strTableComponentBegin;		
		/**		FIN		**/	
		}
	
	
	

	/**
	 * 10.
	 *
	 * @param stringBuilderBeginTableExclusion the string builder begin table exclusion
	 * @param stringBuilderEndTableExlclusion the string builder end table exlclusion
	 * @param strTableComponentBegin the str table component begin
	 * @param stringBuilderBeginTableInclusion the string builder begin table inclusion
	 * @param stringBuilderEndTableInclusion the string builder end table inclusion
	 * @param strTableComponentEnd the str table component end
	 * @return the string builder
	 * @throws ServiceException the service exception
	 */
	public StringBuilder filterRegisteredIssuance(StringBuilder stringBuilderBeginTableExclusion, StringBuilder stringBuilderEndTableExlclusion,
													 StringBuilder strTableComponentBegin,
													 StringBuilder stringBuilderBeginTableInclusion,
													 StringBuilder stringBuilderEndTableInclusion,
													 StringBuilder strTableComponentEnd) throws ServiceException{		
		
		/** Declaration dynamics Table Component**/
		strTableComponentBegin.append(" SELECT T.ID_ISSUER_FK, T.ID_SECURITY_CODE_FK,   ");
		strTableComponentBegin.append("   T.CURRENCY, T.PRICE,   ");
		strTableComponentBegin.append("   T.OPERATION_DATE, '' PART_ORIG, '' PART_DEST,  ");
		strTableComponentBegin.append("   T.SECURITY_CLASS , T.ID_ISSUANCE_CODE_FK  ");
		strTableComponentBegin.append("    FROM 	( ");
		
		
		
		strTableComponentEnd.append(" ) T  ");
		
		
		/** Declaration dynamics Table Exclusion**/
		stringBuilderBeginTableExclusion.append(" SELECT '' ID_PARTICIPANT_FK ,  ");
		stringBuilderBeginTableExclusion.append("        TE.ID_SECURITY_CODE_FK , ");
		stringBuilderBeginTableExclusion.append("        TE.SECURITY_CLASS , ");
		stringBuilderBeginTableExclusion.append(" 		 '' ID_HOLDER_ACCOUNT_FK , ");
		stringBuilderBeginTableExclusion.append("        '' ID_HOLDER_FK , ");
		stringBuilderBeginTableExclusion.append("         TE.ID_ISSUER_FK ,");
		stringBuilderBeginTableExclusion.append(" 		  TE.ID_ISSUANCE_CODE_FK, ");
		stringBuilderBeginTableExclusion.append("  		  TE.OPERATION_DATE, ");
		stringBuilderBeginTableExclusion.append("  		  TE.PRICE, ");
		stringBuilderBeginTableExclusion.append("  		  TE.CURRENCY ");
		stringBuilderBeginTableExclusion.append("        FROM ( ");		

		stringBuilderEndTableExlclusion.append(" ) TE  WHERE  (1=1)     ");
		
		
		/** Declaration dynamics Table Inclusion**/
		stringBuilderBeginTableInclusion.append(" SELECT '' ID_PARTICIPANT_FK ,  ");
		stringBuilderBeginTableInclusion.append("        TI.ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.SECURITY_CLASS , ");
		stringBuilderBeginTableInclusion.append(" 		 '' ID_HOLDER_ACCOUNT_FK , ");
		stringBuilderBeginTableInclusion.append("        '' ID_HOLDER_FK , ");
		stringBuilderBeginTableInclusion.append("        TI.ID_ISSUER_FK , ");
		stringBuilderBeginTableInclusion.append(" 		 TI.ID_ISSUANCE_CODE_FK, ");
		stringBuilderBeginTableInclusion.append(" 		 TI.OPERATION_DATE, ");
		stringBuilderBeginTableInclusion.append(" 		 TI.PRICE, ");
		stringBuilderBeginTableInclusion.append(" 		 TI.CURRENCY ");
		stringBuilderBeginTableInclusion.append("        FROM ( ");		
		
		stringBuilderEndTableInclusion.append(" ) TI  WHERE  (1=1)     "); 
		
		
		return strTableComponentBegin;		
		/**		FIN		**/	
	}
	
	
	

	/**
	 *  11 *.
	 *
	 * @param stringBuilderBeginTableExclusion the string builder begin table exclusion
	 * @param stringBuilderEndTableExlclusion the string builder end table exlclusion
	 * @param parameters the parameters
	 * @param strTableComponentBegin the str table component begin
	 * @param stringBuilderBeginTableInclusion the string builder begin table inclusion
	 * @param stringBuilderEndTableInclusion the string builder end table inclusion
	 * @param strTableComponentEnd the str table component end
	 * @return the string builder
	 * @throws ServiceException the service exception
	 */
	public StringBuilder filterChangeOwnerShip(StringBuilder stringBuilderBeginTableExclusion, StringBuilder stringBuilderEndTableExlclusion,
													 Map parameters,  StringBuilder strTableComponentBegin,
													 StringBuilder stringBuilderBeginTableInclusion,
													 StringBuilder stringBuilderEndTableInclusion,
													 StringBuilder strTableComponentEnd) throws ServiceException{		
		
		/** getting data for do merging into dynamic query**/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);

		
		
		/** Start Query Base **/
		/** Declaration dynamics Table Component**/
		String strEntityCollection="";
		if(Validations.validateIsNotNullAndNotEmpty(billingService.getEntityCollection()) ){
			Integer entityCollection = billingService.getEntityCollection();
			if (entityCollection.equals(EntityCollectionType.HOLDERS.getCode())){
				strEntityCollection= " T.ID_HOLDER_FK ";
			}else if (entityCollection.equals(EntityCollectionType.ISSUERS.getCode())){
				strEntityCollection= " T.ID_ISSUER_FK ";
			}else if(entityCollection.equals(EntityCollectionType.PARTICIPANTS.getCode())){
				strEntityCollection= " T.ID_PARTICIPANT_FK ";
			}
		}
		

		
		strTableComponentBegin.append(" SELECT ").append(strEntityCollection).append(" ,T.ID_SECURITY_CODE_FK , T.ID_MOVEMENT_TYPE_FK, ");
		strTableComponentBegin.append(" T.CURRENCY,  TRUNC(T.MOVEMENT_DATE), T.NOMINAL_VALUE ,T.MOVEMENT_QUANTITY , ");
		strTableComponentBegin.append("  TRUNC(T.OPERATION_DATE), T.OPERATION_NUMBER, ");  
		strTableComponentBegin.append(" T.ID_SOURCE_PARTICIPANT_FK, T.ID_TARGET_PARTICIPANT_FK, T.ID_HOLDER_ACCOUNT_FK ,");
		strTableComponentBegin.append(" T.ID_HOLDER_FK,  ")
								.append("  T.NEMONICO_HOLDER, T.ACT_ECO_PAR, T.CARTERA_PROPIA");  
		strTableComponentBegin.append(" from ( ");
		
		 
		strTableComponentEnd.append(" ) T  ");
		strTableComponentEnd.append(" ORDER BY T.OPERATION_NUMBER,").append(strEntityCollection).
							 append(" , T.ID_SECURITY_CODE_FK , T.ID_MOVEMENT_TYPE_FK,   ").
							 append(" T.CURRENCY,  TRUNC(T.MOVEMENT_DATE), T.MOVEMENT_QUANTITY ,TRUNC(T.OPERATION_DATE), ").
							 append(" T.ID_SOURCE_PARTICIPANT_FK, T.ID_TARGET_PARTICIPANT_FK,  ").
							 append(" T.ID_HOLDER_ACCOUNT_FK , T.ID_HOLDER_FK");
		
		
		/** Declaration dynamics Table Exclusion**/
		
		stringBuilderBeginTableExclusion.append(" SELECT TE.ID_HOLDER_ACCOUNT_FK ,       ");
		stringBuilderBeginTableExclusion.append("        TE.ID_HOLDER_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_PARTICIPANT_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_ISSUER_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.SECURITY_CLASS, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_MOVEMENT_TYPE_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.CURRENCY, ");
		stringBuilderBeginTableExclusion.append("        TE.NEMONICO_HOLDER , ");
		stringBuilderBeginTableExclusion.append(" 		 TE.CARTERA_PROPIA ,   ");
		stringBuilderBeginTableExclusion.append(" 		 TE.ACT_ECO_PAR ,   ");
		stringBuilderBeginTableExclusion.append("        TE.MOVEMENT_DATE, ");
		stringBuilderBeginTableExclusion.append("        TE.NOMINAL_VALUE, ");
		stringBuilderBeginTableExclusion.append("        TE.OPERATION_PRICE, ");
		stringBuilderBeginTableExclusion.append("        TE.MOVEMENT_QUANTITY, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_ISSUANCE_CODE_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.OPERATION_DATE, ");
		stringBuilderBeginTableExclusion.append("        TE.OPERATION_NUMBER, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_SOURCE_PARTICIPANT_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_TARGET_PARTICIPANT_FK ");
		stringBuilderBeginTableExclusion.append(" FROM( ");
		
		


		stringBuilderEndTableExlclusion.append(" ) TE  WHERE  (1=1)     ");
		
		
		/** Declaration dynamics Table Inclusion**/
		stringBuilderBeginTableInclusion.append(" SELECT TI.ID_HOLDER_ACCOUNT_FK ,       ");
		stringBuilderBeginTableInclusion.append("        TI.ID_HOLDER_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_PARTICIPANT_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_ISSUER_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.SECURITY_CLASS, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_MOVEMENT_TYPE_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.CURRENCY, ");
		stringBuilderBeginTableInclusion.append("        TI.NEMONICO_HOLDER , ");
		stringBuilderBeginTableInclusion.append(" 		 TI.CARTERA_PROPIA ,   ");
		stringBuilderBeginTableInclusion.append(" 		 TI.ACT_ECO_PAR ,   ");
		stringBuilderBeginTableInclusion.append("        TI.MOVEMENT_DATE, ");
		stringBuilderBeginTableInclusion.append("        TI.NOMINAL_VALUE, ");
		stringBuilderBeginTableInclusion.append("        TI.OPERATION_PRICE, ");
		stringBuilderBeginTableInclusion.append("        TI.MOVEMENT_QUANTITY, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_ISSUANCE_CODE_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.OPERATION_DATE, ");
		stringBuilderBeginTableInclusion.append("        TI.OPERATION_NUMBER, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_SOURCE_PARTICIPANT_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_TARGET_PARTICIPANT_FK ");
		stringBuilderBeginTableInclusion.append("        FROM ( ");		
		
		stringBuilderEndTableInclusion.append(" ) TI  WHERE  (1=1)     "); 
		
		
		return strTableComponentBegin;		
		/**		FIN		**/	
	}
	
	
	
	/**
	 * EDV 18 REMATERIALIZACION_REVERSION_ANOTACIONES_CUENTA_R_FIJA
	 * 		REMATERIALIZACION_REVERSION_ANOTACIONES_CUENTA_R_VARIABLE .
	 *
	 * @param stringBuilderBeginTableExclusion the string builder begin table exclusion
	 * @param stringBuilderEndTableExclusion the string builder end table exclusion
	 * @param parameters the parameters
	 * @param strTableComponentBegin the str table component begin
	 * @param stringBuilderBeginTableInclusion the string builder begin table inclusion
	 * @param stringBuilderEndTableInclusion the string builder end table inclusion
	 * @param strTableComponentEnd the str table component end
	 * @return the string builder
	 * @throws ServiceException the service exception
	 */
	public StringBuilder filterRematerializationAnnotations(StringBuilder stringBuilderBeginTableExclusion, StringBuilder stringBuilderEndTableExclusion,
				 Map parameters, StringBuilder strTableComponentBegin,
				 StringBuilder stringBuilderBeginTableInclusion,
				 StringBuilder stringBuilderEndTableInclusion,
				 StringBuilder strTableComponentEnd) throws ServiceException{
		
		/** getting data for do merging into dynamic query**/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		
		/** getting data for do merging into dynamic query**/
		ServiceRate serviceRate=(ServiceRate) parameters.get(GeneralConstants.SERVICE_RATE);
		
		
		/** Start Query Base **/
		/** Declaration dynamics Table Component**/
		String strEntityCollection="";
		if(Validations.validateIsNotNullAndNotEmpty(billingService.getEntityCollection()) ){
			Integer entityCollection = billingService.getEntityCollection();
			if (entityCollection.equals(EntityCollectionType.HOLDERS.getCode())){
				strEntityCollection= " T.ID_HOLDER_FK ";
			}else if (entityCollection.equals(EntityCollectionType.ISSUERS.getCode())){
				strEntityCollection= " T.ID_ISSUER_FK ";
			}else if(entityCollection.equals(EntityCollectionType.PARTICIPANTS.getCode())){
				strEntityCollection= " T.ID_PARTICIPANT_FK ";
			}
		}
		
		strTableComponentBegin.append(" SELECT ").append(strEntityCollection).append(" ,T.ID_SECURITY_CODE_FK , T.ID_MOVEMENT_TYPE_FK, ");
		strTableComponentBegin.append(" T.CURRENCY,  TRUNC(T.MOVEMENT_DATE), sum(T.NOMINAL_VALUE) ,T.MOVEMENT_QUANTITY ,TRUNC(T.OPERATION_DATE), T.OPERATION_NUMBER,  "); 
		strTableComponentBegin.append(" T.ID_SOURCE_PARTICIPANT_FK, T.ID_TARGET_PARTICIPANT_FK, T.ID_HOLDER_ACCOUNT_FK , T.ID_HOLDER_FK, ");
		strTableComponentBegin.append(" T.SECURITY_CLASS ");
		strTableComponentBegin.append(" from ( ");
		
		
		strTableComponentEnd.append(" ) T group by").append(strEntityCollection).append(" , T.ID_SECURITY_CODE_FK , T.ID_MOVEMENT_TYPE_FK,   ");
		strTableComponentEnd.append(" T.CURRENCY,  TRUNC(T.MOVEMENT_DATE), T.MOVEMENT_QUANTITY ,TRUNC(T.OPERATION_DATE), T.OPERATION_NUMBER,  "); 
		strTableComponentEnd.append(" T.ID_SOURCE_PARTICIPANT_FK, T.ID_TARGET_PARTICIPANT_FK, T.ID_HOLDER_ACCOUNT_FK , T.ID_HOLDER_FK, ");
		strTableComponentEnd.append(" T.SECURITY_CLASS ");
		
		strTableComponentEnd.append(" order by ").append(strEntityCollection).append(" , T.ID_SECURITY_CODE_FK , T.ID_MOVEMENT_TYPE_FK,   ");
		strTableComponentEnd.append(" T.CURRENCY,  TRUNC(T.MOVEMENT_DATE), T.MOVEMENT_QUANTITY ,TRUNC(T.OPERATION_DATE), T.OPERATION_NUMBER,  "); 
		strTableComponentEnd.append(" T.ID_SOURCE_PARTICIPANT_FK, T.ID_TARGET_PARTICIPANT_FK, T.ID_HOLDER_ACCOUNT_FK , T.ID_HOLDER_FK, ");
		strTableComponentEnd.append(" T.SECURITY_CLASS ");
		
		
		
		
		
		/** Declaration dynamics Table Exclusion**/
		stringBuilderBeginTableExclusion.append(" SELECT TE.ID_HOLDER_ACCOUNT_FK ,       ");
		stringBuilderBeginTableExclusion.append("        TE.ID_HOLDER_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_PARTICIPANT_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_ISSUER_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.SECURITY_CLASS, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_MOVEMENT_TYPE_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.CURRENCY, ");
		stringBuilderBeginTableExclusion.append(" 		 TE.CARTERA_PROPIA ,   ");
		stringBuilderBeginTableExclusion.append("        TE.MOVEMENT_DATE, ");
		stringBuilderBeginTableExclusion.append("        TE.NOMINAL_VALUE, ");
		stringBuilderBeginTableExclusion.append("        TE.OPERATION_PRICE, ");
		stringBuilderBeginTableExclusion.append("        TE.MOVEMENT_QUANTITY, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_ISSUANCE_CODE_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.OPERATION_DATE, ");
		stringBuilderBeginTableExclusion.append("        TE.OPERATION_NUMBER, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_SOURCE_PARTICIPANT_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_TARGET_PARTICIPANT_FK ");
//		stringBuilderBeginTableExclusion.append("        TE.ID_NEGOTIATION_MODALITY_FK ");
		
		stringBuilderBeginTableExclusion.append("        FROM ( ");		
		
		stringBuilderEndTableExclusion.append(" ) TE  WHERE  (1=1)     ");
		
		
		/** Declaration dynamics Table Inclusion**/
		stringBuilderBeginTableInclusion.append(" SELECT TI.ID_HOLDER_ACCOUNT_FK ,       ");
		stringBuilderBeginTableInclusion.append("        TI.ID_HOLDER_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_PARTICIPANT_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_ISSUER_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.SECURITY_CLASS, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_MOVEMENT_TYPE_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.CURRENCY, ");
		stringBuilderBeginTableInclusion.append(" 		 TI.CARTERA_PROPIA ,   ");
		stringBuilderBeginTableInclusion.append("        TI.MOVEMENT_DATE, ");
		stringBuilderBeginTableInclusion.append("        TI.NOMINAL_VALUE, ");
		stringBuilderBeginTableInclusion.append("        TI.OPERATION_PRICE, ");
		stringBuilderBeginTableInclusion.append("        TI.MOVEMENT_QUANTITY, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_ISSUANCE_CODE_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.OPERATION_DATE, ");
		stringBuilderBeginTableInclusion.append("        TI.OPERATION_NUMBER, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_SOURCE_PARTICIPANT_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_TARGET_PARTICIPANT_FK ");
//		stringBuilderBeginTableInclusion.append("        TI.ID_NEGOTIATION_MODALITY_FK ");
		
		stringBuilderBeginTableInclusion.append("        FROM ( ");		
		
		stringBuilderEndTableInclusion.append(" ) TI  WHERE  (1=1)     "); 
		
		
		return strTableComponentBegin;		
		/**		FIN		**/	
		}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 *  1 *.
	 *
	 * @param stringBuilderBeginTableExclusion the string builder begin table exclusion
	 * @param stringBuilderEndTableExlclusion the string builder end table exlclusion
	 * @param parameters the parameters
	 * @param strTableComponentBegin the str table component begin
	 * @param stringBuilderBeginTableInclusion the string builder begin table inclusion
	 * @param stringBuilderEndTableInclusion the string builder end table inclusion
	 * @param strTableComponentEnd the str table component end
	 * @return the string builder
	 * @throws ServiceException the service exception
	 */
	@Deprecated
	public StringBuilder filterMovementQuantityByEntityCollection(StringBuilder stringBuilderBeginTableExclusion, StringBuilder stringBuilderEndTableExlclusion,
																		 Map parameters, StringBuilder strTableComponentBegin,
																		 StringBuilder stringBuilderBeginTableInclusion,
																		 StringBuilder stringBuilderEndTableInclusion,
																		 StringBuilder strTableComponentEnd) throws ServiceException{
		
		/** getting data for do merging into dynamic query**/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		
		/** getting data for do merging into dynamic query**/
		ServiceRate serviceRate=(ServiceRate) parameters.get(GeneralConstants.SERVICE_RATE);
		
		
	/** Start Query Base **/
		/** Declaration dynamics Table Component**/
		String strEntityCollection="";
		if(Validations.validateIsNotNullAndNotEmpty(billingService.getEntityCollection()) ){
			Integer entityCollection = billingService.getEntityCollection();
			if (entityCollection.equals(EntityCollectionType.HOLDERS.getCode())){
				strEntityCollection= " T.ID_HOLDER_FK ";
			}else if (entityCollection.equals(EntityCollectionType.ISSUERS.getCode())){
				strEntityCollection= " T.ID_ISSUER_FK ";
			}else if(entityCollection.equals(EntityCollectionType.PARTICIPANTS.getCode())){
				strEntityCollection= " T.ID_PARTICIPANT_FK ";
			}
		}
		strTableComponentBegin.append(" SELECT ").append(strEntityCollection).append(" ,T.ID_SECURITY_CODE_FK , T.ID_MOVEMENT_TYPE_FK, sum(T.cantidad_movimientos) ");
		strTableComponentBegin.append(" , TRUNC(T.MOVEMENT_DATE), TRUNC(T.OPERATION_DATE), T.OPERATION_NUMBER  "); 
		strTableComponentBegin.append(" , T.ID_SOURCE_PARTICIPANT_FK, T.ID_TARGET_PARTICIPANT_FK, T.OPERATION_PRICE, T.ID_HOLDER_ACCOUNT_FK , T.ID_HOLDER_FK, T.MOVEMENT_QUANTITY ");  
		strTableComponentBegin.append(" from ( ");
		
		
		strTableComponentEnd.append(" ) T group by").append(strEntityCollection).append(" , T.ID_SECURITY_CODE_FK , T.ID_MOVEMENT_TYPE_FK   ");
		strTableComponentEnd.append(" , TRUNC(T.MOVEMENT_DATE), TRUNC(T.OPERATION_DATE), T.OPERATION_NUMBER  "); 
		strTableComponentEnd.append(" , T.ID_SOURCE_PARTICIPANT_FK, T.ID_TARGET_PARTICIPANT_FK, T.OPERATION_PRICE, T.ID_HOLDER_ACCOUNT_FK , T.ID_HOLDER_FK , T.MOVEMENT_QUANTITY");  
		strTableComponentEnd.append(" order by ").append(strEntityCollection).append(" , T.ID_SECURITY_CODE_FK , T.ID_MOVEMENT_TYPE_FK   ");
		strTableComponentEnd.append(" , TRUNC(T.MOVEMENT_DATE), TRUNC(T.OPERATION_DATE), T.OPERATION_NUMBER  "); 
		strTableComponentEnd.append(" , T.ID_SOURCE_PARTICIPANT_FK, T.ID_TARGET_PARTICIPANT_FK, T.OPERATION_PRICE, T.ID_HOLDER_ACCOUNT_FK , T.ID_HOLDER_FK , T.MOVEMENT_QUANTITY");  
		
		/** Declaration dynamics Table Exclusion**/
		stringBuilderBeginTableExclusion.append(" SELECT TE.CANTIDAD_MOVIMIENTOS ,TE.ACCOUNT_NUMBER,TE.ID_HOLDER_ACCOUNT_FK,");
		stringBuilderBeginTableExclusion.append(" TE.ID_HOLDER_FK,TE.ID_PARTICIPANT_FK,TE.ID_SECURITY_CODE_FK,TE.ID_ISSUER_FK");
		stringBuilderBeginTableExclusion.append(" ,TE.SECURITY_CLASS,TE.ID_MOVEMENT_TYPE_FK");
		stringBuilderBeginTableExclusion.append(" , '' ID_ISSUANCE_CODE_FK ");
		stringBuilderBeginTableExclusion.append(" , TE.OPERATION_PRICE, TE.ID_SOURCE_PARTICIPANT_FK, TE.ID_TARGET_PARTICIPANT_FK , TE.OPERATION_DATE ");
		stringBuilderBeginTableExclusion.append(" , TE.OPERATION_NUMBER, TE.MOVEMENT_DATE, TE.MOVEMENT_QUANTITY ");
		stringBuilderBeginTableExclusion.append(" from ( ");		
		
		stringBuilderEndTableExlclusion.append(" ) TE  where  (1=1)     ");

		
		/**	Declaration dynamics Table Inclusion	**/
		List<RateInclusion> rateInclusionList= rateExceptionServiceBean.findRateInclusionListByIdServiceRatePk(serviceRate.getIdServiceRatePk());		
		if (Validations.validateListIsNotNullAndNotEmpty(rateInclusionList)){
			
			stringBuilderBeginTableInclusion.append(" SELECT TI.CANTIDAD_MOVIMIENTOS ,TI.ACCOUNT_NUMBER,TI.ID_HOLDER_ACCOUNT_FK,");
			stringBuilderBeginTableInclusion.append(" TI.ID_HOLDER_FK,TI.ID_PARTICIPANT_FK,TI.ID_SECURITY_CODE_FK,TI.ID_ISSUER_FK");
			stringBuilderBeginTableInclusion.append(" ,TI.SECURITY_CLASS,TI.ID_MOVEMENT_TYPE_FK");
			stringBuilderBeginTableInclusion.append(" , '' ID_ISSUANCE_CODE_FK ");
			stringBuilderBeginTableInclusion.append(" , TI.OPERATION_PRICE, TI.ID_SOURCE_PARTICIPANT_FK, TI.ID_TARGET_PARTICIPANT_FK , TI.OPERATION_DATE ");
			stringBuilderBeginTableInclusion.append(" , TI.OPERATION_NUMBER, TI.MOVEMENT_DATE, TI.MOVEMENT_QUANTITY ");
			stringBuilderBeginTableInclusion.append(" from ( ");		
			
			stringBuilderEndTableInclusion.append(" ) TI  where  (1=1)     ");
			
		}
		
		
		return strTableComponentBegin;
		/**		FIN		**/	
	}
	

	/**
	 *  2 *.
	 *
	 * @param stringBuilderBeginTableExclusion the string builder begin table exclusion
	 * @param stringBuilderEndTableExlclusion the string builder end table exlclusion
	 * @param parameters the parameters
	 * @param strTableComponentBegin the str table component begin
	 * @param stringBuilderBeginTableInclusion the string builder begin table inclusion
	 * @param stringBuilderEndTableInclusion the string builder end table inclusion
	 * @param strTableComponentEnd the str table component end
	 * @return the string builder
	 */
	@Deprecated
	public StringBuilder filterMovementSettlementWithOutMechanismByEntityCollection(StringBuilder stringBuilderBeginTableExclusion, StringBuilder stringBuilderEndTableExlclusion,
																					Map parameters, StringBuilder strTableComponentBegin,
																					StringBuilder stringBuilderBeginTableInclusion,
																					StringBuilder stringBuilderEndTableInclusion,
																					StringBuilder strTableComponentEnd){
		

		/** getting data for do merging into dynamic query**/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		
		/** getting data for do merging into dynamic query**/
		ServiceRate serviceRate=(ServiceRate) parameters.get(GeneralConstants.SERVICE_RATE);
		
		String strEntityCollection="";
		if(Validations.validateIsNotNullAndNotEmpty(billingService.getEntityCollection()) ){
			Integer entityCollection = billingService.getEntityCollection();
			if (entityCollection.equals(EntityCollectionType.HOLDERS.getCode())){
				strEntityCollection= " T.ID_HOLDER_FK ";
			}else if (entityCollection.equals(EntityCollectionType.ISSUERS.getCode())){
				strEntityCollection= " T.ID_ISSUER_FK ";
			}else if(entityCollection.equals(EntityCollectionType.PARTICIPANTS.getCode())){
				strEntityCollection= " T.ID_PARTICIPANT_FK ";
			}
		}
		
		
		strTableComponentBegin.append(" SELECT ").append(strEntityCollection).append(" ,T.ID_SECURITY_CODE_FK , T.ID_MOVEMENT_TYPE_FK, ");
		strTableComponentBegin.append(" T.CURRENCY ,");
		strTableComponentBegin.append(" T.MOVEMENT_DATE ,");
		strTableComponentBegin.append(" T.OPERATION_PRICE ,");
		strTableComponentBegin.append(" T.MOVEMENT_QUANTITY  ");
			strTableComponentBegin.append(" , T.OPERATION_DATE, T.OPERATION_NUMBER ");
			strTableComponentBegin.append(" , T.ID_SOURCE_PARTICIPANT_FK, T.ID_TARGET_PARTICIPANT_FK ");
			strTableComponentBegin.append(" , T.ID_HOLDER_ACCOUNT_FK , T.ID_HOLDER_FK  from 	(");
		
		strTableComponentEnd.append(" ) T order by").append(strEntityCollection).append(" , T.ID_SECURITY_CODE_FK , T.ID_MOVEMENT_TYPE_FK   ");
	
		
		/** Declaration dynamics Table Exclusion**/
		stringBuilderBeginTableExclusion.append(" SELECT TE.ID_HOLDER_ACCOUNT_FK ,  ");
		stringBuilderBeginTableExclusion.append("        TE.ID_HOLDER_FK , ");
		stringBuilderBeginTableExclusion.append("        TE.ID_PARTICIPANT_FK , ");
		stringBuilderBeginTableExclusion.append(" 		  TE.ID_SECURITY_CODE_FK , ");
		stringBuilderBeginTableExclusion.append("        TE.ID_ISSUER_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.SECURITY_CLASS,");
		stringBuilderBeginTableExclusion.append(" 		  TE.ID_MOVEMENT_TYPE_FK ,");
		stringBuilderBeginTableExclusion.append(" 		  TE.CURRENCY , ");
		stringBuilderBeginTableExclusion.append(" 		  TE.MOVEMENT_DATE , ");
		stringBuilderBeginTableExclusion.append(" 		  TE.OPERATION_PRICE , ");
		stringBuilderBeginTableExclusion.append(" 		  TE.MOVEMENT_QUANTITY,  ");		
		stringBuilderBeginTableExclusion.append(" 		  '' ID_ISSUANCE_CODE_FK ");
		stringBuilderBeginTableExclusion.append(" 		  , TE.OPERATION_DATE  ,  TE.OPERATION_NUMBER  ");
		stringBuilderBeginTableExclusion.append(" 		  , TE.ID_SOURCE_PARTICIPANT_FK , TE.ID_TARGET_PARTICIPANT_FK ");
		stringBuilderBeginTableExclusion.append("   from ( ");		
		
		stringBuilderEndTableExlclusion.append(" ) TE  where  (1=1)     ");		
			
		
		/**	Declaration dynamics Table Inclusion	**/
		List<RateInclusion> rateInclusionList= rateExceptionServiceBean.findRateInclusionListByIdServiceRatePk(serviceRate.getIdServiceRatePk());
		if (Validations.validateListIsNotNullAndNotEmpty(rateInclusionList)){
			/** Declaration dynamics Table Inclusion**/
			stringBuilderBeginTableInclusion.append(" SELECT TI.ID_HOLDER_ACCOUNT_FK ,  ");
			stringBuilderBeginTableInclusion.append("        TI.ID_HOLDER_FK , ");
			stringBuilderBeginTableInclusion.append("        TI.ID_PARTICIPANT_FK , ");
			stringBuilderBeginTableInclusion.append(" 		  TI.ID_SECURITY_CODE_FK , ");
			stringBuilderBeginTableInclusion.append("        TI.ID_ISSUER_FK, ");
			stringBuilderBeginTableInclusion.append("        TI.SECURITY_CLASS,");
			stringBuilderBeginTableInclusion.append(" 		  TI.ID_MOVEMENT_TYPE_FK ,");
			stringBuilderBeginTableInclusion.append(" 		  TI.CURRENCY , ");
			stringBuilderBeginTableInclusion.append(" 		  TI.MOVEMENT_DATE , ");
			stringBuilderBeginTableInclusion.append(" 		  TI.OPERATION_PRICE , ");
			stringBuilderBeginTableInclusion.append(" 		  TI.MOVEMENT_QUANTITY,  ");	
			stringBuilderBeginTableInclusion.append(" 		  '' ID_ISSUANCE_CODE_FK  ");
				stringBuilderBeginTableInclusion.append(" 		  , TI.OPERATION_DATE  ,  TI.OPERATION_NUMBER  ");
				stringBuilderBeginTableInclusion.append(" 		  , TI.ID_SOURCE_PARTICIPANT_FK , TI.ID_TARGET_PARTICIPANT_FK ");
			stringBuilderBeginTableInclusion.append("        from ( ");		
			
			stringBuilderEndTableInclusion.append(" ) TI  where  (1=1)     ");
			
		}		
		
		
		return strTableComponentBegin;		
		/**		FIN		**/		
	}
	
	/**
	 *  3 *.
	 *
	 * @param stringBuilderBeginTableExclusion the string builder begin table exclusion
	 * @param stringBuilderEndTableExlclusion the string builder end table exlclusion
	 * @param parameters the parameters
	 * @param strTableComponentBegin the str table component begin
	 * @param stringBuilderBeginTableInclusion the string builder begin table inclusion
	 * @param stringBuilderEndTableInclusion the string builder end table inclusion
	 * @param strTableComponentEnd the str table component end
	 * @return the string builder
	 */
	@Deprecated
	public StringBuilder filterBalanceValuedInternationalSecurities(StringBuilder stringBuilderBeginTableExclusion, StringBuilder stringBuilderEndTableExlclusion,
																	Map parameters, StringBuilder strTableComponentBegin,
																	StringBuilder stringBuilderBeginTableInclusion,
																	StringBuilder stringBuilderEndTableInclusion,
																	StringBuilder strTableComponentEnd){
		

		/** getting data for do merging into dynamic query**/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		
		/** getting data for do merging into dynamic query**/
		ServiceRate serviceRate=(ServiceRate) parameters.get(GeneralConstants.SERVICE_RATE);
	
		 
		strTableComponentBegin.append(" SELECT T.ID_PARTICIPANT_FK , SUM(T.MONTO),T.CURRENCY  FROM 	( ");
		
		strTableComponentEnd.append(" ) T GROUP BY T.ID_PARTICIPANT_FK,T.CURRENCY ");
		
		
		/** Declaration dynamics Table Exclusion**/
		stringBuilderBeginTableExclusion.append(" SELECT TE.ID_PARTICIPANT_FK ,  ");
		stringBuilderBeginTableExclusion.append("        TE.ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.SECURITY_CLASS , ");
		stringBuilderBeginTableExclusion.append(" 		  TE.ID_HOLDER_ACCOUNT_FK , ");
		stringBuilderBeginTableExclusion.append("        TE.ID_HOLDER_FK , ");
		stringBuilderBeginTableExclusion.append("        TE.ID_ISSUER_FK ,");
		stringBuilderBeginTableExclusion.append(" 		  '' ID_ISSUANCE_CODE_FK  ,");
		stringBuilderBeginTableExclusion.append(" 		  TOTAL_BALANCE*(SELECT CURRENT_NOMINAL_VALUE FROM SECURITY  WHERE TE.ID_SECURITY_CODE_FK=ID_SECURITY_CODE_PK ) AS MONTO , ");	
		stringBuilderBeginTableExclusion.append(" 		  TE.CURRENCY ");
		stringBuilderBeginTableExclusion.append("        FROM ( ");		
		
		stringBuilderEndTableExlclusion.append(" ) TE  WHERE  (1=1)     ");
		
		
		
		/**	Declaration dynamics Table Inclusion	**/
		List<RateInclusion> rateInclusionList= rateExceptionServiceBean.findRateInclusionListByIdServiceRatePk(serviceRate.getIdServiceRatePk());		
		if (Validations.validateListIsNotNullAndNotEmpty(rateInclusionList)){
			/** Declaration dynamics Table Inclusion**/
			stringBuilderBeginTableInclusion.append(" SELECT TI.ID_PARTICIPANT_FK ,  ");
			stringBuilderBeginTableInclusion.append("        TI.ID_SECURITY_CODE_FK, ");
			stringBuilderBeginTableInclusion.append("        TI.SECURITY_CLASS , ");
			stringBuilderBeginTableInclusion.append(" 		  TI.ID_HOLDER_ACCOUNT_FK , ");
			stringBuilderBeginTableInclusion.append("        TI.ID_HOLDER_FK , ");
			stringBuilderBeginTableInclusion.append("        TI.ID_ISSUER_FK ,");
			stringBuilderBeginTableInclusion.append(" 		   '' ID_ISSUANCE_CODE_FK  ,");
			stringBuilderBeginTableInclusion.append(" 		  TOTAL_BALANCE*(SELECT CURRENT_NOMINAL_VALUE FROM SECURITY  WHERE TI.ID_SECURITY_CODE_FK=ID_SECURITY_CODE_PK ) AS MONTO ,");	
			stringBuilderBeginTableInclusion.append(" 		  TI.CURRENCY ");
			stringBuilderBeginTableInclusion.append("        FROM ( ");		
			
			stringBuilderEndTableInclusion.append(" ) TI  WHERE  (1=1)     ");
		
		}
		
		
		return strTableComponentBegin;		
		/**		FIN		**/	
	}
	
	
	
	
	
	/**
	 *  5 *.
	 *
	 * @param stringBuilderBeginTableExclusion the string builder begin table exclusion
	 * @param stringBuilderEndTableExlclusion the string builder end table exlclusion
	 * @param strTableComponentBegin the str table component begin
	 * @param stringBuilderBeginTableInclusion the string builder begin table inclusion
	 * @param stringBuilderEndTableInclusion the string builder end table inclusion
	 * @param strTableComponentEnd the str table component end
	 * @return the string builder
	 */
	@Deprecated
	public StringBuilder filterRegisteredParticipants(StringBuilder stringBuilderBeginTableExclusion, StringBuilder stringBuilderEndTableExlclusion,
														StringBuilder strTableComponentBegin,
														StringBuilder stringBuilderBeginTableInclusion,
														StringBuilder stringBuilderEndTableInclusion,
														StringBuilder strTableComponentEnd){
		
		
		/** Declaration dynamics Table Component**/
		strTableComponentBegin.append(" SELECT T.ID_PARTICIPANT_FK   FROM 	( ");
	
		strTableComponentEnd.append(" ) T  ");
		
		
		
		/** Declaration dynamics Table Exclusion**/
		stringBuilderBeginTableExclusion.append(" SELECT TE.ID_PARTICIPANT_FK ,  ");
		stringBuilderBeginTableExclusion.append("        '' ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableExclusion.append("        '' SECURITY_CLASS , ");
		stringBuilderBeginTableExclusion.append(" 		  '' ID_HOLDER_ACCOUNT_FK , ");
		stringBuilderBeginTableExclusion.append("        '' ID_HOLDER_FK , ");
		stringBuilderBeginTableExclusion.append("        '' ID_ISSUER_FK ,");
		stringBuilderBeginTableExclusion.append(" 		  '' ID_ISSUANCE_CODE_FK ");
		stringBuilderBeginTableExclusion.append("        FROM ( ");		

		stringBuilderEndTableExlclusion.append(" ) TE  WHERE  (1=1)     ");
		
		
		/** Declaration dynamics Table Inclusion**/
		stringBuilderBeginTableInclusion.append(" SELECT TI.ID_PARTICIPANT_FK ,  ");
		stringBuilderBeginTableInclusion.append("        '' ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableInclusion.append("        '' SECURITY_CLASS , ");
		stringBuilderBeginTableInclusion.append(" 		 '' ID_HOLDER_ACCOUNT_FK , ");
		stringBuilderBeginTableInclusion.append("        '' ID_HOLDER_FK , ");
		stringBuilderBeginTableInclusion.append("        '' ID_ISSUER_FK , ");
		stringBuilderBeginTableInclusion.append(" 		 '' ID_ISSUANCE_CODE_FK ");
		stringBuilderBeginTableInclusion.append("        FROM ( ");		

		stringBuilderEndTableInclusion.append(" ) TI  WHERE  (1=1)     ");
		
				
		return strTableComponentBegin;		
		/**		FIN		**/		
	}
	
	
	/**
	 *  6 *.
	 *
	 * @param stringBuilderBeginTableExclusion the string builder begin table exclusion
	 * @param stringBuilderEndTableExlclusion the string builder end table exlclusion
	 * @param strTableComponentBegin the str table component begin
	 * @param stringBuilderBeginTableInclusion the string builder begin table inclusion
	 * @param stringBuilderEndTableInclusion the string builder end table inclusion
	 * @param strTableComponentEnd the str table component end
	 * @return the string builder
	 * @throws ServiceException the service exception
	 */
	@Deprecated
	public StringBuilder filterRegisteredIssuer(StringBuilder stringBuilderBeginTableExclusion, StringBuilder stringBuilderEndTableExlclusion,
																		 StringBuilder strTableComponentBegin,
																		 StringBuilder stringBuilderBeginTableInclusion,
																		 StringBuilder stringBuilderEndTableInclusion,
																		 StringBuilder strTableComponentEnd) throws ServiceException{
		

		/** Declaration dynamics Table Component**/
		strTableComponentBegin.append(" SELECT T.ID_ISSUER_FK    FROM 	( ");

		strTableComponentEnd.append(" ) T  ");
		
		
		/** Declaration dynamics Table Exclusion**/
		stringBuilderBeginTableExclusion.append(" SELECT '' ID_PARTICIPANT_FK , ");
		stringBuilderBeginTableExclusion.append("        '' ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableExclusion.append("        '' SECURITY_CLASS , ");
		stringBuilderBeginTableExclusion.append(" 		  '' ID_HOLDER_ACCOUNT_FK , ");
		stringBuilderBeginTableExclusion.append("        '' ID_HOLDER_FK , ");
		stringBuilderBeginTableExclusion.append("        TE.ID_ISSUER_FK ,");
		stringBuilderBeginTableExclusion.append(" 		  '' ID_ISSUANCE_CODE_FK ");
		stringBuilderBeginTableExclusion.append("        FROM ( ");		

		stringBuilderEndTableExlclusion.append(" ) TE  WHERE  (1=1)     ");
		
		
		/** Declaration dynamics Table Inclusion**/
		stringBuilderBeginTableInclusion.append(" SELECT '' ID_PARTICIPANT_FK ,  ");
		stringBuilderBeginTableInclusion.append("        '' ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableInclusion.append("        '' SECURITY_CLASS , ");
		stringBuilderBeginTableInclusion.append(" 		 '' ID_HOLDER_ACCOUNT_FK , ");
		stringBuilderBeginTableInclusion.append("        '' ID_HOLDER_FK , ");
		stringBuilderBeginTableInclusion.append("        TI.ID_ISSUER_FK , ");
		stringBuilderBeginTableInclusion.append(" 		  '' ID_ISSUANCE_CODE_FK ");
		stringBuilderBeginTableInclusion.append("        FROM ( ");		

		stringBuilderEndTableInclusion.append(" ) TI  WHERE  (1=1)     ");
		
		
		return strTableComponentBegin;		
		/**		FIN		**/	
	}
	
	
	/**
	 *  7 *.
	 *
	 * @param stringBuilderBeginTableExclusion the string builder begin table exclusion
	 * @param stringBuilderEndTableExlclusion the string builder end table exlclusion
	 * @param strTableComponentBegin the str table component begin
	 * @param stringBuilderBeginTableInclusion the string builder begin table inclusion
	 * @param stringBuilderEndTableInclusion the string builder end table inclusion
	 * @param strTableComponentEnd the str table component end
	 * @return the string builder
	 * @throws ServiceException the service exception
	 */
	@Deprecated
	public StringBuilder filterParticipantsForQuotaAnnual(StringBuilder stringBuilderBeginTableExclusion, StringBuilder stringBuilderEndTableExlclusion,
																			 StringBuilder strTableComponentBegin,
																			 StringBuilder stringBuilderBeginTableInclusion,
																			 StringBuilder stringBuilderEndTableInclusion,
																			 StringBuilder strTableComponentEnd) throws ServiceException{
		
		
		/** Declaration dynamics Table Component**/
		strTableComponentBegin.append(" SELECT T.ID_PARTICIPANT_FK   FROM 	( ");
		
		strTableComponentEnd.append(" ) T  ");
		

		/** Declaration dynamics Table Exclusion**/
		stringBuilderBeginTableExclusion.append(" SELECT TE.ID_PARTICIPANT_FK ,  ");
		stringBuilderBeginTableExclusion.append("        '' ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableExclusion.append("        '' SECURITY_CLASS , ");
		stringBuilderBeginTableExclusion.append(" 		  '' ID_HOLDER_ACCOUNT_FK , ");
		stringBuilderBeginTableExclusion.append("        '' ID_HOLDER_FK , ");
		stringBuilderBeginTableExclusion.append("        '' ID_ISSUER_FK ,");
		stringBuilderBeginTableExclusion.append(" 		  '' ID_ISSUANCE_CODE_FK ");
		stringBuilderBeginTableExclusion.append("        FROM ( ");		

		stringBuilderEndTableExlclusion.append(" ) TE  WHERE  (1=1)     ");
		
		
		/** Declaration dynamics Table Inclusion**/
		stringBuilderBeginTableInclusion.append(" SELECT TI.ID_PARTICIPANT_FK ,  ");
		stringBuilderBeginTableInclusion.append("        '' ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableInclusion.append("        '' SECURITY_CLASS , ");
		stringBuilderBeginTableInclusion.append(" 		 '' ID_HOLDER_ACCOUNT_FK , ");
		stringBuilderBeginTableInclusion.append("        '' ID_HOLDER_FK , ");
		stringBuilderBeginTableInclusion.append("        '' ID_ISSUER_FK , ");
		stringBuilderBeginTableInclusion.append(" 		  '' ID_ISSUANCE_CODE_FK ");
		stringBuilderBeginTableInclusion.append("        FROM ( ");		

		stringBuilderEndTableInclusion.append(" ) TI  WHERE  (1=1)     ");
		
		
		return strTableComponentBegin;		
		/**		FIN		**/	
	}

	
	
	
	/**
	 *  9 *.
	 *
	 * @param stringBuilderBeginTableExclusion the string builder begin table exclusion
	 * @param stringBuilderEndTableExlclusion the string builder end table exlclusion
	 * @param strTableComponentBegin the str table component begin
	 * @param stringBuilderBeginTableInclusion the string builder begin table inclusion
	 * @param stringBuilderEndTableInclusion the string builder end table inclusion
	 * @param strTableComponentEnd the str table component end
	 * @return the string builder
	 * @throws ServiceException the service exception
	 */
	@Deprecated
	public StringBuilder filterIssuanceForQuota(StringBuilder stringBuilderBeginTableExclusion, StringBuilder stringBuilderEndTableExlclusion,
																		 StringBuilder strTableComponentBegin,
																		 StringBuilder stringBuilderBeginTableInclusion,
																		 StringBuilder stringBuilderEndTableInclusion,
																		 StringBuilder strTableComponentEnd) throws ServiceException{
		
		
		/** Declaration dynamics Table Component**/
		strTableComponentBegin.append(" SELECT DISTINCT T.ID_ISSUER_FK ,T.ID_ISSUANCE_CODE_FK, T.FACTOR FROM 	( ");

		strTableComponentEnd.append(" ) T ORDER BY T.ID_ISSUER_FK ,T.ID_ISSUANCE_CODE_FK ");
		
		
				
		/** Declaration dynamics Table Exclusion**/
		stringBuilderBeginTableExclusion.append(" SELECT '' ID_PARTICIPANT_FK ,  ");
		stringBuilderBeginTableExclusion.append("        TE.ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.SECURITY_CLASS , ");
		stringBuilderBeginTableExclusion.append(" 		  '' ID_HOLDER_ACCOUNT_FK , ");
		stringBuilderBeginTableExclusion.append("        '' ID_HOLDER_FK , ");
		stringBuilderBeginTableExclusion.append("        TE.ID_ISSUER_FK ,");
		stringBuilderBeginTableExclusion.append(" 		  TE.ID_ISSUANCE_CODE_FK, ");
		stringBuilderBeginTableExclusion.append(" 		  TE.FACTOR ");
		stringBuilderBeginTableExclusion.append("        FROM ( ");		

		stringBuilderEndTableExlclusion.append(" ) TE  WHERE  (1=1)     ");
		
		
		/** Declaration dynamics Table Inclusion**/
		stringBuilderBeginTableInclusion.append(" SELECT '' ID_PARTICIPANT_FK ,  ");
		stringBuilderBeginTableInclusion.append("        TI.ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.SECURITY_CLASS , ");
		stringBuilderBeginTableInclusion.append(" 		 '' ID_HOLDER_ACCOUNT_FK , ");
		stringBuilderBeginTableInclusion.append("        '' ID_HOLDER_FK , ");
		stringBuilderBeginTableInclusion.append("        TI.ID_ISSUER_FK , ");
		stringBuilderBeginTableInclusion.append(" 		 TI.ID_ISSUANCE_CODE_FK, ");
		stringBuilderBeginTableInclusion.append(" 		 TI.FACTOR ");
		
		stringBuilderEndTableInclusion.append(" ) TI  WHERE  (1=1)     ");
				
		
		return strTableComponentBegin;		
		/**		FIN		**/	
	}
		
	
	/**
	 * 17.
	 *
	 * @param stringBuilderBeginTableExclusion the string builder begin table exclusion
	 * @param stringBuilderEndTableExclusion the string builder end table exclusion
	 * @param parameters the parameters
	 * @param strTableComponentBegin the str table component begin
	 * @param stringBuilderBeginTableInclusion the string builder begin table inclusion
	 * @param stringBuilderEndTableInclusion the string builder end table inclusion
	 * @param strTableComponentEnd the str table component end
	 * @return the string builder
	 * @throws ServiceException the service exception
	 */
	public StringBuilder findMovementPawnBlockAffectation(StringBuilder stringBuilderBeginTableExclusion, StringBuilder stringBuilderEndTableExclusion,
																 Map parameters, StringBuilder strTableComponentBegin,
																 StringBuilder stringBuilderBeginTableInclusion,
																 StringBuilder stringBuilderEndTableInclusion,
																 StringBuilder strTableComponentEnd) throws ServiceException{
		
		
		/** getting data for do merging into dynamic query**/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		
		/** Start Query Base **/
		/** Declaration dynamics Table Component**/
		String strEntityCollection="";
		if(Validations.validateIsNotNullAndNotEmpty(billingService.getEntityCollection()) ){
			Integer entityCollection = billingService.getEntityCollection();
			if (entityCollection.equals(EntityCollectionType.ISSUERS.getCode())){
				strEntityCollection= " T.ID_ISSUER_FK ";
			}else if(entityCollection.equals(EntityCollectionType.PARTICIPANTS.getCode())){
				strEntityCollection= " T.ID_PARTICIPANT_FK ";
			}
		}
		
		strTableComponentBegin.append(" SELECT ").append(strEntityCollection).append(" , T.ID_SECURITY_CODE_FK ,  ")
							  .append(" T.ID_MOVEMENT_TYPE_FK, T.CURRENCY, TRUNC(T.OPERATION_DATE) AS MOVEMENT_DATE , ")
							  .append(" T.NOMINAL_VALUE , ")
							  .append(" T.TOTAL_BALANCE ,TRUNC(T.OPERATION_DATE)  , T.OPERATION_NUMBER, ")
							  .append(" '' AS PART_ORIG, '' AS PART_DEST, T.ID_HOLDER_ACCOUNT_FK, T.ID_HOLDER_FK , ")
							  .append(" T.ID_BLOCK_REQUEST_FK, T.ID_ISSUANCE_CODE_FK, T.SECURITY_CLASS, ")
							  .append("  T.NEMONICO_HOLDER, T.ACT_ECO_PAR, T.CARTERA_PROPIA")
							  .append(" FROM ( ");

		
		strTableComponentEnd.append(" ) T ORDER BY T.ID_BLOCK_REQUEST_FK, T.ID_ISSUANCE_CODE_FK, ").
							 append(strEntityCollection).append(" , T.ID_SECURITY_CODE_FK ,    ");
		strTableComponentEnd.append(" T.CURRENCY,  TRUNC(T.OPERATION_DATE)   ");
		
		
		/** Declaration dynamics Table Exclusion**/
		stringBuilderBeginTableExclusion.append(" SELECT TE.ID_PARTICIPANT_FK ,       ");
		stringBuilderBeginTableExclusion.append("        TE.TOTAL_BALANCE, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.CURRENCY, ");
		stringBuilderBeginTableExclusion.append("        TE.NOMINAL_VALUE, ");
		stringBuilderBeginTableExclusion.append("        TE.SECURITY_CLASS, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_MOVEMENT_TYPE_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_ISSUER_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.NEMONICO_HOLDER , ");
		stringBuilderBeginTableExclusion.append(" 		 TE.CARTERA_PROPIA ,   ");
		stringBuilderBeginTableExclusion.append(" 		 TE.ACT_ECO_PAR ,   ");
		stringBuilderBeginTableExclusion.append("        TE.OPERATION_DATE, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_HOLDER_ACCOUNT_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_HOLDER_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.OPERATION_NUMBER, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_ISSUANCE_CODE_FK, ");
		stringBuilderBeginTableExclusion.append("		 TE.ID_BLOCK_REQUEST_FK");
		
		stringBuilderBeginTableExclusion.append("        FROM ( ");		
		
		stringBuilderEndTableExclusion.append(" ) TE  WHERE  (1=1)     ");
		
		
		/** Declaration dynamics Table Inclusion**/
		stringBuilderBeginTableInclusion.append(" SELECT TI.ID_PARTICIPANT_FK,       ");
		stringBuilderBeginTableInclusion.append("        TI.TOTAL_BALANCE, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.CURRENCY, ");
		stringBuilderBeginTableInclusion.append("        TI.NOMINAL_VALUE, ");
		stringBuilderBeginTableInclusion.append("        TI.SECURITY_CLASS, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_MOVEMENT_TYPE_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_ISSUER_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.NEMONICO_HOLDER , ");
		stringBuilderBeginTableInclusion.append(" 		 TI.CARTERA_PROPIA ,   ");
		stringBuilderBeginTableInclusion.append(" 		 TI.ACT_ECO_PAR ,   ");
		stringBuilderBeginTableInclusion.append("        TI.OPERATION_DATE, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_HOLDER_ACCOUNT_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_HOLDER_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.OPERATION_NUMBER, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_ISSUANCE_CODE_FK, ");
		stringBuilderBeginTableInclusion.append("		 TI.ID_BLOCK_REQUEST_FK ");
		
		stringBuilderBeginTableInclusion.append("        FROM ( ");		
		
		stringBuilderEndTableInclusion.append(" ) TI  WHERE  (1=1)     "); 
		
				
		
		return strTableComponentBegin;		
		/**		FIN		**/	
	}
	
	/**
	 *  15.
	 *
	 * @param stringBuilderBeginTableExclusion the string builder begin table exclusion
	 * @param stringBuilderEndTableExclusion the string builder end table exclusion
	 * @param parameters the parameters
	 * @param strTableComponentBegin the str table component begin
	 * @param stringBuilderBeginTableInclusion the string builder begin table inclusion
	 * @param stringBuilderEndTableInclusion the string builder end table inclusion
	 * @param strTableComponentEnd the str table component end
	 * @return the string builder
	 * @throws ServiceException the service exception
	 */
	public StringBuilder findCertificateAccreditation(StringBuilder stringBuilderBeginTableExclusion, StringBuilder stringBuilderEndTableExclusion,
																 Map parameters, StringBuilder strTableComponentBegin,
																 StringBuilder stringBuilderBeginTableInclusion,
																 StringBuilder stringBuilderEndTableInclusion,
																 StringBuilder strTableComponentEnd) throws ServiceException{
		
		
		/** getting data for do merging into dynamic query**/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		
		/** Start Query Base **/
		/** Declaration dynamics Table Component**/
		String strEntityCollection="";
		if(Validations.validateIsNotNullAndNotEmpty(billingService.getEntityCollection()) ){
			Integer entityCollection = billingService.getEntityCollection();
			if(entityCollection.equals(EntityCollectionType.PARTICIPANTS.getCode())){
				strEntityCollection= " T.ID_PARTICIPANT_FK ,";
			}
		}
		
		strTableComponentBegin.append(" SELECT ").append(strEntityCollection).append("  T.ID_SECURITY_CODE_FK ,  ");
		strTableComponentBegin.append(" TRUNC(T.OPERATION_DATE), T.OPERATION_NUMBER,  ");
		strTableComponentBegin.append(" T.SECURITY_CLASS, T.ID_HOLDER_FK,  ");
		strTableComponentBegin.append(" T.ID_ISSUANCE_CODE_FK, T.ID_HOLDER_ACCOUNT_FK, ")
								.append("  T.NEMONICO_HOLDER, T.ACT_ECO_PAR, T.CARTERA_PROPIA ");
		strTableComponentBegin.append(" FROM ( ");

		strTableComponentEnd.append(" ) T ");
		
		
		strTableComponentEnd.append(" ORDER BY T.OPERATION_NUMBER, ").append(strEntityCollection); //.append(" , ")
		strTableComponentEnd.append(" T.ID_SECURITY_CODE_FK ,    ");
		strTableComponentEnd.append(" TRUNC(T.OPERATION_DATE)   ");
		
		
		/** Declaration dynamics Table Exclusion**/
		stringBuilderBeginTableExclusion.append(" SELECT TE.ID_PARTICIPANT_FK ,       ");
		stringBuilderBeginTableExclusion.append("        TE.ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.SECURITY_CLASS, ");
		stringBuilderBeginTableExclusion.append("        TE.OPERATION_DATE, ");
		stringBuilderBeginTableExclusion.append("        TE.OPERATION_NUMBER, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_ISSUANCE_CODE_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_HOLDER_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_HOLDER_ACCOUNT_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.NEMONICO_HOLDER , ");
		stringBuilderBeginTableExclusion.append(" 		 TE.CARTERA_PROPIA ,   ");
		stringBuilderBeginTableExclusion.append(" 		 TE.ACT_ECO_PAR    ");
		stringBuilderBeginTableExclusion.append("        FROM ( ");		
		
		stringBuilderEndTableExclusion.append(" ) TE  WHERE  (1=1)     ");
		
		
		/** Declaration dynamics Table Inclusion**/
		stringBuilderBeginTableInclusion.append(" SELECT TI.ID_PARTICIPANT_FK,       ");
		stringBuilderBeginTableInclusion.append("        TI.ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.SECURITY_CLASS, ");
		stringBuilderBeginTableInclusion.append("        TI.OPERATION_DATE, ");
		stringBuilderBeginTableInclusion.append("        TI.OPERATION_NUMBER, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_ISSUANCE_CODE_FK, ");
		stringBuilderBeginTableInclusion.append(" 		 TI.ID_HOLDER_FK, ");
		stringBuilderBeginTableInclusion.append(" 		 TI.ID_HOLDER_ACCOUNT_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.NEMONICO_HOLDER , ");
		stringBuilderBeginTableInclusion.append(" 		 TI.CARTERA_PROPIA ,   ");
		stringBuilderBeginTableInclusion.append(" 		 TI.ACT_ECO_PAR    ");
		stringBuilderBeginTableInclusion.append("        FROM ( ");		
		
		stringBuilderEndTableInclusion.append(" ) TI  WHERE  (1=1)     "); 
		
				
		
		return strTableComponentBegin;		
		/**		FIN		**/	
	}
	
	
	/**
	 *  16.
	 *
	 * @param stringBuilderBeginTableExclusion the string builder begin table exclusion
	 * @param stringBuilderEndTableExclusion the string builder end table exclusion
	 * @param parameters the parameters
	 * @param strTableComponentBegin the str table component begin
	 * @param stringBuilderBeginTableInclusion the string builder begin table inclusion
	 * @param stringBuilderEndTableInclusion the string builder end table inclusion
	 * @param strTableComponentEnd the str table component end
	 * @return the string builder
	 * @throws ServiceException the service exception
	 */
	public StringBuilder findPayingAgent(StringBuilder stringBuilderBeginTableExclusion, StringBuilder stringBuilderEndTableExclusion,
																 Map parameters, StringBuilder strTableComponentBegin,
																 StringBuilder stringBuilderBeginTableInclusion,
																 StringBuilder stringBuilderEndTableInclusion,
																 StringBuilder strTableComponentEnd) throws ServiceException{
		
		
		/** getting data for do merging into dynamic query**/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		
		/** Start Query Base **/
		/** Declaration dynamics Table Component**/
		String strEntityCollection="";
		if(Validations.validateIsNotNullAndNotEmpty(billingService.getEntityCollection()) ){
			Integer entityCollection = billingService.getEntityCollection();
			if (entityCollection.equals(EntityCollectionType.HOLDERS.getCode())){
				strEntityCollection= " T.ID_HOLDER_FK ";
			}else if (entityCollection.equals(EntityCollectionType.ISSUERS.getCode())){
				strEntityCollection= " T.ID_ISSUER_FK ";
			}else if(entityCollection.equals(EntityCollectionType.PARTICIPANTS.getCode())){
				strEntityCollection= " T.ID_PARTICIPANT_FK ";
			}
		}
		
		strTableComponentBegin.append(" SELECT ").append(strEntityCollection).append(" , T.ID_SECURITY_CODE_FK ,  ");
		strTableComponentBegin.append(" T.CURRENCY, T.PRICE,  ");
		strTableComponentBegin.append(" TRUNC(T.OPERATION_DATE),   ");
		strTableComponentBegin.append(" '' AS PART_ORIG, '' AS PART_DEST, T.SECURITY_CLASS,   ");
		strTableComponentBegin.append(" T.OPERATION_NUMBER ");
		strTableComponentBegin.append(" FROM ( ");
		
		
		

		strTableComponentEnd.append(" ) T ");
		
		
		strTableComponentEnd.append(" ORDER BY T.OPERATION_NUMBER, ").append(strEntityCollection).append(" , ");
		strTableComponentEnd.append(" T.ID_SECURITY_CODE_FK ,    ");
		strTableComponentEnd.append(" TRUNC(T.OPERATION_DATE)   ");
		
		
		/** Declaration dynamics Table Exclusion**/
		stringBuilderBeginTableExclusion.append(" SELECT TE.ID_PARTICIPANT_FK ,       ");
		stringBuilderBeginTableExclusion.append("        TE.ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.SECURITY_CLASS, ");
		stringBuilderBeginTableExclusion.append("        TE.CURRENCY, ");
		stringBuilderBeginTableExclusion.append("        TE.OPERATION_DATE, ");
		stringBuilderBeginTableExclusion.append("        TE.OPERATION_NUMBER, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_ISSUANCE_CODE_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_HOLDER_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_HOLDER_ACCOUNT_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.PRICE, ");
		stringBuilderBeginTableExclusion.append("        TE.ID_ISSUER_FK ");
		
		
		stringBuilderBeginTableExclusion.append("        FROM ( ");		
		
		stringBuilderEndTableExclusion.append(" ) TE  WHERE  (1=1)     ");
		
		
		/** Declaration dynamics Table Inclusion**/
		stringBuilderBeginTableInclusion.append(" SELECT TI.ID_PARTICIPANT_FK,       ");
		stringBuilderBeginTableInclusion.append("        TI.ID_SECURITY_CODE_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.SECURITY_CLASS, ");
		stringBuilderBeginTableInclusion.append("        TI.CURRENCY, ");
		stringBuilderBeginTableInclusion.append("        TI.OPERATION_DATE, ");
		stringBuilderBeginTableInclusion.append("        TI.OPERATION_NUMBER, ");
		stringBuilderBeginTableInclusion.append("        TI.ID_ISSUANCE_CODE_FK, ");
		stringBuilderBeginTableInclusion.append(" 		 TI.ID_HOLDER_FK, ");
		stringBuilderBeginTableInclusion.append(" 		 TI.ID_HOLDER_ACCOUNT_FK, ");
		stringBuilderBeginTableInclusion.append(" 		 TI.PRICE, ");
		stringBuilderBeginTableInclusion.append(" 		 TI.ID_ISSUER_FK ");
		
		stringBuilderBeginTableInclusion.append("        FROM ( ");		
		
		stringBuilderEndTableInclusion.append(" ) TI  WHERE  (1=1)     "); 
		
				
		
		return strTableComponentBegin;		
		/**		FIN		**/	
	}
	
	
	/**
	 *  EVENTUAL CASE ISSUER  *.
	 *
	 * @param stringBuilderBeginTableExclusion the string builder begin table exclusion
	 * @param stringBuilderEndTableExclusion the string builder end table exclusion
	 * @param strTableComponentBegin the str table component begin
	 * @param stringBuilderBeginTableInclusion the string builder begin table inclusion
	 * @param stringBuilderEndTableInclusion the string builder end table inclusion
	 * @param strTableComponentEnd the str table component end
	 * @return the string builder
	 */
	public StringBuilder filterCorporativeIssuer(StringBuilder stringBuilderBeginTableExclusion, StringBuilder stringBuilderEndTableExclusion,
														StringBuilder strTableComponentBegin,
														StringBuilder stringBuilderBeginTableInclusion,
														StringBuilder stringBuilderEndTableInclusion,
														StringBuilder strTableComponentEnd){
		
		/** Declaration dynamics Table Component**/
		strTableComponentBegin.append("  select DISTINCT T.ID_ISSUER_FK, T.ID_SECURITY_CODE_FK from ( ");
		
		strTableComponentEnd.append(" )  T  where 1 = 1  ");
		
		
		/** Declaration dynamics Table Exclusion**/
		stringBuilderBeginTableExclusion.append(" SELECT TE.ID_SECURITY_CODE_FK , ");
		stringBuilderBeginTableExclusion.append("        TE.ID_ISSUER_FK, ");
		stringBuilderBeginTableExclusion.append("        TE.SECURITY_CLASS,");	
		stringBuilderBeginTableExclusion.append(" 		  '' AS ID_ISSUANCE_CODE_FK ");
		stringBuilderBeginTableExclusion.append("        from ( ");		
		
		stringBuilderEndTableExclusion.append(" ) TE  where  (1=1)     ");		
		
		
		/**	Declaration dynamics Table Inclusion	**/
		stringBuilderBeginTableInclusion.append(" SELECT TI.ID_SECURITY_CODE_FK , ");
		stringBuilderBeginTableInclusion.append("        TI.ID_ISSUER_FK, ");
		stringBuilderBeginTableInclusion.append("        TI.SECURITY_CLASS,");	
		stringBuilderBeginTableInclusion.append(" 		 '' AS ID_ISSUANCE_CODE_FK ");
		stringBuilderBeginTableInclusion.append("        from ( ");		
		
		stringBuilderEndTableInclusion.append(" ) TI  where  (1=1)     ");
		
		
		return strTableComponentBegin;		
		/**		FIN		**/	
		}
	
	/**
	 * 			END BASE COLLECTION EVENTUAL		*.
	 *
	 * @param stringBuilderBeginTableExclusion the string builder begin table exclusion
	 * @param stringBuilderEndTableExclusion the string builder end table exclusion
	 * @param parameters the parameters
	 * @param strTableComponentBegin the str table component begin
	 * @param stringBuilderBeginTableInclusion the string builder begin table inclusion
	 * @param stringBuilderEndTableInclusion the string builder end table inclusion
	 * @param strTableComponentEnd the str table component end
	 * @return the string builder
	 */
		
	
	/** ENTITY BASE COLLECTION  **/	
	public StringBuilder processForEntity(StringBuilder stringBuilderBeginTableExclusion, StringBuilder stringBuilderEndTableExclusion,
														Map parameters, StringBuilder strTableComponentBegin,
														StringBuilder stringBuilderBeginTableInclusion,
														StringBuilder stringBuilderEndTableInclusion,
														StringBuilder strTableComponentEnd){
 
		
		 
		Long participantId = null;
		Long holderId = null;
		
		if(Validations.validateIsNotNull(parameters.get(GeneralConstants.ID_PARTICIPANT_PK))){
			participantId = Long.parseLong(parameters.get(GeneralConstants.ID_PARTICIPANT_PK).toString());
		}
		
		if(Validations.validateIsNotNull(parameters.get(GeneralConstants.ID_HOLDER_PK))){
			holderId = Long.parseLong(parameters.get(GeneralConstants.ID_HOLDER_PK).toString());
		}
		
		
		if( Validations.validateIsNotNull(holderId) ){
			strTableComponentBegin.append("  select DISTINCT T.ID_HOLDER_FK from (      ");
			stringBuilderBeginTableExclusion.append(" SELECT TE.ID_HOLDER_FK,          ");
			stringBuilderBeginTableExclusion.append("        '' AS ID_ISSUER_FK,       ");
			stringBuilderBeginTableExclusion.append("        '' AS ID_PARTICIPANT_FK , ");
			stringBuilderBeginTableExclusion.append("        '' AS ID_HOLDER_ACCOUNT_FK , ");		
			stringBuilderBeginTableExclusion.append(" 		  '' AS ID_SECURITY_CODE_FK ,      ");
			stringBuilderBeginTableExclusion.append("        '' AS SECURITY_CLASS,        ");	
			stringBuilderBeginTableExclusion.append(" 		  '' AS ID_ISSUANCE_CODE_FK    ");
			stringBuilderBeginTableExclusion.append("        from ( ");
		}
		
		if( Validations.validateIsNotNull(parameters.get(GeneralConstants.ID_ISSUER_PK)) ){
			strTableComponentBegin.append("  select DISTINCT T.ID_ISSUER_FK from (      ");
			stringBuilderBeginTableExclusion.append(" SELECT '' AS ID_HOLDER_FK ,      ");
			stringBuilderBeginTableExclusion.append("        TE.ID_ISSUER_FK,          ");
			stringBuilderBeginTableExclusion.append("        '' AS ID_PARTICIPANT_FK , ");
			stringBuilderBeginTableExclusion.append("        '' AS ID_HOLDER_ACCOUNT_FK , ");		
			stringBuilderBeginTableExclusion.append(" 		  '' AS ID_SECURITY_CODE_FK ,      ");
			stringBuilderBeginTableExclusion.append("        '' AS SECURITY_CLASS,        ");	
			stringBuilderBeginTableExclusion.append(" 		  '' AS ID_ISSUANCE_CODE_FK    ");
			stringBuilderBeginTableExclusion.append("        from ( ");
		}				 
		
		if( Validations.validateIsNotNull(participantId) ){
			strTableComponentBegin.append("  select DISTINCT T.ID_PARTICIPANT_FK from (");
			stringBuilderBeginTableExclusion.append(" SELECT '' AS ID_HOLDER_FK ,     ");
			stringBuilderBeginTableExclusion.append("        '' AS ID_ISSUER_FK,      ");
			stringBuilderBeginTableExclusion.append("        TE.ID_PARTICIPANT_FK ,   ");
			stringBuilderBeginTableExclusion.append("        '' AS ID_HOLDER_ACCOUNT_FK , ");		
			stringBuilderBeginTableExclusion.append(" 		  '' AS ID_SECURITY_CODE_FK ,      ");
			stringBuilderBeginTableExclusion.append("        '' AS SECURITY_CLASS,        ");	
			stringBuilderBeginTableExclusion.append(" 		  '' AS ID_ISSUANCE_CODE_FK    ");
			stringBuilderBeginTableExclusion.append("        from ( ");
		}
		 
		stringBuilderEndTableExclusion.append(" ) TE  where  (1=1)     ");		
		
		
		
		strTableComponentEnd.append(" )  T  where 1 = 1  ");
		
					
		/**	Declaration dynamics Table Inclusion	**/	
		if( Validations.validateIsNotNull(holderId) ){
			stringBuilderBeginTableInclusion.append(" SELECT TI.ID_HOLDER_FK,          ");
			stringBuilderBeginTableInclusion.append("        '' AS ID_ISSUER_FK,       ");
			stringBuilderBeginTableInclusion.append("        '' AS ID_PARTICIPANT_FK , ");
			stringBuilderBeginTableInclusion.append("        '' AS ID_HOLDER_ACCOUNT_FK ,");
			stringBuilderBeginTableInclusion.append(" 		 '' AS ID_SECURITY_CODE_FK ,     ");		
			stringBuilderBeginTableInclusion.append("        '' AS SECURITY_CLASS,       ");	
			stringBuilderBeginTableInclusion.append(" 		 '' AS ID_ISSUANCE_CODE_FK   ");
			stringBuilderBeginTableInclusion.append("        from ( ");
		}
		
		if( Validations.validateIsNotNull(parameters.get(GeneralConstants.ID_ISSUER_PK)) ){
			stringBuilderBeginTableInclusion.append(" SELECT '' AS ID_HOLDER_FK ,      ");
			stringBuilderBeginTableInclusion.append("        TI.ID_ISSUER_FK,          ");
			stringBuilderBeginTableInclusion.append("        '' AS ID_PARTICIPANT_FK , ");
			stringBuilderBeginTableInclusion.append("        '' AS ID_HOLDER_ACCOUNT_FK ,");
			stringBuilderBeginTableInclusion.append(" 		 '' AS ID_SECURITY_CODE_FK ,     ");		
			stringBuilderBeginTableInclusion.append("        '' AS SECURITY_CLASS,       ");	
			stringBuilderBeginTableInclusion.append(" 		 '' AS ID_ISSUANCE_CODE_FK   ");
			stringBuilderBeginTableInclusion.append("        from ( ");
		}
		
		if( Validations.validateIsNotNull(participantId) ){
			stringBuilderBeginTableInclusion.append(" SELECT '' AS ID_HOLDER_FK ,     ");
			stringBuilderBeginTableInclusion.append("        '' AS ID_ISSUER_FK,      ");
			stringBuilderBeginTableInclusion.append("        TI.ID_PARTICIPANT_FK ,   ");
			stringBuilderBeginTableInclusion.append("        '' AS ID_HOLDER_ACCOUNT_FK ,");
			stringBuilderBeginTableInclusion.append(" 		 '' AS ID_SECURITY_CODE_FK ,     ");		
			stringBuilderBeginTableInclusion.append("        '' AS SECURITY_CLASS,       ");	
			stringBuilderBeginTableInclusion.append(" 		 '' AS ID_ISSUANCE_CODE_FK   ");
			stringBuilderBeginTableInclusion.append("        from ( ");
		}
			
				
		
		stringBuilderEndTableInclusion.append(" ) TI  where  (1=1)     ");
		
		
		return strTableComponentBegin;		
		/**		FIN		**/	
	}
			
	
	/**
	 *  ENTITY BASE COLLECTION  *.
	 *
	 * @param stringBuilderBeginTableExclusion the string builder begin table exclusion
	 * @param stringBuilderEndTableExlclusion the string builder end table exlclusion
	 * @param parameters the parameters
	 * @param strTableComponentBegin the str table component begin
	 * @param stringBuilderBeginTableInclusion the string builder begin table inclusion
	 * @param stringBuilderEndTableInclusion the string builder end table inclusion
	 * @param strTableComponentEnd the str table component end
	 * @return the string builder
	 */	
	public StringBuilder processForIssuer(StringBuilder stringBuilderBeginTableExclusion, StringBuilder stringBuilderEndTableExlclusion,
														Map parameters, StringBuilder strTableComponentBegin,
														StringBuilder stringBuilderBeginTableInclusion,
														StringBuilder stringBuilderEndTableInclusion,
														StringBuilder strTableComponentEnd){
 
		
		 
		String issuerId = null;
		String securityCode=null;
		
		
		if(Validations.validateIsNotNull(parameters.get(GeneralConstants.ID_ISSUER_PK))){
			issuerId =  parameters.get(GeneralConstants.ID_ISSUER_PK).toString();
		}
		if(Validations.validateIsNotNull(parameters.get(GeneralConstants.ID_SECURITY_CODE))){
			securityCode =  parameters.get(GeneralConstants.ID_SECURITY_CODE).toString();
		} 
		
		
		if( Validations.validateIsNotNull(issuerId) ){
			strTableComponentBegin.append("  select  T.ID_ISSUER_FK,      ");
			strTableComponentBegin.append("     T.ID_HOLDER_ACCOUNT_FK,    ");
			strTableComponentBegin.append("    T.ID_PARTICIPANT_FK,     ");
			strTableComponentBegin.append("   T.ID_HOLDER_FK,   ");
			strTableComponentBegin.append("     T.ID_SECURITY_CODE_FK,    ");
			strTableComponentBegin.append("      T.SECURITY_CLASS,      ");
			strTableComponentBegin.append("     T.ID_ISSUANCE_CODE_FK   FROM (  ");
			
			stringBuilderBeginTableExclusion.append("  SELECT TE.ID_HOLDER_ACCOUNT_FK,           ");
			stringBuilderBeginTableExclusion.append("         TE.ID_HOLDER_FK,           ");
			stringBuilderBeginTableExclusion.append("        TE.ID_PARTICIPANT_FK ,   ");
			stringBuilderBeginTableExclusion.append("         TE.ID_SECURITY_CODE_FK , ");		
			stringBuilderBeginTableExclusion.append(" 		     TE.ID_ISSUER_FK ,          ");
			stringBuilderBeginTableExclusion.append("       TE.SECURITY_CLASS,         ");	
			stringBuilderBeginTableExclusion.append(" 		    TE.ID_ISSUANCE_CODE_FK     ");
			stringBuilderBeginTableExclusion.append("        from ( ");
		}
		 	
		 
		stringBuilderEndTableExlclusion.append(" ) TE  where  (1=1)     ");		
		
		
		
		strTableComponentEnd.append(" )  T  where 1 = 1  ");
		
					
		/**	Declaration dynamics Table Inclusion	**/	
		if( Validations.validateIsNotNull(issuerId) ){
			
			stringBuilderBeginTableInclusion.append("  SELECT TI.ID_HOLDER_ACCOUNT_FK,           ");
			stringBuilderBeginTableInclusion.append("         TI.ID_HOLDER_FK,           ");
			stringBuilderBeginTableInclusion.append("        TI.ID_PARTICIPANT_FK ,   ");
			stringBuilderBeginTableInclusion.append("         TI.ID_SECURITY_CODE_FK , ");		
			stringBuilderBeginTableInclusion.append(" 		  TI.ID_ISSUER_FK ,          ");
			stringBuilderBeginTableInclusion.append("       TI.SECURITY_CLASS,         ");	
			stringBuilderBeginTableInclusion.append(" 		    TI.ID_ISSUANCE_CODE_FK     ");
			stringBuilderBeginTableInclusion.append("        from ( ");
			
		}
		stringBuilderEndTableInclusion.append(" ) TI  where  (1=1)     ");
		
		
		return strTableComponentBegin;		
		/**		FIN		**/	
	}
	
	/**
	 * 	 BASE COLLECTION ENTITY ACCESS	*.
	 *
	 * @param stringBuilderBeginTableExclusion the string builder begin table exclusion
	 * @param stringBuilderEndTableExlclusion the string builder end table exlclusion
	 * @param parameters the parameters
	 * @param strTableComponentBegin the str table component begin
	 * @param stringBuilderBeginTableInclusion the string builder begin table inclusion
	 * @param stringBuilderEndTableInclusion the string builder end table inclusion
	 * @param strTableComponentEnd the str table component end
	 * @return the string builder
	 */	
	public StringBuilder filterAccessQuotaForEntity(StringBuilder stringBuilderBeginTableExclusion, StringBuilder stringBuilderEndTableExlclusion,
														Map parameters, StringBuilder strTableComponentBegin,
														StringBuilder stringBuilderBeginTableInclusion,
														StringBuilder stringBuilderEndTableInclusion,
														StringBuilder strTableComponentEnd){
 
		/** getting data for do merging into dynamic query**/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		
		/** Declaration dynamics Table Exclusion**/
		
		if(  billingService.getEntityCollection().equals(EntityCollectionType.ISSUERS.getCode())  ){
			strTableComponentBegin.append("  select DISTINCT T.ID_ISSUER_FK from (      ");
			stringBuilderBeginTableExclusion.append(" SELECT '' AS ID_HOLDER_FK ,      ");
			stringBuilderBeginTableExclusion.append("        TE.ID_ISSUER_FK,          ");
			stringBuilderBeginTableExclusion.append("        '' AS ID_PARTICIPANT_FK , ");
			stringBuilderBeginTableExclusion.append("        '' AS ID_HOLDER_ACCOUNT_FK , ");		
			stringBuilderBeginTableExclusion.append(" 		  '' AS ID_SECURITY_CODE_FK ,      ");
			stringBuilderBeginTableExclusion.append("        '' AS SECURITY_CLASS,        ");	
			stringBuilderBeginTableExclusion.append(" 		  '' AS ID_ISSUANCE_CODE_FK    ");
			stringBuilderBeginTableExclusion.append("        from ( ");	
		}		
		
		else if(  billingService.getEntityCollection().equals(EntityCollectionType.PARTICIPANTS.getCode()) 
				||billingService.getEntityCollection().equals(EntityCollectionType.AFP.getCode()) ){
			strTableComponentBegin.append("  select DISTINCT T.ID_PARTICIPANT_FK from (");
			stringBuilderBeginTableExclusion.append(" SELECT '' AS ID_HOLDER_FK ,     ");
			stringBuilderBeginTableExclusion.append("        '' AS ID_ISSUER_FK,      ");
			stringBuilderBeginTableExclusion.append("        TE.ID_PARTICIPANT_FK ,   ");
			stringBuilderBeginTableExclusion.append("        '' AS ID_HOLDER_ACCOUNT_FK , ");		
			stringBuilderBeginTableExclusion.append(" 		  '' AS ID_SECURITY_CODE_FK ,      ");
			stringBuilderBeginTableExclusion.append("        '' AS SECURITY_CLASS,        ");	
			stringBuilderBeginTableExclusion.append(" 		  '' AS ID_ISSUANCE_CODE_FK    ");
			stringBuilderBeginTableExclusion.append("        from ( ");	
		}
		 
		
		strTableComponentEnd.append(" )  T  where 1 = 1  ");
		
		
		stringBuilderEndTableExlclusion.append(" ) TE  where  (1=1)     ");		
		
		
		/**	Declaration dynamics Table Inclusion	**/	

		if( billingService.getEntityCollection().equals(EntityCollectionType.ISSUERS.getCode()) ){
			stringBuilderBeginTableInclusion.append(" SELECT '' AS ID_HOLDER_FK ,      ");
			stringBuilderBeginTableInclusion.append("        TI.ID_ISSUER_FK,          ");
			stringBuilderBeginTableInclusion.append("        '' AS ID_PARTICIPANT_FK , ");
			stringBuilderBeginTableInclusion.append("        '' AS ID_HOLDER_ACCOUNT_FK ,");
			stringBuilderBeginTableInclusion.append(" 		 '' AS ID_SECURITY_CODE_FK ,     ");		
			stringBuilderBeginTableInclusion.append("        '' AS SECURITY_CLASS,       ");	
			stringBuilderBeginTableInclusion.append(" 		 '' AS ID_ISSUANCE_CODE_FK   ");
			stringBuilderBeginTableInclusion.append("        from ( ");	
		}
		
		if( billingService.getEntityCollection().equals(EntityCollectionType.PARTICIPANTS.getCode()) ){
			stringBuilderBeginTableInclusion.append(" SELECT '' AS ID_HOLDER_FK ,     ");
			stringBuilderBeginTableInclusion.append("        '' AS ID_ISSUER_FK,      ");
			stringBuilderBeginTableInclusion.append("        TI.ID_PARTICIPANT_FK ,   ");
			stringBuilderBeginTableInclusion.append("        '' AS ID_HOLDER_ACCOUNT_FK ,");
			stringBuilderBeginTableInclusion.append(" 		 '' AS ID_SECURITY_CODE_FK ,     ");		
			stringBuilderBeginTableInclusion.append("        '' AS SECURITY_CLASS,       ");	
			stringBuilderBeginTableInclusion.append(" 		 '' AS ID_ISSUANCE_CODE_FK   ");
			stringBuilderBeginTableInclusion.append("        from ( ");	
		}
			
			
		
		stringBuilderEndTableInclusion.append(" ) TI  where  (1=1)     ");
		
		
		return strTableComponentBegin;		
		/**		FIN		**/	
	}
	
	 
	
	/**
	 * 		Exclusion	*.
	 *
	 * @param stringBuilderBeginTableExclusion the string builder begin table exclusion
	 * @param stringBuilderSqlBase the string builder sql base
	 * @param stringBuilderEndTableExlclusion the string builder end table exlclusion
	 * @param strTableComponentBegin the str table component begin
	 * @param parameters the parameters
	 * @return the string builder
	 * @throws ServiceException the service exception
	 */
	public StringBuilder bodyExclusion(StringBuilder stringBuilderBeginTableExclusion, StringBuilder stringBuilderSqlBase, 
									   StringBuilder stringBuilderEndTableExlclusion, StringBuilder strTableComponentBegin,
									   Map parameters) throws ServiceException{
		
		/** getting data for do merging into dynamic query**/
		ServiceRate serviceRate=(ServiceRate) parameters.get(GeneralConstants.SERVICE_RATE);
		
		/** Counter to Exceptions Overflow*/
		int counter=0;
		parameters.put(GeneralConstants.COUNTER_EXCEPTIONS, counter);
		List<RateExclusion> rateExclusionList= rateExceptionServiceBean.findRateExclusionListByIdServiceRatePk(serviceRate.getIdServiceRatePk());			
		if (Validations.validateListIsNotNullAndNotEmpty(rateExclusionList)){
			
			/**making the table exclusion*/					
			stringBuilderBeginTableExclusion.append(stringBuilderSqlBase.toString()).append(stringBuilderEndTableExlclusion.toString());
			for (RateExclusion rateExclusion : rateExclusionList) {
			
				rateExceptionServiceBean.getAtributtesRateExclusion("TE", stringBuilderBeginTableExclusion, rateExclusion, parameters);
			}			
		 
			strTableComponentBegin.append(stringBuilderBeginTableExclusion.toString());
			
		}else{
		
			strTableComponentBegin.append(stringBuilderBeginTableExclusion.toString());
			strTableComponentBegin.append(stringBuilderSqlBase.toString());
			strTableComponentBegin.append(stringBuilderEndTableExlclusion.toString());
		
		} 
		
		return strTableComponentBegin;

	}
	
	
	/**
	 * 		Inclusion	*.
	 *
	 * @param stringBuilderBeginTableInclusion the string builder begin table inclusion
	 * @param stringBuilderSqlBase the string builder sql base
	 * @param stringBuilderEndTableInclusion the string builder end table inclusion
	 * @param strTableComponentBegin the str table component begin
	 * @param parameters the parameters
	 * @return the string builder
	 * @throws ServiceException the service exception
	 */
	public StringBuilder bodyInclusion(StringBuilder stringBuilderBeginTableInclusion, StringBuilder stringBuilderSqlBase,
									   StringBuilder stringBuilderEndTableInclusion, StringBuilder strTableComponentBegin,
									   Map parameters) throws ServiceException{
		
		/** getting data for do merging into dynamic query**/
		ServiceRate serviceRate=(ServiceRate) parameters.get(GeneralConstants.SERVICE_RATE);
		
		List<RateInclusion> rateInclusionList= rateExceptionServiceBean.findRateInclusionListByIdServiceRatePk(serviceRate.getIdServiceRatePk());
		
		if (Validations.validateListIsNotNullAndNotEmpty(rateInclusionList)){
			
			/**making the table inclusion*/
			stringBuilderBeginTableInclusion.append(stringBuilderSqlBase.toString()).append(stringBuilderEndTableInclusion.toString());		
			int index=0;
			for (RateInclusion rateInclusion : rateInclusionList) {
				
				rateExceptionServiceBean.getAtributtesRateInclusion("TI", stringBuilderBeginTableInclusion, rateInclusion,index, parameters);
				index++;
			}
				
			strTableComponentBegin.append(PropertiesConstants.CONNECTOR_UNION);
			strTableComponentBegin.append(stringBuilderBeginTableInclusion.toString());			 
			
		}
		
		
		return strTableComponentBegin;
		
	}
	
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * **
	 * COLLECTION EVENTUAL PAYING AGENT.
	 *
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<Object>  processSecurityCorporativePayingAgentHolder(Map parameters ) throws ServiceException {
	
	/**
	 * I have to recived the 
	 * BillingService 
	 * ServiceRate 
	 * Holder, HolderAccount
	 * IsinCoce 	
	 */
	 
	String issuer = null;
	String securityCode=null;

	
	if(Validations.validateIsNotNull(parameters.get(GeneralConstants.ID_ISSUER_PK))){
		issuer = parameters.get(GeneralConstants.ID_ISSUER_PK).toString();
	}

	
	if(Validations.validateIsNotNull(parameters.get(GeneralConstants.ID_SECURITY_CODE))){
		securityCode =  parameters.get(GeneralConstants.ID_SECURITY_CODE).toString();
	}


	StringBuilder strTableComponentBegin = new StringBuilder();
	
	
/** Start Query Base **/		
	StringBuilder stringBuilderSqlBase = new StringBuilder();		
	stringBuilderSqlBase.append(" SELECT CO.ID_ORIGIN_SECURITY_CODE_FK AS ID_SECURITY_CODE_FK,S.SECURITY_CLASS AS SECURITY_CLASS, ");
	stringBuilderSqlBase.append(" CO.DELIVERY_DATE AS DELIVERY_DATE, CO.PAYMENT_AMOUNT, CO.ID_ISSUER_FK, CO.CURRENCY,  ");
	stringBuilderSqlBase.append(" CET.CORPORATIVE_EVENT_TYPE, ");
	stringBuilderSqlBase.append(" CO.CORPORATIVE_EVENT_TYPE ");
	stringBuilderSqlBase.append(" FROM CORPORATIVE_OPERATION CO  ");
	stringBuilderSqlBase.append(" INNER JOIN CORPORATIVE_EVENT_TYPE CET ");
	stringBuilderSqlBase.append(" ON CO.CORPORATIVE_EVENT_TYPE=CET.ID_CORPORATIVE_EVENT_TYPE_PK  ");
	stringBuilderSqlBase.append(" LEFT OUTER JOIN SECURITY S ON ( CO.ID_ORIGIN_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK )   ");
	stringBuilderSqlBase.append(" WHERE 1=1   AND CET.CORPORATIVE_EVENT_TYPE IN (  ");//type interest payment and capital amortization
	stringBuilderSqlBase.append( ImportanceEventType.INTEREST_PAYMENT.getCode()).append(",");
	stringBuilderSqlBase.append( ImportanceEventType.CAPITAL_AMORTIZATION.getCode()).append(" ) ");
	stringBuilderSqlBase.append("   AND CO.STATE= ").append(CorporateProcessStateType.REGISTERED.getCode());


	if ( Validations.validateIsNotNull(issuer) ){
		stringBuilderSqlBase.append(" AND CO.ID_ISSUER_FK = :issuer  ");
	}
	if ( Validations.validateIsNotNull(securityCode) ){
		stringBuilderSqlBase.append(" AND CO.ID_ORIGIN_SECURITY_CODE_FK = :securityCode  ");
	}
	stringBuilderSqlBase.append(" ORDER BY CO.ID_ISSUER_FK  ");
	        
	/** Making the table EXCEPTION to Eventual Base Collection **/
	strTableComponentBegin = this.filterExceptionByBaseCollection(parameters, stringBuilderSqlBase);
         
	
	Query query = em.createNativeQuery(strTableComponentBegin.toString());	

	
	if ( Validations.validateIsNotNull(issuer) ){
		query.setParameter("issuer", issuer );
	}
	if ( Validations.validateIsNotNull(securityCode) ){
		query.setParameter("securityCode",securityCode);
	}
	 
	
	List<Object> listObject = null;
	try {
		
		  listObject = query.getResultList();
		  
	} catch (Exception ex) {			
		if(ex.getCause() instanceof SQLGrammarException){
			log.error(":::::::::QUERY GRAMMAR::::::::::::");
			throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
		}
		
	}
	
	return listObject;
}
	
	/**
	 *  
	 * 	BASE COLLECTION FOR ENTITY ACCESS.
	 *
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<CollectionRecordTo>  accessQuotaForEntity(Map parameters) throws ServiceException {
			
		/** getting data for do merging into dynamic query **/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		Date dateCalculation=(Date)parameters.get(GeneralConstants.CALCULATION_DATE);
		String strDate = CommonsUtilities.convertDatetoString(dateCalculation);
		
		List<Long> longPkParticipant = new ArrayList<Long>();
		List<String> longPkIssuer = new ArrayList<String>();
		
		List<UserAccountSession> usersAccounts= null;
	    Integer institution;
	    ParameterTableTO parameterTableTO = new ParameterTableTO();
	    ParameterTable parameterTable;
	    
	    List<CollectionRecordTo> collectionRecordToList=new ArrayList<CollectionRecordTo>(); 
	    	    	   
	    parameterTableTO.setParameterTablePk(billingService.getEntityCollection());
	    parameterTable= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO).get(0);	    	  
	    institution=parameterTable.getElementSubclasification1();
	
	    if(Validations.validateIsNotNull(institution)){
	     
	    	usersAccounts = clientRestService.getUsersInformationForBilling(UserAccountStateType.CONFIRMED.getCode(), institution , null,null,strDate);
	  
	    }
		
	    if(Validations.validateListIsNotNullAndNotEmpty(usersAccounts)){
	    
	    	for (UserAccountSession usersAccount : usersAccounts) {
				
				if(Validations.validateIsNotNull(usersAccount.getInstitutionType())){
					
					CollectionRecordTo collectionRecordTo=new CollectionRecordTo();
					
					if(     (usersAccount.getInstitutionType().equals(InstitutionType.PARTICIPANT.getCode()) )
							&& (Validations.validateIsNotNullAndPositive(usersAccount.getParticipantCode())) ){
						
						collectionRecordTo.setParticipant(new Participant(usersAccount.getParticipantCode()));
						collectionRecordTo.setUserCount(usersAccount.getCountUser());
						collectionRecordTo.setEntityCollection(EntityCollectionType.PARTICIPANTS.getCode());
						
						collectionRecordToList.add(collectionRecordTo);
					}
					else if(     usersAccount.getInstitutionType().equals(InstitutionType.AFP.getCode()) 
							&& (Validations.validateIsNotNullAndPositive(usersAccount.getParticipantCode())) ){
						
						collectionRecordTo.setParticipant(new Participant(usersAccount.getParticipantCode()));
						collectionRecordTo.setUserCount(usersAccount.getCountUser());
						collectionRecordTo.setEntityCollection(EntityCollectionType.AFP.getCode());
						
						collectionRecordToList.add(collectionRecordTo);
					}
					else if(usersAccount.getInstitutionType().equals(InstitutionType.ISSUER.getCode())
							&& (Validations.validateIsNotNullAndNotEmpty(usersAccount.getIssuerCode()))){
						
						collectionRecordTo.setIssuer(new Issuer(usersAccount.getIssuerCode()));
						collectionRecordTo.setUserCount(usersAccount.getCountUser());
						collectionRecordTo.setEntityCollection(EntityCollectionType.ISSUERS.getCode());
						
						collectionRecordToList.add(collectionRecordTo);
					}
					else if(usersAccount.getInstitutionType().equals(InstitutionType.BCR.getCode())){
						collectionRecordTo.setUserCount(usersAccount.getCountUser());
						collectionRecordTo.setEntityCollection(EntityCollectionType.CENTRAL_BANK.getCode());
						
						collectionRecordToList.add(collectionRecordTo);
					}else if (usersAccount.getInstitutionType().equals(InstitutionType.BOLSA.getCode())){
						collectionRecordTo.setUserCount(usersAccount.getCountUser());
						collectionRecordTo.setEntityCollection(EntityCollectionType.STOCK_EXCHANGE.getCode());
						
						collectionRecordToList.add(collectionRecordTo);
//					}else if(usersAccount.getInstitutionType().equals(InstitutionType.MIN_HAC.getCode())){
//						collectionRecordTo.setUserCount(usersAccount.getCountUser());
//						collectionRecordTo.setEntityCollection(EntityCollectionType.HACIENDA.getCode());
//						
//						collectionRecordToList.add(collectionRecordTo);
//					}
//					else if(usersAccount.getInstitutionType().equals(InstitutionType.REGULATOR_ENTITY.getCode())){
//						collectionRecordTo.setUserCount(usersAccount.getCountUser());
//						collectionRecordTo.setEntityCollection(EntityCollectionType.REGULATOR_ENTITY.getCode());
//						
//						collectionRecordToList.add(collectionRecordTo);
					}
					
					
					
				}
				
				
			}
	   
	   
	    	
	    if(!(billingService.getEntityCollection().equals(EntityCollectionType.ISSUERS.getCode())
	    		||billingService.getEntityCollection().equals(EntityCollectionType.PARTICIPANTS.getCode()) 
	    		||billingService.getEntityCollection().equals(EntityCollectionType.AFP.getCode()))){
	    	return collectionRecordToList;
	    }
	     
		/** MAKING FILTERS BY EACH ENTITY  */
	    
	    StringBuilder strTableComponentBegin = new StringBuilder();
		
		/** Start Query Base **/		
		StringBuilder stringBuilderSqlBase = new StringBuilder();	
		
		if( billingService.getEntityCollection().equals(EntityCollectionType.ISSUERS.getCode()) ){
			stringBuilderSqlBase.append(" select id_issuer_pk AS id_issuer_fk, '' ID_PARTICIPANT_FK , '' ID_SECURITY_CODE_FK, '' SECURITY_CLASS,  ");
			stringBuilderSqlBase.append(" '' ID_HOLDER_ACCOUNT_FK , '' ID_HOLDER_FK, '' ID_ISSUANCE_CODE_FK "); 
			stringBuilderSqlBase.append(" from ISSUER   where  ");
			stringBuilderSqlBase.append("  id_issuer_pk in ( :idIssuer )  ");
			
			
			for( CollectionRecordTo collectionRecordTo : collectionRecordToList ){
				
				longPkIssuer.add(collectionRecordTo.getIssuer().getIdIssuerPk()) ;
				
			}	
		}
		
		else if( billingService.getEntityCollection().equals(EntityCollectionType.PARTICIPANTS.getCode()) ||
				billingService.getEntityCollection().equals(EntityCollectionType.AFP.getCode())){
			stringBuilderSqlBase.append("  select ID_PARTICIPANT_PK as ID_PARTICIPANT_FK, '' ID_ISSUER_FK,          '' ID_HOLDER_FK,  ");
			stringBuilderSqlBase.append(" '' ID_SECURITY_CODE_FK,       '' SECURITY_CLASS,   '' ID_HOLDER_ACCOUNT_FK , '' ID_ISSUANCE_CODE_FK "); 
			stringBuilderSqlBase.append(" from PARTICIPANT  where  ");
			stringBuilderSqlBase.append(" ID_PARTICIPANT_PK in ( :idParticipant )  ");
			
			 
			for( CollectionRecordTo collectionRecordTo : collectionRecordToList ){
				
				longPkParticipant.add(collectionRecordTo.getParticipant().getIdParticipantPk()) ;
			}
		}
				
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters, stringBuilderSqlBase);		
		Query query = em.createNativeQuery(strTableComponentBegin.toString());	

		
		if( billingService.getEntityCollection().equals(EntityCollectionType.ISSUERS.getCode()) ){
			
			query.setParameter("idIssuer",  longPkIssuer );			
		}
		
		if( billingService.getEntityCollection().equals(EntityCollectionType.PARTICIPANTS.getCode()) ||
				billingService.getEntityCollection().equals(EntityCollectionType.AFP.getCode()) ){
			  
			query.setParameter("idParticipant", longPkParticipant );
		}
		
		 
		List<Object> listObject = null;
		try {
			
			  listObject = query.getResultList();
			  
		} catch (Exception ex) {			
			if(ex.getCause() instanceof SQLGrammarException){
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}else{
				throw new RuntimeException(ex.getMessage());
			}
		}

		List<Object> listUserNotFound=new ArrayList<Object>();
		if (Validations.validateListIsNotNullAndNotEmpty(listObject)){
									
				for( Iterator<CollectionRecordTo> iterOriginal=collectionRecordToList.iterator(); iterOriginal.hasNext(); ) {	
					CollectionRecordTo collectionRecordTo = (CollectionRecordTo) iterOriginal.next();
					Boolean exist = Boolean.FALSE;
					for( Iterator<Object> iter=listObject.iterator(); iter.hasNext(); ) {
						 Object object = (Object) iter.next();
						 
						 if( billingService.getEntityCollection().equals(EntityCollectionType.PARTICIPANTS.getCode()) ||
									billingService.getEntityCollection().equals(EntityCollectionType.AFP.getCode()) ){
							 
								 if( collectionRecordTo.getParticipant().getIdParticipantPk().equals(((BigDecimal)object).longValue()) ){		
									 
									 exist = Boolean.TRUE;
									 break;
								} 
								 
							 
						 }else if( billingService.getEntityCollection().equals(EntityCollectionType.ISSUERS.getCode()) ){
							
								 if( collectionRecordTo.getIssuer().getIdIssuerPk().equals(((String)object)) ){						
									 
									 exist = Boolean.TRUE;
									 break;
								 }
							}
						 
					}
					
					/**
					 * Aqui si no existe la entidad de cobro, notificar
					 * */
					if(!exist){
						listUserNotFound.add(collectionRecordTo.getParticipant().getIdParticipantPk());
						iterOriginal.remove();
					}
				}
				if(Validations.validateListIsNotNullAndNotEmpty(listUserNotFound)){
					BusinessProcess businessProcess = new BusinessProcess();
					
					businessProcess.setIdBusinessProcessPk(BusinessProcessType.MONTHLY_COLLECT_SERVICE_CALCULATION.getCode());
					
					String message = messageConfiguration.getProperty("body.message.warning.quota.access");					 
					message = MessageFormat.format(message, EntityCollectionType.get(billingService.getEntityCollection()),BillingUtils.convertListObjectToString(listUserNotFound,GeneralConstants.STR_COMMA_WITHOUT_SPACE)); 
					
					List<UserAccountSession> listUser
					=this.businessLayerCommonFacade.getListUsserByBusinessProcess(BusinessProcessType.MONTHLY_COLLECT_SERVICE_CALCULATION.getCode());
					
					businessLayerCommonFacade.sendNotificationManual(businessProcess, listUser, message, message, NotificationType.MESSAGE);
					businessLayerCommonFacade.sendNotificationManual(businessProcess, listUser, message, message, NotificationType.EMAIL);
				}
				
		 }
		
		
		  
	    }
		return collectionRecordToList;
	}
	
	/**
	 * Filter exception by list.
	 *
	 * @param parameters the parameters
	 * @param object the object
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Object> filterExceptionByList(Map parameters, List<Object> object) throws ServiceException{
		
		Integer lengthList=((Object[])object.get(0)).length;
		int indexEntity=0;
		/***
		 * Each Base collection, were created a list of the parameters how are they, in the following order
		 * 0: Participant
		 * 1: Holder
		 * 2: Security
		 * 3: Security Class
		 * 4: Holder Account
		 * 5: Issuer
		 * 6: Issuance
		 * 7: Length array to Object
		 */
		int[] INDEX_EXCEPTION = {
				0, 3, 4, 5, 6, 7, 10, lengthList
			};
		List<Object> listObject=null;

		listObject=getListToInclusion(parameters, object, INDEX_EXCEPTION);
		listObject=getListToExclusion(parameters, listObject, INDEX_EXCEPTION);
		
		/**
		 * Organize Entity Collection
		 */
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		
		if(Validations.validateIsNotNullAndNotEmpty(billingService.getEntityCollection()) ){
			Integer entityCollection = billingService.getEntityCollection();
			if (entityCollection.equals(EntityCollectionType.HOLDERS.getCode())){
				indexEntity=INDEX_EXCEPTION[IndexExceptionType.HOLDER.getCode()];
			}else if (entityCollection.equals(EntityCollectionType.ISSUERS.getCode())){
				indexEntity=INDEX_EXCEPTION[IndexExceptionType.ISSUER.getCode()];
			}else if(entityCollection.equals(EntityCollectionType.PARTICIPANTS.getCode())){
				indexEntity=INDEX_EXCEPTION[IndexExceptionType.PARTICIPANT.getCode()];
			}
		}
		
		for (Iterator iterator = listObject.iterator(); iterator.hasNext();) {
			Object[] objects = (Object[]) iterator.next();
			objects[0]=objects[indexEntity];
		}
		
		return listObject;
	}
	
	/**
	 * Gets the list to inclusion.
	 *
	 * @param parameters the parameters
	 * @param objectList the object list
	 * @param index the index
	 * @return the list to inclusion
	 * @throws ServiceException the service exception
	 */
	public List<Object>  getListToInclusion(Map parameters, List<Object> objectList, int[] index) throws ServiceException{
		/** getting data for do merging into dynamic query**/
		ServiceRate serviceRate=(ServiceRate) parameters.get(GeneralConstants.SERVICE_RATE);
		List<Object> objectListResult=null;
		List<RateInclusion> rateInclusionList= rateExceptionServiceBean.findRateInclusionListByIdServiceRatePk(serviceRate.getIdServiceRatePk());

		
		
		for (RateInclusion rateInclusion : rateInclusionList) {
			objectListResult= rateExceptionServiceBean.getListToInclusion(parameters, objectList,objectListResult, rateInclusion, index);
		}
		return objectListResult;
	}
	
	/**
	 * Gets the list to exclusion.
	 *
	 * @param parameters the parameters
	 * @param objectList the object list
	 * @param index the index
	 * @return the list to exclusion
	 * @throws ServiceException the service exception
	 */
	public List<Object>  getListToExclusion(Map parameters, List<Object> objectList, int[] index) throws ServiceException{
		/** getting data for do merging into dynamic query**/
		ServiceRate serviceRate=(ServiceRate) parameters.get(GeneralConstants.SERVICE_RATE);

		List<RateExclusion> rateExclusionList= rateExceptionServiceBean.findRateExclusionListByIdServiceRatePk(serviceRate.getIdServiceRatePk());

		for (RateExclusion rateExclusion : rateExclusionList) {
			objectList= rateExceptionServiceBean.getListToExclusion(parameters, objectList, rateExclusion, index);
		}
		
		return objectList;
	}
	
}
