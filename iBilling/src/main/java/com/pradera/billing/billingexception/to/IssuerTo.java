package com.pradera.billing.billingexception.to;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class IssuerTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class IssuerTo implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id issuer pk. */
	private String  	idIssuerPk;
	
	/** The business name. */
	private String  	businessName;
	
	/** The address issuer. */
	private String 		addressIssuer;
	
	/** The country. */
	private Integer 	country;
	
	/** The departament. */
	private Integer 	departament;
	
	/** The province. */
	private Integer 	province;
	
	/** The district. */
	private Integer 	district;
	
	/** The mnemonic. */
	private String		mnemonic;
	
	
	/**
	 * Instantiates a new issuer to.
	 */
	public IssuerTo() {
	}
	
	
	/**
	 * Instantiates a new issuer to.
	 *
	 * @param idIssuerPk the id issuer pk
	 */
	public IssuerTo(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}


	/**
	 * Gets the id issuer pk.
	 *
	 * @return the idIssuerPk
	 */
	public String getIdIssuerPk() {
		return idIssuerPk;
	}
	
	/**
	 * Sets the id issuer pk.
	 *
	 * @param idIssuerPk the idIssuerPk to set
	 */
	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}
	
	/**
	 * Gets the business name.
	 *
	 * @return the businessName
	 */
	public String getBusinessName() {
		return businessName;
	}
	
	/**
	 * Sets the business name.
	 *
	 * @param businessName the businessName to set
	 */
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	
	/**
	 * Gets the address issuer.
	 *
	 * @return the addressIssuer
	 */
	public String getAddressIssuer() {
		return addressIssuer;
	}
	
	/**
	 * Sets the address issuer.
	 *
	 * @param addressIssuer the addressIssuer to set
	 */
	public void setAddressIssuer(String addressIssuer) {
		this.addressIssuer = addressIssuer;
	}
	
	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public Integer getCountry() {
		return country;
	}
	
	/**
	 * Sets the country.
	 *
	 * @param country the country to set
	 */
	public void setCountry(Integer country) {
		this.country = country;
	}
	
	/**
	 * Gets the departament.
	 *
	 * @return the departament
	 */
	public Integer getDepartament() {
		return departament;
	}
	
	/**
	 * Sets the departament.
	 *
	 * @param departament the departament to set
	 */
	public void setDepartament(Integer departament) {
		this.departament = departament;
	}
	
	/**
	 * Gets the province.
	 *
	 * @return the province
	 */
	public Integer getProvince() {
		return province;
	}
	
	/**
	 * Sets the province.
	 *
	 * @param province the province to set
	 */
	public void setProvince(Integer province) {
		this.province = province;
	}
	
	/**
	 * Gets the district.
	 *
	 * @return the district
	 */
	public Integer getDistrict() {
		return district;
	}
	
	/**
	 * Sets the district.
	 *
	 * @param district the district to set
	 */
	public void setDistrict(Integer district) {
		this.district = district;
	}


	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}


	/**
	 * Sets the mnemonic.
	 *
	 * @param mnemonic the mnemonic to set
	 */
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}
	
	
	
	
	
}