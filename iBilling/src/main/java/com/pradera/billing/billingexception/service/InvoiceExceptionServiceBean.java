package com.pradera.billing.billingexception.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pradera.billing.billingexception.invoice.to.ExceptionInvoiceGenerationCutTo;
import com.pradera.billing.billingexception.invoice.to.ExceptionInvoiceGenerationTo;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.billing.BillingEntityException;
import com.pradera.model.billing.BillingSchedule;
import com.pradera.model.billing.ExceptionInvoiceGenerations;
import com.pradera.model.billing.ProcessedExceptionInvoice;
import com.pradera.model.billing.type.BillingPeriodType;
import com.pradera.model.billing.type.InvoiceExceptionStatus;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class InvoiceExceptionServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class InvoiceExceptionServiceBean  extends CrudDaoServiceBean{
	
	/** The log. */
	@Inject
	private PraderaLogger 	log;
	
	/** The general parameters service. */
	@EJB
	ParameterServiceBean 	generalParametersService;
	
	/** The parameter service. */
	@EJB
	ParameterServiceBean 	parameterService;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade 	helperComponentFacade;

	/**
	 * Save all processed exception invoice.
	 *
	 * @param listProcessedExceptionInvoice the list processed exception invoice
	 */
	public void saveAllProcessedExceptionInvoice(List<ProcessedExceptionInvoice> listProcessedExceptionInvoice){
		this.saveAll(listProcessedExceptionInvoice);
		log.debug("Save Successful the list Processed Exception Invoice");
	
	}
	
	/**
	 * Save all exception invoice.
	 *
	 * @param listExceptionInvoiceGeneration the list exception invoice generation
	 */
	public void saveAllExceptionInvoice(List<ExceptionInvoiceGenerations> listExceptionInvoiceGeneration){
		this.saveAll(listExceptionInvoiceGeneration);
		log.debug("Save Successful the list ");
	
	}
	
	
	/**
	 * Save processed exception invoice.
	 *
	 * @param processedExceptionInvoice the processed exception invoice
	 * @return the processed exception invoice
	 * @throws ServiceException the service exception
	 */
	public ProcessedExceptionInvoice saveProcessedExceptionInvoice(ProcessedExceptionInvoice processedExceptionInvoice)
			throws ServiceException{
		
		this.create(processedExceptionInvoice);
		log.debug("Save Successful");
		return processedExceptionInvoice;
	}
	
	/**
	 * Save exception invoice.
	 *
	 * @param exceptionInvoiceGeneration the exception invoice generation
	 * @return the exception invoice generations
	 * @throws ServiceException the service exception
	 */
	public ExceptionInvoiceGenerations saveExceptionInvoice(ExceptionInvoiceGenerations exceptionInvoiceGeneration) 
			throws ServiceException{				
		this.create(exceptionInvoiceGeneration);
		log.debug("Save Successful");
		return exceptionInvoiceGeneration;

	}
	
	/**
	 * Save billing schedule.
	 *
	 * @param billingSchedule the billing schedule
	 * @return the billing schedule
	 * @throws ServiceException the service exception
	 */
	public BillingSchedule saveBillingSchedule(BillingSchedule billingSchedule) 
			throws ServiceException{				
		this.create(billingSchedule);
		log.debug("Save Successful Billing Schedule id:"+billingSchedule.getIdBillingSchedulePk());
		return billingSchedule;

	}
	
	/**
	 * Save billing entity exception.
	 *
	 * @param billingEntityException the billing entity exception
	 * @return the billing entity exception
	 * @throws ServiceException the service exception
	 */
	public BillingEntityException saveBillingEntityException(BillingEntityException billingEntityException) 
			throws ServiceException{				
		this.create(billingEntityException);
		log.debug("Save Successful Billing Entity Exception id: "+billingEntityException.getIdBillingEntityExceptionPk());
		return billingEntityException;

	}

/**
 * Find invoice exception.
 *
 * @param filter the filter
 * @return the list
 */
//		
	public List<ExceptionInvoiceGenerations> findInvoiceException(ExceptionInvoiceGenerationTo filter){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select distinct eig From ExceptionInvoiceGenerations eig  ");
		sbQuery.append("  left join fetch eig.billingService  bs ");
		sbQuery.append("  left join fetch eig.billingEntityException bex ");
		sbQuery.append(" left join fetch eig.billingSchedule bsc ");
		sbQuery.append(" Where 1=1 ");
		
		if(Validations.validateIsNotNullAndPositive(filter.getBillingService().getIdBillingServicePk())){
			sbQuery.append(" AND eig.billingService.idBillingServicePk = :idBillingServicePk ");
		}

		if(Validations.validateIsNotNullAndPositive(filter.getExceptionState())){
			sbQuery.append(" AND eig.exceptionState = :exceptionState ");
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getTypeException())){
			sbQuery.append(" AND eig.typeException = :typeException ");
		}
					
		sbQuery.append(" ORDER BY  eig.billingService.idBillingServicePk,  eig.exceptionState,eig.typeException ");
		
		TypedQuery<ExceptionInvoiceGenerations> query =  em.createQuery(sbQuery.toString(),ExceptionInvoiceGenerations.class);
		

		
		if(Validations.validateIsNotNullAndPositive(filter.getBillingService().getIdBillingServicePk())){
			query.setParameter("idBillingServicePk",filter.getBillingService().getIdBillingServicePk());
		}

		if(Validations.validateIsNotNullAndPositive(filter.getExceptionState())){
			query.setParameter("exceptionState",filter.getExceptionState());
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getTypeException())){
			query.setParameter("typeException",filter.getTypeException());
		}
		
		List<ExceptionInvoiceGenerations> listInvoiceExc = query.getResultList();
		
		return listInvoiceExc;
		
	}
	
	/**
	 * Find billing service inclusion.
	 *
	 * @param filter the filter
	 * @return the list
	 */
	public List<ExceptionInvoiceGenerations> findBillingServiceInclusion(ExceptionInvoiceGenerationTo filter){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select distinct eig From ExceptionInvoiceGenerations eig  ");
		sbQuery.append("  left join fetch eig.billingService  bs ");
		sbQuery.append("  left join fetch eig.billingEntityException bex ");
		sbQuery.append(" left join fetch eig.billingSchedule bsc ");
		sbQuery.append(" Where 1=1 ");
		
		if(Validations.validateIsNotNullAndPositive(filter.getBillingService().getIdBillingServicePk())){
			sbQuery.append(" AND eig.billingService.idBillingServicePk = :idBillingServicePk ");
		}

		if(Validations.validateIsNotNullAndPositive(filter.getExceptionState())){
			sbQuery.append(" AND eig.exceptionState = :exceptionState ");
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getTypeException())){
			sbQuery.append(" AND eig.typeException = :typeException ");
		}
		 
					
		sbQuery.append(" ORDER BY  eig.billingService.idBillingServicePk,  eig.exceptionState,eig.typeException ");
		
		TypedQuery<ExceptionInvoiceGenerations> query =  em.createQuery(sbQuery.toString(),ExceptionInvoiceGenerations.class);
		

		
		if(Validations.validateIsNotNullAndPositive(filter.getBillingService().getIdBillingServicePk())){
			query.setParameter("idBillingServicePk",filter.getBillingService().getIdBillingServicePk());
		}

		if(Validations.validateIsNotNullAndPositive(filter.getExceptionState())){
			query.setParameter("exceptionState",filter.getExceptionState());
		}
		
		List<ExceptionInvoiceGenerations> listInvoiceExc = query.getResultList();
		
		return listInvoiceExc;
		
	}
	
	
	/**
	 * Metodo para actualizar la fecha de la excepcion de facturacion del BCB
	 * @param idBillingSchedulePk
	 * @param day
	 * @return
	 */
	public void saveExceptionBilling (Long idBillingSchedulePk, String day, LoggerUser loggerUser){
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer query = new StringBuffer(
				" Update BillingSchedule bSchedule set  "+				
				" bSchedule.dayOfMonth = :day , " +							
				" bSchedule.lastModifyUser = :parLastModifyUser , " +
				" bSchedule.lastModifyDate = :parLastModifyDate , " +
				" bSchedule.lastModifyIp = :parLastModifyIp , " +
				" bSchedule.lastModifyApp = :parLastModifyApp " +
				" where bSchedule.idBillingSchedulePk in (:idBSchedule) ");
		parameters.put("idBSchedule", idBillingSchedulePk);		
		parameters.put("day", day);				
		parameters.put("parLastModifyUser", loggerUser.getUserName());
		parameters.put("parLastModifyDate", loggerUser.getAuditTime());
		parameters.put("parLastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		parameters.put("parLastModifyIp", loggerUser.getIpAddress());
		updateByQuery(query.toString(), parameters);
				
	}
	
	
	/**
	 * Extrayendo el pk de la Excepcion del BCB
	 * @param idBillingSchedulePk
	 * @param day
	 * @return
	 */
	public List<Long> getIdBillingSchedule (){
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select bsc.idBillingSchedulePk ");
		sbQuery.append(" From ExceptionInvoiceGenerations eig ");
		sbQuery.append(" join eig.billingSchedule bsc ");
		sbQuery.append(" Where 1=1 ");
		sbQuery.append(" AND eig.exceptionState = "+InvoiceExceptionStatus.REGISTER.getCode());
		sbQuery.append(" AND bsc.collectionPeriod = "+ BillingPeriodType.MONTHLY.getCode());
		
		Query query = em.createQuery(sbQuery.toString());
		
		List<Long> IdBillingSckedule = query.getResultList();
			
		return IdBillingSckedule;		
	}
	
	/**
	 * Metodo que obtiene la lista de excepciones de facturacion segun la lista de billingschedule pk
	 * @param idListBillingSchedule
	 * @return
	 */
	public List<ExceptionInvoiceGenerationCutTo> getExceptionInvoiceGenerationsCutTo(Date cutDate){
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select                                                                                                                             ");
		sbQuery.append(" eig.id_exception_invoice_pk                                                                                                        ");
		sbQuery.append(" ,bs.reference_rate tarifa                                                                                                          ");
		sbQuery.append(" ,bs.service_code servicio                                                                                                          ");
		sbQuery.append(" ,(select description from parameter_table where parameter_table_pk = eig.type_exception) tipo                                      ");
		sbQuery.append(" ,(select description from parameter_table where parameter_table_pk = bsc.collection_period) periodo                                ");
		sbQuery.append(" ,bsc.day_of_month dia_mes                                                                                                          ");
		sbQuery.append(" ,bsc.day_of_week dia_semana                                                                                                        ");
		sbQuery.append(" ,decode(eig.ind_select_all,1,'NO','SI') incluidos_todos                                                                            ");
		sbQuery.append(" ,(SELECT LISTAGG((SELECT description FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = bee.entity_collection)||':'||                 ");
		sbQuery.append("     case when bee.entity_collection = 1406 then (select mnemonic from issuer where id_issuer_pk = bee.id_issuer_fk)                ");
		sbQuery.append("          when bee.entity_collection = 1407 then (select mnemonic from participant where id_participant_pk = bee.id_participant_fk) ");
		sbQuery.append("          when bee.entity_collection = 1408 then (select mnemonic from holder where id_holder_pk = bee.id_holder_fk)                ");
		sbQuery.append("     end                                                                                                                            ");
		sbQuery.append("     ,', ') WITHIN GROUP (ORDER BY bee.id_exception_invoice_fk)                                                                     ");
		sbQuery.append("     FROM billing_entity_exception bee                                                                                              ");
		sbQuery.append("     where bee.id_exception_invoice_fk = eig.id_exception_invoice_pk                                                                ");
		sbQuery.append("  ) AS entidad                                                                                                                      ");
		sbQuery.append(" ,to_char(:process_date,'DD')as nuevo_dia_mes                                                                 ");
		sbQuery.append(" from exception_invoice_generations eig                                                                                             ");
		sbQuery.append(" inner join billing_schedule bsc on bsc.id_billing_schedule_pk = eig.id_billing_schedule_fk                                         ");
		sbQuery.append(" inner join billing_service bs on bs.id_billing_service_pk = eig.id_billing_service_fk                                              ");
		sbQuery.append(" where eig.exception_state = 2075                                                                                         			");
		//sbQuery.append(" and bsc.id_billing_schedule_pk in (:list)                                                                                          ");
		sbQuery.append(" order by 2,3                                                                                                                       ");
		
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("process_date", cutDate);
		//query.setParameter("list", idListBillingSchedule);
		List<Object[]> listObj = query.getResultList();
		
		List<ExceptionInvoiceGenerationCutTo> list = new ArrayList<ExceptionInvoiceGenerationCutTo>();
		ExceptionInvoiceGenerationCutTo exceptionInvoiceGenerationCutTo;
		for(Object[] obj: listObj){
			exceptionInvoiceGenerationCutTo = new ExceptionInvoiceGenerationCutTo();
			exceptionInvoiceGenerationCutTo.setIdExceptionInvoicePk(new BigDecimal(obj[0].toString()).longValue());
			exceptionInvoiceGenerationCutTo.setRate((String)obj[1]);
			exceptionInvoiceGenerationCutTo.setServiceCode((String)obj[2]);
			exceptionInvoiceGenerationCutTo.setExceptionType((String)obj[3]);
			exceptionInvoiceGenerationCutTo.setCollectionPeriod((String)obj[4]);
			exceptionInvoiceGenerationCutTo.setMonthDay((String)obj[5]);
			exceptionInvoiceGenerationCutTo.setWeekDay((String)obj[6]);
			exceptionInvoiceGenerationCutTo.setIncludeAll((String)obj[7]);
			exceptionInvoiceGenerationCutTo.setEntity((String)obj[8]);
			exceptionInvoiceGenerationCutTo.setNewMonthDay((String)obj[9]);
			list.add(exceptionInvoiceGenerationCutTo);
		}
		
		return list;		
	}
	
}

