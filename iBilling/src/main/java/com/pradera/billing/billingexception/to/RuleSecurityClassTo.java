package com.pradera.billing.billingexception.to;

import java.io.Serializable;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RuleSecurityClassTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class RuleSecurityClassTo implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5971392722821878141L;
	
	/** The security class. */
	private Integer 	securityClass; /** paramater table. */
	
	private String  	descriptionSecurityClass;

	/**  0 : All Participant  1 : One Participant. */
	private Integer		indicatorOneOrAll;
	
	/**
	 * Gets the security class.
	 *
	 * @return the security class
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}
	
	/**
	 * Sets the security class.
	 *
	 * @param securityClass the new security class
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}
	
	/**
	 * Gets the description security class.
	 *
	 * @return the description security class
	 */
	public String getDescriptionSecurityClass() {
		return descriptionSecurityClass;
	}
	
	/**
	 * Sets the description security class.
	 *
	 * @param descriptionSecurityClass the new description security class
	 */
	public void setDescriptionSecurityClass(String descriptionSecurityClass) {
		this.descriptionSecurityClass = descriptionSecurityClass;
	}
	
	/**
	 * Gets the indicator one or all.
	 *
	 * @return the indicator one or all
	 */
	public Integer getIndicatorOneOrAll() {
		return indicatorOneOrAll;
	}
	
	/**
	 * Sets the indicator one or all.
	 *
	 * @param indicatorOneOrAll the new indicator one or all
	 */
	public void setIndicatorOneOrAll(Integer indicatorOneOrAll) {
		this.indicatorOneOrAll = indicatorOneOrAll;
	}
	
	

}
