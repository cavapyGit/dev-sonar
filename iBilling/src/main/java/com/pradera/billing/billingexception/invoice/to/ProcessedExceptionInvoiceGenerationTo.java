package com.pradera.billing.billingexception.invoice.to;
 

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.billing.ProcessedExceptionInvoice;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ProcessedExceptionInvoiceGenerationTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class ProcessedExceptionInvoiceGenerationTo implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id processed exception invoice pk. */
	private Long 				idProcessedExceptionInvoicePk;
	
	/** The exception state. */
	private Integer 			exceptionState;
	
	/** The ind integrated bill. */
	private Integer 			indIntegratedBill;

	/** The description exception state. */
	private String 				descriptionExceptionState;

	/** The exception invoice generation to. */
	private ExceptionInvoiceGenerationTo 			exceptionInvoiceGenerationTo;
	
	/** The processed exception invoice. */
	private ProcessedExceptionInvoice 				processedExceptionInvoice;

	/** The list exception invoice generation to. */
	private List<ExceptionInvoiceGenerationTo> 		listExceptionInvoiceGenerationTo;
	
	/** The data model exception invoice generation to. */
	private GenericDataModel<ExceptionInvoiceGenerationTo> 	dataModelExceptionInvoiceGenerationTo;
	
	/**
	 * Instantiates a new processed exception invoice generation to.
	 */
	public ProcessedExceptionInvoiceGenerationTo() {
		
		listExceptionInvoiceGenerationTo=new ArrayList<ExceptionInvoiceGenerationTo>();
		dataModelExceptionInvoiceGenerationTo=new GenericDataModel<ExceptionInvoiceGenerationTo>();
	}
	
	

	/**
	 * Instantiates a new processed exception invoice generation to.
	 *
	 * @param idProcessedExceptionInvoicePk the id processed exception invoice pk
	 * @param exceptionState the exception state
	 * @param indIntegratedBill the ind integrated bill
	 * @param listExceptionInvoiceGenerationTo the list exception invoice generation to
	 * @param dataModelExceptionInvoiceGenerationTo the data model exception invoice generation to
	 * @param exceptionInvoiceGenerationTo the exception invoice generation to
	 * @param descriptionExceptionState the description exception state
	 */
	public ProcessedExceptionInvoiceGenerationTo(
			Long idProcessedExceptionInvoicePk,
			Integer exceptionState,
			Integer indIntegratedBill,
			List<ExceptionInvoiceGenerationTo> listExceptionInvoiceGenerationTo,
			GenericDataModel<ExceptionInvoiceGenerationTo> dataModelExceptionInvoiceGenerationTo,
			ExceptionInvoiceGenerationTo exceptionInvoiceGenerationTo,
			String descriptionExceptionState) {
		this.idProcessedExceptionInvoicePk = idProcessedExceptionInvoicePk;
		this.exceptionState = exceptionState;
		this.indIntegratedBill = indIntegratedBill;
		this.listExceptionInvoiceGenerationTo = listExceptionInvoiceGenerationTo;
		this.dataModelExceptionInvoiceGenerationTo = dataModelExceptionInvoiceGenerationTo;
		this.exceptionInvoiceGenerationTo = exceptionInvoiceGenerationTo;
		this.descriptionExceptionState = descriptionExceptionState;
	}



	/**
	 * Instantiates a new processed exception invoice generation to.
	 *
	 * @param idProcessedExceptionInvoicePk the id processed exception invoice pk
	 * @param exceptionState the exception state
	 * @param listExceptionInvoiceGenerationTo the list exception invoice generation to
	 * @param dataModelExceptionInvoiceGenerationTo the data model exception invoice generation to
	 * @param exceptionInvoiceGenerationTo the exception invoice generation to
	 * @param descriptionExceptionState the description exception state
	 */
	public ProcessedExceptionInvoiceGenerationTo(
			Long idProcessedExceptionInvoicePk,
			Integer exceptionState,
			List<ExceptionInvoiceGenerationTo> listExceptionInvoiceGenerationTo,
			GenericDataModel<ExceptionInvoiceGenerationTo> dataModelExceptionInvoiceGenerationTo,
			ExceptionInvoiceGenerationTo exceptionInvoiceGenerationTo,
			String descriptionExceptionState) {
		this.idProcessedExceptionInvoicePk = idProcessedExceptionInvoicePk;
		this.exceptionState = exceptionState;
		this.listExceptionInvoiceGenerationTo = listExceptionInvoiceGenerationTo;
		this.dataModelExceptionInvoiceGenerationTo = dataModelExceptionInvoiceGenerationTo;
		this.exceptionInvoiceGenerationTo = exceptionInvoiceGenerationTo;
		this.descriptionExceptionState = descriptionExceptionState;
	}

	/**
	 * Gets the id processed exception invoice pk.
	 *
	 * @return the idProcessedExceptionInvoicePk
	 */
	public Long getIdProcessedExceptionInvoicePk() {
		return idProcessedExceptionInvoicePk;
	}

	/**
	 * Sets the id processed exception invoice pk.
	 *
	 * @param idProcessedExceptionInvoicePk the idProcessedExceptionInvoicePk to set
	 */
	public void setIdProcessedExceptionInvoicePk(Long idProcessedExceptionInvoicePk) {
		this.idProcessedExceptionInvoicePk = idProcessedExceptionInvoicePk;
	}

	/**
	 * Gets the exception state.
	 *
	 * @return the exceptionState
	 */
	public Integer getExceptionState() {
		return exceptionState;
	}

	/**
	 * Sets the exception state.
	 *
	 * @param exceptionState the exceptionState to set
	 */
	public void setExceptionState(Integer exceptionState) {
		this.exceptionState = exceptionState;
	}

	/**
	 * Gets the list exception invoice generation to.
	 *
	 * @return the listExceptionInvoiceGenerationTo
	 */
	public List<ExceptionInvoiceGenerationTo> getListExceptionInvoiceGenerationTo() {
		return listExceptionInvoiceGenerationTo;
	}

	/**
	 * Sets the list exception invoice generation to.
	 *
	 * @param listExceptionInvoiceGenerationTo the listExceptionInvoiceGenerationTo to set
	 */
	public void setListExceptionInvoiceGenerationTo(
			List<ExceptionInvoiceGenerationTo> listExceptionInvoiceGenerationTo) {
		this.listExceptionInvoiceGenerationTo = listExceptionInvoiceGenerationTo;
	}

	/**
	 * Gets the data model exception invoice generation to.
	 *
	 * @return the dataModelExceptionInvoiceGenerationTo
	 */
	public GenericDataModel<ExceptionInvoiceGenerationTo> getDataModelExceptionInvoiceGenerationTo() {
		return dataModelExceptionInvoiceGenerationTo;
	}

	/**
	 * Sets the data model exception invoice generation to.
	 *
	 * @param dataModelExceptionInvoiceGenerationTo the dataModelExceptionInvoiceGenerationTo to set
	 */
	public void setDataModelExceptionInvoiceGenerationTo(
			GenericDataModel<ExceptionInvoiceGenerationTo> dataModelExceptionInvoiceGenerationTo) {
		this.dataModelExceptionInvoiceGenerationTo = dataModelExceptionInvoiceGenerationTo;
	}

	/**
	 * Gets the exception invoice generation to.
	 *
	 * @return the exceptionInvoiceGenerationTo
	 */
	public ExceptionInvoiceGenerationTo getExceptionInvoiceGenerationTo() {
		return exceptionInvoiceGenerationTo;
	}

	/**
	 * Sets the exception invoice generation to.
	 *
	 * @param exceptionInvoiceGenerationTo the exceptionInvoiceGenerationTo to set
	 */
	public void setExceptionInvoiceGenerationTo(
			ExceptionInvoiceGenerationTo exceptionInvoiceGenerationTo) {
		this.exceptionInvoiceGenerationTo = exceptionInvoiceGenerationTo;
	}

	/**
	 * Gets the description exception state.
	 *
	 * @return the descriptionExceptionState
	 */
	public String getDescriptionExceptionState() {
		return descriptionExceptionState;
	}

	/**
	 * Sets the description exception state.
	 *
	 * @param descriptionExceptionState the descriptionExceptionState to set
	 */
	public void setDescriptionExceptionState(String descriptionExceptionState) {
		this.descriptionExceptionState = descriptionExceptionState;
	}

	/**
	 * Gets the ind integrated bill.
	 *
	 * @return the indIntegratedBill
	 */
	public Integer getIndIntegratedBill() {
		return indIntegratedBill;
	}

	/**
	 * Sets the ind integrated bill.
	 *
	 * @param indIntegratedBill the indIntegratedBill to set
	 */
	public void setIndIntegratedBill(Integer indIntegratedBill) {
		this.indIntegratedBill = indIntegratedBill;
	}



	/**
	 * Gets the processed exception invoice.
	 *
	 * @return the processedExceptionInvoice
	 */
	public ProcessedExceptionInvoice getProcessedExceptionInvoice() {
		return processedExceptionInvoice;
	}



	/**
	 * Sets the processed exception invoice.
	 *
	 * @param processedExceptionInvoice the processedExceptionInvoice to set
	 */
	public void setProcessedExceptionInvoice(
			ProcessedExceptionInvoice processedExceptionInvoice) {
		this.processedExceptionInvoice = processedExceptionInvoice;
	}
	


	
}