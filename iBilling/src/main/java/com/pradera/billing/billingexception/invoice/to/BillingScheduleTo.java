package com.pradera.billing.billingexception.invoice.to;
 

import java.io.Serializable;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingScheduleTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class BillingScheduleTo implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id billing schedule pk. */
	private Long 				idBillingSchedulePk;

	/** The collection period. */
	private Integer 			collectionPeriod;
	
	/** The day of week. */
	private String 				dayOfWeek;
		
	/** The day of month. */
	private String 				dayOfMonth;
	
	/** The month. */
	private String 				month;
	
	/** The year. */
	private String 				year;
	
	/** The ind end of month. */
	private Integer 			indEndOfMonth;
	
	/** The description day of week. */
	private String 				descriptionDayOfWeek;
	
	/** The description month. */
	private String 				descriptionMonth;
	
	/** The description collection period. */
	private String 				descriptionCollectionPeriod;
	
	
	/** The indBcbException. */
	private Integer 			indBcbException;

	/** The list exception invoice generation. */
	private List<ExceptionInvoiceGenerationTo> 	listExceptionInvoiceGeneration;
	
	/**
	 * Instantiates a new billing schedule to.
	 *
	 * @param idBillingSchedulePk the id billing schedule pk
	 * @param collectionPeriod the collection period
	 * @param dayOfWeek the day of week
	 * @param dayOfMonth the day of month
	 * @param month the month
	 * @param year the year
	 * @param indEndOfMonth the ind end of month
	 * @param listExceptionInvoiceGeneration the list exception invoice generation
	 */
	public BillingScheduleTo(Long idBillingSchedulePk,
			Integer collectionPeriod, String dayOfWeek, String dayOfMonth,
			String month, String year, Integer indEndOfMonth, Integer indBcbException,
			List<ExceptionInvoiceGenerationTo> listExceptionInvoiceGeneration) {

		this.idBillingSchedulePk = idBillingSchedulePk;
		this.collectionPeriod = collectionPeriod;
		this.dayOfWeek = dayOfWeek;
		this.dayOfMonth = dayOfMonth;
		this.month = month;
		this.year = year;
		this.indEndOfMonth=indEndOfMonth;
		this.indBcbException=indBcbException;
		this.listExceptionInvoiceGeneration = listExceptionInvoiceGeneration;
	}

	/**
	 * Instantiates a new billing schedule to.
	 */
	public BillingScheduleTo() {
	}

	/**
	 * Gets the id billing schedule pk.
	 *
	 * @return the idBillingSchedulePk
	 */
	public Long getIdBillingSchedulePk() {
		return idBillingSchedulePk;
	}

	/**
	 * Sets the id billing schedule pk.
	 *
	 * @param idBillingSchedulePk the idBillingSchedulePk to set
	 */
	public void setIdBillingSchedulePk(Long idBillingSchedulePk) {
		this.idBillingSchedulePk = idBillingSchedulePk;
	}

	/**
	 * Gets the description collection period.
	 *
	 * @return the descriptionCollectionPeriod
	 */
	public String getDescriptionCollectionPeriod() {
		return descriptionCollectionPeriod;
	}

	/**
	 * Sets the description collection period.
	 *
	 * @param descriptionCollectionPeriod the descriptionCollectionPeriod to set
	 */
	public void setDescriptionCollectionPeriod(String descriptionCollectionPeriod) {
		this.descriptionCollectionPeriod = descriptionCollectionPeriod;
	}

	/**
	 * Gets the collection period.
	 *
	 * @return the collectionPeriod
	 */
	public Integer getCollectionPeriod() {
		return collectionPeriod;
	}

	/**
	 * Sets the collection period.
	 *
	 * @param collectionPeriod the collectionPeriod to set
	 */
	public void setCollectionPeriod(Integer collectionPeriod) {
		this.collectionPeriod = collectionPeriod;
	}

	/**
	 * Gets the day of week.
	 *
	 * @return the dayOfWeek
	 */
	public String getDayOfWeek() {
		return dayOfWeek;
	}

	/**
	 * Sets the day of week.
	 *
	 * @param dayOfWeek the dayOfWeek to set
	 */
	public void setDayOfWeek(String dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	/**
	 * Gets the day of month.
	 *
	 * @return the dayOfMonth
	 */
	public String getDayOfMonth() {
		return dayOfMonth;
	}

	/**
	 * Sets the day of month.
	 *
	 * @param dayOfMonth the dayOfMonth to set
	 */
	public void setDayOfMonth(String dayOfMonth) {
		this.dayOfMonth = dayOfMonth;
	}

	/**
	 * Gets the month.
	 *
	 * @return the month
	 */
	public String getMonth() {
		return month;
	}

	/**
	 * Sets the month.
	 *
	 * @param month the month to set
	 */
	public void setMonth(String month) {
		this.month = month;
	}

	/**
	 * Gets the year.
	 *
	 * @return the year
	 */
	public String getYear() {
		return year;
	}

	/**
	 * Sets the year.
	 *
	 * @param year the year to set
	 */
	public void setYear(String year) {
		this.year = year;
	}

	/**
	 * Gets the list exception invoice generation.
	 *
	 * @return the listExceptionInvoiceGeneration
	 */
	public List<ExceptionInvoiceGenerationTo> getListExceptionInvoiceGeneration() {
		return listExceptionInvoiceGeneration;
	}

	/**
	 * Sets the list exception invoice generation.
	 *
	 * @param listExceptionInvoiceGeneration the listExceptionInvoiceGeneration to set
	 */
	public void setListExceptionInvoiceGeneration(
			List<ExceptionInvoiceGenerationTo> listExceptionInvoiceGeneration) {
		this.listExceptionInvoiceGeneration = listExceptionInvoiceGeneration;
	}

	

	/**
	 * Gets the description day of week.
	 *
	 * @return the descriptionDayOfWeek
	 */
	public String getDescriptionDayOfWeek() {
		return descriptionDayOfWeek;
	}

	/**
	 * Sets the description day of week.
	 *
	 * @param descriptionDayOfWeek the descriptionDayOfWeek to set
	 */
	public void setDescriptionDayOfWeek(String descriptionDayOfWeek) {
		this.descriptionDayOfWeek = descriptionDayOfWeek;
	}

	/**
	 * Gets the description month.
	 *
	 * @return the descriptionMonth
	 */
	public String getDescriptionMonth() {
		return descriptionMonth;
	}

	/**
	 * Sets the description month.
	 *
	 * @param descriptionMonth the descriptionMonth to set
	 */
	public void setDescriptionMonth(String descriptionMonth) {
		this.descriptionMonth = descriptionMonth;
	}

	/**
	 * Gets the ind end of month.
	 *
	 * @return the indEndOfMonth
	 */
	public Integer getIndEndOfMonth() {
		return indEndOfMonth;
	}

	/**
	 * Sets the ind end of month.
	 *
	 * @param indEndOfMonth the indEndOfMonth to set
	 */
	public void setIndEndOfMonth(Integer indEndOfMonth) {
		this.indEndOfMonth = indEndOfMonth;
	}

	public Integer getIndBcbException() {
		return indBcbException;
	}

	public void setIndBcbException(Integer indBcbException) {
		this.indBcbException = indBcbException;
	}
	
	
	
}