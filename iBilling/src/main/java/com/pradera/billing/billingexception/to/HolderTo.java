package com.pradera.billing.billingexception.to;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class HolderTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class HolderTo implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id participant pk. */
	private Long 		idParticipantPk;
	
	/** The id holder pk. */
	private Long 		idHolderPk;
	
	/** The full name holder. */
	private String 		fullNameHolder;
	
	/** The holder account pk. */
	private Long 		holderAccountPk;
	
	/** The account number pk. */
	private Long 		accountNumberPk;
	
	/** The account number consult. */
	private Integer 	accountNumberConsult;
	
	/** The id security code pk. */
	private String 		idSecurityCodePk;
	
	/** The is representative. */
	private Integer 	isRepresentative;
	
	/** The description representative. */
	private String 		descriptionRepresentative;
	
	/** The key participant holder account. */
	private String 		keyParticipantHolderAccount;
	
	/**
	 * Instantiates a new holder to.
	 */
	public HolderTo() {
	}
	

	/**
	 * Instantiates a new holder to.
	 *
	 * @param idHolderPk the id holder pk
	 */
	public HolderTo(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}

	

	/**
	 * Instantiates a new holder to.
	 *
	 * @param idHolderPk the id holder pk
	 * @param fullNameHolder the full name holder
	 */
	public HolderTo(Long idHolderPk, String fullNameHolder) {
		this.idHolderPk = idHolderPk;
		this.fullNameHolder = fullNameHolder;
	}


	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * Gets the id holder pk.
	 *
	 * @return the id holder pk
	 */
	public Long getIdHolderPk() {
		return idHolderPk;
	}

	/**
	 * Gets the full name holder.
	 *
	 * @return the full name holder
	 */
	public String getFullNameHolder() {
		return fullNameHolder;
	}

	/**
	 * Sets the full name holder.
	 *
	 * @param fullNameHolder the new full name holder
	 */
	public void setFullNameHolder(String fullNameHolder) {
		this.fullNameHolder = fullNameHolder;
	}

	/**
	 * Sets the id holder pk.
	 *
	 * @param idHolderPk the new id holder pk
	 */
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}
	
	/**
	 * Gets the holder account pk.
	 *
	 * @return the holder account pk
	 */
	public Long getHolderAccountPk() {
		return holderAccountPk;
	}

	/**
	 * Sets the holder account pk.
	 *
	 * @param holderAccountPk the new holder account pk
	 */
	public void setHolderAccountPk(Long holderAccountPk) {
		this.holderAccountPk = holderAccountPk;
	}

	/**
	 * Gets the account number pk.
	 *
	 * @return the account number pk
	 */
	public Long getAccountNumberPk() {
		return accountNumberPk;
	}

	/**
	 * Sets the account number pk.
	 *
	 * @param accountNumberPk the new account number pk
	 */
	public void setAccountNumberPk(Long accountNumberPk) {
		this.accountNumberPk = accountNumberPk;
	}

	
	/**
	 * Gets the id security code pk.
	 *
	 * @return the idSecurityCodePk
	 */
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	/**
	 * Sets the id security code pk.
	 *
	 * @param idSecurityCodePk the idSecurityCodePk to set
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	/**
	 * Gets the checks if is representative.
	 *
	 * @return the checks if is representative
	 */
	public Integer getIsRepresentative() {
		return isRepresentative;
	}

	/**
	 * Sets the checks if is representative.
	 *
	 * @param isRepresentative the new checks if is representative
	 */
	public void setIsRepresentative(Integer isRepresentative) {
		this.isRepresentative = isRepresentative;
	}

	/**
	 * Gets the description representative.
	 *
	 * @return the description representative
	 */
	public String getDescriptionRepresentative() {
		return descriptionRepresentative;
	}

	/**
	 * Sets the description representative.
	 *
	 * @param descriptionRepresentative the new description representative
	 */
	public void setDescriptionRepresentative(String descriptionRepresentative) {
		this.descriptionRepresentative = descriptionRepresentative;
	}

	/**
	 * Gets the key participant holder account.
	 *
	 * @return the key participant holder account
	 */
	public String getKeyParticipantHolderAccount() {
		keyParticipantHolderAccount = getIdHolderPk()+"-"+getAccountNumberPk()+"-"+getIdSecurityCodePk();
		return keyParticipantHolderAccount;
	}

	/**
	 * Sets the key participant holder account.
	 *
	 * @param keyParticipantHolderAccount the new key participant holder account
	 */
	public void setKeyParticipantHolderAccount(String keyParticipantHolderAccount) {
		this.keyParticipantHolderAccount = keyParticipantHolderAccount;
	}

	/**
	 * Gets the account number consult.
	 *
	 * @return the account number consult
	 */
	public Integer getAccountNumberConsult() {
		return accountNumberConsult;
	}

	/**
	 * Sets the account number consult.
	 *
	 * @param accountNumberConsult the new account number consult
	 */
	public void setAccountNumberConsult(Integer accountNumberConsult) {
		this.accountNumberConsult = accountNumberConsult;
	}



}
