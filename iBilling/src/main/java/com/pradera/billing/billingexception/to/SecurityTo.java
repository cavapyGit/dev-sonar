package com.pradera.billing.billingexception.to;

import java.io.Serializable;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SecurityTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class SecurityTo implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	
	
	/** The id security code pk. */
	private String 		idSecurityCodePk; //Id Security Code
	
	/** The description. */
	private String 		description;
	
	/** The mnemonic. */
	private String 		mnemonic;
	 
	
	/**
	 * Instantiates a new security to.
	 */
	public SecurityTo() {
	}


	
	/**
	 * Instantiates a new security to.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @param description the description
	 * @param mnemonic the mnemonic
	 */
	public SecurityTo(String idSecurityCodePk, String description,
			String mnemonic) {
		
		this.idSecurityCodePk = idSecurityCodePk;
		this.description = description;
		this.mnemonic = mnemonic;
	}



	/**
	 * Gets the id security code pk.
	 *
	 * @return the idSecurityCodePk
	 */
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}


	/**
	 * Sets the id security code pk.
	 *
	 * @param idSecurityCodePk the idSecurityCodePk to set
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}


	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}


	/**
	 * Sets the description.
	 *
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}


	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}


	/**
	 * Sets the mnemonic.
	 *
	 * @param mnemonic the mnemonic to set
	 */
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	


	
}
