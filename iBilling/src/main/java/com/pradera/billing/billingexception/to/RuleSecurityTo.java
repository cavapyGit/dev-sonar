package com.pradera.billing.billingexception.to;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RuleSecurityTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class RuleSecurityTo implements Serializable {

	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id security code pk. */
	private String 		idSecurityCodePk; //Id Security Code
	
	/** The description. */
	private String 		description;
	
	/** The security mnemonic consult. */
	private String 		securityMnemonicConsult;
	
	/** The security description consult. */
	private String 		securityDescriptionConsult;
	
/** The security selected. */
//	private Security	securitySelected;
	private SecurityTo	securitySelected;

	/**  0 : All Participant  1 : One Participant. */
	private Integer	indicatorOneOrAll;
	
	/**
	 * Instantiates a new rule security to.
	 */
	public RuleSecurityTo(){
		securitySelected=	new SecurityTo();
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Gets the indicator one or all.
	 *
	 * @return the indicator one or all
	 */
	public Integer getIndicatorOneOrAll() {
		return indicatorOneOrAll;
	}
	
	/**
	 * Sets the indicator one or all.
	 *
	 * @param indicatorOneOrAll the new indicator one or all
	 */
	public void setIndicatorOneOrAll(Integer indicatorOneOrAll) {
		this.indicatorOneOrAll = indicatorOneOrAll;
	}
	
	/**
	 * Gets the security mnemonic consult.
	 *
	 * @return the security mnemonic consult
	 */
	public String getSecurityMnemonicConsult() {
		return securityMnemonicConsult;
	}
	
	/**
	 * Sets the security mnemonic consult.
	 *
	 * @param securityMnemonicConsult the new security mnemonic consult
	 */
	public void setSecurityMnemonicConsult(String securityMnemonicConsult) {
		this.securityMnemonicConsult = securityMnemonicConsult;
	}
	
	/**
	 * Gets the security description consult.
	 *
	 * @return the security description consult
	 */
	public String getSecurityDescriptionConsult() {
		return securityDescriptionConsult;
	}
	
	/**
	 * Sets the security description consult.
	 *
	 * @param securityDescriptionConsult the new security description consult
	 */
	public void setSecurityDescriptionConsult(String securityDescriptionConsult) {
		this.securityDescriptionConsult = securityDescriptionConsult;
	}
	
//	public Security getSecuritySelected() {
//		return securitySelected;
//	}
//	public void setSecuritySelected(Security securitySelected) {
//		this.securitySelected = securitySelected;
//	}

	/**
 * Gets the id security code pk.
 *
 * @return the idSecurityCodePk
 */
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	/**
	 * Sets the id security code pk.
	 *
	 * @param idSecurityCodePk the idSecurityCodePk to set
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	/**
	 * Gets the security selected.
	 *
	 * @return the securitySelected
	 */
	public SecurityTo getSecuritySelected() {
		return securitySelected;
	}

	/**
	 * Sets the security selected.
	 *
	 * @param securitySelected the securitySelected to set
	 */
	public void setSecuritySelected(SecurityTo securitySelected) {
		this.securitySelected = securitySelected;
	}
	
	
	
}
