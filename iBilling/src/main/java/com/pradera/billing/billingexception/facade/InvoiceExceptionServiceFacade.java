package com.pradera.billing.billingexception.facade;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.billing.billing.service.BillingProcessedServiceBean;
import com.pradera.billing.billing.to.BillingProcessedFilter;
import com.pradera.billing.billingexception.invoice.to.BillingEntityExceptionTo;
import com.pradera.billing.billingexception.invoice.to.BillingScheduleTo;
import com.pradera.billing.billingexception.invoice.to.ExceptionInvoiceGenerationCutTo;
import com.pradera.billing.billingexception.invoice.to.ExceptionInvoiceGenerationTo;
import com.pradera.billing.billingexception.invoice.to.ProcessedExceptionInvoiceGenerationTo;
import com.pradera.billing.billingexception.service.InvoiceExceptionServiceBean;
import com.pradera.billing.billingexception.to.HolderTo;
import com.pradera.billing.billingexception.to.IssuerTo;
import com.pradera.billing.billingexception.to.ParticipantTo;
import com.pradera.billing.billingservice.to.BillingServiceTo;
import com.pradera.billing.common.facade.BusinessLayerCommonFacade;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.DayType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.helperui.service.IssuerQueryServiceBean;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.billing.BillingEntityException;
import com.pradera.model.billing.BillingSchedule;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.ExceptionInvoiceGenerations;
import com.pradera.model.billing.ProcessedExceptionInvoice;
import com.pradera.model.billing.type.EntityCollectionType;
import com.pradera.model.billing.type.InvoiceExceptionStatus;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MonthsOfTheYearType;
import com.pradera.model.issuancesecuritie.Issuer;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class InvoiceExceptionServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)

@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class InvoiceExceptionServiceFacade {
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry 	transactionRegistry;

	/** The bussiness layer common facade. */
	@EJB
	BusinessLayerCommonFacade 			bussinessLayerCommonFacade;
	
	/** The invoice exception service bean. */
	@EJB
	InvoiceExceptionServiceBean 		invoiceExceptionServiceBean;
	@EJB
	BillingProcessedServiceBean  billingProcessed;
	/** The issuer query service. */
	@EJB
	IssuerQueryServiceBean 				issuerQueryService;
	
	/**
	 * Preparate process invoice exception.
	 *
	 * @param listProcessedExceptionInvoiceGenerationTo the list processed exception invoice generation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	public List<ProcessedExceptionInvoice> preparateProcessInvoiceException(List<ProcessedExceptionInvoiceGenerationTo> listProcessedExceptionInvoiceGenerationTo) throws ServiceException
			 {
		List<ProcessedExceptionInvoice> listProcessedExceptionInvoice=new ArrayList<ProcessedExceptionInvoice>();
		ProcessedExceptionInvoice processedExceptionInvoice=null;
		
		for (Iterator iterator = listProcessedExceptionInvoiceGenerationTo.iterator(); iterator.hasNext();) {
			
			ProcessedExceptionInvoiceGenerationTo processedExceptionInvoiceGenerationTo = 
					(ProcessedExceptionInvoiceGenerationTo) iterator.next();
			
			processedExceptionInvoice=new ProcessedExceptionInvoice();
			
			processedExceptionInvoice.setExceptionState(InvoiceExceptionStatus.REGISTER.getCode());
			processedExceptionInvoice.setIndIntegratedBill(processedExceptionInvoiceGenerationTo.getIndIntegratedBill());
			processedExceptionInvoice.setExceptionInvoiceGenerations(preparateInvoiceException(
					processedExceptionInvoiceGenerationTo.getListExceptionInvoiceGenerationTo(),processedExceptionInvoice));
			listProcessedExceptionInvoice.add(processedExceptionInvoice);
		}
		
		return listProcessedExceptionInvoice;
	}
	
	/**
	 * Preparate invoice exception.
	 *
	 * @param listExceptionInvoiceGenerationTo the list exception invoice generation to
	 * @param processedExceptionInvoice the processed exception invoice
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	public List<ExceptionInvoiceGenerations> preparateInvoiceException(List<ExceptionInvoiceGenerationTo> listExceptionInvoiceGenerationTo,
											ProcessedExceptionInvoice processedExceptionInvoice)
			throws ServiceException {
		List<ExceptionInvoiceGenerations> listExceptionInvoiceGenerations=new ArrayList<ExceptionInvoiceGenerations>();
		ExceptionInvoiceGenerations exceptionInvoice=null;
		
		for (Iterator iterInvoiceException = listExceptionInvoiceGenerationTo.iterator(); iterInvoiceException.hasNext();) {
			ExceptionInvoiceGenerationTo exceptionInvoiceGenerationTo = 
					(ExceptionInvoiceGenerationTo) iterInvoiceException.next();
			
			exceptionInvoice=new ExceptionInvoiceGenerations();
			
			exceptionInvoice.setBillingService(new BillingService(exceptionInvoiceGenerationTo.getBillingService().getIdBillingServicePk()));
			exceptionInvoice.setExceptionDate(exceptionInvoiceGenerationTo.getExceptionDate());
			exceptionInvoice.setTypeException(exceptionInvoiceGenerationTo.getTypeException());
			exceptionInvoice.setIndSelectAll(exceptionInvoiceGenerationTo.getIndSelectAll());
			exceptionInvoice.setExceptionState(exceptionInvoiceGenerationTo.getExceptionState());
			exceptionInvoice.setProcessedExceptionInvoice(processedExceptionInvoice);
			
			/**Get object Billing Schedule*/
			BillingSchedule billingSchedule=new BillingSchedule();
			if(Validations.validateIsNotNull(exceptionInvoiceGenerationTo.getBillingSchedule())){
				
				BillingScheduleTo billingScheduleTo= exceptionInvoiceGenerationTo.getBillingSchedule();
				Integer collectionPeriod=billingScheduleTo.getCollectionPeriod();
				
				billingSchedule.setCollectionPeriod(collectionPeriod);
				billingSchedule.setListExceptionInvoiceGenerations(listExceptionInvoiceGenerations);
				if(PropertiesConstants.COLLECTION_PERIOD_DAILY_PK.equals(collectionPeriod)){
					
				}else if(PropertiesConstants.COLLECTION_PERIOD_WEEKLY_PK.equals(collectionPeriod)){
					
					billingSchedule.setDayOfWeek(billingScheduleTo.getDayOfWeek());
					
				}else if(PropertiesConstants.COLLECTION_PERIOD_MONTHLY_PK.equals(collectionPeriod)){
					
					billingSchedule.setDayOfMonth(billingScheduleTo.getDayOfMonth());
					billingSchedule.setIndEndOfMonth(billingScheduleTo.getIndEndOfMonth());
					
				}else if(PropertiesConstants.COLLECTION_PERIOD_QUARTERLY_PK.equals(collectionPeriod)){
					
					billingSchedule.setDayOfMonth(billingScheduleTo.getDayOfMonth());
					billingSchedule.setMonth(billingScheduleTo.getMonth());
					
				}else if(PropertiesConstants.COLLECTION_PERIOD_YEARLY_PK.equals(collectionPeriod)){
					
					billingSchedule.setDayOfMonth(billingScheduleTo.getDayOfMonth());
					billingSchedule.setMonth(billingScheduleTo.getMonth());
					
				}
				
				billingSchedule.setIndBcbException(billingScheduleTo.getIndBcbException());
			}
			/**Get object List Billing Entity*/
			List<BillingEntityException> listBillingEntityException=new ArrayList<BillingEntityException>();
			
			List<BillingEntityExceptionTo> listBillingEntityExceptionTo=exceptionInvoiceGenerationTo.getBillingEntityException();
			
			
			for (Iterator iterEntity = listBillingEntityExceptionTo.iterator(); iterEntity.hasNext();) {
				BillingEntityExceptionTo billingEntityExceptionTo = (BillingEntityExceptionTo) iterEntity.next();

				Integer entityCollection=billingEntityExceptionTo.getEntityCollectionId();
				
				BillingEntityException billingEntity=new BillingEntityException();
				billingEntity.setExceptionInvoiceGenerations(exceptionInvoice);
				billingEntity.setEntityCollectionId(entityCollection);
				
				if(EntityCollectionType.ISSUERS.getCode().equals(entityCollection)){
					
					billingEntity.setIssuer(new Issuer(billingEntityExceptionTo.getIssuer().getIdIssuerPk()));
					
				}else if(EntityCollectionType.PARTICIPANTS.getCode().equals(entityCollection)){
					
					billingEntity.setParticipant(new Participant(billingEntityExceptionTo.getIdParticipantPk()));
					
				}else if(EntityCollectionType.HOLDERS.getCode().equals(entityCollection)){
					
					billingEntity.setHolder(new Holder(billingEntityExceptionTo.getHolder().getIdHolderPk()));
					
				}else  if(EntityCollectionType.OTHERS.getCode().equals(entityCollection)){
					
					billingEntity.setOtherEntityDocument(billingEntityExceptionTo.getDescriptionEntityCode());
					billingEntity.setOtherEntityDescription(billingEntityExceptionTo.getDescriptionEntityCollection());
					
				}
				
				listBillingEntityException.add(billingEntity);
			}
			
			
			/****/
			exceptionInvoice.setBillingEntityException(listBillingEntityException);
			exceptionInvoice.setBillingSchedule(billingSchedule);
			
			listExceptionInvoiceGenerations.add(exceptionInvoice);
		}
		
		return listExceptionInvoiceGenerations;
	}
	
	/**
	 * Save processed invoice exception.
	 *
	 * @param listExceptionInvoiceGenerationTo the list exception invoice generation to
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	public void saveProcessedInvoiceException(List<ProcessedExceptionInvoiceGenerationTo> listExceptionInvoiceGenerationTo) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
	    
	    List<ProcessedExceptionInvoice> listProcessedExceptionInvoice=null;
	    
	    listProcessedExceptionInvoice=preparateProcessInvoiceException(listExceptionInvoiceGenerationTo);
	    
	    for (ProcessedExceptionInvoice processedExceptionInvoice : listProcessedExceptionInvoice) {

	    	invoiceExceptionServiceBean.saveProcessedExceptionInvoice(processedExceptionInvoice);
	    	
	    	saveInvoiceException(processedExceptionInvoice.getExceptionInvoiceGenerations());
	 
	    	
		}
	}
	
	/**
	 * Save invoice exception.
	 *
	 * @param listExceptionInvoiceGeneration the list exception invoice generation
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	public List<ExceptionInvoiceGenerations> saveInvoiceException(List<ExceptionInvoiceGenerations> listExceptionInvoiceGeneration) 
			throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
				
		
		for (ExceptionInvoiceGenerations exceptionInvoiceGenerations : listExceptionInvoiceGeneration) {
			invoiceExceptionServiceBean.saveBillingSchedule(exceptionInvoiceGenerations.getBillingSchedule());
		}
		
		invoiceExceptionServiceBean.saveAllExceptionInvoice(listExceptionInvoiceGeneration);
		
		for (ExceptionInvoiceGenerations exceptionInvoiceGenerations : listExceptionInvoiceGeneration) {
			List<BillingEntityException> listBillingEntity =exceptionInvoiceGenerations.getBillingEntityException();
			for (BillingEntityException billingEntityException : listBillingEntity) {
				invoiceExceptionServiceBean.saveBillingEntityException(billingEntityException);
				
			}
		}
		return listExceptionInvoiceGeneration;
	}
	
	/**
	 * Gets the exception invoice generations.
	 *
	 * @param id the id
	 * @return the exception invoice generations
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public ExceptionInvoiceGenerations getExceptionInvoiceGenerations(Long id){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		return invoiceExceptionServiceBean.find(ExceptionInvoiceGenerations.class, id);
	}
	
	/**
	 *  CHANGE STATES - DELETE INVOICE GENERATION.
	 *
	 * @param invoiceGeneration the invoice generation
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_MODIFICATION)
	public void deletedInvoiceGeneration(ExceptionInvoiceGenerations invoiceGeneration) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeDelete());
		
		invoiceExceptionServiceBean.update(invoiceGeneration);
	}
	
	/**
	 * Find invoice exception by filter.
	 *
	 * @param exceptionInvoiceGenerationTo the exception invoice generation to
	 * @return the list
	 */
	public  List<ExceptionInvoiceGenerations> findInvoiceExceptionByFilter(ExceptionInvoiceGenerationTo exceptionInvoiceGenerationTo){
		List<ExceptionInvoiceGenerations> listExceptionInvoiceExc=null;
		
		listExceptionInvoiceExc=invoiceExceptionServiceBean.findInvoiceException(exceptionInvoiceGenerationTo);
		
		return listExceptionInvoiceExc;
	}
	
	/**
	 * Search invoice exception.
	 *
	 * @param listParameters the list parameters
	 * @param exceptionInvoiceGenerationTo the exception invoice generation to
	 * @return the list
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<ExceptionInvoiceGenerationTo> searchInvoiceException(Map listParameters,ExceptionInvoiceGenerationTo exceptionInvoiceGenerationTo){
		
		List<ParameterTable> listCollectionPeriod=	(List<ParameterTable>) listParameters.get(GeneralConstants.COLLECTION_PERIOD);
		List<ParameterTable> listEntityCollection=	(List<ParameterTable>) listParameters.get(GeneralConstants.ENTITY_COLLECTION); 
		List<ParameterTable> listException=			(List<ParameterTable>) listParameters.get(GeneralConstants.EXCEPTION); 
		List<ParameterTable> listExceptionStatus=	(List<ParameterTable>) listParameters.get(GeneralConstants.EXCEPTION_STATUS);
		List<ExceptionInvoiceGenerations> listExceptionInvoiceExc=null;
		List<ExceptionInvoiceGenerationTo> listExceptionInvoiceGenerationTo=null;
		
		ExceptionInvoiceGenerationTo  exceptionInvoiceGeneration=null;
		BillingScheduleTo billingScheduleTo=null;
		
		
		/**
		 * Find Exception Invoice
		 */
		listExceptionInvoiceExc=invoiceExceptionServiceBean.findInvoiceException(exceptionInvoiceGenerationTo);
		
		if(Validations.validateListIsNotNullAndNotEmpty(listExceptionInvoiceExc)){
			listExceptionInvoiceGenerationTo=new ArrayList<ExceptionInvoiceGenerationTo>();
			
			for (ExceptionInvoiceGenerations exceptionInvoiceGenerationEntity : listExceptionInvoiceExc) {
				
				
				/** Exception */
				exceptionInvoiceGeneration=new ExceptionInvoiceGenerationTo();
				exceptionInvoiceGeneration.setIdExceptionInvoicePk(exceptionInvoiceGenerationEntity.getIdExceptionInvoicePk());
				exceptionInvoiceGeneration.setTypeException(exceptionInvoiceGenerationEntity.getTypeException());
				exceptionInvoiceGeneration.setExceptionDate(exceptionInvoiceGenerationEntity.getExceptionDate());
				exceptionInvoiceGeneration.setExceptionState(exceptionInvoiceGenerationEntity.getExceptionState());
				exceptionInvoiceGeneration.setIndSelectAll(exceptionInvoiceGenerationEntity.getIndSelectAll());
				exceptionInvoiceGeneration.setDescriptionExceptionState(bussinessLayerCommonFacade.getValueParameterTable(listExceptionStatus, 
																		exceptionInvoiceGenerationEntity.getExceptionState()));
				exceptionInvoiceGeneration.setDescriptionTypeException(bussinessLayerCommonFacade.getValueParameterTable(listException, 
																		exceptionInvoiceGenerationEntity.getTypeException()));
				exceptionInvoiceGeneration.setBillingService(new BillingServiceTo(exceptionInvoiceGenerationEntity.getBillingService().getIdBillingServicePk(),
																		exceptionInvoiceGenerationEntity.getBillingService().getServiceCode()));
				exceptionInvoiceGeneration.setDescriptionEntityCollection(bussinessLayerCommonFacade.getValueParameterTable(listEntityCollection, 
																		exceptionInvoiceGenerationEntity.getBillingService().getEntityCollection()));
				
				/** Schedule */
				billingScheduleTo=settingBillingSchedule(exceptionInvoiceGenerationEntity.getBillingSchedule(),listCollectionPeriod);
				exceptionInvoiceGeneration.setBillingSchedule(billingScheduleTo);
				
				/** Entity Exception, HAVE ENTITY */
				exceptionInvoiceGeneration.setBillingEntityException(settingEntityException(exceptionInvoiceGenerationEntity));
				
				/**Add Exception To List*/
				listExceptionInvoiceGenerationTo.add(exceptionInvoiceGeneration);
			}
			
			
		}
		return listExceptionInvoiceGenerationTo;
		
	}
	
	public BillingSchedule getScheduleByBillingService(BillingProcessedFilter billingProcessedFilter ){
		return billingProcessed.getScheduleByBillingService(billingProcessedFilter);
	}
	
	/**
	 * Setting Billing Schedule.
	 *
	 * @param billingSchedule the billing schedule
	 * @param listCollectionPeriod the list collection period
	 * @return the billing schedule to
	 */
	public BillingScheduleTo settingBillingSchedule(BillingSchedule billingSchedule,List<ParameterTable> listCollectionPeriod){
		
		Integer billingPeriod=billingSchedule.getCollectionPeriod();
		
		BillingScheduleTo billingScheduleTo=new BillingScheduleTo();
		billingScheduleTo.setCollectionPeriod(billingSchedule.getCollectionPeriod());
		billingScheduleTo.setDayOfMonth(billingSchedule.getDayOfMonth());
		billingScheduleTo.setDayOfWeek(billingSchedule.getDayOfWeek());
		billingScheduleTo.setMonth(billingSchedule.getMonth());
		billingScheduleTo.setYear(billingSchedule.getYear());
		billingScheduleTo.setIndEndOfMonth(billingSchedule.getIndEndOfMonth());
		billingScheduleTo.setDescriptionCollectionPeriod(bussinessLayerCommonFacade.getValueParameterTable(listCollectionPeriod, billingPeriod));
		
		if(PropertiesConstants.COLLECTION_PERIOD_WEEKLY_PK.equals(billingPeriod)){
			for (DayType type : DayType.list) {
				if(type.getCode().equals(Integer.parseInt(billingSchedule.getDayOfWeek()))){
					billingScheduleTo.setDescriptionDayOfWeek(type.getValue());
					break;
				}
			}
			
			
		}else if(PropertiesConstants.COLLECTION_PERIOD_QUARTERLY_PK.equals(billingPeriod)){
			for (MonthsOfTheYearType type : MonthsOfTheYearType.list) {
				if(type.getCode().equals(Integer.parseInt(billingSchedule.getMonth()))){
					billingScheduleTo.setDescriptionMonth(type.getValue());
					break;
				}
			}
			
		}else if(PropertiesConstants.COLLECTION_PERIOD_YEARLY_PK.equals(billingPeriod)){
			for (MonthsOfTheYearType type : MonthsOfTheYearType.list) {
				if(type.getCode().equals(Integer.parseInt(billingSchedule.getMonth()))){
					billingScheduleTo.setDescriptionMonth(type.getValue());
					break;
				}
			}
			
			
		}
		
		return billingScheduleTo;
	}
	
	/**
	 * Setting entity exception.
	 *
	 * @param exceptionInvoiceGenerationEntity the exception invoice generation entity
	 * @return the list
	 */
	public List<BillingEntityExceptionTo> settingEntityException(ExceptionInvoiceGenerations exceptionInvoiceGenerationEntity){
		
		List<BillingEntityExceptionTo> listBillingEntityExceptionTo=null;
		BillingEntityExceptionTo billingEntityExceptionTo=null;
		/** Entity Exception, HAVE ENTITY */
		if(Validations.validateIsNotNullAndPositive(exceptionInvoiceGenerationEntity.getIndSelectAll()) &&
				Validations.validateListIsNotNullAndNotEmpty(exceptionInvoiceGenerationEntity.getBillingEntityException())){
			listBillingEntityExceptionTo=new ArrayList<BillingEntityExceptionTo>();
			
			for (BillingEntityException billingEntityException : exceptionInvoiceGenerationEntity.getBillingEntityException()) {
				Integer entityCollection=billingEntityException.getEntityCollectionId();
				billingEntityExceptionTo=new BillingEntityExceptionTo();
				billingEntityExceptionTo.setIdBillingEntityExceptionPk(billingEntityException.getIdBillingEntityExceptionPk());
				billingEntityExceptionTo.setEntityCollectionId(billingEntityException.getEntityCollectionId());
				
				
				if(EntityCollectionType.HOLDERS.getCode().equals(entityCollection)){
					billingEntityExceptionTo.setHolder(new HolderTo(billingEntityException.getHolder().getIdHolderPk()));
					billingEntityExceptionTo.setIdHolderPk(billingEntityException.getHolder().getIdHolderPk());
					billingEntityExceptionTo.setDescriptionEntityCode(billingEntityException.getHolder().getIdHolderPk().toString());
					billingEntityExceptionTo.setDescriptionEntityCollection(billingEntityException.getHolder().getDescriptionHolder());
				}else if(EntityCollectionType.PARTICIPANTS.getCode().equals(entityCollection)){
					billingEntityExceptionTo.setParticipant(new ParticipantTo(billingEntityException.getParticipant().getIdParticipantPk()));
					billingEntityExceptionTo.setIdParticipantPk(billingEntityException.getParticipant().getIdParticipantPk());
					billingEntityExceptionTo.setDescriptionEntityCode(billingEntityException.getParticipant().getIdParticipantPk().toString());
					billingEntityExceptionTo.setDescriptionEntityCollection(billingEntityException.getParticipant().getDescription());
				}else if(EntityCollectionType.ISSUERS.getCode().equals(entityCollection)){
					billingEntityExceptionTo.setIssuer(new IssuerTo(billingEntityException.getIssuer().getIdIssuerPk()));
					billingEntityExceptionTo.setIdIssuerPk(billingEntityException.getIssuer().getIdIssuerPk());
					billingEntityExceptionTo.setDescriptionEntityCode(billingEntityException.getIssuer().getIdIssuerPk());
					billingEntityExceptionTo.setDescriptionEntityCollection(billingEntityException.getIssuer().getBusinessName());
				}else if(EntityCollectionType.OTHERS.getCode().equals(entityCollection)){
					billingEntityExceptionTo.setDescriptionEntityCode(billingEntityException.getOtherEntityDocument());
					billingEntityExceptionTo.setDescriptionEntityCollection(billingEntityException.getOtherEntityDescription());
				}
				
				listBillingEntityExceptionTo.add(billingEntityExceptionTo);
			}
			
		}
		return listBillingEntityExceptionTo;
	}
	
	/**
	 *  BCB EXCEPTION CHANGE DATE
	 *
	 * @param invoiceGeneration the invoice generation
	 * @throws ServiceException the service exception
	 */

	//	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_MODIFICATION)
	public void saveExceptionBilling(Long idBillingSchedulePk, String day) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		invoiceExceptionServiceBean.saveExceptionBilling(idBillingSchedulePk, day, loggerUser);
	}
	
	/**
	 * Get IdBillingSchedulePK
	 * @return
	 * @throws ServiceException
	 */

	public List<Long> getIdBillingSckedule() throws ServiceException {
		return invoiceExceptionServiceBean.getIdBillingSchedule();
	}
	
	public List<ExceptionInvoiceGenerationCutTo> getExceptionInvoiceGenerationsCutTo(Date cutDate) throws ServiceException{
		return invoiceExceptionServiceBean.getExceptionInvoiceGenerationsCutTo(cutDate);
	}
	
}
