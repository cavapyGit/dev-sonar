package com.pradera.billing.billingexception.facade;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.apache.commons.lang3.SerializationUtils;

import com.pradera.billing.billingexception.service.RateExceptionServiceBean;
import com.pradera.billing.billingexception.to.HolderAccountTo;
import com.pradera.billing.billingexception.to.HolderTo;
import com.pradera.billing.billingexception.to.IssuanceTo;
import com.pradera.billing.billingexception.to.RateExceptionTo;
import com.pradera.billing.billingexception.to.RuleHolderTo;
import com.pradera.billing.billingexception.to.RuleIssuerTo;
import com.pradera.billing.billingrate.to.ServiceRateExceptionsTo;
import com.pradera.billing.billingrate.to.ServiceRateTo;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.component.helperui.service.IssuerQueryServiceBean;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.billing.RateExclusion;
import com.pradera.model.billing.RateInclusion;
import com.pradera.model.billing.ServiceRate;
import com.pradera.model.billing.type.RateExceptionStatus;
import com.pradera.model.billing.type.RulerExceptionType;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RateExceptionServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)

@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class RateExceptionServiceFacade {
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry 	transactionRegistry;
	
	/** The rate exception service bean. */
	@EJB
	RateExceptionServiceBean 			rateExceptionServiceBean;
	
	/** The issuer query service. */
	@EJB
	IssuerQueryServiceBean 				issuerQueryService;
	
	
	 
	/**
	 * Save exception exclusion.
	 *
	 * @param rateExclusion the rate exclusion
	 * @return the rate exclusion
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	public RateExclusion saveExceptionExclusion(RateExclusion rateExclusion)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());	
	    
		return rateExceptionServiceBean.saveExceptionExclusion(rateExclusion);
	}
	
	
	/**
	 * Preparate rate exception.
	 *
	 * @param rateExceptionToList the rate exception to list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	public void preparateRateException(List<RateExceptionTo> rateExceptionToList) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());	
		
		
	    ServiceRate 		serviceRateTemp;
		RateExclusion		rateExclusionTempSave;
		List<RateExclusion> rateExclusionList = new ArrayList<RateExclusion>();
		
		RateInclusion 		rateInclusionTempSave;
		List<RateInclusion> rateInclusionList = new ArrayList<RateInclusion>();
		
		for(RateExceptionTo rateExceptionTo : rateExceptionToList){
		
			/***			SAVE ON EXCLUSION				***/
					if( PropertiesConstants.EXCLUSION_PK.equals( rateExceptionTo.getTypeException()) ){
					
							rateExclusionTempSave = new RateExclusion();												
							rateExclusionTempSave.setExclusionDate(new Date());
							rateExclusionTempSave.setComments(rateExceptionTo.getComments());
							rateExclusionTempSave.setExceptionRule(rateExceptionTo.getTypeRule()); 
							rateExclusionTempSave.setExclusionState(RateExceptionStatus.REGISTER.getCode());
							
							
							if (rateExceptionTo.getTypeRule().equals(RulerExceptionType.SECURITY_CLASS_EXCEPTION_RATE.getCode())){
								/**	SAVE ON	*/
								if( rateExceptionTo.getRuleSecurityClassTo().getIndicatorOneOrAll().equals(PropertiesConstants.RDB_ONE)  ){
									/** ADD NEW EXCEPTION :  EXCLUSION ONE TO SECURITY CLASS ****/
									rateExclusionTempSave.setSecurityClass(rateExceptionTo.getRuleSecurityClassTo().getSecurityClass());
									rateExclusionTempSave.setIndSelectAll(PropertiesConstants.RDB_ONE);
								}
								else{
									/** ADD NEW EXCEPTION :  EXCLUSION ALL TO SECURITY CLASS ****/
									rateExclusionTempSave.setIndSelectAll(PropertiesConstants.RDB_ALL);
								}
								/** ADD NEW EXCEPTION :  EXCLUSION TO SECURITY CLASS ****/
								rateExclusionList.add(rateExclusionTempSave);								
								
							}else if (rateExceptionTo.getTypeRule().equals(RulerExceptionType.SECURITY_EXCEPTION_RATE.getCode())){
								/**	SAVE ON	*/
								if( rateExceptionTo.getRuleSecurityTo().getIndicatorOneOrAll().equals(PropertiesConstants.RDB_ONE)){
									/** ADD NEW EXCEPTION :  EXCLUSION ONE TO SECURITY ****/
									Security security = new Security(rateExceptionTo.getRuleSecurityTo().getSecuritySelected().getIdSecurityCodePk()); 
									rateExclusionTempSave.setIndSelectAll(PropertiesConstants.RDB_ONE);
									rateExclusionTempSave.setSecurities(security);
								}
								else{
									/** ADD NEW EXCEPTION :  EXCLUSION ALL TO SECURITY ****/
									rateExclusionTempSave.setIndSelectAll(PropertiesConstants.RDB_ALL);	
								}			 					
								/** ADD NEW EXCEPTION :  EXCLUSION TO CLASS ****/
								rateExclusionList.add(rateExclusionTempSave);
								
							}else if (rateExceptionTo.getTypeRule().equals(RulerExceptionType.PARTICIPANT_EXCEPTION_RATE.getCode())){
							/**	SAVE ON	*/
								if (rateExceptionTo.getRuleParticipantTo().getIndicatorOneOrAll().equals(PropertiesConstants.RDB_ONE)){
									
									List<HolderTo>	holderToList  = rateExceptionTo.getRuleParticipantTo().getHolderToList();
									/** ADD HOLDER ****/
									if (Validations.validateListIsNotNullAndNotEmpty(holderToList)){
										Participant participant=null;
										HolderAccount holderAccount=null;
										Holder holder=null;
										Security security=null;
										RateExclusion rateExclusionParticipant=null;
										for (HolderTo holderTo : holderToList) {
											rateExclusionParticipant=SerializationUtils.clone(rateExclusionTempSave);
											
											participant=new Participant();
											participant.setIdParticipantPk(holderTo.getIdParticipantPk());
											rateExclusionParticipant.setParticipant(participant);
											
											holderAccount=new HolderAccount();
											holderAccount.setIdHolderAccountPk(holderTo.getHolderAccountPk());
											rateExclusionParticipant.setHolderAccount(holderAccount);
											
											holder=new Holder();
											holder.setIdHolderPk(holderTo.getIdHolderPk());
											rateExclusionParticipant.setHolder(holder);
											
											security=new Security();
											security.setIdSecurityCodePk(holderTo.getIdSecurityCodePk());
											rateExclusionParticipant.setSecurities(security);
											/** ADD NEW EXCEPTION :  EXCLUSION ONE TO PARTICIPANT ****/
											rateExclusionParticipant.setIndSelectAll(PropertiesConstants.RDB_ONE);
											/** ADD NEW EXCEPTION :  EXCLUSION****/
											rateExclusionList.add(rateExclusionParticipant);
										}
									}else{
										/** setear solo al participante **/
										Participant participant=new Participant();
										participant.setIdParticipantPk(rateExceptionTo.getParticipant());
										rateExclusionTempSave.setParticipant(participant);
										/** ADD NEW EXCEPTION :  EXCLUSION****/
										rateExclusionTempSave.setIndSelectAll(PropertiesConstants.RDB_ONE);
										
										rateExclusionList.add(rateExclusionTempSave);
									}
								}else{
									/** ADD NEW EXCEPTION :  EXCLUSION ALL TO PARTICIPANT ****/
									rateExclusionTempSave.setIndSelectAll(PropertiesConstants.RDB_ALL);
									/** ADD NEW EXCEPTION :  EXCLUSION****/
									rateExclusionList.add(rateExclusionTempSave);
								}
								
							/**END RULE PARTICIPANT***/
							}else if (rateExceptionTo.getTypeRule().equals(RulerExceptionType.ISSUER_EXCEPTION_RATE.getCode())) {
								/**	SAVE ON	*/
								RuleIssuerTo ruleIssuerTo = rateExceptionTo.getRuleIssuerTo();
								
								if (PropertiesConstants.RDB_ONE.equals(ruleIssuerTo.getIndicatorOneOrAll())){
									
									List<IssuanceTo> issuanceToList = ruleIssuerTo.getIssuanceTolist();
									/** ADD ISSUANCE ****/
									if (Validations.validateListIsNotNullAndNotEmpty(issuanceToList)){
										
										Issuer 	 issuer   = null;
										Security security = null;
										Issuance issuance = null;
										RateExclusion rateExclusionIssuer = null;
										 
										for(IssuanceTo issuanceToTemp : issuanceToList ){
											rateExclusionIssuer	= SerializationUtils.clone(rateExclusionTempSave);
											
											issuer		=new Issuer(rateExceptionTo.getRuleIssuerTo().getIssuerSelected().getIdIssuerPk());
											rateExclusionIssuer.setIssuer(issuer);
											
											issuance 	= rateExceptionServiceBean.find(Issuance.class, issuanceToTemp.getIdIssuanceCodePk()); 
											rateExclusionIssuer.setIssuance(issuance);
											
											if(Validations.validateIsNotNullAndNotEmpty(issuanceToTemp.getIdSecurityCodePk())){
												security	= new Security();
												security.setIdSecurityCodePk(issuanceToTemp.getIdSecurityCodePk());
												rateExclusionIssuer.setSecurities(security);
											}										
											rateExclusionIssuer.setIndSelectAll(PropertiesConstants.RDB_ONE);
											/** ADD NEW EXCEPTION :  EXCLUSION****/
											rateExclusionList.add(rateExclusionIssuer);											
										}
									}else{
										Issuer	issuer  = 	null;
										/** ADD NEW EXCEPTION :  EXCLUSION ****/
										issuer		=new Issuer(rateExceptionTo.getRuleIssuerTo().getIssuerSelected().getIdIssuerPk());
										rateExclusionTempSave.setIssuer(issuer);
										rateExclusionTempSave.setIndSelectAll(PropertiesConstants.RDB_ONE);
										rateExclusionList.add(rateExclusionTempSave);
									}									
								}else{
									/** ADD NEW EXCEPTION :  EXCLUSION ALL TO ISSUER ****/
									rateExclusionTempSave.setIndSelectAll(PropertiesConstants.RDB_ALL);
									rateExclusionList.add(rateExclusionTempSave);
								}
							}
							
					    /**END RULE ISSUER***/
						else if( rateExceptionTo.getTypeRule().equals(RulerExceptionType.HOLDER_EXCEPTION_RATE.getCode()) ){
							/**	SAVE ON	*/
							RuleHolderTo ruleHolderTo = rateExceptionTo.getRuleHolderTo();
							
							if (ruleHolderTo.getIndicatorOneOrAll().equals(PropertiesConstants.RDB_ONE)){
								
								List<HolderAccountTo> holderAccountToList = ruleHolderTo.getHolderAccountToList();
								/** ADD HOLDER_ACCOUNT ****/
								if ( Validations.validateListIsNotNullAndNotEmpty(holderAccountToList) ){
									
									Holder        holder        = null;
									HolderAccount holderAccount = null;
									RateExclusion rateExclusionHolder = null;
									
									for( HolderAccountTo holderAccountToTemp :  holderAccountToList){
										rateExclusionHolder	= SerializationUtils.clone(rateExclusionTempSave);
										
										holder        = new Holder(rateExceptionTo.getRuleHolderTo().getIdHolderPk());
										rateExclusionHolder.setHolder(holder);
										
										holderAccount = new HolderAccount();
										holderAccount.setIdHolderAccountPk(holderAccountToTemp.getIdHolderAccountPk());
										rateExclusionHolder.setHolderAccount(holderAccount);
										
										rateExclusionHolder.setIndSelectAll(PropertiesConstants.RDB_ONE);
										/** ADD NEW EXCEPTION :  EXCLUSION****/
										rateExclusionList.add(rateExclusionHolder);
									}
								}else{
									Holder holder = null;
									/** ADD NEW EXCEPTION :  EXCLUSION ****/
									holder = new Holder(rateExceptionTo.getRuleHolderTo().getIdHolderPk());
									rateExclusionTempSave.setHolder(holder);
									rateExclusionTempSave.setIndSelectAll(PropertiesConstants.RDB_ONE);
									rateExclusionList.add(rateExclusionTempSave);
								}
							}else{
								/** ADD NEW EXCEPTION :  EXCLUSION ALL TO HOLDER ****/
								rateExclusionTempSave.setIndSelectAll(PropertiesConstants.RDB_ALL);
								rateExclusionList.add(rateExclusionTempSave);									
							}
						}/** END RULE HOLDER ***/
				
					
				if (Validations.validateListIsNotNullAndNotEmpty(rateExclusionList)){
					
					/** ASSOCIATED WITH SERVICE RATE**/
					for (RateExclusion rateExclusion : rateExclusionList) {
						
						for(ServiceRateExceptionsTo serviceRateTo : rateExceptionTo.getServiceRateToList() ){
							
							RateExclusion rateExclusionClone = SerializationUtils.clone(rateExclusion);
							rateExclusionClone.setIdRateExclusionPk(null);
							serviceRateTemp = new ServiceRate();
							serviceRateTemp.setIdServiceRatePk(serviceRateTo.getIdServiceRatePk()); 
							rateExclusionClone.setServiceRate(serviceRateTemp);
							/****  save */
							rateExceptionServiceBean.saveExceptionExclusion(rateExclusionClone);
						}
					}
					rateExclusionList.clear();
				}/**END FOR  -- EN CADA VUELTA SE REGISTRA UNA EXCEPCION , CONSIDERAR QUE CADA EXCEPCION PUEDE PERTENECER A UNA O VARIAS TARIFAS ***/
			}
					
					
					/*** START INCLUSION  ****/
					
					
				else if( rateExceptionTo.getTypeException().equals(PropertiesConstants.INCLUSION_PK) ){						
						
						rateInclusionTempSave = new RateInclusion();												
						rateInclusionTempSave.setInclusionDate(new Date());
						rateInclusionTempSave.setComments(rateExceptionTo.getComments());
						rateInclusionTempSave.setExceptionRule(rateExceptionTo.getTypeRule()); 
						rateInclusionTempSave.setInclusionState(RateExceptionStatus.REGISTER.getCode());
						
						
						if (rateExceptionTo.getTypeRule().equals(RulerExceptionType.SECURITY_CLASS_EXCEPTION_RATE.getCode())){
							/**	SAVE ON	*/
							if( rateExceptionTo.getRuleSecurityClassTo().getIndicatorOneOrAll().equals(PropertiesConstants.RDB_ONE)  ){
								/** ADD NEW EXCEPTION :  INCLUSION ONE TO SECURITY CLASS ****/
								rateInclusionTempSave.setSecurityClass(rateExceptionTo.getRuleSecurityClassTo().getSecurityClass());
								rateInclusionTempSave.setIndSelectAll(PropertiesConstants.RDB_ONE);
							}
							else{
								/** ADD NEW EXCEPTION :  INCLUSION ALL TO SECURITY CLASS ****/
								rateInclusionTempSave.setIndSelectAll(PropertiesConstants.RDB_ALL);
							}								
							/** ADD NEW EXCEPTION :  INCLUSION TO SECURITY CLASS ****/
							rateInclusionList.add(rateInclusionTempSave);								
							
						}else if (rateExceptionTo.getTypeRule().equals(RulerExceptionType.SECURITY_EXCEPTION_RATE.getCode())){
							/**	SAVE ON	*/
							if( rateExceptionTo.getRuleSecurityTo().getIndicatorOneOrAll().equals(PropertiesConstants.RDB_ONE)){
								/** ADD NEW EXCEPTION :  INCLUSION ONE TO SECURITY ****/
								Security security = new Security(rateExceptionTo.getRuleSecurityTo().getSecuritySelected().getIdSecurityCodePk());
								rateInclusionTempSave.setIndSelectAll(PropertiesConstants.RDB_ONE);
								rateInclusionTempSave.setSecurities(security);
							}
							else{
								/** ADD NEW EXCEPTION :  INCLUSION ALL TO SECURITY ****/
								rateInclusionTempSave.setIndSelectAll(PropertiesConstants.RDB_ALL);	
							}			 					
							/** ADD NEW EXCEPTION :  INCLUSION TO CLASS ****/
							rateInclusionList.add(rateInclusionTempSave);
							
						}else if (rateExceptionTo.getTypeRule().equals(RulerExceptionType.PARTICIPANT_EXCEPTION_RATE.getCode())){
						/**	SAVE ON	*/
							if (rateExceptionTo.getRuleParticipantTo().getIndicatorOneOrAll().equals(PropertiesConstants.RDB_ONE)){
								
								List<HolderTo>	holderToList  = rateExceptionTo.getRuleParticipantTo().getHolderToList();
								/** ADD HOLDER ****/
								if (Validations.validateListIsNotNullAndNotEmpty(holderToList)){
									Participant participant=null;
									HolderAccount holderAccount=null;
									Holder holder=null;
									Security security=null;
									RateInclusion rateInclusionParticipant=null;
									for (HolderTo holderTo : holderToList) {
										rateInclusionParticipant=SerializationUtils.clone(rateInclusionTempSave);
										
										participant=new Participant();
										participant.setIdParticipantPk(holderTo.getIdParticipantPk());
										rateInclusionParticipant.setParticipant(participant);
										
										holderAccount=new HolderAccount();
										holderAccount.setIdHolderAccountPk(holderTo.getHolderAccountPk());
										rateInclusionParticipant.setHolderAccount(holderAccount);
										
										holder=new Holder();
										holder.setIdHolderPk(holderTo.getIdHolderPk());
										rateInclusionParticipant.setHolder(holder);
										
										security=new Security();
										security.setIdSecurityCodePk(holderTo.getIdSecurityCodePk());
										rateInclusionParticipant.setSecurities(security);
										/** ADD NEW EXCEPTION :  INCLUSION ONE TO PARTICIPANT ****/
										rateInclusionParticipant.setIndSelectAll(PropertiesConstants.RDB_ONE);
										/** ADD NEW EXCEPTION :  INCLUSION****/
										rateInclusionList.add(rateInclusionParticipant);
									}
								}else{									
									/** setear solo al participante **/
									Participant participant=new Participant();
									participant.setIdParticipantPk(rateExceptionTo.getParticipant());
									rateInclusionTempSave.setParticipant(participant);
									/** ADD NEW EXCEPTION :  INCLUSION****/
									rateInclusionTempSave.setIndSelectAll(PropertiesConstants.RDB_ONE);
									rateInclusionList.add(rateInclusionTempSave);
								}
							}else{
								/** ADD NEW EXCEPTION :  INCLUSION ALL TO PARTICIPANT ****/
								rateInclusionTempSave.setIndSelectAll(PropertiesConstants.RDB_ALL);
								/** ADD NEW EXCEPTION :  INCLUSION****/
								rateInclusionList.add(rateInclusionTempSave);
							}
							
						/**END RULE PARTICIPANT***/
						}else if (rateExceptionTo.getTypeRule().equals(RulerExceptionType.ISSUER_EXCEPTION_RATE.getCode())) {
							/**	SAVE ON	*/
							RuleIssuerTo ruleIssuerTo = rateExceptionTo.getRuleIssuerTo();
							
							if (ruleIssuerTo.getIndicatorOneOrAll().equals(PropertiesConstants.RDB_ONE)){
								
								List<IssuanceTo> issuanceToList = ruleIssuerTo.getIssuanceTolist();
								/** ADD ISSUANCE ****/
								if (Validations.validateListIsNotNullAndNotEmpty(issuanceToList)){
									
									Issuer 	 issuer   = null;
									Security security = null;
									Issuance issuance = null;
									RateInclusion rateInclusionIssuer = null;
									 
									for(IssuanceTo issuanceToTemp : issuanceToList ){
										rateInclusionIssuer	= SerializationUtils.clone(rateInclusionTempSave);
										
										issuer		= new Issuer(rateExceptionTo.getRuleIssuerTo().getIssuerSelected().getIdIssuerPk());
										rateInclusionIssuer.setIssuer(issuer);
										
										issuance 	= rateExceptionServiceBean.find(Issuance.class, issuanceToTemp.getIdIssuanceCodePk());
										rateInclusionIssuer.setIssuance(issuance);
										
										if(Validations.validateIsNotNullAndNotEmpty(issuanceToTemp.getIdSecurityCodePk())){
											security	= new Security();
											security.setIdSecurityCodePk(issuanceToTemp.getIdSecurityCodePk());
											rateInclusionIssuer.setSecurities(security);
										}											
										rateInclusionIssuer.setIndSelectAll(PropertiesConstants.RDB_ONE);
										/** ADD NEW EXCEPTION :  INCLUSION****/
										rateInclusionList.add(rateInclusionIssuer);											
									}
								}else{
									Issuer	issuer  = 	null;
									/** ADD NEW EXCEPTION :  INCLUSION ****/
									issuer		= new Issuer(rateExceptionTo.getRuleIssuerTo().getIssuerSelected().getIdIssuerPk());
									rateInclusionTempSave.setIssuer(issuer);
									rateInclusionTempSave.setIndSelectAll(PropertiesConstants.RDB_ONE);
									rateInclusionList.add(rateInclusionTempSave);
								}									
							}else{
								/** ADD NEW EXCEPTION :  INCLUSION ALL TO ISSUER ****/
								rateInclusionTempSave.setIndSelectAll(PropertiesConstants.RDB_ALL);
								rateInclusionList.add(rateInclusionTempSave);
							}
						}
						
				    /**END RULE ISSUER***/
					else if( rateExceptionTo.getTypeRule().equals(RulerExceptionType.HOLDER_EXCEPTION_RATE.getCode()) ){
						/**	SAVE ON	*/
						RuleHolderTo ruleHolderTo = rateExceptionTo.getRuleHolderTo();
						
						if (ruleHolderTo.getIndicatorOneOrAll().equals(PropertiesConstants.RDB_ONE)){
							
							List<HolderAccountTo> holderAccountToList = ruleHolderTo.getHolderAccountToList();
							/** ADD HOLDER_ACCOUNT ****/
							if ( Validations.validateListIsNotNullAndNotEmpty(holderAccountToList) ){
								
								Holder        holder        = null;
								HolderAccount holderAccount = null;
								RateInclusion rateInclusionHolder = null;
								
								for( HolderAccountTo holderAccountToTemp :  holderAccountToList){
									rateInclusionHolder	= SerializationUtils.clone(rateInclusionTempSave);
									
									holder        = new Holder(rateExceptionTo.getRuleHolderTo().getIdHolderPk());
									rateInclusionHolder.setHolder(holder);
									
									holderAccount = new HolderAccount();
									holderAccount.setIdHolderAccountPk(holderAccountToTemp.getIdHolderAccountPk());
									rateInclusionHolder.setHolderAccount(holderAccount);
									
									rateInclusionHolder.setIndSelectAll(PropertiesConstants.RDB_ONE);
									/** ADD NEW EXCEPTION :  INCLUSION****/
									rateInclusionList.add(rateInclusionHolder);
								}
							}else{
								Holder holder = null;
								/** ADD NEW EXCEPTION :  INCLUSION ****/
								holder = new Holder(rateExceptionTo.getRuleHolderTo().getIdHolderPk());
								rateInclusionTempSave.setHolder(holder);
								rateInclusionTempSave.setIndSelectAll(PropertiesConstants.RDB_ONE);
								rateInclusionList.add(rateInclusionTempSave);
							}
						}else{
							/** ADD NEW EXCEPTION :  INCLUSION ALL TO HOLDER ****/
							rateInclusionTempSave.setIndSelectAll(PropertiesConstants.RDB_ALL);
							rateInclusionList.add(rateInclusionTempSave);									
						}
					}/** END RULE HOLDER ***/
			/****		END INCLUSION		****/
				
			if (Validations.validateListIsNotNullAndNotEmpty(rateInclusionList)){
				
				/** ASSOCIATED WITH SERVICE RATE**/
				for (RateInclusion rateInclusion : rateInclusionList) {
					
					for(ServiceRateExceptionsTo serviceRateTo : rateExceptionTo.getServiceRateToList() ){
						
						RateInclusion rateInclusionClone = SerializationUtils.clone(rateInclusion);
						rateInclusionClone.setIdRateInclusionPk(null);
						serviceRateTemp = new ServiceRate();
						serviceRateTemp.setIdServiceRatePk(serviceRateTo.getIdServiceRatePk()); 
						rateInclusionClone.setServiceRate(serviceRateTemp);
						/****  save */
						rateExceptionServiceBean.saveExceptionInclusion(rateInclusionClone);
					}
				}
				rateInclusionList.clear();
			}/**END FOR  -- EN CADA VUELTA SE REGISTRA UNA EXCEPCION , CONSIDERAR QUE CADA EXCEPCION PUEDE PERTENECER A UNA O VARIAS TARIFAS ***/
		}
			
					
		
					
					
		}/** END FOR RATEEXCEPTION  **/

	}/**
 *  FIN METODO *.
 *
 * @param rateExclusionList the rate exclusion list
 */
	
	
	public void  saveAllRateExclusion(List<RateExclusion> rateExclusionList) {
		rateExceptionServiceBean.saveAllExceptionRateExclusion(rateExclusionList);
		
	}
	 
	
	

	/**
	 * Save all rate inclusion.
	 *
	 * @param rateInclusionList the rate inclusion list
	 */
	public void  saveAllRateInclusion(List<RateInclusion> rateInclusionList) {
		rateExceptionServiceBean.saveAllExceptionRateInclusion(rateInclusionList);
		
	}
	
	/**
	 * Gets the rate exception exclusion list.
	 *
	 * @param rateExceptionTo the rate exception to
	 * @return the rate exception exclusion list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<RateExclusion> getRateExceptionExclusionList(RateExceptionTo rateExceptionTo) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());	 
	    
		return rateExceptionServiceBean.findRateExclusionListByIdServiceRatePk(rateExceptionTo);
	}
	
	
	/**
	 * Gets the rate exception inclusion list.
	 *
	 * @param rateExceptionTo the rate exception to
	 * @return the rate exception inclusion list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<RateInclusion> getRateExceptionInclusionList(RateExceptionTo rateExceptionTo) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());	
	    
		return rateExceptionServiceBean.findRateInclusionListByIdServiceRatePk(rateExceptionTo);
	}
	
	/**
	 *  DELETE.
	 *
	 * @param listRateExceptionToDelete the list rate exception to delete
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_MODIFICATION)
	public void prepareDeleteExcepctions(List<RateExceptionTo> listRateExceptionToDelete) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
			
			for(RateExceptionTo rateExceptionTo : listRateExceptionToDelete){	
				if( rateExceptionTo.getTypeException().equals(PropertiesConstants.EXCLUSION_PK) ){				
					rateExceptionServiceBean.removeExceptionExclusion(rateExceptionTo.getIdRateException());
					
				}else if( rateExceptionTo.getTypeException().equals(PropertiesConstants.INCLUSION_PK) ){				
					rateExceptionServiceBean.removeExceptionInclusion(rateExceptionTo.getIdRateException());			
				}
			}
			
	}
	
	/**
	 *  CONSULT RATES BY ID_RATE_|1EXCEPTION.
	 *
	 * @param rateExceptionId the rate exception id
	 * @return the services rate by exception id
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<ServiceRateTo> getServicesRateByExceptionId(Long rateExceptionId) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		List<ServiceRateTo> list = rateExceptionServiceBean.getServicesRateByRateExceptions(rateExceptionId);	
		return list;
	}
	
	/**
	 *  CONSULT RATES BY ID_RATE_|1EXCEPTION.
	 *
	 * @param rateExceptionId the rate exception id
	 * @return the services rate by exception id two
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<ServiceRateTo> getServicesRateByExceptionIdTwo(Long rateExceptionId) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		List<ServiceRateTo> list = rateExceptionServiceBean.getServicesRateByRateExceptions(rateExceptionId);	
		return list;
	}
	
	/**
	 * Find data participant to.
	 *
	 * @param participantPk the participant pk
	 * @return the list
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<HolderTo> findDataParticipantTo(Long participantPk){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return rateExceptionServiceBean.findDataHolderTo(participantPk);
	}
	
	/**
	 * Gets the rate exception exclusion by pk.
	 *
	 * @param id the id
	 * @return the rate exception exclusion by pk
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public RateExclusion getRateExceptionExclusionByPk(Long id){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return  rateExceptionServiceBean.find(RateExclusion.class, id);
	}
	
	/**
	 * Gets the rate exception inclusion by pk.
	 *
	 * @param id the id
	 * @return the rate exception inclusion by pk
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public RateInclusion getRateExceptionInclusionByPk(Long id){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return  rateExceptionServiceBean.find(RateInclusion.class, id);
	}
	
	/**
	 *  CHANGE STATES - DELETE EXCLUSION.
	 *
	 * @param rateExclusion the rate exclusion
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_MODIFICATION)
	public void deletedExclusion(RateExclusion rateExclusion) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		
		rateExceptionServiceBean.update(rateExclusion);
	}
	
	/**
	 *  CHANGE STATES - DELETE INCLUSION.
	 *
	 * @param rateInclusion the rate inclusion
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_MODIFICATION)
	public void deletedInclusion(RateInclusion rateInclusion) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		
		rateExceptionServiceBean.update(rateInclusion);
	}
	
	/**
	 *  FIND ISSUANCE AND SUCURITY *.
	 *
	 * @param issuerCode the issuer code
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<IssuanceTo> findIssuanceTo(String issuerCode) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return rateExceptionServiceBean.findDataIssuanceTo(issuerCode);
	}
	
	/**
	 *  FIND HOLDER_ACCOUNT *.
	 *
	 * @param idHolder the id holder
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<HolderAccountTo> findHolderAccount(Long idHolder)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return rateExceptionServiceBean.findHolderAccount(idHolder);
	}
	
	
	
	
}
