package com.pradera.billing.billingexception.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RuleParticipantTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class RuleParticipantTo implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The participant to list. */
	private List<ParticipantTo> 			participantToList;
	
	/** The holder to list. */
	private List<HolderTo> 					holderToList;
	
	
	/**  0 : All Participant  1 : One Participant. */
	private Integer	indicatorOneOrAll;



	/**
	 * Instantiates a new rule participant to.
	 */
	public RuleParticipantTo(){
		participantToList=new ArrayList<ParticipantTo>();
		holderToList=new ArrayList<HolderTo>();
	}

	/**
	 * Gets the participant to list.
	 *
	 * @return the participant to list
	 */
	public List<ParticipantTo> getParticipantToList() {
		return participantToList;
	}



	/**
	 * Sets the participant to list.
	 *
	 * @param participantToList the new participant to list
	 */
	public void setParticipantToList(List<ParticipantTo> participantToList) {
		this.participantToList = participantToList;
	}



	/**
	 * Gets the holder to list.
	 *
	 * @return the holder to list
	 */
	public List<HolderTo> getHolderToList() {
		return holderToList;
	}



	/**
	 * Sets the holder to list.
	 *
	 * @param holderToList the new holder to list
	 */
	public void setHolderToList(List<HolderTo> holderToList) {
		this.holderToList = holderToList;
	}

	/**
	 * Gets the indicator one or all.
	 *
	 * @return the indicator one or all
	 */
	public Integer getIndicatorOneOrAll() {
		return indicatorOneOrAll;
	}

	/**
	 * Sets the indicator one or all.
	 *
	 * @param indicatorOneOrAll the new indicator one or all
	 */
	public void setIndicatorOneOrAll(Integer indicatorOneOrAll) {
		this.indicatorOneOrAll = indicatorOneOrAll;
	}

	
}
