package com.pradera.billing.billingexception.invoice.to;

import java.io.Serializable;

public class ExceptionInvoiceGenerationCutTo implements Serializable {

	/** ExceptionInvoiceGenerationTo. */
	private static final long serialVersionUID = 1L;

	private Long idExceptionInvoicePk;
	private String rate;
	private String serviceCode;
	private String exceptionType;
	private String collectionPeriod;
	private String monthDay;
	private String weekDay;
	private String includeAll;
	private String entity;
	private String newMonthDay;
	
	public ExceptionInvoiceGenerationCutTo() {
	}
	public Long getIdExceptionInvoicePk() {
		return idExceptionInvoicePk;
	}
	public void setIdExceptionInvoicePk(Long idExceptionInvoicePk) {
		this.idExceptionInvoicePk = idExceptionInvoicePk;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String getExceptionType() {
		return exceptionType;
	}
	public void setExceptionType(String exceptionType) {
		this.exceptionType = exceptionType;
	}
	public String getMonthDay() {
		return monthDay;
	}
	public void setMonthDay(String monthDay) {
		this.monthDay = monthDay;
	}
	public String getWeekDay() {
		return weekDay;
	}
	public void setWeekDay(String weekDay) {
		this.weekDay = weekDay;
	}
	public String getIncludeAll() {
		return includeAll;
	}
	public void setIncludeAll(String includeAll) {
		this.includeAll = includeAll;
	}
	public String getEntity() {
		return entity;
	}
	public void setEntity(String entity) {
		this.entity = entity;
	}
	public String getNewMonthDay() {
		return newMonthDay;
	}
	public void setNewMonthDay(String newMonthDay) {
		this.newMonthDay = newMonthDay;
	}
	public String getCollectionPeriod() {
		return collectionPeriod;
	}
	public void setCollectionPeriod(String collectionPeriod) {
		this.collectionPeriod = collectionPeriod;
	}
	
	
}