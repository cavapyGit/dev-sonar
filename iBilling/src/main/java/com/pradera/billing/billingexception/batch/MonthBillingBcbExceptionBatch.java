package com.pradera.billing.billingexception.batch;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Validation;

import com.pradera.billing.billingexception.facade.InvoiceExceptionServiceFacade;
import com.pradera.billing.common.utils.BillingUtils;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MonthBillingBcbExceptionBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19-ago-2015
 */
@BatchProcess(name="MonthBillingBcbExceptionBatch")
@RequestScoped
public class MonthBillingBcbExceptionBatch implements Serializable,JobExecution {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
		
	/** The invoice exception service facade. */
	@EJB
	InvoiceExceptionServiceFacade 		invoiceExceptionServiceFacade;
	
	/** The holiday query service bean. */
	@EJB
	private HolidayQueryServiceBean holidayQueryServiceBean;
	
	@Inject
	private PraderaLogger log;

	/* (non-Javadoc)
	 * Batchero para registrar cat electronicos en la BD de la web
	 */
	@SuppressWarnings("deprecation")
	@Override
	public void startJob(ProcessLogger processLogger) {
		log.info("************************************* INICIO BATCH PARA FACTURACION DEL BCB  ******************************************************");
		try {
			/*Integer day = null;
			
			//Todos los que tienen indBCB = 1
			List <Long> listBillingSchedulePk = invoiceExceptionServiceFacade.getIdBillingSckedule();
			
			/*Date endOfMonth=BillingUtils.getLastDayOfMonth(CommonsUtilities.currentDate() );
			
			//Verificamos si el dia es feriado
			
			while (holidayQueryServiceBean.isNonWorkingDayServiceBean(CommonsUtilities.truncateDateTime(endOfMonth),true)) {
				
				//Tenemos el utlimo dia habil de la fecha
				endOfMonth=CommonsUtilities.addDate(endOfMonth,-1);
			}
			
			endOfMonth=CommonsUtilities.addDate(endOfMonth,-1);
			//day = endOfMonth.getDate();
			
			//Requisito que se antes del ultimo dia habil -1 dia
			//day = day - 1;
			
			//Ahora agregamos al penultimo dia habil
			while (holidayQueryServiceBean.isNonWorkingDayServiceBean(CommonsUtilities.truncateDateTime(endOfMonth),true)) {
				
				//Tenemos el utlimo dia habil de la fecha
				endOfMonth=CommonsUtilities.addDate(endOfMonth,-1);
			}
			day = endOfMonth.getDate();*/
			
			//Recibimos el parametro desde la pantalla
			Integer day = null;
			List<ProcessLoggerDetail> processLoggerDetails = processLogger.getProcessLoggerDetails();
			
			for(ProcessLoggerDetail processLoggerDetail: processLoggerDetails){
				 
					if(processLoggerDetail.getParameterName().equals("date")){
						day = Integer.valueOf(processLoggerDetail.getParameterValue());
					}
			}
			
			//Si el dato es correcto entonces ejecutamos el cambio para todos los procesos
			if(Validations.validateIsNotNullAndNotEmpty(day)){
				
				//Todos los que tienen indBCB = 1
				List <Long> listBillingSchedulePk = invoiceExceptionServiceFacade.getIdBillingSckedule();
				
				String strDay = (String) day.toString();
				
				for (Long idBillingSchedulePk : listBillingSchedulePk){
					invoiceExceptionServiceFacade.saveExceptionBilling(idBillingSchedulePk, strDay);
				}
			}
			
		System.out.println("************************************* FIN BATCH PARA FACTURACION DEL BCB ******************************************************");
				
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/* (non-Javadoc)
	 * Metodo implementado para obtener los parametros de notificaciones
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * Metodo implementado para obtener a los destinatarios
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * Metodo Implementado para enviar las notificaciones
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}
