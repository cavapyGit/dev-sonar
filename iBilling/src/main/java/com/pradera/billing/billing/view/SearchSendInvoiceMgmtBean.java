package com.pradera.billing.billing.view;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.xml.bind.JAXBElement;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;

import org.primefaces.model.StreamedContent;
import org.xml.sax.SAXException;

import com.edv.org.datacontract.schemas._2004._07.iedvservicios.ArrayOfEIRESAD;
import com.edv.org.datacontract.schemas._2004._07.iedvservicios.EIRESAD;
import com.edv.org.datacontract.schemas._2004._07.iedvservicios.EIRESAS;
import com.edv.org.tempuri.ServFacturaResponse;
import com.pradera.billing.billing.facade.BillingProcessedServiceFacade;
import com.pradera.billing.billing.to.BillingProcessedFilter;
import com.pradera.billing.billing.to.ParameterInvoiceSearchTO;
import com.pradera.billing.billing.to.SendInterfaceXmlSapTO;
import com.pradera.billing.billing.to.XmlObjectSap;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.auditprocess.type.StateSapResponseType;
import com.pradera.model.billing.BillingCalculationProcess;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.type.BillingCalculationType;
import com.pradera.model.billing.type.EntityCollectionType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;

@DepositaryWebBean
@LoggerCreateBean
public class SearchSendInvoiceMgmtBean extends GenericBaseBean{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8256683105234224835L;
	
	private static final String CASE_REGISTER_PAGE = "registerSendInvoices";
	
	private static final String CASE_SEARCH_PAGE = "searchSendInvoices";
	
	private static final String CASE_RETURN_SEARCH_PAGE = "returnSearchPageInvoice";
	

	@Inject
	private BillingProcessedServiceFacade facade;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;
	
	@Inject
	private transient PraderaLogger log;
	
	/**Filtro para busqueda de facturas*/
	private ParameterInvoiceSearchTO invoiceSearchTO;
	
	private GenericDataModel<BillingCalculationProcess> calculationProcessDataModel;
	
	private GenericDataModel<SendInterfaceXmlSapTO>  sendInterfaceXmlDataModel;
	
	private int totalAccepted = 0;
	
	/*Seleccionados*/
	private SendInterfaceXmlSapTO selectedForwarding;
	
	private List<BillingCalculationProcess> listSelecteds;
	
	private Integer collectionInstitutionSelected;
	
	private Integer collectionPeriodSelected;
	
	private BillingService billingServiceSelection;
	
	private boolean disableHolderECVisible;
	private boolean disableIssuerECVisible;
	private boolean disableParticipantECVisible;
	private boolean disableParticipantAFPECVisible;
	private boolean disableOtherECVisible;
	private Issuer  issuer=new Issuer();
	private Holder holderSelected = new Holder();
	private List<ParameterTable> listCollectionInstitution;
	
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	@EJB
	private BillingProcessedServiceFacade billingProcessedServiceFacade;

	@PostConstruct
	public void init(){
		invoiceSearchTO = new ParameterInvoiceSearchTO();
		listSelecteds = new ArrayList<>();
		billingServiceSelection= new BillingService();
	}
	
	
	public String loadRegistrePage(){
		if (Validations.validateListIsNullOrEmpty(listCollectionInstitution)) {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			log.debug("lista entidad de cobro :");
			parameterTableTO.setMasterTableFk(PropertiesConstants.COLLECTION_ENTITY_PK);
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			try {
				listCollectionInstitution = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
				
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		return CASE_REGISTER_PAGE;
	}
	
	
	/**Buscamos las facturas registradas pantalla de registro*/
	public void searchInvoiceRecords(){
		
		log.info("SAP::: Buscando registos de facturas");
			BillingProcessedFilter billingProcessedFilter= new BillingProcessedFilter();
			billingProcessedFilter.setEntityCollection(collectionInstitutionSelected);
			billingProcessedFilter.setCollectionPeriod(collectionPeriodSelected);
			billingProcessedFilter.setBillingDateStart(invoiceSearchTO.getRegistreInitDate());
			billingProcessedFilter.setBillingDateEnd(invoiceSearchTO.getRegistreFinalDate());
			billingProcessedFilter.setProcessedState(BillingCalculationType.DEFINITIVO.getCode());
			List<BillingCalculationProcess> listBillingCalculationProcesses=billingProcessedServiceFacade.findBillingCalculationProcessSap(billingProcessedFilter,billingServiceSelection.getIdBillingServicePk());
			/**/
			calculationProcessDataModel=new GenericDataModel<>(listBillingCalculationProcesses);
	}
	/**
	 * Busqueda de envios registrados
	 * */
	public void searchSentSapInvoices(){
		List<SendInterfaceXmlSapTO> list= facade.searchSentInvoices(invoiceSearchTO);
		totalAccepted = 0;
		for (SendInterfaceXmlSapTO sendInterfaceXmlSapTO : list)
			totalAccepted = totalAccepted +sendInterfaceXmlSapTO.getNumberAccepted();
		sendInterfaceXmlDataModel = new GenericDataModel<>(list);
	}
	/*
	public void searchInvoiceNumber(){
		System.out.println("Buscar factura:"+numInvoice);
		for (iterable_type iterable_element : iterable) {
			
		}
	}*/
	
	public void validateBillingId(){
		//invoiceSearchTO.setBillingDate(CommonsUtilities.currentDate());
		invoiceSearchTO.setRegistreInitDate(CommonsUtilities.currentDate());
		invoiceSearchTO.setRegistreFinalDate(CommonsUtilities.currentDate());
		calculationProcessDataModel= new GenericDataModel<>();
	}

	/**
	 *  Acciones con los paneles de Entidad de Cobro.
	 */
	public void updateComponentEntityCollection() {
		log.info("SAP::: entidad de cobro:"+collectionInstitutionSelected);
		Integer entityCollection = this.collectionInstitutionSelected;
		if (Validations.validateIsNotNullAndPositive(entityCollection)) {
			if (entityCollection.equals(EntityCollectionType.ISSUERS.getCode())) {
				disableHolderECVisible = Boolean.FALSE;
				disableIssuerECVisible = Boolean.TRUE;
				disableParticipantECVisible = Boolean.FALSE;
				disableParticipantAFPECVisible=Boolean.FALSE;
				disableOtherECVisible = Boolean.FALSE;
			} else if (entityCollection.equals(EntityCollectionType.PARTICIPANTS.getCode() )) {
				disableHolderECVisible = Boolean.FALSE;
				disableIssuerECVisible = Boolean.FALSE;
				disableParticipantECVisible = Boolean.TRUE;
				disableParticipantAFPECVisible=Boolean.FALSE;
				disableOtherECVisible = Boolean.FALSE;
				//verifyHolder();
			} else if (entityCollection.equals(EntityCollectionType.HOLDERS.getCode())) {
				disableHolderECVisible = Boolean.TRUE;
				disableIssuerECVisible = Boolean.FALSE;
				disableParticipantECVisible = Boolean.FALSE;
				disableParticipantAFPECVisible=Boolean.FALSE;
				disableOtherECVisible = Boolean.FALSE;
			} else if (entityCollection.equals(EntityCollectionType.OTHERS.getCode())) {
				disableHolderECVisible = Boolean.FALSE;
				disableIssuerECVisible = Boolean.FALSE;
				disableParticipantECVisible = Boolean.FALSE;
				disableParticipantAFPECVisible=Boolean.FALSE;
				disableOtherECVisible = Boolean.TRUE;
			}
			
			else if (entityCollection.equals(EntityCollectionType.MANAGER.getCode())
					||entityCollection.equals(EntityCollectionType.STOCK_EXCHANGE.getCode())
					||entityCollection.equals(EntityCollectionType.CENTRAL_BANK.getCode())
					||entityCollection.equals(EntityCollectionType.HACIENDA.getCode())
//					||entityCollection.equals(EntityCollectionType.REGULATOR_ENTITY.getCode())
					) {
				disableHolderECVisible = Boolean.FALSE;
				disableIssuerECVisible = Boolean.FALSE;
				disableParticipantECVisible = Boolean.FALSE;
				disableParticipantAFPECVisible=Boolean.FALSE;
				disableOtherECVisible = Boolean.FALSE;
			} else if (entityCollection.equals(EntityCollectionType.AFP.getCode())) {
				disableHolderECVisible = Boolean.FALSE;
				disableIssuerECVisible = Boolean.FALSE;
				disableParticipantECVisible = Boolean.FALSE;
				disableParticipantAFPECVisible=Boolean.TRUE;
				disableOtherECVisible = Boolean.FALSE;
			}
		} else {
			disableHolderECVisible = Boolean.FALSE;
			disableIssuerECVisible = Boolean.FALSE;
			disableParticipantECVisible = Boolean.FALSE;
			disableParticipantAFPECVisible=Boolean.FALSE;
			disableOtherECVisible = Boolean.FALSE;
		}
		clearEntitys();
		
		//Llamada a Ultima Fecha
		//getLastBillingDateByParameters();
	}
	
	public void updateComponentCollectionPeriod(){
		log.info("SAP:::Perido de cobro:"+collectionPeriodSelected);
	}
	/**
	 * Clear entitys.
	 */
	public void clearEntitys(){
		this.issuer=new Issuer();
		this.holderSelected = new Holder();

	}

	public void cleanSearchPage(){
		sendInterfaceXmlDataModel = null;
		invoiceSearchTO = new ParameterInvoiceSearchTO();
		
	}
	/**Limpiar pagina de registro*/
	public void cleanRegistrePage(){
		invoiceSearchTO = new ParameterInvoiceSearchTO();
		calculationProcessDataModel = new GenericDataModel<>();
		collectionInstitutionSelected = null;
		collectionPeriodSelected = null;
		billingServiceSelection = new BillingService();
	}
	public String backRegistrePage(){
		invoiceSearchTO = new ParameterInvoiceSearchTO();
		calculationProcessDataModel = new GenericDataModel<>();
		return CASE_RETURN_SEARCH_PAGE;
	}
	
	public String preSendInvoiceSapClient(){
		if(listSelecteds.isEmpty()){
			/**Mensaje de alerta, se debe seleccionar mas un mensaje*/
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage("confirm.type.service.rate.no.selected");
			
			JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
			JSFUtilities.putViewMap("headerMessageView", headerMessage);
			
			JSFUtilities.showSimpleValidationDialog();
			
		}else{
			/**Dialogo de desea enviar */
			 JSFUtilities.executeJavascriptFunction("PF('wVBtnConfirnSendXml').show();");
			 JSFUtilities.updateComponent("opnlDialogs");
		}
		return null;
	}
	/*
	 * Re envio de facuras
	 * */
	public String preReSendInvoiceSapClient(){
		if(selectedForwarding==null){
			/**Mensaje de alerta, se debe seleccionar mas un mensaje*/
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage("confirm.type.service.rate.no.selected");
			
			JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
			JSFUtilities.putViewMap("headerMessageView", headerMessage);
			
			JSFUtilities.showSimpleValidationDialog();
			
		}else{
				/**Dialogo de desea enviar */
			 JSFUtilities.executeJavascriptFunction("PF('wVBtnConfirnReSendXml').show();");
			 JSFUtilities.updateComponent("opnlDialogs");
		}
		return null;
	}
	public String reSendInvoiceSapClient(){
		XmlObjectSap xmlObjectSap = facade.sendInvoiceSapClient(selectedForwarding);
		facade.updateSendSapXml(xmlObjectSap,selectedForwarding);
		selectedForwarding =  null;
		searchInvoiceRecords();
		
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		String bodyMessage = PropertiesUtilities.getMessage("billing.invoice.file.re.send");
		
		JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
		JSFUtilities.putViewMap("headerMessageView", headerMessage);
		
		JSFUtilities.showSimpleValidationDialog();
		
		FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("fmrBillingInvoiceSending:panelListado");
		return null;
	}
	
	public String sendInvoiceSapClient(){
		if(listSelecteds.isEmpty()){
			/**Mensaje de alerta, se debe seleccionar mas un mensaje*/
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage("confirm.type.service.rate.no.selected");
			
			JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
			JSFUtilities.putViewMap("headerMessageView", headerMessage);
			
			JSFUtilities.showSimpleValidationDialog();
			return "";
		}
		for (BillingCalculationProcess process: listSelecteds) {
			log.info("SAP::::Id de billing calculaton process"+process.getIdBillingCalcProcessPk());
			try { 
				//List<InvoiceTO> list = facade.getOBjectBilling(process.getIdBillingCalcProcessPk());
				XmlObjectSap xmlObjectSap = facade.sendInvoiceSapClient(null,process.getIdBillingCalcProcessPk());
				SendInterfaceXmlSapTO interfaceXmlSapTO = buildSendInterfaceXmlSapTO(xmlObjectSap, process);
				facade.registreSendSapXml(interfaceXmlSapTO);
			} catch (SOAPException | ParserConfigurationException | SAXException | IOException e) {
				e.printStackTrace();
			}
		}
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		String bodyMessage = PropertiesUtilities.getMessage("billing.invoice.alert.send.xml.ok");
		
		JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
		JSFUtilities.putViewMap("headerMessageView", headerMessage);
		
		JSFUtilities.putViewMap("headerMessageView", headerMessage);
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('wVSatisfactoryDeliveryAlert').show();");
		return null;
	}
	
	public SendInterfaceXmlSapTO buildSendInterfaceXmlSapTO(XmlObjectSap xmlObjectSap,BillingCalculationProcess process){
		int numberCorrect = 0;
		int numberFailed = 0;
		int numberDuplicate = 0;
		String responseValue = CommonsUtilities.STR_BLANK;
		SendInterfaceXmlSapTO interfaceXmlSapTO = new SendInterfaceXmlSapTO();
		
		if(xmlObjectSap!=null){
			ServFacturaResponse response = xmlObjectSap.getFacturaResponse();
			JAXBElement<EIRESAS> jaxbElements = response.getServFacturaResult();
			EIRESAS eiresas  = jaxbElements.getValue();
			
			/**Respuesta de erroes*/
			JAXBElement<String> jaxbElement1 =  eiresas.getMsgrespuesta();
			/**Respuesta de detalle*/
			JAXBElement<ArrayOfEIRESAD> jaxbElement2 = eiresas.getDetalle();
			
			//arrayOfEIRESAD null, solo hay en caso de que sea valido
			ArrayOfEIRESAD arrayOfEIRESAD = jaxbElement2.getValue();
			//Correcto en caso de tener resultado y luego contar los aceptdos
			interfaceXmlSapTO.setState(StateSapResponseType.CORRECT.getCode());
			if(xmlObjectSap!=null&&arrayOfEIRESAD!=null){
				List<EIRESAD> listFinal = arrayOfEIRESAD.getEIRESAD();
				for (EIRESAD eiresad : listFinal) {
					JAXBElement<String> element = eiresad.getEstado();
					if(StateSapResponseType.CORRECT.getValue().equals(element.getValue()))
						numberCorrect++;
					if(StateSapResponseType.ERROR.getValue().equals(element.getValue()))
						numberFailed++;
					if(StateSapResponseType.DUPLICATE.getValue().equals(element.getValue()))
						numberDuplicate++;
				}
			}else{
				try {
					responseValue = jaxbElement1.getValue();
					interfaceXmlSapTO.setState(StateSapResponseType.ERROR.getCode());
				} catch (Exception e) {
					interfaceXmlSapTO.setResponseValue("ERROR/NO SE OBTUVO NIGUN RESULTADO");
					interfaceXmlSapTO.setState(StateSapResponseType.ERROR.getCode());
				}
			}
		}else{
			responseValue = "ERROR/NO SE OBTUVO NIGUN RESULTADO";
			interfaceXmlSapTO.setState(StateSapResponseType.ERROR.getCode());
		}
		interfaceXmlSapTO.setIdBillingCalcProcessFk(process.getIdBillingCalcProcessPk());
		interfaceXmlSapTO.setNumberAccepted(numberCorrect);
		interfaceXmlSapTO.setNumberDuplicate(numberDuplicate);
		interfaceXmlSapTO.setNumberRejected(numberFailed);
		interfaceXmlSapTO.setResponseValue(responseValue);
		interfaceXmlSapTO.setInvoiceList(xmlObjectSap.getListSendInvoice());
		String xmlSend = new String();
		ByteArrayOutputStream baos = null;
		baos = new ByteArrayOutputStream();
		if(xmlObjectSap!=null&&xmlObjectSap.getSoapMessageSend()!=null)
			try {
				xmlObjectSap.getSoapMessageSend().writeTo(baos);
				xmlSend = baos.toString();
				System.out.println("SAP: Factura envio :"+xmlSend);
				interfaceXmlSapTO.setXmlSoapSendMessage(xmlSend.getBytes());
				
				String xmlRecep = null;
				baos = new ByteArrayOutputStream();
				if(xmlObjectSap!=null&&xmlObjectSap.getMessageResponse()!=null)
					xmlObjectSap.getMessageResponse().writeTo(baos); 
			    xmlRecep = baos.toString();
			    System.out.println("SAP: Factura recepcion:"+xmlRecep);
				interfaceXmlSapTO.setXmlSoapRecepMessage(xmlRecep.getBytes());
			} catch (SOAPException | IOException e) {
				e.printStackTrace();
			} 
		return interfaceXmlSapTO;
	}
	@LoggerAuditWeb
	public StreamedContent getStreamedContentSendFile(SendInterfaceXmlSapTO interfaceXmlSapTO) throws IOException{
		byte[] fileContent = interfaceXmlSapTO.getXmlSoapSendMessage();
		return getStreamedContentFromFile(fileContent, null,"File"+ interfaceXmlSapTO.getIdSendInterfaceXmlSapPk()+".xml");
	}
	@LoggerAuditWeb
	public StreamedContent getStreamedContentReceptFile(SendInterfaceXmlSapTO interfaceXmlSapTO) throws IOException{
		byte[] fileContent = interfaceXmlSapTO.getXmlSoapRecepMessage();
		return getStreamedContentFromFile(fileContent, null,"File"+ interfaceXmlSapTO.getIdSendInterfaceXmlSapPk()+".xml");
	}
	@LoggerAuditWeb
	public StreamedContent getStreamedContent(BillingCalculationProcess billingCalculationProcess) throws IOException{
		byte[] fileContent = billingProcessedServiceFacade.getBillingCalculationProcessFileContent(billingCalculationProcess.getIdBillingCalcProcessPk());
		return getStreamedContentFromFile(fileContent, null, billingCalculationProcess.getNameFile());
	}
	public List<String> getListaDetalles(String detalle){
        if (detalle != null && !detalle.isEmpty()){
              String [] detalles = detalle.split("#");
              List<String> detallesList = Arrays.asList(detalles);
              return detallesList;
        }
        return new ArrayList<>();
	}


	public ParameterInvoiceSearchTO getInvoiceSearchTO() {
		return invoiceSearchTO;
	}


	public void setInvoiceSearchTO(ParameterInvoiceSearchTO invoiceSearchTO) {
		this.invoiceSearchTO = invoiceSearchTO;
	}


	public GenericDataModel<BillingCalculationProcess> getCalculationProcessDataModel() {
		return calculationProcessDataModel;
	}


	public void setCalculationProcessDataModel(GenericDataModel<BillingCalculationProcess> calculationProcessDataModel) {
		this.calculationProcessDataModel = calculationProcessDataModel;
	}


	public int getTotalAccepted() {
		return totalAccepted;
	}


	public void setTotalAccepted(int totalAccepted) {
		this.totalAccepted = totalAccepted;
	}


	public SendInterfaceXmlSapTO getSelectedForwarding() {
		return selectedForwarding;
	}


	public void setSelectedForwarding(SendInterfaceXmlSapTO selectedForwarding) {
		this.selectedForwarding = selectedForwarding;
	}


	public List<BillingCalculationProcess> getListSelecteds() {
		return listSelecteds;
	}


	public void setListSelecteds(List<BillingCalculationProcess> listSelecteds) {
		this.listSelecteds = listSelecteds;
	}


	public Integer getCollectionInstitutionSelected() {
		return collectionInstitutionSelected;
	}


	public void setCollectionInstitutionSelected(Integer collectionInstitutionSelected) {
		this.collectionInstitutionSelected = collectionInstitutionSelected;
	}


	public Integer getCollectionPeriodSelected() {
		return collectionPeriodSelected;
	}


	public void setCollectionPeriodSelected(Integer collectionPeriodSelected) {
		this.collectionPeriodSelected = collectionPeriodSelected;
	}


	public BillingService getBillingServiceSelection() {
		return billingServiceSelection;
	}


	public void setBillingServiceSelection(BillingService billingServiceSelection) {
		this.billingServiceSelection = billingServiceSelection;
	}


	public boolean isDisableHolderECVisible() {
		return disableHolderECVisible;
	}


	public void setDisableHolderECVisible(boolean disableHolderECVisible) {
		this.disableHolderECVisible = disableHolderECVisible;
	}


	public boolean isDisableIssuerECVisible() {
		return disableIssuerECVisible;
	}


	public void setDisableIssuerECVisible(boolean disableIssuerECVisible) {
		this.disableIssuerECVisible = disableIssuerECVisible;
	}


	public boolean isDisableParticipantECVisible() {
		return disableParticipantECVisible;
	}


	public void setDisableParticipantECVisible(boolean disableParticipantECVisible) {
		this.disableParticipantECVisible = disableParticipantECVisible;
	}


	public boolean isDisableParticipantAFPECVisible() {
		return disableParticipantAFPECVisible;
	}


	public void setDisableParticipantAFPECVisible(boolean disableParticipantAFPECVisible) {
		this.disableParticipantAFPECVisible = disableParticipantAFPECVisible;
	}


	public boolean isDisableOtherECVisible() {
		return disableOtherECVisible;
	}


	public void setDisableOtherECVisible(boolean disableOtherECVisible) {
		this.disableOtherECVisible = disableOtherECVisible;
	}


	public Issuer getIssuer() {
		return issuer;
	}


	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}


	public Holder getHolderSelected() {
		return holderSelected;
	}


	public void setHolderSelected(Holder holderSelected) {
		this.holderSelected = holderSelected;
	}


	public List<ParameterTable> getListCollectionInstitution() {
		return listCollectionInstitution;
	}


	public void setListCollectionInstitution(List<ParameterTable> listCollectionInstitution) {
		this.listCollectionInstitution = listCollectionInstitution;
	}


	public GenericDataModel<SendInterfaceXmlSapTO> getSendInterfaceXmlDataModel() {
		return sendInterfaceXmlDataModel;
	}


	public void setSendInterfaceXmlDataModel(GenericDataModel<SendInterfaceXmlSapTO> sendInterfaceXmlDataModel) {
		this.sendInterfaceXmlDataModel = sendInterfaceXmlDataModel;
	}
	
	
}
