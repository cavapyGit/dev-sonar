package com.pradera.billing.billing.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.model.accounts.Holder;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingProcessedFilter.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class BillingProcessedFilter implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The process billing calculation pk. */
	private Long    processBillingCalculationPk;
	
	/** The billing service pk. */
	private Long 	billingServicePk;
	
	/** The entity collection. */
	private Integer entityCollection;
	
	/** The document code other. */
	private String 	documentCodeOther;
	
	/** The document type other. */
	private Integer documentTypeOther;
	
	/** The description other. */
	private String	descriptionOther;
	
	/** The participant pk. */
	private Long 	participantPk;
	
	/** The issuer pk. */
	private String  issuerPk;
	
	/** The holder pk. */
	private Long 	holderPk;

	
	/** The collection period. */
	private Integer collectionPeriod;

	/** The month billing. */
	private Integer	monthBilling;
	
	/** The year billing. */
	private Integer	yearBilling;
	
	/** The billing date. */
	private Date 	billingDate;
	
	/** The billing date start. */
	private Date 	billingDateStart;
	
	/** The billing date end. */
	private Date 	billingDateEnd;
	
	/** The ind integrated bill. */
	private Integer indIntegratedBill=-1;
	
	/** The processed state. */
	private Integer	processedState;
	
	/** The billing processed state. */
	private Integer	billingProcessedState;
	
	/** Date input Screen. */
	private Date 	billingDateBecoming;
	
	/** The day Weedk*/
	private String dayOfWeek;
	
	private Long idHolderByParticipant;
	
		
	private List<Holder> holderList;
	
	private Integer numberBilling;
	
	/**
	 * Gets the process billing calculation pk.
	 *
	 * @return the processBillingCalculationPk
	 */
	public Long getProcessBillingCalculationPk() {
		return processBillingCalculationPk;
	}
	
	/**
	 * Sets the process billing calculation pk.
	 *
	 * @param processBillingCalculationPk the processBillingCalculationPk to set
	 */
	public void setProcessBillingCalculationPk(Long processBillingCalculationPk) {
		this.processBillingCalculationPk = processBillingCalculationPk;
	}
	
	/**
	 * Gets the billing service pk.
	 *
	 * @return the billingServicePk
	 */
	public Long getBillingServicePk() {
		return billingServicePk;
	}
	
	/**
	 * Sets the billing service pk.
	 *
	 * @param billingServicePk the billingServicePk to set
	 */
	public void setBillingServicePk(Long billingServicePk) {
		this.billingServicePk = billingServicePk;
	}
	
	/**
	 * Gets the entity collection.
	 *
	 * @return the entityCollection
	 */
	public Integer getEntityCollection() {
		return entityCollection;
	}
	
	/**
	 * Sets the entity collection.
	 *
	 * @param entityCollection the entityCollection to set
	 */
	public void setEntityCollection(Integer entityCollection) {
		this.entityCollection = entityCollection;
	}
	
	/**
	 * Gets the document code other.
	 *
	 * @return the documentCodeOther
	 */
	public String getDocumentCodeOther() {
		return documentCodeOther;
	}
	
	/**
	 * Sets the document code other.
	 *
	 * @param documentCodeOther the documentCodeOther to set
	 */
	public void setDocumentCodeOther(String documentCodeOther) {
		this.documentCodeOther = documentCodeOther;
	}
	
	/**
	 * Gets the document type other.
	 *
	 * @return the documentTypeOther
	 */
	public Integer getDocumentTypeOther() {
		return documentTypeOther;
	}
	
	/**
	 * Sets the document type other.
	 *
	 * @param documentTypeOther the documentTypeOther to set
	 */
	public void setDocumentTypeOther(Integer documentTypeOther) {
		this.documentTypeOther = documentTypeOther;
	}
	
	/**
	 * Gets the description other.
	 *
	 * @return the descriptionOther
	 */
	public String getDescriptionOther() {
		return descriptionOther;
	}
	
	/**
	 * Sets the description other.
	 *
	 * @param descriptionOther the descriptionOther to set
	 */
	public void setDescriptionOther(String descriptionOther) {
		this.descriptionOther = descriptionOther;
	}
	
	/**
	 * Gets the participant pk.
	 *
	 * @return the participantPk
	 */
	public Long getParticipantPk() {
		return participantPk;
	}
	
	/**
	 * Sets the participant pk.
	 *
	 * @param participantPk the participantPk to set
	 */
	public void setParticipantPk(Long participantPk) {
		this.participantPk = participantPk;
	}
	
	/**
	 * Gets the issuer pk.
	 *
	 * @return the issuerPk
	 */
	public String getIssuerPk() {
		return issuerPk;
	}
	
	/**
	 * Sets the issuer pk.
	 *
	 * @param issuerPk the issuerPk to set
	 */
	public void setIssuerPk(String issuerPk) {
		this.issuerPk = issuerPk;
	}
	
	/**
	 * Gets the holder pk.
	 *
	 * @return the holderPk
	 */
	public Long getHolderPk() {
		return holderPk;
	}
	
	/**
	 * Sets the holder pk.
	 *
	 * @param holderPk the holderPk to set
	 */
	public void setHolderPk(Long holderPk) {
		this.holderPk = holderPk;
	}
	
	/**
	 * Gets the collection period.
	 *
	 * @return the collectionPeriod
	 */
	public Integer getCollectionPeriod() {
		return collectionPeriod;
	}
	
	/**
	 * Sets the collection period.
	 *
	 * @param collectionPeriod the collectionPeriod to set
	 */
	public void setCollectionPeriod(Integer collectionPeriod) {
		this.collectionPeriod = collectionPeriod;
	}
	
	/**
	 * Gets the month billing.
	 *
	 * @return the monthBilling
	 */
	public Integer getMonthBilling() {
		return monthBilling;
	}
	
	/**
	 * Sets the month billing.
	 *
	 * @param monthBilling the monthBilling to set
	 */
	public void setMonthBilling(Integer monthBilling) {
		this.monthBilling = monthBilling;
	}
	
	/**
	 * Gets the year billing.
	 *
	 * @return the yearBilling
	 */
	public Integer getYearBilling() {
		return yearBilling;
	}
	
	/**
	 * Sets the year billing.
	 *
	 * @param yearBilling the yearBilling to set
	 */
	public void setYearBilling(Integer yearBilling) {
		this.yearBilling = yearBilling;
	}
	
	/**
	 * Gets the billing date.
	 *
	 * @return the billingDate
	 */
	public Date getBillingDate() {
		return billingDate;
	}
	
	/**
	 * Sets the billing date.
	 *
	 * @param billingDate the billingDate to set
	 */
	public void setBillingDate(Date billingDate) {
		this.billingDate = billingDate;
	}
	
	/**
	 * Gets the billing date start.
	 *
	 * @return the billingDateStart
	 */
	public Date getBillingDateStart() {
		return billingDateStart;
	}
	
	/**
	 * Sets the billing date start.
	 *
	 * @param billingDateStart the billingDateStart to set
	 */
	public void setBillingDateStart(Date billingDateStart) {
		this.billingDateStart = billingDateStart;
	}
	
	/**
	 * Gets the billing date end.
	 *
	 * @return the billingDateEnd
	 */
	public Date getBillingDateEnd() {
		return billingDateEnd;
	}
	
	/**
	 * Sets the billing date end.
	 *
	 * @param billingDateEnd the billingDateEnd to set
	 */
	public void setBillingDateEnd(Date billingDateEnd) {
		this.billingDateEnd = billingDateEnd;
	}
	
	/**
	 * Gets the ind integrated bill.
	 *
	 * @return the indIntegratedBill
	 */
	public Integer getIndIntegratedBill() {
		return indIntegratedBill;
	}
	
	/**
	 * Sets the ind integrated bill.
	 *
	 * @param indIntegratedBill the indIntegratedBill to set
	 */
	public void setIndIntegratedBill(Integer indIntegratedBill) {
		this.indIntegratedBill = indIntegratedBill;
	}
	
	/**
	 * Gets the processed state.
	 *
	 * @return the processedState
	 */
	public Integer getProcessedState() {
		return processedState;
	}
	
	/**
	 * Sets the processed state.
	 *
	 * @param processedState the processedState to set
	 */
	public void setProcessedState(Integer processedState) {
		this.processedState = processedState;
	}
	
	/**
	 * Gets the billing processed state.
	 *
	 * @return the billingProcessedState
	 */
	public Integer getBillingProcessedState() {
		return billingProcessedState;
	}
	
	/**
	 * Sets the billing processed state.
	 *
	 * @param billingProcessedState the billingProcessedState to set
	 */
	public void setBillingProcessedState(Integer billingProcessedState) {
		this.billingProcessedState = billingProcessedState;
	}
	
	/**
	 * Gets the billing date becoming.
	 *
	 * @return the billingDateBecoming
	 */
	public Date getBillingDateBecoming() {
		return billingDateBecoming;
	}
	
	/**
	 * Sets the billing date becoming.
	 *
	 * @param billingDateBecoming the billingDateBecoming to set
	 */
	public void setBillingDateBecoming(Date billingDateBecoming) {
		this.billingDateBecoming = billingDateBecoming;
	}
	
	/**
	 * @return the dayOfWeek
	 */
	public String getDayOfWeek() {
		return dayOfWeek;
	}

	/**
	 * @param dayOfWeek the dayOfWeek to set
	 */
	public void setDayOfWeek(String dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	/**
	 * @return the idHolderByParticipant
	 */
	public Long getIdHolderByParticipant() {
		return idHolderByParticipant;
	}

	/**
	 * @param idHolderByParticipant the idHolderByParticipant to set
	 */
	public void setIdHolderByParticipant(Long idHolderByParticipant) {
		this.idHolderByParticipant = idHolderByParticipant;
	}

	public List<Holder> getHolderList() {
		return holderList;
	}

	public void setHolderList(List<Holder> holderList) {
		this.holderList = holderList;
	}

	/**
	 * @return the numberBilling
	 */
	public Integer getNumberBilling() {
		return numberBilling;
	}

	/**
	 * @param numberBilling the numberBilling to set
	 */
	public void setNumberBilling(Integer numberBilling) {
		this.numberBilling = numberBilling;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BillingProcessedFilter [");
		if (processBillingCalculationPk != null)
			builder.append("processBillingCalculationPk=").append(processBillingCalculationPk).append(", ");
		if (billingServicePk != null)
			builder.append("billingServicePk=").append(billingServicePk).append(", ");
		if (entityCollection != null)
			builder.append("entityCollection=").append(entityCollection).append(", ");
		if (documentCodeOther != null)
			builder.append("documentCodeOther=").append(documentCodeOther).append(", ");
		if (documentTypeOther != null)
			builder.append("documentTypeOther=").append(documentTypeOther).append(", ");
		if (descriptionOther != null)
			builder.append("descriptionOther=").append(descriptionOther).append(", ");
		if (participantPk != null)
			builder.append("participantPk=").append(participantPk).append(", ");
		if (issuerPk != null)
			builder.append("issuerPk=").append(issuerPk).append(", ");
		if (holderPk != null)
			builder.append("holderPk=").append(holderPk).append(", ");
		if (collectionPeriod != null)
			builder.append("collectionPeriod=").append(collectionPeriod).append(", ");
		if (monthBilling != null)
			builder.append("monthBilling=").append(monthBilling).append(", ");
		if (yearBilling != null)
			builder.append("yearBilling=").append(yearBilling).append(", ");
		if (billingDate != null)
			builder.append("billingDate=").append(billingDate).append(", ");
		if (billingDateStart != null)
			builder.append("billingDateStart=").append(billingDateStart).append(", ");
		if (billingDateEnd != null)
			builder.append("billingDateEnd=").append(billingDateEnd).append(", ");
		if (indIntegratedBill != null)
			builder.append("indIntegratedBill=").append(indIntegratedBill).append(", ");
		if (processedState != null)
			builder.append("processedState=").append(processedState).append(", ");
		if (billingProcessedState != null)
			builder.append("billingProcessedState=").append(billingProcessedState).append(", ");
		if (billingDateBecoming != null)
			builder.append("billingDateBecoming=").append(billingDateBecoming).append(", ");
		if (dayOfWeek != null)
			builder.append("dayOfWeek=").append(dayOfWeek).append(", ");
		if (idHolderByParticipant != null)
			builder.append("idHolderByParticipant=").append(idHolderByParticipant);
		builder.append("]");
		return builder.toString();
	}

	
	
}
