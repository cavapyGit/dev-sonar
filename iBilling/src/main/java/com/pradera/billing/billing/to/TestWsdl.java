package com.pradera.billing.billing.to;

import javax.xml.soap.*;

public class TestWsdl {

    // SAAJ - SOAP Client Testing
        public static void main(String args[]) {

            String soapEndpointUrl = "http://192.168.100.148:81/ServicioIntegracion.svc";
            String soapAction = "http://tempuri.org/IServicioIntegracion/ServFactura";

            callSoapWebService(soapEndpointUrl, soapAction);
        }

        private static void createSoapEnvelope(SOAPMessage soapMessage) throws SOAPException {
            SOAPPart soapPart = soapMessage.getSOAPPart();

            String myNamespace = "tem";
            String myNamespaceURI = "http://tempuri.org/";
            
            
            String myNameSpaceChild ="ied";
            String myNamespaceURIchild = "http://schemas.datacontract.org/2004/07/IEDVServicios";
            // SOAP Envelope
            SOAPEnvelope envelope = soapPart.getEnvelope();
            envelope.addNamespaceDeclaration(myNamespace, myNamespaceURI);
            envelope.addNamespaceDeclaration(myNameSpaceChild, myNamespaceURIchild);

             /*
               <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/" xmlns:ied="http://schemas.datacontract.org/2004/07/IEDVServicios">
			   <soapenv:Header/>
			   <soapenv:Body>
			      <tem:ServFactura>
			         <!--Optional:-->
			         <tem:facturaC>
			            <!--Zero or more repetitions:-->
			            <ied:FacturaC>
			               <!--Optional:-->
			               <ied:CLIENTE>2</ied:CLIENTE>
			               <!--Optional:-->
			               <ied:CLIENTE_FACTURAR>3</ied:CLIENTE_FACTURAR>
			               <!--Optional:-->
			               <ied:CUENTA>3</ied:CUENTA>
			               <!--Optional:-->
			               <ied:DESCRIPCION>test1</ied:DESCRIPCION>
			               <!--Optional:-->
			               <ied:Detalle>
			                  <!--Zero or more repetitions:-->
			                  <ied:FacturaD>
			                     <!--Optional:-->
			                     <ied:CANTIDAD>1</ied:CANTIDAD>
			                     <!--Optional:-->
			                     <ied:ITEM>ITEM003</ied:ITEM>
			                     <!--Optional:-->
			                     <ied:PRECIO>1</ied:PRECIO>
			                  </ied:FacturaD>
			               </ied:Detalle>
			               <!--Optional:-->
			               <ied:FECOBRO>01/06/2020</ied:FECOBRO>
			               <!--Optional:-->
			               <ied:IMPUESTO>1</ied:IMPUESTO>
			               <!--Optional:-->
			               <ied:MONEDA>BOB</ied:MONEDA>
			               <!--Optional:-->
			               <ied:NUMCXC>100</ied:NUMCXC>
			               <!--Optional:-->
			               <ied:NUMFACT>4</ied:NUMFACT>
			               <!--Optional:-->
			               <ied:REF_AUX>11</ied:REF_AUX>
			               <!--Optional:-->
			               <ied:TIPOSERVICIO>2</ied:TIPOSERVICIO>
			            </ied:FacturaC>
			         </tem:facturaC>
			      </tem:ServFactura>
			   </soapenv:Body>
			</soapenv:Envelope>
             */
            // SOAP Body
            SOAPBody soapBody = envelope.getBody();
            SOAPElement servFactura = soapBody.addChildElement("ServFactura", myNamespace);
            SOAPElement facturaC = servFactura.addChildElement("facturaC", myNamespace);
            SOAPElement FacturaC = facturaC.addChildElement("FacturaC",myNameSpaceChild);
            
            SOAPElement client = FacturaC.addChildElement("CLIENTE",myNameSpaceChild);
            client.addTextNode("1");
            
            SOAPElement clientInvoice = FacturaC.addChildElement("CLIENTE_FACTURAR",myNameSpaceChild);
            clientInvoice.addTextNode("1");
            
            SOAPElement account = FacturaC.addChildElement("CUENTA",myNameSpaceChild);
            account.addTextNode("3");
            
            SOAPElement description = FacturaC.addChildElement("DESCRIPCION",myNameSpaceChild);
            description.addTextNode("test");
            
            SOAPElement detail = FacturaC.addChildElement("Detalle",myNameSpaceChild);
            
            SOAPElement invoiceD = detail.addChildElement("FacturaD",myNameSpaceChild);
            
            SOAPElement quantity = invoiceD.addChildElement("CANTIDAD",myNameSpaceChild);
            quantity.addTextNode("1");
            
            SOAPElement item = invoiceD.addChildElement("ITEM",myNameSpaceChild);
            item.addTextNode("ITEM003");
            
            SOAPElement price = invoiceD.addChildElement("PRECIO",myNameSpaceChild);
            price.addTextNode("1");
            
            SOAPElement paymentDate = FacturaC.addChildElement("FECOBRO",myNameSpaceChild);
            paymentDate.addTextNode("01/06/2020");
            
            SOAPElement tax = FacturaC.addChildElement("IMPUESTO",myNameSpaceChild);
            tax.addTextNode("1");
            
            FacturaC.addChildElement("MONEDA",myNameSpaceChild).addTextNode("BOB");
            
            FacturaC.addChildElement("NUMCXC",myNameSpaceChild).addTextNode("10");
            
            FacturaC.addChildElement("NUMFAC",myNameSpaceChild).addTextNode("7");
            
            FacturaC.addChildElement("REF_AUX",myNameSpaceChild).addTextNode("11");
            
            FacturaC.addChildElement("TIPOSERVICIO",myNameSpaceChild).addTextNode("2");
            
        }

        private static void callSoapWebService(String soapEndpointUrl, String soapAction) {
            try {
                // Create SOAP Connection
                SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
                SOAPConnection soapConnection = soapConnectionFactory.createConnection();

                // Send SOAP Message to SOAP Server
                SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(soapAction), soapEndpointUrl);

                // Print the SOAP Response
                System.out.println("Response SOAP Message:");
                soapResponse.writeTo(System.out);
                System.out.println();

                soapConnection.close();
            } catch (Exception e) {
                System.err.println("\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
                e.printStackTrace();
            }
        }

        private static SOAPMessage createSOAPRequest(String soapAction) throws Exception {
            MessageFactory messageFactory = MessageFactory.newInstance();
            SOAPMessage soapMessage = messageFactory.createMessage();

            createSoapEnvelope(soapMessage);

            MimeHeaders headers = soapMessage.getMimeHeaders();
            headers.addHeader("SOAPAction", soapAction);

            soapMessage.saveChanges();

            /* Print the request message, just for debugging purposes */
            System.out.println("Request SOAP Message:");
            soapMessage.writeTo(System.out);
            System.out.println("\n");

            return soapMessage;
        }

    }