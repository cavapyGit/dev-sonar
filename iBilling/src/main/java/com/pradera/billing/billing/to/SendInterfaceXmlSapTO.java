package com.pradera.billing.billing.to;

import java.io.Serializable;
import java.util.Date;


public class SendInterfaceXmlSapTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4583426120617175596L;
	
	private Long idSendInterfaceXmlSapPk;
	
	private String responseValue;
	
	private Integer numberTotal;
	
	private Integer numberDuplicate;
	
	private Integer numberAccepted;
	
	private Integer numberRejected;
	
	private Integer numberReSend;
	
	private Integer idTicketSap;
	
	private String invoiceList;
	
	private String invoiceListAll;
	
	private Date registreDate;
	
	private Long idBillingCalcProcessFk;
	
	private Integer state;
	
	private Date billingDate;
	
	private Integer collectionIntity;
	
	private String descCollectionIntity;
	
	private Integer collectionPeriod;
	
	private String descCollectionPeriod;
	
	private String serviceBillingCode;
	
	private String descStatus;
	
	private byte[] xmlSoapSendMessage;
	
	private byte[] xmlSoapRecepMessage;
	
	public SendInterfaceXmlSapTO() {
		responseValue = "";
		numberDuplicate = 0;
		numberRejected = 0;
		numberAccepted = 0;
		numberTotal = 0;
		registreDate = null;
		numberReSend = 0;
	}

	public Long getIdSendInterfaceXmlSapPk() {
		return idSendInterfaceXmlSapPk;
	}

	public void setIdSendInterfaceXmlSapPk(Long idSendInterfaceXmlSapPk) {
		this.idSendInterfaceXmlSapPk = idSendInterfaceXmlSapPk;
	}

	public String getResponseValue() {
		return responseValue;
	}

	public void setResponseValue(String responseValue) {
		this.responseValue = responseValue;
	}

	public Integer getNumberTotal() {
		return numberTotal;
	}

	public void setNumberTotal(Integer numberTotal) {
		this.numberTotal = numberTotal;
	}

	public Integer getNumberDuplicate() {
		return numberDuplicate;
	}

	public void setNumberDuplicate(Integer numberDuplicate) {
		this.numberDuplicate = numberDuplicate;
	}

	public Integer getNumberAccepted() {
		return numberAccepted;
	}

	public void setNumberAccepted(Integer numberAccepted) {
		this.numberAccepted = numberAccepted;
	}

	public Integer getNumberRejected() {
		return numberRejected;
	}

	public void setNumberRejected(Integer numberRejected) {
		this.numberRejected = numberRejected;
	}

	public Integer getNumberReSend() {
		return numberReSend;
	}

	public void setNumberReSend(Integer numberReSend) {
		this.numberReSend = numberReSend;
	}

	public Integer getIdTicketSap() {
		return idTicketSap;
	}

	public void setIdTicketSap(Integer idTicketSap) {
		this.idTicketSap = idTicketSap;
	}

	public String getInvoiceList() {
		return invoiceList;
	}

	public void setInvoiceList(String invoiceList) {
		this.invoiceList = invoiceList;
	}

	public String getInvoiceListAll() {
		return invoiceListAll;
	}

	public void setInvoiceListAll(String invoiceListAll) {
		this.invoiceListAll = invoiceListAll;
	}

	public Date getRegistreDate() {
		return registreDate;
	}

	public void setRegistreDate(Date registreDate) {
		this.registreDate = registreDate;
	}

	public Long getIdBillingCalcProcessFk() {
		return idBillingCalcProcessFk;
	}

	public void setIdBillingCalcProcessFk(Long idBillingCalcProcessFk) {
		this.idBillingCalcProcessFk = idBillingCalcProcessFk;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Date getBillingDate() {
		return billingDate;
	}

	public void setBillingDate(Date billingDate) {
		this.billingDate = billingDate;
	}

	public Integer getCollectionIntity() {
		return collectionIntity;
	}

	public void setCollectionIntity(Integer collectionIntity) {
		this.collectionIntity = collectionIntity;
	}

	public String getDescCollectionIntity() {
		return descCollectionIntity;
	}

	public void setDescCollectionIntity(String descCollectionIntity) {
		this.descCollectionIntity = descCollectionIntity;
	}

	public Integer getCollectionPeriod() {
		return collectionPeriod;
	}

	public void setCollectionPeriod(Integer collectionPeriod) {
		this.collectionPeriod = collectionPeriod;
	}

	public String getDescCollectionPeriod() {
		return descCollectionPeriod;
	}

	public void setDescCollectionPeriod(String descCollectionPeriod) {
		this.descCollectionPeriod = descCollectionPeriod;
	}

	public String getServiceBillingCode() {
		return serviceBillingCode;
	}

	public void setServiceBillingCode(String serviceBillingCode) {
		this.serviceBillingCode = serviceBillingCode;
	}

	public String getDescStatus() {
		return descStatus;
	}

	public void setDescStatus(String descStatus) {
		this.descStatus = descStatus;
	}

	public byte[] getXmlSoapSendMessage() {
		return xmlSoapSendMessage;
	}

	public void setXmlSoapSendMessage(byte[] xmlSoapSendMessage) {
		this.xmlSoapSendMessage = xmlSoapSendMessage;
	}

	public byte[] getXmlSoapRecepMessage() {
		return xmlSoapRecepMessage;
	}

	public void setXmlSoapRecepMessage(byte[] xmlSoapRecepMessage) {
		this.xmlSoapRecepMessage = xmlSoapRecepMessage;
	}
}
