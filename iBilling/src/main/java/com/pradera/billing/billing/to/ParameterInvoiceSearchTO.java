package com.pradera.billing.billing.to;

import java.io.Serializable;
import java.util.Date;

import com.pradera.commons.utils.CommonsUtilities;

public class ParameterInvoiceSearchTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5117619940005818609L;
	
	private Date billingDate;
	
	private Date registreInitDate;
	
	private Date registreFinalDate;
	
	public ParameterInvoiceSearchTO() {
		//fecha de facturacion en la pantalla de busqueda
		billingDate = null;
		registreInitDate = CommonsUtilities.currentDate();
		registreFinalDate = CommonsUtilities.currentDate();
	}

	public Date getBillingDate() {
		return billingDate;
	}

	public void setBillingDate(Date billingDate) {
		this.billingDate = billingDate;
	}

	public Date getRegistreInitDate() {
		return registreInitDate;
	}

	public void setRegistreInitDate(Date registreInitDate) {
		this.registreInitDate = registreInitDate;
	}

	public Date getRegistreFinalDate() {
		return registreFinalDate;
	}

	public void setRegistreFinalDate(Date registreFinalDate) {
		this.registreFinalDate = registreFinalDate;
	}
}
