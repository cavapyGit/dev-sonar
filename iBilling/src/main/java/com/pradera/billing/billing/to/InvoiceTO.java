package com.pradera.billing.billing.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


public class InvoiceTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3235832413471047305L;

	private Integer numCxc;
	
	private String account;

	private String client;
	
	private String description;
	
	private String tax;//impuesto

	private String currency;
	
	private Integer quantity;
	
	private BigDecimal price;
	
	private Date date ;//fecha de cobro
	
	private String serviceType;
	
	private String refAux;
	
	public InvoiceTO() {
	}

	public Integer getNumCxc() {
		return numCxc;
	}

	public void setNumCxc(Integer numCxc) {
		this.numCxc = numCxc;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getRefAux() {
		return refAux;
	}

	public void setRefAux(String refAux) {
		this.refAux = refAux;
	}
	
}
