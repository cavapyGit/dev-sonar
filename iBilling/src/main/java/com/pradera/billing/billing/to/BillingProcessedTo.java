package com.pradera.billing.billing.to;

import java.io.Serializable;
import java.util.Date;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingProcessedTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class BillingProcessedTo implements Serializable {
	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id billing calc process pk. */
	private Long idBillingCalcProcessPk;

	/** The billing period. */
	private Integer billingPeriod;

	/** The operation date. */
	private Date operationDate;

	/** The billing date. */
	private Date billingDate;
	
	/** The processed billing number. */
	private Integer processedBillingNumber;
	
	/** The billing year. */
	private Integer billingYear;
	
	/** The billing month. */
	private Integer billingMonth;
	
	/** The processed state. */
	private Integer processedState;
	
	/** The entity description. */
	private String entityDescription;
	
	/** The entity collection. */
	private Integer entityCollection;
	
	/** Count the billing detail. */
	private Integer countBillingDetail;
	
	
	/** Descriptions. */
	private String descriptionBillingPeriod;
	
	/** The description processed state. */
	private String descriptionProcessedState;
	
	/** The description entity collection. */
	private String descriptionEntityCollection;
	
	/** The description name entity collection. */
	private String descriptionNameEntityCollection;
	
	/** The billing service code. */
	private String billingServiceCode;
	
	/**
	 * Gets the id billing calc process pk.
	 *
	 * @return the idBillingCalcProcessPk
	 */
	public Long getIdBillingCalcProcessPk() {
		return idBillingCalcProcessPk;
	}
	
	/**
	 * Sets the id billing calc process pk.
	 *
	 * @param idBillingCalcProcessPk the idBillingCalcProcessPk to set
	 */
	public void setIdBillingCalcProcessPk(Long idBillingCalcProcessPk) {
		this.idBillingCalcProcessPk = idBillingCalcProcessPk;
	}
	
	/**
	 * Gets the billing period.
	 *
	 * @return the billingPeriod
	 */
	public Integer getBillingPeriod() {
		return billingPeriod;
	}
	
	/**
	 * Sets the billing period.
	 *
	 * @param billingPeriod the billingPeriod to set
	 */
	public void setBillingPeriod(Integer billingPeriod) {
		this.billingPeriod = billingPeriod;
	}
	
	/**
	 * Gets the operation date.
	 *
	 * @return the operationDate
	 */
	public Date getOperationDate() {
		return operationDate;
	}
	
	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the operationDate to set
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}
	
	/**
	 * Gets the billing date.
	 *
	 * @return the billingDate
	 */
	public Date getBillingDate() {
		return billingDate;
	}
	
	/**
	 * Sets the billing date.
	 *
	 * @param billingDate the billingDate to set
	 */
	public void setBillingDate(Date billingDate) {
		this.billingDate = billingDate;
	}
	
	/**
	 * Gets the processed billing number.
	 *
	 * @return the processedBillingNumber
	 */
	public Integer getProcessedBillingNumber() {
		return processedBillingNumber;
	}
	
	/**
	 * Sets the processed billing number.
	 *
	 * @param processedBillingNumber the processedBillingNumber to set
	 */
	public void setProcessedBillingNumber(Integer processedBillingNumber) {
		this.processedBillingNumber = processedBillingNumber;
	}
	
	/**
	 * Gets the billing year.
	 *
	 * @return the billingYear
	 */
	public Integer getBillingYear() {
		return billingYear;
	}
	
	/**
	 * Sets the billing year.
	 *
	 * @param billingYear the billingYear to set
	 */
	public void setBillingYear(Integer billingYear) {
		this.billingYear = billingYear;
	}
	
	/**
	 * Gets the billing month.
	 *
	 * @return the billingMonth
	 */
	public Integer getBillingMonth() {
		return billingMonth;
	}
	
	/**
	 * Sets the billing month.
	 *
	 * @param billingMonth the billingMonth to set
	 */
	public void setBillingMonth(Integer billingMonth) {
		this.billingMonth = billingMonth;
	}
	
	/**
	 * Gets the processed state.
	 *
	 * @return the processedState
	 */
	public Integer getProcessedState() {
		return processedState;
	}
	
	/**
	 * Sets the processed state.
	 *
	 * @param processedState the processedState to set
	 */
	public void setProcessedState(Integer processedState) {
		this.processedState = processedState;
	}
	
	/**
	 * Gets the entity description.
	 *
	 * @return the entityDescription
	 */
	public String getEntityDescription() {
		return entityDescription;
	}
	
	/**
	 * Sets the entity description.
	 *
	 * @param entityDescription the entityDescription to set
	 */
	public void setEntityDescription(String entityDescription) {
		this.entityDescription = entityDescription;
	}
	
	/**
	 * Gets the entity collection.
	 *
	 * @return the entityCollection
	 */
	public Integer getEntityCollection() {
		return entityCollection;
	}
	
	/**
	 * Sets the entity collection.
	 *
	 * @param entityCollection the entityCollection to set
	 */
	public void setEntityCollection(Integer entityCollection) {
		this.entityCollection = entityCollection;
	}
	
	/**
	 * Gets the count billing detail.
	 *
	 * @return the countBillingDetail
	 */
	public Integer getCountBillingDetail() {
		return countBillingDetail;
	}
	
	/**
	 * Sets the count billing detail.
	 *
	 * @param countBillingDetail the countBillingDetail to set
	 */
	public void setCountBillingDetail(Integer countBillingDetail) {
		this.countBillingDetail = countBillingDetail;
	}
	
	/**
	 * Gets the description billing period.
	 *
	 * @return the descriptionBillingPeriod
	 */
	public String getDescriptionBillingPeriod() {
		return descriptionBillingPeriod;
	}
	
	/**
	 * Sets the description billing period.
	 *
	 * @param descriptionBillingPeriod the descriptionBillingPeriod to set
	 */
	public void setDescriptionBillingPeriod(String descriptionBillingPeriod) {
		this.descriptionBillingPeriod = descriptionBillingPeriod;
	}
	
	/**
	 * Gets the description processed state.
	 *
	 * @return the descriptionProcessedState
	 */
	public String getDescriptionProcessedState() {
		return descriptionProcessedState;
	}
	
	/**
	 * Sets the description processed state.
	 *
	 * @param descriptionProcessedState the descriptionProcessedState to set
	 */
	public void setDescriptionProcessedState(String descriptionProcessedState) {
		this.descriptionProcessedState = descriptionProcessedState;
	}
	
	/**
	 * Gets the description entity collection.
	 *
	 * @return the descriptionEntityCollection
	 */
	public String getDescriptionEntityCollection() {
		return descriptionEntityCollection;
	}
	
	/**
	 * Sets the description entity collection.
	 *
	 * @param descriptionEntityCollection the descriptionEntityCollection to set
	 */
	public void setDescriptionEntityCollection(String descriptionEntityCollection) {
		this.descriptionEntityCollection = descriptionEntityCollection;
	}
	
	/**
	 * Gets the description name entity collection.
	 *
	 * @return the descriptionNameEntityCollection
	 */
	public String getDescriptionNameEntityCollection() {
		return descriptionNameEntityCollection;
	}
	
	/**
	 * Sets the description name entity collection.
	 *
	 * @param descriptionNameEntityCollection the descriptionNameEntityCollection to set
	 */
	public void setDescriptionNameEntityCollection(
			String descriptionNameEntityCollection) {
		this.descriptionNameEntityCollection = descriptionNameEntityCollection;
	}
	
	/**
	 * Gets the billing service code.
	 *
	 * @return the billingServiceCode
	 */
	public String getBillingServiceCode() {
		return billingServiceCode;
	}
	
	/**
	 * Sets the billing service code.
	 *
	 * @param billingServiceCode the billingServiceCode to set
	 */
	public void setBillingServiceCode(String billingServiceCode) {
		this.billingServiceCode = billingServiceCode;
	}

	
	
}
