package com.pradera.billing.billing.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import com.pradera.billing.billing.to.BillingProcessedFilter;
import com.pradera.billing.billing.to.BillingProcessedTo;
import com.pradera.billing.billing.to.ParameterInvoiceSearchTO;
import com.pradera.billing.billing.to.SendInterfaceXmlSapTO;
import com.pradera.billing.billingservice.type.BillingServiceStatus;
import com.pradera.billing.common.utils.BillingUtils;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.accounts.Participant;
import com.pradera.model.auditprocess.type.StateSapResponseType;
import com.pradera.model.billing.BillingCalculation;
import com.pradera.model.billing.BillingCalculationProcess;
import com.pradera.model.billing.BillingSchedule;
import com.pradera.model.billing.ProcessedService;
import com.pradera.model.billing.type.BillingCalculationType;
import com.pradera.model.billing.type.BillingPeriodType;
import com.pradera.model.billing.type.EntityCollectionType;
import com.pradera.model.billing.type.InvoiceExceptionStatus;
import com.pradera.model.billing.type.ProcessedServiceType;
import com.pradera.model.billing.type.ServiceStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.externalinterface.InterfaceProcess;
import com.pradera.model.generalparameter.externalinterface.InterfaceTransaction;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingProcessedServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class BillingProcessedServiceBean extends CrudDaoServiceBean{
	
	/** The dao service. */
	@EJB
	CrudDaoServiceBean daoService;
	
	@Inject    
	private Instance<InterfaceComponentService> interfaceComponentServiceBean;

	
    //@EJB
    //InterfaceComponentService componentService;
	/**
	 * 
	 * @param billingProcessedFilter
	 * @return
	 */
	
	public List<Object[]> getScheduleMonthlyBilling(BillingProcessedFilter billingProcessedFilter ){
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT distinct   ");
		sbQuery.append("  bsc.day_of_month, bs.id_billing_service_pk, bs.entity_collection ");
		sbQuery.append("  ");
		sbQuery.append("  FROM EXCEPTION_INVOICE_GENERATIONS eig ");
		sbQuery.append("  inner join billing_schedule bsc on bsc.id_billing_schedule_pk=eig.id_billing_schedule_fk");
		sbQuery.append("  inner join billing_service bs on bs.id_billing_service_pk=eig.id_billing_service_fk");
		sbQuery.append("  ");
		sbQuery.append("  ");
		sbQuery.append(" Where   ");/** default: PROCESADO**/
		sbQuery.append(" eig.exception_state = :exceptionState ");/** default REGISTER*/
		sbQuery.append(" and bsc.collection_period = :collectionPeriod ");
		sbQuery.append(" and eig.type_exception = :typeException ");
		sbQuery.append(" and bs.STATE_SERVICE = :stateActive ");
		
		sbQuery.append(" Order By bs.id_billing_service_pk ");
		
		 Query query = em.createNativeQuery(sbQuery.toString());
		
		query.setParameter("exceptionState", InvoiceExceptionStatus.REGISTER.getCode());
		query.setParameter("collectionPeriod", billingProcessedFilter.getCollectionPeriod());
		query.setParameter("typeException", PropertiesConstants.INCLUSION_PK);
		query.setParameter("stateActive", ServiceStateType.SERVICE_STATUS_ACTIVE_PK.getCode());
		
		try{
			return query.getResultList();
		} catch(NoResultException  nrex){
			nrex.printStackTrace();
			return null;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	
	
	public BillingSchedule getScheduleByBillingService(BillingProcessedFilter billingProcessedFilter ){
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select   bsc From BillingSchedule bsc   ");
		sbQuery.append(" join fetch bsc.listExceptionInvoiceGenerations eig ");
		sbQuery.append(" inner join  eig.billingService  bs ");
		sbQuery.append(" left join  eig.billingEntityException bee  ");
		sbQuery.append(" left join  bee.participant pa  ");
		sbQuery.append(" left join  bee.holder ho  ");
		sbQuery.append(" left join  bee.issuer iss  ");
		sbQuery.append("  ");
		sbQuery.append("  ");
		sbQuery.append(" Where   ");/** default: PROCESADO**/
		sbQuery.append(" eig.exceptionState = :exceptionState ");/** default REGISTER*/
		sbQuery.append(" and bs.idBillingServicePk = :idBillingServicePk ");
		sbQuery.append(" and bsc.collectionPeriod = :collectionPeriod ");
		sbQuery.append(" and bs.entityCollection = :entityCollection ");
		Integer entityCollection =billingProcessedFilter.getEntityCollection();
		
		if(EntityCollectionType.PARTICIPANTS.getCode().equals(entityCollection) && 
				Validations.validateIsNotNullAndPositive(billingProcessedFilter.getParticipantPk())){
			sbQuery.append(" and pa.idParticipantPk = :idParticipantPk  ");
		}else if(EntityCollectionType.ISSUERS.getCode().equals(entityCollection) &&
				Validations.validateIsNotNullAndNotEmpty(billingProcessedFilter.getIssuerPk())){
			sbQuery.append(" and iss.idIssuerPk = :idIssuerPk ");
		}else if(EntityCollectionType.HOLDERS.getCode().equals(entityCollection)&&
				Validations.validateIsNotNullAndPositive(billingProcessedFilter.getHolderPk())){
			sbQuery.append(" and ho.idHolderPk = :idHolderPk");
		}
		
		sbQuery.append(" Order By bsc.idBillingSchedulePk desc");
		TypedQuery<BillingSchedule> query =  em.createQuery(sbQuery.toString(),BillingSchedule.class);
		
		query.setParameter("exceptionState", InvoiceExceptionStatus.REGISTER.getCode());
		query.setParameter("idBillingServicePk", billingProcessedFilter.getBillingServicePk());
		query.setParameter("collectionPeriod", billingProcessedFilter.getCollectionPeriod());
		query.setParameter("entityCollection", entityCollection);
		
		if(EntityCollectionType.PARTICIPANTS.getCode().equals(entityCollection) && 
				Validations.validateIsNotNullAndPositive(billingProcessedFilter.getParticipantPk())){
			query.setParameter("idParticipantPk", billingProcessedFilter.getParticipantPk());
		}else if(EntityCollectionType.ISSUERS.getCode().equals(entityCollection) &&
				Validations.validateIsNotNullAndNotEmpty(billingProcessedFilter.getIssuerPk())){
			query.setParameter("idIssuerPk", billingProcessedFilter.getIssuerPk());
		}else if(EntityCollectionType.HOLDERS.getCode().equals(entityCollection)&&
				Validations.validateIsNotNullAndPositive(billingProcessedFilter.getHolderPk())){
			query.setParameter("idHolderPk", billingProcessedFilter.getHolderPk());
		}
		BillingSchedule billingSchedule=null;
		try{
			List<BillingSchedule> listBillingSchedule= query.getResultList();
			if(Validations.validateListIsNotNullAndNotEmpty(listBillingSchedule)){
				billingSchedule=listBillingSchedule.get(0);
			}
		} catch(NoResultException  nrex){
			billingSchedule=null;
		}
		
		return billingSchedule;
	}
	
	
	
	
	
	/**
	 * Gets the processed service for calculate billing with exception.
	 *
	 * @param billingProcessedFilter the billing processed filter
	 * @return the processed service for calculate billing with exception
	 */
	public List<ProcessedService> getProcessedServiceForCalculateBillingWithException(BillingProcessedFilter billingProcessedFilter, boolean endMonth ){
		
		Integer collectionPeriod=billingProcessedFilter.getCollectionPeriod();
		Integer entityCollection=billingProcessedFilter.getEntityCollection();
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select distinct ps From ProcessedService ps  join fetch ps.billingService bs ");
		//sbQuery.append(" left join fetch  bs.listExceptionInvoiceGenerations eig ");// left join fetch
		//sbQuery.append(" join fetch eig.billingSchedule bsc ");
		sbQuery.append(" Where ps.processedState= :processedState ");/** default: PROCESADO**/
		//sbQuery.append(" AND eig.exceptionState = :exceptionState");/** default REGISTER*/
		
		/**
		 * The same Date for   COLLECTION_PERIOD_DAILY_PK and COLLECTION_PERIOD_MONTHLY_PK 
		 * 
		 * */
		sbQuery.append(" AND TRUNC(ps.calculationDate, 'DD') >= TRUNC(:startDate, 'DD') AND TRUNC(ps.calculationDate, 'DD') <= TRUNC(:endDate, 'DD')");
	
		if(Validations.validateIsNotNullAndPositive(entityCollection)){
			sbQuery.append(" AND ps.billingService.entityCollection = :entityCollection");
		}
		if(Validations.validateIsNotNullAndPositive(billingProcessedFilter.getBillingServicePk())){
			sbQuery.append(" AND ps.billingService.idBillingServicePk = :idBillingServicePk");
		}
		
		if(Validations.validateIsNotNullAndPositive(collectionPeriod)){

			//sbQuery.append(" AND bsc.collectionPeriod = :collectionPeriod");
			if(BillingPeriodType.DAILY.getCode().equals(collectionPeriod)){
				
			}else if(BillingPeriodType.WEEKLY.getCode().equals(collectionPeriod)){
				
				//sbQuery.append(" AND bsc.dayOfWeek = :dayOfWeek");
				
			}else if(BillingPeriodType.MONTHLY.getCode().equals(collectionPeriod)){
				if(!endMonth){
					//Los Schedule programados con el indicador de Fin de mes NO tienen el dia del mes
					//sbQuery.append(" AND bsc.dayOfMonth = :dayOfMonth");
				}else{
					//sbQuery.append(" AND bsc.indEndOfMonth = :indEndOfMonth");
				}
				
				
			}else if(BillingPeriodType.QUARTERLY.getCode().equals(collectionPeriod)){
				
				//sbQuery.append(" AND bsc.dayOfMonth = :dayOfMonth");
				//sbQuery.append(" AND bsc.month = :listMonthAvailable");
				
			}else if(BillingPeriodType.YEARLY.getCode().equals(collectionPeriod)){
				
				//sbQuery.append(" AND bsc.dayOfMonth = :dayOfMonth");
				//sbQuery.append(" AND bsc.month = :monthOfYear");
				
			}
		}
		

		if(Validations.validateIsNotNull(billingProcessedFilter.getIndIntegratedBill())
				&& (billingProcessedFilter.getIndIntegratedBill()>=0)){
			sbQuery.append(" AND ps.billingService.inIntegratedBill = :inIntegratedBill");
		}
					
		sbQuery.append(" ORDER BY bs.inIntegratedBill,bs.currencyBilling, bs.idBillingServicePk  ");
		
		TypedQuery<ProcessedService> query =  em.createQuery(sbQuery.toString(),ProcessedService.class);
		
		//query.setParameter("exceptionState", InvoiceExceptionStatus.REGISTER.getCode());
		query.setParameter("processedState", ProcessedServiceType.PROCESADO.getCode());

		query.setParameter("startDate", billingProcessedFilter.getBillingDateStart(), TemporalType.DATE);
		query.setParameter("endDate", billingProcessedFilter.getBillingDateEnd(), TemporalType.DATE);

		if(Validations.validateIsNotNullAndPositive(entityCollection)){
			query.setParameter(GeneralConstants.FIELD_ENTITY_COLLECTION,entityCollection);
		}
		if(Validations.validateIsNotNullAndPositive(collectionPeriod)){
			//query.setParameter(GeneralConstants.FIELD_COLLECTION_PERIOD,billingProcessedFilter.getCollectionPeriod());
			if(BillingPeriodType.DAILY.getCode().equals(collectionPeriod)){
				
			}else if(BillingPeriodType.WEEKLY.getCode().equals(collectionPeriod)){
				if(endMonth){
					//query.setParameter("dayOfWeek", billingProcessedFilter.getDayOfWeek());
				}else{
					//query.setParameter("dayOfWeek", BillingUtils.getDayOfTheWeek(billingProcessedFilter.getBillingDateEnd()).toString());
				}
				
			}else if(BillingPeriodType.MONTHLY.getCode().equals(collectionPeriod)){
				if(!endMonth){
					//Los Schedule programados con el indicador de Fin de mes NO tienen el dia del mes
					//query.setParameter("dayOfMonth", BillingUtils.getDayToDate(billingProcessedFilter.getBillingDateEnd()).toString());
				}else{
					//query.setParameter("indEndOfMonth", BooleanType.YES.getCode());
				}
					
				
				
			}else if(BillingPeriodType.QUARTERLY.getCode().equals(collectionPeriod)){
				Integer seedMonth=BillingUtils.getMonthToDate(billingProcessedFilter.getBillingDateEnd());
				
				//query.setParameter("dayOfMonth", BillingUtils.getDayToDate(billingProcessedFilter.getBillingDateEnd()).toString());
				//query.setParameter("listMonthAvailable", seedMonth.toString());
				
			}else if(BillingPeriodType.YEARLY.getCode().equals(collectionPeriod)){
				
				//query.setParameter("dayOfMonth", BillingUtils.getDayToDate(billingProcessedFilter.getBillingDateEnd()).toString());
				//query.setParameter("monthOfYear", BillingUtils.getMonthToDate(billingProcessedFilter.getBillingDateEnd()).toString());
			}
		}
		
		
		
		if(Validations.validateIsNotNullAndPositive(billingProcessedFilter.getBillingServicePk())){
			query.setParameter("idBillingServicePk", billingProcessedFilter.getBillingServicePk());
		}
		
		if(Validations.validateIsNotNull(billingProcessedFilter.getIndIntegratedBill())
				&& (billingProcessedFilter.getIndIntegratedBill()>=0)){
			query.setParameter("inIntegratedBill",billingProcessedFilter.getIndIntegratedBill());
		}
		
		List<ProcessedService> listProcessedService = query.getResultList();
		
		return listProcessedService;
	}
	
	/**
	 * Gets the processed service for calculate billing.
	 *
	 * @param billingProcessedFilter the billing processed filter
	 * @return the processed service for calculate billing
	 */
	public List<ProcessedService> getProcessedServiceForCalculateBilling(BillingProcessedFilter billingProcessedFilter ){
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select ps From ProcessedService ps  join fetch ps.billingService bs ");
		sbQuery.append(" Where ps.processedState= :processedState ");/** default: PROCESADO**/
		sbQuery.append(" AND ps.billingService.entityCollection = :entityCollection");
		sbQuery.append(" AND ps.billingService.collectionPeriod = :collectionPeriod");	
		
		/**
		 * The same Date for   COLLECTION_PERIOD_DAILY_PK and COLLECTION_PERIOD_MONTHLY_PK
		 * 
		 * */
		sbQuery.append(" AND trunc(ps.calculationDate) between trunc(:startDate) and trunc(:endDate) ");
	
		if(Validations.validateIsNotNullAndPositive(billingProcessedFilter.getBillingServicePk())){
			sbQuery.append(" AND ps.billingService.idBillingServicePk = :idBillingServicePk");
		}
		

		if(Validations.validateIsNotNull(billingProcessedFilter.getIndIntegratedBill())
				&& (billingProcessedFilter.getIndIntegratedBill()>=0)){
			sbQuery.append(" AND ps.billingService.inIntegratedBill = :inIntegratedBill");
		}
					
		sbQuery.append(" ORDER BY bs.inIntegratedBill,bs.currencyBilling, bs.idBillingServicePk  ");
		
		TypedQuery<ProcessedService> query =  em.createQuery(sbQuery.toString(),ProcessedService.class); 
		
		query.setParameter("processedState", ProcessedServiceType.PROCESADO.getCode());
		query.setParameter(GeneralConstants.FIELD_ENTITY_COLLECTION,billingProcessedFilter.getEntityCollection());
		query.setParameter(GeneralConstants.FIELD_COLLECTION_PERIOD,billingProcessedFilter.getCollectionPeriod());
		
		query.setParameter("startDate", billingProcessedFilter.getBillingDateStart(),TemporalType.DATE);
		query.setParameter("endDate", billingProcessedFilter.getBillingDateEnd(),TemporalType.DATE);
		
		
		
		if(Validations.validateIsNotNullAndPositive(billingProcessedFilter.getBillingServicePk())){
			query.setParameter("idBillingServicePk",billingProcessedFilter.getBillingServicePk());
		}
		
		if(Validations.validateIsNotNullAndPositive(billingProcessedFilter.getIndIntegratedBill())){
			query.setParameter("inIntegratedBill",billingProcessedFilter.getIndIntegratedBill());
		}
		
		List<ProcessedService> ListProcessedService = query.getResultList();
		
		return ListProcessedService;
	}
	
	
	/**
	 * Gets the billing calculation process for process preliminary.
	 *
	 * @param billingProcessedFilter the billing processed filter
	 * @return the billing calculation process for process preliminary
	 */
	public List<BillingCalculationProcess> getBillingCalculationProcessForProcessPreliminary(BillingProcessedFilter billingProcessedFilter ){
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select bcp From BillingCalculationProcess bcp   ");
		sbQuery.append(" Where bcp.processedState= :processedState ");/** default: PRELIMINARY**/
		sbQuery.append(" AND bcp.billingPeriod = :billingPeriod");
		sbQuery.append(" AND bcp.entityCollection = :entityCollection");	
		
		if ( billingProcessedFilter.getCollectionPeriod().equals(PropertiesConstants.COLLECTION_PERIOD_DAILY_PK)
//				|| billingProcessedFilter.getCollectionPeriod().equals(PropertiesConstants.COLLECTION_PERIOD_WEEKLY_PK)	
				){
			sbQuery.append(" AND bcp.billingDate = :billingDate");
		}else if (billingProcessedFilter.getCollectionPeriod().equals(PropertiesConstants.COLLECTION_PERIOD_MONTHLY_PK)){
			sbQuery.append(" AND bcp.billingYear = :billingYear AND bcp.billingMonth = :billingMonth ");
		}
		
		
		TypedQuery<BillingCalculationProcess> query =  em.createQuery(sbQuery.toString(),BillingCalculationProcess.class); 
		
		query.setParameter("processedState", BillingCalculationType.PRELIMINAR.getCode() );
		query.setParameter("billingPeriod",billingProcessedFilter.getCollectionPeriod());
		query.setParameter(GeneralConstants.FIELD_ENTITY_COLLECTION,billingProcessedFilter.getEntityCollection());
		
		if (billingProcessedFilter.getCollectionPeriod().equals(PropertiesConstants.COLLECTION_PERIOD_DAILY_PK)){
			query.setParameter("billingDate", billingProcessedFilter.getBillingDate(),TemporalType.DATE);
		}else if (billingProcessedFilter.getCollectionPeriod().equals(PropertiesConstants.COLLECTION_PERIOD_MONTHLY_PK)){
			query.setParameter("billingYear", billingProcessedFilter.getYearBilling());
			query.setParameter("billingMonth", billingProcessedFilter.getMonthBilling());
		}
			
		
		List<BillingCalculationProcess> listBillingCalculationProcess = query.getResultList();
		
		return listBillingCalculationProcess;
	}
	
	
	
	
	/**
	 * Save all billing processed service.
	 *
	 * @param billingCalculationProcess the billing calculation process
	 */
	public void saveAllBillingProcessedService(BillingCalculationProcess billingCalculationProcess){
		create(billingCalculationProcess);
	}
	
	
	/**
	 * Find billing calculation by billing calculation process.
	 *
	 * @param idBillingCalcProcessPk the id billing calc process pk
	 * @return the list
	 */
	public List<BillingCalculation>   findBillingCalculationByBillingCalculationProcess(Long idBillingCalcProcessPk){
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select bc From BillingCalculation bc   ");
		sbQuery.append(" Where	bc.billingCalculationProcess.idBillingCalcProcessPk = :idBillingCalcProcessPk ");
							
		
		TypedQuery<BillingCalculation> query =  em.createQuery(sbQuery.toString(),BillingCalculation.class); 
		query.setParameter("idBillingCalcProcessPk", idBillingCalcProcessPk);		
		List<BillingCalculation> billingCalculationList = query.getResultList();
		
		return billingCalculationList;
		
	}
	
	
	/**
	 * Delete billing calculation by id.
	 *
	 * @param idBillingCalculationPk the id billing calculation pk
	 */
	public  void deleteBillingCalculationById(Long idBillingCalculationPk){
		this.delete(BillingCalculation.class, idBillingCalculationPk);
	}
	
	/**
	 * Gets the billing preliminary for processed.
	 *
	 * @param billingProcessedFilter the billing processed filter
	 * @return the billing preliminary for processed
	 */
	public List<BillingProcessedTo> getBillingPreliminaryForProcessed(BillingProcessedFilter billingProcessedFilter){
		StringBuilder sbQuery = new StringBuilder();
	    sbQuery.append("  SELECT  ");
	    sbQuery.append("  BCP.ID_BILLING_CALC_PROCESS_PK,  "); //0
	    sbQuery.append("  COUNT(BC.ID_BILLING_CALC_PROCESS_FK) AS COUNT_BILL,  "); //1
	    sbQuery.append("  BCP.OPERATION_DATE, "); //2
	    sbQuery.append("  BCP.BILLING_DATE, "); //3
	    sbQuery.append("  (CASE WHEN BCP.PROCESSED_BILLING_NUMBER IS NOT NULL THEN BCP.PROCESSED_BILLING_NUMBER ||'' "); 
	    sbQuery.append("  WHEN BCP.PROCESSED_BILLING_NUMBER IS NULL THEN ' ' END) AS PROCESSED_BILLING_NUMBER ,  ");//4
	    sbQuery.append(" ( select parameter_name from parameter_table where parameter_table_pk=BCP.PROCESSED_STATE) AS DESC_PROCESSED_STATE, ");//5
	    sbQuery.append(" ( select parameter_name from parameter_table where parameter_table_pk=BCP.ENTITY_COLLECTION) AS ENTITY_COLLECTION, ");//6
	    sbQuery.append(" ( select parameter_name from parameter_table where parameter_table_pk=BCP.BILLING_PERIOD) AS BILLING_PERIOD, ");//7
	    sbQuery.append(" BCP.PROCESSED_STATE ");//8
	    
	    sbQuery.append(" FROM  ");
	    sbQuery.append(" BILLING_CALCULATION_PROCESS BCP  ");
	    sbQuery.append(" left join  BILLING_CALCULATION BC  on BC.ID_BILLING_CALC_PROCESS_FK=BCP.ID_BILLING_CALC_PROCESS_PK ");
	    sbQuery.append(" where 1=1  ");
	    
	    if(Validations.validateIsNotNullAndPositive(billingProcessedFilter.getEntityCollection())){
	    	sbQuery.append(" and bcp.ENTITY_COLLECTION=:entityCollectionPrm  ");
	    }
	    if(Validations.validateIsNotNullAndPositive(billingProcessedFilter.getProcessBillingCalculationPk())){
	    	sbQuery.append(" and bcp.ID_BILLING_CALC_PROCESS_PK=:idBillingCalcProcessPk  ");
	    }
	    if(Validations.validateIsNotNullAndPositive(billingProcessedFilter.getCollectionPeriod())){
	    	sbQuery.append(" and bcp.BILLING_PERIOD=:collectionPeriodPrm  ");
	    	Integer collectionPeriod=billingProcessedFilter.getCollectionPeriod();
	    	if(collectionPeriod.equals(PropertiesConstants.COLLECTION_PERIOD_DAILY_PK)){
	    		sbQuery.append(" and BCP.BILLING_DATE=:billingDatePrm  ");
	    	}
	    	else if(collectionPeriod.equals(PropertiesConstants.COLLECTION_PERIOD_WEEKLY_PK)){
	    		sbQuery.append(" and BC.BILLING_DATE_START=:initialDatePrm  ");
	    		sbQuery.append(" and BC.BILLING_COURT_DATE=:finalDatePrm  ");
	    	}
	    	else if(collectionPeriod.equals(PropertiesConstants.COLLECTION_PERIOD_MONTHLY_PK)){
	    		sbQuery.append(" and BC.BILLING_DATE_START=:initialDatePrm  ");
	    		sbQuery.append(" and BC.BILLING_COURT_DATE=:finalDatePrm  ");
	    	}
	    	else if(collectionPeriod.equals(PropertiesConstants.COLLECTION_PERIOD_QUARTERLY_PK)){
	    		sbQuery.append(" and BC.BILLING_DATE_START=:initialDatePrm  ");
	    		sbQuery.append(" and BC.BILLING_COURT_DATE=:finalDatePrm  ");
	    	}
	    	else if(collectionPeriod.equals(PropertiesConstants.COLLECTION_PERIOD_YEARLY_PK)){
	    		sbQuery.append(" and BC.BILLING_DATE_START=:initialDatePrm  ");
	    		sbQuery.append(" and BC.BILLING_COURT_DATE=:finalDatePrm  ");
	    	}
	    }
	    
	    sbQuery.append(" GROUP BY  ");
	    sbQuery.append("  BCP.ID_BILLING_CALC_PROCESS_PK,  ");	   
	    sbQuery.append("  BCP.OPERATION_DATE,    ");
	    sbQuery.append("  BCP.BILLING_DATE, ");
	    sbQuery.append("  (CASE WHEN BCP.PROCESSED_BILLING_NUMBER IS NOT NULL THEN BCP.PROCESSED_BILLING_NUMBER ||''   ");
	    sbQuery.append("   WHEN BCP.PROCESSED_BILLING_NUMBER IS NULL THEN ' '");
	    sbQuery.append("   END) ,");
	    sbQuery.append("  BCP.PROCESSED_STATE , ");
	    sbQuery.append("  BCP.ENTITY_COLLECTION, ");
	    sbQuery.append("  BCP.BILLING_PERIOD , ");
	    sbQuery.append("  BCP.PROCESSED_STATE   ");
	    sbQuery.append("  order by  ");
	    sbQuery.append("  BCP.OPERATION_DATE desc,   ");
	    sbQuery.append("  BCP.ENTITY_COLLECTION ");
	    Query query = em.createNativeQuery(sbQuery.toString());
	    
	    if(Validations.validateIsNotNullAndPositive(billingProcessedFilter.getEntityCollection())){
	    	query.setParameter("entityCollectionPrm", billingProcessedFilter.getEntityCollection());
	    }
	    if(Validations.validateIsNotNullAndPositive(billingProcessedFilter.getProcessBillingCalculationPk())){
	    	query.setParameter("idBillingCalcProcessPk", billingProcessedFilter.getProcessBillingCalculationPk());
	    }
	    if(Validations.validateIsNotNullAndPositive(billingProcessedFilter.getCollectionPeriod())){
	    	query.setParameter("collectionPeriodPrm", billingProcessedFilter.getCollectionPeriod());
	    	Integer collectionPeriod=billingProcessedFilter.getCollectionPeriod();
	    	
	    	 DateFormat inputDF  = new SimpleDateFormat("mm/dd/yy");
 	        //Date date1 = inputDF.parse("9/30/11");

 	        Calendar cal = Calendar.getInstance();
 	        cal.setTime(billingProcessedFilter.getBillingDateStart());

 	        int month = cal.get(Calendar.MONTH);
 	        int day = cal.get(Calendar.DAY_OF_MONTH);
 	        int year = cal.get(Calendar.YEAR);

	    	if(collectionPeriod.equals(PropertiesConstants.COLLECTION_PERIOD_DAILY_PK)){
	    		query.setParameter("billingDatePrm", billingProcessedFilter.getBillingDateStart());
	    	}
	    	else if(collectionPeriod.equals(PropertiesConstants.COLLECTION_PERIOD_WEEKLY_PK)){
	    		query.setParameter("initialDatePrm",billingProcessedFilter.getBillingDateStart() );
	    		query.setParameter("finalDatePrm",billingProcessedFilter.getBillingDateEnd() );
	    	}
	    	else if(collectionPeriod.equals(PropertiesConstants.COLLECTION_PERIOD_MONTHLY_PK)){
	    		query.setParameter("initialDatePrm",billingProcessedFilter.getBillingDateStart() );
	    		query.setParameter("finalDatePrm",billingProcessedFilter.getBillingDateEnd() );
	    	}
	    	else if(collectionPeriod.equals(PropertiesConstants.COLLECTION_PERIOD_QUARTERLY_PK)){
	    		query.setParameter("initialDatePrm",billingProcessedFilter.getBillingDateStart() );
	    		query.setParameter("finalDatePrm",billingProcessedFilter.getBillingDateEnd() );
	    	}
	    	else if(collectionPeriod.equals(PropertiesConstants.COLLECTION_PERIOD_YEARLY_PK)){
	    		query.setParameter("initialDatePrm",billingProcessedFilter.getBillingDateStart() );
	    		query.setParameter("finalDatePrm",billingProcessedFilter.getBillingDateEnd() );
	    	}
	    }
	    
	    List<Object[]>  objectList = query.getResultList();
	    List<BillingProcessedTo> billingProcessedToList=new ArrayList<BillingProcessedTo>();
	    BillingProcessedTo billingProcessedTo;
	    for(int i=0;i<objectList.size();++i){
	    	 billingProcessedTo=new BillingProcessedTo();
	    	 Object[] sResults = (Object[])objectList.get(i);

	    	 billingProcessedTo.setIdBillingCalcProcessPk(sResults[0]==null?null:Long.parseLong(sResults[0].toString()));
	    	 billingProcessedTo.setCountBillingDetail(sResults[1]==null?0:Integer.parseInt(sResults[1].toString()));
	    	 billingProcessedTo.setOperationDate(sResults[2]==null?null:(Date) sResults[2]);
	    	 billingProcessedTo.setBillingDate(sResults[3]==null?null:(Date) sResults[3]);
	    	 
	    	 billingProcessedTo.setDescriptionProcessedState(sResults[5]==null?"":sResults[5].toString());
	    	 billingProcessedTo.setDescriptionEntityCollection(sResults[6]==null?"":sResults[6].toString());
	    	 billingProcessedTo.setDescriptionBillingPeriod(sResults[7]==null?"":sResults[7].toString());
	    	 billingProcessedTo.setProcessedState(sResults[8]==null?0:Integer.parseInt(sResults[8].toString()));
	    	 billingProcessedToList.add(billingProcessedTo);
	    }
	    
	    
		return billingProcessedToList;
	}

	
	/**
	 * Find billing calculation process. By Billing Logger
	 *
	 * @param billingProcessedFilter the billing processed filter
	 * @return the list
	 */
	public List<BillingCalculationProcess> findBillingCalculationProcess(BillingProcessedFilter billingProcessedFilter ){
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select new  BillingCalculationProcess(   ");
		sbQuery.append(" bcp.idBillingCalcProcessPk, ");
		sbQuery.append(" bcp.operationDate, bcp.billingDate,  ");
		sbQuery.append(" bcp.processedState, bcp.nameFile, ");
		sbQuery.append(" (select p.parameterName from ParameterTable p where p.parameterTablePk = bcp.billingPeriod) ,");
		sbQuery.append(" (select p.parameterName from ParameterTable p where p.parameterTablePk = bcp.entityCollection), ");
		sbQuery.append(" (select p.parameterName from ParameterTable p where p.parameterTablePk = bcp.processedState) )");
		sbQuery.append(" From BillingCalculationProcess bcp");
		sbQuery.append(" Where bcp.processedState= :processedState ");/** default: DEFINITIVO**/

		sbQuery.append(" AND trunc(bcp.billingDate) between trunc(:startDate) and trunc(:endDate) ");
	
		if(Validations.validateIsNotNullAndPositive(billingProcessedFilter.getBillingServicePk())){
			sbQuery.append(" AND bcp.idBillingCalcProcessPk = :idBillingCalcProcessPk");
		}
		
		sbQuery.append(" ORDER BY bcp.billingDate desc  ");
		
		TypedQuery<BillingCalculationProcess> query =  em.createQuery(sbQuery.toString(),BillingCalculationProcess.class);

		query.setParameter("processedState", BillingCalculationType.DEFINITIVO.getCode());
		
		query.setParameter("startDate", billingProcessedFilter.getBillingDateStart(),TemporalType.DATE);
		query.setParameter("endDate", billingProcessedFilter.getBillingDateEnd(),TemporalType.DATE);

		if(Validations.validateIsNotNullAndPositive(billingProcessedFilter.getBillingServicePk())){
			query.setParameter("idBillingCalcProcessPk", billingProcessedFilter.getBillingServicePk());
		}
		
		List<BillingCalculationProcess> listBillingCalculationProcess = query.getResultList();
		
		return listBillingCalculationProcess;
	}
	
	/**
	 * Get Concatenated: [REFERENCE_RATE - SERVICE_CODE]  Of Billing Service
	 * Since BillingCalculationProcess
	 * @param idBillingCalculationProcess
	 * @return
	 */
	public String getReferenceRateByProcessCalculation(Long idBillingCalculationProcess){
		String referenceRate=GeneralConstants.DASH;
		StringBuilder sbQuery = new StringBuilder();
	    sbQuery.append("  SELECT  ");
	    sbQuery.append("   LISTAGG(T.REFERENCE_RATE||'-' ||T.SERVICE_CODE,', ')   ");
	    sbQuery.append("     WITHIN GROUP (ORDER BY T.REFERENCE_RATE) AS TARIFA");
	    sbQuery.append("   FROM  ");
	    sbQuery.append("    ( ");
	    sbQuery.append("    SELECT DISTINCT  BCP.ID_BILLING_CALC_PROCESS_PK,BS.REFERENCE_RATE, BS.SERVICE_CODE FROM  ");
	    sbQuery.append("     BILLING_CALCULATION_PROCESS BCP  ");
	    sbQuery.append("    LEFT join  BILLING_CALCULATION BC  on BC.ID_BILLING_CALC_PROCESS_FK=BCP.ID_BILLING_CALC_PROCESS_PK  ");
	    sbQuery.append("    LEFT join billing_calculation_detail bcd on bcd.id_billing_calculation_fk=bc.id_billing_calculation_pk  ");
	    sbQuery.append("    LEFT join billing_service bs on bs.id_billing_service_pk=bcd.id_billing_service_fk   ");
	    sbQuery.append("     ");
	    sbQuery.append("     ) T");
	    sbQuery.append("    WHERE ID_BILLING_CALC_PROCESS_PK=:idBillingCalculationProcessPk ");
	    sbQuery.append("    GROUP BY ID_BILLING_CALC_PROCESS_PK ");
	    sbQuery.append("     ");
	    
	    
	    Query query = em.createNativeQuery(sbQuery.toString());
	    
	    query.setParameter("idBillingCalculationProcessPk", idBillingCalculationProcess);
	    try{
	    	referenceRate = (String) query.getSingleResult();
	    } catch(NoResultException  nrex){
	    	return referenceRate;
		}
	    
		return referenceRate;
		
	}
	
	/**
	 * Get Last Billing Date with Filters idBillingServicePk,
	 * entity Collection, billing Period
	 * @return Date Last Billing Definitive
	 */
	public Date getLastBillingDate(BillingProcessedFilter billingProcessedFilter){

		List<Object> listObject = null;
		Integer entityCollection = billingProcessedFilter.getEntityCollection();
		Integer collectionPeriod = billingProcessedFilter.getCollectionPeriod();
		StringBuilder sbQuery = new StringBuilder();
	    sbQuery.append("  SELECT  ");
	    sbQuery.append("   	BCP.BILLING_DATE    ");
	    sbQuery.append("     ");
	    sbQuery.append("   FROM BILLING_CALCULATION_PROCESS BCP");
	    sbQuery.append("   INNER JOIN BILLING_CALCULATION BC ON BC.id_billing_calc_process_Fk=BCP.id_billing_calc_process_pk");
	    sbQuery.append("   INNER JOIN BILLING_CALCULATION_DETAIL BCD ON BCD.id_billing_calculation_fk=BC.id_billing_calculation_Pk");
	    sbQuery.append("   INNER JOIN BILLING_SERVICE BS ON BCD.id_billing_service_fk=BS.id_billing_service_Pk");
	    sbQuery.append("   ");
	    sbQuery.append("   WHERE ");
	    sbQuery.append("   BS.ENTITY_COLLECTION= :entityCollection ");
	    sbQuery.append("   AND BCP.BILLING_PERIOD= :collectionPeriod ");
	    if(Validations.validateIsNotNullAndPositive(billingProcessedFilter.getBillingServicePk())) {
	    	sbQuery.append("   AND BS.ID_BILLING_SERVICE_PK = :idBillingServicePk ");
	    }
	    if(Validations.validateIsNotNullAndPositive(billingProcessedFilter.getProcessedState())){
	    	sbQuery.append("   AND BCP.PROCESSED_STATE= :stateBilling ");//1644	DEFINITIVO
	    }
	    
	    
		if(EntityCollectionType.PARTICIPANTS.getCode().equals(entityCollection)&&
	    		Validations.validateIsNotNullAndPositive(billingProcessedFilter.getParticipantPk())){
			sbQuery.append(" AND BC.ID_PARTICIPANT_FK= :idParticipantPk  ");

		}else if(EntityCollectionType.HOLDERS.getCode().equals(entityCollection)&&
				Validations.validateIsNotNullAndPositive(billingProcessedFilter.getHolderPk())){
			sbQuery.append("  AND BC.ID_HOLDER_FK= :idHolderPk  ");

		}else if(EntityCollectionType.ISSUERS.getCode().equals(entityCollection)&&
				Validations.validateIsNotNullAndNotEmpty(billingProcessedFilter.getIssuerPk())){
			sbQuery.append("  AND BC.ID_ISSUER_FK= :idIssuerPk  ");

		}else if(EntityCollectionType.OTHERS.getCode().equals(entityCollection)&&
				Validations.validateIsNotNullAndPositive(billingProcessedFilter.getDocumentTypeOther())&&
				Validations.validateIsNotNullAndNotEmpty(billingProcessedFilter.getDocumentCodeOther())){
			sbQuery.append("  AND BC.INDENTIFY_TYPE= :indentifyType ");
			sbQuery.append("  AND BC.ENTITY_CODE= :entityCode  ");

		}

	    sbQuery.append("   ");
	    sbQuery.append("   ORDER BY BCP.BILLING_DATE DESC ");
	    sbQuery.append("   ");
	    
	    
	    Query query = em.createNativeQuery(sbQuery.toString());
	    
	    query.setParameter("entityCollection", entityCollection);
	    query.setParameter("collectionPeriod", collectionPeriod);
	    if(Validations.validateIsNotNullAndPositive(billingProcessedFilter.getBillingServicePk())) {
	    	query.setParameter("idBillingServicePk", billingProcessedFilter.getBillingServicePk());
	    }
	    if(Validations.validateIsNotNullAndPositive(billingProcessedFilter.getProcessedState())){
	    	query.setParameter("stateBilling", billingProcessedFilter.getProcessedState());
	    }
	    
	    
	    if(EntityCollectionType.PARTICIPANTS.getCode().equals(entityCollection)&&
	    		Validations.validateIsNotNullAndPositive(billingProcessedFilter.getParticipantPk())){
	    	query.setParameter("idParticipantPk", billingProcessedFilter.getParticipantPk());

		}else if(EntityCollectionType.HOLDERS.getCode().equals(entityCollection)&&
				Validations.validateIsNotNullAndPositive(billingProcessedFilter.getHolderPk())){
			query.setParameter("idHolderPk", billingProcessedFilter.getHolderPk());

		}else if(EntityCollectionType.ISSUERS.getCode().equals(entityCollection)&&
				Validations.validateIsNotNullAndNotEmpty(billingProcessedFilter.getIssuerPk())){
			query.setParameter("idIssuerPk", billingProcessedFilter.getIssuerPk());

		}else if(EntityCollectionType.OTHERS.getCode().equals(entityCollection)&&
				Validations.validateIsNotNullAndPositive(billingProcessedFilter.getDocumentTypeOther())&&
				Validations.validateIsNotNullAndNotEmpty(billingProcessedFilter.getDocumentCodeOther())){
			query.setParameter("indentifyType", billingProcessedFilter.getDocumentTypeOther());
			query.setParameter("entityCode", billingProcessedFilter.getDocumentCodeOther());

		}
	    
	    try{
	    	listObject=query.getResultList();
	    	if(Validations.validateListIsNotNullAndNotEmpty(listObject)){
	    		return (Date)listObject.get(0);
	    	}
	    } catch(NoResultException  nrex){
	    	return null;
		}
	    
		return null;
	}
	/**
	 * Get Last Billing Date with Filters idBillingServicePk,
	 * entity Collection, billing Period
	 * @return Date Last Billing Definitive
	 */
	public Date getLastBillingFinalDate(BillingProcessedFilter billingProcessedFilter){

		List<Object> listObject = null;
		Integer entityCollection = billingProcessedFilter.getEntityCollection();
		Integer collectionPeriod = billingProcessedFilter.getCollectionPeriod();
		StringBuilder sbQuery = new StringBuilder();
	    sbQuery.append("  SELECT  ");
	    sbQuery.append("   	BC.BILLING_COURT_DATE    ");
	    sbQuery.append("     ");
	    sbQuery.append("   FROM BILLING_CALCULATION_PROCESS BCP");
	    sbQuery.append("   INNER JOIN BILLING_CALCULATION BC ON BC.id_billing_calc_process_Fk=BCP.id_billing_calc_process_pk");
	    sbQuery.append("   INNER JOIN BILLING_CALCULATION_DETAIL BCD ON BCD.id_billing_calculation_fk=BC.id_billing_calculation_Pk");
	    sbQuery.append("   INNER JOIN BILLING_SERVICE BS ON BCD.id_billing_service_fk=BS.id_billing_service_Pk");
	    sbQuery.append("   ");
	    sbQuery.append("   WHERE 1=1");
	    if(Validations.validateIsNotNullAndPositive(entityCollection)) {
	    	sbQuery.append("   AND BS.ENTITY_COLLECTION= :entityCollection ");
	    }	    
	    if(Validations.validateIsNotNullAndPositive(collectionPeriod)) {
	    	sbQuery.append("   AND BCP.BILLING_PERIOD= :collectionPeriod ");
	    }
	    
	    if(Validations.validateIsNotNullAndPositive(billingProcessedFilter.getBillingServicePk())) {
	    	sbQuery.append("   AND BS.ID_BILLING_SERVICE_PK = :idBillingServicePk ");
	    }
	    if(Validations.validateIsNotNullAndPositive(billingProcessedFilter.getProcessedState())){
	    	sbQuery.append("   AND BCP.PROCESSED_STATE= :stateBilling ");//1644	DEFINITIVO
	    }
	    
	    
		if(EntityCollectionType.PARTICIPANTS.getCode().equals(entityCollection)&&
	    		Validations.validateIsNotNullAndPositive(billingProcessedFilter.getParticipantPk())){
			sbQuery.append(" AND BC.ID_PARTICIPANT_FK= :idParticipantPk  ");

		}else if(EntityCollectionType.HOLDERS.getCode().equals(entityCollection)&&
				Validations.validateIsNotNullAndPositive(billingProcessedFilter.getHolderPk())){
			sbQuery.append("  AND BC.ID_HOLDER_FK= :idHolderPk  ");

		}else if(EntityCollectionType.ISSUERS.getCode().equals(entityCollection)&&
				Validations.validateIsNotNullAndNotEmpty(billingProcessedFilter.getIssuerPk())){
			sbQuery.append("  AND BC.ID_ISSUER_FK= :idIssuerPk  ");

		}else if(EntityCollectionType.OTHERS.getCode().equals(entityCollection)&&
				Validations.validateIsNotNullAndPositive(billingProcessedFilter.getDocumentTypeOther())&&
				Validations.validateIsNotNullAndNotEmpty(billingProcessedFilter.getDocumentCodeOther())){
			sbQuery.append("  AND BC.INDENTIFY_TYPE= :indentifyType ");
			sbQuery.append("  AND BC.ENTITY_CODE= :entityCode  ");

		}

	    sbQuery.append("   ");
	    sbQuery.append("   ORDER BY BCP.BILLING_DATE DESC ");
	    sbQuery.append("   ");
	    
	    
	    Query query = em.createNativeQuery(sbQuery.toString());

	    if(Validations.validateIsNotNullAndPositive(entityCollection)) {
	    	query.setParameter("entityCollection", entityCollection);
	    }
	    

	    if(Validations.validateIsNotNullAndPositive(collectionPeriod)) {
	    	query.setParameter("collectionPeriod", collectionPeriod);
	    }
	    
	    if(Validations.validateIsNotNullAndPositive(billingProcessedFilter.getBillingServicePk())) {
	    	query.setParameter("idBillingServicePk", billingProcessedFilter.getBillingServicePk());
	    }
	    if(Validations.validateIsNotNullAndPositive(billingProcessedFilter.getProcessedState())){
	    	query.setParameter("stateBilling", billingProcessedFilter.getProcessedState());
	    }
	    
	    
	    if(EntityCollectionType.PARTICIPANTS.getCode().equals(entityCollection)&&
	    		Validations.validateIsNotNullAndPositive(billingProcessedFilter.getParticipantPk())){
	    	query.setParameter("idParticipantPk", billingProcessedFilter.getParticipantPk());

		}else if(EntityCollectionType.HOLDERS.getCode().equals(entityCollection)&&
				Validations.validateIsNotNullAndPositive(billingProcessedFilter.getHolderPk())){
			query.setParameter("idHolderPk", billingProcessedFilter.getHolderPk());

		}else if(EntityCollectionType.ISSUERS.getCode().equals(entityCollection)&&
				Validations.validateIsNotNullAndNotEmpty(billingProcessedFilter.getIssuerPk())){
			query.setParameter("idIssuerPk", billingProcessedFilter.getIssuerPk());

		}else if(EntityCollectionType.OTHERS.getCode().equals(entityCollection)&&
				Validations.validateIsNotNullAndPositive(billingProcessedFilter.getDocumentTypeOther())&&
				Validations.validateIsNotNullAndNotEmpty(billingProcessedFilter.getDocumentCodeOther())){
			query.setParameter("indentifyType", billingProcessedFilter.getDocumentTypeOther());
			query.setParameter("entityCode", billingProcessedFilter.getDocumentCodeOther());

		}
	    
	    try{
	    	listObject=query.getResultList();
	    	if(Validations.validateListIsNotNullAndNotEmpty(listObject)){
	    		return (Date)listObject.get(0);
	    	}
	    } catch(NoResultException  nrex){
	    	return null;
		}
	    
		return null;
	}
	
	
	public byte[] getBillingProcessFileContent(Long idBillingCalcProcessPk) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idBillingCalcProcessPk", idBillingCalcProcessPk);
		return (byte[]) daoService.findObjectByNamedQuery(BillingCalculationProcess.BILLING_CALCULATION_PROCESS_CONTENT_BY_PK, parameters);
	}
	
	/**Buscando el servcio por el cual fue facturado*/
	public Long getIdBillingServiceRef(Long idBillingCalcProcessPk, Long idBillingServicePk){
		StringBuilder sbQuery = new StringBuilder();
	    sbQuery.append(" SELECT  DISTINCT  BS.ID_BILLING_SERVICE_PK ");
	    sbQuery.append(" FROM BILLING_CALCULATION_PROCESS BCP ");
	    sbQuery.append(" LEFT join  BILLING_CALCULATION BC  on BC.ID_BILLING_CALC_PROCESS_FK=BCP.ID_BILLING_CALC_PROCESS_PK ");
	    sbQuery.append(" LEFT join  billing_calculation_detail bcd on bcd.id_billing_calculation_fk=bc.id_billing_calculation_pk ");
	    sbQuery.append(" LEFT join  billing_service bs on bs.id_billing_service_pk=bcd.id_billing_service_fk ");
	    sbQuery.append(" WHERE 1 = 1 ");
	    sbQuery.append(" AND  BCP.ID_BILLING_CALC_PROCESS_PK = "+idBillingCalcProcessPk);
	    sbQuery.append(" AND  BS.ID_BILLING_SERVICE_PK = "+idBillingServicePk);
	    
	    try {
		    Query query = em.createNativeQuery(sbQuery.toString());
		    
		    BigDecimal id = (BigDecimal) query.getSingleResult();

		    return id.longValue();
	    } catch (NoResultException e) {
	    	return null;
	    }
	}
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Long getCorrelativeNumberInvoice(){
		final String SEQUENCE = "SQ_INVOICE_NUMBER_PK";
		return super.getNextCorrelativeOperations(SEQUENCE);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Long registreInterfaceTransaction(InterfaceProcess interfaceProcess, Integer transactionState, LoggerUser loggerUser){
		//return interfaceComponentServiceBean.get().saveInterfaceTransaction(operationInterfaceTO,transactionState, loggerUser);
		InterfaceTransaction interfaceTransaction = new InterfaceTransaction();
		Participant participant = em.find(Participant.class, 133L);
		interfaceTransaction.setParticipant(participant);
		interfaceTransaction.setTransactionState(transactionState);
		em.persist(interfaceTransaction);
		em.flush();
		return interfaceTransaction.getIdInterfaceTransactionPk();
	}
	
	/**Contrutendo el item equibalente al SAP*/
	public String findTermServiceCode(Long idBillingCalcProcessPk){
		StringBuilder sbQuery = new StringBuilder();
	    sbQuery.append(" SELECT  DISTINCT  BS.SERVICE_CODE ");
	    sbQuery.append(" FROM BILLING_CALCULATION_PROCESS BCP ");
	    sbQuery.append(" LEFT join  BILLING_CALCULATION BC  on BC.ID_BILLING_CALC_PROCESS_FK=BCP.ID_BILLING_CALC_PROCESS_PK ");
	    sbQuery.append(" LEFT join  billing_calculation_detail bcd on bcd.id_billing_calculation_fk=bc.id_billing_calculation_pk ");
	    sbQuery.append(" LEFT join  billing_service bs on bs.id_billing_service_pk=bcd.id_billing_service_fk ");
	    sbQuery.append(" WHERE 1 = 1 ");
	    sbQuery.append(" AND  BCP.ID_BILLING_CALC_PROCESS_PK = "+idBillingCalcProcessPk);
	    
	    Query query = em.createNativeQuery(sbQuery.toString());
	    
	    String serviceCode =  (String) query.getSingleResult();
	    if(serviceCode.length()==1)
	    	serviceCode = "T000"+serviceCode;
	    if(serviceCode.length()==2)
	    	serviceCode = "T00"+serviceCode;
	    return serviceCode;
	}
	//TODO*******************
	public List<SendInterfaceXmlSapTO> searchSentInvoices(ParameterInvoiceSearchTO invoiceSearchTO){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT SIX.ID_SEND_INTERFACE_XML_SAP_PK, ");
		sbQuery.append(" SIX.TOTAL_RECORDS, SIX.ACCEPTED_RECORDS, ");
		sbQuery.append(" SIX.REJECT_RECORDS, SIX.REGISTRY_DATE, ");
		sbQuery.append(" SIX.RESPONSE_VALUE, (select DESCRIPTION from parameter_table  where  PARAMETER_TABLE_PK = SIX.STATE) as STATE, ");
		sbQuery.append(" SIX.ID_BILLING_CALC_PROCESS_FK, ");
		sbQuery.append(" bcp.BILLING_DATE, ");
		sbQuery.append(" (select DESCRIPTION from parameter_table  where  PARAMETER_TABLE_PK = bcp.ENTITY_COLLECTION) as ENTITY_COLLECTION, ");
		sbQuery.append(" (select DESCRIPTION from parameter_table  where  PARAMETER_TABLE_PK = bcp.BILLING_PERIOD) as BILLING_PERIOD , ");
		sbQuery.append(" IP.ID_INTERFACE_PROCESS_PK, ");
		sbQuery.append(" SIX.ID_TICKET, ");
		sbQuery.append(" SIX.NUMBER_RESEND, ");
		sbQuery.append(" SIX.INVOICE_LIST_STRING ");
		sbQuery.append(" FROM SEND_INTERFACE_XML_SAP SIX ");
		sbQuery.append(" INNER JOIN INTERFACE_TRANSACTION IT ON SIX.ID_INTERFACE_TRANSACTION_FK = IT.ID_INTERFACE_TRANSACTION_PK ");
		sbQuery.append(" INNER JOIN INTERFACE_PROCESS IP ON IT.ID_INTERFACE_PROCESS_FK = IP.ID_INTERFACE_PROCESS_PK ");
		sbQuery.append(" INNER JOIN BILLING_CALCULATION_PROCESS BCP ON BCP.ID_BILLING_CALC_PROCESS_PK = SIX.ID_BILLING_CALC_PROCESS_FK ");
		sbQuery.append(" WHERE 1 = 1 ");
		sbQuery.append(" AND SIX.ID_BILLING_CALC_PROCESS_FK IS NOT NULL ");
		sbQuery.append(" AND IP.ID_EXTERNAL_INTERFACE_FK = 1060 ");
		if(invoiceSearchTO.getBillingDate()!=null)
			sbQuery.append(" AND TRUNC(BCP.BILLING_DATE) = TO_DATE('"+CommonsUtilities.convertDateToString(invoiceSearchTO.getRegistreInitDate(), "dd/MM/yyyy")+"','dd/MM/yyyy')");
		sbQuery.append(" AND ( SIX.REGISTRY_DATE BETWEEN  TO_DATE('"+CommonsUtilities.convertDateToString(invoiceSearchTO.getRegistreInitDate(), "dd/MM/yyyy")+"','dd/MM/yyyy')");
		sbQuery.append(" AND TO_DATE('"+CommonsUtilities.convertDateToString(invoiceSearchTO.getRegistreFinalDate(), "dd/MM/yyyy")+"','dd/MM/yyyy'))");
		sbQuery.append(" ORDER BY SIX.ID_SEND_INTERFACE_XML_SAP_PK DESC ");
		
		 
		Query query = em.createNativeQuery(sbQuery.toString());
		List<Object[]>  objectList = query.getResultList();
		
		SendInterfaceXmlSapTO interfaceXmlSapTO = null;
		
		List<SendInterfaceXmlSapTO> list  = new ArrayList<>();
		for (Object[] objects : objectList) {
			interfaceXmlSapTO = new SendInterfaceXmlSapTO();
			interfaceXmlSapTO.setIdSendInterfaceXmlSapPk(new BigDecimal(objects[0].toString()).longValue());
			interfaceXmlSapTO.setNumberAccepted(new BigDecimal(objects[2].toString()).intValue());
			interfaceXmlSapTO.setNumberRejected(new BigDecimal(objects[3].toString()).intValue());
			interfaceXmlSapTO.setNumberTotal(interfaceXmlSapTO.getNumberAccepted()+interfaceXmlSapTO.getNumberRejected());
			interfaceXmlSapTO.setRegistreDate(CommonsUtilities.convertObjectToDate(objects[4]));
			interfaceXmlSapTO.setResponseValue(objects[5]!=null?objects[5].toString():"Sin observaciones");
			//interfaceXmlSapTO.setState(new BigDecimal(objects[6].toString()).intValue());
			if(objects[6]!=null)
				interfaceXmlSapTO.setDescStatus(objects[6].toString());
			interfaceXmlSapTO.setIdBillingCalcProcessFk(new BigDecimal(objects[7].toString()).longValue());
			interfaceXmlSapTO.setServiceBillingCode(getReferenceRateByProcessCalculation(interfaceXmlSapTO.getIdBillingCalcProcessFk()));
			interfaceXmlSapTO.setBillingDate(CommonsUtilities.convertObjectToDate(objects[8]));
			interfaceXmlSapTO.setDescCollectionIntity(objects[9].toString());
			interfaceXmlSapTO.setDescCollectionPeriod(objects[10].toString());
			Long id = new BigDecimal(objects[11].toString()).longValue();
			interfaceXmlSapTO.setIdTicketSap(objects[12]!=null?new BigDecimal(objects[12].toString()).intValue():0 );
			interfaceXmlSapTO.setNumberReSend(objects[13]!=null?new BigDecimal(objects[13].toString()).intValue():0 );
			
			String string = objects[14]!=null?objects[14].toString():"";
			interfaceXmlSapTO.setInvoiceListAll(string);
			if(string.length()>20)
				string = string.substring(0,20)+"...";
			interfaceXmlSapTO.setInvoiceList(string);
			InterfaceProcess interfaceProcess = this.find(InterfaceProcess.class, id);
			interfaceXmlSapTO.setXmlSoapSendMessage(interfaceProcess.getFileUpload());
			interfaceXmlSapTO.setXmlSoapRecepMessage(interfaceProcess.getFileResponse());
			list.add(interfaceXmlSapTO);
		}
		return list;
	}
	public List<BillingCalculationProcess> getBillingCalculationSap(BillingProcessedFilter billingProcessedFilter){

		Integer entityCollection = billingProcessedFilter.getEntityCollection();
		Integer collectionPeriod = billingProcessedFilter.getCollectionPeriod();
		StringBuilder sbQuery = new StringBuilder();
	    sbQuery.append("  SELECT  ");
	    sbQuery.append("   DISTINCT	BCP.ID_BILLING_CALC_PROCESS_PK    ");
	    sbQuery.append("   FROM BILLING_CALCULATION_PROCESS BCP");
	    sbQuery.append("   INNER JOIN BILLING_CALCULATION BC ON BC.id_billing_calc_process_Fk=BCP.id_billing_calc_process_pk");
	    sbQuery.append("   INNER JOIN BILLING_CALCULATION_DETAIL BCD ON BCD.id_billing_calculation_fk=BC.id_billing_calculation_Pk");
	    sbQuery.append("   INNER JOIN BILLING_SERVICE BS ON BCD.id_billing_service_fk=BS.id_billing_service_Pk");
	    sbQuery.append("   ");
	    sbQuery.append("   WHERE 1 = 1");
	    
	    if(billingProcessedFilter.getBillingServicePk()!=null)
	    	sbQuery.append(" AND  BS.ID_BILLING_SERVICE_PK = :idBillingServicePk ");
	    
	    if(billingProcessedFilter.getEntityCollection()!=null)
	    	sbQuery.append("  AND BS.ENTITY_COLLECTION= :entityCollection ");
	    
	    if(billingProcessedFilter.getCollectionPeriod()!=null)
	    	sbQuery.append("  AND BCP.BILLING_PERIOD= :collectionPeriod ");
	    if(Validations.validateIsNotNullAndPositive(billingProcessedFilter.getProcessedState())){
	    	sbQuery.append("  AND BCP.PROCESSED_STATE= :stateBilling ");//1644	DEFINITIVO
	    }
	    
	    
		if(EntityCollectionType.PARTICIPANTS.getCode().equals(entityCollection)&&
	    		Validations.validateIsNotNullAndPositive(billingProcessedFilter.getParticipantPk())){
			sbQuery.append(" AND BC.ID_PARTICIPANT_FK= :idParticipantPk  ");

		}else if(EntityCollectionType.HOLDERS.getCode().equals(entityCollection)&&
				Validations.validateIsNotNullAndPositive(billingProcessedFilter.getHolderPk())){
			sbQuery.append("  AND BC.ID_HOLDER_FK= :idHolderPk  ");

		}else if(EntityCollectionType.ISSUERS.getCode().equals(entityCollection)&&
				Validations.validateIsNotNullAndNotEmpty(billingProcessedFilter.getIssuerPk())){
			sbQuery.append("  AND BC.ID_ISSUER_FK= :idIssuerPk  ");

		}else if(EntityCollectionType.OTHERS.getCode().equals(entityCollection)&&
				Validations.validateIsNotNullAndPositive(billingProcessedFilter.getDocumentTypeOther())&&
				Validations.validateIsNotNullAndNotEmpty(billingProcessedFilter.getDocumentCodeOther())){
			sbQuery.append("  AND BC.INDENTIFY_TYPE= :indentifyType ");
			sbQuery.append("  AND BC.ENTITY_CODE= :entityCode  ");

		}
		sbQuery.append(" AND ( TRUNC(BCP.BILLING_DATE) BETWEEN  TO_DATE('"+CommonsUtilities.convertDateToString(billingProcessedFilter.getBillingDateStart(), "dd/MM/yyyy")+"','dd/MM/yyyy')");
		sbQuery.append(" AND TO_DATE('"+CommonsUtilities.convertDateToString(billingProcessedFilter.getBillingDateEnd(), "dd/MM/yyyy")+"','dd/MM/yyyy'))");
	    sbQuery.append(" ORDER BY BCP.ID_BILLING_CALC_PROCESS_PK DESC ");
	    
	    
	    Query query = em.createNativeQuery(sbQuery.toString());
	    
	    if(billingProcessedFilter.getBillingServicePk()!=null)
	    	query.setParameter("idBillingServicePk", billingProcessedFilter.getBillingServicePk());
	    
	    if(billingProcessedFilter.getEntityCollection()!=null)
	    	query.setParameter("entityCollection", entityCollection);
	    
	    if(billingProcessedFilter.getCollectionPeriod()!=null)
	    	query.setParameter("collectionPeriod", collectionPeriod);
	    if(Validations.validateIsNotNullAndPositive(billingProcessedFilter.getProcessedState())){
	    	query.setParameter("stateBilling", billingProcessedFilter.getProcessedState());
	    }
	    
	    
	    if(EntityCollectionType.PARTICIPANTS.getCode().equals(entityCollection)&&
	    		Validations.validateIsNotNullAndPositive(billingProcessedFilter.getParticipantPk())){
	    	query.setParameter("idParticipantPk", billingProcessedFilter.getParticipantPk());

		}else if(EntityCollectionType.HOLDERS.getCode().equals(entityCollection)&&
				Validations.validateIsNotNullAndPositive(billingProcessedFilter.getHolderPk())){
			query.setParameter("idHolderPk", billingProcessedFilter.getHolderPk());

		}else if(EntityCollectionType.ISSUERS.getCode().equals(entityCollection)&&
				Validations.validateIsNotNullAndNotEmpty(billingProcessedFilter.getIssuerPk())){
			query.setParameter("idIssuerPk", billingProcessedFilter.getIssuerPk());

		}else if(EntityCollectionType.OTHERS.getCode().equals(entityCollection)&&
				Validations.validateIsNotNullAndPositive(billingProcessedFilter.getDocumentTypeOther())&&
				Validations.validateIsNotNullAndNotEmpty(billingProcessedFilter.getDocumentCodeOther())){
			query.setParameter("indentifyType", billingProcessedFilter.getDocumentTypeOther());
			query.setParameter("entityCode", billingProcessedFilter.getDocumentCodeOther());

		}
	    List<BillingCalculationProcess> list = new ArrayList<>();
	    try{
			@SuppressWarnings("unchecked")
			List<Object>  objectList = query.getResultList();
			Long primaryKey= null;
			for (Object object : objectList) {
				primaryKey  = new BigDecimal(object.toString()).longValue();
				if(checkRegistration(primaryKey,StateSapResponseType.CORRECT.getCode()))
					continue;
				BillingCalculationProcess billingCalculationProcess = em.find(BillingCalculationProcess.class, primaryKey);
				ParameterTable parameterTable = em.find(ParameterTable.class, billingCalculationProcess.getEntityCollection());
				billingCalculationProcess.setDescriptionEntityCollection(parameterTable.getDescription());
		    	
				parameterTable = em.find(ParameterTable.class, billingCalculationProcess.getBillingPeriod());
				billingCalculationProcess.setDescriptionBillingPeriod(parameterTable.getDescription()); 
				
				list.add(billingCalculationProcess);
			}
	    	
	    } catch(NoResultException  nrex){
	    	return null;
		}
		return list;
	}
	public boolean checkRegistration(Long idBillingCalcProcessPk, Integer state){
		StringBuilder sbQuery = new StringBuilder();
	    sbQuery.append(" SELECT ID_SEND_INTERFACE_XML_SAP_PK FROM SEND_INTERFACE_XML_SAP ");
	    sbQuery.append(" WHERE 1 = 1 ");
	    sbQuery.append(" AND ID_BILLING_CALC_PROCESS_FK = "+idBillingCalcProcessPk);
	    sbQuery.append(" AND STATE = "+state); 
	    
	    Query query = em.createNativeQuery(sbQuery.toString());
	    
	    try {
	    	 query.getSingleResult();
	    	 return true;
		} catch (Exception e) {
			// TODO: handle exception
		}
	   return false;
	}
	
	

	/**
	 * Gets the account receivable.
	 *
	 * @param processBillingCalculationPk the process billing calculation pk
	 * @return the account receivable
	 */
	public List<Object[]> getAccountReceivable(Long processBillingCalculationPk){
		
		StringBuilder sbQuery=new StringBuilder();
		 /**PK the entity*/
		sbQuery.append(" SELECT ");
		sbQuery.append("	BC.BILLING_NUMBER AS NUMCXC, ");
		sbQuery.append("	BS.ACCOUNTING_CODE AS CUENTA, ");
	    sbQuery.append("  	CASE   ");
	    sbQuery.append("   		WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN NVL(ISS.MNEMONIC , ' ' )    "); 
	    sbQuery.append("   		WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN  PAR.MNEMONIC || ' '   ");
	    sbQuery.append("   		WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN PART.MNEMONIC || ' '        ");
	    sbQuery.append("   		WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ')     ");
	    sbQuery.append("	END AS CLIENTE,  ");
	    sbQuery.append("	BS.SERVICE_NAME||' '||TO_CHAR(BC.BILLING_DATE_START,'DD-MM-YYYY') ");
	    sbQuery.append("	||' AL '||TO_CHAR(BC.BILLING_COURT_DATE,'DD-MM-YYYY') AS DESCRIPCION, ");
	    sbQuery.append("	'VFP' AS IMPUESTO, ");
	    sbQuery.append("	(SELECT PT.DESCRIPTION FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK=BC.CURRENCY_TYPE) AS MONEDA, ");
//	    sbQuery.append("	DECODE(BC.COUNT_REQUEST, 0 , 1 , BC.COUNT_REQUEST ) AS CANTIDAD, ");
	    sbQuery.append("	'1' AS CANTIDAD, ");
	    sbQuery.append("	ROUND(BC.GROSS_AMOUNT , 2) AS PRECIO, ");
	    sbQuery.append("	TO_CHAR(BC.BILLING_DATE,'YYYYMMDD') AS FECOBRO, ");
	    sbQuery.append("	BS.REFERENCE_RATE AS TIPOSERVICIO, ");
	    sbQuery.append("	BS.REFERENCE_RATE||'-1000' AS REF_AUX, ");//10
	    sbQuery.append("	BS.BASE_COLLECTION,  ");//11
	    sbQuery.append("	BC.ENTITY_CODE||' '||BC.ENTITY_DESCRIPTION,  ");//12
	    
	    sbQuery.append("  	CASE   ");
	    sbQuery.append("		WHEN BC.MNEMONIC_CONCATENATED IS NOT NULL THEN BC.MNEMONIC_CONCATENATED  ");//13
	    sbQuery.append("   		WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN NVL(ISS.MNEMONIC , ' ' )    "); 
	    sbQuery.append("   		WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN  PAR.MNEMONIC || ' '   ");
	    sbQuery.append("   		WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN PART.MNEMONIC || ' '        ");
	    sbQuery.append("   		WHEN BCP.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" THEN NVL(BC.ENTITY_CODE, ' ')     ");
	    sbQuery.append("	END AS MNEMONIC_CONCATENATED,  ");//13
	    sbQuery.append("  	BCD.ID_BILLING_SERVICE_FK,   ");//14
	    sbQuery.append("  	'T'||LPAD(BS.SERVICE_CODE,4,'0'),   ");//15
	    sbQuery.append("  	NVL(PART.MNEMONIC ,' ')||' - '||NVL(HOL.ID_HOLDER_PK,'')||' - '|| NVL(HOL.FULL_NAME,' ')||' - '|| NVL(HOL.FUND_TYPE,' ')   ");//16
	    
	    sbQuery.append(" FROM  ");
	    sbQuery.append(" 	BILLING_CALCULATION_PROCESS BCP  ");
	    sbQuery.append(" 	INNER JOIN BILLING_CALCULATION BC  on BC.ID_BILLING_CALC_PROCESS_FK=BCP.ID_BILLING_CALC_PROCESS_PK  ");
	    sbQuery.append(" 	INNER JOIN BILLING_CALCULATION_DETAIL BCD on BCD.ID_BILLING_CALCULATION_FK=BC.ID_BILLING_CALCULATION_PK  ");
	    sbQuery.append(" 	INNER JOIN BILLING_SERVICE BS ON BS.ID_BILLING_SERVICE_PK=BCD.ID_BILLING_SERVICE_FK  "); 
	    sbQuery.append(" 	LEFT JOIN ISSUER ISS ON(ISS.ID_ISSUER_PK=BC.ID_ISSUER_FK )   ");
	    sbQuery.append(" 	LEFT JOIN PARTICIPANT PAR ON (PAR.ID_PARTICIPANT_PK=BC.ID_PARTICIPANT_FK)   ");
	    sbQuery.append(" 	LEFT JOIN HOLDER HOL ON (HOL.ID_HOLDER_PK=BC.ID_HOLDER_FK)    ");
	    sbQuery.append(" 	LEFT JOIN PARTICIPANT PART ON (PART.DOCUMENT_NUMBER=HOL.DOCUMENT_NUMBER)    ");
	    
	    sbQuery.append(" WHERE 1=1  ");
	    
	    if(Validations.validateIsNotNullAndPositive(processBillingCalculationPk)){
	    	sbQuery.append(" AND BCP.ID_BILLING_CALC_PROCESS_PK=:billingCalcProcessPk  ");	
	    }
	    
	    Query query = em.createNativeQuery(sbQuery.toString());
	    
	    if(Validations.validateIsNotNullAndPositive(processBillingCalculationPk)){
	    	query.setParameter("billingCalcProcessPk", processBillingCalculationPk);	    	
	    }

	    return query.getResultList();
	}
}
