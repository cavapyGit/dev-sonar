package com.pradera.billing.billing.facade;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.pradera.billing.billing.service.BillingProcessedServiceBean;
import com.pradera.billing.billing.to.BillingProcessedFilter;
import com.pradera.billing.billing.to.BillingProcessedTo;
import com.pradera.billing.billing.to.InvoiceTO;
import com.pradera.billing.billing.to.ParameterInvoiceSearchTO;
import com.pradera.billing.billing.to.SendInterfaceXmlSapTO;
import com.pradera.billing.billing.to.XmlObjectSap;
import com.pradera.billing.billingservice.service.BillingServiceMgmtServiceBean;
import com.pradera.billing.common.utils.BillingUtils;
import com.pradera.billing.process.service.BillingCalculationService;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.services.remote.reports.ReportGenerationService;
import com.pradera.commons.sessionuser.ClientSoapService;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.core.framework.batchprocess.service.BatchServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.usersession.UserAcctions;
import com.pradera.model.auditprocess.type.StateSapResponseType;
import com.pradera.model.billing.BillingCalculation;
import com.pradera.model.billing.BillingCalculationDetail;
import com.pradera.model.billing.BillingCalculationProcess;
import com.pradera.model.billing.BillingEconomicActivity;
import com.pradera.model.billing.BillingSchedule;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.ProcessedService;
import com.pradera.model.billing.SendInterfaceXmlSap;
import com.pradera.model.billing.type.BaseCollectionType;
import com.pradera.model.billing.type.BillingCalculationType;
import com.pradera.model.billing.type.BillingPeriodType;
import com.pradera.model.billing.type.BillingServiceFilter;
import com.pradera.model.billing.type.BillingStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.externalinterface.ExtensionFileType;
import com.pradera.model.generalparameter.externalinterface.ExternalInterface;
import com.pradera.model.generalparameter.externalinterface.InterfaceProcess;
import com.pradera.model.generalparameter.externalinterface.InterfaceTransaction;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceExecutionType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.report.type.ReportFormatType;
import com.edv.org.tempuri.ServFacturaResponse;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;




// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingProcessedServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class BillingProcessedServiceFacade{

	/** The log. */
	@Inject
	PraderaLogger log;
    
    /** The billing processed service bean. */
    @EJB
    BillingProcessedServiceBean 		billingProcessedServiceBean;
    
    /** The billing calculation executor. */
    @EJB
	BillingCalculationService  			billingCalculationExecutor;
	
	/** The transaction registry. */
	@Resource
	TransactionSynchronizationRegistry 	transactionRegistry;

	/** The report generation service. */
	@Inject
    Instance<ReportGenerationService> 	reportGenerationService;

	/** The batch service bean. */
	@EJB 
	BatchServiceBean					batchServiceBean;

	/** The crud dao service bean. */
	@EJB
	CrudDaoServiceBean 					crudDaoServiceBean;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;

	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	@Inject
	private ClientSoapService clientSoapService;
	
	@Inject
	private ParameterServiceBean parameterServiceBean;

	

	/** The billing service mgmt service bean. */
	@EJB
	private BillingServiceMgmtServiceBean billingServiceMgmtServiceBean;
	
	//@Inject
	//private SoapClientInterfaceComponent clientInterfaceComponent;
	
	

	/** Parameter outPut Billing. */
	private static int INDEX_NUMCXC= 0;
	
	/** The index account. */
	private static int INDEX_ACCOUNT=1;
	
	/** The index client. */
	private static int INDEX_CLIENT=2;
	
	/** The index description. */
	private static int INDEX_DESCRIPTION=3;
	
	/** The index impuesto. */
	private static int INDEX_IMPUESTO=4;
	
	/** The index moneda. */
	private static int INDEX_MONEDA=5;
	
	/** The index cantidad. */
	private static int INDEX_CANTIDAD=6;
	
	/** The index precio. */
	private static int INDEX_PRECIO=7;
	
	/** The index fecobro. */
	private static int INDEX_FECOBRO=8;
	
	/** The index tiposervicio. */
	private static int INDEX_TIPOSERVICIO=9;
	
	/** The index ref aux. */
	private static int INDEX_REF_AUX=10;

	/** The index ref aux. */
	private static int INDEX_BASE_COLLECTION=11;
	
	/** The index ref aux. */
	private static int INDEX_ENTITY_DESCRIPTION=12;
	
	/** The  Index Mnemonic Holder*/
	private static int INDEX_MNEMONIC_HOLDER=13;
	
	/** The  Index Mnemonic Holder*/
	private static int INDEX_ID_BILLING_SERVICE_PK=14;
	
	/** The  Index Mnemonic Holder*/
	private static int INDEX_SERVICE_CODE=15;
	
	/**  The description AFP Tarifa 1*/
	private static int INDEX_DESCRIPTION_TARIFA_ONE_AFP=16;
	
	/**  The Constant DATE_PATTERN_BILLING *. */
	public static final String DATE_PATTERN_BILLING = "yyyyMMdd";
	
	/**
	 *  
	 *
	 * @param billingProcessedFilter the billing processed filter
	 * @return the processed service for calculate billing
	 */
	public List<ProcessedService> getProcessedServiceForCalculateBillingWithException(BillingProcessedFilter billingProcessedFilter ){
		List<ProcessedService> listProcessedService=null;
		/**
		 * Validaciones:
		 * 1.- Semanal con remanente de Fin de mes.
		 * 2.- Procesos con Indicador de Fin de mes.
		 */
		//Remanente del semanal
		if(isBillingRemainderWeekkly(billingProcessedFilter)){

			//Buscar los Schedule segun servicio, periodo de factura.
			BillingSchedule billingSchedule= billingProcessedServiceBean.getScheduleByBillingService(billingProcessedFilter);
			if(Validations.validateIsNotNull(billingSchedule)){
				billingProcessedFilter.setDayOfWeek(billingSchedule.getDayOfWeek());
			}
			listProcessedService=billingProcessedServiceBean.getProcessedServiceForCalculateBillingWithException(billingProcessedFilter,true);
			
		}else if(isBillingRemainderMonthly(billingProcessedFilter)){
			
			listProcessedService=billingProcessedServiceBean.getProcessedServiceForCalculateBillingWithException(billingProcessedFilter,true);
			
		}else{

			listProcessedService=billingProcessedServiceBean.getProcessedServiceForCalculateBillingWithException(billingProcessedFilter,false);
			
		}
		return listProcessedService;
	}
	

	/**
	 * If is Billing remainder Weekly 
	 * @param billingProcessedFilter
	 * @return
	 */
	public boolean isBillingRemainderWeekkly(BillingProcessedFilter billingProcessedFilter){
		
		Integer collectionPeriod=billingProcessedFilter.getCollectionPeriod();
		if(BillingPeriodType.WEEKLY.getCode().equals(collectionPeriod)){
			
			/**
			 *	Validacion, si el proceso es semanal y quedan menos de 6 dias para fin de mes,
			 *	Buscar las excepciones cuyo dia de Fi= Excp-1
			 *	Facturar segun rango de fecha
			 * 
			 */
			Date endMonth=BillingUtils.getLastDayOfMonth(billingProcessedFilter.getBillingDateStart());
			Integer daysBetweenEndMonth=CommonsUtilities.getDaysBetween(billingProcessedFilter.getBillingDateStart(), endMonth);
			Integer daysBetweenRange=CommonsUtilities.getDaysBetween(billingProcessedFilter.getBillingDateStart(), billingProcessedFilter.getBillingDateEnd());
	
			//Si quedan menos de 7 dias para acabar el mes
			if(daysBetweenEndMonth<GeneralConstants.SEVEN_VALUE_INTEGER  && 
					daysBetweenRange<GeneralConstants.SEVEN_VALUE_INTEGER){
				return true;
	
			}
				
		}
		return false;
	}
	
	/**
	 * If is Billing remainder Monthly 
	 * @param billingProcessedFilter
	 * @return
	 */
	public boolean isBillingRemainderMonthly(BillingProcessedFilter billingProcessedFilter){
		
		Integer collectionPeriod=billingProcessedFilter.getCollectionPeriod();
		if(BillingPeriodType.MONTHLY.getCode().equals(collectionPeriod)){
			Date endMonth=BillingUtils.getLastDayOfMonth(billingProcessedFilter.getBillingDateStart());
			Date firstDayMonth=BillingUtils.getFirstDayOfMonth(billingProcessedFilter.getBillingDateStart());
			
			if(CommonsUtilities.isEqualDate(endMonth, billingProcessedFilter.getBillingDateEnd())&&
					CommonsUtilities.isEqualDate(firstDayMonth, billingProcessedFilter.getBillingDateStart())
					){
				return true;
			}
			
			
			/**
			 * Buscar las excepciones mensuales marcadas como FIN DE MES
			 * Modificar la Fecha Inicial = Primer dia del mes.
			 * 				Fecha Final   = Ultimo dia del mes.
			 */

			
		}
		
		return false;
	}
	/**
	 *  
	 *
	 * @param billingProcessedFilter the billing processed filter
	 * @return the processed service for calculate billing
	 */
	public List<ProcessedService> getProcessedServiceForCalculateBilling(BillingProcessedFilter billingProcessedFilter ){
		return billingProcessedServiceBean.getProcessedServiceForCalculateBilling(billingProcessedFilter);
	}
	
	
	/**
	 * Gets the billing calculation process for process preliminary.
	 *
	 * @param billingProcessedFilter the billing processed filter
	 * @return the billing calculation process for process preliminary
	 */
	public List<BillingCalculationProcess> getBillingCalculationProcessForProcessPreliminary(BillingProcessedFilter billingProcessedFilter ){
		return billingProcessedServiceBean.getBillingCalculationProcessForProcessPreliminary(billingProcessedFilter);
	}
	
	
	
	/**
	 * Delete billing calculation to process preliminary.
	 *
	 * @param billingCalculationListNew the billing calculation list new
	 * @param billingProcessedFilter the billing processed filter
	 */
	public void deleteBillingCalculationToProcessPreliminary(List<BillingCalculation> 	billingCalculationListNew , BillingProcessedFilter billingProcessedFilter ){
		
		/** GETTING ALL LOTS BILLING CALCULATION ON DATA BASE :: TEMP ****/		
		List<BillingCalculationProcess>	billingCalculationTempList = billingProcessedServiceBean.getBillingCalculationProcessForProcessPreliminary(billingProcessedFilter);
		
		for (BillingCalculationProcess billingCalculationProcessTemp : billingCalculationTempList) {
			
			if (BillingCalculationType.PRELIMINAR.getCode().equals( billingCalculationProcessTemp.getProcessedState() )){
				
				/** GETTING  BILLING CALCULATION BY LOT  ON DATA BASE :: TEMP ****/	
				List<BillingCalculation> 	billingCalculationListTemp	= billingProcessedServiceBean.findBillingCalculationByBillingCalculationProcess(billingCalculationProcessTemp.getIdBillingCalcProcessPk());
				
				for (BillingCalculation billingCalculationTemp : billingCalculationListTemp) {
					
					for (BillingCalculation billingCalculationNew : billingCalculationListNew) {
						
						if (billingCalculationTemp.getKeyEntityCollection().equals(billingCalculationNew.getKeyEntityCollection())){
							billingProcessedServiceBean.deleteBillingCalculationById(billingCalculationTemp.getIdBillingCalculationPk());
							break;
						}
					}
				}
			}

		}
		
		
	}
	
	/**
	 * Execute preliminary process billing.
	 *
	 * @param billingProcessedFilter the billing processed filter
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	public void executePreliminaryProcessBilling(BillingProcessedFilter billingProcessedFilter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

		Map<String,Object> parameters = new HashMap<String, Object>();	

		/** GETTING PARAMETERS **/
		if (Validations.validateIsNotNull(billingProcessedFilter.getEntityCollection())) {
			parameters.put(GeneralConstants.FIELD_ENTITY_COLLECTION,
					billingProcessedFilter.getEntityCollection().toString());
		}

		if (Validations.validateIsNotNull(billingProcessedFilter.getDocumentCodeOther())) {
			parameters.put("documentCodeOther", billingProcessedFilter.getDocumentCodeOther());
		}

		if (Validations.validateIsNotNull(billingProcessedFilter.getDescriptionOther())) {
			parameters.put("descriptionOther", billingProcessedFilter.getDescriptionOther());
		}

		if (Validations.validateIsNotNull(billingProcessedFilter.getDocumentTypeOther())) {
			parameters.put("documentType", billingProcessedFilter.getDocumentTypeOther());
		}

		if (Validations.validateIsNotNull(billingProcessedFilter.getParticipantPk())) {
			parameters.put("participantPk", billingProcessedFilter.getParticipantPk().toString());
		}

		if (Validations.validateIsNotNull(billingProcessedFilter.getIssuerPk())) {
			parameters.put("issuerPk", billingProcessedFilter.getIssuerPk().toString());
		}

		if (Validations.validateIsNotNull(billingProcessedFilter.getHolderPk())) {
			parameters.put("holderPk", billingProcessedFilter.getHolderPk().toString());
		}

		if (Validations.validateIsNotNull(billingProcessedFilter.getProcessedState())) {
			parameters.put("processedState", billingProcessedFilter.getProcessedState().toString());
		}

		if (Validations.validateIsNotNull(billingProcessedFilter.getCollectionPeriod())) {
			parameters.put("collectionPeriod", billingProcessedFilter.getCollectionPeriod().toString());
		}

		if (Validations.validateIsNotNull(billingProcessedFilter.getBillingDateStart())) {
			parameters.put("startDate",
					CommonsUtilities.convertDatetoString(billingProcessedFilter.getBillingDateStart()));
		}

		if (Validations.validateIsNotNull(billingProcessedFilter.getBillingDateEnd())) {
			parameters.put("endDate", CommonsUtilities.convertDatetoString(billingProcessedFilter.getBillingDateEnd()));
		}

		if (Validations.validateIsNotNull(billingProcessedFilter.getBillingDate())) {
			parameters.put("billingDate",
					CommonsUtilities.convertDatetoString(billingProcessedFilter.getBillingDate()));
		}

		if (Validations.validateIsNotNull(billingProcessedFilter.getBillingServicePk())) {
			parameters.put("idBillingServicePk", billingProcessedFilter.getBillingServicePk().toString());
		}

		if (Validations.validateIsNotNull(billingProcessedFilter.getIndIntegratedBill())) {
			parameters.put("inIntegratedBill", billingProcessedFilter.getIndIntegratedBill().toString());
		}

		if (Validations.validateIsNotNull(billingProcessedFilter.getIdHolderByParticipant())) {
			parameters.put("idHolderByParticipant", billingProcessedFilter.getIdHolderByParticipant().toString());
		}
				
		Long codeProcess = BusinessProcessType.PRELIMINARY_PROCESS_BILLING.getCode();
		BusinessProcess process = crudDaoServiceBean.find(BusinessProcess.class,codeProcess);

		batchServiceBean.registerBatchTx(userInfo.getUserAccountSession().getUserName(), process, parameters);
	}

	/**
	 * Execute definitive process billing.
	 *
	 * @param billingProcessedFilter the billing processed filter
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	public void executeDefinitiveProcessBilling( BillingProcessedFilter billingProcessedFilter ) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

		Map<String,Object> parameters = new HashMap<String, Object>();	

		/**	GETTING PARAMETERS **/
		if (Validations.validateIsNotNull(billingProcessedFilter.getProcessBillingCalculationPk())){
			parameters.put(GeneralConstants.FIELD_CALCULATION_PROCESS, billingProcessedFilter.getProcessBillingCalculationPk());
		}
		
		if(Validations.validateIsNotNull(billingProcessedFilter.getBillingDateBecoming())){
			String dateBecoming=CommonsUtilities.convertDatetoString(billingProcessedFilter.getBillingDateBecoming());
			parameters.put(GeneralConstants.FIELD_DATE_BECOMING, dateBecoming);
		}
	
		Long codeProcess = BusinessProcessType.DEFINITIVE_PROCESS_BILLING.getCode();
		BusinessProcess process = crudDaoServiceBean.find(BusinessProcess.class,codeProcess);

		batchServiceBean.registerBatchTx(userInfo.getUserAccountSession().getUserName(), process, parameters);

	}
	

	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	public void executePreliminaryMonthlyProcessBilling(BillingProcessedFilter billingProcessedFilter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

		Map<String,Object> parameters = new HashMap<String, Object>();
		Long codeProcess = BusinessProcessType.PRELIMINARY_MONTH_PROCESS_BILLING.getCode();
		BusinessProcess process = crudDaoServiceBean.find(BusinessProcess.class,codeProcess);

		batchServiceBean.registerBatchTx(userInfo.getUserAccountSession().getUserName(), process, parameters);
	
		
	}


	/**
	 * Save all billing processed service.
	 *
	 * @param billingCalculationProcess the billing calculation process
	 */
	public void saveAllBillingProcessedService(BillingCalculationProcess billingCalculationProcess){

		List<BillingCalculation>   billingCalculations	=	billingCalculationProcess.getBillingCalculations();
		
		if (Validations.validateListIsNotNullAndNotEmpty(billingCalculations)){
			for (BillingCalculation billingCalculation : billingCalculations) {
				List<BillingCalculationDetail>	billingCalculationDetails	= billingCalculation.getBillingCalculationDetails();
				
				if (Validations.validateListIsNotNullAndNotEmpty(billingCalculationDetails)){
					
					for (BillingCalculationDetail billingCalculationDetail : billingCalculationDetails) {
						billingCalculationDetail.setBillingCalculation(billingCalculation);
					}
					billingCalculation.setBillingCalculationProcess(billingCalculationProcess);
				}
			}
		
		}	
		billingProcessedServiceBean.saveAllBillingProcessedService(billingCalculationProcess);
				
	}


	/**
	 * Sending report on billing.
	 *
	 * @param billingProcessedFilter the billing processed filter
	 * @param reportId the report id
	 * @throws ServiceException the service exception
	 */
	public void sendingReportOnBilling(BillingProcessedFilter billingProcessedFilter,Long reportId) throws ServiceException{
		/**LoggerUser*/
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		/**Parameter*/
		Map<String,String> parameters = new HashMap<String, String>();
		parameters.put("process_id", billingProcessedFilter.getProcessBillingCalculationPk().toString());
		/**ParameterDetails*/
		Map<String,String> parameterDetails = new HashMap<String, String>();
		/**ReportUser*/
		ReportUser reportUser = new ReportUser();
				
		reportUser.setUserName(loggerUser.getUserName());
		reportUser.setReportFormat(ReportFormatType.PDF.getCode());
		//how report is send for screen so for default send notification
		reportUser.setShowNotification(BooleanType.YES.getCode());
		
		reportGenerationService.get().saveReportExecution(reportId, parameters, parameterDetails, reportUser, loggerUser);
		
	}
	
	/**
	 * Save calculation process.
	 *
	 * @param billingCalculationProcess the billing calculation process
	 * @return the billing calculation process
	 * @throws ServiceException the service exception
	 */
	public BillingCalculationProcess saveCalculationProcess(BillingCalculationProcess billingCalculationProcess) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		return billingProcessedServiceBean.create(billingCalculationProcess);
	}

	/**
	 * Save all billing calculation.
	 *
	 * @param lstBillingCalulation the lst billing calulation
	 * @throws ServiceException the service exception
	 */
	public void saveAllBillingCalculation(List<BillingCalculation> lstBillingCalulation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		billingProcessedServiceBean.saveAll(lstBillingCalulation);
	}
	
	/**
	 * Gets the billing preliminary for processed.
	 *
	 * @param billingProcessedFilter the billing processed filter
	 * @return the billing preliminary for processed
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<BillingProcessedTo> getBillingPreliminaryForProcessed(BillingProcessedFilter billingProcessedFilter){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());	 		
	    List<BillingProcessedTo> listBillingProcessTo=billingProcessedServiceBean.getBillingPreliminaryForProcessed(billingProcessedFilter);
	    
	    /**
	     * Fill To Concatenated ReferenceRate- ServiceCode 
	     */
		for (BillingProcessedTo billingProcessedTo : listBillingProcessTo) {
			billingProcessedTo.setBillingServiceCode(billingProcessedServiceBean.getReferenceRateByProcessCalculation(billingProcessedTo.getIdBillingCalcProcessPk()));
		}
		
	    return listBillingProcessTo;
	}
	
	/**
	 * Find billing calculation process by id.
	 *
	 * @param idBillingCalcProcessPk the id billing calc process pk
	 * @return the billing calculation process
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public BillingCalculationProcess	findBillingCalculationProcessById( Long idBillingCalcProcessPk ){
		  LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		  loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return billingProcessedServiceBean.find(BillingCalculationProcess.class, idBillingCalcProcessPk);
	}
	
	/**
	 * Update billing calculation process.
	 *
	 * @param billingCalculationProcess the billing calculation process
	 * @throws ServiceException the service exception
	 */
	public void updateBillingCalculationProcess(BillingCalculationProcess billingCalculationProcess) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		billingProcessedServiceBean.update(billingCalculationProcess);
	}
	
	/**
	 * Update billing calculation.
	 *
	 * @param billingCalculation the billing calculation
	 * @throws ServiceException the service exception
	 */
	public void updateBillingCalculation(BillingCalculation billingCalculation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		billingProcessedServiceBean.update(billingCalculation);
	}
	
	/**
	 * Refresh state billing.
	 *
	 * @param billingProcessedFilter the billing processed filter
	 * @return the billing processed to
	 */
	public BillingProcessedTo refreshStateBilling(BillingProcessedFilter billingProcessedFilter){
		List<BillingProcessedTo> listBillingProcessedTo=
		
		billingProcessedServiceBean.getBillingPreliminaryForProcessed(billingProcessedFilter);
		BillingProcessedTo billingProcessedTo=null;
		if(Validations.validateIsNotNull(listBillingProcessedTo)){
			billingProcessedTo=listBillingProcessedTo.get(0);
		}else{
			
		}
		
		return	billingProcessedTo;
		
		
	}	
	
	/**
	 * Detele billing calculation process.
	 *
	 * @param billingCalculationProcess the billing calculation process
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public void deteleBillingCalculationProcess(BillingCalculationProcess billingCalculationProcess) {
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	     loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeDelete());
	     
		billingProcessedServiceBean.delete(BillingCalculationProcess.class, billingCalculationProcess.getIdBillingCalcProcessPk());
	}
	 
	/**
	 * Find billing calculation process.
	 *
	 * @param billingProcessedFilter the billing processed filter
	 * @return the list
	 */
	public List<BillingCalculationProcess> findBillingCalculationProcess(BillingProcessedFilter billingProcessedFilter){
		List<BillingCalculationProcess> listBillingCalculationProcess=billingProcessedServiceBean.findBillingCalculationProcess(billingProcessedFilter);
		
		
		for (BillingCalculationProcess billingCalculationProcess : listBillingCalculationProcess) {
			String referenceServiceCode=billingProcessedServiceBean.getReferenceRateByProcessCalculation(billingCalculationProcess.getIdBillingCalcProcessPk());
			billingCalculationProcess.setBillingServiceCode(referenceServiceCode);
		}
		
		return listBillingCalculationProcess;
	}
	/**Busqueda de facturas para SAP***/
	public List<BillingCalculationProcess> findBillingCalculationProcessSap(BillingProcessedFilter billingProcessedFilter,Long idBillingServicePk){
		List<BillingCalculationProcess> listBillingCalculationProcess=billingProcessedServiceBean.getBillingCalculationSap(billingProcessedFilter);
		Long idBillingServicePkRef = null;
		
		List<BillingCalculationProcess> listBillingCalculationProcessSap = new ArrayList<>();
		for (BillingCalculationProcess billingCalculationProcess : listBillingCalculationProcess) {
			String referenceServiceCode=billingProcessedServiceBean.getReferenceRateByProcessCalculation(billingCalculationProcess.getIdBillingCalcProcessPk());
			billingCalculationProcess.setBillingServiceCode(referenceServiceCode);//ocessBillingCalculationPk()
			if(Validations.validateIsNotNull(idBillingServicePkRef)) {
				idBillingServicePkRef = billingProcessedServiceBean.getIdBillingServiceRef(billingCalculationProcess.getIdBillingCalcProcessPk(), idBillingServicePk);
				if(Validations.validateIsNotNull(idBillingServicePkRef))
					listBillingCalculationProcessSap.add(billingCalculationProcess);
			} else {
				listBillingCalculationProcessSap.add(billingCalculationProcess);
			}
		}
		if(idBillingServicePk!=null)
			return listBillingCalculationProcessSap;
		else		
			return listBillingCalculationProcess;
	}
	
	/**
	 * Gets the billing calculation process file content.
	 *
	 * @param idBillCalcProcessPk the id bill calc process pk
	 * @return the billing calculation process file content
	 */
	public byte[] getBillingCalculationProcessFileContent(Long idBillCalcProcessPk) {
   	   Map<String, Object> parameters = new HashMap<String, Object>();
   	   parameters.put("idBillingCalcProcessPk", idBillCalcProcessPk);
   	   return (byte[])billingProcessedServiceBean.findObjectByNamedQuery(BillingCalculationProcess.BILLING_CALCULATION_PROCESS_CONTENT_BY_PK, parameters);
	}
	
	/**
	 * Find filter billing processed batch.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<BillingProcessedFilter> findFilterBillingProcessedBatch() throws ServiceException{
		List<BillingProcessedFilter> listbillingProcessedFilter=new ArrayList<BillingProcessedFilter>();
		List<ParameterTable> listCollectionPeriod;
		List<ParameterTable> listEntityCollection;
		BillingProcessedFilter billingProcessedFilter=null;
		
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		/*Fill Collection Period*/
		parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
		parameterTableTO.setOrderbyParameterTableCd(GeneralConstants.ONE_VALUE_INTEGER);
		parameterTableTO.setMasterTableFk(BillingServiceFilter.COLLECTION_PERIOD_PK.getCode());
		listCollectionPeriod= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		
		/*Fill Entity Collection*/
		parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
		parameterTableTO.setOrderbyParameterTableCd(GeneralConstants.ONE_VALUE_INTEGER);
		parameterTableTO.setMasterTableFk(BillingServiceFilter.ENTITY_COLLECTION_PK.getCode());
		listEntityCollection= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		
		
		Date initialDate=null; 
		Date finalDate=null;
		for (ParameterTable entityCollection: listEntityCollection) {
			
			for (ParameterTable collectionPeriod : listCollectionPeriod) {
				billingProcessedFilter=new BillingProcessedFilter();
				if(BillingPeriodType.DAILY.getCode().equals(collectionPeriod.getParameterTablePk())){
					initialDate=CommonsUtilities.currentDate(); 
					finalDate=CommonsUtilities.currentDate();
				}else if(BillingPeriodType.WEEKLY.getCode().equals(collectionPeriod.getParameterTablePk())){
					initialDate=CommonsUtilities.addDate(CommonsUtilities.currentDate(),-7); 
					finalDate=CommonsUtilities.currentDate();
				}else if(BillingPeriodType.MONTHLY.getCode().equals(collectionPeriod.getParameterTablePk())){
					finalDate=CommonsUtilities.currentDate();
					initialDate=BillingUtils.getDateInitialForPeriod(finalDate);
				}else if(BillingPeriodType.QUARTERLY.getCode().equals(collectionPeriod.getParameterTablePk())){
					initialDate=CommonsUtilities.addDate(CommonsUtilities.currentDate(),-90); 
					finalDate=CommonsUtilities.currentDate();
				}else if(BillingPeriodType.YEARLY.getCode().equals(collectionPeriod.getParameterTablePk())){
					initialDate=CommonsUtilities.addDate(CommonsUtilities.currentDate(),-360); 
					finalDate=CommonsUtilities.currentDate();
				}else if(BillingPeriodType.EVENT.getCode().equals(collectionPeriod.getParameterTablePk())){
					initialDate=CommonsUtilities.currentDate(); 
					finalDate=CommonsUtilities.currentDate();
				}else{
					initialDate=CommonsUtilities.currentDate(); 
					finalDate=CommonsUtilities.currentDate();
				}
				
				billingProcessedFilter.setBillingDateStart(initialDate);
				billingProcessedFilter.setBillingDateEnd(finalDate);
				billingProcessedFilter.setEntityCollection(entityCollection.getParameterTablePk());
				billingProcessedFilter.setCollectionPeriod(collectionPeriod.getParameterTablePk());
				listbillingProcessedFilter.add(billingProcessedFilter);
			}
		}
			
		return listbillingProcessedFilter;
	}
	
	/**
	 * Find filter billing processed batch.
	 * For End Month
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<BillingProcessedFilter> findFilterBillingEndMonthProcessedBatch(Date calculationDate) throws ServiceException{
		List<BillingProcessedFilter> listbillingProcessedFilter=new ArrayList<BillingProcessedFilter>();
		List<ParameterTable> listEntityCollection;
		BillingProcessedFilter billingProcessedFilter=null;
		
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		
		/*Fill Entity Collection*/
		parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
		parameterTableTO.setOrderbyParameterTableCd(GeneralConstants.ONE_VALUE_INTEGER);
		parameterTableTO.setMasterTableFk(BillingServiceFilter.ENTITY_COLLECTION_PK.getCode());
		listEntityCollection= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		
		
		Date initialDate=BillingUtils.getFirstDayOfMonth(calculationDate); 
		Date finalDate=calculationDate;
		for (ParameterTable entityCollection: listEntityCollection) {
				billingProcessedFilter=new BillingProcessedFilter();
				billingProcessedFilter.setBillingDateStart(initialDate);
				billingProcessedFilter.setBillingDateEnd(finalDate);
				billingProcessedFilter.setEntityCollection(entityCollection.getParameterTablePk());
				billingProcessedFilter.setCollectionPeriod(BillingPeriodType.MONTHLY.getCode());
				listbillingProcessedFilter.add(billingProcessedFilter);
			
		}
		
		
			 
			
		return listbillingProcessedFilter;
	}
	
	/**
	 * Find filter billing processed batch.
	 * For End Month
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<BillingProcessedFilter> findFilterBillingMonthlyProcessedBatch(Date calculationDate) throws ServiceException{
		List<BillingProcessedFilter> listbillingProcessedFilter=new ArrayList<BillingProcessedFilter>();
		BillingProcessedFilter billingProcessedFilter=new BillingProcessedFilter();
		
		
		
		
		
//867
		billingProcessedFilter.setCollectionPeriod(BillingPeriodType.MONTHLY.getCode());
		List<Object[]> billScheduleList=billingProcessedServiceBean.getScheduleMonthlyBilling(billingProcessedFilter);
		if(Validations.validateListIsNotNullAndNotEmpty(billScheduleList)) {
			billingProcessedFilter.setProcessedState(BillingStateType.DEFINITIVO.getCode());
			Date lastFinalMonthly=billingProcessedServiceBean.getLastBillingFinalDate(billingProcessedFilter);
			for (Object[] billSchedule : billScheduleList) {
				String dayOfMonth=billSchedule[0]==null?"1":billSchedule[0].toString();
				Long idBillingServicePk=billSchedule[1]==null?null:Long.parseLong(billSchedule[1].toString());
				Integer entityCollection=billSchedule[2]==null?0:Integer.parseInt(billSchedule[2].toString());
				
				String dateString=CommonsUtilities.convertDateToString(calculationDate, BillingUtils.DATE_PATTERN_yyyy_MM_dd);
				dateString=dateString.substring(0, 8).concat(dayOfMonth);
				Date finalDate=CommonsUtilities.convertStringtoDate(dateString, BillingUtils.DATE_PATTERN_yyyy_MM_dd);
				
				log.info(">>>DayOfMonth: "+dayOfMonth+ ", idBillingServicePk: "+idBillingServicePk +", entityCollection: "+entityCollection);
				System.out.println(">>>DayOfMonth: "+dayOfMonth+ ", idBillingServicePk: "+idBillingServicePk +", entidyCollection: "+entityCollection);
				
				//Obtener fecha de Inicio de la ultima factura procesada
				billingProcessedFilter.setBillingServicePk(idBillingServicePk);
				billingProcessedFilter.setEntityCollection(entityCollection);
				Date lastfinalDateBilling=billingProcessedServiceBean.getLastBillingFinalDate(billingProcessedFilter);
				log.info("lastfinalDateBilling "+lastfinalDateBilling);
				System.out.println("lastfinalDateBilling "+lastfinalDateBilling);
				
				//Validar si el servicio es nuevo, toma cualquiera
				if(lastfinalDateBilling==null) {
					lastfinalDateBilling=lastFinalMonthly;
					log.info("lastfinalDateBilling fue nulo, se toma la fecha: "+lastfinalDateBilling);
					System.out.println("lastfinalDateBilling fue nulo, se toma la fecha: "+lastfinalDateBilling);
				}
				
				Date initialDate=CommonsUtilities.addDaysHabilsToDate(lastfinalDateBilling, 1);
				
				BillingProcessedFilter billingProcessedSend=new BillingProcessedFilter();
				billingProcessedSend.setBillingDateStart(initialDate);
				billingProcessedSend.setBillingDateEnd(finalDate);
				billingProcessedSend.setBillingServicePk(idBillingServicePk);
				billingProcessedSend.setEntityCollection(entityCollection);
				billingProcessedSend.setCollectionPeriod(BillingPeriodType.MONTHLY.getCode());
				listbillingProcessedFilter.add(billingProcessedSend);
				System.out.println("lista: "+listbillingProcessedFilter.size());
			}
		}
		
		 
		
		
		
		
			 
			
		return listbillingProcessedFilter;
	}
	
	public Date getLastBillingDate(BillingProcessedFilter billingProcessedFilter){
		return billingProcessedServiceBean.getLastBillingDate(billingProcessedFilter);
	}
	
	public Date getLastBillingFinalDate(BillingProcessedFilter billingProcessedFilter){
		return billingProcessedServiceBean.getLastBillingFinalDate(billingProcessedFilter);
	}
	
	public List<InvoiceTO> getOBjectBilling(Long idBillingProcessPk) throws ParserConfigurationException, SAXException, IOException{
		byte[] xmlData = billingProcessedServiceBean.getBillingProcessFileContent(idBillingProcessPk);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    factory.setNamespaceAware(true);
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    Document documentXml =  (Document) builder.parse(new ByteArrayInputStream(xmlData));
	    
	    Element root = documentXml.getDocumentElement();
	    System.out.println("TAG RAIZ:"+root.getNodeName());
	    NodeList listSales = documentXml.getElementsByTagName("VENTAS");
	    Element elementos = (Element) listSales.item(0);
	    List<InvoiceTO> listInvoiceTOs = new ArrayList<>();
		System.out.println("************************CXC**************************************");
		NodeList listCxc = elementos.getElementsByTagName("CXC");
		 /*Elemento CXC*/
		 InvoiceTO invoiceTO;
	     for (int j = 0; j < listCxc.getLength(); j++){
	    	 invoiceTO = new InvoiceTO();
	    	 
	    	 Element elementCxc = (Element) listCxc.item(j);
	    	 Integer numCxc = Integer.parseInt(elementCxc.getElementsByTagName("NUMCXC").item(0).getTextContent());
	    	 invoiceTO.setNumCxc(numCxc);
	    	 System.out.println("----- NUMCXC"+invoiceTO.getNumCxc());
	       	 
	    	 String account = elementCxc.getElementsByTagName("CUENTA").item(0).getTextContent();
	    	 invoiceTO.setAccount(account);
	    	 System.out.println("---------- CUENTA: "+invoiceTO.getAccount());
	    	 
	    	 String client = elementCxc.getElementsByTagName("CLIENTE").item(0).getTextContent();
	    	 invoiceTO.setClient(client);
	    	 System.out.println("---------- CLIENTE: "+invoiceTO.getClient());
	    	 
	    	 String description = elementCxc.getElementsByTagName("DESCRIPCION").item(0).getTextContent();
	    	 invoiceTO.setDescription(description);
	    	 System.out.println("---------- DESCRIPCION: "+invoiceTO.getDescription());
	    	 
	    	 String tax = elementCxc.getElementsByTagName("IMPUESTO").item(0).getTextContent();
	    	 invoiceTO.setTax(tax);
	    	 System.out.println("---------- IMPUESTO: "+invoiceTO.getTax());
	    	 
	    	 String currency = elementCxc.getElementsByTagName("MONEDA").item(0).getTextContent();
	    	 invoiceTO.setCurrency(currency);
	    	 System.out.println("---------- MONEDA: "+invoiceTO.getCurrency());
	    	 
	    	 Integer quantity = Integer.parseInt(elementCxc.getElementsByTagName("CANTIDAD").item(0).getTextContent());
	    	 invoiceTO.setQuantity(quantity);
	    	 System.out.println("---------- CANTIDAD: "+invoiceTO.getQuantity());
	    	 
	    	 BigDecimal price = new BigDecimal(elementCxc.getElementsByTagName("PRECIO").item(0).getTextContent());
	    	 invoiceTO.setPrice(price);
	    	 System.out.println("---------- MONTO: "+invoiceTO.getPrice());
	    	 
	    	 Date date = CommonsUtilities.convertStringtoDate(elementCxc.getElementsByTagName("FECOBRO").item(0).getTextContent(), "yyyyMMdd");
	    	 invoiceTO.setDate(date);
	         System.out.println("---------- FECHA:"+CommonsUtilities.convertDatetoString(date));
	         
	         String serviceType = elementCxc.getElementsByTagName("TIPOSERVICIO").item(0).getTextContent();
	    	 invoiceTO.setServiceType(serviceType);
	    	 System.out.println("---------- TIPOSERVICIO: "+invoiceTO.getServiceType());
	    	 
	    	 String refAux = elementCxc.getElementsByTagName("REF_AUX").item(0).getTextContent();
	    	 invoiceTO.setRefAux(refAux);
	    	 System.out.println("---------- REF_AUX: "+invoiceTO.getRefAux());
	    	 
	    	 listInvoiceTOs.add(invoiceTO);
	     }
		return listInvoiceTOs;
	}
	/**Aqui envio mi lista de fact
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException */
	public XmlObjectSap sendInvoiceSapClient(List<InvoiceTO> list,Long idBillingCalcProcessPk) throws SOAPException, ParserConfigurationException, SAXException, IOException{
		StringBuilder sql = new StringBuilder();
		sql.append(" select pt.description from ParameterTable pt where pt.masterTable.masterTablePk = "+MasterTableType.END_POINT_SAP.getCode());
		@SuppressWarnings("unchecked")
		List<String> listDesc = parameterServiceBean.findListByQueryString(sql.toString());
		String endPoint = listDesc.get(0);
		
		byte[] xmlData = billingProcessedServiceBean.getBillingProcessFileContent(idBillingCalcProcessPk);
		
		
	    InputStream is = new ByteArrayInputStream(xmlData);
	    SOAPMessage soapMessage = MessageFactory.newInstance().createMessage(null, is);
		try {
			SOAPMessage messageResponse= clientSoapService.sendSapClient(soapMessage,endPoint,clientSoapService.SOAP_ACTION_INVOICE);
			XmlObjectSap objectSap = buildXmlObjectSap(soapMessage, messageResponse);
			return objectSap;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
		//SOAPMessage soapMessageSend = createSoapEnvelope(list,idBillingCalcProcessPk);
		/*MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();

		String myNamespace = "tem";
		String myNamespaceURI = "http://tempuri.org/";

		String myNameSpaceChild = "ied";
		String myNamespaceURIchild = "http://schemas.datacontract.org/2004/07/IEDVServicios";
		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration(myNamespace, myNamespaceURI);
		envelope.addNamespaceDeclaration(myNameSpaceChild, myNamespaceURIchild);

		SOAPBody soapBody = envelope.getBody();
		SOAPElement servFactura = soapBody.addChildElement("ServFactura",myNamespace);
		SOAPElement facturaC = servFactura.addChildElement("facturaC",myNamespace);
		List<Object> listNumInvoices = new ArrayList<>();
		for (InvoiceTO invoiceTO : list) {
			SOAPElement FacturaC = facturaC.addChildElement("FacturaC",myNameSpaceChild);

			SOAPElement client = FacturaC.addChildElement("CLIENTE",myNameSpaceChild);
			client.addTextNode(invoiceTO.getClient());

			SOAPElement clientInvoice = FacturaC.addChildElement("CLIENTE_FACTURAR", myNameSpaceChild);
			clientInvoice.addTextNode(invoiceTO.getClient());

			SOAPElement account = FacturaC.addChildElement("CUENTA",myNameSpaceChild);
			account.addTextNode(invoiceTO.getAccount());

			SOAPElement description = FacturaC.addChildElement("DESCRIPCION",myNameSpaceChild);
			description.addTextNode(invoiceTO.getDescription());

			SOAPElement detail = FacturaC.addChildElement("Detalle",myNameSpaceChild);

			SOAPElement invoiceD = detail.addChildElement("FacturaD",myNameSpaceChild);

			SOAPElement quantity = invoiceD.addChildElement("CANTIDAD",myNameSpaceChild);
			quantity.addTextNode(invoiceTO.getQuantity().toString());

			SOAPElement item = invoiceD.addChildElement("ITEM", myNameSpaceChild);
			String itemEquivalent = billingProcessedServiceBean.findTermServiceCode(idBillingCalcProcessPk);
			item.addTextNode(itemEquivalent);
			//Su server solo acepta enteros, no acepta ni con coma decimal ni punto
			SOAPElement price = invoiceD.addChildElement("PRECIO", myNameSpaceChild);
			price.addTextNode(invoiceTO.getPrice()+"");

			SOAPElement paymentDate = FacturaC.addChildElement("FECOBRO",myNameSpaceChild);
			paymentDate.addTextNode(CommonsUtilities.convertDateToString(invoiceTO.getDate(),"yyyyMMdd"));

			SOAPElement tax = FacturaC.addChildElement("IMPUESTO", myNameSpaceChild);
			tax.addTextNode(invoiceTO.getTax());

			FacturaC.addChildElement("MONEDA", myNameSpaceChild).addTextNode(invoiceTO.getCurrency());

			FacturaC.addChildElement("NUMCXC", myNameSpaceChild).addTextNode(invoiceTO.getNumCxc().toString());

			Long numInvoice = billingProcessedServiceBean.getCorrelativeNumberInvoice();
			
			listNumInvoices.add(numInvoice);
			
			FacturaC.addChildElement("NUMFACT", myNameSpaceChild).addTextNode(numInvoice.toString());

			FacturaC.addChildElement("REF_AUX", myNameSpaceChild).addTextNode(invoiceTO.getRefAux());

			FacturaC.addChildElement("TIPOSERVICIO", myNameSpaceChild).addTextNode(invoiceTO.getServiceType());
			
			StringBuilder sql = new StringBuilder();
			sql.append(" select pt.description from ParameterTable pt where pt.masterTable.masterTablePk = "+MasterTableType.TOKKEN_CODE_SAP.getCode());
			List<String> listDesc = parameterServiceBean.findListByQueryString(sql.toString());
			String tokken = listDesc.get(0);
			FacturaC.addChildElement("TOKKEN", myNameSpaceChild).addTextNode(""+tokken);
		}
		
		StringBuilder sql = new StringBuilder();
		sql.append(" select pt.description from ParameterTable pt where pt.masterTable.masterTablePk = "+MasterTableType.END_POINT_SAP.getCode());
		@SuppressWarnings("unchecked")
		List<String> listDesc = parameterServiceBean.findListByQueryString(sql.toString());
		String endPoint = listDesc.get(0);
		SOAPMessage messageResponse= clientSoapService.sendSapClient(soapMessage,endPoint,clientSoapService.SOAP_ACTION_INVOICE);
		XmlObjectSap objectSap = buildXmlObjectSap(soapMessage, messageResponse);
		String listSendInvoice = CommonsUtilities.convertListObjectToString(listNumInvoices, ",");
		objectSap.setListSendInvoice(listSendInvoice);
		return objectSap;*/
		
	}
	private XmlObjectSap buildXmlObjectSap(SOAPMessage soapMessageSend,SOAPMessage messageResponse){
		XmlObjectSap objectSap = new XmlObjectSap();
		objectSap.setSoapMessageSend(soapMessageSend);
		objectSap.setMessageResponse(messageResponse);
		if(messageResponse==null)
			return null;
		try {
			JAXBContext jc;
			jc = JAXBContext.newInstance(ServFacturaResponse.class);
			Unmarshaller um = jc.createUnmarshaller();
			ServFacturaResponse object = new ServFacturaResponse();
			object = (ServFacturaResponse)um.unmarshal(messageResponse.getSOAPBody().extractContentAsDocument());
			objectSap.setFacturaResponse(object);
		} catch (JAXBException | SOAPException e) {
			e.printStackTrace();
			return null;
		}
		return objectSap;
	}
	public XmlObjectSap sendInvoiceSapClient(SendInterfaceXmlSapTO interfaceXmlSapTO){
		byte[] fileContent = interfaceXmlSapTO.getXmlSoapSendMessage();
		InputStream is = new ByteArrayInputStream(fileContent);
		try {
			SOAPMessage soapMessageSend = MessageFactory.newInstance().createMessage(null, is);
			StringBuilder sql = new StringBuilder();
			sql.append(" select pt.description from ParameterTable pt where pt.masterTable.masterTablePk = "+MasterTableType.END_POINT_SAP.getCode());
			@SuppressWarnings("unchecked")
			List<String> listDesc = parameterServiceBean.findListByQueryString(sql.toString());
			String endPoint = listDesc.get(0);
			SOAPMessage messageResponse= clientSoapService.sendSapClient(soapMessageSend,endPoint,clientSoapService.SOAP_ACTION_INVOICE);
			XmlObjectSap objectSap = new XmlObjectSap();
			objectSap.setMessageResponse(messageResponse);
			objectSap.setSoapMessageSend(soapMessageSend);
		} catch (IOException | SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/*
	private SOAPMessage createSoapEnvelope(List<InvoiceTO> list,Long idBillingCalcProcessPk)
			throws SOAPException {
		
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();

		String myNamespace = "tem";
		String myNamespaceURI = "http://tempuri.org/";

		String myNameSpaceChild = "ied";
		String myNamespaceURIchild = "http://schemas.datacontract.org/2004/07/IEDVServicios";
		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration(myNamespace, myNamespaceURI);
		envelope.addNamespaceDeclaration(myNameSpaceChild, myNamespaceURIchild);

		SOAPBody soapBody = envelope.getBody();
		SOAPElement servFactura = soapBody.addChildElement("ServFactura",myNamespace);
		SOAPElement facturaC = servFactura.addChildElement("facturaC",myNamespace);
		List<Long> listNumInvoices = new ArrayList<>();
		for (InvoiceTO invoiceTO : list) {
			SOAPElement FacturaC = facturaC.addChildElement("FacturaC",myNameSpaceChild);

			SOAPElement client = FacturaC.addChildElement("CLIENTE",myNameSpaceChild);
			client.addTextNode(invoiceTO.getClient());

			SOAPElement clientInvoice = FacturaC.addChildElement("CLIENTE_FACTURAR", myNameSpaceChild);
			clientInvoice.addTextNode(invoiceTO.getClient());

			SOAPElement account = FacturaC.addChildElement("CUENTA",myNameSpaceChild);
			account.addTextNode(invoiceTO.getAccount());

			SOAPElement description = FacturaC.addChildElement("DESCRIPCION",myNameSpaceChild);
			description.addTextNode(invoiceTO.getDescription());

			SOAPElement detail = FacturaC.addChildElement("Detalle",myNameSpaceChild);

			SOAPElement invoiceD = detail.addChildElement("FacturaD",myNameSpaceChild);

			SOAPElement quantity = invoiceD.addChildElement("CANTIDAD",myNameSpaceChild);
			quantity.addTextNode(invoiceTO.getQuantity().toString());

			SOAPElement item = invoiceD.addChildElement("ITEM", myNameSpaceChild);
			String itemEquivalent = billingProcessedServiceBean.findTermServiceCode(idBillingCalcProcessPk);
			item.addTextNode(itemEquivalent);
			//Su server solo acepta enteros, no acepta ni con coma decimal ni punto
			SOAPElement price = invoiceD.addChildElement("PRECIO", myNameSpaceChild);
			price.addTextNode(invoiceTO.getPrice()+"");

			SOAPElement paymentDate = FacturaC.addChildElement("FECOBRO",myNameSpaceChild);
			paymentDate.addTextNode(CommonsUtilities.convertDateToString(invoiceTO.getDate(),"yyyyMMdd"));

			SOAPElement tax = FacturaC.addChildElement("IMPUESTO", myNameSpaceChild);
			tax.addTextNode(invoiceTO.getTax());

			FacturaC.addChildElement("MONEDA", myNameSpaceChild).addTextNode(invoiceTO.getCurrency());

			FacturaC.addChildElement("NUMCXC", myNameSpaceChild).addTextNode(invoiceTO.getNumCxc().toString());

			Long numInvoice = billingProcessedServiceBean.getCorrelativeNumberInvoice();
			
			listNumInvoices.add(numInvoice);
			
			FacturaC.addChildElement("NUMFACT", myNameSpaceChild).addTextNode(numInvoice.toString());

			FacturaC.addChildElement("REF_AUX", myNameSpaceChild).addTextNode(invoiceTO.getRefAux());

			FacturaC.addChildElement("TIPOSERVICIO", myNameSpaceChild).addTextNode(invoiceTO.getServiceType());
			
			StringBuilder sql = new StringBuilder();
			sql.append(" select pt.description from ParameterTable pt where pt.masterTable.masterTablePk = "+MasterTableType.TOKKEN_CODE_SAP.getCode());
			List<String> listDesc = parameterServiceBean.findListByQueryString(sql.toString());
			String tokken = listDesc.get(0);
			FacturaC.addChildElement("TOKKEN", myNameSpaceChild).addTextNode(""+tokken);
		}
		return soapMessage;
	}*/
	
	 public SendInterfaceXmlSap updateSendSapXml(XmlObjectSap xmlObjectSap,SendInterfaceXmlSapTO selectedForwarding ){
		 SendInterfaceXmlSap interfaceXmlSap = parameterServiceBean.find(SendInterfaceXmlSap.class, selectedForwarding.getIdSendInterfaceXmlSapPk());
		 interfaceXmlSap.setState(StateSapResponseType.RE_ENVIO.getCode());
		 Integer numberReSend = selectedForwarding.getNumberReSend()+1;
		 interfaceXmlSap.setNumberReSend(numberReSend);
		 try {
			parameterServiceBean.updateWithOutFlush(interfaceXmlSap);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return null;
	 }
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public SendInterfaceXmlSap registreSendSapXml(SendInterfaceXmlSapTO interfaceXmlSapTO){
    	ExternalInterface externalInterface = parameterServiceBean.getExternalInterface("REG-FAC", false);
    	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
    	if (loggerUser == null || loggerUser.getIdPrivilegeOfSystem() == null) {
			if (loggerUser == null) {
				loggerUser = new LoggerUser();
				loggerUser.setUserAction(new UserAcctions());
			}
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction()
					.getIdPrivilegeRegister());
		}
    	
	    InterfaceProcess interfaceProcess = new InterfaceProcess();
	    interfaceProcess.setProcessDate(CommonsUtilities.currentDate());
	    interfaceProcess.setIdExternalInterfaceFk(externalInterface);
	    interfaceProcess.setAudit(loggerUser);
	    interfaceProcess.setFileUpload(interfaceXmlSapTO.getXmlSoapSendMessage());
	    interfaceProcess.setFileResponse(interfaceXmlSapTO.getXmlSoapRecepMessage());
	    interfaceProcess.setFileName(CommonsUtilities.CHARACTER_TRUNCATE_STRING);
	    interfaceProcess.setIdExtensionFileTypeFk(new ExtensionFileType(5L));//XML
	    interfaceProcess.setProcessDate(CommonsUtilities.currentDate());
	    interfaceProcess.setRegistryDate(CommonsUtilities.currentDate());
	    interfaceProcess.setRegistryUser(userInfo.getUserAccountSession().getUserName());
	    interfaceProcess.setProcessType(ExternalInterfaceExecutionType.MANUAL.getCode());
	    interfaceProcess.setProcessState(ExternalInterfaceReceptionStateType.CORRECT.getCode());
	    
	    interfaceProcess.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
	    interfaceProcess.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
	    interfaceProcess.setLastModifyDate(CommonsUtilities.currentDate());
	    interfaceProcess.setLastModifyApp(userInfo.getLastModifyPriv());
	    
	    
	    billingProcessedServiceBean.createWithFlush(interfaceProcess);
	    
	    InterfaceTransaction interfaceTransaction = new InterfaceTransaction();
    	interfaceTransaction.setInterfaceProcess(interfaceProcess);
    	interfaceTransaction.setTransactionState(BooleanType.YES.getCode());
    	//interfaceTransaction.setParticipant(new Participant("EDB"));
    	
    	interfaceTransaction.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
    	interfaceTransaction.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
    	interfaceTransaction.setLastModifyDate(CommonsUtilities.currentDate());
    	interfaceTransaction.setLastModifyApp(userInfo.getLastModifyPriv());
    	
    	billingProcessedServiceBean.createWithFlush(interfaceTransaction);
    	//Este es nuestra tabla
    	SendInterfaceXmlSap interfaceXmlSap = new SendInterfaceXmlSap();
    	interfaceXmlSap.setIdBillingCalcProcessFk(interfaceXmlSapTO.getIdBillingCalcProcessFk());
    	interfaceXmlSap.setIdInterfacesTransactionFk(interfaceTransaction);
    	interfaceXmlSap.setResponseValue(interfaceXmlSapTO.getResponseValue());
		interfaceXmlSap.setAcceptedRecords(interfaceXmlSapTO.getNumberAccepted());
		interfaceXmlSap.setRejectRecords(interfaceXmlSapTO.getNumberRejected());
		interfaceXmlSap.setTotalRecords(interfaceXmlSapTO.getNumberAccepted()+interfaceXmlSapTO.getNumberRejected());
    	//si ya se encuentra un registro correcto no deberiamos de poder enviar de nuevo
		interfaceXmlSap.setState(interfaceXmlSapTO.getState());
		interfaceXmlSap.setResponseValue(interfaceXmlSapTO.getResponseValue());
		interfaceXmlSap.setInvoiceListString(interfaceXmlSapTO.getInvoiceList());
		
		interfaceXmlSap.setRegistryDate(CommonsUtilities.currentDate());
    	interfaceXmlSap.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
    	interfaceXmlSap.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
    	interfaceXmlSap.setLastModifyDate(CommonsUtilities.currentDate());
    	interfaceXmlSap.setLastModifyApp(userInfo.getLastModifyPriv());
    	
    	billingProcessedServiceBean.createWithFlush(interfaceXmlSap);
    	
    	return interfaceXmlSap;
    }
	
	/**
	 * Busqueda de facturas enviadas
	 * */
	public List<SendInterfaceXmlSapTO> searchSentInvoices(ParameterInvoiceSearchTO invoiceSearchTO){
		List<SendInterfaceXmlSapTO> list = billingProcessedServiceBean.searchSentInvoices(invoiceSearchTO);
		return list;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public void flushTransaction() {
		billingProcessedServiceBean.flushTransaction();
	}
	
	

	/**
	 * TODO
	 * @param processBillingCalculationPk
	 * @throws IOException
	 */
	public void generateInterfaceXml(Long processBillingCalculationPk) throws IOException {
		ByteArrayOutputStream byteArrayFile = this.generateFileXml(processBillingCalculationPk);
		saveFileXMlInterfaceBilling(byteArrayFile, processBillingCalculationPk);
	}
	

	/**
	 * Generate file xml.
	 *
	 * @param processBillingCalculationPk the process billing calculation pk
	 * @return the byte array output stream
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public ByteArrayOutputStream generateFileXml(Long processBillingCalculationPk) throws IOException{

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ByteArrayOutputStream baosReturn = new ByteArrayOutputStream();
		try {
			List<Object[]> billing=this.billingProcessedServiceBean.getAccountReceivable(processBillingCalculationPk);
			XMLOutputFactory xof = XMLOutputFactory.newInstance();
			XMLStreamWriter xmlsw;

			xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(baos));

			//open document file

//			xmlsw.writeStartDocument(PropertiesConstants.ENCODING_XML, "1.0");
			

			//root tag of report
			/**
			 * SERVICES
			 * */
			xmlsw.writeStartElement(PropertiesConstants.TAG_SOAP_START);
			createTagString(xmlsw,PropertiesConstants.TAG_SOAP_HEADER, GeneralConstants.EMPTY_STRING);
			xmlsw.writeStartElement(PropertiesConstants.TAG_SOAP_BODY);
			xmlsw.writeStartElement(PropertiesConstants.TAG_SOAP_ServFactura);
			xmlsw.writeStartElement(PropertiesConstants.TAG_SOAP_facturaC);
			


		if(!billing.isEmpty()) {
			//Method create body of File XML
			createBodyReport(xmlsw,  billing);

		} else {
			//when not exist data we need to create empty structure xml
//			xmlsw.writeStartElement(PropertiesConstants.TAG_CXC);
			createTagString(xmlsw, PropertiesConstants.TAG_SOAP_CLIENTE,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, PropertiesConstants.TAG_SOAP_CLIENTE_FACTURAR,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, PropertiesConstants.TAG_SOAP_CUENTA,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, PropertiesConstants.TAG_SOAP_DESCRIPCION, GeneralConstants.EMPTY_STRING  );
			xmlsw.writeStartElement(PropertiesConstants.TAG_SOAP_Detalle);
			xmlsw.writeStartElement(PropertiesConstants.TAG_SOAP_FacturaD);
			createTagString(xmlsw, PropertiesConstants.TAG_SOAP_CANTIDAD,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, PropertiesConstants.TAG_SOAP_ITEM,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, PropertiesConstants.TAG_SOAP_PRECIO,  GeneralConstants.EMPTY_STRING );
			xmlsw.writeEndElement();
			xmlsw.writeEndElement();
			createTagString(xmlsw, PropertiesConstants.TAG_SOAP_FECOBRO,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, PropertiesConstants.TAG_SOAP_IMPUESTO,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, PropertiesConstants.TAG_SOAP_MONEDA,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, PropertiesConstants.TAG_SOAP_NUMCXC,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, PropertiesConstants.TAG_SOAP_NUMFACT,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, PropertiesConstants.TAG_SOAP_REF_AUX,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, PropertiesConstants.TAG_SOAP_TIPOSERVICIO,  GeneralConstants.EMPTY_STRING );
			createTagString(xmlsw, PropertiesConstants.TAG_SOAP_TOKKEN,  GeneralConstants.EMPTY_STRING );
//			xmlsw.writeEndElement();
		}

		//close report tag SERVICES
		xmlsw.writeEndElement();
		xmlsw.writeEndElement();
		xmlsw.writeEndElement();
		xmlsw.writeEndElement();
		//close document
//		xmlsw.writeEndDocument();
		xmlsw.flush();
        xmlsw.close();
        

        String replaceQuote=baos.toString();
        replaceQuote=replaceQuote.replace("<"+PropertiesConstants.TAG_SOAP_START, 
        		"<"+PropertiesConstants.TAG_SOAP_START.concat(PropertiesConstants.TAG_SOAP_START_CONTENT));
        replaceQuote=replaceQuote.replace("></SOAP-ENV:Header>", "/>");
        replaceQuote=replaceQuote.replace("'", "\"");
        replaceQuote=replaceQuote+" _Prueba";
		System.out.println(replaceQuote);
        baosReturn.write(replaceQuote.getBytes());
		} catch (XMLStreamException ex) {
			log.error(ex.getMessage());
			ex.printStackTrace();
			throw new RuntimeException();
		}
		
		return baosReturn;
	}
	

	/**
	 * Creates the body report.
	 * good practice factoring
	 *
	 * @param xmlsw the xmlsw
	 * @param billings the billings
	 * @throws XMLStreamException the xML stream exception
	 */
	public void createBodyReport(XMLStreamWriter xmlsw,	List<Object[]> billings)  throws XMLStreamException{
		int i=1;
		for(Object[] billing : billings) {
			System.out.println(billing);
			Long idBillignServicePk=billing[INDEX_ID_BILLING_SERVICE_PK]!=null?new Long(billing[INDEX_ID_BILLING_SERVICE_PK].toString()):0l;
			BillingService billingService = billingProcessedServiceBean.find(BillingService.class,idBillignServicePk );
			List<BillingEconomicActivity> listEconActBd=billingServiceMgmtServiceBean.getBillingEconomicActivityByBillingServicePk(billingService.getIdBillingServicePk());
			List<Integer> listEconomicAct=BillingUtils.getBillingEconomicActivityListInteger(listEconActBd);
			Integer portFolio=billingService.getPortfolio();
			String mnemonicHolder=null;
			if(BillingUtils.isAfpPortfolioClient(listEconomicAct, portFolio)) {
				mnemonicHolder="TGN";
			}else {
				mnemonicHolder=billing[INDEX_MNEMONIC_HOLDER].toString().trim();
			}
			Integer baseCollection=Integer.parseInt(billing[INDEX_BASE_COLLECTION].toString()) ;
			
			
			String indexClient=billing[INDEX_CLIENT]!=null?billing[INDEX_CLIENT].toString().trim():"";
			String indexAccount=billing[INDEX_ACCOUNT]!=null?billing[INDEX_ACCOUNT].toString().trim():"";
			//NEW dETAIL REPORT
			xmlsw.writeStartElement(PropertiesConstants.TAG_SOAP_IED_FACTURAC);
			createTagString(xmlsw, PropertiesConstants.TAG_SOAP_CLIENTE,  indexClient );
			createTagString(xmlsw, PropertiesConstants.TAG_SOAP_CLIENTE_FACTURAR,  mnemonicHolder );
			createTagString(xmlsw, PropertiesConstants.TAG_SOAP_CUENTA,  indexAccount.concat(GeneralConstants.DOT.concat(indexClient)) );
			/**
			 * Issuer 23 Mantis EDV, Incluir en el Tag de Descripcion del archivo 
			 * de Servicios, el Nemonico del Participante, CUI y Descripcion del Titular.
			 * Este cambio para los Servicios 3 y 4 para AFPs.
			 */
			if(BaseCollectionType.RECORD_BOOK_ENTRY_SECURITIES_FIXED_INCOME_AFP.getCode().equals(baseCollection)||
					BaseCollectionType.RECORD_BOOK_ENTRY_SECURITIES_VARIABLE_INCOME_AFP.getCode().equals(baseCollection)){
				createTagString(xmlsw, PropertiesConstants.TAG_SOAP_DESCRIPCION,  billing[INDEX_DESCRIPTION].toString().concat(GeneralConstants.TWO_POINTS).concat(indexClient).concat(GeneralConstants.DASH).concat(billing[INDEX_ENTITY_DESCRIPTION].toString()) );
			}else{
				StringBuilder sb=new StringBuilder();
				sb.append(billing[INDEX_DESCRIPTION]);
				Object descriptionTarifa1=billing[INDEX_DESCRIPTION_TARIFA_ONE_AFP];
				if(BillingUtils.isBaseCollectionOneAfp(listEconomicAct, baseCollection) &&
						(descriptionTarifa1!=null && descriptionTarifa1.toString().trim().length()>20)) {
					sb.append(" - ");
					sb.append(billing[INDEX_DESCRIPTION_TARIFA_ONE_AFP]);
				}
				createTagString(xmlsw, PropertiesConstants.TAG_SOAP_DESCRIPCION,  sb.toString() );
			
			}
			xmlsw.writeStartElement(PropertiesConstants.TAG_SOAP_Detalle);
			xmlsw.writeStartElement(PropertiesConstants.TAG_SOAP_FacturaD);
			createTagString(xmlsw, PropertiesConstants.TAG_SOAP_CANTIDAD,  billing[INDEX_CANTIDAD] );
			createTagString(xmlsw, PropertiesConstants.TAG_SOAP_ITEM,  billing[INDEX_SERVICE_CODE] );
			createTagString(xmlsw, PropertiesConstants.TAG_SOAP_PRECIO,  billing[INDEX_PRECIO] );
			xmlsw.writeEndElement();
			xmlsw.writeEndElement();
			createTagString(xmlsw, PropertiesConstants.TAG_SOAP_FECOBRO,  billing[INDEX_FECOBRO] );
			createTagString(xmlsw, PropertiesConstants.TAG_SOAP_IMPUESTO,  billing[INDEX_IMPUESTO] );
			createTagString(xmlsw, PropertiesConstants.TAG_SOAP_MONEDA,  billing[INDEX_MONEDA] );
			createTagString(xmlsw, PropertiesConstants.TAG_SOAP_NUMCXC,  i );
			createTagString(xmlsw, PropertiesConstants.TAG_SOAP_NUMFACT, billing[INDEX_NUMCXC] );
			createTagString(xmlsw, PropertiesConstants.TAG_SOAP_REF_AUX,  billing[INDEX_REF_AUX] );
			createTagString(xmlsw, PropertiesConstants.TAG_SOAP_TIPOSERVICIO,  billing[INDEX_TIPOSERVICIO] );
			/**Valor de tokken SAP*/
			StringBuilder sql = new StringBuilder();
			sql.append(" select pt.description from ParameterTable pt where pt.masterTable.masterTablePk = "+MasterTableType.TOKKEN_CODE_SAP.getCode());
			List<String> listDesc = parameterServiceBean.findListByQueryString(sql.toString());
			String tokken = listDesc.get(0);
			createTagString(xmlsw, PropertiesConstants.TAG_SOAP_TOKKEN,  tokken );
			
			i++;
			xmlsw.writeEndElement();
		}
		
	}
	
	
	
	
	public void saveFileXMlInterfaceBilling(ByteArrayOutputStream byteArray, Long processBillingCalculationPk) throws IOException{
		try{
			ParameterTable parameter=billingProcessedServiceBean.find(ParameterTable.class, 2135);
			String nameFile=PropertiesConstants.TAG_CXC.concat(CommonsUtilities.convertDatetoStringForTemplate(
								CommonsUtilities.currentDateTime(), CommonsUtilities.DATETIME_HOURS_PATTERN)).concat(GeneralConstants.XML_FORMAT);

			
			BillingCalculationProcess billingCalculationProcess = billingProcessedServiceBean.find(BillingCalculationProcess.class, processBillingCalculationPk);
			billingCalculationProcess.setFileXml(byteArray.toByteArray());
			billingCalculationProcess.setNameFile("HC"+nameFile);
			billingProcessedServiceBean.update(billingCalculationProcess);
			


		} catch(ServiceException ex){
			log.error(ex.getMessage());
		}
	}

	/**
	 * Creates the tag string.
	 *
	 * @param xmlsw the xmlsw
	 * @param nameTag the name tag
	 * @param data the data
	 * @throws XMLStreamException the xML stream exception
	 */
	public void createTagString(XMLStreamWriter xmlsw,String nameTag,Object data) throws XMLStreamException{
		xmlsw.writeStartElement(nameTag);
		xmlsw.writeCharacters(formatString(data));
		xmlsw.writeEndElement();
	}
	
	/**
	 * Format string.
	 *
	 * @param data the data
	 * @return the string
	 */
	public String formatString(Object data){
		String value = GeneralConstants.EMPTY_STRING;
		if(data!=null) {
			value = data.toString();
		}
		return value;
	}	
	
	
	
}
