package com.pradera.billing.billing.to;

import java.io.Serializable;

import javax.xml.soap.SOAPMessage;

import com.edv.org.tempuri.ServFacturaResponse;


public class XmlObjectSap implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5564528999508317975L;
	
	private ServFacturaResponse facturaResponse;
	
	private SOAPMessage soapMessageSend;
	
	private SOAPMessage messageResponse;
	
	private String listSendInvoice;
	
	public XmlObjectSap() {
		// TODO Auto-generated constructor stub
	}

	public ServFacturaResponse getFacturaResponse() {
		return facturaResponse;
	}

	public void setFacturaResponse(ServFacturaResponse facturaResponse) {
		this.facturaResponse = facturaResponse;
	}

	public SOAPMessage getSoapMessageSend() {
		return soapMessageSend;
	}

	public void setSoapMessageSend(SOAPMessage soapMessageSend) {
		this.soapMessageSend = soapMessageSend;
	}

	public SOAPMessage getMessageResponse() {
		return messageResponse;
	}

	public void setMessageResponse(SOAPMessage messageResponse) {
		this.messageResponse = messageResponse;
	}

	public String getListSendInvoice() {
		return listSendInvoice;
	}

	public void setListSendInvoice(String listSendInvoice) {
		this.listSendInvoice = listSendInvoice;
	}
}
