package com.pradera.billing.billing.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.StreamedContent;

import com.pradera.billing.billing.facade.BillingProcessedServiceFacade;
import com.pradera.billing.billing.to.BillingProcessedFilter;
import com.pradera.billing.billing.to.BillingProcessedTo;
import com.pradera.billing.billingcollection.facade.CollectionServiceMgmtServiceFacade;
import com.pradera.billing.billingcollection.to.CollectionRecordTo;
import com.pradera.billing.common.utils.BillingUtils;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.security.model.type.LogoutMotiveType;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.view.IssuerHelperBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantClassType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.billing.BillingCalculationProcess;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.type.BaseCollectionType;
import com.pradera.model.billing.type.BillingCalculationType;
import com.pradera.model.billing.type.BillingStateType;
import com.pradera.model.billing.type.EntityCollectionType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class BillingMgmtBean extends GenericBaseBean{
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;
	
	/** The log. */
	@Inject
	private transient PraderaLogger log;
	

	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;

	/** The billing processed service facade. */
	@EJB
	BillingProcessedServiceFacade billingProcessedServiceFacade;
	
	/** The collection service mgmt service facade. */
	@EJB 
	CollectionServiceMgmtServiceFacade collectionServiceMgmtServiceFacade;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	/** The issuer helper bean. */
	@Inject
	IssuerHelperBean issuerHelperBean;
	
	
	
	/** The billing processed to data model. */

	private GenericDataModel<BillingProcessedTo> billingProcessedToDataModel;
	
	/** The list billing processed to. */
	private List<BillingProcessedTo> listBillingProcessedTo;
	
	/** The billing processed to selected. */
	private BillingProcessedTo billingProcessedToSelected;
	
	/** The billing processed to executed. */
	private BillingProcessedTo billingProcessedToExecuted;
	
	/** The list billing processed to executed. */
	private List<BillingProcessedTo> listBillingProcessedToExecuted;
	
	/** The calculation process data model. */
	private GenericDataModel<BillingCalculationProcess> calculationProcessDataModel;
	
	/** The list calculation process. */
	private List<BillingCalculationProcess> listCalculationProcess;
	
	/** The calculation process selected. */
	private BillingCalculationProcess calculationProcessSelected;
	
	/** The disable holder ec visible. */
	private Boolean disableHolderECVisible;
	
	/** The disable issuer ec visible. */
	private Boolean disableIssuerECVisible;
	
	/** The disable participant ec visible. */
	private Boolean disableParticipantECVisible;
	
	/** The disable participant afpec visible. */
	private Boolean disableParticipantAFPECVisible;
	
	/** The disable other ec visible. */
	private Boolean disableOtherECVisible;
	
	/** The applied tax disable. */
	private Boolean appliedTaxDisable;
	
	/** The disable daily visible. */
	private Boolean disableDailyVisible;
	
	/** The disable weekly visible. */
	private Boolean disableWeeklyVisible;
	
	/** The disable month visible. */
	private Boolean disableMonthVisible;
	
	/** The disable quarterly visible. */
	private Boolean disableQuarterlyVisible;
	
	/** The disable yearly visible. */
	private Boolean disableYearlyVisible;
	
	/** The disable helper billing visible. */
	private Boolean disableHelperBillingVisible;
	
	/** The Disable Last Billing Date. */
	private Boolean disableLastBillingDate; 
	
	/** Visible to Poll. */
	private Boolean visiblePoll;

	/** The holder selected. */
	private Holder holderSelected;
	
	/** The list client participant. */
	private List<Participant> listClientParticipant;
	
	/** The list client participant afp. */
	private List<Participant> listClientParticipantAFP;
	
	/** The participant selected. */
	private Long participantSelected;
	
	/** The participant afp selected. */
	private Long participantAFPSelected;
	
	/** The model collection record to. */
	private CollectionRecordTo modelCollectionRecordTo;
	 
 	/** The lst document type. */
 	private List<ParameterTable> 					lstDocumentType;
	
	/** The issuer. */
	private Issuer issuer;

	/** The validity initial date. */
	private Date validityInitialDate;
	
	/** The validity final date. */
	private Date validityFinalDate;
	
	/** The initial date. */
	private Date initialDate = CommonsUtilities.currentDate();
	
	/** The final date. */
	private Date finalDate = CommonsUtilities.currentDate();
	
	/**The Last Billing Date by Entity Collection, Collection Period and Billing Service*/
	private Date lastBillingDate;
	
	/**  Variable month. */
	private Date periodDate = CommonsUtilities.currentDate();

	/** The list collection period. */
	private List<ParameterTable> listCollectionPeriod;
	
	/** The collection period selected. */
	private Integer collectionPeriodSelected;
	
	/** The collection institution selected. */
	private Integer collectionInstitutionSelected;
	
	/** The serie billing. */
	private String serieBilling;
	
	/**  Set Date for Billing Weekly. */
	private boolean boolDateBecoming;
	
	/** The date becoming. */
	private Date   	dateBecoming=CommonsUtilities.currentDate();
	
	/** The list serie billing. */
	private List<ParameterTable> listSerieBilling;
	
	/** The voucher billing. */
	private String voucherBilling;
	
	/** The list voucher billing. */
	private List<ParameterTable> listVoucherBilling;
	
	/** The list collection institution. */
	private List<ParameterTable> listCollectionInstitution;


	/** The in integrated bill selected. */
	private Integer inIntegratedBillSelected;
	
	/** The ind no integrated. */
	private Integer indNoIntegrated=PropertiesConstants.NOT_IND_INTEGRATED_BILL;
	
	/** The ind integrated. */
	private Integer indIntegrated=PropertiesConstants.YES_IND_INTEGRATED_BILL;
	
	/** The months of the year. */
	private Integer monthsOfTheYear;
	
	/** The year selected. */
	private Integer yearSelected;

	/** The billing status preliminary. */
	private Integer billingStatusPreliminary=BillingStateType.PRELIMINAR.getCode();
	
	/** The billing status definitive. */
	private Integer billingStatusDefinitive=BillingStateType.DEFINITIVO.getCode();;
	
	/** The billing status error. */
	private Integer billingStatusError=BillingStateType.ERROR.getCode();
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The billing service selection. */
	private BillingService billingServiceSelection;

	/** The validation message. */
	private String validationMessage = "";

	/**
	 *  POST CONSTRUCTORS *.
	 */
	@PostConstruct
	public void postConstructorSearch() {
		/* Inicializo rendered's de pantalla */
		billingServiceSelection = new BillingService();
		appliedTaxDisable = Boolean.FALSE;
		listBillingProcessedTo = new ArrayList<BillingProcessedTo>();
		listBillingProcessedToExecuted = new ArrayList<BillingProcessedTo>();
		loadParticipant();

		loadCmbData();
	}
	
	
	/**
	 * Gets the period date.
	 *
	 * @return the period date
	 */
	public Date getPeriodDate() {
		return periodDate;
	}

	/**
	 * Sets the period date.
	 *
	 * @param periodDate the new period date
	 */
	public void setPeriodDate(Date periodDate) {
		this.periodDate = periodDate;
	}

	/**
	 * Gets the billing service selection.
	 *
	 * @return the billing service selection
	 */
	public BillingService getBillingServiceSelection() {
		return billingServiceSelection;
	}

	/**
	 * Sets the billing service selection.
	 *
	 * @param billingServiceSelection the new billing service selection
	 */
	public void setBillingServiceSelection(
			BillingService billingServiceSelection) {
		this.billingServiceSelection = billingServiceSelection;
	}
	
	/**
	 * Gets the disable helper billing visible.
	 *
	 * @return the disable helper billing visible
	 */
	public Boolean getDisableHelperBillingVisible() {
		return disableHelperBillingVisible;
	}

	/**
	 * Sets the disable helper billing visible.
	 *
	 * @param disableHelperBillingVisible the new disable helper billing visible
	 */
	public void setDisableHelperBillingVisible(
			Boolean disableHelperBillingVisible) {
		this.disableHelperBillingVisible = disableHelperBillingVisible;
	}
	

	/**
	 * @return the disableLastBillingDate
	 */
	public Boolean getDisableLastBillingDate() {
		return disableLastBillingDate;
	}


	/**
	 * @param disableLastBillingDate the disableLastBillingDate to set
	 */
	public void setDisableLastBillingDate(Boolean disableLastBillingDate) {
		this.disableLastBillingDate = disableLastBillingDate;
	}


	/**
	 * Gets the year selected.
	 *
	 * @return the year selected
	 */
	public Integer getYearSelected() {
		return yearSelected;
	}

	/**
	 * Sets the year selected.
	 *
	 * @param yearSelected the new year selected
	 */
	public void setYearSelected(Integer yearSelected) {
		this.yearSelected = yearSelected;
	}

	/**
	 * Gets the months of the year.
	 *
	 * @return the months of the year
	 */
	public Integer getMonthsOfTheYear() {
		return monthsOfTheYear;
	}

	/**
	 * Sets the months of the year.
	 *
	 * @param monthsOfTheYear the new months of the year
	 */
	public void setMonthsOfTheYear(Integer monthsOfTheYear) {
		this.monthsOfTheYear = monthsOfTheYear;
	}

	

	/**
	 * Gets the disable daily visible.
	 *
	 * @return the disableDailyVisible
	 */
	public Boolean getDisableDailyVisible() {
		return disableDailyVisible;
	}


	/**
	 * Sets the disable daily visible.
	 *
	 * @param disableDailyVisible the disableDailyVisible to set
	 */
	public void setDisableDailyVisible(Boolean disableDailyVisible) {
		this.disableDailyVisible = disableDailyVisible;
	}


	/**
	 * Gets the disable weekly visible.
	 *
	 * @return the disableWeeklyVisible
	 */
	public Boolean getDisableWeeklyVisible() {
		return disableWeeklyVisible;
	}


	/**
	 * Sets the disable weekly visible.
	 *
	 * @param disableWeeklyVisible the disableWeeklyVisible to set
	 */
	public void setDisableWeeklyVisible(Boolean disableWeeklyVisible) {
		this.disableWeeklyVisible = disableWeeklyVisible;
	}


	/**
	 * Gets the disable month visible.
	 *
	 * @return the disableMonthVisible
	 */
	public Boolean getDisableMonthVisible() {
		return disableMonthVisible;
	}


	/**
	 * Sets the disable month visible.
	 *
	 * @param disableMonthVisible the disableMonthVisible to set
	 */
	public void setDisableMonthVisible(Boolean disableMonthVisible) {
		this.disableMonthVisible = disableMonthVisible;
	}


	/**
	 * Gets the disable quarterly visible.
	 *
	 * @return the disableQuarterlyVisible
	 */
	public Boolean getDisableQuarterlyVisible() {
		return disableQuarterlyVisible;
	}


	/**
	 * Sets the disable quarterly visible.
	 *
	 * @param disableQuarterlyVisible the disableQuarterlyVisible to set
	 */
	public void setDisableQuarterlyVisible(Boolean disableQuarterlyVisible) {
		this.disableQuarterlyVisible = disableQuarterlyVisible;
	}


	/**
	 * Gets the disable yearly visible.
	 *
	 * @return the disableYearlyVisible
	 */
	public Boolean getDisableYearlyVisible() {
		return disableYearlyVisible;
	}


	/**
	 * Sets the disable yearly visible.
	 *
	 * @param disableYearlyVisible the disableYearlyVisible to set
	 */
	public void setDisableYearlyVisible(Boolean disableYearlyVisible) {
		this.disableYearlyVisible = disableYearlyVisible;
	}


	/**
	 * Gets the applied tax disable.
	 *
	 * @return the applied tax disable
	 */
	public Boolean getAppliedTaxDisable() {
		return appliedTaxDisable;
	}

	/**
	 * Sets the applied tax disable.
	 *
	 * @param appliedTaxDisable the new applied tax disable
	 */
	public void setAppliedTaxDisable(Boolean appliedTaxDisable) {
		this.appliedTaxDisable = appliedTaxDisable;
	}

	/**
	 * Gets the in integrated bill selected.
	 *
	 * @return the in integrated bill selected
	 */
	public Integer getInIntegratedBillSelected() {
		return inIntegratedBillSelected;
	}

	/**
	 * Sets the in integrated bill selected.
	 *
	 * @param inIntegratedBillSelected the new in integrated bill selected
	 */
	public void setInIntegratedBillSelected(Integer inIntegratedBillSelected) {
		this.inIntegratedBillSelected = inIntegratedBillSelected;
	}

	/**
	 * Gets the collection institution selected.
	 *
	 * @return the collection institution selected
	 */
	public Integer getCollectionInstitutionSelected() {
		return collectionInstitutionSelected;
	}

	/**
	 * Sets the collection institution selected.
	 *
	 * @param collectionInstitutionSelected the new collection institution selected
	 */
	public void setCollectionInstitutionSelected(
			Integer collectionInstitutionSelected) {
		this.collectionInstitutionSelected = collectionInstitutionSelected;
	}

	/**
	 * Gets the list collection institution.
	 *
	 * @return the list collection institution
	 */
	public List<ParameterTable> getListCollectionInstitution() {
		return listCollectionInstitution;
	}

	/**
	 * Sets the list collection institution.
	 *
	 * @param listCollectionInstitution the new list collection institution
	 */
	public void setListCollectionInstitution(
			List<ParameterTable> listCollectionInstitution) {
		this.listCollectionInstitution = listCollectionInstitution;
	}

	/**
	 * Gets the list collection period.
	 *
	 * @return the list collection period
	 */
	public List<ParameterTable> getListCollectionPeriod() {
		return listCollectionPeriod;
	}

	/**
	 * Sets the list collection period.
	 *
	 * @param listCollectionPeriod the new list collection period
	 */
	public void setListCollectionPeriod(
			List<ParameterTable> listCollectionPeriod) {
		this.listCollectionPeriod = listCollectionPeriod;
	}

	/**
	 * Gets the collection period selected.
	 *
	 * @return the collection period selected
	 */
	public Integer getCollectionPeriodSelected() {
		return collectionPeriodSelected;
	}

	/**
	 * Sets the collection period selected.
	 *
	 * @param collectionPeriodSelected the new collection period selected
	 */
	public void setCollectionPeriodSelected(Integer collectionPeriodSelected) {
		this.collectionPeriodSelected = collectionPeriodSelected;
	}

	/**
	 * Gets the validity initial date.
	 *
	 * @return the validity initial date
	 */
	public Date getValidityInitialDate() {
		return validityInitialDate;
	}

	/**
	 * Sets the validity initial date.
	 *
	 * @param validityInitialDate the new validity initial date
	 */
	public void setValidityInitialDate(Date validityInitialDate) {
		this.validityInitialDate = validityInitialDate;
	}

	/**
	 * Gets the validity final date.
	 *
	 * @return the validity final date
	 */
	public Date getValidityFinalDate() {
		return validityFinalDate;
	}

	/**
	 * Sets the validity final date.
	 *
	 * @param validityFinalDate the new validity final date
	 */
	public void setValidityFinalDate(Date validityFinalDate) {
		this.validityFinalDate = validityFinalDate;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	/**
	 * Gets the issuer helper bean.
	 *
	 * @return the issuer helper bean
	 */
	public IssuerHelperBean getIssuerHelperBean() {
		return issuerHelperBean;
	}

	/**
	 * Sets the issuer helper bean.
	 *
	 * @param issuerHelperBean the new issuer helper bean
	 */
	public void setIssuerHelperBean(IssuerHelperBean issuerHelperBean) {
		this.issuerHelperBean = issuerHelperBean;
	}

	

	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}


	/**
	 * Sets the issuer.
	 *
	 * @param issuer the issuer to set
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}


	/**
	 * Gets the holder selected.
	 *
	 * @return the holder selected
	 */
	public Holder getHolderSelected() {
		return holderSelected;
	}

	/**
	 * Sets the holder selected.
	 *
	 * @param holderSelected the new holder selected
	 */
	public void setHolderSelected(Holder holderSelected) {
		this.holderSelected = holderSelected;
	}

	/**
	 * Gets the list client participant.
	 *
	 * @return the list client participant
	 */
	public List<Participant> getListClientParticipant() {
		return listClientParticipant;
	}

	/**
	 * Sets the list client participant.
	 *
	 * @param listClientParticipant the new list client participant
	 */
	public void setListClientParticipant(List<Participant> listClientParticipant) {
		this.listClientParticipant = listClientParticipant;
	}

	/**
	 * Gets the list client participant afp.
	 *
	 * @return the list client participant afp
	 */
	public List<Participant> getListClientParticipantAFP() {
		return listClientParticipantAFP;
	}


	/**
	 * Sets the list client participant afp.
	 *
	 * @param listClientParticipantAFP the new list client participant afp
	 */
	public void setListClientParticipantAFP(
			List<Participant> listClientParticipantAFP) {
		this.listClientParticipantAFP = listClientParticipantAFP;
	}


	/**
	 * Gets the participant selected.
	 *
	 * @return the participant selected
	 */
	public Long getParticipantSelected() {
		return participantSelected;
	}

	/**
	 * Sets the participant selected.
	 *
	 * @param participantSelected the new participant selected
	 */
	public void setParticipantSelected(Long participantSelected) {
		this.participantSelected = participantSelected;
	}

	/**
	 * Gets the participant afp selected.
	 *
	 * @return the participant afp selected
	 */
	public Long getParticipantAFPSelected() {
		return participantAFPSelected;
	}


	/**
	 * Sets the participant afp selected.
	 *
	 * @param participantAFPSelected the new participant afp selected
	 */
	public void setParticipantAFPSelected(Long participantAFPSelected) {
		this.participantAFPSelected = participantAFPSelected;
	}
 

	/**
	 * Gets the model collection record to.
	 *
	 * @return the model collection record to
	 */
	public CollectionRecordTo getModelCollectionRecordTo() {
		return modelCollectionRecordTo;
	}

	/**
	 * Sets the model collection record to.
	 *
	 * @param modelCollectionRecordTo the new model collection record to
	 */
	public void setModelCollectionRecordTo(
			CollectionRecordTo modelCollectionRecordTo) {
		this.modelCollectionRecordTo = modelCollectionRecordTo;
	}

	/**
	 * Gets the disable holder ec visible.
	 *
	 * @return the disable holder ec visible
	 */
	public Boolean getDisableHolderECVisible() {
		return disableHolderECVisible;
	}

	/**
	 * Sets the disable holder ec visible.
	 *
	 * @param disableHolderECVisible the new disable holder ec visible
	 */
	public void setDisableHolderECVisible(Boolean disableHolderECVisible) {
		this.disableHolderECVisible = disableHolderECVisible;
	}

	/**
	 * Gets the disable issuer ec visible.
	 *
	 * @return the disable issuer ec visible
	 */
	public Boolean getDisableIssuerECVisible() {
		return disableIssuerECVisible;
	}

	/**
	 * Sets the disable issuer ec visible.
	 *
	 * @param disableIssuerECVisible the new disable issuer ec visible
	 */
	public void setDisableIssuerECVisible(Boolean disableIssuerECVisible) {
		this.disableIssuerECVisible = disableIssuerECVisible;
	}

	/**
	 * Gets the disable participant ec visible.
	 *
	 * @return the disable participant ec visible
	 */
	public Boolean getDisableParticipantECVisible() {
		return disableParticipantECVisible;
	}

	/**
	 * Sets the disable participant ec visible.
	 *
	 * @param disableParticipantECVisible the new disable participant ec visible
	 */
	public void setDisableParticipantECVisible(
			Boolean disableParticipantECVisible) {
		this.disableParticipantECVisible = disableParticipantECVisible;
	}

	/**
	 * Gets the disable participant afpec visible.
	 *
	 * @return the disable participant afpec visible
	 */
	public Boolean getDisableParticipantAFPECVisible() {
		return disableParticipantAFPECVisible;
	}


	/**
	 * Sets the disable participant afpec visible.
	 *
	 * @param disableParticipantAFPECVisible the new disable participant afpec visible
	 */
	public void setDisableParticipantAFPECVisible(
			Boolean disableParticipantAFPECVisible) {
		this.disableParticipantAFPECVisible = disableParticipantAFPECVisible;
	}


	/**
	 * Gets the disable other ec visible.
	 *
	 * @return the disable other ec visible
	 */
	public Boolean getDisableOtherECVisible() {
		return disableOtherECVisible;
	}

	/**
	 * Sets the disable other ec visible.
	 *
	 * @param disableOtherECVisible the new disable other ec visible
	 */
	public void setDisableOtherECVisible(Boolean disableOtherECVisible) {
		this.disableOtherECVisible = disableOtherECVisible;
	}

	/**
	 * Gets the billing processed to data model.
	 *
	 * @return the billing processed to data model
	 */
	public GenericDataModel<BillingProcessedTo> getBillingProcessedToDataModel() {
		return billingProcessedToDataModel;
	}

	/**
	 * Sets the billing processed to data model.
	 *
	 * @param billingProcessedToDataModel the new billing processed to data model
	 */
	public void setBillingProcessedToDataModel(
			GenericDataModel<BillingProcessedTo> billingProcessedToDataModel) {
		this.billingProcessedToDataModel = billingProcessedToDataModel;
	}

	/**
	 * Gets the list billing processed to.
	 *
	 * @return the list billing processed to
	 */
	public List<BillingProcessedTo> getListBillingProcessedTo() {
		return listBillingProcessedTo;
	}

	/**
	 * Sets the list billing processed to.
	 *
	 * @param listBillingProcessedTo the new list billing processed to
	 */
	public void setListBillingProcessedTo(
			List<BillingProcessedTo> listBillingProcessedTo) {
		this.listBillingProcessedTo = listBillingProcessedTo;
	}

	

	/**
	 * Gets the ind no integrated.
	 *
	 * @return the ind no integrated
	 */
	public Integer getIndNoIntegrated() {
		return indNoIntegrated;
	}

	/**
	 * Sets the ind no integrated.
	 *
	 * @param indNoIntegrated the new ind no integrated
	 */
	public void setIndNoIntegrated(Integer indNoIntegrated) {
		this.indNoIntegrated = indNoIntegrated;
	}

	/**
	 * Gets the ind integrated.
	 *
	 * @return the ind integrated
	 */
	public Integer getIndIntegrated() {
		return indIntegrated;
	}

	/**
	 * Sets the ind integrated.
	 *
	 * @param indIntegrated the new ind integrated
	 */
	public void setIndIntegrated(Integer indIntegrated) {
		this.indIntegrated = indIntegrated;
	}

	
	/**
	 * Gets the billing status preliminary.
	 *
	 * @return the billing status preliminary
	 */
	public Integer getBillingStatusPreliminary() {
		return billingStatusPreliminary;
	}


	/**
	 * Sets the billing status preliminary.
	 *
	 * @param billingStatusPreliminary the new billing status preliminary
	 */
	public void setBillingStatusPreliminary(Integer billingStatusPreliminary) {
		this.billingStatusPreliminary = billingStatusPreliminary;
	}


	/**
	 * Gets the billing status definitive.
	 *
	 * @return the billing status definitive
	 */
	public Integer getBillingStatusDefinitive() {
		return billingStatusDefinitive;
	}


	/**
	 * Sets the billing status definitive.
	 *
	 * @param billingStatusDefinitive the new billing status definitive
	 */
	public void setBillingStatusDefinitive(Integer billingStatusDefinitive) {
		this.billingStatusDefinitive = billingStatusDefinitive;
	}


	/**
	 * Gets the billing status error.
	 *
	 * @return the billing status error
	 */
	public Integer getBillingStatusError() {
		return billingStatusError;
	}


	/**
	 * Sets the billing status error.
	 *
	 * @param billingStatusError the new billing status error
	 */
	public void setBillingStatusError(Integer billingStatusError) {
		this.billingStatusError = billingStatusError;
	}


	/**
	 * Gets the billing processed to selected.
	 *
	 * @return the billing processed to selected
	 */
	public BillingProcessedTo getBillingProcessedToSelected() {
		return billingProcessedToSelected;
	}


	/**
	 * Sets the billing processed to selected.
	 *
	 * @param billingProcessedToSelected the new billing processed to selected
	 */
	public void setBillingProcessedToSelected(
			BillingProcessedTo billingProcessedToSelected) {
		this.billingProcessedToSelected = billingProcessedToSelected;
	}


	/**
	 * Gets the serie billing.
	 *
	 * @return the serie billing
	 */
	public String getSerieBilling() {
		return serieBilling;
	}


	/**
	 * Sets the serie billing.
	 *
	 * @param serieBilling the new serie billing
	 */
	public void setSerieBilling(String serieBilling) {
		this.serieBilling = serieBilling;
	}



	/**
	 * Gets the voucher billing.
	 *
	 * @return the voucher billing
	 */
	public String getVoucherBilling() {
		return voucherBilling;
	}


	/**
	 * Sets the voucher billing.
	 *
	 * @param voucherBilling the new voucher billing
	 */
	public void setVoucherBilling(String voucherBilling) {
		this.voucherBilling = voucherBilling;
	}


	/**
	 * Checks if is bool date becoming.
	 *
	 * @return the boolDateBecoming
	 */
	public boolean isBoolDateBecoming() {
		return boolDateBecoming;
	}


	/**
	 * Sets the bool date becoming.
	 *
	 * @param boolDateBecoming the boolDateBecoming to set
	 */
	public void setBoolDateBecoming(boolean boolDateBecoming) {
		this.boolDateBecoming = boolDateBecoming;
	}


	/**
	 * Gets the date becoming.
	 *
	 * @return the dateBecoming
	 */
	public Date getDateBecoming() {
		return dateBecoming;
	}


	/**
	 * Sets the date becoming.
	 *
	 * @param dateBecoming the dateBecoming to set
	 */
	public void setDateBecoming(Date dateBecoming) {
		this.dateBecoming = dateBecoming;
	}


	/**
	 * Gets the list serie billing.
	 *
	 * @return the list serie billing
	 */
	public List<ParameterTable> getListSerieBilling() {
		return listSerieBilling;
	}


	/**
	 * Sets the list serie billing.
	 *
	 * @param listSerieBilling the new list serie billing
	 */
	public void setListSerieBilling(List<ParameterTable> listSerieBilling) {
		this.listSerieBilling = listSerieBilling;
	}


	/**
	 * Gets the list voucher billing.
	 *
	 * @return the list voucher billing
	 */
	public List<ParameterTable> getListVoucherBilling() {
		return listVoucherBilling;
	}


	/**
	 * Sets the list voucher billing.
	 *
	 * @param listVoucherBilling the new list voucher billing
	 */
	public void setListVoucherBilling(List<ParameterTable> listVoucherBilling) {
		this.listVoucherBilling = listVoucherBilling;
	}







	/**
	 * Gets the lst document type.
	 *
	 * @return the lst document type
	 */
	public List<ParameterTable> getLstDocumentType() {
		return lstDocumentType;
	}


	/**
	 * Sets the lst document type.
	 *
	 * @param lstDocumentType the new lst document type
	 */
	public void setLstDocumentType(List<ParameterTable> lstDocumentType) {
		this.lstDocumentType = lstDocumentType;
	}


	/**
	 * Gets the visible poll.
	 *
	 * @return the visiblePoll
	 */
	public Boolean getVisiblePoll() {
		return visiblePoll;
	}


	/**
	 * Sets the visible poll.
	 *
	 * @param visiblePoll the visiblePoll to set
	 */
	public void setVisiblePoll(Boolean visiblePoll) {
		this.visiblePoll = visiblePoll;
	}


	/**
	 * Gets the list calculation process.
	 *
	 * @return the listCalculationProcess
	 */
	public List<BillingCalculationProcess> getListCalculationProcess() {
		return listCalculationProcess;
	}


	/**
	 * Sets the list calculation process.
	 *
	 * @param listCalculationProcess the listCalculationProcess to set
	 */
	public void setListCalculationProcess(
			List<BillingCalculationProcess> listCalculationProcess) {
		this.listCalculationProcess = listCalculationProcess;
	}


	/**
	 * Gets the calculation process data model.
	 *
	 * @return the calculationProcessDataModel
	 */
	public GenericDataModel<BillingCalculationProcess> getCalculationProcessDataModel() {
		return calculationProcessDataModel;
	}


	/**
	 * Sets the calculation process data model.
	 *
	 * @param calculationProcessDataModel the calculationProcessDataModel to set
	 */
	public void setCalculationProcessDataModel(
			GenericDataModel<BillingCalculationProcess> calculationProcessDataModel) {
		this.calculationProcessDataModel = calculationProcessDataModel;
	}


	/**
	 * Gets the calculation process selected.
	 *
	 * @return the calculationProcessSelected
	 */
	public BillingCalculationProcess getCalculationProcessSelected() {
		return calculationProcessSelected;
	}


	/**
	 * Sets the calculation process selected.
	 *
	 * @param calculationProcessSelected the calculationProcessSelected to set
	 */
	public void setCalculationProcessSelected(
			BillingCalculationProcess calculationProcessSelected) {
		this.calculationProcessSelected = calculationProcessSelected;
	}

	

	/**
	 * @return the lastBillingDate
	 */
	public Date getLastBillingDate() {
		return lastBillingDate;
	}


	/**
	 * @param lastBillingDate the lastBillingDate to set
	 */
	public void setLastBillingDate(Date lastBillingDate) {
		this.lastBillingDate = lastBillingDate;
	}


	/**
	 * Sets the up variables.
	 */
	public void setUpVariables() {

	}

	
	/**
	 * Load cmb data.
	 */
	public void loadCmbData() {

		log.debug(" cargando listas si no est\u00E1n en sesi\u00F3n :");
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		/** llenar la lista de periodo de cobro */
		if (Validations.validateListIsNullOrEmpty(listCollectionPeriod)) {
			log.debug("lista periodo de cobro :");
			parameterTableTO.setMasterTableFk(PropertiesConstants.COLLECTION_PERIOD_PK);
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO.setOrderbyParameterTableCd(GeneralConstants.ONE_VALUE_INTEGER);
			try {
				listCollectionPeriod = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}

		/** llenar la lista de entidad de cobro */
		if (Validations.validateListIsNullOrEmpty(listCollectionInstitution)) {
			log.debug("lista entidad de cobro :");
			parameterTableTO.setMasterTableFk(PropertiesConstants.COLLECTION_ENTITY_PK);
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			try {
				listCollectionInstitution = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
				
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		
		/**Llenar lista Serie de Facturacion*/
		if (Validations.validateListIsNullOrEmpty(listSerieBilling)) {
			log.debug("lista Serie de Facturacion :");
			parameterTableTO.setMasterTableFk(PropertiesConstants.SERIE_CLIENT_PK);
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			try {
				listSerieBilling = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		
		/**Llenar la lista de Tipo de Comprobante*/
		if (Validations.validateListIsNullOrEmpty(listVoucherBilling)) {
			log.debug("listaTipo de Comprobante :");
			parameterTableTO.setMasterTableFk(PropertiesConstants.TYPE_BOUCHER_PK);
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			try {
				listVoucherBilling = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
	}

	
	/**
	 * Load data preliminary.
	 */
	public void loadDataPreliminary() {

		loadCmbData();
		cleanBillingProcessPreliminary();
		getDocumentTypeCombo();
		
		
		
		try {
			getParticipantAFPListToCombo();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Gets the document type combo.
	 *
	 * @return the document type combo
	 */
	public void getDocumentTypeCombo() {
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
					
		//List the document types of Participant
		filterParameterTable.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
		try {
			lstDocumentType = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
		} catch (ServiceException e) {
			
			e.printStackTrace();
		}	
		
	}
	
	/**
	 *  Acciones con los paneles de Entidad de Cobro.
	 */
	public void updateComponentEntityCollection() {
		Integer entityCollection = this.collectionInstitutionSelected;
		if (Validations.validateIsNotNullAndPositive(entityCollection)) {
			if (entityCollection.equals(EntityCollectionType.ISSUERS.getCode())) {
				disableHolderECVisible = Boolean.FALSE;
				disableIssuerECVisible = Boolean.TRUE;
				disableParticipantECVisible = Boolean.FALSE;
				disableParticipantAFPECVisible=Boolean.FALSE;
				disableOtherECVisible = Boolean.FALSE;
			} else if (entityCollection.equals(EntityCollectionType.PARTICIPANTS.getCode() )) {
				disableHolderECVisible = Boolean.FALSE;
				disableIssuerECVisible = Boolean.FALSE;
				disableParticipantECVisible = Boolean.TRUE;
				disableParticipantAFPECVisible=Boolean.FALSE;
				disableOtherECVisible = Boolean.FALSE;
				verifyHolder();
			} else if (entityCollection.equals(EntityCollectionType.HOLDERS.getCode())) {
				disableHolderECVisible = Boolean.TRUE;
				disableIssuerECVisible = Boolean.FALSE;
				disableParticipantECVisible = Boolean.FALSE;
				disableParticipantAFPECVisible=Boolean.FALSE;
				disableOtherECVisible = Boolean.FALSE;
			} else if (entityCollection.equals(EntityCollectionType.OTHERS.getCode())) {
				disableHolderECVisible = Boolean.FALSE;
				disableIssuerECVisible = Boolean.FALSE;
				disableParticipantECVisible = Boolean.FALSE;
				disableParticipantAFPECVisible=Boolean.FALSE;
				disableOtherECVisible = Boolean.TRUE;
			}
			
			else if (entityCollection.equals(EntityCollectionType.MANAGER.getCode())
					||entityCollection.equals(EntityCollectionType.STOCK_EXCHANGE.getCode())
					||entityCollection.equals(EntityCollectionType.CENTRAL_BANK.getCode())
					||entityCollection.equals(EntityCollectionType.HACIENDA.getCode())
//					||entityCollection.equals(EntityCollectionType.REGULATOR_ENTITY.getCode())
					) {
				disableHolderECVisible = Boolean.FALSE;
				disableIssuerECVisible = Boolean.FALSE;
				disableParticipantECVisible = Boolean.FALSE;
				disableParticipantAFPECVisible=Boolean.FALSE;
				disableOtherECVisible = Boolean.FALSE;
			} else if (entityCollection.equals(EntityCollectionType.AFP.getCode())) {
				disableHolderECVisible = Boolean.FALSE;
				disableIssuerECVisible = Boolean.FALSE;
				disableParticipantECVisible = Boolean.FALSE;
				disableParticipantAFPECVisible=Boolean.TRUE;
				disableOtherECVisible = Boolean.FALSE;
			}
		} else {
			disableHolderECVisible = Boolean.FALSE;
			disableIssuerECVisible = Boolean.FALSE;
			disableParticipantECVisible = Boolean.FALSE;
			disableParticipantAFPECVisible=Boolean.FALSE;
			disableOtherECVisible = Boolean.FALSE;
		}
		clearEntitys();
		
		//Llamada a Ultima Fecha
		getLastBillingDateByParameters();
	}
	
	/**
	 * Clear entitys.
	 */
	public void clearEntitys(){
		this.participantSelected=GeneralConstants.NEGATIVE_ONE_LONG;
		this.issuer=new Issuer();
		this.holderSelected = new Holder();
		modelCollectionRecordTo.setOtherEntityDescription(GeneralConstants.EMPTY_STRING);
		modelCollectionRecordTo.setOtherEntityDocument(GeneralConstants.EMPTY_STRING);		

	}

	/**
	 * Show Component By Collection Period.
	 */
	public void updateComponentCollectionPeriod() {
	
		cleanBillingProcessedToDataModel();
		
		Integer collectionPeriod = this.collectionPeriodSelected;
		if (Validations.validateIsNotNullAndPositive(collectionPeriod)) {
			if (collectionPeriod.equals(PropertiesConstants.COLLECTION_PERIOD_DAILY_PK)) {
				
				hideAllPanelCollectionPeriod();
				disableDailyVisible = Boolean.TRUE;
				
			} else if (collectionPeriod.equals(PropertiesConstants.COLLECTION_PERIOD_WEEKLY_PK)) {
				
				hideAllPanelCollectionPeriod();
				disableWeeklyVisible = Boolean.TRUE;

			} else if (collectionPeriod.equals(PropertiesConstants.COLLECTION_PERIOD_MONTHLY_PK)) {
				
				hideAllPanelCollectionPeriod();
				disableMonthVisible = Boolean.TRUE;
				
			}  else if (collectionPeriod.equals(PropertiesConstants.COLLECTION_PERIOD_QUARTERLY_PK)) { 
				
				hideAllPanelCollectionPeriod();
				disableQuarterlyVisible=Boolean.TRUE;
				
			}else if (collectionPeriod.equals(PropertiesConstants.COLLECTION_PERIOD_YEARLY_PK)) {
				
				hideAllPanelCollectionPeriod();
				disableYearlyVisible = Boolean.TRUE;
			}
			
		} else {
			hideAllPanelCollectionPeriod();
		}
		
		resetDateBilling();
		
		//Llamada a Ultima Fecha
		getLastBillingDateByParameters();
	}
	
	/**
	 * Reset Calendar Of Date Initial and Final of Billing.
	 */
	public void resetDateBilling(){
		this.initialDate = CommonsUtilities.currentDate();
		this.finalDate = CommonsUtilities.currentDate();
	}
	
	/**
	 * Hide All Panel To Collection Period.
	 */
	public void hideAllPanelCollectionPeriod(){
		disableDailyVisible = Boolean.FALSE;
		disableWeeklyVisible = Boolean.FALSE;
		disableMonthVisible = Boolean.FALSE;
		disableQuarterlyVisible = Boolean.FALSE;
		disableYearlyVisible = Boolean.FALSE;
	}

	/**
	 * Show Helper BIlling Service if inIntegratedBillSelected is single and
	 * Hide if grouping.
	 */
	public void updateComponentIndIntegratedBill() {
		Integer inIntegratedBill = this.inIntegratedBillSelected;
		if (Validations.validateIsNotNull(inIntegratedBill)) {
			if (inIntegratedBill.equals(PropertiesConstants.YES_IND_INTEGRATED_BILL)) {
				disableHelperBillingVisible = Boolean.FALSE;
			} else if (inIntegratedBill.equals(PropertiesConstants.NOT_IND_INTEGRATED_BILL)) {
				disableHelperBillingVisible = Boolean.TRUE;
			}
		} else {
			disableHelperBillingVisible = Boolean.FALSE;
		}
		verifyHolder();
	}
	
	
	
	/**
	 * Gets the issuer helper.
	 *
	 * @param event the event
	 * @return the issuer helper
	 */
	public void getIssuerHelper(ActionEvent event) {
		if (Validations.validateIsNotNullAndNotEmpty(issuerHelperBean)) {
//			this.issuer = issuerHelperBean.getSelected();
		}
	}

	/**
	 *  Clean windows search.
	 */
	public void cleanBillingProcessSearch() {
		billingServiceSelection = new BillingService();
		modelCollectionRecordTo = new CollectionRecordTo();		
		
		collectionInstitutionSelected = -1;
		collectionPeriodSelected = -1;
		this.participantSelected = null;
		/** for the helpers */
		this.issuer = new Issuer();
		this.holderSelected = new Holder();
		inIntegratedBillSelected=null;
		resetDateBilling();
		periodDate=CommonsUtilities.currentDate();
		boolDateBecoming=Boolean.FALSE;
		disableHelperBillingVisible = Boolean.FALSE;

		hideAllPanelCollectionPeriod();
		
		appliedTaxDisable= Boolean.FALSE;
		
		/**Windows Search DataTable, Button and Panel*/
		billingProcessedToDataModel=null;
		billingProcessedToSelected=null;
		voucherBilling=GeneralConstants.EMPTY_STRING;
		serieBilling=GeneralConstants.EMPTY_STRING;
		
		/**Reset Component*/

		if(JSFUtilities.findViewComponent("fmrBillingProcess")!=null){
			JSFUtilities.resetComponent("fmrBillingProcess");
			JSFUtilities.resetComponent("fmrBillingProcess2");
		}
		
	}

	
	/**
	 *  Clean windows search.
	 */
	public void cleanBillingProcessPreliminary() {
		
		modelCollectionRecordTo = new CollectionRecordTo();
		this.clearEntitys();
		collectionInstitutionSelected = -1;
		collectionPeriodSelected = -1;
		billingServiceSelection = new BillingService();
//		inIntegratedBillSelected=null;
		resetDateBilling();
		periodDate=CommonsUtilities.currentDate();
		disableHolderECVisible = Boolean.FALSE;
		disableIssuerECVisible = Boolean.FALSE;
		disableParticipantECVisible = Boolean.FALSE;
		disableParticipantAFPECVisible=Boolean.FALSE;
		disableOtherECVisible = Boolean.FALSE;

//		disableHelperBillingVisible = Boolean.FALSE;

		hideAllPanelCollectionPeriod();
		
		appliedTaxDisable= Boolean.FALSE;
		
		/**
		 * Default Not Integrated
		 */
		inIntegratedBillSelected=PropertiesConstants.NOT_IND_INTEGRATED_BILL;
		disableHelperBillingVisible=Boolean.TRUE;
	}
	
	
	/**
	 * Disable participant or issuer.
	 *
	 * @return the boolean
	 */
	public Boolean disableParticipantOrIssuer() {
		Boolean entityCollec = Boolean.FALSE;

		if (userInfo.getUserAccountSession().isInstitution(
				InstitutionType.PARTICIPANT)
				|| userInfo.getUserAccountSession().isInstitution(
						InstitutionType.AFP)) {

			entityCollec = Boolean.TRUE;

		} else if (userInfo.getUserAccountSession().isInstitution(
				InstitutionType.ISSUER)) {

			entityCollec = Boolean.TRUE;

		}

		if (entityCollec) {

			return entityCollec;

		}
		return entityCollec;
	}

	/**
	 * Go to.
	 *
	 * @param viewToGo the view to go
	 * @return the string
	 */
	public String goTo(String viewToGo) {

		if (viewToGo.equals("edit")) {
			viewToGo = "preliminary";
		}
		else if(viewToGo.equals("searchBilling")){
			cleanBillingProcessSearch();
			viewToGo = "searchBilling";
		}
		return viewToGo;
	}

	/**
	 * Before on cancel.
	 */
	public void beforeOnCancel() {
		if(Validations.validateIsNotNull(billingProcessedToSelected)){
			if(billingProcessedToSelected.getProcessedState().equals(BillingStateType.PRELIMINAR.getCode())
					||billingProcessedToSelected.getProcessedState().equals(BillingStateType.ERROR.getCode())){
				
				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
						, PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_BILLING_PROCESS_ANNUL));
				JSFUtilities.executeJavascriptFunction("cnfwServiceCancel.show();");
				
			}
			else if(billingProcessedToSelected.getProcessedState().equals(BillingStateType.DEFINITIVO.getCode())){
				
				/**Show Message Confirm for executing action*/
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_DEFINITIVE_PROCESSED));
				JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
			}
		}
		else{
			showMessageOnDialog(PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.MODIFY_RATE_NO_SELECTED));
			JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
		}

	}
	
	/**
	 * Execute cancel processed.
	 */
	@LoggerAuditWeb
	public void executeCancelProcessed(){
		if(Validations.validateIsNotNull(billingProcessedToSelected)){
			BillingCalculationProcess billingCalculationProcess;
			billingCalculationProcess=billingProcessedServiceFacade.findBillingCalculationProcessById(billingProcessedToSelected.getIdBillingCalcProcessPk());
			billingProcessedServiceFacade.deteleBillingCalculationProcess(billingCalculationProcess);
			listBillingProcessedToExecuted.remove(billingProcessedToSelected);
			billingProcessedToDataModel=new GenericDataModel<BillingProcessedTo>(listBillingProcessedToExecuted);
			System.out.println();
		}
	}

	
	/**
	 * Hide dialogs from new.
	 */
	public void hideDialogsFromNew(){
		JSFUtilities.hideComponent("fmrBillingProcessPreliminary:cnfidbillingprocessedsave");
		JSFUtilities.hideGeneralDialogues();
	}
	
	/**
	 * Hide dialogs from search.
	 */
	public void hideDialogsFromSearch(){
		JSFUtilities.hideComponent("search_popups:cnfIdServiceCancel");
		JSFUtilities.hideComponent("search_popups:cnfIdServiceConfirm");
	}
	
	
	/**
	 * Before On Definitive Process.
	 */
	public void beforeOnProcessDefinitive() {
		
		
		if(Validations.validateIsNotNull(this.billingProcessedToSelected)){
			
			if(billingProcessedToSelected.getCountBillingDetail()==0){
				/**Show Message Confirm for executing action*/
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_DEFINITIVE_NO_EXIST));
				JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
			}
			else if(billingProcessedToSelected.getProcessedState().equals(BillingStateType.PRELIMINAR.getCode())){
				
				/**Show Message Confirm for executing action*/
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
						, PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_BILLING_PROCESS_CONFIRM));
				JSFUtilities.executeJavascriptFunction("PF('cnfwServiceConfirm').show();");
			}
			else if(billingProcessedToSelected.getProcessedState().equals(BillingStateType.ERROR.getCode())){
				
				/**Show Message Confirm for executing action*/
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_DEFINITIVE_ERROR));
				JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
			}
			else if(billingProcessedToSelected.getProcessedState().equals(BillingStateType.DEFINITIVO.getCode())){
				
				/**Show Message Confirm for executing action*/
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_DEFINITIVE_PROCESSED));
				JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
			}
		}
		else{
			showMessageOnDialog(PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.MODIFY_RATE_NO_SELECTED));
			JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
		}
		
		
	}
	
	
	/**
	 * Execute billing definitive process.
	 */
	@LoggerAuditWeb
	public void executeBillingDefinitiveProcess() {
		
		Long billingCalculationProcessId	=	this.billingProcessedToSelected.getIdBillingCalcProcessPk();
		billingProcessedToExecuted=SerializationUtils.clone(billingProcessedToSelected);
		
		BillingProcessedFilter billingProcessedFilter	= new BillingProcessedFilter();
		 
		if ( Validations.validateIsNotNull(billingCalculationProcessId)){
			
			try {
				if(boolDateBecoming){
					billingProcessedFilter.setBillingDateBecoming(dateBecoming);
				}else{
					billingProcessedFilter.setBillingDateBecoming(null);
				}
				
 				billingProcessedFilter.setProcessBillingCalculationPk(billingCalculationProcessId);
				
				billingProcessedServiceFacade.executeDefinitiveProcessBilling(billingProcessedFilter);
				
				/***
				 * Activate Poll
				 */
				visiblePoll=Boolean.TRUE;
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
						, PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_DEFINITIVE_PROCESS_EXECUTION));
				JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
									
			} catch (ServiceException e) {
				e.printStackTrace();
			}
			
		}

		
	}
	
	
	/**
	 * Before on process preliminary.
	 */
	public void beforeOnProcessPreliminary() {

		/**
		 * Validate whether there bills according to the parameters entered: If
		 * it is not yet billed , they ask if you want to continue.
		 ***/
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
				, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_PRELIMINARY_PROCESS_VALIDATION));
		JSFUtilities.executeJavascriptFunction("PF('cnfwBillingProcessedSave').show()");	
		
	}

	
	/**
	 * Execute billing preliminary process.
	 */
	@LoggerAuditWeb
	public void executeBillingPreliminaryProcess() {
		
		/** Preliminary processing the billing **/
		
		BillingProcessedFilter  billingProcessedFilter= new BillingProcessedFilter();
		
		/**Entity Collection*/
		if(Validations.validateIsNotNullAndPositive(collectionInstitutionSelected)){
		
				billingProcessedFilter.setEntityCollection(collectionInstitutionSelected);
				
				if(collectionInstitutionSelected.equals(EntityCollectionType.ISSUERS.getCode())){
					if(Validations.validateIsNotNull(issuer)){
							billingProcessedFilter.setIssuerPk(issuer.getIdIssuerPk());
					}
				}else if( collectionInstitutionSelected.equals(EntityCollectionType.PARTICIPANTS.getCode())){
					if(Validations.validateIsNotNullAndPositive(participantSelected)){
						billingProcessedFilter.setParticipantPk(participantSelected);
					}
					if(Validations.validateIsNotNullAndNotEmpty(holderSelected)){				
						billingProcessedFilter.setIdHolderByParticipant(holderSelected.getIdHolderPk());
					}
				}else if(collectionInstitutionSelected.equals(EntityCollectionType.AFP.getCode())){
					if(Validations.validateIsNotNullAndPositive(participantAFPSelected)){
						billingProcessedFilter.setParticipantPk(participantAFPSelected);
					}
				}
				else if(collectionInstitutionSelected.equals(EntityCollectionType.HOLDERS.getCode())){
					if(Validations.validateIsNotNull(holderSelected)){					
							billingProcessedFilter.setHolderPk(holderSelected.getIdHolderPk());						
					}
				}
				else if(collectionInstitutionSelected.equals(EntityCollectionType.OTHERS.getCode())){
					if(Validations.validateIsNotNull(modelCollectionRecordTo)){
						if(Validations.validateIsNotNullAndNotEmpty(modelCollectionRecordTo.getOtherEntityDocument())){
							billingProcessedFilter.setDocumentCodeOther(modelCollectionRecordTo.getOtherEntityDocument().trim().toUpperCase());
						}
						
 
						if(Validations.validateIsNotNullAndPositive(modelCollectionRecordTo.getDocumentType())){
							billingProcessedFilter.setDocumentTypeOther(modelCollectionRecordTo.getDocumentType());
						}
						
					}
				}else if(collectionInstitutionSelected.equals(EntityCollectionType.AFP.getCode())){
					if(Validations.validateIsNotNullAndPositive(participantSelected)){
						billingProcessedFilter.setParticipantPk(participantSelected);
					}					
				}
				
			
		}
		/**Collection Period*/
		if(Validations.validateIsNotNullAndPositive(collectionPeriodSelected)){
			billingProcessedFilter.setCollectionPeriod(collectionPeriodSelected);
			if(collectionPeriodSelected.equals(PropertiesConstants.COLLECTION_PERIOD_DAILY_PK)){
				billingProcessedFilter.setBillingDateStart(initialDate);
				billingProcessedFilter.setBillingDateEnd(initialDate);
				billingProcessedFilter.setBillingDate(initialDate);
			}
			else if(collectionPeriodSelected.equals(PropertiesConstants.COLLECTION_PERIOD_WEEKLY_PK)){/**Periodo de Facturacion Semanal*/
				billingProcessedFilter.setBillingDateStart(initialDate);
				billingProcessedFilter.setBillingDateEnd(finalDate);
			}
			else if(collectionPeriodSelected.equals(PropertiesConstants.COLLECTION_PERIOD_MONTHLY_PK)){/**Periodo de Facturacion Mensual*/
				billingProcessedFilter.setBillingDateStart(initialDate);
				billingProcessedFilter.setBillingDateEnd(finalDate);
			}
			else if(collectionPeriodSelected.equals(PropertiesConstants.COLLECTION_PERIOD_QUARTERLY_PK)){/**Periodo de Facturacion Trimestral*/
				billingProcessedFilter.setBillingDateStart(initialDate);
				billingProcessedFilter.setBillingDateEnd(finalDate);
			}
			else if(collectionPeriodSelected.equals(PropertiesConstants.COLLECTION_PERIOD_YEARLY_PK)){/**Facturacion Anual, A la fecha le quito 365 dias*/
				billingProcessedFilter.setBillingDateStart(CommonsUtilities.addDaysToDate(periodDate,-365));
				billingProcessedFilter.setBillingDateEnd(periodDate);
			}
			
		}
		
		if ( Validations.validateIsNotNull(this.inIntegratedBillSelected) && (this.inIntegratedBillSelected>=0)){
			
			billingProcessedFilter.setIndIntegratedBill(this.inIntegratedBillSelected); 
			
			/**Individual*/
			if ( this.inIntegratedBillSelected.equals(PropertiesConstants.NOT_IND_INTEGRATED_BILL)){
				
				if ( Validations.validateIsNotNull(this.billingServiceSelection)){
					billingProcessedFilter.setBillingServicePk(this.billingServiceSelection.getIdBillingServicePk());
				}
				
			}
		}
		
		
		try {
			
			billingProcessedServiceFacade.executePreliminaryProcessBilling(billingProcessedFilter);
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
					, PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_PRELIMINARY_PROCESS_VALIDATION));
			JSFUtilities.executeJavascriptFunction("PF('cnfSuccessDialog').show();");
								
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		
	}

	
	/**
	 * Metodo que valida antes de limpiar.
	 */
	public void validCleanFormSearch() {
//		removeAlert();
		if (Validations.validateIsNotNullAndPositive(collectionInstitutionSelected)
				|| Validations.validateIsNotNullAndPositive(collectionPeriodSelected)
				||Validations.validateIsNotNull(billingProcessedToDataModel)) {
			
			validationMessage = PropertiesUtilities.getMessage(
					PropertiesConstants.PROPERTYFILE, FacesContext
					.getCurrentInstance().getViewRoot().getLocale(),
					PropertiesConstants.CLEAN_BILLING_PROCESS);
			alertQuestion(validationMessage);// *
		} else {
			cleanFormSearch();
		}

	}
	


	/**
	 * ******** EXTRAS **********.
	 */

	public void executeAlertQuestionSearch() {

		cleanBillingProcessSearch();
	}
	
	/**
	 * Execute alert question preliminary.
	 */
	public void executeAlertQuestionPreliminary() {

		cleanBillingProcessPreliminary();
	}

	/**
	 * Clean form search.
	 */
	public void cleanFormSearch() {

		this.cleanBillingProcessSearch();
		

	}

	/**
	 * Load participant.
	 */
	private void loadParticipant() {
		try {
			listClientParticipant = new ArrayList<Participant>();
			
			Participant participant = new Participant();
			List<Integer> states = new ArrayList<Integer>();
			states.add(ParticipantStateType.REGISTERED.getCode());
			participant.setLstParticipantStates(states);
			listClientParticipant = helperComponentFacade.getLisParticipantServiceFacade(participant);
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}

	}
	
	/**
	 * Gets the participant afp list to combo.
	 *
	 * @return the participant afp list to combo
	 * @throws ServiceException the service exception
	 */
	public void getParticipantAFPListToCombo() throws ServiceException{
		if(Validations.validateListIsNullOrEmpty(listClientParticipantAFP)){
			Participant participantTemp=new Participant();
			participantTemp.setAccountClass(ParticipantClassType.AFP.getCode());
			participantTemp.setState(ParticipantStateType.REGISTERED.getCode());
			listClientParticipantAFP=helperComponentFacade.findParticipantNativeQueryFacade(participantTemp);
		}
	}


	/**
	 * Alert.
	 *
	 * @param message the message
	 */
	public void alert(String message) {
		validationMessage = PropertiesUtilities.getMessage(
				GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES, FacesContext
				.getCurrentInstance().getViewRoot().getLocale(),
				GeneralConstants.LBL_HEADER_ALERT_WARNING);

		showMessageOnDialog(validationMessage, message);
		JSFUtilities.executeJavascriptFunction("alterValidationWidget.show();");
	}

	/**
	 * Alert question.
	 *
	 * @param message the message
	 */
	public void alertQuestion(String message) {
		validationMessage = PropertiesUtilities.getMessage(
				GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES, FacesContext
				.getCurrentInstance().getViewRoot().getLocale(),
				GeneralConstants.LBL_HEADER_ALERT_WARNING);

		showMessageOnDialog(validationMessage, message);
		JSFUtilities.executeJavascriptFunction("confirmAlertWidget.show();");
	}

	
	/**
	 * Search billing.
	 */
	@LoggerAuditWeb
	public void searchBilling(){
		billingProcessedToSelected = null;
		BillingProcessedFilter billingProcessedFilter= new BillingProcessedFilter();
		List<BillingProcessedTo> billingProcessedToList=new ArrayList<BillingProcessedTo>();
		/**Entity Collection*/
		if(Validations.validateIsNotNullAndPositive(collectionInstitutionSelected)){
			billingProcessedFilter.setEntityCollection(collectionInstitutionSelected);
		}
		/**Collection Period*/
		if(Validations.validateIsNotNullAndPositive(collectionPeriodSelected)){
			billingProcessedFilter.setCollectionPeriod(collectionPeriodSelected);
			if(collectionPeriodSelected.equals(PropertiesConstants.COLLECTION_PERIOD_DAILY_PK)){
				billingProcessedFilter.setBillingDateStart(initialDate);
				billingProcessedFilter.setBillingDateEnd(initialDate);
			}
			
			else if(collectionPeriodSelected.equals(PropertiesConstants.COLLECTION_PERIOD_WEEKLY_PK)){
				billingProcessedFilter.setBillingDateStart(initialDate);
				billingProcessedFilter.setBillingDateEnd(finalDate);
			}
			
			else if(collectionPeriodSelected.equals(PropertiesConstants.COLLECTION_PERIOD_MONTHLY_PK)){
				billingProcessedFilter.setBillingDateStart(initialDate);
				billingProcessedFilter.setBillingDateEnd(finalDate);
			}
			
			else if(collectionPeriodSelected.equals(PropertiesConstants.COLLECTION_PERIOD_QUARTERLY_PK)){
				billingProcessedFilter.setBillingDateStart(initialDate);
				billingProcessedFilter.setBillingDateEnd(finalDate);
			}
			
			else if(collectionPeriodSelected.equals(PropertiesConstants.COLLECTION_PERIOD_YEARLY_PK)){
				billingProcessedFilter.setBillingDateStart(initialDate);
				billingProcessedFilter.setBillingDateEnd(finalDate);
			}
			
		}
		
		billingProcessedToList=billingProcessedServiceFacade.getBillingPreliminaryForProcessed(billingProcessedFilter);

		
		listBillingProcessedToExecuted=BillingUtils.cloneList(billingProcessedToList);
		dateBecoming=finalDate;
		billingProcessedToDataModel=new GenericDataModel<BillingProcessedTo>(billingProcessedToList);
		if(JSFUtilities.findViewComponent("fmrBillingProcess2")!=null){
			JSFUtilities.resetViewRoot();
		}
			
		voucherBilling=GeneralConstants.EMPTY_STRING;
		serieBilling=GeneralConstants.EMPTY_STRING;
		
		
		
	}
	
	/**
	 * Check billing.
	 *
	 * @param e the e
	 */
	public void checkBilling(SelectEvent e){
		
	}
	
	/**
	 * Method for logout the user session on idle time of the application.
	 */
	public void sessionLogout() {
		try {
			JSFUtilities.setHttpSessionAttribute(
					GeneralConstants.LOGOUT_MOTIVE,
					LogoutMotiveType.EXPIREDSESSION);
			JSFUtilities.killSession();
			JSFUtilities.showMessageOnDialog(null, null,
					PropertiesConstants.IDLE_TIME_EXPIRED, null);
			JSFUtilities.showComponent(":idCnfDialogKillSession");
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * *
	 * Refresh status process.
	 */
	public void updateProcessOnExecution(){
		
		log.debug(" REFRESH  ESTATUS PROCESS THAT ARE EXECUTING ");
		/** REFRESH  ESTATUS PROCESS THAT ARE EXECUTING **/
		
			if (Validations.validateIsNotNullAndNotEmpty(this.billingProcessedToExecuted)){
				BillingProcessedFilter billingProcessedFilter=new BillingProcessedFilter();
				billingProcessedFilter.setProcessBillingCalculationPk(billingProcessedToExecuted.getIdBillingCalcProcessPk());
				BillingProcessedTo processedServiceToRefresh= null;
				
				try {
					processedServiceToRefresh=billingProcessedServiceFacade.refreshStateBilling(billingProcessedFilter);			
					
				    if(Validations.validateIsNotNull(processedServiceToRefresh)){
				    	billingProcessedToExecuted.setCountBillingDetail(processedServiceToRefresh.getCountBillingDetail());
				    	billingProcessedToExecuted.setDescriptionProcessedState(processedServiceToRefresh.getDescriptionProcessedState());
				    	billingProcessedToExecuted.setProcessedState(processedServiceToRefresh.getProcessedState());
				    }
				    if(processedServiceToRefresh.getIdBillingCalcProcessPk().equals(billingProcessedToExecuted.getIdBillingCalcProcessPk())){
				    	if(processedServiceToRefresh.getProcessedState().equals(BillingStateType.ERROR.getCode())
				    			||billingProcessedToExecuted.getProcessedState().equals(BillingStateType.DEFINITIVO.getCode())){
				    		if(billingProcessedToSelected!=null){
				    			billingProcessedToSelected.setProcessedState(processedServiceToRefresh.getProcessedState());
					    		billingProcessedToSelected.setDescriptionProcessedState(processedServiceToRefresh.getDescriptionProcessedState());
					    		billingProcessedToSelected.setCountBillingDetail(processedServiceToRefresh.getCountBillingDetail());
					    		billingProcessedToExecuted=null;
				    		}
				    		
				    	}
				    }
			   }
				
				/**Agregar la forma de mandar que nunca entre a BD en un for setear NULL*/
			  catch (Exception e) {
				  e.printStackTrace();
				  
				  return; /** this exception is catch when the user clean the page **/
			 }
		 }else{
			 visiblePoll=Boolean.FALSE;
		 }

	}		
	

	/**
	 * Clean billing processed to data model.
	 */
	public void cleanBillingProcessedToDataModel(){
		
		if(Validations.validateIsNotNullAndNotEmpty(billingProcessedToDataModel) && billingProcessedToDataModel.getRowCount() > 0) {
			
			billingProcessedToDataModel = null ;
			
			
			if(Validations.validateListIsNotNullAndNotEmpty(listVoucherBilling)){

				voucherBilling = GeneralConstants.EMPTY_STRING ;
			}
			
			if(Validations.validateListIsNotNullAndNotEmpty(listSerieBilling)){
				
				serieBilling = GeneralConstants.EMPTY_STRING ;
			}
		}	
		
	}
	
	/**
	 * Check date becoming.
	 */
	public void checkDateBecoming(){
		
		log.debug("checkDateBecoming");
	}

	/**
	 * Search logger billing.
	 */
	@LoggerAuditWeb
	public void searchLoggerBilling(){
		
		BillingProcessedFilter billingProcessedFilter= new BillingProcessedFilter();
		billingProcessedFilter.setBillingDateStart(initialDate);
		billingProcessedFilter.setBillingDateEnd(finalDate);
		billingProcessedFilter.setProcessedState(BillingCalculationType.DEFINITIVO.getCode());
		
		listCalculationProcess=billingProcessedServiceFacade.findBillingCalculationProcess(billingProcessedFilter);
		calculationProcessDataModel=new GenericDataModel<>(listCalculationProcess);
	}
	

	
	/**
	 * Gets the streamed content.
	 *
	 * @param billingCalculationProcess the billing calculation process
	 * @return the streamed content
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@LoggerAuditWeb
	public StreamedContent getStreamedContent(BillingCalculationProcess billingCalculationProcess) throws IOException{
		byte[] fileContent = billingProcessedServiceFacade.getBillingCalculationProcessFileContent(billingCalculationProcess.getIdBillingCalcProcessPk());
		return getStreamedContentFromFile(fileContent, null, billingCalculationProcess.getNameFile());
	}
	
	/**
	 * Verify Billing Service
	 */
	public void verifyHolder(){
		if(Validations.validateIsNotNullAndNotEmpty(inIntegratedBillSelected) &&
				Validations.validateIsNotNullAndPositive(participantSelected) &&
				Validations.validateIsNotNullAndNotEmpty(collectionInstitutionSelected) &&
				Validations.validateIsNotNullAndNotEmpty(billingServiceSelection) &&
				inIntegratedBillSelected.equals(PropertiesConstants.NOT_IND_INTEGRATED_BILL) && 
				isValidService() &&
				collectionInstitutionSelected.equals(EntityCollectionType.PARTICIPANTS.getCode())
				){
			disableHolderECVisible = Boolean.TRUE;
		}else{
			disableHolderECVisible = Boolean.FALSE;
			holderSelected = new Holder();
		}
		
		//Llamada a Ultima Fecha
		getLastBillingDateByParameters();
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isValidService(){
		boolean flag = false;
		if(billingServiceSelection.getBaseCollection().equals(BaseCollectionType.RECORD_BOOK_ENTRY_PHYSICAL_CUSTODY_USD.getCode()) || 
				billingServiceSelection.getBaseCollection().equals(BaseCollectionType.RECORD_BOOK_ENTRY_PHYSICAL_CUSTODY_LOCAL.getCode()) || 
				billingServiceSelection.getBaseCollection().equals(BaseCollectionType.RECORD_BOOK_ENTRY_SECURITIES_FIXED_INCOME_AFP.getCode()) || 
				billingServiceSelection.getBaseCollection().equals(BaseCollectionType.RECORD_NOT_PLACED_SECURITIES_FIXED_INCOME.getCode()) || 
				billingServiceSelection.getBaseCollection().equals(BaseCollectionType.CUSTODY_SECURITIES_PHYSICAL.getCode()) || 
				billingServiceSelection.getBaseCollection().equals(BaseCollectionType.CUSTODY_SECURITIES_PHYSICAL_AFP.getCode()) || 
				billingServiceSelection.getBaseCollection().equals(BaseCollectionType.SETTLEMENT_SECURITIES_TRANSACTIONS_FIXED_INCOME.getCode()) || 
				billingServiceSelection.getBaseCollection().equals(BaseCollectionType.RECORD_BOOK_ENTRY_SECURITIES_VARIABLE_INCOME.getCode()) || 
				billingServiceSelection.getBaseCollection().equals(BaseCollectionType.RECORD_BOOK_ENTRY_SECURITIES_VARIABLE_INCOME_AFP.getCode()) || 
				billingServiceSelection.getBaseCollection().equals(BaseCollectionType.RECORD_NOT_PLACED_SECURITIES_VARIABLE_INCOME.getCode()) || 
				billingServiceSelection.getBaseCollection().equals(BaseCollectionType.SETTLEMENT_SECURITIES_TRANSACTIONS_VARIABLE_INCOME.getCode()) || 
				billingServiceSelection.getBaseCollection().equals(BaseCollectionType.RECORD_BOOK_ENTRY_DESMATERIALIZED_CUSTODY.getCode()) || 
				billingServiceSelection.getBaseCollection().equals(BaseCollectionType.CUSTODY_SECURITIES_PHYSICAL_PRIVATE_OFFERT_VARIABLE_INCOME.getCode())){
			flag = true;
		}
		return flag;
	}
	
	/**
	 * Getting Last Billing Date With Parameters:
	 * <br> Collection Period
	 * <br> Entity Collection
	 * <br> Billing Service
	 */
	public void getLastBillingDateByParameters(){
		BillingProcessedFilter billingProcessedFilter=new BillingProcessedFilter();
		disableLastBillingDate=Boolean.FALSE;
		//Valida que los 3 filtros esten 
		if(Validations.validateIsNotNullAndPositive(collectionPeriodSelected)&&
				Validations.validateIsNotNullAndPositive(collectionInstitutionSelected)&&
				Validations.validateIsNotNull(billingServiceSelection)&&
				Validations.validateIsNotNullAndPositive(billingServiceSelection.getIdBillingServicePk())){
			
			billingProcessedFilter.setCollectionPeriod(collectionPeriodSelected);
			billingProcessedFilter.setEntityCollection(collectionInstitutionSelected);
			billingProcessedFilter.setBillingServicePk(billingServiceSelection.getIdBillingServicePk());
			billingProcessedFilter.setProcessedState(BillingStateType.DEFINITIVO.getCode());
			
			if(EntityCollectionType.PARTICIPANTS.getCode().equals(collectionInstitutionSelected)&&
		    		Validations.validateIsNotNullAndPositive(participantSelected)){
				billingProcessedFilter.setParticipantPk(participantSelected);
								
			}else if(EntityCollectionType.HOLDERS.getCode().equals(collectionInstitutionSelected)&&
					Validations.validateIsNotNullAndPositive(holderSelected.getIdHolderPk())){
				billingProcessedFilter.setHolderPk(holderSelected.getIdHolderPk());

			}else if(EntityCollectionType.ISSUERS.getCode().equals(collectionInstitutionSelected)&&
					Validations.validateIsNotNullAndNotEmpty(issuer.getIdIssuerPk())){
				billingProcessedFilter.setIssuerPk(issuer.getIdIssuerPk());

			}else if(EntityCollectionType.OTHERS.getCode().equals(collectionInstitutionSelected)&&
					Validations.validateIsNotNullAndPositive(modelCollectionRecordTo.getDocumentType())&&
					Validations.validateIsNotNullAndNotEmpty(modelCollectionRecordTo.getOtherEntityDocument())){
				billingProcessedFilter.setDocumentTypeOther(modelCollectionRecordTo.getDocumentType());
				billingProcessedFilter.setDocumentCodeOther(modelCollectionRecordTo.getOtherEntityDocument());

			}
			lastBillingDate = billingProcessedServiceFacade.getLastBillingDate(billingProcessedFilter);
			if(lastBillingDate!=null){
				disableLastBillingDate=Boolean.TRUE;
			}
			
		}
		
	}
	@LoggerAuditWeb
	public void sendMonthlyProcess() {
		BillingProcessedFilter billingProcessedFilter= new BillingProcessedFilter();
		try {
			billingProcessedServiceFacade.executePreliminaryMonthlyProcessBilling(billingProcessedFilter);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}