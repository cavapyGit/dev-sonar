/*
 * 
 */
package com.pradera.billing.utils.view;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PropertiesConstants.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class PropertiesConstants {
	
	
	
	/**
	 * Instantiates a new properties constants.
	 */
	public PropertiesConstants() {
		super();
	}
	
	/**  Begin the constant generic. */
	public static final Integer CURRENCY_TYPE_PK = Integer.valueOf(53);
	
	/** The Constant SOURCE_INFORMATION_PK. */
	public static final Integer SOURCE_INFORMATION_PK = Integer.valueOf(133);
	
	/** The Constant PROPERTYFILE. */
	public static final String PROPERTYFILE = "Messages";
	
	/** The Constant CONSTANTS. */
	public static final String CONSTANTS = "Constants";
	
	/** The Constant CONNECTOR_UNION. */
	public static final String CONNECTOR_UNION = " UNION ";
	
	/** The Constant CONNECTOR_UNION_ALL. */
	public static final String CONNECTOR_UNION_ALL = " UNION ALL ";
	
	/**  End the constant generic. */
	
	/** Begin The constant Billing Service */
	
	public static final Integer SERVICE_TYPE_PK = Integer.valueOf(383);
	
	/** The Constant SERVICE_TYPE_AUTOMATIC_PK. */
	public static final Integer SERVICE_TYPE_AUTOMATIC_PK = Integer.valueOf(1431);
	
	/** The Constant SERVICE_TYPE_MANUAL_PK. */
	public static final Integer SERVICE_TYPE_MANUAL_PK = Integer.valueOf(1432);
	
	/** The Constant SERVICE_TYPE_EVENTUAL_PK. */
	public static final Integer SERVICE_TYPE_EVENTUAL_PK = Integer.valueOf(1433);
	
	/** The Constant SERVICE_STATUS_PK. */
	public static final Integer SERVICE_STATUS_PK = Integer.valueOf(172);	
	
	/** The Constant SERVICE_STATUS_REGISTER_PK. */
	public static final Integer SERVICE_STATUS_REGISTER_PK = Integer.valueOf(252);
	
	/** The Constant SERVICE_STATUS_ACTIVE_PK. */
	public static final Integer SERVICE_STATUS_ACTIVE_PK = Integer.valueOf(1546);
	
	/** The Constant SERVICE_STATUS_CANCELLED_PK. */
	public static final Integer SERVICE_STATUS_CANCELLED_PK = Integer.valueOf(1409);
	
	/** The Constant SERVICE_STATUS_EXPIRED_PK. */
	public static final Integer SERVICE_STATUS_EXPIRED_PK = Integer.valueOf(1411);
	
	/** The Constant SERVICE_STATUS_LOCK_PK. */
	public static final Integer SERVICE_STATUS_LOCK_PK = Integer.valueOf(1410);
	
	/** The Constant SERVICE_STATUS_UNLOCK_PK. */
	public static final Integer SERVICE_STATUS_UNLOCK_PK = Integer.valueOf(1);
	
	/** The Constant PERIOD_CALCULATION_PK. */
	public static final Integer PERIOD_CALCULATION_PK = Integer.valueOf(174);
	
	/** The Constant PERIOD_CALCULATION_DAILY_PK. */
	public static final Integer PERIOD_CALCULATION_DAILY_PK = Integer.valueOf(254);
	
	/** The Constant PERIOD_CALCULATION_MONTHLY_PK. */
	public static final Integer PERIOD_CALCULATION_MONTHLY_PK = Integer.valueOf(1425);
	
	/** The Constant PERIOD_CALCULATION_YEARLY_PK. */
	public static final Integer PERIOD_CALCULATION_YEARLY_PK = Integer.valueOf(1426);
	
	/** The Constant PERIOD_CALCULATION_EVENT_PK. */
	public static final Integer PERIOD_CALCULATION_EVENT_PK = Integer.valueOf(1427);
		
	/** The Constant COLLECTION_PERIOD_PK. */
	public static final Integer COLLECTION_PERIOD_PK = Integer.valueOf(175);
	
	/** The Constant COLLECTION_PERIOD_DAILY_PK. */
	public static final Integer COLLECTION_PERIOD_DAILY_PK = Integer.valueOf(255);//DIARIO
	
	/** The Constant COLLECTION_PERIOD_MONTHLY_PK. */
	public static final Integer COLLECTION_PERIOD_MONTHLY_PK = Integer.valueOf(1428);//MENSUAL
	
	/** The Constant COLLECTION_PERIOD_YEARLY_PK. */
	public static final Integer COLLECTION_PERIOD_YEARLY_PK = Integer.valueOf(1429);//ANUAL
	
	/** The Constant COLLECTION_PERIOD_EVENT_PK. */
	public static final Integer COLLECTION_PERIOD_EVENT_PK = Integer.valueOf(1430);//POR EVENTO
	
	/** The Constant COLLECTION_PERIOD_WEEKLY_PK. */
	public static final Integer COLLECTION_PERIOD_WEEKLY_PK = Integer.valueOf(2077);///SEMANAL
	
	/** The Constant COLLECTION_PERIOD_QUARTERLY_PK. */
	public static final Integer COLLECTION_PERIOD_QUARTERLY_PK = Integer.valueOf(2078);//TRIMESTRAL
	
	/** The Constant COLLECTION_TYPE_PK. */
	public static final Integer COLLECTION_TYPE_PK = Integer.valueOf(384);
	
	/** The Constant COLLECTION_TYPE_AUTOMATIC_DETRACTION_PK. */
	public static final Integer COLLECTION_TYPE_AUTOMATIC_DETRACTION_PK = Integer.valueOf(1434);
	
	/** The Constant COLLECTION_TYPE_PAID_COSTUMER_PK. */
	public static final Integer COLLECTION_TYPE_PAID_COSTUMER_PK = Integer.valueOf(1436);
	
	/** The Constant COLLECTION_BASE_PK. */
	public static final Integer COLLECTION_BASE_PK = Integer.valueOf(173);
	
	
	/** The Constant COLLECTION_ENTITY_PK. */
	public static final Integer COLLECTION_ENTITY_PK = Integer.valueOf(171);
	 
	
	
	
	
	/** The Constant SERVICE_CLIENT_PK. */
	public static final Integer SERVICE_CLIENT_PK = Integer.valueOf(403);
	
	/** The Constant SERVICE_CLIENT_ISSUER_PK. */
	public static final Integer SERVICE_CLIENT_ISSUER_PK = Integer.valueOf(1520);
	
	/** The Constant SERVICE_CLIENT_HOLDER_PK. */
	public static final Integer SERVICE_CLIENT_HOLDER_PK = Integer.valueOf(1522);
	
	/** The Constant SERVICE_CLIENT_PARTICIPANT_PK. */
	public static final Integer SERVICE_CLIENT_PARTICIPANT_PK = Integer.valueOf(1521);
	
	/** The Constant SERVICE_CLIENT_OTHER_PK. */
	public static final Integer SERVICE_CLIENT_OTHER_PK = Integer.valueOf(1523);
		
	/** The Constant CONFIRM_BILLING_SERVICE_SAVE. */
	public static final String CONFIRM_BILLING_SERVICE_SAVE="confirm.billing.service.save_confirm";
	public static final String CONFIRM_REQUEST_SERVICE_REGISTER="confirm.request.service.register";
	public static final String CONFIRM_REQUEST_SERVICE_BILLING="confirm.request.service.billing";
	public static final String CONFIRM_REQUEST_SERVICE_REJECT="confirm.request.service.reject";
	
	/** The Constant CONFIRM_BILLING_SERVICE_UPDATE. */
	public static final String CONFIRM_BILLING_SERVICE_UPDATE="confirm.billing.service.update_confirm";
	
	/** The Constant CONFIRM_BILLING_SERVICE_LOCK. */
	public static final String CONFIRM_BILLING_SERVICE_LOCK="confirm.billing.service.lock_confirm";
	
	/** The Constant CONFIRM_BILLING_SERVICE_UNLOCK. */
	public static final String CONFIRM_BILLING_SERVICE_UNLOCK="confirm.billing.service.unlock_confirm";
	
	/** The Constant CONFIRM_BILLING_SERVICE_CANCELLED. */
	public static final String CONFIRM_BILLING_SERVICE_CANCELLED="confirm.billing.service.cancelled_confirm";
	
	/** The Constant CONFIRM_BILLING_SERVICE_DELETED. */
	public static final String CONFIRM_BILLING_SERVICE_DELETED="confirm.billing.service.deleted_confirm";
	
	/** The Constant CONFIRM_BILLING_SERVICE_ACTIVED. */
	public static final String CONFIRM_BILLING_SERVICE_ACTIVED="confirm.billing.service.actived_confirm";
	
	/** The Constant SUCCESS_BILLING_SERVICE_SAVE. */
	public static final String SUCCESS_BILLING_SERVICE_SAVE="success.billing.service.save";
	
	/** The Constant SUCCESS_BILLING_SERVICE_SAVE_AND_RATES. */
	public static final String SUCCESS_BILLING_SERVICE_SAVE_AND_RATES="success.billing.service.save.and.rates";
	
	public static final String SUCCESS_BILLING_SERVICE_REQUEST_SAVE="success.billing.request.service.save";
	
	/** The Constant SUCCESS_BILLING_SERVICE_UPDATE. */
	public static final String SUCCESS_BILLING_SERVICE_UPDATE="success.billing.service.update";
	
	/** The Constant SUCCESS_BILLING_SERVICE_RATE_MODIFY. */
	public static final String SUCCESS_BILLING_SERVICE_RATE_MODIFY="success.billing.rate.modify";
	
	/** The Constant SUCCESS_BILLING_SERVICE_LOCK. */
	public static final String SUCCESS_BILLING_SERVICE_LOCK="success.billing.service.lock";
	
	/** The Constant SUCCESS_BILLING_SERVICE_UNLOCK. */
	public static final String SUCCESS_BILLING_SERVICE_UNLOCK="success.billing.service.unlock";
	
	/** The Constant SUCCESS_BILLING_SERVICE_CANCELLED. */
	public static final String SUCCESS_BILLING_SERVICE_CANCELLED="success.billing.service.cancelled";
	
	/** The Constant SUCCESS_BILLING_SERVICE_DELETED. */
	public static final String SUCCESS_BILLING_SERVICE_DELETED="success.billing.service.deleted";
	
	/** The Constant SUCCESS_BILLING_SERVICE_ACTIVE. */
	public static final String SUCCESS_BILLING_SERVICE_ACTIVE="success.billing.service.active";
	
	/** The Constant WARNING_BILLING_SERVICE_LOCK. */
	public static final String WARNING_BILLING_SERVICE_LOCK="warning.billing.service.lock";
	
	/** The Constant WARNING_BILLING_SERVICE_UNLOCK. */
	public static final String WARNING_BILLING_SERVICE_UNLOCK="warning.billing.service.unlock";
	
	/** The Constant WARNING_BILLING_SERVICE_CANCELLED. */
	public static final String WARNING_BILLING_SERVICE_CANCELLED="warning.billing.service.cancel";
	
	/** The Constant WARNING_BILLING_SERVICE_EXPIRED. */
	public static final String WARNING_BILLING_SERVICE_EXPIRED="warning.billing.service.expired";
	
	/** The Constant WARNING_BILLING_SERVICE_ACTIVED. */
	public static final String WARNING_BILLING_SERVICE_ACTIVED="warning.billing.service.actived";
	
	/** The Constant WARNING_BILLING_SERVICE_REGISTERED. */
	public static final String WARNING_BILLING_SERVICE_REGISTERED="warning.billing.service.registered";
	
	/** The Constant WARNING_BILLING_SERVICE_STATUS_ACTIVE. */
	public static final String WARNING_BILLING_SERVICE_STATUS_ACTIVE="warning.billing.service.status.active";	
	
	/** The Constant WARNING_BILLING_SERVICE_ENTITY_ADMIN. */
	public static final String WARNING_BILLING_SERVICE_ENTITY_ADMIN="warning.billing.service.entity.admin";
	
	/** The Constant WARNING_BILLING_SERVICE_ENTITY_EXCHANGE. */
	public static final String WARNING_BILLING_SERVICE_ENTITY_EXCHANGE="warning.billing.service.entity.exchange";
	
	/** The Constant WARNING_BILLING_SERVICE_NAME_LONG. */
	public static final String WARNING_BILLING_SERVICE_NAME_LONG="warning.billing.service.name.long";
	
	public static final String WARNING_BILLING_SERVICE_NAME_EMPTY="warning.billing.service.name.empty";
	
	public static final String WARNING_BILLING_SERVICE_REFERENCE_RATE_EMPTY="warning.billing.service.reference.rate.empty";
	
	public static final String WARNING_BILLING_SERVICE_ACCOUNTING_CODE_EMPTY="warning.billing.service.accounting.code.empty";
	
	/** The Constant WARNING_SERVICE_RATE_NAME_LONG. */
	public static final String WARNING_SERVICE_RATE_NAME_LONG="warning.service.rate.name.long";
	
	/** The Constant WARNING_BILLING_SERVICE_HAVE_RATE. */
	public static final String WARNING_BILLING_SERVICE_HAVE_RATE="warning.billing.service.have.service.rate";
	
	/** The Constant WARNING_BILLING_SERVICE_AFTER_DATE. */
	public static final String WARNING_BILLING_SERVICE_AFTER_DATE="warning.billing.service.date.after";
	
	public static final String WARNING_REQUEST_SERVICE_CLIENT="warning.request.service.client";
	public static final String WARNING_REQUEST_SERVICE="warning.request.service";
	public static final String WARNING_REQUEST_SERVICE_SCALE="warning.request.service.scale";
	
	/**  Messages for PROCESSED SERVICES *. */
	public static final String CONFIRM_PROCESSED_SERVICE_SAVE="confirm.processed.service.save_confirm";
	
	/** The Constant CONFIRM_PROCESSED_SERVICE_UPDATE. */
	public static final String CONFIRM_PROCESSED_SERVICE_UPDATE="confirm.processed.service.update_confirm";
	
	/** The Constant CONFIRM_PROCESSED_SERVICE_CONFIRM. */
	public static final String CONFIRM_PROCESSED_SERVICE_CONFIRM="confirm.processed.service.confirm_confirm";
	
	/** The Constant CONFIRM_PROCESSED_SERVICE_ANNUL. */
	public static final String CONFIRM_PROCESSED_SERVICE_ANNUL="confirm.processed.service.annul_confirm";
	
	/** The Constant CONFIRM_PROCESSED_SERVICE_SEND_CALCULATION. */
	public static final String CONFIRM_PROCESSED_SERVICE_SEND_CALCULATION="confirm.processed.service.send.calculation";
	
	/** The Constant SUCCESS_PROCESSED_SERVICE_SEND_CALCULATION. */
	public static final String SUCCESS_PROCESSED_SERVICE_SEND_CALCULATION="success.processed.service.send.calculation";
	
	
	/**  Messages for BILLING CALCULATION PROCESS *. */
	public static final String CONFIRM_BILLING_PROCESS_CONFIRM="confirm.billing.process.definitive";
	
	/** The Constant CONFIRM_BILLING_PROCESS_ANNUL. */
	public static final String CONFIRM_BILLING_PROCESS_ANNUL="confirm.billing.process.annul";
	
	/** The Constant CONFIRM_BILLING_PRELIMINARY_EXECUTE. */
	public static final String CONFIRM_BILLING_PRELIMINARY_EXECUTE="confirm.billing.preliminary.confirm";
	
	/** The Constant WARNING_BILLING_DEFINITIVE_PROCESSED. */
	public static final String WARNING_BILLING_DEFINITIVE_PROCESSED="warning.billing.definitive.processed";
	
	/** The Constant WARNING_BILLING_DEFINITIVE_ERROR. */
	public static final String WARNING_BILLING_DEFINITIVE_ERROR="warning.billing.definitive.error";
	
	/** The Constant WARNING_BILLING_DEFINITIVE_NO_EXIST. */
	public static final String WARNING_BILLING_DEFINITIVE_NO_EXIST="warning.billing.definitive.no.exist";
	
	
	
	/** Messages  for consult. */
	public static final String WARNING_CONSULT_CHANGE_DATA="warning.consult.change.data";
	
	/** The Constant WARNING_SEARCH_NO_RESULT. */
	public static final String WARNING_SEARCH_NO_RESULT="lbl.search.no.result";
	
	/** The Constant SUCCESS_PROCESSED_SERVICE_SAVE. */
	public static final String SUCCESS_PROCESSED_SERVICE_SAVE="success.processed.service.save";
	
	/** The Constant SUCCESS_PROCESSED_SERVICE_UPDATE. */
	public static final String SUCCESS_PROCESSED_SERVICE_UPDATE="success.processed.service.update";
	
	/** The Constant SUCCESS_PROCESSED_SERVICE_CONFIRM. */
	public static final String SUCCESS_PROCESSED_SERVICE_CONFIRM="success.processed.service.confirm";
	
	/** The Constant SUCCESS_PROCESSED_SERVICE_ANNUL. */
	public static final String SUCCESS_PROCESSED_SERVICE_ANNUL="success.processed.service.annul";
	public static final String SUCCESS_REQUEST_SERVICE_CONFIRM="success.request.service.confirm";
	public static final String SUCCESS_REQUEST_SERVICE_REJECT="success.request.service.reject";
	
	/** The Constant WARNING_PROCESSED_SERVICE_CONFIRM. */
	public static final String WARNING_PROCESSED_SERVICE_CONFIRM="warning.processed.service.confirm";
	public static final String WARNING_REQUEST_SERVICE_CONFIRM="warning.request.service.confirm";
	/** The Constant WARNING_PROCESSED_SERVICE_ANNUL. */
	public static final String WARNING_PROCESSED_SERVICE_ANNUL="warning.processed.service.annul";
	
	/** The Constant WARNING_PROCESSED_SERVICE_BILLED. */
	public static final String WARNING_PROCESSED_SERVICE_BILLED="warning.processed.service.billed";
	
	/** The Constant WARNING_PRELIMINARY_PROCESS_VALIDATION. */
	public static final String WARNING_PRELIMINARY_PROCESS_VALIDATION="warning.preliminary.process.validation";
	
	/** The Constant SUCCESS_PRELIMINARY_PROCESS_VALIDATION. */
	public static final String SUCCESS_PRELIMINARY_PROCESS_VALIDATION="success.preliminary.process.validation";
	
	/** The Constant SUCCESS_DEFINITIVE_PROCESS_EXECUTION. */
	public static final String SUCCESS_DEFINITIVE_PROCESS_EXECUTION="success.definitive.process.execution";
	
	
	/** The Constant DONT_EXIST_OTHER_DOCUMENT_NUMBER. */
	public static final String DONT_EXIST_OTHER_DOCUMENT_NUMBER = "billingService.alert.dontexist.documentnumber";
	
	/** The Constant SUCCESS_BILLING_PROCESS. */
	public static final String SUCCESS_BILLING_PROCESS = "billingService.alert.success.billingprocess";
	
	/** The Constant CLEAN_BILLING_PROCESS. */
	public static final String CLEAN_BILLING_PROCESS = "billingService.alert.cleanData";
	
	/** The Constant QUESTION_BILLING_PROCESS. */
	public static final String QUESTION_BILLING_PROCESS = "billingService.alert.question.billingprocess";
	
	/** The Constant DONT_IN_DOCUMENT_NUMBER. */
	public static final String DONT_IN_DOCUMENT_NUMBER = "billingService.alert.question.dontIn.documentnumber";
	
	/** The Constant ERROR_BILLING_PROCESS. */
	public static final String ERROR_BILLING_PROCESS = "billingService.alert.error.billingprocess";
	
	/** The Constant GENERAL_ALERT_INCOMPLETE_DATA. */
	public static final String GENERAL_ALERT_INCOMPLETE_DATA = "billingService.alert.dataIncomplete";


	/**  End the constant Billing Service. */
	
	
	
	
	
	/** Begin The Constant Billing Rate */
	
	public static final String SEARCH_MOVEMENT_TYPE="title";
	
	/** The Constant BODY_NOT_FOUND_MOVEMENT_TYPE. */
	public static final String BODY_NOT_FOUND_MOVEMENT_TYPE="MovementTypeHelpMessage";
	
	/**  Status for Rate Services. */
	public static final Integer RATE_STATUS= Integer.valueOf(391);
	
	/**  types of Rate Services. */
	public static final Integer RATE_TYPE= Integer.valueOf(392);   
	
	
	/** The Constant YES_APPLY_TAXED. */
	public static final Integer YES_APPLY_TAXED=Integer.valueOf(1);
	
	/** The Constant NOT_APPLY_TAXED. */
	public static final Integer NOT_APPLY_TAXED=Integer.valueOf(0);
	
	/** The Constant YES_VALUE_INTEGER. */
	public static final Integer YES_VALUE_INTEGER=Integer.valueOf(1);
	
	/** The Constant NOT_VALUE_INTEGER. */
	public static final Integer NOT_VALUE_INTEGER=Integer.valueOf(0);
	
	/** The Constant MINIMUM_BILLING. */
	public static final Integer MINIMUM_BILLING=Integer.valueOf(1);
	
	/** The Constant MINIMUM_CALCULATION. */
	public static final Integer MINIMUM_CALCULATION=Integer.valueOf(0);
	
	/** The Constant YES_BILLED. */
	public static final Integer YES_BILLED=Integer.valueOf(1);
	
	/** The Constant NOT_BILLED. */
	public static final Integer NOT_BILLED=Integer.valueOf(0);
	
	/** Agrupada. */
	public static final Integer YES_IND_INTEGRATED_BILL=Integer.valueOf(1);
	
	/** Individual. */
	public static final Integer NOT_IND_INTEGRATED_BILL=Integer.valueOf(0);
	
	/** The Constant RDB_ONE. */
	public static final Integer RDB_ONE=Integer.valueOf(1);
	
	/** The Constant RDB_ALL. */
	public static final Integer RDB_ALL=Integer.valueOf(0);
	 
	/** The Constant CONFIRM_SERVICE_RATE_SAVE. */
	public static final String CONFIRM_SERVICE_RATE_SAVE="confirm.billing.rate.save_confirm";
	
	/** The Constant CONFIRM_SERVICE_RATE_MODIFY. */
	public static final String CONFIRM_SERVICE_RATE_MODIFY="confirm.billing.rate.modify_confirm";
	
	/** The Constant SUCCESS_SERVICE_RATE_SAVE. */
	public static final String SUCCESS_SERVICE_RATE_SAVE="success.billing.rate.save"; 
	
	/** The Constant WARNING_TYPE_SERVICE_RATE_BACK. */
	public static final String WARNING_TYPE_SERVICE_RATE_BACK="warning.type.service.rate.back";
	
	/** The Constant CONFIRM_TYPE_SERVICE_RATE_ADD. */
	public static final String CONFIRM_TYPE_SERVICE_RATE_ADD="confirm.type.service.rate.type.add";
	
	/** The Constant CONFIRM_TYPE_NEW_SERVICE_RATE_ADD. */
	public static final String CONFIRM_TYPE_NEW_SERVICE_RATE_ADD="confirm.type.with.out.code.service.rate.type.add";
	
	/** The Constant WARNING_ROLL_BACK_COLLECTION_RECORD. */
	public static final String WARNING_ROLL_BACK_COLLECTION_RECORD="warning.roll.back.collection";
	
	/** The Constant WARNING_ROLL_BACK_BILLING_SERVICE_SEARCH. */
	public static final String WARNING_ROLL_BACK_BILLING_SERVICE_SEARCH="warning.roll.back.service.search";
	
	/** The Constant WARNING_ROLL_BACK_BILLING_SERVICE_REGISTER. */
	public static final String WARNING_ROLL_BACK_BILLING_SERVICE_REGISTER="warning.roll.back.service.register";
	
	/** The Constant MODIFY_RATE_NO_SELECTED. */
	public static final String MODIFY_RATE_NO_SELECTED="confirm.type.service.rate.no.selected";
	
	/** The Constant MODIFY_RATE_EMPTY. */
	public static final String MODIFY_RATE_EMPTY="confirm.type.service.rate.empty";
	
	/** The Constant TEMP_SCALE_ERROR. */
	public static final String TEMP_SCALE_ERROR="message.scale.error"; 

	/** The Constant TEMP_SCALE_ERROR. */
	public static final String TEMP_MIN_SCALE_ERROR="message.min.scale.error"; 
	
	/** The Constant EXCESSIVE_SPREAD. */
	public static final String EXCESSIVE_SPREAD="message.scale.excesive"; 
	
	/** The Constant EXCESSIVE_MOVEMENT. */
	public static final String EXCESSIVE_MOVEMENT="message.movement.excesive";
	
	/** The Constant ALERT_SCALE_MIN_REQUIRED. */
	public static final String ALERT_SCALE_MIN_REQUIRED="alert.message.scale.min.required"; 
	
	/** The Constant ALERT_SCALE_MAX_REQUIRED. */
	public static final String ALERT_SCALE_MAX_REQUIRED="alert.message.scale.max.required"; 
	
	/** The Constant WARNING_EMPTY_SERVICE_RATE. */
	public static final String WARNING_EMPTY_SERVICE_RATE="warning.empty.service.rate";
	
	/** The Constant WARNING_DOUBLE_MOVEMENT_CODE. */
	public static final String WARNING_DOUBLE_MOVEMENT_CODE="warning.double.movement.code";
	
	/** The Constant WARNING_EMPTY_SERVICE_MODIFY_RATE. */
	public static final String WARNING_EMPTY_SERVICE_MODIFY_RATE="warning.empty.service.modify.rate";
	
	/** The Constant WARNING_ENTER_LIMIT_MAX. */
	public static final String WARNING_ENTER_LIMIT_MAX="warning.enter.limit.max";
	
	/** The Constant WARNING_ENTER_LIMIT_MIN. */
	public static final String WARNING_ENTER_LIMIT_MIN="warning.enter.limit.min";
	
	/** The Constant WARNING_ENTER_AMOUNT. */
	public static final String WARNING_ENTER_AMOUNT="warning.enter.amount";
	
	/** The Constant MINIMUN_LIMIT. */
	public static final String MINIMUN_LIMIT= "minimum.limit";
	
	/** The Constant WARNING_EMPTY_MOVEMENT_LIST. */
	public static final String WARNING_EMPTY_MOVEMENT_LIST="warning.empty.movement.list";
	
	/** The Constant CONFIRM_SERVICE_RATE_DELETE. */
	public static final String CONFIRM_SERVICE_RATE_DELETE="confirm.billing.rate.delete_confirm";
	
	/** The Constant MODIFY_RATE_ADD_SERVICE. */
	public static final String MODIFY_RATE_ADD_SERVICE ="confirm.type.service.rate.no.add.services";
	
	/** The Constant WARNING_EMPTY_AMOUNT_MINIMUM. */
	public static final String WARNING_EMPTY_AMOUNT_MINIMUM ="warning.empty.amount.minimum";
	
	/** The Constant WARNING_EMPTY_CURRENCY_AMOUNT_MINIMUM. */
	public static final String WARNING_EMPTY_CURRENCY_AMOUNT_MINIMUM ="warning.empty.currency.amount.minimum";
	
	
	/**  End The Constant Billing Rate. */
	
	
	/** Begin The Constant Processed Service */
	public static final Integer PROC_SERV_STATUS_PK= Integer.valueOf(402);
	
	/**  End The Constant Processed Service. */
	
	/** Begin The Constant Billing PROCESSED*/
	public static final Integer BILLING_PROCESSED_STATUS_PK= Integer.valueOf(444);
	
	/**  End The Constant Processed Service. */
	
	/** Begin The Constant Billing Serie*/
	public static final Integer SERIE_CLIENT_PK = Integer.valueOf(458);
	
	/**  Begin The Constant Billing Type Voucher. */
	public static final Integer TYPE_BOUCHER_PK = Integer.valueOf(459);
	
	/** The Constant LONG_SEQUENCE. */
	public static final Integer LONG_SEQUENCE	= Integer.valueOf(8);
	
	/**  Constant Module type. */
	public static final Integer MODULE_TYPE= Integer.valueOf(215);
	
	/** The Constant MOVEMENT_TYPE_PROPERTIES. */
	public static final String  MOVEMENT_TYPE_PROPERTIES="MovementType";
	
	
	/** Begin The Constant Billing Collection. */
	public static final Integer TAX_PK= Integer.valueOf(1593);
	
	/** The Constant WARNING_CALCULATION_SERVICE_EXECUTING. */
	public static final String  WARNING_CALCULATION_SERVICE_EXECUTING="warning.calculation.service.executing";
	
	/** The Constant WARNING_CALCULATION_SERVICE_NOT_SELECTION. */
	public static final String  WARNING_CALCULATION_SERVICE_NOT_SELECTION="warning.calculation.no.selection";
	
	/** The Constant WARNING_CALCULATION_SERVICE_SELECTED_ERROR. */
	public static final String  WARNING_CALCULATION_SERVICE_SELECTED_ERROR="warning.calculation.service.selected.error";
	
	/** The Constant WARNING_CALCULATION_SERVICE_SELECTED_BILLING. */
	public static final String  WARNING_CALCULATION_SERVICE_SELECTED_BILLING="warning.calculation.service.selected.billing";
	
	/** Fix Processed Calculation. */
	public static final String  WARNING_CALCULATION_SERVICE_NOT_FIX="warning.calculation.service.not.fix";
	
	/** The Constant CONFIRM_CALCULATION_SERVICE_FIX_CALCULATION. */
	public static final String  CONFIRM_CALCULATION_SERVICE_FIX_CALCULATION="confirm.processed.service.fix.calculation";
	
	/** The Constant SUCCESS_PROCESSED_SERVICE_FIX_CALCULATION. */
	public static final String  SUCCESS_PROCESSED_SERVICE_FIX_CALCULATION="success.processed.service.fix.calculation";
	 
	/** End The Constant Billing Collection. */
	 
	
	/** types of Exceptions*/

	public static final Integer RULER_TYPE_PK = Integer.valueOf(408);
		
	/** The Constant EXCEPTION_PK. */
	public static final Integer EXCEPTION_PK = Integer.valueOf(443);	
	
	/** The Constant INCLUSION_PK. */
	public static final Integer INCLUSION_PK = Integer.valueOf(1651);
	
	/** The Constant EXCLUSION_PK. */
	public static final Integer EXCLUSION_PK = Integer.valueOf(1652);
	
	/**  EXCEPTION STATE. */
	public static final Integer EXCEPTION_STATUS_PK = Integer.valueOf(455);
	
	/** EXCEPTION INVOICE STATE*. */
	public static final Integer EXCEPTION_INVOICE_STATUS_PK = Integer.valueOf(540);
	
	
	/** The Constant CONFIRM_ADD_EXCEPTION. */
	public static final String CONFIRM_ADD_EXCEPTION="confirm.type.exception.add";
	
	/** The Constant CONFIRM_SAVE_EXCEPTION. */
	public static final String CONFIRM_SAVE_EXCEPTION="confirm.rate.save.exception";
	
	/** The Constant WARNING_EMPTY_EXCEPTION. */
	public static final String WARNING_EMPTY_EXCEPTION="warning.empty.exception";
	
	/** The Constant WARNING_NO_EXCEPTION_DELETE. */
	public static final String WARNING_NO_EXCEPTION_DELETE="warning.no.exception.delete";
	
	/** The Constant SUCCESS_EXCEPTION_SAVE. */
	public static final String SUCCESS_EXCEPTION_SAVE="success.exception.save";
	
	/** The Constant SUCCESS_EXCEPTION_SAVE_CONTINUE. */
	public static final String SUCCESS_EXCEPTION_SAVE_CONTINUE="warning.data.for.save.continue"; 
	
	/** The Constant WARNING_DATA_SAVE. */
	public static final String WARNING_DATA_SAVE="warning.data.for.save";
	
	/** The Constant WARNING_DATA_CLEAN. */
	public static final String WARNING_DATA_CLEAN="warning.on.clean.exception";	
	
	/** The Constant CONFIRM_DELETE_EXCEPTION. */
	public static final String CONFIRM_DELETE_EXCEPTION="confirm.rate.delete.exception";
	
	/** The Constant SUCCESS_EXCEPTION_DELETE. */
	public static final String SUCCESS_EXCEPTION_DELETE="success.exception.delete";
	
	/** The Constant WARNING_COMPLETE_DATA_EXCEPTION. */
	public static final String WARNING_COMPLETE_DATA_EXCEPTION="warning.complete.data.exception";
	
	/** The Constant WARNING_BACK_EXCEPTION_SEARCH. */
	public static final String WARNING_BACK_EXCEPTION_SEARCH="warning.back.search.excepction";
	
	/** The Constant WARNING_BACK_EXCEPTION_SEARCH_DELETE. */
	public static final String WARNING_BACK_EXCEPTION_SEARCH_DELETE="warning.back.search.excepction.delete";
	
	/** The Constant WARNING_RATE_EXCEPTION_DELETED. */
	public static final String WARNING_RATE_EXCEPTION_DELETED="warning.exception.deleted";
	
	/**  The Constant for displaying the session expired message on Idle time. */
	public static final String IDLE_TIME_EXPIRED = "idle.session.expired";
	
	/** The Invoice Exception*. */
	public static final String SUCCES_INVOICE_EXCEPTION_SAVE = "success.invoice.exception.save";
	
	/** The Constant WARNING_EXCLUSION_REQUIRED. */
	public static final String WARNING_EXCLUSION_REQUIRED = "warning.exclusion.required";
	
	/** The Constant WARNING_TIPE_EXCEPTION_REQUIRED. */
	public static final String WARNING_TIPE_EXCEPTION_REQUIRED = "warning.ind.type.exception";
	
	/** The Constant WARNING_BILLING_SERVICE_REQUIRED. */
	public static final String WARNING_BILLING_SERVICE_REQUIRED = "warning.billing.service.required";
	
	/** The Constant WARNING_INVOICE_EXCEPTION_REQUIRED. */
	public static final String WARNING_INVOICE_EXCEPTION_REQUIRED = "warning.exception.invoice.required";
	
	/** The Constant WARNING_INDICATOR_ONE_ALL_REQUIRED. */
	public static final String WARNING_INDICATOR_ONE_ALL_REQUIRED = "warning.ind.select.one.all";
	
	/** The Constant WARNING_INCLUSION_REPEAT. */
	public static final String WARNING_INCLUSION_REPEAT= "warning.inclusion.repeat";
	
	/** The Constant WARNING_ENTITY_ALREADY_LIST. */
	public static final String WARNING_ENTITY_ALREADY_LIST= "warning.entity.already.the.list";
	
	/** The Constant WARNING_BILLING_SERVICE_ALREADY_LIST. */
	public static final String WARNING_BILLING_SERVICE_ALREADY_LIST= "warning.billing.service.already.the.list";
	
	/** The Constant WARNING_SAME_ENTITY_BY_SERVICE. */
	public static final String WARNING_SAME_ENTITY_BY_SERVICE= "warning.billing.service.clustered.same.entity";
	
	/** The Constant WARNING_BILLING_SERVICE_AT_LEAST_ONE. */
	public static final String WARNING_BILLING_SERVICE_AT_LEAST_ONE= "warning.billing.service.at.least.one";
	
	/** The Constant CONFIRM_DELETE_EXCEPTION_INVOICE. */
	public static final String CONFIRM_DELETE_EXCEPTION_INVOICE= "confirm.delete.exception.invoice";
	
	
	/** The Constant LBL_HEADER_ALERT_WARNING. GENERAL COMMONS */ 
	public static final String LBL_HEADER_ALERT_WARNING = "messages.alert";
	
	/** The Constant MESSAGE_NOTIFICATION_SENDER_BILLING. */
	public static final String MESSAGE_NOTIFICATION_SENDER_BILLING="message.notification.sender.billing";
	
	
	/**  EntityType *. */
	public static final String FROM_ISSUER = "FROM ISSUER";
	
	/** The Constant FROM_PARTICIPANT. */
	public static final String FROM_PARTICIPANT = "FROM PARTICIPANT";

	/** Begin Billing Source*. */
	public static final String WARNING_SOURCE_DELETED ="warning.source.deleted";
	
	/** The Constant WARNING_SOURCE_NO_SELECTED. */
	public static final String WARNING_SOURCE_NO_SELECTED ="warning.source.not.select";
	
	/** The Constant WARNING_BACK_SOURCE. */
	public static final String WARNING_BACK_SOURCE="warning.back.source"; 
	
	/** The Constant SUCCESS_DELETED_SOURCE. */
	public static final String SUCCESS_DELETED_SOURCE ="success.deleted.source";
	
	/** The Constant CONFIRM_DELETED_SOURCE. */
	public static final String CONFIRM_DELETED_SOURCE ="confirm.deleted.source";  

	/** The Constant WARNING_SAVE_SOURCE. */
	public static final String WARNING_SAVE_SOURCE ="warning.save.source";
	
	/** The Constant CONFIRM_SAVE_SOURCE. */
	public static final String CONFIRM_SAVE_SOURCE ="confirm.save.source";
	
	/** The Constant CONFIRM_MODIFY_SOURCE. */
	public static final String CONFIRM_MODIFY_SOURCE ="confirm.modify.source";
	
	/** The Constant CONFIRM_CLEAN_SOURCE. */
	public static final String CONFIRM_CLEAN_SOURCE ="confirm.clean.source";
	
	/** The Constant CONFIRM_BACK_SOURCE. */
	public static final String CONFIRM_BACK_SOURCE ="confirm.back.source";
	
	/** The Constant SUCCESS_SAVE_SOURCE. */
	public static final String SUCCESS_SAVE_SOURCE ="success.save.source";
	
	/** The Constant CONFIRM_MODIFY_PARAMETER. */
	public static final String CONFIRM_MODIFY_PARAMETER ="confirm.modify.parameter";
	
	/** The Constant SUCCESS_MODIFY_PARAMETER. */
	public static final String SUCCESS_MODIFY_PARAMETER ="success.modify.parameter";
	
	/** Exception Invoice. */
	public static final String CONFIRM_EXCP_INVOICE_ADD="confirm.exception.invoice.add";
	
	/** Parametros de estructura del archivo*. */
	public static final String BILLING_SERVICE_SOURCE_INFO="billingSourceInfo";
	
	/** The Constant BILLING_SERVICE_SOURCE_FIELD. */
	public static final String BILLING_SERVICE_SOURCE_FIELD="field";
	
	/** The Constant BILLING_SERVICE_SOURCE_VISIBILITY. */
	public static final String BILLING_SERVICE_SOURCE_VISIBILITY="visibility";
	
	/** The Constant BILLING_SERVICE_SOURCE_TYPE. */
	public static final String BILLING_SERVICE_SOURCE_TYPE="type";
	
	/** The Constant BILLING_SERVICE_SOURCE_POSICION. */
	public static final String BILLING_SERVICE_SOURCE_POSICION="posicion";
	
	/** The Constant BILLING_SERVICE_SOURCE_LEVEL. */
	public static final String BILLING_SERVICE_SOURCE_LEVEL="level";
	
	/** The Constant BILLING_SERVICE_SOURCE_VALUE. */
	public static final String BILLING_SERVICE_SOURCE_VALUE="value";
	
	/** The Constant BILLING_SERVICE_SOURCE_QUERY. */
	public static final String BILLING_SERVICE_SOURCE_QUERY="query";
	
	/** The Constant BILLING_SERVICE_SOURCE_DATE. */
	public static final String BILLING_SERVICE_SOURCE_DATE="date";  
	
	/** The Constant BILLING_SERVICE_SOURCE_NAME. */
	public static final String BILLING_SERVICE_SOURCE_NAME="name";
	
	/** The Constant BILLING_SERVICE_SOURCE_DESCRIPTION. */
	public static final String BILLING_SERVICE_SOURCE_DESCRIPTION="description";
	
	/**  Fin Parametros de estructura del archivo*. */
	
	/**Tipo de errores*/
	public static final String BILLING_SERVICE_SOURCE_WRONG_STRUCTURED ="billing.service.source.wrong.structured";
	
	/** The Constant BILLING_SERVICE_SOURCE_VALUE_FORMAT. */
	public static final String BILLING_SERVICE_SOURCE_VALUE_FORMAT ="billing.service.source.value.format";
	
	/** The Constant BILLING_SERVICE_SOURCE_VISIBILITY_FORMAT. */
	public static final String BILLING_SERVICE_SOURCE_VISIBILITY_FORMAT ="billing.service.source.visibility.format";
	
	/** The Constant BILLING_SERVICE_SOURCE_POSITION_FORMAT. */
	public static final String BILLING_SERVICE_SOURCE_POSITION_FORMAT ="billing.service.source.position.format";
	
	/** The Constant BILLING_SERVICE_SOURCE_LEVEL_FORMAT. */
	public static final String BILLING_SERVICE_SOURCE_LEVEL_FORMAT ="billing.service.source.level.format";
	
	/** The Constant BILLING_SERVICE_SOURCE_TYPE_FORMAT. */
	public static final String BILLING_SERVICE_SOURCE_TYPE_FORMAT ="billing.service.source.type.format";
	/**Fin Tipo de errores*/
	
	/** update change the status to start charging for rates 5 and 6 ALERT_UPDATE_STATUS_START_CHARGING_RATES. */
	public static final String ALERT_UPDATE_STATUS_START_CHARGING_RATES ="alert.update.status.start.charging.rates";
	
	/**End Billing Source**/
	
	
	


	
	/** The tag cxc. */
	public static String TAG_CXC="CXC";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_START="SOAP-ENV:Envelope";
	
	public static String TAG_SOAP_START_CONTENT=" xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ied='http://schemas.datacontract.org/2004/07/IEDVServicios' xmlns:tem='http://tempuri.org/'";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_HEADER="SOAP-ENV:Header";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_BODY="SOAP-ENV:Body";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_ServFactura="tem:ServFactura";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_facturaC="tem:facturaC";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_IED_FACTURAC="ied:FacturaC";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_CLIENTE="ied:CLIENTE";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_CLIENTE_FACTURAR="ied:CLIENTE_FACTURAR";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_CUENTA="ied:CUENTA";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_DESCRIPCION="ied:DESCRIPCION";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_Detalle="ied:Detalle";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_FacturaD="ied:FacturaD";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_CANTIDAD="ied:CANTIDAD";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_ITEM="ied:ITEM";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_PRECIO="ied:PRECIO";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_FECOBRO="ied:FECOBRO";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_IMPUESTO="ied:IMPUESTO";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_MONEDA="ied:MONEDA";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_NUMCXC="ied:NUMCXC";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_NUMFACT="ied:NUMFACT";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_REF_AUX="ied:REF_AUX";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_TIPOSERVICIO="ied:TIPOSERVICIO";
	
	/** The Billing Service Invoice Tag. */
	public static String TAG_SOAP_TOKKEN="ied:TOKKEN";
}
