package com.pradera.billing.common.facade;


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.inject.Inject;

import com.pradera.billing.billingcollection.facade.CollectionServiceMgmtServiceFacade;
import com.pradera.billing.billingrate.to.ServiceRateTo;
import com.pradera.billing.billingservice.facade.BillingServiceMgmtServiceFacade;
import com.pradera.billing.billingservice.to.BillingServiceTo;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.services.remote.billing.BillingConexionService;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.billing.BillingService;
 


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ConexionRemoteServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Remote(BillingConexionService.class)
public class ConexionRemoteServiceFacade implements BillingConexionService{
	
 
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	
	/** The collection service mgmt service facade. */
	@EJB
	CollectionServiceMgmtServiceFacade collectionServiceMgmtServiceFacade;
	
	/** The billing service mgmt service facade. */
	@EJB
	BillingServiceMgmtServiceFacade	billingServiceMgmtServiceFacade;
	
	/** The logger. */
	@Inject
	private PraderaLogger logger;
	
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.services.remote.billing.BillingConexionService#findBillingServiceRemote(java.util.Map, com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public Map   findBillingServiceRemote(Map parameters , LoggerUser loggerUser) throws ServiceException{
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
		Date calculationDate = null;
		
		BillingServiceTo billingServiceTo = new BillingServiceTo();
		String baseCollectionId	=	(String) parameters.get(GeneralConstants.BASE_COLLECTION);		
		billingServiceTo.setBaseCollection(Integer.parseInt(baseCollectionId));
		
		if(Validations.validateIsNotNull(parameters.get(GeneralConstants.CALCULATION_DATE))){
			calculationDate	= CommonsUtilities.convertStringtoDate(parameters.get(GeneralConstants.CALCULATION_DATE).toString());
		}
		
		ServiceRateTo serviceRateTo = new ServiceRateTo();
		serviceRateTo.setLastEffectiveDate(calculationDate);
		billingServiceTo.setServiceRateToSelected(serviceRateTo);
		
		List<BillingService> billingServiceList= billingServiceMgmtServiceFacade.findRemoteBillingService(billingServiceTo);
		
		
		Map parameter	= new HashMap<String,Object>();
		parameter.put(GeneralConstants.BILLING_SERVICE_LIST, billingServiceList);
				
		return parameter;
		
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.services.remote.billing.BillingConexionService#saveRemoteProcessedService(java.util.Map, com.pradera.integration.contextholder.LoggerUser)
	 */
	//Remote Method
	@Override
	public void  saveRemoteProcessedService(Map parameters , LoggerUser loggerUser) throws ServiceException{
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
		
		try {
			collectionServiceMgmtServiceFacade.saveRemoteProcessedService(parameters);
		} catch (ServiceException e) {
			logger.error(":::::::::::::::::::ConexionRemote:saveRemoteProcessedService ::::::::::::::::");
			logger.error("Error ");
			e.printStackTrace();
			throw e;
		}
	}
	

	
}
