package com.pradera.billing.common.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.billing.billing.to.BillingProcessedFilter;
import com.pradera.billing.billingcollection.to.CollectionRecordTo;
import com.pradera.billing.billingcollection.to.DailyExchangeRateFilter;
import com.pradera.billing.billingexception.to.IssuerTo;
import com.pradera.billing.billingservice.service.BillingServiceMgmtServiceBean;
import com.pradera.billing.common.utils.BillingUtils;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.billing.service.BillingComponentService;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.billing.BillingCalculation;
import com.pradera.model.billing.BillingCorrelative;
import com.pradera.model.billing.BillingEconomicActivity;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.CollectionRecord;
import com.pradera.model.billing.type.EntityCollectionType;
import com.pradera.model.component.MovementType;
import com.pradera.model.generalparameter.DailyExchangeRates;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BusinessLayerCommonServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
public class BusinessLayerCommonServiceBean extends CrudDaoServiceBean {

	
	/** The log. */
	@Inject
	private transient PraderaLogger log;
	
	@EJB
	private BillingComponentService billingComponentService;
	

	/** The billing service mgmt service bean. */
	@EJB
	private BillingServiceMgmtServiceBean billingServiceMgmtServiceBean;
	
	/**
	 * Find movement types service bean.
	 *
	 * @param movementType the movement type
	 * @return the list
	 */
	public List<Object[]>  findMovementTypesServiceBean(MovementType movementType){
		log.info("consulta de tipos de movimientos ");
		StringBuilder sb = new StringBuilder();
		sb.append(" select    " +
				"	mt.id_movement_type_pk,		" +
				"	mt.movement_name,				" +
				"	(select description	from parameter_table	" +
				"	where parameter_table_pk =mt.module_type )	" +
				"	from movement_type mt		" +	
				"	where 1=1 ");
		if(Validations.validateIsNotNullAndPositive(movementType.getIdMovementTypePk())){
			 log.info("filtro 1" + movementType.getIdMovementTypePk() );
			  sb.append(" AND mt.id_movement_type_pk = :id_movement_type_pk ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(movementType.getMovementName())){
			 log.info("filtro 2" + movementType.getMovementName() );
			  sb.append(" AND upper(mt.movement_name) like :movement_name ");
		}
		if(Validations.validateIsNotNullAndPositive(movementType.getModuleType())){
			log.info("filtro 3" + movementType.getModuleType() );
			  sb.append(" AND mt.module_type = :module_type ");
		}
			
		Query query = em.createNativeQuery(sb.toString());
		
		if(Validations.validateIsNotNullAndPositive(movementType.getIdMovementTypePk())){
			query.setParameter("id_movement_type_pk", movementType.getIdMovementTypePk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(movementType.getMovementName())){
			query.setParameter("movement_name","%"+movementType.getMovementName().trim().toUpperCase()+"%");
		}
		if(Validations.validateIsNotNullAndPositive(movementType.getModuleType())){
			query.setParameter("module_type",movementType.getModuleType());
		}
	
		List<Object[]> searchResult = (List<Object[]>)query.getResultList();
		log.info("Resultado consulta de movimientos "+searchResult.size());
		
	   return searchResult;				
	}
	
	/**
	 * Find movement type By code.
	 *
	 * @param idMovementTypePk the id movement type pk
	 * @return the movement type
	 */
	public MovementType findMovementTypeByCode(Long idMovementTypePk){
		MovementType movementType=null;
		
		try {
			StringBuilder sb = new StringBuilder();
			sb.append(" select mt from MovementType mt where mt.idMovementTypePk="+idMovementTypePk);
			Query query = em.createQuery(sb.toString());
			movementType = (MovementType)query.getSingleResult();
		}catch(NoResultException nex) {
			log.error("No results found for  code " + idMovementTypePk);
			movementType = null;
		}
		
		return movementType;
	}

	public Map<String,DailyExchangeRates> searchAndMapExchangeMoneyTypeRandomListService(DailyExchangeRateFilter dailyExchangerate) throws ServiceException {
		List<DailyExchangeRates> bdRes = this.searchExchangeMoneyTypeRandomListService(dailyExchangerate);
		
		Map<String, DailyExchangeRates> mRes = new HashMap<String, DailyExchangeRates>();
		
		if(Validations.validateIsNotNull(bdRes)) {
			mRes = bdRes.stream().collect(Collectors.toMap(bdEx -> CommonsUtilities.convertDatetoString(bdEx.getDateRate()) , bdEx -> bdEx));
		}
		
		return mRes;		
	}
	
	
	/**
	 * Proceso que devuelve la fecha de tipo de cambio mas reciente
	 * 
	 * @param mapExchRates: mapa obtenido en funcion searchAndMapExchangeMoneyTypeRandomListService
	 * @param searchDate: fecha de busqueda
	 * @param modifyMap: flag que indica si se va actualizando el mapa para optimizar el proceso
	 * @return
	 */
	public DailyExchangeRates getExchRateFromMap(Map<String,DailyExchangeRates> mapExchRates, Date searchDate, Boolean modifyMap) {
		String searchKey = CommonsUtilities.convertDatetoString(searchDate);
		Integer size = mapExchRates.size();
		
		if(mapExchRates.containsKey(searchKey)) {
			return mapExchRates.get(searchKey);
		} else {
			for(int i=1; i<=size; i++) {
				String newSearchKey = CommonsUtilities.convertDatetoString(CommonsUtilities.addDate(searchDate, -i));
				if(mapExchRates.containsKey(newSearchKey)) {
					DailyExchangeRates res = mapExchRates.get(newSearchKey);
					if(modifyMap) {
						mapExchRates.put(searchKey, res);
					}
					return res;
				} 
			}
		}
		
		return null;
	}
	
	/**
	 * To search ChangeTypeSession in DailyExchangerates .
	 *
	 * @param dailyExchangerate the daily exchangerate
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<DailyExchangeRates> searchExchangeMoneyTypeRandomListService(DailyExchangeRateFilter dailyExchangerate)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		List<DailyExchangeRates> dailyExchangeList = null;
		sbQuery.append(" select d From DailyExchangeRates d  Where");
		sbQuery.append(" d.idSourceinformation = :source ");
		if (Validations.validateIsNotNullAndNotEmpty(dailyExchangerate.getIdCurrency())
				&& dailyExchangerate.getIdCurrency() > 0) {
		sbQuery.append("and d.idCurrency = :currencyType");
		}
		if (Validations.validateIsNotNullAndNotEmpty(dailyExchangerate.getDateRate())) {
		sbQuery.append(" and d.dateRate = :creationDate");
		}
		if(Validations.validateIsNotNullAndNotEmpty(dailyExchangerate.getInitialDate())){
			sbQuery.append(" And d.dateRate >= :initialDate");
		}
		if(Validations.validateIsNotNullAndNotEmpty(dailyExchangerate.getFinalDate())){
			sbQuery.append(" And d.dateRate <= :finalDate");
		}
		if(Validations.validateIsNotNullAndNotEmpty(dailyExchangerate.getYear())){
			sbQuery.append(" And YEAR(d.dateRate) = :year");
		}
		if(Validations.validateIsNotNullAndNotEmpty(dailyExchangerate.getMonth())){
			sbQuery.append(" And MONTH(d.dateRate) = :month");
		}
		sbQuery.append(" Order By d.iddailyExchangepk");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("source",dailyExchangerate.getIdSourceinformation());	
		if (Validations.validateIsNotNullAndNotEmpty(dailyExchangerate.getIdCurrency())
				&& dailyExchangerate.getIdCurrency() > 0) {
		query.setParameter("currencyType",dailyExchangerate.getIdCurrency());
		}
		if (Validations.validateIsNotNullAndNotEmpty(dailyExchangerate.getDateRate())) {
		query.setParameter("creationDate",dailyExchangerate.getDateRate());
		}
		if(Validations.validateIsNotNullAndNotEmpty(dailyExchangerate.getInitialDate())){
			query.setParameter(GeneralConstants.FIELD_INITIAL_DATE, dailyExchangerate.getInitialDate());
		}
		if(Validations.validateIsNotNullAndNotEmpty(dailyExchangerate.getFinalDate())){
			query.setParameter(GeneralConstants.FIELD_FINAL_DATE, dailyExchangerate.getFinalDate());
		}
		if(Validations.validateIsNotNullAndNotEmpty(dailyExchangerate.getYear())){
			query.setParameter("year", dailyExchangerate.getYear());
		}
		if(Validations.validateIsNotNullAndNotEmpty(dailyExchangerate.getMonth())){
			query.setParameter("month", dailyExchangerate.getMonth());
		}
		
		dailyExchangeList = query.getResultList();
		
		return dailyExchangeList;
	}
	
	
	
	/**
	 * Get Reference Price.
	 *
	 * @param dailyExchangeRateFilter the daily exchange rate filter
	 * @return the ting reference price
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public BigDecimal gettingReferencePrice(DailyExchangeRateFilter dailyExchangeRateFilter) throws ServiceException{
		
		BigDecimal referencePrice = BigDecimal.ZERO; 
		
					if(Validations.validateIsNotNull(dailyExchangeRateFilter.getInitialDate()) &&
					 Validations.validateIsNotNull(dailyExchangeRateFilter.getFinalDate()) ){
	
					 DailyExchangeRates dailyExchangeRates = null; 
					 List<DailyExchangeRates> listDailyExchangeRates = searchExchangeMoneyTypeRandomListService(dailyExchangeRateFilter);								 
					 
					 if (Validations.validateListIsNotNullAndNotEmpty(listDailyExchangeRates)){
						 dailyExchangeRates = listDailyExchangeRates.get(0);						
						 referencePrice	= new  BigDecimal(dailyExchangeRates.getReferencePrice().toString());
					 }
					 
					 else {
						 throw new ServiceException(ErrorServiceType.BILLING_PROCESS_EXCHANGE_RATE);
					 }
					 
				 }else if (Validations.validateIsNull(dailyExchangeRateFilter.getInitialDate()) ||
					Validations.validateIsNull(dailyExchangeRateFilter.getFinalDate())){
					 
					 throw new ServiceException(ErrorServiceType.BILLING_PROCESS_EXCHANGE_RATE);
					 
				 }
		 
		 return referencePrice;
	}
	
	/**
	 * Get Buy Price.
	 *
	 * @param dailyExchangeRateFilter the daily exchange rate filter
	 * @return the ting buy price
	 * @throws ServiceException the service exception
	 */
	public BigDecimal gettingBuyPrice(DailyExchangeRateFilter dailyExchangeRateFilter) throws ServiceException{
		
		BigDecimal referencePrice = BigDecimal.ZERO; 
		
					if(Validations.validateIsNotNull(dailyExchangeRateFilter.getInitialDate()) &&
					 Validations.validateIsNotNull(dailyExchangeRateFilter.getFinalDate()) ){
	
					 DailyExchangeRates dailyExchangeRates = null; 
					 List<DailyExchangeRates> listDailyExchangeRates = searchExchangeMoneyTypeRandomListService(dailyExchangeRateFilter);								 
					 
					 if (Validations.validateListIsNotNullAndNotEmpty(listDailyExchangeRates)){
						 dailyExchangeRates = listDailyExchangeRates.get(0);						
						 referencePrice	= new  BigDecimal(dailyExchangeRates.getBuyPrice().toString());
					 }
					 
					 else {
						 throw new ServiceException(ErrorServiceType.BILLING_PROCESS_EXCHANGE_RATE);
					 }
					 
				 }else if (Validations.validateIsNull(dailyExchangeRateFilter.getInitialDate()) ||
					Validations.validateIsNull(dailyExchangeRateFilter.getFinalDate())){
					 
					 throw new ServiceException(ErrorServiceType.BILLING_PROCESS_EXCHANGE_RATE);
					 
				 }
		 
		 return referencePrice;
	}
	
	/**
	 * Get Buy Price.
	 *
	 * @param dailyExchangeRateFilter the daily exchange rate filter
	 * @return the ting sell price
	 * @throws ServiceException the service exception
	 */
	public BigDecimal gettingSellPrice(DailyExchangeRateFilter dailyExchangeRateFilter) throws ServiceException {
		BigDecimal referencePrice = BigDecimal.ZERO;
		if (Validations.validateIsNotNull(dailyExchangeRateFilter.getInitialDate())
				&& Validations.validateIsNotNull(dailyExchangeRateFilter.getFinalDate())) {
			DailyExchangeRates dailyExchangeRates = null;
			List<DailyExchangeRates> listDailyExchangeRates = searchExchangeMoneyTypeRandomListService(dailyExchangeRateFilter);

			if (Validations.validateListIsNotNullAndNotEmpty(listDailyExchangeRates)) {
				dailyExchangeRates = listDailyExchangeRates.get(0);
				referencePrice = new BigDecimal(dailyExchangeRates.getSellPrice().toString());
			} else {
				throw new ServiceException(ErrorServiceType.BILLING_PROCESS_EXCHANGE_RATE);
			}
		} else if (Validations.validateIsNull(dailyExchangeRateFilter.getInitialDate())
				|| Validations.validateIsNull(dailyExchangeRateFilter.getFinalDate())) {
			throw new ServiceException(ErrorServiceType.BILLING_PROCESS_EXCHANGE_RATE);
		}
		
		return referencePrice;
	}
	
	/**
	 * Gets the last sequence generator code.
	 *
	 * @param billingCorrelativeFilter the billing correlative filter
	 * @return the last sequence generator code
	 * @throws ServiceException the service exception
	 */
	public  BillingCorrelative    getLastSequenceGeneratorCode(BillingCorrelative billingCorrelativeFilter) throws ServiceException{

		StringBuilder stringBuilderSql = new StringBuilder();	
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		BillingCorrelative	billingCorrelative = null;
		
		stringBuilderSql.append("SELECT bc FROM  BillingCorrelative bc  ");		
					
		if (Validations.validateIsNotNullAndNotEmpty(billingCorrelativeFilter.getCorrelativeCD())){
		 
			stringBuilderSql.append(" WHERE  bc.correlativeCD = :correlativeCD  ");
			parameters.put("correlativeCD", billingCorrelativeFilter.getCorrelativeCD());
		}else{
			
			throw new ServiceException(ErrorServiceType.BILLING_SERVICE_CODE_ERROR);
		}
			
		
		/** ONLY FOR BILLS ***/
		if (Validations.validateIsNotNullAndNotEmpty(billingCorrelativeFilter.getBillingTCF()) &&
			Validations.validateIsNotNullAndNotEmpty(billingCorrelativeFilter.getBillingSerie())	){
			 
			stringBuilderSql.append(" and bc.billingSerie = :billingSerie  ");
			stringBuilderSql.append(" and bc.billingTCF = :billingTCF  ");
			parameters.put("billingSerie", billingCorrelativeFilter.getBillingSerie());
			parameters.put("billingTCF", billingCorrelativeFilter.getBillingTCF());
		}
		
		try{
			billingCorrelative = (BillingCorrelative) findObjectByQueryString( stringBuilderSql.toString(), parameters);
		} catch(Exception  ex){
			
			if ( ex instanceof NoResultException){
				
				return billingCorrelative;
			}else{
			
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_CODE_ERROR);
			}
			
		}
		
		return billingCorrelative;
	}
	
	/**
	 * Gets the key entity collection.
	 *
	 * @param collectionRecord the collection record
	 * @param indIntegratedBill the ind integrated bill
	 * @param billingProcessedFilter the billing processed filter
	 * @return key of Collection Record : Holder ,
	 * Participant , Issuer or Others
	 */
	public String getKeyEntityCollection ( CollectionRecord collectionRecord , Integer indIntegratedBill , BillingProcessedFilter billingProcessedFilter){
		
		String keyEntity	=	"";  
		
								/*** 
								 *   	  type entity | code entity | currency type | indicator integrated | collection period | collection date end
								 * 
								 * ***/
		String suffix		=	collectionRecord.getCurrencyType().toString().concat(BillingUtils.VERTICAL).concat(indIntegratedBill.toString()).
								concat(BillingUtils.VERTICAL).concat(billingProcessedFilter.getCollectionPeriod().toString()).concat(BillingUtils.VERTICAL).
								concat(CommonsUtilities.convertDatetoString(billingProcessedFilter.getBillingDateEnd()));
		
		
		if(collectionRecord.getEntityCollectionId().equals(EntityCollectionType.HOLDERS.getCode())){
			keyEntity = EntityCollectionType.HOLDERS.getCode().toString().concat(BillingUtils.VERTICAL).concat(collectionRecord.getHolder().getIdHolderPk().toString())
						.concat(BillingUtils.VERTICAL).concat(suffix);
			
		}else if(collectionRecord.getEntityCollectionId().equals(EntityCollectionType.ISSUERS.getCode())){
			keyEntity = EntityCollectionType.ISSUERS.getCode().toString().concat(BillingUtils.VERTICAL).concat(collectionRecord.getIssuer().getIdIssuerPk())
						.concat(BillingUtils.VERTICAL).concat(suffix);
			
		}else if(collectionRecord.getEntityCollectionId().equals(EntityCollectionType.PARTICIPANTS.getCode())){
			keyEntity = EntityCollectionType.PARTICIPANTS.getCode().toString().concat(BillingUtils.VERTICAL).concat(collectionRecord.getParticipant().getIdParticipantPk().toString())
						.concat(BillingUtils.VERTICAL).concat(suffix);
			
		}else if(collectionRecord.getEntityCollectionId().equals(EntityCollectionType.OTHERS.getCode())){
			keyEntity = EntityCollectionType.OTHERS.getCode().toString().concat(BillingUtils.VERTICAL).concat(collectionRecord.getDocumentType().toString()).
					    concat(BillingUtils.VERTICAL).concat(collectionRecord.getOtherEntityDocument().trim().concat(BillingUtils.VERTICAL).concat(suffix));
			
		}else if(collectionRecord.getEntityCollectionId().equals(EntityCollectionType.CENTRAL_BANK.getCode())){
			keyEntity = EntityCollectionType.CENTRAL_BANK.getCode().toString().concat(BillingUtils.VERTICAL).concat(suffix);
		
		}else if(collectionRecord.getEntityCollectionId().equals(EntityCollectionType.STOCK_EXCHANGE.getCode())){
			keyEntity = EntityCollectionType.STOCK_EXCHANGE.getCode().toString().concat(BillingUtils.VERTICAL).concat(suffix);
		
		}else if(collectionRecord.getEntityCollectionId().equals(EntityCollectionType.HACIENDA.getCode())){
			keyEntity = EntityCollectionType.HACIENDA.getCode().toString().concat(BillingUtils.VERTICAL).concat(suffix);
		
		}else if(collectionRecord.getEntityCollectionId().equals(EntityCollectionType.AFP.getCode())){
			keyEntity = EntityCollectionType.PARTICIPANTS.getCode().toString().concat(BillingUtils.VERTICAL).concat(collectionRecord.getParticipant().getIdParticipantPk().toString())
					.concat(BillingUtils.VERTICAL).concat(suffix);		
			
		}else if(collectionRecord.getEntityCollectionId().equals(EntityCollectionType.REGULATOR_ENTITY.getCode())){
			keyEntity = EntityCollectionType.REGULATOR_ENTITY.getCode().toString().concat(BillingUtils.VERTICAL).concat(suffix);	
		}
		
		
		/**
		 * Change Only Not Integrated
		 */
		if(PropertiesConstants.NOT_IND_INTEGRATED_BILL.equals(indIntegratedBill)){
			keyEntity=keyEntity.concat(BillingUtils.VERTICAL).concat(billingProcessedFilter.getBillingServicePk().toString());
			
			//Si es safi: buscar PK y colocar nemonico
			BillingService billingService = find(BillingService.class, billingProcessedFilter.getBillingServicePk());
			List<BillingEconomicActivity> listEconActBd=billingServiceMgmtServiceBean.getBillingEconomicActivityByBillingServicePk(billingService.getIdBillingServicePk());
			List<Integer> listEconomicAct=BillingUtils.getBillingEconomicActivityListInteger(listEconActBd);
			Integer portFolio=billingService.getPortfolio();
			

			/**
			 * Cambio: si es SAFI con cartera de clientes, crear un listado de CollectionRecord
			 */
			if(BillingUtils.isSafiPortfolioClient(listEconomicAct, portFolio)) {
				if(Validations.validateIsNotNullAndNotEmpty(collectionRecord.getMnemonicConcatenated())) {
					keyEntity=keyEntity.concat(BillingUtils.VERTICAL).concat(collectionRecord.getMnemonicConcatenated());	
				}
				
			
			}
		}
		
		return keyEntity;
	}
	
	/**
	 * Setting entity collection.
	 *
	 * @param billingCalculation the billing calculation
	 * @param collectionRecord the collection record
	 */
	public void settingEntityCollection(BillingCalculation billingCalculation , CollectionRecord collectionRecord){
		
		if(collectionRecord.getEntityCollectionId().equals(EntityCollectionType.HOLDERS.getCode())){			
			billingCalculation.setEntityCollection(EntityCollectionType.HOLDERS.getCode());
			billingCalculation.setHolder(collectionRecord.getHolder());
			
			billingCalculation.setEntityCode(collectionRecord.getHolder().getIdHolderPk().toString());
			billingCalculation.setEntityDescription(collectionRecord.getHolder().getFullName());
		}else if(collectionRecord.getEntityCollectionId().equals(EntityCollectionType.ISSUERS.getCode())){			
			billingCalculation.setEntityCollection(EntityCollectionType.ISSUERS.getCode());
			billingCalculation.setIssuer(collectionRecord.getIssuer());
			
			billingCalculation.setEntityCode(collectionRecord.getIssuer().getIdIssuerPk());
			billingCalculation.setEntityDescription(collectionRecord.getIssuer().getBusinessName());
		}else if(collectionRecord.getEntityCollectionId().equals(EntityCollectionType.PARTICIPANTS.getCode())){			
			billingCalculation.setEntityCollection(EntityCollectionType.PARTICIPANTS.getCode());
			billingCalculation.setParticipant(collectionRecord.getParticipant());
			
			billingCalculation.setEntityCode(collectionRecord.getParticipant().getIdParticipantPk().toString());
			billingCalculation.setEntityDescription(collectionRecord.getParticipant().getDescription());
		}else if(collectionRecord.getEntityCollectionId().equals(EntityCollectionType.OTHERS.getCode())){	
			billingCalculation.setEntityCollection(EntityCollectionType.OTHERS.getCode());
			
			billingCalculation.setEntityCode(collectionRecord.getOtherEntityDocument());
			billingCalculation.setEntityDescription(collectionRecord.getOtherEntityDescription());			
		}
	}
	
	/**
	 * Gets the information entity other.
	 *
	 * @param filter the filter
	 * @return the information entity other
	 * @throws ServiceException the service exception
	 */
	public CollectionRecordTo getInformationEntityOther(CollectionRecordTo filter)throws ServiceException{
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("  SELECT ROWNUM,ID_COLLECTION_RECORD_PK,   ");
		sbQuery.append("    OTHER_ENTITY_DESCRIPTION,OTHER_ENTITY_ADDRESS,OTHER_ENTITY_COUNTRY,  ");
		sbQuery.append("    OTHER_ENTITY_DEPARTMENT, OTHER_ENTITY_PROVINCE,OTHER_ENTITY_DISTRICT  ");
		sbQuery.append("  FROM( ");
		sbQuery.append(" 	 SELECT  CR.ID_COLLECTION_RECORD_PK,");
		sbQuery.append("   	 CR.OTHER_ENTITY_DESCRIPTION,CR.OTHER_ENTITY_ADDRESS,CR.OTHER_ENTITY_COUNTRY, ");
		sbQuery.append("   	 CR.OTHER_ENTITY_DEPARTMENT, CR.OTHER_ENTITY_PROVINCE,CR.OTHER_ENTITY_DISTRICT ");
		sbQuery.append("    FROM COLLECTION_RECORD CR   ");
		sbQuery.append("    WHERE 1=1  ");	 
		
		if(Validations.validateIsNotNull(filter.getDocumentType())&&Validations.validateIsNotNull(filter.getOtherEntityDocument())){
			 sbQuery.append(" AND CR.DOCUMENT_TYPE||CR.OTHER_ENTITY_DOCUMENT =:keyDocumentPrm");
		}
		sbQuery.append(" 	ORDER BY to_char(CR.last_modify_date,'yyyy-mm-dd hh24:mi:ss') DESC ");
		sbQuery.append("  )");
		sbQuery.append("   where  rownum=1 ");
		
		Query query = em.createNativeQuery(sbQuery.toString());
		  
		if(	Validations.validateIsNotNull(filter.getDocumentType())&&
			Validations.validateIsNotNull(filter.getOtherEntityDocument())){
			query.setParameter("keyDocumentPrm", filter.getDocumentType().toString().concat(filter.getOtherEntityDocument()));
		}
		  
	   List<Object[]>  objectList = query.getResultList();
	   
	   CollectionRecordTo collectionRecordTo=null;
	   
	   if(Validations.validateListIsNotNullAndNotEmpty(objectList)){
		   	collectionRecordTo=new CollectionRecordTo();
			Object[] sResults = (Object[])objectList.get(0);
			
			collectionRecordTo.setOtherEntityDescription(sResults[2]==null?"":sResults[2].toString());
			collectionRecordTo.setOtherEntityAddress(sResults[3]==null?"":sResults[3].toString());
			collectionRecordTo.setOtherEntityCountry(sResults[4]==null?0:Integer.parseInt(sResults[4].toString()));
			collectionRecordTo.setOtherEntityDepartment(sResults[5]==null?0:Integer.parseInt(sResults[5].toString()));
			collectionRecordTo.setOtherEntityProvince(sResults[6]==null?0:Integer.parseInt(sResults[6].toString()));
			collectionRecordTo.setOtherEntityDistrict(sResults[7]==null?0:Integer.parseInt(sResults[7].toString()));
			
	   }	
				
		return collectionRecordTo;
		
	}
	
		
	/**
	 * Gets the geographic location for issuer.
	 *
	 * @param filter the filter
	 * @return the geographic location for issuer
	 */
	public IssuerTo getGeographicLocationForIssuer(String filter){
		IssuerTo issuerResult=null;
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("    SELECT ");
		sbQuery.append("    	ID_ISSUER_PK,  ");		
		sbQuery.append("    	LEGAL_ADDRESS,  ");
		sbQuery.append("    	RESIDENCE_COUNTRY,   ");//country
		sbQuery.append("    	DEPARTMENT, ");//department
		sbQuery.append("    	PROVINCE, ");//province
		sbQuery.append("    	DISTRICT, ");//district
		sbQuery.append("    	BUSINESS_NAME  ");
		sbQuery.append("    FROM  ");
		sbQuery.append("    	ISSUER ");
		sbQuery.append("    WHERE ");
		sbQuery.append("    	ID_ISSUER_PK=:idIssuerPk ");
		Query query = em.createNativeQuery(sbQuery.toString());
		
		if(Validations.validateIsNotNullAndNotEmpty(filter)){
			query.setParameter("idIssuerPk", filter);
		}
		
		List<Object[]>  objectList = query.getResultList();
		if(Validations.validateListIsNotNullAndNotEmpty(objectList)){
			Object[] object = (Object[])objectList.get(0);
			issuerResult= new IssuerTo();
			issuerResult.setIdIssuerPk(object[0]==null?"":(String)object[0].toString());
			issuerResult.setAddressIssuer(object[1]==null?"":object[1].toString());
			issuerResult.setCountry(object[2]==null?0:Integer.parseInt(object[2].toString()));
			issuerResult.setProvince(object[3]==null?0:Integer.parseInt(object[3].toString()));
			issuerResult.setDepartament(object[4]==null?0:Integer.parseInt(object[4].toString()));
			issuerResult.setDistrict(object[5]==null?0:Integer.parseInt(object[5].toString()));
			issuerResult.setIdIssuerPk(object[6]==null?"":(String)object[6].toString());
			
		}

		return issuerResult;
		}
	
	/**
	 * Gets the list user by business process.
	 *
	 * @param businessProcess the business process
	 * @return the list user by business process
	 */
	public List<UserAccountSession> getListUserByBusinessProcess(Long businessProcess){
		
		return billingComponentService.getListUserByBusinessProcess(businessProcess);

	}
	
	
}
