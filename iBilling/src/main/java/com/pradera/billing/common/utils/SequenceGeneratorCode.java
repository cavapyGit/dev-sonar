package com.pradera.billing.common.utils;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;

import com.pradera.billing.common.facade.BusinessLayerCommonFacade;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.framework.entity.producer.DepositaryDataBase;
import com.pradera.core.framework.entity.service.LoaderEntityServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.billing.BillingCorrelative;
import com.pradera.model.generalparameter.type.BillingCorrelativeType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SequenceGeneratorCode.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Singleton
public class SequenceGeneratorCode    {


	/** The log. */
	@Inject
	PraderaLogger log;
	 
	/** The load entity service. */
	@EJB
	LoaderEntityServiceBean 	loadEntityService;
	
	/** The crud dao service bean. */
	@EJB
	CrudDaoServiceBean 			crudDaoServiceBean;
	
	/** The business layer common facade. */
	@EJB
	BusinessLayerCommonFacade	businessLayerCommonFacade;
	
	 	
	/** The em. */
	@Inject @DepositaryDataBase
	private EntityManager em;
	
	
	
	/**
	 * Gets the last sequence generator code.
	 *
	 * @param billingCorrelativeFilter the billing correlative filter
	 * @return the last sequence generator code
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	@Performance
	public  Long    getLastSequenceGeneratorCode(BillingCorrelative billingCorrelativeFilter) throws ServiceException{
		
		Long     	codeGenerate  = null;
			
		BillingCorrelative correlativeResult = businessLayerCommonFacade.getLastSequenceGeneratorCode(billingCorrelativeFilter);
						
		if( Validations.validateIsNull(correlativeResult)
			|| Validations.validateIsNull(correlativeResult.getIdBillingCorrelativePk()) ){
		
			correlativeResult	= new BillingCorrelative();	
			correlativeResult.setCorrelativeCD(billingCorrelativeFilter.getCorrelativeCD());
			correlativeResult.setCorrelativeNumber(1L);
			correlativeResult.setDescription(billingCorrelativeFilter.getCorrelativeCD());
		
			if (billingCorrelativeFilter.getCorrelativeCD().equals(BillingCorrelativeType.BILL_CODE_NCF_COR.getValue()) ){
				correlativeResult.setBillingSerie(billingCorrelativeFilter.getBillingSerie());
				correlativeResult.setBillingTCF(billingCorrelativeFilter.getBillingTCF());									
			}		
			
			crudDaoServiceBean.create(correlativeResult);							
		}else{
			
			Long sequence = correlativeResult.getCorrelativeNumber();
			correlativeResult.setCorrelativeNumber(sequence +1);						
			crudDaoServiceBean.update(correlativeResult);						
		}	
		
		em.flush();
		codeGenerate = correlativeResult.getCorrelativeNumber();
					
	
		return codeGenerate;
		
	}
	
	

	/**
	 * Gets the last sequence for bill.
	 *
	 * @param billingCorrelativeFilter the billing correlative filter
	 * @return the last sequence for bill
	 * @throws ServiceException the service exception
	 */
	public  String    getLastSequenceForBill(BillingCorrelative billingCorrelativeFilter) throws ServiceException{
		
		Long       codeGenerate  = getLastSequenceGeneratorCode(billingCorrelativeFilter);		
		String 	   strSecuence	 = "";
		Integer    numDigits	 = 0;
		
		if (Validations.validateIsNotNull(codeGenerate)){
				
				strSecuence = codeGenerate.toString();
				numDigits	= codeGenerate.toString().length();
				
				for (int i = 0; i < (PropertiesConstants.LONG_SEQUENCE - numDigits); i++) {
					
					strSecuence=GeneralConstants.ZERO_VALUE_STRING+strSecuence;
				}
			
		}else{
			return "";
		}
		 	
		return strSecuence;
		
	}
	
	

}
