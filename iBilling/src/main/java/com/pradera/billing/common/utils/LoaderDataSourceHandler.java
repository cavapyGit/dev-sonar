package com.pradera.billing.common.utils;



import java.math.BigDecimal;
import java.util.Stack;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.pradera.billing.billingsource.to.BillingServiceSourceInfo;
import com.pradera.billing.billingsource.to.FieldSourceTo;
import com.pradera.billing.billingsource.to.QuerySourceTo;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.billing.type.BillingServiceFieldSourceType;
import com.pradera.model.billing.type.BillingServiceFieldSourceVisibilityType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class LoaderDataSourceHandler.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public  class LoaderDataSourceHandler extends DefaultHandler {
	 
	 	
	 /** The billing service source info. */
 	private BillingServiceSourceInfo   billingServiceSourceInfo ;	 
	 
 	/** The element stack. */
 	private Stack<String> 		 	elementStack = new Stack<String>();
	 
 	/** The object stack. */
 	private Stack<Object> 		 	objectStack  = new Stack<Object>();
	
	 
	 /**
 	 * *
 	 * Begin Tag Elements XML.
 	 *
 	 * @param uri the uri
 	 * @param localName the local name
 	 * @param qName the q name
 	 * @param attributes the attributes
 	 * @throws SAXException the SAX exception
 	 */
	 public void startElement(String uri, String localName,
		        String qName, Attributes attributes) throws SAXException {
		 			 
	        this.elementStack.push(qName);
	        
	        /**Tag Parent**/
	        if(PropertiesConstants.BILLING_SERVICE_SOURCE_INFO.equals(qName)){
	        	billingServiceSourceInfo = new BillingServiceSourceInfo();
	        }
	        else if(PropertiesConstants.BILLING_SERVICE_SOURCE_FIELD.equals(qName)){
	        	
	        	if (billingServiceSourceInfo==null) {
	        		throw new SAXException(PropertiesConstants.BILLING_SERVICE_SOURCE_WRONG_STRUCTURED);
	        	}
	        	FieldSourceTo fieldSourceTo = new FieldSourceTo();
	          
	            for (int i = 0; i < attributes.getLength(); i++) {
	                System.out.println("             attribute: /" + attributes.getURI(i) + "/" + attributes.getLocalName(i) + "/" + attributes.getQName(i) + "/" + attributes.getType(i) + "/" + attributes.getValue(i) + "/");
	               
	                /**Validate Field attribute Structure File XML
	                 * Example:
	                 * 		<field  visibility="true" type="ID_HOLDER_ACCOUNT_FK" posicion="0" level="0">
	                 * 
	                 * **/
	                
	                /**Attribute Visibility**/
	                if (attributes.getQName(i).trim().equalsIgnoreCase(PropertiesConstants.BILLING_SERVICE_SOURCE_VISIBILITY)){
	                	Boolean exist=Boolean.FALSE;
	                	for (BillingServiceFieldSourceVisibilityType type : BillingServiceFieldSourceVisibilityType.list) {
							if(type.getValue().compareTo(attributes.getValue(i).toUpperCase())==0){
								exist=Boolean.TRUE;
								break;
							}
							
						}
	                	if(exist){
	                		fieldSourceTo.setVisibility(Boolean.valueOf(attributes.getValue(i)));
	                	}
	                	else{
	                		throw new SAXException(PropertiesConstants.BILLING_SERVICE_SOURCE_VISIBILITY_FORMAT);
	                	}
	                	
	                /**Attribute type**/	
	                }else  if (attributes.getQName(i).trim().equalsIgnoreCase(PropertiesConstants.BILLING_SERVICE_SOURCE_TYPE)){
	                	
	                	Boolean exist=Boolean.FALSE;
	                	for (BillingServiceFieldSourceType type : BillingServiceFieldSourceType.list) {
							if(type.getValue().compareTo(attributes.getValue(i).toUpperCase())==0){
								exist=Boolean.TRUE;
								break;
							}
							
						}
	                	if(exist){
	                		fieldSourceTo.setParameterType(attributes.getValue(i));
	                	}
	                	else{
	                		throw new SAXException(PropertiesConstants.BILLING_SERVICE_SOURCE_TYPE_FORMAT);
	                	}
	                	
	                	
	                /***Attribute position**/	
	                }else if (attributes.getQName(i).trim().equalsIgnoreCase(PropertiesConstants.BILLING_SERVICE_SOURCE_POSICION)){
	                	
	                	try{
	                		fieldSourceTo.setPosicion(Integer.valueOf(attributes.getValue(i)));
	                	}
	                	catch(Exception ex){
	             		   if(ex instanceof NumberFormatException){
	             			   throw new SAXException(PropertiesConstants.BILLING_SERVICE_SOURCE_POSITION_FORMAT);
	             		   }
	             	   }
	                	
	                /**Attribute Level**/
	                }else if (attributes.getQName(i).trim().equalsIgnoreCase(PropertiesConstants.BILLING_SERVICE_SOURCE_LEVEL)){
	                	
	                	try{
	                		fieldSourceTo.setLevel(Integer.valueOf(attributes.getValue(i)));
	                	}
	                	catch(Exception ex){
	             		   if(ex instanceof NumberFormatException){
	             			   throw new SAXException(PropertiesConstants.BILLING_SERVICE_SOURCE_LEVEL_FORMAT);
	             		   }
	             	   }
	                	
	                /**Attribute value**/
	                }else  if (attributes.getQName(i).trim().equalsIgnoreCase(PropertiesConstants.BILLING_SERVICE_SOURCE_VALUE)){
	                	fieldSourceTo.setParameterType(attributes.getValue(i));
	                }
	            }
	            	            
	            this.objectStack.push(fieldSourceTo);
	          
	        } else if(PropertiesConstants.BILLING_SERVICE_SOURCE_QUERY.equals(qName)){
	        	
	        	
	        	/**
	        	 * Validate Query attribute Structure File XML
	        	 * Example:
	        	 * 		<query  date="true">	
	        	 * 
	        	 * **/
	        	if (billingServiceSourceInfo==null) {
	        		
	        		throw new SAXException(PropertiesConstants.BILLING_SERVICE_SOURCE_WRONG_STRUCTURED);
	        		
	        	}
	        	
	        	QuerySourceTo	querySourceTo = new  QuerySourceTo();
	        		        	
	        	 for (int i = 0; i < attributes.getLength(); i++) {
//		                System.out.println("             attribute: /" + attributes.getURI(i) + "/" + attributes.getLocalName(i) + "/" + attributes.getQName(i) + "/" + attributes.getType(i) + "/" + attributes.getValue(i) + "/");
		               
		                if (attributes.getQName(i).trim().equalsIgnoreCase(PropertiesConstants.BILLING_SERVICE_SOURCE_DATE)){		                	
		                	querySourceTo.setDate(Boolean.valueOf(attributes.getValue(i)));
		                }
		          }
	        	
	        	 this.objectStack.push(querySourceTo);
	        }
	 }
	 
	 /**
 	 * End Tag Elements File XML.
 	 *
 	 * @param uri the uri
 	 * @param localName the local name
 	 * @param qName the q name
 	 * @throws SAXException the SAX exception
 	 */
	 public void endElement(String uri, String localName,
		        String qName) throws SAXException {

        this.elementStack.pop();

        if(PropertiesConstants.BILLING_SERVICE_SOURCE_FIELD.equals(qName)){
        	
        	Object object = this.objectStack .pop();
        	FieldSourceTo fieldSourceTo = (FieldSourceTo) object;
            
            this.billingServiceSourceInfo.setFieldSourceTo(fieldSourceTo.getPosicion(), fieldSourceTo);
            
        }else if(PropertiesConstants.BILLING_SERVICE_SOURCE_NAME.equals(qName)){
        }else if(PropertiesConstants.BILLING_SERVICE_SOURCE_DESCRIPTION.equals(qName)){
        	
        }else if(PropertiesConstants.BILLING_SERVICE_SOURCE_VALUE.equals(qName)){
        	
        }else if(PropertiesConstants.BILLING_SERVICE_SOURCE_QUERY.equals(qName)){
        	   
        	Object object = this.objectStack .pop();
        	QuerySourceTo querySource =  (QuerySourceTo) object;
        	this.billingServiceSourceInfo.setQuerySource(querySource);
        	
        }else if(PropertiesConstants.BILLING_SERVICE_SOURCE_INFO.equals(qName)){
        	
        }
        
	 }
	 
	 /* (non-Javadoc)
 	 * @see org.xml.sax.helpers.DefaultHandler#characters(char[], int, int)
 	 */
 	public void characters(char ch[], int start, int length)
		        throws SAXException {

		 /**Value: Content between the <> and </> by line **/
        String value = new String(ch, start, length);
        if(value.length() == 0) return; // ignore white space
        
        /**Tag parent <field> **/
        if (PropertiesConstants.BILLING_SERVICE_SOURCE_FIELD.equals(currentElementParent())){
        	
        	/**Tag <name> **/
           if(PropertiesConstants.BILLING_SERVICE_SOURCE_NAME.equals(currentElement())){

           	FieldSourceTo fieldSourceTo = (FieldSourceTo) this.objectStack.peek();
           	fieldSourceTo.setName(  (fieldSourceTo.getName() != null ? fieldSourceTo.getName()  : "") .concat(value));
           
           	/**Tag <description>**/
           }else  if(PropertiesConstants.BILLING_SERVICE_SOURCE_DESCRIPTION.equals(currentElement())){

           	FieldSourceTo fieldSourceTo = (FieldSourceTo) this.objectStack.peek();
           	fieldSourceTo.setDescription((fieldSourceTo.getDescription() != null ? fieldSourceTo.getDescription()  : "") .concat(value)) ;
          
           	/**Tag <value>**/
           }else  if(PropertiesConstants.BILLING_SERVICE_SOURCE_VALUE.equals(currentElement())){

        	   
        	   try{
        		   BigDecimal valueBigDecimal=new BigDecimal(value);
        		   FieldSourceTo fieldSourceTo = (FieldSourceTo) this.objectStack.peek();
                   fieldSourceTo.setValue( (fieldSourceTo.getValue() != null ? fieldSourceTo.getValue()  : "") .concat(value));
        	   }
        	   catch(Exception ex){
        		   if(ex instanceof NumberFormatException){
        			   throw new SAXException(PropertiesConstants.BILLING_SERVICE_SOURCE_VALUE_FORMAT);
        		   }
        	   }
        	   
              	
            }
        }

        
        /**Contains label <query>, includes the query and the attribute date**/
       if(PropertiesConstants.BILLING_SERVICE_SOURCE_QUERY.equals(currentElement()) &&
          PropertiesConstants.BILLING_SERVICE_SOURCE_INFO.equals(currentElementParent())){
    	       	   
    	   Object object = this.objectStack.peek();
       	   QuerySourceTo querySource =  (QuerySourceTo) object;
    	 
       	  if (Validations.validateIsNullOrEmpty(querySource.getQuery())){ 
       		querySource.setQuery(value);
    	  }else {
    		  String queryOld = querySource.getQuery();
    		  if(start==0){
    			  querySource.setQuery(queryOld.concat(value));
    		  }else{
    			  querySource.setQuery(queryOld.concat(" ").concat(value));
    		  }
    		  
    		  
    	  }
       }
		      
	}
	 
	 /**
 	 * Current element.
 	 *
 	 * @return the string
 	 */
 	private String currentElement() {
	    return this.elementStack.peek();
	 }

	 /**
 	 * Current element parent.
 	 *
 	 * @return the string
 	 */
 	private String currentElementParent() {
	    if(this.elementStack.size() < 2) return null;
	    return this.elementStack.get(this.elementStack.size()-2);
	 }

	/**
	 * Gets the billing service source info.
	 *
	 * @return the billingServiceSourceInfo
	 */
	public BillingServiceSourceInfo getBillingServiceSourceInfo() {
		return billingServiceSourceInfo;
	}


    /**
     * Clean billing service source info.
     */
    public void cleanBillingServiceSourceInfo(){
   	 this.billingServiceSourceInfo=null;
    }


     
     
}
