package com.pradera.billing.common.utils;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TreeSet;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.commons.lang3.SerializationUtils;
import org.xml.sax.SAXException;

import com.pradera.billing.billingcollection.to.CollectionRecordTo;
import com.pradera.billing.billingcollection.to.ProcessedServiceTo;
import com.pradera.billing.billingexception.to.RateExceptionTo;
import com.pradera.billing.billingrate.to.ServiceRateTo;
import com.pradera.billing.billingsource.to.BillingServiceSourceInfo;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.type.EconomicActivityType;
import com.pradera.model.billing.BillingEconomicActivity;
import com.pradera.model.billing.ServiceRate;
import com.pradera.model.billing.type.BaseCollectionType;
import com.pradera.model.generalparameter.ParameterTable;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingUtils.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class BillingUtils {

	/** The Constant DATE_FORMAT_YYYY. */
	public static final String 	DATE_FORMAT_YYYY		="yyyy";
	
	/** The Constant DATE_FORMAT_MM. */
	public static final String 	DATE_FORMAT_MM			="MM";
	
	/** The Constant DATE_FORMAT_DD. */
	public static final String 	DATE_FORMAT_DD			="dd";
	
	/** The Constant DATE_PATTERN_yyyy_MM. */
	public static final String 	DATE_PATTERN_yyyy_MM	="yyyy/MM"; 	
	
	/** The Constant DATE_PATTERN_yyyy_MM_dd. */
	public static final String  DATE_PATTERN_yyyy_MM_dd = "yyyy/MM/dd";
	
	/** The Constant SLACH. */
	public static final String  SLACH					="/";
	
	/** The Constant VERTICAL. */
	public static final String  VERTICAL				="|";
	
	/** The Constant BREAK_LINE. */
	public static final String  BREAK_LINE				="<br>";
	
	/** The Constant HORIZONTAL_LINE. */
	public static final String  HORIZONTAL_LINE			=" - ";
	
	
	
	/**
	 * Get Year to Date.
	 *
	 * @param date the date
	 * @return yyyy
	 */
	public static Integer getYearToDate(Date date){

	if (null == date){

		return 0;

	}else{

		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_YYYY);
		return Integer.parseInt(dateFormat.format(date));

	}

	}


	/**
	 * Get Month To Date.
	 *
	 * @param date the date
	 * @return MM
	 */
	public static Integer getMonthToDate(Date date){

	if (null == date){

		return 0;

	}
	else{
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_MM);
		return Integer.parseInt(dateFormat.format(date));
	}

	}

	/**
	 * *
	 * Get Day To Date.
	 *
	 * @param date the date
	 * @return dd
	 */
	public static Integer getDayToDate(Date date){

	if (null == date){

		return 0;

	}
	else{

		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_DD);
		return Integer.parseInt(dateFormat.format(date));

	}

	}

	/**
	 * Gets the pattern year month.
	 *
	 * @param strDate the str date
	 * @param date_pattern yyyy/MM/dd
	 * @return yyyy/MM
	 */
	public  static String getPatternYearMonth(String strDate,String date_pattern){
		String strYearMonth="";
		 if (BillingUtils.DATE_PATTERN_yyyy_MM_dd.equalsIgnoreCase(date_pattern)){
			 strYearMonth = strDate.substring(0, strDate.lastIndexOf("/"));
		 }
		return strYearMonth;
	}
	
	/**
	 * Gets the pattern year.
	 *
	 * @param strDate the str date
	 * @param date_pattern the date_pattern
	 * @return the pattern year
	 */
	public static Integer getPatternYear(String strDate,String date_pattern){
		Integer intYear=null;
		 if (BillingUtils.DATE_PATTERN_yyyy_MM.equalsIgnoreCase(date_pattern)){
				 String strYear= strDate.substring(0, strDate.lastIndexOf("/"));
				 intYear= Integer.parseInt(strYear);
		 }
		return intYear;
	}
	
	/**
	 * Gets the pattern month.
	 *
	 * @param strDate the str date
	 * @param date_pattern the date_pattern
	 * @return the pattern month
	 */
	public static Integer getPatternMonth(String strDate,String date_pattern){
		Integer intMonth=null;
		 if (BillingUtils.DATE_PATTERN_yyyy_MM.equalsIgnoreCase(date_pattern)){			
			 String strMonth= strDate.substring(strDate.lastIndexOf("/")+1);
			 intMonth= Integer.parseInt(strMonth);
		 }
		return intMonth;
	}
	
	/**
	 * Gets the max date of month.
	 *
	 * @param objDate the obj date
	 * @return the max date of month
	 */
	public static Date truncateDayOfMonth(Date objDate){
		Calendar cal = Calendar.getInstance();
		cal.setTime(objDate);
		cal.set(cal.DAY_OF_MONTH, 0);
		return cal.getTime();
	}
	
	/**
	 * Get String with Code Service Rate.
	 *
	 * @param listServiceRate the list service rate
	 * @return the string list service rate
	 */
	public static String getStringListServiceRate(List<ServiceRate> listServiceRate){
		String codeRate="";
		for(int i=0;i<listServiceRate.size();i++){
			if(i>0){
				codeRate+=", "+listServiceRate.get(i).getRateCode().toString();
			}
			else{
				codeRate=listServiceRate.get(i).getRateCode().toString();
			}
		}
		return codeRate;
	}
	
	/**
	 * Clone List T.
	 *
	 * @param <T> the generic type
	 * @param list the list
	 * @return the list
	 */
	public  static  <T> List<T>  cloneList(List<T> list) {
		List<T> clone = new ArrayList<T>(list.size());
		for (T item: list) {	
			clone.add(item);
		}
		return clone;
	}
	
	/**
	 * Clone List To Service Rate To.
	 *
	 * @param list the list
	 * @return the list
	 */
	public static  List<ServiceRateTo>  cloneServiceRateToList(List<ServiceRateTo> list){
		List<ServiceRateTo> clone = new ArrayList<ServiceRateTo>(list.size());
		 
		for (ServiceRateTo item: list) {
			ServiceRateTo serviceRateTo = SerializationUtils.clone(item);
			
			clone.add(serviceRateTo);
		}
		return clone;
	}
	
	/**
	 * Clone List Rate exception To.
	 *
	 * @param list the list
	 * @return the list
	 */
	public static  List<RateExceptionTo>  cloneRateExceptionToList(List<RateExceptionTo> list){
		List<RateExceptionTo> clone = new ArrayList<RateExceptionTo>(list.size());
		 
		for (RateExceptionTo item: list) {				
			RateExceptionTo rateExceptionTo = SerializationUtils.clone(item);
			
			clone.add(rateExceptionTo);
		}
		return clone;
	}
	
	/**
	 * Clone List to Processed Service To.
	 *
	 * @param list the list
	 * @return the list
	 */
	public static  List<ProcessedServiceTo>  cloneProcessedServiceToList(List<ProcessedServiceTo> list){
		List<ProcessedServiceTo> clone = new ArrayList<ProcessedServiceTo>(list.size());
		 
		for (ProcessedServiceTo item: list) {				
			ProcessedServiceTo processedServiceTo = SerializationUtils.clone(item);
			
			clone.add(processedServiceTo);
		}
		return clone;
	}
	

	/**
	 * Round Big Decimal to Rounding Mode BigDecimal.ROUND_HALF_UP
	 *
	 * @param numDecimal the num decimal
	 * @param round the round
	 * @return the big decimal
	 */
	 public static BigDecimal roundBigDecimal(BigDecimal numDecimal,Integer round){
		 BigDecimal result=new BigDecimal(0);
		 if(Validations.validateIsNull(numDecimal))
			 numDecimal=BigDecimal.ZERO;
		
		 int mode = BigDecimal.ROUND_HALF_UP;
		 result=numDecimal.setScale(round, mode);
		 return result;
	 }
	
	
	 /**
 	 * Multiply decimal integer.
 	 *
 	 * @param numDecimal the num decimal
 	 * @param numEntero the num entero
 	 * @param round GeneralConstants.ROUNDING_DIGIT_NUMBER
 	 * @return Multiply BigDecimal and Integer result BigDecimal
 	 */
	public static BigDecimal multiplyDecimalInteger(BigDecimal numDecimal,Integer numEntero,Integer round){
		  BigDecimal decimalOfInteger=null;
		  BigDecimal result=new BigDecimal(0);
		  int mode = BigDecimal.ROUND_HALF_UP;
		  if(Validations.validateIsNull(numDecimal)){
			  numDecimal=BigDecimal.ZERO;
		  }
		  if(Validations.validateIsNull(numEntero)){
			  decimalOfInteger=BigDecimal.ZERO; 
		  }else{
			  decimalOfInteger=new BigDecimal(numEntero);
		  }
		  result=numDecimal.multiply(decimalOfInteger).setScale(round,mode);
		  return result;
	}
	
	
	
	/**
	 * Multiply Number <code>BigDecimal</code> and <code>Long</code>.
	 *
	 * @param numDecimal Number BigDecimal
	 * @param numEntero Number Long
	 * @param round Ind. Round GeneralConstants.ROUNDING_DIGIT_NUMBER
	 * @return the big decimal
	 */
	public static BigDecimal multiplyDecimalLong(BigDecimal numDecimal,Long numEntero,Integer round){
		  BigDecimal decimalOfLong=null;
		  BigDecimal result=new BigDecimal(0);
		  int mode = BigDecimal.ROUND_HALF_UP;
		  if(Validations.validateIsNull(numDecimal)){
			  numDecimal=BigDecimal.ZERO; 
		  } 
		  if(Validations.validateIsNull(numEntero)){
			  numEntero=Long.parseLong(GeneralConstants.ZERO_VALUE_STRING); 
		  }	  
		  decimalOfLong=new BigDecimal(numEntero);
		  result=numDecimal.multiply(decimalOfLong).setScale(round,mode);
		  return result;
	}

	/**
	 * *
	 * Multiply two BigDecimal result BigDecimal.
	 *
	 * @param numDecimal1 the num decimal1
	 * @param numDecimal2 the num decimal2
	 * @param round GeneralConstants.ROUNDING_DIGIT_NUMBER
	 * @return the big decimal
	 */
	 public static BigDecimal multiplyDecimals(BigDecimal numDecimal1,BigDecimal numDecimal2,Integer round){
		 BigDecimal result=new BigDecimal(0);
		 int mode = BigDecimal.ROUND_HALF_UP;
		 if(Validations.validateIsNull(numDecimal1)){
			 numDecimal1=BigDecimal.ZERO;
		 }	 
		 if(Validations.validateIsNull(numDecimal2)){
			 numDecimal2=BigDecimal.ZERO;
		 }
		 
		 result=numDecimal1.multiply(numDecimal2).setScale(round,mode);
	  return result;
	}
	 
	 /**
 	 * Add Two Integer .
 	 *
 	 * @param numInt1 Number Integer 1
 	 * @param numInt2 Number Integer 2
 	 * @return the integer
 	 */
	 public static Integer addTwoInteger(Integer numInt1,Integer numInt2){

		  if(Validations.validateIsNull(numInt1)){
			  numInt1=GeneralConstants.ZERO_VALUE_INTEGER;
		  }
		  if(Validations.validateIsNull(numInt2)){
			  numInt2=GeneralConstants.ZERO_VALUE_INTEGER; 
		  }
		  
		  return numInt1+numInt2;
	}
	 
 	/**
 	 * Add two BigDecimal result BigDecimal.
 	 *
 	 * @param numDecimal1 the num decimal1
 	 * @param numDecimal2 the num decimal2
 	 * @param round GeneralConstants.ROUNDING_DIGIT_NUMBER
 	 * @return the big decimal
 	 */
	 public static BigDecimal addTwoDecimal(BigDecimal numDecimal1,BigDecimal numDecimal2,Integer round){
		 BigDecimal result=new BigDecimal(0);
		 int mode = BigDecimal.ROUND_HALF_UP;
		 if(Validations.validateIsNull(numDecimal1)){
			 numDecimal1=BigDecimal.ZERO;
		 }
		 if(Validations.validateIsNull(numDecimal2)){
			 numDecimal2=BigDecimal.ZERO;
		 }
		 result=numDecimal1.add(numDecimal2).setScale(round, mode);
		 return result;
	 }

	 /**
 	 * Multiply BigDecimal and Integer result BigDecimal.
 	 *
 	 * @param numDecimal the num decimal
 	 * @param numEntero the num entero
 	 * @param round GeneralConstants.ROUNDING_DIGIT_NUMBER
 	 * @return the big decimal
 	 */
		public static BigDecimal addDecimalInteger(BigDecimal numDecimal,Integer numEntero,Integer round){
			  BigDecimal decimalOfInteger=null;
			  BigDecimal result=new BigDecimal(0);
			  int mode = BigDecimal.ROUND_HALF_UP;
			  if(Validations.validateIsNull(numDecimal)){
				  numDecimal=BigDecimal.ZERO;
			  }
			  if(Validations.validateIsNull(numEntero)){
				  decimalOfInteger=BigDecimal.ZERO; 
			  }
			  decimalOfInteger=new BigDecimal(numEntero);
			  result=numDecimal.add(decimalOfInteger).setScale(round, mode);
			  return result;
		}

		/**
		 * Add two BigDecimal  (BigDecimal, Object)result BigDecimal.
		 *
		 * @param numDecimal1 the num decimal1
		 * @param obj the obj
		 * @param round GeneralConstants.ROUNDING_DIGIT_NUMBER
		 * @return the big decimal
		 */
		 public static BigDecimal addDecimalObject(BigDecimal numDecimal1,Object obj,Integer round){
			 BigDecimal result=new BigDecimal(0);
			 int mode = BigDecimal.ROUND_HALF_UP;
			 BigDecimal objDecimal=null;
			 if(Validations.validateIsNull(numDecimal1)){
				 numDecimal1=BigDecimal.ZERO;
			 }
				 
			 if(Validations.validateIsNull(obj)){
				 objDecimal=BigDecimal.ZERO;
			 }
			 else{
				 objDecimal= new BigDecimal(obj.toString());
			 }
			 result=numDecimal1.add(objDecimal).setScale(round, mode);
			 return result;
		 }
		 
		 
		 /**
 		 * Divide two decimal.
 		 *
 		 * @param numDecimal1 the num decimal1
 		 * @param numDecimal2 the num decimal2
 		 * @param round GeneralConstants.ROUNDING_DIGIT_NUMBER
 		 * @return *Divide two Decimal result BigDecimal
 		 * @throws ArithmeticException the arithmetic exception
 		 */
		 public static BigDecimal divideTwoDecimal(BigDecimal numDecimal1,BigDecimal numDecimal2,Integer round) throws ArithmeticException{
			 BigDecimal result=new BigDecimal(0);

			 int mode = BigDecimal.ROUND_HALF_UP;
			 if(numDecimal1==null){
				 numDecimal1=BigDecimal.ZERO;
			 }
			 if(numDecimal2==null){
				 return null;
			 }			 
			 result=numDecimal1.divide(numDecimal2,round,mode);
			 return result;
		 }
		 
		 
		 /**
 		 * Divide decimal object.
 		 *
 		 * @param numDecimal1 the num decimal1
 		 * @param obj the obj
 		 * @param round GeneralConstants.ROUNDING_DIGIT_NUMBER
 		 * @return Divide two Decimal (BigDecimal, Object) result BigDecimal
 		 * @throws ArithmeticException the arithmetic exception
 		 */
		 public static BigDecimal divideDecimalObject(BigDecimal numDecimal1,Object obj,Integer round) throws ArithmeticException{
			 BigDecimal result=new BigDecimal(0);
			 BigDecimal objDecimal=null;
			 int mode = BigDecimal.ROUND_HALF_UP;
			 if(Validations.validateIsNull(numDecimal1)){
				 numDecimal1=BigDecimal.ZERO;
			 }
			 if(Validations.validateIsNull(obj)){
				 return null;
			 }
			 else {
				 objDecimal= new BigDecimal(obj.toString());
			 }
			 result=numDecimal1.divide(objDecimal,round,mode);
			 return result;
		 }
		 
		 /**
 		 * getFirstDayOfMonth.
 		 *
 		 * @param date the date
 		 * @return the first day of month
 		 */
		 public static Date getFirstDayOfMonth(Date date){
	    	 Calendar cal = Calendar.getInstance();
	    	 cal.setTime(date);
	    	 cal.set(cal.get(Calendar.YEAR),
	    			 
	                 cal.get(Calendar.MONTH),
	 
	                 cal.getActualMinimum(Calendar.DAY_OF_MONTH),
	 
	                 cal.getMinimum(Calendar.HOUR_OF_DAY),
	 
	                 cal.getMinimum(Calendar.MINUTE),
	 
	                 cal.getMinimum(Calendar.SECOND));
	 
	         return cal.getTime();
	     }
	     
     	/**
     	 * getLastDayOfMonth.
     	 *
     	 * @param date the date
     	 * @return the last day of month
     	 */
	     public static Date getLastDayOfMonth(Date date){
	    	 Calendar cal = Calendar.getInstance();
	    	 cal.setTime(date);
	         cal.set(cal.get(Calendar.YEAR),
	 
	                 cal.get(Calendar.MONTH),
	 
	                 cal.getActualMaximum(Calendar.DAY_OF_MONTH),
	 
	                 cal.getMaximum(Calendar.HOUR_OF_DAY),
	 
	                 cal.getMaximum(Calendar.MINUTE),
	 
	                 cal.getMaximum(Calendar.SECOND));
	 
	         return cal.getTime();
	     }
	     
	     /**
     	 * Get First Day Of the Year.
     	 *
     	 * @param date the date
     	 * @return the first day of year
     	 */
	     public static Date getFirstDayOfYear(Date date){
	    	 Calendar cal = Calendar.getInstance();
	    	 cal.setTime(date);
	    	 cal.set(cal.get(Calendar.YEAR),
	    			 
	                 cal.getActualMinimum(Calendar.MONTH),
	 
	                 cal.getActualMinimum(Calendar.DAY_OF_MONTH),
	 
	                 cal.getMinimum(Calendar.HOUR_OF_DAY),
	 
	                 cal.getMinimum(Calendar.MINUTE),
	 
	                 cal.getMinimum(Calendar.SECOND));
	 
	         return cal.getTime();
	     }
	    
	     
	     /**
     	 * Get Last Day of the Year.
     	 *
     	 * @param date the date
     	 * @return the last day of year
     	 */
	     public static Date getLastDayOfYear(Date date){
	    	 Calendar cal = Calendar.getInstance();
	    	 cal.setTime(date);
	         cal.set(cal.get(Calendar.YEAR),
	 
	                 cal.getActualMaximum(Calendar.MONTH),
	 
	                 cal.getActualMaximum(Calendar.DAY_OF_MONTH),
	 
	                 cal.getMaximum(Calendar.HOUR_OF_DAY),
	 
	                 cal.getMaximum(Calendar.MINUTE),
	 
	                 cal.getMaximum(Calendar.SECOND));
	 
	         return cal.getTime();
	     }
	     
	     /**
     	 * Obtain the initial date of the day.
     	 *
     	 * @param calculateDate the calculate date
     	 * @return the date initial day
     	 */
	     public static Date getDateInitialDay(Date calculateDate){
	    	 
	    	Calendar calInitialTime = Calendar.getInstance();
	 		calInitialTime.setTime(new Date());
	 		calInitialTime.set(Calendar.HOUR_OF_DAY,0);
	 		calInitialTime.set(Calendar.MINUTE,0);
	 		calInitialTime.set(Calendar.SECOND,0);
	 		calInitialTime.set(Calendar.MILLISECOND,0);
	 		
	 		return CommonsUtilities.setTimeToDate(calculateDate,calInitialTime.getTime());
	     }
	     
	     /**
     	 * Obtain the final date of the day.
     	 *
     	 * @param calculateDate the calculate date
     	 * @return the date final day
     	 */
	     public static Date getDateFinalDay(Date calculateDate){
	    	 
	    	Calendar calFinalTime = Calendar.getInstance();
	 		calFinalTime.setTime(new Date());
	 		calFinalTime.set(Calendar.HOUR_OF_DAY,23);
	 		calFinalTime.set(Calendar.MINUTE,59);
	 		calFinalTime.set(Calendar.SECOND,59);
	 		
	 		return CommonsUtilities.setTimeToDate(calculateDate,calFinalTime.getTime());
	     }
	     
	     /**
     	 * Gets the day of the week.
     	 *
     	 * @param date Date
     	 * @return Day Of the Week
     	 */
	     public static Integer getDayOfTheWeek(Date date){
	    		GregorianCalendar cal = new GregorianCalendar();
	    		cal.setTime(date);
	    		return cal.get(Calendar.DAY_OF_WEEK);		
	    }
	     
     	/**
     	 * Subtract an entire month period Monthly.
     	 *
     	 * @param finalDate FinalDate
     	 * @return Initial Date result for Month subtract one month
     	 */
			public static Date getDateInitialForPeriod(Date finalDate){
				Date initialDate=null;
				String strDate 			=CommonsUtilities.convertDateToString(finalDate,DATE_PATTERN_yyyy_MM_dd);
				String strYearMonth  	=getPatternYearMonth(strDate, DATE_PATTERN_yyyy_MM_dd);
				Integer month=getPatternMonth(strYearMonth,DATE_PATTERN_yyyy_MM);
				Integer year=getPatternYear(strYearMonth,DATE_PATTERN_yyyy_MM);
				Integer daysSub=0;
				switch (month-1) {
				case 0: case 1: case 3: case 5: case 7: case 8: case 10:
					daysSub=30;
					break;
				case 4: case 6: case 9: case 11:
					daysSub=29;
					break;
				case 2:
					if ((year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0))){
						daysSub=28;
					}else{
						daysSub=27;
					}
				}
				initialDate=CommonsUtilities.addDaysToDate(finalDate, -daysSub);
				return initialDate;
			}
	     
     	/**
     	 * Delete space white.
     	 *
     	 * @param parameter the parameter
     	 * @return the string
     	 */
		 public String deleteSpaceWhite(String parameter){
			 
			 return (Validations.validateIsNullOrEmpty(parameter) ? parameter:parameter.replaceAll(" ", parameter));
		 }
		 
		 /**
 		 * Convert list object to string.
 		 *
 		 * @param lstObjects the lst objects
 		 * @param separator the separator
 		 * @return the string
 		 */
		 public static String convertListObjectToString(List<Object> lstObjects,String separator) {		
				
				StringBuilder sbString = new StringBuilder();
				for (int i=0; i<lstObjects.size(); i++) {
					sbString.append(lstObjects.get(i).toString());
					if(i < (lstObjects.size() - 1)){
						sbString.append(separator);
					}
				}
				
				return sbString.toString();
		}
		 
		 /**
 		 * Convert Tree Set To String concatenated .
 		 *
 		 * @param lstTreeSetObjects the lst tree set objects
 		 * @return the string
 		 */
		 public static String convertListObjectToString(TreeSet<Object> lstTreeSetObjects) {		
			List<Object> lstObjects =new ArrayList<>(lstTreeSetObjects);
			StringBuilder sbString = new StringBuilder();
				
			for (int i=0; i<lstObjects.size(); i++) {
				sbString.append(lstObjects.get(i).toString());
				if(i < (lstObjects.size() - 1)){
					sbString.append(",");
				}
			}
			
			return sbString.toString();
		}
		 
	 /**
 	 * Convert list collection record to list long.
 	 *
 	 * @param listCollection the list collection
 	 * @return the list
 	 */
	 public static List<Long> convertListCollectionRecordToListLong(List<CollectionRecordTo> listCollection) {		
			
		 List<Long> listLongCollection=new ArrayList<Long>();
		 
		 for (CollectionRecordTo collection  : listCollection) {
			 listLongCollection.add(collection.getIdCollectionRecordPk());
		 }

			return listLongCollection;
		}

		/**
		 * Load file to object source.
		 *
		 * @param path the path
		 * @return the billing service source info
		 */
		public static BillingServiceSourceInfo loadFileToObjectSource(String path){
			
			/**
			 *  byte[] byteArr = new byte[] { 0xC, 0xA, 0xF, 0xE };
				InputStream is = new ByteArrayInputStream(byteArr);
			 */
			SAXParserFactory factory = SAXParserFactory.newInstance();
			BillingServiceSourceInfo billingServiceSourceInfo = null  ;
			
			try {
				InputStream    xmlInput  =
						new FileInputStream(path);

				SAXParser      saxParser = factory.newSAXParser();
				LoaderDataSourceHandler handler   = new LoaderDataSourceHandler();
				saxParser.parse(xmlInput, handler);
				
				billingServiceSourceInfo	= handler.getBillingServiceSourceInfo();
				handler.cleanBillingServiceSourceInfo();
				
				
			} 
			catch (SAXException se) {
				System.out.println("SAX exception: " + se.getMessage());
			}catch (IOException ioe) {
				System.out.println("IO exception: " + ioe.getMessage());
			} catch (ParserConfigurationException e) {
				System.out.println("ParserConfiguration exception: " + e.getMessage());
			}
			
			return billingServiceSourceInfo;
		}

		/**
		 * *
		 * 
		 * Make AccountingSourceFile from XML 
		 * Validate Structure the File XML and Load AccountingSourceInfo with the information
		 * 
		 * byte[] byteArr = new byte[] { 0xC, 0xA, 0xF, 0xE };
		 * InputStream is = new ByteArrayInputStream(byteArr);.
		 *
		 * @param byteArr the byte arr
		 * @return AccountingSourceInfo
		 * @throws ServiceException the service exception
		 */
		public static BillingServiceSourceInfo loadXMLToObjectSource(byte[] byteArr)throws ServiceException{

			SAXParserFactory factory = SAXParserFactory.newInstance();
			BillingServiceSourceInfo billingServiceSourceInfo = null  ;

			try {
				
				InputStream xmlInput = new ByteArrayInputStream(byteArr);

				SAXParser      saxParser = factory.newSAXParser();
				LoaderDataSourceHandler handler   = new LoaderDataSourceHandler();
				saxParser.parse(xmlInput, handler);
				
				billingServiceSourceInfo	= handler.getBillingServiceSourceInfo();
				handler.cleanBillingServiceSourceInfo();
				
				
			}
			catch (SAXException se) {
				if(se.getMessage().equals(PropertiesConstants.BILLING_SERVICE_SOURCE_WRONG_STRUCTURED)){
					throw new ServiceException(ErrorServiceType.BILLING_SERVICE_SOURCE_WRONG_STRUCTURED);
				}
				else if(se.getMessage().equals(PropertiesConstants.BILLING_SERVICE_SOURCE_VALUE_FORMAT)){
					throw new ServiceException(ErrorServiceType.BILLING_SERVICE_SOURCE_VALUE_FORMAT);
				}
				else if(se.getMessage().equals(PropertiesConstants.BILLING_SERVICE_SOURCE_VISIBILITY_FORMAT)){
					throw new ServiceException(ErrorServiceType.BILLING_SERVICE_SOURCE_VISIBILITY_FORMAT);
				}
				else if(se.getMessage().equals(PropertiesConstants.BILLING_SERVICE_SOURCE_TYPE_FORMAT)){
					throw new ServiceException(ErrorServiceType.BILLING_SERVICE_SOURCE_TYPE_FORMAT);
				}
				else if(se.getMessage().equals(PropertiesConstants.BILLING_SERVICE_SOURCE_POSITION_FORMAT)){
					throw new ServiceException(ErrorServiceType.BILLING_SERVICE_SOURCE_POSITION_FORMAT);
				}
				else if(se.getMessage().equals(PropertiesConstants.BILLING_SERVICE_SOURCE_LEVEL_FORMAT)){
					throw new ServiceException(ErrorServiceType.BILLING_SERVICE_SOURCE_LEVEL_FORMAT);
				}
				se.printStackTrace();
//					System.out.println("SAX exception: " + se.getMessage());
			}
			catch (IOException ioe) {
				ioe.printStackTrace();
//					System.out.println("IO exception: " + ioe.getMessage());
			} catch (ParserConfigurationException e) {
				
				e.printStackTrace();
			}
			
			return billingServiceSourceInfo;
		}
		
		
		
		/**
		 * 3 List to 
		 * GUARDA_Y_CUSTODIA_DE_VALORES_FISICOS
		 * GUARDA_Y_CUSTODIA_DE_VALORES_FISICOS_AFP.
		 *
		 * @return the list
		 */
		public static List custodyPhisicalSecurity(){
			List list=new ArrayList<>();
			list.add(BaseCollectionType.CUSTODY_SECURITIES_PHYSICAL.getCode());
			list.add(BaseCollectionType.CUSTODY_SECURITIES_PHYSICAL_AFP.getCode());
			return list;
		}
		
		/**
		 * 4 List to 
		 * LIQUIDACION_OPERACIONES_BURSATILES_R_FIJA
		 * LIQUIDACION_OPERACIONES_BURSATILES_R_VARIABLE.
		 *
		 * @return the list
		 */
		public static List settlementTrading(){
			List list=new ArrayList<>();
			list.add(BaseCollectionType.SETTLEMENT_SECURITIES_TRANSACTIONS_FIXED_INCOME.getCode());
			list.add(BaseCollectionType.SETTLEMENT_SECURITIES_TRANSACTIONS_VARIABLE_INCOME.getCode());
			return list;
		}
		
		/**
		 * 8 List To
		 * TRANSFERENCIAS_VALORES_SIRTEX_R_FIJA
		 * TRANSFERENCIAS_VALORES_SIRTEX_R_VARIABLE.
		 *
		 * @return the list
		 */
		public static List transfersSecuritySirtex(){
			List list=new ArrayList<>();
			list.add(BaseCollectionType.TRANSFERS_SECURITIES_SIRTEX_FIXED_INCOME.getCode());
			list.add(BaseCollectionType.TRANSFERS_SECURITIES_SIRTEX_VARIABLE_INCOME.getCode());
			return list;
		}
		
		/**
		 * 9
		 * TRANSFERENCIAS_VALORES_OPERACIONES_EXTRABURSATILES_PRIVADAS_R_FIJA
		 * TRANSFERENCIAS_VALORES_OPERACIONES_EXTRABURSATILES_PRIVADAS_R_VARIABLE.
		 *
		 * @return the list
		 */
		public static List transfersOtcTransactions(){
			List list=new ArrayList<>();
			list.add(BaseCollectionType.INDIRECT_PARTICIPANT.getCode());
			list.add(BaseCollectionType.SETTLEMENT_OTC_TRANSACTIONS_VARIABLE_INCOME.getCode());
			return list;
		}
		
		/**
		 * 10
		 * List to 
		 * REGISTRO_VALORES_DESMATERIALIZADOS_R_FIJA
		 * REGISTRO_VALORES_DESMATERIALIZADOS_R_VARIABLE.
		 *
		 * @return the list
		 */
		public static List registerSecurityDematerialized(){
			List list=new ArrayList<>();
			list.add(BaseCollectionType.RECORD_SECURITIES_DEMATERIALIZED_FIXED_INCOME.getCode());
			list.add(BaseCollectionType.RECORD_SECURITIES_DEMATERIALIZED_VARIABLE_INCOME.getCode());
			list.add(BaseCollectionType.RECORD_SECURITIES_DEMATERIALIZED_DPF.getCode());
			return list;
		}
		
		/**
		 * 11
		 * List to 
		 * SOLICITUDES_CAMBIO_TITULARIDAD_R_FIJA
		 * SOLICITUDES_CAMBIO_TITULARIDAD_R_VARIABLE.
		 *
		 * @return the list
		 */
		public static List registerOwnerChange(){
			List list=new ArrayList<>();
			list.add(BaseCollectionType.REQUEST_CHANGE_OWNERSHIP_FIXED_INCOME.getCode());
			list.add(BaseCollectionType.REQUEST_CHANGE_OWNERSHIP_VARIABLE_INCOME.getCode());
			return list;
		}
		
		/**
		 * 15 
		 * List to EMISION_CERTIFICADOS_ACREDITACION.
		 *
		 * @return the list
		 */
		public static List processAccreditationCertificate(){
			List list=new ArrayList<>();
			list.add(BaseCollectionType.ISSUANCE_CAT.getCode());
			return list;
		}
		
		/**
		 * 16
		 * List to SERVICIO_AGENTE_PAGADOR.
		 *
		 * @return the list
		 */
		public static List payingAgent(){
			List list=new ArrayList<>();
			list.add(BaseCollectionType.PAYING_AGENT.getCode());
			return list;
		}
		
		
		
		/**
		 * 17 
		 * List to INSCRIPCION_LEVANTAMIENTO_MEDIDAS_PRECAUTORIAS.
		 *
		 * @return the list
		 */
		public static List requestAffectations(){
			List list=new ArrayList<>();
			list.add(BaseCollectionType.REGISTRATION_PRECAUTIONARY_MEASURES.getCode());
			list.add(BaseCollectionType.LIFTING_PRECAUTIONARY_MEASURES.getCode());
			return list;
		}
		
		
		/**
		 * 18
		 * REMATERIALIZACION_REVERSION_ANOTACIONES_CUENTA_R_FIJA
		 * REMATERIALIZACION_REVERSION_ANOTACIONES_CUENTA_R_FIJA.
		 *
		 * @return the list
		 */
		public static List rematerializationAnnotations(){
			List list=new ArrayList<>();
			list.add(BaseCollectionType.TRANSACTION_CHARGE.getCode());
			list.add(BaseCollectionType.REMATERIALIZATION_OR_REVERSAL_ACCOUNT_ENTRIES_VARIABLE_INCOME.getCode());
			return list;
		}
		
	
		
		/**
		 * 3 Is
		 * 
		 * GUARDA_Y_CUSTODIA_DE_VALORES_FISICOS
		 * GUARDA_Y_CUSTODIA_DE_VALORES_FISICOS_AFP.
		 *
		 * @param baseCollection the base collection
		 * @return the boolean
		 */
		public static Boolean isCustodyPhisicalSecurity(Integer baseCollection ){
			
			if(Validations.validateIsNotNullAndPositive(baseCollection)&&
					custodyPhisicalSecurity().contains(baseCollection)){
				return Boolean.TRUE;
			}
			return Boolean.FALSE;
		}
		
		/**
		 * 4 Is
		 * 
		 * LIQUIDACION_OPERACIONES_BURSATILES_R_FIJA
		 * LIQUIDACION_OPERACIONES_BURSATILES_R_VARIABLE.
		 *
		 * @param baseCollection the base collection
		 * @return the boolean
		 */
		public static Boolean isBaseSettlementTrading(Integer baseCollection ){
			
			if(Validations.validateIsNotNullAndPositive(baseCollection)&&
					settlementTrading().contains(baseCollection)){
				return Boolean.TRUE;
			}
			return Boolean.FALSE;
		}
		
		/**
		 * 8 Is 
		 * TRANSFERENCIAS_VALORES_SIRTEX_R_FIJA
		 * TRANSFERENCIAS_VALORES_SIRTEX_R_VARIABLE.
		 *
		 * @param baseCollection the base collection
		 * @return the boolean
		 */
		public static Boolean isTransfersSecuritySirtex(Integer baseCollection ){
			
			if(Validations.validateIsNotNullAndPositive(baseCollection)&&
					transfersSecuritySirtex().contains(baseCollection)){
				return Boolean.TRUE;
			}
			return Boolean.FALSE;
		}
		
		/**
		 * 9 Is
		 * 
		 * TRANSFERENCIAS_VALORES_OPERACIONES_EXTRABURSATILES_PRIVADAS_R_FIJA
		 * TRANSFERENCIAS_VALORES_OPERACIONES_EXTRABURSATILES_PRIVADAS_R_VARIABLE.
		 *
		 * @param baseCollection the base collection
		 * @return the boolean
		 */
		public static Boolean isTransfersOtcTransactions(Integer baseCollection ){
			
			if(Validations.validateIsNotNullAndPositive(baseCollection)&&
					transfersOtcTransactions().contains(baseCollection)){
				return Boolean.TRUE;
			}
			return Boolean.FALSE;
		}
	
		/**
		 * 10 Is
		 * REGISTRO_VALORES_DESMATERIALIZADOS_R_FIJA
		 * REGISTRO_VALORES_DESMATERIALIZADOS_R_VARIABLE.
		 *
		 * @param baseCollection the base collection
		 * @return the boolean
		 */
		public static Boolean isRegisterSecurityDematerialized(Integer baseCollection ){
			
			if(Validations.validateIsNotNullAndPositive(baseCollection)&&
					registerSecurityDematerialized().contains(baseCollection)){
				return Boolean.TRUE;
			}
			return Boolean.FALSE;
		}
		
		

		/**
		 * 11 Is
		 * SOLICITUDES_CAMBIO_TITULARIDAD_R_FIJA
		 * SOLICITUDES_CAMBIO_TITULARIDAD_R_VARIABLE.
		 *
		 * @param baseCollection the base collection
		 * @return the boolean
		 */
		public static Boolean isRegisterOwnerChange(Integer baseCollection ){
	
			if(Validations.validateIsNotNullAndPositive(baseCollection)&&
					registerOwnerChange().contains(baseCollection)){
				return Boolean.TRUE;
			}
			return Boolean.FALSE;
		}
		
		/**
		 * 15
		 * Is EMISION_CERTIFICADOS_ACREDITACION.
		 *
		 * @param baseCollection the base collection
		 * @return the boolean
		 */
		public static Boolean isProcessAccreditationCertificate(Integer baseCollection ){
			
			if(Validations.validateIsNotNullAndPositive(baseCollection)&&
					processAccreditationCertificate().contains(baseCollection)){
				return Boolean.TRUE;
			}
			return Boolean.FALSE;
		}
		
		/**
		 * 16
		 * Is SERVICIO_AGENTE_PAGADOR.
		 *
		 * @param baseCollection the base collection
		 * @return the boolean
		 */
		public static Boolean isProcessPayingAgent(Integer baseCollection ){
			
			if(Validations.validateIsNotNullAndPositive(baseCollection)&&
					payingAgent().contains(baseCollection)){
				return Boolean.TRUE;
			}
			return Boolean.FALSE;
		}
		

		/**
		 * 17
		 * Is INSCRIPCION_LEVANTAMIENTO_MEDIDAS_PRECAUTORIAS.
		 *
		 * @param baseCollection the base collection
		 * @return the boolean
		 */
		public static Boolean isRequestAffectations(Integer baseCollection ){
			
			if(Validations.validateIsNotNullAndPositive(baseCollection)&&
					requestAffectations().contains(baseCollection)){
				return Boolean.TRUE;
			}
			return Boolean.FALSE;
		}
		
		/**
		 *  
		 * 18 Is
		 * 
		 * REMATERIALIZACION_REVERSION_ANOTACIONES_CUENTA_R_FIJA
		 * REMATERIALIZACION_REVERSION_ANOTACIONES_CUENTA_R_VARIABLE.
		 *
		 * @param baseCollection the base collection
		 * @return the boolean
		 */
		public static Boolean isRematerializationAnnotations(Integer baseCollection ){
			
			if(Validations.validateIsNotNullAndPositive(baseCollection)&&
					rematerializationAnnotations().contains(baseCollection)){
				return Boolean.TRUE;
			}
			return Boolean.FALSE;
		}
		
		/**
		 * Gets the parameter description.
		 *
		 * @param listParameter the list parameter
		 * @param idParameter the id parameter
		 * @return the parameter description
		 */
		public static String getParameterDescription(List<ParameterTable> listParameter, Integer idParameter){
			
			if(Validations.validateListIsNotNullAndNotEmpty(listParameter)){
				for (ParameterTable parameter : listParameter) {
					if(parameter.getParameterTablePk().equals(idParameter)){
						
						return parameter.getDescription();
					}
				}
			}
			
			return GeneralConstants.BLANK_SPACE;
		}
		
		/**
		 * Gets the parameter name.
		 *
		 * @param listParameter the list parameter
		 * @param idParameter the id parameter
		 * @return the parameter name
		 */
		public static String getParameterName(List<ParameterTable> listParameter, Integer idParameter){
			
			if(Validations.validateListIsNotNullAndNotEmpty(listParameter)){
				for (ParameterTable parameter : listParameter) {
					if(parameter.getParameterTablePk().equals(idParameter)){
						
						return parameter.getParameterName();
					}
				}
			}
			
			return GeneralConstants.BLANK_SPACE;
		}

		
		public static List<Integer> getBillingEconomicActivityListInteger(List<BillingEconomicActivity> listBillingEconomicActivityBD){
			List<Integer> listEconomic=new ArrayList<>();
			listEconomic.add(0);//parar evitar que envie lista vacia
			if(Validations.validateListIsNotNullAndNotEmpty(listBillingEconomicActivityBD)) {
				for(BillingEconomicActivity select:listBillingEconomicActivityBD) {
					listEconomic.add(select.getEconomicActivity()!=null?select.getEconomicActivity():null);
					
				}
			}
			
			return listEconomic;
		}

		
		public static List<Integer> getBillingEconomicActivityListString(List<BillingEconomicActivity> listBillingEconomicActivityBD){
			List<Integer> listEconomic=new ArrayList<>();
			if(Validations.validateListIsNotNullAndNotEmpty(listBillingEconomicActivityBD)) {
				for(BillingEconomicActivity select:listBillingEconomicActivityBD) {
					listEconomic.add(select.getEconomicActivity()!=null?select.getEconomicActivity():null);
					
				}
			}
			
			return listEconomic;
		}
		public static List<Integer> isSafiEconomicActivity(){
			List<Integer> list= new ArrayList<>();
			list.add(EconomicActivityType.SOCIEDADES_ADMINISTRADORAS_FONDOS_INVERSION.getCode());
			list.add(EconomicActivityType.FONDOS_INVERSION_ABIERTOS.getCode());
			list.add(EconomicActivityType.FONDOS_INVERSION_CERRADOS.getCode());
			list.add(EconomicActivityType.FONDOS_INVERSION_MIXTOS.getCode());
			return list;
		}
		
		
		public static Boolean isSafiPortfolioClient(List<Integer> economicActivityList, Integer portfolio ){
			
			if(portfolio!=null && GeneralConstants.ZERO_VALUE_INTEGER.equals(portfolio)) {
				if(!Collections.disjoint(economicActivityList, isSafiEconomicActivity())) {
					
					return Boolean.TRUE;
				}
			}
			
			return Boolean.FALSE;
		}
		


		public static Boolean isAfpPortfolioClient(List<Integer> economicActivityList, Integer portfolio ){
			
			if(portfolio!=null && GeneralConstants.ZERO_VALUE_INTEGER.equals(portfolio)) {
				if(economicActivityList.contains(EconomicActivityType.ADMINISTRADORAS_FONDOS_PENSIONES.getCode())) {
					return Boolean.TRUE;
				}
			}
			
			return Boolean.FALSE;
		}


		public static Boolean isBaseCollectionOneAfp(List<Integer> economicActivityList, Integer baseCollection ){
			
			if(baseCollection!=null && (BaseCollectionType.RECORD_BOOK_ENTRY_PHYSICAL_CUSTODY_USD.getCode().equals(baseCollection)||
					BaseCollectionType.RECORD_BOOK_ENTRY_PHYSICAL_CUSTODY_LOCAL.getCode().equals(baseCollection)||
					BaseCollectionType.RECORD_BOOK_ENTRY_SECURITIES_VARIABLE_INCOME.getCode().equals(baseCollection))) {
				if(economicActivityList.contains(EconomicActivityType.ADMINISTRADORAS_FONDOS_PENSIONES.getCode())) {
					return Boolean.TRUE;
				}
			}
			
			return Boolean.FALSE;
		}
}
