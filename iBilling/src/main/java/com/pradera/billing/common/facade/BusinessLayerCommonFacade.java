package com.pradera.billing.common.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.apache.commons.lang3.SerializationUtils;

import com.pradera.billing.billing.to.BillingProcessedFilter;
import com.pradera.billing.billingcollection.to.CollectionRecordDetailTo;
import com.pradera.billing.billingcollection.to.CollectionRecordTo;
import com.pradera.billing.billingcollection.to.DailyExchangeRateFilter;
import com.pradera.billing.billingexception.to.IssuerTo;
import com.pradera.billing.billingrate.to.RateMovementTo;
import com.pradera.billing.billingrate.to.ServiceRateScaleTo;
import com.pradera.billing.billingrate.to.ServiceRateTo;
import com.pradera.billing.billingservice.service.BillingServiceMgmtServiceBean;
import com.pradera.billing.billingservice.to.BillingServiceTo;
import com.pradera.billing.billingsource.facade.SourceBillingServiceFacade;
import com.pradera.billing.billingsource.to.BillingServiceSourceInfo;
import com.pradera.billing.billingsource.to.BillingServiceSourceLoggerTo;
import com.pradera.billing.common.service.BusinessLayerCommonServiceBean;
import com.pradera.billing.common.utils.BillingUtils;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.security.model.type.ExternalInterfaceType;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.core.framework.entity.service.LoaderEntityServiceBean;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.type.EconomicActivityType;
import com.pradera.model.billing.BillingCalculation;
import com.pradera.model.billing.BillingCorrelative;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.BillingServiceSource;
import com.pradera.model.billing.CollectionRecord;
import com.pradera.model.billing.CollectionRecordDetail;
import com.pradera.model.billing.ServiceRate;
import com.pradera.model.billing.ServiceRateScale;
import com.pradera.model.billing.type.BaseCollectionType;
import com.pradera.model.billing.type.BillingServicePortfolioType;
import com.pradera.model.billing.type.RateType;
import com.pradera.model.component.MovementType;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.DailyExchangeRoleType;
import com.pradera.model.process.BusinessProcess;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BusinessLayerCommonFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class BusinessLayerCommonFacade {

		
	/** The billing service mgmt service bean. */
	@EJB
	private BillingServiceMgmtServiceBean 	billingServiceMgmtServiceBean;
	
	/** The bussiness layer common service bean. */
	@EJB
	private BusinessLayerCommonServiceBean 	bussinessLayerCommonServiceBean;
	
	/** The notification service facade. */
	@EJB 
	NotificationServiceFacade 				notificationServiceFacade;

	/** The holiday query service bean. */
	@EJB
	HolidayQueryServiceBean 				holidayQueryServiceBean;
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry 		transactionRegistry;
	

	 /** The source billing service facade. */
 	@EJB
	 SourceBillingServiceFacade sourceBillingServiceFacade;
	 
	 
	/** The client rest service. */
	@Inject
	ClientRestService 							clientRestService;
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The loader entity service. */
	@EJB
	LoaderEntityServiceBean 						loaderEntityService;

	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	
	/**
	 * Get Value Parameter Table by Key.
	 *
	 * @param parameterTableList the parameter table list
	 * @param key the key
	 * @return the value parameter table
	 */
	public String getValueParameterTable(List<ParameterTable> parameterTableList ,Integer key ){
		String name="";
			for (ParameterTable parameterTable : parameterTableList) {
				if (parameterTable.getParameterTablePk().intValue()==key.intValue()) {
					name=parameterTable.getDescription();
				}
			}
		return name;
	}

	/**
	 * Gets the list parameter table.
	 *
	 * @param filter the filter
	 * @return the list parameter table
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getListParameterTable(ParameterTableTO filter) throws ServiceException {		
		return billingServiceMgmtServiceBean.getListParameterTable(filter);
	}
	
	/**
	 * Find movement types service bean.
	 *
	 * @param movementTypeSearch the movement type search
	 * @return the list
	 */
	public List<MovementType>  findMovementTypesServiceBean(MovementType movementTypeSearch){		
		
		List<MovementType> listMovementType=null;;
		List<Object[]> lstResult =bussinessLayerCommonServiceBean.findMovementTypesServiceBean(movementTypeSearch) ;		
		MovementType movementType;
		if (Validations.validateListIsNotNullAndNotEmpty(lstResult)){
			listMovementType= new ArrayList<MovementType>();
			
			for(Object[] objResult:lstResult){
				if(	Validations.validateIsNotNullAndNotEmpty(objResult[0]) &&     // 	CODE MOVEMENT
						Validations.validateIsNotNullAndNotEmpty(objResult[1]) && //	DESCRIPTION MOVEMENT
						Validations.validateIsNotNullAndNotEmpty(objResult[2]) )	  //	DESCRIPTION MODULE
				{	movementType = new MovementType();
				    movementType.setIdMovementTypePk(Long.parseLong(objResult[0].toString()));
				    movementType.setMovementName(objResult[1].toString());				    
				    movementType.setDescriptionModuleType(objResult[2].toString());
					listMovementType.add(movementType);
				}							
			}
			
		}
		return listMovementType;
	}
	
	/**
	 * Find movement type by code.
	 *
	 * @param idMovementTypePk the id movement type pk
	 * @return the movement type
	 * @throws ServiceException the service exception
	 */
	public MovementType findMovementTypeByCode(Long idMovementTypePk) throws ServiceException{
		return bussinessLayerCommonServiceBean.findMovementTypeByCode(idMovementTypePk);
	}
	
	
	/**
	 * Adds the service rate to.
	 *
	 * @param billingServiceTo the billing service to
	 * @param serviceRateList the service rate list
	 */
	public void addServiceRateTo(BillingServiceTo billingServiceTo , List<ServiceRate> serviceRateList){
		
		ServiceRateTo serviceRateTo;
		for (ServiceRate serviceRate : serviceRateList) {
			serviceRateTo = new ServiceRateTo();
			serviceRateTo.setRateType(serviceRate.getRateType());
			serviceRateTo.setIdServiceRatePk(serviceRate.getIdServiceRatePk());
			serviceRateTo.setRateCode(serviceRate.getRateCode());
			serviceRateTo.setRateName(serviceRate.getRateName());
			serviceRateTo.setRateStatus(serviceRate.getRateState());
			serviceRateTo.setCurrencyRate(serviceRate.getCurrencyRate());
			serviceRateTo.setDescriptionStatus(serviceRate.getDescriptionStatus());
			serviceRateTo.setEndEffectiveDate(serviceRate.getEndEffectiveDate());
			serviceRateTo.setInitialEffectiveDate(serviceRate.getInitialEffectiveDate());
			serviceRateTo.setDescriptionType(serviceRate.getDescriptionType());
			serviceRateTo.setRateAmount(serviceRate.getRateAmount());
			
			/**Amount minimum*/
			serviceRateTo.setIndAmountMinimum(serviceRate.getIndMinimumAmount());
			serviceRateTo.setRateMinimum(serviceRate.getMinimumAmount());
			serviceRateTo.setCurrencyMinimum(serviceRate.getCurrencyMinimumAmount());
			
			serviceRateTo.setIndScaleMovement(serviceRate.getIndScaleMovement());
			BigDecimal getRatePercentTo = BillingUtils.multiplyDecimalInteger(serviceRate.getRatePercent(), 100, GeneralConstants.ROUNDING_DIGIT_NUMBER_FIFTY);
			serviceRateTo.setRatePercent(getRatePercentTo);
			
			if( serviceRateTo.getRateType().equals(RateType.TIPO_MOVIMIENTO_FIJO.getCode()) ||
				serviceRateTo.getRateType().equals(RateType.TIPO_MOVIMIENTO_PORCENTUAL.getCode()) ){
				if (Validations.validateListIsNotNullAndNotEmpty(serviceRate.getRateMovements())){
					serviceRateTo.setListRateMovementTo(getListRateMovementTo(serviceRate));	
				 }

			}  
			
			else if( serviceRateTo.getRateType().equals(RateType.ESCALONADO_FIJO.getCode()) ||
					 serviceRateTo.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode()) ||
					 serviceRateTo.getRateType().equals(RateType.ESCALONADO_MIXTO.getCode()) ) {			
				if (Validations.validateListIsNotNullAndNotEmpty(serviceRate.getServiceRateScales())){


					serviceRateTo.setListServiceRateScaleTo(getListServiceRateScaleTo(serviceRate));
					serviceRateTo.setAccumulate(serviceRate.getServiceRateScales().get(0).getIndAccumulativeScale());
					
					if(serviceRate.getIndScaleMovement().equals(GeneralConstants.ONE_VALUE_INTEGER)){

						if (Validations.validateListIsNotNullAndNotEmpty(serviceRate.getRateMovements())){
							
							serviceRateTo.setListRateMovementTo(getListRateMovementTo(serviceRate));
									
						 }

					
					}
					
				 }
			}
						
			billingServiceTo.getListServiceRateTo().add(serviceRateTo);
		}
	}
	
	/**
	 * Get ListRateMovementTo.
	 *
	 * @param serviceRate the service rate
	 * @return List<RateMovementTo>
	 */
	public List<RateMovementTo> getListRateMovementTo(ServiceRate serviceRate){
		
		List<RateMovementTo> listRateMovementTo = new ArrayList<RateMovementTo>();
		RateMovementTo rateMovementTo;
		MovementType  movementTypeNew;
		for (int i=0; i<serviceRate.getRateMovements().size();i++){
			rateMovementTo = new RateMovementTo();
			MovementType movementType =serviceRate.getRateMovements().get(i).getMovementType();
			movementTypeNew = new MovementType();
			movementTypeNew.setIdMovementTypePk(movementType.getIdMovementTypePk());
			movementTypeNew.setMovementName(movementType.getMovementName());
			movementTypeNew.setIndBilling(movementType.getIndBilling());
			rateMovementTo.setMovementType(movementTypeNew);
			rateMovementTo.setRateAmount(serviceRate.getRateMovements().get(i).getRateAmount());						
			BigDecimal ratePercentTo = BillingUtils.multiplyDecimalInteger(serviceRate.getRateMovements().get(i).getRatePercent(), 100, GeneralConstants.ROUNDING_DIGIT_NUMBER);
			rateMovementTo.setRatePercent(ratePercentTo);
			
			listRateMovementTo.add(rateMovementTo);
		 }
		return listRateMovementTo;		
	 
	}
	
	/**
	 * Get  ListServiceRateScaleTo.
	 *
	 * @param serviceRate the service rate
	 * @return List<ServiceRateScaleTo>
	 */
	public List<ServiceRateScaleTo> getListServiceRateScaleTo(ServiceRate serviceRate){
		List<ServiceRateScaleTo> listServiceRateScaleTo = new ArrayList<ServiceRateScaleTo>();
		ServiceRateScaleTo serviceRateScaleToNew;
		for (ServiceRateScale serviceRateScale: serviceRate.getServiceRateScales()) {
			serviceRateScaleToNew = new ServiceRateScaleTo();
			serviceRateScaleToNew.setSequenceCode(serviceRateScale.getScaleOrder());
			serviceRateScaleToNew.setIdServiceRateScalePk(serviceRateScale.getIdServiceRateScalePk());						
			serviceRateScaleToNew.setMinimumRange(serviceRateScale.getMinimumRange());
			serviceRateScaleToNew.setMaximumRange(serviceRateScale.getMaximumRange());
			serviceRateScaleToNew.setMinimumCollectionAmount(serviceRateScale.getMinimumCollectionAmount());
			serviceRateScaleToNew.setMaximumCollectionAmount(serviceRateScale.getMaximumCollectionAmount());
			serviceRateScaleToNew.setScaleAmount(serviceRateScale.getScaleAmount());
			serviceRateScaleToNew.setIndAccumulativeScale(serviceRateScale.getIndAccumulativeScale());
			BigDecimal ratePercentStaggeredTo=null;
			if(Validations.validateIsNotNull(serviceRateScale.getScalePercent())){
				ratePercentStaggeredTo = BillingUtils.multiplyDecimalInteger(serviceRateScale.getScalePercent(), 100, GeneralConstants.ROUNDING_DIGIT_NUMBER);
			}
			serviceRateScaleToNew.setScalePercent(ratePercentStaggeredTo); 
			serviceRateScaleToNew.setIndStaging(serviceRateScale.getIndStaging());
			serviceRateScaleToNew.setScaleType(serviceRateScale.getScaleType());
			
			listServiceRateScaleTo.add(serviceRateScaleToNew);
		}
		return listServiceRateScaleTo;
	}
	
	public Map<String,DailyExchangeRates> searchAndMapExchangeMoneyTypeRandomListService(DailyExchangeRateFilter dailyExchangerate) throws ServiceException {
		return this.bussinessLayerCommonServiceBean.searchAndMapExchangeMoneyTypeRandomListService(dailyExchangerate);
	}
	
	public DailyExchangeRates getExchRateFromMap(Map<String,DailyExchangeRates> mapExchRates, Date searchDate, Boolean modifyMap) {
		return this.bussinessLayerCommonServiceBean.getExchRateFromMap(mapExchRates, searchDate, modifyMap);
	}
	/**
	 * *.
	 *
	 * @param listCollectionRecordTo the list collection record to
	 * @return the ting exchange by range
	 */
	public TreeSet<Date> gettingExchangeByRange(List<CollectionRecordTo> listCollectionRecordTo){
		TreeSet<Date> listOrder=new TreeSet<Date>();
		for (CollectionRecordTo collection : listCollectionRecordTo) {
			for (CollectionRecordDetailTo detail : collection.getCollectionRecordDetailToList()) {
				listOrder.add(detail.getCalculationDate());
			}
		}
		
		return listOrder;
		
	}
	
	/**
	 * Matrix to Exchange Rate by Range, to Billing.
	 *
	 * @param listCollectionRecordTo the list collection record to
	 * @param dataMaps the data maps
	 * @param indexDate the index date
	 * @param parameters the parameters
	 * @param buySell the buy sell
	 * @return the ting exchange by range date
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("rawtypes")
	public Map<Date,Map> gettingExchangeByRangeDate(List<CollectionRecordTo> listCollectionRecordTo,
													List<Object> dataMaps,Integer indexDate, 
													Map parameters, Integer buySell) throws ServiceException{
		BillingService 	billingService 	= (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		TreeSet<Date> listDateCalculation=null;
		listDateCalculation=new TreeSet<Date>();
		if(Validations.validateListIsNotNullAndNotEmpty(listCollectionRecordTo)){
			listDateCalculation = gettingExchangeByRange(listCollectionRecordTo);
		}else if(Validations.validateListIsNotNullAndNotEmpty(dataMaps)){
			for (Object object : dataMaps) {
				Date       operationDate   		= (Date)(( (Object[])object )[indexDate]);
				listDateCalculation.add(CommonsUtilities.truncateDateTime(operationDate));
			}
		}
		
		Map<Date,Map> exchangeRangeByDate= new HashMap<Date,Map>();
		Map<Integer,Map> matrixExchange=null;
		
		DailyExchangeRateFilter dailyExchangeRateFilter=new DailyExchangeRateFilter();
		dailyExchangeRateFilter.setIdSourceinformation(billingService.getSourceInformation());
		
		for (Date date : listDateCalculation) {
			dailyExchangeRateFilter.setInitialDate((date==null?null:CommonsUtilities.truncateDateTime(date) ));
			dailyExchangeRateFilter.setFinalDate((date==null?null:CommonsUtilities.truncateDateTime(date) ));
			matrixExchange=chargeExchangeMemoryBuyBs(dailyExchangeRateFilter, buySell);
			exchangeRangeByDate.put(date, matrixExchange);
		}
		
		return exchangeRangeByDate;
	}
	
	/**
	 * Process exchange to range.
	 *
	 * @param parameters the parameters
	 * @param operationDate the operation date
	 * @param origin the origin
	 * @param target the target
	 * @param type the type
	 * @return the big decimal
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public BigDecimal processExchangeToRange(Map parameters, Date operationDate,
												Integer origin, Integer target, Integer type){
		Map<Date,Map> 		exchangeRange	=(Map<Date,Map>) parameters.get(GeneralConstants.MATRIX_EXCHANGE_RATE_RANGE);
		Map<Integer,Map> 	matrixExchange	=(Map<Integer,Map>)exchangeRange.get(operationDate);
		BigDecimal 			price			=null;
		
		if(matrixExchange != null && !matrixExchange.isEmpty()){
			
			Map<Integer,BigDecimal> listCurrencyTarget=(Map<Integer,BigDecimal>)matrixExchange.get(origin);
			
			if(listCurrencyTarget!=null && !listCurrencyTarget.isEmpty()){

				price=(BigDecimal)listCurrencyTarget.get(target);
				
			}
				
		}
		return price;
	}

	
	/**
	 * For Calculation
	 * Get Matrix with Exchange Currency Price Buy.
	 *
	 * @param dailyExchangeRateFilter the daily exchange rate filter
	 * @param buySell 1 Buy 2 Sell
	 * @return the map
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("rawtypes")
	public Map<Integer,Map>  chargeExchangeMemoryBuyBs(DailyExchangeRateFilter dailyExchangeRateFilter, Integer buySell) throws ServiceException{
		
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		Map<Integer,BigDecimal> changeTarget= new HashMap<Integer,BigDecimal>();
		Map<Integer,Map> exchangeMemory= new HashMap<Integer,Map>();
		BigDecimal price=null,buyPriceTemp=null,buyPriceTempBs=null;

		parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
		parameterTableTO.setMasterTableFk(PropertiesConstants.CURRENCY_TYPE_PK);
		List<ParameterTable> listCurrency=getListParameterTable(parameterTableTO);
		
		for (ParameterTable origin : listCurrency) {

			Integer currencyOrigin=origin.getIndicator4();
			changeTarget= new HashMap<Integer,BigDecimal>();
				for (ParameterTable target : listCurrency) {

					buyPriceTemp = BigDecimal.ONE;
					buyPriceTempBs = BigDecimal.ONE;
					Integer currencyTarget=target.getIndicator4();
					if(!currencyOrigin.equals(currencyTarget)){
						if(!currencyOrigin.equals(CurrencyType.PYG.getCode())){
							 
							 dailyExchangeRateFilter.setIdCurrency(currencyOrigin);
							 buyPriceTemp = gettingBuyPrice(dailyExchangeRateFilter);
							 
							 if(!currencyTarget.equals(CurrencyType.PYG.getCode())){
								 dailyExchangeRateFilter.setIdCurrency(currencyTarget);
								 buyPriceTempBs = gettingBuyPrice(dailyExchangeRateFilter);
								 buyPriceTempBs=BillingUtils.divideTwoDecimal(BigDecimal.ONE, buyPriceTempBs, GeneralConstants.ROUNDING_DIGIT_NUMBER_FIFTY);
							 }
							 price=BillingUtils.multiplyDecimals(buyPriceTemp, buyPriceTempBs, GeneralConstants.ROUNDING_DIGIT_NUMBER_FIFTY);
						 }else{
							 dailyExchangeRateFilter.setIdCurrency(currencyTarget);
							 buyPriceTemp = gettingBuyPrice(dailyExchangeRateFilter);
							 price=BillingUtils.divideTwoDecimal(BigDecimal.ONE, buyPriceTemp, GeneralConstants.ROUNDING_DIGIT_NUMBER_FIFTY);
						 }
					}else{
						price =new BigDecimal(1) ; /** FACTOR 1**/
					}
					changeTarget.put(target.getParameterTablePk(), price);
				}
				exchangeMemory.put(origin.getParameterTablePk(), changeTarget);
			}

		return exchangeMemory;
		
	}
	
	/**
	 * Get Matrix with Exchange Currency Price Sell.
	 *
	 * @param dailyExchangeRateFilter the daily exchange rate filter
	 * @param buySell 1 Buy 2 Sell
	 * @return the map
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("rawtypes")
	public Map<Integer,Map>  chargeExchangeMemorySelllBs(DailyExchangeRateFilter dailyExchangeRateFilter, Integer buySell) throws ServiceException{
		
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		Map<Integer,BigDecimal> monedaOrigen= new HashMap<Integer,BigDecimal>();
		Map<Integer,Map> exchangeMemory= new HashMap<Integer,Map>();
		BigDecimal price=null,buyPriceTemp=null,buyPriceTempBs=null;

		parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
		parameterTableTO.setMasterTableFk(PropertiesConstants.CURRENCY_TYPE_PK);
		List<ParameterTable> listCurrency=getListParameterTable(parameterTableTO);
		
		for (ParameterTable origin : listCurrency) {

			Integer currencyOrigin=origin.getIndicator4();
			monedaOrigen= new HashMap<Integer,BigDecimal>();
				for (ParameterTable target : listCurrency) {

					buyPriceTemp = BigDecimal.ONE;
					buyPriceTempBs = BigDecimal.ONE;
					Integer currencyTarget=target.getIndicator4();
					if(!currencyOrigin.equals(currencyTarget)){
						if(!currencyOrigin.equals(CurrencyType.PYG.getCode())){
							 
							 dailyExchangeRateFilter.setIdCurrency(currencyOrigin);
							 buyPriceTemp = gettingSellPrice(dailyExchangeRateFilter);
							 
							 if(!currencyTarget.equals(CurrencyType.PYG.getCode())){
								 dailyExchangeRateFilter.setIdCurrency(currencyTarget);
								 buyPriceTempBs = gettingSellPrice(dailyExchangeRateFilter);
								 buyPriceTempBs=BillingUtils.divideTwoDecimal(BigDecimal.ONE, buyPriceTempBs, GeneralConstants.ROUNDING_DIGIT_NUMBER_FIFTY);
							 }
							 price=BillingUtils.multiplyDecimals(buyPriceTemp, buyPriceTempBs, GeneralConstants.ROUNDING_DIGIT_NUMBER_FIFTY);
						 }else{
							 dailyExchangeRateFilter.setIdCurrency(currencyTarget);
							 buyPriceTemp = gettingSellPrice(dailyExchangeRateFilter);
							 price=BillingUtils.divideTwoDecimal(BigDecimal.ONE, buyPriceTemp, GeneralConstants.ROUNDING_DIGIT_NUMBER_FIFTY);
						 }
					}else{
						price =new BigDecimal(1) ; /** FACTOR 1**/
					}
					monedaOrigen.put(target.getParameterTablePk(), price);
				}
				exchangeMemory.put(origin.getParameterTablePk(), monedaOrigen);
			}

		return exchangeMemory;
		
	}
	
	/**
	 * Getting Buy Price For Calculation.
	 *
	 * @param tarjetCalc : Target
	 * @param dailyExchangeRateFilter the daily exchange rate filter
	 * @param origin  : Origin
	 * @return the ting buy price for calculation
	 * @throws ServiceException the service exception
	 */
	public BigDecimal gettingBuyPriceForCalculation(Integer tarjetCalc, DailyExchangeRateFilter dailyExchangeRateFilter, 
			Integer origin ) throws ServiceException{
		
		BigDecimal buyPrice = BigDecimal.ONE;
		BigDecimal buyPriceTemp = BigDecimal.ONE;
		BigDecimal buyPriceTempBs = BigDecimal.ONE;
		 

		 /**
		  * Bring to the currency of calculation
		  */
		 /** GETTING DAILY EXCHANGE ***/
		if(Validations.validateIsNotNullAndNotEmpty(origin)){
			 if ( !origin.equals(tarjetCalc)) {
				 /**Si es UFV cambiar a Bs*/
				 if(!origin.equals(CurrencyType.PYG.getCode())){
					 
					 dailyExchangeRateFilter.setIdCurrency(origin);
					 buyPriceTemp = gettingBuyPrice(dailyExchangeRateFilter);
					 
					 if(!tarjetCalc.equals(CurrencyType.PYG.getCode())){
						 dailyExchangeRateFilter.setIdCurrency(tarjetCalc);
						 buyPriceTempBs = gettingBuyPrice(dailyExchangeRateFilter);
						 buyPriceTempBs=BillingUtils.divideTwoDecimal(BigDecimal.ONE, buyPriceTempBs, GeneralConstants.ROUNDING_DIGIT_NUMBER);
					 }
					 buyPrice=BillingUtils.multiplyDecimals(buyPriceTemp, buyPriceTempBs, GeneralConstants.ROUNDING_DIGIT_NUMBER);
				 }else{
					 dailyExchangeRateFilter.setIdCurrency(tarjetCalc);
					 buyPriceTemp = gettingBuyPrice(dailyExchangeRateFilter);
					 buyPrice=BillingUtils.divideTwoDecimal(BigDecimal.ONE, buyPriceTemp, GeneralConstants.ROUNDING_DIGIT_NUMBER);
				 }
				 
				 
			 }else{
				 buyPrice =new BigDecimal(1); /** FACTOR 1**/
			 }
		}
		
		 
		 return buyPrice;
	}
	
	/**
	 * Getting Sell Price For Billing.
	 *
	 * @param currencyBilled the currency billed
	 * @param dailyExchangeRateFilter the daily exchange rate filter
	 * @param currencyCalc the currency calc
	 * @return PriceSeller
	 * @throws ServiceException the service exception
	 */
	public BigDecimal gettingSellPriceForBilling(Integer currencyBilled, DailyExchangeRateFilter dailyExchangeRateFilter, 
												 Integer currencyCalc ) throws ServiceException{
		
		BigDecimal sellPrice = BigDecimal.ONE;
		BigDecimal sellPriceTemp = BigDecimal.ONE;
		BigDecimal sellPriceTempBs = BigDecimal.ONE;
		 

		 /**
		  * Bring to the currency of calculation
		  */
		 /** GETTING DAILY EXCHANGE ***/
		 if ( !currencyCalc.equals(currencyBilled)) {
			 /**Si es UFV cambiar a Bs*/
			 if(!currencyCalc.equals(CurrencyType.PYG.getCode())){
				 dailyExchangeRateFilter.setIdCurrency(currencyCalc);
				 sellPriceTemp = gettingSellPrice(dailyExchangeRateFilter);
				 
				 if(!currencyBilled.equals(CurrencyType.PYG.getCode())){
					 dailyExchangeRateFilter.setIdCurrency(currencyBilled);
					 sellPriceTempBs = gettingSellPrice(dailyExchangeRateFilter);
					 sellPriceTempBs=BillingUtils.divideTwoDecimal(BigDecimal.ONE, sellPriceTempBs, GeneralConstants.ROUNDING_DIGIT_NUMBER);
				 }
				 sellPrice=BillingUtils.multiplyDecimals(sellPriceTemp, sellPriceTempBs, GeneralConstants.ROUNDING_DIGIT_NUMBER);
			 }else{
				 dailyExchangeRateFilter.setIdCurrency(currencyBilled);
				 sellPriceTemp = gettingSellPrice(dailyExchangeRateFilter);
				 sellPrice=BillingUtils.divideTwoDecimal(BigDecimal.ONE, sellPriceTemp, GeneralConstants.ROUNDING_DIGIT_NUMBER);
			 }
			 
			 
		 }else{
			 sellPrice =new BigDecimal(1) ; /** FACTOR 1**/
		 }
		 
		 return sellPrice;
	}
	
	
	/**
	 * Process exchange.
	 *
	 * @param parameters the parameters
	 * @param origin the origin
	 * @param target the target
	 * @param type buySell 1 Buy 2 Sell
	 * @return the big decimal
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public BigDecimal processExchange(Map parameters, Integer origin, Integer target, Integer type){
		BigDecimal price=null;
		
		Map<Integer,Map> matrixExchange=null;
		
		if(DailyExchangeRoleType.BUY.getCode().equals(type)){
			matrixExchange=(Map<Integer,Map>) parameters.get(GeneralConstants.MATRIX_EXCHANGE_RATE_BUY);
		}else if(DailyExchangeRoleType.SELL.getCode().equals(type)){
			matrixExchange=(Map<Integer,Map>) parameters.get(GeneralConstants.MATRIX_EXCHANGE_RATE_SELL);
		}

		if(CurrencyType.DMV.getCode().equals(origin)&&
				CurrencyType.USD.getCode().equals(target)){
			return BigDecimal.ONE;
		}
		if(matrixExchange != null && !matrixExchange.isEmpty()){
			
			Map<Integer,BigDecimal> listCurrencyTarget=(Map<Integer,BigDecimal>)matrixExchange.get(origin);
			
			if(listCurrencyTarget!=null && !listCurrencyTarget.isEmpty()){

				price=(BigDecimal)listCurrencyTarget.get(target);
				
			}
				
		}
		
		return price;
	}
	
	
	/**
	 * The exchange rate at the date of billing.
	 *
	 * @param collection the collection
	 * @param currencyCalculation the currency calculation
	 * @param newCollectionRecordDetail the new collection record detail
	 * @param parameters the parameters
	 * @param role the role
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public void convertCalculationToBilled(BigDecimal collection, Integer currencyCalculation,
															CollectionRecordDetail  newCollectionRecordDetail,
															Map parameters, Integer role) throws ServiceException{
		
		BillingService 	billingService 	= (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		List<Integer> listEconomicAct=(List<Integer>)parameters.get(GeneralConstants.ECONOMIC_ACTIVITY_LIST_INT);
		Integer currencyBilling=billingService.getCurrencyBilling();
		BigDecimal exchangeRate=BigDecimal.ZERO;
		BigDecimal billed=BigDecimal.ZERO;
		
		if(BaseCollectionType.RECORD_BOOK_ENTRY_SECURITIES_FIXED_INCOME_AFP.getCode().equals(billingService.getBaseCollection())||
				BaseCollectionType.RECORD_BOOK_ENTRY_SECURITIES_VARIABLE_INCOME_AFP.getCode().equals(billingService.getBaseCollection())||
				BaseCollectionType.CUSTODY_SECURITIES_PHYSICAL_AFP.getCode().equals(billingService.getBaseCollection())||
				(listEconomicAct.contains(EconomicActivityType.ADMINISTRADORAS_FONDOS_PENSIONES.getCode())&&(
						BaseCollectionType.RECORD_BOOK_ENTRY_PHYSICAL_CUSTODY_USD.getCode().equals(billingService.getBaseCollection())||
						BaseCollectionType.RECORD_BOOK_ENTRY_PHYSICAL_CUSTODY_LOCAL.getCode().equals(billingService.getBaseCollection())||
						BaseCollectionType.RECORD_BOOK_ENTRY_SECURITIES_VARIABLE_INCOME.getCode().equals(billingService.getBaseCollection())
						)&&
						(BillingServicePortfolioType.CLIENT.getCode().equals(billingService.getPortfolio()))
					)){
			exchangeRate= processExchange(parameters, currencyCalculation, currencyBilling, DailyExchangeRoleType.BUY.getCode() );
		}else{
			exchangeRate= processExchange(parameters, currencyCalculation, currencyBilling, role );
		}
		
		billed=	BillingUtils.multiplyDecimals(collection, exchangeRate, GeneralConstants.ROUNDING_DIGIT_NUMBER);
		newCollectionRecordDetail.setRateAmountBilled(billed);
		newCollectionRecordDetail.setExchangeRateCurrent(exchangeRate);
		
	}
	
	/**
	 * The exchange rate at the date of billing.
	 *
	 * @param collectionRecord the collection record
	 * @param parameters the parameters
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public void billingChargesToConvertCurrency(CollectionRecord collectionRecord,
															Map parameters) throws ServiceException{
		
		BillingService 	billingService 	= (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		List<Integer> listEconomicAct=(List<Integer>)parameters.get(GeneralConstants.ECONOMIC_ACTIVITY_LIST_INT);
		Integer currencyBilling=billingService.getCurrencyBilling();
		Integer currencyCalculation =collectionRecord.getCurrencyType();
		BigDecimal exchangeRate=BigDecimal.ZERO;
		BigDecimal collectionAmountBilled=BigDecimal.ZERO;
		BigDecimal grossAmountBilled=BigDecimal.ZERO;
		BigDecimal taxAppliedBilled=BigDecimal.ZERO;
		
		if(BaseCollectionType.RECORD_BOOK_ENTRY_SECURITIES_FIXED_INCOME_AFP.getCode().equals(billingService.getBaseCollection())||
				BaseCollectionType.RECORD_BOOK_ENTRY_SECURITIES_VARIABLE_INCOME_AFP.getCode().equals(billingService.getBaseCollection())||
				BaseCollectionType.CUSTODY_SECURITIES_PHYSICAL_AFP.getCode().equals(billingService.getBaseCollection())||
				(listEconomicAct.contains(EconomicActivityType.ADMINISTRADORAS_FONDOS_PENSIONES.getCode())&&(
				BaseCollectionType.RECORD_BOOK_ENTRY_PHYSICAL_CUSTODY_USD.getCode().equals(billingService.getBaseCollection())||
				BaseCollectionType.RECORD_BOOK_ENTRY_PHYSICAL_CUSTODY_LOCAL.getCode().equals(billingService.getBaseCollection())||
				BaseCollectionType.RECORD_BOOK_ENTRY_SECURITIES_VARIABLE_INCOME.getCode().equals(billingService.getBaseCollection())
				)&&
				(BillingServicePortfolioType.CLIENT.getCode().equals(billingService.getPortfolio()))
					)){
			exchangeRate= processExchange(parameters, currencyCalculation, currencyBilling, DailyExchangeRoleType.BUY.getCode());
		}else{
			exchangeRate= processExchange(parameters, currencyCalculation, currencyBilling, DailyExchangeRoleType.SELL.getCode());
		}
		
		collectionAmountBilled=	BillingUtils.multiplyDecimals(collectionRecord.getCollectionAmount(), exchangeRate, GeneralConstants.ROUNDING_DIGIT_NUMBER);
		grossAmountBilled=		BillingUtils.multiplyDecimals(collectionRecord.getGrossAmount(), exchangeRate, GeneralConstants.ROUNDING_DIGIT_NUMBER);
		taxAppliedBilled=		BillingUtils.multiplyDecimals(collectionRecord.getTaxApplied(), exchangeRate, GeneralConstants.ROUNDING_DIGIT_NUMBER);
		
        /** billing services 50 - 51 and verify leap year */
		int periodoDias = (isLeapYear()) ? 366 : 365;
		//solapamos en calculo para estos servicios
		if (billingService.getServiceCode().equals("50") || billingService.getServiceCode().equals("51")) {
			collectionAmountBilled = BillingUtils.divideDecimalObject(collectionRecord.getCollectionAmount(), periodoDias, GeneralConstants.ROUNDING_DIGIT_NUMBER);
			grossAmountBilled      = BillingUtils.divideDecimalObject(collectionRecord.getGrossAmount(), periodoDias, GeneralConstants.ROUNDING_DIGIT_NUMBER);
			taxAppliedBilled       = BillingUtils.divideDecimalObject(collectionRecord.getTaxApplied(), periodoDias, GeneralConstants.ROUNDING_DIGIT_NUMBER);
		}
		
		collectionRecord.setCollectionAmountBilled(collectionAmountBilled);
		collectionRecord.setGrossAmountBilled(grossAmountBilled);
		collectionRecord.setTaxAppliedBilled(taxAppliedBilled);
		collectionRecord.setExchangeRateCurrent(exchangeRate);
		
	}	
	
	/** verify current leap year */
	public boolean isLeapYear() {
		Calendar anio = Calendar.getInstance();
		GregorianCalendar cal = (GregorianCalendar) GregorianCalendar.getInstance();
		return cal.isLeapYear(anio.get(Calendar.YEAR));
	}
	
	/**
	 * Gets the ting reference price.
	 *
	 * @param dailyExchangeRateFilter the daily exchange rate filter
	 * @return the ting reference price
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public BigDecimal gettingReferencePrice(DailyExchangeRateFilter dailyExchangeRateFilter) throws ServiceException{
		
		return bussinessLayerCommonServiceBean.gettingReferencePrice(dailyExchangeRateFilter);
	}
	
	/**
	 * Gets the ting buy price.
	 *
	 * @param dailyExchangeRateFilter the daily exchange rate filter
	 * @return the ting buy price
	 * @throws ServiceException the service exception
	 */
	public BigDecimal gettingBuyPrice(DailyExchangeRateFilter dailyExchangeRateFilter) throws ServiceException{
		
		return bussinessLayerCommonServiceBean.gettingBuyPrice(dailyExchangeRateFilter);
	}

	/**
	 * Gets the ting sell price.
	 *
	 * @param dailyExchangeRateFilter the daily exchange rate filter
	 * @return the ting sell price
	 * @throws ServiceException the service exception
	 */
	public BigDecimal gettingSellPrice(DailyExchangeRateFilter dailyExchangeRateFilter) throws ServiceException{
	
	return bussinessLayerCommonServiceBean.gettingSellPrice(dailyExchangeRateFilter);
	}
	
	/**
	 * Gets the last sequence generator code.
	 *
	 * @param billingCorrelativeFilter the billing correlative filter
	 * @return the last sequence generator code
	 * @throws ServiceException the service exception
	 */
	public  BillingCorrelative    getLastSequenceGeneratorCode(BillingCorrelative billingCorrelativeFilter) throws ServiceException{
	    return  bussinessLayerCommonServiceBean.getLastSequenceGeneratorCode(billingCorrelativeFilter);
	}
	
	/**
	 * Gets the key entity collection.
	 *
	 * @param collectionRecord the collection record
	 * @param indIntegratedBill the ind integrated bill
	 * @param billingProcessedFilter the billing processed filter
	 * @return the key entity collection
	 */
	public String getKeyEntityCollection( CollectionRecord collectionRecord , Integer indIntegratedBill,BillingProcessedFilter billingProcessedFilter){
		return bussinessLayerCommonServiceBean.getKeyEntityCollection(collectionRecord,indIntegratedBill,billingProcessedFilter);
	}
	
	/**
	 * Setting entity collection.
	 *
	 * @param billingCalculation the billing calculation
	 * @param collectionRecord the collection record
	 */
	public void settingEntityCollection(BillingCalculation billingCalculation , CollectionRecord collectionRecord){
		bussinessLayerCommonServiceBean.settingEntityCollection(billingCalculation, collectionRecord);
	}
	



	/**
	 * Send notification.
	 *
	 * @param businessProcess 		the business process
	 * @param subject 				the subject
	 * @param message 			the message
	 * @param notificationType 	the Notification Type
	 */
	public void sendNotificationManual(BusinessProcess businessProcess,String subject,String message,NotificationType notificationType ){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);	
		List<UserAccountSession> usersAccounts= null;
		usersAccounts = clientRestService.getUsersInformation(null, null, null, null,loggerUser.getUserName(),ExternalInterfaceType.NO.getCode());
		
		notificationServiceFacade.sendNotificationManual(loggerUser.getUserName(), businessProcess, usersAccounts,subject, message, notificationType);
		
	}
	
	/**
	 * Send notification manual.
	 *
	 * @param businessProcess the business process
	 * @param usersAccounts the users accounts
	 * @param subject the subject
	 * @param message the message
	 * @param notificationType the notification type
	 */
	public void sendNotificationManual(BusinessProcess businessProcess,List<UserAccountSession> usersAccounts,String subject,String message,NotificationType notificationType ){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);	
		
		notificationServiceFacade.sendNotificationManual(loggerUser.getUserName(), businessProcess, usersAccounts,subject, message, notificationType);
		
	}
	
	
	/**
	 * Send notification automatic.
	 *
	 * @param businessProcess the business process
	 * @param intitutionCode the intitution code
	 * @param parameters the parameters
	 */
	public void sendNotificationAutomatic(BusinessProcess businessProcess,List<Object> intitutionCode, Object[] parameters){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);	
		notificationServiceFacade.sendNotification(loggerUser.getUserName(), businessProcess, intitutionCode, parameters);
	}
	
	/**
	 * Get Logger User to transaction.
	 *
	 * @return LoggerUser
	 */
	public LoggerUser getLoggerUser(){
		return (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	}
	
	/**
	 * Gets the information entity other.
	 *
	 * @param filter the filter
	 * @return the information entity other
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public CollectionRecordTo getInformationEntityOther(CollectionRecordTo filter) throws ServiceException{
		return bussinessLayerCommonServiceBean.getInformationEntityOther(filter);
	}
	
	/**
	 * Gets the geographic location for issuer.
	 *
	 * @param filter the filter
	 * @return the geographic location for issuer
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public IssuerTo getGeographicLocationForIssuer(String filter){
		return bussinessLayerCommonServiceBean.getGeographicLocationForIssuer(filter);
	}
	
	/**
	 * Gets the list usser by business process.
	 *
	 * @param businessProcess the business process
	 * @return the list usser by business process
	 */
	public List<UserAccountSession> getListUsserByBusinessProcess(Long businessProcess){
		return bussinessLayerCommonServiceBean.getListUserByBusinessProcess(businessProcess);
	}
	
	/**
	 * Detach list.
	 *
	 * @param <T> the generic type
	 * @param entities the entities
	 */
	public <T> void detachList(List<T> entities){
		loaderEntityService.detachmentEntities(entities);
	}
	
	/**
	 * Execute Base Collection By File XML.
	 *
	 * @param billingServiceTo the billing service to
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<CollectionRecordTo> executeFileBaseCollection(BillingServiceTo billingServiceTo, Map parameters) throws ServiceException{
		
		BillingServiceSource billingServiceSource=null;
		Date calculateDate= (Date) parameters.get(GeneralConstants.CALCULATION_DATE);
		BillingServiceSourceInfo	billingServiceSourceInfo=new BillingServiceSourceInfo();
		List<BillingServiceSourceLoggerTo> listBillingServiceSourceLoggerTo=null;
		
		try {
			
			billingServiceSource=sourceBillingServiceFacade.findBillingServiceSourceByBillingService(billingServiceTo);
			
			/**Load Entity Billing Service Source with Info The Content File XML */
			billingServiceSourceInfo=sourceBillingServiceFacade.loadAccountingSourceInfoById(billingServiceSource.getIdBillingServiceSourcePk());
			/**Setting Calculation Date */
			billingServiceSourceInfo.setExecutionDate(calculateDate);
			
			listBillingServiceSourceLoggerTo=sourceBillingServiceFacade.fillBillingServiceSourceLogger(parameters,billingServiceSourceInfo);
			

			return sourceBillingServiceFacade.processForBillingSource(parameters,listBillingServiceSourceLoggerTo);
		} catch (ServiceException e) {
			 
			e.printStackTrace();
		}
		return null;
		
	}
	
	/**
	 * Method Applied Minimum Amount to Calculation.
	 *
	 * @param serviceRate Rate
	 * @param amountCompare Amount to Compare with Rate Minimum if Exists
	 * @param parameters the parameters
	 * @return the big decimal
	 * @throws ServiceException the service exception
	 */
	public BigDecimal appliedMinimumAmountCalculation(ServiceRate serviceRate, BigDecimal amountCompare, 
						Map parameters) throws ServiceException{
		
		/**APPLLIED RATE MINIMUM**/
		if(PropertiesConstants.YES_VALUE_INTEGER.equals(serviceRate.getIndMinimumAmount())){
			if(PropertiesConstants.MINIMUM_CALCULATION.equals(serviceRate.getIndMinimumProcess())){
				Integer currencyMinimun=serviceRate.getCurrencyMinimumAmount();
				BigDecimal minimumAmount	=serviceRate.getMinimumAmount();
				BigDecimal minimumAmountNew	= BigDecimal.ONE;
				BigDecimal buyPrice			=BigDecimal.ONE;
				//Si existe monto minimo entonces cobrar el minimo
				
				
				if(amountCompare==null){
					return BigDecimal.ZERO;
				}
				/**Tipo de Cambio del minimo
				 * 6UFV/30= 0.5UFV  a Dolares
				 * */
				/**
				 * if amountCompare > amountCompare return minimumAmountNew
				 */;
				buyPrice=this.processExchange(parameters, currencyMinimun, 
						serviceRate.getCurrencyRate(),DailyExchangeRoleType.SELL.getCode());
				minimumAmountNew=BillingUtils.multiplyDecimals(minimumAmount, buyPrice, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
				if(minimumAmountNew.compareTo(amountCompare)>0){
					return minimumAmountNew;
				}
			}
			
			else if(PropertiesConstants.MINIMUM_BILLING.equals(serviceRate.getIndMinimumAmount())){
				CollectionRecord collectionMinimum=new CollectionRecord();
				collectionMinimum.setIndMinimumAmount(PropertiesConstants.YES_VALUE_INTEGER);
				collectionMinimum.setMinimumAmount(serviceRate.getMinimumAmount());
				collectionMinimum.setCurrencyMinimumAmount(serviceRate.getCurrencyMinimumAmount());
				parameters.put(GeneralConstants.MINIMUM_BILLING, collectionMinimum);
				
				return amountCompare;
			}
			
			
			
		}
		return amountCompare;
	}
	
	/**
	 * Applied minimum amount billing.
	 *
	 * @param amountCompare the amount compare
	 * @param indMinimumAmount the ind minimum amount
	 * @param minimumAmount the minimum amount
	 * @param currencyMinimun the currency minimun
	 * @param parameters the parameters
	 * @param currencyBilling the currency billing
	 * @return the big decimal
	 * @throws ServiceException the service exception
	 */
	public BigDecimal appliedMinimumAmountBilling(BigDecimal amountCompare, Integer indMinimumAmount,
			BigDecimal minimumAmount , Integer currencyMinimun, Map<String, Object> parameters,
			Integer currencyBilling
			) throws ServiceException{

		/**APPLLIED RATE MINIMUM**/
		if(PropertiesConstants.YES_VALUE_INTEGER.equals(indMinimumAmount)){
			BigDecimal minimumAmountNew	= BigDecimal.ONE;
			BigDecimal buyPrice			=BigDecimal.ONE;
			//Si existe monto minimo entonces cobrar el minimo
			
			if(amountCompare==null){
				return BigDecimal.ZERO;
			}

			/**
			 * if amountCompare > amountCompare return minimumAmountNew
			 */;
			buyPrice=this.processExchange(parameters, currencyMinimun, 
					currencyBilling, DailyExchangeRoleType.SELL.getCode());
			minimumAmountNew=BillingUtils.multiplyDecimals(minimumAmount, buyPrice, GeneralConstants.ROUNDING_DIGIT_NUMBER_TWO);
			if(minimumAmountNew.compareTo(amountCompare)>0){
				return minimumAmountNew;
			}
		}
		
		return amountCompare;
		}
	
	/**
	 * *.
	 *
	 * @param collectionRecord the collection record
	 * @param parameters the parameters
	 */
	public void collectionToMinimumBilling(CollectionRecord collectionRecord, Map<String, Object> parameters){
		
		if(Validations.validateIsNotNull(parameters.get(GeneralConstants.MINIMUM_BILLING))){
			CollectionRecord collectionMinimumBill=(CollectionRecord)parameters.get(GeneralConstants.MINIMUM_BILLING);
			collectionRecord.setIndMinimumAmount(collectionMinimumBill.getIndMinimumAmount());
			collectionRecord.setMinimumAmount(collectionMinimumBill.getMinimumAmount());
			collectionRecord.setCurrencyMinimumAmount(collectionMinimumBill.getCurrencyMinimumAmount());
		}
		
	}
	
	
	/**
	 * If the day is not a business.
	 *
	 * @param date : Date consult
	 * @return TRUE if is Holiday
	 * @throws ServiceException the service exception
	 */
	public  Boolean ifDayHoliday(Date date) throws ServiceException{
		
		if(!CommonsUtilities.isUtilDate(date) || holidayQueryServiceBean.isHolidayDateServiceBean(date)){
			return true;
		}
		return false;
	}
	
	
	/**
	 * *
	 * Generate Number to Request .
	 *
	 * @param collectionRecord the collection record
	 * @param collectionRecordTo the collection record to
	 */
	public void addNumberRequestToCollection(CollectionRecord collectionRecord,CollectionRecordTo collectionRecordTo){

		TreeSet<Object> requestList=new TreeSet<>();
		String requestConcatenated="";
		if(Validations.validateListIsNotNullAndNotEmpty(collectionRecordTo.getCollectionRecordDetailToList())){
			for (CollectionRecordDetailTo detail : collectionRecordTo.getCollectionRecordDetailToList()) {
				if(Validations.validateIsNotNull(detail.getTmpRequest())){
					requestList.add(detail.getTmpRequest());
				}else{
					continue;
				}
			}
			requestConcatenated=BillingUtils.convertListObjectToString(requestList);
			collectionRecord.setCountRequest(requestList.size());
			collectionRecord.setRequestConcatenated(requestConcatenated);
			
		}else {
			collectionRecord.setCountRequest(GeneralConstants.ZERO_VALUE_INTEGER);
		}


	}

	
	
	/**
	 * *
	 * Generate MnemonicHolder to Request .
	 *
	 * @param collectionRecord the collection record
	 * @param collectionRecordTo the collection record to
	 */
	public void addMnemonicHolderToCollection(CollectionRecord collectionRecord, CollectionRecordTo collectionRecordTo){

		TreeSet<Object> mnemonicList=new TreeSet<>();
		String mnemonicConcatenated="";
		if(Validations.validateListIsNotNullAndNotEmpty(collectionRecordTo.getCollectionRecordDetailToList())){
			for (CollectionRecordDetailTo detail : collectionRecordTo.getCollectionRecordDetailToList()) {
				if(Validations.validateIsNotNull(detail.getMnemonicHolder())){
					mnemonicList.add(detail.getMnemonicHolder());
				}else{
					continue;
				}
			}
			mnemonicConcatenated=BillingUtils.convertListObjectToString(mnemonicList);
			collectionRecord.setMnemonicConcatenated(mnemonicConcatenated);
			
		}


	}
	

	public List<CollectionRecordTo> generateListForSafi(CollectionRecordTo collectionRecordTo){
		List<CollectionRecordTo> listCollectionRecord=new ArrayList<>();
		TreeSet<String> mnemonicList=new TreeSet<>();
		if(Validations.validateListIsNotNullAndNotEmpty(collectionRecordTo.getCollectionRecordDetailToList())){
			for (CollectionRecordDetailTo detail : collectionRecordTo.getCollectionRecordDetailToList()) {
				if(Validations.validateIsNotNullAndNotEmpty(detail.getMnemonicHolder())){
					mnemonicList.add(detail.getMnemonicHolder());
				}else{
					mnemonicList.add("");
				}
			}
			//Crear n CollectionRecord con sus detalles respectivos
			Iterator<String> itr=mnemonicList.iterator();
			while(itr.hasNext()) {
				String mnemonic=itr.next();
				CollectionRecordTo cloneCollection=SerializationUtils.clone(collectionRecordTo);
				cloneCollection.setMnemonicHolder(mnemonic);
				cloneCollection.setCollectionRecordDetailToList(populateCollectionDetailByMnemonic(cloneCollection, mnemonic));
				listCollectionRecord.add(cloneCollection);
			}
			
		}else {
			listCollectionRecord.add(collectionRecordTo);
		}


		return listCollectionRecord;
	}
	
	public List<CollectionRecordDetailTo> populateCollectionDetailByMnemonic(CollectionRecordTo cloneCollection, String mnemonicHolder){
		List<CollectionRecordDetailTo> detailList=new ArrayList<>();
		for (CollectionRecordDetailTo detail : cloneCollection.getCollectionRecordDetailToList()) {
			if(mnemonicHolder.equals(detail.getMnemonicHolder())) {
				detailList.add(detail);
			}
			if(Validations.validateIsNull(detail.getMnemonicHolder())&&
					GeneralConstants.EMPTY_STRING.equals(mnemonicHolder)
					) {
				detailList.add(detail);
			}
		}
		return detailList;
	}
	

	
	/**
	 * Find entity by code.
	 *
	 * @param <T> the generic type
	 * @param type the type
	 * @param id the id
	 * @return the t
	 */
	public <T> T findEntityByCode(Class<T> type, Object id) {
		return (T) bussinessLayerCommonServiceBean.find(type, id);
	}
}
