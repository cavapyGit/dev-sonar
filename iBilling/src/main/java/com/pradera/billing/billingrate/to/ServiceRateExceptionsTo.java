package com.pradera.billing.billingrate.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ServiceRateExceptionsTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class ServiceRateExceptionsTo  implements Serializable{
	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 271239143676189098L;
	
	/** The id service rate pk. */
	private Long 				idServiceRatePk;
	
	/** The rate code. */
	private Integer  			rateCode;
	
	/** The rate name. */
	private String  			rateName;
	
	/** The currency rate. */
	private Integer 			currencyRate;
	
	/** The rate status. */
	private Integer 			rateStatus;
	
	/** The new rate status. */
	private Integer 			newRateStatus;
	
	/** The end effective date. */
	private Date 				endEffectiveDate;
	
	/** The initial effective date. */
	private Date 				initialEffectiveDate;
	
	/** The last effective date. */
	private Date 				lastEffectiveDate;
	
	/** The applied tax selected. */
	private Integer 			appliedTaxSelected;
	
    /** The description rate. */
    private String 				descriptionRate;
    
    /** The description status. */
    private String 				descriptionStatus;
    
	/** The description type. */
	private String 				descriptionType;
	
	/** The description currency. */
	private String 				descriptionCurrency;
	
	/** The rate type. */
	private Integer 			rateType;
	
	/** The scale type. */
	private Integer 			scaleType;
	
	/** The id billing service. */
	private String  			idBillingService;
	
	/** The service code. */
	private String  			serviceCode;
	
	/** The count calculation. */
	private Integer 			countCalculation;
	
	
	/** The selected. */
	private boolean 			selected;	
	
	/**  only for Fixed. */
	private BigDecimal 			amountRate;
	
	/**  only for Percentage. */
	private BigDecimal 			percentageRate;
	
	/**  only for Staggered. */
	private Integer 			sequenceNumber;
	
	/** The unit. */
	private Integer 			unit=0;
	
	/** The accumulate. */
	private Integer 			accumulate=0;
	
	/** The ind scale movement. */
	private Integer 			indScaleMovement=0;
	
	/** The ind staging. */
	private Integer 			indStaging;
	
	/** The disable seq number. */
	private Boolean 			disableSeqNumber;
	
	/** The disable btn generate. */
	private Boolean 			disableBtnGenerate;
	
	/** The disable unit. */
	private Boolean 			disableUnit;
	
	/** The min scale value. */
	private BigDecimal 			minScaleValue;
	
	/** The max scale value. */
	private BigDecimal 			maxScaleValue;
	
	
	/**  only for Movement. */
	private String 				descriptionMovementType;
	
	/** The code movement type. */
	private Long 				codeMovementType;
	
	/** The rate amount. */
	private BigDecimal 			rateAmount;
	
	/** The rate percent. */
	private BigDecimal 			ratePercent; 
	
	/** The movement count. */
	private Integer 			movementCount;
	
	/** The operation count. */
	private Integer 			operationCount;
	
	
	/**  flag for visibility on page. */
    private boolean 			flagFixed;
    
    /** The flag percentage. */
    private boolean 			flagPercentage;
    
    /** The flag staggered fixed. */
    private boolean 			flagStaggeredFixed;
    
    /** The flag staggered percentage. */
    private boolean 			flagStaggeredPercentage;
    
    /** The flag staggered mix. */
    private boolean 			flagStaggeredMix;
    
    /** The flag movement type fixed. */
    private boolean 			flagMovementTypeFixed;
    
    /** The flag movement type percentage. */
    private boolean 			flagMovementTypePercentage;
    
    /** The flag percentage limits. */
    private boolean 			flagPercentageLimits;
    
    /** The flag minimum amount. */
    private boolean 			flagMinimumAmount;
    
    /** Rate Staggered with Movements*. */
    private boolean 			flagStaggeredMovement;
    
    /** Flag Amount Minimum. */
    private boolean 			flagAmountMinimum;
    
   
    /** Flag is Rate Staggered with Movement for RadioButton Add Movement. */
    private boolean 			flagIsMovement;
    
    /** The flag service rate. */
    private boolean 			flagServiceRate;
    
    /** The flag delete service rate. */
    private boolean 			flagDeleteServiceRate = Boolean.FALSE;

    /** If modify Rate Scale. */
    private boolean 			flagIsModifyStaggeredFixed = Boolean.FALSE;
    
    /** Currency distinct Currency Rate in Service Rate Scale Fixed. */
    private Integer 			indOtherCurrency;
    
    /** The other currency. */
    private Integer 			otherCurrency;

    /** Montos Minimos aplicados. */
    private Integer 			indAmountMinimum;
    
    /** The rate minimum. */
    private BigDecimal 			rateMinimum;
    
    /** The currency minimum. */
    private Integer 			currencyMinimum;
    
    /** The ind minimum process. */
    private Integer 			indMinimumProcess; 
    
    /**
     * Instantiates a new service rate exceptions to.
     */
    public ServiceRateExceptionsTo(){

		
	}

    
    /**
     * Sets the service rate to.
     *
     * @param serviceRateTo the new service rate to
     */
    public void setServiceRateTo(ServiceRateExceptionsTo serviceRateTo){
    	
    	this.idServiceRatePk=serviceRateTo.getIdServiceRatePk();
    	this.rateCode=serviceRateTo.getRateCode();
    	this.rateName=serviceRateTo.getRateName();
    	this.currencyRate=serviceRateTo.getCurrencyRate();
    	this.rateStatus=serviceRateTo.getRateStatus();
    	this.endEffectiveDate =serviceRateTo.getEndEffectiveDate();
    	this.initialEffectiveDate =serviceRateTo.getInitialEffectiveDate();
    	this.appliedTaxSelected=serviceRateTo.getAppliedTaxSelected();
    	this.descriptionRate=serviceRateTo.getDescriptionRate();    	
        this.descriptionStatus=serviceRateTo.getDescriptionStatus();
        this.descriptionType=serviceRateTo.getDescriptionType();
    	this.rateType=serviceRateTo.getRateType();
    	this.rateAmount=serviceRateTo.getRateAmount();
    	this.ratePercent = serviceRateTo.getRatePercent();
    	
    	this.indScaleMovement = serviceRateTo.getIndScaleMovement();
    	this.indMinimumProcess=serviceRateTo.getIndMinimumProcess();
    }
	
	/**
	 * Gets the id service rate pk.
	 *
	 * @return the idServiceRatePk
	 */
	public Long getIdServiceRatePk() {
		return idServiceRatePk;
	}
	
	/**
	 * Sets the id service rate pk.
	 *
	 * @param idServiceRatePk the idServiceRatePk to set
	 */
	public void setIdServiceRatePk(Long idServiceRatePk) {
		this.idServiceRatePk = idServiceRatePk;
	}
	
	/**
	 * Gets the rate code.
	 *
	 * @return the rateCode
	 */
	public Integer getRateCode() {
		return rateCode;
	}
	
	/**
	 * Sets the rate code.
	 *
	 * @param rateCode the rateCode to set
	 */
	public void setRateCode(Integer rateCode) {
		this.rateCode = rateCode;
	}
	
	/**
	 * Gets the rate name.
	 *
	 * @return the rateName
	 */
	public String getRateName() {
		return rateName;
	}
	
	/**
	 * Sets the rate name.
	 *
	 * @param rateName the rateName to set
	 */
	public void setRateName(String rateName) {
		this.rateName = rateName;
	}
	
	/**
	 * Gets the currency rate.
	 *
	 * @return the currencyRate
	 */
	public Integer getCurrencyRate() {
		return currencyRate;
	}
	
	/**
	 * Sets the currency rate.
	 *
	 * @param currencyRate the currencyRate to set
	 */
	public void setCurrencyRate(Integer currencyRate) {
		this.currencyRate = currencyRate;
	}
	
	/**
	 * Gets the rate status.
	 *
	 * @return the rateStatus
	 */
	public Integer getRateStatus() {
		return rateStatus;
	}
	
	/**
	 * Sets the rate status.
	 *
	 * @param rateStatus the rateStatus to set
	 */
	public void setRateStatus(Integer rateStatus) {
		this.rateStatus = rateStatus;
	}
	
	/**
	 * Gets the new rate status.
	 *
	 * @return the newRateStatus
	 */
	public Integer getNewRateStatus() {
		return newRateStatus;
	}
	
	/**
	 * Sets the new rate status.
	 *
	 * @param newRateStatus the newRateStatus to set
	 */
	public void setNewRateStatus(Integer newRateStatus) {
		this.newRateStatus = newRateStatus;
	}
	
	/**
	 * Gets the end effective date.
	 *
	 * @return the endEffectiveDate
	 */
	public Date getEndEffectiveDate() {
		return endEffectiveDate;
	}
	
	/**
	 * Sets the end effective date.
	 *
	 * @param endEffectiveDate the endEffectiveDate to set
	 */
	public void setEndEffectiveDate(Date endEffectiveDate) {
		this.endEffectiveDate = endEffectiveDate;
	}
	
	/**
	 * Gets the initial effective date.
	 *
	 * @return the initialEffectiveDate
	 */
	public Date getInitialEffectiveDate() {
		return initialEffectiveDate;
	}
	
	/**
	 * Sets the initial effective date.
	 *
	 * @param initialEffectiveDate the initialEffectiveDate to set
	 */
	public void setInitialEffectiveDate(Date initialEffectiveDate) {
		this.initialEffectiveDate = initialEffectiveDate;
	}
	
	/**
	 * Gets the last effective date.
	 *
	 * @return the lastEffectiveDate
	 */
	public Date getLastEffectiveDate() {
		return lastEffectiveDate;
	}
	
	/**
	 * Sets the last effective date.
	 *
	 * @param lastEffectiveDate the lastEffectiveDate to set
	 */
	public void setLastEffectiveDate(Date lastEffectiveDate) {
		this.lastEffectiveDate = lastEffectiveDate;
	}
	
	/**
	 * Gets the applied tax selected.
	 *
	 * @return the appliedTaxSelected
	 */
	public Integer getAppliedTaxSelected() {
		return appliedTaxSelected;
	}
	
	/**
	 * Sets the applied tax selected.
	 *
	 * @param appliedTaxSelected the appliedTaxSelected to set
	 */
	public void setAppliedTaxSelected(Integer appliedTaxSelected) {
		this.appliedTaxSelected = appliedTaxSelected;
	}
	
	/**
	 * Gets the description rate.
	 *
	 * @return the descriptionRate
	 */
	public String getDescriptionRate() {
		return descriptionRate;
	}
	
	/**
	 * Sets the description rate.
	 *
	 * @param descriptionRate the descriptionRate to set
	 */
	public void setDescriptionRate(String descriptionRate) {
		this.descriptionRate = descriptionRate;
	}
	
	/**
	 * Gets the description status.
	 *
	 * @return the descriptionStatus
	 */
	public String getDescriptionStatus() {
		return descriptionStatus;
	}
	
	/**
	 * Sets the description status.
	 *
	 * @param descriptionStatus the descriptionStatus to set
	 */
	public void setDescriptionStatus(String descriptionStatus) {
		this.descriptionStatus = descriptionStatus;
	}
	
	/**
	 * Gets the description type.
	 *
	 * @return the descriptionType
	 */
	public String getDescriptionType() {
		return descriptionType;
	}
	
	/**
	 * Sets the description type.
	 *
	 * @param descriptionType the descriptionType to set
	 */
	public void setDescriptionType(String descriptionType) {
		this.descriptionType = descriptionType;
	}
	
	/**
	 * Gets the description currency.
	 *
	 * @return the descriptionCurrency
	 */
	public String getDescriptionCurrency() {
		return descriptionCurrency;
	}
	
	/**
	 * Sets the description currency.
	 *
	 * @param descriptionCurrency the descriptionCurrency to set
	 */
	public void setDescriptionCurrency(String descriptionCurrency) {
		this.descriptionCurrency = descriptionCurrency;
	}
	
	/**
	 * Gets the rate type.
	 *
	 * @return the rateType
	 */
	public Integer getRateType() {
		return rateType;
	}
	
	/**
	 * Sets the rate type.
	 *
	 * @param rateType the rateType to set
	 */
	public void setRateType(Integer rateType) {
		this.rateType = rateType;
	}
	
	/**
	 * Gets the scale type.
	 *
	 * @return the scaleType
	 */
	public Integer getScaleType() {
		return scaleType;
	}
	
	/**
	 * Sets the scale type.
	 *
	 * @param scaleType the scaleType to set
	 */
	public void setScaleType(Integer scaleType) {
		this.scaleType = scaleType;
	}
	
	/**
	 * Gets the id billing service.
	 *
	 * @return the idBillingService
	 */
	public String getIdBillingService() {
		return idBillingService;
	}
	
	/**
	 * Sets the id billing service.
	 *
	 * @param idBillingService the idBillingService to set
	 */
	public void setIdBillingService(String idBillingService) {
		this.idBillingService = idBillingService;
	}
	
	/**
	 * Gets the service code.
	 *
	 * @return the serviceCode
	 */
	public String getServiceCode() {
		return serviceCode;
	}
	
	/**
	 * Sets the service code.
	 *
	 * @param serviceCode the serviceCode to set
	 */
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	
	/**
	 * Gets the count calculation.
	 *
	 * @return the countCalculation
	 */
	public Integer getCountCalculation() {
		return countCalculation;
	}
	
	/**
	 * Sets the count calculation.
	 *
	 * @param countCalculation the countCalculation to set
	 */
	public void setCountCalculation(Integer countCalculation) {
		this.countCalculation = countCalculation;
	}
	
	/**
	 * Checks if is selected.
	 *
	 * @return the selected
	 */
	public boolean isSelected() {
		return selected;
	}
	
	/**
	 * Sets the selected.
	 *
	 * @param selected the selected to set
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	/**
	 * Gets the amount rate.
	 *
	 * @return the amountRate
	 */
	public BigDecimal getAmountRate() {
		return amountRate;
	}
	
	/**
	 * Sets the amount rate.
	 *
	 * @param amountRate the amountRate to set
	 */
	public void setAmountRate(BigDecimal amountRate) {
		this.amountRate = amountRate;
	}
	
	/**
	 * Gets the percentage rate.
	 *
	 * @return the percentageRate
	 */
	public BigDecimal getPercentageRate() {
		return percentageRate;
	}
	
	/**
	 * Sets the percentage rate.
	 *
	 * @param percentageRate the percentageRate to set
	 */
	public void setPercentageRate(BigDecimal percentageRate) {
		this.percentageRate = percentageRate;
	}
	
	/**
	 * Gets the sequence number.
	 *
	 * @return the sequenceNumber
	 */
	public Integer getSequenceNumber() {
		return sequenceNumber;
	}
	
	/**
	 * Sets the sequence number.
	 *
	 * @param sequenceNumber the sequenceNumber to set
	 */
	public void setSequenceNumber(Integer sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	
	/**
	 * Gets the unit.
	 *
	 * @return the unit
	 */
	public Integer getUnit() {
		return unit;
	}
	
	/**
	 * Sets the unit.
	 *
	 * @param unit the unit to set
	 */
	public void setUnit(Integer unit) {
		this.unit = unit;
	}
	
	/**
	 * Gets the accumulate.
	 *
	 * @return the accumulate
	 */
	public Integer getAccumulate() {
		return accumulate;
	}
	
	/**
	 * Sets the accumulate.
	 *
	 * @param accumulate the accumulate to set
	 */
	public void setAccumulate(Integer accumulate) {
		this.accumulate = accumulate;
	}
	
	/**
	 * Gets the ind scale movement.
	 *
	 * @return the indScaleMovement
	 */
	public Integer getIndScaleMovement() {
		return indScaleMovement;
	}
	
	/**
	 * Sets the ind scale movement.
	 *
	 * @param indScaleMovement the indScaleMovement to set
	 */
	public void setIndScaleMovement(Integer indScaleMovement) {
		this.indScaleMovement = indScaleMovement;
	}
	
	/**
	 * Gets the ind staging.
	 *
	 * @return the indStaging
	 */
	public Integer getIndStaging() {
		return indStaging;
	}
	
	/**
	 * Sets the ind staging.
	 *
	 * @param indStaging the indStaging to set
	 */
	public void setIndStaging(Integer indStaging) {
		this.indStaging = indStaging;
	}
	
	/**
	 * Gets the disable seq number.
	 *
	 * @return the disableSeqNumber
	 */
	public Boolean getDisableSeqNumber() {
		return disableSeqNumber;
	}
	
	/**
	 * Sets the disable seq number.
	 *
	 * @param disableSeqNumber the disableSeqNumber to set
	 */
	public void setDisableSeqNumber(Boolean disableSeqNumber) {
		this.disableSeqNumber = disableSeqNumber;
	}
	
	/**
	 * Gets the disable btn generate.
	 *
	 * @return the disableBtnGenerate
	 */
	public Boolean getDisableBtnGenerate() {
		return disableBtnGenerate;
	}
	
	/**
	 * Sets the disable btn generate.
	 *
	 * @param disableBtnGenerate the disableBtnGenerate to set
	 */
	public void setDisableBtnGenerate(Boolean disableBtnGenerate) {
		this.disableBtnGenerate = disableBtnGenerate;
	}
	
	/**
	 * Gets the disable unit.
	 *
	 * @return the disableUnit
	 */
	public Boolean getDisableUnit() {
		return disableUnit;
	}
	
	/**
	 * Sets the disable unit.
	 *
	 * @param disableUnit the disableUnit to set
	 */
	public void setDisableUnit(Boolean disableUnit) {
		this.disableUnit = disableUnit;
	}
	
	/**
	 * Gets the min scale value.
	 *
	 * @return the minScaleValue
	 */
	public BigDecimal getMinScaleValue() {
		return minScaleValue;
	}
	
	/**
	 * Sets the min scale value.
	 *
	 * @param minScaleValue the minScaleValue to set
	 */
	public void setMinScaleValue(BigDecimal minScaleValue) {
		this.minScaleValue = minScaleValue;
	}
	
	/**
	 * Gets the max scale value.
	 *
	 * @return the maxScaleValue
	 */
	public BigDecimal getMaxScaleValue() {
		return maxScaleValue;
	}
	
	/**
	 * Sets the max scale value.
	 *
	 * @param maxScaleValue the maxScaleValue to set
	 */
	public void setMaxScaleValue(BigDecimal maxScaleValue) {
		this.maxScaleValue = maxScaleValue;
	}
	
	/**
	 * Gets the description movement type.
	 *
	 * @return the descriptionMovementType
	 */
	public String getDescriptionMovementType() {
		return descriptionMovementType;
	}
	
	/**
	 * Sets the description movement type.
	 *
	 * @param descriptionMovementType the descriptionMovementType to set
	 */
	public void setDescriptionMovementType(String descriptionMovementType) {
		this.descriptionMovementType = descriptionMovementType;
	}
	
	/**
	 * Gets the code movement type.
	 *
	 * @return the codeMovementType
	 */
	public Long getCodeMovementType() {
		return codeMovementType;
	}
	
	/**
	 * Sets the code movement type.
	 *
	 * @param codeMovementType the codeMovementType to set
	 */
	public void setCodeMovementType(Long codeMovementType) {
		this.codeMovementType = codeMovementType;
	}
	
	/**
	 * Gets the rate amount.
	 *
	 * @return the rateAmount
	 */
	public BigDecimal getRateAmount() {
		return rateAmount;
	}
	
	/**
	 * Sets the rate amount.
	 *
	 * @param rateAmount the rateAmount to set
	 */
	public void setRateAmount(BigDecimal rateAmount) {
		this.rateAmount = rateAmount;
	}
	
	/**
	 * Gets the rate percent.
	 *
	 * @return the ratePercent
	 */
	public BigDecimal getRatePercent() {
		return ratePercent;
	}
	
	/**
	 * Sets the rate percent.
	 *
	 * @param ratePercent the ratePercent to set
	 */
	public void setRatePercent(BigDecimal ratePercent) {
		this.ratePercent = ratePercent;
	}
	
	/**
	 * Gets the movement count.
	 *
	 * @return the movementCount
	 */
	public Integer getMovementCount() {
		return movementCount;
	}
	
	/**
	 * Sets the movement count.
	 *
	 * @param movementCount the movementCount to set
	 */
	public void setMovementCount(Integer movementCount) {
		this.movementCount = movementCount;
	}
	
	/**
	 * Gets the operation count.
	 *
	 * @return the operationCount
	 */
	public Integer getOperationCount() {
		return operationCount;
	}
	
	/**
	 * Sets the operation count.
	 *
	 * @param operationCount the operationCount to set
	 */
	public void setOperationCount(Integer operationCount) {
		this.operationCount = operationCount;
	}
	
	/**
	 * Checks if is flag fixed.
	 *
	 * @return the flagFixed
	 */
	public boolean isFlagFixed() {
		return flagFixed;
	}
	
	/**
	 * Sets the flag fixed.
	 *
	 * @param flagFixed the flagFixed to set
	 */
	public void setFlagFixed(boolean flagFixed) {
		this.flagFixed = flagFixed;
	}
	
	/**
	 * Checks if is flag percentage.
	 *
	 * @return the flagPercentage
	 */
	public boolean isFlagPercentage() {
		return flagPercentage;
	}
	
	/**
	 * Sets the flag percentage.
	 *
	 * @param flagPercentage the flagPercentage to set
	 */
	public void setFlagPercentage(boolean flagPercentage) {
		this.flagPercentage = flagPercentage;
	}
	
	/**
	 * Checks if is flag staggered fixed.
	 *
	 * @return the flagStaggeredFixed
	 */
	public boolean isFlagStaggeredFixed() {
		return flagStaggeredFixed;
	}
	
	/**
	 * Sets the flag staggered fixed.
	 *
	 * @param flagStaggeredFixed the flagStaggeredFixed to set
	 */
	public void setFlagStaggeredFixed(boolean flagStaggeredFixed) {
		this.flagStaggeredFixed = flagStaggeredFixed;
	}
	
	/**
	 * Checks if is flag staggered percentage.
	 *
	 * @return the flagStaggeredPercentage
	 */
	public boolean isFlagStaggeredPercentage() {
		return flagStaggeredPercentage;
	}
	
	/**
	 * Sets the flag staggered percentage.
	 *
	 * @param flagStaggeredPercentage the flagStaggeredPercentage to set
	 */
	public void setFlagStaggeredPercentage(boolean flagStaggeredPercentage) {
		this.flagStaggeredPercentage = flagStaggeredPercentage;
	}
	
	/**
	 * Checks if is flag staggered mix.
	 *
	 * @return the flagStaggeredMix
	 */
	public boolean isFlagStaggeredMix() {
		return flagStaggeredMix;
	}
	
	/**
	 * Sets the flag staggered mix.
	 *
	 * @param flagStaggeredMix the flagStaggeredMix to set
	 */
	public void setFlagStaggeredMix(boolean flagStaggeredMix) {
		this.flagStaggeredMix = flagStaggeredMix;
	}
	
	/**
	 * Checks if is flag movement type fixed.
	 *
	 * @return the flagMovementTypeFixed
	 */
	public boolean isFlagMovementTypeFixed() {
		return flagMovementTypeFixed;
	}
	
	/**
	 * Sets the flag movement type fixed.
	 *
	 * @param flagMovementTypeFixed the flagMovementTypeFixed to set
	 */
	public void setFlagMovementTypeFixed(boolean flagMovementTypeFixed) {
		this.flagMovementTypeFixed = flagMovementTypeFixed;
	}
	
	/**
	 * Checks if is flag movement type percentage.
	 *
	 * @return the flagMovementTypePercentage
	 */
	public boolean isFlagMovementTypePercentage() {
		return flagMovementTypePercentage;
	}
	
	/**
	 * Sets the flag movement type percentage.
	 *
	 * @param flagMovementTypePercentage the flagMovementTypePercentage to set
	 */
	public void setFlagMovementTypePercentage(boolean flagMovementTypePercentage) {
		this.flagMovementTypePercentage = flagMovementTypePercentage;
	}
	
	/**
	 * Checks if is flag percentage limits.
	 *
	 * @return the flagPercentageLimits
	 */
	public boolean isFlagPercentageLimits() {
		return flagPercentageLimits;
	}
	
	/**
	 * Sets the flag percentage limits.
	 *
	 * @param flagPercentageLimits the flagPercentageLimits to set
	 */
	public void setFlagPercentageLimits(boolean flagPercentageLimits) {
		this.flagPercentageLimits = flagPercentageLimits;
	}
	
	/**
	 * Checks if is flag minimum amount.
	 *
	 * @return the flagMinimumAmount
	 */
	public boolean isFlagMinimumAmount() {
		return flagMinimumAmount;
	}
	
	/**
	 * Sets the flag minimum amount.
	 *
	 * @param flagMinimumAmount the flagMinimumAmount to set
	 */
	public void setFlagMinimumAmount(boolean flagMinimumAmount) {
		this.flagMinimumAmount = flagMinimumAmount;
	}
	
	/**
	 * Checks if is flag staggered movement.
	 *
	 * @return the flagStaggeredMovement
	 */
	public boolean isFlagStaggeredMovement() {
		return flagStaggeredMovement;
	}
	
	/**
	 * Sets the flag staggered movement.
	 *
	 * @param flagStaggeredMovement the flagStaggeredMovement to set
	 */
	public void setFlagStaggeredMovement(boolean flagStaggeredMovement) {
		this.flagStaggeredMovement = flagStaggeredMovement;
	}
	
	/**
	 * Checks if is flag amount minimum.
	 *
	 * @return the flagAmountMinimum
	 */
	public boolean isFlagAmountMinimum() {
		return flagAmountMinimum;
	}
	
	/**
	 * Sets the flag amount minimum.
	 *
	 * @param flagAmountMinimum the flagAmountMinimum to set
	 */
	public void setFlagAmountMinimum(boolean flagAmountMinimum) {
		this.flagAmountMinimum = flagAmountMinimum;
	}
	
	/**
	 * Checks if is flag is movement.
	 *
	 * @return the flagIsMovement
	 */
	public boolean isFlagIsMovement() {
		return flagIsMovement;
	}
	
	/**
	 * Sets the flag is movement.
	 *
	 * @param flagIsMovement the flagIsMovement to set
	 */
	public void setFlagIsMovement(boolean flagIsMovement) {
		this.flagIsMovement = flagIsMovement;
	}
	
	/**
	 * Checks if is flag service rate.
	 *
	 * @return the flagServiceRate
	 */
	public boolean isFlagServiceRate() {
		return flagServiceRate;
	}
	
	/**
	 * Sets the flag service rate.
	 *
	 * @param flagServiceRate the flagServiceRate to set
	 */
	public void setFlagServiceRate(boolean flagServiceRate) {
		this.flagServiceRate = flagServiceRate;
	}
	
	/**
	 * Checks if is flag delete service rate.
	 *
	 * @return the flagDeleteServiceRate
	 */
	public boolean isFlagDeleteServiceRate() {
		return flagDeleteServiceRate;
	}
	
	/**
	 * Sets the flag delete service rate.
	 *
	 * @param flagDeleteServiceRate the flagDeleteServiceRate to set
	 */
	public void setFlagDeleteServiceRate(boolean flagDeleteServiceRate) {
		this.flagDeleteServiceRate = flagDeleteServiceRate;
	}
	
	/**
	 * Checks if is flag is modify staggered fixed.
	 *
	 * @return the flagIsModifyStaggeredFixed
	 */
	public boolean isFlagIsModifyStaggeredFixed() {
		return flagIsModifyStaggeredFixed;
	}
	
	/**
	 * Sets the flag is modify staggered fixed.
	 *
	 * @param flagIsModifyStaggeredFixed the flagIsModifyStaggeredFixed to set
	 */
	public void setFlagIsModifyStaggeredFixed(boolean flagIsModifyStaggeredFixed) {
		this.flagIsModifyStaggeredFixed = flagIsModifyStaggeredFixed;
	}
	
	/**
	 * Gets the ind other currency.
	 *
	 * @return the indOtherCurrency
	 */
	public Integer getIndOtherCurrency() {
		return indOtherCurrency;
	}
	
	/**
	 * Sets the ind other currency.
	 *
	 * @param indOtherCurrency the indOtherCurrency to set
	 */
	public void setIndOtherCurrency(Integer indOtherCurrency) {
		this.indOtherCurrency = indOtherCurrency;
	}
	
	/**
	 * Gets the other currency.
	 *
	 * @return the otherCurrency
	 */
	public Integer getOtherCurrency() {
		return otherCurrency;
	}
	
	/**
	 * Sets the other currency.
	 *
	 * @param otherCurrency the otherCurrency to set
	 */
	public void setOtherCurrency(Integer otherCurrency) {
		this.otherCurrency = otherCurrency;
	}
	
	/**
	 * Gets the ind amount minimum.
	 *
	 * @return the indAmountMinimum
	 */
	public Integer getIndAmountMinimum() {
		return indAmountMinimum;
	}
	
	/**
	 * Sets the ind amount minimum.
	 *
	 * @param indAmountMinimum the indAmountMinimum to set
	 */
	public void setIndAmountMinimum(Integer indAmountMinimum) {
		this.indAmountMinimum = indAmountMinimum;
	}
	
	/**
	 * Gets the rate minimum.
	 *
	 * @return the rateMinimum
	 */
	public BigDecimal getRateMinimum() {
		return rateMinimum;
	}
	
	/**
	 * Sets the rate minimum.
	 *
	 * @param rateMinimum the rateMinimum to set
	 */
	public void setRateMinimum(BigDecimal rateMinimum) {
		this.rateMinimum = rateMinimum;
	}
	
	/**
	 * Gets the currency minimum.
	 *
	 * @return the currencyMinimum
	 */
	public Integer getCurrencyMinimum() {
		return currencyMinimum;
	}
	
	/**
	 * Sets the currency minimum.
	 *
	 * @param currencyMinimum the currencyMinimum to set
	 */
	public void setCurrencyMinimum(Integer currencyMinimum) {
		this.currencyMinimum = currencyMinimum;
	}
	
	/**
	 * Gets the ind minimum process.
	 *
	 * @return the indMinimumProcess
	 */
	public Integer getIndMinimumProcess() {
		return indMinimumProcess;
	}
	
	/**
	 * Sets the ind minimum process.
	 *
	 * @param indMinimumProcess the indMinimumProcess to set
	 */
	public void setIndMinimumProcess(Integer indMinimumProcess) {
		this.indMinimumProcess = indMinimumProcess;
	}
	
}
