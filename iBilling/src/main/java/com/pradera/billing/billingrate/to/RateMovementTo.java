package com.pradera.billing.billingrate.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.pradera.model.component.MovementType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RateMovementTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class RateMovementTo implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4446264087665914645L;
	
	
	/** The id rate movement pk. */
	private Integer 		idRateMovementPk;
	
	/** The movement type. */
	private MovementType 	movementType;
	
	/** The rate amount. */
	private BigDecimal 		rateAmount;
	
	/** The rate percent. */
	private BigDecimal 		ratePercent;

	/** The description movement type. */
	private String 			descriptionMovementType;
	
	/** The movement count. */
	private Integer 		movementCount;
	
	/** The gross amount. */
	private BigDecimal 		grossAmount;
	
	/** The security code. */
	private String 			securityCode;
	
	/** The description currency rate. */
	private String 			descriptionCurrencyRate;
	
	/** The currency rate. */
	private Integer 		currencyRate;
	
	/** The movement quantity. */
	private Long 			movementQuantity;
	
	/** The operation price. */
	private BigDecimal 		operationPrice;
	
	/** The dirty amount. */
	private BigDecimal 		dirtyAmount;

	/** The operation count. */
	private Integer 		operationCount;
	
	
	/** The list detail rate movement to. */
	private List<DetailRateMovementTo> 		listDetailRateMovementTo;
	
	/** The service rate to. */
	private ServiceRateTo 	serviceRateTo;
	
	
	/**
	 * Instantiates a new rate movement to.
	 */
	public RateMovementTo() {
		listDetailRateMovementTo= new ArrayList<DetailRateMovementTo>();
	}


	/**
	 * Gets the id rate movement pk.
	 *
	 * @return the idRateMovementPk
	 */
	public Integer getIdRateMovementPk() {
		return idRateMovementPk;
	}


	/**
	 * Sets the id rate movement pk.
	 *
	 * @param idRateMovementPk the idRateMovementPk to set
	 */
	public void setIdRateMovementPk(Integer idRateMovementPk) {
		this.idRateMovementPk = idRateMovementPk;
	}


	/**
	 * Gets the movement type.
	 *
	 * @return the movementType
	 */
	public MovementType getMovementType() {
		return movementType;
	}


	/**
	 * Sets the movement type.
	 *
	 * @param movementType the movementType to set
	 */
	public void setMovementType(MovementType movementType) {
		this.movementType = movementType;
	}


	/**
	 * Gets the rate amount.
	 *
	 * @return the rateAmount
	 */
	public BigDecimal getRateAmount() {
		return rateAmount;
	}


	/**
	 * Sets the rate amount.
	 *
	 * @param rateAmount the rateAmount to set
	 */
	public void setRateAmount(BigDecimal rateAmount) {
		this.rateAmount = rateAmount;
	}


	/**
	 * Gets the rate percent.
	 *
	 * @return the ratePercent
	 */
	public BigDecimal getRatePercent() {
		return ratePercent;
	}


	/**
	 * Sets the rate percent.
	 *
	 * @param ratePercent the ratePercent to set
	 */
	public void setRatePercent(BigDecimal ratePercent) {
		this.ratePercent = ratePercent;
	}


	/**
	 * Gets the description movement type.
	 *
	 * @return the descriptionMovementType
	 */
	public String getDescriptionMovementType() {
		return descriptionMovementType;
	}


	/**
	 * Sets the description movement type.
	 *
	 * @param descriptionMovementType the descriptionMovementType to set
	 */
	public void setDescriptionMovementType(String descriptionMovementType) {
		this.descriptionMovementType = descriptionMovementType;
	}


	/**
	 * Gets the movement count.
	 *
	 * @return the movementCount
	 */
	public Integer getMovementCount() {
		return movementCount;
	}


	/**
	 * Sets the movement count.
	 *
	 * @param movementCount the movementCount to set
	 */
	public void setMovementCount(Integer movementCount) {
		this.movementCount = movementCount;
	}


	/**
	 * Gets the gross amount.
	 *
	 * @return the grossAmount
	 */
	public BigDecimal getGrossAmount() {
		return grossAmount;
	}


	/**
	 * Sets the gross amount.
	 *
	 * @param grossAmount the grossAmount to set
	 */
	public void setGrossAmount(BigDecimal grossAmount) {
		this.grossAmount = grossAmount;
	}


	/**
	 * Gets the security code.
	 *
	 * @return the securityCode
	 */
	public String getSecurityCode() {
		return securityCode;
	}


	/**
	 * Sets the security code.
	 *
	 * @param securityCode the securityCode to set
	 */
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}


	/**
	 * Gets the description currency rate.
	 *
	 * @return the descriptionCurrencyRate
	 */
	public String getDescriptionCurrencyRate() {
		return descriptionCurrencyRate;
	}


	/**
	 * Sets the description currency rate.
	 *
	 * @param descriptionCurrencyRate the descriptionCurrencyRate to set
	 */
	public void setDescriptionCurrencyRate(String descriptionCurrencyRate) {
		this.descriptionCurrencyRate = descriptionCurrencyRate;
	}


	/**
	 * Gets the currency rate.
	 *
	 * @return the currencyRate
	 */
	public Integer getCurrencyRate() {
		return currencyRate;
	}


	/**
	 * Sets the currency rate.
	 *
	 * @param currencyRate the currencyRate to set
	 */
	public void setCurrencyRate(Integer currencyRate) {
		this.currencyRate = currencyRate;
	}


	/**
	 * Gets the movement quantity.
	 *
	 * @return the movementQuantity
	 */
	public Long getMovementQuantity() {
		return movementQuantity;
	}


	/**
	 * Sets the movement quantity.
	 *
	 * @param movementQuantity the movementQuantity to set
	 */
	public void setMovementQuantity(Long movementQuantity) {
		this.movementQuantity = movementQuantity;
	}


	/**
	 * Gets the operation price.
	 *
	 * @return the operationPrice
	 */
	public BigDecimal getOperationPrice() {
		return operationPrice;
	}


	/**
	 * Sets the operation price.
	 *
	 * @param operationPrice the operationPrice to set
	 */
	public void setOperationPrice(BigDecimal operationPrice) {
		this.operationPrice = operationPrice;
	}


	/**
	 * Gets the dirty amount.
	 *
	 * @return the dirtyAmount
	 */
	public BigDecimal getDirtyAmount() {
		return dirtyAmount;
	}


	/**
	 * Sets the dirty amount.
	 *
	 * @param dirtyAmount the dirtyAmount to set
	 */
	public void setDirtyAmount(BigDecimal dirtyAmount) {
		this.dirtyAmount = dirtyAmount;
	}


	/**
	 * Gets the operation count.
	 *
	 * @return the operationCount
	 */
	public Integer getOperationCount() {
		return operationCount;
	}


	/**
	 * Sets the operation count.
	 *
	 * @param operationCount the operationCount to set
	 */
	public void setOperationCount(Integer operationCount) {
		this.operationCount = operationCount;
	}


	/**
	 * Gets the list detail rate movement to.
	 *
	 * @return the listDetailRateMovementTo
	 */
	public List<DetailRateMovementTo> getListDetailRateMovementTo() {
		return listDetailRateMovementTo;
	}


	/**
	 * Sets the list detail rate movement to.
	 *
	 * @param listDetailRateMovementTo the listDetailRateMovementTo to set
	 */
	public void setListDetailRateMovementTo(
			List<DetailRateMovementTo> listDetailRateMovementTo) {
		this.listDetailRateMovementTo = listDetailRateMovementTo;
	}


	/**
	 * Gets the service rate to.
	 *
	 * @return the serviceRateTo
	 */
	public ServiceRateTo getServiceRateTo() {
		return serviceRateTo;
	}


	/**
	 * Sets the service rate to.
	 *
	 * @param serviceRateTo the serviceRateTo to set
	 */
	public void setServiceRateTo(ServiceRateTo serviceRateTo) {
		this.serviceRateTo = serviceRateTo;
	}


	
	
	
	
}
