package com.pradera.billing.billingrate.to;

import java.io.Serializable;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RateServiceToDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class  RateServiceToDataModel extends ListDataModel<ServiceRateTo> implements SelectableDataModel<ServiceRateTo> , Serializable {
	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new rate service to data model.
	 */
	public RateServiceToDataModel() {
		
	}
	
	/**
	 * Instantiates a new rate service to data model.
	 *
	 * @param data the data
	 */
	public RateServiceToDataModel(List<ServiceRateTo> data) {
		super(data);	
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public ServiceRateTo getRowData(String rowKey) {
		
		@SuppressWarnings("unchecked")
		List<ServiceRateTo> serviceRateToList=(List<ServiceRateTo>)getWrappedData();
		
        for(ServiceRateTo serviceRateTo : serviceRateToList) {  
            if(String.valueOf(serviceRateTo.getIdServiceRatePk()).equals(rowKey))
                return serviceRateTo;  
        }

		return null;
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(ServiceRateTo arg0) {
		// TODO Auto-generated method stub
		return arg0.getIdServiceRatePk();
	}

	
}
