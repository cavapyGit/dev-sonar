package com.pradera.billing.billingrate.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.pradera.commons.view.GenericBaseBean;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CollectionRateDetailTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class CollectionRateDetailTo  extends GenericBaseBean implements Serializable{

	/** Detalle de las tarifas. */
	private static final long serialVersionUID = 1L;
	
	/** The rate code detail. */
	private Long 					rateCodeDetail;
	
	/** The amount. */
	private BigDecimal 				amount;
	
	/** The movement count. */
	private Integer 				movementCount;/** The movement name. */
	
	private String 					movementName;
	
	/** The id rate movement. */
	private Long 					idRateMovement;/** Id rateMovement. */
	
	private Integer 				movementNumber;/** The rate movement to list. */
	
	private List<RateMovementTo> 	rateMovementToList;/**
 * Instantiates a new collection rate detail to.
 */

	
	public CollectionRateDetailTo(){
		
	}
	
	/**
	 * Gets the rate code detail.
	 *
	 * @return the rateCodeDetail
	 */
	public Long getRateCodeDetail() {
		return rateCodeDetail;
	}

	/**
	 * Sets the rate code detail.
	 *
	 * @param rateCodeDetail the rateCodeDetail to set
	 */
	public void setRateCodeDetail(Long rateCodeDetail) {
		this.rateCodeDetail = rateCodeDetail;
	}

	/**
	 * Gets the amount.
	 *
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * Sets the amount.
	 *
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * Gets the movement count.
	 *
	 * @return the movementCount
	 */
	public Integer getMovementCount() {
		return movementCount;
	}

	/**
	 * Sets the movement count.
	 *
	 * @param movementCount the movementCount to set
	 */
	public void setMovementCount(Integer movementCount) {
		this.movementCount = movementCount;
	}

	/**
	 * Gets the movement name.
	 *
	 * @return the movementName
	 */
	public String getMovementName() {
		return movementName;
	}

	/**
	 * Sets the movement name.
	 *
	 * @param movementName the movementName to set
	 */
	public void setMovementName(String movementName) {
		this.movementName = movementName;
	}

	/**
	 * Gets the id rate movement.
	 *
	 * @return the idRateMovement
	 */
	public Long getIdRateMovement() {
		return idRateMovement;
	}

	/**
	 * Sets the id rate movement.
	 *
	 * @param idRateMovement the idRateMovement to set
	 */
	public void setIdRateMovement(Long idRateMovement) {
		this.idRateMovement = idRateMovement;
	}

	/**
	 * Gets the movement number.
	 *
	 * @return the movementNumber
	 */
	public Integer getMovementNumber() {
		return movementNumber;
	}

	/**
	 * Sets the movement number.
	 *
	 * @param movementNumber the movementNumber to set
	 */
	public void setMovementNumber(Integer movementNumber) {
		this.movementNumber = movementNumber;
	}

	/**
	 * Gets the rate movement to list.
	 *
	 * @return the rateMovementToList
	 */
	public List<RateMovementTo> getRateMovementToList() {
		return rateMovementToList;
	}

	/**
	 * Sets the rate movement to list.
	 *
	 * @param rateMovementToList the rateMovementToList to set
	 */
	public void setRateMovementToList(List<RateMovementTo> rateMovementToList) {
		this.rateMovementToList = rateMovementToList;
	}
	
	
	
	

}
