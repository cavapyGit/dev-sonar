package com.pradera.billing.billingrate.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class DetailRateMovementTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class DetailRateMovementTo implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8635974991844396304L;
	
	/** The description source participant. */
	private String 		descriptionSourceParticipant;
	
	/** The description target participant. */
	private String 		descriptionTargetParticipant;
	
	/** The description rnt. */
	private String 		descriptionRnt;
	
	/** The operation date. */
	private Date 		operationDate;
	
	/** The movement date. */
	private Date 		movementDate;
	
	/** The operation number. */
	private Integer 	operationNumber;
	
	/** The holder account. */
	private Integer 	holderAccount;
	
	/** The security code. */
	private String 		securityCode;
	
	/** The amount. */
	private BigDecimal 	amount;
	
	/** The movement quantity. */
	private Long 		movementQuantity;
	
	/** The operation price. */
	private BigDecimal 	operationPrice;
	
	/** The operation amount. */
	private BigDecimal 	operationAmount;

	/**
	 * Gets the description source participant.
	 *
	 * @return the descriptionSourceParticipant
	 */
	public String getDescriptionSourceParticipant() {
		return descriptionSourceParticipant;
	}

	/**
	 * Sets the description source participant.
	 *
	 * @param descriptionSourceParticipant the descriptionSourceParticipant to set
	 */
	public void setDescriptionSourceParticipant(String descriptionSourceParticipant) {
		this.descriptionSourceParticipant = descriptionSourceParticipant;
	}

	/**
	 * Gets the description target participant.
	 *
	 * @return the descriptionTargetParticipant
	 */
	public String getDescriptionTargetParticipant() {
		return descriptionTargetParticipant;
	}

	/**
	 * Sets the description target participant.
	 *
	 * @param descriptionTargetParticipant the descriptionTargetParticipant to set
	 */
	public void setDescriptionTargetParticipant(String descriptionTargetParticipant) {
		this.descriptionTargetParticipant = descriptionTargetParticipant;
	}

	/**
	 * Gets the description rnt.
	 *
	 * @return the descriptionRnt
	 */
	public String getDescriptionRnt() {
		return descriptionRnt;
	}

	/**
	 * Sets the description rnt.
	 *
	 * @param descriptionRnt the descriptionRnt to set
	 */
	public void setDescriptionRnt(String descriptionRnt) {
		this.descriptionRnt = descriptionRnt;
	}

	/**
	 * Gets the operation date.
	 *
	 * @return the operationDate
	 */
	public Date getOperationDate() {
		return operationDate;
	}

	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the operationDate to set
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	/**
	 * Gets the movement date.
	 *
	 * @return the movementDate
	 */
	public Date getMovementDate() {
		return movementDate;
	}

	/**
	 * Sets the movement date.
	 *
	 * @param movementDate the movementDate to set
	 */
	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}

	/**
	 * Gets the operation number.
	 *
	 * @return the operationNumber
	 */
	public Integer getOperationNumber() {
		return operationNumber;
	}

	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the operationNumber to set
	 */
	public void setOperationNumber(Integer operationNumber) {
		this.operationNumber = operationNumber;
	}

	/**
	 * Gets the holder account.
	 *
	 * @return the holderAccount
	 */
	public Integer getHolderAccount() {
		return holderAccount;
	}

	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the holderAccount to set
	 */
	public void setHolderAccount(Integer holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * Gets the security code.
	 *
	 * @return the securityCode
	 */
	public String getSecurityCode() {
		return securityCode;
	}

	/**
	 * Sets the security code.
	 *
	 * @param securityCode the securityCode to set
	 */
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	/**
	 * Gets the amount.
	 *
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * Sets the amount.
	 *
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * Gets the movement quantity.
	 *
	 * @return the movementQuantity
	 */
	public Long getMovementQuantity() {
		return movementQuantity;
	}

	/**
	 * Sets the movement quantity.
	 *
	 * @param movementQuantity the movementQuantity to set
	 */
	public void setMovementQuantity(Long movementQuantity) {
		this.movementQuantity = movementQuantity;
	}

	/**
	 * Gets the operation price.
	 *
	 * @return the operationPrice
	 */
	public BigDecimal getOperationPrice() {
		return operationPrice;
	}

	/**
	 * Sets the operation price.
	 *
	 * @param operationPrice the operationPrice to set
	 */
	public void setOperationPrice(BigDecimal operationPrice) {
		this.operationPrice = operationPrice;
	}

	/**
	 * Gets the operation amount.
	 *
	 * @return the operationAmount
	 */
	public BigDecimal getOperationAmount() {
		return operationAmount;
	}

	/**
	 * Sets the operation amount.
	 *
	 * @param operationAmount the operationAmount to set
	 */
	public void setOperationAmount(BigDecimal operationAmount) {
		this.operationAmount = operationAmount;
	}
	
	
	
	
}
