package com.pradera.billing.billingrate.service;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.Query;

import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.billing.RateMovement;
import com.pradera.model.billing.ServiceRate;
import com.pradera.model.billing.ServiceRateScale;
import com.pradera.model.billing.type.RateStateType;
import com.pradera.model.billing.type.RateType;
import com.pradera.model.generalparameter.ParameterTable;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingRateMgmtServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class BillingRateMgmtServiceBean extends CrudDaoServiceBean{
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;

	/**
	 * Instantiates a new billing rate mgmt service bean.
	 */
	public BillingRateMgmtServiceBean() {
		// TODO Auto-generated constructor stub		
	}

	
	/**
	 * Save rate service.
	 *
	 * @param serviceRate the service rate
	 * @return the service rate
	 * @throws ServiceException the service exception
	 */
	public ServiceRate saveRateService (ServiceRate serviceRate)throws ServiceException{
		this.create(serviceRate);
		return serviceRate;
	}
	
	
	/**
	 * Save service rate scale.
	 *
	 * @param serviceRateScale the service rate scale
	 * @return the service rate scale
	 */
	public ServiceRateScale saveServiceRateScale (ServiceRateScale serviceRateScale){
		this.create(serviceRateScale);
		return serviceRateScale;
	}
	
	/**
	 * Save service rate movement.
	 *
	 * @param rateMovement the rate movement
	 * @return the rate movement
	 */
	public RateMovement saveServiceRateMovement(RateMovement rateMovement){
		this.create(rateMovement);
		return rateMovement;
	}
	
	
	/**
	 * Save all rate service.
	 *
	 * @param serviceRateList the service rate list
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ServiceRate>  saveAllRateService (List<ServiceRate> serviceRateList) throws ServiceException{
		
		Integer lastCode=null;
		for (ServiceRate serviceRate : serviceRateList) {
			lastCode=generateRateServiceCodeByBillingService(serviceRate.getBillingService().getIdBillingServicePk());
			serviceRate.setRateCode(lastCode);
			serviceRate.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
			serviceRate.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
			serviceRate.setLastModifyDate(CommonsUtilities.currentDate());
			serviceRate.setLastModifyApp(userInfo.getLastModifyPriv());
//			serviceRate = em.merge(serviceRate);
			serviceRate = saveRateService(serviceRate);
			if ( RateType.ESCALONADO_FIJO.getCode().equals(serviceRate.getRateType())){
				
				saveAllRateServiceRateScale(serviceRate);

				if(serviceRate.getIndScaleMovement().equals(GeneralConstants.ONE_VALUE_INTEGER)){
					saveAllRateServiceRateMovement(serviceRate);
				}
				
			}else if (  RateType.ESCALONADO_PORCENTUAL.getCode().equals(serviceRate.getRateType())){
				
				saveAllRateServiceRateScale(serviceRate);
				
				if(serviceRate.getIndScaleMovement().equals(GeneralConstants.ONE_VALUE_INTEGER)){
					saveAllRateServiceRateMovement(serviceRate);
				}
			}else if (  RateType.ESCALONADO_MIXTO.getCode().equals(serviceRate.getRateType())){
				
				saveAllRateServiceRateScale(serviceRate);

				if(GeneralConstants.ONE_VALUE_INTEGER.equals(serviceRate.getIndScaleMovement())){
					saveAllRateServiceRateMovement(serviceRate);
				}
				
			}else if (  RateType.TIPO_MOVIMIENTO_FIJO.getCode().equals(serviceRate.getRateType())){
				
				saveAllRateServiceRateMovement(serviceRate);
				
			}else if (  RateType.TIPO_MOVIMIENTO_PORCENTUAL.getCode().equals(serviceRate.getRateType())){
				
				saveAllRateServiceRateMovement(serviceRate);
				
			}
			
		}
		return serviceRateList;
	}
	
	/**
	 * Save all rate service rate scale.
	 *
	 * @param serviceRate the service rate
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ServiceRateScale>  saveAllRateServiceRateScale (ServiceRate serviceRate) throws ServiceException{
		List<ServiceRateScale> serviceRateScaleList = serviceRate.getServiceRateScales();		
		for (ServiceRateScale serviceRateScale : serviceRateScaleList) {
			serviceRateScale.setServiceRate(serviceRate);
			this.saveServiceRateScale(serviceRateScale);
		}
		return serviceRateScaleList;
	}
	
	/**
	 * Save all rate service rate movement.
	 *
	 * @param serviceRate the service rate
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<RateMovement>  saveAllRateServiceRateMovement (ServiceRate serviceRate) throws ServiceException{
		List<RateMovement> serviceRateMovementList = serviceRate.getRateMovements();	
		for (RateMovement serviceRateMovement : serviceRateMovementList) {
			serviceRateMovement.setServiceRate(serviceRate);
			this.saveServiceRateMovement(serviceRateMovement);
		}
		return serviceRateMovementList;
	}
	
	
	
	/**
	 * Gets the services rate by billing service.
	 *
	 * @param billingServiceId the billing service id
	 * @return the services rate by billing service
	 * @throws ServiceException the service exception
	 */
	public List<ServiceRate> getServicesRateByBillingService(Long billingServiceId) throws ServiceException{
		
		StringBuilder sb = new StringBuilder();
		sb.append(" select DISTINCT sr from ServiceRate sr left join fetch sr.rateMovements " +
				"   where sr.billingService.idBillingServicePk=:billingServiceId ");
		sb.append(" and not sr.rateState in(:statusCancel) ");
		sb.append(" order by sr.idServiceRatePk");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("statusCancel", RateStateType.CANCELED.getCode());
		parameters.put("billingServiceId", billingServiceId);
		
		/* search only rate service that are'nt with status cancel*/
		List list =findListByQueryString(sb.toString(), parameters);
		
		
		if (Validations.validateIsNotNullAndNotEmpty(list) && list.size()>0){
		
		/* getting description type of rate */	
		ServiceRate serviceRate = (ServiceRate)list.get(0);
		Integer rateType =serviceRate.getRateType();				
		List<ParameterTable> descriptionRateTypeList = getListParameterTable(rateType);
	    
		/* Setting description rate  service type */
		if (Validations.validateListIsNotNullAndNotEmpty(descriptionRateTypeList)){
			for (Object objServiceRate : list) {			
					ServiceRate serviceRateAux = (ServiceRate)objServiceRate;
					for (Object objRateType : descriptionRateTypeList) {
						ParameterTable parameterTable =(ParameterTable)objRateType;
						if (serviceRateAux.getRateType().intValue()==parameterTable.getParameterTablePk().intValue()){
							serviceRateAux.setDescriptionType(parameterTable.getParameterName());
							break;
						}
					}
				}		
		}
		
		/* Setting description  rate service status*/
		ParameterTableTO filter = new ParameterTableTO();
		filter.setMasterTableFk(PropertiesConstants.RATE_STATUS);
		List<ParameterTable> rateServiceTypeList =getListParameterTable(filter);
			if (Validations.validateListIsNotNullAndNotEmpty(rateServiceTypeList)){
				
				for (Object objServiceRate : list) {
						ServiceRate serviceRateAux = (ServiceRate)objServiceRate;
						for (Object objRateType : rateServiceTypeList) {
							ParameterTable parameterTable =(ParameterTable)objRateType;
							if (serviceRateAux.getRateState().intValue()==parameterTable.getParameterTablePk().intValue()){
								serviceRateAux.setDescriptionStatus(parameterTable.getParameterName());
								break;
							}
						}
				}
			}		
		}
        
		return list;
	}
	
	
	/**
	 * Gets the services rate by all billing service.
	 *
	 * @param billingServiceId the billing service id
	 * @return the services rate by all billing service
	 * @throws ServiceException the service exception
	 */
	public List<ServiceRate> getServicesRateByAllBillingService(Long billingServiceId) throws ServiceException{
		
		StringBuilder sb = new StringBuilder();
		sb.append(" select DISTINCT sr from ServiceRate sr   left join fetch sr.rateMovements " +
				"   where sr.billingService.idBillingServicePk=:billingServiceId ");
		sb.append(" order by sr.idServiceRatePk");
		
		Map<String, Object> parameters = new HashMap<String, Object>(); 
		parameters.put("billingServiceId", billingServiceId); 

		List list =findListByQueryString(sb.toString(), parameters);
		
		
		if (Validations.validateIsNotNullAndNotEmpty(list) && list.size()>0){
		
		/* getting description type of rate */	
		ServiceRate serviceRate = (ServiceRate)list.get(0);
		Integer rateType =serviceRate.getRateType();				
		List<ParameterTable> descriptionRateTypeList = getListParameterTable(rateType);
	    
		/* Setting description rate  service type */
		if (Validations.validateListIsNotNullAndNotEmpty(descriptionRateTypeList)){
			for (Object objServiceRate : list) {			
					ServiceRate serviceRateAux = (ServiceRate)objServiceRate;
					for (Object objRateType : descriptionRateTypeList) {
						ParameterTable parameterTable =(ParameterTable)objRateType;
						if (serviceRateAux.getRateType().intValue()==parameterTable.getParameterTablePk().intValue()){
							serviceRateAux.setDescriptionType(parameterTable.getParameterName());
							break;
						}
					}
				}		
		}
		
		/* Setting description  rate service status*/
		ParameterTableTO filter = new ParameterTableTO();
		filter.setMasterTableFk(PropertiesConstants.RATE_STATUS);
		List<ParameterTable> rateServiceTypeList =getListParameterTable(filter);
			if (Validations.validateListIsNotNullAndNotEmpty(rateServiceTypeList)){
				
				for (Object objServiceRate : list) {			
						ServiceRate serviceRateAux = (ServiceRate)objServiceRate;
						for (Object objRateType : rateServiceTypeList) {
							ParameterTable parameterTable =(ParameterTable)objRateType;
							if (serviceRateAux.getRateState().intValue()==parameterTable.getParameterTablePk().intValue()){
								serviceRateAux.setDescriptionStatus(parameterTable.getParameterName());
								break;
							}
						}
				}
			}		
		}
        
		return list;
	}
	
	
	/**
	 *  Use for consult scale *.
	 *
	 * @param billingServiceId the billing service id
	 * @return the services rate by all billing service to scale
	 * @throws ServiceException the service exception
	 */
	public List<ServiceRate> getServicesRateByAllBillingServiceToScale(Long billingServiceId) throws ServiceException{
		
		StringBuilder sb = new StringBuilder();
		sb.append(" select DISTINCT sr from ServiceRate sr   left join fetch sr.serviceRateScales  srs" +
				"   where sr.billingService.idBillingServicePk=:billingServiceId   ");
		sb.append(" order by sr.idServiceRatePk , srs.idServiceRateScalePk");
		
		Map<String, Object> parameters = new HashMap<String, Object>(); 
		parameters.put("billingServiceId", billingServiceId); 

		List list =findListByQueryString(sb.toString(), parameters);
		
		
		if (Validations.validateIsNotNullAndNotEmpty(list) && list.size()>0){
		
		/* getting description type of rate */	
		ServiceRate serviceRate = (ServiceRate)list.get(0);
		Integer rateType =serviceRate.getRateType();				
		List<ParameterTable> descriptionRateTypeList = getListParameterTable(rateType);
	    
		/* Setting description rate  service type */
		if (Validations.validateListIsNotNullAndNotEmpty(descriptionRateTypeList)){
			for (Object objServiceRate : list) {			
					ServiceRate serviceRateAux = (ServiceRate)objServiceRate;
					for (Object objRateType : descriptionRateTypeList) {
						ParameterTable parameterTable =(ParameterTable)objRateType;
						if (serviceRateAux.getRateType().intValue()==parameterTable.getParameterTablePk().intValue()){
							serviceRateAux.setDescriptionType(parameterTable.getParameterName());
							break;
						}
					}
				}		
		}
		
		/* Setting description  rate service status*/
		ParameterTableTO filter = new ParameterTableTO();
		filter.setMasterTableFk(PropertiesConstants.RATE_STATUS);
		List<ParameterTable> rateServiceTypeList =getListParameterTable(filter);
			if (Validations.validateListIsNotNullAndNotEmpty(rateServiceTypeList)){
				
				for (Object objServiceRate : list) {			
						ServiceRate serviceRateAux = (ServiceRate)objServiceRate;
						for (Object objRateType : rateServiceTypeList) {
							ParameterTable parameterTable =(ParameterTable)objRateType;
							if (serviceRateAux.getRateState().intValue()==parameterTable.getParameterTablePk().intValue()){
								serviceRateAux.setDescriptionStatus(parameterTable.getParameterName());
								break;
							}
						}
				}
			}		
		}
        
		return list;
	}
	
	
	/**
	 * Get List To Parameter Table To Filters.
	 *
	 * @param filter ParameterTableTO
	 * @return the list parameter table
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getListParameterTable(ParameterTableTO filter) throws ServiceException	{
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select P.parameterTablePk,P.parameterName From ParameterTable P ");
		sbQuery.append(" Where 1 = 1 ");

		if(Validations.validateIsNotNull(filter.getState())){
			sbQuery.append(" And P.parameterState = :statePrm");
		}

		if(Validations.validateIsNotNull(filter.getParameterTablePk())){
			sbQuery.append(" And P.parameterTablePk = :parameterTablePkPrm");
		}

		if(Validations.validateIsNotNull(filter.getMasterTableFk())){
			sbQuery.append(" And P.masterTable.masterTablePk = :masterTableFkPrm");
		}

		if(Validations.validateIsNotNull(filter.getParameterTableCd())){
			sbQuery.append(" And P.parameterTableCd = :parameterTableCdPrm");
		}
				
    	Query query = em.createQuery(sbQuery.toString());  

    	if(Validations.validateIsNotNull(filter.getState())){
        	query.setParameter("statePrm", filter.getState());
		}

		if(Validations.validateIsNotNull(filter.getParameterTablePk())){
	    	query.setParameter("parameterTablePkPrm", filter.getParameterTablePk());
		}

		if(Validations.validateIsNotNull(filter.getMasterTableFk())){
	    	query.setParameter("masterTableFkPrm", filter.getMasterTableFk());
		}

		if(Validations.validateIsNotNull(filter.getParameterTableCd())){
	    	query.setParameter("parameterTableCdPrm", filter.getParameterTableCd());
		}
		List<Object> objectList = query.getResultList();
		List<ParameterTable> parameterTableList = new ArrayList<ParameterTable>();
		for(int i=0;i<objectList.size();++i){			
			//Getting Results from data base
			Object[] sResults = (Object[])objectList.get(i);
			ParameterTable parameterTable = new ParameterTable();
			parameterTable.setParameterTablePk((Integer) sResults[0]);
			parameterTable.setParameterName((String)sResults[1]);
			parameterTableList.add(parameterTable);
		}
		return parameterTableList;
	}
	
	/**
	 * .
	 *
	 * @param parameterTableId the parameter table id
	 * @return the list parameter table
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getListParameterTable(Integer parameterTableId) throws ServiceException	{
		
		StringBuilder sbParam= new StringBuilder();
		sbParam.append(" select pt.parameter_table_pk , pt.description from parameter_table pt ");
 		sbParam.append("where pt.master_table_fk =( select master_table_fk from parameter_table ");
		sbParam.append("where parameter_table_pk= :master_table_fk )" );
		Map<String, Object> parameters = new HashMap<String, Object>();		
		parameters.put("master_table_fk", parameterTableId);		
		List<Object>  objectList = findByNativeQuery(sbParam.toString(), parameters);
		
		List<ParameterTable> parameterTableList = new ArrayList<ParameterTable>();
		for(int i=0;i<objectList.size();++i){			
			//Getting Results from data base
			Object[] sResults = (Object[])objectList.get(i);
			ParameterTable parameterTable = new ParameterTable();
			parameterTable.setParameterTablePk( Integer.parseInt(((BigDecimal) sResults[0]).toString()));
			parameterTable.setParameterName((String)sResults[1]);
			parameterTableList.add(parameterTable);
		}
		return parameterTableList;		
	}
	
	
	
	/**
	 * Generate rate service code by billing service.
	 *
	 * @param billingServiceId the billing service id
	 * @return the integer
	 * @throws ServiceException the service exception
	 */
	public Integer  generateRateServiceCodeByBillingService(Long billingServiceId)throws ServiceException{
		
		StringBuilder sbQuery= new StringBuilder();
		sbQuery.append("Select max(sr.rateCode) from ServiceRate sr where " +
				"sr.billingService.idBillingServicePk =:billingServiceId  ");		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("billingServiceId", billingServiceId);
		Integer LastCode;
		
		try {
			 LastCode= (Integer) query.getSingleResult();
		} catch (Exception e) {
			log.info("::::::::Error to query generateRateServiceCodeByBillingService");
			LastCode=null;
		}
		
		return LastCode==null?1:LastCode +1;
	}


	/**
	 * Find rate movement by service rate.
	 *
	 * @param serviceRatePk the service rate pk
	 * @return the list
	 */
	public List<RateMovement> findRateMovementByServiceRate(Long serviceRatePk) {
		
		StringBuilder sb = new StringBuilder();
		sb.append(" select distinct  rm from RateMovement rm  join fetch  rm.movementType   " +
				 "   where rm.serviceRate.idServiceRatePk = :serviceRatePk");
		
		List<RateMovement> rateMovementList =null;
		
		if (Validations.validateIsNotNull(serviceRatePk)){
						
			Query query= em.createQuery(sb.toString());
			
			query.setParameter("serviceRatePk", serviceRatePk);
			
			rateMovementList=query.getResultList();
		}
		
		return rateMovementList;
	}
	
	/**
	 * Find service rate scale by service rate.
	 *
	 * @param serviceRatePk the service rate pk
	 * @return the list
	 */
	public List<ServiceRateScale> findServiceRateScaleByServiceRate(Long serviceRatePk) {
		
		StringBuilder sb = new StringBuilder();
		sb.append(" select sr from ServiceRateScale sr     " +
				 "  where sr.serviceRate.idServiceRatePk = :serviceRatePk ");
		
		List<ServiceRateScale> serviceRateScaleList =null;
		
		if (Validations.validateIsNotNull(serviceRatePk)){
						
			Query query= em.createQuery(sb.toString());
			
			query.setParameter("serviceRatePk", serviceRatePk);
			
			serviceRateScaleList=query.getResultList();
		}
		
		return serviceRateScaleList;
	}
	
	
	/**
	 * Removes the service rate.
	 *
	 * @param serviceRate the service rate
	 * @throws ServiceException the service exception
	 */
	public void removeServiceRate(ServiceRate serviceRate) throws ServiceException{		
		this.delete(ServiceRate.class, serviceRate.getIdServiceRatePk());
	}
	
	
	
	/**
	 * Removes the rate exclusion all.
	 *
	 * @param serviceRate the service rate
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int removeRateExclusionAll(ServiceRate serviceRate) throws ServiceException{
		
		StringBuilder sb = new StringBuilder();
		sb.append("delete from rate_exclusion where id_service_rate_fk=:serviceRatePk  ");
		
		int i=0;
		if (Validations.validateIsNotNull(serviceRate)){
			
			Query query= em.createNativeQuery(sb.toString());		
			query.setParameter("serviceRatePk", serviceRate.getIdServiceRatePk());
			i=query.executeUpdate();
			
		}
		return i;
	}
	
	/**
	 * Removes the rate inclusion all.
	 *
	 * @param serviceRate the service rate
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int removeRateInclusionAll(ServiceRate serviceRate) throws ServiceException{
		StringBuilder sb = new StringBuilder();
		sb.append("delete from rate_inclusion where id_service_rate_fk=:serviceRatePk ");
		
		int i=0;
		if (Validations.validateIsNotNull(serviceRate)){
			
			Query query= em.createNativeQuery(sb.toString());	
			
			query.setParameter("serviceRatePk", serviceRate.getIdServiceRatePk());
			i=query.executeUpdate();
			
		}
		return i;
	}
	
	/**
	 * Removes the rate movement all.
	 *
	 * @param serviceRate the service rate
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int removeRateMovementAll(ServiceRate serviceRate) throws ServiceException{
		
		StringBuilder sb = new StringBuilder();
		sb.append("delete from rate_movement  where id_service_rate_fk=:serviceRatePk ");
		int i=0;
		if (Validations.validateIsNotNull(serviceRate)){
			
			Query query= em.createNativeQuery(sb.toString());	
			
			query.setParameter("serviceRatePk", serviceRate.getIdServiceRatePk());
			
			i=query.executeUpdate();
		}
		return i;
	}
	
	
	/**
	 * Removes the service rate scale all.
	 *
	 * @param serviceRate the service rate
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int removeServiceRateScaleAll(ServiceRate serviceRate) throws ServiceException{
		
		StringBuilder sb = new StringBuilder();
		sb.append("delete from service_rate_scale  where id_service_rate_fk=:serviceRatePk ");
		int i=0;
		if (Validations.validateIsNotNull(serviceRate)){
			
			Query query= em.createNativeQuery(sb.toString());	
			
			query.setParameter("serviceRatePk", serviceRate.getIdServiceRatePk());
			
			i=query.executeUpdate();
		}
		return i;
	}
	
	
	
	/**
	 * Gets the all service rate by billing service.
	 *
	 * @param billingServiceId the billing service id
	 * @return the all service rate by billing service
	 * @throws ServiceException the service exception
	 */
	public List<ServiceRate> getAllServiceRateByBillingService(Long billingServiceId) throws ServiceException{
		
		StringBuilder sb = new StringBuilder();
		sb.append(" select DISTINCT sr from ServiceRate sr   left join fetch sr.rateMovements ");
		sb.append(" where  1=1 ");
		if(Validations.validateIsNotNullAndPositive(billingServiceId))
		sb.append(" sr.billingService.idBillingServicePk=:billingServiceId ");
		sb.append(" and sr.rateState =:state");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		if(Validations.validateIsNotNullAndPositive(billingServiceId))
		parameters.put("billingServiceId", billingServiceId);
		parameters.put("state", RateStateType.REGISTERED.getCode());
		List list =findListByQueryString(sb.toString(), parameters);
		
		
		if (Validations.validateIsNotNullAndNotEmpty(list) && list.size()>0){
		
		/* getting description type of rate */	
		ServiceRate serviceRate = (ServiceRate)list.get(0);
		Integer rateType =serviceRate.getRateType();				
		List<ParameterTable> descriptionRateTypeList = getListParameterTable(rateType);
	    
		/* Setting description rate  service type */
		if (Validations.validateListIsNotNullAndNotEmpty(descriptionRateTypeList)){
			for (Object objServiceRate : list) {			
					ServiceRate serviceRateAux = (ServiceRate)objServiceRate;
					for (Object objRateType : descriptionRateTypeList) {
						ParameterTable parameterTable =(ParameterTable)objRateType;
						if (serviceRateAux.getRateType().intValue()==parameterTable.getParameterTablePk().intValue()){
							serviceRateAux.setDescriptionType(parameterTable.getParameterName());
							break;
						}
					}
				}		
		}
		
		/* Setting description  rate service status*/
		ParameterTableTO filter = new ParameterTableTO();
		filter.setMasterTableFk(PropertiesConstants.RATE_STATUS);
		List<ParameterTable> rateServiceTypeList =getListParameterTable(filter);
			if (Validations.validateListIsNotNullAndNotEmpty(rateServiceTypeList)){
				
				for (Object objServiceRate : list) {			
						ServiceRate serviceRateAux = (ServiceRate)objServiceRate;
						for (Object objRateType : rateServiceTypeList) {
							ParameterTable parameterTable =(ParameterTable)objRateType;
							if (serviceRateAux.getRateState().intValue()==parameterTable.getParameterTablePk().intValue()){
								serviceRateAux.setDescriptionStatus(parameterTable.getParameterName());
								break;
							}
						}
				}
			}		
		}
        
		return list;
	}
	


	
	
/**
 * Gets the services rate to expited.
 *
 * @param billingServiceId the billing service id
 * @param dateSystem the date system
 * @return the services rate to expited
 * @throws ServiceException the service exception
 */
// Search ServiceRate Actived and Lock
	public List<ServiceRate> getServicesRateToExpited(Long billingServiceId, Date dateSystem) throws ServiceException{
		
		StringBuilder sb = new StringBuilder();
		sb.append(" select DISTINCT sr from ServiceRate sr  " );
		sb.append(	  " where  sr.billingService.idBillingServicePk = :billingServiceId 	 " );
		sb.append(	  " and (sr.rateState = :stateActive or sr.rateState = :stateLock )		 " );
		
		if(Validations.validateIsNotNull(dateSystem)){
			sb.append(	  " and trunc(sr.endEffectiveDate) < trunc(:dateSystem)		 " );
		}
		
		sb.append(" order  by sr.idServiceRatePk");
		
		
		List<ServiceRate> serviceRateList =null;
						
			Query query= em.createQuery(sb.toString());
			
			query.setParameter("billingServiceId",  billingServiceId); 
			query.setParameter("stateActive", RateStateType.ACTIVED.getCode() );
			query.setParameter("stateLock",   RateStateType.LOCKED.getCode()  );
			
			if(Validations.validateIsNotNull(dateSystem)){
				query.setParameter("dateSystem",  dateSystem);
			}
			
			serviceRateList = query.getResultList();
		
		return serviceRateList;  
	}
	
 

	
/**
 * Gets the all services rate to active.
 *
 * @param billingServiceId the billing service id
 * @return the all services rate to active
 * @throws ServiceException the service exception
 */
// Search ALL ServiceRate
	public List<ServiceRate> getAllServicesRateToActive(Long billingServiceId) throws ServiceException{
		
		StringBuilder sb = new StringBuilder();
		sb.append(" select DISTINCT sr from ServiceRate sr   " );
		sb.append(" where sr.billingService.idBillingServicePk = :billingServiceId	  "	);
		sb.append(" and not sr.rateState = :stateCancelled  and not  sr.rateState = :stateExpired  "	);
		sb.append(" order  by sr.idServiceRatePk	");

		List<ServiceRate> serviceRateList =null;
						
			Query query= em.createQuery(sb.toString());
			
			query.setParameter("billingServiceId",  billingServiceId);
			query.setParameter("stateCancelled",  RateStateType.CANCELED.getCode());	
			query.setParameter("stateExpired",  RateStateType.EXPIRED.getCode());	
			
			serviceRateList = query.getResultList();
		
		return serviceRateList;  
	}
		
	
	/**
	 * Gets the services rate to active.
	 *
	 * @param billingServiceId the billing service id
	 * @param dateSystem the date system
	 * @return the services rate to active
	 * @throws ServiceException the service exception
	 */
	// Search ServiceRate Register
		public List<ServiceRate> getServicesRateToActive(Long billingServiceId, Date dateSystem ) throws ServiceException{
			
			StringBuilder sb = new StringBuilder();
			sb.append(" select DISTINCT sr from ServiceRate sr   " );
			sb.append(  " where sr.billingService.idBillingServicePk = :billingServiceId	  "	);
			sb.append(  " and sr.rateState = :stateRegister  	                                  " );
			
			if(Validations.validateIsNotNull(dateSystem)){
				sb.append( "  trunc(sr.initialEffectiveDate) <=  trunc(:dateSystem) 		      "	);
			}
			
			sb.append(" order  by sr.idServiceRatePk	");
			 
			
			List<ServiceRate> serviceRateList =null;
							
				Query query= em.createQuery(sb.toString());
				
				query.setParameter("billingServiceId",  billingServiceId);			
				query.setParameter("stateRegister", RateStateType.REGISTERED.getCode());
				
				if(Validations.validateIsNotNull(dateSystem)){
					query.setParameter("dateSystem",  CommonsUtilities.currentDate());
				}
				
				serviceRateList = query.getResultList();
			
			return serviceRateList;  
		}
		

/**
 * Gets the services rate state register.
 *
 * @param billingServiceId the billing service id
 * @param dateSystem the date system
 * @return the services rate state register
 * @throws ServiceException the service exception
 */
// Search ServiceRate Register
		public List<ServiceRate> getServicesRateStateRegister(Long billingServiceId, Date dateSystem ) throws ServiceException{
			
			StringBuilder sb = new StringBuilder();
			sb.append(" select DISTINCT sr from ServiceRate sr " );
			sb.append(" where sr.billingService.idBillingServicePk = :billingServiceId " );
			sb.append(" and sr.rateState = :stateRegister " );
			
			if(Validations.validateIsNotNull(dateSystem)){
				sb.append(" trunc(sr.initialEffectiveDate) <= trunc(:dateSystem) " );
			}
			
			sb.append(" order by sr.idServiceRatePk ");
			 
			
			List<ServiceRate> serviceRateList =null;
							
				Query query= em.createQuery(sb.toString());
				
				query.setParameter("billingServiceId",  billingServiceId);			
				query.setParameter("stateRegister", RateStateType.REGISTERED.getCode());
				
				if(Validations.validateIsNotNull(dateSystem)){
					query.setParameter("dateSystem",  CommonsUtilities.currentDate());
				}
				
				serviceRateList = query.getResultList();
			
			return serviceRateList;  
		}

/**
 * Gets the services rate actived lock.
 *
 * @param billingServiceId the billing service id
 * @param dateSystem the date system
 * @return the services rate actived lock
 * @throws ServiceException the service exception
 */
// Search ServiceRate Actived and Lock
		public List<ServiceRate> getServicesRateActivedLock(Long billingServiceId, Date dateSystem) throws ServiceException{
			
			StringBuilder sb = new StringBuilder();
			sb.append(" select DISTINCT sr from ServiceRate sr  " );
			sb.append(	  " where  sr.billingService.idBillingServicePk = :billingServiceId 	 " );
			sb.append(	  " and (sr.rateState = :stateActive or sr.rateState = :stateLock )		 " );
			
			if(Validations.validateIsNotNull(dateSystem)){
				sb.append(	  " and trunc(sr.endEffectiveDate) < trunc(:dateSystem)		 " );
			}
			
			sb.append(" order  by sr.idServiceRatePk");
			
			
			List<ServiceRate> serviceRateList =null;
							
				Query query= em.createQuery(sb.toString());
				
				query.setParameter("billingServiceId",  billingServiceId); 
				query.setParameter("stateActive", RateStateType.ACTIVED.getCode() );
				query.setParameter("stateLock",   RateStateType.LOCKED.getCode()  );
				
				if(Validations.validateIsNotNull(dateSystem)){
					query.setParameter("dateSystem",  dateSystem);
				}
				
				serviceRateList = query.getResultList();
			
			return serviceRateList;  
		}

		
}

