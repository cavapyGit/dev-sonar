package com.pradera.billing.billingrate.to;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RateServiceScaleToDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class RateServiceScaleToDataModel extends ListDataModel<ServiceRateScaleTo> implements SelectableDataModel<ServiceRateScaleTo>, Serializable {
	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new rate service scale to data model.
	 */
	public RateServiceScaleToDataModel() {
		
	}
	
	/**
	 * Instantiates a new rate service scale to data model.
	 *
	 * @param data the data
	 */
	public RateServiceScaleToDataModel(List<ServiceRateScaleTo> data) {
		super(data);	
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public ServiceRateScaleTo getRowData(String rowKey) {
		@SuppressWarnings("unchecked")
		List<ServiceRateScaleTo> serviceRateScaleToList=(List<ServiceRateScaleTo>)getWrappedData();
        for(ServiceRateScaleTo serviceRateScaleTo : serviceRateScaleToList) {  
        	
            if(String.valueOf(serviceRateScaleTo.getIdServiceRateScalePk()).equals(rowKey)) {
                return serviceRateScaleTo;  
            }
        }  
		return null;
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(ServiceRateScaleTo serviceRateScaleTo) {
		// TODO Auto-generated method stub
		return serviceRateScaleTo.getIdServiceRateScalePk();
	}



}
