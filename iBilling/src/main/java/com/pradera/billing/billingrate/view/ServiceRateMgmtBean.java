package com.pradera.billing.billingrate.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.apache.log4j.Logger;

import com.pradera.billing.billingrate.facade.BillingRateMgmtServiceFacade;
import com.pradera.billing.billingrate.to.RateMovementTo;
import com.pradera.billing.billingrate.to.RateServiceScaleToDataModel;
import com.pradera.billing.billingrate.to.RateServiceToDataModel;
import com.pradera.billing.billingrate.to.ServiceRateScaleTo;
import com.pradera.billing.billingrate.to.ServiceRateTo;
import com.pradera.billing.billingservice.facade.BillingServiceMgmtServiceFacade;
import com.pradera.billing.billingservice.to.BillingServiceTo;
import com.pradera.billing.billingservice.type.RateServiceStatus;
import com.pradera.billing.billingservice.view.BillingServiceDataModel;
import com.pradera.billing.common.facade.BusinessLayerCommonFacade;
import com.pradera.billing.common.utils.BillingUtils;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.ServiceRate;
import com.pradera.model.billing.type.RateStateType;
import com.pradera.model.billing.type.RateType;
import com.pradera.model.component.MovementType;
import com.pradera.model.generalparameter.ParameterTable;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ServiceRateMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class ServiceRateMgmtBean extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4235424872271583422L;

	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;

	/** The billing service mgmt service facade. */
	@EJB
	BillingServiceMgmtServiceFacade billingServiceMgmtServiceFacade;

	/** The billing rate mgmt service facade. */
	@EJB
	BillingRateMgmtServiceFacade billingRateMgmtServiceFacade;

	/** The business layer common facade. */
	@EJB
	BusinessLayerCommonFacade businessLayerCommonFacade;

	/** The user info. */
	@Inject
	UserInfo userInfo;

	/**  parameters used on page search. */
	private List<ParameterTable> listCollectionInstitution;
	
	/** The list service type. */
	private List<ParameterTable> listServiceType;
	
	/** The list service status. */
	private List<ParameterTable> listServiceStatus;
	
	/** The list collection base. */
	private List<ParameterTable> listCollectionBase;
	
	/** The list rate status. */
	private List<ParameterTable> listRateStatus;
	
	/** The list currency service. */
	private List<ParameterTable> listCurrencyService;
	
	/** The list rate movement. */
	private List<ParameterTable> listRateMovement;

    
	/** The data model movement type list. */
	private GenericDataModel<MovementType> 		dataModelMovementTypeList;
	
	/** The data model movement type list consult. */
	private GenericDataModel<MovementType> 		dataModelMovementTypeListConsult;
	
	/** The data model rate movement to list. */
	private GenericDataModel<RateMovementTo> 	dataModelRateMovementToList;
	
	/** The data model rate movement to list consult. */
	private GenericDataModel<RateMovementTo> 	dataModelRateMovementToListConsult;
	
	/** The rate service scale to data model. */
	private RateServiceScaleToDataModel 		rateServiceScaleToDataModel;
	
	/** The rate service scale to data model consult. */
	private RateServiceScaleToDataModel 		rateServiceScaleToDataModelConsult;
	
	/** The rate service to data model. */
	private RateServiceToDataModel 				rateServiceToDataModel;
	
	/**  attributes for get values on page search of rates. */
	private Integer collectionInstitutionSelected;
	
	/**
	 * Gets the list rate status.
	 *
	 * @return the list rate status
	 */
	public List<ParameterTable> getListRateStatus() {
		return listRateStatus;
	}


	/**
	 * Sets the list rate status.
	 *
	 * @param listRateStatus the new list rate status
	 */
	public void setListRateStatus(List<ParameterTable> listRateStatus) {
		this.listRateStatus = listRateStatus;
	}

	/** The service code. */
	private String serviceCode;
	
	/** The service type selected. */
	private Integer serviceTypeSelected;
	
	/** The service status selected. */
	private Integer serviceStatusSelected;
	
	/** The new status rate selected. */
	private Integer newStatusRateSelected;

	/** The disable btn modify. */
	private Boolean disableBtnModify;
	
	/** The disable btn add. */
	private Boolean disableBtnAdd;


	/**  parameters used on page search. */
	private List<ParameterTable> listRateType;
	
	/** The list currency rate. */
	private List<ParameterTable> listCurrencyRate;
	
	/** The list status service rate. */
	private List<ParameterTable> listStatusServiceRate;
	
	/** The rate type selected. */
	private Integer rateTypeSelected;
	
	/** The billing service selection. */
	private BillingService billingServiceSelection;
	
	/** The billing service data model. */
	private BillingServiceDataModel billingServiceDataModel;	
	
	/** The service rate to. */
	private ServiceRateTo serviceRateTo;
	
	/** The service rate consult to. */
	private ServiceRateTo serviceRateConsultTo;

	/** The billing service to. */
	private BillingServiceTo billingServiceTo;
	
	/** The add rate disable. */
	private Boolean addRateDisable;
	
	/** The delete rate disable. */
	private Boolean deleteRateDisable;
	
	/** The flag dialog modify. */
	private Boolean flagDialogModify;

	/** The Constant log. */
	private static final Logger log = Logger.getLogger(ServiceRateMgmtBean.class);

	/**  parameters used on helper movement types. */
	private Boolean renderedDialog=Boolean.FALSE;
	
	/** The list module movement. */
	private List<ParameterTable> listModuleMovement;
	
	/** The list movement empty. */
	private Boolean listMovementEmpty;	



	/**  Attributes for disable buttoms. */

	private Boolean disableBeforeAddTypeRate=Boolean.FALSE;
	
	/** The disable before clean type rate. */
	private Boolean disableBeforeCleanTypeRate=Boolean.FALSE;
	
	/** The disable before back type rate. */
	private Boolean disableBeforeBackTypeRate=Boolean.FALSE;
	
	

	/** The ind staggered percentage mix. */
	private Boolean indStaggeredPercentageMix=Boolean.FALSE;
	
	/** The ind staggered fixed mix. */
	private Boolean indStaggeredFixedMix=Boolean.FALSE;

	/**
	 * Inits the.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException {
		initDataModel();
		if (!FacesContext.getCurrentInstance().isPostback()) {
			log.debug("Carga de parámetros generales:  ");
			loadCmbDataSearch();			
			this.billingServiceTo = new BillingServiceTo();

		}

	}

	/**
	 * Inits the data model.
	 */
	public void initDataModel(){
		
		dataModelMovementTypeList 			= new GenericDataModel<>();
		
		dataModelMovementTypeListConsult 	= new GenericDataModel<>();
		
		dataModelRateMovementToList 		= new GenericDataModel<>();
		
		dataModelRateMovementToListConsult 	= new GenericDataModel<>();
		
		rateServiceScaleToDataModel 		= new RateServiceScaleToDataModel();
		
		rateServiceScaleToDataModelConsult 	= new RateServiceScaleToDataModel();
		
		rateServiceToDataModel				= new RateServiceToDataModel();
	}
	
	/**
	 * Gets the service code.
	 *
	 * @return the service code
	 */
	public String getServiceCode() {
		return serviceCode;
	}


	/**
	 * Sets the service code.
	 *
	 * @param serviceCode the new service code
	 */
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}


	/**
	 * Gets the new status rate selected.
	 *
	 * @return the new status rate selected
	 */
	public Integer getNewStatusRateSelected() {
		return newStatusRateSelected;
	}


	/**
	 * Sets the new status rate selected.
	 *
	 * @param newStatusRateSelected the new new status rate selected
	 */
	public void setNewStatusRateSelected(Integer newStatusRateSelected) {
		this.newStatusRateSelected = newStatusRateSelected;
	}

	/**
	 * Gets the list module movement.
	 *
	 * @return the list module movement
	 */
	public List<ParameterTable> getListModuleMovement() {
		return listModuleMovement;
	}


	/**
	 * Sets the list module movement.
	 *
	 * @param listModuleMovement the new list module movement
	 */
	public void setListModuleMovement(List<ParameterTable> listModuleMovement) {
		this.listModuleMovement = listModuleMovement;
	}



	/**
	 * Gets the disable before add type rate.
	 *
	 * @return the disable before add type rate
	 */
	public Boolean getDisableBeforeAddTypeRate() {
		return disableBeforeAddTypeRate;
	}


	/**
	 * Sets the disable before add type rate.
	 *
	 * @param disableBeforeAddTypeRate the new disable before add type rate
	 */
	public void setDisableBeforeAddTypeRate(Boolean disableBeforeAddTypeRate) {
		this.disableBeforeAddTypeRate = disableBeforeAddTypeRate;
	}


	/**
	 * Gets the disable before clean type rate.
	 *
	 * @return the disable before clean type rate
	 */
	public Boolean getDisableBeforeCleanTypeRate() {
		return disableBeforeCleanTypeRate;
	}


	/**
	 * Sets the disable before clean type rate.
	 *
	 * @param disableBeforeCleanTypeRate the new disable before clean type rate
	 */
	public void setDisableBeforeCleanTypeRate(Boolean disableBeforeCleanTypeRate) {
		this.disableBeforeCleanTypeRate = disableBeforeCleanTypeRate;
	}


	/**
	 * Gets the disable before back type rate.
	 *
	 * @return the disable before back type rate
	 */
	public Boolean getDisableBeforeBackTypeRate() {
		return disableBeforeBackTypeRate;
	}


	/**
	 * Sets the disable before back type rate.
	 *
	 * @param disableBeforeBackTypeRate the new disable before back type rate
	 */
	public void setDisableBeforeBackTypeRate(Boolean disableBeforeBackTypeRate) {
		this.disableBeforeBackTypeRate = disableBeforeBackTypeRate;
	}


	/**
	 * Gets the ind staggered percentage mix.
	 *
	 * @return the indStaggeredPercentageMix
	 */
	public Boolean getIndStaggeredPercentageMix() {
		return indStaggeredPercentageMix;
	}


	/**
	 * Sets the ind staggered percentage mix.
	 *
	 * @param indStaggeredPercentageMix the indStaggeredPercentageMix to set
	 */
	public void setIndStaggeredPercentageMix(Boolean indStaggeredPercentageMix) {
		this.indStaggeredPercentageMix = indStaggeredPercentageMix;
	}


	/**
	 * Gets the ind staggered fixed mix.
	 *
	 * @return the indStaggeredFixedMix
	 */
	public Boolean getIndStaggeredFixedMix() {
		return indStaggeredFixedMix;
	}


	/**
	 * Sets the ind staggered fixed mix.
	 *
	 * @param indStaggeredFixedMix the indStaggeredFixedMix to set
	 */
	public void setIndStaggeredFixedMix(Boolean indStaggeredFixedMix) {
		this.indStaggeredFixedMix = indStaggeredFixedMix;
	}


	/**
	 * Gets the list movement empty.
	 *
	 * @return the list movement empty
	 */
	public Boolean getListMovementEmpty() {
		return listMovementEmpty;
	}


	/**
	 * Sets the list movement empty.
	 *
	 * @param listMovementEmpty the new list movement empty
	 */
	public void setListMovementEmpty(Boolean listMovementEmpty) {
		this.listMovementEmpty = listMovementEmpty;
	}




	/**
	 * Gets the rendered dialog.
	 *
	 * @return the rendered dialog
	 */
	public Boolean getRenderedDialog() {
		return renderedDialog;
	}


	/**
	 * Sets the rendered dialog.
	 *
	 * @param renderedDialog the new rendered dialog
	 */
	public void setRenderedDialog(Boolean renderedDialog) {
		this.renderedDialog = renderedDialog;
	}


	/**
	 * Gets the billing service to.
	 *
	 * @return the billing service to
	 */
	public BillingServiceTo getBillingServiceTo() {
		return billingServiceTo;
	}

	/**
	 * Sets the billing service to.
	 *
	 * @param billingServiceTo the new billing service to
	 */
	public void setBillingServiceTo(BillingServiceTo billingServiceTo) {
		this.billingServiceTo = billingServiceTo;
	}

	/**
	 * Gets the rate type selected.
	 *
	 * @return the rate type selected
	 */
	public Integer getRateTypeSelected() {
		return rateTypeSelected;
	}

	/**
	 * Sets the rate type selected.
	 *
	 * @param rateTypeSelected the new rate type selected
	 */
	public void setRateTypeSelected(Integer rateTypeSelected) {
		this.rateTypeSelected = rateTypeSelected;
	}


	/**
	 * Gets the disable btn modify.
	 *
	 * @return the disable btn modify
	 */
	public Boolean getDisableBtnModify() {
		return disableBtnModify;
	}

	/**
	 * Sets the disable btn modify.
	 *
	 * @param disableBtnModify the new disable btn modify
	 */
	public void setDisableBtnModify(Boolean disableBtnModify) {
		this.disableBtnModify = disableBtnModify;
	}

	/**
	 * Gets the disable btn add.
	 *
	 * @return the disable btn add
	 */
	public Boolean getDisableBtnAdd() {
		return disableBtnAdd;
	}

	/**
	 * Sets the disable btn add.
	 *
	 * @param disableBtnAdd the new disable btn add
	 */
	public void setDisableBtnAdd(Boolean disableBtnAdd) {
		this.disableBtnAdd = disableBtnAdd;
	}

	/**
	 * Gets the adds the rate disable.
	 *
	 * @return the adds the rate disable
	 */
	public Boolean getAddRateDisable() {
		return addRateDisable;
	}

	/**
	 * Sets the adds the rate disable.
	 *
	 * @param addRateDisable the new adds the rate disable
	 */
	public void setAddRateDisable(Boolean addRateDisable) {
		this.addRateDisable = addRateDisable;
	}


	/**
	 * Gets the delete rate disable.
	 *
	 * @return the delete rate disable
	 */
	public Boolean getDeleteRateDisable() {
		return deleteRateDisable;
	}


	/**
	 * Sets the delete rate disable.
	 *
	 * @param deleteRateDisable the new delete rate disable
	 */
	public void setDeleteRateDisable(Boolean deleteRateDisable) {
		this.deleteRateDisable = deleteRateDisable;
	}


	/**
	 * Gets the service rate to.
	 *
	 * @return the service rate to
	 */
	public ServiceRateTo getServiceRateTo() {
		return serviceRateTo;
	}

	/**
	 * Sets the service rate to.
	 *
	 * @param serviceRateTo the new service rate to
	 */
	public void setServiceRateTo(ServiceRateTo serviceRateTo) {
		this.serviceRateTo = serviceRateTo;
	}

	/**
	 * Gets the collection institution selected.
	 *
	 * @return the collection institution selected
	 */
	public Integer getCollectionInstitutionSelected() {
		return collectionInstitutionSelected;
	}

	/**
	 * Sets the collection institution selected.
	 *
	 * @param collectionInstitutionSelected the new collection institution selected
	 */
	public void setCollectionInstitutionSelected(
			Integer collectionInstitutionSelected) {
		this.collectionInstitutionSelected = collectionInstitutionSelected;
	}

	/**
	 * Gets the service type selected.
	 *
	 * @return the service type selected
	 */
	public Integer getServiceTypeSelected() {
		return serviceTypeSelected;
	}

	/**
	 * Sets the service type selected.
	 *
	 * @param serviceTypeSelected the new service type selected
	 */
	public void setServiceTypeSelected(Integer serviceTypeSelected) {
		this.serviceTypeSelected = serviceTypeSelected;
	}

	/**
	 * Gets the service status selected.
	 *
	 * @return the service status selected
	 */
	public Integer getServiceStatusSelected() {
		return serviceStatusSelected;
	}

	/**
	 * Sets the service status selected.
	 *
	 * @param serviceStatusSelected the new service status selected
	 */
	public void setServiceStatusSelected(Integer serviceStatusSelected) {
		this.serviceStatusSelected = serviceStatusSelected;
	}

	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * Gets the billing service data model.
	 *
	 * @return the billing service data model
	 */
	public BillingServiceDataModel getBillingServiceDataModel() {
		return billingServiceDataModel;
	}

	/**
	 * Sets the billing service data model.
	 *
	 * @param billingServiceDataModel the new billing service data model
	 */
	public void setBillingServiceDataModel(
			BillingServiceDataModel billingServiceDataModel) {
		this.billingServiceDataModel = billingServiceDataModel;
	}

	/**
	 * Gets the list collection institution.
	 *
	 * @return the list collection institution
	 */
	public List<ParameterTable> getListCollectionInstitution() {
		return listCollectionInstitution;
	}

	/**
	 * Sets the list collection institution.
	 *
	 * @param listCollectionInstitution the new list collection institution
	 */
	public void setListCollectionInstitution(
			List<ParameterTable> listCollectionInstitution) {
		this.listCollectionInstitution = listCollectionInstitution;
	}

	/**
	 * Gets the list collection base.
	 *
	 * @return the list collection base
	 */
	public List<ParameterTable> getListCollectionBase() {
		return listCollectionBase;
	}

	/**
	 * Sets the list collection base.
	 *
	 * @param listCollectionBase the new list collection base
	 */
	public void setListCollectionBase(List<ParameterTable> listCollectionBase) {
		this.listCollectionBase = listCollectionBase;
	}


	/**
	 * Gets the list currency rate.
	 *
	 * @return the list currency rate
	 */
	public List<ParameterTable> getListCurrencyRate() {
		return listCurrencyRate;
	}

	/**
	 * Sets the list currency rate.
	 *
	 * @param listCurrencyRate the new list currency rate
	 */
	public void setListCurrencyRate(List<ParameterTable> listCurrencyRate) {
		this.listCurrencyRate = listCurrencyRate;
	}

	/**
	 * Gets the list service type.
	 *
	 * @return the list service type
	 */
	public List<ParameterTable> getListServiceType() {
		return listServiceType;
	}


	/**
	 * Gets the list rate type.
	 *
	 * @return the list rate type
	 */
	public List<ParameterTable> getListRateType() {
		return listRateType;
	}

	/**
	 * Sets the list rate type.
	 *
	 * @param listRateType the new list rate type
	 */
	public void setListRateType(List<ParameterTable> listRateType) {
		this.listRateType = listRateType;
	}

	/**
	 * Sets the list service type.
	 *
	 * @param listServiceType the new list service type
	 */
	public void setListServiceType(List<ParameterTable> listServiceType) {
		this.listServiceType = listServiceType;
	}

	/**
	 * Gets the list service status.
	 *
	 * @return the list service status
	 */
	public List<ParameterTable> getListServiceStatus() {
		return listServiceStatus;
	}

	/**
	 * Sets the list service status.
	 *
	 * @param listServiceStatus the new list service status
	 */
	public void setListServiceStatus(List<ParameterTable> listServiceStatus) {
		this.listServiceStatus = listServiceStatus;
	}

	/**
	 * Gets the billing service selection.
	 *
	 * @return the billing service selection
	 */
	public BillingService getBillingServiceSelection() {
		return billingServiceSelection;
	}

	/**
	 * Sets the billing service selection.
	 *
	 * @param billingServiceSelection the new billing service selection
	 */
	public void setBillingServiceSelection(
			BillingService billingServiceSelection) {
		this.billingServiceSelection = billingServiceSelection;
	}

	/**
	 * Gets the list status service rate.
	 *
	 * @return the list status service rate
	 */
	public List<ParameterTable> getListStatusServiceRate() {
		return listStatusServiceRate;
	}


	/**
	 * Sets the list status service rate.
	 *
	 * @param listStatusServiceRate the new list status service rate
	 */
	public void setListStatusServiceRate(List<ParameterTable> listStatusServiceRate) {
		this.listStatusServiceRate = listStatusServiceRate;
	}	


	/**
	 * Gets the list currency service.
	 *
	 * @return the list currency service
	 */
	public List<ParameterTable> getListCurrencyService() {
		return listCurrencyService;
	}


	/**
	 * Sets the list currency services.
	 *
	 * @param listCurrecncyService the new list currency services
	 */
	public void setListCurrencyServices(List<ParameterTable> listCurrecncyService) {
		this.listCurrencyService = listCurrecncyService;
	}

	/**
	 * Gets the service rate consult to.
	 *
	 * @return the service rate consult to
	 */
	public ServiceRateTo getServiceRateConsultTo() {
		return serviceRateConsultTo;
	}


	/**
	 * Sets the service rate consult to.
	 *
	 * @param serviceRateConsultTo the new service rate consult to
	 */
	public void setServiceRateConsultTo(ServiceRateTo serviceRateConsultTo) {
		this.serviceRateConsultTo = serviceRateConsultTo;
	}
	
	/**
	 * Close modal window.
	 */
	public void closeModalWindow(){
		flagDialogModify=false; 
	}

	/**
	 * Gets the flag dialog modify.
	 *
	 * @return the flag dialog modify
	 */
	public Boolean getFlagDialogModify() {
		return flagDialogModify;
	}


	/**
	 * Sets the flag dialog modify.
	 *
	 * @param flagDialogModify the new flag dialog modify
	 */
	public void setFlagDialogModify(Boolean flagDialogModify) {
		this.flagDialogModify = flagDialogModify;
	}

	
	/**
	 * Gets the data model movement type list.
	 *
	 * @return the dataModelMovementTypeList
	 */
	public GenericDataModel<MovementType> getDataModelMovementTypeList() {
		return dataModelMovementTypeList;
	}


	/**
	 * Sets the data model movement type list.
	 *
	 * @param dataModelMovementTypeList the dataModelMovementTypeList to set
	 */
	public void setDataModelMovementTypeList(
			GenericDataModel<MovementType> dataModelMovementTypeList) {
		this.dataModelMovementTypeList = dataModelMovementTypeList;
	}


	/**
	 * Gets the data model movement type list consult.
	 *
	 * @return the dataModelMovementTypeListConsult
	 */
	public GenericDataModel<MovementType> getDataModelMovementTypeListConsult() {
		return dataModelMovementTypeListConsult;
	}


	/**
	 * Sets the data model movement type list consult.
	 *
	 * @param dataModelMovementTypeListConsult the dataModelMovementTypeListConsult to set
	 */
	public void setDataModelMovementTypeListConsult(
			GenericDataModel<MovementType> dataModelMovementTypeListConsult) {
		this.dataModelMovementTypeListConsult = dataModelMovementTypeListConsult;
	}


	/**
	 * Gets the data model rate movement to list.
	 *
	 * @return the dataModelRateMovementToList
	 */
	public GenericDataModel<RateMovementTo> getDataModelRateMovementToList() {
		return dataModelRateMovementToList;
	}


	/**
	 * Sets the data model rate movement to list.
	 *
	 * @param dataModelRateMovementToList the dataModelRateMovementToList to set
	 */
	public void setDataModelRateMovementToList(
			GenericDataModel<RateMovementTo> dataModelRateMovementToList) {
		this.dataModelRateMovementToList = dataModelRateMovementToList;
	}


	/**
	 * Gets the data model rate movement to list consult.
	 *
	 * @return the dataModelRateMovementToListConsult
	 */
	public GenericDataModel<RateMovementTo> getDataModelRateMovementToListConsult() {
		return dataModelRateMovementToListConsult;
	}


	/**
	 * Sets the data model rate movement to list consult.
	 *
	 * @param dataModelRateMovementToListConsult the dataModelRateMovementToListConsult to set
	 */
	public void setDataModelRateMovementToListConsult(
			GenericDataModel<RateMovementTo> dataModelRateMovementToListConsult) {
		this.dataModelRateMovementToListConsult = dataModelRateMovementToListConsult;
	}


	/**
	 * Gets the rate service scale to data model.
	 *
	 * @return the rateServiceScaleToDataModel
	 */
	public RateServiceScaleToDataModel getRateServiceScaleToDataModel() {
		return rateServiceScaleToDataModel;
	}


	/**
	 * Sets the rate service scale to data model.
	 *
	 * @param rateServiceScaleToDataModel the rateServiceScaleToDataModel to set
	 */
	public void setRateServiceScaleToDataModel(
			RateServiceScaleToDataModel rateServiceScaleToDataModel) {
		this.rateServiceScaleToDataModel = rateServiceScaleToDataModel;
	}


	/**
	 * Gets the rate service scale to data model consult.
	 *
	 * @return the rateServiceScaleToDataModelConsult
	 */
	public RateServiceScaleToDataModel getRateServiceScaleToDataModelConsult() {
		return rateServiceScaleToDataModelConsult;
	}


	/**
	 * Sets the rate service scale to data model consult.
	 *
	 * @param rateServiceScaleToDataModelConsult the rateServiceScaleToDataModelConsult to set
	 */
	public void setRateServiceScaleToDataModelConsult(
			RateServiceScaleToDataModel rateServiceScaleToDataModelConsult) {
		this.rateServiceScaleToDataModelConsult = rateServiceScaleToDataModelConsult;
	}


	/**
	 * Load cmb data search.
	 *
	 * @throws ServiceException the service exception
	 */
	protected void loadCmbDataSearch() throws ServiceException {

		ParameterTableTO parameterTableTO = new ParameterTableTO();

		if (Validations.validateListIsNullOrEmpty(listCollectionInstitution)) {
			log.debug("lista de instituciones de cobranza :");
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO
			.setMasterTableFk(PropertiesConstants.COLLECTION_ENTITY_PK);
			listCollectionInstitution = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		}

		if (Validations.validateListIsNullOrEmpty(listServiceStatus)) {
			log.debug("lista de estados del servicio:");
			parameterTableTO
			.setMasterTableFk(PropertiesConstants.SERVICE_STATUS_PK);
			listServiceStatus = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		}

		if (Validations.validateListIsNullOrEmpty(listServiceType)) {
			log.debug("lista de tipos  de servicio:");
			parameterTableTO.setMasterTableFk(PropertiesConstants.SERVICE_TYPE_PK);
			parameterTableTO.setElementSubclasification1(GeneralConstants.ONE_VALUE_INTEGER);
			listServiceType = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}
	}


	/**
	 * Load data services.
	 *
	 * @throws ServiceException the service exception
	 */
	protected void loadDataServices() throws ServiceException {

		log.debug(" cargando listas si no están en sesión :");
		ParameterTableTO parameterTableTO = new ParameterTableTO();

		if (Validations.validateListIsNullOrEmpty(listCollectionInstitution)) {
			log.debug("lista de instituciones de cobranza :");
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO
			.setMasterTableFk(PropertiesConstants.COLLECTION_ENTITY_PK);
			listCollectionInstitution = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		}

		if (Validations.validateListIsNullOrEmpty(listServiceType)) {
			log.debug("lista de tipos  de servicio:");
			parameterTableTO
			.setMasterTableFk(PropertiesConstants.SERVICE_TYPE_PK);
			listServiceType = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		}

		if (Validations.validateListIsNullOrEmpty(listCollectionBase)) {
			log.debug("lista de base de cobro:");
			parameterTableTO.setElementSubclasification1(null);
			parameterTableTO.setOrderbyParameterTableCd(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO.setMasterTableFk(PropertiesConstants.COLLECTION_BASE_PK);
			listCollectionBase = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}
		if (Validations.validateListIsNullOrEmpty(listCurrencyService)) {
			log.debug("lista de tipo de monedas :");

			parameterTableTO
			.setMasterTableFk(PropertiesConstants.CURRENCY_TYPE_PK);
			listCurrencyService = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		}

	}

	/**
	 * Load data rate service.
	 *
	 * @throws ServiceException the service exception
	 */
	protected void loadDataRateService() throws ServiceException {

		log.debug(" cargando listas si no estan en sesion :");
		ParameterTableTO parameterTableTO = new ParameterTableTO();

		if (Validations.validateListIsNullOrEmpty(listCurrencyRate)) {
			log.debug("lista de instituciones de cobranza :");

			parameterTableTO.setMasterTableFk(PropertiesConstants.CURRENCY_TYPE_PK);
			listCurrencyRate = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		}

		if (Validations.validateListIsNullOrEmpty(listRateType)) {
			log.debug("lista de instituciones de cobranza :");

			parameterTableTO.setMasterTableFk(PropertiesConstants.RATE_TYPE);
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			listRateType = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		}
		if (Validations.validateListIsNullOrEmpty(listStatusServiceRate)) {
			log.debug("lista de estado de servicio de cobro :");

			parameterTableTO.setMasterTableFk(PropertiesConstants.RATE_STATUS);
			listStatusServiceRate = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		}
		if (Validations.validateListIsNullOrEmpty(listCurrencyService)) {
			log.debug("lista de tipo de monedas :");

			parameterTableTO.setMasterTableFk(PropertiesConstants.CURRENCY_TYPE_PK);
			listCurrencyService = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}
	}

	/**
	 * Search service billing.
	 *
	 * @param event the event
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void searchServiceBilling(ActionEvent event) throws ServiceException {

		log.debug(" busqueda de servicios de facturacion segun filtro");
		BillingServiceTo billingServiceTo = new BillingServiceTo();

		billingServiceTo.setEntityCollection(this.collectionInstitutionSelected);
		billingServiceTo.setServiceType(this.serviceTypeSelected);
		billingServiceTo.setStateService(this.serviceStatusSelected);
		billingServiceTo.setServiceCode(this.serviceCode);

		List<BillingService> listBillingService = billingServiceMgmtServiceFacade.getBillingServiceListAutomaticAndEventual(billingServiceTo);

		billingServiceDataModel = new BillingServiceDataModel(listBillingService);

		/** clean selected on data table */
		this.billingServiceSelection=null;

		if(billingServiceDataModel.getRowCount()>0){

			setTrueValidButtons();
		}		

	}


	/**
	 * Enable buttons search.
	 */
	public void enableButtonsSearch(){
		this.setDisableBtnAdd(false);
		this.setDisableBtnModify(false);
	}

	/**
	 * Disabled buttons search.
	 */
	public void disabledButtonsSearch(){
		this.setDisableBtnAdd(true);
		this.setDisableBtnModify(true);
	}

	/**
	 * Adds the rate.
	 *
	 * @param event the event
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void addRate(ActionEvent event) throws ServiceException { 

		try {


			if (event != null) {


				if (Validations.validateIsNotNull(billingServiceSelection)) {

					loadDataServices();
					loadDataRateService();
					this.billingServiceTo = new BillingServiceTo(this.billingServiceSelection);
					this.serviceRateTo = new ServiceRateTo();
					this.serviceRateTo.setInitialEffectiveDate(this.billingServiceTo.getInitialEffectiveDate());
					this.serviceRateTo.setEndEffectiveDate(this.billingServiceTo.getEndEffectiveDate());
					this.serviceRateTo.setCurrencyRate(this.billingServiceTo.getCurrencyBilling());
					this.addRateDisable=Boolean.TRUE;
					this.deleteRateDisable=Boolean.TRUE;

					List<ServiceRate> serviceRateList = billingRateMgmtServiceFacade.getServicesRateByAllBillingService(billingServiceSelection.getIdBillingServicePk());

					if (Validations.validateIsNotNullAndNotEmpty(serviceRateList)) {
						businessLayerCommonFacade.addServiceRateTo(billingServiceTo, serviceRateList);
					} 
				} 
			}


		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));

		}

	}

	/**
	 * Consult billing service.
	 *
	 * @param event the event
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb		
	public void consultBillingService (ActionEvent event) throws ServiceException{

		if (event != null) {
			BillingService billingServiceSearch = (BillingService) event.getComponent().getAttributes().get("blockInformation");
			loadDataServices();
			loadDataRateService();
			this.billingServiceTo = new BillingServiceTo(billingServiceSearch);
			this.serviceRateTo = new ServiceRateTo();
			this.addRateDisable=Boolean.TRUE;
			this.deleteRateDisable=Boolean.TRUE;

			List<ServiceRate> serviceRateList = billingRateMgmtServiceFacade.getServicesRateByAllBillingServiceToScale(billingServiceSearch.getIdBillingServicePk());
			
			if (Validations.validateIsNotNullAndNotEmpty(serviceRateList)) {
				businessLayerCommonFacade.addServiceRateTo(billingServiceTo, serviceRateList);
			} 
		}
	}


	/**
	 * Method invoke for show a new Service Rate.
	 *
	 * @param event the event
	 */
	public void consultBillingServiceRate(ActionEvent event){
		if (event != null) {
			
			ServiceRateTo serviceRateToEvent=  (ServiceRateTo)event.getComponent().getAttributes().get("serviceRate");
			this.serviceRateTo.setServiceRateTo(serviceRateToEvent);
			this.serviceRateTo.disableFlagRateServices();

			JSFUtilities.hideComponent("formAddRate:cnIdFlagConsult");
			JSFUtilities.hideGeneralDialogues();

			if (this.serviceRateTo.getRateType().equals(RateType.FIJO.getCode())) {
				
				this.serviceRateTo.setFlagFixed(Boolean.TRUE);		
				
			} else if(this.serviceRateTo.getRateType().equals(RateType.PORCENTUAL.getCode())) {		
				
				this.serviceRateTo.setFlagPercentage(Boolean.TRUE);					
				
			} else if(this.serviceRateTo.getRateType().equals(RateType.ESCALONADO_FIJO.getCode())) {
				
				this.serviceRateTo.setFlagStaggeredFixed(Boolean.TRUE);					
				this.getRateServiceScaleToDataModel().setWrappedData(this.serviceRateTo.getListServiceRateScaleTo());
				this.serviceRateTo.setUnit(serviceRateTo.getListServiceRateScaleTo().get(0).getScaleType());
				
				/**Rate with Movement**/
				if(serviceRateTo.getIndScaleMovement().equals(GeneralConstants.ONE_VALUE_INTEGER)){
					this.serviceRateTo.setFlagStaggeredMovement(Boolean.TRUE);
					this.getDataModelRateMovementToList().setWrappedData(this.serviceRateTo.getListRateMovementTo());
				}
				
			} else if(this.serviceRateTo.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode())) {
				
				this.serviceRateTo.setFlagStaggeredPercentage(Boolean.TRUE);					
				this.getRateServiceScaleToDataModel().setWrappedData(this.serviceRateTo.getListServiceRateScaleTo());
				this.serviceRateTo.setUnit(serviceRateTo.getListServiceRateScaleTo().get(0).getScaleType());
				
				/**Rate with Movement**/
				if(serviceRateTo.getIndScaleMovement().equals(GeneralConstants.ONE_VALUE_INTEGER)){		
					this.serviceRateTo.setFlagStaggeredMovement(Boolean.TRUE);
					this.getDataModelRateMovementToList().setWrappedData(this.serviceRateTo.getListRateMovementTo());
				}
				
			} else if(this.serviceRateTo.getRateType().equals(RateType.ESCALONADO_MIXTO.getCode())) {
				
				this.serviceRateTo.setFlagStaggeredMix(Boolean.TRUE);					
				this.getRateServiceScaleToDataModel().setWrappedData(this.serviceRateTo.getListServiceRateScaleTo());
				this.serviceRateTo.setUnit(serviceRateTo.getListServiceRateScaleTo().get(0).getScaleType());
				
				/**Rate with Movement**/
				if(serviceRateTo.getIndScaleMovement().equals(GeneralConstants.ONE_VALUE_INTEGER)){
					this.serviceRateTo.setFlagStaggeredMovement(Boolean.TRUE);
					this.getDataModelRateMovementToList().setWrappedData(this.serviceRateTo.getListRateMovementTo());
				}
				
			} else if(this.serviceRateTo.getRateType().equals(RateType.TIPO_MOVIMIENTO_FIJO.getCode())) {
				
				this.serviceRateTo.setFlagMovementTypeFixed(Boolean.TRUE);			
				this.getDataModelRateMovementToList().setWrappedData(this.serviceRateTo.getListRateMovementTo());
				
			} else if(this.serviceRateTo.getRateType().equals(RateType.TIPO_MOVIMIENTO_PORCENTUAL.getCode())) {	
				
				this.serviceRateTo.setFlagMovementTypePercentage(Boolean.TRUE);			
				this.getDataModelRateMovementToList().setWrappedData(this.serviceRateTo.getListRateMovementTo());
				
			}

			JSFUtilities.showComponent("formAddRate:cnIdFlagConsult");
		}
	}


	/**
	 *  active button  addRateId that allow show panel type Service Rate.
	 */
	public void onSelectItemRate() {

		JSFUtilities.setValidViewComponentAndChildrens("frmRateRegister:otpRatesID");
		if (Validations.validateIsNotNull(this.serviceRateTo)) {
			if (this.serviceRateTo.getRateType().intValue() == -1) {
				this.addRateDisable = Boolean.TRUE;
			} else {
				this.addRateDisable = Boolean.FALSE;
			}
		}
		disableOutputPanelToRateType();
		cleanRateDialog();
		clean();
		
		
		
	}
	
	/**
	 * Clean rate from accumulative.
	 */
	public void cleanRateFromAccumulative(){
		serviceRateTo.setUnit(GeneralConstants.ZERO_VALUE_INTEGER);
		/***
		 * Staggered Mixed
		 */
		serviceRateTo.setIndStaging(GeneralConstants.ZERO_VALUE_INTEGER);
		cleanRateDialog();
	}
	

	/**
	 * Clean rate dialog.
	 */
	public void cleanRateDialog(){ 
		serviceRateTo.setMinScaleValue(null);
		serviceRateTo.setMaxScaleValue(null);
		serviceRateTo.setAmountRate(null);
		serviceRateTo.setPercentageRate(null);

		JSFUtilities.setValidViewComponentAndChildrens("frmRateRegister:cnIdStaggered");


		if (Validations.validateListIsNotNullAndNotEmpty(serviceRateTo.getListServiceRateScaleTo())){
			serviceRateTo.getListServiceRateScaleTo().clear();
		}
	}

	/**
	 * Clean.
	 */
	public void clean(){  

		serviceRateTo.setMinScaleValue(null);
		serviceRateTo.setMaxScaleValue(null);
		serviceRateTo.setAmountRate(null); 
		serviceRateTo.setPercentageRate(null);
		serviceRateTo.setRateAmount(null);		
		serviceRateTo.setCodeMovementType(null);
		serviceRateTo.setDescriptionMovementType(null); 
		serviceRateTo.setUnit(GeneralConstants.ZERO_VALUE_INTEGER); 
		/**
		 * Staggered Mixed
		 */
		serviceRateTo.setIndStaging(GeneralConstants.ZERO_VALUE_INTEGER);
		serviceRateTo.setCodeMovementType(null); 
		serviceRateTo.setIndScaleMovement(0);
		
		
		/** setting list */
		serviceRateTo.getListServiceRateScaleTo().clear();	
		serviceRateTo.getListRateMovementTo().clear();

		if(serviceRateTo.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode())) {
//			serviceRateTo.setUnit(1);
		}
	}
	
	/**
	 * Clean modify.
	 */
	public void cleanModify(){
		
		serviceRateTo.setMinScaleValue(null);
		serviceRateTo.setMaxScaleValue(null);
		serviceRateTo.setAmountRate(null); 
		serviceRateTo.setPercentageRate(null);
		serviceRateTo.setRateAmount(null);		
		serviceRateTo.setCodeMovementType(null);
		serviceRateTo.setDescriptionMovementType(null);
		
			if(serviceRateTo.getRateType().equals(RateType.ESCALONADO_FIJO.getCode()) ||
					serviceRateTo.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode())||
					serviceRateTo.getRateType().equals(RateType.ESCALONADO_MIXTO.getCode())){
				
				serviceRateTo.setUnit(serviceRateTo.getListServiceRateScaleTo().get(0).getScaleType());
				
				/**
				 * Values by Default Rate Type Mixed Staggered
				 */
				serviceRateTo.setIndStaging(GeneralConstants.ZERO_VALUE_INTEGER);
				indStaggeredFixedMix=Boolean.TRUE;
				indStaggeredPercentageMix=Boolean.FALSE;
			}
		
		
		}

	/**
	 * Show panel service rate.
	 */
	public void showPanelServiceRate() {


		clean();

		this.serviceRateTo.disableFlagRateServices();
		JSFUtilities.hideComponent("formAddRate:cnIdFlagFixed");
		JSFUtilities.hideComponent("formAddRate:cnIdFlagPercent");
		JSFUtilities.hideComponent("formAddRate:cnIdStaggered");
		JSFUtilities.hideComponent("formAddRate:cnIdMovement");
		JSFUtilities.hideGeneralDialogues();

		disableBeforeButtons();


		if (this.serviceRateTo.getRateType().intValue() == RateType.FIJO.getCode()) {
			this.serviceRateTo.setFlagFixed(Boolean.TRUE);		
			JSFUtilities.showComponent("formAddRate:cnIdFlagFixed");
		} else if(this.serviceRateTo.getRateType().intValue() == RateType.PORCENTUAL.getCode()) {			
			this.serviceRateTo.setFlagPercentage(Boolean.TRUE);	
			JSFUtilities.showComponent("formAddRate:cnIdFlagPercent");
		} else if(this.serviceRateTo.getRateType().intValue() == RateType.ESCALONADO_FIJO.getCode()) {
			this.serviceRateTo.setFlagStaggeredFixed(Boolean.TRUE);	
			JSFUtilities.showComponent("formAddRate:cnIdStaggered");
		} else if(this.serviceRateTo.getRateType().intValue() == RateType.TIPO_MOVIMIENTO_FIJO.getCode()) {	
			this.serviceRateTo.setFlagMovementTypeFixed(Boolean.TRUE);
			JSFUtilities.showComponent("formAddRate:cnIdMovement");
		} 

		setDisableBeforeCleanTypeRate(false);
		setDisableBeforeBackTypeRate(false);

	}


	/**
	 * Before delete rate pending.
	 */
	public void beforeDeleteRatePending(){
		// show  pop-up 
		showMessageOnDialog(
				PropertiesUtilities
				.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
				PropertiesUtilities.getMessage(
						PropertiesConstants.CONFIRM_SERVICE_RATE_SAVE));
		JSFUtilities.showComponent("frmRateRegister:cnfIdRateDeletePending");
	}

	/**
	 * Delete rate pending.
	 */
	public void deleteRatePending(){
		// 


	}
	
	
	/**
	 * Before on modify.
	 *
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public String beforeOnModify() throws ServiceException {

		String strView="";
		

		if (Validations.validateIsNotNull(billingServiceSelection)) {

			/** Modify to get rate scale**/
			List<ServiceRate> serviceRateList = billingRateMgmtServiceFacade.getServicesRateByAllBillingServiceToScale(billingServiceSelection.getIdBillingServicePk());

			
			 
//			if (Validations.validateIsNotNullAndNotEmpty(serviceRateList)) {
//				bussinessLayerCommonFacade.addServiceRateTo(billingServiceTo, serviceRateList);
//			}
			
			
			
			
			
			if(Validations.validateIsNullOrNotPositive(serviceRateList.size())){
				executeAction();
				showMessageOnDialog(PropertiesUtilities
						.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.MODIFY_RATE_EMPTY));
				JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");  


			}else{ 

				loadDataServices();
				loadDataRateService();
				loadCmbRateStatus();
				this.billingServiceTo = new BillingServiceTo(this.billingServiceSelection);
				this.billingServiceTo.setStateService(billingServiceTo.getStateService());
				this.serviceRateTo = new ServiceRateTo();							 
				this.addRateDisable=Boolean.TRUE;
				this.deleteRateDisable=Boolean.TRUE;

				if (Validations.validateListIsNotNullAndNotEmpty(serviceRateList)) {

					businessLayerCommonFacade.addServiceRateTo(billingServiceTo, serviceRateList);
					List<ServiceRateTo> serviceRateListModify =BillingUtils.cloneServiceRateToList(billingServiceTo.getListServiceRateTo());
					billingServiceTo.setListServiceRateToModify(serviceRateListModify);
//					if (billingServiceTo.getRateServiceToDataModel()!=null){
//						billingServiceTo.getRateServiceToDataModel().setWrappedData(serviceRateListModify);
//					}
					if (this.getRateServiceToDataModel()!=null){
						this.getRateServiceToDataModel().setWrappedData(serviceRateListModify);
					}
				}  

				strView=	this.goTo("editBillingRate");
			}
		}else{
			strView=	this.goTo("");	
		}


		return strView;
	}

	/**
	 * Before on consult.
	 */
	public void beforeOnConsult() {

	}

	/**
	 * Update panel rate.
	 */
	public void updatePanelRate(){

	}

	/**
	 * Go to.
	 *
	 * @param viewToGo            the view to go
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String goTo(String viewToGo) throws ServiceException {


		/** redirect for modify**/
		if (Validations.validateIsNotNull(this.billingServiceSelection)) {

			if  ( viewToGo.equals("editBillingRate")){			
				if (Validations.validateIsNotNull(billingServiceSelection))
				{   
					setViewOperationType(ViewOperationsType.MODIFY.getCode());
					viewToGo="editBillingRate"; 
				}
			}
			return viewToGo;
		}


		if (Validations.validateIsNotNull(viewToGo) && !viewToGo.equals("")){
			if  (viewToGo.equals("searchRateMgmt") || viewToGo.equals("consultBillingRate")){

				return viewToGo;
			}
		}

		executeAction();
		showMessageOnDialog(PropertiesUtilities
				.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities.getMessage(PropertiesConstants.MODIFY_RATE_NO_SELECTED));
		JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");   
		return  ""; 
	}

	/**
	 * Section for Fixed Rate.
	 */
	public void beforeAddFixed() {

		/** Only add rate service at list temporal , it's on memory but It is'nt on DB */
		ServiceRateTo serviceRateTo = SerializationUtils
				.clone(this.serviceRateTo);

		serviceRateTo.setRateStatus(RateServiceStatus.PENDING.getCode());
		serviceRateTo.setDescriptionStatus(RateServiceStatus.PENDING.getValue());

		String descriptionRateType =businessLayerCommonFacade.
				getValueParameterTable(listRateType, serviceRateTo.getRateType());
		serviceRateTo.setDescriptionType(descriptionRateType);
		this.billingServiceTo.addServiceRateTo(serviceRateTo);

		/** finish */		

		/** preparing data for next service rate */		

		this.serviceRateTo = new ServiceRateTo();
		this.addRateDisable = Boolean.TRUE;

	}

	/**
	 * Clean register fixed.
	 */
	public void cleanRegisterFixed() {

	}

	/**
	 * Section for Percentage Rate.
	 */
	public void beforeAddPercentage(){

		/** Only add rate service at list temporal , it's on memory but It is'nt on DB */
		ServiceRateTo serviceRateTo = SerializationUtils
				.clone(this.serviceRateTo);

		serviceRateTo.setRateStatus(RateServiceStatus.PENDING.getCode());
		serviceRateTo.setDescriptionStatus(RateServiceStatus.PENDING.getValue());

		String descriptionRateType =businessLayerCommonFacade.
				getValueParameterTable(listRateType, serviceRateTo.getRateType());
		serviceRateTo.setDescriptionType(descriptionRateType);
		this.billingServiceTo.addServiceRateTo(serviceRateTo);

		/** finish */		

		/** preparing data for next service rate */

		this.serviceRateTo = new ServiceRateTo();
		this.addRateDisable = Boolean.TRUE;
		JSFUtilities.hideComponent("formAddRate:cnIdFlagPercent");
		JSFUtilities.executeJavascriptFunction("PF('cnfwFlagPercent').hide();");

	}

	/**
	 * Clean register percentage.
	 */
	public void cleanRegisterPercentage() {


	}



	/**
	 * Section for Staggered Rate.
	 */


	public void generateOneStageRate(){

		this.serviceRateTo.generateOneStageRate();


	}


	/**
	 * Only add rate service at list temporal , it's on memory but It is'nt on DB.
	 */
	public void addStaggeredRate(){
		
		ServiceRateTo serviceRateTo = SerializationUtils.clone(this.serviceRateTo);

		serviceRateTo.setRateStatus(RateServiceStatus.PENDING.getCode());
		serviceRateTo.setDescriptionStatus(RateServiceStatus.PENDING.getValue());

		String descriptionRateType =businessLayerCommonFacade.getValueParameterTable(listRateType, serviceRateTo.getRateType());
		serviceRateTo.setDescriptionType(descriptionRateType);
		this.billingServiceTo.addServiceRateTo(serviceRateTo);

		/** finish */		

		/** preparing data for next service rate */

		this.serviceRateTo = new ServiceRateTo();
		this.addRateDisable = Boolean.TRUE;
		JSFUtilities.hideComponent("formAddRate:cnIdStaggered");
		JSFUtilities.executeJavascriptFunction("PF('cnfwStaggered').hide();");
	}





	/**
	 * *
	 * Section for Movement Types Rates.
	 *
	 * @param movementType the movement type
	 */
	/**
	 * 
	 * @param movementType
	 */
	public void assignMovementType(MovementType movementType){

		this.serviceRateTo.setMovementTypeSelected(movementType);
		this.serviceRateTo.setCodeMovementType(movementType.getIdMovementTypePk());
		this.serviceRateTo.setDescriptionMovementType(movementType.getMovementName());
		JSFUtilities.setValidViewComponentAndChildrens("frmRateRegister:otpRatesID");
		JSFUtilities.executeJavascriptFunction("PF('dlgMovement').hide();");

	}

	/**
	 * Show shearch movement.
	 *
	 * @throws ServiceException the service exception
	 */
	public void showShearchMovement() throws ServiceException{

		ParameterTableTO parameterTableTO = new ParameterTableTO();						
		if (Validations.validateListIsNullOrEmpty(listModuleMovement)) {
			log.debug("Listado de Modulos  de Movimientos  :");

			parameterTableTO.setMasterTableFk(PropertiesConstants.MODULE_TYPE);
			listModuleMovement = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);
		}
		JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationBlur').hide();");
		JSFUtilities.executeJavascriptFunction("PF('dlgMovement').show();");

	}

	/**
	 * Find movement typeby code.
	 *
	 * @throws ServiceException the service exception
	 */
	//METHOD CALLED FROM BLUR EVENT
	public void findMovementTypebyCode() throws ServiceException{

		Long codeMovementType = this.serviceRateTo.getCodeMovementType();
		if(Validations.validateIsNotNullAndNotEmpty(codeMovementType)){
			MovementType movementType = businessLayerCommonFacade.findMovementTypeByCode(codeMovementType);
			if(Validations.validateIsNotNull(movementType)){
				this.serviceRateTo.setMovementTypeSelected(movementType);
				this.serviceRateTo.setCodeMovementType(movementType.getIdMovementTypePk());
				this.serviceRateTo.setDescriptionMovementType(movementType.getMovementName());
			}else {


				String holderHelpMessage = PropertiesUtilities.getMessage(GeneralConstants.MOVEMENT_TYPE_PROPERTIES, JSFUtilities.getCurrentLocale(),
						PropertiesConstants.SEARCH_MOVEMENT_TYPE);
				String title = PropertiesUtilities.getMessage(GeneralConstants.MOVEMENT_TYPE_PROPERTIES, JSFUtilities.getCurrentLocale(),
						PropertiesConstants.BODY_NOT_FOUND_MOVEMENT_TYPE);
				JSFUtilities.putViewMap("bodyMessageView", holderHelpMessage);
				JSFUtilities.putViewMap("headerMessageView", title);
				JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationBlur').show();");
				//JSFUtilities.executeJavascriptFunction("cnfwMsgCustomValidationBlur.show();");			

				/** Deleting attributes on  page **/
				this.serviceRateTo.setCodeMovementType(null);
				this.serviceRateTo.setDescriptionMovementType(null);

			}

		}

	}



	/**
	 * Search movement types.
	 *
	 * @throws IllegalArgumentException the illegal argument exception
	 * @throws IllegalAccessException the illegal access exception
	 */
	//METHOD CALLED FROM CLICK EVENT
	public void searchMovementTypes() throws IllegalArgumentException, IllegalAccessException{
		log.info("Consulta de Tipos de movimiento por filtro :");		

		MovementType movementTypeSearch = this.serviceRateTo.getMovementTypeSearch();

		/** searching with paramters **/

		List<MovementType> listMovementType=businessLayerCommonFacade.findMovementTypesServiceBean(movementTypeSearch); 

		if (Validations.validateListIsNullOrEmpty(listMovementType)){			
			this.listMovementEmpty=true;
			this.setDataModelMovementTypeList(null);
			return;
		}	
		this.setDataModelMovementTypeList(new GenericDataModel(listMovementType));


	}

	/**
	 * Clear helper search movement types.
	 */
	public void clearHelperSearchMovementTypes(){

		this.serviceRateTo.setMovementTypeSearch(new MovementType());
		this.setDataModelMovementTypeList(null);				
	}


	/**
	 *  Making MovementType's List before adding to one new Service Rate  --- *.
	 */
	public void beforeAddMovementToList(){

		MovementType movementTypeNew = this.serviceRateTo.getMovementTypeSelected();
		List<RateMovementTo> listRateMovementTo = serviceRateTo.getListRateMovementTo();

		if ( Validations.validateIsNotNull(movementTypeNew) && Validations.validateIsNotNull(movementTypeNew.getIdMovementTypePk()) ){

			/** VALIDATION IF REPEAT THE IdMovement**/
			for (RateMovementTo rateMovementTo : listRateMovementTo) {
				if(listRateMovementTo.size()+1<=100){
					if( rateMovementTo.getMovementType().getIdMovementTypePk().equals(movementTypeNew.getIdMovementTypePk()) ){
						executeAction();
						showMessageOnDialog(PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_DOUBLE_MOVEMENT_CODE));
						JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
						return;
					}
				}
				else if( listRateMovementTo.size()+1>100 ) {
					executeAction();
					showMessageOnDialog(PropertiesUtilities
							.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.EXCESSIVE_MOVEMENT));
					JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
					return ; 
				}
			}

			movementTypeNew =SerializationUtils.clone(movementTypeNew);
			RateMovementTo rateMovementTo = new RateMovementTo();			
			rateMovementTo.setMovementType(movementTypeNew);
			rateMovementTo.setRateAmount(this.serviceRateTo.getAmountRate());
			rateMovementTo.setRatePercent(this.serviceRateTo.getPercentageRate());
			this.serviceRateTo.getListRateMovementTo().add(rateMovementTo);
			this.setDataModelRateMovementToList(new GenericDataModel(this.serviceRateTo.getListRateMovementTo()));

			/** Removing movement type selected  from the search*/			
			this.serviceRateTo.setMovementTypeSelected(new MovementType());
			/** Deleting attributes on  page **/
			this.serviceRateTo.setCodeMovementType(null);
			this.serviceRateTo.setDescriptionMovementType(null);
			this.serviceRateTo.setAmountRate(null);
			this.serviceRateTo.setPercentageRate(null);
		}	
	}


	/**
	 * Before delete movement to list.
	 */
	public void beforeDeleteMovementToList(){
		RateMovementTo rateMovementTo = this.serviceRateTo.getRateMovementToSelected();
		List<RateMovementTo> listRateMovementTo= this.serviceRateTo.removeMovementType(rateMovementTo, this.serviceRateTo.getListRateMovementTo());
		this.serviceRateTo.setListRateMovementTo(listRateMovementTo);
		this.setDataModelRateMovementToList(new GenericDataModel(this.serviceRateTo.getListRateMovementTo()));
	}

	/**
	 * Adds the movement rate.
	 */
	public void addMovementRate(){
		/** Only add rate service at list temporal , it's on memory but It is'nt on DB */
		ServiceRateTo serviceRateTo = SerializationUtils.clone(this.serviceRateTo);
		serviceRateTo.setRateStatus(RateServiceStatus.PENDING.getCode());
		serviceRateTo.setDescriptionStatus(RateServiceStatus.PENDING.getValue());

		String descriptionRateType =businessLayerCommonFacade.
				getValueParameterTable(listRateType, serviceRateTo.getRateType());
		serviceRateTo.setDescriptionType(descriptionRateType);
		this.billingServiceTo.addServiceRateTo(serviceRateTo);

		/** finish */		

		/** preparing data for next service rate */

		this.serviceRateTo = new ServiceRateTo();
		this.addRateDisable = Boolean.TRUE;
		JSFUtilities.hideComponent("formAddRate:cnIdMovement");
		JSFUtilities.executeJavascriptFunction("PF('cnfwMovement').hide();");
	}

	/**
	 * Clean register movement rate.
	 */
	public void cleanRegisterMovementRate(){


	}

	/**
	 * Back search movement rate.
	 */
	public void backSearchMovementRate(){


	}

	/**
	 * *
	 *  Section for Billing Rate  : save or update's.
	 */

	public void beforeAddStaggeredRate(){

	}



	/**
	 * Enable button remove service rate.
	 */
	public void enableButtonRemoveServiceRate(){
		this.addRateDisable = Boolean.TRUE;
		this.deleteRateDisable=Boolean.FALSE;
	}

	/**
	 * Delete pending service rate.
	 */
	public void deletePendingServiceRate(){

		/**Only remove one service rate from memory when its status is PENDING**/
		ServiceRateTo serviceRateTo = this.billingServiceTo.getServiceRateToSelected();
		this.billingServiceTo.deleteServiceRateTo(serviceRateTo);
		this.deleteRateDisable = Boolean.TRUE;
		JSFUtilities.hideComponent("formAddRate:cnfIdRateDeletePending");
		JSFUtilities.executeJavascriptFunction("PF('cnfwRateDeletePending').hide();");
	}

	/**
	 * Before service registration listener.
	 */
	public void beforeServiceRegistrationListener() {
		log.debug("beforeServiceRegistrationListener :");	
		List<ServiceRateTo> listServiceRateTo = billingServiceTo.getListServiceRateTo();
		Boolean stateForSave=Boolean.FALSE;
		if(Validations.validateIsNotNullAndNotEmpty(billingServiceTo.getListServiceRateTo())){
			for (ServiceRateTo serviceRateTo : listServiceRateTo) {
				if (serviceRateTo.getRateStatus().intValue()==RateServiceStatus.PENDING.getCode().intValue()){
					stateForSave=Boolean.TRUE;
					break;
				}
			}
			if(stateForSave){
				showMessageOnDialog(
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
						PropertiesUtilities.getMessage(
								PropertiesConstants.CONFIRM_SERVICE_RATE_SAVE,this.billingServiceSelection.getServiceCode()));
				JSFUtilities.executeJavascriptFunction("cnfwRateSave.show();");
			}
			else{
				executeAction();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_EMPTY_SERVICE_RATE));
				JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");  

			}
		} 
	}


	/**
	 * Save service rate.
	 *
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void saveServiceRate() throws ServiceException {
		log.info("saveServiceRate");

		List<ServiceRate> serviceRateList = billingServiceMgmtServiceFacade.prepareRateServicesForSave(billingServiceTo);		
		serviceRateList=billingRateMgmtServiceFacade.saveAllRateService(serviceRateList);	
		
		String codeRate="";
		for(int i=0;i<serviceRateList.size();i++){
			if(i>0){
				codeRate+=", "+serviceRateList.get(i).getRateCode().toString();
			}
			else{
				codeRate=serviceRateList.get(i).getRateCode().toString();
			}
		}
		showMessageOnDialog(PropertiesUtilities
				.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_SERVICE_RATE_SAVE,codeRate));
		JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
	}

	/**
	 * Before add table movement to staggered.
	 */
	public void beforeAddTableMovementToStaggered(){

		 
			beforeAddMovementToList();
			
	}
	
	/**
	 * Before add table movement fixed.
	 */
	public void beforeAddTableMovementFixed(){

		if(Validations.validateIsNullOrNotPositive(this.serviceRateTo.getAmountRate().longValue())){
			JSFUtilities.setInvalidViewComponent("formAddRate:txtAmountFixedMovement");	
			JSFUtilities.addContextMessage("formAddRate:txtAmountFixedMovement",
					FacesMessage.SEVERITY_ERROR,
					"Debe ingresar el monto", 
					"Debe ingresar el monto");
			return;
		}
		else{
			beforeAddMovementToList();
			return;			
		}
	}

	/**
	 * Before add table movement percentage.
	 */
	public void beforeAddTableMovementPercentage(){
		if(Validations.validateIsNull(this.serviceRateTo.getPercentageRate())){
			JSFUtilities.setInvalidViewComponent("formAddRate:txtAmountFixedMovementP");	
			JSFUtilities.addContextMessage("formAddRate:txtAmountFixedMovementP",
					FacesMessage.SEVERITY_ERROR,
					"Debe ingresar el monto", 
					"Debe ingresar el monto");
			return;
		}
		else{
			beforeAddMovementToList();
			return;			
		}
	}

	/**
	 * Before add type service rate.
	 */
	public void  beforeAddTypeServiceRate(){


		if( serviceRateTo.getRateName().length()>100 ){
			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SERVICE_RATE_NAME_LONG));
			JSFUtilities.executeJavascriptFunction("PF('warningDialog').show()");  


		}else if( !(serviceRateTo.getRateName().length()>100) ){
			if( serviceRateTo.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode()) ) {
				serviceRateTo.setUnit(GeneralConstants.ONE_VALUE_INTEGER);
			}
			if(serviceRateTo.getIndScaleMovement().equals(GeneralConstants.ONE_VALUE_INTEGER)){
				if(Validations.validateListIsNullOrEmpty(serviceRateTo.getListRateMovementTo())){
					showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							PropertiesUtilities.getMessage(PropertiesConstants.WARNING_EMPTY_MOVEMENT_LIST));
					JSFUtilities.executeJavascriptFunction("PF('warningDialog').show()");  
					return;
				}
			}

			Object codigo=this.billingServiceSelection.getServiceCode();
			executeAction();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
					, PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_TYPE_SERVICE_RATE_ADD,codigo));
			JSFUtilities.executeJavascriptFunction("cnfwTypeRateAdd.show();");
		}
	} 


	/**
	 * Adds the type service rate.
	 */
	public void  addTypeServiceRate(){
		
		/** Only add rate service at list temporal , it's on memory but It is'nt on DB */
		ServiceRateTo serviceRateTo = SerializationUtils.clone(this.serviceRateTo);

		serviceRateTo.setRateStatus(RateServiceStatus.PENDING.getCode());
		serviceRateTo.setDescriptionStatus(RateServiceStatus.PENDING.getValue());

		String descriptionRateType =businessLayerCommonFacade.getValueParameterTable(listRateType,serviceRateTo.getRateType());
		serviceRateTo.setDescriptionType(descriptionRateType);
		this.billingServiceTo.addServiceRateTo(serviceRateTo);

		/** preparing data for next service rate */
		this.serviceRateTo = new ServiceRateTo(billingServiceTo);
		this.serviceRateTo.setCurrencyRate(billingServiceTo.getCurrencyBilling());
		this.addRateDisable = Boolean.TRUE;

	}

	/**
	 * Clean type service rate.
	 */
	public void cleanTypeServiceRate() {

		if (this.serviceRateTo.isFlagPercentage()){
			this.serviceRateTo.setPercentageRate(null);			
		}else if (this.serviceRateTo.isFlagFixed()){
			this.serviceRateTo.setAmountRate(null);
		}else if (this.serviceRateTo.isFlagStaggeredFixed()){

			List<ServiceRateScaleTo> listServicerateScaleTo = new ArrayList<ServiceRateScaleTo>();	
			this.serviceRateTo.setSequenceNumber(null);
			this.serviceRateTo.setListServiceRateScaleTo(listServicerateScaleTo);
			this.getRateServiceScaleToDataModel().setWrappedData(listServicerateScaleTo);
			serviceRateTo.setDisableSeqNumber(false);
			serviceRateTo.setDisableBtnGenerate(false);
			serviceRateTo.setDisableUnit(false);
		}else if (this.serviceRateTo.isFlagMovementTypeFixed()){


		}

		this.setDisableBeforeAddTypeRate(true);
	}

	/**
	 * Disable before buttons.
	 */
	public void disableBeforeButtons(){
		this.setDisableBeforeAddTypeRate(true);
		this.setDisableBeforeBackTypeRate(true);
		this.setDisableBeforeCleanTypeRate(true);
	}

	/**
	 * executed this method when no there are  changes on form
	 * , only hide the dialog.
	 */

	public void beforeBackServiceRate() {

		JSFUtilities.hideComponent("formAddRate:cnIdFlagFixed");
		JSFUtilities.hideComponent("formAddRate:cnIdFlagPercent");
		JSFUtilities.hideComponent("formAddRate:cnIdStaggered");
		JSFUtilities.hideComponent("formAddRate:cnIdMovement");
		JSFUtilities.hideGeneralDialogues();
		disableBeforeButtons();

		if (this.serviceRateTo.isFlagPercentage()){ 
			this.serviceRateTo.setFlagPercentage(Boolean.FALSE);	

		}else if (this.serviceRateTo.isFlagFixed()){ 
			this.serviceRateTo.setFlagFixed(Boolean.FALSE);

		}else if (this.serviceRateTo.isFlagStaggeredFixed()){

			this.serviceRateTo.setFlagStaggeredFixed(Boolean.FALSE);
			if(this.serviceRateTo.getIdBillingService().equals(GeneralConstants.ONE_VALUE_STRING)){
				this.serviceRateTo.setFlagStaggeredMovement(Boolean.FALSE);
			}
			

		}else if (this.serviceRateTo.isFlagMovementTypeFixed()){ 
			this.serviceRateTo.setFlagMovementTypeFixed(Boolean.FALSE);
		}

		this.serviceRateTo.disableFlagRateServices();
	}


	/**
	 * Back type service rate.
	 */
	public void backTypeServiceRate(){

		

		if (this.serviceRateTo.isFlagPercentage()){
			this.serviceRateTo.setPercentageRate(null);
			this.serviceRateTo.setFlagPercentage(Boolean.FALSE);	

		}else if (this.serviceRateTo.isFlagFixed()){
			this.serviceRateTo.setAmountRate(null);
			this.serviceRateTo.setFlagFixed(Boolean.FALSE);

		}else if (this.serviceRateTo.isFlagStaggeredFixed()){
			List<ServiceRateScaleTo> listServicerateScaleTo = new ArrayList<ServiceRateScaleTo>();	
			this.serviceRateTo.setSequenceNumber(null);
			this.serviceRateTo.setListServiceRateScaleTo(listServicerateScaleTo);
			this.getRateServiceScaleToDataModel().setWrappedData(listServicerateScaleTo);

			this.serviceRateTo.setFlagStaggeredFixed(Boolean.FALSE);
			if(this.serviceRateTo.getIdBillingService().equals(GeneralConstants.ONE_VALUE_STRING)){
				this.serviceRateTo.setFlagStaggeredMovement(Boolean.FALSE);
			}
			
			

		}else if (this.serviceRateTo.isFlagMovementTypeFixed()){
			clean(); 
			this.serviceRateTo.setFlagMovementTypeFixed(Boolean.FALSE);
		}

	}


	/**
	 * getting service rate commandLink.
	 *
	 * @param event the event
	 */
	public void loadOnModifyRate(ActionEvent event){
		if(event!=null){
			ServiceRateTo serviceRateToModify=(ServiceRateTo)event.getComponent().getAttributes().get("blockInformation");

			/**Load*/
			this.serviceRateTo.setIdServiceRatePk(serviceRateToModify.getIdServiceRatePk());  /**save on memory*/
			this.serviceRateTo.setRateCode(serviceRateToModify.getRateCode());
			this.serviceRateTo.setRateType(serviceRateToModify.getRateType());
			this.serviceRateTo.setRateName(serviceRateToModify.getRateName());
			this.serviceRateTo.setCurrencyRate(serviceRateToModify.getCurrencyRate());
			this.serviceRateTo.setEndEffectiveDate(serviceRateToModify.getEndEffectiveDate());
			this.serviceRateTo.setInitialEffectiveDate(serviceRateToModify.getInitialEffectiveDate());
			this.serviceRateTo.setRateStatus(serviceRateToModify.getRateStatus());
			this.serviceRateTo.setIndScaleMovement(serviceRateToModify.getIndScaleMovement());

			/***
			 * Load Rate staggered fixed, percentage and mixed 
			 */
			if(serviceRateToModify.getRateType().equals(RateType.ESCALONADO_FIJO.getCode())||
					serviceRateToModify.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode())||
					serviceRateToModify.getRateType().equals(RateType.ESCALONADO_MIXTO.getCode())){
				if(Validations.validateIsNotNull(serviceRateToModify.getListServiceRateScaleTo())){
					serviceRateTo.setAccumulate(serviceRateToModify.getListServiceRateScaleTo().get(0).getIndAccumulativeScale());
					serviceRateTo.setListServiceRateScaleTo(serviceRateToModify.getListServiceRateScaleTo());
					this.setRateServiceScaleToDataModel(new RateServiceScaleToDataModel(serviceRateToModify.getListServiceRateScaleTo()));
					
				}
			}
			
			cleanModify();
			
			flagDialogModify=true;
			JSFUtilities.showComponent("formModalModifyRate: cnIdRateModify");
		}

	}


	/**
	 * Modificar el serviceRate en la BD.
	 *
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void modifyServiceRate() throws ServiceException {		

		log.info("modifyServiceRate");
		
		billingRateMgmtServiceFacade.modifyServiceRate(billingServiceTo);

		executeAction();		
		showMessageOnDialog(PropertiesUtilities
				.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_BILLING_SERVICE_RATE_MODIFY,
						this.billingServiceSelection.getServiceCode()) );
		JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();"); 

		/** reset billing service */
		//	this.billingServiceTo = new BillingServiceTo();

		/** Search All Billing Service by default*/
		this.collectionInstitutionSelected=-1;
		this.serviceTypeSelected=-1;
		this.serviceStatusSelected=-1;
	}


	/**
	 * Back search.
	 */
	public void backSearch(){ }

	/**
	 * El boton aceptar de la ventana Modal.
	 */
	public void beforeUpdateServiceRateDialog(){ 
		/** getting the list serviceRateTo*/
		List<ServiceRateTo>  serviceRateToListModify= this.billingServiceTo.getListServiceRateToModify();


		for (ServiceRateTo serviceRateToSeq : serviceRateToListModify) {

			if (Validations.validateIsNotNull(serviceRateToSeq)){

				if (serviceRateToSeq.getRateCode().equals(this.serviceRateTo.getRateCode())){   /* first , find object**/

					if (     !( serviceRateToSeq.getEndEffectiveDate().equals(this.serviceRateTo.getEndEffectiveDate()) ) 
							|| !(serviceRateToSeq.getRateStatus().equals(this.serviceRateTo.getRateStatus()) )            ) {  /* second, set  date  ***/

						serviceRateToSeq.setEndEffectiveDate(this.serviceRateTo.getEndEffectiveDate());
						if( this.serviceRateTo.getRateStatus().equals(RateStateType.REGISTERED.getCode()) ){
							serviceRateToSeq.setRateStatus(this.serviceRateTo.getRateStatus()); 
							serviceRateToSeq.setDescriptionStatus(RateStateType.get(this.serviceRateTo.getRateStatus()).getValue());										
						}
						else if( this.serviceRateTo.getRateStatus().equals(RateStateType.UNLOCKED.getCode()) ){
							if( this.serviceRateTo.getInitialEffectiveDate().before(CommonsUtilities.currentDate()) ){
								serviceRateToSeq.setRateStatus(RateServiceStatus.ACTIVE.getCode());
								serviceRateToSeq.setDescriptionStatus(RateStateType.get(this.serviceRateTo.getRateStatus()).getValue());
								break;
							}
							else if( this.serviceRateTo.getInitialEffectiveDate().after(CommonsUtilities.currentDate()) ){
								serviceRateToSeq.setRateStatus(RateServiceStatus.REGISTER.getCode());
								serviceRateToSeq.setDescriptionStatus(RateStateType.get(this.serviceRateTo.getRateStatus()).getValue());
								break;
							}else // Is the same day 
								serviceRateToSeq.setRateStatus(RateServiceStatus.ACTIVE.getCode());
								serviceRateToSeq.setDescriptionStatus(RateStateType.get(this.serviceRateTo.getRateStatus()).getValue());
								break;
						}
						serviceRateToSeq.setRateStatus(this.serviceRateTo.getRateStatus()); 
						serviceRateToSeq.setDescriptionStatus(RateStateType.get(this.serviceRateTo. getRateStatus()).getValue());
						break;
					}
					
					/**
					 * Third, changes in Rate Staggered
					 */
					if(serviceRateTo.getRateType().equals(RateType.ESCALONADO_FIJO.getCode()) ||
							serviceRateTo.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode()) ||
							serviceRateTo.getRateType().equals(RateType.ESCALONADO_MIXTO.getCode())){
						
						if(serviceRateTo.isFlagIsModifyStaggeredFixed()){
							serviceRateToSeq.setFlagIsModifyStaggeredFixed(serviceRateTo.isFlagIsModifyStaggeredFixed());
							
						}
						
						/**
						 * 
						 */
						serviceRateToSeq.setAccumulate(serviceRateTo.getAccumulate());
					}
					
					
					
				}
			}
		}
		closeModalWindow();
	}


	/**
	 * Before update service rate.
	 */
	public void beforeUpdateServiceRate() {

		log.debug("beforeServiceRegistrationListener :");


		if (existChanges()){
			showMessageOnDialog(
					PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MODIFY),
					PropertiesUtilities.getMessage(
							PropertiesConstants.CONFIRM_SERVICE_RATE_MODIFY,
							this.billingServiceSelection.getServiceCode()));
			JSFUtilities.executeJavascriptFunction("cnfwRateModify.show()");  
		}else{
			showMessageOnDialog(
					PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage(
							PropertiesConstants.WARNING_EMPTY_SERVICE_MODIFY_RATE));
			JSFUtilities.executeJavascriptFunction("cnfwModifiedFormSaveRate.show()"); 
		}

	}


	/**
	 * Before back service rate to search.
	 */
	public void beforeBackServiceRateToSearch() {
		log.debug("beforeBackServiceRateToSearch :");


		if (existChanges()){
			showMessageOnDialog(
					PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MODIFY),
					PropertiesUtilities.getMessage(
							PropertiesConstants.WARNING_TYPE_SERVICE_RATE_BACK,
							this.billingServiceSelection.getServiceCode()));
			JSFUtilities.executeJavascriptFunction("cnfwRateBack.show()");  
		}else{

			JSFUtilities.executeJavascriptFunction("backToSearch();");

		}

	}


	/**
	 * Exist changes.
	 *
	 * @return the boolean
	 */
	public Boolean existChanges(){
		log.debug("existChanges :");

		Boolean flagExistChanges= Boolean.FALSE;

		List<ServiceRateTo> serviceRateToList = this.billingServiceTo.getListServiceRateTo();
		List<ServiceRateTo> serviceRateToListTemp = this.billingServiceTo.getListServiceRateToModify();

		/** Begin validation between service of  rates  **/

		if ( !(serviceRateToList.size()==serviceRateToListTemp.size()) ){
			log.debug("Exist Changes by Delete rates");
			flagExistChanges=true;
		}else {

			for (ServiceRateTo serviceRateToOrig : serviceRateToList) {

				for (ServiceRateTo serviceRateToTemp : serviceRateToListTemp) {

					if (serviceRateToOrig.getRateCode().equals(serviceRateToTemp.getRateCode())){

						/** first -- compare date end **/
						if (!serviceRateToOrig.getEndEffectiveDate().equals(serviceRateToTemp.getEndEffectiveDate())){
							log.debug(" compare date end");
							flagExistChanges=true;
						}

						/** second -- compare status  ***/
						if (!(serviceRateToOrig.getRateStatus().equals(serviceRateToTemp.getRateStatus())) )
						{
							log.debug(" compare status");
							flagExistChanges=true;
						}
						
						/**validate id Exist Changes in Rate Staggered**/
						if(serviceRateToOrig.getRateType().equals(RateType.ESCALONADO_FIJO.getCode()) ||
								 serviceRateToOrig.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode()) ||
								 serviceRateToOrig.getRateType().equals(RateType.ESCALONADO_MIXTO.getCode())){
							
							if(Validations.validateListIsNotNullAndNotEmpty(serviceRateToTemp.getListServiceRateScaleTo())){
								/**If Delete Staggered*/
								if (!(serviceRateToOrig.getListServiceRateScaleTo().size()==serviceRateToTemp.getListServiceRateScaleTo().size())){
									log.debug("If Delete Staggered");
									flagExistChanges=true;
								}
								else if(serviceRateToTemp.isFlagIsModifyStaggeredFixed()){
									log.debug("If modify staggered ");
									flagExistChanges=true;
								}
								/**If changes indicator to accumulate**/
								if(  !(serviceRateToOrig.getAccumulate().equals(serviceRateToTemp.getAccumulate()))  ){
									log.debug("If changes indicator to accumulate");
									flagExistChanges=true;
								}
							}
						}

						break;

					}
				}

				if (flagExistChanges){
					break;
				}

			}

		}
		return flagExistChanges;
	}


	/**
	 * Muestra el panel Dialog.
	 *
	 * @param event the event
	 * @throws ServiceException the service exception
	 */
	public void modifyRateService(ActionEvent event) throws ServiceException {
		if (event != null) {
			flagDialogModify=true;
			JSFUtilities.showComponent("formModalModifyRate: cnIdRateModify");
		}

		if (this.serviceRateTo.isFlagPercentage()){
			this.serviceRateTo.setPercentageRate(null);
			this.serviceRateTo.setFlagPercentage(Boolean.FALSE);	

		}else if (this.serviceRateTo.isFlagFixed()){
			this.serviceRateTo.setAmountRate(null);
			this.serviceRateTo.setFlagFixed(Boolean.FALSE);

		}
		else if (this.serviceRateTo.isFlagStaggeredFixed()){
			List<ServiceRateScaleTo> listServicerateScaleTo = new ArrayList<ServiceRateScaleTo>();	
			this.serviceRateTo.setSequenceNumber(null);
			this.serviceRateTo.setListServiceRateScaleTo(listServicerateScaleTo);
			this.getRateServiceScaleToDataModel().setWrappedData(listServicerateScaleTo);
			this.serviceRateTo.setFlagStaggeredFixed(Boolean.FALSE);
		}
		else if (this.serviceRateTo.isFlagMovementTypeFixed()){ 
			this.serviceRateTo.setFlagMovementTypeFixed(Boolean.FALSE);
		} 
	}

	

	/**
	 * Clean rate dates.
	 *
	 * @throws ServiceException the service exception
	 */
	public void cleanRateDates() throws ServiceException{ 
		serviceRateTo=new ServiceRateTo(); 

		this.serviceRateTo.setCurrencyRate(billingServiceTo.getCurrencyBilling());		
		JSFUtilities.setValidViewComponentAndChildrens("frmRateRegister:otpRatesID");
		JSFUtilities.setValidViewComponentAndChildrens("frmRateRegister:flsRateID");
		JSFUtilities.setValidViewComponentAndChildrens("frmRateRegister:flsRate");
		serviceRateTo.setRateType(GeneralConstants.NEGATIVE_ONE_INTEGER);
	}

	/**
	 * Clean on search.
	 */
	public void cleanOnSearch(){

		this.collectionInstitutionSelected=null;
		this.serviceTypeSelected=null;
		this.serviceStatusSelected=null;
		this.billingServiceDataModel=null; 
		this.serviceCode=null;
	}

	/**
	 * Rate remove.
	 *
	 * @param event the event
	 */
	public void rateRemove(ActionEvent event) {
		ServiceRateScaleTo serviceRateScaleTo = (ServiceRateScaleTo) event
				.getComponent().getAttributes().get("removeName");

		serviceRateTo.getListServiceRateScaleTo().remove(serviceRateScaleTo); 
		serviceRateTo.setFlagIsModifyStaggeredFixed(Boolean.TRUE);
	}

	/**
	 * Movement rate remove.
	 *
	 * @param event the event
	 */
	public void movementRateRemove(ActionEvent event) { 
		RateMovementTo rateMovementTo = (RateMovementTo) event	 .getComponent().getAttributes().get("removeName");
		serviceRateTo.getListRateMovementTo().remove(rateMovementTo); 
	}


	/**
	 * Return to rate modify.
	 */
	public void returnToRateModify(){

		List<ServiceRateTo>  serviceRateToListModify= this.billingServiceTo.getListServiceRateToModify();		
		Boolean flagModifyWarning=false;		

		for (ServiceRateTo serviceRateToSeq : serviceRateToListModify) {
			if (serviceRateToSeq.getRateCode().equals(this.serviceRateTo.getRateCode())){   /* first , find object**/
				if (!serviceRateToSeq.getEndEffectiveDate().equals(this.serviceRateTo.getEndEffectiveDate())){  /* second, compare dates  ***/
					flagModifyWarning=true;
					break;
				}
			}
		}


		if(flagModifyWarning){
			showMessageOnDialog(
					PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage(
							PropertiesConstants.WARNING_TYPE_SERVICE_RATE_BACK,
							this.billingServiceSelection.getServiceCode()));
			JSFUtilities.executeJavascriptFunction("cnfwConfirmBack.show()");  
		}
		else  
			closeModalWindow();
	}

	/**
	 * Removes the rate to.
	 *
	 * @param event the event
	 */
	public void removeRateTo(ActionEvent event) {  

		ServiceRateTo serviceRateTo = (ServiceRateTo) event .getComponent().getAttributes().get("removeName");
		List<ServiceRateTo> serviceRateToList= this.billingServiceTo.getListServiceRateTo();
		serviceRateToList.remove(serviceRateTo);
	}

	/**
	 * Load cmb rate status.
	 *
	 * @throws ServiceException the service exception
	 */
	protected void loadCmbRateStatus() throws ServiceException {
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(PropertiesConstants.RATE_STATUS);
		listRateStatus = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO); 
		ParameterTable unblock = new ParameterTable();
		unblock.setParameterTablePk(Integer.valueOf(1));
		unblock.setParameterName("DESBLOQUEAR");
		listRateStatus.add(5,unblock);
	}


	/**
	 * Before delete rate.
	 *
	 * @param event the event
	 */
	public void beforeDeleteRate(ActionEvent event){
		executeAction();
		showMessageOnDialog(
				PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_DELETE),
				PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_SERVICE_RATE_DELETE));
		JSFUtilities.executeJavascriptFunction("cnfwDeleteRate.show();");

		if (event != null) {
			ServiceRateTo serviceRateToTemp = (ServiceRateTo) event.getComponent()
					.getAttributes().get("removeName");
			this.serviceRateTo.setIdServiceRatePk(serviceRateToTemp.getIdServiceRatePk());  /**save on memory*/
			this.serviceRateTo.setRateCode(serviceRateToTemp.getRateCode());
			this.serviceRateTo.setFlagDeleteServiceRate(true);
		}
	}	


	/**
	 * Confirm delete service rate on list temp.
	 */
	public void confirmDeleteServiceRateOnListTemp(){

		List<ServiceRateTo> serviceRateListModify =BillingUtils.cloneServiceRateToList(billingServiceTo.getListServiceRateToModify());
		billingServiceTo.setListServiceRateToModify(serviceRateListModify);
//		if (billingServiceTo.getRateServiceToDataModel()!=null){
//			billingServiceTo.getRateServiceToDataModel().setWrappedData(serviceRateListModify);
//		}
		if (this.getRateServiceToDataModel()!=null){
			this.getRateServiceToDataModel().setWrappedData(serviceRateListModify);
		}

		for (ServiceRateTo serviceRateToSeq : serviceRateListModify) {
			if (this.serviceRateTo.getRateCode().equals(serviceRateToSeq.getRateCode()) && (this.serviceRateTo.isFlagDeleteServiceRate())){							
				serviceRateListModify.remove(serviceRateToSeq);
				break ;
			}
		}

	} 

	/**
	 * Checks if is disable status.
	 *
	 * @param rate the rate
	 * @param rateStatus the rate status
	 * @return true, if is disable status
	 */
	public boolean isDisableStatus(ServiceRateTo rate,ParameterTable rateStatus){


		List<ServiceRateTo> serviceRateToList = this.billingServiceTo.getListServiceRateTo();
		ServiceRateTo serviceRateToTemp= new ServiceRateTo();
		for (ServiceRateTo serviceRateToSeq : serviceRateToList) {
			if(serviceRateToSeq.getRateCode().equals(rate.getRateCode())){
				serviceRateToTemp=serviceRateToSeq;
				break;
			}
		}


		return (RateStateType.CANCELED.getCode().equals(serviceRateToTemp.getRateStatus()) 
				|| RateStateType.EXPIRED.getCode().equals(serviceRateToTemp.getRateStatus()) )
				||
				(  RateStateType.REGISTERED.getCode().equals(serviceRateToTemp.getRateStatus()) && 

						(RateStateType.CANCELED.getCode().equals(rateStatus.getParameterTablePk())
								|| RateStateType.EXPIRED.getCode().equals(rateStatus.getParameterTablePk())						                	
								|| RateStateType.ACTIVED.getCode().equals(rateStatus.getParameterTablePk()) 
								|| RateStateType.UNLOCKED.getCode().equals(rateStatus.getParameterTablePk())
								)
						)			                			 
						|| 
						( RateStateType.ACTIVED.getCode().equals(serviceRateToTemp.getRateStatus()) &&  

								(RateStateType.REGISTERED.getCode().equals(rateStatus.getParameterTablePk())
										|| RateStateType.EXPIRED.getCode().equals(rateStatus.getParameterTablePk())						                	
										|| RateStateType.UNLOCKED.getCode().equals(rateStatus.getParameterTablePk())
										)
								)
								|| 
								( RateStateType.LOCKED.getCode().equals( serviceRateToTemp.getRateStatus()) && 
										(RateStateType.CANCELED.getCode().equals(rateStatus.getParameterTablePk())						                	
												|| RateStateType.EXPIRED.getCode().equals(rateStatus.getParameterTablePk())
												|| RateStateType.REGISTERED.getCode().equals(rateStatus.getParameterTablePk())
												|| RateStateType.ACTIVED.getCode().equals(rateStatus.getParameterTablePk()) 
												) 
										) ;  
	}

	/**
	 * Disable register status.
	 *
	 * @param rate the rate
	 * @return true, if successful
	 */
	public boolean disableRegisterStatus(ServiceRateTo rate){	

		return (RateStateType.REGISTERED.getCode().equals(rate.getRateStatus()));		

	}

	/**
	 * Disable modify.
	 *
	 * @param rate the rate
	 * @return true, if successful
	 */
	public boolean disableModify(ServiceRateTo rate){	
		Boolean flagDisable=false; 


		List<ServiceRateTo> serviceRateToList = this.billingServiceTo.getListServiceRateTo();

		for (ServiceRateTo serviceRateToSeq : serviceRateToList) {
			if(serviceRateToSeq.getRateCode().equals(rate.getRateCode())){
				if (RateStateType.CANCELED.getCode().equals(serviceRateToSeq.getRateStatus()) || 
						RateStateType.EXPIRED.getCode().equals(serviceRateToSeq.getRateStatus())){
					flagDisable= true;
					break;
				}

			}
		}


		return flagDisable;
	}

	/**
	 * Initial effective date case.
	 *
	 * @return the date
	 */
	public Date initialEffectiveDateCase(){ 

		if(this.serviceRateTo.getInitialEffectiveDate().equals(this.serviceRateTo.getEndEffectiveDate())){
			return this.serviceRateTo.getInitialEffectiveDate();
		}
		else{				
			return this.serviceRateTo.getEndEffectiveDate();
		}  
	}

	/**
	 * Disable radio button.
	 *
	 * @param service the service
	 * @return true, if successful
	 */
	public boolean disableRadioButton(BillingServiceTo service){

		return (!(service.getStateService().equals(RateStateType.ACTIVED.getCode()) && 
				service.getStateService().equals(RateStateType.REGISTERED.getCode())));  

	}
 

	/**
	 *  Validations on Rates to Show OuputPanel.
	 */
	public void disableOutputPanelToRateType(){
		serviceRateTo.disableFlagRateServices();
		
		if(serviceRateTo.getRateType().equals(RateType.FIJO.getCode())) {			
			
			serviceRateTo.setFlagFixed(true);
			
		}else if(serviceRateTo.getRateType().equals(RateType.PORCENTUAL.getCode())) {
			
			serviceRateTo.setFlagPercentage(true);
			
		}else if(serviceRateTo.getRateType().equals(RateType.ESCALONADO_FIJO.getCode())) {
			
			serviceRateTo.setFlagStaggeredFixed(true);
			serviceRateTo.setFlagIsMovement(true);
			 
			
		}else if(serviceRateTo.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode())) {
			
			serviceRateTo.setFlagStaggeredPercentage(true);
			serviceRateTo.setFlagIsMovement(true);
			 
			
		}else if(serviceRateTo.getRateType().equals(RateType.TIPO_MOVIMIENTO_FIJO.getCode())) {
			
			serviceRateTo.setFlagMovementTypeFixed(true);
			
		}else if(serviceRateTo.getRateType().equals(RateType.TIPO_MOVIMIENTO_PORCENTUAL.getCode())) {
			
			serviceRateTo.setFlagMovementTypePercentage(true);
			
		}
		else if(serviceRateTo.getRateType().equals(RateType.ESCALONADO_MIXTO.getCode())) {
			
			serviceRateTo.setFlagStaggeredMix(true);
			serviceRateTo.setFlagIsMovement(true);
			indStaggeredFixedMix=Boolean.TRUE;
			indStaggeredPercentageMix=Boolean.FALSE;
			
		}
	}

	/**
	 * Setear los botones de privilegios.
	 */
	public void setTrueValidButtons(){

		PrivilegeComponent privilegeComponent = new PrivilegeComponent();

		if(userInfo.getUserAcctions().isModify()){
			privilegeComponent.setBtnModifyView(true);    
		}

		if(userInfo.getUserAcctions().isAdd()){
			privilegeComponent.setBtnAdd(true);
		} 

		userInfo.setPrivilegeComponent(privilegeComponent);

		log.info("userInfo.getUserAcctions().isModify() "+userInfo.getUserAcctions().isModify());
		log.info("userInfo.getUserAcctions().iAdd() "+userInfo.getUserAcctions().isAdd()); 
	}

	
	
	/**
	 *  VIEW RATE WHILE IS REGISTER *.
	 *
	 * @param serviceRateTo the service rate to
	 */
	@LoggerAuditWeb
	public void viewMemoryRate(ServiceRateTo serviceRateTo){
		log.debug("cargar tarifa :");
		this.serviceRateConsultTo = serviceRateTo; 		

		if (this.serviceRateConsultTo.getRateType().intValue() == RateType.FIJO.getCode()) {
			
			if( Validations.validateIsNotNullAndNotEmpty(serviceRateTo.getRateAmount()) ){			 
				this.serviceRateConsultTo.setAmountRate(serviceRateTo.getRateAmount());				
				JSFUtilities.executeJavascriptFunction("cnfwFlagConsultMemory.show()");
			}else if( Validations.validateIsNotNullAndNotEmpty(serviceRateTo.getAmountRate()) ){
				this.serviceRateConsultTo.setAmountRate(serviceRateTo.getAmountRate());
				JSFUtilities.executeJavascriptFunction("cnfwFlagConsultMemory.show()");
			}  
			this.serviceRateConsultTo.setFlagFixed(Boolean.TRUE);
			
		} else if(this.serviceRateConsultTo.getRateType().intValue() == RateType.PORCENTUAL.getCode()) {	
			
			if( Validations.validateIsNotNullAndNotEmpty(serviceRateTo.getPercentageRate()) ){
				this.serviceRateConsultTo.setRatePercent(serviceRateTo.getPercentageRate());				
				JSFUtilities.executeJavascriptFunction("cnfwFlagConsultMemory.show()");
			}else if( Validations.validateIsNotNullAndNotEmpty(serviceRateTo.getRatePercent()) ){
				this.serviceRateConsultTo.setRatePercent(serviceRateTo.getRatePercent());				
				JSFUtilities.executeJavascriptFunction("cnfwFlagConsultMemory.show()");
			} 
			this.serviceRateConsultTo.setFlagPercentage(Boolean.TRUE);		

			/**		 MOVEMENTS 		  **/	
		}else if(this.serviceRateConsultTo.getRateType().intValue() == RateType.TIPO_MOVIMIENTO_FIJO.getCode()) {
			JSFUtilities.executeJavascriptFunction("cnfwFlagConsultMemory.show()");			
			this.serviceRateConsultTo.setFlagMovementTypeFixed(Boolean.TRUE);			
			this.getDataModelRateMovementToListConsult().setWrappedData(this.serviceRateConsultTo.getListRateMovementTo());
		} else if(this.serviceRateConsultTo.getRateType().intValue() == RateType.TIPO_MOVIMIENTO_PORCENTUAL.getCode()) {
			JSFUtilities.executeJavascriptFunction("cnfwFlagConsultMemory.show()");
			this.serviceRateConsultTo.setFlagMovementTypePercentage(Boolean.TRUE);			
			this.getDataModelRateMovementToListConsult().setWrappedData(this.serviceRateConsultTo.getListRateMovementTo());

			
			/**		 STAGGERED 		  **/		
		} else if(this.serviceRateConsultTo.getRateType().intValue() == RateType.ESCALONADO_FIJO.getCode()) {
			JSFUtilities.executeJavascriptFunction("cnfwFlagConsultMemory.show()");
			this.serviceRateConsultTo.setFlagStaggeredFixed(Boolean.TRUE);
			this.getRateServiceScaleToDataModelConsult().setWrappedData(this.serviceRateConsultTo.getListServiceRateScaleTo());
			this.serviceRateConsultTo.setUnit(serviceRateTo.getListServiceRateScaleTo().get(0).getScaleType());
			
			if(serviceRateConsultTo.getIndScaleMovement().equals(GeneralConstants.ONE_VALUE_INTEGER)){
				this.serviceRateConsultTo.setFlagStaggeredMovement(Boolean.TRUE);
				this.getDataModelRateMovementToListConsult().setWrappedData(this.serviceRateConsultTo.getListRateMovementTo());
			}
			else{
				this.serviceRateConsultTo.setFlagStaggeredMovement(Boolean.FALSE);
			}
			

		} else if(this.serviceRateConsultTo.getRateType().intValue() == RateType.ESCALONADO_PORCENTUAL.getCode()) {
			
			JSFUtilities.executeJavascriptFunction("cnfwFlagConsultMemory.show()");
			this.serviceRateConsultTo.setFlagStaggeredPercentage(Boolean.TRUE);
			this.getRateServiceScaleToDataModelConsult().setWrappedData(this.serviceRateConsultTo.getListServiceRateScaleTo());
			this.serviceRateConsultTo.setUnit(serviceRateTo.getListServiceRateScaleTo().get(0).getScaleType());
			
			if(serviceRateConsultTo.getIndScaleMovement().equals(GeneralConstants.ONE_VALUE_INTEGER)){
				this.serviceRateConsultTo.setFlagStaggeredMovement(Boolean.TRUE);
				this.getDataModelRateMovementToListConsult().setWrappedData(this.serviceRateConsultTo.getListRateMovementTo());
			}else{
				this.serviceRateConsultTo.setFlagStaggeredMovement(Boolean.FALSE);
			}
		} else if(this.serviceRateConsultTo.getRateType().intValue() == RateType.ESCALONADO_MIXTO.getCode()) {
			JSFUtilities.executeJavascriptFunction("cnfwFlagConsultMemory.show()");
			this.serviceRateConsultTo.setFlagStaggeredMix(Boolean.TRUE);
			this.getRateServiceScaleToDataModelConsult().setWrappedData(this.serviceRateConsultTo.getListServiceRateScaleTo());
			this.serviceRateConsultTo.setUnit(serviceRateTo.getListServiceRateScaleTo().get(0).getScaleType());
			
			if(serviceRateConsultTo.getIndScaleMovement().equals(GeneralConstants.ONE_VALUE_INTEGER)){
				this.serviceRateConsultTo.setFlagStaggeredMovement(Boolean.TRUE);
				this.getDataModelRateMovementToListConsult().setWrappedData(this.serviceRateConsultTo.getListRateMovementTo());
			}
			else{
				this.serviceRateConsultTo.setFlagStaggeredMovement(Boolean.FALSE);
			}
			
			
		}
	}

	/**
	 * Before back.
	 */
	public void beforeBack(){

		Boolean flagExistChanges= Boolean.FALSE; 
		List<ServiceRateTo> serviceRateToList = this.billingServiceTo.getListServiceRateTo();
		List<ServiceRateTo> serviceRateToListTemp = this.billingServiceTo.getListServiceRateToModify();

		/** Begin validation between service of  rates  **/ 
		if ( !(serviceRateToList.size()==serviceRateToListTemp.size()) ){
			flagExistChanges=true;
		}else {
			for (ServiceRateTo serviceRateToOrig : serviceRateToList) { 
				for (ServiceRateTo serviceRateToTemp : serviceRateToListTemp) { 
					if (serviceRateToOrig.getRateCode().equals(serviceRateToTemp.getRateCode())){ 
						/** first -- compare date end **/
						if (!serviceRateToOrig.getEndEffectiveDate().equals(serviceRateToTemp.getEndEffectiveDate())){
							flagExistChanges=true;  } 
						/** second -- compare status  ***/
						if (!(serviceRateToOrig.getRateStatus().equals(serviceRateToTemp.getRateStatus())) )
						{ flagExistChanges=true;  } 
						break; 
					}
				}

				if (flagExistChanges){ break; } 
			} 
		}

		if (flagExistChanges){
			executeAction();
			showMessageOnDialog( PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MODIFY),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_TYPE_SERVICE_RATE_BACK,""));
			JSFUtilities.executeJavascriptFunction("cnfwRateModify.show()");
		}else{
			return ;
		} 
	 }

	/**
	 * Expired BillingService and ServiceRate.
	 *
	 * @param event the event
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void expireServiceAndRate(ActionEvent event) throws ServiceException { 
		try{
			BillingServiceTo billingServiceTo = new BillingServiceTo();
			List<BillingService> billingServiceList;
			billingServiceTo.setStateService(PropertiesConstants.SERVICE_STATUS_ACTIVE_PK);
			/**Obtengo el servicio*/
			billingServiceList=this.billingServiceMgmtServiceFacade.findAllBillingService(billingServiceTo);
			List<ServiceRate> serviceRateList = billingRateMgmtServiceFacade.getAllServiceRateByBillingService(null);
			for(BillingService service:billingServiceList)
			{
				if(service.getServiceState().equals(PropertiesConstants.SERVICE_STATUS_ACTIVE_PK)&&(service.getInitialEffectiveDate().compareTo(getYesterdaySystemDate())==0)){
					service.setServiceState(PropertiesConstants.SERVICE_STATUS_EXPIRED_PK);
					billingServiceMgmtServiceFacade.updateBillingService(service);
					
				}
			}
			
			for(ServiceRate serviceRate:serviceRateList){
				if(serviceRate.getRateState().equals(RateStateType.ACTIVED.getCode())&&(serviceRate.getInitialEffectiveDate().compareTo(getYesterdaySystemDate())==0)){
					
					serviceRate.setRateState(RateStateType.EXPIRED.getCode());
					billingRateMgmtServiceFacade.updateServiceRate(serviceRate);
				}
			}
			 
		}
		catch(Exception e){
				e.printStackTrace();
		}
	}
	 
	
	/**
	 * Clean result billing service data model.
	 */
	public void cleanResultBillingServiceDataModel(){
		
		if(Validations.validateIsNotNullAndNotEmpty(billingServiceDataModel) && billingServiceDataModel.getRowCount() > 0) {
			
			billingServiceDataModel = null ;			
		}	
		
	}
	
	/**
	 * *
	 * For Rate staggered with Movement.
	 */
	public void showRateMovement(){
		
		if(serviceRateTo.getIndScaleMovement().equals(GeneralConstants.ONE_VALUE_INTEGER)){
			serviceRateTo.setFlagStaggeredMovement(Boolean.TRUE);
		}
		else{
			serviceRateTo.setFlagStaggeredMovement(Boolean.FALSE);
		}
		
	}
	
	/**
	 * *
	 * 0: Fixed
	 * 1: Percentage
	 * 
	 * Change option Staging Rate.
	 */
	public void stagingRate(){
		
		/**
		 * Percentage
		 */
		if(serviceRateTo.getIndStaging().equals(GeneralConstants.ONE_VALUE_INTEGER)){
			indStaggeredFixedMix=Boolean.FALSE;
			indStaggeredPercentageMix=Boolean.TRUE;
		}else{
			/***
			 * Fixed
			 */
			indStaggeredFixedMix=Boolean.TRUE;
			indStaggeredPercentageMix=Boolean.FALSE;
		}
		serviceRateTo.setPercentageRate(null);
		serviceRateTo.setAmountRate(null);
		
	}


	/**
	 * Gets the rate service to data model.
	 *
	 * @return the rateServiceToDataModel
	 */
	public RateServiceToDataModel getRateServiceToDataModel() {
		return rateServiceToDataModel;
	}


	/**
	 * Sets the rate service to data model.
	 *
	 * @param rateServiceToDataModel the rateServiceToDataModel to set
	 */
	public void setRateServiceToDataModel(
			RateServiceToDataModel rateServiceToDataModel) {
		this.rateServiceToDataModel = rateServiceToDataModel;
	}

}
