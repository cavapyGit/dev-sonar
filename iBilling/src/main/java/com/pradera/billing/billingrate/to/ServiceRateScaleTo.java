package com.pradera.billing.billingrate.to;

import java.io.Serializable;
import java.math.BigDecimal;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ServiceRateScaleTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class ServiceRateScaleTo implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id service rate scale pk. */
	private Long 		idServiceRateScalePk;
	
	/** The maximum collection amount. */
	private BigDecimal 	maximumCollectionAmount;
	
	/** The maximum range. */
	private BigDecimal 	maximumRange;
	
	/** The minimum collection amount. */
	private BigDecimal 	minimumCollectionAmount;
	
	/** The minimum range. */
	private BigDecimal 	minimumRange;
	
	/** The scale percent. */
	private BigDecimal 	scalePercent;
	
	/** The scale amount. */
	private BigDecimal 	scaleAmount;
	
	/** The disable maximum range. */
	private Boolean 	disableMaximumRange = Boolean.TRUE;
	
	/** The disable minimum range. */
	private Boolean 	disableMinimumRange = Boolean.TRUE;
	
	/** The disable collection. */
	private Boolean 	disableCollection= Boolean.TRUE;
    
	/** The sequence code. */
	private Integer 	sequenceCode;
	
	/** The scale type. */
	private Integer 	scaleType;
	
	/** The ind accumulative scale. */
	private Integer 	indAccumulativeScale;
	
	/** The ind staging. */
	private Integer 	indStaging;	
	
	/** The ind other currency. */
	private Integer 	indOtherCurrency;
	
	/** The other currency. */
	private Integer 	otherCurrency;



	/**
	 * Gets the scale amount.
	 *
	 * @return the scaleAmount
	 */
	public BigDecimal getScaleAmount() {
		return scaleAmount;
	}


	/**
	 * Sets the scale amount.
	 *
	 * @param scaleAmount the scaleAmount to set
	 */
	public void setScaleAmount(BigDecimal scaleAmount) {
		this.scaleAmount = scaleAmount;
	}


	/**
	 * Gets the disable maximum range.
	 *
	 * @return the disableMaximumRange
	 */
	public Boolean getDisableMaximumRange() {
		return disableMaximumRange;
	}


	/**
	 * Sets the disable maximum range.
	 *
	 * @param disableMaximumRange the disableMaximumRange to set
	 */
	public void setDisableMaximumRange(Boolean disableMaximumRange) {
		this.disableMaximumRange = disableMaximumRange;
	}


	/**
	 * Gets the disable minimum range.
	 *
	 * @return the disableMinimumRange
	 */
	public Boolean getDisableMinimumRange() {
		return disableMinimumRange;
	}


	/**
	 * Sets the disable minimum range.
	 *
	 * @param disableMinimumRange the disableMinimumRange to set
	 */
	public void setDisableMinimumRange(Boolean disableMinimumRange) {
		this.disableMinimumRange = disableMinimumRange;
	}


	/**
	 * Gets the disable collection.
	 *
	 * @return the disableCollection
	 */
	public Boolean getDisableCollection() {
		return disableCollection;
	}


	/**
	 * Sets the disable collection.
	 *
	 * @param disableCollection the disableCollection to set
	 */
	public void setDisableCollection(Boolean disableCollection) {
		this.disableCollection = disableCollection;
	}


	/**
	 * Gets the id service rate scale pk.
	 *
	 * @return the idServiceRateScalePk
	 */
	public Long getIdServiceRateScalePk() {
		return idServiceRateScalePk;
	}


	/**
	 * Gets the sequence code.
	 *
	 * @return the sequenceCode
	 */
	public Integer getSequenceCode() {
		return sequenceCode;
	}


	/**
	 * Gets the maximum collection amount.
	 *
	 * @return the maximumCollectionAmount
	 */
	public BigDecimal getMaximumCollectionAmount() {
		return maximumCollectionAmount;
	}


	/**
	 * Sets the maximum collection amount.
	 *
	 * @param maximumCollectionAmount the maximumCollectionAmount to set
	 */
	public void setMaximumCollectionAmount(BigDecimal maximumCollectionAmount) {
		this.maximumCollectionAmount = maximumCollectionAmount;
	}


	/**
	 * Gets the maximum range.
	 *
	 * @return the maximumRange
	 */
	public BigDecimal getMaximumRange() {
		return maximumRange;
	}


	/**
	 * Sets the maximum range.
	 *
	 * @param maximumRange the maximumRange to set
	 */
	public void setMaximumRange(BigDecimal maximumRange) {
		this.maximumRange = maximumRange;
	}


	/**
	 * Gets the minimum collection amount.
	 *
	 * @return the minimumCollectionAmount
	 */
	public BigDecimal getMinimumCollectionAmount() {
		return minimumCollectionAmount;
	}


	/**
	 * Sets the minimum collection amount.
	 *
	 * @param minimumCollectionAmount the minimumCollectionAmount to set
	 */
	public void setMinimumCollectionAmount(BigDecimal minimumCollectionAmount) {
		this.minimumCollectionAmount = minimumCollectionAmount;
	}


	/**
	 * Gets the minimum range.
	 *
	 * @return the minimumRange
	 */
	public BigDecimal getMinimumRange() {
		return minimumRange;
	}


	/**
	 * Sets the minimum range.
	 *
	 * @param minimumRange the minimumRange to set
	 */
	public void setMinimumRange(BigDecimal minimumRange) {
		this.minimumRange = minimumRange;
	}


	/**
	 * Gets the scale percent.
	 *
	 * @return the scalePercent
	 */
	public BigDecimal getScalePercent() {
		return scalePercent;
	}


	/**
	 * Sets the scale percent.
	 *
	 * @param scalePercent the scalePercent to set
	 */
	public void setScalePercent(BigDecimal scalePercent) {
		this.scalePercent = scalePercent;
	}


	/**
	 * Gets the scale type.
	 *
	 * @return the scaleType
	 */
	public Integer getScaleType() {
		return scaleType;
	}


	/**
	 * Sets the scale type.
	 *
	 * @param scaleType the scaleType to set
	 */
	public void setScaleType(Integer scaleType) {
		this.scaleType = scaleType;
	}


	/**
	 * Sets the id service rate scale pk.
	 *
	 * @param idServiceRateScalePk the idServiceRateScalePk to set
	 */
	public void setIdServiceRateScalePk(Long idServiceRateScalePk) {
		this.idServiceRateScalePk = idServiceRateScalePk;
	}


	/**
	 * Sets the sequence code.
	 *
	 * @param sequenceCode the sequenceCode to set
	 */
	public void setSequenceCode(Integer sequenceCode) {
		this.sequenceCode = sequenceCode;
	}


	/**
	 * Enable range.
	 */
	public void enableRange(){
		disableMaximumRange = Boolean.FALSE;
		disableCollection=Boolean.FALSE;
		disableMinimumRange = Boolean.FALSE;
	}
	
	/**
	 * Disable range.
	 */
	public void disableRange(){
		disableMaximumRange = Boolean.TRUE;
		disableMinimumRange = Boolean.TRUE;
	}


	/**
	 * Gets the ind accumulative scale.
	 *
	 * @return the indAccumulativeScale
	 */
	public Integer getIndAccumulativeScale() {
		return indAccumulativeScale;
	}


	/**
	 * Sets the ind accumulative scale.
	 *
	 * @param indAccumulativeScale the indAccumulativeScale to set
	 */
	public void setIndAccumulativeScale(Integer indAccumulativeScale) {
		this.indAccumulativeScale = indAccumulativeScale;
	}


	/**
	 * Gets the ind staging.
	 *
	 * @return the indStaging
	 */
	public Integer getIndStaging() {
		return indStaging;
	}


	/**
	 * Sets the ind staging.
	 *
	 * @param indStaging the indStaging to set
	 */
	public void setIndStaging(Integer indStaging) {
		this.indStaging = indStaging;
	}


	/**
	 * Gets the ind other currency.
	 *
	 * @return the indOtherCurrency
	 */
	public Integer getIndOtherCurrency() {
		return indOtherCurrency;
	}


	/**
	 * Sets the ind other currency.
	 *
	 * @param indOtherCurrency the indOtherCurrency to set
	 */
	public void setIndOtherCurrency(Integer indOtherCurrency) {
		this.indOtherCurrency = indOtherCurrency;
	}


	/**
	 * Gets the other currency.
	 *
	 * @return the otherCurrency
	 */
	public Integer getOtherCurrency() {
		return otherCurrency;
	}


	/**
	 * Sets the other currency.
	 *
	 * @param otherCurrency the otherCurrency to set
	 */
	public void setOtherCurrency(Integer otherCurrency) {
		this.otherCurrency = otherCurrency;
	}


	 

	
}
