package com.pradera.billing.billingrate.facade;




import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.billing.billingrate.service.BillingRateMgmtServiceBean;
import com.pradera.billing.billingrate.to.ServiceRateTo;
import com.pradera.billing.billingservice.facade.BillingServiceMgmtServiceFacade;
import com.pradera.billing.billingservice.service.BillingServiceMgmtServiceBean;
import com.pradera.billing.billingservice.to.BillingServiceTo;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.billing.BillingEconomicActivity;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.RateMovement;
import com.pradera.model.billing.ServiceRate;
import com.pradera.model.billing.ServiceRateScale;
import com.pradera.model.billing.type.RateType;




// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingRateMgmtServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class BillingRateMgmtServiceFacade {

	/** The billing rate mgmt service bean. */
	@EJB
	private BillingRateMgmtServiceBean 		billingRateMgmtServiceBean;
	
	/** The billing service mgmt service bean. */
	@EJB 
	private BillingServiceMgmtServiceBean 	billingServiceMgmtServiceBean;
	
	/** The billing service mgmt service facade. */
	@EJB 
	private BillingServiceMgmtServiceFacade billingServiceMgmtServiceFacade;
	
	/** The transaction registry. */
	@Resource
	TransactionSynchronizationRegistry 		transactionRegistry;
	
	/**
	 * Save service rate.
	 *
	 * @param serviceRate the service rate
	 * @return the service rate
	 * @throws ServiceException the service exception
	 */
	public ServiceRate saveServiceRate(ServiceRate serviceRate)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());	  
		return billingRateMgmtServiceBean.saveRateService(serviceRate);
	}
	
	/**
	 * Save all rate service.
	 *
	 * @param serviceRateList the service rate list
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	public List<ServiceRate> saveAllRateService(List<ServiceRate> serviceRateList) throws ServiceException{		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());	    
	    billingRateMgmtServiceBean.saveAllRateService(serviceRateList);
		
		return serviceRateList;
	}
	
	/**
	 * Gets the services rate by billing service.
	 *
	 * @param billingServiceId the billing service id
	 * @return the services rate by billing service
	 * @throws ServiceException the service exception
	 */
	public List<ServiceRate> getServicesRateByBillingService(Long billingServiceId) throws ServiceException{
		
		List<ServiceRate> list=billingRateMgmtServiceBean.getServicesRateByBillingService(billingServiceId);		
		
		for (ServiceRate serviceRate : list) {
			if (Validations.validateListIsNotNullAndNotEmpty(serviceRate.getRateMovements())){
				 for (int i=0; i<serviceRate.getRateMovements().size();i++){
					 serviceRate.getRateMovements().get(i).getMovementType().getIdMovementTypePk();
				 }
								
			 }
			
			if (Validations.validateListIsNotNullAndNotEmpty(serviceRate.getServiceRateScales())){
				serviceRate.getServiceRateScales().size();							
			 }
	    }
		
		return list;
	}
	
	/**
	 * Gets the services rate by all billing service.
	 *
	 * @param billingServiceId the billing service id
	 * @return the services rate by all billing service
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<ServiceRate> getServicesRateByAllBillingService(Long billingServiceId) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
	
		List<ServiceRate> list=billingRateMgmtServiceBean.getServicesRateByAllBillingService(billingServiceId);			
		
		for (ServiceRate serviceRate : list) {
			if (Validations.validateListIsNotNullAndNotEmpty(serviceRate.getRateMovements())){
				 for (int i=0; i<serviceRate.getRateMovements().size();i++){
					 serviceRate.getRateMovements().get(i).getMovementType().getIdMovementTypePk();
				 }								
			 }
			
			if (Validations.validateListIsNotNullAndNotEmpty(serviceRate.getServiceRateScales())){
				serviceRate.getServiceRateScales().size();							
			 }
	    }
		
		return list;
	}
	
	
	
	/**
	 * Gets the services rate by all billing service to scale.
	 *
	 * @param billingServiceId the billing service id
	 * @return the services rate by all billing service to scale
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<ServiceRate> getServicesRateByAllBillingServiceToScale(Long billingServiceId) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		List<ServiceRate> list=billingRateMgmtServiceBean.getServicesRateByAllBillingServiceToScale(billingServiceId);		
		
		
		for (ServiceRate serviceRate : list) {
			if (Validations.validateListIsNotNullAndNotEmpty(serviceRate.getRateMovements())){
				 for (int i=0; i<serviceRate.getRateMovements().size();i++){
					 serviceRate.getRateMovements().get(i).getMovementType().getIdMovementTypePk();
				 }								
			 }
			
			if (Validations.validateListIsNotNullAndNotEmpty(serviceRate.getServiceRateScales())){
				serviceRate.getServiceRateScales().size();							
			 }
	    }
		
		return list;
	}
	
	
	
	
	/**
	 * Find rate movement by service rate.
	 *
	 * @param serviceRatePk the service rate pk
	 * @return the list
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<RateMovement> findRateMovementByServiceRate(Long serviceRatePk) {
		return billingRateMgmtServiceBean.findRateMovementByServiceRate(serviceRatePk);
	}
	
	/**
	 * Find service rate scale by service rate.
	 *
	 * @param serviceRatePk the service rate pk
	 * @return the list
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<ServiceRateScale> findServiceRateScaleByServiceRate(Long serviceRatePk) {
		return billingRateMgmtServiceBean.findServiceRateScaleByServiceRate(serviceRatePk);
	}
	
	
	/**
	 * *.
	 *
	 * @param billingServiceTo the billing service to
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_MODIFICATION)
	public void modifyServiceRate(BillingServiceTo billingServiceTo) throws ServiceException{
	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());	
		/** TODO update list of service rate on memory */
		List<ServiceRateTo> serviceRateToList  = billingServiceTo.getListServiceRateTo();
		List<ServiceRateTo> serviceRateToListTemp  = billingServiceTo.getListServiceRateToModify();
		List<ServiceRate> serviceRateForDelete = new ArrayList<ServiceRate>();
		List<ServiceRate> serviceRateForModify = new ArrayList<ServiceRate>();
		/**
		 * Servicio sin tarifas, le agrego tarifas nuevas
		 */
		if(Validations.validateListIsNullOrEmpty(serviceRateToList)&&
				Validations.validateListIsNotNullAndNotEmpty(serviceRateToListTemp)){
			
			List<ServiceRate> listServiceRate=billingServiceMgmtServiceFacade.prepareServiceRate(billingServiceTo.getIdBillingServicePk(), serviceRateToListTemp);
			saveAllRateService(listServiceRate);
			
		}
			
		BillingService billingService = billingServiceMgmtServiceBean.getBillingServiceWithServiceRate(billingServiceTo);
		List<ServiceRate> listServiceRate=billingService.getServiceRates();
		
		/* Tarifas modificas y eliminadas*/
		for (ServiceRate serviceRateOrig : listServiceRate) {
			Optional<ServiceRateTo> stillExistingServiceRate = serviceRateToListTemp.stream()
					.filter(sr -> sr.getIdServiceRatePk().equals(serviceRateOrig.getIdServiceRatePk()))
					.findFirst();
			
			if(stillExistingServiceRate.isPresent()) {
				serviceRateOrig.setEndEffectiveDate(stillExistingServiceRate.get().getEndEffectiveDate());
				serviceRateOrig.setRateState(stillExistingServiceRate.get().getRateStatus());
				
				if(stillExistingServiceRate.get().isFlagIsModifyRateFixed()) {
					serviceRateOrig.setRateAmount(stillExistingServiceRate.get().getAmountRate());
				}
				serviceRateForModify.add(serviceRateOrig);
			} else {
				serviceRateForDelete.add(serviceRateOrig);
			}
		}
		
		for(ServiceRate toDelete : serviceRateForDelete) {
			try {
				removeServiceRateAll(toDelete);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}	
		
		saveAllRateService(serviceRateForModify);
			
			/*
			BillingService billingService = billingServiceMgmtServiceBean.getBillingServiceWithServiceRate(billingServiceTo);
			List<ServiceRate> listServiceRate=billingService.getServiceRates();
			if (Validations.validateListIsNotNullAndNotEmpty(listServiceRate)){
				
				/**
				 * 
				 * Si No esta en BD pero esta en Temp, entonces agregar
				 * Agregar las nuevas tarifas
				 * 
				 * 
				if(billingServiceTo.getListServiceRateToModify().size()>listServiceRate.size()){
					List<ServiceRate> listServiceRateTemp=null;
					BillingServiceTo billingNew=new BillingServiceTo();
					billingNew.setIdBillingServicePk(billingService.getIdBillingServicePk());
					List<ServiceRateTo> listRate=new ArrayList<ServiceRateTo>();
					
					List<ServiceRate> listServiceRateNew=null;
					//agregar nuevos
					for (ServiceRateTo serviceRateTo : billingServiceTo.getListServiceRateToModify()) {
						
						if(Validations.validateIsNullOrNotPositive(serviceRateTo.getIdServiceRatePk())){
							listRate.add(serviceRateTo);
						}
					}
					billingNew.setListServiceRateTo(listRate);
					listServiceRateNew=billingServiceMgmtServiceFacade.prepareRateServicesForSave(billingNew);
					listServiceRateTemp= saveAllRateService(listServiceRateNew);
					
					listServiceRate.addAll(listServiceRateTemp);
				}
				
				/***
				 * Modificar las tarifas existentes o las eliminadas
				 
				for (Iterator<ServiceRate> iterator = listServiceRate.iterator(); iterator.hasNext();) {
					ServiceRate serviceRate = (ServiceRate) iterator.next();
					
						Boolean exist= Boolean.FALSE;
						for (ServiceRateTo serviceRateTo : billingServiceTo.getListServiceRateToModify()) {
							
							/**Si es un nuevo, agregar/
							if(Validations.validateIsNull(serviceRateTo.getIdServiceRatePk())){
								exist = Boolean.TRUE;
							} else if (serviceRate.getIdServiceRatePk().equals(serviceRateTo.getIdServiceRatePk())){
								/**Si esta en BD pero no esta en temp, entonces remover/
								if (serviceRateTo.isFlagDeleteServiceRate()){
									try {
										removeServiceRateAll(serviceRate);
									} catch (Exception e) {
										e.printStackTrace();
									}									
								} else {								
									serviceRate.setEndEffectiveDate(serviceRateTo.getEndEffectiveDate());
							    	serviceRate.setRateState(serviceRateTo.getRateStatus());
							    	
							    	if(serviceRateTo.getRateType().equals(RateType.ESCALONADO_FIJO.getCode()) ||
							    			serviceRateTo.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode())||
							    			serviceRateTo.getRateType().equals(RateType.ESCALONADO_MIXTO.getCode())){
							    		
							    		billingRateMgmtServiceBean.removeServiceRateScaleAll(serviceRate);
							    		
							    		serviceRateTo.addFieldRateToRateScale(serviceRateTo);
							    		serviceRate.setServiceRateScales(serviceRateTo.prepareServiceRateScale());
							    		
							    	}
							    	
								}
								exist=true;
						    	break;
							}
													
						}
						
						/**If rate not exist then Delete  
						if (!exist){
							try {
								removeServiceRateAll(serviceRate);
								iterator.remove();
							} catch (Exception e) {
								e.printStackTrace();
							}
							
						}
					}
					
					saveAllRateService(listServiceRate);
			}
			*/
		
	}
	
	
	
	/**
	 * Removes the service rate all.
	 *
	 * @param serviceRate the service rate
	 * @throws ServiceException the service exception
	 */
	public void removeServiceRateAll(ServiceRate serviceRate) throws ServiceException{
		
		if(serviceRate.getRateType().equals(RateType.TIPO_MOVIMIENTO_FIJO.getCode()) || 
				serviceRate.getRateType().equals(RateType.TIPO_MOVIMIENTO_PORCENTUAL.getCode())){
			billingRateMgmtServiceBean.removeRateMovementAll(serviceRate);
		}else if(serviceRate.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode()) || 
				serviceRate.getRateType().equals(RateType.ESCALONADO_FIJO.getCode()) ||
				serviceRate.getRateType().equals(RateType.ESCALONADO_MIXTO.getCode())){
			billingRateMgmtServiceBean.removeServiceRateScaleAll(serviceRate);
		}
		
		billingRateMgmtServiceBean.removeRateExclusionAll(serviceRate);
		billingRateMgmtServiceBean.removeRateInclusionAll(serviceRate);
		billingRateMgmtServiceBean.removeServiceRate(serviceRate);
	}
	
	
	
	/**
	 * Update service rate.
	 *
	 * @param serviceRate the service rate
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)	
	public void updateServiceRate(ServiceRate serviceRate)throws ServiceException{					
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
	    billingRateMgmtServiceBean.update(serviceRate);
	}
	
	/**
	 * Gets the all service rate by billing service.
	 *
	 * @param billingServiceId the billing service id
	 * @return the all service rate by billing service
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<ServiceRate> getAllServiceRateByBillingService(Long billingServiceId) throws ServiceException{				
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
	    return billingRateMgmtServiceBean.getAllServiceRateByBillingService(billingServiceId);
	}
	 
	
	/**
	 * Delete billing service.
	 *
	 * @param billingServiceId the billing service id
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public void deleteBillingService(Long billingServiceId) throws ServiceException{				
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeDelete());
	    
	    billingRateMgmtServiceBean.getAllServiceRateByBillingService(billingServiceId);
	}

	
	/**
	 * Metodo que actualiza en la base de datos la tabla BillingEconomicActivity, verificando los nuevos y eliminados
	 * @param billingService
	 * @param listBillingEconomicActivity
	 */
	public void modifyEconomicActivity(BillingService billingService,
			List<BillingEconomicActivity> listBillingEconomicActivity,
			List<BillingEconomicActivity> listBillingEconomicActivityBD) {
		/**Validar los nuevos y eliminados*/
		List<Integer> listNuevo=new ArrayList<>();
		List<Long> listEliminados=new ArrayList<>();

		//Borrar las actividades
		for (BillingEconomicActivity billingEconomicActivity : listBillingEconomicActivityBD) {
			billingServiceMgmtServiceBean.delete(BillingEconomicActivity.class, billingEconomicActivity.getIdBillingEconomicActivityPk());
		}
		for (BillingEconomicActivity billingEconomicActivity : listBillingEconomicActivity) {
			billingServiceMgmtServiceBean.merge(billingEconomicActivity);
		}
		
		
	}
	public static Comparator<BillingEconomicActivity> COMPARE_BY_ORDER =new Comparator<BillingEconomicActivity>(){
		public int compare(BillingEconomicActivity o1, BillingEconomicActivity o2) {
			if(o1.getEconomicActivity()>o2.getEconomicActivity()) {
				return -1;
			}
			return 1;
		}
	};
	 
}
	 
