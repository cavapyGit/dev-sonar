package com.pradera.billing.webservices;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.apache.log4j.Logger;

import com.pradera.billing.billing.to.BillingProcessedFilter;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.report.remote.ReportRegisterTO;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.type.ModuleWarType;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;

@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ServiceAPIClients {

	@Inject
	ClientRestService clientRestService;
	
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
    
	Logger log = Logger.getLogger(ServiceAPIClients.class);
	
	public void generateBillingReport(BillingProcessedFilter billingProcessedFilter, Long reportId) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
	    
		ReportRegisterTO input = new ReportRegisterTO();
		input.setRelatedId(billingProcessedFilter.getProcessBillingCalculationPk());
		input.setReportId(reportId);
		input.setLoggerUser(loggerUser);
		
		this.clientRestService.callLocalWebService(ModuleWarType.REPORT.getValue(), EndPointConstants.GENERATE_PRELIMINARY_BILLING_REPORT, input);
	}
}
