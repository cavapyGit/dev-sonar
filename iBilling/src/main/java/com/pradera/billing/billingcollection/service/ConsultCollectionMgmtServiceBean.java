package com.pradera.billing.billingcollection.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.Query;
import javax.transaction.TransactionSynchronizationRegistry;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.billing.billingcollection.to.CollectionMoreDetailTo;
import com.pradera.billing.billingcollection.to.CollectionRecordTo;
import com.pradera.billing.billingcollection.to.ProcessedServiceTo;
import com.pradera.billing.billingrate.to.DetailRateMovementTo;
import com.pradera.billing.billingrate.to.RateMovementTo;
import com.pradera.billing.billingrate.to.ServiceRateTo;
import com.pradera.billing.billingservice.to.BillingServiceTo;
import com.pradera.billing.common.utils.BillingUtils;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.billing.CollectionRecord;
import com.pradera.model.billing.CollectionRecordDetail;
import com.pradera.model.billing.type.EntityCollectionType;
import com.pradera.model.billing.type.RateType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ConsultCollectionMgmtServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ConsultCollectionMgmtServiceBean extends CrudDaoServiceBean {
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	
	/**
	 * Search List CollectionRecordTo For Consult By Services.
	 *
	 * @param filter the filter
	 * @return List the CollectionRecordTo
	 * @throws ServiceException the service exception
	 */
	public  List<CollectionRecordTo> getSearchListCollectionRecordToToConsultByServices(CollectionRecordTo filter)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("  ");
	    sbQuery.append(" select PS.CALCULATION_DATE, ");    
	    sbQuery.append(" (SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE parameter_table_PK=BS.BASE_COLLECTION ) AS BASE_COLLLECTION,   ");
	    sbQuery.append("  CASE");
	    sbQuery.append("  	WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN nvl((SELECT BUSINESS_NAME FROM ISSUER WHERE ID_ISSUER_PK=cr.ID_ISSUER_FK ),' ') ");
	    sbQuery.append("  	WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN  nvl((SELECT DESCRIPTION from PARTICIPANT WHERE ID_PARTICIPANT_PK=CR.ID_PARTICIPANT_FK ),' ') ");
	    sbQuery.append("  	WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN nvl((SELECT full_name FROM HOLDER WHERE ID_HOLDER_PK=CR.ID_HOLDER_FK  ),' ') ");
	    sbQuery.append("  ");
	    sbQuery.append("  	WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" ");
	    sbQuery.append(" 	THEN	NVL((SELECT PARAMETER_NAME FROM PARAMETER_TABLE WHERE parameter_table_PK=CR.DOCUMENT_TYPE ),' ' )|| ' - ' || ");
	    sbQuery.append("  		nvl(CR.OTHER_ENTITY_DOCUMENT,' ') || ' - ' || nvl(CR.OTHER_ENTITY_DESCRIPTION,' ')         ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.STOCK_EXCHANGE.getCode()+" THEN NVL(   (SELECT DESCRIPTION FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK=BS.ENTITY_COLLECTION),  ' ') ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.CENTRAL_BANK.getCode()+" THEN NVL(   to_char((SELECT DESCRIPTION FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK=BS.ENTITY_COLLECTION)),  ' ') ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.HACIENDA.getCode()+" THEN NVL(   to_char((SELECT DESCRIPTION FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK=BS.ENTITY_COLLECTION) ),  ' ')   ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.AFP.getCode()+" THEN nvl((SELECT DESCRIPTION from PARTICIPANT WHERE ID_PARTICIPANT_PK=CR.ID_PARTICIPANT_FK ),' ')   ");//afp
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.REGULATOR_ENTITY.getCode()+" THEN NVL(   to_char((SELECT DESCRIPTION FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK=BS.ENTITY_COLLECTION) ),  ' ')   ");
	    sbQuery.append(" END AS ENTITY_COLLECTION, ");
	    sbQuery.append("     CR.COLLECTION_AMOUNT,CR.GROSS_AMOUNT, CR.TAX_APPLIED, ");
	    sbQuery.append("  CASE  WHEN  CR.IND_BILLED=0  THEN 'NO FACTURADO'");	    
	    sbQuery.append("   WHEN  CR.IND_BILLED=1  THEN 'FACTURADO' END  AS STATUS_BILLING , "); 
	    sbQuery.append(" cr.id_collection_record_pk,BS.SERVICE_CODE,BS.SERVICE_NAME, ");
	    sbQuery.append("  (SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE parameter_table_PK=BS.SERVICE_TYPE ) AS SERVICE_TYPE,  ");
	    sbQuery.append("   (SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE parameter_table_PK=BS.CLIENT_SERVICE_TYPE ) AS CLIENT_SERVICE_TYPE, ");
	    sbQuery.append("  BS.ID_BILLING_SERVICE_PK, ");
	    sbQuery.append(" (SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE parameter_table_PK=bs.CURRENCY_CALCULATION ) AS DESC_CURRENCY_BILLED_TYPE  ");
	    sbQuery.append("  from  processed_service ps  ");
	    sbQuery.append("  inner join billing_service bs on  ( ps.ID_BILLING_SERVICE_FK = bs.ID_BILLING_SERVICE_PK)");
	    sbQuery.append("  INNER JOIN COLLECTION_RECORD CR ON (PS.ID_PROCESSED_SERVICE_PK= CR.ID_PROCESSED_SERVICE_FK)");
	    sbQuery.append("  WHERE 1=1 ");
	    sbQuery.append("  ");
	    if(Validations.validateIsNotNullAndNotEmpty(filter.getProcessedServiceTo().getBillingServiceTo())){
	    	if(Validations.validateIsNotNullAndNotEmpty(filter.getProcessedServiceTo().getBillingServiceTo().getServiceCode())){
			sbQuery.append(" and BS.SERVICE_CODE= :billingServiceCodePrm");}
	    	
	    	if(Validations.validateIsNotNull(filter.getProcessedServiceTo().getBillingServiceTo().getEntityCollection())){
	    		Integer entityCollection=filter.getProcessedServiceTo().getBillingServiceTo().getEntityCollection();
	    		if(entityCollection.compareTo(EntityCollectionType.HOLDERS.getCode())==0){
	    			if(Validations.validateIsNotNull(filter.getHolder())&&
	    					Validations.validateIsNotNullAndPositive(filter.getHolder().getIdHolderPk())){
	    				sbQuery.append(" and CR.ID_HOLDER_FK= :entityHolderPrm");
	    			}
	    			
	    		}
	    		else if(entityCollection.compareTo(EntityCollectionType.ISSUERS.getCode())==0){
	    			if(Validations.validateIsNotNull(filter.getIssuer())&&
	    					Validations.validateIsNotNullAndNotEmpty(filter.getIssuer().getIdIssuerPk())){
	    				sbQuery.append(" and cr.ID_ISSUER_FK= :entityIssuerPrm");
	    			}
	    			
	    		}
	    		else if(entityCollection.compareTo(EntityCollectionType.PARTICIPANTS.getCode())==0){
	    			if(Validations.validateIsNotNull(filter.getParticipant())){
	    				sbQuery.append(" and CR.ID_PARTICIPANT_FK= :entityParticipantPrm");
	    			}
	    			
	    		}
	    		else if(entityCollection.compareTo(EntityCollectionType.OTHERS.getCode())==0){
	    			if(Validations.validateIsNotNullAndNotEmpty(filter.getOtherEntityDocument())){
	    				sbQuery.append(" and CR.OTHER_ENTITY_DOCUMENT= :entityOtherDocumentPrm");
	    			}
	    			if(Validations.validateIsNotNullAndPositive(filter.getDocumentType())){
	    				sbQuery.append(" and CR.DOCUMENT_TYPE= :documentTypePrm");
	    			}	    			
	    		}
	    	}
		}
	    if(Validations.validateIsNotNull(filter.getInitialDate())&&Validations.validateIsNotNull(filter.getFinalDate())){
			  sbQuery.append(" and ps.CALCULATION_DATE BETWEEN :initialDatePrm and :finalDatePrm");
		  }
	    sbQuery.append(" order by  ");
	    sbQuery.append("  CASE");
	    sbQuery.append("  	WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN nvl((SELECT BUSINESS_NAME FROM ISSUER WHERE ID_ISSUER_PK=cr.ID_ISSUER_FK ),' ') ");
	    sbQuery.append("  	WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN  nvl((SELECT DESCRIPTION from PARTICIPANT WHERE ID_PARTICIPANT_PK=CR.ID_PARTICIPANT_FK ),' ') ");
	    sbQuery.append("  	WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN nvl((SELECT full_name FROM HOLDER WHERE ID_HOLDER_PK=CR.ID_HOLDER_FK  ),' ') ");
	    sbQuery.append("  ");
	    sbQuery.append("  	WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" ");
	    sbQuery.append(" 	THEN	NVL((SELECT PARAMETER_NAME FROM PARAMETER_TABLE WHERE parameter_table_PK=CR.DOCUMENT_TYPE ),' ' )|| ' - ' || ");
	    sbQuery.append("  		nvl(CR.OTHER_ENTITY_DOCUMENT,' ') || ' - ' || nvl(CR.OTHER_ENTITY_DESCRIPTION,' ')         ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.STOCK_EXCHANGE.getCode()+" THEN NVL(   (SELECT DESCRIPTION FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK=BS.ENTITY_COLLECTION),  ' ') ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.CENTRAL_BANK.getCode()+" THEN NVL(   to_char((SELECT DESCRIPTION FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK=BS.ENTITY_COLLECTION)),  ' ') ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.HACIENDA.getCode()+" THEN NVL(   to_char((SELECT DESCRIPTION FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK=BS.ENTITY_COLLECTION) ),  ' ')   ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.AFP.getCode()+" THEN nvl((SELECT DESCRIPTION from PARTICIPANT WHERE ID_PARTICIPANT_PK=CR.ID_PARTICIPANT_FK ),' ')   ");//afp
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.REGULATOR_ENTITY.getCode()+" THEN NVL(   to_char((SELECT DESCRIPTION FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK=BS.ENTITY_COLLECTION) ),  ' ')   ");
	    sbQuery.append(" END ");
	    Query query = em.createNativeQuery(sbQuery.toString());
	    
	    if(Validations.validateIsNotNullAndNotEmpty(filter.getProcessedServiceTo().getBillingServiceTo())){
	    	if(Validations.validateIsNotNullAndNotEmpty(filter.getProcessedServiceTo().getBillingServiceTo().getServiceCode())){
			query.setParameter("billingServiceCodePrm", filter.getProcessedServiceTo().getBillingServiceTo().getServiceCode());
	    	}
	    	if(Validations.validateIsNotNull(filter.getProcessedServiceTo().getBillingServiceTo().getEntityCollection())){
	    		Integer entityCollection=filter.getProcessedServiceTo().getBillingServiceTo().getEntityCollection();
	    		if(entityCollection.compareTo(EntityCollectionType.HOLDERS.getCode())==0){
	    			if(Validations.validateIsNotNull(filter.getHolder())&&
	    					Validations.validateIsNotNullAndPositive(filter.getHolder().getIdHolderPk())){
	    				query.setParameter("entityHolderPrm", filter.getHolder().getIdHolderPk());
	    			}
	    			
	    		}
	    		else if(entityCollection.compareTo(EntityCollectionType.ISSUERS.getCode())==0){
	    			if(Validations.validateIsNotNull(filter.getIssuer())&&
	    					Validations.validateIsNotNullAndNotEmpty(filter.getIssuer().getIdIssuerPk())){
	    				query.setParameter("entityIssuerPrm", filter.getIssuer().getIdIssuerPk());
	    			}
	    			
	    		}
	    		else if(entityCollection.compareTo(EntityCollectionType.PARTICIPANTS.getCode())==0){
	    			if(Validations.validateIsNotNull(filter.getParticipant())){
	    				query.setParameter("entityParticipantPrm", filter.getParticipant().getIdParticipantPk());
	    			}
	    			
	    		}
	    		else if(entityCollection.compareTo(EntityCollectionType.OTHERS.getCode())==0){
	    			if(Validations.validateIsNotNullAndNotEmpty(filter.getOtherEntityDocument())){
	    				query.setParameter("entityOtherDocumentPrm", filter.getOtherEntityDocument());
	    			}
	    			if(Validations.validateIsNotNullAndPositive(filter.getDocumentType())){
	    				query.setParameter("documentTypePrm", filter.getDocumentType());
	    			}
	    			
	    		}
	    	}
		}
	    if(Validations.validateIsNotNull(filter.getInitialDate())&&Validations.validateIsNotNull(filter.getFinalDate())){
	    	query.setParameter("initialDatePrm", filter.getInitialDate());
			  query.setParameter("finalDatePrm", filter.getFinalDate());
		  }
	    List<Object[]>  objectList = query.getResultList();
	    List<CollectionRecordTo> collectionRecordToList=new ArrayList<CollectionRecordTo>();
	    CollectionRecordTo collectionRecordTo;
	    BillingServiceTo billingServiceTo;
	    ProcessedServiceTo processedServiceTo;
	    for(int i=0;i<objectList.size();++i){
	    	collectionRecordTo=new CollectionRecordTo();
	    	Object[] sResults = (Object[])objectList.get(i);
	    	processedServiceTo=new ProcessedServiceTo();
	    	billingServiceTo=new BillingServiceTo();
	    	collectionRecordTo.setCalculationDate(sResults[0]==null?null:(Date) sResults[0]);
	    	billingServiceTo.setDescriptionBaseCollection(sResults[1]==null?"":sResults[1].toString());
	    	collectionRecordTo.setDescriptionEntityCollection(sResults[2]==null?"":sResults[2].toString());
	    	collectionRecordTo.setCollectionAmount(sResults[3]==null?null:new BigDecimal(sResults[3].toString()));
	    	collectionRecordTo.setGrossAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
	    	collectionRecordTo.setTaxApplied(sResults[5]==null?null:new BigDecimal(sResults[5].toString()));
	    	collectionRecordTo.setDescriptionIndBilled(sResults[6]==null?"":sResults[6].toString());
	    	collectionRecordTo.setIdCollectionRecordPk(Long.parseLong(sResults[7]==null?"":sResults[7].toString()));
	    	billingServiceTo.setServiceCode(sResults[8]==null?"":sResults[8].toString());
	    	billingServiceTo.setServiceName(sResults[9]==null?"":sResults[9].toString());
	    	billingServiceTo.setDescriptionServiceType(sResults[10]==null?"":sResults[10].toString());
	    	billingServiceTo.setDescriptionClientServiceType(sResults[11]==null?"":sResults[11].toString());
	    	billingServiceTo.setIdBillingServicePk(Long.parseLong(sResults[12]==null?"":sResults[12].toString()));
	    	collectionRecordTo.setDescriptionCurrencyType(sResults[13]==null?"":sResults[13].toString());
	    	processedServiceTo.setBillingServiceTo(billingServiceTo);
	    	collectionRecordTo.setProcessedServiceTo(processedServiceTo);
	    	collectionRecordToList.add(collectionRecordTo);
	    }
		return collectionRecordToList;
	}
	
	/**
	 *  Get Detail For Collection Record.
	 *
	 * @param filter the filter
	 * @return the collection record detail for collection
	 * @throws ServiceException the service exception
	 */
//	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
//	@TransactionTimeout(unit=TimeUnit.HOURS,value=3)
//	@Performance
	public List<CollectionMoreDetailTo> getCollectionRecordDetailForCollection(CollectionRecordTo filter)throws ServiceException{
		
		List<CollectionMoreDetailTo> listCollectionRecordDetailTo=new ArrayList<>();
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT CRD.ID_COLLECTION_RECORD_DETAIL_PK,");//0
		sbQuery.append(" 		CRD.SECURITY_CLASS,");//1
		sbQuery.append(" 		CRD.CURRENCY_ORIGIN,");//2
		sbQuery.append(" 		CRD.RATE_AMOUNT,");//3
		sbQuery.append(" 		CRD.RATE_PERCENT,");//4
		sbQuery.append(" 		CRD.ID_SECURITY_CODE_FK,");//5
		sbQuery.append(" 		CRD.ID_HOLDER_FK,");//6
		sbQuery.append(" 		CRD.ID_MOVEMENT_TYPE_FK,");//7
		sbQuery.append(" 		CRD.CURRENCY_RATE,");//8
		sbQuery.append("  		(SELECT PT.DESCRIPTION from PARAMETER_TABLE PT where ");//  description currency origin
		sbQuery.append(" 	 parameter_table_pk= crd.CURRENCY_ORIGIN) as des_currency_origin,	");//9
		sbQuery.append("  		(SELECT PT.DESCRIPTION from PARAMETER_TABLE PT where ");//  description currency Rate
		sbQuery.append(" 	 parameter_table_pk= crd.CURRENCY_RATE) as des_currency_rate,	");//10
		sbQuery.append("  		(SELECT PT.DESCRIPTION from PARAMETER_TABLE PT where ");//  description security_class
		sbQuery.append(" 	 parameter_table_pk= crd.SECURITY_CLASS) as desc_security_class,	");//11
		sbQuery.append(" 		NVL((select FULL_NAME from holder HLD ");
		sbQuery.append(" 	where HLD.ID_HOLDER_PK=crd.ID_HOLDER_FK),' ') as DESC_RNT	");//12
		sbQuery.append("	FROM COLLECTION_RECORD_DETAIL CRD");
		sbQuery.append("    WHERE 1=1 ");
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdCollectionRecordPk()))
		{
			sbQuery.append(" And CRD.ID_COLLECTION_RECORD_FK = :collectionRecordId");
		}
		sbQuery.append("   ORDER BY CRD.ID_COLLECTION_RECORD_FK , crd.ID_SECURITY_CODE_FK, crd.CURRENCY_RATE, ");
		sbQuery.append(" 		CRD.ID_HOLDER_FK");
		Query query = em.createNativeQuery(sbQuery.toString()); 
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdCollectionRecordPk()))
		{
			query.setParameter("collectionRecordId", filter.getIdCollectionRecordPk());			
		}
		List<Object[]>  objectList = query.getResultList();
		for(int i=0;i<objectList.size();++i)
	    {
	    	Object[] sResults = (Object[])objectList.get(i);
	    	CollectionMoreDetailTo moreCollectionDetail= new CollectionMoreDetailTo();
	    	moreCollectionDetail.setIdCollectionRecordDetailPk(Long.parseLong(sResults[0].toString()));
	    	moreCollectionDetail.setSecurityClass(sResults[1]==null?0:Integer.parseInt(sResults[1].toString()));
	    	moreCollectionDetail.setCurrencyOrigin(sResults[2]==null?0:Integer.parseInt(sResults[2].toString()));
	    	moreCollectionDetail.setAmount(sResults[3]==null?null:new BigDecimal(sResults[3].toString()));
	    	moreCollectionDetail.setPercentage(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
	    	moreCollectionDetail.setIdSecurityCodeFk(sResults[5]==null?"":sResults[5].toString());
	    	moreCollectionDetail.setCalculationDate(filter.getCalculationDate());
	    	moreCollectionDetail.setIdHolderPk(sResults[6]==null?new Long(0):Long.parseLong(sResults[6].toString()));
	    	moreCollectionDetail.setIdMovementTypePk(sResults[7]==null?new Long(0):Long.parseLong(sResults[7].toString()));
	    	moreCollectionDetail.setCurrencyRate(sResults[8]==null?0:Integer.parseInt(sResults[8].toString()));
	    	moreCollectionDetail.setDescriptionCurrencyOrigin(sResults[9]==null?"":sResults[9].toString());
	    	moreCollectionDetail.setDescriptionCurrencyRate(sResults[10]==null?"":sResults[10].toString());
	    	moreCollectionDetail.setDescriptionSecurityClass(sResults[11]==null?"":sResults[11].toString());
	    	moreCollectionDetail.setDescriptionHolder(sResults[12]==null?"":sResults[12].toString());

	    	listCollectionRecordDetailTo.add(moreCollectionDetail);
	    }
		return listCollectionRecordDetailTo;
	}
	
	/**
	 * Gets the service rate to list for collection.
	 *
	 * @param filter the filter
	 * @return the service rate to list for collection
	 * @throws ServiceException the service exception
	 */
//	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
//	@TransactionTimeout(unit=TimeUnit.HOURS,value=3)
//	@Performance
	public List<ServiceRateTo> getServiceRateToListForCollection(CollectionRecordTo filter)throws ServiceException{
		log.info(":::::::::::::::::::::::: inicio de LA CONSULTA :::::::::::::::::::::::::::::::");
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select crd.ID_COLLECTION_RECORD_DETAIL_PK,");//0
		sbQuery.append(" crd.MOVEMENT_COUNT,");//1
		sbQuery.append(" crd.ID_MOVEMENT_TYPE_FK, ");//2
		sbQuery.append(" crd.ID_SECURITY_CODE_FK,");//3
		sbQuery.append(" crd.RATE_AMOUNT, ");//4
		sbQuery.append(" crd.RATE_PERCENT, ");//5

		sbQuery.append(" crd.ID_COLLECTION_RECORD_FK,");//6
		sbQuery.append(" crd.SEQUENCE_NUMBER, ");//7
		
		sbQuery.append(" crd.ID_SERVICE_RATE_FK    as id_service_rate_pk , ");//8
		sbQuery.append("  ( select sr.RATE_CODE from service_rate sr where sr.id_service_rate_pk=crd.ID_SERVICE_RATE_FK ) as RATE_CODE ,");//9
		sbQuery.append("  ( select sr.RATE_NAME from service_rate sr where sr.id_service_rate_pk=crd.ID_SERVICE_RATE_FK ) as RATE_NAME ,");//10
		sbQuery.append("  ( select sr.RATE_Type from service_rate sr where sr.id_service_rate_pk=crd.ID_SERVICE_RATE_FK ) as Rate_Type ,  ");//11
		sbQuery.append("  ( select (select pt.parameter_name from parameter_table pt where parameter_table_pk= sr.RATE_Type)");
		sbQuery.append("  from service_rate sr where sr.id_service_rate_pk=crd.ID_SERVICE_RATE_FK ) as desription_Rate_Type ,");//12
		sbQuery.append(" ( select mt.id_movement_type_pk|| '-' || mt.MOVEMENT_NAME from movement_type mt");
		sbQuery.append("  where mt.ID_MOVEMENT_TYPE_PK=crd.ID_MOVEMENT_TYPE_FK ) as desription_MOVEMENT_TYPE , ");//13
		sbQuery.append(" crd.CURRENCY_RATE,");//14  currency operation 
		sbQuery.append("  (select pt.description from parameter_table pt where parameter_table_pk= crd.CURRENCY_RATE) as description_current, ");//15  description currency operation 
		sbQuery.append(" crd.MOVEMENT_QUANTITY,  ");//16
		sbQuery.append("  crd.OPERATION_PRICE, ");//17
		sbQuery.append(" ( select (select rm.rate_amount from rate_movement rm where rm.id_service_rate_fk= sr.id_service_rate_pk and rm.ID_MOVEMENT_TYPE_FK=crd.ID_MOVEMENT_TYPE_FK)  ");
		sbQuery.append("  from service_rate sr where sr.id_service_rate_pk=crd.ID_SERVICE_RATE_FK ) as rate_amount_movement , ");//18
		sbQuery.append(" ( select (select rm.rate_percent from rate_movement rm where rm.id_service_rate_fk= sr.id_service_rate_pk and rm.ID_MOVEMENT_TYPE_FK=crd.ID_MOVEMENT_TYPE_FK)");
		sbQuery.append("  from service_rate sr where sr.id_service_rate_pk=crd.ID_SERVICE_RATE_FK ) as rate_percent_movement, ");//19
		sbQuery.append("  crd.OPERATION_PRICE *");
		sbQuery.append(" crd.MOVEMENT_QUANTITY *");
		sbQuery.append(" ( select (select rm.rate_percent from rate_movement rm where rm.id_service_rate_fk= sr.id_service_rate_pk and rm.ID_MOVEMENT_TYPE_FK=crd.ID_MOVEMENT_TYPE_FK)");
		sbQuery.append("  from service_rate sr where sr.id_service_rate_pk=crd.ID_SERVICE_RATE_FK ) as DIRTY_AMOUNT , " );//20
 
		sbQuery.append("  NVL((select description from participant prt where prt.id_participant_pk=crd.ID_SOURCE_PARTICIPANT_FK),' ') as ID_SOURCE_PARTICIPANT_FK , ");//21
		sbQuery.append("  NVL((select description from participant prt where prt.id_participant_pk=crd.ID_TARGET_PARTICIPANT_FK),' ') as ID_TARGET_PARTICIPANT_FK ,   ");//22
		sbQuery.append("  NVL((select FULL_NAME from holder HLD where HLD.ID_HOLDER_PK=crd.ID_HOLDER_FK),' ') as DESC_RNT  ,  ");//23
		sbQuery.append("  crd.MOVEMENT_DATE,    ");//24
		sbQuery.append("  crd.OPERATION_DATE,    ");//25
		sbQuery.append("  crd.OPERATION_NUMBER,   ");//26 
		sbQuery.append("  NVL((SELECT ACCOUNT_NUMBER FROM HOLDER_ACCOUNT HA WHERE HA.ID_HOLDER_ACCOUNT_PK =crd.ID_HOLDER_ACCOUNT_FK), 0 ) AS ACCOUNT_NUMBER, 		  ");//27 
		sbQuery.append("  NVL(crd.ID_HOLDER_FK,0) AS RNT, ");//28
		sbQuery.append("  (SELECT currency_rate FROM service_rate WHERE id_service_rate_pk = CRD.ID_SERVICE_RATE_FK  ) CURRENCY_SERV_RATE,   ");//29  currency service rate
		sbQuery.append("  ( select sr.IND_SCALE_MOVEMENT from service_rate sr where sr.id_service_rate_pk=crd.ID_SERVICE_RATE_FK ) as IND_SCALE_MOVEMENT ");//30  IND_SCALE_MOVEMENT
		
		
		sbQuery.append(" from collection_record_detail crd");
		sbQuery.append("  Where 1 = 1  ");
		sbQuery.append(" ");
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdCollectionRecordPk()))
		{
			sbQuery.append(" And crd.ID_COLLECTION_RECORD_FK = :collectionRecordId");
		}
		sbQuery.append("   order by  crd.ID_COLLECTION_RECORD_FK ,crd.ID_MOVEMENT_TYPE_FK,crd.ID_SECURITY_CODE_FK,crd.CURRENCY_RATE ");
		Query query = em.createNativeQuery(sbQuery.toString()); 
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdCollectionRecordPk()))
		{
			query.setParameter("collectionRecordId", filter.getIdCollectionRecordPk());			
		} 
		List<Object[]>  objectList = query.getResultList();
	    List<ServiceRateTo> serviceRateToList=new ArrayList<ServiceRateTo>();
	    ServiceRateTo serviceRateTo=null;
	    List<RateMovementTo> listRateMovementTo=new ArrayList<RateMovementTo>();
	    
	    List<DetailRateMovementTo> listDetailRateMovementTo= new ArrayList<DetailRateMovementTo>();
	    DetailRateMovementTo detailRateMovementTo;
	    Long idServiceRate;
	   
	    /**Counter variable*/
	    BigDecimal rateAmountByRate=new BigDecimal(0);
	  
	    RateMovementTo rateMovementTo=null;
	   
	    Integer countOperationByRate=0;
	    Integer countOperationByKey=0;  // el key es formado por TIPO DE MOVIMIENTO , VALOR Y MONEDA.
	    
	    String keyDetailRateMovement="";
	    String keyDetailRateMovementTemp="";
	    
	    BigDecimal temporal=BigDecimal.ZERO;  
	    BigDecimal amountOperation=BigDecimal.ZERO;
	    BigDecimal movementAmountTemp=BigDecimal.ZERO;  // used for custody' movement transaction .
	    
	    for(int i=0;i<objectList.size();++i)
	    {
	    	Object[] sResults = (Object[])objectList.get(i); 
	    	if(Validations.validateIsNotNull(sResults[8]))
	    	{
	    		idServiceRate=Long.parseLong(sResults[8].toString());
	    	}
	    	else{
	    		idServiceRate=1L;
	    	}
	    		
	    	
	    	/**Key para el primer quiebre**/
	    	 if(Validations.validateIsNotNullAndNotEmpty(sResults[2])
    				&&Validations.validateIsNotNullAndNotEmpty(sResults[3])
    				&&Validations.validateIsNotNullAndNotEmpty(sResults[14])){
	    		 /** 
						VALOR:  				3
						TIPO DE MOVIMIENTO:  	2
						MONEDA:					14
					*
					*
					*/ 
    			keyDetailRateMovement=sResults[2].toString(). concat(BillingUtils.VERTICAL).
    					concat(sResults[3].toString(). concat(BillingUtils.VERTICAL).
    					concat(sResults[14].toString())); 
    		}
	    	 else if(Validations.validateIsNotNullAndNotEmpty(sResults[2])
	    				&&Validations.validateIsNotNullAndNotEmpty(sResults[3])){
	    		 /** SI NO HAY MONEDA
	    		  * VALOR:  				3
	    		  * TIPO DE MOVIMIENTO:  	2
	    		  */ 
			keyDetailRateMovement=sResults[2].toString(). concat(BillingUtils.VERTICAL).
					concat(sResults[3].toString()); 
	    	 }
    		else{
    			keyDetailRateMovement="";
    		}
	    	 
	    	if(i==0){
	    		/**
	    		 * FIRST ITERATION
	    		 * */
	    		keyDetailRateMovementTemp=keyDetailRateMovement;
	    		rateMovementTo=new RateMovementTo();
	    		detailRateMovementTo=new DetailRateMovementTo();
	    		serviceRateTo=new ServiceRateTo();
		    	serviceRateTo.setIdServiceRatePk(sResults[8]==null?new Long(0):Long.parseLong(sResults[8].toString()));
		    	serviceRateTo.setRateCode(sResults[9]==null?0:Integer.parseInt(sResults[9].toString()));
		    	serviceRateTo.setRateName(sResults[10]==null?"":sResults[10].toString());
		    	serviceRateTo.setRateType(sResults[11]==null?0:Integer.parseInt(sResults[11].toString()));
		    	serviceRateTo.setDescriptionType(sResults[12]==null?"":sResults[12].toString());
		    	serviceRateTo.setCurrencyRate(sResults[29]==null?0:Integer.parseInt(sResults[29].toString()));
		    	serviceRateTo.setDescriptionCurrency(filter.getDescriptionCurrencyType());
		    	serviceRateTo.setIndScaleMovement(sResults[30]==null?0:Integer.parseInt(sResults[30].toString()));
		    	 
		    	if(serviceRateTo.getRateType().equals(RateType.TIPO_MOVIMIENTO_FIJO.getCode())){
		    	
		    		/**Detail movement*/
		    		detailRateMovementTo=loadDetailRateMovement(detailRateMovementTo,sResults);
		    		
			    	detailRateMovementTo.setAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
			    	detailRateMovementTo.setMovementQuantity(sResults[16]==null?new Long(0):Long.parseLong(sResults[16].toString()));//
			    	listDetailRateMovementTo.add(detailRateMovementTo);
		    		
			    	rateMovementTo.setDescriptionCurrencyRate(sResults[15]==null?"":sResults[15].toString());
		    		rateMovementTo.setDescriptionMovementType(sResults[13]==null?"":sResults[13].toString());
		    		rateMovementTo.setSecurityCode(sResults[3]==null?"":sResults[3].toString());
			    	rateMovementTo.setRateAmount(sResults[18]==null?null:new BigDecimal(sResults[18].toString()));
			    	rateMovementTo.setGrossAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
			    
			    
			    	rateMovementTo.setListDetailRateMovementTo(listDetailRateMovementTo);
			    	countOperationByKey++;
			    		    	
			    	rateMovementTo.setOperationCount(countOperationByKey);
			    	
			    	listRateMovementTo.add(rateMovementTo);
			    	serviceRateTo.setListRateMovementTo(listRateMovementTo);
			    	rateAmountByRate=sResults[4]==null?null:new BigDecimal(sResults[4].toString());
		    	}
		    	else if(serviceRateTo.getRateType().equals(RateType.TIPO_MOVIMIENTO_PORCENTUAL.getCode())){
		    		BigDecimal ratePercent=sResults[19]==null?null:new BigDecimal(sResults[19].toString());

		    		/**Detail movement*/
		    		detailRateMovementTo=loadDetailRateMovement(detailRateMovementTo,sResults);
			    	detailRateMovementTo.setAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
			    	detailRateMovementTo.setMovementQuantity(sResults[16]==null?new Long(0):Long.parseLong(sResults[16].toString()));
			    	detailRateMovementTo.setOperationPrice(sResults[17]==null?null:new BigDecimal(sResults[17].toString()));
			    	temporal  = BillingUtils.multiplyDecimalLong(detailRateMovementTo.getOperationPrice() ,detailRateMovementTo.getMovementQuantity()  , GeneralConstants.ROUNDING_DIGIT_NUMBER);
			    	amountOperation = BillingUtils.multiplyDecimals(ratePercent, temporal, GeneralConstants.ROUNDING_DIGIT_NUMBER);
			    	detailRateMovementTo.setOperationAmount(amountOperation);
			    	
			    

			    	
			    	listDetailRateMovementTo.add(detailRateMovementTo);


		    		rateMovementTo.setDescriptionCurrencyRate(sResults[15]==null?"":sResults[15].toString());
		    		rateMovementTo.setDescriptionMovementType(sResults[13]==null?"":sResults[13].toString());
		    		rateMovementTo.setSecurityCode(sResults[3]==null?"":sResults[3].toString());
			    
			    	rateMovementTo.setRatePercent(BillingUtils.multiplyDecimalInteger(ratePercent, 100, GeneralConstants.ROUNDING_DIGIT_NUMBER)); //5
			    	rateMovementTo.setMovementQuantity(sResults[16]==null?new Long(0):Long.parseLong(sResults[16].toString())); 
			    	rateMovementTo.setGrossAmount(sResults[5]==null?null:new BigDecimal(sResults[5].toString()));
			    	rateMovementTo.setDirtyAmount(sResults[20]==null?null:new BigDecimal(sResults[20].toString()));
			    	
			    	countOperationByKey++;
			    	rateMovementTo.setOperationCount(countOperationByKey);
			    	
			    	rateMovementTo.setListDetailRateMovementTo(listDetailRateMovementTo);
			    	
			    	listRateMovementTo.add(rateMovementTo);
			    	serviceRateTo.setListRateMovementTo(listRateMovementTo);
			    	rateAmountByRate=sResults[5]==null?null:new BigDecimal(sResults[5].toString());
		    	}
		    	else if(serviceRateTo.getRateType().equals(RateType.PORCENTUAL.getCode())){
		    		rateAmountByRate=sResults[5]==null?null:new BigDecimal(sResults[5].toString());
		    	}
		    	else if(serviceRateTo.getRateType().equals(RateType.FIJO.getCode())){
		    		rateAmountByRate=sResults[4]==null?null:new BigDecimal(sResults[4].toString());
		    	}
		    	else if(serviceRateTo.getRateType().equals(RateType.ESCALONADO_FIJO.getCode())){
		    		/**Obtener los movimientos**/
		    		if(serviceRateTo.getIndScaleMovement().equals(PropertiesConstants.YES_VALUE_INTEGER)){

				    	
			    		/**Detail movement*/
			    		detailRateMovementTo=loadDetailRateMovement(detailRateMovementTo,sResults);
			    		
				    	detailRateMovementTo.setAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
				    	detailRateMovementTo.setMovementQuantity(sResults[16]==null?new Long(0):Long.parseLong(sResults[16].toString()));//
				    	listDetailRateMovementTo.add(detailRateMovementTo);
			    		
				    	rateMovementTo.setDescriptionCurrencyRate(sResults[15]==null?"":sResults[15].toString());
			    		rateMovementTo.setDescriptionMovementType(sResults[13]==null?"":sResults[13].toString());
			    		rateMovementTo.setSecurityCode(sResults[3]==null?"":sResults[3].toString());
				    	rateMovementTo.setRateAmount(sResults[18]==null?null:new BigDecimal(sResults[18].toString()));
				    	rateMovementTo.setGrossAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
				    
				    
				    	rateMovementTo.setListDetailRateMovementTo(listDetailRateMovementTo);
				    	countOperationByKey++;
				    		    	
				    	rateMovementTo.setOperationCount(countOperationByKey);
				    	
				    	listRateMovementTo.add(rateMovementTo);
				    	serviceRateTo.setListRateMovementTo(listRateMovementTo);
				    	rateAmountByRate=sResults[4]==null?null:new BigDecimal(sResults[4].toString());
			    	
		    		}
		    		
		    		rateAmountByRate=sResults[4]==null?null:new BigDecimal(sResults[4].toString());
		    	}
		    	else if(serviceRateTo.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode())){
		    		/**Obtener los movimientos**/
		    		if(serviceRateTo.getIndScaleMovement().equals(PropertiesConstants.YES_VALUE_INTEGER)){

			    		BigDecimal ratePercent=sResults[19]==null?null:new BigDecimal(sResults[19].toString());

			    		/**Detail movement*/
			    		detailRateMovementTo=loadDetailRateMovement(detailRateMovementTo,sResults);
				    	
				    	detailRateMovementTo.setAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
				    	detailRateMovementTo.setMovementQuantity(sResults[16]==null?new Long(0):Long.parseLong(sResults[16].toString()));
				    	detailRateMovementTo.setOperationPrice(sResults[17]==null?null:new BigDecimal(sResults[17].toString()));
				    	temporal  = BillingUtils.multiplyDecimalLong(detailRateMovementTo.getOperationPrice() ,detailRateMovementTo.getMovementQuantity()  , GeneralConstants.ROUNDING_DIGIT_NUMBER);
				    	amountOperation = BillingUtils.multiplyDecimals(ratePercent, temporal, GeneralConstants.ROUNDING_DIGIT_NUMBER);
				    	detailRateMovementTo.setOperationAmount(amountOperation);
				    	
				    	listDetailRateMovementTo.add(detailRateMovementTo);


			    		rateMovementTo.setDescriptionCurrencyRate(sResults[15]==null?"":sResults[15].toString());
			    		rateMovementTo.setDescriptionMovementType(sResults[13]==null?"":sResults[13].toString());
			    		rateMovementTo.setSecurityCode(sResults[3]==null?"":sResults[3].toString());
				    
				    	rateMovementTo.setRatePercent(BillingUtils.multiplyDecimalInteger(ratePercent, 100, GeneralConstants.ROUNDING_DIGIT_NUMBER)); //5
				    	rateMovementTo.setMovementQuantity(sResults[16]==null?new Long(0):Long.parseLong(sResults[16].toString())); 
				    	rateMovementTo.setGrossAmount(sResults[5]==null?null:new BigDecimal(sResults[5].toString()));
				    	rateMovementTo.setDirtyAmount(sResults[20]==null?null:new BigDecimal(sResults[20].toString()));
				    	
				    	countOperationByKey++;
				    	rateMovementTo.setOperationCount(countOperationByKey);
				    	
				    	rateMovementTo.setListDetailRateMovementTo(listDetailRateMovementTo);
				    	
				    	listRateMovementTo.add(rateMovementTo);
				    	serviceRateTo.setListRateMovementTo(listRateMovementTo);
				    	rateAmountByRate=sResults[5]==null?null:new BigDecimal(sResults[5].toString());
			    	
		    		}
		    		
		    		rateAmountByRate=sResults[5]==null?null:new BigDecimal(sResults[5].toString());
		    	}
		    	else if(serviceRateTo.getRateType().equals(RateType.ESCALONADO_MIXTO.getCode())){
		    		/**Obtener los movimientos**/
		    		if(PropertiesConstants.YES_VALUE_INTEGER.equals(serviceRateTo.getIndScaleMovement())){

				    	
			    		/**Detail movement*/
			    		detailRateMovementTo=loadDetailRateMovement(detailRateMovementTo,sResults);
			    		
				    	detailRateMovementTo.setAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
				    	detailRateMovementTo.setMovementQuantity(sResults[16]==null?new Long(0):Long.parseLong(sResults[16].toString()));//
				    	listDetailRateMovementTo.add(detailRateMovementTo);
			    		
				    	rateMovementTo.setDescriptionCurrencyRate(sResults[15]==null?"":sResults[15].toString());
			    		rateMovementTo.setDescriptionMovementType(sResults[13]==null?"":sResults[13].toString());
			    		rateMovementTo.setSecurityCode(sResults[3]==null?"":sResults[3].toString());
				    	rateMovementTo.setRateAmount(sResults[18]==null?null:new BigDecimal(sResults[18].toString()));
				    	rateMovementTo.setGrossAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
				    
				    
				    	rateMovementTo.setListDetailRateMovementTo(listDetailRateMovementTo);
				    	countOperationByKey++;
				    		    	
				    	rateMovementTo.setOperationCount(countOperationByKey);
				    	
				    	listRateMovementTo.add(rateMovementTo);
				    	serviceRateTo.setListRateMovementTo(listRateMovementTo);
				    	rateAmountByRate=sResults[4]==null?null:new BigDecimal(sResults[4].toString());
			    	
		    		}
		    		
		    		rateAmountByRate=sResults[4]==null?new BigDecimal(sResults[5].toString()):new BigDecimal(sResults[4].toString());
		    	}
		    	

		    	countOperationByRate++;
		    	serviceRateTo.setOperationCount(countOperationByRate);
		    	serviceRateTo.setAmountRate(rateAmountByRate);
		    	
		    	serviceRateToList.add(serviceRateTo);
			}
	    	
	    	else 
	    	{
	    		
	    		/** when service rate is same */
	    		if(idServiceRate.compareTo(serviceRateTo.getIdServiceRatePk())==0){
	    			
	    			detailRateMovementTo= new DetailRateMovementTo();
	    			if(serviceRateTo.getRateType().equals(RateType.TIPO_MOVIMIENTO_FIJO.getCode())){
	    				if(!keyDetailRateMovement.equals(keyDetailRateMovementTemp)){
		    				keyDetailRateMovementTemp=keyDetailRateMovement;
		    				listDetailRateMovementTo= new ArrayList<DetailRateMovementTo>();
		    				rateMovementTo=new RateMovementTo();
		    				countOperationByKey=0;
		    				
		    				/**Detail movement*/
		    				detailRateMovementTo=loadDetailRateMovement(detailRateMovementTo,sResults);
		    				
					    	detailRateMovementTo.setAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
					    	detailRateMovementTo.setMovementQuantity(sResults[16]==null?new Long(0):Long.parseLong(sResults[16].toString()));
					    	listDetailRateMovementTo.add(detailRateMovementTo);
		    				
				    		rateMovementTo.setDescriptionMovementType(sResults[13]==null?"":sResults[13].toString());
				    		rateMovementTo.setSecurityCode(sResults[3]==null?"":sResults[3].toString());
					    	
					    	rateMovementTo.setDescriptionCurrencyRate(sResults[15]==null?"":sResults[15].toString());
					    	rateMovementTo.setRateAmount(sResults[18]==null?null:new BigDecimal(sResults[18].toString()));
					    	rateMovementTo.setGrossAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));	
					    	
					    	rateMovementTo.setListDetailRateMovementTo(listDetailRateMovementTo);
					    	
					    	listRateMovementTo.add(rateMovementTo);
					    	serviceRateTo.setListRateMovementTo(listRateMovementTo);
					    	rateAmountByRate=BillingUtils.addDecimalObject(rateAmountByRate, sResults[4]==null?null:new BigDecimal(sResults[4].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
					    	
					    	countOperationByKey++;
					    	rateMovementTo.setOperationCount(countOperationByKey);
					    	
			    		}
	    				else{
	    					/** MORE Detail movement*/
	    					detailRateMovementTo=loadDetailRateMovement(detailRateMovementTo,sResults);
					    	
					    	detailRateMovementTo.setAmount(sResults[20]==null?null:new BigDecimal(sResults[20].toString()));//20
					    	detailRateMovementTo.setMovementQuantity(sResults[16]==null?new Long(0):Long.parseLong(sResults[16].toString()));
					    	
					    	countOperationByKey++;
					    	rateMovementTo.setOperationCount(countOperationByKey);
					    	listDetailRateMovementTo.add(detailRateMovementTo);
		    				
					    	movementAmountTemp=sResults[4]==null?null:new BigDecimal(sResults[4].toString());
					    	BigDecimal amountTemp = BillingUtils.addTwoDecimal(movementAmountTemp, rateMovementTo.getGrossAmount(), GeneralConstants.ROUNDING_DIGIT_NUMBER) ;					    	
					    	rateMovementTo.setGrossAmount(amountTemp);
					    	
					    	
					    	BigDecimal amount=BillingUtils.addTwoDecimal(detailRateMovementTo.getAmount(), rateMovementTo.getDirtyAmount(), GeneralConstants.ROUNDING_DIGIT_NUMBER);
						    rateMovementTo.setDirtyAmount(amount);				    		
					    	rateAmountByRate=BillingUtils.addDecimalObject(rateAmountByRate, sResults[4]==null?null:new BigDecimal(sResults[4].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
	    				}
	    				
			    	}
			    	else if(serviceRateTo.getRateType().equals(RateType.TIPO_MOVIMIENTO_PORCENTUAL.getCode())){
			    		BigDecimal ratePercent=sResults[19]==null?null:new BigDecimal(sResults[19].toString());
			    		if(!keyDetailRateMovement.equals(keyDetailRateMovementTemp)){
		    				keyDetailRateMovementTemp=keyDetailRateMovement;
		    				listDetailRateMovementTo= new ArrayList<DetailRateMovementTo>();
		    				rateMovementTo=new RateMovementTo();
		    				countOperationByKey=0;
		    				
		    				/**Detail movement*/
		    				detailRateMovementTo=loadDetailRateMovement(detailRateMovementTo,sResults);
		    				
					    	detailRateMovementTo.setAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
					    	detailRateMovementTo.setMovementQuantity(sResults[16]==null?new Long(0):Long.parseLong(sResults[16].toString()));
					    	detailRateMovementTo.setOperationPrice(sResults[17]==null?null:new BigDecimal(sResults[17].toString()));
					    	
					    	temporal  = BillingUtils.multiplyDecimalLong(detailRateMovementTo.getOperationPrice() ,detailRateMovementTo.getMovementQuantity()  , GeneralConstants.ROUNDING_DIGIT_NUMBER);
					    	amountOperation = BillingUtils.multiplyDecimals(ratePercent, temporal, GeneralConstants.ROUNDING_DIGIT_NUMBER);
					    	detailRateMovementTo.setOperationAmount(amountOperation);
					    	
					    	listDetailRateMovementTo.add(detailRateMovementTo);
				    		 
					    	rateMovementTo.setRatePercent(BillingUtils.multiplyDecimalInteger(ratePercent, 100, GeneralConstants.ROUNDING_DIGIT_NUMBER)); //5
					    	rateMovementTo.setDescriptionMovementType(sResults[13]==null?"":sResults[13].toString());
				    		rateMovementTo.setSecurityCode(sResults[3]==null?"":sResults[3].toString());
					    	
					    	rateMovementTo.setMovementQuantity(sResults[16]==null?new Long(0):Long.parseLong(sResults[16].toString()));
					
					    	rateMovementTo.setDescriptionCurrencyRate(sResults[15]==null?"":sResults[15].toString());
					    	//
					    	rateMovementTo.setGrossAmount(sResults[5]==null?null:new BigDecimal(sResults[5].toString()));
					    	//
					    	rateMovementTo.setDirtyAmount(sResults[20]==null?null:new BigDecimal(sResults[20].toString()));
					    	rateMovementTo.setListDetailRateMovementTo(listDetailRateMovementTo);
					    	
					    	countOperationByKey++;
					    	rateMovementTo.setOperationCount(countOperationByKey);
					    	
					    	listRateMovementTo.add(rateMovementTo);
					    	serviceRateTo.setListRateMovementTo(listRateMovementTo);
					    	rateAmountByRate=BillingUtils.addDecimalObject(rateAmountByRate, sResults[5]==null?null:new BigDecimal(sResults[5].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
				    	
			    		}
			    		else{
			    		/**MORE Detail movement*/
			    			detailRateMovementTo=loadDetailRateMovement(detailRateMovementTo,sResults);
				    	
				    	detailRateMovementTo.setAmount(sResults[20]==null?null:new BigDecimal(sResults[20].toString()));//20
				    	detailRateMovementTo.setMovementQuantity(sResults[16]==null?new Long(0):Long.parseLong(sResults[16].toString()));
				    	detailRateMovementTo.setOperationPrice(sResults[17]==null?null:new BigDecimal(sResults[17].toString()));
				    	
				    	temporal  = BillingUtils.multiplyDecimalLong(detailRateMovementTo.getOperationPrice() ,detailRateMovementTo.getMovementQuantity()  , GeneralConstants.ROUNDING_DIGIT_NUMBER);
				    	amountOperation = BillingUtils.multiplyDecimals(ratePercent, temporal, GeneralConstants.ROUNDING_DIGIT_NUMBER);
				    	detailRateMovementTo.setOperationAmount(amountOperation);
				    	
				    	listDetailRateMovementTo.add(detailRateMovementTo);
			    		 
				    	Long movementQuantity=detailRateMovementTo.getMovementQuantity()+rateMovementTo.getMovementQuantity();
				    	rateMovementTo.setMovementQuantity(movementQuantity);
				    	BigDecimal amount=BillingUtils.addTwoDecimal(detailRateMovementTo.getAmount(), rateMovementTo.getDirtyAmount(), GeneralConstants.ROUNDING_DIGIT_NUMBER);
				    	rateMovementTo.setDirtyAmount(amount);
				    	rateMovementTo.setListDetailRateMovementTo(listDetailRateMovementTo);
				    	
				    	countOperationByKey++;
				    	rateMovementTo.setOperationCount(countOperationByKey);
				    	rateAmountByRate=BillingUtils.addDecimalObject(rateAmountByRate, sResults[5]==null?null:new BigDecimal(sResults[5].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
			    	}
			     }else if(serviceRateTo.getRateType().equals(RateType.ESCALONADO_FIJO.getCode()) &&
			    		 serviceRateTo.getIndScaleMovement().equals(PropertiesConstants.YES_VALUE_INTEGER)){

	    				if(!keyDetailRateMovement.equals(keyDetailRateMovementTemp)){
		    				keyDetailRateMovementTemp=keyDetailRateMovement;
		    				listDetailRateMovementTo= new ArrayList<DetailRateMovementTo>();
		    				rateMovementTo=new RateMovementTo();
		    				countOperationByKey=0;
		    				
		    				/**Detail movement*/
		    				detailRateMovementTo=loadDetailRateMovement(detailRateMovementTo,sResults);
		    				
					    	detailRateMovementTo.setAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
					    	detailRateMovementTo.setMovementQuantity(sResults[16]==null?new Long(0):Long.parseLong(sResults[16].toString()));
					    	listDetailRateMovementTo.add(detailRateMovementTo);
		    				
				    		rateMovementTo.setDescriptionMovementType(sResults[13]==null?"":sResults[13].toString());
				    		rateMovementTo.setSecurityCode(sResults[3]==null?"":sResults[3].toString());
					    	
					    	rateMovementTo.setDescriptionCurrencyRate(sResults[15]==null?"":sResults[15].toString());
					    	rateMovementTo.setRateAmount(sResults[18]==null?null:new BigDecimal(sResults[18].toString()));
					    	rateMovementTo.setGrossAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));	
					    	
					    	rateMovementTo.setListDetailRateMovementTo(listDetailRateMovementTo);
					    	
					    	listRateMovementTo.add(rateMovementTo);
					    	serviceRateTo.setListRateMovementTo(listRateMovementTo);
					    	rateAmountByRate=BillingUtils.addDecimalObject(rateAmountByRate, sResults[4]==null?null:new BigDecimal(sResults[4].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
					    	
					    	countOperationByKey++;
					    	rateMovementTo.setOperationCount(countOperationByKey);
					    	
			    		}
	    				else{
	    					/** MORE Detail movement*/
	    					detailRateMovementTo=loadDetailRateMovement(detailRateMovementTo,sResults);
					    	
					    	detailRateMovementTo.setAmount(sResults[20]==null?null:new BigDecimal(sResults[20].toString()));//20
					    	detailRateMovementTo.setMovementQuantity(sResults[16]==null?new Long(0):Long.parseLong(sResults[16].toString()));
					    	
					    	countOperationByKey++;
					    	rateMovementTo.setOperationCount(countOperationByKey);
					    	listDetailRateMovementTo.add(detailRateMovementTo);
		    				
					    	movementAmountTemp=sResults[4]==null?null:new BigDecimal(sResults[4].toString());
					    	BigDecimal amountTemp = BillingUtils.addTwoDecimal(movementAmountTemp, rateMovementTo.getGrossAmount(), GeneralConstants.ROUNDING_DIGIT_NUMBER) ;					    	
					    	rateMovementTo.setGrossAmount(amountTemp);
					    	
					    	
					    	BigDecimal amount=BillingUtils.addTwoDecimal(detailRateMovementTo.getAmount(), rateMovementTo.getDirtyAmount(), GeneralConstants.ROUNDING_DIGIT_NUMBER);
						    rateMovementTo.setDirtyAmount(amount);				    		
					    	rateAmountByRate=BillingUtils.addDecimalObject(rateAmountByRate, sResults[4]==null?null:new BigDecimal(sResults[4].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
	    				}
	    				
			    	
			    	 
			     }else if(serviceRateTo.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode()) &&
			    		 serviceRateTo.getIndScaleMovement().equals(PropertiesConstants.YES_VALUE_INTEGER)){
			    		BigDecimal ratePercent=sResults[19]==null?null:new BigDecimal(sResults[19].toString());
			    		if(!keyDetailRateMovement.equals(keyDetailRateMovementTemp)){
		    				keyDetailRateMovementTemp=keyDetailRateMovement;
		    				listDetailRateMovementTo= new ArrayList<DetailRateMovementTo>();
		    				rateMovementTo=new RateMovementTo();
		    				countOperationByKey=0;
		    				
		    				/**Detail movement*/
		    				detailRateMovementTo=loadDetailRateMovement(detailRateMovementTo,sResults);
		    				
					    	detailRateMovementTo.setAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
					    	detailRateMovementTo.setMovementQuantity(sResults[16]==null?new Long(0):Long.parseLong(sResults[16].toString()));
					    	detailRateMovementTo.setOperationPrice(sResults[17]==null?null:new BigDecimal(sResults[17].toString()));
					    	
					    	temporal  = BillingUtils.multiplyDecimalLong(detailRateMovementTo.getOperationPrice() ,detailRateMovementTo.getMovementQuantity()  , GeneralConstants.ROUNDING_DIGIT_NUMBER);
					    	amountOperation = BillingUtils.multiplyDecimals(ratePercent, temporal, GeneralConstants.ROUNDING_DIGIT_NUMBER);
					    	detailRateMovementTo.setOperationAmount(amountOperation);
					    	
					    	listDetailRateMovementTo.add(detailRateMovementTo);
				    		 
					    	rateMovementTo.setRatePercent(BillingUtils.multiplyDecimalInteger(ratePercent, 100, GeneralConstants.ROUNDING_DIGIT_NUMBER)); //5
					    	rateMovementTo.setDescriptionMovementType(sResults[13]==null?"":sResults[13].toString());
				    		rateMovementTo.setSecurityCode(sResults[3]==null?"":sResults[3].toString());
					    	
					    	rateMovementTo.setMovementQuantity(sResults[16]==null?new Long(0):Long.parseLong(sResults[16].toString()));
					
					    	rateMovementTo.setDescriptionCurrencyRate(sResults[15]==null?"":sResults[15].toString());
					    	//
					    	rateMovementTo.setGrossAmount(sResults[5]==null?null:new BigDecimal(sResults[5].toString()));
					    	//
					    	rateMovementTo.setDirtyAmount(sResults[20]==null?null:new BigDecimal(sResults[20].toString()));
					    	rateMovementTo.setListDetailRateMovementTo(listDetailRateMovementTo);
					    	
					    	countOperationByKey++;
					    	rateMovementTo.setOperationCount(countOperationByKey);
					    	
					    	listRateMovementTo.add(rateMovementTo);
					    	serviceRateTo.setListRateMovementTo(listRateMovementTo);
					    	rateAmountByRate=BillingUtils.addDecimalObject(rateAmountByRate, sResults[5]==null?null:new BigDecimal(sResults[5].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
				    	
			    		}
			    		else{
			    			/**MORE Detail movement*/
			    			detailRateMovementTo=loadDetailRateMovement(detailRateMovementTo,sResults);
				    	
					    	detailRateMovementTo.setAmount(sResults[20]==null?null:new BigDecimal(sResults[20].toString()));//20
					    	detailRateMovementTo.setMovementQuantity(sResults[16]==null?new Long(0):Long.parseLong(sResults[16].toString()));
					    	detailRateMovementTo.setOperationPrice(sResults[17]==null?null:new BigDecimal(sResults[17].toString()));
					    	
					    	temporal  = BillingUtils.multiplyDecimalLong(detailRateMovementTo.getOperationPrice() ,detailRateMovementTo.getMovementQuantity()  , GeneralConstants.ROUNDING_DIGIT_NUMBER);
					    	amountOperation = BillingUtils.multiplyDecimals(ratePercent, temporal, GeneralConstants.ROUNDING_DIGIT_NUMBER);
					    	detailRateMovementTo.setOperationAmount(amountOperation);
					    	
					    	listDetailRateMovementTo.add(detailRateMovementTo);

						    	Long movementQuantity=detailRateMovementTo.getMovementQuantity()+rateMovementTo.getMovementQuantity();

						    	rateMovementTo.setMovementQuantity(movementQuantity);

					    	BigDecimal amount=BillingUtils.addTwoDecimal(detailRateMovementTo.getAmount(), rateMovementTo.getDirtyAmount(), GeneralConstants.ROUNDING_DIGIT_NUMBER);
					    	rateMovementTo.setDirtyAmount(amount);
					    	rateMovementTo.setListDetailRateMovementTo(listDetailRateMovementTo);
					    	
					    	countOperationByKey++;
					    	rateMovementTo.setOperationCount(countOperationByKey);
					    	rateAmountByRate=BillingUtils.addDecimalObject(rateAmountByRate, sResults[5]==null?null:new BigDecimal(sResults[5].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
			    	}
			     
			    	 
			    	 
			     }else if(serviceRateTo.getRateType().equals(RateType.ESCALONADO_MIXTO.getCode()) &&
			    		 serviceRateTo.getIndScaleMovement().equals(PropertiesConstants.YES_VALUE_INTEGER)){

	    				if(!keyDetailRateMovement.equals(keyDetailRateMovementTemp)){
		    				keyDetailRateMovementTemp=keyDetailRateMovement;
		    				listDetailRateMovementTo= new ArrayList<DetailRateMovementTo>();
		    				rateMovementTo=new RateMovementTo();
		    				countOperationByKey=0;
		    				
		    				/**Detail movement*/
		    				detailRateMovementTo=loadDetailRateMovement(detailRateMovementTo,sResults);
		    				
					    	detailRateMovementTo.setAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
					    	detailRateMovementTo.setMovementQuantity(sResults[16]==null?new Long(0):Long.parseLong(sResults[16].toString()));
					    	listDetailRateMovementTo.add(detailRateMovementTo);
		    				
				    		rateMovementTo.setDescriptionMovementType(sResults[13]==null?"":sResults[13].toString());
				    		rateMovementTo.setSecurityCode(sResults[3]==null?"":sResults[3].toString());
					    	
					    	rateMovementTo.setDescriptionCurrencyRate(sResults[15]==null?"":sResults[15].toString());
					    	rateMovementTo.setRateAmount(sResults[18]==null?null:new BigDecimal(sResults[18].toString()));
					    	rateMovementTo.setGrossAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));	
					    	
					    	rateMovementTo.setListDetailRateMovementTo(listDetailRateMovementTo);
					    	
					    	listRateMovementTo.add(rateMovementTo);
					    	serviceRateTo.setListRateMovementTo(listRateMovementTo);
					    	rateAmountByRate=BillingUtils.addDecimalObject(rateAmountByRate, sResults[4]==null?null:new BigDecimal(sResults[4].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
					    	
					    	countOperationByKey++;
					    	rateMovementTo.setOperationCount(countOperationByKey);
					    	
			    		}
	    				else{
	    					/** MORE Detail movement*/
	    					detailRateMovementTo=loadDetailRateMovement(detailRateMovementTo,sResults);
					    	
					    	detailRateMovementTo.setAmount(sResults[20]==null?null:new BigDecimal(sResults[20].toString()));//20
					    	detailRateMovementTo.setMovementQuantity(sResults[16]==null?new Long(0):Long.parseLong(sResults[16].toString()));
					    	
					    	countOperationByKey++;
					    	rateMovementTo.setOperationCount(countOperationByKey);
					    	listDetailRateMovementTo.add(detailRateMovementTo);
		    				
					    	movementAmountTemp=sResults[4]==null?null:new BigDecimal(sResults[4].toString());
					    	BigDecimal amountTemp = BillingUtils.addTwoDecimal(movementAmountTemp, rateMovementTo.getGrossAmount(), GeneralConstants.ROUNDING_DIGIT_NUMBER) ;					    	
					    	rateMovementTo.setGrossAmount(amountTemp);
					    	
					    	
					    	BigDecimal amount=BillingUtils.addTwoDecimal(detailRateMovementTo.getAmount(), rateMovementTo.getDirtyAmount(), GeneralConstants.ROUNDING_DIGIT_NUMBER);
						    rateMovementTo.setDirtyAmount(amount);				    		
					    	rateAmountByRate=BillingUtils.addDecimalObject(rateAmountByRate, sResults[4]==null?null:new BigDecimal(sResults[4].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
	    				}
	    				
			    	
			    	 
			     }else if(serviceRateTo.getRateType().equals(RateType.PORCENTUAL.getCode())){
//			    		rateAmountByRate=sResults[5]==null?null:new BigDecimal(sResults[5].toString());
			    		rateAmountByRate=BillingUtils.addDecimalObject(rateAmountByRate, sResults[5]==null?null:new BigDecimal(sResults[5].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);	
			    	
		    	}
		    	else if(serviceRateTo.getRateType().equals(RateType.FIJO.getCode())){
		    		rateAmountByRate=BillingUtils.addDecimalObject(rateAmountByRate, sResults[4]==null?null:new BigDecimal(sResults[4].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
//		    		rateAmountByRate=sResults[4]==null?null:new BigDecimal(sResults[4].toString());
		    	}

			    	countOperationByRate++;			    	
			    	serviceRateTo.setOperationCount(countOperationByRate);
			    				    	
			    	serviceRateTo.setAmountRate(rateAmountByRate);
		    	}
	    		
	    		/**Otra tarifa*/
	    		else{
	    		
	    			countOperationByRate=0;
	    			countOperationByKey=0;
	    			rateAmountByRate=new BigDecimal(0);
	    		
	    			detailRateMovementTo=new DetailRateMovementTo();
	    			serviceRateTo=new ServiceRateTo();
	    			serviceRateTo.setIdServiceRatePk(sResults[8]==null?Long.parseLong("0"):Long.parseLong(sResults[8].toString()));
			    	serviceRateTo.setRateCode(sResults[9]==null?0:Integer.parseInt(sResults[9].toString()));
			    	serviceRateTo.setRateName(sResults[10]==null?"":sResults[10].toString());
			    	serviceRateTo.setRateType(sResults[11]==null?0:Integer.parseInt(sResults[11].toString()));
			    	serviceRateTo.setDescriptionType(sResults[12]==null?"":sResults[12].toString());
			    	serviceRateTo.setCurrencyRate(sResults[29]==null?0:Integer.parseInt(sResults[29].toString()));
			    	serviceRateTo.setIndScaleMovement(sResults[30]==null?0:Integer.parseInt(sResults[30].toString()));
			    	serviceRateTo.setDescriptionCurrency(filter.getDescriptionCurrencyType());
			    	
			    	if(serviceRateTo.getRateType().equals(RateType.TIPO_MOVIMIENTO_FIJO.getCode())){
			    	
			    		if(!keyDetailRateMovement.equals(keyDetailRateMovementTemp)){
		    				keyDetailRateMovementTemp=keyDetailRateMovement;
		    				listDetailRateMovementTo= new ArrayList<DetailRateMovementTo>();
		    				rateMovementTo=new RateMovementTo();
		    				
		    				/**Detail movement*/
		    				detailRateMovementTo=loadDetailRateMovement(detailRateMovementTo,sResults);
		    				
					    	detailRateMovementTo.setAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
					    	detailRateMovementTo.setMovementQuantity(sResults[16]==null?new Long(0):Long.parseLong(sResults[16].toString()));
					    	listDetailRateMovementTo.add(detailRateMovementTo);
				    		 
				    		rateMovementTo.setDescriptionCurrencyRate(sResults[15]==null?"":sResults[15].toString());
				    		rateMovementTo.setDescriptionMovementType(sResults[13]==null?"":sResults[13].toString());
				    		rateMovementTo.setSecurityCode(sResults[3]==null?"":sResults[3].toString());
					    	
					    	rateMovementTo.setRateAmount(sResults[18]==null?null:new BigDecimal(sResults[18].toString()));
					    	
					    	rateMovementTo.setGrossAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
					    	rateMovementTo.setListDetailRateMovementTo(listDetailRateMovementTo);					    	
					    	
					    	
					    	
					    	countOperationByKey++;
					    	rateMovementTo.setOperationCount(countOperationByKey);					    	
					    	listRateMovementTo.add(rateMovementTo);
					    	serviceRateTo.setListRateMovementTo(listRateMovementTo);
					    	
					    	rateAmountByRate=BillingUtils.addDecimalObject(rateAmountByRate, sResults[4]==null?null:new BigDecimal(sResults[4].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
				    
			    		}
			    		else{
			    		/**Detail movement*/
		    			detailRateMovementTo=loadDetailRateMovement(detailRateMovementTo,sResults);
		    			
				    	detailRateMovementTo.setAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
				    	detailRateMovementTo.setMovementQuantity(sResults[16]==null?new Long(0):Long.parseLong(sResults[16].toString()));
				    	listDetailRateMovementTo.add(detailRateMovementTo);
			    		 
			    		rateMovementTo.setDescriptionCurrencyRate(sResults[15]==null?"":sResults[15].toString());
			    		rateMovementTo.setDescriptionMovementType(sResults[13]==null?"":sResults[13].toString());
			    		rateMovementTo.setSecurityCode(sResults[3]==null?"":sResults[3].toString());
			    		
				    	rateMovementTo.setRateAmount(sResults[18]==null?null:new BigDecimal(sResults[18].toString()));
				    	
				    	movementAmountTemp=sResults[4]==null?null:new BigDecimal(sResults[4].toString());
				    	BigDecimal amountTemp = BillingUtils.addTwoDecimal(movementAmountTemp, rateMovementTo.getGrossAmount(), GeneralConstants.ROUNDING_DIGIT_NUMBER) ;					    	
				    	rateMovementTo.setGrossAmount(amountTemp);
				    	
				    	//rateMovementTo.setGrossAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
				    	countOperationByKey++;
				    	rateMovementTo.setOperationCount(countOperationByKey);
				    	rateMovementTo.setListDetailRateMovementTo(listDetailRateMovementTo);
				    	
				    	listRateMovementTo.add(rateMovementTo);
				    	rateAmountByRate=BillingUtils.addDecimalObject(rateAmountByRate, sResults[4]==null?null:new BigDecimal(sResults[4].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
			    	  }
			    	}
			    	else if(serviceRateTo.getRateType().equals(RateType.TIPO_MOVIMIENTO_PORCENTUAL.getCode())){
			    		
			    		BigDecimal ratePercent=sResults[19]==null?null:new BigDecimal(sResults[19].toString());
			    		if(!keyDetailRateMovement.equals(keyDetailRateMovementTemp)){
		    				keyDetailRateMovementTemp=keyDetailRateMovement;
		    				listDetailRateMovementTo= new ArrayList<DetailRateMovementTo>();
		    				rateMovementTo=new RateMovementTo();
		    				
		    				
		    				/**Detail movement*/
		    				detailRateMovementTo=loadDetailRateMovement(detailRateMovementTo,sResults);
		    				
					    	detailRateMovementTo.setAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
					    	detailRateMovementTo.setOperationPrice(sResults[17]==null?null:new BigDecimal(sResults[17].toString()));
					    	
					    	temporal  = BillingUtils.multiplyDecimalLong(detailRateMovementTo.getOperationPrice() ,detailRateMovementTo.getMovementQuantity()  , GeneralConstants.ROUNDING_DIGIT_NUMBER);
					    	amountOperation = BillingUtils.multiplyDecimals(ratePercent, temporal, GeneralConstants.ROUNDING_DIGIT_NUMBER);
					    	detailRateMovementTo.setOperationAmount(amountOperation);
					    	
					    	listDetailRateMovementTo.add(detailRateMovementTo);
				    		
					    	 
					    	rateMovementTo.setRatePercent(BillingUtils.multiplyDecimalInteger(ratePercent, 100, GeneralConstants.ROUNDING_DIGIT_NUMBER)); //5
					    	rateMovementTo.setDescriptionMovementType(sResults[13]==null?"":sResults[13].toString());
				    		rateMovementTo.setSecurityCode(sResults[3]==null?"":sResults[3].toString());
					    	
					    	rateMovementTo.setMovementQuantity(sResults[16]==null?new Long(0):Long.parseLong(sResults[16].toString()));
					    	rateMovementTo.setDescriptionCurrencyRate(sResults[15]==null?"":sResults[15].toString());
					    	rateMovementTo.setGrossAmount(sResults[5]==null?null:new BigDecimal(sResults[5].toString()));

					    	rateMovementTo.setDirtyAmount(sResults[20]==null?null:new BigDecimal(sResults[20].toString()));
					    	
					    	rateMovementTo.setListDetailRateMovementTo(listDetailRateMovementTo);
					    	
					    	countOperationByKey++;
					    	rateMovementTo.setOperationCount(countOperationByKey);	
					    	
					    	listRateMovementTo.add(rateMovementTo);
					    	serviceRateTo.setListRateMovementTo(listRateMovementTo);
					    	
					    	rateAmountByRate=BillingUtils.addDecimalObject(rateAmountByRate, sResults[5]==null?null:new BigDecimal(sResults[5].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
			    		
			    		}
			    		else{
			    		/**Detail movement*/
			    		
		    			detailRateMovementTo=loadDetailRateMovement(detailRateMovementTo,sResults);
			    			
				    	detailRateMovementTo.setAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
				    	detailRateMovementTo.setOperationPrice(sResults[17]==null?null:new BigDecimal(sResults[17].toString()));
				    	
				    	temporal  = BillingUtils.multiplyDecimalLong(detailRateMovementTo.getOperationPrice() ,detailRateMovementTo.getMovementQuantity()  , GeneralConstants.ROUNDING_DIGIT_NUMBER);
				    	amountOperation = BillingUtils.multiplyDecimals(ratePercent, temporal, GeneralConstants.ROUNDING_DIGIT_NUMBER);
				    	detailRateMovementTo.setOperationAmount(amountOperation);
				    	
				    	listDetailRateMovementTo.add(detailRateMovementTo);
			    		
				    	rateMovementTo.setRatePercent(BillingUtils.multiplyDecimalInteger(ratePercent, 100, GeneralConstants.ROUNDING_DIGIT_NUMBER)); //5
				    	rateMovementTo.setDescriptionMovementType(sResults[13]==null?"":sResults[13].toString());
			    		rateMovementTo.setSecurityCode(sResults[3]==null?"":sResults[3].toString());
				    
				    	rateMovementTo.setMovementQuantity(sResults[16]==null?new Long(0):Long.parseLong(sResults[16].toString()));
				    	rateMovementTo.setDescriptionCurrencyRate(sResults[15]==null?"":sResults[15].toString());
				    	rateMovementTo.setGrossAmount(sResults[5]==null?null:new BigDecimal(sResults[5].toString()));
				    	rateMovementTo.setDirtyAmount(sResults[20]==null?null:new BigDecimal(sResults[20].toString()));
				    	rateMovementTo.setListDetailRateMovementTo(listDetailRateMovementTo);
				    	rateAmountByRate=BillingUtils.addDecimalObject(rateAmountByRate, sResults[5]==null?null:new BigDecimal(sResults[5].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
				    	
				    	countOperationByKey++;
				    	rateMovementTo.setOperationCount(countOperationByKey);
				    	
			    	}
			    	}
			    	else if(serviceRateTo.getRateType().equals(RateType.FIJO.getCode())){
			    		rateAmountByRate=BillingUtils.addDecimalObject(rateAmountByRate, sResults[4]==null?null:new BigDecimal(sResults[4].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
			    	}
			    	else if(serviceRateTo.getRateType().equals(RateType.PORCENTUAL.getCode())){
			    		rateAmountByRate=BillingUtils.addDecimalObject(rateAmountByRate, sResults[5]==null?null:new BigDecimal(sResults[5].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
			    	}
			    	else if(serviceRateTo.getRateType().equals(RateType.ESCALONADO_FIJO.getCode())){
			    		 /**Obtener los movimientos**/
			    		if(serviceRateTo.getIndScaleMovement().equals(PropertiesConstants.YES_VALUE_INTEGER)){

					    	
				    		if(!keyDetailRateMovement.equals(keyDetailRateMovementTemp)){
			    				keyDetailRateMovementTemp=keyDetailRateMovement;
			    				listDetailRateMovementTo= new ArrayList<DetailRateMovementTo>();
			    				rateMovementTo=new RateMovementTo();
			    				
			    				/**Detail movement*/
			    				detailRateMovementTo=loadDetailRateMovement(detailRateMovementTo,sResults);
			    				
						    	detailRateMovementTo.setAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
						    	detailRateMovementTo.setMovementQuantity(sResults[16]==null?new Long(0):Long.parseLong(sResults[16].toString()));
						    	listDetailRateMovementTo.add(detailRateMovementTo);
					    		 
					    		rateMovementTo.setDescriptionCurrencyRate(sResults[15]==null?"":sResults[15].toString());
					    		rateMovementTo.setDescriptionMovementType(sResults[13]==null?"":sResults[13].toString());
					    		rateMovementTo.setSecurityCode(sResults[3]==null?"":sResults[3].toString());
						    	
						    	rateMovementTo.setRateAmount(sResults[18]==null?null:new BigDecimal(sResults[18].toString()));
						    	
						    	rateMovementTo.setGrossAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
						    	rateMovementTo.setListDetailRateMovementTo(listDetailRateMovementTo);					    	
						    	
						    	
						    	
						    	countOperationByKey++;
						    	rateMovementTo.setOperationCount(countOperationByKey);					    	
						    	listRateMovementTo.add(rateMovementTo);
						    	serviceRateTo.setListRateMovementTo(listRateMovementTo);
						    	
						    	rateAmountByRate=BillingUtils.addDecimalObject(rateAmountByRate, sResults[4]==null?null:new BigDecimal(sResults[4].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
					    
				    		}
				    		else{
				    		/**Detail movement*/
			    			detailRateMovementTo=loadDetailRateMovement(detailRateMovementTo,sResults);
			    			
					    	detailRateMovementTo.setAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
					    	detailRateMovementTo.setMovementQuantity(sResults[16]==null?new Long(0):Long.parseLong(sResults[16].toString()));
					    	listDetailRateMovementTo.add(detailRateMovementTo);
				    		 
				    		rateMovementTo.setDescriptionCurrencyRate(sResults[15]==null?"":sResults[15].toString());
				    		rateMovementTo.setDescriptionMovementType(sResults[13]==null?"":sResults[13].toString());
				    		rateMovementTo.setSecurityCode(sResults[3]==null?"":sResults[3].toString());
				    		
					    	rateMovementTo.setRateAmount(sResults[18]==null?null:new BigDecimal(sResults[18].toString()));
					    	
					    	movementAmountTemp=sResults[4]==null?null:new BigDecimal(sResults[4].toString());
					    	BigDecimal amountTemp = BillingUtils.addTwoDecimal(movementAmountTemp, rateMovementTo.getGrossAmount(), GeneralConstants.ROUNDING_DIGIT_NUMBER) ;					    	
					    	rateMovementTo.setGrossAmount(amountTemp);
					    	
					    	//rateMovementTo.setGrossAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
					    	countOperationByKey++;
					    	rateMovementTo.setOperationCount(countOperationByKey);
					    	rateMovementTo.setListDetailRateMovementTo(listDetailRateMovementTo);
					    	
					    	listRateMovementTo.add(rateMovementTo);
					    	rateAmountByRate=BillingUtils.addDecimalObject(rateAmountByRate, sResults[4]==null?null:new BigDecimal(sResults[4].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
				    	  }
				    	
			    		}

			    		rateAmountByRate=BillingUtils.addDecimalObject(rateAmountByRate, sResults[4]==null?null:new BigDecimal(sResults[4].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
			    	}
			    	else if(serviceRateTo.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode())){
			    		 /**Obtener los movimientos**/
			    		if(serviceRateTo.getIndScaleMovement().equals(PropertiesConstants.YES_VALUE_INTEGER)){

				    		
				    		BigDecimal ratePercent=sResults[19]==null?null:new BigDecimal(sResults[19].toString());
				    		if(!keyDetailRateMovement.equals(keyDetailRateMovementTemp)){
			    				keyDetailRateMovementTemp=keyDetailRateMovement;
			    				listDetailRateMovementTo= new ArrayList<DetailRateMovementTo>();
			    				rateMovementTo=new RateMovementTo();
			    				
			    				
			    				/**Detail movement*/
			    				detailRateMovementTo=loadDetailRateMovement(detailRateMovementTo,sResults);
			    				
						    	detailRateMovementTo.setAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
						    	detailRateMovementTo.setOperationPrice(sResults[17]==null?null:new BigDecimal(sResults[17].toString()));
						    	
						    	temporal  = BillingUtils.multiplyDecimalLong(detailRateMovementTo.getOperationPrice() ,detailRateMovementTo.getMovementQuantity()  , GeneralConstants.ROUNDING_DIGIT_NUMBER);
						    	amountOperation = BillingUtils.multiplyDecimals(ratePercent, temporal, GeneralConstants.ROUNDING_DIGIT_NUMBER);
						    	detailRateMovementTo.setOperationAmount(amountOperation);
						    	
						    	listDetailRateMovementTo.add(detailRateMovementTo);
					    		
						    	 
						    	rateMovementTo.setRatePercent(BillingUtils.multiplyDecimalInteger(ratePercent, 100, GeneralConstants.ROUNDING_DIGIT_NUMBER)); //5
						    	rateMovementTo.setDescriptionMovementType(sResults[13]==null?"":sResults[13].toString());
					    		rateMovementTo.setSecurityCode(sResults[3]==null?"":sResults[3].toString());
						    	
						    	rateMovementTo.setMovementQuantity(sResults[16]==null?new Long(0):Long.parseLong(sResults[16].toString()));
						    	rateMovementTo.setDescriptionCurrencyRate(sResults[15]==null?"":sResults[15].toString());
						    	rateMovementTo.setGrossAmount(sResults[5]==null?null:new BigDecimal(sResults[5].toString()));

						    	rateMovementTo.setDirtyAmount(sResults[20]==null?null:new BigDecimal(sResults[20].toString()));
						    	
						    	rateMovementTo.setListDetailRateMovementTo(listDetailRateMovementTo);
						    	
						    	countOperationByKey++;
						    	rateMovementTo.setOperationCount(countOperationByKey);	
						    	
						    	listRateMovementTo.add(rateMovementTo);
						    	serviceRateTo.setListRateMovementTo(listRateMovementTo);
						    	
						    	rateAmountByRate=BillingUtils.addDecimalObject(rateAmountByRate, sResults[5]==null?null:new BigDecimal(sResults[5].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
				    		
				    		}
				    		else{
				    		/**Detail movement*/
				    		
			    			detailRateMovementTo=loadDetailRateMovement(detailRateMovementTo,sResults);
				    			
					    	detailRateMovementTo.setAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
					    	detailRateMovementTo.setOperationPrice(sResults[17]==null?null:new BigDecimal(sResults[17].toString()));
					    	
					    	temporal  = BillingUtils.multiplyDecimalLong(detailRateMovementTo.getOperationPrice() ,detailRateMovementTo.getMovementQuantity()  , GeneralConstants.ROUNDING_DIGIT_NUMBER);
					    	amountOperation = BillingUtils.multiplyDecimals(ratePercent, temporal, GeneralConstants.ROUNDING_DIGIT_NUMBER);
					    	detailRateMovementTo.setOperationAmount(amountOperation);
					    	
					    	listDetailRateMovementTo.add(detailRateMovementTo);
				    		
					    	rateMovementTo.setRatePercent(BillingUtils.multiplyDecimalInteger(ratePercent, 100, GeneralConstants.ROUNDING_DIGIT_NUMBER)); //5
					    	rateMovementTo.setDescriptionMovementType(sResults[13]==null?"":sResults[13].toString());
				    		rateMovementTo.setSecurityCode(sResults[3]==null?"":sResults[3].toString());
					    
					    	rateMovementTo.setMovementQuantity(sResults[16]==null?new Long(0):Long.parseLong(sResults[16].toString()));
					    	rateMovementTo.setDescriptionCurrencyRate(sResults[15]==null?"":sResults[15].toString());
					    	rateMovementTo.setGrossAmount(sResults[5]==null?null:new BigDecimal(sResults[5].toString()));
					    	rateMovementTo.setDirtyAmount(sResults[20]==null?null:new BigDecimal(sResults[20].toString()));
					    	rateMovementTo.setListDetailRateMovementTo(listDetailRateMovementTo);
					    	rateAmountByRate=BillingUtils.addDecimalObject(rateAmountByRate, sResults[5]==null?null:new BigDecimal(sResults[5].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
					    	
					    	countOperationByKey++;
					    	rateMovementTo.setOperationCount(countOperationByKey);
					    	
				    	}
				    	
			    		}

			    		rateAmountByRate=BillingUtils.addDecimalObject(rateAmountByRate, sResults[5]==null?null:new BigDecimal(sResults[5].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
			    	}else if(serviceRateTo.getRateType().equals(RateType.ESCALONADO_MIXTO.getCode())){
			    		/**Obtener los movimientos**/
			    		if(serviceRateTo.getIndScaleMovement().equals(PropertiesConstants.YES_VALUE_INTEGER)){

					    	
				    		if(!keyDetailRateMovement.equals(keyDetailRateMovementTemp)){
			    				keyDetailRateMovementTemp=keyDetailRateMovement;
			    				listDetailRateMovementTo= new ArrayList<DetailRateMovementTo>();
			    				rateMovementTo=new RateMovementTo();
			    				
			    				/**Detail movement*/
			    				detailRateMovementTo=loadDetailRateMovement(detailRateMovementTo,sResults);
			    				
						    	detailRateMovementTo.setAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
						    	detailRateMovementTo.setMovementQuantity(sResults[16]==null?new Long(0):Long.parseLong(sResults[16].toString()));
						    	listDetailRateMovementTo.add(detailRateMovementTo);
					    		 
					    		rateMovementTo.setDescriptionCurrencyRate(sResults[15]==null?"":sResults[15].toString());
					    		rateMovementTo.setDescriptionMovementType(sResults[13]==null?"":sResults[13].toString());
					    		rateMovementTo.setSecurityCode(sResults[3]==null?"":sResults[3].toString());
						    	
						    	rateMovementTo.setRateAmount(sResults[18]==null?null:new BigDecimal(sResults[18].toString()));
						    	
						    	rateMovementTo.setGrossAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
						    	rateMovementTo.setListDetailRateMovementTo(listDetailRateMovementTo);					    	
						    	
						    	
						    	
						    	countOperationByKey++;
						    	rateMovementTo.setOperationCount(countOperationByKey);					    	
						    	listRateMovementTo.add(rateMovementTo);
						    	serviceRateTo.setListRateMovementTo(listRateMovementTo);
						    	
						    	rateAmountByRate=BillingUtils.addDecimalObject(rateAmountByRate, sResults[4]==null?null:new BigDecimal(sResults[4].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
					    
				    		}
				    		else{
				    		/**Detail movement*/
			    			detailRateMovementTo=loadDetailRateMovement(detailRateMovementTo,sResults);
			    			
					    	detailRateMovementTo.setAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
					    	detailRateMovementTo.setMovementQuantity(sResults[16]==null?new Long(0):Long.parseLong(sResults[16].toString()));
					    	listDetailRateMovementTo.add(detailRateMovementTo);
				    		 
				    		rateMovementTo.setDescriptionCurrencyRate(sResults[15]==null?"":sResults[15].toString());
				    		rateMovementTo.setDescriptionMovementType(sResults[13]==null?"":sResults[13].toString());
				    		rateMovementTo.setSecurityCode(sResults[3]==null?"":sResults[3].toString());
				    		
					    	rateMovementTo.setRateAmount(sResults[18]==null?null:new BigDecimal(sResults[18].toString()));
					    	
					    	movementAmountTemp=sResults[4]==null?null:new BigDecimal(sResults[4].toString());
					    	BigDecimal amountTemp = BillingUtils.addTwoDecimal(movementAmountTemp, rateMovementTo.getGrossAmount(), GeneralConstants.ROUNDING_DIGIT_NUMBER) ;					    	
					    	rateMovementTo.setGrossAmount(amountTemp);
					    	
					    	//rateMovementTo.setGrossAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
					    	countOperationByKey++;
					    	rateMovementTo.setOperationCount(countOperationByKey);
					    	rateMovementTo.setListDetailRateMovementTo(listDetailRateMovementTo);
					    	
					    	listRateMovementTo.add(rateMovementTo);
					    	rateAmountByRate=BillingUtils.addDecimalObject(rateAmountByRate, sResults[4]==null?null:new BigDecimal(sResults[4].toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
				    	  }
				    	
			    		}
			    		
			    		rateAmountByRate=sResults[4]==null?new BigDecimal(sResults[5].toString()):new BigDecimal(sResults[4].toString());
			    	}
			    	
			    	rateAmountByRate=BillingUtils.addDecimalObject(rateAmountByRate, sResults[18]==null?null:new BigDecimal(sResults[18].toString()), BigDecimal.ROUND_HALF_UP);
			    	
			    	countOperationByRate++;
			    	serviceRateTo.setOperationCount(countOperationByRate);
			    	serviceRateTo.setAmountRate(rateAmountByRate);
			    	
			    	serviceRateToList.add(serviceRateTo);
			    	
	    		}
	    		
	    	}
	    	
	    }
	    log.info(":::::::::::::::::::::::: FIN de LA CONSULTA :::::::::::::::::::::::::::::::");
		return serviceRateToList;
	}
	

	/**
	 * <ul>
	 * <li>	Load Detail Rate Movement: </li>
	 * <li>    DescriptionSourceParticipant</li>
	 * <li>    DescriptionTargetParticipant</li>
	 * <li>    DescriptionRnt</li>
	 * <li>    MovementDate</li>
	 * <li>    OperationDate</li>
	 * <li>    OperationNumber</li>
	 * <li>    HolderAccount</li>
	 * <li>    SecurityCode</li>
	 *     </ul>.
	 *
	 * @param detailRateMovementTo the detail rate movement to
	 * @param sResults : Object By Query
	 * @return DetailRateMovementTo
	 */
	public DetailRateMovementTo loadDetailRateMovement(DetailRateMovementTo detailRateMovementTo,Object[] sResults){
		
		detailRateMovementTo.setDescriptionSourceParticipant(sResults[21]==null?"":sResults[21].toString());
		detailRateMovementTo.setDescriptionTargetParticipant(sResults[22]==null?"":sResults[22].toString());
		detailRateMovementTo.setDescriptionRnt(sResults[28].toString().concat(" - ").concat(sResults[23].toString()));
		detailRateMovementTo.setMovementDate(sResults[24]==null?null:(Date) sResults[24]);
		detailRateMovementTo.setOperationDate(sResults[25]==null?null:(Date) sResults[25]);
		detailRateMovementTo.setOperationNumber(sResults[26]==null?0:Integer.parseInt(sResults[26].toString()));
		detailRateMovementTo.setHolderAccount(sResults[27]==null?0:Integer.parseInt(sResults[27].toString()));
		detailRateMovementTo.setSecurityCode(sResults[3]==null?"":sResults[3].toString());
		
		
		return detailRateMovementTo;
	}
	
	/**
	 * Search List CollectionRecordTo For Consult By Entity Collection.
	 *
	 * @param filter the filter
	 * @return List the CollectionRecordTo
	 * @throws ServiceException the service exception
	 */
	public  List<ProcessedServiceTo> getSearchListProcessedServiceToToConsultByEntityCollection(CollectionRecordTo filter)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
	    sbQuery.append(" select PS.CALCULATION_DATE, ");
	    sbQuery.append(" (SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE parameter_table_PK=BS.BASE_COLLECTION ) AS BASE_COLLLECTION,   ");
	    sbQuery.append("  CASE");
	    sbQuery.append("  	WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN nvl((SELECT BUSINESS_NAME FROM ISSUER WHERE ID_ISSUER_PK=cr.ID_ISSUER_FK ),' ') ");
	    sbQuery.append("  	WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN  nvl((SELECT DESCRIPTION from PARTICIPANT WHERE ID_PARTICIPANT_PK=CR.ID_PARTICIPANT_FK ),' ') ");
	    sbQuery.append("  	WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN nvl((SELECT full_name FROM HOLDER WHERE ID_HOLDER_PK=CR.ID_HOLDER_FK  ),' ') ");
	    sbQuery.append("  ");
	    sbQuery.append("  	WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" ");
	    sbQuery.append("   		THEN ");
	    sbQuery.append(" 		NVL((SELECT PARAMETER_NAME FROM PARAMETER_TABLE WHERE parameter_table_PK=CR.DOCUMENT_TYPE ),' ' )|| ' - ' || ");
	    sbQuery.append("  ");
	    sbQuery.append("  		nvl(CR.OTHER_ENTITY_DOCUMENT,' ') || ' - ' || nvl(CR.OTHER_ENTITY_DESCRIPTION,' ')         ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.STOCK_EXCHANGE.getCode()+" THEN NVL(   (SELECT DESCRIPTION FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK=BS.ENTITY_COLLECTION),  ' ') ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.CENTRAL_BANK.getCode()+" THEN NVL(   to_char((SELECT DESCRIPTION FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK=BS.ENTITY_COLLECTION)),  ' ') ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.HACIENDA.getCode()+" THEN NVL(   to_char((SELECT DESCRIPTION FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK=BS.ENTITY_COLLECTION) ),  ' ')   ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.AFP.getCode()+" THEN nvl((SELECT DESCRIPTION from PARTICIPANT WHERE ID_PARTICIPANT_PK=CR.ID_PARTICIPANT_FK ),' ')   ");//afp
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.REGULATOR_ENTITY.getCode()+" THEN NVL(   to_char((SELECT DESCRIPTION FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK=BS.ENTITY_COLLECTION) ),  ' ')   ");

	    sbQuery.append(" END AS ENTITY_COLLECTION, ");
	    sbQuery.append("     CR.COLLECTION_AMOUNT,CR.GROSS_AMOUNT, CR.TAX_APPLIED, ");
	    sbQuery.append("  CASE  WHEN  CR.IND_BILLED=0  THEN 'NO FACTURADO'");	    
	    sbQuery.append("   WHEN  CR.IND_BILLED=1  THEN 'FACTURADO' END  AS STATUS_BILLING , ");
	    sbQuery.append(" cr.id_collection_record_pk,");
	    sbQuery.append(" BS.SERVICE_CODE,  ");
	    sbQuery.append(" BS.SERVICE_NAME, ");
	    sbQuery.append("  (SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE parameter_table_PK=BS.SERVICE_TYPE ) AS SERVICE_TYPE,  ");
	    sbQuery.append("   (SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE parameter_table_PK=BS.CLIENT_SERVICE_TYPE ) AS CLIENT_SERVICE_TYPE, ");
	    sbQuery.append("  BS.ID_BILLING_SERVICE_PK, ");
	    sbQuery.append(" (SELECT DESCRIPTION FROM PARAMETER_TABLE WHERE parameter_table_PK=bs.CURRENCY_CALCULATION ) AS DESC_CURRENCY_BILLED_TYPE,  ");
	    sbQuery.append("  cr.CALCULATION_DATE as CALCULATE_DATE_COLLECTION, ");
	    sbQuery.append(" (select pt.description from parameter_table pt where  bs.COLLECTION_PERIOD=pt.parameter_table_pk) as description_COLLECTION_PERIOD, ");
	    sbQuery.append("  (select pt.description from parameter_table pt where  bs.CALCULATION_PERIOD=pt.parameter_table_pk) as description_CALCULATION_PERIOD ");
	    sbQuery.append("  from  processed_service ps  ");
	    sbQuery.append("  inner join billing_service bs on  ( ps.ID_BILLING_SERVICE_FK = bs.ID_BILLING_SERVICE_PK)");
	    sbQuery.append("  INNER JOIN COLLECTION_RECORD CR ON (PS.ID_PROCESSED_SERVICE_PK= CR.ID_PROCESSED_SERVICE_FK)");
	    sbQuery.append("  WHERE 1=1 ");
	    sbQuery.append("  ");
	    	    	
	    	if(Validations.validateIsNotNull(filter.getProcessedServiceTo().getBillingServiceTo().getEntityCollection())){
	    		Integer entityCollection=filter.getProcessedServiceTo().getBillingServiceTo().getEntityCollection();
	    		sbQuery.append("  and BS.ENTITY_COLLECTION= :billingEntityCollectionPrm");
	    		
	    		if(entityCollection.compareTo(EntityCollectionType.HOLDERS.getCode())==0){
	    			if(Validations.validateIsNotNull(filter.getHolder())&&
	    					Validations.validateIsNotNullAndPositive(filter.getHolder().getIdHolderPk())){
	    				sbQuery.append(" and CR.ID_HOLDER_FK= :entityHolderPrm");
	    			}
	    			
	    		}
	    		else if(entityCollection.compareTo(EntityCollectionType.ISSUERS.getCode())==0){
	    			if(Validations.validateIsNotNull(filter.getIssuer())&&
	    					Validations.validateIsNotNullAndNotEmpty(filter.getIssuer().getIdIssuerPk())){
		    			sbQuery.append(" and cr.ID_ISSUER_FK= :entityIssuerPrm");
	    			}
	    		}
	    		else if(entityCollection.compareTo(EntityCollectionType.PARTICIPANTS.getCode())==0
	    				||entityCollection.compareTo(EntityCollectionType.AFP.getCode())==0){
	    			if(Validations.validateIsNotNull(filter.getParticipant())){
	    				sbQuery.append(" and CR.ID_PARTICIPANT_FK= :entityParticipantPrm");
	    			}
	    			
	    		}
	    		else if(entityCollection.compareTo(EntityCollectionType.OTHERS.getCode())==0){
	    			if(Validations.validateIsNotNullAndNotEmpty(filter.getOtherEntityDocument())){
	    				sbQuery.append(" and CR.OTHER_ENTITY_DOCUMENT= :entityOtherDocumentPrm");
	    			}
	    			if(Validations.validateIsNotNullAndPositive(filter.getDocumentType())){
	    				sbQuery.append(" and CR.DOCUMENT_TYPE= :documentTypePrm");
	    			}
	    		}
	    	}
		
	    if(Validations.validateIsNotNull(filter.getInitialDate())&&Validations.validateIsNotNull(filter.getFinalDate())){
			  sbQuery.append(" and ps.CALCULATION_DATE BETWEEN :initialDatePrm and :finalDatePrm");
		  }
	    sbQuery.append(" order by TO_NUMBER(BS.SERVICE_CODE,9999), ");
	    sbQuery.append("  CASE");
	    sbQuery.append("  	WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.ISSUERS.getCode()+" THEN nvl((SELECT BUSINESS_NAME FROM ISSUER WHERE ID_ISSUER_PK=cr.ID_ISSUER_FK ),' ') ");
	    sbQuery.append("  	WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.PARTICIPANTS.getCode()+" THEN  nvl((SELECT DESCRIPTION from PARTICIPANT WHERE ID_PARTICIPANT_PK=CR.ID_PARTICIPANT_FK ),' ') ");
	    sbQuery.append("  	WHEN bs.ENTITY_COLLECTION="+EntityCollectionType.HOLDERS.getCode()+" THEN nvl((SELECT full_name FROM HOLDER WHERE ID_HOLDER_PK=CR.ID_HOLDER_FK  ),' ') ");
	    sbQuery.append("  ");
	    sbQuery.append("  	WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.OTHERS.getCode()+" ");
	    sbQuery.append("   		THEN ");
	    sbQuery.append(" 		NVL((SELECT PARAMETER_NAME FROM PARAMETER_TABLE WHERE parameter_table_PK=CR.DOCUMENT_TYPE ),' ' )|| ' - ' || ");
	    sbQuery.append("  ");
	    sbQuery.append("  		nvl(CR.OTHER_ENTITY_DOCUMENT,' ') || ' - ' || nvl(CR.OTHER_ENTITY_DESCRIPTION,' ')         ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.STOCK_EXCHANGE.getCode()+" THEN NVL(   (SELECT DESCRIPTION FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK=BS.ENTITY_COLLECTION),  ' ') ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.CENTRAL_BANK.getCode()+" THEN NVL(   to_char((SELECT DESCRIPTION FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK=BS.ENTITY_COLLECTION)),  ' ') ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.HACIENDA.getCode()+" THEN NVL(   to_char((SELECT DESCRIPTION FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK=BS.ENTITY_COLLECTION) ),  ' ')   ");
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.AFP.getCode()+" THEN nvl((SELECT DESCRIPTION from PARTICIPANT WHERE ID_PARTICIPANT_PK=CR.ID_PARTICIPANT_FK ),' ')   ");//afp
	    sbQuery.append("  WHEN BS.ENTITY_COLLECTION="+EntityCollectionType.REGULATOR_ENTITY.getCode()+" THEN NVL(   to_char((SELECT DESCRIPTION FROM PARAMETER_TABLE PT WHERE PT.PARAMETER_TABLE_PK=BS.ENTITY_COLLECTION) ),  ' ')   ");
	    sbQuery.append(" END ");
	    sbQuery.append("   ");
	    Query query = em.createNativeQuery(sbQuery.toString());
	    
	       	if(Validations.validateIsNotNull(filter.getProcessedServiceTo().getBillingServiceTo().getEntityCollection())){
	       		Integer entityCollection=filter.getProcessedServiceTo().getBillingServiceTo().getEntityCollection();
	    		query.setParameter("billingEntityCollectionPrm", entityCollection);
	    	    	
	    		
	    		if(entityCollection.compareTo(EntityCollectionType.HOLDERS.getCode())==0){
	    			if(Validations.validateIsNotNull(filter.getHolder()))
	    			query.setParameter("entityHolderPrm", filter.getHolder().getIdHolderPk());
	    		}
	    		else if(entityCollection.compareTo(EntityCollectionType.ISSUERS.getCode())==0){
	    			if(Validations.validateIsNotNull(filter.getIssuer()))
	    			query.setParameter("entityIssuerPrm", filter.getIssuer().getIdIssuerPk());
	    		}
	    		else if(entityCollection.compareTo(EntityCollectionType.PARTICIPANTS.getCode())==0
	    				||entityCollection.compareTo(EntityCollectionType.AFP.getCode())==0){
	    			if(Validations.validateIsNotNull(filter.getParticipant()))
	    			query.setParameter("entityParticipantPrm", filter.getParticipant().getIdParticipantPk());
	    		}
	    		else if(entityCollection.compareTo(EntityCollectionType.OTHERS.getCode())==0){
	    			if(Validations.validateIsNotNullAndNotEmpty(filter.getOtherEntityDocument())){
	    				query.setParameter("entityOtherDocumentPrm", filter.getOtherEntityDocument());
	    			}
	    			if(Validations.validateIsNotNullAndPositive(filter.getDocumentType())){
	    				query.setParameter("documentTypePrm", filter.getDocumentType());
	    			}
	    			
	    			
	    			
	    		}
	    	}
		
	    if(Validations.validateIsNotNull(filter.getInitialDate())&&Validations.validateIsNotNull(filter.getFinalDate())){
	    	query.setParameter("initialDatePrm", filter.getInitialDate());
			query.setParameter("finalDatePrm", filter.getFinalDate());
		  }
	    List<Object[]>  objectList = query.getResultList();
	    List<ProcessedServiceTo> processedServiceToList=new ArrayList<ProcessedServiceTo>();
	    List<CollectionRecordTo> collectionRecordToList=new ArrayList<CollectionRecordTo>();
	    CollectionRecordTo collectionRecordTo=null;
	    BillingServiceTo billingServiceTo=null;
	    ProcessedServiceTo processedServiceTo=null;
	    Long idBillingServicePk=null;
	    Date dateCollection=null;
	    BigDecimal collectionAmountProcessed=new BigDecimal(0);
		BigDecimal grossAmountProcessed=new BigDecimal(0);
		BigDecimal taxAppliedProcessed=new BigDecimal(0);
	    for(int i=0;i<objectList.size();++i){
	    	
	    	
	    	Object[] sResults = (Object[])objectList.get(i);
	    	if(Validations.validateIsNotNull(sResults[12])){
	    		idBillingServicePk=Long.parseLong(sResults[12].toString());
	    	}
	    	else{
	    		idBillingServicePk=1L;
	    	}
	    	
	    	dateCollection=sResults[0]==null?null:(Date) sResults[0];
	    	if(i==0){
	    		
	    		collectionRecordTo=new CollectionRecordTo();
	    		processedServiceTo=new ProcessedServiceTo();
		    	billingServiceTo=new BillingServiceTo();
		    	processedServiceTo.setCalculationDate(sResults[0]==null?null:(Date) sResults[0]);
		    	/**billingServiceTo*/
		    	billingServiceTo.setDescriptionBaseCollection(sResults[1]==null?"":sResults[1].toString());
		    	billingServiceTo.setServiceCode(sResults[8]==null?"":sResults[8].toString());
		    	billingServiceTo.setServiceName(sResults[9]==null?"":sResults[9].toString());
		    	billingServiceTo.setDescriptionServiceType(sResults[10]==null?"":sResults[10].toString());
		    	billingServiceTo.setDescriptionClientServiceType(sResults[11]==null?"":sResults[11].toString());
		    	billingServiceTo.setIdBillingServicePk(Long.parseLong(sResults[12]==null?"":sResults[12].toString()));
		    	billingServiceTo.setDescriptionCurrency(sResults[13]==null?"":sResults[13].toString());
		    	
		    	billingServiceTo.setDescriptionCollectionPeriod(sResults[15]==null?"":sResults[15].toString());
		    	billingServiceTo.setDescriptionCalculationPeriod(sResults[16]==null?"":sResults[16].toString());
		    	processedServiceTo.setBillingServiceTo(billingServiceTo);
		    	/**CollectionRecordTo*/
		    	collectionRecordTo.setDescriptionEntityCollection(sResults[2]==null?"":sResults[2].toString());
		    	collectionRecordTo.setCollectionAmount(sResults[3]==null?null:new BigDecimal(sResults[3].toString()));
		    	collectionRecordTo.setGrossAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
		    	collectionRecordTo.setTaxApplied(sResults[5]==null?null:new BigDecimal(sResults[5].toString()));
		    	collectionRecordTo.setDescriptionIndBilled(sResults[6]==null?"":sResults[6].toString());
		    	collectionRecordTo.setIdCollectionRecordPk(Long.parseLong(sResults[7]==null?"":sResults[7].toString()));
		    	collectionRecordTo.setDescriptionCurrencyType(sResults[13]==null?"":sResults[13].toString());
		    	collectionRecordTo.setCalculationDate(sResults[14]==null?null:(Date) sResults[14]);
		    	collectionRecordToList.add(collectionRecordTo);
		    	/**Add this processed: amount*/
		    	processedServiceTo.setCollectionRecordsTo(collectionRecordToList);
		    	grossAmountProcessed=BillingUtils.addDecimalObject(grossAmountProcessed, sResults[4], GeneralConstants.ROUNDING_DIGIT_NUMBER);
		    	taxAppliedProcessed=BillingUtils.addDecimalObject(taxAppliedProcessed, sResults[5], GeneralConstants.ROUNDING_DIGIT_NUMBER);
		    	collectionAmountProcessed=BillingUtils.addDecimalObject(collectionAmountProcessed, sResults[3], GeneralConstants.ROUNDING_DIGIT_NUMBER);
		    	processedServiceTo.setGrossAmount(grossAmountProcessed);
		    	processedServiceTo.setTaxApplied(taxAppliedProcessed);
		    	processedServiceTo.setCollectionAmount(collectionAmountProcessed);
		    	processedServiceToList.add(processedServiceTo);
	    	}
	    	/***/
	    	else{
	    		/**si pertenece al mismo processedServiceTo*/
	    		if(idBillingServicePk.compareTo(billingServiceTo.getIdBillingServicePk())==0)
	    		{
	    			collectionRecordTo=new CollectionRecordTo();
	    			collectionRecordTo.setDescriptionEntityCollection(sResults[2]==null?"":sResults[2].toString());
			    	collectionRecordTo.setCollectionAmount(sResults[3]==null?null:new BigDecimal(sResults[3].toString()));
			    	collectionRecordTo.setGrossAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
			    	collectionRecordTo.setTaxApplied(sResults[5]==null?null:new BigDecimal(sResults[5].toString()));
			    	collectionRecordTo.setDescriptionIndBilled(sResults[6]==null?"":sResults[6].toString());
			    	collectionRecordTo.setIdCollectionRecordPk(Long.parseLong(sResults[7]==null?"":sResults[7].toString()));
			    	collectionRecordTo.setDescriptionCurrencyType(sResults[13]==null?"":sResults[13].toString());
			    	collectionRecordTo.setCalculationDate(sResults[14]==null?null:(Date) sResults[14]);
			    	collectionRecordToList.add(collectionRecordTo);
			    	/**Add this processed*/
			    	processedServiceTo.setCollectionRecordsTo(collectionRecordToList);
			    	
			    	grossAmountProcessed=BillingUtils.addDecimalObject(grossAmountProcessed, sResults[4], GeneralConstants.ROUNDING_DIGIT_NUMBER);
			    	taxAppliedProcessed=BillingUtils.addDecimalObject(taxAppliedProcessed, sResults[5], GeneralConstants.ROUNDING_DIGIT_NUMBER);
			    	collectionAmountProcessed=BillingUtils.addDecimalObject(collectionAmountProcessed, sResults[3], GeneralConstants.ROUNDING_DIGIT_NUMBER);
			    	processedServiceTo.setGrossAmount(grossAmountProcessed);
			    	processedServiceTo.setTaxApplied(taxAppliedProcessed);
			    	processedServiceTo.setCollectionAmount(collectionAmountProcessed);
	    		}
	    		else
	    		{
	    			/**Cuando es un nuevo ProcessedServiceTo*/
	    			processedServiceTo=new ProcessedServiceTo();
			    	billingServiceTo=new BillingServiceTo();
			    	collectionRecordTo=new CollectionRecordTo();
			    	grossAmountProcessed=new BigDecimal(0);
			    	collectionAmountProcessed=new BigDecimal(0);
			    	taxAppliedProcessed=new BigDecimal(0);
			    	collectionRecordToList=new ArrayList<CollectionRecordTo>();
			    	processedServiceTo.setCalculationDate(sResults[0]==null?null:(Date) sResults[0]);
			    	/**billingServiceTo*/
			    	billingServiceTo.setDescriptionBaseCollection(sResults[1]==null?"":sResults[1].toString());
			    	billingServiceTo.setServiceCode(sResults[8]==null?"":sResults[8].toString());
			    	billingServiceTo.setServiceName(sResults[9]==null?"":sResults[9].toString());
			    	billingServiceTo.setDescriptionServiceType(sResults[10]==null?"":sResults[10].toString());
			    	billingServiceTo.setDescriptionClientServiceType(sResults[11]==null?"":sResults[11].toString());
			    	billingServiceTo.setIdBillingServicePk(Long.parseLong(sResults[12]==null?"":sResults[12].toString()));
			    	billingServiceTo.setDescriptionCurrency(sResults[13]==null?"":sResults[13].toString());
			    	billingServiceTo.setDescriptionCollectionPeriod(sResults[15]==null?"":sResults[15].toString());
			    	billingServiceTo.setDescriptionCalculationPeriod(sResults[16]==null?"":sResults[16].toString());
			    	processedServiceTo.setBillingServiceTo(billingServiceTo);
			    	/**CollectionRecordTo*/
			    	collectionRecordTo.setDescriptionEntityCollection(sResults[2]==null?"":sResults[2].toString());
			    	collectionRecordTo.setCollectionAmount(sResults[3]==null?null:new BigDecimal(sResults[3].toString()));
			    	collectionRecordTo.setGrossAmount(sResults[4]==null?null:new BigDecimal(sResults[4].toString()));
			    	collectionRecordTo.setTaxApplied(sResults[5]==null?null:new BigDecimal(sResults[5].toString()));
			    	collectionRecordTo.setDescriptionIndBilled(sResults[6]==null?"":sResults[6].toString());
			    	collectionRecordTo.setIdCollectionRecordPk(Long.parseLong(sResults[7]==null?"":sResults[7].toString()));
			    	collectionRecordTo.setDescriptionCurrencyType(sResults[13]==null?"":sResults[13].toString());
			    	collectionRecordTo.setCalculationDate(sResults[14]==null?null:(Date) sResults[14]);
			    	collectionRecordToList.add(collectionRecordTo);
			    	/**Add this processed*/
			    	processedServiceTo.setCollectionRecordsTo(collectionRecordToList);
			    	
			    	grossAmountProcessed=BillingUtils.addDecimalObject(grossAmountProcessed, sResults[4], GeneralConstants.ROUNDING_DIGIT_NUMBER);
			    	taxAppliedProcessed=BillingUtils.addDecimalObject(taxAppliedProcessed, sResults[5], GeneralConstants.ROUNDING_DIGIT_NUMBER);
			    	collectionAmountProcessed=BillingUtils.addDecimalObject(collectionAmountProcessed, sResults[3], GeneralConstants.ROUNDING_DIGIT_NUMBER);
			    	processedServiceTo.setGrossAmount(grossAmountProcessed);
			    	processedServiceTo.setTaxApplied(taxAppliedProcessed);
			    	processedServiceTo.setCollectionAmount(collectionAmountProcessed);
			    	processedServiceToList.add(processedServiceTo);
	    		}
	    		
	    	}
	    	
	    }
		return processedServiceToList;
	}
	
	 /**
 	 * Gets the collection record detail.
 	 *
 	 * @param filter the filter
 	 * @return the collection record detail
 	 * @throws ServiceException the service exception
 	 */
 	public CollectionRecordDetail getCollectionRecordDetail(CollectionRecord  filter)throws ServiceException{
		 StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" Select bs From CollectionRecordDetail sr ");
			sbQuery.append(" Where 1 = 1");
			if(Validations.validateIsNotNullAndNotEmpty(filter.getIdCollectionRecordPk())){
				sbQuery.append(" And sr.collectionRecord = :collectionRecordId");
			}
			
			Query query = em.createQuery(sbQuery.toString());   
			
			if(Validations.validateIsNotNullAndNotEmpty(filter)){
				query.setParameter("collectionRecordId", filter);			
			}
			
			CollectionRecordDetail collectionRecordDetail=new CollectionRecordDetail();
			try {
				collectionRecordDetail = (CollectionRecordDetail) query.getSingleResult();
			} catch (Exception e) {
				e.printStackTrace();

			}
			
			return collectionRecordDetail;
	 }
}
