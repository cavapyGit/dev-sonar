package com.pradera.billing.billingcollection.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.apache.deltaspike.core.api.resourceloader.InjectableResource;
import org.primefaces.event.SelectEvent;

import com.pradera.billing.billing.facade.BillingProcessedServiceFacade;
import com.pradera.billing.billingcollection.facade.BaseCollectionMgmtServiceFacade;
import com.pradera.billing.billingcollection.facade.CollectionServiceMgmtServiceFacade;
import com.pradera.billing.billingcollection.facade.ServiceCalculationServiceFacade;
import com.pradera.billing.billingcollection.service.CollectionServiceMgmtServiceBean;
import com.pradera.billing.billingcollection.service.SenderBillingService;
import com.pradera.billing.billingcollection.to.CollectionRecordTo;
import com.pradera.billing.billingcollection.to.DailyExchangeRateFilter;
import com.pradera.billing.billingcollection.to.ProcessedServiceTo;
import com.pradera.billing.billingservice.facade.BillingServiceMgmtServiceFacade;
import com.pradera.billing.billingservice.to.BillingServiceTo;
import com.pradera.billing.common.facade.BusinessLayerCommonFacade;
import com.pradera.billing.common.utils.BillingUtils;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.view.IssuerHelperBean;
import com.pradera.core.framework.batchprocess.service.BatchServiceBean;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.CollectionRecord;
import com.pradera.model.billing.ProcessedService;
import com.pradera.model.billing.type.EntityCollectionType;
import com.pradera.model.billing.type.ProcessedServiceType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.DailyExchangeRoleType;
import com.pradera.model.generalparameter.type.GeographicLocationStateType;
import com.pradera.model.generalparameter.type.GeographicLocationType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.MonthsOfTheYearType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Project iDepositary. Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CollectionServiceMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class CollectionServiceMgmtBean extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6509733311059672538L;

	/** The user info. */
	@Inject
	UserInfo userInfo;

	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;

	/** The log. */
	@Inject
	private transient PraderaLogger log;

	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade;

	/** The client rest service. */
	@Inject
	ClientRestService clientRestService;

	/** The message configuration. */
	@Inject
	@InjectableResource(location = "Messages_es.properties")
	private Properties messageConfiguration;

	/** The business layer common facade. */
	@EJB
	private BusinessLayerCommonFacade businessLayerCommonFacade;

	/** The base collection mgmt service facade. */
	@EJB
	BaseCollectionMgmtServiceFacade baseCollectionMgmtServiceFacade;

	/** The collection service mgmt service bean. */
	@EJB
	CollectionServiceMgmtServiceBean collectionServiceMgmtServiceBean;

	/** The batch service bean. */
	@EJB
	BatchServiceBean batchServiceBean;

	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;

	/** The billing service mgmt service facade. */
	@EJB
	BillingServiceMgmtServiceFacade billingServiceMgmtServiceFacade;

	/** The billing processed service facade. */
	@EJB
	BillingProcessedServiceFacade billingProcessedServiceFacade;

	/** The collection service mgmt service facade. */
	@EJB
	CollectionServiceMgmtServiceFacade collectionServiceMgmtServiceFacade;

	/** The service calculation service facade. */
	@EJB
	ServiceCalculationServiceFacade serviceCalculationServiceFacade;

	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;

	/** The issuer helper bean. */
	@Inject
	IssuerHelperBean issuerHelperBean;

	/** The sender billing service. */
	@EJB
	private SenderBillingService senderBillingService;

	/** filter for search billing service automatic. */
	private List<ParameterTable> listCalculationPeriod;

	/** The list service type. */
	private List<ParameterTable> listServiceType;

	/** The list collection base. */
	private List<ParameterTable> listCollectionBase;

	/** The list processed state. */
	private List<ParameterTable> listProcessedState;

	/** The source information list. */
	private List<ParameterTable> sourceInformationList;

	/** The list month. */
	private List<MonthsOfTheYearType> listMonth;

	/** Participant list. */
	private List<Participant> listClientParticipant;

	/** parameters for search. */
	private Integer collectionBaseSelected;

	/** The document type selected. */
	private Integer documentTypeSelected;

	/** The service type selected. */
	private Integer serviceTypeSelected;

	/** The disable service type. */
	private Boolean disableServiceType = Boolean.TRUE;

	/** The calculation period selected. */
	private Integer calculationPeriodSelected;

	/** The period date. */
	private Date periodDate = CommonsUtilities.currentDate();

	/** The source type. */
	private Integer sourceType;

	/** The month of the year. */
	private Date monthOfTheYear = CommonsUtilities.currentDate();

	/** The visible month. */
	private Boolean visibleMonth;

	/** The visible poll. */
	private Boolean visiblePoll;

	/** The billing service to. */
	private BillingServiceTo billingServiceTo;

	/** The processed service to. */
	private ProcessedServiceTo processedServiceTo;

	/** The collection record selection. */
	private CollectionRecordTo collectionRecordSelection;

	/** The list billing service to select. */
	private List<BillingServiceTo> listBillingServiceToSelect;

	/** The list processed service to select. */
	private List<ProcessedServiceTo> listProcessedServiceToSelect;

	/** The list processed service to executed. */
	private List<ProcessedServiceTo> listProcessedServiceToExecuted;

	/** The billing service to data model. */
	private GenericDataModel<BillingServiceTo> billingServiceToDataModel;

	/** The processed service to data model. */
	private GenericDataModel<ProcessedServiceTo> processedServiceToDataModel;

	/** The billing service data model. */
	private GenericDataModel<BillingService> billingServiceDataModel;

	/** The processed service data model. */
	private GenericDataModel<ProcessedService> processedServiceDataModel;

	/** The list billing service to. */
	private List<BillingServiceTo> listBillingServiceTo;

	/** The list processed service to. */
	private List<ProcessedServiceTo> listProcessedServiceTo;
	/** * LIST USED FOR TABLE. */
	private List<ProcessedServiceTo> listProcessedServiceToError;

	/** The disable btn modify. */
	private Boolean disableBtnModify;

	/** The disable btn add. */
	private Boolean disableBtnAdd;

	/** The holder account selected. */
	private HolderAccount holderAccountSelected;

	/** The holder selected. */
	private Holder holderSelected;

	/** The issuer. */
	private Issuer issuer;

	/** The collection record data model. */
	private GenericDataModel<CollectionRecordTo> collectionRecordDataModel;

	/** The lst participant document type. */
	private List<ParameterTable> lstDocumentType;
	/** The lst countries. */
	private List<GeographicLocation> lstCountries;
	/** The lst Departament. */
	private List<GeographicLocation> lstDepartament;
	/** The lst provinces. */
	private List<GeographicLocation> lstProvinces;
	/** The lst districts. */
	private List<GeographicLocation> lstDistricts;

	/** variables para ClientService. */
	private Boolean disabledServiceClientSelected;

	/** variables para entityCollection. */
	private Boolean disableAdminECVisible;

	/** The disable stock exchange ec visible. */
	private Boolean disableStockExchangeECVisible;

	/** The disable holder ec visible. */
	private Boolean disableHolderECVisible;

	/** The disable issuer ec visible. */
	private Boolean disableIssuerECVisible;

	/** The disable participant ec visible. */
	private Boolean disableParticipantECVisible;

	/** The disable other ec visible. */
	private Boolean disableOtherECVisible;

	/** The disable collection institution selected. */
	private Boolean disableCollectionInstitutionSelected;

	/** The disable processed state selected. */
	private Boolean disableProcessedStateSelected;

	/** variables. */
	private Boolean disableTaxApplied;

	/** The disable collection amount. */
	private Boolean disableCollectionAmount;

	/** The disable gross amount. */
	private Boolean disableGrossAmount;

	/** The disable general form visible. */
	private Boolean disableGeneralFormVisible;

	/** The processed service. */
	private ProcessedService processedService;

	/** The model collection record. */
	private CollectionRecord modelCollectionRecord;

	/** The processed service selection. */
	private ProcessedService processedServiceSelection;

	/** The service client selected. */
	private Integer serviceClientSelected;

	/** The collection institution selected. */
	private Integer collectionInstitutionSelected;

	/** The processed state selected. */
	private Integer processedStateSelected;

	/** The participant selected. */
	private Long participantSelected;

	/** The validity initial date. */
	private Date validityInitialDate;

	/** The validity final date. */
	private Date validityFinalDate;

	/** The initial date. */
	private Date initialDate = CommonsUtilities.currentDate();

	/** The final date. */
	private Date finalDate = CommonsUtilities.currentDate();

	private Date calculationDate;

	/** The validity final date disable. */
	private Boolean validityFinalDateDisable = Boolean.FALSE;

	/** The validity initial date disable. */
	private Boolean validityInitialDateDisable = Boolean.FALSE;

	/** The billing service. */
	private BillingService billingService;

	/** The billing service search. */
	private BillingService billingServiceSearch;

	/** The service type helper. */
	private Integer serviceTypeHelper;

	/** The state execute. */
	private Integer stateExecute = ProcessedServiceType.PENDIENTE.getCode();

	/** The processed state pending. */
	private Integer processedStatePending = ProcessedServiceType.PENDIENTE.getCode();

	/** The processed state register. */
	private Integer processedStateRegister = ProcessedServiceType.REGISTRADO.getCode();

	/** The processed state no process. */
	private Integer processedStateNoProcess = ProcessedServiceType.NO_PROCESADO.getCode();

	/** The processed state exexute. */
	private Integer processedStateExexute = ProcessedServiceType.EJECUTANDO.getCode();

	/** The processed state process. */
	private Integer processedStateProcess = ProcessedServiceType.PROCESADO.getCode();

	/** The processed state billing. */
	private Integer processedStateBilling = ProcessedServiceType.FACTURADO.getCode();

	/** The processed state error. */
	private Integer processedStateError = ProcessedServiceType.ERROR.getCode();

	/** Tax Applied daily. */
	private BigDecimal taxApplied;

	/** The codigo source. */
	private Integer codigoSource;

	/** The parameters. */
	private Map parameters;

	/**
	 * Gets the billing service to.
	 *
	 * @return the billing service to
	 */
	public BillingServiceTo getBillingServiceTo() {
		return billingServiceTo;
	}

	/**
	 * getTaxAppliedDaily, a single charge.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException {

		if (!FacesContext.getCurrentInstance().isPostback()) {

			this.listBillingServiceToSelect = new ArrayList<BillingServiceTo>();
			this.listProcessedServiceToSelect = new ArrayList<ProcessedServiceTo>();
			log.debug("Carga de filtros de búsquedas : ");
			loadCmbDataSearch();
			loadCmbDataSearchProcessed();

			/** Instancia a BillingService */
			billingService = new BillingService();
			billingServiceSearch = new BillingService();
			serviceTypeHelper = PropertiesConstants.SERVICE_TYPE_MANUAL_PK;
			this.billingService.setServiceType(PropertiesConstants.SERVICE_TYPE_MANUAL_PK);
			this.billingServiceSearch.setServiceType(PropertiesConstants.SERVICE_TYPE_MANUAL_PK);

			parameters = new HashMap<>();

		}

	}

	/**
	 * Load cmb data search processed.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCmbDataSearchProcessed() throws ServiceException {
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		if (Validations.validateListIsNullOrEmpty(listProcessedState)) {
			log.debug("lista de clientes de los servicios de EDV:");
			parameterTableTO.setMasterTableFk(PropertiesConstants.PROC_SERV_STATUS_PK);
			listProcessedState = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			/** By removed status processedService */
			List<ParameterTable> listProcessedStateTemp = BillingUtils.cloneList(this.listProcessedState);
			Collection<ParameterTable> collectioRemove = new ArrayList<ParameterTable>();
			for (ParameterTable parameterTable : listProcessedStateTemp) {
				if (parameterTable.getParameterTablePk().equals(ProcessedServiceType.EJECUTANDO.getCode())) {
					collectioRemove.add(parameterTable);
				} else if (parameterTable.getParameterTablePk().equals(ProcessedServiceType.NO_PROCESADO.getCode())) {
					collectioRemove.add(parameterTable);
				} else if (parameterTable.getParameterTablePk().equals(ProcessedServiceType.ERROR.getCode())) {
					collectioRemove.add(parameterTable);
				}
			}
			listProcessedStateTemp.removeAll(collectioRemove);
			listProcessedState = BillingUtils.cloneList(listProcessedStateTemp);
			listMonth = MonthsOfTheYearType.list;
		}
		disableProcessedStateSelected = Boolean.FALSE;

	}

	/**
	 * Load cmb data search.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCmbDataSearch() throws ServiceException {

		ParameterTableTO parameterTableTO = new ParameterTableTO();
		if (Validations.validateListIsNullOrEmpty(listCalculationPeriod)) {
			log.debug("lista de periodos de calculo :");

			/** Only Daily and Monthly */

			/** Default set Daily **/
			this.calculationPeriodSelected = PropertiesConstants.PERIOD_CALCULATION_DAILY_PK;

			List<ParameterTableTO> listParamaterTableTo = new ArrayList<ParameterTableTO>();

			ParameterTableTO pt_yearly = new ParameterTableTO();
			pt_yearly.setMasterTableFk(PropertiesConstants.PERIOD_CALCULATION_PK);
			pt_yearly.setParameterTablePk(PropertiesConstants.PERIOD_CALCULATION_YEARLY_PK);

			ParameterTableTO pt_event = new ParameterTableTO();
			pt_event.setParameterTablePk(PropertiesConstants.PERIOD_CALCULATION_EVENT_PK);
			listParamaterTableTo.add(pt_yearly);
			listParamaterTableTo.add(pt_event);

			listCalculationPeriod = billingServiceMgmtServiceFacade
					.getListParameterTableExcludePksServiceBean(listParamaterTableTo);
		}

		if (Validations.validateListIsNullOrEmpty(listServiceType)) {
			log.debug("lista de tipos  de servicio :");
			parameterTableTO.setMasterTableFk(PropertiesConstants.SERVICE_TYPE_PK);
			listServiceType = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			this.serviceTypeSelected = PropertiesConstants.SERVICE_TYPE_AUTOMATIC_PK;

		}

		if (Validations.validateListIsNullOrEmpty(listCollectionBase)) {
			log.debug("lista de base de cobro :");

			parameterTableTO.setMasterTableFk(PropertiesConstants.COLLECTION_BASE_PK);

			/** Only getting BASE COLLECTION for BILLING SERVICE AUTOMATIC **/
			parameterTableTO.setElementSubclasification1(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO.setOrderbyParameterTableCd(GeneralConstants.ONE_VALUE_INTEGER);
			listCollectionBase = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}

		if (Validations.validateListIsNullOrEmpty(sourceInformationList)) {
			ParameterTableTO parameterTableToSource = new ParameterTableTO();
			parameterTableToSource.setMasterTableFk(MasterTableType.INFORMATION_SOURCE.getCode());
			sourceInformationList = generalParametersFacade.getListParameterTableServiceBean(parameterTableToSource);

			/** By Default choose BCRD **/
			this.sourceType = sourceInformationList.get(0).getParameterTablePk();
		}
	}

	/**
	 * Clean collection service manual.
	 */
	public void cleanCollectionServiceManual() {
		processedServiceToDataModel = null;
		/** Billing de new */
		billingService = new BillingService();
		this.billingService.setServiceType(PropertiesConstants.SERVICE_TYPE_MANUAL_PK);
		/** Billing de search */
		this.billingServiceSearch = new BillingService();
		this.billingServiceSearch.setServiceType(PropertiesConstants.SERVICE_TYPE_MANUAL_PK);
		this.processedStateSelected = null;
		this.initialDate = CommonsUtilities.currentDate();
		this.finalDate = CommonsUtilities.currentDate();
	}

	/**
	 * Adding services to perform the calculation.
	 *
	 * @param e the e
	 */

	public void checkBillingService(SelectEvent e) {

		ProcessedServiceTo processedServiceTo = (ProcessedServiceTo) e.getObject();
		if (processedServiceTo.getProcessedState().equals(ProcessedServiceType.ERROR.getCode())) {
			Boolean existProcessedServiceTo = Boolean.FALSE;

			existProcessedServiceTo = this.listProcessedServiceToSelect.contains(processedServiceTo);

			if (existProcessedServiceTo) {
				this.listProcessedServiceToSelect.remove(processedServiceTo);
			}

			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_CALCULATION_SERVICE_SELECTED_ERROR));
			JSFUtilities.executeJavascriptFunction("cnfPF('warningDialog').show();");

		}

	}

	/**
	 * Search service billing.
	 */
	@LoggerAuditWeb
	public void searchServiceBilling() {

		ProcessedServiceTo processedServiceTo = new ProcessedServiceTo();

		/*** GETTING FILTERS **/

		if (!Validations.validateIsNotNullAndPositive(this.collectionBaseSelected)) {
			return;
		}

		if (!Validations.validateIsNotNullAndPositive(this.sourceType)) {
			return;
		}

		if (!Validations.validateIsNotNullAndPositive(this.calculationPeriodSelected)) {

			return;

		} else {

			if (this.calculationPeriodSelected.equals(PropertiesConstants.PERIOD_CALCULATION_DAILY_PK)) {
				processedServiceTo.setCalculationDate(this.periodDate);
			} else if (this.calculationPeriodSelected.equals(PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK)) {

				String strDate = CommonsUtilities.convertDateToString(this.initialDate,
						BillingUtils.DATE_PATTERN_yyyy_MM_dd);
				String strYearMonth = BillingUtils.getPatternYearMonth(strDate, BillingUtils.DATE_PATTERN_yyyy_MM_dd);
				processedServiceTo
						.setYearProcess(BillingUtils.getPatternYear(strYearMonth, BillingUtils.DATE_PATTERN_yyyy_MM));
				processedServiceTo
						.setMonthProcess(BillingUtils.getPatternMonth(strYearMonth, BillingUtils.DATE_PATTERN_yyyy_MM));

				Integer maxDayOfMonth = CommonsUtilities.getMaxDateOfMonth(this.monthOfTheYear);
				Date maxDateOfMonth = BillingUtils.truncateDayOfMonth(this.monthOfTheYear);
				maxDateOfMonth = CommonsUtilities.addDaysToDate(maxDateOfMonth, maxDayOfMonth);
				processedServiceTo.setCalculationDate(this.finalDate);

				processedServiceTo.setInitialDate(this.initialDate);
				processedServiceTo.setFinalDate(this.finalDate);
			}

		}

		/** FIND DATA WITH FILTERS ***/

		BillingServiceTo billingServiceToTemp = new BillingServiceTo();
		billingServiceToTemp.setCalculationPeriod(this.calculationPeriodSelected);
		billingServiceToTemp.setBaseCollection(this.collectionBaseSelected);
		billingServiceToTemp.setServiceType(this.serviceTypeSelected); /** DEFAULT AUTOMATIC */
		processedServiceTo.setBillingServiceTo(billingServiceToTemp);
		processedServiceTo.setSourceType(this.sourceType);

		/** LIST OF BILLING SERVICES EXECUTED IN OTHER MOMENT **/

		if (Validations.validateListIsNotNullAndNotEmpty(this.listProcessedServiceToExecuted)) {
			this.listProcessedServiceToExecuted.clear();
		} else {

			this.listProcessedServiceToExecuted = new ArrayList<ProcessedServiceTo>();
		}

		try {

			/** This temporary list services loaded previously executed ***/
			List<ProcessedServiceTo> listProcessedServiceExecutedTemp = new ArrayList<ProcessedServiceTo>();

			this.listProcessedServiceTo = collectionServiceMgmtServiceFacade
					.loadProcessedServiceToForCalculate(processedServiceTo, listProcessedServiceExecutedTemp);

			// agregar cobros q se esten ejecutando a la lista executed

			for (ProcessedServiceTo processedServiceToTemp : listProcessedServiceExecutedTemp) {
				this.listProcessedServiceToExecuted.add(processedServiceToTemp);
			}

		} catch (Exception ex) {

			if (ex instanceof ServiceException) {
				showExceptionMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING, ex.getMessage());

				JSFUtilities.executeJavascriptFunction("cnfPF('warningDialog').show();");

			} else {
				ex.printStackTrace();
				return;
			}

		}

		if (Validations.validateListIsNotNullAndNotEmpty(this.listProcessedServiceTo)) {

			/*** Load processedService with Error ***/
			this.listProcessedServiceToError = this.listProcessedServiceToError == null
					? new ArrayList<ProcessedServiceTo>()
					: this.listProcessedServiceToError;
			this.listProcessedServiceToError.clear();

			List<ProcessedServiceTo> listProcessedServiceTemp = BillingUtils
					.cloneProcessedServiceToList(this.listProcessedServiceTo);

			for (ProcessedServiceTo processedServiceToTemp : listProcessedServiceTemp) {
				if (Validations.validateIsNotNull(processedServiceToTemp.getErrorType())) {
					this.listProcessedServiceToError.add(processedServiceToTemp);
				}
			}

			listProcessedServiceTemp.removeAll(this.listProcessedServiceToError);
			this.listProcessedServiceTo = BillingUtils.cloneProcessedServiceToList(listProcessedServiceTemp);

		}

		showPanelWithBillingServices();
	}

	/**
	 * Show panel with billing services.
	 */
	public void showPanelWithBillingServices() {

		if (Validations.validateListIsNotNullAndNotEmpty(this.listProcessedServiceToError)) {

			String parameters = "";

			ProcessedServiceTo processedServiceTo = this.listProcessedServiceToError.get(0);
			ErrorServiceType errorType = processedServiceTo.getErrorType();

			List<ProcessedServiceTo> listProcessedServiceTemp = BillingUtils
					.cloneProcessedServiceToList(this.listProcessedServiceToError);
			List<ProcessedServiceTo> listProcessedServiceRemove = new ArrayList<ProcessedServiceTo>();

			int i = 0;
			for (ProcessedServiceTo processedServiceToTemp : listProcessedServiceTemp) {
				if (errorType.equals(processedServiceToTemp.getErrorType())) {
					listProcessedServiceRemove.add(processedServiceToTemp);

					if (i > 0) {
						parameters = parameters + ", " + processedServiceToTemp.getBillingServiceTo().getServiceCode();
					} else {
						parameters = parameters + processedServiceToTemp.getBillingServiceTo().getServiceCode();
					}

					i++;
				}
			}

			listProcessedServiceTemp.removeAll(listProcessedServiceRemove);
			this.listProcessedServiceToError = BillingUtils.cloneProcessedServiceToList(listProcessedServiceTemp);

			Object[] objParameters = new Object[1];
			objParameters[0] = parameters;

			showExceptionMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING, null, errorType.getMessage(),
					objParameters);

			JSFUtilities.executeJavascriptFunction("cnfWarningError.show();");

		} else {

			if (Validations.validateIsNull(processedServiceToDataModel)) {

				this.listProcessedServiceTo = this.listProcessedServiceTo == null ? new ArrayList<ProcessedServiceTo>()
						: this.listProcessedServiceTo;
				this.processedServiceToDataModel = new GenericDataModel<ProcessedServiceTo>();
			}

			this.processedServiceToDataModel.setWrappedData(this.listProcessedServiceTo);

		}
	}

	/**
	 * Process automatic execution.
	 */
	@LoggerAuditWeb
	public void processAutomaticExecution() {

		serviceCalculationServiceFacade.executeServiceCalculation(this.listProcessedServiceToSelect);

		/** CLONE LIST FOR UPDATE STATUS ON FRONT END **/
		List<ProcessedServiceTo> listProcessedServiceExecutedTemp = BillingUtils
				.cloneProcessedServiceToList(this.listProcessedServiceToSelect);

		// estos son nuevos cobros , deberian agregarse a la lista executed

		if (Validations.validateListIsNotNullAndNotEmpty(this.listProcessedServiceToExecuted)) {

			Boolean exist = Boolean.TRUE;

			List<ProcessedServiceTo> listProcessedServiceExecutedAux = new ArrayList<ProcessedServiceTo>();
			for (ProcessedServiceTo processedServiceTo : listProcessedServiceExecutedTemp) {
				exist = Boolean.FALSE;
				for (ProcessedServiceTo processedServiceToExecuted : this.listProcessedServiceToExecuted) {

					if (processedServiceTo.getBillingServiceTo().getIdBillingServicePk()
							.equals(processedServiceToExecuted.getBillingServiceTo().getIdBillingServicePk())) {
						exist = Boolean.TRUE;
						break;
					}
				}

				if (!exist) {
					listProcessedServiceExecutedAux.add(processedServiceTo);
				}

			}

			this.listProcessedServiceToExecuted.addAll(listProcessedServiceExecutedAux);

		} else {

			this.listProcessedServiceToExecuted = new ArrayList<ProcessedServiceTo>();

			for (ProcessedServiceTo processedServiceTo : listProcessedServiceExecutedTemp) {
				this.listProcessedServiceToExecuted.add(processedServiceTo);
			}
		}

		/***
		 * Active Poll
		 */
		visiblePoll = Boolean.TRUE;

		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_PROCESSED_SERVICE_SEND_CALCULATION));
		JSFUtilities.executeJavascriptFunction("PF('cnfWarningDialog').show()");

	}

	/**
	 * Fix problem calculation.
	 */
	@LoggerAuditWeb
	public void fixProblemCalculation() {

		try {

			for (ProcessedServiceTo processedServiceTo : listProcessedServiceToSelect) {

				collectionServiceMgmtServiceFacade.fixProblemCalculation(processedServiceTo);

			}

			/***
			 * Active Poll
			 */
			visiblePoll = Boolean.TRUE;

			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_PROCESSED_SERVICE_FIX_CALCULATION));
			JSFUtilities.executeJavascriptFunction("cnfPF('warningDialog').show();");

		} catch (ServiceException e) {

			e.printStackTrace();
		}
	}

	/**
	 * Before fix processed calculation.
	 */
	@LoggerAuditWeb
	public void beforeFixProcessedCalculation() {

		if (Validations.validateListIsNotNullAndNotEmpty(this.listProcessedServiceToSelect)) {

			for (ProcessedServiceTo nextProcessedServiceTo : this.listProcessedServiceToSelect) {

				/**
				 * Solo cuando el estado esta en error o Ejecutando
				 */
				if (nextProcessedServiceTo.getProcessedState().equals(ProcessedServiceType.PENDIENTE.getCode())
						|| nextProcessedServiceTo.getProcessedState().equals(ProcessedServiceType.PROCESADO.getCode())
						|| nextProcessedServiceTo.getProcessedState()
								.equals(ProcessedServiceType.NO_PROCESADO.getCode())
						|| nextProcessedServiceTo.getProcessedState().equals(ProcessedServiceType.REGISTRADO.getCode())
						|| nextProcessedServiceTo.getProcessedState()
								.equals(ProcessedServiceType.FACTURADO.getCode())) {

					showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							PropertiesUtilities.getMessage(PropertiesConstants.WARNING_CALCULATION_SERVICE_NOT_FIX));
					JSFUtilities.executeJavascriptFunction("PF('cnfWarningDialog').show();");

					this.stateExecute = ProcessedServiceType.PENDIENTE.getCode();
					return;
				}

			}
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_CALCULATION_SERVICE_FIX_CALCULATION));
			JSFUtilities.executeJavascriptFunction("PF('cnfwProcessedServiceFix').show();");
		} else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_CALCULATION_SERVICE_NOT_SELECTION));
			JSFUtilities.executeJavascriptFunction("PF('cnfWarningDialog').show();");
		}

		this.stateExecute = ProcessedServiceType.PENDIENTE.getCode();

	}

	/**
	 * Before process automatic execution.
	 */
	@LoggerAuditWeb
	public void beforeProcessAutomaticExecution() {

		// si no esta bloqueado el acceso
		if (this.stateExecute.equals(ProcessedServiceType.PENDIENTE.getCode())) {
			// bloquear acceso si esta preparando servicios para ser lanzados

			this.stateExecute = ProcessedServiceType.EJECUTANDO.getCode();

			if (Validations.validateListIsNotNullAndNotEmpty(this.listProcessedServiceToSelect)) {

				for (ProcessedServiceTo nextProcessedServiceTo : this.listProcessedServiceToSelect) {

					if (ProcessedServiceType.EJECUTANDO.getCode().equals(nextProcessedServiceTo.getProcessedState())) {

						showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
								PropertiesUtilities
										.getMessage(PropertiesConstants.WARNING_CALCULATION_SERVICE_EXECUTING));
						JSFUtilities.executeJavascriptFunction("PF('cnfWarningDialog').show()");

						this.stateExecute = ProcessedServiceType.PENDIENTE.getCode();
						return;
					} else if (ProcessedServiceType.ERROR.getCode()
							.equals(nextProcessedServiceTo.getProcessedState())) {
						showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
								PropertiesUtilities
										.getMessage(PropertiesConstants.WARNING_CALCULATION_SERVICE_SELECTED_ERROR));
						JSFUtilities.executeJavascriptFunction("PF('cnfWarningDialog').show()");

						this.stateExecute = ProcessedServiceType.PENDIENTE.getCode();
						return;
					} else if (ProcessedServiceType.FACTURADO.getCode()
							.equals(nextProcessedServiceTo.getProcessedState())) {
						showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
								PropertiesUtilities
										.getMessage(PropertiesConstants.WARNING_CALCULATION_SERVICE_SELECTED_BILLING));
						JSFUtilities.executeJavascriptFunction("PF('cnfWarningDialog').show()");

						this.stateExecute = ProcessedServiceType.PENDIENTE.getCode();
						return;
					}

				}

				/** IF THERE AREN´T PROCESS EXECUTING AND NOT ERROR **/

				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
						PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_PROCESSED_SERVICE_SEND_CALCULATION));
				JSFUtilities.executeJavascriptFunction("PF('cnfwProcessedServiceSend').show()");

			} else {

				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_CALCULATION_SERVICE_NOT_SELECTION));
				JSFUtilities.executeJavascriptFunction("PF('cnfWarningDialog').show();");
			}

			this.stateExecute = ProcessedServiceType.PENDIENTE.getCode();

		}

	}

	/**
	 * Update process on execution.
	 */
	public void updateProcessOnExecution() {

		log.debug(" REFRESH  ESTATUS PROCESS THAT ARE EXECUTING ");

		/** REFRESH ESTATUS PROCESS THAT ARE EXECUTING **/

		if (Validations.validateListIsNotNullAndNotEmpty(this.listProcessedServiceToExecuted)) {

			List<ProcessedServiceTo> listProcessedServiceToRefresh = null;

			try {

				listProcessedServiceToRefresh = BillingUtils
						.cloneProcessedServiceToList(this.listProcessedServiceToExecuted);

				collectionServiceMgmtServiceFacade
						.refreshStateProcessedServiceToOnCalculation(listProcessedServiceToRefresh);

				for (ProcessedServiceTo processedServiceToExecute : listProcessedServiceToRefresh) {

					for (ProcessedServiceTo processedServiceTo : this.listProcessedServiceTo) {

						if (processedServiceToExecute.getBillingServiceTo().getIdBillingServicePk().toString()
								.equals(processedServiceTo.getBillingServiceTo().getIdBillingServicePk().toString())) {

							if (!processedServiceToExecute.getProcessedState()
									.equals(ProcessedServiceType.PENDIENTE.getCode())) {
								processedServiceTo.setProcessedState(processedServiceToExecute.getProcessedState());
								processedServiceTo.setDescriptionProcessedState(
										processedServiceToExecute.getDescriptionProcessedState());
								processedServiceTo
										.setCountNumCollection(processedServiceToExecute.getCountNumCollection());
								processedServiceTo.setLastModifyDate(processedServiceToExecute.getLastModifyDate());
								processedServiceTo
										.setIdProcessedServicePk(processedServiceToExecute.getIdProcessedServicePk());
								break;
							}
						}
					}
				}

				int cantProcessFinish = 0;
				for (ProcessedServiceTo processedServiceToRefresh : listProcessedServiceToRefresh) {
					for (ProcessedServiceTo processedServiceToExecuted : this.listProcessedServiceToExecuted) {

						if (processedServiceToExecuted.getBillingServiceTo().getIdBillingServicePk().toString().equals(
								processedServiceToRefresh.getBillingServiceTo().getIdBillingServicePk().toString())) {

							if (processedServiceToRefresh.getProcessedState()
									.equals(ProcessedServiceType.ERROR.getCode())
									|| processedServiceToRefresh.getProcessedState()
											.equals(ProcessedServiceType.PROCESADO.getCode())
									|| processedServiceToRefresh.getProcessedState()
											.equals(ProcessedServiceType.NO_PROCESADO.getCode())) {

								processedServiceToExecuted
										.setProcessedState(processedServiceToRefresh.getProcessedState());
								processedServiceToExecuted.setDescriptionProcessedState(
										processedServiceToRefresh.getDescriptionProcessedState());
								cantProcessFinish++;
								break;
							}
						}
					}

				}

				if (cantProcessFinish == listProcessedServiceToRefresh.size()) {
					this.listProcessedServiceToExecuted = null;
				}

			} catch (Exception e) {

				if (!(e instanceof ServiceException)) {
					e.printStackTrace();
					return; /** this exception is catch when the user clean the page **/
				}
			}
		} else {
			visiblePoll = Boolean.FALSE;
		}

	}

	/**
	 * Clean process automatic.
	 */
	public void cleanProcessAutomatic() {

		if (Validations.validateListIsNotNullAndNotEmpty(this.listProcessedServiceTo)) {
			this.listProcessedServiceTo.clear();
			this.processedServiceToDataModel = null;

			if (Validations.validateListIsNotNullAndNotEmpty(this.listProcessedServiceToExecuted)) {
				this.listProcessedServiceToExecuted.clear();
			}

			this.collectionBaseSelected = -1;
			periodDate = CommonsUtilities.currentDate();
		}

	}

	/**
	 * Validation execution.
	 */
	public void validationExecution() {

		if (this.stateExecute.equals(ProcessedServiceType.EJECUTANDO.getCode())) {

			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_EXPIRED));
			JSFUtilities.executeJavascriptFunction("PF('cnfWarningDialog').show();");
		}
	}

	/**
	 * Search list Processed Service.
	 */
	@LoggerAuditWeb
	public void searchCollectionServiceManual() {
		ProcessedServiceTo processedServiceTo = new ProcessedServiceTo();
		BillingServiceTo billingServiceTo = new BillingServiceTo();
		if (Validations.validateIsNotNull(billingServiceSearch)) {
			if (Validations.validateIsNotNullAndNotEmpty(billingServiceSearch.getServiceCode())) {
				billingServiceTo.setIdBillingServicePk(billingServiceSearch.getIdBillingServicePk());

			}
			billingServiceTo.setServiceType(PropertiesConstants.SERVICE_TYPE_MANUAL_PK);
			processedServiceTo.setBillingServiceTo(billingServiceTo);
		}

		if (Validations.validateIsNotNullAndPositive(this.processedStateSelected)) {
			processedServiceTo.setProcessedState(processedStateSelected);/** Los estados */
		}
		processedServiceTo.setInitialDate(initialDate);
		processedServiceTo.setFinalDate(finalDate);
		processedServiceTo.setBillingServiceTo(billingServiceTo);
		try {
			listProcessedServiceToSelect = collectionServiceMgmtServiceFacade
					.getSearchProcessedServiceForCollectionManual(processedServiceTo);
		} catch (ServiceException e) {

			e.printStackTrace();
		}

		processedServiceToDataModel = new GenericDataModel<ProcessedServiceTo>(listProcessedServiceToSelect);

	}

	/**
	 * Disabled buttons search.
	 */
	protected void disabledButtonsSearch() {
		this.setDisableBtnAdd(true);
		this.setDisableBtnModify(true);
	}

	/**
	 * Go to collection.
	 *
	 * @param viewToGo the view to go
	 * @return the string
	 */
	public String goToCollection(String viewToGo) {

		if (viewToGo.equals("editCollectionMgmt")) {
			if (Validations.validateIsNotNull(billingService)) {
				viewToGo = "newCollectionMgmt";
			}
		}
		return viewToGo;
	}

	/**
	 * Load registration page.
	 */
	@LoggerAuditWeb
	public void loadRegistrationPage() {
		modelCollectionRecord = new CollectionRecord();
		documentTypeSelected = new Integer(-1);

		this.finalDate = CommonsUtilities.currentDate();
		this.billingService = new BillingService();
		this.billingService.setServiceType(PropertiesConstants.SERVICE_TYPE_MANUAL_PK);
		this.billingServiceSearch = new BillingService();
		this.billingServiceSearch.setServiceType(PropertiesConstants.SERVICE_TYPE_MANUAL_PK);
		this.issuer = new Issuer();
		this.holderSelected = new Holder();
		this.participantSelected = null;
		serviceClientSelected = null;
		collectionInstitutionSelected = null;
		this.modelCollectionRecord.setGrossAmount(null);
		this.modelCollectionRecord.setCollectionAmount(null);
		this.modelCollectionRecord.setTaxApplied(null);

		setTaxApplied(getTaxAppliedDaily());

		getParticipantListToCombo();
		loadNewCollectionRecord();
		disabledServiceClientSelected = Boolean.TRUE;
		disableCollectionInstitutionSelected = Boolean.TRUE;
		getDocumentTypeCombo();
		log.debug("limpiando contenido ");
	}

	/**
	 * Sets the billing service data model.
	 *
	 * @param billingServiceDataModel the new billing service data model
	 */
	public void setBillingServiceDataModel(GenericDataModel<BillingService> billingServiceDataModel) {
		this.billingServiceDataModel = billingServiceDataModel;
	}

	/**
	 * Before registration listener collection.
	 */
	public void beforeRegistrationListenerCollection() {
		log.debug("beforeRegistrationListener :");
		Boolean flagSave = Boolean.FALSE;
		if (Validations.validateIsNotNull(this.modelCollectionRecord)) {
			if (Validations.validateIsNotNull(this.modelCollectionRecord.getGrossAmount())) {
				if (!this.modelCollectionRecord.getGrossAmount().equals(BigDecimal.ZERO.setScale(2)))

					flagSave = Boolean.TRUE;
			}
		}
		if (flagSave) {
			Object codigo = billingService.getServiceCode();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
					PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_PROCESSED_SERVICE_SAVE, codigo));
			JSFUtilities.showComponent("frmSearchManualCollection:cnfIdProcessedServiceSave");

		}

	}

	/**
	 * Save processed service.
	 */
	@LoggerAuditWeb
	public void saveProcessedService() {
		log.debug("saveProcessedService  :");

		Integer valueEntityCollection = this.billingService.getEntityCollection();
		ProcessedService processedService = new ProcessedService();
		List<CollectionRecord> collectionRecordList = new ArrayList<CollectionRecord>();
		CollectionRecord collectionRecord = new CollectionRecord();
		processedService.setBillingService(this.billingService);
		processedService.setProcessedState(ProcessedServiceType.REGISTRADO.getCode());
		processedService.setOperationDate(CommonsUtilities.currentDate());
		processedService.setCalculationDate(CommonsUtilities.currentDate());
		collectionRecord.setCollectionAmount(this.modelCollectionRecord.getCollectionAmount());
		collectionRecord.setEntityCollectionId(valueEntityCollection);
		collectionRecord.setTaxApplied(this.calculateTaxApplied());
		collectionRecord.setGrossAmount(this.modelCollectionRecord.getGrossAmount());
		collectionRecord.setRecordState(ProcessedServiceType.REGISTRADO.getCode());
		collectionRecord.setCalculationDate(CommonsUtilities.currentDate());//
		collectionRecord.setOperationDate(CommonsUtilities.currentDate());
		collectionRecord.setCurrencyType(billingService.getCurrencyCalculation());
		collectionRecord.setCurrencyBilling(billingService.getCurrencyBilling());

		collectionRecord.setIndBilled(GeneralConstants.ZERO_VALUE_INTEGER);
		if (Validations.validateIsNotNullAndNotEmpty(valueEntityCollection)) {
			if (valueEntityCollection.equals(EntityCollectionType.ISSUERS.getCode())) {
				collectionRecord.setIssuer(issuer);
			} else if (valueEntityCollection.equals(EntityCollectionType.PARTICIPANTS.getCode())) {// participant
				Participant participant = new Participant();
				participant.setIdParticipantPk(participantSelected);
				collectionRecord.setParticipant(participant);

			} else if (valueEntityCollection.equals(EntityCollectionType.HOLDERS.getCode())) {// titular
				collectionRecord.setHolder(holderSelected);

			} else if (valueEntityCollection.equals(EntityCollectionType.OTHERS.getCode())) {// otros
				collectionRecord.setDocumentType(this.modelCollectionRecord.getDocumentType());
				collectionRecord.setOtherEntityDescription(
						this.modelCollectionRecord.getOtherEntityDescription().trim().toUpperCase());
				collectionRecord.setOtherEntityDocument(this.modelCollectionRecord.getOtherEntityDocument());
				collectionRecord.setOtherEntityCountry(this.modelCollectionRecord.getOtherEntityCountry());
				collectionRecord.setOtherEntityDepartment(this.modelCollectionRecord.getOtherEntityDepartment());
				collectionRecord.setOtherEntityProvince(this.modelCollectionRecord.getOtherEntityProvince());
				collectionRecord.setOtherEntityDistrict(this.modelCollectionRecord.getOtherEntityDistrict());
				collectionRecord.setOtherEntityAddress(this.modelCollectionRecord.getOtherEntityAddress());
			} else if (valueEntityCollection.equals(EntityCollectionType.AFP.getCode())) {
				Participant participant = new Participant();
				participant.setIdParticipantPk(participantSelected);
				collectionRecord.setParticipant(participant);
			} else if (valueEntityCollection.equals(EntityCollectionType.CENTRAL_BANK.getCode())) {
				collectionRecord.setOtherEntityDocument(EntityCollectionType.CENTRAL_BANK.getCode().toString());
				collectionRecord.setOtherEntityDescription(EntityCollectionType.CENTRAL_BANK.getValue());
			} else if (valueEntityCollection.equals(EntityCollectionType.HACIENDA.getCode())) {
				collectionRecord.setOtherEntityDocument(EntityCollectionType.HACIENDA.getCode().toString());
				collectionRecord.setOtherEntityDescription(EntityCollectionType.HACIENDA.getValue());
			} else if (valueEntityCollection.equals(EntityCollectionType.STOCK_EXCHANGE.getCode())) {
				collectionRecord.setOtherEntityDocument(EntityCollectionType.STOCK_EXCHANGE.getCode().toString());
				collectionRecord.setOtherEntityDescription(EntityCollectionType.STOCK_EXCHANGE.getValue());
			} else if (valueEntityCollection.equals(EntityCollectionType.REGULATOR_ENTITY.getCode())) {
				collectionRecord.setOtherEntityDocument(EntityCollectionType.REGULATOR_ENTITY.getCode().toString());
				collectionRecord.setOtherEntityDescription(EntityCollectionType.REGULATOR_ENTITY.getValue());
			}

		}
		collectionRecord.setProcessedService(processedService);
		collectionRecordList.add(collectionRecord);
		processedService.setCollectionRecords(collectionRecordList);

		try {
			collectionServiceMgmtServiceFacade.saveProcessedService(processedService);

		} catch (ServiceException e) {
			executeAction();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_ROLL_BACK_BILLING_SERVICE_REGISTER, ""));
			JSFUtilities.executeJavascriptFunction("cnfWCollectionDialogRollBack.show();");
			cleanCollectionServiceManual();
		}

		JSFUtilities.hideComponent("frmSearchManualCollection:cnfIdProcessedServiceSave");
		JSFUtilities.executeJavascriptFunction("PF('cnfwProcessedServiceSave').hide();");
		executeAction();
		Object codigo = billingService.getServiceCode();

		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_PROCESSED_SERVICE_SAVE, codigo));
		JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		cleanRegisterCollection();
	}

	/**
	 * Calculate net mount.
	 */
	public void calculateNetMount() {

		BigDecimal tax = modelCollectionRecord.getTaxApplied();
		BigDecimal gross = modelCollectionRecord.getGrossAmount();
		BigDecimal collectionAmountTemp = null;
		if (Validations.validateIsNotNull(tax) && Validations.validateIsNotNull(gross)) {
			BigDecimal percentage = gross.multiply(tax).divide(new BigDecimal(100), 2);
			collectionAmountTemp = gross.add(percentage);
		}
		this.modelCollectionRecord.setCollectionAmount(collectionAmountTemp);
	}

	/**
	 * Calculate tax applied.
	 *
	 * @return the big decimal
	 */
	public BigDecimal calculateTaxApplied() {
		BigDecimal tax = modelCollectionRecord.getTaxApplied();
		BigDecimal net = modelCollectionRecord.getCollectionAmount();
		BigDecimal percentage = null;
		if (Validations.validateIsNotNull(tax) && Validations.validateIsNotNull(net)) {
			percentage = net.multiply(tax).divide(new BigDecimal(100), 2);
		}
		return percentage;

	}

	/**
	 * Calculate gross amount.
	 */
	public void calculateGrossAmount() {
		if (Validations.validateIsNotNull(modelCollectionRecord.getCollectionAmount())) {
			if (modelCollectionRecord.getCollectionAmount().equals(BigDecimal.ZERO.setScale(2))) {
				modelCollectionRecord.setCollectionAmount(null);
				modelCollectionRecord.setGrossAmount(null);
			} else {
				BigDecimal tax = modelCollectionRecord.getTaxApplied();
				BigDecimal net = modelCollectionRecord.getCollectionAmount();
				BigDecimal netAmountTemp = null;
				if (Validations.validateIsNotNull(tax) && Validations.validateIsNotNull(net)) {
					BigDecimal percentage = net.multiply(tax).divide(new BigDecimal(100), 2);
					netAmountTemp = net.add(percentage);
				}
				this.modelCollectionRecord.setGrossAmount(netAmountTemp);
			}
		} else {
			modelCollectionRecord.setCollectionAmount(null);
			modelCollectionRecord.setGrossAmount(null);
		}
	}

	/**
	 * Update selected combo.
	 *
	 * @param event the event
	 */
	public void updateSelectedCombo(ActionEvent event) {
		Boolean flagClean = Boolean.FALSE;
		if (Validations.validateIsNotNullAndNotEmpty(this.billingService)) {
			Object codigo = billingService.getServiceCode();
			if (Validations.validateIsNotNull(codigo)) {
				if (codigo.equals(GeneralConstants.EMPTY_STRING)) {
					flagClean = Boolean.TRUE;
				} else if (Validations.validateIsNotNull(billingService.getServiceState())) {
					if (billingService.getServiceState().compareTo(PropertiesConstants.SERVICE_STATUS_ACTIVE_PK) == 0) {
						this.updateClientType();
						try {
							this.updateCollectionInstitution();
						} catch (ServiceException e) {
							e.printStackTrace();
						}
					} else {
						billingService = new BillingService();
						executeAction();
						showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
								PropertiesUtilities
										.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_STATUS_ACTIVE, codigo));
						JSFUtilities.executeJavascriptFunction("cnfWCollectionDialog.show();");
						return;
					}
				} else {
					flagClean = Boolean.TRUE;
				}

				if (Validations.validateIsNotNull(billingService.getEntityCollection())) {
					if (billingService.getEntityCollection().compareTo(EntityCollectionType.MANAGER.getCode()) == 0) {
						billingService = new BillingService();
						executeAction();
						showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_ENTITY_ADMIN,
										codigo));
						JSFUtilities.executeJavascriptFunction("cnfWCollectionDialog.show();");
						return;

					}
				} else
					flagClean = Boolean.TRUE;

				/** Update Amount if Change Billing Service */
				this.modelCollectionRecord = new CollectionRecord();
				this.modelCollectionRecord.setGrossAmount(null);
				this.modelCollectionRecord.setCollectionAmount(null);
				/** Update input Tax Applied */
				if (Validations.validateIsNotNull(billingService.getTaxApplied())) {
					if (billingService.getTaxApplied().equals(PropertiesConstants.YES_APPLY_TAXED)) {

						modelCollectionRecord.setTaxApplied(getTaxApplied());
					} else if (billingService.getTaxApplied().equals(PropertiesConstants.NOT_APPLY_TAXED)) {
						modelCollectionRecord.setTaxApplied(BigDecimal.ZERO);
					}
				}

			} else
				flagClean = Boolean.TRUE;

		} else {
			flagClean = Boolean.TRUE;
		}
		if (flagClean) {
			cleanRegisterCollection();
		}
	}

	/**
	 * Acciones con los paneles de tipo de cliente.
	 */
	public void updateClientType() {
		Integer valueTypeClient = this.billingService.getClientServiceType();
		if (Validations.validateIsNotNullAndNotEmpty(valueTypeClient)) {
			serviceClientSelected = valueTypeClient;
		}

	}

	/**
	 * Acciones con los paneles de Entidad de Cobro.
	 *
	 * @throws ServiceException the service exception
	 */
	public void updateCollectionInstitution() throws ServiceException {
		Integer valueEntityCollection = this.billingService.getEntityCollection();
		if (Validations.validateIsNotNullAndNotEmpty(valueEntityCollection)) {
			collectionInstitutionSelected = valueEntityCollection;

			if (valueEntityCollection.equals(EntityCollectionType.ISSUERS.getCode())) {
				disableHolderECVisible = Boolean.FALSE;
				disableIssuerECVisible = Boolean.TRUE;
				disableParticipantECVisible = Boolean.FALSE;
				disableOtherECVisible = Boolean.FALSE;
				disableAdminECVisible = Boolean.FALSE;
				disableStockExchangeECVisible = Boolean.FALSE;
			} else if (valueEntityCollection.equals(EntityCollectionType.PARTICIPANTS.getCode())) {
				disableHolderECVisible = Boolean.FALSE;
				disableIssuerECVisible = Boolean.FALSE;
				disableParticipantECVisible = Boolean.TRUE;
				disableOtherECVisible = Boolean.FALSE;
				disableAdminECVisible = Boolean.FALSE;
				disableStockExchangeECVisible = Boolean.FALSE;
			} else if (valueEntityCollection.equals(EntityCollectionType.HOLDERS.getCode())) {
				disableHolderECVisible = Boolean.TRUE;
				disableIssuerECVisible = Boolean.FALSE;
				disableParticipantECVisible = Boolean.FALSE;
				disableOtherECVisible = Boolean.FALSE;
				disableAdminECVisible = Boolean.FALSE;
				disableStockExchangeECVisible = Boolean.FALSE;
			} else if (valueEntityCollection.equals(EntityCollectionType.OTHERS.getCode())) {
				disableHolderECVisible = Boolean.FALSE;
				disableIssuerECVisible = Boolean.FALSE;
				disableParticipantECVisible = Boolean.FALSE;
				disableOtherECVisible = Boolean.TRUE;
				disableAdminECVisible = Boolean.FALSE;
				disableStockExchangeECVisible = Boolean.FALSE;
				loadCountries();
			} else if (valueEntityCollection.equals(EntityCollectionType.MANAGER.getCode())) {
				disableHolderECVisible = Boolean.FALSE;
				disableIssuerECVisible = Boolean.FALSE;
				disableParticipantECVisible = Boolean.FALSE;
				disableOtherECVisible = Boolean.FALSE;
				disableAdminECVisible = Boolean.TRUE;
				disableStockExchangeECVisible = Boolean.FALSE;
			} else if (valueEntityCollection.equals(EntityCollectionType.STOCK_EXCHANGE.getCode())) {
				disableHolderECVisible = Boolean.FALSE;
				disableIssuerECVisible = Boolean.FALSE;
				disableParticipantECVisible = Boolean.FALSE;
				disableOtherECVisible = Boolean.FALSE;
				disableAdminECVisible = Boolean.FALSE;
				disableStockExchangeECVisible = Boolean.TRUE;
			}
		} else {
			disableHolderECVisible = Boolean.FALSE;
			disableIssuerECVisible = Boolean.FALSE;
			disableParticipantECVisible = Boolean.FALSE;
			disableOtherECVisible = Boolean.FALSE;
			disableAdminECVisible = Boolean.FALSE;
			disableStockExchangeECVisible = Boolean.FALSE;
		}

	}

	/**
	 * Before on save listener.
	 */
	public void beforeOnSaveListener() {
		log.debug("beforeOnSaveListener :");
		executeAction();
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
				PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_SERVICE_RATE_SAVE, ""));
		JSFUtilities.showComponent("frmRateModify:cnfIdRateModify");

	}

	/**
	 * Load new collection record.
	 */
	public void loadNewCollectionRecord() {
		disableAdminECVisible = Boolean.FALSE;
		disableStockExchangeECVisible = Boolean.FALSE;
		disableHolderECVisible = Boolean.FALSE;
		disableIssuerECVisible = Boolean.FALSE;
		disableParticipantECVisible = Boolean.FALSE;
		disableOtherECVisible = Boolean.FALSE;
		disableCollectionInstitutionSelected = Boolean.TRUE;

		disableTaxApplied = Boolean.TRUE;
		disableCollectionAmount = Boolean.FALSE;
		disableGrossAmount = Boolean.TRUE;
		this.setDisableGeneralFormVisible(Boolean.FALSE);
	}

	/**
	 * getParticipantListToCombo.
	 *
	 * @return the participant list to combo
	 */
	public void getParticipantListToCombo() {
		if (Validations.validateListIsNullOrEmpty(listClientParticipant)) {
			Participant participantTemp = new Participant();

			participantTemp.setState(ParticipantStateType.REGISTERED.getCode());
			try {
				listClientParticipant = helperComponentFacade.getLisParticipantServiceFacade(participantTemp);
			} catch (ServiceException e) {

				e.printStackTrace();
			}
		}

	}

	/**
	 * getDocumentTypeCombo.
	 *
	 * @return the document type combo
	 */
	public void getDocumentTypeCombo() {
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());

		// List the document types of Participant
		filterParameterTable.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
		try {
			lstDocumentType = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
		} catch (ServiceException e) {

			e.printStackTrace();
		}

	}

	/**
	 * Gets the issuer helper.
	 *
	 * @param event the event
	 * @return the issuer helper
	 */
	public void getIssuerHelper(ActionEvent event) {
		if (Validations.validateIsNotNullAndNotEmpty(issuerHelperBean)) {
//			this.issuer=issuerHelperBean.getSelected();
		}
	}

	/**
	 * Billing on consult.
	 *
	 * @param event the event
	 */

	public void billingOnConsult(ActionEvent event) {

		if (event != null) {

			ProcessedServiceTo processedServiceToFilter = (ProcessedServiceTo) event.getComponent().getAttributes()
					.get("blockInformation");
			this.processedServiceTo = processedServiceToFilter;
			this.calculationDate = CommonsUtilities.currentDate();
			collectionRecordDataModel = new GenericDataModel<CollectionRecordTo>(
					processedServiceTo.getCollectionRecordsTo());

		}

	}

	/**
	 * Before on cancel collection.
	 *
	 * @param e the e
	 */
	public void beforeOnCancelCollection(ActionEvent e) {

		if (Validations.validateIsNotNull(collectionRecordSelection)) {
			/** collectionRecordSelection contains the state Collection = State Processed */
			Object codigo = this.processedServiceTo.getBillingServiceTo().getServiceCode();
			/** processedServiceTo contains state BillingService **/
			Integer stateService = processedServiceTo.getBillingServiceTo().getStateService();
			if (stateService.equals(PropertiesConstants.SERVICE_STATUS_ACTIVE_PK)) {

				if (collectionRecordSelection.getRecordState().equals(ProcessedServiceType.PROCESADO.getCode())) {
					/** Si el estado esta confirmado */
					executeAction();
					showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							PropertiesUtilities.getMessage(PropertiesConstants.WARNING_PROCESSED_SERVICE_CONFIRM,
									codigo));
					JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
					return;
				} else if (collectionRecordSelection.getRecordState()
						.equals(ProcessedServiceType.FACTURADO.getCode())) {
					executeAction();
					showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							PropertiesUtilities.getMessage(PropertiesConstants.WARNING_PROCESSED_SERVICE_BILLED,
									codigo));
					JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
					return;
				}

			} else {
				/** El servicio de facturacion no es de estado activo */
				executeAction();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_STATUS_ACTIVE,
								codigo));
				JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
				return;
			}
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_PROCESSED_SERVICE_ANNUL, codigo));

			JSFUtilities.showComponent("search_popups:cnfIdServiceCancel");
		} else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.MODIFY_RATE_NO_SELECTED));
			JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
		}

	}

	/**
	 * Before on confirm collection. With Calculation Date Calendar record in
	 * CollectionRecord.CalculationDate and ProcessedService.CalculationDate for
	 * Daily Exchange
	 *
	 * @param e the e
	 */
	public void beforeOnConfirmCollection(ActionEvent e) {

		if (Validations.validateIsNotNull(collectionRecordSelection)) {
			/** collectionRecordSelection contains the state Collection = State Processed */
			Object codigo = this.processedServiceTo.getBillingServiceTo().getServiceCode();
			/** processedServiceTo contains state BillingService **/
			Integer stateService = processedServiceTo.getBillingServiceTo().getStateService();

			if (stateService.equals(PropertiesConstants.SERVICE_STATUS_ACTIVE_PK)) {
				if (collectionRecordSelection.getRecordState().equals(ProcessedServiceType.PROCESADO.getCode())) {
					executeAction();
					showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							PropertiesUtilities.getMessage(PropertiesConstants.WARNING_PROCESSED_SERVICE_CONFIRM,
									codigo));
					JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
					return;
				} else if (collectionRecordSelection.getRecordState()
						.equals(ProcessedServiceType.FACTURADO.getCode())) {
					executeAction();
					showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							PropertiesUtilities.getMessage(PropertiesConstants.WARNING_PROCESSED_SERVICE_BILLED,
									codigo));
					JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
					return;
				}

			} else {
				executeAction();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_STATUS_ACTIVE,
								codigo));
				JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
				return;
			}

			try {
				gettingParameter(processedServiceTo.getBillingServiceTo());
				/** Show Message Confirm for executing action */
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
						PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_PROCESSED_SERVICE_CONFIRM, codigo));
				JSFUtilities.showComponent("search_popups:cnfIdServiceConfirm");
			} catch (ServiceException ex) {
				if (ex instanceof ServiceException) {
					Object[] objParameters = new Object[1];
					objParameters[0] = codigo.toString();

					showExceptionMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING, null, ex.getMessage(),
							objParameters);

					JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");

				} else {
					ex.printStackTrace();
					return;
				}
			}

		} else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.MODIFY_RATE_NO_SELECTED));
			JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
		}
	}

	/**
	 * Cancel ProcessedService.
	 */
	@LoggerAuditWeb
	public void cancelProcessedService() {
		ProcessedService processedServiceCancel = new ProcessedService();
		Long processedServicePk = collectionRecordSelection.getIdProcessedServiceFk();
		try {
			processedServiceCancel = this.collectionServiceMgmtServiceFacade
					.getProcessedServiceByCode(processedServicePk);
			collectionServiceMgmtServiceFacade.cancelProcessedService(processedServiceCancel);
		} catch (ServiceException e) {

			e.printStackTrace();
		}

		hideDialogsFromSearch();
		log.debug(" Se eliminó el  servicio de facturacion manual");
		Object codigo = processedServiceTo.getBillingServiceTo().getServiceCode();
		executeAction();
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_PROCESSED_SERVICE_ANNUL, codigo));
		JSFUtilities.executeJavascriptFunction("cnfWConfirmDialogCancel.show();");

		searchCollectionServiceManual();

	}

	/**
	 * Confirm Processed.
	 */
	@LoggerAuditWeb
	public void confirmProcessedService() {

		Long processedServicePk = collectionRecordSelection.getIdProcessedServiceFk();
		parameters.put(GeneralConstants.CALCULATION_DATE, calculationDate);
		try {
			collectionServiceMgmtServiceFacade.confirmProcessedService(processedServicePk, parameters);
		} catch (ServiceException e) {

			e.printStackTrace();
		}

		hideDialogsFromSearch();
		log.debug(" se actualizo el  servicio de facturacion manual");
		Object codigo = processedServiceTo.getBillingServiceTo().getServiceCode();
		executeAction();
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_PROCESSED_SERVICE_CONFIRM, codigo));
		JSFUtilities.executeJavascriptFunction("cnfWConfirmDialogConfirm.show();");
		searchCollectionServiceManual();

	}

	/**
	 * Method that open dialog to confirm or cancel when this operation go to
	 * register one service.
	 */
	public void hideDialogsFromNew() {
		JSFUtilities.hideComponent("frmSearchManualCollection:cnfIdProcessedServiceSave");
		JSFUtilities.hideGeneralDialogues();
	}

	/**
	 * Hide dialogs from search.
	 */
	public void hideDialogsFromSearch() {
		JSFUtilities.hideComponent("search_popups:cnfIdServiceCancel");
		JSFUtilities.hideComponent("search_popups:cnfIdServiceConfirm");
	}

	/**
	 * Limpia las cajas de NewCollectionManagement.
	 */
	public void cleanRegisterCollection() {

		this.billingService = new BillingService();
		this.billingService.setServiceType(PropertiesConstants.SERVICE_TYPE_MANUAL_PK);
		serviceClientSelected = null;
		collectionInstitutionSelected = null;
		this.participantSelected = null;
		this.modelCollectionRecord = new CollectionRecord();
		this.modelCollectionRecord.setGrossAmount(null);
		this.modelCollectionRecord.setCollectionAmount(null);
		this.issuer = new Issuer();
		this.holderSelected = new Holder();
		modelCollectionRecord.setTaxApplied(BigDecimal.ZERO);
		loadNewCollectionRecord();
		JSFUtilities.resetComponent("frmSearchManualCollection");

	}

	/**
	 * View detail collection.
	 *
	 * @param collectionRecordTo the collection record to
	 */
	public void viewDetailCollection(CollectionRecordTo collectionRecordTo) {
		this.collectionRecordSelection = collectionRecordTo;
		JSFUtilities.executeJavascriptFunction("cnfwFlagConsult.show()");
	}

	/**
	 * Update months of the year.
	 */
	public void updateMonthsOfTheYear() {

		// Clean processedServiceToDataModel
		cleanProcessedServiceToDataModel();

		if (calculationPeriodSelected.equals(PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK)) {
			visibleMonth = Boolean.TRUE;
		} else if (calculationPeriodSelected.equals(PropertiesConstants.PERIOD_CALCULATION_DAILY_PK)) {
			visibleMonth = Boolean.FALSE;
		}
	}

	/**
	 * Metodo que limpia y retorna a search.
	 */
	public void returnToSearchCollection() {
		cleanCollectionServiceManual();
	}

	/**
	 * Gets the tax applied daily.
	 *
	 * @return the tax applied daily
	 */
	public BigDecimal getTaxAppliedDaily() {
		ParameterTable parameterTableTax = billingServiceMgmtServiceFacade
				.getParameterTableById(PropertiesConstants.TAX_PK);
		Double taxed = new Double(GeneralConstants.ZERO_VALUE_STRING);
		if (Validations.validateIsNotNull(parameterTableTax)) {
			if (Validations.validateIsNotNull(parameterTableTax.getDecimalAmount1())) {
				taxed = parameterTableTax.getDecimalAmount1();
			}
		}

		return new BigDecimal(taxed);
	}

	/**
	 * Load countries.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadCountries() throws ServiceException {
		GeographicLocationTO filterCountry = new GeographicLocationTO();
		filterCountry.setGeographicLocationType(GeographicLocationType.COUNTRY.getCode());
		filterCountry.setState(GeographicLocationStateType.REGISTERED.getCode());
		lstCountries = generalParametersFacade.getListGeographicLocationServiceFacade(filterCountry);
	}

	/**
	 * Get List To Department Location.
	 *
	 * @return the lst departament location
	 */
	public void getLstDepartamentLocation() {
		try {

			GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
			geographicLocationTO.setGeographicLocationType(GeographicLocationType.DEPARTMENT.getCode());
			geographicLocationTO.setState(GeographicLocationStateType.REGISTERED.getCode());
			geographicLocationTO.setIdLocationReferenceFk(this.modelCollectionRecord.getOtherEntityCountry());

			lstDepartament = generalParametersFacade.getListGeographicLocationServiceFacade(geographicLocationTO);

			modelCollectionRecord.setOtherEntityDepartment(GeneralConstants.NEGATIVE_ONE_INTEGER);
			modelCollectionRecord.setOtherEntityProvince(GeneralConstants.NEGATIVE_ONE_INTEGER);
			lstProvinces = null;
			modelCollectionRecord.setOtherEntityDistrict(GeneralConstants.NEGATIVE_ONE_INTEGER);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}

	/**
	 * Get List To Province Location.
	 *
	 * @return the lst province location
	 */
	public void getLstProvinceLocation() {
		try {

			GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
			geographicLocationTO.setGeographicLocationType(GeographicLocationType.PROVINCE.getCode());
			geographicLocationTO.setState(GeographicLocationStateType.REGISTERED.getCode());
			geographicLocationTO.setIdLocationReferenceFk(this.modelCollectionRecord.getOtherEntityDepartment());

			lstProvinces = generalParametersFacade.getListGeographicLocationServiceFacade(geographicLocationTO);

			modelCollectionRecord.setOtherEntityProvince(GeneralConstants.NEGATIVE_ONE_INTEGER);
			lstDistricts = null;
			modelCollectionRecord.setOtherEntityDistrict(GeneralConstants.NEGATIVE_ONE_INTEGER);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}

	/**
	 * Change selected province.
	 *
	 * @return the lst districts location
	 */
	@LoggerAuditWeb
	public void getLstDistrictsLocation() {
		try {

			GeographicLocationTO filter = new GeographicLocationTO();
			filter.setGeographicLocationType(GeographicLocationType.DISTRICT.getCode());
			filter.setState(GeographicLocationStateType.REGISTERED.getCode());
			filter.setIdLocationReferenceFk(this.modelCollectionRecord.getOtherEntityProvince());

			lstDistricts = generalParametersFacade.getListGeographicLocationServiceFacade(filter);
			modelCollectionRecord.setOtherEntityDistrict(GeneralConstants.NEGATIVE_ONE_INTEGER);

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * * Apply privileges to Rate Security **.
	 */
	public void loadPrivileges() {

		PrivilegeComponent privilegeComponent = new PrivilegeComponent();

		if (userPrivilege.getUserAcctions().isReject()) {
			privilegeComponent.setBtnRejectView(true);
		}

		if (userPrivilege.getUserAcctions().isConfirm()) {
			privilegeComponent.setBtnConfirmView(true);
		}

		userPrivilege.setPrivilegeComponent(privilegeComponent);

	}

	/**
	 * * Get Information History Entity Collection whit filters DocumentType and
	 * EntityDocument.
	 *
	 * @return the information entity other
	 */
	@LoggerAuditWeb
	public void getInformationEntityOther() {
		CollectionRecordTo filter1 = new CollectionRecordTo();
		CollectionRecordTo result = null;

		if (Validations.validateIsNotNullAndPositive(modelCollectionRecord.getDocumentType())) {
			try {
				filter1.setDocumentType(modelCollectionRecord.getDocumentType());
				filter1.setOtherEntityDocument(modelCollectionRecord.getOtherEntityDocument());
				result = new CollectionRecordTo();
				result = businessLayerCommonFacade.getInformationEntityOther(filter1);

				if (Validations.validateIsNotNull(result)) {
					GeographicLocationTO filter = new GeographicLocationTO();
					filter.setState(GeographicLocationStateType.REGISTERED.getCode());

					/** List The Departament **/
					filter.setGeographicLocationType(GeographicLocationType.DEPARTMENT.getCode());
					filter.setIdLocationReferenceFk(result.getOtherEntityCountry());
					lstDepartament = generalParametersFacade.getListGeographicLocationServiceFacade(filter);

					/** List The Provinces **/
					filter.setGeographicLocationType(GeographicLocationType.PROVINCE.getCode());
					filter.setIdLocationReferenceFk(result.getOtherEntityDepartment());
					lstProvinces = generalParametersFacade.getListGeographicLocationServiceFacade(filter);

					/** List The District **/
					filter.setGeographicLocationType(GeographicLocationType.DISTRICT.getCode());
					filter.setIdLocationReferenceFk(result.getOtherEntityProvince());
					lstDistricts = generalParametersFacade.getListGeographicLocationServiceFacade(filter);

					/** Setting values of CollectionRecord */
					modelCollectionRecord.setOtherEntityDescription(result.getOtherEntityDescription());
					modelCollectionRecord.setOtherEntityCountry(result.getOtherEntityCountry());
					modelCollectionRecord.setOtherEntityDepartment(result.getOtherEntityDepartment());
					modelCollectionRecord.setOtherEntityProvince(result.getOtherEntityProvince());
					modelCollectionRecord.setOtherEntityDistrict(result.getOtherEntityDistrict());
					modelCollectionRecord.setOtherEntityAddress(result.getOtherEntityAddress());

				} else {
					/** Setting values of CollectionRecord */
					modelCollectionRecord.setOtherEntityDescription(null);
					modelCollectionRecord.setOtherEntityCountry(null);
					modelCollectionRecord.setOtherEntityDepartment(null);
					modelCollectionRecord.setOtherEntityProvince(null);
					modelCollectionRecord.setOtherEntityDistrict(null);
					modelCollectionRecord.setOtherEntityAddress(null);
				}
//				if(Validations.validateIsNotNull(result.getOtherEntityCountry())&&
//					Validations.validateIsNotNull(result.getOtherEntityDepartment())&&
//					Validations.validateIsNotNull(result.getOtherEntityProvince())&&
//					Validations.validateIsNotNull(result.getOtherEntityDistrict())){
//					
//						GeographicLocationTO filter = new GeographicLocationTO();
//						filter.setState(GeographicLocationStateType.REGISTERED.getCode());
//						
//						/**List The Departament **/
//						filter.setGeographicLocationType(GeographicLocationType.DEPARTMENT.getCode());			
//						filter.setIdLocationReferenceFk(result.getOtherEntityCountry());
//						lstDepartament = generalParametersFacade.getListGeographicLocationServiceFacade(filter);	
//						
//						/**List The Provinces**/
//						filter.setGeographicLocationType(GeographicLocationType.PROVINCE.getCode());			
//						filter.setIdLocationReferenceFk(result.getOtherEntityDepartment());
//						lstProvinces = generalParametersFacade.getListGeographicLocationServiceFacade(filter);			
//						
//						/**List The District**/
//						filter.setGeographicLocationType(GeographicLocationType.DISTRICT.getCode());			
//						filter.setIdLocationReferenceFk(result.getOtherEntityProvince());
//						lstDistricts = generalParametersFacade.getListGeographicLocationServiceFacade(filter);
//						
//				}

			} catch (ServiceException e) {
				e.printStackTrace();
			}

		} else {
//			executeAction();				
//			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
//					, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_COMPLETE_DATA_EXCEPTION));
//			JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
//			return;
		}

	}

	/**
	 * Check collection record.
	 *
	 * @param e the e
	 */
	public void checkCollectionRecord(SelectEvent e) {

	}

	/**
	 * Clean processed service to data model.
	 */
	public void cleanProcessedServiceToDataModel() {

		if (Validations.validateIsNotNullAndNotEmpty(processedServiceToDataModel)
				&& processedServiceToDataModel.getRowCount() > 0) {

			processedServiceToDataModel = null;
		}
	}

	/**
	 * Gets the visible poll.
	 *
	 * @return the visible poll
	 */
	public Boolean getVisiblePoll() {
		return visiblePoll;
	}

	/**
	 * Sets the visible poll.
	 *
	 * @param visiblePoll the new visible poll
	 */
	public void setVisiblePoll(Boolean visiblePoll) {
		this.visiblePoll = visiblePoll;
	}

	/**
	 * Gets the codigo source.
	 *
	 * @return the codigoSource
	 */
	public Integer getCodigoSource() {
		return codigoSource;
	}

	/**
	 * Sets the codigo source.
	 *
	 * @param codigoSource the codigoSource to set
	 */
	public void setCodigoSource(Integer codigoSource) {
		this.codigoSource = codigoSource;
	}

	/**
	 * Gets the ting parameter.
	 *
	 * @param billingServiceTo the billing service to
	 * @return the ting parameter
	 * @throws ServiceException the service exception
	 */
	public void gettingParameter(BillingServiceTo billingServiceTo) throws ServiceException {

		if (Validations.validateIsNullOrNotPositive(billingServiceTo.getIdBillingServicePk())) {
			throw new ServiceException(ErrorServiceType.BILLING_SERVICE_OTHER_ERROR);
		}
		BillingService billingService = billingServiceMgmtServiceFacade
				.getBillingServiceByPk(billingServiceTo.getIdBillingServicePk());
		parameters.put(GeneralConstants.BILLING_SERVICE, billingService);
		/**
		 * Obtener el tipo de cambio
		 */
		DailyExchangeRateFilter dailyExchangeRateFilter = new DailyExchangeRateFilter();
		dailyExchangeRateFilter.setIdSourceinformation(billingService.getSourceInformation());
		dailyExchangeRateFilter.setInitialDate(calculationDate);
		dailyExchangeRateFilter.setFinalDate(calculationDate);

		Map<Integer, Map> exchangeBilling = new HashMap<Integer, Map>();
		exchangeBilling = businessLayerCommonFacade.chargeExchangeMemoryBuyBs(dailyExchangeRateFilter,
				DailyExchangeRoleType.SELL.getCode());
		parameters.put(GeneralConstants.MATRIX_EXCHANGE_RATE_SELL, exchangeBilling);
	}

	/**
	 * Sets the billing service to.
	 *
	 * @param billingServiceTo the new billing service to
	 */
	public void setBillingServiceTo(BillingServiceTo billingServiceTo) {
		this.billingServiceTo = billingServiceTo;
	}

	/**
	 * Gets the processed service to.
	 *
	 * @return the processed service to
	 */
	public ProcessedServiceTo getProcessedServiceTo() {
		return processedServiceTo;
	}

	/**
	 * Sets the processed service to.
	 *
	 * @param processedServiceTo the new processed service to
	 */
	public void setProcessedServiceTo(ProcessedServiceTo processedServiceTo) {
		this.processedServiceTo = processedServiceTo;
	}

	/**
	 * Gets the list billing service to select.
	 *
	 * @return the list billing service to select
	 */
	public List<BillingServiceTo> getListBillingServiceToSelect() {
		return listBillingServiceToSelect;
	}

	/**
	 * Sets the list billing service to select.
	 *
	 * @param listBillingServiceToSelect the new list billing service to select
	 */
	public void setListBillingServiceToSelect(List<BillingServiceTo> listBillingServiceToSelect) {
		this.listBillingServiceToSelect = listBillingServiceToSelect;
	}

	/**
	 * Gets the billing service to data model.
	 *
	 * @return the billing service to data model
	 */
	public GenericDataModel<BillingServiceTo> getBillingServiceToDataModel() {
		return billingServiceToDataModel;
	}

	/**
	 * Gets the list billing service to.
	 *
	 * @return the list billing service to
	 */
	public List<BillingServiceTo> getListBillingServiceTo() {
		return listBillingServiceTo;
	}

	/**
	 * Sets the list billing service to.
	 *
	 * @param listBillingServiceTo the new list billing service to
	 */
	public void setListBillingServiceTo(List<BillingServiceTo> listBillingServiceTo) {
		this.listBillingServiceTo = listBillingServiceTo;
	}

	/**
	 * Sets the billing service to data model.
	 *
	 * @param billingServiceToDataModel the new billing service to data model
	 */
	public void setBillingServiceToDataModel(GenericDataModel<BillingServiceTo> billingServiceToDataModel) {
		this.billingServiceToDataModel = billingServiceToDataModel;
	}

	/**
	 * Gets the billing service mgmt service facade.
	 *
	 * @return the billing service mgmt service facade
	 */
	public BillingServiceMgmtServiceFacade getBillingServiceMgmtServiceFacade() {
		return billingServiceMgmtServiceFacade;
	}

	/**
	 * Sets the billing service mgmt service facade.
	 *
	 * @param billingServiceMgmtServiceFacade the new billing service mgmt service
	 *                                        facade
	 */
	public void setBillingServiceMgmtServiceFacade(BillingServiceMgmtServiceFacade billingServiceMgmtServiceFacade) {
		this.billingServiceMgmtServiceFacade = billingServiceMgmtServiceFacade;
	}

	/**
	 * Gets the list calculation period.
	 *
	 * @return the list calculation period
	 */
	public List<ParameterTable> getListCalculationPeriod() {
		return listCalculationPeriod;
	}

	/**
	 * Gets the disable service type.
	 *
	 * @return the disable service type
	 */
	public Boolean getDisableServiceType() {
		return disableServiceType;
	}

	/**
	 * Sets the disable service type.
	 *
	 * @param disableServiceType the new disable service type
	 */
	public void setDisableServiceType(Boolean disableServiceType) {
		this.disableServiceType = disableServiceType;
	}

	/**
	 * Gets the period date.
	 *
	 * @return the period date
	 */
	public Date getPeriodDate() {
		return periodDate;
	}

	/**
	 * Sets the period date.
	 *
	 * @param periodDate the new period date
	 */
	public void setPeriodDate(Date periodDate) {
		this.periodDate = periodDate;
	}

	/**
	 * Sets the list calculation period.
	 *
	 * @param listCalculationPeriod the new list calculation period
	 */
	public void setListCalculationPeriod(List<ParameterTable> listCalculationPeriod) {
		this.listCalculationPeriod = listCalculationPeriod;
	}

	/**
	 * Gets the list service type.
	 *
	 * @return the list service type
	 */
	public List<ParameterTable> getListServiceType() {
		return listServiceType;
	}

	/**
	 * Sets the list service type.
	 *
	 * @param listServiceType the new list service type
	 */
	public void setListServiceType(List<ParameterTable> listServiceType) {
		this.listServiceType = listServiceType;
	}

	/**
	 * Gets the list processed service to executed.
	 *
	 * @return the list processed service to executed
	 */
	public List<ProcessedServiceTo> getListProcessedServiceToExecuted() {
		return listProcessedServiceToExecuted;
	}

	/**
	 * Sets the list processed service to executed.
	 *
	 * @param listProcessedServiceToExecuted the new list processed service to
	 *                                       executed
	 */
	public void setListProcessedServiceToExecuted(List<ProcessedServiceTo> listProcessedServiceToExecuted) {
		this.listProcessedServiceToExecuted = listProcessedServiceToExecuted;
	}

	/**
	 * Gets the list processed service to select.
	 *
	 * @return the list processed service to select
	 */
	public List<ProcessedServiceTo> getListProcessedServiceToSelect() {
		return listProcessedServiceToSelect;
	}

	/**
	 * Sets the list processed service to select.
	 *
	 * @param listProcessedServiceToSelect the new list processed service to select
	 */
	public void setListProcessedServiceToSelect(List<ProcessedServiceTo> listProcessedServiceToSelect) {
		this.listProcessedServiceToSelect = listProcessedServiceToSelect;
	}

	/**
	 * Gets the list processed service to.
	 *
	 * @return the list processed service to
	 */
	public List<ProcessedServiceTo> getListProcessedServiceTo() {
		return listProcessedServiceTo;
	}

	/**
	 * Sets the list processed service to.
	 *
	 * @param listProcessedServiceTo the new list processed service to
	 */
	public void setListProcessedServiceTo(List<ProcessedServiceTo> listProcessedServiceTo) {
		this.listProcessedServiceTo = listProcessedServiceTo;
	}

	/**
	 * Gets the list collection base.
	 *
	 * @return the list collection base
	 */
	public List<ParameterTable> getListCollectionBase() {
		return listCollectionBase;
	}

	/**
	 * Sets the list collection base.
	 *
	 * @param listCollectionBase the new list collection base
	 */
	public void setListCollectionBase(List<ParameterTable> listCollectionBase) {
		this.listCollectionBase = listCollectionBase;
	}

	/**
	 * Gets the collection base selected.
	 *
	 * @return the collection base selected
	 */
	public Integer getCollectionBaseSelected() {
		return collectionBaseSelected;
	}

	/**
	 * Sets the collection base selected.
	 *
	 * @param collectionBaseSelected the new collection base selected
	 */
	public void setCollectionBaseSelected(Integer collectionBaseSelected) {
		this.collectionBaseSelected = collectionBaseSelected;
	}

	/**
	 * Gets the service type selected.
	 *
	 * @return the service type selected
	 */
	public Integer getServiceTypeSelected() {
		return serviceTypeSelected;
	}

	/**
	 * Sets the service type selected.
	 *
	 * @param serviceTypeSelected the new service type selected
	 */
	public void setServiceTypeSelected(Integer serviceTypeSelected) {
		this.serviceTypeSelected = serviceTypeSelected;
	}

	/**
	 * Gets the calculation period selected.
	 *
	 * @return the calculation period selected
	 */
	public Integer getCalculationPeriodSelected() {
		return calculationPeriodSelected;
	}

	/**
	 * Sets the calculation period selected.
	 *
	 * @param calculationPeriodSelected the new calculation period selected
	 */
	public void setCalculationPeriodSelected(Integer calculationPeriodSelected) {
		this.calculationPeriodSelected = calculationPeriodSelected;
	}

	/**
	 * Gets the disable general form visible.
	 *
	 * @return the disable general form visible
	 */
	public Boolean getDisableGeneralFormVisible() {
		return disableGeneralFormVisible;
	}

	/**
	 * Sets the disable general form visible.
	 *
	 * @param disableGeneralFormVisible the new disable general form visible
	 */
	public void setDisableGeneralFormVisible(Boolean disableGeneralFormVisible) {
		this.disableGeneralFormVisible = disableGeneralFormVisible;
	}

	/**
	 * Gets the service type helper.
	 *
	 * @return the service type helper
	 */
	public Integer getServiceTypeHelper() {
		return serviceTypeHelper;
	}

	/**
	 * Sets the service type helper.
	 *
	 * @param serviceTypeHelper the new service type helper
	 */
	public void setServiceTypeHelper(Integer serviceTypeHelper) {
		this.serviceTypeHelper = serviceTypeHelper;
	}

	/**
	 * Gets the state execute.
	 *
	 * @return the state execute
	 */
	public Integer getStateExecute() {
		return stateExecute;
	}

	/**
	 * Sets the state execute.
	 *
	 * @param stateExecute the new state execute
	 */
	public void setStateExecute(Integer stateExecute) {
		this.stateExecute = stateExecute;
	}

	/**
	 * Gets the billing service.
	 *
	 * @return the billing service
	 */
	public BillingService getBillingService() {
		return billingService;
	}

	/**
	 * Sets the billing service.
	 *
	 * @param billingService the new billing service
	 */
	public void setBillingService(BillingService billingService) {
		this.billingService = billingService;
	}

	/**
	 * Gets the disable btn modify.
	 *
	 * @return the disable btn modify
	 */
	public Boolean getDisableBtnModify() {
		return disableBtnModify;
	}

	/**
	 * Sets the disable btn modify.
	 *
	 * @param disableBtnModify the new disable btn modify
	 */
	public void setDisableBtnModify(Boolean disableBtnModify) {
		this.disableBtnModify = disableBtnModify;
	}

	/**
	 * Gets the disable btn add.
	 *
	 * @return the disable btn add
	 */
	public Boolean getDisableBtnAdd() {
		return disableBtnAdd;
	}

	/**
	 * Sets the disable btn add.
	 *
	 * @param disableBtnAdd the new disable btn add
	 */
	public void setDisableBtnAdd(Boolean disableBtnAdd) {
		this.disableBtnAdd = disableBtnAdd;
	}

	/**
	 * Gets the billing service data model.
	 *
	 * @return the billing service data model
	 */
	public GenericDataModel<BillingService> getBillingServiceDataModel() {
		return billingServiceDataModel;
	}

	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}

	/**
	 * Sets the issuer.
	 *
	 * @param issuer the issuer to set
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	/**
	 * Gets the processed service.
	 *
	 * @return the processed service
	 */
	public ProcessedService getProcessedService() {
		return processedService;
	}

	/**
	 * Sets the processed service.
	 *
	 * @param processedService the new processed service
	 */
	public void setProcessedService(ProcessedService processedService) {
		this.processedService = processedService;
	}

	/**
	 * Gets the service client selected.
	 *
	 * @return the service client selected
	 */
	public Integer getServiceClientSelected() {
		return serviceClientSelected;
	}

	/**
	 * Sets the service client selected.
	 *
	 * @param serviceClientSelected the new service client selected
	 */
	public void setServiceClientSelected(Integer serviceClientSelected) {
		this.serviceClientSelected = serviceClientSelected;
	}

	/**
	 * Gets the validity initial date.
	 *
	 * @return the validity initial date
	 */
	public Date getValidityInitialDate() {
		return validityInitialDate;
	}

	/**
	 * Sets the validity initial date.
	 *
	 * @param validityInitialDate the new validity initial date
	 */
	public void setValidityInitialDate(Date validityInitialDate) {
		this.validityInitialDate = validityInitialDate;
	}

	/**
	 * Gets the validity final date.
	 *
	 * @return the validity final date
	 */
	public Date getValidityFinalDate() {
		return validityFinalDate;
	}

	/**
	 * Sets the validity final date.
	 *
	 * @param validityFinalDate the new validity final date
	 */
	public void setValidityFinalDate(Date validityFinalDate) {
		this.validityFinalDate = validityFinalDate;
	}

	/**
	 * Gets the validity final date disable.
	 *
	 * @return the validity final date disable
	 */
	public Boolean getValidityFinalDateDisable() {
		return validityFinalDateDisable;
	}

	/**
	 * Sets the validity final date disable.
	 *
	 * @param validityFinalDateDisable the new validity final date disable
	 */
	public void setValidityFinalDateDisable(Boolean validityFinalDateDisable) {
		this.validityFinalDateDisable = validityFinalDateDisable;
	}

	/**
	 * Gets the validity initial date disable.
	 *
	 * @return the validity initial date disable
	 */
	public Boolean getValidityInitialDateDisable() {
		return validityInitialDateDisable;
	}

	/**
	 * Sets the validity initial date disable.
	 *
	 * @param validityInitialDateDisable the new validity initial date disable
	 */
	public void setValidityInitialDateDisable(Boolean validityInitialDateDisable) {
		this.validityInitialDateDisable = validityInitialDateDisable;
	}

	/**
	 * Gets the list client participant.
	 *
	 * @return the list client participant
	 */
	public List<Participant> getListClientParticipant() {
		return listClientParticipant;
	}

	/**
	 * Sets the list client participant.
	 *
	 * @param listClientParticipant the new list client participant
	 */
	public void setListClientParticipant(List<Participant> listClientParticipant) {
		this.listClientParticipant = listClientParticipant;
	}

	/**
	 * Gets the participant selected.
	 *
	 * @return the participant selected
	 */
	public Long getParticipantSelected() {
		return participantSelected;
	}

	/**
	 * Sets the participant selected.
	 *
	 * @param participantSelected the new participant selected
	 */
	public void setParticipantSelected(Long participantSelected) {
		this.participantSelected = participantSelected;
	}

	/**
	 * Gets the model collection record.
	 *
	 * @return the model collection record
	 */
	public CollectionRecord getModelCollectionRecord() {
		return modelCollectionRecord;
	}

	/**
	 * Sets the model collection record.
	 *
	 * @param modelCollectionRecord the new model collection record
	 */
	public void setModelCollectionRecord(CollectionRecord modelCollectionRecord) {
		this.modelCollectionRecord = modelCollectionRecord;
	}

	/**
	 * Gets the month of the year.
	 *
	 * @return the month of the year
	 */
	public Date getMonthOfTheYear() {
		return monthOfTheYear;
	}

	/**
	 * Sets the month of the year.
	 *
	 * @param monthOfTheYear the new month of the year
	 */
	public void setMonthOfTheYear(Date monthOfTheYear) {
		this.monthOfTheYear = monthOfTheYear;
	}

	/**
	 * Gets the collection institution selected.
	 *
	 * @return the collection institution selected
	 */
	public Integer getCollectionInstitutionSelected() {
		return collectionInstitutionSelected;
	}

	/**
	 * Sets the collection institution selected.
	 *
	 * @param collectionInstitutionSelected the new collection institution selected
	 */
	public void setCollectionInstitutionSelected(Integer collectionInstitutionSelected) {
		this.collectionInstitutionSelected = collectionInstitutionSelected;
	}

	/**
	 * Gets the disable collection institution selected.
	 *
	 * @return the disable collection institution selected
	 */
	public Boolean getDisableCollectionInstitutionSelected() {
		return disableCollectionInstitutionSelected;
	}

	/**
	 * Sets the disable collection institution selected.
	 *
	 * @param disableCollectionInstitutionSelected the new disable collection
	 *                                             institution selected
	 */
	public void setDisableCollectionInstitutionSelected(Boolean disableCollectionInstitutionSelected) {
		this.disableCollectionInstitutionSelected = disableCollectionInstitutionSelected;
	}

	/**
	 * Gets the disabled service client selected.
	 *
	 * @return the disabled service client selected
	 */
	public Boolean getDisabledServiceClientSelected() {
		return disabledServiceClientSelected;
	}

	/**
	 * Sets the disabled service client selected.
	 *
	 * @param disabledServiceClientSelected the new disabled service client selected
	 */
	public void setDisabledServiceClientSelected(Boolean disabledServiceClientSelected) {
		this.disabledServiceClientSelected = disabledServiceClientSelected;
	}

	/**
	 * Gets the disable admin ec visible.
	 *
	 * @return the disable admin ec visible
	 */
	public Boolean getDisableAdminECVisible() {
		return disableAdminECVisible;
	}

	/**
	 * Sets the disable admin ec visible.
	 *
	 * @param disableAdminECVisible the new disable admin ec visible
	 */
	public void setDisableAdminECVisible(Boolean disableAdminECVisible) {
		this.disableAdminECVisible = disableAdminECVisible;
	}

	/**
	 * Gets the disable holder ec visible.
	 *
	 * @return the disable holder ec visible
	 */
	public Boolean getDisableHolderECVisible() {
		return disableHolderECVisible;
	}

	/**
	 * Sets the disable holder ec visible.
	 *
	 * @param disableHolderECVisible the new disable holder ec visible
	 */
	public void setDisableHolderECVisible(Boolean disableHolderECVisible) {
		this.disableHolderECVisible = disableHolderECVisible;
	}

	/**
	 * Gets the disable issuer ec visible.
	 *
	 * @return the disable issuer ec visible
	 */
	public Boolean getDisableIssuerECVisible() {
		return disableIssuerECVisible;
	}

	/**
	 * Sets the disable issuer ec visible.
	 *
	 * @param disableIssuerECVisible the new disable issuer ec visible
	 */
	public void setDisableIssuerECVisible(Boolean disableIssuerECVisible) {
		this.disableIssuerECVisible = disableIssuerECVisible;
	}

	/**
	 * Gets the disable participant ec visible.
	 *
	 * @return the disable participant ec visible
	 */
	public Boolean getDisableParticipantECVisible() {
		return disableParticipantECVisible;
	}

	/**
	 * Sets the disable participant ec visible.
	 *
	 * @param disableParticipantECVisible the new disable participant ec visible
	 */
	public void setDisableParticipantECVisible(Boolean disableParticipantECVisible) {
		this.disableParticipantECVisible = disableParticipantECVisible;
	}

	/**
	 * Gets the disable other ec visible.
	 *
	 * @return the disable other ec visible
	 */
	public Boolean getDisableOtherECVisible() {
		return disableOtherECVisible;
	}

	/**
	 * Sets the disable other ec visible.
	 *
	 * @param disableOtherECVisible the new disable other ec visible
	 */
	public void setDisableOtherECVisible(Boolean disableOtherECVisible) {
		this.disableOtherECVisible = disableOtherECVisible;
	}

	/**
	 * Gets the disable stock exchange ec visible.
	 *
	 * @return the disable stock exchange ec visible
	 */
	public Boolean getDisableStockExchangeECVisible() {
		return disableStockExchangeECVisible;
	}

	/**
	 * Sets the disable stock exchange ec visible.
	 *
	 * @param disableStockExchangeECVisible the new disable stock exchange ec
	 *                                      visible
	 */
	public void setDisableStockExchangeECVisible(Boolean disableStockExchangeECVisible) {
		this.disableStockExchangeECVisible = disableStockExchangeECVisible;
	}

	/**
	 * Gets the disable tax applied.
	 *
	 * @return the disable tax applied
	 */
	public Boolean getDisableTaxApplied() {
		return disableTaxApplied;
	}

	/**
	 * Sets the disable tax applied.
	 *
	 * @param disableTaxApplied the new disable tax applied
	 */
	public void setDisableTaxApplied(Boolean disableTaxApplied) {
		this.disableTaxApplied = disableTaxApplied;
	}

	/**
	 * Gets the disable collection amount.
	 *
	 * @return the disable collection amount
	 */
	public Boolean getDisableCollectionAmount() {
		return disableCollectionAmount;
	}

	/**
	 * Sets the disable collection amount.
	 *
	 * @param disableCollectionAmount the new disable collection amount
	 */
	public void setDisableCollectionAmount(Boolean disableCollectionAmount) {
		this.disableCollectionAmount = disableCollectionAmount;
	}

	/**
	 * Gets the billing service search.
	 *
	 * @return the billing service search
	 */
	public BillingService getBillingServiceSearch() {
		return billingServiceSearch;
	}

	/**
	 * Sets the billing service search.
	 *
	 * @param billingServiceSearch the new billing service search
	 */
	public void setBillingServiceSearch(BillingService billingServiceSearch) {
		this.billingServiceSearch = billingServiceSearch;
	}

	/**
	 * Gets the disable gross amount.
	 *
	 * @return the disable gross amount
	 */
	public Boolean getDisableGrossAmount() {
		return disableGrossAmount;
	}

	/**
	 * Sets the disable gross amount.
	 *
	 * @param disableGrossAmount the new disable gross amount
	 */
	public void setDisableGrossAmount(Boolean disableGrossAmount) {
		this.disableGrossAmount = disableGrossAmount;
	}

	/**
	 * Gets the holder account selected.
	 *
	 * @return the holder account selected
	 */
	public HolderAccount getHolderAccountSelected() {
		return holderAccountSelected;
	}

	/**
	 * Sets the holder account selected.
	 *
	 * @param holderAccountSelected the new holder account selected
	 */
	public void setHolderAccountSelected(HolderAccount holderAccountSelected) {
		this.holderAccountSelected = holderAccountSelected;
	}

	/**
	 * Gets the holder selected.
	 *
	 * @return the holder selected
	 */
	public Holder getHolderSelected() {
		return holderSelected;
	}

	/**
	 * Sets the holder selected.
	 *
	 * @param holderSelected the new holder selected
	 */
	public void setHolderSelected(Holder holderSelected) {
		this.holderSelected = holderSelected;
	}

	/**
	 * Gets the processed service to data model.
	 *
	 * @return the processed service to data model
	 */
	public GenericDataModel<ProcessedServiceTo> getProcessedServiceToDataModel() {
		return processedServiceToDataModel;
	}

	/**
	 * Sets the processed service to data model.
	 *
	 * @param processedServiceToDataModel the new processed service to data model
	 */
	public void setProcessedServiceToDataModel(GenericDataModel<ProcessedServiceTo> processedServiceToDataModel) {
		this.processedServiceToDataModel = processedServiceToDataModel;
	}

	/**
	 * Gets the processed service data model.
	 *
	 * @return the processed service data model
	 */
	public GenericDataModel<ProcessedService> getProcessedServiceDataModel() {
		return processedServiceDataModel;
	}

	/**
	 * Sets the processed service data model.
	 *
	 * @param processedServiceDataModel the new processed service data model
	 */
	public void setProcessedServiceDataModel(GenericDataModel<ProcessedService> processedServiceDataModel) {
		this.processedServiceDataModel = processedServiceDataModel;
	}

	/**
	 * Gets the processed service selection.
	 *
	 * @return the processed service selection
	 */
	public ProcessedService getProcessedServiceSelection() {
		return processedServiceSelection;
	}

	/**
	 * Sets the processed service selection.
	 *
	 * @param processedServiceSelection the new processed service selection
	 */
	public void setProcessedServiceSelection(ProcessedService processedServiceSelection) {
		this.processedServiceSelection = processedServiceSelection;
	}

	/**
	 * Gets the list processed state.
	 *
	 * @return the list processed state
	 */
	public List<ParameterTable> getListProcessedState() {
		return listProcessedState;
	}

	/**
	 * Sets the list processed state.
	 *
	 * @param listProcessedState the new list processed state
	 */
	public void setListProcessedState(List<ParameterTable> listProcessedState) {
		this.listProcessedState = listProcessedState;
	}

	/**
	 * Gets the disable processed state selected.
	 *
	 * @return the disable processed state selected
	 */
	public Boolean getDisableProcessedStateSelected() {
		return disableProcessedStateSelected;
	}

	/**
	 * Sets the disable processed state selected.
	 *
	 * @param disableProcessedStateSelected the new disable processed state selected
	 */
	public void setDisableProcessedStateSelected(Boolean disableProcessedStateSelected) {
		this.disableProcessedStateSelected = disableProcessedStateSelected;
	}

	/**
	 * Gets the processed state selected.
	 *
	 * @return the processed state selected
	 */
	public Integer getProcessedStateSelected() {
		return processedStateSelected;
	}

	/**
	 * Sets the processed state selected.
	 *
	 * @param processedStateSelected the new processed state selected
	 */
	public void setProcessedStateSelected(Integer processedStateSelected) {
		this.processedStateSelected = processedStateSelected;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	/**
	 * Gets the processed state pending.
	 *
	 * @return the processed state pending
	 */
	public Integer getProcessedStatePending() {
		return processedStatePending;
	}

	/**
	 * Sets the processed state pending.
	 *
	 * @param processedStatePending the new processed state pending
	 */
	public void setProcessedStatePending(Integer processedStatePending) {
		this.processedStatePending = processedStatePending;
	}

	/**
	 * Gets the processed state billing.
	 *
	 * @return the processed state billing
	 */
	public Integer getProcessedStateBilling() {
		return processedStateBilling;
	}

	/**
	 * Sets the processed state billing.
	 *
	 * @param processedStateBilling the new processed state billing
	 */
	public void setProcessedStateBilling(Integer processedStateBilling) {
		this.processedStateBilling = processedStateBilling;
	}

	/**
	 * Gets the processed state exexute.
	 *
	 * @return the processed state exexute
	 */
	public Integer getProcessedStateExexute() {
		return processedStateExexute;
	}

	/**
	 * Sets the processed state exexute.
	 *
	 * @param processedStateExexute the new processed state exexute
	 */
	public void setProcessedStateExexute(Integer processedStateExexute) {
		this.processedStateExexute = processedStateExexute;
	}

	/**
	 * Gets the processed state error.
	 *
	 * @return the processed state error
	 */
	public Integer getProcessedStateError() {
		return processedStateError;
	}

	/**
	 * Gets the processed state process.
	 *
	 * @return the processed state process
	 */
	public Integer getProcessedStateProcess() {
		return processedStateProcess;
	}

	/**
	 * Sets the processed state process.
	 *
	 * @param processedStateProcess the new processed state process
	 */
	public void setProcessedStateProcess(Integer processedStateProcess) {
		this.processedStateProcess = processedStateProcess;
	}

	/**
	 * Sets the processed state error.
	 *
	 * @param processedStateError the new processed state error
	 */
	public void setProcessedStateError(Integer processedStateError) {
		this.processedStateError = processedStateError;
	}

	/**
	 * Gets the processed state register.
	 *
	 * @return the processed state register
	 */
	public Integer getProcessedStateRegister() {
		return processedStateRegister;
	}

	/**
	 * Sets the processed state register.
	 *
	 * @param processedStateRegister the new processed state register
	 */
	public void setProcessedStateRegister(Integer processedStateRegister) {
		this.processedStateRegister = processedStateRegister;
	}

	/**
	 * Gets the processed state no process.
	 *
	 * @return the processed state no process
	 */
	public Integer getProcessedStateNoProcess() {
		return processedStateNoProcess;
	}

	/**
	 * Sets the processed state no process.
	 *
	 * @param processedStateNoProcess the new processed state no process
	 */
	public void setProcessedStateNoProcess(Integer processedStateNoProcess) {
		this.processedStateNoProcess = processedStateNoProcess;
	}

	/**
	 * Gets the document type selected.
	 *
	 * @return the document type selected
	 */
	public Integer getDocumentTypeSelected() {
		return documentTypeSelected;
	}

	/**
	 * Sets the document type selected.
	 *
	 * @param documentTypeSelected the new document type selected
	 */
	public void setDocumentTypeSelected(Integer documentTypeSelected) {
		this.documentTypeSelected = documentTypeSelected;
	}

	/**
	 * Gets the lst document type.
	 *
	 * @return the lst document type
	 */
	public List<ParameterTable> getLstDocumentType() {
		return lstDocumentType;
	}

	/**
	 * Sets the lst document type.
	 *
	 * @param lstDocumentType the new lst document type
	 */
	public void setLstDocumentType(List<ParameterTable> lstDocumentType) {
		this.lstDocumentType = lstDocumentType;
	}

	/**
	 * Gets the lst countries.
	 *
	 * @return the lstCountries
	 */
	public List<GeographicLocation> getLstCountries() {
		return lstCountries;
	}

	/**
	 * Sets the lst countries.
	 *
	 * @param lstCountries the lstCountries to set
	 */
	public void setLstCountries(List<GeographicLocation> lstCountries) {
		this.lstCountries = lstCountries;
	}

	/**
	 * Gets the lst departament.
	 *
	 * @return the lstDepartament
	 */
	public List<GeographicLocation> getLstDepartament() {
		return lstDepartament;
	}

	/**
	 * Sets the lst departament.
	 *
	 * @param lstDepartament the lstDepartament to set
	 */
	public void setLstDepartament(List<GeographicLocation> lstDepartament) {
		this.lstDepartament = lstDepartament;
	}

	/**
	 * Gets the lst provinces.
	 *
	 * @return the lstProvinces
	 */
	public List<GeographicLocation> getLstProvinces() {
		return lstProvinces;
	}

	/**
	 * Sets the lst provinces.
	 *
	 * @param lstProvinces the lstProvinces to set
	 */
	public void setLstProvinces(List<GeographicLocation> lstProvinces) {
		this.lstProvinces = lstProvinces;
	}

	/**
	 * Gets the lst districts.
	 *
	 * @return the lstDistricts
	 */
	public List<GeographicLocation> getLstDistricts() {
		return lstDistricts;
	}

	/**
	 * Sets the lst districts.
	 *
	 * @param lstDistricts the lstDistricts to set
	 */
	public void setLstDistricts(List<GeographicLocation> lstDistricts) {
		this.lstDistricts = lstDistricts;
	}

	/**
	 * Gets the collection record selection.
	 *
	 * @return the collectionRecordSelection
	 */
	public CollectionRecordTo getCollectionRecordSelection() {
		return collectionRecordSelection;
	}

	/**
	 * Sets the collection record selection.
	 *
	 * @param collectionRecordSelection the collectionRecordSelection to set
	 */
	public void setCollectionRecordSelection(CollectionRecordTo collectionRecordSelection) {
		this.collectionRecordSelection = collectionRecordSelection;
	}

	/**
	 * Gets the collection record data model.
	 *
	 * @return the collectionRecordDataModel
	 */
	public GenericDataModel<CollectionRecordTo> getCollectionRecordDataModel() {
		return collectionRecordDataModel;
	}

	/**
	 * Sets the collection record data model.
	 *
	 * @param collectionRecordDataModel the collectionRecordDataModel to set
	 */
	public void setCollectionRecordDataModel(GenericDataModel<CollectionRecordTo> collectionRecordDataModel) {
		this.collectionRecordDataModel = collectionRecordDataModel;
	}

	/**
	 * Gets the source information list.
	 *
	 * @return the sourceInformationList
	 */
	public List<ParameterTable> getSourceInformationList() {
		return sourceInformationList;
	}

	/**
	 * Sets the source information list.
	 *
	 * @param sourceInformationList the sourceInformationList to set
	 */
	public void setSourceInformationList(List<ParameterTable> sourceInformationList) {
		this.sourceInformationList = sourceInformationList;
	}

	/**
	 * Gets the source type.
	 *
	 * @return the sourceType
	 */
	public Integer getSourceType() {
		return sourceType;
	}

	/**
	 * Sets the source type.
	 *
	 * @param sourceType the sourceType to set
	 */
	public void setSourceType(Integer sourceType) {
		this.sourceType = sourceType;
	}

	/**
	 * Gets the list month.
	 *
	 * @return the listMonth
	 */
	public List<MonthsOfTheYearType> getListMonth() {
		return listMonth;
	}

	/**
	 * Sets the list month.
	 *
	 * @param listMonth the listMonth to set
	 */
	public void setListMonth(List<MonthsOfTheYearType> listMonth) {
		this.listMonth = listMonth;
	}

	/**
	 * Gets the visible month.
	 *
	 * @return the visibleMonth
	 */
	public Boolean getVisibleMonth() {
		return visibleMonth;
	}

	/**
	 * Sets the visible month.
	 *
	 * @param visibleMonth the visibleMonth to set
	 */
	public void setVisibleMonth(Boolean visibleMonth) {
		this.visibleMonth = visibleMonth;
	}

	/**
	 * Gets the tax applied.
	 *
	 * @return the taxApplied
	 */
	public BigDecimal getTaxApplied() {
		return taxApplied;
	}

	/**
	 * Sets the tax applied.
	 *
	 * @param taxApplied the taxApplied to set
	 */
	public void setTaxApplied(BigDecimal taxApplied) {
		this.taxApplied = taxApplied;
	}

	/**
	 * @return the calculationDate
	 */
	public Date getCalculationDate() {
		return calculationDate;
	}

	/**
	 * @param calculationDate the calculationDate to set
	 */
	public void setCalculationDate(Date calculationDate) {
		this.calculationDate = calculationDate;
	}

}
