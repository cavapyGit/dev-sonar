package com.pradera.billing.billingcollection.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;

import com.pradera.billing.billingrate.to.ServiceRateTo;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CollectionRecordDetailTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class CollectionRecordDetailTo implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id collection record detail pk. */
	private Long 		 	idCollectionRecordDetailPk;
	
	/** The description rate. */
	private String 			descriptionRate;
	
	/** The movement count. */
	private Integer 	 	movementCount;
	
	/** The id movement type pk. */
	private Long 		 	idMovementTypePk;
	
	/** The id security code fk. */
	private String 		 	idSecurityCodeFk;	
	
	/** The id issuance code pk. */
	private String 		 	idIssuanceCodePk;
	
	/** The id issuer fk. */
	private String 	   		idIssuerFk;
	
	/** The amount. */
	private BigDecimal    	amount;	
	
	/** The percentage. */
	private BigDecimal 		percentage;
	
	/** The movement quantity. */
	private Long	 		movementQuantity;
	
	/** The operation price. */
	private BigDecimal 		operationPrice;	
	
	/** The description movement type. */
	private String 			descriptionMovementType;
	
	/** The sequence number. */
	private Integer 		sequenceNumber;
	
	/** The rate code. */
	private Integer 		rateCode;
	
	/** The currency rate. */
	private Integer			currencyRate;
	
	/** The description currency rate. */
	private String			descriptionCurrencyRate;
	
	/** The description rate service type. */
	private String			descriptionRateServiceType;
	
	/** The calculation date. */
	private Date 			calculationDate;
	
	/** The service rate to. */
	private ServiceRateTo 	serviceRateTo;
	
	/** The factor. */
	private BigDecimal		factor;
	
	/** The price by quantity. */
	private BigDecimal		priceByQuantity;
	
	/** The security class. */
	private Integer			securityClass;
	
	/** The currency origin. */
	private Integer 		currencyOrigin;
	
	/** The modality. */
	private Integer 		modality;
	
	/** The description security class. */
	private String 			descriptionSecurityClass;
	
	/** The description currency origin. */
	private String 			descriptionCurrencyOrigin;
	
	/** The description modality. */
	private String 			descriptionModality;
	
	/** The nominal value. */
	private BigDecimal 		nominalValue;
	
	/** The calculation balance. */
	private BigDecimal 		calculationBalance;
	
	/** The ballot sequential. */
	private String 			ballotSequential;
	
	/** The negotiated amount origin. */
	private BigDecimal 		negotiatedAmountOrigin;
	
	/** The negotiated amount. */
	private BigDecimal 		negotiatedAmount;
	
	/** The role. */
	private Integer 		role;
	
	/**  To show the detail. */ 
	private Date 			movementDate;
	
	/** The operation date. */
	private Date 			operationDate;
	
	/** The operation number. */
	private Long 			operationNumber;
	
	/** The id source participant. */
	private Long 			idSourceParticipant;
	
	/** The id target participant. */
	private Long 			idTargetParticipant;
	
	/** The holder account. */
	private Long 			holderAccount;
	
	/** The id holder pk. */
	private Long 			idHolderPk;
	
	/** The tmp request. */
	private String 			tmpRequest;
	
	/** Mnemonic Holder*/
	private String 			mnemonicHolder;

	private Integer economicActivityParticipant;
	
	
	private Integer portfolio;
 
	
	/**
	 * Gets the id holder pk.
	 *
	 * @return the id holder pk
	 */
	public Long getIdHolderPk() {
		return idHolderPk;
	}

	/**
	 * Sets the id holder pk.
	 *
	 * @param idHolderPk the new id holder pk
	 */
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}

	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public Long getHolderAccount() {
		return holderAccount;
	}

	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(Long holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * Gets the id target participant.
	 *
	 * @return the id target participant
	 */
	public Long getIdTargetParticipant() {
		return idTargetParticipant;
	}

	/**
	 * Sets the id target participant.
	 *
	 * @param idTargetParticipant the new id target participant
	 */
	public void setIdTargetParticipant(Long idTargetParticipant) {
		this.idTargetParticipant = idTargetParticipant;
	}

	/**
	 * Gets the movement date.
	 *
	 * @return the movement date
	 */
	public Date getMovementDate() {
		return movementDate;
	}

	/**
	 * Sets the movement date.
	 *
	 * @param movementDate the new movement date
	 */
	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}

	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return operationDate;
	}

	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}

	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}

	/**
	 * Gets the id source participant.
	 *
	 * @return the id source participant
	 */
	public Long getIdSourceParticipant() {
		return idSourceParticipant;
	}

	/**
	 * Sets the id source participant.
	 *
	 * @param idSourceParticipant the new id source participant
	 */
	public void setIdSourceParticipant(Long idSourceParticipant) {
		this.idSourceParticipant = idSourceParticipant;
	}

	/**
	 * Gets the id collection record detail pk.
	 *
	 * @return the id collection record detail pk
	 */
	public Long getIdCollectionRecordDetailPk() {
		return idCollectionRecordDetailPk;
	}

	/**
	 * Sets the id collection record detail pk.
	 *
	 * @param idCollectionRecordDetailPk the new id collection record detail pk
	 */
	public void setIdCollectionRecordDetailPk(Long idCollectionRecordDetailPk) {
		this.idCollectionRecordDetailPk = idCollectionRecordDetailPk;
	}

	/**
	 * Gets the movement count.
	 *
	 * @return the movement count
	 */
	public Integer getMovementCount() {
		return movementCount;
	}

	/**
	 * Sets the movement count.
	 *
	 * @param movementCount the new movement count
	 */
	public void setMovementCount(Integer movementCount) {
		this.movementCount = movementCount;
	}

	/**
	 * Gets the id movement type pk.
	 *
	 * @return the id movement type pk
	 */
	public Long getIdMovementTypePk() {
		return idMovementTypePk;
	}

	/**
	 * Sets the id movement type pk.
	 *
	 * @param idMovementTypePk the new id movement type pk
	 */
	public void setIdMovementTypePk(Long idMovementTypePk) {
		this.idMovementTypePk = idMovementTypePk;
	}


	/**
	 * Gets the id security code fk.
	 *
	 * @return the idSecurityCodeFk
	 */
	public String getIdSecurityCodeFk() {
		return idSecurityCodeFk;
	}

	/**
	 * Sets the id security code fk.
	 *
	 * @param idSecurityCodeFk the idSecurityCodeFk to set
	 */
	public void setIdSecurityCodeFk(String idSecurityCodeFk) {
		this.idSecurityCodeFk = idSecurityCodeFk;
	}

	/**
	 * Gets the amount.
	 *
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * Sets the amount.
	 *
	 * @param amount the new amount
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	
	
	/**
	 * Gets the percentage.
	 *
	 * @return the percentage
	 */
	public BigDecimal getPercentage() {
		return percentage;
	}

	/**
	 * Sets the percentage.
	 *
	 * @param percentage the new percentage
	 */
	public void setPercentage(BigDecimal percentage) {
		this.percentage = percentage;
	}

	/**
	 * Gets the description movement type.
	 *
	 * @return the description movement type
	 */
	public String getDescriptionMovementType() {
		return descriptionMovementType;
	}

	/**
	 * Sets the description movement type.
	 *
	 * @param descriptionMovementType the new description movement type
	 */
	public void setDescriptionMovementType(String descriptionMovementType) {
		this.descriptionMovementType = descriptionMovementType;
	}

	/**
	 * Gets the sequence number.
	 *
	 * @return the sequence number
	 */
	public Integer getSequenceNumber() {
		return sequenceNumber;
	}

	/**
	 * Sets the sequence number.
	 *
	 * @param sequenceNumber the new sequence number
	 */
	public void setSequenceNumber(Integer sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	/**
	 * Gets the description rate.
	 *
	 * @return the description rate
	 */
	public String getDescriptionRate() {
		return descriptionRate;
	}

	/**
	 * Sets the description rate.
	 *
	 * @param descriptionRate the new description rate
	 */
	public void setDescriptionRate(String descriptionRate) {
		this.descriptionRate = descriptionRate;
	}

	/**
	 * Gets the rate code.
	 *
	 * @return the rate code
	 */
	public Integer getRateCode() {
		return rateCode;
	}

	/**
	 * Sets the rate code.
	 *
	 * @param rateCode the new rate code
	 */
	public void setRateCode(Integer rateCode) {
		this.rateCode = rateCode;
	}

	/**
	 * Gets the currency rate.
	 *
	 * @return the currency rate
	 */
	public Integer getCurrencyRate() {
		return currencyRate;
	}

	/**
	 * Sets the currency rate.
	 *
	 * @param currencyRate the new currency rate
	 */
	public void setCurrencyRate(Integer currencyRate) {
		this.currencyRate = currencyRate;
	}

	/**
	 * Gets the description currency rate.
	 *
	 * @return the description currency rate
	 */
	public String getDescriptionCurrencyRate() {
		return descriptionCurrencyRate;
	}

	/**
	 * Sets the description currency rate.
	 *
	 * @param descriptionCurrencyRate the new description currency rate
	 */
	public void setDescriptionCurrencyRate(String descriptionCurrencyRate) {
		this.descriptionCurrencyRate = descriptionCurrencyRate;
	}

	/**
	 * Gets the description rate service type.
	 *
	 * @return the description rate service type
	 */
	public String getDescriptionRateServiceType() {
		return descriptionRateServiceType;
	}

	/**
	 * Sets the description rate service type.
	 *
	 * @param descriptionRateServiceType the new description rate service type
	 */
	public void setDescriptionRateServiceType(String descriptionRateServiceType) {
		this.descriptionRateServiceType = descriptionRateServiceType;
	}

	/**
	 * Gets the service rate to.
	 *
	 * @return the service rate to
	 */
	public ServiceRateTo getServiceRateTo() {
		return serviceRateTo;
	}

	/**
	 * Sets the service rate to.
	 *
	 * @param serviceRateTo the new service rate to
	 */
	public void setServiceRateTo(ServiceRateTo serviceRateTo) {
		this.serviceRateTo = serviceRateTo;
	}

	
	
	/**
	 * Gets the movement quantity.
	 *
	 * @return the movement quantity
	 */
	public Long getMovementQuantity() {
		return movementQuantity;
	}

	/**
	 * Sets the movement quantity.
	 *
	 * @param movementQuantity the new movement quantity
	 */
	public void setMovementQuantity(Long movementQuantity) {
		this.movementQuantity = movementQuantity;
	}

	/**
	 * Gets the operation price.
	 *
	 * @return the operation price
	 */
	public BigDecimal getOperationPrice() {
		return operationPrice;
	}

	/**
	 * Sets the operation price.
	 *
	 * @param operationPrice the new operation price
	 */
	public void setOperationPrice(BigDecimal operationPrice) {
		this.operationPrice = operationPrice;
	}

	/**
	 * Gets the calculation date.
	 *
	 * @return the calculation date
	 */
	public Date getCalculationDate() {
		return calculationDate;
	}

	/**
	 * Sets the calculation date.
	 *
	 * @param calculationDate the new calculation date
	 */
	public void setCalculationDate(Date calculationDate) {
		this.calculationDate = calculationDate;
	}

	/**
	 * Gets the id issuance code pk.
	 *
	 * @return the id issuance code pk
	 */
	public String getIdIssuanceCodePk() {
		return idIssuanceCodePk;
	}

	/**
	 * Sets the id issuance code pk.
	 *
	 * @param idIssuanceCodePk the new id issuance code pk
	 */
	public void setIdIssuanceCodePk(String idIssuanceCodePk) {
		this.idIssuanceCodePk = idIssuanceCodePk;
	}

	/**
	 * Gets the factor.
	 *
	 * @return the factor
	 */
	public BigDecimal getFactor() {
		return factor;
	}

	/**
	 * Sets the factor.
	 *
	 * @param factor the new factor
	 */
	public void setFactor(BigDecimal factor) {
		this.factor = factor;
	}

	/**
	 * Gets the price by quantity.
	 *
	 * @return the priceByQuantity
	 */
	public BigDecimal getPriceByQuantity() {
		return priceByQuantity;
	}

	/**
	 * Sets the price by quantity.
	 *
	 * @param priceByQuantity the priceByQuantity to set
	 */
	public void setPriceByQuantity(BigDecimal priceByQuantity) {
		this.priceByQuantity = priceByQuantity;
	}

	/**
	 * Gets the security class.
	 *
	 * @return the securityClass
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}

	/**
	 * Sets the security class.
	 *
	 * @param securityClass the securityClass to set
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	/**
	 * Gets the currency origin.
	 *
	 * @return the currencyOrigin
	 */
	public Integer getCurrencyOrigin() {
		return currencyOrigin;
	}

	/**
	 * Sets the currency origin.
	 *
	 * @param currencyOrigin the currencyOrigin to set
	 */
	public void setCurrencyOrigin(Integer currencyOrigin) {
		this.currencyOrigin = currencyOrigin;
	}

	/**
	 * Gets the description security class.
	 *
	 * @return the descriptionSecurityClass
	 */
	public String getDescriptionSecurityClass() {
		return descriptionSecurityClass;
	}

	/**
	 * Sets the description security class.
	 *
	 * @param descriptionSecurityClass the descriptionSecurityClass to set
	 */
	public void setDescriptionSecurityClass(String descriptionSecurityClass) {
		this.descriptionSecurityClass = descriptionSecurityClass;
	}

	/**
	 * Gets the description currency origin.
	 *
	 * @return the descriptionCurrencyOrigin
	 */
	public String getDescriptionCurrencyOrigin() {
		return descriptionCurrencyOrigin;
	}

	/**
	 * Sets the description currency origin.
	 *
	 * @param descriptionCurrencyOrigin the descriptionCurrencyOrigin to set
	 */
	public void setDescriptionCurrencyOrigin(String descriptionCurrencyOrigin) {
		this.descriptionCurrencyOrigin = descriptionCurrencyOrigin;
	}

	/**
	 * Gets the modality.
	 *
	 * @return the modality
	 */
	public Integer getModality() {
		return modality;
	}

	/**
	 * Sets the modality.
	 *
	 * @param modality the modality to set
	 */
	public void setModality(Integer modality) {
		this.modality = modality;
	}

	/**
	 * Gets the description modality.
	 *
	 * @return the descriptionModality
	 */
	public String getDescriptionModality() {
		return descriptionModality;
	}

	/**
	 * Sets the description modality.
	 *
	 * @param descriptionModality the descriptionModality to set
	 */
	public void setDescriptionModality(String descriptionModality) {
		this.descriptionModality = descriptionModality;
	}

	/**
	 * Gets the nominal value.
	 *
	 * @return the nominalValue
	 */
	public BigDecimal getNominalValue() {
		return nominalValue;
	}

	/**
	 * Sets the nominal value.
	 *
	 * @param nominalValue the nominalValue to set
	 */
	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}

	/**
	 * Gets the calculation balance.
	 *
	 * @return the calculationBalance
	 */
	public BigDecimal getCalculationBalance() {
		return calculationBalance;
	}

	/**
	 * Sets the calculation balance.
	 *
	 * @param calculationBalance the calculationBalance to set
	 */
	public void setCalculationBalance(BigDecimal calculationBalance) {
		this.calculationBalance = calculationBalance;
	}

	/**
	 * Gets the ballot sequential.
	 *
	 * @return the ballotSequential
	 */
	public String getBallotSequential() {
		return ballotSequential;
	}

	/**
	 * Sets the ballot sequential.
	 *
	 * @param ballotSequential the ballotSequential to set
	 */
	public void setBallotSequential(String ballotSequential) {
		this.ballotSequential = ballotSequential;
	}

	/**
	 * Gets the negotiated amount origin.
	 *
	 * @return the negotiatedAmountOrigin
	 */
	public BigDecimal getNegotiatedAmountOrigin() {
		return negotiatedAmountOrigin;
	}

	/**
	 * Sets the negotiated amount origin.
	 *
	 * @param negotiatedAmountOrigin the negotiatedAmountOrigin to set
	 */
	public void setNegotiatedAmountOrigin(BigDecimal negotiatedAmountOrigin) {
		this.negotiatedAmountOrigin = negotiatedAmountOrigin;
	}

	/**
	 * Monto Negociado a La Moneda De calculo.
	 *
	 * @return the negotiatedAmount
	 */
	public BigDecimal getNegotiatedAmount() {
		return negotiatedAmount;
	}

	/**
	 * Monto Negociado a La Moneda De calculo.
	 *
	 * @param negotiatedAmount the negotiatedAmount to set
	 */
	public void setNegotiatedAmount(BigDecimal negotiatedAmount) {
		this.negotiatedAmount = negotiatedAmount;
	}

	/**
	 * 1: Comprador (buyer)  2: Vendedor(Seller).
	 *
	 * @return the role
	 */
	public Integer getRole() {
		return role;
	}

	/**
	 * 1: Comprador (buyer)  2: Vendedor(Seller).
	 *
	 * @param role the role to set
	 */
	public void setRole(Integer role) {
		this.role = role;
	}

	/**
	 * Gets the tmp request.
	 *
	 * @return the tmpRequest
	 */
	public String getTmpRequest() {
		return tmpRequest;
	}

	/**
	 * Sets the tmp request.
	 *
	 * @param tmpRequest the tmpRequest to set
	 */
	public void setTmpRequest(String tmpRequest) {
		this.tmpRequest = tmpRequest;
	}

	/**
	 * Gets the id issuer fk.
	 *
	 * @return the idIssuerFk
	 */
	public String getIdIssuerFk() {
		return idIssuerFk;
	}

	/**
	 * Sets the id issuer fk.
	 *
	 * @param idIssuerFk the idIssuerFk to set
	 */
	public void setIdIssuerFk(String idIssuerFk) {
		this.idIssuerFk = idIssuerFk;
	}

	/**
	 * @return the mnemonicHolder
	 */
	public String getMnemonicHolder() {
		return mnemonicHolder;
	}

	/**
	 * @param mnemonicHolder the mnemonicHolder to set
	 */
	public void setMnemonicHolder(String mnemonicHolder) {
		this.mnemonicHolder = mnemonicHolder;
	}

	/**
	 * @return the economicActivityParticipant
	 */
	public Integer getEconomicActivityParticipant() {
		return economicActivityParticipant;
	}

	/**
	 * @param economicActivityParticipant the economicActivityParticipant to set
	 */
	public void setEconomicActivityParticipant(Integer economicActivityParticipant) {
		this.economicActivityParticipant = economicActivityParticipant;
	}

	/**
	 * @return the portfolio
	 */
	public Integer getPortfolio() {
		return portfolio;
	}

	/**
	 * @param portfolio the portfolio to set
	 */
	public void setPortfolio(Integer portfolio) {
		this.portfolio = portfolio;
	}


	
	
}
