package com.pradera.billing.billingcollection.service;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.Query;
import javax.transaction.TransactionSynchronizationRegistry;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;
import org.hibernate.exception.SQLGrammarException;

import com.pradera.billing.billingcollection.to.CollectionRecordTo;
import com.pradera.billing.billingcollection.to.DailyExchangeRateFilter;
import com.pradera.billing.billingexception.service.ExceptionLayerServiceBean;
import com.pradera.billing.billingservice.service.BillingServiceMgmtServiceBean;
import com.pradera.billing.common.facade.BusinessLayerCommonFacade;
import com.pradera.billing.common.utils.BillingUtils;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantClassType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.RateMovement;
import com.pradera.model.billing.ServiceRate;
import com.pradera.model.billing.type.BillingServicePortfolioType;
import com.pradera.model.billing.type.EntityCollectionType;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.custody.affectation.type.AffectationStateType;
import com.pradera.model.custody.type.AccreditationOperationMotive;
import com.pradera.model.custody.type.AccreditationOperationStateType;
import com.pradera.model.custody.type.ChangeOwnershipStatusType;
import com.pradera.model.custody.type.PetitionerType;
import com.pradera.model.custody.type.StateType;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.DailyExchangeRoleType;
import com.pradera.model.generalparameter.type.InformationSourceType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.placementsegment.type.PlacementSegmentStateType;
import com.pradera.model.issuancesecuritie.type.IssuanceStateType;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.issuancesecuritie.type.SecurityType;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.NegotiationModalityType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.negotiation.type.SirtexOperationStateType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.settlement.type.OperationPartType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Project iDepositary. Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BaseCollectionMgmtServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class BaseCollectionMgmtServiceBean extends CrudDaoServiceBean {

	/** The transaction registry. */
	@Resource
	TransactionSynchronizationRegistry transactionRegistry;

	/** The business layer common facade. */
	@EJB
	BusinessLayerCommonFacade businessLayerCommonFacade;

	/** The exception layer service bean. */
	@EJB
	ExceptionLayerServiceBean exceptionLayerServiceBean;

	/** The client rest service. */
	@Inject
	ClientRestService clientRestService;

	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;

	/** The billing service mgmt service bean. */
	@EJB
	private BillingServiceMgmtServiceBean billingServiceMgmtServiceBean;

	/** The message configuration. */
	@Inject
	@InjectableResource(location = "Messages_es.properties")
	private Properties messageConfiguration;

	/** The log. */
	@Inject
	private PraderaLogger log;

	/**
	 * EDV 1 Calculation daily
	 * 
	 * 1. CUSTODIA FISICA 
	 *
	 * @param parameters       the parameters
	 * @param idInstrumentType the id instrument type
	 * @return the list
	 * @throws ServiceException the service exception CALCULATION_DATE
	 */
	@SuppressWarnings("unchecked")
	public List<Object> findBalanceSecurityAnnotationOnAccount(Map parameters, Integer currency) throws ServiceException {
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		List<Integer> listEconomicAct = (List<Integer>) parameters.get(GeneralConstants.ECONOMIC_ACTIVITY_LIST_INT);
		Integer portFolio = billingService.getPortfolio();

		StringBuilder strTableComponentBegin = new StringBuilder();

		StringBuilder stringBuilderSqlBase = new StringBuilder();
		stringBuilderSqlBase.append(" SELECT QUERY_ALL.* FROM( 																			   ");
		stringBuilderSqlBase.append(" SELECT BASE_ALL.*, 																				   ");
		stringBuilderSqlBase.append(" (CASE WHEN BASE_ALL.ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P 					   ");
		stringBuilderSqlBase.append("										where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)  		   ");
		stringBuilderSqlBase.append(		"THEN 1 ELSE 0 END) CARTERA_PROPIA															   ");
		stringBuilderSqlBase.append(" 																									   ");
		stringBuilderSqlBase.append(" FROM ( 																							   ");
		stringBuilderSqlBase.append("																									   ");
		stringBuilderSqlBase.append(" SELECT DISTINCT  HAB.ID_PARTICIPANT_PK AS ID_PARTICIPANT_FK , 									   ");
		stringBuilderSqlBase.append("					HAB.TOTAL_BALANCE*S.CURRENT_NOMINAL_VALUE AS PRICE, 							   ");
		stringBuilderSqlBase.append("         			S.CURRENCY,   																	   ");
		stringBuilderSqlBase.append("					(SELECT HO.ID_HOLDER_PK FROM HOLDER_ACCOUNT_DETAIL HAD, HOLDER HO				   ");
		stringBuilderSqlBase.append("					WHERE HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK						       ");
		stringBuilderSqlBase.append("					AND HO.ID_HOLDER_PK=HAD.ID_HOLDER_FK and rownum=1) AS ID_HOLDER_FK,				   ");
		stringBuilderSqlBase.append(" 		  			HAB.ID_SECURITY_CODE_PK AS ID_SECURITY_CODE_FK ,  								   ");
		stringBuilderSqlBase.append(" 		  			S.SECURITY_CLASS AS SECURITY_CLASS, 											   ");
		stringBuilderSqlBase.append("         			HAB.TOTAL_BALANCE TOTAL_BALANCE ,   											   ");
		stringBuilderSqlBase.append("					ROUND ( S.CURRENT_NOMINAL_VALUE, DECODE (S.SECURITY_CLASS,426,4,2) ) NOMINAL_VALUE,");
		stringBuilderSqlBase.append("         			HAB.ID_HOLDER_ACCOUNT_PK AS ID_HOLDER_ACCOUNT_FK, 								   ");
		stringBuilderSqlBase.append("        			S.ID_ISSUER_FK AS ID_ISSUER_FK ,												   ");
		stringBuilderSqlBase.append("        			S.ID_ISSUANCE_CODE_FK AS ID_ISSUANCE_CODE_FK  									   ");
		stringBuilderSqlBase.append("   FROM HOLDER_ACCOUNT_BALANCE HAB  																   ");
		stringBuilderSqlBase.append("   INNER JOIN SECURITY S ON (S.ID_SECURITY_CODE_PK= HAB.ID_SECURITY_CODE_PK) 						   ");
		stringBuilderSqlBase.append("   INNER JOIN HOLDER_ACCOUNT HA ON ( HA.ID_HOLDER_ACCOUNT_PK = HAB.ID_HOLDER_ACCOUNT_PK ) 			   ");
		stringBuilderSqlBase.append("   INNER JOIN ISSUER I ON (I.ID_ISSUER_PK=S.ID_ISSUER_FK)   										   ");
		stringBuilderSqlBase.append("     																								   ");
		stringBuilderSqlBase.append(" 	WHERE   																						   ");

		stringBuilderSqlBase.append("  	  S.ISSUANCE_FORM in (").append(400);// ---400 RENTA FIJA --132 Y 401 ELECTRONICO
		stringBuilderSqlBase.append("  	  )																							   	   ");
		stringBuilderSqlBase.append("  	  AND S.STATE_SECURITY = ").append(SecurityStateType.REGISTERED.getCode());// ---131 VALOR EN ESTADO REGISTRADO
		if(Validations.validateIsNotNull(currency)) {
			stringBuilderSqlBase.append(" AND S.CURRENCY = ").append(currency.toString());
		}
		stringBuilderSqlBase.append("  	  AND HAB.TOTAL_BALANCE > " + GeneralConstants.ZERO_VALUE_INTEGER);// CON SALDO EN CUENTA MATRIZ
		
		stringBuilderSqlBase.append("	UNION	                                                                                           ");
		stringBuilderSqlBase.append("	SELECT DISTINCT  HAB.ID_PARTICIPANT_PK AS ID_PARTICIPANT_FK ,                                      ");
		stringBuilderSqlBase.append("   				HAB.REPORTED_BALANCE*S.CURRENT_NOMINAL_VALUE AS PRICE,                             ");
		stringBuilderSqlBase.append("   				S.CURRENCY,                                                                        ");
		stringBuilderSqlBase.append("   				(SELECT HO.ID_HOLDER_PK FROM HOLDER_ACCOUNT_DETAIL HAD, HOLDER HO                  ");
		stringBuilderSqlBase.append("   					WHERE HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK                         ");
		stringBuilderSqlBase.append("   						AND HO.ID_HOLDER_PK=HAD.ID_HOLDER_FK and rownum=1) AS ID_HOLDER_FK,        ");
		stringBuilderSqlBase.append("   				HAB.ID_SECURITY_CODE_PK AS ID_SECURITY_CODE_FK ,                       			   ");
		stringBuilderSqlBase.append("   				S.SECURITY_CLASS AS SECURITY_CLASS,        	                           			   ");
		stringBuilderSqlBase.append("   				HAB.REPORTED_BALANCE TOTAL_BALANCE ,                                   			   ");
		stringBuilderSqlBase.append("   				ROUND ( S.CURRENT_NOMINAL_VALUE, DECODE (S.SECURITY_CLASS,426,4,2) )   			   ");
		stringBuilderSqlBase.append("					NOMINAL_VALUE,   													   			   ");
		stringBuilderSqlBase.append("   				HAB.ID_HOLDER_ACCOUNT_PK AS ID_HOLDER_ACCOUNT_FK,	     	          			   ");
		stringBuilderSqlBase.append("   				S.ID_ISSUER_FK AS ID_ISSUER_FK ,       				                   			   ");
		stringBuilderSqlBase.append("   				S.ID_ISSUANCE_CODE_FK AS ID_ISSUANCE_CODE_FK                           			   ");
		stringBuilderSqlBase.append("   FROM HOLDER_ACCOUNT_BALANCE HAB                                                                    ");
		stringBuilderSqlBase.append("   INNER JOIN SECURITY S ON (S.ID_SECURITY_CODE_PK= HAB.ID_SECURITY_CODE_PK)                          ");
		stringBuilderSqlBase.append("   INNER JOIN HOLDER_ACCOUNT HA ON ( HA.ID_HOLDER_ACCOUNT_PK = HAB.ID_HOLDER_ACCOUNT_PK )             ");
		stringBuilderSqlBase.append("   INNER JOIN ISSUER I ON (I.ID_ISSUER_PK=S.ID_ISSUER_FK)                                             ");
		stringBuilderSqlBase.append("   WHERE S.ISSUANCE_FORM in (").append(400);// ---400 RENTA FIJA --132 Y 401 ELECTRONICO
		stringBuilderSqlBase.append("  	)																							   	   ");
		if(Validations.validateIsNotNull(currency)) {
			stringBuilderSqlBase.append(" AND S.CURRENCY = ").append(currency.toString());
		}
		stringBuilderSqlBase.append("   AND S.STATE_SECURITY=  ").append(SecurityStateType.REGISTERED.getCode());// ---131 VALOR EN ESTADO REGISTRADO
		stringBuilderSqlBase.append("   AND HAB.REPORTED_BALANCE > " + GeneralConstants.ZERO_VALUE_INTEGER);// CON SALDO EN REPORTO
		stringBuilderSqlBase.append("   AND HAB.ID_SECURITY_CODE_PK in (SELECT b.ID_SECURITY_CODE_PK FROM HOLDER_ACCOUNT_BALANCE b         ");
		stringBuilderSqlBase.append("   								WHERE  b.ID_PARTICIPANT_PK = '114'  							   ");
		stringBuilderSqlBase.append(" 									AND  b.REPORTING_BALANCE >0)                                       ");
		stringBuilderSqlBase.append(" 																									   ");
		
		stringBuilderSqlBase.append(" ) BASE_ALL																						   ");
		stringBuilderSqlBase.append(" ) QUERY_ALL																						   ");
		stringBuilderSqlBase.append(" WHERE 1=1 																						   ");
		
		if (Validations.validateIsNotNull(portFolio) && !BillingServicePortfolioType.ALL.getCode().equals(portFolio)) {
			stringBuilderSqlBase.append(" AND QUERY_ALL.CARTERA_PROPIA=" + BooleanType.get(portFolio).getBinaryValue());
		}
		/** Making the table EXCEPTION **/
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters, stringBuilderSqlBase);
		log.debug(" :::::::::::::::::::::::::::::::::::::Consulta Transaccion con Reglas de Excepcion :::::::::::::::::::::::::::: ");
		log.debug(strTableComponentBegin.toString());
		Query query = null;
		List<Object> listObject = null;
		try {
			/** IF NUMBER DE EXCEPTIONS EXCEEDS TO CAPACITY LIMIT, A */
			if (Integer.parseInt(parameters.get(GeneralConstants.COUNTER_EXCEPTIONS)
					.toString()) >= GeneralConstants.CAP_LIMIT_EXCEPTIONS) {
				query = em.createNativeQuery(stringBuilderSqlBase.toString());
				listObject = exceptionLayerServiceBean.filterExceptionByList(parameters, query.getResultList());
			} else {
				query = em.createNativeQuery(strTableComponentBegin.toString());
				listObject = query.getResultList();
			}
		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				log.error(":::::::::QUERY GRAMMAR::::::::::::");
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}
		}
		return listObject;
	}
	
	
	/*
	 * 
	 *  2. CUSTODIA ELECTRONICA
	 */
	@SuppressWarnings("unchecked")
	public List<Object> findBalanceSecurityAnnotationOnAccountDesmaterialized(Map parameters) throws ServiceException {
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		List<Integer> listEconomicAct = (List<Integer>) parameters.get(GeneralConstants.ECONOMIC_ACTIVITY_LIST_INT);
		Integer portFolio = billingService.getPortfolio();

		StringBuilder strTableComponentBegin = new StringBuilder();

		StringBuilder stringBuilderSqlBase = new StringBuilder();
		stringBuilderSqlBase.append(" SELECT QUERY_ALL.* FROM( 																			   ");
		stringBuilderSqlBase.append(" SELECT BASE_ALL.*, 																				   ");
		stringBuilderSqlBase.append(" (CASE WHEN BASE_ALL.ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P 					   ");
		stringBuilderSqlBase.append("										where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)  		   ");
		stringBuilderSqlBase.append(		"THEN 1 ELSE 0 END) CARTERA_PROPIA															   ");
		stringBuilderSqlBase.append(" 																									   ");
		stringBuilderSqlBase.append(" FROM ( 																							   ");
		stringBuilderSqlBase.append("																									   ");
		stringBuilderSqlBase.append(" SELECT DISTINCT  HAB.ID_PARTICIPANT_PK AS ID_PARTICIPANT_FK , 									   ");
		stringBuilderSqlBase.append("					HAB.TOTAL_BALANCE*S.CURRENT_NOMINAL_VALUE AS PRICE, 							   ");
		stringBuilderSqlBase.append("         			S.CURRENCY,   																	   ");
		stringBuilderSqlBase.append("					(SELECT HO.ID_HOLDER_PK FROM HOLDER_ACCOUNT_DETAIL HAD, HOLDER HO				   ");
		stringBuilderSqlBase.append("					WHERE HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK						       ");
		stringBuilderSqlBase.append("					AND HO.ID_HOLDER_PK=HAD.ID_HOLDER_FK and rownum=1) AS ID_HOLDER_FK,				   ");
		stringBuilderSqlBase.append(" 		  			HAB.ID_SECURITY_CODE_PK AS ID_SECURITY_CODE_FK ,  								   ");
		stringBuilderSqlBase.append(" 		  			S.SECURITY_CLASS AS SECURITY_CLASS, 											   ");
		stringBuilderSqlBase.append("         			HAB.TOTAL_BALANCE TOTAL_BALANCE ,   											   ");
		stringBuilderSqlBase.append("					ROUND ( S.CURRENT_NOMINAL_VALUE, DECODE (S.SECURITY_CLASS,426,4,2) ) NOMINAL_VALUE,");
		stringBuilderSqlBase.append("         			HAB.ID_HOLDER_ACCOUNT_PK AS ID_HOLDER_ACCOUNT_FK, 								   ");
		stringBuilderSqlBase.append("        			S.ID_ISSUER_FK AS ID_ISSUER_FK ,												   ");
		stringBuilderSqlBase.append("        			S.ID_ISSUANCE_CODE_FK AS ID_ISSUANCE_CODE_FK  									   ");
		stringBuilderSqlBase.append("   FROM HOLDER_ACCOUNT_BALANCE HAB  																   ");
		stringBuilderSqlBase.append("   INNER JOIN SECURITY S ON (S.ID_SECURITY_CODE_PK= HAB.ID_SECURITY_CODE_PK) 						   ");
		stringBuilderSqlBase.append("   INNER JOIN HOLDER_ACCOUNT HA ON ( HA.ID_HOLDER_ACCOUNT_PK = HAB.ID_HOLDER_ACCOUNT_PK ) 			   ");
		stringBuilderSqlBase.append("   INNER JOIN ISSUER I ON (I.ID_ISSUER_PK=S.ID_ISSUER_FK)   										   ");
		stringBuilderSqlBase.append("     																								   ");
		stringBuilderSqlBase.append(" 	WHERE   																						   ");

		stringBuilderSqlBase.append("  	  S.ISSUANCE_FORM in (132, 401)");// --132 Y 401 ELECTRONICO
		stringBuilderSqlBase.append("  	  AND S.STATE_SECURITY = ").append(SecurityStateType.REGISTERED.getCode());// ---131 VALOR EN ESTADO REGISTRADO
		stringBuilderSqlBase.append("  	  AND HAB.TOTAL_BALANCE > " + GeneralConstants.ZERO_VALUE_INTEGER);// CON SALDO EN CUENTA MATRIZ
		
		stringBuilderSqlBase.append("	UNION	                                                                                           ");
		stringBuilderSqlBase.append("	SELECT DISTINCT  HAB.ID_PARTICIPANT_PK AS ID_PARTICIPANT_FK ,                                      ");
		stringBuilderSqlBase.append("   				HAB.REPORTED_BALANCE*S.CURRENT_NOMINAL_VALUE AS PRICE,                             ");
		stringBuilderSqlBase.append("   				S.CURRENCY,                                                                        ");
		stringBuilderSqlBase.append("   				(SELECT HO.ID_HOLDER_PK FROM HOLDER_ACCOUNT_DETAIL HAD, HOLDER HO                  ");
		stringBuilderSqlBase.append("   					WHERE HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK                         ");
		stringBuilderSqlBase.append("   						AND HO.ID_HOLDER_PK=HAD.ID_HOLDER_FK and rownum=1) AS ID_HOLDER_FK,        ");
		stringBuilderSqlBase.append("   				HAB.ID_SECURITY_CODE_PK AS ID_SECURITY_CODE_FK ,                       			   ");
		stringBuilderSqlBase.append("   				S.SECURITY_CLASS AS SECURITY_CLASS,        	                           			   ");
		stringBuilderSqlBase.append("   				HAB.REPORTED_BALANCE TOTAL_BALANCE ,                                   			   ");
		stringBuilderSqlBase.append("   				ROUND ( S.CURRENT_NOMINAL_VALUE, DECODE (S.SECURITY_CLASS,426,4,2) )   			   ");
		stringBuilderSqlBase.append("					NOMINAL_VALUE,   													   			   ");
		stringBuilderSqlBase.append("   				HAB.ID_HOLDER_ACCOUNT_PK AS ID_HOLDER_ACCOUNT_FK,	     	          			   ");
		stringBuilderSqlBase.append("   				S.ID_ISSUER_FK AS ID_ISSUER_FK ,       				                   			   ");
		stringBuilderSqlBase.append("   				S.ID_ISSUANCE_CODE_FK AS ID_ISSUANCE_CODE_FK                           			   ");
		stringBuilderSqlBase.append("   FROM HOLDER_ACCOUNT_BALANCE HAB                                                                    ");
		stringBuilderSqlBase.append("   INNER JOIN SECURITY S ON (S.ID_SECURITY_CODE_PK= HAB.ID_SECURITY_CODE_PK)                          ");
		stringBuilderSqlBase.append("   INNER JOIN HOLDER_ACCOUNT HA ON ( HA.ID_HOLDER_ACCOUNT_PK = HAB.ID_HOLDER_ACCOUNT_PK )             ");
		stringBuilderSqlBase.append("   INNER JOIN ISSUER I ON (I.ID_ISSUER_PK=S.ID_ISSUER_FK)                                             ");
		stringBuilderSqlBase.append("   WHERE S.ISSUANCE_FORM in (132, 401)");// ---400 RENTA FIJA --132 Y 401 ELECTRONICO
		stringBuilderSqlBase.append("   AND S.STATE_SECURITY=  ").append(SecurityStateType.REGISTERED.getCode());// ---131 VALOR EN ESTADO REGISTRADO
		stringBuilderSqlBase.append("   AND HAB.REPORTED_BALANCE > " + GeneralConstants.ZERO_VALUE_INTEGER);// CON SALDO EN REPORTO
		stringBuilderSqlBase.append("   AND HAB.ID_SECURITY_CODE_PK in (SELECT b.ID_SECURITY_CODE_PK FROM HOLDER_ACCOUNT_BALANCE b         ");
		stringBuilderSqlBase.append("   								WHERE  b.ID_PARTICIPANT_PK = '114'  							   ");
		stringBuilderSqlBase.append(" 									AND  b.REPORTING_BALANCE >0)                                       ");
		stringBuilderSqlBase.append(" 																									   ");
		
		stringBuilderSqlBase.append(" ) BASE_ALL																						   ");
		stringBuilderSqlBase.append(" ) QUERY_ALL																						   ");
		stringBuilderSqlBase.append(" WHERE 1=1 																						   ");
		
		if (!BillingServicePortfolioType.ALL.getCode().equals(portFolio)) {
			stringBuilderSqlBase.append(" AND QUERY_ALL.CARTERA_PROPIA=" + BooleanType.get(portFolio).getBinaryValue());
		}
		/** Making the table EXCEPTION **/
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters, stringBuilderSqlBase);
		log.debug(" :::::::::::::::::::::::::::::::::::::Consulta Transaccion con Reglas de Excepcion :::::::::::::::::::::::::::: ");
		log.debug(strTableComponentBegin.toString());
		Query query = null;
		List<Object> listObject = null;
		try {
			/** IF NUMBER DE EXCEPTIONS EXCEEDS TO CAPACITY LIMIT, A */
			if (Integer.parseInt(parameters.get(GeneralConstants.COUNTER_EXCEPTIONS)
					.toString()) >= GeneralConstants.CAP_LIMIT_EXCEPTIONS) {
				query = em.createNativeQuery(stringBuilderSqlBase.toString());
				listObject = exceptionLayerServiceBean.filterExceptionByList(parameters, query.getResultList());
			} else {
				query = em.createNativeQuery(strTableComponentBegin.toString());
				listObject = query.getResultList();
			}
		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				log.error(":::::::::QUERY GRAMMAR::::::::::::");
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}
		}
		return listObject;
	}

	/**
	 * 1 <br>
	 * REGISTRO_VALORES_ANOTACIONES_CUENTA_R_FIJA_AFP <br>
	 * REGISTRO_VALORES_ANOTACIONES_CUENTA_R_VARIABLE_AFP.
	 *
	 * @param parameters       the parameters
	 * @param idInstrumentType the id instrument type
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Object> findBalanceSecurityAnnotationOnAccountAfp(Map parameters, Integer idInstrumentType)
			throws ServiceException {

		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		List<Integer> listEconomicAct = (List<Integer>) parameters.get(GeneralConstants.ECONOMIC_ACTIVITY_LIST_INT);
		Integer portFolio = billingService.getPortfolio();

		StringBuilder strTableComponentBegin = new StringBuilder();

		StringBuilder stringBuilderSqlBase = new StringBuilder();
		stringBuilderSqlBase.append(" SELECT QUERY_ALL.* FROM( ");
		stringBuilderSqlBase.append(" SELECT BASE_ALL.*, ");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL.ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)  THEN 1 ELSE 0 END) CARTERA_PROPIA,");
		stringBuilderSqlBase.append(
				"																																																	");
		stringBuilderSqlBase.append(
				" (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) ACT_ECO_PAR,																																				");
		stringBuilderSqlBase.append(
				"                                                                                                                                                                                                                                  						");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL. ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) THEN (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)           ");
		stringBuilderSqlBase.append(
				"      WHEN  BASE_ALL. ID_HOLDER_FK <>  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) and  (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) = 1879   ");
		stringBuilderSqlBase.append(
				"  THEN (SELECT h.ECONOMIC_ACTIVITY from holder h where H.ID_HOLDER_PK = BASE_ALL.ID_HOLDER_FK   AND  STATE_HOLDER = 496 AND h.ECONOMIC_ACTIVITY IN(55,56,57))ELSE NULL END) H_ECONOMIC_ACTIVITY,                                         				");
		stringBuilderSqlBase.append(
				"                                                                                                                                                                                                                                  						");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL. ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) THEN (SELECT MNEMONIC FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)                    ");
		stringBuilderSqlBase.append(
				"      WHEN  BASE_ALL. ID_HOLDER_FK <>  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) and  (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) = 1879   ");
		stringBuilderSqlBase.append(
				" THEN (SELECT h.MNEMONIC from holder h where H.ID_HOLDER_PK = BASE_ALL.ID_HOLDER_FK   AND  STATE_HOLDER = 496 AND h.ECONOMIC_ACTIVITY IN(55,56,57))ELSE NULL END) NEMONICO_HOLDER                                                               				");
		stringBuilderSqlBase.append(" ");
		stringBuilderSqlBase.append(" FROM ( ");
		stringBuilderSqlBase.append("");
		stringBuilderSqlBase.append("");
		stringBuilderSqlBase.append(" SELECT DISTINCT  HAB.ID_PARTICIPANT_PK AS ID_PARTICIPANT_FK , ");
		stringBuilderSqlBase.append("					HAB.TOTAL_BALANCE*S.CURRENT_NOMINAL_VALUE AS PRICE, ");
		stringBuilderSqlBase.append("         			S.CURRENCY,   ");
		stringBuilderSqlBase
				.append("					(SELECT HO.ID_HOLDER_PK FROM HOLDER_ACCOUNT_DETAIL HAD, HOLDER HO");
		stringBuilderSqlBase.append("					WHERE HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK");
		stringBuilderSqlBase
				.append("					AND HO.ID_HOLDER_PK=HAD.ID_HOLDER_FK and rownum=1) AS ID_HOLDER_FK,");
		stringBuilderSqlBase.append(" 		  			HAB.ID_SECURITY_CODE_PK AS ID_SECURITY_CODE_FK ,  ");
		stringBuilderSqlBase.append(" 		  			S.SECURITY_CLASS AS SECURITY_CLASS, ");
		stringBuilderSqlBase.append("         			HAB.TOTAL_BALANCE TOTAL_BALANCE ,   ");
		stringBuilderSqlBase.append(
				"					ROUND ( S.CURRENT_NOMINAL_VALUE  , DECODE (S.SECURITY_CLASS,426,4,2) )    NOMINAL_VALUE, ");
		stringBuilderSqlBase.append("         			HAB.ID_HOLDER_ACCOUNT_PK AS ID_HOLDER_ACCOUNT_FK, ");
		stringBuilderSqlBase.append("        			S.ID_ISSUER_FK AS ID_ISSUER_FK ,");
		stringBuilderSqlBase.append("        			S.ID_ISSUANCE_CODE_FK AS ID_ISSUANCE_CODE_FK  ");
//		stringBuilderSqlBase.append("   FROM MARKET_FACT_VIEW HAB   ");
		stringBuilderSqlBase.append("   FROM HOLDER_ACCOUNT_BALANCE HAB   ");
		stringBuilderSqlBase.append("   INNER JOIN SECURITY S ON (S.ID_SECURITY_CODE_PK= HAB.ID_SECURITY_CODE_PK) ");
		stringBuilderSqlBase
				.append("   INNER JOIN HOLDER_ACCOUNT HA ON ( HA.ID_HOLDER_ACCOUNT_PK = HAB.ID_HOLDER_ACCOUNT_PK )  ");
		stringBuilderSqlBase.append("   INNER JOIN ISSUER I ON (I.ID_ISSUER_PK=S.ID_ISSUER_FK)   ");
		stringBuilderSqlBase
				.append("   INNER JOIN PARTICIPANT PA ON ( PA.ID_PARTICIPANT_PK=HAB.ID_PARTICIPANT_PK)     ");
		stringBuilderSqlBase.append("     ");
		stringBuilderSqlBase.append(" 	WHERE  ");

		stringBuilderSqlBase.append("  	  	S.INSTRUMENT_TYPE = ").append(idInstrumentType);// ---124 RENTA FIJA --399
																							// RENTA VARIABLE
		stringBuilderSqlBase.append("  	  	AND S.STATE_SECURITY = ").append(SecurityStateType.REGISTERED.getCode());// ---VALOR
																														// EN
																														// ESTADO
																														// REGISTRADO
		stringBuilderSqlBase.append("  	  	AND HAB.TOTAL_BALANCE > ").append(GeneralConstants.ZERO_VALUE_INTEGER);// CON
																													// SALDO
																													// EN
																													// CUENTA
																													// MATRIZ
		stringBuilderSqlBase.append("  		AND PA.ACCOUNT_CLASS= ").append(ParticipantClassType.AFP.getCode());

//		stringBuilderSqlBase.append("  		AND TRUNC(HAB.CUT_DATE)='02/01/16'   ");

//		stringBuilderSqlBase.append("  	ORDER BY HAB.ID_PARTICIPANT_PK,HAB.ID_SECURITY_CODE_PK ,  S.CURRENCY ");

		stringBuilderSqlBase.append(" ) BASE_ALL");
		stringBuilderSqlBase.append(" ) QUERY_ALL");
		stringBuilderSqlBase.append(" WHERE 1=1 ");
		if (!BillingServicePortfolioType.ALL.getCode().equals(portFolio)) {
			stringBuilderSqlBase.append(" AND QUERY_ALL.CARTERA_PROPIA=" + BooleanType.get(portFolio).getBinaryValue());
		}
		stringBuilderSqlBase.append(" AND QUERY_ALL.ACT_ECO_PAR IN(:listEconomicAct)");

		/** Making the table EXCEPTION **/
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters,
				stringBuilderSqlBase);

		log.debug(
				" :::::::::::::::::::::::::::::::::::::Consulta Transaccion con Reglas de Excepcion ::::::::::::::::::::::::::::");
		log.debug(strTableComponentBegin.toString());

		Query query = null;
		List<Object> listObject = null;
		try {

			if (Integer.parseInt(parameters.get(GeneralConstants.COUNTER_EXCEPTIONS)
					.toString()) >= GeneralConstants.CAP_LIMIT_EXCEPTIONS) {
				query = em.createNativeQuery(stringBuilderSqlBase.toString());
				query.setParameter("listEconomicAct", listEconomicAct);
				listObject = exceptionLayerServiceBean.filterExceptionByList(parameters, query.getResultList());

			} else {
				query = em.createNativeQuery(strTableComponentBegin.toString());
				query.setParameter("listEconomicAct", listEconomicAct);
				listObject = query.getResultList();
			}

		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				log.error(":::::::::QUERY GRAMMAR::::::::::::");
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}

		}

		return listObject;
	}
	
	/**
	 * Cavapy 1 Custodia fisica
	 * 
	 * 1. CUSTODIA FISICA 
	 *
	 * @param parameters       the parameters
	 * @param idInstrumentType the id instrument type
	 * @return the list
	 * @throws ServiceException the service exception CALCULATION_DATE
	 */
	@SuppressWarnings("unchecked")
	public List<Object> findBalancePhysicalCustody(Map parameters) throws ServiceException {
		
		Date calculationDate = (Date) parameters.get(GeneralConstants.CALCULATION_DATE);
		
		StringBuilder strTableComponentBegin = new StringBuilder();

		StringBuilder stringBuilderSqlBase = new StringBuilder();
		stringBuilderSqlBase.append(" SELECT DISTINCT  PCBC.ID_PARTICIPANT_PK AS ID_PARTICIPANT_FK , 									   ");//0
		stringBuilderSqlBase.append("					PCBC.PSY_TOTAL_NOMINAL_AMOUNT AS PRICE, 							   ");
		stringBuilderSqlBase.append("         			PCBC.CURRENCY,   																	   ");
		stringBuilderSqlBase.append("					(SELECT HO.ID_HOLDER_PK FROM HOLDER_ACCOUNT_DETAIL HAD, HOLDER HO				   ");
		stringBuilderSqlBase.append("					WHERE HAD.ID_HOLDER_ACCOUNT_FK=PCBC.ID_HOLDER_ACCOUNT_PK						       ");
		stringBuilderSqlBase.append("					AND HO.ID_HOLDER_PK=HAD.ID_HOLDER_FK and rownum=1) AS ID_HOLDER_FK,				   "); // 3
		stringBuilderSqlBase.append(" 		  			PCBC.ID_SECURITY_CODE_FK AS ID_SECURITY_CODE_FK ,  								   ");
		stringBuilderSqlBase.append(" 		  			PCBC.SECURITY_CLASS AS SECURITY_CLASS, 											   ");
		stringBuilderSqlBase.append("         			PCBC.TOTAL_BALANCE AS TOTAL_BALANCE ,   											   ");
		stringBuilderSqlBase.append("					PCBC.PSY_TOTAL_NOMINAL_VALUE AS NOMINAL_VALUE,");
		stringBuilderSqlBase.append("         			PCBC.ID_HOLDER_ACCOUNT_PK AS ID_HOLDER_ACCOUNT_FK, 								   ");
		stringBuilderSqlBase.append("        			PCBC.ID_ISSUER_FK AS ID_ISSUER_FK ,												   ");
		stringBuilderSqlBase.append("        			PCBC.ID_ISSUANCE_CODE_FK AS ID_ISSUANCE_CODE_FK,  									   "); //10
		stringBuilderSqlBase.append("        			PCBC.CUT_DATE AS CUT_DATE  									   						"); //11
		stringBuilderSqlBase.append("   FROM PSY_CUSTODY_BILLING_CALC PCBC  																   ");
		stringBuilderSqlBase.append("     																								   ");
		stringBuilderSqlBase.append(" 	WHERE   																						   ");
		stringBuilderSqlBase.append("   		EXTRACT (YEAR FROM PCBC.CUT_DATE) = EXTRACT (YEAR FROM :calculationDate) AND 				");
		stringBuilderSqlBase.append("   		EXTRACT (MONTH FROM PCBC.CUT_DATE)= EXTRACT (MONTH FROM :calculationDate) 					");

		/** Making the table EXCEPTION **/
		parameters.put(GeneralConstants.COUNTER_EXCEPTIONS, 0);
		/*strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters, stringBuilderSqlBase);
		log.debug(" :::::::::::::::::::::::::::::::::::::Consulta Transaccion con Reglas de Excepcion :::::::::::::::::::::::::::: ");
		log.debug(strTableComponentBegin.toString());*/
		Query query = null;
		List<Object> lstRes = null;
		List<Object> lstTempPartByDate = null;
		List<Object> listObject = null;
		try {
			/** IF NUMBER DE EXCEPTIONS EXCEEDS TO CAPACITY LIMIT, A */
			if (Integer.parseInt(parameters.get(GeneralConstants.COUNTER_EXCEPTIONS)
					.toString()) >= GeneralConstants.CAP_LIMIT_EXCEPTIONS) {
				query = em.createNativeQuery(stringBuilderSqlBase.toString());
				//listObject = exceptionLayerServiceBean.filterExceptionByList(parameters, query.getResultList());
				query.setParameter("calculationDate", calculationDate);
				listObject = query.getResultList();
			} else {
				query = em.createNativeQuery(stringBuilderSqlBase.toString());
				query.setParameter("calculationDate", calculationDate);
				listObject = query.getResultList();
			}
		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				log.error(":::::::::QUERY GRAMMAR::::::::::::");
				log.error(ex.getMessage());
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(listObject)) {
			/*
			 * 1. Cargamos la data de tipo de cambio
			 */
			DailyExchangeRateFilter exchFilter = new DailyExchangeRateFilter();
			exchFilter.setMonth(CommonsUtilities.getMonths(calculationDate));
			exchFilter.setYear(CommonsUtilities.getYear(calculationDate));
			exchFilter.setIdCurrency(CurrencyType.USD.getCode());
			exchFilter.setIdSourceinformation(InformationSourceType.BCRD.getCode());
			
			Map<String, DailyExchangeRates> mExch = this.businessLayerCommonFacade.searchAndMapExchangeMoneyTypeRandomListService(exchFilter);
			
			for(Object object : listObject) {
				/* 2. Cargamos la data de cada registro a cobrar encontrado*/
				Long 	   	idParticipantPk		= Long.parseLong(((Object[])object)[0].toString());
				BigDecimal 	amount	   			= ((BigDecimal)((Object[])object)[1]);
				Integer    	currency          	= Integer.parseInt(((BigDecimal)((Object[])object)[2]).toString());
				Date    	cutDate          	= (Date)(((Object[])object)[11]);
							    
				/* 3. Convertimos los montos de guaranies a dolares */
				if(!currency.equals(CurrencyType.USD.getCode())) {
					DailyExchangeRates exchangeRate = this.businessLayerCommonFacade.getExchRateFromMap(mExch, cutDate, Boolean.TRUE);
					
					if(Validations.validateIsNotNull(exchangeRate)) {
						amount = BillingUtils.divideTwoDecimal(amount, exchangeRate.getBuyPrice(), 10);
						currency = CurrencyType.USD.getCode();
					}
				}
				
				/*4. Si el monto tenia tipo de cambio o ya era de USD originalmente, se procesa para sacar el promedio diario */
				if(currency.equals(CurrencyType.USD.getCode())){
					if(Validations.validateIsNull(lstTempPartByDate)) {
						lstTempPartByDate = new ArrayList<Object>();
					}
					
					collectByParticipantAndCutDate(lstTempPartByDate, idParticipantPk, cutDate, amount);
				}
				
				
			}
		}
		/*5. ahora que ya se tiene el consolidado de montos en dolares por cada depositante, por cada dia del mes que hubo custodia.
		 * Se procede a hacer el promedio mensual de custodia por depositante
		 */
		if(Validations.validateListIsNotNullAndNotEmpty(lstTempPartByDate)) {
			lstRes = new ArrayList<Object>();
			
			for(int i=0; i<lstTempPartByDate.size(); i++) {
				boolean isPresent = Boolean.FALSE;
				
				Object[] tempObj = (Object[])lstTempPartByDate.get(i);
				Long 	   	newParticipantPk		= Long.parseLong(tempObj[0].toString());
				BigDecimal 	newAmount	   			= (BigDecimal)(tempObj[2]);
				
				for(int j=0; j<lstRes.size(); j++) {
					Object[] res = (Object[])lstRes.get(j);
					Long 	   	idParticipantPk		= Long.parseLong(res[0].toString());
					BigDecimal 	amount	   			= (BigDecimal)(res[1]);
					Integer 	count	   			= Integer.parseInt(res[3].toString());
					
					if(idParticipantPk.equals(newParticipantPk)) {
						isPresent = Boolean.TRUE;
						res[1] = newAmount.add(amount);
						res[3] = count+1;
					}
				}
				
				if(!isPresent) {
					Object[] newElement = {newParticipantPk, newAmount, CurrencyType.USD.getCode(), 1};
					lstRes.add(newElement);
				}
			}
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstRes)) {
			for(int i=0; i<lstRes.size(); i++) {
				Object[] res = (Object[])lstRes.get(i);
				BigDecimal 	amount	   			= (BigDecimal)(res[1]);
				Integer 	count	   			= (Integer)(res[3]);
				
				res[1] = BillingUtils.divideTwoDecimal(amount, new BigDecimal(count), 10);
			}
		}
		
		return lstRes;
	}
	
	private void collectByParticipantAndCutDate(List<Object> lstRes, Long newParticipantPk, Date newCutDate, BigDecimal newAmount) {
		boolean isPresent = Boolean.FALSE;
		
		for(int i=0; i<lstRes.size(); i++) {
			Object[] res = (Object[])lstRes.get(i);
			Long 	   	idParticipantPk		= Long.parseLong(res[0].toString());
			Date    	cutDate          	= (Date)(res[1]);
			BigDecimal 	amount	   			= (BigDecimal)(res[2]);
			
			if(CommonsUtilities.isEqualDate(cutDate, newCutDate) && newParticipantPk.equals(idParticipantPk)) {
				isPresent = Boolean.TRUE;
				res[2] = amount.add(newAmount);
				break;
			}
		}
		
		if(!isPresent) {
			Object[] newElement = {newParticipantPk, newCutDate, newAmount, CurrencyType.USD.getCode()};
			lstRes.add(newElement);
		}
	}

	/**
	 * EDV 2 Calculation daily <BR>
	 * REGISTRO_VALORES_NO_COLOCADOS_R_FIJA AND <BR>
	 * REGISTRO_VALORES_NO_COLOCADOS_R_VARIABLE.
	 *
	 * @param parameters       the parameters
	 * @param idInstrumentType the id instrument type
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Object> findBalanceSecurityNotPlaced(Map parameters, Integer idInstrumentType) throws ServiceException {

		StringBuilder strTableComponentBegin = new StringBuilder();

		StringBuilder stringBuilderSqlBase = new StringBuilder();

		stringBuilderSqlBase.append("  SELECT DISTINCT ");
		stringBuilderSqlBase.append("    PSD.PLACED_AMOUNT - NVL(PSD.REQUEST_AMOUNT,0) AS NOT_PLACED, ");
		stringBuilderSqlBase.append("    ISS.ID_ISSUER_FK AS ID_ISSUER_FK, 	");
		stringBuilderSqlBase.append("    ISS.ID_ISSUANCE_CODE_PK AS ID_ISSUANCE_CODE_FK, ");
		stringBuilderSqlBase.append("    PSS.ID_PARTICIPANT_FK AS ID_PARTICIPANT_FK, ");
		stringBuilderSqlBase.append("    ISS.CURRENCY AS CURRENCY, ");
		stringBuilderSqlBase.append("    PSD.ID_SECURITY_CODE_FK AS SECURITY_CODE, ");
		stringBuilderSqlBase.append("    ISS.SECURITY_CLASS AS SECURITY_CLASS	");
		stringBuilderSqlBase.append("  FROM ISSUANCE ISS ");
		stringBuilderSqlBase.append("	INNER JOIN PLACEMENT_SEGMENT PS 	");
		stringBuilderSqlBase.append("		ON ISS.ID_ISSUANCE_CODE_PK=PS.ID_ISSUANCE_CODE_FK ");
		stringBuilderSqlBase.append("	INNER JOIN PLACEMENT_SEGMENT_DETAIL PSD	");
		stringBuilderSqlBase.append("		ON PS.ID_PLACEMENT_SEGMENT_PK=PSD.ID_PLACEMENT_SEGMENT_FK ");
		stringBuilderSqlBase.append("	INNER JOIN PLACEMENT_SEG_PARTICIPA_STRUCT PSS	");
		stringBuilderSqlBase.append("		ON PS.ID_PLACEMENT_SEGMENT_PK=PSS.ID_PLACEMENT_SEGMENT_FK ");
		stringBuilderSqlBase.append("	INNER JOIN SECURITY SE	");
		stringBuilderSqlBase.append("		ON SE.ID_SECURITY_CODE_PK= PSD.ID_SECURITY_CODE_FK ");
		stringBuilderSqlBase.append("    ");
		stringBuilderSqlBase.append("  WHERE ISS.INSTRUMENT_TYPE= " + idInstrumentType);// ---124 RENTA FIJA --399 RENTA
																						// VARIABLE
		stringBuilderSqlBase.append("  	 AND ISS.STATE_ISSUANCE= " + IssuanceStateType.AUTHORIZED.getCode());// ---128
																												// REGISTRADO
		stringBuilderSqlBase
				.append("  	 AND PS.STATE_PLACEMENT_SEGMENT= " + PlacementSegmentStateType.OPENED.getCode());// ---582
																												// APERTURADO
		stringBuilderSqlBase.append(
				"    AND PSD.PLACED_AMOUNT - NVL(PSD.REQUEST_AMOUNT,0) > " + GeneralConstants.ZERO_VALUE_INTEGER);// --EXISTA
																													// MONTO
																													// NO
																													// COLOCADO
		stringBuilderSqlBase.append("    AND PSD.PENDING_ANNOTATION=0 ");

		stringBuilderSqlBase
				.append("  ORDER BY PSS.ID_PARTICIPANT_FK, ISS.ID_ISSUER_FK, ISS.ID_ISSUANCE_CODE_PK,  ISS.CURRENCY  ");

		/** Making the table EXCEPTION **/
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters,
				stringBuilderSqlBase);

		log.debug(
				" :::::::::::::::::::::::::::::::::::::Consulta Transaccion con Reglas de Excepcion ::::::::::::::::::::::::::::");
		log.debug(strTableComponentBegin.toString());

		Query query = em.createNativeQuery(strTableComponentBegin.toString());
		List<Object> listObject = null;
		try {

			listObject = query.getResultList();

		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}

		}

		return listObject;
	}

	/**
	 * EDV 3 <BR>
	 * GUARDA_Y_CUSTODIA_DE_VALORES_FISICOS <BR>
	 * GUARDA_Y_CUSTODIA_DE_VALORES_FISICOS_AFP <br>
	 * Calculation daily <br>
	 * 3 GUARDA_Y_CUSTODIA_DE_VALORES_FISICOS.
	 *
	 * @param parameters       the parameters
	 * @param idInstrumentType the id instrument type
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Object> findPhysicalCustodyOfSecurities(Map parameters, Integer idInstrumentType)
			throws ServiceException {

		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		List<Integer> listEconomicAct = (List<Integer>) parameters.get(GeneralConstants.ECONOMIC_ACTIVITY_LIST_INT);
		Integer portFolio = billingService.getPortfolio();

		StringBuilder strTableComponentBegin = new StringBuilder();

		StringBuilder stringBuilderSqlBase = new StringBuilder();
		stringBuilderSqlBase.append(" SELECT QUERY_ALL.* FROM( ");
		stringBuilderSqlBase.append(" SELECT BASE_ALL.*, ");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL.ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)  THEN 1 ELSE 0 END) CARTERA_PROPIA,");
		stringBuilderSqlBase.append(
				"																																																	");
		stringBuilderSqlBase.append(
				" (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) ACT_ECO_PAR,																																				");
		stringBuilderSqlBase.append(
				"                                                                                                                                                                                                                                  						");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL. ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) THEN (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)           ");
		stringBuilderSqlBase.append(
				"      WHEN  BASE_ALL. ID_HOLDER_FK <>  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) and  (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) = 1879   ");
		stringBuilderSqlBase.append(
				"  THEN (SELECT h.ECONOMIC_ACTIVITY from holder h where H.ID_HOLDER_PK = BASE_ALL.ID_HOLDER_FK   AND  STATE_HOLDER = 496 AND h.ECONOMIC_ACTIVITY IN(55,56,57))ELSE NULL END) H_ECONOMIC_ACTIVITY,                                         				");
		stringBuilderSqlBase.append(
				"                                                                                                                                                                                                                                  						");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL. ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) THEN (SELECT MNEMONIC FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)                    ");
		stringBuilderSqlBase.append(
				"      WHEN  BASE_ALL. ID_HOLDER_FK <>  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) and  (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) = 1879   ");
		stringBuilderSqlBase.append(
				" THEN (SELECT h.MNEMONIC from holder h where H.ID_HOLDER_PK = BASE_ALL.ID_HOLDER_FK   AND  STATE_HOLDER = 496 AND h.ECONOMIC_ACTIVITY IN(55,56,57))ELSE NULL END) NEMONICO_HOLDER                                                               				");
		stringBuilderSqlBase.append(" ");
		stringBuilderSqlBase.append(" FROM ( ");
		stringBuilderSqlBase.append("");
		stringBuilderSqlBase.append("");
		stringBuilderSqlBase.append(" SELECT   	PBD.ID_PARTICIPANT_FK AS ID_PARTICIPANT_FK, ");
		stringBuilderSqlBase.append(" 		  			PC.ID_SECURITY_CODE_FK AS ID_SECURITY_CODE_FK, ");
		stringBuilderSqlBase.append(" 		  			S.SECURITY_CLASS AS SECURITY_CLASS, ");
		stringBuilderSqlBase.append("         			PBD.ID_HOLDER_ACCOUNT_FK AS ID_HOLDER_ACCOUNT_FK, ");
		stringBuilderSqlBase
				.append("					(SELECT HO.ID_HOLDER_PK FROM HOLDER_ACCOUNT_DETAIL HAD, HOLDER HO");
		stringBuilderSqlBase.append("					WHERE HAD.ID_HOLDER_ACCOUNT_FK=PBD.ID_HOLDER_ACCOUNT_FK");
		stringBuilderSqlBase
				.append("					AND HO.ID_HOLDER_PK=HAD.ID_HOLDER_FK and rownum=1) AS ID_HOLDER_FK,");
		stringBuilderSqlBase.append("        			PC.ID_ISSUER_FK AS ID_ISSUER_FK,");
		stringBuilderSqlBase.append("         			'' AS ID_ISSUANCE_CODE_FK ,  ");
		stringBuilderSqlBase.append(
				"        			ROUND (S.CURRENT_NOMINAL_VALUE, DECODE (S.SECURITY_CLASS,426,4,2) )  AS NOMINAL_VALUE,   ");
		stringBuilderSqlBase.append("         			S.CURRENCY,   ");
		stringBuilderSqlBase.append("         			PBD.CERTIFICATE_QUANTITY AS TOTAL_BALANCE   ");
		stringBuilderSqlBase.append("   FROM PHYSICAL_BALANCE_DETAIL PBD  ");
		stringBuilderSqlBase.append(
				"   INNER JOIN PHYSICAL_CERTIFICATE PC ON PC.ID_PHYSICAL_CERTIFICATE_PK=PBD.ID_PHYSICAL_CERTIFICATE_FK ");
		stringBuilderSqlBase.append("   INNER JOIN SECURITY S ON S.ID_SECURITY_CODE_PK=PC.ID_SECURITY_CODE_FK ");
		stringBuilderSqlBase.append(" 	WHERE PBD.STATE=" + StateType.DEPOSITED.getCode());// --605 DEPOSITADO

		stringBuilderSqlBase.append("  		AND S.SECURITY_TYPE <> " + SecurityType.PRIVATE.getCode());// ---1763
																										// PRIVADO

		if (Validations.validateIsNotNullAndPositive(idInstrumentType)) {
			stringBuilderSqlBase.append("  		AND PC.INSTRUMENT_TYPE=" + idInstrumentType);// ---124 RENTA FIJA --399
																								// RENTA VARIABLE
		}

		stringBuilderSqlBase.append("   ");
		// stringBuilderSqlBase.append(" ORDER BY PBD.ID_PARTICIPANT_FK ");
		stringBuilderSqlBase.append(" ) BASE_ALL");
		stringBuilderSqlBase.append(" ) QUERY_ALL");
		stringBuilderSqlBase.append(" WHERE 1=1 ");
		if (!BillingServicePortfolioType.ALL.getCode().equals(portFolio)) {
			stringBuilderSqlBase.append(" AND QUERY_ALL.CARTERA_PROPIA=" + BooleanType.get(portFolio).getBinaryValue());
		}
		stringBuilderSqlBase.append(" AND QUERY_ALL.ACT_ECO_PAR IN(:listEconomicAct)");

		/** Making the table EXCEPTION **/
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters,
				stringBuilderSqlBase);

		log.debug(
				" :::::::::::::::::::::::::::::::::::::Consulta Transaccion con Reglas de Excepcion ::::::::::::::::::::::::::::");
		log.debug(strTableComponentBegin.toString());

		Query query = em.createNativeQuery(strTableComponentBegin.toString());
		query.setParameter("listEconomicAct", listEconomicAct);
		List<Object> listObject = null;
		try {

			listObject = query.getResultList();

		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}

		}

		return listObject;
	}

	/**
	 * EDV 24 <br>
	 * CUSTODIA_VALORES_FISICOS_OFERTA_PRIVADA_R_FIJA <br>
	 * CUSTODIA_VALORES_FISICOS_OFERTA_PRIVADA_R_VARIABLE <br>
	 * Calculation daily <br>
	 * 24 CUSTODIA_VALORES_FISICOS_OFERTA_PRIVADA_R_FIJA AND R_VARIABLE .
	 *
	 * @param parameters       the parameters
	 * @param idInstrumentType the id instrument type
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Object> findPhysicalCustodyOfSecuritiesPrivate(Map parameters, Integer idInstrumentType)
			throws ServiceException {
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		List<Integer> listEconomicAct = (List<Integer>) parameters.get(GeneralConstants.ECONOMIC_ACTIVITY_LIST_INT);
		Integer portFolio = billingService.getPortfolio();
		StringBuilder strTableComponentBegin = new StringBuilder();

		StringBuilder stringBuilderSqlBase = new StringBuilder();
		stringBuilderSqlBase.append(" SELECT QUERY_ALL.* FROM( ");
		stringBuilderSqlBase.append(" SELECT BASE_ALL.*, ");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL.ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)  THEN 1 ELSE 0 END) CARTERA_PROPIA,");
		stringBuilderSqlBase.append(
				"																																																	");
		stringBuilderSqlBase.append(
				" (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) ACT_ECO_PAR,																																				");
		stringBuilderSqlBase.append(
				"                                                                                                                                                                                                                                  						");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL. ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) THEN (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)           ");
		stringBuilderSqlBase.append(
				"      WHEN  BASE_ALL. ID_HOLDER_FK <>  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) and  (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) = 1879   ");
		stringBuilderSqlBase.append(
				"  THEN (SELECT h.ECONOMIC_ACTIVITY from holder h where H.ID_HOLDER_PK = BASE_ALL.ID_HOLDER_FK   AND  STATE_HOLDER = 496 AND h.ECONOMIC_ACTIVITY IN(55,56,57))ELSE NULL END) H_ECONOMIC_ACTIVITY,                                         				");
		stringBuilderSqlBase.append(
				"                                                                                                                                                                                                                                  						");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL. ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) THEN (SELECT MNEMONIC FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)                    ");
		stringBuilderSqlBase.append(
				"      WHEN  BASE_ALL. ID_HOLDER_FK <>  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) and  (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) = 1879   ");
		stringBuilderSqlBase.append(
				" THEN (SELECT h.MNEMONIC from holder h where H.ID_HOLDER_PK = BASE_ALL.ID_HOLDER_FK   AND  STATE_HOLDER = 496 AND h.ECONOMIC_ACTIVITY IN(55,56,57))ELSE NULL END) NEMONICO_HOLDER                                                               				");
		stringBuilderSqlBase.append(" ");
		stringBuilderSqlBase.append(" FROM ( ");
		stringBuilderSqlBase.append("");
		stringBuilderSqlBase.append("");
		stringBuilderSqlBase.append(" SELECT PBD.ID_PARTICIPANT_FK AS ID_PARTICIPANT_FK, ");
		stringBuilderSqlBase.append(" 		  			PC.ID_SECURITY_CODE_FK AS ID_SECURITY_CODE_FK, ");
		stringBuilderSqlBase.append(" 		  			S.SECURITY_CLASS AS SECURITY_CLASS, ");
		stringBuilderSqlBase.append("         			PBD.ID_HOLDER_ACCOUNT_FK AS ID_HOLDER_ACCOUNT_FK, ");
		stringBuilderSqlBase
				.append("					(SELECT HO.ID_HOLDER_PK FROM HOLDER_ACCOUNT_DETAIL HAD, HOLDER HO");
		stringBuilderSqlBase.append("					WHERE HAD.ID_HOLDER_ACCOUNT_FK=PBD.ID_HOLDER_ACCOUNT_FK");
		stringBuilderSqlBase
				.append("					AND HO.ID_HOLDER_PK=HAD.ID_HOLDER_FK and rownum=1) AS ID_HOLDER_FK,");
		stringBuilderSqlBase.append("        			PC.ID_ISSUER_FK AS ID_ISSUER_FK,");
		stringBuilderSqlBase.append("         			 '' AS ID_ISSUANCE_CODE_FK ,  ");
		stringBuilderSqlBase.append(
				"        			ROUND ( S.CURRENT_NOMINAL_VALUE, DECODE (S.SECURITY_CLASS,426,4,2) )  AS NOMINAL_VALUE,   ");
		stringBuilderSqlBase.append("         			S.CURRENCY,   ");
		stringBuilderSqlBase.append("         			PBD.CERTIFICATE_QUANTITY AS TOTAL_BALANCE   ");
		stringBuilderSqlBase.append("   FROM PHYSICAL_BALANCE_DETAIL PBD  ");
		stringBuilderSqlBase.append(
				"   INNER JOIN PHYSICAL_CERTIFICATE PC ON PC.ID_PHYSICAL_CERTIFICATE_PK=PBD.ID_PHYSICAL_CERTIFICATE_FK ");
		stringBuilderSqlBase.append("   INNER JOIN SECURITY S ON S.ID_SECURITY_CODE_PK=PC.ID_SECURITY_CODE_FK ");
		stringBuilderSqlBase.append(" 	WHERE PBD.STATE=" + StateType.DEPOSITED.getCode());// DEPOSITADO
		stringBuilderSqlBase.append("  		AND PC.INSTRUMENT_TYPE=" + idInstrumentType);// ---124 RENTA FIJA --399
																							// RENTA VARIABLE
		stringBuilderSqlBase.append("  		AND S.SECURITY_TYPE=" + SecurityType.PRIVATE.getCode());// ---1763 OFERTA
																									// PRIVADA

		stringBuilderSqlBase.append("   ");
		stringBuilderSqlBase.append("  ORDER BY PBD.ID_PARTICIPANT_FK ");

		stringBuilderSqlBase.append(" ) BASE_ALL");
		stringBuilderSqlBase.append(" ) QUERY_ALL");
		stringBuilderSqlBase.append(" WHERE 1=1 ");
		if (!BillingServicePortfolioType.ALL.getCode().equals(portFolio)) {
			stringBuilderSqlBase.append(" AND QUERY_ALL.CARTERA_PROPIA=" + BooleanType.get(portFolio).getBinaryValue());
		}
		stringBuilderSqlBase.append(" AND QUERY_ALL.ACT_ECO_PAR IN(:listEconomicAct)");

		/** Making the table EXCEPTION **/
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters,
				stringBuilderSqlBase);

		log.debug(
				" :::::::::::::::::::::::::::::::::::::Consulta Transaccion con Reglas de Excepcion ::::::::::::::::::::::::::::");
		log.debug(strTableComponentBegin.toString());

		Query query = em.createNativeQuery(strTableComponentBegin.toString());
		query.setParameter("listEconomicAct", listEconomicAct);
		List<Object> listObject = null;
		try {

			listObject = query.getResultList();

		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}

		}

		return listObject;
	}

	/**
	 * LIQUIDACION_OPERACIONES_BURSATILES_R_FIJA
	 * LIQUIDACION_OPERACIONES_BURSATILES_R_VARIABLE
	 * 
	 * 
	 * 4 LIQUIDACION_OPERACIONES_BURSATILES.
	 *
	 * @param parameters             the parameters
	 * @param idInstrumentType       the id instrument type
	 * @param idNegotiationMechanism the id negotiation mechanism
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Object> findMovementSettlementWithMechanismByEntityCollection(Map parameters, Integer idInstrumentType,
			Long idNegotiationMechanism) throws ServiceException {

		StringBuilder strTableComponentBegin = new StringBuilder();

		/** getting data for do merging into dynamic query **/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		List<Integer> listEconomicAct = (List<Integer>) parameters.get(GeneralConstants.ECONOMIC_ACTIVITY_LIST_INT);
		Integer portFolio = billingService.getPortfolio();

		/** getting data for do merging into dynamic query **/
		ServiceRate serviceRate = (ServiceRate) parameters.get(GeneralConstants.SERVICE_RATE);

		/** Range to Dates */
		Date initialDate = (Date) parameters.get(GeneralConstants.FIELD_INITIAL_DATE);
		Date finalDate = (Date) parameters.get(GeneralConstants.FIELD_FINAL_DATE);

		List<RateMovement> rateMovementList = serviceRate.getRateMovements();

		StringBuilder stringBuilderSqlBase = new StringBuilder();
		StringBuilder sBPrimaryPlacement = new StringBuilder();

		sBPrimaryPlacement.append("	SELECT  DISTINCT	");
		sBPrimaryPlacement.append("		HAO.ID_HOLDER_ACCOUNT_FK AS ID_HOLDER_ACCOUNT_FK, ");
		sBPrimaryPlacement.append("		(SELECT HO.ID_HOLDER_PK FROM HOLDER_ACCOUNT_DETAIL HAD, HOLDER HO");
		sBPrimaryPlacement.append("		WHERE HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK");
		sBPrimaryPlacement.append("		AND HO.ID_HOLDER_PK=HAD.ID_HOLDER_FK and rownum=1) AS ID_HOLDER_FK,");
		sBPrimaryPlacement.append("		MO.ID_SELLER_PARTICIPANT_FK AS ID_PARTICIPANT_FK , ");
		sBPrimaryPlacement.append("		S.ID_SECURITY_CODE_PK  AS ID_SECURITY_CODE_FK, ");
		sBPrimaryPlacement.append("		S.ID_ISSUER_FK AS ID_ISSUER_FK, ");
		sBPrimaryPlacement.append("		S.SECURITY_CLASS AS SECURITY_CLASS  , ");
		sBPrimaryPlacement.append("		DECODE (S.INSTRUMENT_TYPE, 124, 300264, ");
		sBPrimaryPlacement.append("				399, 300270)  AS ID_MOVEMENT_TYPE_FK , ");
		sBPrimaryPlacement.append("		SO.SETTLEMENT_CURRENCY  AS CURRENCY, ");
		sBPrimaryPlacement.append("		MO.OPERATION_DATE  as MOVEMENT_DATE, ");
		sBPrimaryPlacement.append("		ROUND(SO.SETTLEMENT_PRICE,2)  AS OPERATION_PRICE, ");// MO.CASH_PRICE
		sBPrimaryPlacement.append("		MO.STOCK_QUANTITY  AS MOVEMENT_QUANTITY, ");
		sBPrimaryPlacement.append("		S.ID_ISSUANCE_CODE_FK AS ID_ISSUANCE_CODE_FK, ");
		sBPrimaryPlacement.append("		MO.OPERATION_DATE  AS OPERATION_DATE, ");
		sBPrimaryPlacement.append("		MO.OPERATION_NUMBER AS OPERATION_NUMBER, ");
		sBPrimaryPlacement.append("		MO.ID_BUYER_PARTICIPANT_FK AS ID_SOURCE_PARTICIPANT_FK, ");
		sBPrimaryPlacement.append("		MO.ID_SELLER_PARTICIPANT_FK AS ID_TARGET_PARTICIPANT_FK, ");
		sBPrimaryPlacement.append("		MO.ID_NEGOTIATION_MODALITY_FK AS ID_NEGOTIATION_MODALITY_FK, ");
		sBPrimaryPlacement.append("		MO.BALLOT_NUMBER||'/'||MO.SEQUENTIAL AS BALLOT_SEQUENTIAL, ");
		sBPrimaryPlacement.append("		HAO.ROLE AS ROLE");
		sBPrimaryPlacement.append("		,1 AS ID_HOLDER_ACCOUNT_MOVEMENT_PK");
		sBPrimaryPlacement.append("	FROM MECHANISM_OPERATION MO 	");
		sBPrimaryPlacement.append("	INNER JOIN SECURITY S on ( MO.ID_SECURITY_CODE_FK=S.ID_SECURITY_CODE_PK )	");
		sBPrimaryPlacement.append("	INNER JOIN ISSUER I on (I.ID_ISSUER_PK=S.ID_ISSUER_FK)	");
		sBPrimaryPlacement.append(
				"	INNER JOIN HOLDER_ACCOUNT_OPERATION HAO ON (HAO.ID_MECHANISM_OPERATION_FK=MO.ID_MECHANISM_OPERATION_PK)	");
		sBPrimaryPlacement
				.append("	INNER JOIN HOLDER_ACCOUNT HA on (HA.ID_HOLDER_ACCOUNT_PK = HAO.ID_HOLDER_ACCOUNT_FK)	");
		sBPrimaryPlacement.append(
				" INNER JOIN SETTLEMENT_ACCOUNT_OPERATION SAO ON (SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = HAO.ID_HOLDER_ACCOUNT_OPERATION_PK) ");
		sBPrimaryPlacement.append(
				" INNER JOIN SETTLEMENT_OPERATION SO on MO.ID_MECHANISM_OPERATION_PK=SO.ID_MECHANISM_OPERATION_FK");
		sBPrimaryPlacement.append(
				" INNER JOIN SETTLEMENT_OPERATION SOP on SAO.ID_SETTLEMENT_OPERATION_FK=SO.ID_SETTLEMENT_OPERATION_PK");
		sBPrimaryPlacement.append("	WHERE  ");
		sBPrimaryPlacement.append("		HAO.ROLE=").append(DailyExchangeRoleType.SELL.getCode());
		sBPrimaryPlacement.append("		AND HAO.OPERATION_PART=").append(OperationPartType.CASH_PART.getCode());
		sBPrimaryPlacement.append("		AND SO.OPERATION_PART=").append(OperationPartType.CASH_PART.getCode());
		sBPrimaryPlacement.append("		AND S.INSTRUMENT_TYPE=").append(idInstrumentType);
		sBPrimaryPlacement.append("		AND ( MO.ID_NEGOTIATION_MODALITY_FK=")
				.append(NegotiationModalityType.PRIMARY_PLACEMENT_FIXED_INCOME.getCode())
				.append("	OR  MO.ID_NEGOTIATION_MODALITY_FK=")
				.append(NegotiationModalityType.PRIMARY_PLACEMENT_EQUITIES.getCode()).append(" )");
		if (Validations.validateIsNotNull(idNegotiationMechanism)) {
			sBPrimaryPlacement.append("  AND MO.ID_NEGOTIATION_MECHANISM_FK=:idNegotiationMechanism ");
		}
		if (Validations.validateIsNotNull(initialDate) && Validations.validateIsNotNull(finalDate)) {
			sBPrimaryPlacement
					.append("		AND  MO.OPERATION_DATE >= :initialDate AND MO.OPERATION_DATE<= :finalDate ");

		}

		stringBuilderSqlBase.append(" SELECT QUERY_ALL.* FROM( ");
		stringBuilderSqlBase.append(" SELECT BASE_ALL.*, ");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL.ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)  THEN 1 ELSE 0 END) CARTERA_PROPIA,");
		stringBuilderSqlBase.append(
				"																																																	");
		stringBuilderSqlBase.append(
				" (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) ACT_ECO_PAR,																																				");
		stringBuilderSqlBase.append(
				"                                                                                                                                                                                                                                  						");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL. ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) THEN (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)           ");
		stringBuilderSqlBase.append(
				"      WHEN  BASE_ALL. ID_HOLDER_FK <>  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) and  (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) = 1879   ");
		stringBuilderSqlBase.append(
				"  THEN (SELECT h.ECONOMIC_ACTIVITY from holder h where H.ID_HOLDER_PK = BASE_ALL.ID_HOLDER_FK   AND  STATE_HOLDER = 496 AND h.ECONOMIC_ACTIVITY IN(55,56,57))ELSE NULL END) H_ECONOMIC_ACTIVITY,                                         				");
		stringBuilderSqlBase.append(
				"                                                                                                                                                                                                                                  						");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL. ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) THEN (SELECT MNEMONIC FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)                    ");
		stringBuilderSqlBase.append(
				"      WHEN  BASE_ALL. ID_HOLDER_FK <>  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) and  (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) = 1879   ");
		stringBuilderSqlBase.append(
				" THEN (SELECT h.MNEMONIC from holder h where H.ID_HOLDER_PK = BASE_ALL.ID_HOLDER_FK   AND  STATE_HOLDER = 496 AND h.ECONOMIC_ACTIVITY IN(55,56,57))ELSE NULL END) NEMONICO_HOLDER                                                               				");
		stringBuilderSqlBase.append(" ");
		stringBuilderSqlBase.append(" FROM ( ");
		stringBuilderSqlBase.append("");
		stringBuilderSqlBase.append("");
		stringBuilderSqlBase.append(" SELECT DISTINCT ");
		stringBuilderSqlBase.append(" 		  DECODE(HAO.IND_INCHARGE,0,HA.ID_HOLDER_ACCOUNT_PK, ");
		stringBuilderSqlBase.append(" 		  	(SELECT HAO2.ID_HOLDER_ACCOUNT_FK ");
		stringBuilderSqlBase.append(" 		  	FROM HOLDER_ACCOUNT_OPERATION HAO2, HOLDER_ACCOUNT HA2 ");
		stringBuilderSqlBase.append(" 		  WHERE HAO2.ID_HOLDER_ACCOUNT_FK = HA2.ID_HOLDER_ACCOUNT_PK");
		stringBuilderSqlBase.append(" 		  AND HAO2.ID_MECHANISM_OPERATION_FK = MO.ID_MECHANISM_OPERATION_PK");
		stringBuilderSqlBase.append(" 		  AND HAO2.HOLDER_ACCOUNT_STATE= "
				+ HolderAccountOperationStateType.MODIFIED_ACCOUNT_OPERATION_STATE.getCode());
		stringBuilderSqlBase.append(" 		  AND (HA2.ID_PARTICIPANT_FK = MO.ID_SELLER_PARTICIPANT_FK");
		stringBuilderSqlBase.append(" 		  AND HAO2.ROLE=2 OR");
		stringBuilderSqlBase.append(" 		  HA2.ID_PARTICIPANT_FK = MO.ID_BUYER_PARTICIPANT_FK AND HAO2.ROLE=1))) ");
		stringBuilderSqlBase.append(" 		  AS ID_HOLDER_ACCOUNT_FK,");
		stringBuilderSqlBase.append("		  (SELECT HO.ID_HOLDER_PK FROM HOLDER_ACCOUNT_DETAIL HAD, HOLDER HO ");
		stringBuilderSqlBase.append("		  WHERE HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK ");
		stringBuilderSqlBase.append("		  AND HO.ID_HOLDER_PK=HAD.ID_HOLDER_FK and rownum=1) AS ID_HOLDER_FK, ");
		stringBuilderSqlBase.append(" 		  DECODE ( SAO.ROLE ,  ").append(DailyExchangeRoleType.BUY.getCode())
				.append(" , MO.ID_BUYER_PARTICIPANT_FK, ").append(DailyExchangeRoleType.SELL.getCode())
				.append(",  MO.ID_SELLER_PARTICIPANT_FK ) AS ID_PARTICIPANT_FK, ");
		stringBuilderSqlBase.append("         S.ID_SECURITY_CODE_PK as ID_SECURITY_CODE_FK,");
		stringBuilderSqlBase.append("         S.ID_ISSUER_FK AS ID_ISSUER_FK ,");
		stringBuilderSqlBase.append("         S.SECURITY_CLASS AS SECURITY_CLASS  ,");
		stringBuilderSqlBase.append("         HAM.ID_MOVEMENT_TYPE_FK AS ID_MOVEMENT_TYPE_FK ,");
		stringBuilderSqlBase.append("         SO.SETTLEMENT_CURRENCY  AS CURRENCY,");
		stringBuilderSqlBase.append("         HAM.MOVEMENT_DATE as MOVEMENT_DATE,");
		stringBuilderSqlBase.append("         HAM.OPERATION_PRICE,");
		stringBuilderSqlBase.append("         HAM.MOVEMENT_QUANTITY AS MOVEMENT_QUANTITY, ");
		stringBuilderSqlBase.append("         S.ID_ISSUANCE_CODE_FK AS ID_ISSUANCE_CODE_FK, ");
		stringBuilderSqlBase.append("  		  HAM.MOVEMENT_DATE AS OPERATION_DATE  ,   ");
		stringBuilderSqlBase.append("		  NVL(HAM.OPERATION_NUMBER, 0) AS OPERATION_NUMBER,");
		stringBuilderSqlBase.append("  		  NVL(HAM.ID_SOURCE_PARTICIPANT, 0) as ID_SOURCE_PARTICIPANT_FK	 , ");
		stringBuilderSqlBase.append("		  NVL(HAM.ID_TARGET_PARTICIPANT, 0) AS ID_TARGET_PARTICIPANT_FK,   ");
		stringBuilderSqlBase.append("		  MO.ID_NEGOTIATION_MODALITY_FK AS ID_NEGOTIATION_MODALITY_FK,		");
		stringBuilderSqlBase.append("		  MO.BALLOT_NUMBER||'/'||MO.SEQUENTIAL AS BALLOT_SEQUENTIAL,  ");
		stringBuilderSqlBase.append("		  HAO.ROLE AS ROLE");
		stringBuilderSqlBase.append("		   , HAM.ID_HOLDER_ACCOUNT_MOVEMENT_PK AS ID_HOLDER_ACCOUNT_MOVEMENT_PK");
		stringBuilderSqlBase.append(" FROM HOLDER_ACCOUNT_MOVEMENT HAM ");
		stringBuilderSqlBase.append(
				" INNER JOIN MECHANISM_OPERATION MO on ( MO.ID_MECHANISM_OPERATION_PK=HAM.ID_TRADE_OPERATION_FK ) ");
		stringBuilderSqlBase.append(
				" INNER JOIN HOLDER_ACCOUNT_OPERATION HAO    on HAO.ID_MECHANISM_OPERATION_FK=MO.ID_MECHANISM_OPERATION_PK");
		stringBuilderSqlBase
				.append(" INNER JOIN MOVEMENT_TYPE MT ON (MT.ID_MOVEMENT_TYPE_PK = HAM.ID_MOVEMENT_TYPE_FK) ");
		stringBuilderSqlBase
				.append(" INNER JOIN HOLDER_ACCOUNT HA on (HA.ID_HOLDER_ACCOUNT_PK = HAM.ID_HOLDER_ACCOUNT_FK) ");
		stringBuilderSqlBase.append(" INNER JOIN SECURITY S on ( HAM.ID_SECURITY_CODE_FK=S.ID_SECURITY_CODE_PK ) ");
		stringBuilderSqlBase.append(" INNER JOIN ISSUER I on (I.ID_ISSUER_PK=S.ID_ISSUER_FK) ");
		stringBuilderSqlBase.append(
				" INNER JOIN SETTLEMENT_ACCOUNT_OPERATION SAO ON (SAO.ID_HOLDER_ACCOUNT_OPERATION_FK = HAO.ID_HOLDER_ACCOUNT_OPERATION_PK) ");
		stringBuilderSqlBase.append(
				" INNER JOIN SETTLEMENT_OPERATION SO ON MO.ID_MECHANISM_OPERATION_PK=SO.ID_MECHANISM_OPERATION_FK ");
		stringBuilderSqlBase.append(" ");
		stringBuilderSqlBase.append(" WHERE  ");
		stringBuilderSqlBase.append(" 		S.INSTRUMENT_TYPE=").append(idInstrumentType);// ---124 RENTA FIJA --399
																							// RENTA VARIABLE
		stringBuilderSqlBase.append(" 		AND HAO.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK");
		stringBuilderSqlBase.append(" 		AND HAO.OPERATION_PART=").append(OperationPartType.CASH_PART.getCode());
		stringBuilderSqlBase.append("		AND SO.OPERATION_PART=").append(OperationPartType.CASH_PART.getCode());
		stringBuilderSqlBase.append(" 		AND HAO.HOLDER_ACCOUNT_STATE=")
				.append(HolderAccountOperationStateType.CONFIRMED.getCode());
		stringBuilderSqlBase.append(" 		AND SAO.ID_SETTLEMENT_OPERATION_FK=SO.ID_SETTLEMENT_OPERATION_PK");

		if (Validations.validateIsNotNull(idNegotiationMechanism)) {
			stringBuilderSqlBase.append("  AND HAM.ID_MECHANISM=:idNegotiationMechanism ");
		}

		if (Validations.validateListIsNotNullAndNotEmpty(rateMovementList)) {

			stringBuilderSqlBase.append(" AND HAM.ID_MOVEMENT_TYPE_FK in (");
			StringBuilder stringBuilderMovements = new StringBuilder();

			for (int i = 0; i < rateMovementList.size(); i++) {
				RateMovement rateMovement = rateMovementList.get(i);
				String caracter = (i == 0 ? " " : ",");
				stringBuilderMovements.append(caracter + rateMovement.getMovementType().getIdMovementTypePk() + " ");
			}

			stringBuilderSqlBase.append(stringBuilderMovements.toString()).append(" )");
		}
		/** structure ::::::::: and HAM.ID_MOVEMENT_TYPE_FK in ( 1,2,3,4) */

		if (Validations.validateIsNotNull(initialDate) && Validations.validateIsNotNull(finalDate)) {
			stringBuilderSqlBase
					.append(" AND  HAM.MOVEMENT_DATE >= :initialDate  AND HAM.MOVEMENT_DATE<= :finalDate   ");
		}

		stringBuilderSqlBase.append(PropertiesConstants.CONNECTOR_UNION).append(sBPrimaryPlacement);

		stringBuilderSqlBase.append(" ) BASE_ALL");
		stringBuilderSqlBase.append(" ) QUERY_ALL");
		stringBuilderSqlBase.append(" WHERE 1=1 ");
		if (!BillingServicePortfolioType.ALL.getCode().equals(portFolio)) {
			stringBuilderSqlBase.append(" AND QUERY_ALL.CARTERA_PROPIA=" + BooleanType.get(portFolio).getBinaryValue());
		}
		stringBuilderSqlBase.append(" AND QUERY_ALL.ACT_ECO_PAR IN(:listEconomicAct)");

		/** Making the table EXCEPTION **/
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters,
				stringBuilderSqlBase);

		log.debug(
				" :::::::::::::::::::::::::::::::::::::Consulta Transaccion con Reglas de Excepcion ::::::::::::::::::::::::::::");
		log.debug(strTableComponentBegin.toString());

		Query query = em.createNativeQuery(strTableComponentBegin.toString());
		query.setParameter("listEconomicAct", listEconomicAct);

		if (Validations.validateIsNotNull(initialDate) && Validations.validateIsNotNull(finalDate)) {
			query.setParameter(GeneralConstants.FIELD_INITIAL_DATE, initialDate);
			query.setParameter(GeneralConstants.FIELD_FINAL_DATE, finalDate);
		}

		if (Validations.validateIsNotNull(idNegotiationMechanism)) {
			query.setParameter("idNegotiationMechanism", idNegotiationMechanism);
		}

		List<Object> listObject = null;
		try {

			listObject = query.getResultList();

		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}

		}

		return listObject;
	}

	/**
	 * 
	 *  9.	Registro de Depositante Directo  - 10. Registro de Depositante Indirecto 
	 *  
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	/** 5 **/
	public List<Object> findRegisteredParticipants(Map parameters) throws ServiceException {

		StringBuilder strTableComponentBegin = new StringBuilder();

		/** getting data for do merging into dynamic query **/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		Date calculateDate = (Date) parameters.get(GeneralConstants.CALCULATION_DATE);
		String entityTipe;

		StringBuilder stringBuilderSqlBase = new StringBuilder();
		stringBuilderSqlBase.append("SELECT  P.ID_PARTICIPANT_PK AS ID_PARTICIPANT_FK,  ");
		stringBuilderSqlBase
				.append("  '' AS ID_SECURITY_CODE_FK, '' AS SECURITY_CLASS, '' AS ID_HOLDER_ACCOUNT_FK,   ");
		stringBuilderSqlBase.append("  '' AS ID_HOLDER_FK, '' AS ID_ISSUER_FK, '' AS ID_ISSUANCE_CODE_FK  ");
		stringBuilderSqlBase.append(" FROM PARTICIPANT P ");
		stringBuilderSqlBase.append(" WHERE 1=1  ");
		/** and STATE=6 Only Active ***/

		if (billingService.getCalculationPeriod().equals(PropertiesConstants.PERIOD_CALCULATION_DAILY_PK)) {

			stringBuilderSqlBase.append(" and to_char(p.REGISTRY_DATE,'yyyy/MM/dd')=:dateCalculationPeriod ");

		} else if (billingService.getCalculationPeriod().equals(PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK)) {

			stringBuilderSqlBase.append(" and to_char(p.REGISTRY_DATE,'YYYY/MM')=:dateCalculationPeriod ");
		} else if (billingService.getCalculationPeriod().equals(PropertiesConstants.PERIOD_CALCULATION_YEARLY_PK)) {

			stringBuilderSqlBase.append(" and to_char(p.REGISTRY_DATE,'YYYY')=:dateCalculationPeriod ");
		}

		entityTipe = "FROM PARTICIPANT";

		parameters.put("entidad", entityTipe);

		/** Making the table EXCEPTION **/
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters,
				stringBuilderSqlBase);

		log.debug(
				" :::::::::::::::::::::::::::::::::::::Consulta Transaccion con Reglas de Excepcion ::::::::::::::::::::::::::::");
		log.debug(strTableComponentBegin.toString());

		Query query = em.createNativeQuery(strTableComponentBegin.toString());

		if (billingService.getCalculationPeriod().equals(PropertiesConstants.PERIOD_CALCULATION_DAILY_PK)) {

			query.setParameter("dateCalculationPeriod",
					CommonsUtilities.convertDateToString(calculateDate, BillingUtils.DATE_PATTERN_yyyy_MM_dd));

		} else if (billingService.getCalculationPeriod().equals(PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK)) {
			String dateCalculationPeriod = CommonsUtilities.convertDateToString(calculateDate,
					BillingUtils.DATE_PATTERN_yyyy_MM);
			query.setParameter("dateCalculationPeriod", dateCalculationPeriod);
		}

		List<Object> listObject = null;
		try {

			listObject = query.getResultList();

		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}

		}

		return listObject;
	}

	/**
	 * Calculation daily or monthly MANTENIMIENTO_CUENTA_EMISOR.
	 *
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	/** 6 **/
	public List<Object> findRegisteredIssuer(Map parameters) throws ServiceException {

		StringBuilder strTableComponentBegin = new StringBuilder();

		/** getting data for do merging into dynamic query **/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		Date calculateDate = (Date) parameters.get(GeneralConstants.CALCULATION_DATE);
		String entityType;

		StringBuilder stringBuilderSqlBase = new StringBuilder();
		stringBuilderSqlBase.append("select  I.ID_ISSUER_PK AS ID_ISSUER_FK , '' as ID_PARTICIPANT_FK ");
		stringBuilderSqlBase.append(
				" , '' AS ID_SECURITY_CODE_FK, '' AS SECURITY_CLASS, '' AS ID_HOLDER_ACCOUNT_FK, '' AS ID_HOLDER_FK, '' AS ID_ISSUANCE_CODE_FK ");
		stringBuilderSqlBase.append(" FROM ISSUER I ");
		stringBuilderSqlBase.append(" WHERE  1=1   ");
		/** I.STATE_ISSUER=578 Only Active ***/

		if (billingService.getCalculationPeriod().equals(PropertiesConstants.PERIOD_CALCULATION_DAILY_PK)) {
			stringBuilderSqlBase.append(" and to_char(I.REGISTRY_DATE,'yyyy/MM/dd')=:dateCalculationPeriod ");
		} else if (billingService.getCalculationPeriod().equals(PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK)) {
			stringBuilderSqlBase.append(" and to_char(I.REGISTRY_DATE,'YYYY/MM')=:dateCalculationPeriod ");
		}

		entityType = "FROM ISSUER";

		parameters.put("entidad", entityType);
		/** Making the table EXCEPTION **/
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters,
				stringBuilderSqlBase);

		log.debug(
				" :::::::::::::::::::::::::::::::::::::Consulta Transaccion con Reglas de Excepcion ::::::::::::::::::::::::::::");
		log.debug(strTableComponentBegin.toString());

		Query query = em.createNativeQuery(strTableComponentBegin.toString());

		if (billingService.getCalculationPeriod().equals(PropertiesConstants.PERIOD_CALCULATION_DAILY_PK)) {

			query.setParameter("dateCalculationPeriod",
					CommonsUtilities.convertDateToString(calculateDate, BillingUtils.DATE_PATTERN_yyyy_MM_dd));

		} else if (billingService.getCalculationPeriod().equals(PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK)) {
			String dateCalculationPeriod = CommonsUtilities.convertDateToString(calculateDate,
					BillingUtils.DATE_PATTERN_yyyy_MM);
			query.setParameter("dateCalculationPeriod", dateCalculationPeriod);
		}

		List<Object> listObject = null;
		try {

			listObject = query.getResultList();

		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}

		}

		return listObject;
	}

	/**
	 * 8 REGISTRO SIRTEX *.
	 *
	 * @param parameters             the parameters
	 * @param idInstrumentType       the id instrument type
	 * @param idNegotiationMechanism the id negotiation mechanism
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Object> findMovementSecuritiesTransferSirtex(Map parameters, Integer idInstrumentType,
			Long idNegotiationMechanism) throws ServiceException {

		StringBuilder strTableComponentBegin = new StringBuilder();

		/** getting data for do merging into dynamic query **/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		List<Integer> listEconomicAct = (List<Integer>) parameters.get(GeneralConstants.ECONOMIC_ACTIVITY_LIST_INT);
		Integer portFolio = billingService.getPortfolio();

		/** getting data for do merging into dynamic query **/
		ServiceRate serviceRate = (ServiceRate) parameters.get(GeneralConstants.SERVICE_RATE);

		/** Range to Dates */
		Date initialDate = (Date) parameters.get(GeneralConstants.FIELD_INITIAL_DATE);
		Date finalDate = (Date) parameters.get(GeneralConstants.FIELD_FINAL_DATE);

		String participantBuyer = " SELECT  TR.ID_BUYER_PARTICIPANT_FK as ID_PARTICIPANT_FK, '1' AS ROLE, ";
		String participantSeller = " SELECT  TR.ID_SELLER_PARTICIPANT_FK as ID_PARTICIPANT_FK, '2' AS ROLE, ";

		StringBuilder queryBase = new StringBuilder();

		StringBuilder sbSqlBase = new StringBuilder();

		queryBase.append(" 		  TR.STOCK_QUANTITY AS TOTAL_BALANCE, ");
		queryBase.append(" 		  S.ID_SECURITY_CODE_PK AS ID_SECURITY_CODE_FK,  ");
		queryBase.append("         S.CURRENCY AS CURRENCY, ");
		queryBase.append(" 		DECODE(S.SECURITY_CLASS,426,                                                     ");
		queryBase.append(" 		ROUND(DECODE(S.STATE_SECURITY,131,S.CURRENT_NOMINAL_VALUE,                       ");
		queryBase.append(" 		FN_NOMINAL_PORTFOLIO(S.ID_SECURITY_CODE_PK , TR.OPERATION_DATE)), 4),            ");
		queryBase.append(" 		ROUND(DECODE(S.STATE_SECURITY,131,S.CURRENT_NOMINAL_VALUE,                       ");
		queryBase.append(
				" 		FN_NOMINAL_PORTFOLIO(S.ID_SECURITY_CODE_PK , TR.OPERATION_DATE)), 2)) AS NOMINAL_VALUE,    ");
		queryBase.append("                                                                                       ");
		queryBase.append("         S.SECURITY_CLASS as SECURITY_CLASS  ,");
		queryBase.append("         S.ID_ISSUER_FK as ID_ISSUER_FK ,");
		queryBase.append("         TR.OPERATION_DATE as OPERATION_DATE,");
		queryBase.append("         '' AS ID_ISSUANCE_CODE_FK, ");
		queryBase.append("		   TR.ID_TRADE_REQUEST_PK AS OPERATION_NUMBER, ");
		queryBase.append("			(SELECT HO.ID_HOLDER_PK FROM HOLDER_ACCOUNT_DETAIL HAD, HOLDER HO");
		queryBase.append("			WHERE HAD.ID_HOLDER_ACCOUNT_FK=ATR.ID_HOLDER_ACCOUNT_FK");
		queryBase.append("			AND HO.ID_HOLDER_PK=HAD.ID_HOLDER_FK and rownum=1) AS ID_HOLDER_FK");
		queryBase.append(" FROM TRADE_REQUEST TR  ");
		queryBase.append(" 		INNER JOIN ACCOUNT_TRADE_REQUEST ATR ");
		queryBase.append(" 		on atr.ID_TRADE_REQUEST_FK=tr.ID_TRADE_REQUEST_PK ");
		queryBase.append(" 		INNER JOIN SECURITY S ");
		queryBase.append(" 		on ( TR.ID_SECURITY_CODE_FK=S.ID_SECURITY_CODE_PK ) ");
		queryBase.append(" WHERE 1=1  ");
		queryBase.append(" 		AND S.INSTRUMENT_TYPE=" + idInstrumentType);// ---124 RENTA FIJA --399 RENTA VARIABLE
		queryBase.append(" 		AND ATR.ROLE= ").append(ParticipantRoleType.SELL.getCode());
		if (Validations.validateIsNotNull(idNegotiationMechanism)) {
			queryBase.append("  AND TR.ID_NEGOTIATION_MECHANISM_FK=:idNegotiationMechanism ");
		}
		queryBase.append(" AND TR.OPERATION_STATE=  " + SirtexOperationStateType.CONFIRMED.getCode());// 1866 Confirmada

		if (Validations.validateIsNotNull(initialDate) && Validations.validateIsNotNull(finalDate)) {
			queryBase.append(" AND  TR.OPERATION_DATE >= :initialDate  AND TR.OPERATION_DATE<= :finalDate   ");
		}

		sbSqlBase.append(" SELECT QUERY_ALL.* FROM( ");
		sbSqlBase.append(" SELECT BASE_ALL.*, ");
		sbSqlBase.append(
				" (CASE WHEN BASE_ALL.ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)  THEN 1 ELSE 0 END) CARTERA_PROPIA,");
		sbSqlBase.append(
				"																																																	");
		sbSqlBase.append(
				" (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) ACT_ECO_PAR,																																				");
		sbSqlBase.append(
				"                                                                                                                                                                                                                                  						");
		sbSqlBase.append(
				" (CASE WHEN BASE_ALL. ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) THEN (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)           ");
		sbSqlBase.append(
				"      WHEN  BASE_ALL. ID_HOLDER_FK <>  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) and  (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) = 1879   ");
		sbSqlBase.append(
				"  THEN (SELECT h.ECONOMIC_ACTIVITY from holder h where H.ID_HOLDER_PK = BASE_ALL.ID_HOLDER_FK   AND  STATE_HOLDER = 496 AND h.ECONOMIC_ACTIVITY IN(55,56,57))ELSE NULL END) H_ECONOMIC_ACTIVITY,                                         				");
		sbSqlBase.append(
				"                                                                                                                                                                                                                                  						");
		sbSqlBase.append(
				" (CASE WHEN BASE_ALL. ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) THEN (SELECT MNEMONIC FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)                    ");
		sbSqlBase.append(
				"      WHEN  BASE_ALL. ID_HOLDER_FK <>  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) and  (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) = 1879   ");
		sbSqlBase.append(
				" THEN (SELECT h.MNEMONIC from holder h where H.ID_HOLDER_PK = BASE_ALL.ID_HOLDER_FK   AND  STATE_HOLDER = 496 AND h.ECONOMIC_ACTIVITY IN(55,56,57))ELSE NULL END) NEMONICO_HOLDER                                                               				");
		sbSqlBase.append(" ");
		sbSqlBase.append(" FROM ( ");
		sbSqlBase.append("");
		sbSqlBase.append("");
		sbSqlBase.append(participantBuyer).append(queryBase).append(PropertiesConstants.CONNECTOR_UNION)
				.append(participantSeller).append(queryBase);
		sbSqlBase.append(" ) BASE_ALL");
		sbSqlBase.append(" ) QUERY_ALL");
		sbSqlBase.append(" WHERE 1=1 ");
		if (!BillingServicePortfolioType.ALL.getCode().equals(portFolio)) {
			sbSqlBase.append(" AND QUERY_ALL.CARTERA_PROPIA=" + BooleanType.get(portFolio).getBinaryValue());
		}
		sbSqlBase.append(" AND QUERY_ALL.ACT_ECO_PAR IN(:listEconomicAct)");

		/** Making the table EXCEPTION **/
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters, sbSqlBase);

		log.debug(
				" :::::::::::::::::::::::::::::::::::::Consulta Transaccion con Reglas de Excepcion ::::::::::::::::::::::::::::");
		log.debug(strTableComponentBegin.toString());

		Query query = em.createNativeQuery(strTableComponentBegin.toString());
		if (Validations.validateIsNotNull(initialDate) && Validations.validateIsNotNull(finalDate)) {

			query.setParameter(GeneralConstants.FIELD_INITIAL_DATE, initialDate);
			query.setParameter(GeneralConstants.FIELD_FINAL_DATE, finalDate);
		}

		if (Validations.validateIsNotNull(idNegotiationMechanism)) {
			query.setParameter("idNegotiationMechanism", idNegotiationMechanism);
		}
		query.setParameter("listEconomicAct", listEconomicAct);

		List<Object> listObject = null;
		try {

			listObject = query.getResultList();

		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}

		}

		return listObject;
	}

	/**
	 * 9 REGISTRO TRANSFERENCIAS DE VALORES ANOTADOS EN CUENTA, OPERACIONES
	 * EXTRABURSATILES PRIVADAS
	 * TRANSFERENCIAS_VALORES_OPERACIONES_EXTRABURSATILES_PRIVADAS_R_FIJA
	 * TRANSFERENCIAS_VALORES_OPERACIONES_EXTRABURSATILES_PRIVADAS_R_VARIABLE.
	 *
	 * @param parameters             the parameters
	 * @param idInstrumentType       the id instrument type
	 * @param idNegotiationMechanism the id negotiation mechanism
	 * @return List<Object>
	 * @throws ServiceException the service exception
	 */
	public List<Object> findMovementPrivateSecuritiesTransferOperationsOtc(Map parameters, Integer idInstrumentType,
			Long idNegotiationMechanism) throws ServiceException {

		StringBuilder strTableComponentBegin = new StringBuilder();

		/** getting data for do merging into dynamic query **/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		List<Integer> listEconomicAct = (List<Integer>) parameters.get(GeneralConstants.ECONOMIC_ACTIVITY_LIST_INT);
		Integer portFolio = billingService.getPortfolio();

		/** getting data for do merging into dynamic query **/
		ServiceRate serviceRate = (ServiceRate) parameters.get(GeneralConstants.SERVICE_RATE);

		/** Range to Dates */
		Date initialDate = (Date) parameters.get(GeneralConstants.FIELD_INITIAL_DATE);
		Date finalDate = (Date) parameters.get(GeneralConstants.FIELD_FINAL_DATE);

		List<RateMovement> rateMovementList = serviceRate.getRateMovements();

		StringBuilder stringBuilderSqlBase = new StringBuilder();
		stringBuilderSqlBase.append(" SELECT QUERY_ALL.* FROM( ");
		stringBuilderSqlBase.append(" SELECT BASE_ALL.*, ");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL.ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)  THEN 1 ELSE 0 END) CARTERA_PROPIA,");
		stringBuilderSqlBase.append(
				"																																																	");
		stringBuilderSqlBase.append(
				" (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) ACT_ECO_PAR,																																				");
		stringBuilderSqlBase.append(
				"                                                                                                                                                                                                                                  						");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL. ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) THEN (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)           ");
		stringBuilderSqlBase.append(
				"      WHEN  BASE_ALL. ID_HOLDER_FK <>  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) and  (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) = 1879   ");
		stringBuilderSqlBase.append(
				"  THEN (SELECT h.ECONOMIC_ACTIVITY from holder h where H.ID_HOLDER_PK = BASE_ALL.ID_HOLDER_FK   AND  STATE_HOLDER = 496 AND h.ECONOMIC_ACTIVITY IN(55,56,57))ELSE NULL END) H_ECONOMIC_ACTIVITY,                                         				");
		stringBuilderSqlBase.append(
				"                                                                                                                                                                                                                                  						");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL. ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) THEN (SELECT MNEMONIC FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)                    ");
		stringBuilderSqlBase.append(
				"      WHEN  BASE_ALL. ID_HOLDER_FK <>  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) and  (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) = 1879   ");
		stringBuilderSqlBase.append(
				" THEN (SELECT h.MNEMONIC from holder h where H.ID_HOLDER_PK = BASE_ALL.ID_HOLDER_FK   AND  STATE_HOLDER = 496 AND h.ECONOMIC_ACTIVITY IN(55,56,57))ELSE NULL END) NEMONICO_HOLDER                                                               		");
		stringBuilderSqlBase.append(" ");
		stringBuilderSqlBase.append(" FROM ( ");
		stringBuilderSqlBase.append("");
		stringBuilderSqlBase.append("");
		stringBuilderSqlBase.append(" SELECT  HA.ID_HOLDER_ACCOUNT_PK as ID_HOLDER_ACCOUNT_FK , ");
		stringBuilderSqlBase.append("		  (SELECT HO.ID_HOLDER_PK FROM HOLDER_ACCOUNT_DETAIL HAD, HOLDER HO");
		stringBuilderSqlBase.append("		  WHERE HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK");
		stringBuilderSqlBase.append("		  AND HO.ID_HOLDER_PK=HAD.ID_HOLDER_FK and rownum=1) AS ID_HOLDER_FK,");
		stringBuilderSqlBase.append(" 		  HAM.ID_SOURCE_PARTICIPANT as ID_PARTICIPANT_FK , ");
		stringBuilderSqlBase.append("         S.ID_SECURITY_CODE_PK as ID_SECURITY_CODE_FK,");
		stringBuilderSqlBase.append("         S.ID_ISSUER_FK as ID_ISSUER_FK ,");
		stringBuilderSqlBase.append("         S.SECURITY_CLASS as SECURITY_CLASS  ,");
		stringBuilderSqlBase.append("         HAM.ID_MOVEMENT_TYPE_FK AS ID_MOVEMENT_TYPE_FK ,");
		stringBuilderSqlBase.append("         S.CURRENCY AS CURRENCY,");
		stringBuilderSqlBase.append("         HAM.MOVEMENT_DATE as MOVEMENT_DATE,");
		// stringBuilderSqlBase.append(" ROUND (S.CURRENT_NOMINAL_VALUE, DECODE
		// (S.SECURITY_CLASS,426,4,2) ) AS NOMINAL_VALUE,");
		stringBuilderSqlBase
				.append(" 		   ROUND(HAM.NOMINAL_VALUE, DECODE (S.SECURITY_CLASS, 426,4,2))as NOMINAL_VALUE, "); // issue
																														// 224
		stringBuilderSqlBase.append("         HAM.OPERATION_PRICE AS OPERATION_PRICE,");
		stringBuilderSqlBase.append("         HAM.MOVEMENT_QUANTITY AS MOVEMENT_QUANTITY, ");
		stringBuilderSqlBase.append("         '' AS ID_ISSUANCE_CODE_FK, ");
		stringBuilderSqlBase.append(" 		 HAM.MOVEMENT_DATE AS OPERATION_DATE  ,  ");
		stringBuilderSqlBase.append("		nvl(HAM.OPERATION_NUMBER, 0) AS OPERATION_NUMBER, ");
		stringBuilderSqlBase.append("  		nvl(HAM.ID_SOURCE_PARTICIPANT, 0) as ID_SOURCE_PARTICIPANT_FK	 ,  ");
		stringBuilderSqlBase.append("		nvl(HAM.ID_TARGET_PARTICIPANT, 0) AS ID_TARGET_PARTICIPANT_FK, ");
		stringBuilderSqlBase.append("		MO.ID_NEGOTIATION_MODALITY_FK AS ID_NEGOTIATION_MODALITY_FK	");
		stringBuilderSqlBase.append(" from HOLDER_ACCOUNT_MOVEMENT HAM ");
		stringBuilderSqlBase.append(
				" left outer join MECHANISM_OPERATION MO on ( MO.ID_MECHANISM_OPERATION_PK=HAM.ID_TRADE_OPERATION_FK ) ");
		stringBuilderSqlBase
				.append(" inner join MOVEMENT_TYPE MT ON (MT.ID_MOVEMENT_TYPE_PK = HAM.ID_MOVEMENT_TYPE_FK) ");
		stringBuilderSqlBase
				.append(" left outer join HOLDER_ACCOUNT HA on (HA.ID_HOLDER_ACCOUNT_PK = HAM.ID_HOLDER_ACCOUNT_FK) ");
		stringBuilderSqlBase
				.append(" left outer join SECURITY S on ( HAM.ID_SECURITY_CODE_FK=S.ID_SECURITY_CODE_PK ) ");
		stringBuilderSqlBase.append(" left outer join ISSUER I on (I.ID_ISSUER_PK=S.ID_ISSUER_FK) ");
		stringBuilderSqlBase.append(" WHERE ");
		stringBuilderSqlBase.append(" 	S.INSTRUMENT_TYPE=" + idInstrumentType);// ---124 RENTA FIJA --399 RENTA
																				// VARIABLE

		if (Validations.validateIsNotNull(idNegotiationMechanism)) {
			stringBuilderSqlBase.append("  AND HAM.ID_MECHANISM=:idNegotiationMechanism ");
		}

		if (Validations.validateListIsNotNullAndNotEmpty(rateMovementList)) {

			stringBuilderSqlBase.append(" AND HAM.ID_MOVEMENT_TYPE_FK in (");
			StringBuilder stringBuilderMovements = new StringBuilder();

			for (int i = 0; i < rateMovementList.size(); i++) {
				RateMovement rateMovement = rateMovementList.get(i);
				String caracter = (i == 0 ? " " : ",");
				stringBuilderMovements.append(caracter + rateMovement.getMovementType().getIdMovementTypePk() + " ");
			}

			stringBuilderSqlBase.append(stringBuilderMovements.toString()).append(" )");
		}
		/** structure ::::::::: and HAM.ID_MOVEMENT_TYPE_FK in ( 1,2,3,4) */

		if (Validations.validateIsNotNull(initialDate) && Validations.validateIsNotNull(finalDate)) {

			stringBuilderSqlBase
					.append(" AND  HAM.MOVEMENT_DATE >= :initialDate  AND HAM.MOVEMENT_DATE<= :finalDate   ");
		}

		log.debug(
				" ::::::::::::::::::::::::::::::::::::: SOLUCION PRELIMINAR FACTURACION DPF DPA POR ID DEL SERVICIO CREADO ::::::::::::::::::::::::::::");
		if (billingService.getServiceCode().equals("49")) { // CODIGO SERVICIO 9.3 PROD
//		if (billingService.getServiceCode().equals("51")) { //CODIGO SERVICIO 9.3 PREPROD
			stringBuilderSqlBase.append(" AND S.SECURITY_CLASS IN (420,1976)   ");
		}

		stringBuilderSqlBase.append(" ORDER BY HA.ID_HOLDER_ACCOUNT_PK, ID_HOLDER_FK , ");
		stringBuilderSqlBase
				.append(" HAM.ID_PARTICIPANT_FK , S.ID_SECURITY_CODE_PK, S.ID_ISSUER_FK, HAM.ID_MOVEMENT_TYPE_FK  ");
		stringBuilderSqlBase.append(" ) BASE_ALL");
		stringBuilderSqlBase.append(" ) QUERY_ALL");
		stringBuilderSqlBase.append(" WHERE 1=1 ");
		if (!BillingServicePortfolioType.ALL.getCode().equals(portFolio)) {
			stringBuilderSqlBase.append(" AND QUERY_ALL.CARTERA_PROPIA=" + BooleanType.get(portFolio).getBinaryValue());
		}
		stringBuilderSqlBase.append(" AND QUERY_ALL.ACT_ECO_PAR IN(:listEconomicAct)");

		/** Making the table EXCEPTION **/
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters,
				stringBuilderSqlBase);

		log.debug(
				" :::::::::::::::::::::::::::::::::::::Consulta Transaccion con Reglas de Excepcion ::::::::::::::::::::::::::::");
		log.debug(strTableComponentBegin.toString());

		Query query = em.createNativeQuery(strTableComponentBegin.toString());
		if (Validations.validateIsNotNull(initialDate) && Validations.validateIsNotNull(finalDate)) {

			query.setParameter(GeneralConstants.FIELD_INITIAL_DATE, initialDate);
			query.setParameter(GeneralConstants.FIELD_FINAL_DATE, finalDate);
		}

		if (Validations.validateIsNotNull(idNegotiationMechanism)) {
			query.setParameter("idNegotiationMechanism", idNegotiationMechanism);
		}
		query.setParameter("listEconomicAct", listEconomicAct);

		List<Object> listObject = null;
		try {

			listObject = query.getResultList();

		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}

		}

		return listObject;
	}

	/**
	 * 10 REGISTRO TRANSFERENCIAS DE VALORES ANOTADOS EN CUENTA <br>
	 * REGISTRO_VALORES_DESMATERIALIZADOS_R_FIJA <br>
	 * REGISTRO_VALORES_DESMATERIALIZADOS_R_FIJA.
	 *
	 * @param parameters             the parameters
	 * @param idInstrumentType       the id instrument type
	 * @param idNegotiationMechanism the id negotiation mechanism
	 * @return List<Object>
	 * @throws ServiceException the service exception
	 */
	public List<Object> findIssuanceDematerializedAnnotationOnAccount(Map parameters, Integer idInstrumentType,
			Long idNegotiationMechanism) throws ServiceException {

		StringBuilder strTableComponentBegin = new StringBuilder();

		/** State For Issuance */
		List<Integer> stateList = new ArrayList<>();
		stateList.add(IssuanceStateType.AUTHORIZED.getCode());

		/** getting data for do merging into dynamic query **/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		Date calculateDate = (Date) parameters.get(GeneralConstants.CALCULATION_DATE);
		/** Range to Dates */
		Date initialDate = (Date) parameters.get(GeneralConstants.FIELD_INITIAL_DATE);
		Date finalDate = (Date) parameters.get(GeneralConstants.FIELD_FINAL_DATE);

		StringBuilder stringBuilderSqlBase = new StringBuilder();
		stringBuilderSqlBase.append(" SELECT    IA.ID_ISSUER_FK AS ID_ISSUER_FK,   ");
		stringBuilderSqlBase.append(" 	IA.ID_ISSUANCE_CODE_PK AS ID_ISSUANCE_CODE_FK , ");
		stringBuilderSqlBase.append("	'' AS ID_SECURITY_CODE_FK, ");
		stringBuilderSqlBase.append("  	'' AS ID_PARTICIPANT_FK, 	 ");
		stringBuilderSqlBase.append("	IA.SECURITY_CLASS AS SECURITY_CLASS, ");
		stringBuilderSqlBase.append("	'' AS ID_HOLDER_ACCOUNT_FK, ");
		stringBuilderSqlBase.append("	'' AS ID_HOLDER_FK, ");
		stringBuilderSqlBase.append(" 	IA.REGISTRY_DATE AS OPERATION_DATE , ");
		stringBuilderSqlBase.append("	IA.ISSUANCE_AMOUNT AS PRICE, ");
		stringBuilderSqlBase.append(" 	IA.CURRENCY AS CURRENCY ");
		stringBuilderSqlBase.append(" FROM ISSUANCE IA ");
		stringBuilderSqlBase.append(" 	 INNER JOIN ISSUER I ON (I.ID_ISSUER_PK=IA.ID_ISSUER_FK) ");
		stringBuilderSqlBase.append(" WHERE 1=1 ");
		stringBuilderSqlBase.append("   AND IA.STATE_ISSUANCE in :stateIssuance ");/** 128 Only Active ***/
		stringBuilderSqlBase.append("   AND IA.ISSUANCE_TYPE = :issuanceType "); // issue 0105

		if (Validations.validateIsNotNullAndPositive(idInstrumentType)) {

			stringBuilderSqlBase.append("   AND IA.INSTRUMENT_TYPE =:instrumentType");

		}

		if (billingService.getCalculationPeriod().equals(PropertiesConstants.PERIOD_CALCULATION_DAILY_PK)) {
			stringBuilderSqlBase.append(" AND TO_CHAR(IA.REGISTRY_DATE,'yyyy/MM/dd')=:dateCalculationPeriod ");
		} else if (billingService.getCalculationPeriod().equals(PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK)) {
			stringBuilderSqlBase.append(" AND TRUNC(IA.REGISTRY_DATE) BETWEEN  :initialDate AND :finalDate  ");
		}

		/** Making the table EXCEPTION **/
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters,
				stringBuilderSqlBase);

		log.debug(
				" :::::::::::::::::::::::::::::::::::::Consulta Transaccion con Reglas de Excepcion ::::::::::::::::::::::::::::");
		log.debug(strTableComponentBegin.toString());

		Query query = em.createNativeQuery(strTableComponentBegin.toString());

		if (PropertiesConstants.PERIOD_CALCULATION_DAILY_PK.equals(billingService.getCalculationPeriod())) {

			query.setParameter("dateCalculationPeriod",
					CommonsUtilities.convertDateToString(calculateDate, BillingUtils.DATE_PATTERN_yyyy_MM_dd));

		} else if (PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK.equals(billingService.getCalculationPeriod())) {

			query.setParameter(GeneralConstants.FIELD_INITIAL_DATE, initialDate);
			query.setParameter(GeneralConstants.FIELD_FINAL_DATE, finalDate);

		}
		if (Validations.validateIsNotNullAndPositive(idInstrumentType)) {

			query.setParameter("instrumentType", idInstrumentType);

		}

		// issue 0105

		query.setParameter("issuanceType", IssuanceType.DEMATERIALIZED.getCode());

		query.setParameter("stateIssuance", stateList);
		List<Object> listObject = null;
		try {

			listObject = query.getResultList();

		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}

		}

		return listObject;

	}

	/**
	 * 10.3 REGISTRO DE EMISIONES DE VALORES DPF <br>
	 * REGISTRO_EMISIONES_VALORES_DESMATERIALIZADO_DPF
	 *
	 * @param parameters             the parameters
	 * @param idInstrumentType       the id instrument type
	 * @param idNegotiationMechanism the id negotiation mechanism
	 * @return List<Object>
	 * @throws ServiceException the service exception
	 */
	public List<Object> findIssuanceDematerializedAnnotationDpf(Map parameters, Integer idInstrumentType,
			Long idNegotiationMechanism) throws ServiceException {

		StringBuilder strTableComponentBegin = new StringBuilder();

		/** State For Issuance */
		List<Integer> stateList = new ArrayList<>();
		List<Integer> stateListExp = new ArrayList<>();

		stateList.add(IssuanceStateType.AUTHORIZED.getCode());// REGISTRADOS
		stateListExp.add(IssuanceStateType.EXPIRED.getCode());// VENCIDOS
		stateListExp.add(IssuanceStateType.SUSPENDED.getCode());// SUSPENDIDOS

		/** getting data for do merging into dynamic query **/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		Date calculateDate = (Date) parameters.get(GeneralConstants.CALCULATION_DATE);
		/** Range to Dates */
		Date initialDate = (Date) parameters.get(GeneralConstants.FIELD_INITIAL_DATE);
		Date finalDate = (Date) parameters.get(GeneralConstants.FIELD_FINAL_DATE);

		StringBuilder stringBuilderSqlBase = new StringBuilder();
		stringBuilderSqlBase.append(" SELECT    IA.ID_ISSUER_FK AS ID_ISSUER_FK,   ");
		stringBuilderSqlBase.append(" 	IA.ID_ISSUANCE_CODE_PK AS ID_ISSUANCE_CODE_FK , ");
		stringBuilderSqlBase.append("	S.ID_SECURITY_CODE_PK AS ID_SECURITY_CODE_FK, ");
		stringBuilderSqlBase.append("  	'' AS ID_PARTICIPANT_FK, 	 ");
		stringBuilderSqlBase.append("	IA.SECURITY_CLASS AS SECURITY_CLASS, ");
		stringBuilderSqlBase.append("	'' AS ID_HOLDER_ACCOUNT_FK, ");
		stringBuilderSqlBase.append("	'' AS ID_HOLDER_FK, ");
		stringBuilderSqlBase.append(" 	IA.REGISTRY_DATE AS OPERATION_DATE , ");
		stringBuilderSqlBase.append("	IA.ISSUANCE_AMOUNT AS PRICE, ");
		stringBuilderSqlBase.append(" 	IA.CURRENCY AS CURRENCY ");
		stringBuilderSqlBase.append(" FROM ISSUANCE IA ");
		stringBuilderSqlBase.append(" 	 INNER JOIN ISSUER I ON (I.ID_ISSUER_PK=IA.ID_ISSUER_FK) ");
		stringBuilderSqlBase.append(" 	 INNER JOIN SECURITY S ON (S.ID_ISSUANCE_CODE_FK=IA.ID_ISSUANCE_CODE_PK)	");

		stringBuilderSqlBase.append(" WHERE 1=1 ");
		stringBuilderSqlBase.append(" 	AND IA.ISSUANCE_TYPE =").append(IssuanceType.DEMATERIALIZED.getCode());// 132

		if (Validations.validateIsNotNullAndPositive(idInstrumentType)) {

			stringBuilderSqlBase.append("   AND IA.INSTRUMENT_TYPE =:instrumentType");

		}

		if (billingService.getCalculationPeriod().equals(PropertiesConstants.PERIOD_CALCULATION_DAILY_PK)) {
			stringBuilderSqlBase.append(" AND TO_CHAR(IA.REGISTRY_DATE,'yyyy/MM/dd')=:dateCalculationPeriod ");
			stringBuilderSqlBase.append(" AND TO_CHAR(IA.EXPIRATION_DATE,'yyyy/MM/dd')=:dateCalculationPeriod ");
		} else if (billingService.getCalculationPeriod().equals(PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK)) {
			stringBuilderSqlBase.append(" AND TRUNC(IA.REGISTRY_DATE) BETWEEN  :initialDate AND :finalDate  ");
			stringBuilderSqlBase.append(" AND ( ");
			stringBuilderSqlBase.append("	IA.STATE_ISSUANCE IN  :stateIssuance OR");
			stringBuilderSqlBase.append("  ( ");
			stringBuilderSqlBase.append("   IA.STATE_ISSUANCE in :stateExpired  ");
			stringBuilderSqlBase.append(" AND 1=1 ");
			stringBuilderSqlBase.append("  ) ");
			stringBuilderSqlBase.append("   ) ");
		}

		/** Making the table EXCEPTION **/
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters,
				stringBuilderSqlBase);

		log.debug(
				" :::::::::::::::::::::::::::::::::::::Consulta Transaccion con Reglas de Excepcion ::::::::::::::::::::::::::::");
		log.debug(strTableComponentBegin.toString());

		Query query = em.createNativeQuery(strTableComponentBegin.toString());

		if (PropertiesConstants.PERIOD_CALCULATION_DAILY_PK.equals(billingService.getCalculationPeriod())) {

			query.setParameter("dateCalculationPeriod",
					CommonsUtilities.convertDateToString(calculateDate, BillingUtils.DATE_PATTERN_yyyy_MM_dd));

		} else if (PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK.equals(billingService.getCalculationPeriod())) {
			query.setParameter("stateIssuance", stateList);
			query.setParameter("stateExpired", stateListExp);
			query.setParameter(GeneralConstants.FIELD_INITIAL_DATE, initialDate);
			query.setParameter(GeneralConstants.FIELD_FINAL_DATE, finalDate);

		}
		if (Validations.validateIsNotNullAndPositive(idInstrumentType)) {

			query.setParameter("instrumentType", idInstrumentType);

		}

		List<Object> listObject = null;
		try {

			listObject = query.getResultList();

		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}

		}

		return listObject;

	}

	/**
	 * 11 <br>
	 * SOLICITUDES_CAMBIO_TITULARIDAD_R_FIJA <br>
	 * SOLICITUDES_CAMBIO_TITULARIDAD_R_VARIABLE.
	 *
	 * @param parameters             the parameters
	 * @param idInstrumentType       the id instrument type
	 * @param idNegotiationMechanism the id negotiation mechanism
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<Object> findMovementSecuritiesChangeOwnerShip(Map parameters) throws ServiceException {

		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		Integer portFolio = billingService.getPortfolio();

		StringBuilder strTableComponentBegin = new StringBuilder();

		/** getting data for do merging into dynamic query **/
		ServiceRate serviceRate = (ServiceRate) parameters.get(GeneralConstants.SERVICE_RATE);

		/** Range to Dates */
		Date initialDate = (Date) parameters.get(GeneralConstants.FIELD_INITIAL_DATE);
		Date finalDate = (Date) parameters.get(GeneralConstants.FIELD_FINAL_DATE);

		/**
		 * Begin To eventuals
		 */
		Long idChangeOwnerShip = null;
		Long idParticipant = null;
		if (Validations.validateIsNotNull(parameters.get(GeneralConstants.ID_CUSTODY_PK))) {
			idChangeOwnerShip = Long.parseLong(parameters.get(GeneralConstants.ID_CUSTODY_PK).toString());
		}
		if (Validations.validateIsNotNull(parameters.get(GeneralConstants.ID_PARTICIPANT_PK))) {
			idParticipant = Long.parseLong(parameters.get(GeneralConstants.ID_PARTICIPANT_PK).toString());
		}
		/**
		 * End to Eventuals
		 */
		List<RateMovement> rateMovementList = serviceRate.getRateMovements();

		StringBuilder stringBuilderSqlBase = new StringBuilder();
		stringBuilderSqlBase.append(" SELECT QUERY_ALL.* FROM( ");
		stringBuilderSqlBase.append(" SELECT BASE_ALL.*, ");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL.ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)  THEN 1 ELSE 0 END) CARTERA_PROPIA");
		stringBuilderSqlBase.append(
				"																																																	");
		stringBuilderSqlBase.append(" ");
		stringBuilderSqlBase.append(" FROM ( ");
		stringBuilderSqlBase.append("");
		stringBuilderSqlBase.append("");
		stringBuilderSqlBase.append(" SELECT  HA.ID_HOLDER_ACCOUNT_PK as ID_HOLDER_ACCOUNT_FK , ");
		stringBuilderSqlBase.append("		  (SELECT HO.ID_HOLDER_PK FROM HOLDER_ACCOUNT_DETAIL HAD, HOLDER HO");
		stringBuilderSqlBase.append("		  WHERE HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK");
		stringBuilderSqlBase.append("		  AND HO.ID_HOLDER_PK=HAD.ID_HOLDER_FK and rownum=1) AS ID_HOLDER_FK,");
		stringBuilderSqlBase.append(" 		  HAM.ID_PARTICIPANT_FK as ID_PARTICIPANT_FK , ");
		stringBuilderSqlBase.append("         S.ID_SECURITY_CODE_PK as ID_SECURITY_CODE_FK,");
		stringBuilderSqlBase.append("         S.ID_ISSUER_FK as ID_ISSUER_FK ,");
		stringBuilderSqlBase.append("         S.SECURITY_CLASS as SECURITY_CLASS  ,");
		stringBuilderSqlBase.append("         HAM.ID_MOVEMENT_TYPE_FK AS ID_MOVEMENT_TYPE_FK ,");
		stringBuilderSqlBase.append("         S.CURRENCY AS CURRENCY,");
		stringBuilderSqlBase.append("         HAM.MOVEMENT_DATE as MOVEMENT_DATE,");
		stringBuilderSqlBase
				.append("         ROUND (HAM.NOMINAL_VALUE, DECODE (S.SECURITY_CLASS,426,4,2) )  AS  NOMINAL_VALUE,");
		stringBuilderSqlBase.append("         (HAM.NOMINAL_VALUE * HAM.MOVEMENT_QUANTITY) AS OPERATION_PRICE,");
		stringBuilderSqlBase.append("         HAM.MOVEMENT_QUANTITY AS MOVEMENT_QUANTITY, ");
		stringBuilderSqlBase.append("         '' AS ID_ISSUANCE_CODE_FK, ");
		stringBuilderSqlBase.append(" 		  HAM.MOVEMENT_DATE AS OPERATION_DATE, ");
		stringBuilderSqlBase.append(" 		  NVL(HAM.OPERATION_NUMBER, 0) AS OPERATION_NUMBER,  ");
		stringBuilderSqlBase.append(" 		  NVL(HAM.ID_SOURCE_PARTICIPANT, 0) as ID_SOURCE_PARTICIPANT_FK	 ,  ");
		stringBuilderSqlBase.append("		  NVL(HAM.ID_TARGET_PARTICIPANT, 0) AS ID_TARGET_PARTICIPANT_FK 	");
		stringBuilderSqlBase.append("	FROM CHANGE_OWNERSHIP_OPERATION COW	");
		stringBuilderSqlBase.append("		INNER JOIN CUSTODY_OPERATION CO ");
		stringBuilderSqlBase.append("		  ON CO.ID_CUSTODY_OPERATION_PK=COW.ID_CHANGE_OWNERSHIP_PK ");
		stringBuilderSqlBase.append("		INNER JOIN HOLDER_ACCOUNT_MOVEMENT HAM ");
		stringBuilderSqlBase.append("		  ON HAM.ID_CUSTODY_OPERATION_FK=CO.ID_CUSTODY_OPERATION_PK ");
		stringBuilderSqlBase.append("		INNER JOIN MOVEMENT_TYPE MT ");
		stringBuilderSqlBase.append("		  ON (MT.ID_MOVEMENT_TYPE_PK = HAM.ID_MOVEMENT_TYPE_FK)");
		stringBuilderSqlBase.append("		");
		stringBuilderSqlBase.append(" 		 ");
		stringBuilderSqlBase
				.append(" LEFT OUTER join HOLDER_ACCOUNT HA on (HA.ID_HOLDER_ACCOUNT_PK = HAM.ID_HOLDER_ACCOUNT_FK) ");
		stringBuilderSqlBase
				.append(" LEFT OUTER join SECURITY S on ( HAM.ID_SECURITY_CODE_FK=S.ID_SECURITY_CODE_PK ) ");
		stringBuilderSqlBase.append(" LEFT OUTER join ISSUER I on (I.ID_ISSUER_PK=S.ID_ISSUER_FK) ");
		stringBuilderSqlBase.append(" WHERE ");
		stringBuilderSqlBase.append(" 	S.ISSUANCE_FORM IN (400, 132, 401)");// ---400 RENTA FIJA --132 Y 401 ELECTRONICO

		/** Begin To eventual **/
		if (Validations.validateIsNotNullAndPositive(idChangeOwnerShip)) {

			stringBuilderSqlBase.append(" AND COW.ID_CHANGE_OWNERSHIP_PK= :idChangeOwnerShip");
		}
		if (Validations.validateIsNotNullAndPositive(idParticipant)) {
			stringBuilderSqlBase.append(" AND COW.ID_SOURCE_PARTICIPANT_FK = :idParticipant");
		}
		/** End To eventual **/

		stringBuilderSqlBase.append(" AND COW.STATE= :stateChangeOwnership");// 865 Confirmado

		if (Validations.validateListIsNotNullAndNotEmpty(rateMovementList)) {

			stringBuilderSqlBase.append(" AND HAM.ID_MOVEMENT_TYPE_FK in (");
			StringBuilder stringBuilderMovements = new StringBuilder();

			for (int i = 0; i < rateMovementList.size(); i++) {
				RateMovement rateMovement = rateMovementList.get(i);
				String caracter = (i == 0 ? " " : ",");
				stringBuilderMovements.append(caracter + rateMovement.getMovementType().getIdMovementTypePk() + " ");
			}

			stringBuilderSqlBase.append(stringBuilderMovements.toString()).append(" )");
		}

		if (Validations.validateIsNotNull(initialDate) && Validations.validateIsNotNull(finalDate)) {

			stringBuilderSqlBase
					.append(" AND  HAM.MOVEMENT_DATE >= :initialDate  AND HAM.MOVEMENT_DATE<= :finalDate   ");
		}

		stringBuilderSqlBase.append(" ORDER BY HAM.OPERATION_NUMBER,  HAM.ID_PARTICIPANT_FK , S.ID_ISSUER_FK, ");
		stringBuilderSqlBase.append("  HA.ID_HOLDER_ACCOUNT_PK, ID_HOLDER_FK , ");
		stringBuilderSqlBase.append("   S.ID_SECURITY_CODE_PK, HAM.ID_MOVEMENT_TYPE_FK   ");
		stringBuilderSqlBase.append(" ) BASE_ALL");
		stringBuilderSqlBase.append(" ) QUERY_ALL");
		stringBuilderSqlBase.append(" WHERE 1=1 ");
		if (!BillingServicePortfolioType.ALL.getCode().equals(portFolio)) {
			stringBuilderSqlBase.append(" AND QUERY_ALL.CARTERA_PROPIA=" + BooleanType.get(portFolio).getBinaryValue());
		}

		/** Making the table EXCEPTION **/
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters,
				stringBuilderSqlBase);

		log.debug(
				" :::::::::::::::::::::::::::::::::::::Consulta Transaccion con Reglas de Excepcion ::::::::::::::::::::::::::::");
		log.debug(strTableComponentBegin.toString());

		Query query = em.createNativeQuery(strTableComponentBegin.toString());
		if (Validations.validateIsNotNull(initialDate) && Validations.validateIsNotNull(finalDate)) {

			query.setParameter(GeneralConstants.FIELD_INITIAL_DATE, initialDate);
			query.setParameter(GeneralConstants.FIELD_FINAL_DATE, finalDate);
		}

		/** Begin To eventual **/
		if (Validations.validateIsNotNullAndPositive(idChangeOwnerShip)) {
			query.setParameter("idChangeOwnerShip", idChangeOwnerShip);
		}
		if (Validations.validateIsNotNullAndPositive(idParticipant)) {
			query.setParameter("idParticipant", idParticipant);
		}
		/** End To Eventual **/

		query.setParameter("stateChangeOwnership", ChangeOwnershipStatusType.CONFIRMADO.getCode());
		List<Object> listObject = null;
		try {

			listObject = query.getResultList();

		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}

		}

		return listObject;
	}

	/**
	 * 15 EMISION_CERTIFICADOS_ACREDITACION COLLECTION EVENTUAL ACCREDITATION
	 * CERTIFICATE.
	 *
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<Object> processAccreditationCertificate(Map parameters) throws ServiceException {

		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		List<Integer> listEconomicAct = (List<Integer>) parameters.get(GeneralConstants.ECONOMIC_ACTIVITY_LIST_INT);
		Integer portFolio = billingService.getPortfolio();
		/** Range to Dates */
		Date initialDate = (Date) parameters.get(GeneralConstants.FIELD_INITIAL_DATE);
		Date finalDate = (Date) parameters.get(GeneralConstants.FIELD_FINAL_DATE);

		List<Integer> listAccreditationAvailable = new ArrayList<Integer>();
		listAccreditationAvailable.add(AccreditationOperationMotive.POLITICAL.getCode());
		listAccreditationAvailable.add(AccreditationOperationMotive.PROPERTY.getCode());
		listAccreditationAvailable.add(AccreditationOperationMotive.EARLY_REDEMPTION.getCode());

		List<Integer> listAcreditationState = new ArrayList<Integer>();
		listAcreditationState.add(AccreditationOperationStateType.CONFIRMED.getCode());
		listAcreditationState.add(AccreditationOperationStateType.UNBLOCKED.getCode());// for recalculate

		StringBuilder strTableComponentBegin = new StringBuilder();

		/** Start Query Base **/
		StringBuilder stringBuilderSqlBase = new StringBuilder();
		stringBuilderSqlBase.append(" SELECT QUERY_ALL.* FROM( ");
		stringBuilderSqlBase.append(" SELECT BASE_ALL.*, ");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL.ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)  THEN 1 ELSE 0 END) CARTERA_PROPIA,");
		stringBuilderSqlBase.append(
				"																																																	");
		stringBuilderSqlBase.append(
				" (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) ACT_ECO_PAR,																																				");
		stringBuilderSqlBase.append(
				"                                                                                                                                                                                                                                  						");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL. ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) THEN (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)           ");
		stringBuilderSqlBase.append(
				"      WHEN  BASE_ALL. ID_HOLDER_FK <>  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) and  (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) = 1879   ");
		stringBuilderSqlBase.append(
				"  THEN (SELECT h.ECONOMIC_ACTIVITY from holder h where H.ID_HOLDER_PK = BASE_ALL.ID_HOLDER_FK   AND  STATE_HOLDER = 496 AND h.ECONOMIC_ACTIVITY IN(55,56,57))ELSE NULL END) H_ECONOMIC_ACTIVITY,                                         				");
		stringBuilderSqlBase.append(
				"                                                                                                                                                                                                                                  						");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL. ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) THEN (SELECT MNEMONIC FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)                    ");
		stringBuilderSqlBase.append(
				"      WHEN  BASE_ALL. ID_HOLDER_FK <>  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) and  (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) = 1879   ");
		stringBuilderSqlBase.append(
				" THEN (SELECT h.MNEMONIC from holder h where H.ID_HOLDER_PK = BASE_ALL.ID_HOLDER_FK   AND  STATE_HOLDER = 496 AND h.ECONOMIC_ACTIVITY IN(55,56,57))ELSE NULL END) NEMONICO_HOLDER                                                               				");
		stringBuilderSqlBase.append(" ");
		stringBuilderSqlBase.append(" FROM ( ");
		stringBuilderSqlBase.append("");
		stringBuilderSqlBase.append("");
		stringBuilderSqlBase.append(" SELECT S.ID_SECURITY_CODE_PK AS ID_SECURITY_CODE_FK, ");
		stringBuilderSqlBase.append("  			S.SECURITY_CLASS AS SECURITY_CLASS,");
//		stringBuilderSqlBase.append("  			AOP.ID_PARTICIPANT_PETITIONER_FK AS ID_PARTICIPANT_FK, ");  // Issue 00057
		stringBuilderSqlBase.append("  			ACD.ID_PARTICIPANT_FK AS ID_PARTICIPANT_FK, ");
		stringBuilderSqlBase.append("  			CO.OPERATION_NUMBER AS OPERATION_NUMBER,	");
		stringBuilderSqlBase.append("  			AOP.ID_HOLDER_FK AS ID_HOLDER_FK,	");
		stringBuilderSqlBase.append("  			CO.CONFIRM_DATE AS OPERATION_DATE,   	");
		stringBuilderSqlBase.append("  			S.ID_ISSUANCE_CODE_FK AS ID_ISSUANCE_CODE_FK, ");
		stringBuilderSqlBase.append("  			ACD.ID_HOLDER_ACCOUNT_FK AS ID_HOLDER_ACCOUNT_FK ");
		stringBuilderSqlBase.append(" FROM CUSTODY_OPERATION CO  ");
		stringBuilderSqlBase.append(" INNER JOIN accreditation_operation AOP ON ");
		stringBuilderSqlBase.append("   CO.id_custody_operation_pk=AOP.id_accreditation_operation_pk  ");
		stringBuilderSqlBase.append(" INNER JOIN ACCREDITATION_DETAIL ACD ON ");
		stringBuilderSqlBase.append("  	ACD.ID_ACCREDITATION_OPERATION_FK= aop.ID_ACCREDITATION_OPERATION_PK ");
		stringBuilderSqlBase.append(" LEFT OUTER JOIN SECURITY S ON   ");
		stringBuilderSqlBase.append("   ACD.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK ");
		stringBuilderSqlBase.append(" ");
		stringBuilderSqlBase.append("  WHERE 1=1 ");

		stringBuilderSqlBase.append("  	AND AOP.accreditation_state  IN :listAcreditationState ");
		stringBuilderSqlBase.append("	AND co.OPERATION_STATE=")
				.append(AccreditationOperationStateType.CONFIRMED.getCode());
		stringBuilderSqlBase.append(" and AOP.motive in :listAccreditationAvailable ");
		stringBuilderSqlBase.append(" and AOP.PETITIONER_TYPE in ( :petitionerType , :petitionerTypeIssuer ) "); // issue
																													// 00057

		if (Validations.validateIsNotNull(initialDate) && Validations.validateIsNotNull(finalDate)) {

			stringBuilderSqlBase.append(" and  CO.CONFIRM_DATE >= :initialDate  and CO.CONFIRM_DATE<= :finalDate   ");
		}

		stringBuilderSqlBase.append(" ORDER BY CO.OPERATION_NUMBER  ");
		stringBuilderSqlBase.append(" ) BASE_ALL");
		stringBuilderSqlBase.append(" ) QUERY_ALL");
		stringBuilderSqlBase.append(" WHERE 1=1 ");
		if (!BillingServicePortfolioType.ALL.getCode().equals(portFolio)) {
			stringBuilderSqlBase.append(" AND QUERY_ALL.CARTERA_PROPIA=" + BooleanType.get(portFolio).getBinaryValue());
		}
		stringBuilderSqlBase.append(" AND QUERY_ALL.ACT_ECO_PAR IN(:listEconomicAct)");

		/** Making the table EXCEPTION to Eventual Base Collection **/
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters,
				stringBuilderSqlBase);

		Query query = em.createNativeQuery(strTableComponentBegin.toString());

		query.setParameter("listAccreditationAvailable", listAccreditationAvailable);
		query.setParameter("listAcreditationState", listAcreditationState);
		if (EntityCollectionType.PARTICIPANTS.getCode().equals(billingService.getEntityCollection())) { // issue 00057
			query.setParameter("petitionerType", PetitionerType.PARTICIPANT.getCode());
			query.setParameter("petitionerTypeIssuer", PetitionerType.ISSUER.getCode());
		} else if (EntityCollectionType.HOLDERS.getCode().equals(billingService.getEntityCollection())) {
			query.setParameter("petitionerType", PetitionerType.HOLDER.getCode());
		}

		if (Validations.validateIsNotNull(initialDate) && Validations.validateIsNotNull(finalDate)) {

			query.setParameter(GeneralConstants.FIELD_INITIAL_DATE, initialDate);
			query.setParameter(GeneralConstants.FIELD_FINAL_DATE, finalDate);
		}
		query.setParameter("listEconomicAct", listEconomicAct);

		List<Object> listObject = null;
		try {

			listObject = query.getResultList();

		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}

		}

		return listObject;
	}

	/**
	 * 16 SERVICIO_AGENTE_PAGADOR COLLECTION SERVICIO_AGENTE_PAGADOR.
	 *
	 * @param parameters             the parameters
	 * @param idInstrumentType       the id instrument type
	 * @param idNegotiationMechanism the id negotiation mechanism
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<Object> findPayingAgent(Map parameters, Integer idInstrumentType, Long idNegotiationMechanism)
			throws ServiceException {

		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		List<Integer> listEconomicAct = (List<Integer>) parameters.get(GeneralConstants.ECONOMIC_ACTIVITY_LIST_INT);
		Integer portFolio = billingService.getPortfolio();

		/** Range to Dates */
		Date initialDate = (Date) parameters.get(GeneralConstants.FIELD_INITIAL_DATE);
		Date finalDate = (Date) parameters.get(GeneralConstants.FIELD_FINAL_DATE);

		List<Integer> listImportanceEventType = new ArrayList<Integer>();
		listImportanceEventType.add(ImportanceEventType.INTEREST_PAYMENT.getCode());
		listImportanceEventType.add(ImportanceEventType.CAPITAL_AMORTIZATION.getCode());

		StringBuilder strTableComponentBegin = new StringBuilder();

		/** Start Query Base **/
		StringBuilder stringBuilderSqlBase = new StringBuilder();
		stringBuilderSqlBase.append(" SELECT QUERY_ALL.* FROM( ");
		stringBuilderSqlBase.append(" SELECT BASE_ALL.*, ");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL.ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)  THEN 1 ELSE 0 END) CARTERA_PROPIA,");
		stringBuilderSqlBase.append(
				"																																																	");
		stringBuilderSqlBase.append(
				" (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) ACT_ECO_PAR,																																				");
		stringBuilderSqlBase.append(
				"                                                                                                                                                                                                                                  						");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL. ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) THEN (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)           ");
		stringBuilderSqlBase.append(
				"      WHEN  BASE_ALL. ID_HOLDER_FK <>  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) and  (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) = 1879   ");
		stringBuilderSqlBase.append(
				"  THEN (SELECT h.ECONOMIC_ACTIVITY from holder h where H.ID_HOLDER_PK = BASE_ALL.ID_HOLDER_FK   AND  STATE_HOLDER = 496 AND h.ECONOMIC_ACTIVITY IN(55,56,57))ELSE NULL END) H_ECONOMIC_ACTIVITY,                                         				");
		stringBuilderSqlBase.append(
				"                                                                                                                                                                                                                                  						");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL. ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) THEN (SELECT MNEMONIC FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)                    ");
		stringBuilderSqlBase.append(
				"      WHEN  BASE_ALL. ID_HOLDER_FK <>  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) and  (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) = 1879   ");
		stringBuilderSqlBase.append(
				" THEN (SELECT h.MNEMONIC from holder h where H.ID_HOLDER_PK = BASE_ALL.ID_HOLDER_FK   AND  STATE_HOLDER = 496 AND h.ECONOMIC_ACTIVITY IN(55,56,57))ELSE NULL END) NEMONICO_HOLDER                                                               				");
		stringBuilderSqlBase.append(" ");
		stringBuilderSqlBase.append(" FROM ( ");
		stringBuilderSqlBase.append("");
		stringBuilderSqlBase.append("");
		stringBuilderSqlBase.append(" SELECT SE.ID_SECURITY_CODE_PK AS ID_SECURITY_CODE_FK, ");
		stringBuilderSqlBase.append("  			SE.SECURITY_CLASS AS SECURITY_CLASS,");
		stringBuilderSqlBase.append("  			SE.CURRENCY AS CURRENCY,");
		stringBuilderSqlBase.append("  			ISS.ID_ISSUER_PK AS ID_ISSUER_FK , ");
		stringBuilderSqlBase.append("  			'' AS ID_PARTICIPANT_FK, ");
		stringBuilderSqlBase.append("  			CO.ID_CORPORATIVE_OPERATION_PK AS OPERATION_NUMBER,	");
		stringBuilderSqlBase.append("  			'' AS ID_HOLDER_FK,	");
		stringBuilderSqlBase.append("  			CO.DELIVERY_DATE AS OPERATION_DATE,   	");
		stringBuilderSqlBase.append("  			SE.ID_ISSUANCE_CODE_FK AS ID_ISSUANCE_CODE_FK, ");
		stringBuilderSqlBase.append("  			'' AS ID_HOLDER_ACCOUNT_FK, ");
		stringBuilderSqlBase.append("  			CO.ISSUER_CONFIRMED_AMOUNT AS PRICE ");
		stringBuilderSqlBase.append(" FROM CORPORATIVE_OPERATION CO ");
		stringBuilderSqlBase.append(" INNER JOIN SECURITY SE ");
		stringBuilderSqlBase.append("   ON SE.ID_SECURITY_CODE_PK=CO.ID_ORIGIN_SECURITY_CODE_FK ");
		stringBuilderSqlBase.append(" INNER JOIN ISSUER ISS ");
		stringBuilderSqlBase.append("  	ON SE.ID_ISSUER_FK=ISS.ID_ISSUER_PK ");
		stringBuilderSqlBase.append(" INNER JOIN CORPORATIVE_EVENT_TYPE CET  ");
		stringBuilderSqlBase.append("  	ON CET.ID_CORPORATIVE_EVENT_TYPE_PK=CO.CORPORATIVE_EVENT_TYPE ");
		stringBuilderSqlBase.append(" ");
		stringBuilderSqlBase.append("  WHERE 1=1 ");

		stringBuilderSqlBase.append("  	AND CO.STATE= ").append(CorporateProcessStateType.DEFINITIVE.getCode());
		stringBuilderSqlBase.append("  	AND CET.CORPORATIVE_EVENT_TYPE IN :listImportanceEventType ");
		stringBuilderSqlBase.append("  	AND CO.IND_PAYED= ").append(BooleanType.YES.getCode());

		if (Validations.validateIsNotNull(initialDate) && Validations.validateIsNotNull(finalDate)) {

			stringBuilderSqlBase.append(" AND CO.DELIVERY_DATE >= :initialDate  and CO.DELIVERY_DATE<= :finalDate   ");
		}

		stringBuilderSqlBase.append(" ORDER BY CO.ID_CORPORATIVE_OPERATION_PK  ");
		stringBuilderSqlBase.append(" ) BASE_ALL");
		stringBuilderSqlBase.append(" ) QUERY_ALL");
		stringBuilderSqlBase.append(" WHERE 1=1 ");
		if (!BillingServicePortfolioType.ALL.getCode().equals(portFolio)) {
			stringBuilderSqlBase.append(" AND QUERY_ALL.CARTERA_PROPIA=" + BooleanType.get(portFolio).getBinaryValue());
		}
		stringBuilderSqlBase.append(" AND QUERY_ALL.ACT_ECO_PAR IN(:listEconomicAct)");

		/** Making the table EXCEPTION to Eventual Base Collection **/
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters,
				stringBuilderSqlBase);

		Query query = em.createNativeQuery(strTableComponentBegin.toString());

		query.setParameter("listImportanceEventType", listImportanceEventType);

		if (Validations.validateIsNotNull(initialDate) && Validations.validateIsNotNull(finalDate)) {
			query.setParameter(GeneralConstants.FIELD_INITIAL_DATE, initialDate);
			query.setParameter(GeneralConstants.FIELD_FINAL_DATE, finalDate);
		}
		query.setParameter("listEconomicAct", listEconomicAct);

		List<Object> listObject = null;
		try {

			listObject = query.getResultList();

		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}

		}

		return listObject;
	}

	/**
	 * 17.1 BLOQUEOS
	 *
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<Object> findMovementPawnBlockAffectation(Map parameters) throws ServiceException {

		StringBuilder strTableComponentBegin = new StringBuilder();
		Long idblockOperation = null;

		/** getting data for do merging into dynamic query **/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		List<Integer> listEconomicAct = (List<Integer>) parameters.get(GeneralConstants.ECONOMIC_ACTIVITY_LIST_INT);
		Integer portFolio = billingService.getPortfolio();

		/** getting data for do merging into dynamic query **/
		ServiceRate serviceRate = (ServiceRate) parameters.get(GeneralConstants.SERVICE_RATE);

		/** Range to Dates */
		Date initialDate = (Date) parameters.get(GeneralConstants.FIELD_INITIAL_DATE);
		Date finalDate = (Date) parameters.get(GeneralConstants.FIELD_FINAL_DATE);

		if (Validations.validateIsNotNull(parameters.get(GeneralConstants.ID_BLOCK_OPERATION_PK))) {
			idblockOperation = Long.parseLong(parameters.get(GeneralConstants.ID_BLOCK_OPERATION_PK).toString());
		}

		List<RateMovement> rateMovementList = serviceRate.getRateMovements();

		StringBuilder stringBuilderSqlBase = new StringBuilder();
		stringBuilderSqlBase.append(" SELECT QUERY_ALL.* FROM( ");
		stringBuilderSqlBase.append(" SELECT BASE_ALL.*, ");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL.ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)  THEN 1 ELSE 0 END) CARTERA_PROPIA,");
		stringBuilderSqlBase.append(
				"																																																	");
		stringBuilderSqlBase.append(
				" (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) ACT_ECO_PAR,																																				");
		stringBuilderSqlBase.append(
				"                                                                                                                                                                                                                                  						");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL. ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) THEN (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)           ");
		stringBuilderSqlBase.append(
				"      WHEN  BASE_ALL. ID_HOLDER_FK <>  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) and  (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) = 1879   ");
		stringBuilderSqlBase.append(
				"  THEN (SELECT h.ECONOMIC_ACTIVITY from holder h where H.ID_HOLDER_PK = BASE_ALL.ID_HOLDER_FK   AND  STATE_HOLDER = 496 AND h.ECONOMIC_ACTIVITY IN(55,56,57))ELSE NULL END) H_ECONOMIC_ACTIVITY,                                         				");
		stringBuilderSqlBase.append(
				"                                                                                                                                                                                                                                  						");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL. ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) THEN (SELECT MNEMONIC FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)                    ");
		stringBuilderSqlBase.append(
				"      WHEN  BASE_ALL. ID_HOLDER_FK <>  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) and  (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) = 1879   ");
		stringBuilderSqlBase.append(
				" THEN (SELECT h.MNEMONIC from holder h where H.ID_HOLDER_PK = BASE_ALL.ID_HOLDER_FK   AND  STATE_HOLDER = 496 AND h.ECONOMIC_ACTIVITY IN(55,56,57))ELSE NULL END) NEMONICO_HOLDER                                                               				");
		stringBuilderSqlBase.append(" ");
		stringBuilderSqlBase.append(" FROM ( ");
		stringBuilderSqlBase.append("");
		stringBuilderSqlBase.append("");
		stringBuilderSqlBase.append(" SELECT  BR.ID_BLOCK_REQUEST_PK AS ID_BLOCK_REQUEST_FK,");
		stringBuilderSqlBase.append("		  BO.ID_PARTICIPANT_FK as ID_PARTICIPANT_FK, ");
		stringBuilderSqlBase.append(" 		  BO.ORIGINAL_BLOCK_BALANCE as TOTAL_BALANCE, ");
		stringBuilderSqlBase.append(" 		  BO.ID_SECURITY_CODE_FK as ID_SECURITY_CODE_FK,  ");
		stringBuilderSqlBase.append("         BO.ID_HOLDER_ACCOUNT_FK as ID_HOLDER_ACCOUNT_FK, ");
		stringBuilderSqlBase.append("		  (SELECT HO.ID_HOLDER_PK FROM HOLDER_ACCOUNT_DETAIL HAD, HOLDER HO");
		stringBuilderSqlBase.append("		  	WHERE HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK");
		stringBuilderSqlBase.append("		  	AND HO.ID_HOLDER_PK=HAD.ID_HOLDER_FK and rownum=1) AS ID_HOLDER_FK,");
		stringBuilderSqlBase.append("         S.ID_ISSUER_FK as ID_ISSUER_FK , ");
		stringBuilderSqlBase.append("         S.SECURITY_CLASS as SECURITY_CLASS  , ");
		stringBuilderSqlBase.append("         S.CURRENCY AS CURRENCY, ");
		stringBuilderSqlBase.append("         BM.ID_MOVEMENT_TYPE_FK AS ID_MOVEMENT_TYPE_FK ,");
		stringBuilderSqlBase.append(
				"         ROUND ( S.CURRENT_NOMINAL_VALUE, DECODE (S.SECURITY_CLASS,426,4,2) )  AS  NOMINAL_VALUE,");
		stringBuilderSqlBase.append("         CO.OPERATION_DATE AS OPERATION_DATE, ");
		stringBuilderSqlBase.append("         CO.OPERATION_NUMBER AS OPERATION_NUMBER, ");
		stringBuilderSqlBase.append("         S.ID_ISSUANCE_CODE_FK AS ID_ISSUANCE_CODE_FK ");

		stringBuilderSqlBase.append(" FROM BLOCK_OPERATION BO ");
		stringBuilderSqlBase
				.append(" INNER JOIN BLOCK_MOVEMENT BM on ( BM.id_block_operation_fk=BO.id_block_operation_pk ) ");
		stringBuilderSqlBase.append(
				" INNER JOIN CUSTODY_OPERATION CO ON (  CO.ID_CUSTODY_OPERATION_PK=BO.ID_BLOCK_OPERATION_PK ) ");
		stringBuilderSqlBase.append(" INNER JOIN BLOCK_REQUEST BR  ON BR.ID_BLOCK_REQUEST_PK=BO.ID_BLOCK_REQUEST_FK  ");
		stringBuilderSqlBase
				.append(" left outer join HOLDER_ACCOUNT HA on (HA.ID_HOLDER_ACCOUNT_PK = BO.ID_HOLDER_ACCOUNT_FK) ");
		stringBuilderSqlBase.append(" left outer join SECURITY S on ( BO.ID_SECURITY_CODE_FK=S.ID_SECURITY_CODE_PK ) ");
		stringBuilderSqlBase.append(" left outer join ISSUER I on (I.ID_ISSUER_PK=S.ID_ISSUER_FK) ");
		stringBuilderSqlBase.append(" WHERE  ");

		stringBuilderSqlBase.append("   BO.BLOCK_STATE IN( " + AffectationStateType.BLOCKED.getCode() + ", "
				+ AffectationStateType.UNBLOCKED.getCode() + " )");

		if (Validations.validateIsNotNullAndPositive(idblockOperation)) {
			stringBuilderSqlBase.append(" AND    BO.ID_BLOCK_OPERATION_PK =:blockOperation ");
		}
		if (Validations.validateListIsNotNullAndNotEmpty(rateMovementList)) {

			stringBuilderSqlBase.append(" AND BM.ID_MOVEMENT_TYPE_FK in (");
			StringBuilder stringBuilderMovements = new StringBuilder();

			for (int i = 0; i < rateMovementList.size(); i++) {
				RateMovement rateMovement = rateMovementList.get(i);
				String caracter = (i == 0 ? " " : ",");
				stringBuilderMovements.append(caracter + rateMovement.getMovementType().getIdMovementTypePk() + " ");
			}

			stringBuilderSqlBase.append(stringBuilderMovements.toString()).append(" )");
		}
		/** structure ::::::::: and HAM.ID_MOVEMENT_TYPE_FK in ( 1,2,3,4) */

		if (Validations.validateIsNotNull(initialDate) && Validations.validateIsNotNull(finalDate)) {

			stringBuilderSqlBase
					.append(" AND  CO.OPERATION_DATE >= :initialDate  AND CO.OPERATION_DATE<= :finalDate   ");
		}

		stringBuilderSqlBase
				.append(" ORDER BY BR.ID_BLOCK_REQUEST_PK, S.ID_ISSUANCE_CODE_FK, BO.ID_HOLDER_ACCOUNT_FK, ");
		stringBuilderSqlBase.append("  ID_HOLDER_FK , BO.ID_PARTICIPANT_FK , ");
		stringBuilderSqlBase.append("  S.ID_SECURITY_CODE_PK, S.ID_ISSUER_FK, BM.ID_MOVEMENT_TYPE_FK  ");
		stringBuilderSqlBase.append(" ) BASE_ALL");
		stringBuilderSqlBase.append(" ) QUERY_ALL");
		stringBuilderSqlBase.append(" WHERE 1=1 ");
		if (!BillingServicePortfolioType.ALL.getCode().equals(portFolio)) {
			stringBuilderSqlBase.append(" AND QUERY_ALL.CARTERA_PROPIA=" + BooleanType.get(portFolio).getBinaryValue());
		}
		stringBuilderSqlBase.append(" AND QUERY_ALL.ACT_ECO_PAR IN(:listEconomicAct)");

		/** Making the table EXCEPTION **/
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters,
				stringBuilderSqlBase);

		log.debug(
				" :::::::::::::::::::::::::::::::::::::Consulta Transaccion con Reglas de Excepcion ::::::::::::::::::::::::::::");
		log.debug(strTableComponentBegin.toString());

		Query query = em.createNativeQuery(strTableComponentBegin.toString());
		if (Validations.validateIsNotNull(initialDate) && Validations.validateIsNotNull(finalDate)) {

			query.setParameter(GeneralConstants.FIELD_INITIAL_DATE, initialDate);
			query.setParameter(GeneralConstants.FIELD_FINAL_DATE, finalDate);
		}
		if (Validations.validateIsNotNullAndPositive(idblockOperation)) {

			query.setParameter("blockOperation", idblockOperation);

		}
		query.setParameter("listEconomicAct", listEconomicAct);

		List<Object> listObject = null;
		try {

			listObject = query.getResultList();

		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}

		}

		return listObject;
	}

	/**
	 * 17.2 DESBLOQUEOS
	 *
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<Object> findMovementReversalAffectation(Map parameters) throws ServiceException {

		StringBuilder strTableComponentBegin = new StringBuilder();
		Long idblockOperation = null;

		/** getting data for do merging into dynamic query **/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		List<Integer> listEconomicAct = (List<Integer>) parameters.get(GeneralConstants.ECONOMIC_ACTIVITY_LIST_INT);
		Integer portFolio = billingService.getPortfolio();

		/** getting data for do merging into dynamic query **/
		ServiceRate serviceRate = (ServiceRate) parameters.get(GeneralConstants.SERVICE_RATE);

		/** Range to Dates */
		Date initialDate = (Date) parameters.get(GeneralConstants.FIELD_INITIAL_DATE);
		Date finalDate = (Date) parameters.get(GeneralConstants.FIELD_FINAL_DATE);

		if (Validations.validateIsNotNull(parameters.get(GeneralConstants.ID_BLOCK_OPERATION_PK))) {
			idblockOperation = Long.parseLong(parameters.get(GeneralConstants.ID_BLOCK_OPERATION_PK).toString());
		}

		List<RateMovement> rateMovementList = serviceRate.getRateMovements();

		StringBuilder stringBuilderSqlBase = new StringBuilder();
		stringBuilderSqlBase.append(" SELECT QUERY_ALL.* FROM( ");
		stringBuilderSqlBase.append(" SELECT BASE_ALL.*, ");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL.ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)  THEN 1 ELSE 0 END) CARTERA_PROPIA,");
		stringBuilderSqlBase.append(
				"																																																	");
		stringBuilderSqlBase.append(
				" (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) ACT_ECO_PAR,																																				");
		stringBuilderSqlBase.append(
				"                                                                                                                                                                                                                                  						");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL. ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) THEN (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)           ");
		stringBuilderSqlBase.append(
				"      WHEN  BASE_ALL. ID_HOLDER_FK <>  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) and  (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) = 1879   ");
		stringBuilderSqlBase.append(
				"  THEN (SELECT h.ECONOMIC_ACTIVITY from holder h where H.ID_HOLDER_PK = BASE_ALL.ID_HOLDER_FK   AND  STATE_HOLDER = 496 AND h.ECONOMIC_ACTIVITY IN(55,56,57))ELSE NULL END) H_ECONOMIC_ACTIVITY,                                         				");
		stringBuilderSqlBase.append(
				"                                                                                                                                                                                                                                  						");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL. ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) THEN (SELECT MNEMONIC FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)                    ");
		stringBuilderSqlBase.append(
				"      WHEN  BASE_ALL. ID_HOLDER_FK <>  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) and  (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) = 1879   ");
		stringBuilderSqlBase.append(
				" THEN (SELECT h.MNEMONIC from holder h where H.ID_HOLDER_PK = BASE_ALL.ID_HOLDER_FK   AND  STATE_HOLDER = 496 AND h.ECONOMIC_ACTIVITY IN(55,56,57))ELSE NULL END) NEMONICO_HOLDER                                                               				");
		stringBuilderSqlBase.append(" ");
		stringBuilderSqlBase.append(" FROM ( ");
		stringBuilderSqlBase.append("");
		stringBuilderSqlBase.append("");
		stringBuilderSqlBase.append(" SELECT  BR.ID_BLOCK_REQUEST_PK AS ID_BLOCK_REQUEST_FK,");
		stringBuilderSqlBase.append("		  BO.ID_PARTICIPANT_FK as ID_PARTICIPANT_FK, ");
		stringBuilderSqlBase.append(" 		  BM.MOVEMENT_QUANTITY  as TOTAL_BALANCE, ");
		stringBuilderSqlBase.append(" 		  BM.ID_SECURITY_CODE_FK as ID_SECURITY_CODE_FK,  ");
		stringBuilderSqlBase.append("         BM.ID_HOLDER_ACCOUNT_FK as ID_HOLDER_ACCOUNT_FK, ");
		stringBuilderSqlBase.append("		  (SELECT HO.ID_HOLDER_PK FROM HOLDER_ACCOUNT_DETAIL HAD, HOLDER HO");
		stringBuilderSqlBase.append("		  WHERE HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK");
		stringBuilderSqlBase.append("		  AND HO.ID_HOLDER_PK=HAD.ID_HOLDER_FK and rownum=1) AS ID_HOLDER_FK,");
		stringBuilderSqlBase.append("         S.ID_ISSUER_FK as ID_ISSUER_FK , ");
		stringBuilderSqlBase.append("         S.SECURITY_CLASS as SECURITY_CLASS  , ");
		stringBuilderSqlBase.append("         S.CURRENCY AS CURRENCY, ");
		stringBuilderSqlBase.append("         BM.ID_MOVEMENT_TYPE_FK AS ID_MOVEMENT_TYPE_FK ,");
		stringBuilderSqlBase.append(
				"         ROUND ( S.CURRENT_NOMINAL_VALUE, DECODE (S.SECURITY_CLASS,426,4,2) )  AS  NOMINAL_VALUE,");
		stringBuilderSqlBase.append("         BM.MOVEMENT_DATE AS OPERATION_DATE, ");
		stringBuilderSqlBase.append("         BR.ID_BLOCK_REQUEST_PK AS OPERATION_NUMBER, ");
		stringBuilderSqlBase.append("         S.ID_ISSUANCE_CODE_FK AS ID_ISSUANCE_CODE_FK ");

		stringBuilderSqlBase.append(" from UNBLOCK_OPERATION UNO ");
		stringBuilderSqlBase
				.append(" INNER JOIN BLOCK_MOVEMENT BM ON BM.ID_UNBLOCK_OPERATION_FK=UNO.ID_UNBLOCK_OPERATION_PK ");
		stringBuilderSqlBase
				.append(" INNER JOIN BLOCK_OPERATION BO ON BO.ID_BLOCK_OPERATION_PK=UNO.ID_BLOCK_OPERATION_FK ");
		stringBuilderSqlBase
				.append(" INNER JOIN block_request BR  ON BR.ID_BLOCK_REQUEST_PK=BO.ID_BLOCK_REQUEST_FK   	");
		stringBuilderSqlBase
				.append(" left outer join HOLDER_ACCOUNT HA on (HA.ID_HOLDER_ACCOUNT_PK = BO.ID_HOLDER_ACCOUNT_FK) ");
		stringBuilderSqlBase.append(" left outer join SECURITY S on ( BO.ID_SECURITY_CODE_FK=S.ID_SECURITY_CODE_PK ) ");
		stringBuilderSqlBase.append(" left outer join ISSUER I on (I.ID_ISSUER_PK=S.ID_ISSUER_FK) ");
		stringBuilderSqlBase.append(" WHERE 1=1 ");

//		stringBuilderSqlBase.append("   BO.BLOCK_STATE IN( "+AffectationStateType.BLOCKED.getCode()+", "+AffectationStateType.UNBLOCKED.getCode() +" )");

		if (Validations.validateIsNotNullAndPositive(idblockOperation)) {
			stringBuilderSqlBase.append(" AND    BO.ID_BLOCK_OPERATION_PK =:blockOperation ");
		}
		if (Validations.validateListIsNotNullAndNotEmpty(rateMovementList)) {

			stringBuilderSqlBase.append(" AND BM.ID_MOVEMENT_TYPE_FK in (");
			StringBuilder stringBuilderMovements = new StringBuilder();

			for (int i = 0; i < rateMovementList.size(); i++) {
				RateMovement rateMovement = rateMovementList.get(i);
				String caracter = (i == 0 ? " " : ",");
				stringBuilderMovements.append(caracter + rateMovement.getMovementType().getIdMovementTypePk() + " ");
			}

			stringBuilderSqlBase.append(stringBuilderMovements.toString()).append(" )");
		}
		/** structure ::::::::: and HAM.ID_MOVEMENT_TYPE_FK in ( 1,2,3,4) */

		if (Validations.validateIsNotNull(initialDate) && Validations.validateIsNotNull(finalDate)) {

			stringBuilderSqlBase.append(" AND  BM.MOVEMENT_DATE >= :initialDate  AND BM.MOVEMENT_DATE<= :finalDate   ");
		}

		stringBuilderSqlBase
				.append(" ORDER BY BR.ID_BLOCK_REQUEST_PK, S.ID_ISSUANCE_CODE_FK, BO.ID_HOLDER_ACCOUNT_FK, ");
		stringBuilderSqlBase.append("  ID_HOLDER_FK , BO.ID_PARTICIPANT_FK , ");
		stringBuilderSqlBase.append("  S.ID_SECURITY_CODE_PK, S.ID_ISSUER_FK, BM.ID_MOVEMENT_TYPE_FK  ");

		stringBuilderSqlBase.append(" ) BASE_ALL");
		stringBuilderSqlBase.append(" ) QUERY_ALL");
		stringBuilderSqlBase.append(" WHERE 1=1 ");
		if (!BillingServicePortfolioType.ALL.getCode().equals(portFolio)) {
			stringBuilderSqlBase.append(" AND QUERY_ALL.CARTERA_PROPIA=" + BooleanType.get(portFolio).getBinaryValue());
		}
		stringBuilderSqlBase.append(" AND QUERY_ALL.ACT_ECO_PAR IN(:listEconomicAct)");

		/** Making the table EXCEPTION **/
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters,
				stringBuilderSqlBase);

		log.debug(
				" :::::::::::::::::::::::::::::::::::::Consulta Transaccion con Reglas de Excepcion ::::::::::::::::::::::::::::");
		log.debug(strTableComponentBegin.toString());

		Query query = em.createNativeQuery(strTableComponentBegin.toString());

		if (Validations.validateIsNotNull(initialDate) && Validations.validateIsNotNull(finalDate)) {

			query.setParameter(GeneralConstants.FIELD_INITIAL_DATE, initialDate);
			query.setParameter(GeneralConstants.FIELD_FINAL_DATE, finalDate);
		}
		if (Validations.validateIsNotNullAndPositive(idblockOperation)) {

			query.setParameter("blockOperation", idblockOperation);

		}
		query.setParameter("listEconomicAct", listEconomicAct);

		List<Object> listObject = null;
		try {

			listObject = query.getResultList();

		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}

		}

		return listObject;
	}

	/**
	 * * SERVICE 5 Collection Annual Participant Calculation daily or monthly
	 * MANTENIMIENTO_CUENTA_MATRIZ.
	 *
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 * @see com.pradera.model.billing.type.BaseCollectionType
	 */

	public List<Object> findParticipantsForCreateAccount(Map parameters) throws ServiceException {

		StringBuilder strTableComponentBegin = new StringBuilder();

		/** getting data for do merging into dynamic query **/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		Date calculateDate = (Date) parameters.get(GeneralConstants.CALCULATION_DATE);

		StringBuilder stringBuilderSqlBase = new StringBuilder();
		stringBuilderSqlBase.append(" select  P.ID_PARTICIPANT_PK  AS ID_PARTICIPANT_FK, ");
		stringBuilderSqlBase.append(
				" '' AS ID_SECURITY_CODE_FK, '' AS SECURITY_CLASS, '' AS ID_HOLDER_ACCOUNT_FK, '' AS ID_HOLDER_FK, ");
		stringBuilderSqlBase.append(" '' AS ID_ISSUER_FK, '' AS ID_ISSUANCE_CODE_FK ");
		stringBuilderSqlBase.append(" FROM PARTICIPANT P ");
		stringBuilderSqlBase.append(" WHERE STATE= " + ParticipantStateType.REGISTERED.getCode());
		/** Only Registered ***/

		if (PropertiesConstants.PERIOD_CALCULATION_DAILY_PK.equals(billingService.getCalculationPeriod())) {

			stringBuilderSqlBase.append(" and TRUNC(p.REGISTRY_DATE) = TRUNC(:dateCalculationPeriod) ");

		} else if (PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK.equals(billingService.getCalculationPeriod())) {

			stringBuilderSqlBase
					.append(" and to_char(p.REGISTRY_DATE,'YYYY/MM')=to_char(:dateCalculationPeriod,'YYYY/MM') ");

		} else if (PropertiesConstants.PERIOD_CALCULATION_YEARLY_PK.equals(billingService.getCalculationPeriod())) {
			stringBuilderSqlBase.append(
					" AND MOD(months_between(LAST_DAY(:dateCalculationPeriod),LAST_DAY (p.REGISTRY_DATE )), 12)=0  ");
			stringBuilderSqlBase.append(
					" AND  (months_between(LAST_DAY(:dateCalculationPeriod),LAST_DAY (p.REGISTRY_DATE ))/ 12 > 0) ");
		}

		/** Making the table EXCEPTION **/
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters,
				stringBuilderSqlBase);

		log.debug(
				" :::::::::::::::::::::::::::::::::::::Consulta Transaccion con Reglas de Excepcion ::::::::::::::::::::::::::::");
		log.debug(strTableComponentBegin.toString());

		Query query = em.createNativeQuery(strTableComponentBegin.toString());

		query.setParameter("dateCalculationPeriod", calculateDate);

		List<Object> listObject = null;
		try {

			listObject = query.getResultList();

		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}

		}

		return listObject;
	}

	/** query for service 50 matrix account maintenance **/
	public List<Object> findParticipantsForMatrixAccountMaintenance(Map parameters) throws ServiceException {
		StringBuilder stringBuilderSqlBase = new StringBuilder();
		stringBuilderSqlBase.append(" select P.ID_PARTICIPANT_PK AS ID_PARTICIPANT_FK ");
		stringBuilderSqlBase.append(" from PARTICIPANT P ");
		stringBuilderSqlBase.append(" where 1=1 ");
		stringBuilderSqlBase.append(" and P.STATE = " + ParticipantStateType.REGISTERED.getCode());
		stringBuilderSqlBase.append(" and P.ACCOUNTS_STATE = " + GeneralConstants.ZERO_VALUE_INT);
		stringBuilderSqlBase.append(" and P.PAYMENT_STATE = " + GeneralConstants.ONE_VALUE_INTEGER);

		Query query = em.createNativeQuery(stringBuilderSqlBase.toString());
		List<Object> listObject = null;
		try {
			listObject = query.getResultList();
		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}
		}
		return listObject;
	}

	/**
	 * * SERVICE 6 Collection Annual Issuer Calculation daily or monthly
	 * MANTENIMIENTO_CUENTA_EMISOR.
	 *
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 * @see com.pradera.model.billing.type.BaseCollectionType
	 */
	public List<Object> findIssuersForCreateAccount(Map parameters) throws ServiceException {

		StringBuilder strTableComponentBegin = new StringBuilder();

		/** getting data for do merging into dynamic query **/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		Date calculateDate = (Date) parameters.get(GeneralConstants.CALCULATION_DATE);

		StringBuilder stringBuilderSqlBase = new StringBuilder();
		stringBuilderSqlBase.append(" SELECT  I.ID_ISSUER_PK  AS ID_ISSUER_FK,  ");
		stringBuilderSqlBase.append(
				" '' AS ID_SECURITY_CODE_FK, '' AS SECURITY_CLASS, '' AS ID_HOLDER_ACCOUNT_FK, '' AS ID_HOLDER_FK,  ");
		stringBuilderSqlBase.append(" '' AS ID_PARTICIPANT_FK, '' AS ID_ISSUANCE_CODE_FK ");
		stringBuilderSqlBase.append(" FROM ISSUER I ");
		stringBuilderSqlBase.append(" WHERE STATE_ISSUER= " + IssuerStateType.REGISTERED.getCode());
		/** Only Active ***/

		if (PropertiesConstants.PERIOD_CALCULATION_DAILY_PK.equals(billingService.getCalculationPeriod())) {

			stringBuilderSqlBase.append(" and TRUNC(I.REGISTRY_DATE)=TRUNC(:dateCalculationPeriod) ");

		} else if (PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK.equals(billingService.getCalculationPeriod())) {

			stringBuilderSqlBase
					.append(" and to_char(I.REGISTRY_DATE,'YYYY/MM')=to_char(:dateCalculationPeriod,'YYYY/MM') ");

		} else if (PropertiesConstants.PERIOD_CALCULATION_YEARLY_PK.equals(billingService.getCalculationPeriod())) {

			stringBuilderSqlBase.append(
					" AND MOD(months_between(LAST_DAY(:dateCalculationPeriod),LAST_DAY (I.REGISTRY_DATE )), 12)=0  ");
			stringBuilderSqlBase.append(
					" AND  (months_between(LAST_DAY(:dateCalculationPeriod),LAST_DAY (I.REGISTRY_DATE ))/ 12 > 0) ");

		}

		/** Making the table EXCEPTION **/
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters,
				stringBuilderSqlBase);

		log.debug(
				" :::::::::::::::::::::::::::::::::::::Consulta Transaccion con Reglas de Excepcion ::::::::::::::::::::::::::::");
		log.debug(strTableComponentBegin.toString());

		Query query = em.createNativeQuery(strTableComponentBegin.toString());

		query.setParameter("dateCalculationPeriod", calculateDate);

		List<Object> listObject = null;
		try {

			listObject = query.getResultList();

		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}

		}

		return listObject;
	}

	/** query for service 51 issuing account maintenance **/
	public List<Object> findParticipantsForIssuingAccountMaintenance(Map parameters) throws ServiceException {
		StringBuilder stringBuilderSqlBase = new StringBuilder();
		stringBuilderSqlBase.append(" select P.ID_PARTICIPANT_PK AS ID_PARTICIPANT_FK ");
		stringBuilderSqlBase.append(" from PARTICIPANT P ");
		stringBuilderSqlBase.append(" where 1=1 ");
		stringBuilderSqlBase.append(" and P.STATE = " + ParticipantStateType.REGISTERED.getCode());
		stringBuilderSqlBase.append(" and P.ACCOUNTS_STATE = " + GeneralConstants.ONE_VALUE_INTEGER);
		stringBuilderSqlBase.append(" and P.PAYMENT_STATE = " + GeneralConstants.ONE_VALUE_INTEGER);

		Query query = em.createNativeQuery(stringBuilderSqlBase.toString());
		List<Object> listObject = null;
		try {
			listObject = query.getResultList();
		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}
		}
		return listObject;
	}

	/**
	 * SERVICIO 7 Collection First for Create Account Holder CREACION_CUI.
	 *
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 * @see com.pradera.model.billing.type.BaseCollectionType
	 */
	public List<Object> findHolderForCreation(Map parameters) throws ServiceException {

		StringBuilder strTableComponentBegin = new StringBuilder();

		/** getting data for do merging into dynamic query **/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		Date calculateDate = (Date) parameters.get(GeneralConstants.CALCULATION_DATE);

		StringBuilder stringBuilderSqlBase = new StringBuilder();
		stringBuilderSqlBase.append(" SELECT  H.ID_HOLDER_FK  AS ID_HOLDER_FK ");
		stringBuilderSqlBase.append("  FROM HOLDER_REQUEST H ");
		stringBuilderSqlBase.append(" WHERE STATE_HOLDER_REQUEST= " + HolderStateType.REGISTERED.getCode());
		/** Only Active ***/

		if (PropertiesConstants.PERIOD_CALCULATION_DAILY_PK.equals(billingService.getCalculationPeriod())) {

			stringBuilderSqlBase.append(" and TRUNC(H.REGISTRY_DATE)=TRUNC(:dateCalculationPeriod) ");

		} else if (PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK.equals(billingService.getCalculationPeriod())) {

			stringBuilderSqlBase
					.append(" and to_char(H.REGISTRY_DATE,'YYYY/MM')=to_char(:dateCalculationPeriod,'YYYY/MM') ");

		} else if (PropertiesConstants.PERIOD_CALCULATION_YEARLY_PK.equals(billingService.getCalculationPeriod())) {
			/** Se cobra al cierre del cobro del periodo respectivo */
			stringBuilderSqlBase.append(
					" AND MOD(months_between(LAST_DAY(:dateCalculationPeriod), LAST_DAY (H.REGISTRY_DATE )), 12)=0    ");
			stringBuilderSqlBase.append(
					"  AND TO_CHAR(TRUNC(H.REGISTRY_DATE,'YEAR'), 'YYYY')=TO_CHAR(TRUNC(:dateCalculationPeriod,'YEAR'), 'YYYY') ");

		}

		/** Making the table EXCEPTION **/
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters,
				stringBuilderSqlBase);

		log.debug(
				" :::::::::::::::::::::::::::::::::::::Consulta Transaccion con Reglas de Excepcion ::::::::::::::::::::::::::::");
		log.debug(strTableComponentBegin.toString());

		Query query = em.createNativeQuery(strTableComponentBegin.toString());

		query.setParameter("dateCalculationPeriod", calculateDate);

		List<Object> listObject = null;
		try {

			listObject = query.getResultList();

		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}

		}

		return listObject;
	}

	/**
	 * Collection First for Create Account Holder.
	 *
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Object> findSecurityForCreation(Map parameters) throws ServiceException {

		StringBuilder strTableComponentBegin = new StringBuilder();

		/** getting data for do merging into dynamic query **/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		Date calculateDate = (Date) parameters.get(GeneralConstants.CALCULATION_DATE);

		StringBuilder stringBuilderSqlBase = new StringBuilder();
		stringBuilderSqlBase.append(" SELECT  S.ID_ISSUER_FK  AS ID_ISSUER_FK, ");
		stringBuilderSqlBase.append("  '' AS ID_SECURITY_CODE_FK, '' AS SECURITY_CLASS, '' AS ID_HOLDER_ACCOUNT_FK, ");
		stringBuilderSqlBase.append(" '' AS ID_HOLDER_FK, '' AS ID_PARTICIPANT_FK, '' AS ID_ISSUANCE_CODE_FK ");
		stringBuilderSqlBase.append("  FROM SECURITY S ");
		stringBuilderSqlBase
				.append(" WHERE STATE_SECURITY= " + SecurityStateType.REGISTERED.getCode()); /** Only Active ***/
		stringBuilderSqlBase.append(
				" AND MOD(months_between(LAST_DAY(:dateCalculationPeriod), LAST_DAY (S.REGISTRY_DATE )), 12)=0    ");
		stringBuilderSqlBase.append(
				"  AND TO_CHAR(TRUNC(S.REGISTRY_DATE,'YEAR'), 'YYYY')=TO_CHAR(TRUNC(:dateCalculationPeriod,'YEAR'), 'YYYY') ");
		/** Making the table EXCEPTION **/
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters,
				stringBuilderSqlBase);

		log.debug(
				" :::::::::::::::::::::::::::::::::::::Consulta Transaccion con Reglas de Excepcion ::::::::::::::::::::::::::::");
		log.debug(strTableComponentBegin.toString());

		Query query = em.createNativeQuery(strTableComponentBegin.toString());

		query.setParameter("dateCalculationPeriod", calculateDate);

		List<Object> listObject = null;
		try {

			listObject = query.getResultList();

		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}

		}

		return listObject;
	}

	/**
	 * 18 REMATERIALIZACION O REVERSION DE ANOTACIONES EN CUENTA
	 * REMATERIALIZACION_REVERSION_ANOTACIONES_CUENTA_R_FIJA
	 * REMATERIALIZACION_REVERSION_ANOTACIONES_CUENTA_R_VARIABLE.
	 *
	 * @param parameters             the parameters
	 * @param idInstrumentType       the id instrument type
	 * @param idNegotiationMechanism the id negotiation mechanism
	 * @return List<Object>
	 * @throws ServiceException the service exception
	 */
	public List<Object> findMovementReversionAnnotationIntoAccount(Map parameters, Integer idInstrumentType,
			Long idNegotiationMechanism) throws ServiceException {

		StringBuilder strTableComponentBegin = new StringBuilder();

		/** getting data for do merging into dynamic query **/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		List<Integer> listEconomicAct = (List<Integer>) parameters.get(GeneralConstants.ECONOMIC_ACTIVITY_LIST_INT);
		Integer portFolio = billingService.getPortfolio();

		/** getting data for do merging into dynamic query **/
		ServiceRate serviceRate = (ServiceRate) parameters.get(GeneralConstants.SERVICE_RATE);

		/** Range to Dates */
		Date initialDate = (Date) parameters.get(GeneralConstants.FIELD_INITIAL_DATE);
		Date finalDate = (Date) parameters.get(GeneralConstants.FIELD_FINAL_DATE);

		List<RateMovement> rateMovementList = serviceRate.getRateMovements();

		StringBuilder stringBuilderSqlBase = new StringBuilder();
		stringBuilderSqlBase.append(" SELECT QUERY_ALL.* FROM( ");
		stringBuilderSqlBase.append(" SELECT BASE_ALL.*, ");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL.ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)  THEN 1 ELSE 0 END) CARTERA_PROPIA,");
		stringBuilderSqlBase.append(
				"																																																	");
		stringBuilderSqlBase.append(
				" (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) ACT_ECO_PAR,																																				");
		stringBuilderSqlBase.append(
				"                                                                                                                                                                                                                                  						");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL. ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) THEN (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)           ");
		stringBuilderSqlBase.append(
				"      WHEN  BASE_ALL. ID_HOLDER_FK <>  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) and  (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) = 1879   ");
		stringBuilderSqlBase.append(
				"  THEN (SELECT h.ECONOMIC_ACTIVITY from holder h where H.ID_HOLDER_PK = BASE_ALL.ID_HOLDER_FK   AND  STATE_HOLDER = 496 AND h.ECONOMIC_ACTIVITY IN(55,56,57))ELSE NULL END) H_ECONOMIC_ACTIVITY,                                         				");
		stringBuilderSqlBase.append(
				"                                                                                                                                                                                                                                  						");
		stringBuilderSqlBase.append(
				" (CASE WHEN BASE_ALL. ID_HOLDER_FK =  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) THEN (SELECT MNEMONIC FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK)                    ");
		stringBuilderSqlBase.append(
				"      WHEN  BASE_ALL. ID_HOLDER_FK <>  (select P.ID_HOLDER_FK from participant P where ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) and  (SELECT ECONOMIC_ACTIVITY FROM PARTICIPANT WHERE  ID_PARTICIPANT_PK = BASE_ALL.ID_PARTICIPANT_FK) = 1879   ");
		stringBuilderSqlBase.append(
				" THEN (SELECT h.MNEMONIC from holder h where H.ID_HOLDER_PK = BASE_ALL.ID_HOLDER_FK   AND  STATE_HOLDER = 496 AND h.ECONOMIC_ACTIVITY IN(55,56,57))ELSE NULL END) NEMONICO_HOLDER                                                               				");
		stringBuilderSqlBase.append(" ");
		stringBuilderSqlBase.append(" FROM ( ");
		stringBuilderSqlBase.append("");
		stringBuilderSqlBase.append("");
		stringBuilderSqlBase.append(" SELECT  ha.ID_HOLDER_ACCOUNT_PK as ID_HOLDER_ACCOUNT_FK , ");
		stringBuilderSqlBase.append("		  (SELECT HO.ID_HOLDER_PK FROM HOLDER_ACCOUNT_DETAIL HAD, HOLDER HO");
		stringBuilderSqlBase.append("		  WHERE HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK");
		stringBuilderSqlBase.append("		  AND HO.ID_HOLDER_PK=HAD.ID_HOLDER_FK and rownum=1) AS ID_HOLDER_FK,");
		stringBuilderSqlBase.append(" 		  ham.ID_PARTICIPANT_FK as ID_PARTICIPANT_FK , ");
		stringBuilderSqlBase.append("         s.ID_SECURITY_CODE_PK as ID_SECURITY_CODE_FK,");
		stringBuilderSqlBase.append("         s.ID_ISSUER_FK as ID_ISSUER_FK ,");
		stringBuilderSqlBase.append("         S.SECURITY_CLASS as SECURITY_CLASS  ,");
		stringBuilderSqlBase.append("         HAM.ID_MOVEMENT_TYPE_FK AS ID_MOVEMENT_TYPE_FK ,");
		stringBuilderSqlBase.append("         S.CURRENCY AS CURRENCY,");
		stringBuilderSqlBase.append("         HAM.MOVEMENT_DATE as MOVEMENT_DATE,");
		stringBuilderSqlBase.append(
				"         ROUND ( S.CURRENT_NOMINAL_VALUE, DECODE (S.SECURITY_CLASS,426,4,2) )  AS  NOMINAL_VALUE,");
		stringBuilderSqlBase.append("         HAM.OPERATION_PRICE AS OPERATION_PRICE,");
		stringBuilderSqlBase.append("         HAM.MOVEMENT_QUANTITY AS MOVEMENT_QUANTITY, ");
		stringBuilderSqlBase.append("         '' AS ID_ISSUANCE_CODE_FK ");
		stringBuilderSqlBase
				.append(" , HAM.MOVEMENT_DATE AS OPERATION_DATE  , nvl(HAM.OPERATION_NUMBER, 0) AS OPERATION_NUMBER  ");
		stringBuilderSqlBase.append(
				" , nvl(HAM.ID_SOURCE_PARTICIPANT, 0) as ID_SOURCE_PARTICIPANT_FK	 , nvl(HAM.ID_TARGET_PARTICIPANT, 0) AS ID_TARGET_PARTICIPANT_FK  ");
		stringBuilderSqlBase.append(" from RETIREMENT_OPERATION RO ");
		stringBuilderSqlBase.append(
				" INNER JOIN   custody_operation CO   ON CO.ID_CUSTODY_OPERATION_PK =RO.ID_RETIREMENT_OPERATION_PK ");
		stringBuilderSqlBase.append(
				" INNER JOIN HOLDER_ACCOUNT_MOVEMENT HAM ON HAM.ID_CUSTODY_OPERATION_FK=CO.ID_CUSTODY_OPERATION_PK ");
		stringBuilderSqlBase
				.append(" inner join MOVEMENT_TYPE MT ON (MT.ID_MOVEMENT_TYPE_PK = HAM.ID_MOVEMENT_TYPE_FK) ");
		stringBuilderSqlBase
				.append(" left outer join HOLDER_ACCOUNT HA on (HA.ID_HOLDER_ACCOUNT_PK = HAM.ID_HOLDER_ACCOUNT_FK) ");
		stringBuilderSqlBase
				.append(" left outer join SECURITY S on ( HAM.ID_SECURITY_CODE_FK=S.ID_SECURITY_CODE_PK ) ");
		stringBuilderSqlBase.append(" left outer join ISSUER I on (I.ID_ISSUER_PK=S.ID_ISSUER_FK) ");
		stringBuilderSqlBase.append(" WHERE ");
		stringBuilderSqlBase.append(" 	S.INSTRUMENT_TYPE=" + idInstrumentType);// ---124 RENTA FIJA --399 RENTA
																				// VARIABLE
		stringBuilderSqlBase.append(" AND RO.IND_RETIREMENT_EXPIRATION <> 1");// Excluir las Operaciones de Retiro de
																				// valores Vencidos

		if (Validations.validateIsNotNull(idNegotiationMechanism)) {
			stringBuilderSqlBase.append("  AND HAM.ID_MECHANISM=:idNegotiationMechanism ");
		}

		if (Validations.validateListIsNotNullAndNotEmpty(rateMovementList)) {

			stringBuilderSqlBase.append(" AND HAM.ID_MOVEMENT_TYPE_FK in (");
			StringBuilder stringBuilderMovements = new StringBuilder();

			for (int i = 0; i < rateMovementList.size(); i++) {
				RateMovement rateMovement = rateMovementList.get(i);
				String caracter = (i == 0 ? " " : ",");
				stringBuilderMovements.append(caracter + rateMovement.getMovementType().getIdMovementTypePk() + " ");
			}

			stringBuilderSqlBase.append(stringBuilderMovements.toString()).append(" )");
		}
		/** structure ::::::::: and HAM.ID_MOVEMENT_TYPE_FK in ( 1,2,3,4) */

		if (Validations.validateIsNotNull(initialDate) && Validations.validateIsNotNull(finalDate)) {

			stringBuilderSqlBase
					.append(" AND  HAM.MOVEMENT_DATE >= :initialDate  AND HAM.MOVEMENT_DATE<= :finalDate   ");
		}

		stringBuilderSqlBase.append(" ORDER BY HA.ID_HOLDER_ACCOUNT_PK, ID_HOLDER_FK , ");
		stringBuilderSqlBase
				.append(" HAM.ID_PARTICIPANT_FK , S.ID_SECURITY_CODE_PK, S.ID_ISSUER_FK, HAM.ID_MOVEMENT_TYPE_FK  ");

		stringBuilderSqlBase.append(" ) BASE_ALL");
		stringBuilderSqlBase.append(" ) QUERY_ALL");
		stringBuilderSqlBase.append(" WHERE 1=1 ");
		if (!BillingServicePortfolioType.ALL.getCode().equals(portFolio)) {
			stringBuilderSqlBase.append(" AND QUERY_ALL.CARTERA_PROPIA=" + BooleanType.get(portFolio).getBinaryValue());
		}
		stringBuilderSqlBase.append(" AND QUERY_ALL.ACT_ECO_PAR IN(:listEconomicAct)");

		/** Making the table EXCEPTION **/
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters,
				stringBuilderSqlBase);

		log.debug(
				" :::::::::::::::::::::::::::::::::::::Consulta Transaccion con Reglas de Excepcion ::::::::::::::::::::::::::::");
		log.debug(strTableComponentBegin.toString());

		Query query = em.createNativeQuery(strTableComponentBegin.toString());
		if (Validations.validateIsNotNull(initialDate) && Validations.validateIsNotNull(finalDate)) {

			query.setParameter(GeneralConstants.FIELD_INITIAL_DATE, initialDate);
			query.setParameter(GeneralConstants.FIELD_FINAL_DATE, finalDate);
		}

		if (Validations.validateIsNotNull(idNegotiationMechanism)) {
//			query.setParameter("idNegotiationMechanism", idNegotiationMechanism);
		}
		query.setParameter("listEconomicAct", listEconomicAct);

		List<Object> listObject = null;
		try {

			listObject = query.getResultList();

		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}

		}

		return listObject;
	}

	/**
	 * Find movement settlement with out mechanism by entity collection.
	 *
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 * @deprecated
	 */
	public List<Object> findMovementSettlementWithOutMechanismByEntityCollection(Map parameters)
			throws ServiceException {

		StringBuilder strTableComponentBegin = new StringBuilder();

		/** getting data for do merging into dynamic query **/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);

		/** getting data for do merging into dynamic query **/
		ServiceRate serviceRate = (ServiceRate) parameters.get(GeneralConstants.SERVICE_RATE);

		/** Range to Dates */
		Date initialDate = (Date) parameters.get(GeneralConstants.FIELD_INITIAL_DATE);
		Date finalDate = (Date) parameters.get(GeneralConstants.FIELD_FINAL_DATE);

		List<RateMovement> rateMovementList = serviceRate.getRateMovements();

		StringBuilder stringBuilderSqlBase = new StringBuilder();
		stringBuilderSqlBase.append(" select  HA.ID_HOLDER_ACCOUNT_PK as ID_HOLDER_ACCOUNT_FK , ");
//		stringBuilderSqlBase.append(" 		  had.ID_HOLDER_FK as ID_HOLDER_FK, ");
		stringBuilderSqlBase.append("		  (SELECT HO.ID_HOLDER_PK FROM HOLDER_ACCOUNT_DETAIL HAD, HOLDER HO");
		stringBuilderSqlBase.append("		  WHERE HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK");
		stringBuilderSqlBase.append("		  AND HO.ID_HOLDER_PK=HAD.ID_HOLDER_FK and rownum=1) AS ID_HOLDER_FK,");
		stringBuilderSqlBase.append(" 		  ham.ID_PARTICIPANT_FK as ID_PARTICIPANT_FK , ");
		stringBuilderSqlBase.append("         S.ID_SECURITY_CODE_PK as ID_SECURITY_CODE_FK,");
		stringBuilderSqlBase.append("         S.ID_ISSUER_FK as ID_ISSUER_FK ,");
		stringBuilderSqlBase.append("         S.SECURITY_CLASS as SECURITY_CLASS  ,");
		stringBuilderSqlBase.append("        '' AS ID_ISSUANCE_CODE_FK , ");
		stringBuilderSqlBase.append("         HAM.ID_MOVEMENT_TYPE_FK AS ID_MOVEMENT_TYPE_FK ,");
		stringBuilderSqlBase.append("         MO.CURRENCY AS CURRENCY,");
		stringBuilderSqlBase.append("         HAM.MOVEMENT_DATE as MOVEMENT_DATE,");

//		if(billingService.getBaseCollection().equals(BaseCollectionType.OPERACIONES_DE_LIQUIDACION_FP.getCode())){
//			
//			stringBuilderSqlBase.append("    HAM.NOMINAL_VALUE AS OPERATION_PRICE,");
//			
//		/** Uses for the base collection TRANSACCIONES_EFECTIVO_RECOMPRA - it uses the attribute NOMINAL_VALUE**/
//		}else if(billingService.getBaseCollection().equals(BaseCollectionType.OPERACIONES_DE_LIQUIDACION_DVP.getCode())){
//			
//			stringBuilderSqlBase.append("    HAM.OPERATION_PRICE AS OPERATION_PRICE,");
//		}

		stringBuilderSqlBase.append("   HAM.MOVEMENT_QUANTITY AS MOVEMENT_QUANTITY, ");
		stringBuilderSqlBase.append(" 	HAM.MOVEMENT_DATE AS OPERATION_DATE  ,  ");
		stringBuilderSqlBase.append(" 	NVL(HAM.OPERATION_NUMBER, 0) AS OPERATION_NUMBER , ");
		stringBuilderSqlBase.append(" 	NVL(HAM.ID_SOURCE_PARTICIPANT, 0) as ID_SOURCE_PARTICIPANT_FK	 , ");
		stringBuilderSqlBase.append(" 	NVL(HAM.ID_TARGET_PARTICIPANT, 0) AS ID_TARGET_PARTICIPANT_FK  ");
		stringBuilderSqlBase.append(" FROM HOLDER_ACCOUNT_MOVEMENT HAM ");
		stringBuilderSqlBase.append(
				" left outer join MECHANISM_OPERATION MO on ( MO.ID_MECHANISM_OPERATION_PK=HAM.ID_TRADE_OPERATION_FK ) ");
		stringBuilderSqlBase
				.append(" inner join MOVEMENT_TYPE MT ON (MT.ID_MOVEMENT_TYPE_PK = HAM.ID_MOVEMENT_TYPE_FK) ");
		stringBuilderSqlBase
				.append(" left outer join HOLDER_ACCOUNT HA on (HA.ID_HOLDER_ACCOUNT_PK = HAM.ID_HOLDER_ACCOUNT_FK) ");
//		stringBuilderSqlBase.append(" left outer join HOLDER_ACCOUNT_DETAIL HAD on (HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK) ");
//		stringBuilderSqlBase.append(" left outer join HOLDER HO on (HO.ID_HOLDER_PK=HAD.ID_HOLDER_FK) ");
		stringBuilderSqlBase
				.append(" left outer join SECURITY S on ( HAM.ID_SECURITY_CODE_FK=S.ID_SECURITY_CODE_PK ) ");
		stringBuilderSqlBase.append(" left outer join ISSUER I on (I.ID_ISSUER_PK=S.ID_ISSUER_FK) ");
		stringBuilderSqlBase.append(" WHERE 1=1  ");

		if (Validations.validateListIsNotNullAndNotEmpty(rateMovementList)) {

			stringBuilderSqlBase.append(" AND HAM.ID_MOVEMENT_TYPE_FK in (");
			StringBuilder stringBuilderMovements = new StringBuilder();

			for (int i = 0; i < rateMovementList.size(); i++) {
				RateMovement rateMovement = rateMovementList.get(i);
				String caracter = (i == 0 ? " " : ",");
				stringBuilderMovements.append(caracter + rateMovement.getMovementType().getIdMovementTypePk() + " ");
			}

			stringBuilderSqlBase.append(stringBuilderMovements.toString()).append(" )");
		}
		/** structure ::::::::: AND HAM.ID_MOVEMENT_TYPE_FK in ( 1,2,3,4) */

		if (Validations.validateIsNotNull(initialDate) && Validations.validateIsNotNull(finalDate)) {

			stringBuilderSqlBase
					.append(" and  HAM.MOVEMENT_DATE >= :initialDate  and HAM.MOVEMENT_DATE<= :finalDate   ");
		}

		stringBuilderSqlBase.append(" order by HA.ID_HOLDER_ACCOUNT_PK, ID_HOLDER_FK , ");
		stringBuilderSqlBase
				.append(" HAM.ID_PARTICIPANT_FK , S.ID_SECURITY_CODE_PK, S.ID_ISSUER_FK, HAM.ID_MOVEMENT_TYPE_FK  ");

		/** Making the table EXCEPTION **/
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters,
				stringBuilderSqlBase);

		log.debug(
				" :::::::::::::::::::::::::::::::::::::Consulta Transaccion con Reglas de Excepcion ::::::::::::::::::::::::::::");
		log.debug(strTableComponentBegin.toString());

		Query query = em.createNativeQuery(strTableComponentBegin.toString());
		if (Validations.validateIsNotNull(initialDate) && Validations.validateIsNotNull(finalDate)) {

			query.setParameter(GeneralConstants.FIELD_INITIAL_DATE, initialDate);
			query.setParameter(GeneralConstants.FIELD_FINAL_DATE, finalDate);
		}

		List<Object> listObject = null;
		try {

			listObject = query.getResultList();

		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}

		}

		return listObject;
	}

	/**
	 * 
	 * EVENTUAL COLLECTION.
	 *
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	/**
	 * @deprecated
	 * @param parameters
	 * @return
	 * @throws ServiceException
	 */
	public List<Object> processSecurityCustodyHolder(Map parameters) throws ServiceException {

		/**
		 * I have to recived the BillingService ServiceRate Holder, HolderAccount
		 * SecurityCode
		 */

		Long holderId = null;
		Long holderAccountId = null;

		if (Validations.validateIsNotNull(parameters.get(GeneralConstants.ID_HOLDER_PK))) {
			holderId = Long.parseLong(parameters.get(GeneralConstants.ID_HOLDER_PK).toString());
		}

		if (Validations.validateIsNotNull(parameters.get(GeneralConstants.ID_HOLDER_ACCOUNT_PK))) {
			holderAccountId = Long.parseLong(parameters.get(GeneralConstants.ID_HOLDER_ACCOUNT_PK).toString());
		}

		StringBuilder strTableComponentBegin = new StringBuilder();

		/** Start Query Base **/
		StringBuilder stringBuilderSqlBase = new StringBuilder();
		stringBuilderSqlBase.append(" select ha.id_holder_account_pk as ID_HOLDER_ACCOUNT_FK , ");
//		stringBuilderSqlBase.append("had.ID_HOLDER_FK as ID_HOLDER_FK, ");
		stringBuilderSqlBase.append("		  (SELECT HO.ID_HOLDER_PK FROM HOLDER_ACCOUNT_DETAIL HAD, HOLDER HO");
		stringBuilderSqlBase.append("		  WHERE HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK");
		stringBuilderSqlBase.append("		  AND HO.ID_HOLDER_PK=HAD.ID_HOLDER_FK and rownum=1) AS ID_HOLDER_FK,");
		stringBuilderSqlBase.append(
				" HAB.ID_PARTICIPANT_PK as ID_PARTICIPANT_FK , s.ID_SECURITY_CODE_PK as  ID_SECURITY_CODE_FK, ");
		stringBuilderSqlBase.append(" s.ID_ISSUER_FK as  ID_ISSUER_FK , s.SECURITY_CLASS as SECURITY_CLASS ");
		stringBuilderSqlBase.append(" , '' as ID_ISSUANCE_CODE_FK ");
//		stringBuilderSqlBase.append("  from MARKET_FACT_VIEW HAB ");
		stringBuilderSqlBase.append("   FROM HOLDER_ACCOUNT_BALANCE HAB   ");
		stringBuilderSqlBase.append(
				" left outer join HOLDER_ACCOUNT HA on ( HA.ID_HOLDER_ACCOUNT_PK = HAB.ID_HOLDER_ACCOUNT_PK ) ");
//		stringBuilderSqlBase.append(" left outer join HOLDER_ACCOUNT_DETAIL HAD on ( HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ) ");
//		stringBuilderSqlBase.append(" left outer join HOLDER HO on ( HO.ID_HOLDER_PK = HAD.ID_HOLDER_FK ) ");
		stringBuilderSqlBase
				.append(" left outer join SECURITY S on ( HAB.ID_SECURITY_CODE_PK = S.ID_SECURITY_CODE_PK ) ");
		stringBuilderSqlBase.append(" left outer join ISSUER I on (I.ID_ISSUER_PK=S.ID_ISSUER_FK) ");
		stringBuilderSqlBase.append(" WHERE ");
		stringBuilderSqlBase.append(" 	HA.ID_HOLDER_ACCOUNT_PK = :holderAccount  ");
		stringBuilderSqlBase.append(" 	AND HO.ID_HOLDER_PK = :holder  ");
		stringBuilderSqlBase.append(" 	AND S.ID_SECURITY_CODE_PK = :securityCode  ");

		/** Making the table EXCEPTION to Eventual Base Collection **/
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters,
				stringBuilderSqlBase);

		Query query = em.createNativeQuery(strTableComponentBegin.toString());

		if (Validations.validateIsNotNull(holderAccountId)) {
			query.setParameter("holderAccount", holderAccountId);
		}

		if (Validations.validateIsNotNull(holderId)) {
			query.setParameter("holder", holderId);
		}

		if (Validations.validateIsNotNull(parameters.get(GeneralConstants.ID_SECURITY_CODE))) {
			query.setParameter("securityCode", parameters.get(GeneralConstants.ID_SECURITY_CODE));
		}

		List<Object> listObject = null;
		try {

			listObject = query.getResultList();

		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}

		}

		return listObject;
	}

	/**
	 * Process security custody issuer.
	 *
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 * @deprecated
	 */
	public List<Object> processSecurityCustodyIssuer(Map parameters) throws ServiceException {

		/**
		 * I have to received the BillingService ServiceRate Holder, HolderAccount or
		 * issuer SecurityCode
		 */

		StringBuilder strTableComponentBegin = new StringBuilder();

		/** Start Query Base **/
		StringBuilder stringBuilderSqlBase = new StringBuilder();
		stringBuilderSqlBase.append(" select ha.id_holder_account_pk as ID_HOLDER_ACCOUNT_FK ,  ");
//		stringBuilderSqlBase.append("HAD.ID_HOLDER_FK as ID_HOLDER_FK,");
		stringBuilderSqlBase
				.append("					(SELECT HO.ID_HOLDER_PK FROM HOLDER_ACCOUNT_DETAIL HAD, HOLDER HO");
		stringBuilderSqlBase.append("					WHERE HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK");
		stringBuilderSqlBase
				.append("					AND HO.ID_HOLDER_PK=HAD.ID_HOLDER_FK and rownum=1) AS ID_HOLDER_FK,");
		stringBuilderSqlBase.append(
				" hab.ID_PARTICIPANT_PK as ID_PARTICIPANT_FK , s.ID_SECURITY_CODE_PK as  ID_SECURITY_CODE_FK, ");
		stringBuilderSqlBase.append(" I.ID_ISSUER_PK as  ID_ISSUER_FK , s.SECURITY_CLASS as SECURITY_CLASS ");
		stringBuilderSqlBase.append(" ,'' as ID_ISSUANCE_CODE_FK ");
//		stringBuilderSqlBase.append(" from MARKET_FACT_VIEW HAB ");
		stringBuilderSqlBase.append("   FROM HOLDER_ACCOUNT_BALANCE HAB   ");
		stringBuilderSqlBase.append(
				" left outer join HOLDER_ACCOUNT HA on ( HA.ID_HOLDER_ACCOUNT_PK = HAB.ID_HOLDER_ACCOUNT_PK ) ");
//		stringBuilderSqlBase.append(" left outer join HOLDER_ACCOUNT_DETAIL HAD on ( HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK ) ");
//		stringBuilderSqlBase.append(" left outer join HOLDER HO on ( HO.ID_HOLDER_PK = HAD.ID_HOLDER_FK ) ");
		stringBuilderSqlBase
				.append(" left outer join SECURITY S on ( HAB.ID_SECURITY_CODE_PK = S.ID_SECURITY_CODE_PK ) ");
		stringBuilderSqlBase.append(" left outer join ISSUER I on (I.ID_ISSUER_PK=S.ID_ISSUER_FK) ");
		stringBuilderSqlBase.append(" where 1=1   ");
		stringBuilderSqlBase.append(" AND I.id_issuer_pk = :issuer  ");
		stringBuilderSqlBase.append(" AND S.ID_SECURITY_CODE_PK= :securityCode ");

		/** Making the table EXCEPTION to Eventual Base Collection **/
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters,
				stringBuilderSqlBase);

		Query query = em.createNativeQuery(strTableComponentBegin.toString());

		if (Validations.validateIsNotNull(parameters.get(GeneralConstants.ID_ISSUER_PK))) {
			query.setParameter("issuer", parameters.get(GeneralConstants.ID_ISSUER_PK));
		}

		if (Validations.validateIsNotNull(parameters.get(GeneralConstants.ID_SECURITY_CODE))) {
			query.setParameter("securityCode", parameters.get(GeneralConstants.ID_SECURITY_CODE));
		}

		List<Object> listObject = null;
		try {

			listObject = query.getResultList();

		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}

		}

		return listObject;
	}

	/**
	 * 
	 *
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 * @deprecated BASE COLLECTION FOR ENTITY
	 */
	public List<Object> processCertifiedSecurity(Map parameters) throws ServiceException {

		/**
		 * I have to recived the BillingService ServiceRate Holder or Issuer
		 */
		Long participantId = null;
		Long holderId = null;

		if (Validations.validateIsNotNull(parameters.get(GeneralConstants.ID_PARTICIPANT_PK))) {
			participantId = Long.parseLong(parameters.get(GeneralConstants.ID_PARTICIPANT_PK).toString());
		}

		if (Validations.validateIsNotNull(parameters.get(GeneralConstants.ID_HOLDER_PK))) {
			holderId = Long.parseLong(parameters.get(GeneralConstants.ID_HOLDER_PK).toString());
		}

		StringBuilder strTableComponentBegin = new StringBuilder();

		/** Start Query Base **/
		StringBuilder stringBuilderSqlBase = new StringBuilder();

		if (Validations.validateIsNotNull(holderId)) {
			stringBuilderSqlBase.append(" select '' ID_ISSUER_FK, '' ID_PARTICIPANT_FK , '' ID_SECURITY_CODE_FK,  ");
			stringBuilderSqlBase.append(
					" '' SECURITY_CLASS, '' ID_HOLDER_ACCOUNT_FK , ID_HOLDER_PK as ID_HOLDER_FK, '' ID_ISSUANCE_CODE_FK  ");
			stringBuilderSqlBase.append(" from Holder  where (1=1) ");
			stringBuilderSqlBase.append(" AND ID_HOLDER_PK = :idHolder ");
		}

		if (Validations.validateIsNotNull(parameters.get(GeneralConstants.ID_ISSUER_PK))) {
			stringBuilderSqlBase.append(
					" select id_issuer_pk AS id_issuer_fk, '' ID_PARTICIPANT_FK , '' ID_SECURITY_CODE_FK, '' SECURITY_CLASS,  ");
			stringBuilderSqlBase.append(" '' ID_HOLDER_ACCOUNT_FK , '' ID_HOLDER_FK, '' ID_ISSUANCE_CODE_FK ");
			stringBuilderSqlBase.append(" from ISSUER   where (1=1)  ");
			stringBuilderSqlBase.append(" AND id_issuer_pk = :idIssuer  ");
		}

		if (Validations.validateIsNotNull(participantId)) {
			stringBuilderSqlBase.append(
					"  select ID_PARTICIPANT_PK as ID_PARTICIPANT_FK, '' ID_ISSUER_FK,          '' ID_HOLDER_FK,  ");
			stringBuilderSqlBase.append(
					" '' ID_SECURITY_CODE_FK,       '' SECURITY_CLASS,   '' ID_HOLDER_ACCOUNT_FK , '' ID_ISSUANCE_CODE_FK ");
			stringBuilderSqlBase.append(" from PARTICIPANT  where (1=1)  ");

			stringBuilderSqlBase.append(" AND ID_PARTICIPANT_PK = :idParticipant  ");

		}

		/** Making the table EXCEPTION to Eventual Base Collection **/
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters,
				stringBuilderSqlBase);
		Query query = em.createNativeQuery(strTableComponentBegin.toString());

		if (Validations.validateIsNotNull(holderId)) {

			query.setParameter("idHolder", holderId);
		}

		if (Validations.validateIsNotNull(parameters.get(GeneralConstants.ID_ISSUER_PK))) {

			query.setParameter("idIssuer", parameters.get(GeneralConstants.ID_ISSUER_PK));
		}

		if (Validations.validateIsNotNull(participantId)) {

			query.setParameter("idParticipant", participantId);
		}

		List<Object> listObject = null;
		try {

			listObject = query.getResultList();

		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}

		}

		return listObject;
	}

	/**
	 * 
	 *
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 * @deprecated BASE COLLECTION FOR ENTITY ACCESS
	 */
	public List<CollectionRecordTo> accessQuotaForEntity(Map parameters) throws ServiceException {

		/** getting data for do merging into dynamic query **/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		Date dateCalculation = (Date) parameters.get(GeneralConstants.CALCULATION_DATE);
		String strDate = CommonsUtilities.convertDatetoString(dateCalculation);

		List<Long> longPkParticipant = new ArrayList<Long>();
		List<String> longPkIssuer = new ArrayList<String>();

		List<UserAccountSession> usersAccounts = null;
		Integer institution;
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		ParameterTable parameterTable;

		List<CollectionRecordTo> collectionRecordToList = new ArrayList<CollectionRecordTo>();

		parameterTableTO.setParameterTablePk(billingService.getEntityCollection());
		parameterTable = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO).get(0);
		institution = parameterTable.getElementSubclasification1();

		if (Validations.validateIsNotNull(institution)) {
			/** TRAER LOS USUARIOS BLOQUEADOS Y REGISTRADOS */
			usersAccounts = clientRestService.getUsersInformationForBilling(null, institution, null, null, strDate);

		}

		if (Validations.validateListIsNotNullAndNotEmpty(usersAccounts)) {

			for (UserAccountSession usersAccount : usersAccounts) {

				if (Validations.validateIsNotNull(usersAccount.getInstitutionType())) {

					CollectionRecordTo collectionRecordTo = new CollectionRecordTo();

					if ((usersAccount.getInstitutionType().equals(InstitutionType.PARTICIPANT.getCode()))
							&& (Validations.validateIsNotNullAndPositive(usersAccount.getParticipantCode()))) {

						collectionRecordTo.setParticipant(new Participant(usersAccount.getParticipantCode()));
						collectionRecordTo.setUserCount(usersAccount.getCountUser());
						collectionRecordTo.setEntityCollection(EntityCollectionType.PARTICIPANTS.getCode());

						collectionRecordToList.add(collectionRecordTo);
					} else if (usersAccount.getInstitutionType().equals(InstitutionType.AFP.getCode())
							&& (Validations.validateIsNotNullAndPositive(usersAccount.getParticipantCode()))) {

						collectionRecordTo.setParticipant(new Participant(usersAccount.getParticipantCode()));
						collectionRecordTo.setUserCount(usersAccount.getCountUser());
						collectionRecordTo.setEntityCollection(EntityCollectionType.AFP.getCode());

						collectionRecordToList.add(collectionRecordTo);
					} else if (usersAccount.getInstitutionType().equals(InstitutionType.ISSUER.getCode())
							&& (Validations.validateIsNotNullAndNotEmpty(usersAccount.getIssuerCode()))) {

						collectionRecordTo.setIssuer(new Issuer(usersAccount.getIssuerCode()));
						collectionRecordTo.setUserCount(usersAccount.getCountUser());
						collectionRecordTo.setEntityCollection(EntityCollectionType.ISSUERS.getCode());

						collectionRecordToList.add(collectionRecordTo);
					} else if (usersAccount.getInstitutionType().equals(InstitutionType.BCR.getCode())) {
						collectionRecordTo.setUserCount(usersAccount.getCountUser());
						collectionRecordTo.setEntityCollection(EntityCollectionType.CENTRAL_BANK.getCode());

						collectionRecordToList.add(collectionRecordTo);
					} else if (usersAccount.getInstitutionType().equals(InstitutionType.BOLSA.getCode())) {
						collectionRecordTo.setUserCount(usersAccount.getCountUser());
						collectionRecordTo.setEntityCollection(EntityCollectionType.STOCK_EXCHANGE.getCode());

						collectionRecordToList.add(collectionRecordTo);
//					}else if(usersAccount.getInstitutionType().equals(InstitutionType.MIN_HAC.getCode())){
//						collectionRecordTo.setUserCount(usersAccount.getCountUser());
//						collectionRecordTo.setEntityCollection(EntityCollectionType.HACIENDA.getCode());
//						
//						collectionRecordToList.add(collectionRecordTo);
//					}
//					else if(usersAccount.getInstitutionType().equals(InstitutionType.REGULATOR_ENTITY.getCode())){
//						collectionRecordTo.setUserCount(usersAccount.getCountUser());
//						collectionRecordTo.setEntityCollection(EntityCollectionType.REGULATOR_ENTITY.getCode());
//						
//						collectionRecordToList.add(collectionRecordTo); 
					}

				}

			}

			if (!(billingService.getEntityCollection().equals(EntityCollectionType.ISSUERS.getCode())
					|| billingService.getEntityCollection().equals(EntityCollectionType.PARTICIPANTS.getCode())
					|| billingService.getEntityCollection().equals(EntityCollectionType.AFP.getCode()))) {
				return collectionRecordToList;
			}

			/** MAKING FILTERS BY EACH ENTITY */

			StringBuilder strTableComponentBegin = new StringBuilder();

			/** Start Query Base **/
			StringBuilder stringBuilderSqlBase = new StringBuilder();

			if (billingService.getEntityCollection().equals(EntityCollectionType.ISSUERS.getCode())) {
				stringBuilderSqlBase.append(
						" select id_issuer_pk AS id_issuer_fk, '' ID_PARTICIPANT_FK , '' ID_SECURITY_CODE_FK, '' SECURITY_CLASS,  ");
				stringBuilderSqlBase.append(" '' ID_HOLDER_ACCOUNT_FK , '' ID_HOLDER_FK, '' ID_ISSUANCE_CODE_FK ");
				stringBuilderSqlBase.append(" from ISSUER   where  ");
				stringBuilderSqlBase.append("  id_issuer_pk in ( :idIssuer )  ");

				for (CollectionRecordTo collectionRecordTo : collectionRecordToList) {

					longPkIssuer.add(collectionRecordTo.getIssuer().getIdIssuerPk());
				}
			}

			else if (billingService.getEntityCollection().equals(EntityCollectionType.PARTICIPANTS.getCode())
					|| billingService.getEntityCollection().equals(EntityCollectionType.AFP.getCode())) {
				stringBuilderSqlBase.append(
						"  select ID_PARTICIPANT_PK as ID_PARTICIPANT_FK, '' ID_ISSUER_FK,          '' ID_HOLDER_FK,  ");
				stringBuilderSqlBase.append(
						" '' ID_SECURITY_CODE_FK,       '' SECURITY_CLASS,   '' ID_HOLDER_ACCOUNT_FK , '' ID_ISSUANCE_CODE_FK ");
				stringBuilderSqlBase.append(" from PARTICIPANT  where  ");
				stringBuilderSqlBase.append(" ID_PARTICIPANT_PK in ( :idParticipant )  ");

				for (CollectionRecordTo collectionRecordTo : collectionRecordToList) {

					longPkParticipant.add(collectionRecordTo.getParticipant().getIdParticipantPk());
				}
			}

			strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters,
					stringBuilderSqlBase);
			Query query = em.createNativeQuery(strTableComponentBegin.toString());

			if (billingService.getEntityCollection().equals(EntityCollectionType.ISSUERS.getCode())) {

				query.setParameter("idIssuer", longPkIssuer);
			}

			if (billingService.getEntityCollection().equals(EntityCollectionType.PARTICIPANTS.getCode())
					|| billingService.getEntityCollection().equals(EntityCollectionType.AFP.getCode())) {

				query.setParameter("idParticipant", longPkParticipant);
			}

			List<Object> listObject = null;
			try {

				listObject = query.getResultList();

			} catch (Exception ex) {
				if (ex.getCause() instanceof SQLGrammarException) {
					throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
				} else {
					throw new RuntimeException(ex.getMessage());
				}
			}

			if (Validations.validateListIsNotNullAndNotEmpty(listObject)) {
				List<Object> listUserNotFound = new ArrayList<Object>();
				for (Iterator<CollectionRecordTo> iterOriginal = collectionRecordToList.iterator(); iterOriginal
						.hasNext();) {
					CollectionRecordTo collectionRecordTo = (CollectionRecordTo) iterOriginal.next();

					Boolean exist = Boolean.FALSE;
					for (Iterator<Object> iter = listObject.iterator(); iter.hasNext();) {
						Object object = (Object) iter.next();
						Object excluded = new Object();
						if (billingService.getEntityCollection().equals(EntityCollectionType.PARTICIPANTS.getCode())
								|| billingService.getEntityCollection().equals(EntityCollectionType.AFP.getCode())) {

							if (collectionRecordTo.getParticipant().getIdParticipantPk()
									.equals(((BigDecimal) object).longValue())) {
								excluded = object.toString();
								exist = Boolean.TRUE;
								break;
							}

						} else if (billingService.getEntityCollection()
								.equals(EntityCollectionType.ISSUERS.getCode())) {

							if (collectionRecordTo.getIssuer().getIdIssuerPk().equals(((String) object))) {
								excluded = object.toString();
								exist = Boolean.TRUE;
								break;
							}
						}
						listUserNotFound.add(excluded);
					}

					/**
					 * Aqui si no existe la entidad de cobro, notificar
					 */
					if (!exist) {
						iterOriginal.remove();
					}
				}

				if (Validations.validateIsNotNullAndNotEmpty(listUserNotFound)) {
					BusinessProcess businessProcess = new BusinessProcess();

					businessProcess
							.setIdBusinessProcessPk(BusinessProcessType.MONTHLY_COLLECT_SERVICE_CALCULATION.getCode());

					String message = messageConfiguration.getProperty("body.message.warning.quota.access");
					message = MessageFormat.format(message,
							EntityCollectionType.get(billingService.getEntityCollection()),
							BillingUtils.convertListObjectToString(listUserNotFound,
									GeneralConstants.STR_COMMA_WITHOUT_SPACE));

					List<UserAccountSession> listUser = this.businessLayerCommonFacade.getListUsserByBusinessProcess(
							BusinessProcessType.MONTHLY_COLLECT_SERVICE_CALCULATION.getCode());

					businessLayerCommonFacade.sendNotificationManual(businessProcess, listUser, message, message,
							NotificationType.MESSAGE);
					businessLayerCommonFacade.sendNotificationManual(businessProcess, listUser, message, message,
							NotificationType.EMAIL);

				}

			}

		}
		return collectionRecordToList;
	}

	/*
	 * Busqueda de idParticipantPk segun el emisor
	 */
	public String getIdParticipantPkByIdIssuerPk(String idIssuerPk) {
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(
				"select ID_PARTICIPANT_PK from PARTICIPANT where DOCUMENT_NUMBER=(select DOCUMENT_NUMBER from issuer where ID_ISSUER_PK='"
						+ idIssuerPk + "')");
		Query query = em.createNativeQuery(strQuery.toString());

		BigDecimal idParticipantPk = null;
		try {
			idParticipantPk = (BigDecimal) query.getSingleResult();
		} catch (Exception e) {
			System.out.println("ERROR AL EJECUTAR LA CONSULTA DE BUSQUEDA DE PARTICIPANTE SEGUN EMISOR");
			return idIssuerPk;
		}
		return idParticipantPk.toString();
	}

	/*
	 * Metodo que verifica si el cobro se hace al emisor y se factura al
	 * participante
	 */
	public boolean isPaymentByparticipant(BillingService billingService) {
		if (billingService.getClientServiceType() == 1520 && billingService.getEntityCollection() == 1407)
			return true;
		else
			return false;
	}
}
