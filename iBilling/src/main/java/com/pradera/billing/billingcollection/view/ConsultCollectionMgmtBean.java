package com.pradera.billing.billingcollection.view;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import com.pradera.billing.billingcollection.facade.CollectionServiceMgmtServiceFacade;
import com.pradera.billing.billingcollection.facade.ConsultCollectionMgmtServiceFacade;
import com.pradera.billing.billingcollection.to.CollectionRecordTo;
import com.pradera.billing.billingcollection.to.ProcessedServiceTo;
import com.pradera.billing.billingrate.to.DetailRateMovementTo;
import com.pradera.billing.billingrate.to.RateMovementTo;
import com.pradera.billing.billingrate.to.RateServiceToDataModel;
import com.pradera.billing.billingrate.to.ServiceRateTo;
import com.pradera.billing.billingservice.to.BillingServiceTo;
import com.pradera.billing.common.utils.BillingUtils;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.view.IssuerHelperBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantClassType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.ProcessedService;
import com.pradera.model.billing.type.EntityCollectionType;
import com.pradera.model.billing.type.RateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ConsultCollectionMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class ConsultCollectionMgmtBean extends GenericBaseBean  {

	/** The Constant serialVersionUID. */	
	private static final long serialVersionUID = 1L;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The log. */
	@Inject
	private  transient PraderaLogger log;
	
	/** The issuer helper bean. */
	@Inject  
	IssuerHelperBean 								issuerHelperBean;
	
	/** The helper component facade. */
	@EJB 
	HelperComponentFacade 							helperComponentFacade;
	
	/** The general parameters facade. */
	@EJB 
	GeneralParametersFacade 						generalParametersFacade;
	
	/** The collection service mgmt service facade. */
	@EJB 
	CollectionServiceMgmtServiceFacade 				collectionServiceMgmtServiceFacade;
	
	/** The consult collection mgmt service facade. */
	@EJB 
	ConsultCollectionMgmtServiceFacade 				consultCollectionMgmtServiceFacade;
	
	/** The billing service selection. */
	private BillingService 							billingServiceSelection;
	
	/** The billing service to. */
	private BillingServiceTo 						billingServiceTo;
	
	/** The processed service data model. */
	private GenericDataModel<ProcessedService> 		processedServiceDataModel;
	
	/** The processed service to data model. */
	private GenericDataModel<ProcessedServiceTo> 	processedServiceToDataModel;
	
	/** The collection record to data model. */
	private GenericDataModel<CollectionRecordTo> 	collectionRecordToDataModel;
	
	/** The service rate to data model. */
	private GenericDataModel<ServiceRateTo> 		serviceRateToDataModel;
	
	/** The list service rate to. */
	private List<ServiceRateTo> 					listServiceRateTo;
	
	/** The rate service to data model. */
	private RateServiceToDataModel 					rateServiceToDataModel;
	
	/** The holder selected. */
	private Holder 									holderSelected;
	
	/** The issuer. */
	private Issuer 									issuer;

/** The list client participant. */
//	private String codeIssuer;
	private List<Participant> 						listClientParticipant;
	
	/** The list client participant were change the status to start charging for rates 5 and 6*/
	private List<Participant> 						listClientParticipantRates;//TODO
	
	public List<Participant> getListClientParticipantRates() {
		return listClientParticipantRates;
	}

	public void setListClientParticipantRates(
			List<Participant> listClientParticipantRates) {
		this.listClientParticipantRates = listClientParticipantRates;
	}

	/** The list client participant afp. */
	private List<Participant> 						listClientParticipantAFP;
	/** The lst participant document type. */
    private List<ParameterTable> 					lstDocumentType;
	
	/** The participant selected. */
	private Long 									participantSelected;
	
	/** The participant afp selected. */
	private Long 									participantAFPSelected;
	
	/** The validity initial date. */
	private Date 									validityInitialDate;
	
	/** The validity final date. */
	private Date 									validityFinalDate;
	
	/** The initial date. */
	private Date 									initialDate=CommonsUtilities.currentDate();
	
	/** The final date. */
	private Date 									finalDate=CommonsUtilities.currentDate();
	
	/** The validity final date disable. */
	private Boolean 								validityFinalDateDisable=Boolean.FALSE;
	
	/** The validity initial date disable. */
	private Boolean 								validityInitialDateDisable=Boolean.FALSE;
	
	
	/** The disable holder ec visible. */
	private Boolean 								disableHolderECVisible;
	
	/** The disable issuer ec visible. */
	private Boolean 								disableIssuerECVisible;
	
	/** The disable participant ec visible. */
	private Boolean 								disableParticipantECVisible;
	
	/** The disable participant afpec visible. */
	private Boolean 								disableParticipantAFPECVisible;
	
	/** The disable other ec visible. */
	private Boolean 								disableOtherECVisible;
	
	/** The add detail. */
	private Boolean 								addDetail = Boolean.FALSE;
	
	/** The add more detail. */
	private Boolean 								addMoreDetail = Boolean.FALSE;
	
	/** The add detail collection. */
	private Boolean 								addDetailCollection = Boolean.FALSE;
	
	/** The model collection record to. */
	private CollectionRecordTo 						modelCollectionRecordTo;
	
	/** The processed service to selection. */
	private ProcessedServiceTo 						processedServiceToSelection;
	
	/** The collection record selection. */
	private CollectionRecordTo 						collectionRecordSelection;
	
	/** The service rate to. */
	private ServiceRateTo 							serviceRateTo;
	
	/** The service rate selection. */
	private Object 									serviceRateSelection;
	
	/** For consultCollectionByServices. */
	private Boolean 								disableCollectionInstitutionSelected;
	
	/** The collection institution selected. */
	private Integer 								collectionInstitutionSelected;
	
	/** The entity collection previous. */
	private Integer 								entityCollectionPrevious;
	
	/** The list collection institution. */
	private List<ParameterTable> 					listCollectionInstitution;
	
	/** The rate type movement fixed. */
	private Integer 								rateTypeMovementFixed;
	
	/** The rate type movement percentage. */
	private Integer 								rateTypeMovementPercentage;
	
	/** The rate type staggered fixed. */
	private Integer 								rateTypeStaggeredFixed;
	
	/** The rate type staggered percentage. */
	private Integer 								rateTypeStaggeredPercentage;
	
	/** The rate type staggered mixed. */
	private Integer 								rateTypeStaggeredMixed;
	
	
	/** The collection amount accumulated. */
	private BigDecimal 								collectionAmountAccumulated;
	
	/** The gross amount accumulated. */
	private BigDecimal 								grossAmountAccumulated;
	
	/** The tax applied accumulated. */
	private BigDecimal 								taxAppliedAccumulated;
	
	/** The lis detail rate movement to. */
	private List<DetailRateMovementTo> 				lisDetailRateMovementTo;

	/**
	 * Instantiates a new consult collection mgmt bean.
	 */
	public ConsultCollectionMgmtBean() {
		
	}
	
	/**
	 * Inits the.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException {

		if (!FacesContext.getCurrentInstance().isPostback()) {
			/**Instancia a BillingService*/
			billingServiceSelection=new BillingService();
			disableHolderECVisible=Boolean.FALSE;
			disableIssuerECVisible=Boolean.FALSE;
			disableParticipantECVisible=Boolean.FALSE;
			disableParticipantAFPECVisible=Boolean.FALSE;
			disableOtherECVisible=Boolean.FALSE; 
			modelCollectionRecordTo=new CollectionRecordTo();
			loadCmbData();
			getParticipantListToCombo();
			getParticipantAFPListToCombo();
			getDocumentTypeCombo();
			getParticipantListChangeStatusRates();
			
			this.holderSelected=new Holder();
			this.participantSelected=null;
			modelCollectionRecordTo=new CollectionRecordTo();
			this.issuer=new Issuer();
			collectionAmountAccumulated=new BigDecimal(0);
			grossAmountAccumulated=new BigDecimal(0);
			taxAppliedAccumulated=new BigDecimal(0);
			
			addMoreDetail = Boolean.FALSE;
			addDetailCollection=Boolean.FALSE;
			loadRateType();
			
			
		}

		
		if( userInfo.getUserAccountSession().isInstitution(InstitutionType.PARTICIPANT ) || userInfo.getUserAccountSession().isInstitution(InstitutionType.AFP )){
			collectionInstitutionSelected=EntityCollectionType.PARTICIPANTS.getCode();
			participantSelected = userInfo.getUserAccountSession().getParticipantCode();			
				disableHolderECVisible=Boolean.FALSE;
				disableIssuerECVisible=Boolean.FALSE;
				disableParticipantECVisible=Boolean.TRUE;
				disableParticipantAFPECVisible=Boolean.FALSE;
				disableOtherECVisible=Boolean.FALSE;
		}
		else if( userInfo.getUserAccountSession().isInstitution(InstitutionType.AFP )){
			collectionInstitutionSelected=EntityCollectionType.AFP.getCode();
			participantAFPSelected = userInfo.getUserAccountSession().getParticipantCode();			
				disableHolderECVisible=Boolean.FALSE;
				disableIssuerECVisible=Boolean.FALSE;
				disableParticipantECVisible=Boolean.FALSE;
				disableParticipantAFPECVisible=Boolean.TRUE;
				disableOtherECVisible=Boolean.FALSE;
		}
		else if (userInfo.getUserAccountSession().isInstitution(InstitutionType.ISSUER) ){
			collectionInstitutionSelected=EntityCollectionType.ISSUERS.getCode();
			
			issuer.setIdIssuerPk(userInfo.getUserAccountSession().getIssuerCode());
			
			disableHolderECVisible=Boolean.FALSE;
			disableIssuerECVisible=Boolean.TRUE;
			disableParticipantECVisible=Boolean.FALSE;
			disableParticipantAFPECVisible=Boolean.FALSE;
			disableOtherECVisible=Boolean.FALSE;   
		}
		
	}
	
	/**
	 * Load rate Type.
	 */
	public void loadRateType(){
		rateTypeMovementFixed=RateType.TIPO_MOVIMIENTO_FIJO.getCode();
		rateTypeMovementPercentage=RateType.TIPO_MOVIMIENTO_PORCENTUAL.getCode();
		rateTypeStaggeredFixed=RateType.ESCALONADO_FIJO.getCode();
		rateTypeStaggeredPercentage=RateType.ESCALONADO_PORCENTUAL.getCode();
		rateTypeStaggeredMixed=RateType.ESCALONADO_MIXTO.getCode();
	}
	
	/**
	 * Update Selected Combo.
	 *
	 * @param event the event
	 */
	public void updateSelectedCombo(ActionEvent event){
		if(Validations.validateIsNotNull(this.billingServiceSelection)){
			this.updateCollectionInstitution();
		}
	}
	
	/**
	 * Acciones con los paneles de Entidad de Cobro.
	 */
	public void updateCollectionInstitution(){
		Integer valueEntityCollection= this.billingServiceSelection.getEntityCollection();
		if(Validations.validateIsNotNullAndNotEmpty(valueEntityCollection)){
			
			if(valueEntityCollection.equals(EntityCollectionType.ISSUERS.getCode())){
				disableHolderECVisible=Boolean.FALSE;
				disableIssuerECVisible=Boolean.TRUE;
				disableParticipantECVisible=Boolean.FALSE;
				disableParticipantAFPECVisible=Boolean.FALSE;
				disableOtherECVisible=Boolean.FALSE;
			}
			else if(valueEntityCollection.equals(EntityCollectionType.PARTICIPANTS.getCode())){
				disableHolderECVisible=Boolean.FALSE;
				disableIssuerECVisible=Boolean.FALSE;
				disableParticipantECVisible=Boolean.TRUE;
				disableParticipantAFPECVisible=Boolean.FALSE;
				disableOtherECVisible=Boolean.FALSE;
			}
			else if(valueEntityCollection.equals(EntityCollectionType.HOLDERS.getCode())){
				disableHolderECVisible=Boolean.TRUE;
				disableIssuerECVisible=Boolean.FALSE;
				disableParticipantECVisible=Boolean.FALSE;
				disableParticipantAFPECVisible=Boolean.FALSE;
				disableOtherECVisible=Boolean.FALSE;
			}
			else if(valueEntityCollection.equals(EntityCollectionType.OTHERS.getCode())){
				disableHolderECVisible=Boolean.FALSE;
				disableIssuerECVisible=Boolean.FALSE;
				disableParticipantECVisible=Boolean.FALSE;
				disableParticipantAFPECVisible=Boolean.FALSE;
				disableOtherECVisible=Boolean.TRUE;
			}
			else if(valueEntityCollection.equals(EntityCollectionType.MANAGER.getCode())
					||valueEntityCollection.equals(EntityCollectionType.STOCK_EXCHANGE.getCode())
					||valueEntityCollection.equals(EntityCollectionType.CENTRAL_BANK.getCode())
					||valueEntityCollection.equals(EntityCollectionType.HACIENDA.getCode())
					||valueEntityCollection.equals(EntityCollectionType.REGULATOR_ENTITY.getCode())){
						disableHolderECVisible=Boolean.FALSE;
						disableIssuerECVisible=Boolean.FALSE;
						disableParticipantECVisible=Boolean.FALSE;
						disableParticipantAFPECVisible=Boolean.FALSE;
						disableOtherECVisible=Boolean.FALSE;
			}
			else if(valueEntityCollection.equals(EntityCollectionType.AFP.getCode())){
				disableHolderECVisible=Boolean.FALSE;
				disableIssuerECVisible=Boolean.FALSE;
				disableParticipantECVisible=Boolean.FALSE;
				disableParticipantAFPECVisible=Boolean.TRUE;
				disableOtherECVisible=Boolean.FALSE;
			}
		}
		else{
			disableHolderECVisible=Boolean.FALSE;
			disableIssuerECVisible=Boolean.FALSE;
			disableParticipantECVisible=Boolean.FALSE;
			disableParticipantAFPECVisible=Boolean.FALSE;
			disableOtherECVisible=Boolean.FALSE;
		}
	}
	
	/**
	 *  Method that open dialog to confirm or cancel when this operation go to register one service.
	 */	
	public void hideDialogs(){
		JSFUtilities.hideGeneralDialogues();
	}
	
	/**
	 * Clean windows search.
	 */
	public void cleanConsultCollectionByEntityCollection(){
		
		processedServiceToDataModel=null;
		this.participantSelected=null;
		this.collectionInstitutionSelected=null;
		modelCollectionRecordTo=new CollectionRecordTo();
		/**for the helpers*/
		this.issuer=new Issuer();
		this.holderSelected=new Holder();
		this.initialDate=CommonsUtilities.currentDate();
		this.finalDate=CommonsUtilities.currentDate();	
		disableHolderECVisible=Boolean.FALSE;
		disableIssuerECVisible=Boolean.FALSE;
		disableParticipantECVisible=Boolean.FALSE;
		disableParticipantAFPECVisible=Boolean.FALSE;
		disableOtherECVisible=Boolean.FALSE;
		JSFUtilities.resetComponent("frmSearchConsultCollection");
		
	}
	
	/**
	 * Clean windows search.
	 */
	public void cleanConsultCollectionByServices(){
		collectionRecordToDataModel=null;
		billingServiceSelection=new BillingService();
		modelCollectionRecordTo=new CollectionRecordTo();
		this.participantSelected=null;
		/**for the helpers*/
		this.issuer=new Issuer();
		this.holderSelected=new Holder();
		this.initialDate=CommonsUtilities.currentDate();
		this.finalDate=CommonsUtilities.currentDate();	
		disableHolderECVisible=Boolean.FALSE;
		disableIssuerECVisible=Boolean.FALSE;
		disableParticipantECVisible=Boolean.FALSE;
		disableParticipantAFPECVisible=Boolean.FALSE;
		disableOtherECVisible=Boolean.FALSE; 
		JSFUtilities.resetComponent("frmSearchConsultCollection");
	}
	
	/**
	 *  Clean windows except Combo Entity Collection.
	 */
	public void cleanExceptComboEntity(){
		processedServiceToDataModel=null;
		
		this.participantSelected=null;
		modelCollectionRecordTo=new CollectionRecordTo();
		/**for the helpers*/
		this.issuer=new Issuer();
		this.holderSelected=new Holder();
		this.initialDate=CommonsUtilities.currentDate();
		this.finalDate=CommonsUtilities.currentDate();	
		disableHolderECVisible=Boolean.FALSE;
		disableIssuerECVisible=Boolean.FALSE;
		disableParticipantECVisible=Boolean.FALSE;
		disableParticipantAFPECVisible=Boolean.FALSE;
		disableOtherECVisible=Boolean.FALSE;
		
	}
	
	/**
	 *  Not run change.
	 */
	public void notChangeClean(){
		this.collectionInstitutionSelected=this.entityCollectionPrevious;
		this.hideDialogs();
		
	}
	
	/**
	 * Search list Processed Service.
	 */
	@LoggerAuditWeb
	public void	searchConsultCollectionByEntityCollection() {
		CollectionRecordTo collectionRecordToSearch=new CollectionRecordTo();	
		BillingServiceTo billingServiceTo=new BillingServiceTo();
		Participant participant=new Participant();
		ProcessedServiceTo processedServiceTo=new ProcessedServiceTo();
		if(Validations.validateIsNotNullAndPositive(collectionInstitutionSelected)){
			billingServiceTo.setEntityCollection(collectionInstitutionSelected);
			processedServiceTo.setBillingServiceTo(billingServiceTo);
			collectionRecordToSearch.setProcessedServiceTo(processedServiceTo);
			
			if(collectionInstitutionSelected.equals(EntityCollectionType.ISSUERS.getCode())){
				if(Validations.validateIsNotNull(issuer)&&
						Validations.validateIsNotNullAndNotEmpty(issuer.getIdIssuerPk())){
					 
						collectionRecordToSearch.setIssuer(issuer);
				
				}
				
			}
			/***
			 * If entity collection is PARTICIPANT OR AFP
			 * */
			else if(collectionInstitutionSelected.equals(EntityCollectionType.PARTICIPANTS.getCode())){
				if(Validations.validateIsNotNullAndPositive(participantSelected)){
					participant.setIdParticipantPk(participantSelected);
					try {
						collectionRecordToSearch.setParticipant(this.collectionServiceMgmtServiceFacade.findParticipantByFiltersServiceBean(participant));
					} catch (ServiceException e) {
						e.printStackTrace();
					}
				}
				else{
					
				}
			}
			else if(collectionInstitutionSelected.equals(EntityCollectionType.AFP.getCode())){
				if(Validations.validateIsNotNullAndPositive(participantAFPSelected)){
					participant.setIdParticipantPk(participantAFPSelected);
					try {
						collectionRecordToSearch.setParticipant(this.collectionServiceMgmtServiceFacade.findParticipantByFiltersServiceBean(participant));
					} catch (ServiceException e) {
						e.printStackTrace();
					}
				}
				else{
					
				}
			}
			else if(collectionInstitutionSelected.equals(EntityCollectionType.HOLDERS.getCode())){
				if(Validations.validateIsNotNull(holderSelected)&&
						Validations.validateIsNotNullAndPositive(holderSelected.getIdHolderPk())){
					
					collectionRecordToSearch.setHolder(holderSelected);
					
					
				}
			}
			else if(collectionInstitutionSelected.equals(EntityCollectionType.OTHERS.getCode())){
				collectionRecordToSearch.setDocumentType(this.modelCollectionRecordTo.getDocumentType());
				collectionRecordToSearch.setOtherEntityDocument(this.modelCollectionRecordTo.getOtherEntityDocument());
			}
		}
		collectionRecordToSearch.setInitialDate(initialDate);
		collectionRecordToSearch.setFinalDate(finalDate);
		
		List <ProcessedServiceTo> listProcessedServiceTo=new ArrayList<ProcessedServiceTo>();
		try {
			listProcessedServiceTo=this.consultCollectionMgmtServiceFacade.getSearchListCollectionRecordToToConsultByEntityCollection(collectionRecordToSearch);
		} catch (ServiceException e) {
			
			e.printStackTrace();
		}
		 
			this.collectionAmountAccumulated=this.getAdditionOfAmountProcessed(listProcessedServiceTo,collectionAmountAccumulated);
			this.grossAmountAccumulated=this.getAdditionOfGrossAmountProcessed(listProcessedServiceTo,grossAmountAccumulated);
			this.taxAppliedAccumulated=this.getAdditionOfTaxAppliedProcessed(listProcessedServiceTo,taxAppliedAccumulated);
			this.processedServiceToDataModel=new GenericDataModel<ProcessedServiceTo>(listProcessedServiceTo); 
		
		
	}
	
	/**
	 * Search list Collection Record.
	 */
	@LoggerAuditWeb
	public void	searchConsultCollectionByServices(){
		
		Integer valueEntityCollection= this.billingServiceSelection.getEntityCollection();
		CollectionRecordTo collectionRecordTo=new CollectionRecordTo();	
		Participant participant=new Participant();
		ProcessedServiceTo processedServiceTo=new ProcessedServiceTo();
		billingServiceTo=new BillingServiceTo();
		if(Validations.validateIsNotNull(billingServiceSelection)){
			if(Validations.validateIsNotNullAndNotEmpty(billingServiceSelection.getServiceCode())){
				billingServiceTo.setServiceCode(billingServiceSelection.getServiceCode());
				billingServiceTo.setEntityCollection(valueEntityCollection);
				processedServiceTo.setBillingServiceTo(billingServiceTo);
				collectionRecordTo.setProcessedServiceTo(processedServiceTo);
			}
		}
		if(Validations.validateIsNotNullAndNotEmpty(valueEntityCollection)){
			if(valueEntityCollection.equals(EntityCollectionType.ISSUERS.getCode())){//emisor
				
				if(Validations.validateIsNotNull(issuer)
						&&Validations.validateIsNotNullAndNotEmpty(issuer.getIdIssuerPk())){ 

					collectionRecordTo.setIssuer(issuer);

				}
				
			}
			else if(valueEntityCollection.equals(EntityCollectionType.PARTICIPANTS.getCode())){//participant
				if(Validations.validateIsNotNullAndPositive(participantSelected)){
					participant.setIdParticipantPk(participantSelected);
					try {
						collectionRecordTo.setParticipant(this.collectionServiceMgmtServiceFacade.findParticipantByFiltersServiceBean(participant));
					} catch (ServiceException e) {
						e.printStackTrace();
					}
				}
			}
			else if(valueEntityCollection.equals(EntityCollectionType.HOLDERS.getCode())){//titular
				if(Validations.validateIsNotNull(holderSelected)&&
						Validations.validateIsNotNullAndPositive(holderSelected.getIdHolderPk())){

					collectionRecordTo.setHolder(holderSelected);
					
				}
			}
			else if(valueEntityCollection.equals(EntityCollectionType.OTHERS.getCode())){//otros
				
				collectionRecordTo.setDocumentType(this.modelCollectionRecordTo.getDocumentType());
				collectionRecordTo.setOtherEntityDocument(this.modelCollectionRecordTo.getOtherEntityDocument());
			}
			
		}
		collectionRecordTo.setInitialDate(initialDate);
		collectionRecordTo.setFinalDate(finalDate);
		List <CollectionRecordTo> listCollectionRecordTo=new ArrayList<CollectionRecordTo>();
		try {
			listCollectionRecordTo=consultCollectionMgmtServiceFacade.getSearchListCollectionRecordToToConsultByServices(collectionRecordTo);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		 
				this.collectionAmountAccumulated=this.getAdditionOfAmount(listCollectionRecordTo,collectionAmountAccumulated);
				this.grossAmountAccumulated=this.getAdditionOfGrossAmount(listCollectionRecordTo,grossAmountAccumulated);
				/**el impuesto debe proveer de la tabla */
				this.taxAppliedAccumulated=this.getAdditionOfTaxApplied(listCollectionRecordTo,taxAppliedAccumulated);
				this.collectionRecordToDataModel=new GenericDataModel<CollectionRecordTo>(listCollectionRecordTo);
			 
		
		 
	}
	
	/**
	 * change the status to start charging for rates 5 and 6
	 */
	@LoggerAuditWeb
	public void	changeStatusChargingRates(){
		//TODO
		if(Validations.validateIsNotNullAndNotEmpty(participantSelected)) {
			helperComponentFacade.updateChangeStatusChargingRates(participantSelected);
			showMessageOnDialog(null,null,PropertiesConstants.ALERT_UPDATE_STATUS_START_CHARGING_RATES,null);
			cleanConsultCollectionByServices();
			JSFUtilities.showSimpleEndTransactionDialog("startCollectingParticipants");
		}
	}
	
	/**
	 * Get Participant List To Combo were change the status to start charging for rates 5 and 6
	 *
	 * @return the participant list to combo
	 * @throws ServiceException the service exception
	 */
	public void getParticipantListChangeStatusRates() throws ServiceException{
		if(Validations.validateListIsNullOrEmpty(listClientParticipantRates)){
			Participant participantTemp = new Participant();
			participantTemp.setState(ParticipantStateType.REGISTERED.getCode());
			participantTemp.setPaymentState(GeneralConstants.ZERO_VALUE_INTEGER);
			listClientParticipantRates = helperComponentFacade.findParticipantChangeStatusRates(participantTemp);
		}
	}
	
	/**
	 * Gets the addition of amount.
	 *
	 * @param listCollectionRecordTo the list collection record to
	 * @param accumulated the accumulated
	 * @return the addition of amount
	 */
	public  BigDecimal getAdditionOfAmount(List<CollectionRecordTo> listCollectionRecordTo,BigDecimal accumulated){
		accumulated=new BigDecimal(0);
		for (CollectionRecordTo collectionRecordTo : listCollectionRecordTo) {
			BigDecimal amountCollection=collectionRecordTo.getCollectionAmount();
			
				accumulated=BillingUtils.addTwoDecimal(accumulated, amountCollection,GeneralConstants.ROUNDING_DIGIT_NUMBER);
		}
		return accumulated;
	}
	
	/**
	 * Gets the addition of gross amount.
	 *
	 * @param listCollectionRecordTo the list collection record to
	 * @param accumulated the accumulated
	 * @return the addition of gross amount
	 */
	public  BigDecimal getAdditionOfGrossAmount(List<CollectionRecordTo> listCollectionRecordTo,BigDecimal accumulated){
		accumulated=new BigDecimal(0);
		for (CollectionRecordTo collectionRecordTo : listCollectionRecordTo) {
			BigDecimal grossAmount=collectionRecordTo.getGrossAmount();
			
				accumulated=BillingUtils.addTwoDecimal(accumulated, grossAmount,GeneralConstants.ROUNDING_DIGIT_NUMBER);
		}
		return accumulated;
	}
	
	/**
	 * Gets the addition of tax applied.
	 *
	 * @param listCollectionRecordTo the list collection record to
	 * @param accumulated the accumulated
	 * @return the addition of tax applied
	 */
	public  BigDecimal getAdditionOfTaxApplied(List<CollectionRecordTo> listCollectionRecordTo,BigDecimal accumulated){
		accumulated=new BigDecimal(0);
		for (CollectionRecordTo collectionRecordTo : listCollectionRecordTo) {
			BigDecimal taxApplied=collectionRecordTo.getTaxApplied();
			
				accumulated=BillingUtils.addTwoDecimal(accumulated, taxApplied, GeneralConstants.ROUNDING_DIGIT_NUMBER);
		}
		return accumulated;
	}
	
	/**
	 * Gets the addition of amount processed.
	 *
	 * @param listProcessedServiceTo the list processed service to
	 * @param accumulated the accumulated
	 * @return the addition of amount processed
	 */
	public  BigDecimal getAdditionOfAmountProcessed(List<ProcessedServiceTo> listProcessedServiceTo,BigDecimal accumulated){
		accumulated=new BigDecimal(0);
		for (ProcessedServiceTo processedServiceTo : listProcessedServiceTo) {
			BigDecimal amountCollection=processedServiceTo.getCollectionAmount();
			
				accumulated=BillingUtils.addTwoDecimal(accumulated, amountCollection,GeneralConstants.ROUNDING_DIGIT_NUMBER);
		}
		return accumulated;
	}
	
	/**
	 * Gets the addition of gross amount processed.
	 *
	 * @param listProcessedServiceTo the list processed service to
	 * @param accumulated the accumulated
	 * @return the addition of gross amount processed
	 */
	public  BigDecimal getAdditionOfGrossAmountProcessed(List<ProcessedServiceTo> listProcessedServiceTo,BigDecimal accumulated){
		accumulated=new BigDecimal(0);
		for (ProcessedServiceTo processedServiceTo : listProcessedServiceTo) {
			BigDecimal grossAmount=processedServiceTo.getGrossAmount();
			
				accumulated=BillingUtils.addTwoDecimal(accumulated, grossAmount,GeneralConstants.ROUNDING_DIGIT_NUMBER);
		}
		return accumulated;
	}
	
	/**
	 * Gets the addition of tax applied processed.
	 *
	 * @param listProcessedServiceTo the list processed service to
	 * @param accumulated the accumulated
	 * @return the addition of tax applied processed
	 */
	public  BigDecimal getAdditionOfTaxAppliedProcessed(List<ProcessedServiceTo> listProcessedServiceTo,BigDecimal accumulated){
		accumulated=new BigDecimal(0);
		for (ProcessedServiceTo processedServiceTo : listProcessedServiceTo) {
			BigDecimal taxApplied=processedServiceTo.getTaxApplied();
			
				accumulated=BillingUtils.addTwoDecimal(accumulated, taxApplied, GeneralConstants.ROUNDING_DIGIT_NUMBER);
		}
		return accumulated;
	}
	
	/**
	 * Load cmb data.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCmbData() throws ServiceException{
		
		log.debug(" cargando listas si no están en sesión :");
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		/**llenar la lista de entidad de cobro*/
		if (Validations.validateListIsNullOrEmpty(listCollectionInstitution)){
			log.debug("lista de base de cobro :");
			parameterTableTO.setMasterTableFk(PropertiesConstants.COLLECTION_ENTITY_PK);
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			listCollectionInstitution= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}
		
	}
	
	/**
	 * Update Component Entity Collection.
	 */
	public void updateComponentEntityCollection(){
		
		// Clean processedServiceToDataModel
		cleanProcessedServiceToDataModel();
		
		
		Integer entityCollection=this.collectionInstitutionSelected;
		if(Validations.validateIsNotNullAndPositive(entityCollection)){
			if(entityCollection.equals(EntityCollectionType.ISSUERS.getCode())){
				disableHolderECVisible=Boolean.FALSE;
				disableIssuerECVisible=Boolean.TRUE;
				disableParticipantECVisible=Boolean.FALSE;
				disableParticipantAFPECVisible=Boolean.FALSE;
				disableOtherECVisible=Boolean.FALSE;
			}
			else if(entityCollection.equals(EntityCollectionType.PARTICIPANTS.getCode())){
				disableHolderECVisible=Boolean.FALSE;
				disableIssuerECVisible=Boolean.FALSE;
				disableParticipantECVisible=Boolean.TRUE;
				disableParticipantAFPECVisible=Boolean.FALSE;
				disableOtherECVisible=Boolean.FALSE;
			}
			else if(entityCollection.equals(EntityCollectionType.AFP.getCode())){
				disableHolderECVisible=Boolean.FALSE;
				disableIssuerECVisible=Boolean.FALSE;
				disableParticipantECVisible=Boolean.FALSE;
				disableParticipantAFPECVisible=Boolean.TRUE;
				disableOtherECVisible=Boolean.FALSE;
			}
			else if(entityCollection.equals(EntityCollectionType.HOLDERS.getCode())){
				disableHolderECVisible=Boolean.TRUE;
				disableIssuerECVisible=Boolean.FALSE;
				disableParticipantECVisible=Boolean.FALSE;
				disableParticipantAFPECVisible=Boolean.FALSE;
				disableOtherECVisible=Boolean.FALSE;
			}
			else if(entityCollection.equals(EntityCollectionType.OTHERS.getCode())){
				disableHolderECVisible=Boolean.FALSE;
				disableIssuerECVisible=Boolean.FALSE;
				disableParticipantECVisible=Boolean.FALSE;
				disableParticipantAFPECVisible=Boolean.FALSE;
				disableOtherECVisible=Boolean.TRUE;
			}
			else if(entityCollection.equals(EntityCollectionType.MANAGER.getCode())
					||entityCollection.equals(EntityCollectionType.STOCK_EXCHANGE.getCode())
					||entityCollection.equals(EntityCollectionType.CENTRAL_BANK.getCode())
					||entityCollection.equals(EntityCollectionType.HACIENDA.getCode())
					||entityCollection.equals(EntityCollectionType.REGULATOR_ENTITY.getCode())
					){
				disableHolderECVisible=Boolean.FALSE;
				disableIssuerECVisible=Boolean.FALSE;
				disableParticipantECVisible=Boolean.FALSE;
				disableParticipantAFPECVisible=Boolean.FALSE;
				disableOtherECVisible=Boolean.FALSE;
			}
		} 
		else{
			disableHolderECVisible=Boolean.FALSE;
			disableIssuerECVisible=Boolean.FALSE;
			disableParticipantECVisible=Boolean.FALSE;
			disableParticipantAFPECVisible=Boolean.FALSE;
			disableOtherECVisible=Boolean.FALSE;
		}
		
	}
	
	/**
	 * Get Participant List To Combo.
	 *
	 * @return the participant list to combo
	 * @throws ServiceException the service exception
	 */
	public void getParticipantListToCombo() throws ServiceException{
		if(Validations.validateListIsNullOrEmpty(listClientParticipant)){
			Participant participantTemp=new Participant();
			participantTemp.setState(ParticipantStateType.REGISTERED.getCode());
			listClientParticipant=helperComponentFacade.findParticipantNativeQueryFacade(participantTemp);
		}
	}
	
	/**
	 * Get Participant AFP List to Combo.
	 *
	 * @return the participant afp list to combo
	 * @throws ServiceException the service exception
	 */
	public void getParticipantAFPListToCombo() throws ServiceException{
		if(Validations.validateListIsNullOrEmpty(listClientParticipantAFP)){
			Participant participantTemp=new Participant();
			participantTemp.setAccountClass(ParticipantClassType.AFP.getCode());
			participantTemp.setState(ParticipantStateType.REGISTERED.getCode());
			listClientParticipantAFP=helperComponentFacade.findParticipantNativeQueryFacade(participantTemp);
		}
	}
	
	/**
	 * Get Issuer Helper.
	 *
	 * @param event the event
	 * @return the issuer helper
	 */
	public void getIssuerHelper(ActionEvent event){
		
		//Clean cleanProcessedServiceToDataModel
		cleanProcessedServiceToDataModel();
		cleanCollectionRecordToDataModel();
		
		if(Validations.validateIsNotNullAndNotEmpty(issuerHelperBean)){
//			this.issuer=issuerHelperBean.getSelected();
		}
	}
	
	/**
	 * Send Way Go to.
	 *
	 * @param viewToGo the view to go
	 * @return the string
	 */
	public String goTo(String viewToGo) {
		
		if  (viewToGo.equals("detailConsutCollectionByServices")){
				viewToGo="detailConsutCollectionByServices"; 
		}
		return viewToGo;
	}
	
	/**
	 * ActionListener: consultCollectionByServices,
	 * Action: detailConsutCollectionByServices.
	 *
	 * @param event the event
	 * @throws ServiceException the service exception
	 */
	public void collectionOnConsultByServices(ActionEvent event) throws ServiceException{		
		
		if (event != null) {
			
			CollectionRecordTo collectionRecordTo=(CollectionRecordTo) event.getComponent().getAttributes().get("blockInformation");
			this.collectionRecordSelection=collectionRecordTo;
			this.addDetail = Boolean.FALSE;
			this.addDetailCollection = Boolean.FALSE;
			serviceRateToDataModel=new GenericDataModel<ServiceRateTo>(collectionRecordSelection.getServiceRateToList());
			
		}
	}
		
	/**
	 * *
	 * ActionListener: consultCollectionByEntityCollection,
	 * Action: detailProcessedConsutlByEntityCollection.
	 *
	 * @param event the event
	 * @throws ServiceException the service exception
	 */
	public void collectionOnConsultByEntityCollection(ActionEvent event) throws ServiceException{
			
		if (event != null) {			
			ProcessedServiceTo processedServiceTo=(ProcessedServiceTo) event.getComponent().getAttributes().get("blockInformation");
			this.processedServiceToSelection=processedServiceTo;
			this.collectionRecordToDataModel=new GenericDataModel<CollectionRecordTo>(processedServiceToSelection.getCollectionRecordsTo());			
		}	
		 		
	}
		
	/**
	 * Link detailProcessedConsutlByEntityCollection, List to Collection.
	 *
	 * @param event the event
	 */
	public void serviceRateOnConsultByEntityCollection(ActionEvent event){
		if (event != null) {
			CollectionRecordTo collectionRecordToSelected=(CollectionRecordTo) event.getComponent().getAttributes().get("blockInformation");
			this.addDetail = Boolean.FALSE;
			this.addDetailCollection = Boolean.FALSE;
			this.serviceRateToDataModel=new GenericDataModel<ServiceRateTo>(collectionRecordToSelected.getServiceRateToList());
		}
		
	}
		

	/**
	 * Disable cmb cmbEntityCollection in the case (PARTICIPANT or ISSUER ) .
	 *
	 * @return entityCollec
	 */
	public Boolean disableParticipantOrIssuer(){
	Boolean entityCollec = Boolean.FALSE;
		
		if(userInfo.getUserAccountSession().isInstitution(InstitutionType.PARTICIPANT) || 
		   userInfo.getUserAccountSession().isInstitution(InstitutionType.AFP)){
			
			entityCollec=Boolean.TRUE;			
			
		}else if(userInfo.getUserAccountSession().isInstitution(InstitutionType.ISSUER)){
			   
				entityCollec=Boolean.TRUE;
				
		}
		
		if(entityCollec){
			
			return entityCollec;
			
		}
		return entityCollec;
	}
		
	/**
	 * View Detail Rate.
	 *
	 * @param serviceRateTo the service rate to
	 */
	public void viewDetailRate(ServiceRateTo serviceRateTo){
		this.serviceRateTo=serviceRateTo;
		JSFUtilities.setValidViewComponentAndChildrens("frmCollectionRecord:cnfwFlagConsult"); 
		addDetail = Boolean.TRUE;
	 
	}
	
	/**
	 * View Detail Collection.
	 *
	 * @param serviceRateTo the service rate to
	 */
	public void viewDetailCollection(ServiceRateTo serviceRateTo){
		this.serviceRateTo=serviceRateTo;
		JSFUtilities.setValidViewComponentAndChildrens("frmCollectionRecord:cnfwFlagDetail"); 
		addDetailCollection = Boolean.TRUE;
	 
	}
	
	/**
	 * View rate.
	 */
	public void viewRate(){
		
	}
	
	/**
	 * Gets the document type combo.
	 *
	 * @return the document type combo
	 */
	public void getDocumentTypeCombo() {
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
					
		//List the document types of Participant
		filterParameterTable.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
		try {
			lstDocumentType = generalParametersFacade.getListParameterTableServiceBean(filterParameterTable);
		} catch (ServiceException e) {
			
			e.printStackTrace();
		}	
		
	}
	
	/**
	 * View calculation detail.
	 *
	 * @param rateMovementTo the rate movement to
	 */
	public void viewCalculationDetail(RateMovementTo rateMovementTo){
		
		
	}
	
	
	
	/**
	 * Gets the billing service selection.
	 *
	 * @return the billingServiceSelection
	 */
	public BillingService getBillingServiceSelection() {
		return billingServiceSelection;
	}
	
	/**
	 * Sets the billing service selection.
	 *
	 * @param billingServiceSelection the billingServiceSelection to set
	 */
	public void setBillingServiceSelection(BillingService billingServiceSelection) {
		this.billingServiceSelection = billingServiceSelection;
	}
	
	/**
	 * Gets the billing service to.
	 *
	 * @return the billingServiceTo
	 */
	public BillingServiceTo getBillingServiceTo() {
		return billingServiceTo;
	}
	
	/**
	 * Sets the billing service to.
	 *
	 * @param billingServiceTo the billingServiceTo to set
	 */
	public void setBillingServiceTo(BillingServiceTo billingServiceTo) {
		this.billingServiceTo = billingServiceTo;
	}
	
	/**
	 * Gets the processed service data model.
	 *
	 * @return the processedServiceDataModel
	 */
	public GenericDataModel<ProcessedService> getProcessedServiceDataModel() {
		return processedServiceDataModel;
	}
	
	/**
	 * Sets the processed service data model.
	 *
	 * @param processedServiceDataModel the processedServiceDataModel to set
	 */
	public void setProcessedServiceDataModel(
			GenericDataModel<ProcessedService> processedServiceDataModel) {
		this.processedServiceDataModel = processedServiceDataModel;
	}
	
	/**
	 * Gets the list service rate to.
	 *
	 * @return the listServiceRateTo
	 */
	public List<ServiceRateTo> getListServiceRateTo() {
		return listServiceRateTo;
	}
	
	/**
	 * Sets the list service rate to.
	 *
	 * @param listServiceRateTo the listServiceRateTo to set
	 */
	public void setListServiceRateTo(List<ServiceRateTo> listServiceRateTo) {
		this.listServiceRateTo = listServiceRateTo;
	}
	
	/**
	 * Gets the rate service to data model.
	 *
	 * @return the rateServiceToDataModel
	 */
	public RateServiceToDataModel getRateServiceToDataModel() {
		return rateServiceToDataModel;
	}
	
	/**
	 * Sets the rate service to data model.
	 *
	 * @param rateServiceToDataModel the rateServiceToDataModel to set
	 */
	public void setRateServiceToDataModel(
			RateServiceToDataModel rateServiceToDataModel) {
		this.rateServiceToDataModel = rateServiceToDataModel;
	}
	
	/**
	 * Gets the holder selected.
	 *
	 * @return the holderSelected
	 */
	public Holder getHolderSelected() {
		return holderSelected;
	}
	
	/**
	 * Sets the holder selected.
	 *
	 * @param holderSelected the holderSelected to set
	 */
	public void setHolderSelected(Holder holderSelected) {
		this.holderSelected = holderSelected;
	}
	
	/**
	 * Gets the list client participant.
	 *
	 * @return the listClientParticipant
	 */
	public List<Participant> getListClientParticipant() {
		return listClientParticipant;
	}
	
	/**
	 * Sets the list client participant.
	 *
	 * @param listClientParticipant the listClientParticipant to set
	 */
	public void setListClientParticipant(List<Participant> listClientParticipant) {
		this.listClientParticipant = listClientParticipant;
	}
	
	/**
	 * Gets the list client participant afp.
	 *
	 * @return the listClientParticipantAFP
	 */
	public List<Participant> getListClientParticipantAFP() {
		return listClientParticipantAFP;
	}
	
	/**
	 * Sets the list client participant afp.
	 *
	 * @param listClientParticipantAFP the listClientParticipantAFP to set
	 */
	public void setListClientParticipantAFP(
			List<Participant> listClientParticipantAFP) {
		this.listClientParticipantAFP = listClientParticipantAFP;
	}
	
	/**
	 * Gets the participant selected.
	 *
	 * @return the participantSelected
	 */
	public Long getParticipantSelected() {
		return participantSelected;
	}
	
	/**
	 * Sets the participant selected.
	 *
	 * @param participantSelected the participantSelected to set
	 */
	public void setParticipantSelected(Long participantSelected) {
		this.participantSelected = participantSelected;
	}
	
	/**
	 * Gets the participant afp selected.
	 *
	 * @return the participantAFPSelected
	 */
	public Long getParticipantAFPSelected() {
		return participantAFPSelected;
	}
	
	/**
	 * Sets the participant afp selected.
	 *
	 * @param participantAFPSelected the participantAFPSelected to set
	 */
	public void setParticipantAFPSelected(Long participantAFPSelected) {
		this.participantAFPSelected = participantAFPSelected;
	}
	
	/**
	 * Gets the validity initial date.
	 *
	 * @return the validityInitialDate
	 */
	public Date getValidityInitialDate() {
		return validityInitialDate;
	}
	
	/**
	 * Sets the validity initial date.
	 *
	 * @param validityInitialDate the validityInitialDate to set
	 */
	public void setValidityInitialDate(Date validityInitialDate) {
		this.validityInitialDate = validityInitialDate;
	}
	
	/**
	 * Gets the validity final date.
	 *
	 * @return the validityFinalDate
	 */
	public Date getValidityFinalDate() {
		return validityFinalDate;
	}
	
	/**
	 * Sets the validity final date.
	 *
	 * @param validityFinalDate the validityFinalDate to set
	 */
	public void setValidityFinalDate(Date validityFinalDate) {
		this.validityFinalDate = validityFinalDate;
	}
	
	/**
	 * Gets the initial date.
	 *
	 * @return the initialDate
	 */
	public Date getInitialDate() {
		return initialDate;
	}
	
	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the initialDate to set
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	
	/**
	 * Gets the final date.
	 *
	 * @return the finalDate
	 */
	public Date getFinalDate() {
		return finalDate;
	}
	
	/**
	 * Sets the final date.
	 *
	 * @param finalDate the finalDate to set
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}
	
	/**
	 * Gets the validity final date disable.
	 *
	 * @return the validityFinalDateDisable
	 */
	public Boolean getValidityFinalDateDisable() {
		return validityFinalDateDisable;
	}
	
	/**
	 * Sets the validity final date disable.
	 *
	 * @param validityFinalDateDisable the validityFinalDateDisable to set
	 */
	public void setValidityFinalDateDisable(Boolean validityFinalDateDisable) {
		this.validityFinalDateDisable = validityFinalDateDisable;
	}
	
	/**
	 * Gets the validity initial date disable.
	 *
	 * @return the validityInitialDateDisable
	 */
	public Boolean getValidityInitialDateDisable() {
		return validityInitialDateDisable;
	}
	
	/**
	 * Sets the validity initial date disable.
	 *
	 * @param validityInitialDateDisable the validityInitialDateDisable to set
	 */
	public void setValidityInitialDateDisable(Boolean validityInitialDateDisable) {
		this.validityInitialDateDisable = validityInitialDateDisable;
	}
	
	/**
	 * Gets the disable holder ec visible.
	 *
	 * @return the disableHolderECVisible
	 */
	public Boolean getDisableHolderECVisible() {
		return disableHolderECVisible;
	}
	
	/**
	 * Sets the disable holder ec visible.
	 *
	 * @param disableHolderECVisible the disableHolderECVisible to set
	 */
	public void setDisableHolderECVisible(Boolean disableHolderECVisible) {
		this.disableHolderECVisible = disableHolderECVisible;
	}
	
	/**
	 * Gets the disable issuer ec visible.
	 *
	 * @return the disableIssuerECVisible
	 */
	public Boolean getDisableIssuerECVisible() {
		return disableIssuerECVisible;
	}
	
	/**
	 * Sets the disable issuer ec visible.
	 *
	 * @param disableIssuerECVisible the disableIssuerECVisible to set
	 */
	public void setDisableIssuerECVisible(Boolean disableIssuerECVisible) {
		this.disableIssuerECVisible = disableIssuerECVisible;
	}
	
	/**
	 * Gets the disable participant ec visible.
	 *
	 * @return the disableParticipantECVisible
	 */
	public Boolean getDisableParticipantECVisible() {
		return disableParticipantECVisible;
	}
	
	/**
	 * Sets the disable participant ec visible.
	 *
	 * @param disableParticipantECVisible the disableParticipantECVisible to set
	 */
	public void setDisableParticipantECVisible(Boolean disableParticipantECVisible) {
		this.disableParticipantECVisible = disableParticipantECVisible;
	}
	
	/**
	 * Gets the disable participant afpec visible.
	 *
	 * @return the disableParticipantAFPECVisible
	 */
	public Boolean getDisableParticipantAFPECVisible() {
		return disableParticipantAFPECVisible;
	}
	
	/**
	 * Sets the disable participant afpec visible.
	 *
	 * @param disableParticipantAFPECVisible the disableParticipantAFPECVisible to set
	 */
	public void setDisableParticipantAFPECVisible(
			Boolean disableParticipantAFPECVisible) {
		this.disableParticipantAFPECVisible = disableParticipantAFPECVisible;
	}
	
	/**
	 * Gets the disable other ec visible.
	 *
	 * @return the disableOtherECVisible
	 */
	public Boolean getDisableOtherECVisible() {
		return disableOtherECVisible;
	}
	
	/**
	 * Sets the disable other ec visible.
	 *
	 * @param disableOtherECVisible the disableOtherECVisible to set
	 */
	public void setDisableOtherECVisible(Boolean disableOtherECVisible) {
		this.disableOtherECVisible = disableOtherECVisible;
	}
	
	/**
	 * Gets the model collection record to.
	 *
	 * @return the modelCollectionRecordTo
	 */
	public CollectionRecordTo getModelCollectionRecordTo() {
		return modelCollectionRecordTo;
	}
	
	/**
	 * Sets the model collection record to.
	 *
	 * @param modelCollectionRecordTo the modelCollectionRecordTo to set
	 */
	public void setModelCollectionRecordTo(
			CollectionRecordTo modelCollectionRecordTo) {
		this.modelCollectionRecordTo = modelCollectionRecordTo;
	}
	
	/**
	 * Gets the collection record selection.
	 *
	 * @return the collectionRecordSelection
	 */
	public CollectionRecordTo getCollectionRecordSelection() {
		return collectionRecordSelection;
	}
	
	/**
	 * Sets the collection record selection.
	 *
	 * @param collectionRecordSelection the collectionRecordSelection to set
	 */
	public void setCollectionRecordSelection(
			CollectionRecordTo collectionRecordSelection) {
		this.collectionRecordSelection = collectionRecordSelection;
	}
	
	/**
	 * Gets the service rate to.
	 *
	 * @return the serviceRateTo
	 */
	public ServiceRateTo getServiceRateTo() {
		return serviceRateTo;
	}
	
	/**
	 * Sets the service rate to.
	 *
	 * @param serviceRateTo the serviceRateTo to set
	 */
	public void setServiceRateTo(ServiceRateTo serviceRateTo) {
		this.serviceRateTo = serviceRateTo;
	}
	
	/**
	 * Gets the disable collection institution selected.
	 *
	 * @return the disableCollectionInstitutionSelected
	 */
	public Boolean getDisableCollectionInstitutionSelected() {
		return disableCollectionInstitutionSelected;
	}
	
	/**
	 * Sets the disable collection institution selected.
	 *
	 * @param disableCollectionInstitutionSelected the disableCollectionInstitutionSelected to set
	 */
	public void setDisableCollectionInstitutionSelected(
			Boolean disableCollectionInstitutionSelected) {
		this.disableCollectionInstitutionSelected = disableCollectionInstitutionSelected;
	}
	
	/**
	 * Gets the collection institution selected.
	 *
	 * @return the collectionInstitutionSelected
	 */
	public Integer getCollectionInstitutionSelected() {
		return collectionInstitutionSelected;
	}
	
	/**
	 * Sets the collection institution selected.
	 *
	 * @param collectionInstitutionSelected the collectionInstitutionSelected to set
	 */
	public void setCollectionInstitutionSelected(
			Integer collectionInstitutionSelected) {
		this.collectionInstitutionSelected = collectionInstitutionSelected;
	}
	
	/**
	 * Gets the entity collection previous.
	 *
	 * @return the entityCollectionPrevious
	 */
	public Integer getEntityCollectionPrevious() {
		return entityCollectionPrevious;
	}
	
	/**
	 * Sets the entity collection previous.
	 *
	 * @param entityCollectionPrevious the entityCollectionPrevious to set
	 */
	public void setEntityCollectionPrevious(Integer entityCollectionPrevious) {
		this.entityCollectionPrevious = entityCollectionPrevious;
	}
	
	/**
	 * Gets the list collection institution.
	 *
	 * @return the listCollectionInstitution
	 */
	public List<ParameterTable> getListCollectionInstitution() {
		return listCollectionInstitution;
	}
	
	/**
	 * Sets the list collection institution.
	 *
	 * @param listCollectionInstitution the listCollectionInstitution to set
	 */
	public void setListCollectionInstitution(
			List<ParameterTable> listCollectionInstitution) {
		this.listCollectionInstitution = listCollectionInstitution;
	}
	
	/**
	 * Gets the rate type movement fixed.
	 *
	 * @return the rateTypeMovementFixed
	 */
	public Integer getRateTypeMovementFixed() {
		return rateTypeMovementFixed;
	}
	
	/**
	 * Sets the rate type movement fixed.
	 *
	 * @param rateTypeMovementFixed the rateTypeMovementFixed to set
	 */
	public void setRateTypeMovementFixed(Integer rateTypeMovementFixed) {
		this.rateTypeMovementFixed = rateTypeMovementFixed;
	}
	
	/**
	 * Gets the rate type movement percentage.
	 *
	 * @return the rateTypeMovementPercentage
	 */
	public Integer getRateTypeMovementPercentage() {
		return rateTypeMovementPercentage;
	}
	
	/**
	 * Sets the rate type movement percentage.
	 *
	 * @param rateTypeMovementPercentage the rateTypeMovementPercentage to set
	 */
	public void setRateTypeMovementPercentage(Integer rateTypeMovementPercentage) {
		this.rateTypeMovementPercentage = rateTypeMovementPercentage;
	}
	
	
	
	/**
	 * Gets the rate type staggered fixed.
	 *
	 * @return the rateTypeStaggeredFixed
	 */
	public Integer getRateTypeStaggeredFixed() {
		return rateTypeStaggeredFixed;
	}
	
	/**
	 * Sets the rate type staggered fixed.
	 *
	 * @param rateTypeStaggeredFixed the rateTypeStaggeredFixed to set
	 */
	public void setRateTypeStaggeredFixed(Integer rateTypeStaggeredFixed) {
		this.rateTypeStaggeredFixed = rateTypeStaggeredFixed;
	}
	
	/**
	 * Gets the rate type staggered percentage.
	 *
	 * @return the rateTypeStaggeredPercentage
	 */
	public Integer getRateTypeStaggeredPercentage() {
		return rateTypeStaggeredPercentage;
	}
	
	/**
	 * Sets the rate type staggered percentage.
	 *
	 * @param rateTypeStaggeredPercentage the rateTypeStaggeredPercentage to set
	 */
	public void setRateTypeStaggeredPercentage(Integer rateTypeStaggeredPercentage) {
		this.rateTypeStaggeredPercentage = rateTypeStaggeredPercentage;
	}
	
	/**
	 * Gets the rate type staggered mixed.
	 *
	 * @return the rateTypeStaggeredMixed
	 */
	public Integer getRateTypeStaggeredMixed() {
		return rateTypeStaggeredMixed;
	}
	
	/**
	 * Sets the rate type staggered mixed.
	 *
	 * @param rateTypeStaggeredMixed the rateTypeStaggeredMixed to set
	 */
	public void setRateTypeStaggeredMixed(Integer rateTypeStaggeredMixed) {
		this.rateTypeStaggeredMixed = rateTypeStaggeredMixed;
	}
	
	/**
	 * Gets the processed service to data model.
	 *
	 * @return the processedServiceToDataModel
	 */
	public GenericDataModel<ProcessedServiceTo> getProcessedServiceToDataModel() {
		return processedServiceToDataModel;
	}
	
	/**
	 * Sets the processed service to data model.
	 *
	 * @param processedServiceToDataModel the processedServiceToDataModel to set
	 */
	public void setProcessedServiceToDataModel(
			GenericDataModel<ProcessedServiceTo> processedServiceToDataModel) {
		this.processedServiceToDataModel = processedServiceToDataModel;
	}
	
	/**
	 * Gets the collection record to data model.
	 *
	 * @return the collectionRecordToDataModel
	 */
	public GenericDataModel<CollectionRecordTo> getCollectionRecordToDataModel() {
		return collectionRecordToDataModel;
	}
	
	/**
	 * Sets the collection record to data model.
	 *
	 * @param collectionRecordToDataModel the collectionRecordToDataModel to set
	 */
	public void setCollectionRecordToDataModel(
			GenericDataModel<CollectionRecordTo> collectionRecordToDataModel) {
		this.collectionRecordToDataModel = collectionRecordToDataModel;
	}
	
	/**
	 * Gets the service rate to data model.
	 *
	 * @return the serviceRateToDataModel
	 */
	public GenericDataModel<ServiceRateTo> getServiceRateToDataModel() {
		return serviceRateToDataModel;
	}
	
	/**
	 * Sets the service rate to data model.
	 *
	 * @param serviceRateToDataModel the serviceRateToDataModel to set
	 */
	public void setServiceRateToDataModel(
			GenericDataModel<ServiceRateTo> serviceRateToDataModel) {
		this.serviceRateToDataModel = serviceRateToDataModel;
	}
	
	/**
	 * Gets the lst document type.
	 *
	 * @return the lstDocumentType
	 */
	public List<ParameterTable> getLstDocumentType() {
		return lstDocumentType;
	}
	
	/**
	 * Sets the lst document type.
	 *
	 * @param lstDocumentType the lstDocumentType to set
	 */
	public void setLstDocumentType(List<ParameterTable> lstDocumentType) {
		this.lstDocumentType = lstDocumentType;
	}
	
	/**
	 * Gets the adds the detail.
	 *
	 * @return the addDetail
	 */
	public Boolean getAddDetail() {
		return addDetail;
	}
	
	/**
	 * Sets the adds the detail.
	 *
	 * @param addDetail the addDetail to set
	 */
	public void setAddDetail(Boolean addDetail) {
		this.addDetail = addDetail;
	}
	
	/**
	 * Gets the processed service to selection.
	 *
	 * @return the processedServiceToSelection
	 */
	public ProcessedServiceTo getProcessedServiceToSelection() {
		return processedServiceToSelection;
	}
	
	/**
	 * Sets the processed service to selection.
	 *
	 * @param processedServiceToSelection the processedServiceToSelection to set
	 */
	public void setProcessedServiceToSelection(
			ProcessedServiceTo processedServiceToSelection) {
		this.processedServiceToSelection = processedServiceToSelection;
	}
	
	/**
	 * Gets the service rate selection.
	 *
	 * @return the serviceRateSelection
	 */
	public Object getServiceRateSelection() {
		return serviceRateSelection;
	}
	
	/**
	 * Sets the service rate selection.
	 *
	 * @param serviceRateSelection the serviceRateSelection to set
	 */
	public void setServiceRateSelection(Object serviceRateSelection) {
		this.serviceRateSelection = serviceRateSelection;
	}
	
	/**
	 * Gets the collection amount accumulated.
	 *
	 * @return the collectionAmountAccumulated
	 */
	public BigDecimal getCollectionAmountAccumulated() {
		return collectionAmountAccumulated;
	}
	
	/**
	 * Sets the collection amount accumulated.
	 *
	 * @param collectionAmountAccumulated the collectionAmountAccumulated to set
	 */
	public void setCollectionAmountAccumulated(
			BigDecimal collectionAmountAccumulated) {
		this.collectionAmountAccumulated = collectionAmountAccumulated;
	}
	
	/**
	 * Gets the gross amount accumulated.
	 *
	 * @return the grossAmountAccumulated
	 */
	public BigDecimal getGrossAmountAccumulated() {
		return grossAmountAccumulated;
	}
	
	/**
	 * Sets the gross amount accumulated.
	 *
	 * @param grossAmountAccumulated the grossAmountAccumulated to set
	 */
	public void setGrossAmountAccumulated(BigDecimal grossAmountAccumulated) {
		this.grossAmountAccumulated = grossAmountAccumulated;
	}
	
	/**
	 * Gets the tax applied accumulated.
	 *
	 * @return the taxAppliedAccumulated
	 */
	public BigDecimal getTaxAppliedAccumulated() {
		return taxAppliedAccumulated;
	}
	
	/**
	 * Sets the tax applied accumulated.
	 *
	 * @param taxAppliedAccumulated the taxAppliedAccumulated to set
	 */
	public void setTaxAppliedAccumulated(BigDecimal taxAppliedAccumulated) {
		this.taxAppliedAccumulated = taxAppliedAccumulated;
	}
	
	/**
	 * Gets the adds the more detail.
	 *
	 * @return the adds the more detail
	 */
	public Boolean getAddMoreDetail() {
		return addMoreDetail;
	}
	
	/**
	 * Sets the adds the more detail.
	 *
	 * @param addMoreDetail the new adds the more detail
	 */
	public void setAddMoreDetail(Boolean addMoreDetail) {
		this.addMoreDetail = addMoreDetail;
	}
	
	/**
	 * Gets the adds the detail collection.
	 *
	 * @return the addDetailCollection
	 */
	public Boolean getAddDetailCollection() {
		return addDetailCollection;
	}
	
	/**
	 * Sets the adds the detail collection.
	 *
	 * @param addDetailCollection the addDetailCollection to set
	 */
	public void setAddDetailCollection(Boolean addDetailCollection) {
		this.addDetailCollection = addDetailCollection;
	}
	
	/**
	 * View Calculation Detail.
	 *
	 * @param event the event
	 */
	public void viewCalculationDetail(ActionEvent event){

		// Clean
		cleanDetail();
		
		// Load
		int indicator = Integer.parseInt(event.getComponent().getAttributes().get("rowMovement").toString());
		lisDetailRateMovementTo = this.serviceRateTo.getListRateMovementTo().get(indicator).getListDetailRateMovementTo();

		if(Validations.validateListIsNotNullAndNotEmpty(lisDetailRateMovementTo)){
			addMoreDetail = Boolean.TRUE;
		}	
			
	}
	
	/**
	 * Get lisDetailRateMovementTo.
	 *
	 * @return the lis detail rate movement to
	 */
	public List<DetailRateMovementTo> getLisDetailRateMovementTo() {
		return lisDetailRateMovementTo;
	}
	
	/**
	 * List Detail Rate Movement To.
	 *
	 * @param lisDetailRateMovementTo the new lis detail rate movement to
	 */
	public void setLisDetailRateMovementTo(
			List<DetailRateMovementTo> lisDetailRateMovementTo) {
		this.lisDetailRateMovementTo = lisDetailRateMovementTo;
	}
	
	
	/**
	 * Clean Detail.
	 */
	public void cleanDetail(){
		
		
		if(Validations.validateListIsNotNullAndNotEmpty(lisDetailRateMovementTo)){
		 
		    addMoreDetail = Boolean.FALSE;
		} 
	}
	
	/**
	 * Clean Processed Service To Data Model.
	 */
	public void cleanProcessedServiceToDataModel(){
			
		if(Validations.validateIsNotNullAndNotEmpty(processedServiceToDataModel) && processedServiceToDataModel.getRowCount() > 0) {
		
			processedServiceToDataModel = null ;	
			
			if(Validations.validateIsNotNullAndNotEmpty(participantSelected))
				participantSelected = null ;
		}		
	}
	 
	/**
	 * Clean Collection Record To Data Model.
	 */
	public void cleanCollectionRecordToDataModel(){
		
		if(Validations.validateIsNotNullAndNotEmpty(collectionRecordToDataModel) && collectionRecordToDataModel.getRowCount() > 0) {
		
			collectionRecordToDataModel = null ;	
			
			if(Validations.validateIsNotNullAndNotEmpty(participantSelected))
				participantSelected = null ;
		}		
	}
	
	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}
	
	/**
	 * Sets the issuer.
	 *
	 * @param issuer the issuer to set
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	
	
}