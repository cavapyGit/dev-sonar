package com.pradera.billing.billingcollection.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CollectionMoreDetailTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class CollectionMoreDetailTo implements Serializable{

	/** Detalle de las tarifas. */
	private static final long serialVersionUID = 1L;

	/** The id collection record detail pk. */
	private Long			idCollectionRecordDetailPk;
	
	/** The currency rate. */
	private Integer			currencyRate;
	
	/** The description currency rate. */
	private String			descriptionCurrencyRate;
	
	/** The amount. */
	private BigDecimal 		amount;
	
	/** The percentage. */
	private BigDecimal 		percentage;
	
	/** The id security code fk. */
	private String 		 	idSecurityCodeFk;
	
	/** The id movement type pk. */
	private Long 		 	idMovementTypePk;
	
	/** The id holder pk. */
	private Long 			idHolderPk;
	
	/** The security class. */
	private Integer			securityClass;
	
	/** The currency origin. */
	private Integer 		currencyOrigin;
	
	/** The description security class. */
	private String 			descriptionSecurityClass;
	
	/** The description currency origin. */
	private String 			descriptionCurrencyOrigin;
	
	/** The description holder. */
	private String 			descriptionHolder;
	
	/** The description movement type. */
	private String 			descriptionMovementType;
	
	/** The calculation date. */
	private Date 			calculationDate;
	
	/**
	 * Instantiates a new collection more detail to.
	 */
	public CollectionMoreDetailTo() {
	}
	

	/**
	 * Gets the currency rate.
	 *
	 * @return the currencyRate
	 */
	public Integer getCurrencyRate() {
		return currencyRate;
	}
	
	/**
	 * Sets the currency rate.
	 *
	 * @param currencyRate the currencyRate to set
	 */
	public void setCurrencyRate(Integer currencyRate) {
		this.currencyRate = currencyRate;
	}
	
	/**
	 * Gets the description currency rate.
	 *
	 * @return the descriptionCurrencyRate
	 */
	public String getDescriptionCurrencyRate() {
		return descriptionCurrencyRate;
	}
	
	/**
	 * Sets the description currency rate.
	 *
	 * @param descriptionCurrencyRate the descriptionCurrencyRate to set
	 */
	public void setDescriptionCurrencyRate(String descriptionCurrencyRate) {
		this.descriptionCurrencyRate = descriptionCurrencyRate;
	}
	
	/**
	 * Gets the amount.
	 *
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	
	/**
	 * Sets the amount.
	 *
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	/**
	 * Gets the id security code fk.
	 *
	 * @return the idSecurityCodeFk
	 */
	public String getIdSecurityCodeFk() {
		return idSecurityCodeFk;
	}
	
	/**
	 * Sets the id security code fk.
	 *
	 * @param idSecurityCodeFk the idSecurityCodeFk to set
	 */
	public void setIdSecurityCodeFk(String idSecurityCodeFk) {
		this.idSecurityCodeFk = idSecurityCodeFk;
	}
	
	/**
	 * Gets the id movement type pk.
	 *
	 * @return the idMovementTypePk
	 */
	public Long getIdMovementTypePk() {
		return idMovementTypePk;
	}
	
	/**
	 * Sets the id movement type pk.
	 *
	 * @param idMovementTypePk the idMovementTypePk to set
	 */
	public void setIdMovementTypePk(Long idMovementTypePk) {
		this.idMovementTypePk = idMovementTypePk;
	}
	
	/**
	 * Gets the id holder pk.
	 *
	 * @return the idHolderPk
	 */
	public Long getIdHolderPk() {
		return idHolderPk;
	}
	
	/**
	 * Sets the id holder pk.
	 *
	 * @param idHolderPk the idHolderPk to set
	 */
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}
	
	/**
	 * Gets the security class.
	 *
	 * @return the securityClass
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}
	
	/**
	 * Sets the security class.
	 *
	 * @param securityClass the securityClass to set
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}
	
	/**
	 * Gets the currency origin.
	 *
	 * @return the currencyOrigin
	 */
	public Integer getCurrencyOrigin() {
		return currencyOrigin;
	}
	
	/**
	 * Sets the currency origin.
	 *
	 * @param currencyOrigin the currencyOrigin to set
	 */
	public void setCurrencyOrigin(Integer currencyOrigin) {
		this.currencyOrigin = currencyOrigin;
	}
	
	/**
	 * Gets the description security class.
	 *
	 * @return the descriptionSecurityClass
	 */
	public String getDescriptionSecurityClass() {
		return descriptionSecurityClass;
	}
	
	/**
	 * Sets the description security class.
	 *
	 * @param descriptionSecurityClass the descriptionSecurityClass to set
	 */
	public void setDescriptionSecurityClass(String descriptionSecurityClass) {
		this.descriptionSecurityClass = descriptionSecurityClass;
	}
	
	/**
	 * Gets the description currency origin.
	 *
	 * @return the descriptionCurrencyOrigin
	 */
	public String getDescriptionCurrencyOrigin() {
		return descriptionCurrencyOrigin;
	}
	
	/**
	 * Sets the description currency origin.
	 *
	 * @param descriptionCurrencyOrigin the descriptionCurrencyOrigin to set
	 */
	public void setDescriptionCurrencyOrigin(String descriptionCurrencyOrigin) {
		this.descriptionCurrencyOrigin = descriptionCurrencyOrigin;
	}
	
	/**
	 * Gets the percentage.
	 *
	 * @return the percentage
	 */
	public BigDecimal getPercentage() {
		return percentage;
	}
	
	/**
	 * Sets the percentage.
	 *
	 * @param percentage the percentage to set
	 */
	public void setPercentage(BigDecimal percentage) {
		this.percentage = percentage;
	}
	
	/**
	 * Gets the calculation date.
	 *
	 * @return the calculationDate
	 */
	public Date getCalculationDate() {
		return calculationDate;
	}
	
	/**
	 * Sets the calculation date.
	 *
	 * @param calculationDate the calculationDate to set
	 */
	public void setCalculationDate(Date calculationDate) {
		this.calculationDate = calculationDate;
	}
	
	/**
	 * Gets the id collection record detail pk.
	 *
	 * @return the idCollectionRecordDetailPk
	 */
	public Long getIdCollectionRecordDetailPk() {
		return idCollectionRecordDetailPk;
	}
	
	/**
	 * Sets the id collection record detail pk.
	 *
	 * @param idCollectionRecordDetailPk the idCollectionRecordDetailPk to set
	 */
	public void setIdCollectionRecordDetailPk(Long idCollectionRecordDetailPk) {
		this.idCollectionRecordDetailPk = idCollectionRecordDetailPk;
	}


	/**
	 * Gets the description holder.
	 *
	 * @return the descriptionHolder
	 */
	public String getDescriptionHolder() {
		return descriptionHolder;
	}


	/**
	 * Sets the description holder.
	 *
	 * @param descriptionHolder the descriptionHolder to set
	 */
	public void setDescriptionHolder(String descriptionHolder) {
		this.descriptionHolder = descriptionHolder;
	}


	/**
	 * Gets the description movement type.
	 *
	 * @return the descriptionMovementType
	 */
	public String getDescriptionMovementType() {
		return descriptionMovementType;
	}


	/**
	 * Sets the description movement type.
	 *
	 * @param descriptionMovementType the descriptionMovementType to set
	 */
	public void setDescriptionMovementType(String descriptionMovementType) {
		this.descriptionMovementType = descriptionMovementType;
	}
	
	
}
