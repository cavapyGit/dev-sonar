package com.pradera.billing.billingcollection.service;

import javax.ejb.Local;

import com.pradera.billing.billingcollection.to.ProcessedServiceTo;
import com.pradera.integration.exception.ServiceException;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Project iDepositary. Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Interface SenderBillingService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Local
public interface SenderBillingService {

	/**
	 * Prepare service calculate.
	 *
	 * @param processedServiceTo the processed service to
	 * @throws ServiceException the service exception
	 */
	public void prepareServiceCalculate(ProcessedServiceTo processedServiceTo) throws ServiceException;

	/**
	 * Sender service for calculate.
	 *
	 * @param processedServiceTo the processed service to
	 */
	public void senderServiceForCalculate(ProcessedServiceTo processedServiceTo);

	/**
	 * Sender service for calculate source.
	 *
	 * @param processedServiceTo the processed service to
	 */
	public void senderServiceForCalculateSource(ProcessedServiceTo processedServiceTo);

}
