package com.pradera.billing.billingcollection.service.sender;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.AccessTimeout;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.transaction.TransactionSynchronizationRegistry;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;
import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.billing.billingcollection.facade.CollectionServiceMgmtServiceFacade;
import com.pradera.billing.billingcollection.service.SenderBillingService;
import com.pradera.billing.billingcollection.to.ProcessedServiceTo;
import com.pradera.billing.common.facade.BusinessLayerCommonFacade;
import com.pradera.billing.process.service.CollectionBasicService;
import com.pradera.billing.process.service.CollectionSourceService;
import com.pradera.billing.process.service.CollectionTransactionMovement;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.framework.batchprocess.service.BatchServiceBean;
import com.pradera.core.framework.entity.producer.DepositaryDataBase;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.billing.ProcessedService;
import com.pradera.model.billing.type.BaseCollectionType;
import com.pradera.model.billing.type.ProcessedServiceType;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Project iDepositary. Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SenderServiceCalculation.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
public class SenderServiceCalculation implements SenderBillingService {

	/** The batch service bean. */
	@EJB
	BatchServiceBean batchServiceBean;

	/** The crud dao service bean. */
	@EJB
	CrudDaoServiceBean crudDaoServiceBean;

	/** The collection service mgmt service facade. */
	@EJB
	CollectionServiceMgmtServiceFacade collectionServiceMgmtServiceFacade;

	/** The transaction registry. */
	@Resource
	TransactionSynchronizationRegistry transactionRegistry;

	/** The collection transaction movement. */
	@EJB
	CollectionTransactionMovement collectionTransactionMovement;

	/** The collection basic service executor. */
	@EJB
	CollectionBasicService collectionBasicServiceExecutor;

	/** The collection source service executor. */
	@EJB
	CollectionSourceService collectionSourceServiceExecutor;

	/** The business layer common facade. */
	@EJB
	BusinessLayerCommonFacade businessLayerCommonFacade;

	/** The message configuration. */
	@Inject
	@InjectableResource(location = "Messages_es.properties")
	private Properties messageConfiguration;

	/** The session context. */
	@Resource
	SessionContext sessionContext;

	/** The em. */
	@Inject
	@DepositaryDataBase
	private EntityManager em;

	/** The user info. */
	@Inject
	UserInfo userInfo;

	/** The log. */
	@Inject
	private PraderaLogger log;

	/**
	 * * Prepare Service Calculate, charge Map with parameters for Execution
	 * Calculates.
	 *
	 * @param processedServiceTo the processed service to
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	@Performance
	@Override
	public void prepareServiceCalculate(ProcessedServiceTo processedServiceTo) throws ServiceException {

		Map<String, Object> parameters = new HashMap<String, Object>();

		Long codeProcess = null;
		String date = CommonsUtilities.convertDatetoString(processedServiceTo.getCalculationDate());

		parameters.put(GeneralConstants.FIELD_ID_BILLING_SERVICE_PK,
				processedServiceTo.getBillingServiceTo().getIdBillingServicePk());
		parameters.put(GeneralConstants.FIELD_BASE_COLLECTION,
				processedServiceTo.getBillingServiceTo().getBaseCollection());
		parameters.put(GeneralConstants.FIELD_CALCULATION_PERIOD,
				processedServiceTo.getBillingServiceTo().getCalculationPeriod());
		parameters.put(GeneralConstants.FIELD_SERVICE_CODE, processedServiceTo.getBillingServiceTo().getServiceCode());
		parameters.put(GeneralConstants.FIELD_SERVICE_NAME, processedServiceTo.getBillingServiceTo().getServiceName());

		if (Validations.validateIsNotNull(processedServiceTo.getIdProcessedServicePk())) {
			parameters.put(GeneralConstants.FIELD_ID_PROCESSED_SERVICE_PK,
					processedServiceTo.getIdProcessedServicePk());
		}

		if (processedServiceTo.getBillingServiceTo().getCalculationPeriod()
				.equals(PropertiesConstants.PERIOD_CALCULATION_DAILY_PK)) {

			parameters.put(GeneralConstants.FIELD_CALCULATION_DATE, date);
			codeProcess = BusinessProcessType.DAILY_COLLECT_SERVICE_CALCULATION.getCode();
		} else if (processedServiceTo.getBillingServiceTo().getCalculationPeriod()
				.equals(PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK)) {

			codeProcess = BusinessProcessType.MONTHLY_COLLECT_SERVICE_CALCULATION.getCode();
			parameters.put(GeneralConstants.FIELD_CALCULATION_DATE, date);
			parameters.put(GeneralConstants.FIELD_YEAR, processedServiceTo.getYearProcess());
			parameters.put(GeneralConstants.FIELD_MONTH, processedServiceTo.getMonthProcess());
			parameters.put(GeneralConstants.FIELD_INITIAL_DATE,
					CommonsUtilities.convertDatetoString(processedServiceTo.getInitialDate()));
			parameters.put(GeneralConstants.FIELD_FINAL_DATE,
					CommonsUtilities.convertDatetoString(processedServiceTo.getFinalDate()));
		}

		BusinessProcess process = crudDaoServiceBean.find(BusinessProcess.class, codeProcess);

		/** SAVE ON DB FOR THAT CAN BE GETTING FOR BATCH **/
		batchServiceBean.registerBatchTx(userInfo.getUserAccountSession().getUserName(), process, parameters);
		em.flush();

		processedServiceTo.setProcessedState(ProcessedServiceType.PENDIENTE.getCode());
		processedServiceTo.setDescriptionProcessedState(ProcessedServiceType.PENDIENTE.getValue());
	}

	/**
	 * * By Base Collection find the way If Processing Component Transaction
	 * Movement or Basic.
	 *
	 * @param processedServiceTo the processed service to
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	@Performance
	@Asynchronous
	@AccessTimeout(unit = TimeUnit.MINUTES, value = 60)
	@TransactionTimeout(unit = TimeUnit.MINUTES, value = 60)
	@Override
	public void senderServiceForCalculate(ProcessedServiceTo processedServiceTo) {
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), processedServiceTo.getLoggerUser());
		transactionRegistry.putResource(RegistryContextHolderType.LOGGER_USER, processedServiceTo.getLoggerUser());
		List<ProcessedService> processedServiceListTemp = null;
		ProcessedService processedService = null;
		try {
			/** MOVIMIENTOS, ESCALONADA MOVIMIENTOS */
			if (processedServiceTo.getBillingServiceTo().getBaseCollection()
					.equals(BaseCollectionType.SETTLEMENT_SECURITIES_TRANSACTIONS_FIXED_INCOME.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection()
							.equals(BaseCollectionType.SETTLEMENT_SECURITIES_TRANSACTIONS_VARIABLE_INCOME.getCode())
					//|| processedServiceTo.getBillingServiceTo().getBaseCollection()
							//.equals(BaseCollectionType.INDIRECT_PARTICIPANT.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection()
							.equals(BaseCollectionType.SETTLEMENT_OTC_TRANSACTIONS_VARIABLE_INCOME.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection()
							.equals(BaseCollectionType.REQUEST_CHANGE_OWNERSHIP_FIXED_INCOME.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection()
							.equals(BaseCollectionType.REQUEST_CHANGE_OWNERSHIP_VARIABLE_INCOME.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection()
							.equals(BaseCollectionType.REGISTRATION_PRECAUTIONARY_MEASURES.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection()
							.equals(BaseCollectionType.LIFTING_PRECAUTIONARY_MEASURES.getCode())
//					|| processedServiceTo.getBillingServiceTo().getBaseCollection().equals(
	//						BaseCollectionType.TRANSACTION_CHARGE.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection()
							.equals(BaseCollectionType.REMATERIALIZATION_OR_REVERSAL_ACCOUNT_ENTRIES_VARIABLE_INCOME
									.getCode())) {
				collectionTransactionMovement.startCalculateCollectionTransaction(processedServiceTo);
			}
			/** FIJA, PORCENTUAL, ESCALONADA */
			else if (processedServiceTo.getBillingServiceTo().getBaseCollection()
							.equals(BaseCollectionType.RECORD_BOOK_ENTRY_PHYSICAL_CUSTODY_USD.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection()
							.equals(BaseCollectionType.RECORD_BOOK_ENTRY_PHYSICAL_CUSTODY_LOCAL.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection()
							.equals(BaseCollectionType.RECORD_BOOK_ENTRY_SECURITIES_FIXED_INCOME_AFP.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection()
							.equals(BaseCollectionType.RECORD_BOOK_ENTRY_SECURITIES_VARIABLE_INCOME.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection()
							.equals(BaseCollectionType.RECORD_BOOK_ENTRY_SECURITIES_VARIABLE_INCOME_AFP.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection()
							.equals(BaseCollectionType.RECORD_NOT_PLACED_SECURITIES_FIXED_INCOME.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection()
							.equals(BaseCollectionType.RECORD_NOT_PLACED_SECURITIES_VARIABLE_INCOME.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection()
							.equals(BaseCollectionType.CUSTODY_SECURITIES_PHYSICAL.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection()
							.equals(BaseCollectionType.CUSTODY_SECURITIES_PHYSICAL_AFP.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection()
							.equals(BaseCollectionType.DIRECT_PARTICIPANT.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection()
							.equals(BaseCollectionType.INDIRECT_PARTICIPANT.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection()
							.equals(BaseCollectionType.ACCOUNT_MAINTENANCE_ISSUER.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection()
							.equals(BaseCollectionType.TRANSFERS_SECURITIES_SIRTEX_FIXED_INCOME.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection()
							.equals(BaseCollectionType.TRANSFERS_SECURITIES_SIRTEX_VARIABLE_INCOME.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection()
							.equals(BaseCollectionType.RECORD_SECURITIES_DEMATERIALIZED_FIXED_INCOME.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection()
							.equals(BaseCollectionType.RECORD_SECURITIES_DEMATERIALIZED_DPF.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection()
							.equals(BaseCollectionType.RECORD_SECURITIES_DEMATERIALIZED_VARIABLE_INCOME.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection()
							.equals(BaseCollectionType.ACCOUNT_MODIFICATION.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection().equals(
							BaseCollectionType.TRANSACTION_CHARGE.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection()
							.equals(BaseCollectionType.CREATION_ISIN_CODE.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection()
							.equals(BaseCollectionType.CERTIFICATE_ISIN_CODE.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection()
							.equals(BaseCollectionType.ISSUANCE_CAT.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection()
							.equals(BaseCollectionType.PAYING_AGENT.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection().equals(
							BaseCollectionType.RECORD_BOOK_ENTRY_DESMATERIALIZED_CUSTODY.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection().equals(
							BaseCollectionType.CUSTODY_SECURITIES_PHYSICAL_PRIVATE_OFFERT_VARIABLE_INCOME.getCode())) {

				collectionBasicServiceExecutor.startCalculateCollectionTransaction(processedServiceTo);

			} 
			// Nuevas tarifas Cavapy
			else if(processedServiceTo.getBillingServiceTo().getBaseCollection().equals(BaseCollectionType.PARTICIPANT_REGISTER_DIRECT_THIRD_AND_SELF.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection().equals(BaseCollectionType.PARTICIPANT_REGISTER_INDIRECT_THIRD_AND_SELF.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection().equals(BaseCollectionType.PARTICIPANT_REGISTER_DIRECT_SELF.getCode())
					|| processedServiceTo.getBillingServiceTo().getBaseCollection().equals(BaseCollectionType.PARTICIPANT_REGISTER_INDIRECT_SELF.getCode())) {
				collectionBasicServiceExecutor.startCalculateCollectionTransaction(processedServiceTo);
			}
			else {
				/**
				 * SERVICIOS CON FUENTE EXTERNA CON NUEVAS BASE DE COBRO QUE ESTAN AGREGADOS POR
				 * SCRIPT
				 **/
				collectionSourceServiceExecutor.startCalculateCollectionTransaction(processedServiceTo);
			}
		} catch (Exception ex) {
			processedService = new ProcessedService();
			try {
				processedServiceListTemp = collectionServiceMgmtServiceFacade
						.findProcessedServiceForRecalculate(processedServiceTo);
				if (Validations.validateListIsNotNullAndNotEmpty(processedServiceListTemp)) {
					processedService = processedServiceListTemp.get(0);
				} else {
					processedService = collectionServiceMgmtServiceFacade
							.saveProcessedServiceAutomatic(processedServiceTo);
				}
			} catch (ServiceException e1) {
				e1.printStackTrace();
			}

			if (ex instanceof ServiceException) {
				ServiceException serviceException = (ServiceException) ex;
				if (serviceException.getErrorService() != null) {
					if (serviceException.getErrorService().equals(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR)
							|| serviceException.getErrorService()
									.equals(ErrorServiceType.PROCESSED_SERVICE_NOT_UNIQUE_IN_PERIOD)) {
						processedService.setProcessedState(ProcessedServiceType.ERROR.getCode());
					} else {
						processedService.setProcessedState(ProcessedServiceType.NO_PROCESADO.getCode());
					}
					// log.error(PropertiesUtilities.(ex.getMessage()));
				} else {
					processedService.setProcessedState(ProcessedServiceType.ERROR.getCode());
				}
				
				try {
					processedService.setOperationDate(CommonsUtilities.currentDate());
					collectionServiceMgmtServiceFacade.updateProcessedServiceByBatch(processedService);
				} catch (ServiceException e) {
					e.printStackTrace();
				}

				log.error(ex.getMessage());
				ex.printStackTrace();
			} else {
				try {
					processedService.setOperationDate(CommonsUtilities.currentDate());
					processedService.setProcessedState(ProcessedServiceType.ERROR.getCode());
					collectionServiceMgmtServiceFacade.updateProcessedServiceByBatch(processedService);
				} catch (ServiceException e) {
					e.printStackTrace();
				}
				ex.printStackTrace();
				log.error(ex.getMessage());
			}
			/**
			 * Send Notification Error
			 */
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.DAILY_COLLECT_SERVICE_CALCULATION.getCode());

			String subject = messageConfiguration.getProperty("header.message.warning");

			String message = messageConfiguration.getProperty("warning.calculation.service.error.processed.billing");
			message = MessageFormat.format(message,
					processedServiceTo.getBillingServiceTo().getDescriptionCalculationPeriod(), // 0
					processedServiceTo.getBillingServiceTo().getServiceName(), // 1
					processedServiceTo.getCalculationDate(), // 2
					ex.getMessage());
			List<UserAccountSession> listUser = this.businessLayerCommonFacade
					.getListUsserByBusinessProcess(BusinessProcessType.DAILY_COLLECT_SERVICE_CALCULATION.getCode());
			businessLayerCommonFacade.sendNotificationManual(businessProcess, listUser, subject, message,
					NotificationType.EMAIL);
		}
	}

	/**
	 * Sender By Base Collection is External.
	 *
	 * @param processedServiceTo Processed Service To
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	@Performance
	@Asynchronous
	@Override
	public void senderServiceForCalculateSource(ProcessedServiceTo processedServiceTo) {
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), processedServiceTo.getLoggerUser());

		List<ProcessedService> processedServiceListTemp = null;
		ProcessedService processedService = new ProcessedService();

		try {
			if (Validations.validateIsNotNullAndPositive(processedServiceTo.getBillingServiceTo().getIdSourcePk())) {
				collectionSourceServiceExecutor.startCalculateCollectionTransaction(processedServiceTo);
			}
		} catch (Exception ex) {

			try {

				processedServiceListTemp = collectionServiceMgmtServiceFacade
						.findProcessedServiceForRecalculate(processedServiceTo);

				if (Validations.validateListIsNotNullAndNotEmpty(processedServiceListTemp)) {

					processedService = processedServiceListTemp.get(0);

				} else {
					processedService = collectionServiceMgmtServiceFacade
							.saveProcessedServiceAutomatic(processedServiceTo);
				}

			} catch (ServiceException e1) {
				e1.printStackTrace();
			}

			if (ex instanceof ServiceException) {

				ServiceException serviceException = (ServiceException) ex;
				if (serviceException.getErrorService() != null) {

					if (serviceException.getErrorService().equals(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR)
							|| serviceException.getErrorService()
									.equals(ErrorServiceType.PROCESSED_SERVICE_NOT_UNIQUE_IN_PERIOD)) {

						processedService.setProcessedState(ProcessedServiceType.ERROR.getCode());
					} else {
						processedService.setProcessedState(ProcessedServiceType.NO_PROCESADO.getCode());
					}

				} else {
					processedService.setProcessedState(ProcessedServiceType.ERROR.getCode());
				}

				try {
					processedService.setOperationDate(CommonsUtilities.currentDate());
					collectionServiceMgmtServiceFacade.updateProcessedServiceByBatch(processedService);
				} catch (ServiceException e) {
					e.printStackTrace();
				}

				log.error(ex.getMessage());
				ex.printStackTrace();

			} else {

				try {
					processedService.setOperationDate(CommonsUtilities.currentDate());
					processedService.setProcessedState(ProcessedServiceType.ERROR.getCode());
					collectionServiceMgmtServiceFacade.updateProcessedServiceByBatch(processedService);

				} catch (ServiceException e) {
					e.printStackTrace();
				}

				ex.printStackTrace();
				log.error(ex.getMessage());

			}

		}
	}

}
