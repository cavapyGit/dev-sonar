package com.pradera.billing.billingcollection.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.pradera.billing.billingrate.to.ServiceRateTo;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.billing.type.EntityCollectionType;
import com.pradera.model.issuancesecuritie.Issuer;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CollectionRecordTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class CollectionRecordTo implements Serializable,Comparable<CollectionRecordTo>{
	
	 
	/** The Constant serialVersionUID. */
	private static final long 								serialVersionUID = 1L;
	
	/** The id collection record pk. */
	private Long 											idCollectionRecordPk;
	
	/** The client type. */
	private Integer 										clientType;
	
	/** The collection amount. */
	private BigDecimal 										collectionAmount;
	
	/** The gross amount. */
	private BigDecimal 										grossAmount;
	
	/** The tax applied. */
	private BigDecimal 										taxApplied;
	
	/** The ind billed. */
	private Integer 										indBilled;
	
	/** The currency type. */
	private Integer 										currencyType;
	
	/** The movement count. */
	private Integer 										movementCount;
	
	/** The other entity description. */
	private String 											otherEntityDescription;
	
	/** The other entity document. */
	private String 											otherEntityDocument;
	
	/** The record state. */
	private Integer 										recordState;
	
	/** The holder account. */
	private HolderAccount 									holderAccount;
	
	/** The issuer. */
	private Issuer 											issuer;
	
	/** The participant. */
	private Participant 									participant;
	
	/** The holder. */
	private Holder 											holder;
	
	/** The entity collection. */
	private Integer 										entityCollection;
	
	/** The processed service to. */
	private ProcessedServiceTo 								processedServiceTo;
	
	/** The collection record detail to list. */
	private List<CollectionRecordDetailTo>  				collectionRecordDetailToList;
	
	/** The description entity collection. */
	private String 											descriptionEntityCollection;
	
	/** The description ind billed. */
	private String 											descriptionIndBilled;
	
	/** The calculation date. */
	private Date 											calculationDate;
	
	/** The operation date. */
	private Date 											operationDate;
	
	/** The initial date. */
	private Date 											initialDate;
	
	/** The final date. */
	private Date 											finalDate;
	
	/** The description status record. */
	private String 											descriptionStatusRecord;
	
	/** The id processed service fk. */
	private Long 											idProcessedServiceFk;
	
	/** The service rate to list. */
	private List<ServiceRateTo> 							serviceRateToList;
	
	/** The description currency type. */
	private String 											descriptionCurrencyType;
	
	/** The collection amount accumulated. */
	private BigDecimal 										collectionAmountAccumulated;
	
	/** The gross amount accumulated. */
	private BigDecimal 										grossAmountAccumulated;
	
	/** The tax applied accumulated. */
	private BigDecimal 										taxAppliedAccumulated;

	/** The collection amount billed. */
	private BigDecimal 										collectionAmountBilled;
	
	/** The gross amount billed. */
	private BigDecimal 										grossAmountBilled;
	
	/** The tax applied billed. */
	private BigDecimal 										taxAppliedBilled;
	
	/** The issuer code. */
	private String 	   										issuerCode;
	
	/** The participant code. */
	private Long 	   										participantCode;
	
	/** The holder code. */
	private Long 	   										holderCode;
	
	/** The user count. */
	private Integer    										userCount;
	
	/** The key entity collection. */
	private String     										keyEntityCollection;
	
	/** The currency billing. */
	private Integer 										currencyBilling;
	
	/** The document type. */
	private Integer 										documentType;
	
	/** The other entity country. */
	private Integer 										otherEntityCountry;
	
	/** The other entity department. */
	private Integer 										otherEntityDepartment;
	
	/** The other entity province. */
	private Integer 										otherEntityProvince;
	
	/** The other entity district. */
	private Integer 										otherEntityDistrict;
	
	/** The other entity address. */
	private String 											otherEntityAddress;
	
	/** The count request. */
	private Integer 										countRequest;
	
	/** The ind minimum amount. */
	private Integer 										indMinimumAmount;
	
	/** The minimum amount. */
	private BigDecimal 										minimumAmount;
	
	/** The currency minimum amount. */
	private Integer 										currencyMinimumAmount;
	
	private Long											idHolderByParticipant;
	
	private String 											mnemonicHolder;
	
	/**
	 * Instantiates a new collection record to.
	 */
	public CollectionRecordTo() {
		collectionRecordDetailToList=new ArrayList<CollectionRecordDetailTo>();
		serviceRateToList=new ArrayList<ServiceRateTo>();
	}
	


	/**
	 * Compare If Exists The Object Entity Collection in To 
	 * Collection Record Detail.
	 *
	 * @param entity the entity
	 * @param typeEntity the type entity
	 * @return the boolean
	 */
	
	/*Verificando si existe los cobros de comisiones basicas*/
	public Boolean existEntityCollection(Object entity , Integer typeEntity){
		System.out.println("ENTIDAD :" +entity.toString());
		System.out.println("SE COBRA A :"+typeEntity.toString());
		Boolean exist=false;
		if(EntityCollectionType.HOLDERS.getCode().equals(typeEntity)){
			Long entityHolder = Long.parseLong(entity.toString());
			exist= holder.getIdHolderPk().equals(entityHolder);
		}else if (EntityCollectionType.ISSUERS.getCode().equals(typeEntity)){
			String entityIssuer =  entity.toString();
			exist= issuer.getIdIssuerPk().equalsIgnoreCase(entityIssuer);
		}else if(EntityCollectionType.PARTICIPANTS.getCode().equals(typeEntity)){
			try {
				Long entityParticipant = Long.parseLong((entity).toString());
				exist= participant.getIdParticipantPk().equals(entityParticipant);
			} catch (NumberFormatException e) {
				//e.printStackTrace();
				System.out.println("EL TIPO DE ENTIDAD POR PARTICIPANTE A PARTICIPANTE NO CORRESPONDE");
				return false;
			}
		}
		return exist;
	}

		
	/**
	 * Adds the entity collection.
	 *
	 * @param entity the entity
	 * @param typeEntity the type entity
	 */
	/***/////////////////////////
	public void addEntityCollection(Object entity ,Integer typeEntity ){
		if(EntityCollectionType.HOLDERS.getCode().equals(typeEntity)){
			
			holder=new Holder();
			holder.setIdHolderPk(Long.parseLong(entity.toString()));
			
		}else if (EntityCollectionType.ISSUERS.getCode().equals(typeEntity)){
			
			issuer= new Issuer();
			issuer.setIdIssuerPk( entity.toString());
			
		}else if(EntityCollectionType.PARTICIPANTS.getCode().equals(typeEntity)){
			participant = new Participant();
			try {
				participant.setIdParticipantPk(Long.parseLong(entity.toString()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		this.entityCollection=typeEntity;
	}
	
	/**
	 * Gets the entity collection.
	 *
	 * @param typeEntity the type entity
	 * @return the entity collection
	 */
	public Object  getEntityCollection(Integer typeEntity){
		Object objEntity=null;
		
		if(EntityCollectionType.HOLDERS.getCode().equals(typeEntity)){
			
			objEntity= holder.getIdHolderPk();
			
		}else if (EntityCollectionType.ISSUERS.getCode().equals(typeEntity )){
			
			objEntity= issuer.getIdIssuerPk();
			
		}else if(EntityCollectionType.PARTICIPANTS.getCode().equals(typeEntity)){
			
			objEntity= participant.getIdParticipantPk();			
		}
		
		return objEntity;
	}



	/**
	 * Gets the id collection record pk.
	 *
	 * @return the idCollectionRecordPk
	 */
	public Long getIdCollectionRecordPk() {
		return idCollectionRecordPk;
	}



	/**
	 * Sets the id collection record pk.
	 *
	 * @param idCollectionRecordPk the idCollectionRecordPk to set
	 */
	public void setIdCollectionRecordPk(Long idCollectionRecordPk) {
		this.idCollectionRecordPk = idCollectionRecordPk;
	}



	/**
	 * Gets the client type.
	 *
	 * @return the clientType
	 */
	public Integer getClientType() {
		return clientType;
	}



	/**
	 * Sets the client type.
	 *
	 * @param clientType the clientType to set
	 */
	public void setClientType(Integer clientType) {
		this.clientType = clientType;
	}



	/**
	 * Gets the collection amount.
	 *
	 * @return the collectionAmount
	 */
	public BigDecimal getCollectionAmount() {
		return collectionAmount;
	}



	/**
	 * Sets the collection amount.
	 *
	 * @param collectionAmount the collectionAmount to set
	 */
	public void setCollectionAmount(BigDecimal collectionAmount) {
		this.collectionAmount = collectionAmount;
	}



	/**
	 * Gets the gross amount.
	 *
	 * @return the grossAmount
	 */
	public BigDecimal getGrossAmount() {
		return grossAmount;
	}



	/**
	 * Sets the gross amount.
	 *
	 * @param grossAmount the grossAmount to set
	 */
	public void setGrossAmount(BigDecimal grossAmount) {
		this.grossAmount = grossAmount;
	}



	/**
	 * Gets the tax applied.
	 *
	 * @return the taxApplied
	 */
	public BigDecimal getTaxApplied() {
		return taxApplied;
	}



	/**
	 * Sets the tax applied.
	 *
	 * @param taxApplied the taxApplied to set
	 */
	public void setTaxApplied(BigDecimal taxApplied) {
		this.taxApplied = taxApplied;
	}



	/**
	 * Gets the ind billed.
	 *
	 * @return the indBilled
	 */
	public Integer getIndBilled() {
		return indBilled;
	}



	/**
	 * Sets the ind billed.
	 *
	 * @param indBilled the indBilled to set
	 */
	public void setIndBilled(Integer indBilled) {
		this.indBilled = indBilled;
	}



	/**
	 * Gets the currency type.
	 *
	 * @return the currencyType
	 */
	public Integer getCurrencyType() {
		return currencyType;
	}



	/**
	 * Sets the currency type.
	 *
	 * @param currencyType the currencyType to set
	 */
	public void setCurrencyType(Integer currencyType) {
		this.currencyType = currencyType;
	}



	/**
	 * Gets the movement count.
	 *
	 * @return the movementCount
	 */
	public Integer getMovementCount() {
		return movementCount;
	}



	/**
	 * Sets the movement count.
	 *
	 * @param movementCount the movementCount to set
	 */
	public void setMovementCount(Integer movementCount) {
		this.movementCount = movementCount;
	}



	/**
	 * Gets the other entity description.
	 *
	 * @return the otherEntityDescription
	 */
	public String getOtherEntityDescription() {
		return otherEntityDescription;
	}



	/**
	 * Sets the other entity description.
	 *
	 * @param otherEntityDescription the otherEntityDescription to set
	 */
	public void setOtherEntityDescription(String otherEntityDescription) {
		this.otherEntityDescription = otherEntityDescription;
	}



	/**
	 * Gets the other entity document.
	 *
	 * @return the otherEntityDocument
	 */
	public String getOtherEntityDocument() {
		return otherEntityDocument;
	}



	/**
	 * Sets the other entity document.
	 *
	 * @param otherEntityDocument the otherEntityDocument to set
	 */
	public void setOtherEntityDocument(String otherEntityDocument) {
		this.otherEntityDocument = otherEntityDocument;
	}



	/**
	 * Gets the record state.
	 *
	 * @return the recordState
	 */
	public Integer getRecordState() {
		return recordState;
	}



	/**
	 * Sets the record state.
	 *
	 * @param recordState the recordState to set
	 */
	public void setRecordState(Integer recordState) {
		this.recordState = recordState;
	}



	/**
	 * Gets the holder account.
	 *
	 * @return the holderAccount
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}



	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the holderAccount to set
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}



	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}



	/**
	 * Sets the issuer.
	 *
	 * @param issuer the issuer to set
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}



	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}



	/**
	 * Sets the participant.
	 *
	 * @param participant the participant to set
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}



	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}



	/**
	 * Sets the holder.
	 *
	 * @param holder the holder to set
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}



	/**
	 * Gets the entity collection.
	 *
	 * @return the entityCollection
	 */
	public Integer getEntityCollection() {
		return entityCollection;
	}



	/**
	 * Sets the entity collection.
	 *
	 * @param entityCollection the entityCollection to set
	 */
	public void setEntityCollection(Integer entityCollection) {
		this.entityCollection = entityCollection;
	}



	/**
	 * Gets the processed service to.
	 *
	 * @return the processedServiceTo
	 */
	public ProcessedServiceTo getProcessedServiceTo() {
		return processedServiceTo;
	}



	/**
	 * Sets the processed service to.
	 *
	 * @param processedServiceTo the processedServiceTo to set
	 */
	public void setProcessedServiceTo(ProcessedServiceTo processedServiceTo) {
		this.processedServiceTo = processedServiceTo;
	}



	/**
	 * Gets the collection record detail to list.
	 *
	 * @return the collectionRecordDetailToList
	 */
	public List<CollectionRecordDetailTo> getCollectionRecordDetailToList() {
		return collectionRecordDetailToList;
	}



	/**
	 * Sets the collection record detail to list.
	 *
	 * @param collectionRecordDetailToList the collectionRecordDetailToList to set
	 */
	public void setCollectionRecordDetailToList(
			List<CollectionRecordDetailTo> collectionRecordDetailToList) {
		this.collectionRecordDetailToList = collectionRecordDetailToList;
	}







	/**
	 * Gets the description entity collection.
	 *
	 * @return the descriptionEntityCollection
	 */
	public String getDescriptionEntityCollection() {
		return descriptionEntityCollection;
	}



	/**
	 * Sets the description entity collection.
	 *
	 * @param descriptionEntityCollection the descriptionEntityCollection to set
	 */
	public void setDescriptionEntityCollection(String descriptionEntityCollection) {
		this.descriptionEntityCollection = descriptionEntityCollection;
	}



	/**
	 * Gets the description ind billed.
	 *
	 * @return the descriptionIndBilled
	 */
	public String getDescriptionIndBilled() {
		return descriptionIndBilled;
	}



	/**
	 * Sets the description ind billed.
	 *
	 * @param descriptionIndBilled the descriptionIndBilled to set
	 */
	public void setDescriptionIndBilled(String descriptionIndBilled) {
		this.descriptionIndBilled = descriptionIndBilled;
	}



	/**
	 * Gets the calculation date.
	 *
	 * @return the calculationDate
	 */
	public Date getCalculationDate() {
		return calculationDate;
	}



	/**
	 * Sets the calculation date.
	 *
	 * @param calculationDate the calculationDate to set
	 */
	public void setCalculationDate(Date calculationDate) {
		this.calculationDate = calculationDate;
	}



	/**
	 * Gets the operation date.
	 *
	 * @return the operationDate
	 */
	public Date getOperationDate() {
		return operationDate;
	}



	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the operationDate to set
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}



	/**
	 * Gets the initial date.
	 *
	 * @return the initialDate
	 */
	public Date getInitialDate() {
		return initialDate;
	}



	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the initialDate to set
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}



	/**
	 * Gets the final date.
	 *
	 * @return the finalDate
	 */
	public Date getFinalDate() {
		return finalDate;
	}



	/**
	 * Sets the final date.
	 *
	 * @param finalDate the finalDate to set
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}



	/**
	 * Gets the description status record.
	 *
	 * @return the descriptionStatusRecord
	 */
	public String getDescriptionStatusRecord() {
		return descriptionStatusRecord;
	}



	/**
	 * Sets the description status record.
	 *
	 * @param descriptionStatusRecord the descriptionStatusRecord to set
	 */
	public void setDescriptionStatusRecord(String descriptionStatusRecord) {
		this.descriptionStatusRecord = descriptionStatusRecord;
	}



	/**
	 * Gets the id processed service fk.
	 *
	 * @return the idProcessedServiceFk
	 */
	public Long getIdProcessedServiceFk() {
		return idProcessedServiceFk;
	}



	/**
	 * Sets the id processed service fk.
	 *
	 * @param idProcessedServiceFk the idProcessedServiceFk to set
	 */
	public void setIdProcessedServiceFk(Long idProcessedServiceFk) {
		this.idProcessedServiceFk = idProcessedServiceFk;
	}



	/**
	 * Gets the service rate to list.
	 *
	 * @return the serviceRateToList
	 */
	public List<ServiceRateTo> getServiceRateToList() {
		return serviceRateToList;
	}



	/**
	 * Sets the service rate to list.
	 *
	 * @param serviceRateToList the serviceRateToList to set
	 */
	public void setServiceRateToList(List<ServiceRateTo> serviceRateToList) {
		this.serviceRateToList = serviceRateToList;
	}



	/**
	 * Gets the description currency type.
	 *
	 * @return the descriptionCurrencyType
	 */
	public String getDescriptionCurrencyType() {
		return descriptionCurrencyType;
	}



	/**
	 * Sets the description currency type.
	 *
	 * @param descriptionCurrencyType the descriptionCurrencyType to set
	 */
	public void setDescriptionCurrencyType(String descriptionCurrencyType) {
		this.descriptionCurrencyType = descriptionCurrencyType;
	}



	/**
	 * Gets the collection amount accumulated.
	 *
	 * @return the collectionAmountAccumulated
	 */
	public BigDecimal getCollectionAmountAccumulated() {
		return collectionAmountAccumulated;
	}



	/**
	 * Sets the collection amount accumulated.
	 *
	 * @param collectionAmountAccumulated the collectionAmountAccumulated to set
	 */
	public void setCollectionAmountAccumulated(
			BigDecimal collectionAmountAccumulated) {
		this.collectionAmountAccumulated = collectionAmountAccumulated;
	}



	/**
	 * Gets the gross amount accumulated.
	 *
	 * @return the grossAmountAccumulated
	 */
	public BigDecimal getGrossAmountAccumulated() {
		return grossAmountAccumulated;
	}



	/**
	 * Sets the gross amount accumulated.
	 *
	 * @param grossAmountAccumulated the grossAmountAccumulated to set
	 */
	public void setGrossAmountAccumulated(BigDecimal grossAmountAccumulated) {
		this.grossAmountAccumulated = grossAmountAccumulated;
	}



	/**
	 * Gets the tax applied accumulated.
	 *
	 * @return the taxAppliedAccumulated
	 */
	public BigDecimal getTaxAppliedAccumulated() {
		return taxAppliedAccumulated;
	}



	/**
	 * Sets the tax applied accumulated.
	 *
	 * @param taxAppliedAccumulated the taxAppliedAccumulated to set
	 */
	public void setTaxAppliedAccumulated(BigDecimal taxAppliedAccumulated) {
		this.taxAppliedAccumulated = taxAppliedAccumulated;
	}



	/**
	 * Gets the collection amount billed.
	 *
	 * @return the collectionAmountBilled
	 */
	public BigDecimal getCollectionAmountBilled() {
		return collectionAmountBilled;
	}



	/**
	 * Sets the collection amount billed.
	 *
	 * @param collectionAmountBilled the collectionAmountBilled to set
	 */
	public void setCollectionAmountBilled(BigDecimal collectionAmountBilled) {
		this.collectionAmountBilled = collectionAmountBilled;
	}



	/**
	 * Gets the gross amount billed.
	 *
	 * @return the grossAmountBilled
	 */
	public BigDecimal getGrossAmountBilled() {
		return grossAmountBilled;
	}



	/**
	 * Sets the gross amount billed.
	 *
	 * @param grossAmountBilled the grossAmountBilled to set
	 */
	public void setGrossAmountBilled(BigDecimal grossAmountBilled) {
		this.grossAmountBilled = grossAmountBilled;
	}



	/**
	 * Gets the tax applied billed.
	 *
	 * @return the taxAppliedBilled
	 */
	public BigDecimal getTaxAppliedBilled() {
		return taxAppliedBilled;
	}



	/**
	 * Sets the tax applied billed.
	 *
	 * @param taxAppliedBilled the taxAppliedBilled to set
	 */
	public void setTaxAppliedBilled(BigDecimal taxAppliedBilled) {
		this.taxAppliedBilled = taxAppliedBilled;
	}



	/**
	 * Gets the issuer code.
	 *
	 * @return the issuerCode
	 */
	public String getIssuerCode() {
		return issuerCode;
	}



	/**
	 * Sets the issuer code.
	 *
	 * @param issuerCode the issuerCode to set
	 */
	public void setIssuerCode(String issuerCode) {
		this.issuerCode = issuerCode;
	}



	/**
	 * Gets the participant code.
	 *
	 * @return the participantCode
	 */
	public Long getParticipantCode() {
		return participantCode;
	}



	/**
	 * Sets the participant code.
	 *
	 * @param participantCode the participantCode to set
	 */
	public void setParticipantCode(Long participantCode) {
		this.participantCode = participantCode;
	}



	/**
	 * Gets the holder code.
	 *
	 * @return the holderCode
	 */
	public Long getHolderCode() {
		return holderCode;
	}



	/**
	 * Sets the holder code.
	 *
	 * @param holderCode the holderCode to set
	 */
	public void setHolderCode(Long holderCode) {
		this.holderCode = holderCode;
	}



	/**
	 * Gets the user count.
	 *
	 * @return the userCount
	 */
	public Integer getUserCount() {
		return userCount;
	}



	/**
	 * Sets the user count.
	 *
	 * @param userCount the userCount to set
	 */
	public void setUserCount(Integer userCount) {
		this.userCount = userCount;
	}



	/**
	 * Gets the key entity collection.
	 *
	 * @return the keyEntityCollection
	 */
	public String getKeyEntityCollection() {
		return keyEntityCollection;
	}



	/**
	 * Sets the key entity collection.
	 *
	 * @param keyEntityCollection the keyEntityCollection to set
	 */
	public void setKeyEntityCollection(String keyEntityCollection) {
		this.keyEntityCollection = keyEntityCollection;
	}



	/**
	 * Gets the document type.
	 *
	 * @return the documentType
	 */
	public Integer getDocumentType() {
		return documentType;
	}



	/**
	 * Sets the document type.
	 *
	 * @param documentType the documentType to set
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}



	/**
	 * Gets the other entity country.
	 *
	 * @return the otherEntityCountry
	 */
	public Integer getOtherEntityCountry() {
		return otherEntityCountry;
	}



	/**
	 * Sets the other entity country.
	 *
	 * @param otherEntityCountry the otherEntityCountry to set
	 */
	public void setOtherEntityCountry(Integer otherEntityCountry) {
		this.otherEntityCountry = otherEntityCountry;
	}



	/**
	 * Gets the other entity department.
	 *
	 * @return the otherEntityDepartment
	 */
	public Integer getOtherEntityDepartment() {
		return otherEntityDepartment;
	}



	/**
	 * Sets the other entity department.
	 *
	 * @param otherEntityDepartment the otherEntityDepartment to set
	 */
	public void setOtherEntityDepartment(Integer otherEntityDepartment) {
		this.otherEntityDepartment = otherEntityDepartment;
	}



	/**
	 * Gets the other entity province.
	 *
	 * @return the otherEntityProvince
	 */
	public Integer getOtherEntityProvince() {
		return otherEntityProvince;
	}



	/**
	 * Sets the other entity province.
	 *
	 * @param otherEntityProvince the otherEntityProvince to set
	 */
	public void setOtherEntityProvince(Integer otherEntityProvince) {
		this.otherEntityProvince = otherEntityProvince;
	}



	/**
	 * Gets the other entity district.
	 *
	 * @return the otherEntityDistrict
	 */
	public Integer getOtherEntityDistrict() {
		return otherEntityDistrict;
	}



	/**
	 * Sets the other entity district.
	 *
	 * @param otherEntityDistrict the otherEntityDistrict to set
	 */
	public void setOtherEntityDistrict(Integer otherEntityDistrict) {
		this.otherEntityDistrict = otherEntityDistrict;
	}



	/**
	 * Gets the other entity address.
	 *
	 * @return the otherEntityAddress
	 */
	public String getOtherEntityAddress() {
		return otherEntityAddress;
	}



	/**
	 * Sets the other entity address.
	 *
	 * @param otherEntityAddress the otherEntityAddress to set
	 */
	public void setOtherEntityAddress(String otherEntityAddress) {
		this.otherEntityAddress = otherEntityAddress;
	}



	/**
	 * Gets the currency billing.
	 *
	 * @return the currencyBilling
	 */
	public Integer getCurrencyBilling() {
		return currencyBilling;
	}



	/**
	 * Sets the currency billing.
	 *
	 * @param currencyBilling the currencyBilling to set
	 */
	public void setCurrencyBilling(Integer currencyBilling) {
		this.currencyBilling = currencyBilling;
	}



	/**
	 * Gets the count request.
	 *
	 * @return the countRequest
	 */
	public Integer getCountRequest() {
		return countRequest;
	}



	/**
	 * Sets the count request.
	 *
	 * @param countRequest the countRequest to set
	 */
	public void setCountRequest(Integer countRequest) {
		this.countRequest = countRequest;
	}



	/**
	 * Gets the ind minimum amount.
	 *
	 * @return the indMinimumAmount
	 */
	public Integer getIndMinimumAmount() {
		return indMinimumAmount;
	}



	/**
	 * Sets the ind minimum amount.
	 *
	 * @param indMinimumAmount the indMinimumAmount to set
	 */
	public void setIndMinimumAmount(Integer indMinimumAmount) {
		this.indMinimumAmount = indMinimumAmount;
	}



	/**
	 * Gets the minimum amount.
	 *
	 * @return the minimumAmount
	 */
	public BigDecimal getMinimumAmount() {
		return minimumAmount;
	}



	/**
	 * Sets the minimum amount.
	 *
	 * @param minimumAmount the minimumAmount to set
	 */
	public void setMinimumAmount(BigDecimal minimumAmount) {
		this.minimumAmount = minimumAmount;
	}



	/**
	 * Gets the currency minimum amount.
	 *
	 * @return the currencyMinimumAmount
	 */
	public Integer getCurrencyMinimumAmount() {
		return currencyMinimumAmount;
	}



	/**
	 * Sets the currency minimum amount.
	 *
	 * @param currencyMinimumAmount the currencyMinimumAmount to set
	 */
	public void setCurrencyMinimumAmount(Integer currencyMinimumAmount) {
		this.currencyMinimumAmount = currencyMinimumAmount;
	}



	/**
	 * @return the idHolderByParticipant
	 */
	public Long getIdHolderByParticipant() {
		return idHolderByParticipant;
	}



	/**
	 * @param idHolderByParticipant the idHolderByParticipant to set
	 */
	public void setIdHolderByParticipant(Long idHolderByParticipant) {
		this.idHolderByParticipant = idHolderByParticipant;
	}



	/**
	 * @return the mnemonicHolder
	 */
	public String getMnemonicHolder() {
		return mnemonicHolder;
	}



	/**
	 * @param mnemonicHolder the mnemonicHolder to set
	 */
	public void setMnemonicHolder(String mnemonicHolder) {
		this.mnemonicHolder = mnemonicHolder;
	}



	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(CollectionRecordTo o) {
		CollectionRecordTo accountingAccount= (CollectionRecordTo)o;
		Integer entityCollectionThis=this.getEntityCollection();
		Integer entityCollectionOther=accountingAccount.getEntityCollection();
		String keyThis=entityCollectionThis.toString().concat(GeneralConstants.HYPHEN).
				concat(this.getEntityCollection(entityCollectionThis).toString());
		String keyOther=entityCollectionThis.toString().concat(GeneralConstants.HYPHEN).
				concat(accountingAccount.getEntityCollection(entityCollectionOther).toString());
		
		
		return keyThis.compareTo(keyOther);
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CollectionRecordTo [");
		if (entityCollection != null) {
			builder.append("entityCollection=");
			builder.append(entityCollection);
			builder.append(", ");
		}
		if (issuer != null) {
			builder.append("issuer=");
			builder.append(issuer.getIdIssuerPk());
			builder.append(", ");
		}
		if (participant != null) {
			builder.append("participant=");
			builder.append(participant.getIdParticipantPk());
			builder.append(", ");
		}
		if (holder != null) {
			builder.append("holder=");
			builder.append(holder.getIdHolderPk());
			builder.append(", ");
		}
		if (issuerCode != null) {
			builder.append("issuerCode=");
			builder.append(issuerCode);
			builder.append(", ");
		}
		if (participantCode != null) {
			builder.append("participantCode=");
			builder.append(participantCode);
			builder.append(", ");
		}
		if (holderCode != null) {
			builder.append("holderCode=");
			builder.append(holderCode);
			builder.append(", ");
		}
		if (collectionAmount != null) {
			builder.append("collectionAmount=");
			builder.append(collectionAmount);
			builder.append(", ");
		}
		if (grossAmount != null) {
			builder.append("grossAmount=");
			builder.append(grossAmount);
			builder.append(", ");
		}
		if (taxApplied != null) {
			builder.append("taxApplied=");
			builder.append(taxApplied);
		}
		
		builder.append("]");
		return builder.toString();
	}
	
}
