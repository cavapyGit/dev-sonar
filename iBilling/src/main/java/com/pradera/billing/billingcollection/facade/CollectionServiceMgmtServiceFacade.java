package com.pradera.billing.billingcollection.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.transaction.TransactionSynchronizationRegistry;

import org.apache.commons.lang3.SerializationUtils;

import com.pradera.billing.billingcollection.service.BaseCollectionMgmtServiceBean;
import com.pradera.billing.billingcollection.service.CollectionServiceMgmtServiceBean;
import com.pradera.billing.billingcollection.to.CollectionRecordDetailTo;
import com.pradera.billing.billingcollection.to.CollectionRecordTo;
import com.pradera.billing.billingcollection.to.DailyExchangeRateFilter;
import com.pradera.billing.billingcollection.to.ProcessedServiceTo;
import com.pradera.billing.billingexception.service.RateExceptionServiceBean;
import com.pradera.billing.billingrate.facade.BillingRateMgmtServiceFacade;
import com.pradera.billing.billingservice.facade.BillingServiceMgmtServiceFacade;
import com.pradera.billing.billingservice.service.BillingServiceMgmtServiceBean;
import com.pradera.billing.billingservice.to.BillingServiceTo;
import com.pradera.billing.common.facade.BusinessLayerCommonFacade;
import com.pradera.billing.common.service.BusinessLayerCommonServiceBean;
import com.pradera.billing.common.utils.BillingUtils;
import com.pradera.billing.process.batch.interceptor.LoggerAuditBatch;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.service.IssuerQueryServiceBean;
import com.pradera.core.component.helperui.to.IssuerSearcherTO;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.core.framework.batchprocess.service.BatchServiceBean;
import com.pradera.core.framework.entity.producer.DepositaryDataBase;
import com.pradera.core.framework.entity.service.LoaderEntityServiceBean;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.CollectionRecord;
import com.pradera.model.billing.CollectionRecordDetail;
import com.pradera.model.billing.ProcessedService;
import com.pradera.model.billing.RateMovement;
import com.pradera.model.billing.ServiceRate;
import com.pradera.model.billing.ServiceRateScale;
import com.pradera.model.billing.type.BaseCollectionType;
import com.pradera.model.billing.type.EntityCollectionType;
import com.pradera.model.billing.type.ProcessedServiceType;
import com.pradera.model.billing.type.RateStateType;
import com.pradera.model.billing.type.RateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.DailyExchangeRoleType;
import com.pradera.model.generalparameter.type.InformationSourceType;
import com.pradera.model.issuancesecuritie.Issuer;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CollectionServiceMgmtServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class CollectionServiceMgmtServiceFacade {

	/** The collection service mgmt service bean. */
	@EJB
	CollectionServiceMgmtServiceBean 	collectionServiceMgmtServiceBean;
	
	/** The billing service mgmt service bean. */
	@EJB
	BillingServiceMgmtServiceBean 		billingServiceMgmtServiceBean;
	
	/** The participant service bean. */
	@EJB
	ParticipantServiceBean 				participantServiceBean;
	
	/** The bussiness layer common service bean. */
	@EJB 
	BusinessLayerCommonServiceBean 		bussinessLayerCommonServiceBean;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade 				helperComponentFacade;
	
	/** The issuer query service. */
	@EJB
	IssuerQueryServiceBean 				issuerQueryService;
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry 	transactionRegistry;
	
	/** The notification service facade. */
	@EJB 
	NotificationServiceFacade 			notificationServiceFacade;
	
	/** The loader entity service bean. */
	@EJB
	LoaderEntityServiceBean 			loaderEntityServiceBean;
	
	/** The batch service bean. */
	@EJB 
	BatchServiceBean 					batchServiceBean;
	
	/** The billing rate mgmt service facade. */
	@EJB
	BillingRateMgmtServiceFacade 		billingRateMgmtServiceFacade;
	
	/** The base collection mgmt service facade. */
	@EJB
	BaseCollectionMgmtServiceFacade 	baseCollectionMgmtServiceFacade;
	
	/** The crud dao service bean. */
	@EJB
	CrudDaoServiceBean 					crudDaoServiceBean;

	/** The client rest service. */
	@Inject
	ClientRestService 					clientRestService;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade 			generalParametersFacade;
	 
	/** The billing service mgmt service facade. */
	@EJB
	BillingServiceMgmtServiceFacade 	billingServiceMgmtServiceFacade ;

	/** The business layer common facade. */
	@EJB
	BusinessLayerCommonFacade 			businessLayerCommonFacade;
	
	/** The rate exception service bean. */
	@EJB
	RateExceptionServiceBean 			rateExceptionServiceBean;
	
	/** The em. */
	@Inject @DepositaryDataBase
	private EntityManager 	em;
	
	/** The user info. */
	@Inject
	UserInfo 				userInfo;
	
	/** The log. */
	@Inject
	PraderaLogger 			log;
	
	@EJB
	 BaseCollectionMgmtServiceBean 		baseCollectionMgmtServiceBean;
	/**
	 * Find billing service to for calculate.
	 *
	 * @param billingServiceTo the billing service to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<BillingServiceTo>  findBillingServiceToForCalculate(BillingServiceTo billingServiceTo)throws ServiceException{
				
		return collectionServiceMgmtServiceBean.findBillingServiceToForServiceCalculate(billingServiceTo);
	}
	
	/**
	 * Find Process Automatic Daily Batch.
	 *
	 * @return the list
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<ProcessedServiceTo> findProcessAutomaticDailyBatch(){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		List<ProcessedServiceTo>  listProcessedServiceTo=null;
		/** ACA SE DEBE LISTAR TODOS LOS DIARIOS   **/
		
		List<ProcessedServiceTo>  listProcessedServiceExecutedTemp = new ArrayList<ProcessedServiceTo>();  	
		ProcessedServiceTo processedServiceTo = new ProcessedServiceTo();

		processedServiceTo.setCalculationDate(CommonsUtilities.currentDateTime());
		BillingServiceTo billingServiceToTemp = new BillingServiceTo();
		billingServiceToTemp.setCalculationPeriod(PropertiesConstants.PERIOD_CALCULATION_DAILY_PK);
		billingServiceToTemp.setServiceType(PropertiesConstants.SERVICE_TYPE_AUTOMATIC_PK); /** DEFAULT AUTOMATIC */
		processedServiceTo.setBillingServiceTo(billingServiceToTemp);
		processedServiceTo.setSourceType(InformationSourceType.BCRD.getCode());
		
		try {
			listProcessedServiceTo = loadProcessedServiceToForCalculate(processedServiceTo,listProcessedServiceExecutedTemp);
		} catch (ServiceException e) {
			
			e.printStackTrace();
		}
		
		if (Validations.validateListIsNotNullAndNotEmpty(listProcessedServiceTo)){
			/*** Load processedService with Error ***/
			List<ProcessedServiceTo> listProcessedServiceToError	=	new ArrayList<ProcessedServiceTo>();				

			List<ProcessedServiceTo> listProcessedServiceTemp	=	BillingUtils.cloneProcessedServiceToList(listProcessedServiceTo);
				
			for (ProcessedServiceTo processedServiceToTemp : listProcessedServiceTemp) {
				if ( Validations.validateIsNotNull(processedServiceToTemp.getErrorType())){
					listProcessedServiceToError.add(processedServiceToTemp);
				}
			}
			
			listProcessedServiceTemp.removeAll(listProcessedServiceToError);
			listProcessedServiceTo=BillingUtils.cloneProcessedServiceToList(listProcessedServiceTemp);

		}
		
		return listProcessedServiceTo;
	}
	
	/**
	 * Find Process Automatic Monthly Batch.
	 *
	 * @return the list
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<ProcessedServiceTo> findProcessAutomaticMonthlyBatch(){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		List<ProcessedServiceTo>  listProcessedServiceTo=null;
		/** ACA SE DEBE LISTAR TODOS LOS MENSUALES   **/
		
		List<ProcessedServiceTo>  listProcessedServiceExecutedTemp = new ArrayList<ProcessedServiceTo>();  	
		ProcessedServiceTo processedServiceTo = new ProcessedServiceTo();
		
		BillingServiceTo billingServiceToTemp = new BillingServiceTo();				
		billingServiceToTemp.setCalculationPeriod(PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK);
		Date finalDate=CommonsUtilities.currentDate();
		Date initialDate=BillingUtils.getFirstDayOfMonth(finalDate);
		String strDate       	=CommonsUtilities.convertDateToString(initialDate,BillingUtils.DATE_PATTERN_yyyy_MM_dd);
		String strYearMonth  	=BillingUtils.getPatternYearMonth(strDate, BillingUtils.DATE_PATTERN_yyyy_MM_dd);
		
		
		processedServiceTo.setYearProcess(BillingUtils.getPatternYear(strYearMonth,BillingUtils.DATE_PATTERN_yyyy_MM));
		processedServiceTo.setMonthProcess(BillingUtils.getPatternMonth(strYearMonth,BillingUtils.DATE_PATTERN_yyyy_MM));

		processedServiceTo.setInitialDate(initialDate);
		processedServiceTo.setFinalDate(finalDate);
		processedServiceTo.setCalculationDate(finalDate);
		
		
		billingServiceToTemp.setServiceType(PropertiesConstants.SERVICE_TYPE_AUTOMATIC_PK); /** DEFAULT AUTOMATIC */
		processedServiceTo.setBillingServiceTo(billingServiceToTemp);
		processedServiceTo.setSourceType(InformationSourceType.BCRD.getCode());
		
		try {
			listProcessedServiceTo = loadProcessedServiceToForCalculate(processedServiceTo,listProcessedServiceExecutedTemp);
		} catch (ServiceException e) {

			e.printStackTrace();
		}
		
		return listProcessedServiceTo;
	}
	
	
	/**
	 * *
	 * Load Processed Service For Calculate.
	 *
	 * @param processedServiceToFilter the processed service to filter
	 * @param listProcessedServiceToSent the list processed service to sent
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<ProcessedServiceTo>  loadProcessedServiceToForCalculate(ProcessedServiceTo processedServiceToFilter,List<ProcessedServiceTo> listProcessedServiceToSent)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		
		List<ProcessedServiceTo> listProcessedServiceTo=null;
		
		 /**GETTING ALL BILLING SERVICES WITH FILTER : BASE COLLECION , PERIOD CALCULATE ,CALCULATE DATE ,YEAR , MONTH , AUTOMATIC  AND   ACTIVE STATUS**/
		 List<BillingServiceTo> listBillingServiceTo = collectionServiceMgmtServiceBean.findBillingServiceToForServiceCalculate(processedServiceToFilter.getBillingServiceTo());
		 	
		 
		 if (Validations.validateListIsNotNullAndNotEmpty(listBillingServiceTo)){
				
				 	 listProcessedServiceTo = new ArrayList<ProcessedServiceTo>();			 	 
					 ProcessedServiceTo 	processedServiceTo = null;
					 
					 
					 /** FIND BILLING SERVICE ON LIST  PROCESSED SERVICE , MAYBE  THERE ARE SERVICE SENT ***/
					 /** BEGIN***/
					 for (BillingServiceTo nexBillingServiceTo : listBillingServiceTo) {
						 
						 try {
							 
							 processedServiceTo	=	loadProcessedServiceToUniqueForCalculate(nexBillingServiceTo, processedServiceToFilter, listProcessedServiceToSent);
							 							 
						} catch (Exception ex) {
							
							
							if (ex instanceof ServiceException){
								
								if(  ((ServiceException) ex).getErrorService()!=null ){
									
									ServiceException serviceException = (ServiceException) ex;
									processedServiceTo	= new ProcessedServiceTo( (serviceException).getErrorService());
									processedServiceTo.setBillingServiceTo(nexBillingServiceTo);									
								}
								else{
									processedServiceTo.setOtherErrorType(Boolean.TRUE);
								}
							}
							else{
								processedServiceTo.setOtherErrorType(Boolean.TRUE);
							}
							processedServiceTo.setCalculationDate(processedServiceToFilter.getCalculationDate());
							processedServiceTo.setYearProcess(processedServiceToFilter.getYearProcess());
							processedServiceTo.setMonthProcess(processedServiceToFilter.getMonthProcess());
							processedServiceTo.setInitialDate(processedServiceToFilter.getInitialDate());
							processedServiceTo.setFinalDate(processedServiceToFilter.getFinalDate());
						 
					 }	
						 
						 listProcessedServiceTo.add(processedServiceTo);
		 }
		}
	 return listProcessedServiceTo;
		 
	}

	/**
	 * Find Processed Service For Calculate.
	 *
	 * @param processedServiceToFilter the processed service to filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<ProcessedService>	findProcessedServiceForCalculate( ProcessedServiceTo processedServiceToFilter) throws ServiceException{
		
		 List<ProcessedService>	listProcessedService =collectionServiceMgmtServiceBean.findProcessedServiceForCalculate(processedServiceToFilter);
		 
		
		 BillingService billingService= null;
		 for (ProcessedService processedService : listProcessedService) {
			 billingService = billingServiceMgmtServiceFacade.getBillingServiceByProcessedServicePk(processedService.getIdProcessedServicePk());
			 processedService.setBillingService(billingService);
		 }
		 
		return listProcessedService;
	}
	
	/**
	 * Load processed service to unique for calculate.
	 *
	 * @param billingServiceTo the billing service to
	 * @param processedServiceToFilter the processed service to filter
	 * @param listProcessedServiceToSent the list processed service to sent
	 * @return the processed service to
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ProcessedServiceTo loadProcessedServiceToUniqueForCalculate(BillingServiceTo billingServiceTo , 
														ProcessedServiceTo processedServiceToFilter,
														List<ProcessedServiceTo> listProcessedServiceToSent) throws ServiceException{
		
		 /**GETTING PROCESSED SERVICE THAT WERE SENT FOR CALCULATION**/
		 List<ProcessedService>	listProcessedService = findProcessedServiceForCalculate(processedServiceToFilter);
		 				 
		 businessLayerCommonFacade.detachList(listProcessedService);
		 
		 /** GETTING DAILY EXCHANGE ***/
		 DailyExchangeRateFilter dailyExchangeRateFilter = new DailyExchangeRateFilter();
		 
		 dailyExchangeRateFilter.setInitialDate((processedServiceToFilter.getCalculationDate()==null?null:CommonsUtilities.truncateDateTime(processedServiceToFilter.getCalculationDate()) ));
		 dailyExchangeRateFilter.setFinalDate((processedServiceToFilter.getCalculationDate()==null?null:CommonsUtilities.truncateDateTime(processedServiceToFilter.getCalculationDate())));
		 dailyExchangeRateFilter.setIdSourceinformation(processedServiceToFilter.getSourceType());
		 
		 
		 Boolean billingServiceSent=Boolean.FALSE;
		 
		 /** it's null when never was sent ***/
		 ProcessedService processedService = null; 
		 	
		 	 Integer countProcessedServiceUnique	= 0;
		 		
			 for (ProcessedService nextProcessedService : listProcessedService) {
				
				 	if (billingServiceTo.getIdBillingServicePk().equals(nextProcessedService.getBillingService().getIdBillingServicePk()) ) {
				 			billingServiceSent=Boolean.TRUE;
				 			processedService=nextProcessedService;
				 			countProcessedServiceUnique++;
				 	}
			 }
		 
			 if ( countProcessedServiceUnique >1){
				 
				 throw new ServiceException(ErrorServiceType.PROCESSED_SERVICE_NOT_UNIQUE_IN_PERIOD);
			 }
			 
			 
			 
			 /** create processedServiceTo for show it on screen **/
			 ProcessedServiceTo processedServiceTo = new ProcessedServiceTo();
			 processedServiceTo.setBillingServiceTo(billingServiceTo);
			 
			 
			if (processedServiceToFilter.getBillingServiceTo().getCalculationPeriod().equals(PropertiesConstants.PERIOD_CALCULATION_DAILY_PK)){
				 processedServiceTo.setCalculationDate(processedServiceToFilter.getCalculationDate());
			}else if(processedServiceToFilter.getBillingServiceTo().getCalculationPeriod().equals(PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK)){
				 processedServiceTo.setYearProcess(processedServiceToFilter.getYearProcess());
				 processedServiceTo.setMonthProcess(processedServiceToFilter.getMonthProcess());
				 processedServiceTo.setInitialDate(processedServiceToFilter.getInitialDate());
				 processedServiceTo.setFinalDate(processedServiceToFilter.getFinalDate());
				 processedServiceTo.setCalculationDate(processedServiceToFilter.getCalculationDate());
			}
				
			 /**SETTING EXCHANGE WHEN IT IS'NT DOP**/
			
			 if ( !billingServiceTo.getCurrencyCalculation().equals(CurrencyType.PYG.getCode()) &&
					 !billingServiceTo.getCurrencyCalculation().equals(billingServiceTo.getCurrencyBilling())) {
				 dailyExchangeRateFilter.setIdCurrency(billingServiceTo.getCurrencyCalculation());
				 			 				
				 BigDecimal referencePrice	= businessLayerCommonFacade.gettingReferencePrice(dailyExchangeRateFilter); 
				 processedServiceTo.setReferencePrice(referencePrice);				 
			 }else {
				     processedServiceTo.setReferencePrice(new BigDecimal(1)); /** FACTOR 1**/
			 }
			
			 
			 
		 	if (    ! billingServiceSent 
		 			|| processedService.getProcessedState().equals(ProcessedServiceType.PROCESADO.getCode()) 
		 			|| processedService.getProcessedState().equals(ProcessedServiceType.ERROR.getCode())
		 			|| processedService.getProcessedState().equals(ProcessedServiceType.NO_PROCESADO.getCode()) ){
		 		 					 		 
				 processedServiceTo.setProcessedState(ProcessedServiceType.PENDIENTE.getCode());
				 processedServiceTo.setDescriptionProcessedState(ProcessedServiceType.PENDIENTE.getValue());
				 							
			}
			else  if ( processedService.getProcessedState().equals(ProcessedServiceType.FACTURADO.getCode()) ){
				 processedServiceTo.setProcessedState(ProcessedServiceType.FACTURADO.getCode());
				 processedServiceTo.setDescriptionProcessedState(ProcessedServiceType.FACTURADO.getValue());								 
				 
			}else  if ( processedService.getProcessedState().equals(ProcessedServiceType.EJECUTANDO.getCode()) ){
				 processedServiceTo.setProcessedState(ProcessedServiceType.EJECUTANDO.getCode());
				 processedServiceTo.setDescriptionProcessedState(ProcessedServiceType.EJECUTANDO.getValue());	
				 processedServiceTo.setIdProcessedServicePk(processedService.getIdProcessedServicePk());
				 processedServiceTo.setLastModifyDate(processedService.getLastModifyDate());							
				 listProcessedServiceToSent.add(processedServiceTo);
			}
		 	
		 	
		 	 if(processedService!=null && processedService.getCountCollection()!=null){
				 processedServiceTo.setCountNumCollection(processedService.getCountCollection());
				 processedServiceTo.setIdProcessedServicePk(processedService.getIdProcessedServicePk());
			 }
			 else {
				 processedServiceTo.setCountNumCollection(0L);
			 }
			 					 						 			 	 	
		 	 
	 	 return processedServiceTo;
	}
	
	
	/**
	 * Find Participant By Filters Service Bean .
	 *
	 * @param filter the filter
	 * @return the participant
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public Participant findParticipantByFiltersServiceBean(Participant filter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());	
	    
		return participantServiceBean.findParticipantByFiltersServiceBean(filter);
	}
	
	/**
	 * *
	 * Get Issuer By code.
	 *
	 * @param issuerSearcher that were billed
	 * @return Entity Issuer
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public Issuer getIssuerByCode(IssuerSearcherTO issuerSearcher) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());	
	    
		return issuerQueryService.getIssuerByCode(issuerSearcher) ;
	}
	
	/**
	 * Search Processed Service For Collection Manual.
	 *
	 * @param filter the filter
	 * @return the search processed service for collection manual
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<ProcessedServiceTo> getSearchProcessedServiceForCollectionManual(ProcessedServiceTo filter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());	
	    
		List<Object[]> resultList= collectionServiceMgmtServiceBean.getSearchProcessedServiceForCollectionManual(filter);
		ProcessedServiceTo processedServiceTo=null;		
		List<ProcessedServiceTo> listProcessedServiceTo=new ArrayList<ProcessedServiceTo>();		
		CollectionRecordTo collectionRecordTo=null;
		
		for(int i=0;i<resultList.size();++i){
			
			Object[] sResults = (Object[])resultList.get(i);
			
			if(i==0){
				processedServiceTo = new ProcessedServiceTo();
				collectionServiceMgmtServiceBean.loadProcessedServiceToForCollectionManual(processedServiceTo, sResults);
				
		        listProcessedServiceTo.add(processedServiceTo);
			}
			else {
				if(sResults[7].toString().compareTo(processedServiceTo.getBillingServiceTo().getServiceCode())==0){
					 collectionRecordTo=new CollectionRecordTo();
					 collectionRecordTo.setDescriptionEntityCollection(sResults[9]==null?"":sResults[9].toString());
					 collectionRecordTo.setIdProcessedServiceFk(Long.parseLong(sResults[0]==null?"":sResults[0].toString()));    
				     collectionRecordTo.setRecordState(sResults[1]==null?null:Integer.parseInt(sResults[1].toString()));
					 collectionRecordTo.setCollectionAmount(sResults[19]==null?null:new BigDecimal(sResults[19].toString()));
				     collectionRecordTo.setGrossAmount(sResults[20]==null?null:new BigDecimal(sResults[20].toString()));
				     collectionRecordTo.setTaxApplied(sResults[21]==null?null:new BigDecimal(sResults[21].toString()));
				     collectionRecordTo.setDescriptionStatusRecord(sResults[4]==null?"":sResults[4].toString());
			         collectionRecordTo.setIndBilled(sResults[24]==null?null:Integer.parseInt(sResults[24].toString()));
				     collectionRecordTo.setDescriptionIndBilled(sResults[25]==null?"":sResults[25].toString());
				     collectionRecordTo.setCalculationDate(sResults[26]==null?null:(Date) sResults[26]);
				     collectionRecordTo.setOperationDate(sResults[27]==null?null:(Date) sResults[27]);
				     collectionRecordTo.setIdCollectionRecordPk(Long.parseLong(sResults[28]==null?"":sResults[28].toString()));
				     processedServiceTo.getCollectionRecordsTo().add(collectionRecordTo);
				     
				}
				else{
					processedServiceTo = new ProcessedServiceTo();
					collectionServiceMgmtServiceBean.loadProcessedServiceToForCollectionManual(processedServiceTo, sResults);
			        listProcessedServiceTo.add(processedServiceTo);
				}
			}
		}
		return listProcessedServiceTo;	
	}
	

	/**
	 * Find ProcessedService By Id
	 * ProcessedService with BillingService and ServiceRates.
	 *
	 * @param id idProcessedServicePk
	 * @return the processed service
	 * @throws ServiceException the service exception
	 */
	public ProcessedService findProcessedServiceAllById(Long id) throws ServiceException{
		ProcessedService processedService = null;
		
		processedService = collectionServiceMgmtServiceBean.findProcessedServiceAllById(id);
		
		if (Validations.validateIsNotNull(processedService)){
			log.debug(processedService.getBillingService().getIdBillingServicePk()+"");
		}
		return processedService;
	}
	
	/**
	 * Find ProcessedService By Id.
	 *
	 * @param id idProcessedServicePk
	 * @return the processed service
	 * @throws ServiceException the service exception
	 */
	public ProcessedService findProcessedServiceById(Long id) throws ServiceException{
		ProcessedService processedService = null;
		
		processedService = collectionServiceMgmtServiceBean.findProcessedServiceById(id);
	
		return processedService;
	}
	
	/**
	 * Find Processed Service for Process Batch.
	 *
	 * @param processedServiceTo the processed service to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ProcessedService>	findProcessedServiceForBatch(ProcessedServiceTo processedServiceTo)	throws ServiceException{
		return	collectionServiceMgmtServiceBean.findProcessedServiceForBatch(processedServiceTo);
	}
	
	/**
	 * Find Processed Service for Service Calculate.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ProcessedServiceTo> findProcessedServiceForServiceCalculate(ProcessedServiceTo filter) throws ServiceException{
		return collectionServiceMgmtServiceBean.findProcessedServiceForServiceCalculate(filter);
	}

	/**
	 * Find Processed Service for Recalculate.
	 *
	 * @param processedServiceTo the processed service to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ProcessedService>	findProcessedServiceForRecalculate(ProcessedServiceTo processedServiceTo) throws ServiceException{
		return collectionServiceMgmtServiceBean.findProcessedServiceForCalculate(processedServiceTo);
	}
	
	/**
	 * Find Calculation Record Billed.
	 *
	 * @param processedServiceTo the processed service to
	 * @return the list
	 */
	public List<CollectionRecord>	findCalculationRecordBilled(ProcessedServiceTo processedServiceTo){
		return collectionServiceMgmtServiceBean.findCalculationRecordBilled(processedServiceTo);
	}
	
	/**
	 * Delete Collection Record Detail for recalculate.
	 *
	 * @param processedServiceId the processed service id
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int  deleteCollectionRecordDetailForRecalculate(Long  processedServiceId) throws ServiceException{
		return collectionServiceMgmtServiceBean.deleteCollectionRecordDetailForRecalculate(processedServiceId);
	}
	
	
	/**
	 * Delete Collection record for recalculate.
	 *
	 * @param processedServiceId the processed service id
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int  deleteCollectionRecordForRecalculate(Long  processedServiceId) throws ServiceException{
		return collectionServiceMgmtServiceBean.deleteCollectionRecordForRecalculate(processedServiceId);
	}
	
	
	/**
	 * Delete processed service.
	 *
	 * @param processedServiceId the processed service id
	 * @throws ServiceException the service exception
	 */
	public void deleteProcessedService(Long processedServiceId) throws ServiceException{
		collectionServiceMgmtServiceBean.delete(ProcessedService.class, processedServiceId);
	}
	
	
	/**
	 * Verify Collection UnBilled.
	 *
	 * @param processedServiceTo the processed service to
	 * @return ProcessedService
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@LoggerAuditBatch
	public ProcessedService verifyCollectionUnbilled(ProcessedServiceTo processedServiceTo) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
	    
		/** FIND PROCESSED SERVICE FOR APPPLY RECALCULATE **/
		List<ProcessedService> processedServiceListTemp = null;
		
		ProcessedService processedService =new ProcessedService();
		
		/** Find Processed Service For a  date **/
		processedServiceListTemp = findProcessedServiceForBatch(processedServiceTo);
		
		
		/*** Verify Processed Service Unique***/
			
		this.verifyProcessedServiceUniqueByPeriod(processedServiceTo);
		
		
		/** ALWAYS SHOULD FIND ONE PROCESS SERVICE*/
		 if (processedServiceListTemp.size()==1){
				
			processedService = processedServiceListTemp.get(0);
			processedService.setCalculationDate(processedServiceTo.getCalculationDate());
			loaderEntityServiceBean.detachmentEntity(processedService);
								
			if (!processedService.getProcessedState().equals(ProcessedServiceType.FACTURADO.getCode())){
				
				processedService.setOperationDate(CommonsUtilities.currentDate());	
				processedService.setProcessedState(ProcessedServiceType.EJECUTANDO.getCode());					
				updateProcessedServiceByBatch(processedService);
				
				try {
					deleteCollectionRecordDetailForRecalculate(processedService.getIdProcessedServicePk());
					deleteCollectionRecordForRecalculate(processedService.getIdProcessedServicePk());
				} catch (ServiceException e) {				
					e.printStackTrace();
				}
	
		    }
			
				
		}else if  ( processedServiceListTemp.size() == 0) { 
								
			processedService	= saveProcessedServiceAutomatic(processedServiceTo);
			processedService.setProcessedState(ProcessedServiceType.EJECUTANDO.getCode());
			updateProcessedServiceByBatch(processedService);
			
		}
						
		BillingServiceTo billingServiceTo = new BillingServiceTo();
		
		billingServiceTo.setIdBillingServicePk(processedServiceTo.getBillingServiceTo().getIdBillingServicePk());
		BillingService billingService = billingServiceMgmtServiceFacade.getBillingServiceWithServiceRate(billingServiceTo);					
		processedService.setBillingService(billingService);
		
		em.flush();
		
		
		return processedService;
	}
	
	/**
	 * Cancel Processed Service.
	 *
	 * @param processedService the processed service
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public void cancelProcessedService(ProcessedService processedService)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeDelete());
	    
	    deleteProcessedService(processedService.getIdProcessedServicePk());
	   
	}
	
	/**
	 * *
	 * Confirm Processed Service.
	 *
	 * @param processedServicePk the processed service pk
	 * @param parameters the parameters
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_MODIFICATION)
	public void confirmProcessedService(Long processedServicePk,Map parameters)throws ServiceException{					
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());	    	   
	    ProcessedService	processedService	=  getProcessedServiceByCode(processedServicePk);
	    Date calculationDate= (Date) parameters.get(GeneralConstants.CALCULATION_DATE);
	    
	    if (processedService.getCollectionRecords().size()>1){
	    	throw new ServiceException();
	    }
	    
	    CollectionRecord	collectionRecord	=  processedService.getCollectionRecords().get(0);
	    processedService.setProcessedState(ProcessedServiceType.PROCESADO.getCode());
	    processedService.setCalculationDate(CommonsUtilities.currentDate());
	    collectionRecord.setRecordState(ProcessedServiceType.PROCESADO.getCode());
	    collectionRecord.setCalculationDate(calculationDate);
	    businessLayerCommonFacade.billingChargesToConvertCurrency(collectionRecord, parameters);
	    collectionServiceMgmtServiceBean.update(processedService);
	}

	/**
	 * Save Processed Service.
	 *
	 * @param processedService the processed service
	 * @return the processed service
	 * @throws ServiceException the service exception
	 */
	public ProcessedService saveProcessedService(ProcessedService processedService) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		this.collectionServiceMgmtServiceBean.saveProcessedService(processedService);
		return processedService;
	}
	
	/**
	 * *
	 * Save Processed Service Automatic.
	 *
	 * @param processedServiceTo the processed service to
	 * @return the processed service
	 * @throws ServiceException the service exception
	 */
	public ProcessedService saveProcessedServiceAutomatic(ProcessedServiceTo processedServiceTo) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
				
		ProcessedService processedService = new ProcessedService();	
		BillingService billingService= new BillingService();
		
		
		/** SETTING CALCULATION DATE FOR DAILY OR MONTHLY***/
		if (Validations.validateIsNull(processedServiceTo.getCalculationDate())){
			if(processedServiceTo.getBillingServiceTo().getCalculationPeriod().equals(PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK)){
				
				String strDate       	=CommonsUtilities.convertDateToString(CommonsUtilities.currentDate(),BillingUtils.DATE_PATTERN_yyyy_MM_dd);
				String strYearMonth  	=BillingUtils.getPatternYearMonth(strDate, BillingUtils.DATE_PATTERN_yyyy_MM_dd);
				
				processedService.setYearProcess(BillingUtils.getPatternYear(strYearMonth,BillingUtils.DATE_PATTERN_yyyy_MM));
				processedService.setMonthProcess(BillingUtils.getPatternMonth(strYearMonth,BillingUtils.DATE_PATTERN_yyyy_MM));
				Integer maxDayOfMonth	=	CommonsUtilities.getMaxDateOfMonth(CommonsUtilities.currentDate());				
				Date maxDateOfMonth 	=	BillingUtils.truncateDayOfMonth(CommonsUtilities.currentDate());	
				maxDateOfMonth			=	CommonsUtilities.addDaysToDate(maxDateOfMonth, maxDayOfMonth);
				processedService.setCalculationDate(maxDateOfMonth);	
				
			}else if(processedServiceTo.getBillingServiceTo().getCalculationPeriod().equals(PropertiesConstants.PERIOD_CALCULATION_DAILY_PK)){
				
				processedService.setCalculationDate(CommonsUtilities.currentDate());
			}
			
		}else{
			processedService.setCalculationDate(processedServiceTo.getCalculationDate());
			
			/** SETTING YEAR AND MONTH WHEN SERVICE HAS CALCULATION  MONTHLY***/
			if(processedServiceTo.getBillingServiceTo().getCalculationPeriod().equals(PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK)){
				
				processedService.setYearProcess(processedServiceTo.getYearProcess());
				processedService.setMonthProcess(processedServiceTo.getMonthProcess());
				
				processedService.setInitialDate(processedServiceTo.getInitialDate());
				processedService.setFinalDate(processedServiceTo.getFinalDate());
			}
			
		}
		

		processedService.setOperationDate(CommonsUtilities.currentDate());
		processedService.setProcessedState(ProcessedServiceType.REGISTRADO.getCode());
		
		billingService.setIdBillingServicePk(processedServiceTo.getBillingServiceTo().getIdBillingServicePk());
		processedService.setBillingService(billingService);
		
		saveProcessedService(processedService);	
		
		return processedService;
	}
	
	/**
	 * *
	 * Find Processed Service For Consult.
	 *
	 * @param filter the filter
	 * @return the processed service to
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public ProcessedServiceTo findProcessedServiceToForConsult(ProcessedServiceTo filter) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		return collectionServiceMgmtServiceBean.findProcessedServiceToForConsult(filter);
	}
	
	/**
	 * Save Collection Record And Details.
	 *
	 * @param collectionRecord the collection record
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	public void saveCollectionRecordAndDetails(CollectionRecord collectionRecord) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
	    
	    collectionServiceMgmtServiceBean.saveCollectionRecord(collectionRecord);
	    	
	    List<CollectionRecordDetail> collectionRecordDetailList= collectionRecord.getCollectionRecordDetail();
	    
	    if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordDetailList)){
	    	
	    		for (CollectionRecordDetail collectionRecordDetail : collectionRecordDetailList) {
					collectionRecordDetail.setCollectionRecord(collectionRecord);
					collectionServiceMgmtServiceBean.saveCollectionRecordDetails(collectionRecordDetail);
				}
	    }
	}
	
	
	/**
	 * Save ProcessedService Full and Collection Record By Batch.
	 *
	 * @param processedService the processed service
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	@LoggerAuditBatch
	public void saveProcessedServiceByBatch(ProcessedService processedService) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
	    
		saveProcessedServiceAll(processedService);
	}
	
	/**
	 * *
	 * Save Processed Service and Full Collection Record By Batch .
	 *
	 * @param processedService the processed service
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveProcessedServiceAll(ProcessedService processedService) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
	    if (Validations.validateIsNotNull(processedService.getIdProcessedServicePk())){
	    	
	    	List<CollectionRecord> collectionRecordList = processedService.getCollectionRecords();
	    	if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordList)){
	    		for (CollectionRecord collectionRecord : collectionRecordList) {
	    			
	    			collectionServiceMgmtServiceBean.saveCollectionRecord(collectionRecord);
	    			
	    			List<CollectionRecordDetail> collectionRecordDetailList= collectionRecord.getCollectionRecordDetail();
				    
				    if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordDetailList)){
				    	CollectionRecord newCollectionRecord = new CollectionRecord(); 
				    	newCollectionRecord.setIdCollectionRecordPk(collectionRecord.getIdCollectionRecordPk());
			    		for (CollectionRecordDetail collectionRecordDetail : collectionRecordDetailList) {
			    			//issue 000057
//				    			collectionRecord.setIssuer(null);
			    			collectionRecordDetail.setCollectionRecord(newCollectionRecord);
			    			collectionServiceMgmtServiceBean.saveCollectionRecordDetails(collectionRecordDetail);
						}				    		
				    }
	    		}
	    	}
	    	
	    }else{
	    	collectionServiceMgmtServiceBean.saveProcessedService(processedService);
	    }
	    
	
		log.debug(":::::::::::::::::::::::::::::::::::::: Processed Service :  "+processedService.getIdProcessedServicePk());
		
		List<CollectionRecord> collectionRecordList = processedService.getCollectionRecords();
		
		/*if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordList)){
			
				for (CollectionRecord collectionRecord : collectionRecordList) {
					
					List<CollectionRecordDetail> collectionRecordDetailList= collectionRecord.getCollectionRecordDetail();
				    
				    if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordDetailList)){
		    			collectionRecord = collectionServiceMgmtServiceBean.find(CollectionRecord.class, collectionRecord.getIdCollectionRecordPk());
				    		for (CollectionRecordDetail collectionRecordDetail : collectionRecordDetailList) {
				    			//issue 000057
//				    			collectionRecord.setIssuer(null);
				    			collectionRecordDetail.setCollectionRecord(collectionRecord);
				    			collectionServiceMgmtServiceBean.saveCollectionRecordDetails(collectionRecordDetail);
							}				    		
				    }
				}
		}*/
		
	}
	
	
	/**
	 * Update Processed Service By Batch.
	 *
	 * @param processedService the processed service
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditBatch
	public void updateProcessedServiceByBatch(ProcessedService processedService) throws ServiceException{	
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
	    
	    updateProcessedService(processedService);
	}
	
	/**
	 * Update Processed Service.
	 *
	 * @param processedService the processed service
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_MODIFICATION)
	public void updateProcessedService(ProcessedService  processedService) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		collectionServiceMgmtServiceBean.updateProcessedService(processedService);
		
	}
	
	
	/**
	 * Refresh State Processed Service To On Calculation by screen Send Calculation.
	 *
	 * @param processedServiceToList the processed service to list
	 * @throws ServiceException the service exception
	 */
	public void refreshStateProcessedServiceToOnCalculation(List<ProcessedServiceTo> processedServiceToList) throws ServiceException{
		
		for (ProcessedServiceTo processedServiceTo : processedServiceToList) {
			
			try {
				
				/**GETTING  BILLING SERVICE STATUS FOR UPDATE ON FRONT PAGE **/
				List<ProcessedService> processedServiceList = null;			
				processedServiceList =  findProcessedServiceForRecalculate(processedServiceTo);
				
				
				if (Validations.validateListIsNotNullAndNotEmpty(processedServiceList)){
					if (processedServiceList.size()==1){
						
						ProcessedService processedService = processedServiceList.get(0);						
						
						//if (processedService.getLastModifyUser().equals(userInfo.getUserAccountSession().getUserName()) ){
						
							if (  (Validations.validateIsNotNull(processedServiceTo.getLastModifyDate()) && 
								  processedService.getLastModifyDate().after(processedServiceTo.getLastModifyDate()) ) ||
								  Validations.validateIsNull(processedServiceTo.getLastModifyDate())  ){							
								
								processedServiceTo.setProcessedState(processedService.getProcessedState());
								processedServiceTo.setDescriptionProcessedState(ProcessedServiceType.get(processedService.getProcessedState()).getValue());
								processedServiceTo.setIdProcessedServicePk(processedService.getIdProcessedServicePk());
							}
							
							if (Validations.validateIsNull(processedServiceTo.getLastModifyDate()) ){
								processedServiceTo.setLastModifyDate(processedService.getLastModifyDate());								
							}
							
							processedServiceTo.setCountNumCollection(processedService.getCountCollection());
							
						//}
						
					} else if (processedServiceList.size()>1){
						
						throw new ServiceException(ErrorServiceType.PROCESSED_SERVICE_NOT_UNIQUE_IN_PERIOD);
					}
					
				}
				
									
			} catch (Exception e) {				
				
				processedServiceTo.setProcessedState(ProcessedServiceType.ERROR.getCode());
				processedServiceTo.setDescriptionProcessedState(ProcessedServiceType.get(ProcessedServiceType.ERROR.getCode()).getValue());
				
				if ( !(e instanceof ServiceException)){
					e.printStackTrace();
				}
							
			}

		  
		}
	}
	
	/**
	 * Get Processed Service By Code.
	 *
	 * @param id idProcessedServiceId
	 * @return Processed Service
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public ProcessedService getProcessedServiceByCode(Long id)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return collectionServiceMgmtServiceBean.find(ProcessedService.class, id);
	}
	
	/**
	 * Update Collection Record.
	 *
	 * @param collectionRecord the collection record
	 * @throws ServiceException the service exception
	 */
	public void updateCollectionRecord(CollectionRecord  collectionRecord) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		collectionServiceMgmtServiceBean.updateCollectionRecord(collectionRecord);
		
	}
	
	/**
	 * Update Collection Record.
	 *
	 * @param collectionRecord the collection record
	 * @throws ServiceException the service exception
	 */
	public void updateCollectionRecordByQuery(CollectionRecord  collectionRecord) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		collectionServiceMgmtServiceBean.updateCollectionRecordByQuery(collectionRecord,loggerUser);
		
	}
	
	
	/**
	 * Verify Rate Active.
	 *
	 * @param processedServiceTo the processed service to
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void  verifyRateActives(ProcessedServiceTo processedServiceTo)throws ServiceException{
		
	    
		BillingService billingService = billingServiceMgmtServiceFacade.getBillingServiceWithServiceRate(processedServiceTo.getBillingServiceTo());	
		if(Validations.validateIsNotNull(billingService)&&
				Validations.validateListIsNullOrEmpty(billingService.getServiceRates())){
			throw new ServiceException(ErrorServiceType.BILLING_SERVICE_NO_EXIST_RATE);
		}	 
		
		Boolean isActived=Boolean.FALSE;

		for(ServiceRate serviceRate:billingService.getServiceRates()){
			if(serviceRate.getRateState().equals(RateStateType.ACTIVED.getCode())){
				isActived=Boolean.TRUE;
				break;
			}
		}
		
		if(!isActived){
			throw new ServiceException(ErrorServiceType.BILLING_SERVICE_NO_EXIST_RATE_ACTIVED);
		}
	}
	
	
	/**
	 * Verify exchange rate.
	 *
	 * @param processedServiceTo the processed service to
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void  verifyExchangeRate(ProcessedServiceTo processedServiceTo)throws ServiceException{
		
		BillingService billingService = billingServiceMgmtServiceFacade.getBillingServiceWithServiceRate(processedServiceTo.getBillingServiceTo());	
		if(Validations.validateIsNotNull(billingService)&&
				Validations.validateListIsNullOrEmpty(billingService.getServiceRates())){
			throw new ServiceException(ErrorServiceType.BILLING_SERVICE_NO_EXIST_RATE);
		}	 
		
		Boolean isActived=Boolean.FALSE;

		for(ServiceRate serviceRate:billingService.getServiceRates()){
			if(serviceRate.getRateState().equals(RateStateType.ACTIVED.getCode())){
				isActived=Boolean.TRUE;
				break;
			}
		}
		
		if(!isActived){
			throw new ServiceException(ErrorServiceType.BILLING_SERVICE_NO_EXIST_RATE_ACTIVED);
		}
	}
	
	
	/**
	 * Verify Processed Service Unique By period.
	 *
	 * @param processedServiceTo the processed service to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<ProcessedService>  verifyProcessedServiceUniqueByPeriod(ProcessedServiceTo processedServiceTo) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());	 
	    
		/** FIND ALL PROCESSED SERVICE ON SAME PERIOD****/
		ProcessedServiceTo processedServiceToValidate	= SerializationUtils.clone(processedServiceTo);
		processedServiceToValidate.setIdProcessedServicePk(null);
		List<ProcessedService>	processedServiceList =	findProcessedServiceForRecalculate(processedServiceToValidate);
		
		
		/*** Validations basics  Begin**/	
		if (Validations.validateListIsNotNullAndNotEmpty(processedServiceList)&&(processedServiceList.size()>1)){
			throw new ServiceException(ErrorServiceType.PROCESSED_SERVICE_NOT_UNIQUE_IN_PERIOD);
		}
		
		
		return processedServiceList;
		
	}
	
	
	/**
	 * *
	 * Update Processed Service On Billing.
	 *
	 * @param processedServiceTo the processed service to
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_MODIFICATION)
	public void updateProcessedServiceOnBilling(ProcessedServiceTo processedServiceTo){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
	    
		collectionServiceMgmtServiceBean.updateProcessedServiceOnBilling(processedServiceTo);
	}
	

	/**
	 * Filter entities billed.
	 *
	 * @param processedService the processed service
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	/**
	 * Remove entities that were billed 
	 * @param processedService
	 */
	public void filterEntitiesBilled(ProcessedService processedService){
		
		ProcessedServiceTo processedServiceTo = new ProcessedServiceTo();
		BillingServiceTo billingServiceTo = new BillingServiceTo();
		billingServiceTo.setIdBillingServicePk(processedService.getBillingService().getIdBillingServicePk());
		processedServiceTo.setBillingServiceTo(billingServiceTo);
		processedServiceTo.setCalculationDate(processedService.getCalculationDate());
		processedServiceTo.setIdProcessedServicePk(processedService.getIdProcessedServicePk());
		processedServiceTo.setIndBilled(PropertiesConstants.YES_BILLED);
		
		if( (Validations.validateIsNotNull(processedService.getBillingService().getCalculationPeriod()))
			&& (processedService.getBillingService().getCalculationPeriod().equals(PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK)) ){
			
			processedServiceTo.setYearProcess(processedService.getYearProcess());
			processedServiceTo.setMonthProcess(processedService.getMonthProcess());
			
			processedServiceTo.setInitialDate(processedService.getInitialDate());
			processedServiceTo.setFinalDate(processedService.getFinalDate());
		}
		
		
		List<CollectionRecord>  collectionRecordBilledList = this.findCalculationRecordBilled(processedServiceTo);
		
		if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordBilledList)){
			
			List<CollectionRecord>  collectionRecordList 		=  processedService.getCollectionRecords();	
			Integer 				typeEntity 			 		=  processedService.getBillingService().getEntityCollection();		
			
			List<CollectionRecord>  collectionRecordRemoveList  =  new ArrayList<CollectionRecord>();
			
			
				for (CollectionRecord collectionRecord : collectionRecordList) {
						
					for (CollectionRecord collectionRecordBilled : collectionRecordBilledList) {
						
						Boolean exist=false;
						if(typeEntity.equals(EntityCollectionType.HOLDERS.getCode())){
							exist= collectionRecord.getHolder().getIdHolderPk().equals(collectionRecordBilled.getHolder().getIdHolderPk());
						}else if (typeEntity.equals(EntityCollectionType.ISSUERS.getCode())){
							exist= collectionRecord.getIssuer().getIdIssuerPk().equals(collectionRecordBilled.getIssuer().getIdIssuerPk());
						}else if(typeEntity.equals(EntityCollectionType.PARTICIPANTS.getCode())){
							exist= collectionRecord.getParticipant().getIdParticipantPk().equals(collectionRecordBilled.getParticipant().getIdParticipantPk());
						}
						
						if (exist){
							collectionRecordRemoveList.add(collectionRecord);
							break;
						}
						
					}
				}
				
				if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordRemoveList)){
					collectionRecordList.removeAll(collectionRecordRemoveList);
				}
			
		}
	
	   
	}
	
	/**
	 * Find collection record by parameters exception.
	 *
	 * @param processedService the processed service
	 * @param processedServiceTo the processed service to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	/**
	 * Find Collection Record By Parameters 
	 * <br> Processing Exceptions On Billing and filters entities Accept
	 * 
	 * @param processedService
	 * @param processedServiceTo
	 * @return
	 * @throws ServiceException
	 */
	public List<CollectionRecord>  findCollectionRecordByParametersException(ProcessedService processedService, 
														ProcessedServiceTo processedServiceTo) throws ServiceException{
		
		List<CollectionRecordTo> listCollectionRecordTo=collectionServiceMgmtServiceBean.
				findCollectionRecordToByEntityException(processedServiceTo, processedService.getBillingService().getListExceptionInvoiceGenerations());
		
		processedServiceTo.setCollectionRecordsTo(listCollectionRecordTo);
		
		if(Validations.validateListIsNullOrEmpty(listCollectionRecordTo)){
			return new ArrayList<CollectionRecord>();
		}
		
		return collectionServiceMgmtServiceBean.findCollectionRecordByParameters(processedServiceTo);
	}

	/**
	 * Find Collection Record By Parameters Processed Service Filter.
	 *
	 * @param processedServiceTo Filter
	 * @return the list
	 */
	public List<CollectionRecord>  findCollectionRecordByParameters(ProcessedServiceTo processedServiceTo){
				
		return collectionServiceMgmtServiceBean.findCollectionRecordByParameters(processedServiceTo);
	}
	
	/**
	 * Fix Sender Problem Calculation
	 * <br> Change State the Processed Service To No Process.
	 *
	 * @param processedServiceTo the processed service to
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public void fixProblemCalculation(ProcessedServiceTo processedServiceTo) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
	    
		ProcessedService processedService=findProcessedServiceById(processedServiceTo.getIdProcessedServicePk());
		processedService.setProcessedState(ProcessedServiceType.NO_PROCESADO.getCode());
		collectionServiceMgmtServiceBean.update(processedService);
	}
	
	/**
	 * Remote Processed Service.
	 *
	 * @param parameters the parameters
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	public void saveRemoteProcessedService(Map parameters ) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		

		
		/***
		 *   GETTING INFORMATION
		 */
	    Integer currency		= null;
		BillingServiceTo billingServiceTo 	= new BillingServiceTo();
		Long billingServiceId	= Long.parseLong(parameters.get(GeneralConstants.ID_BILLING_SERVICE_PK).toString());
		billingServiceTo.setIdBillingServicePk(billingServiceId);
		
		BillingService billingService  		= collectionServiceMgmtServiceBean.find(BillingService.class, billingServiceId);
		Integer collectionBase		   		= billingService.getBaseCollection();
		
		Date calculationDate	= CommonsUtilities.convertStringtoDate(parameters.get(GeneralConstants.CALCULATION_DATE).toString());
		BigDecimal netAmount	= new BigDecimal(parameters.get(GeneralConstants.AMOUNT).toString());
		Long serviceRateId		= Long.parseLong( parameters.get(GeneralConstants.ID_SERVICE_RATE_PK).toString());
		
		if(Validations.validateIsNotNull(parameters.get(GeneralConstants.CURRENCY))){
			currency		= Integer.parseInt( parameters.get(GeneralConstants.CURRENCY).toString());		
		}
		
		/**
		 * List Object with <Nominal_value, Quantity, Currency>
		 */
		List<Object[]> keySecurity=null;
		if(Validations.validateIsNotNull(parameters.get(GeneralConstants.CHANGE_OWNERSHIP_SECURITY))){

			keySecurity =	(List<Object[]>)parameters.get(GeneralConstants.CHANGE_OWNERSHIP_SECURITY);
			
		}else if(Validations.validateIsNotNull(parameters.get(GeneralConstants.AFFECTATION_LIST))){

			keySecurity =	(List<Object[]>)parameters.get(GeneralConstants.AFFECTATION_LIST);
			
		}
		
		/***
		 *   GETTING SERVICE RATE
		 */
		
		
		ServiceRate serviceRate = collectionServiceMgmtServiceBean.find(ServiceRate.class , serviceRateId);
		Integer rateType=serviceRate.getRateType();
		
		/***
		 * Add Movement to Service Rate
		 */
		List<RateMovement> rateMovementList = null;
		rateMovementList= billingRateMgmtServiceFacade.findRateMovementByServiceRate(serviceRate.getIdServiceRatePk());		
		serviceRate.setRateMovements(rateMovementList);
		
		parameters.put(GeneralConstants.SERVICE_RATE, serviceRate);
		parameters.put(GeneralConstants.BILLING_SERVICE, billingService);
		
		
		/***
		 * 
		 *      APPLY EXCEPTION  
		 */
		if( BaseCollectionType.REQUEST_CHANGE_OWNERSHIP_FIXED_INCOME.getCode().equals(collectionBase) || 
				BaseCollectionType.REQUEST_CHANGE_OWNERSHIP_VARIABLE_INCOME.getCode().equals(collectionBase) || 
				BaseCollectionType.PAYING_AGENT.getCode().equals(collectionBase) || 
				BaseCollectionType.RECORD_CORPORATIVE_EVENTS.getCode().equals(collectionBase)          ||
				BaseCollectionType.GENERATION_RECORDS.getCode().equals(collectionBase)   ||
				BaseCollectionType.ISSUANCE_CAT.getCode().equals(collectionBase)   ||
				BaseCollectionType.REGISTRATION_PRECAUTIONARY_MEASURES.getCode().equals(collectionBase) ||
				BaseCollectionType.LIFTING_PRECAUTIONARY_MEASURES.getCode().equals(collectionBase)){
			
			List<Object>  lista= baseCollectionMgmtServiceFacade.processExceptionBaseRemote(parameters);
			
			if (! Validations.validateListIsNotNullAndNotEmpty(lista)){
				return;
			}
		}
		
		 
		/***
		 *     EXCHANGE RATE
		 * 
		 */			
		
		 DailyExchangeRateFilter dailyExchangeRateFilter = new DailyExchangeRateFilter();			
		 dailyExchangeRateFilter.setIdSourceinformation(billingService.getSourceInformation());		
		 BigDecimal referencePrice = BigDecimal.ZERO;
		 BigDecimal amountDetail   = BigDecimal.ZERO;
		 dailyExchangeRateFilter.setInitialDate((calculationDate==null?null:CommonsUtilities.truncateDateTime(calculationDate)));
		 dailyExchangeRateFilter.setFinalDate((calculationDate==null?null:CommonsUtilities.truncateDateTime(calculationDate)));

		/**
		 *  GET ALL PROCESS WITH FILTERS
		 * 
		 * */
		ProcessedServiceTo processedServiceTo=new ProcessedServiceTo();
		CollectionRecordTo collectionRecordTo=new CollectionRecordTo();
		processedServiceTo.setCollectionRecordToSearch(collectionRecordTo);
		processedServiceTo.setBillingServiceTo(billingServiceTo);
		processedServiceTo.setCalculationDate(calculationDate);
		processedServiceTo.setProcessedState(ProcessedServiceType.PROCESADO.getCode());

		
		if (collectionBase.equals(BaseCollectionType.REQUEST_CHANGE_OWNERSHIP_FIXED_INCOME.getCode())||
				BaseCollectionType.REQUEST_CHANGE_OWNERSHIP_VARIABLE_INCOME.getCode().equals(collectionBase)){
			if (billingService.getEntityCollection().equals(EntityCollectionType.PARTICIPANTS.getCode())){
				Long participant	= Long.parseLong(parameters.get(GeneralConstants.ID_PARTICIPANT_PK).toString());
				collectionRecordTo.setParticipant(new Participant(participant));
			}
			
			else if (billingService.getEntityCollection().equals(EntityCollectionType.HOLDERS.getCode())){
				Long holder	= Long.parseLong(parameters.get(GeneralConstants.ID_HOLDER_PK).toString());	
				collectionRecordTo.setHolder(new Holder(holder));
			}
			
			netAmount=getAmountToChangeOwner(keySecurity,billingService,dailyExchangeRateFilter);
		}
		else if (collectionBase.equals(BaseCollectionType.ISSUANCE_CAT.getCode())){
			if (billingService.getEntityCollection().equals(EntityCollectionType.PARTICIPANTS.getCode())){
				Long participant	= Long.parseLong(parameters.get(GeneralConstants.ID_PARTICIPANT_PK).toString());
				collectionRecordTo.setParticipant(new Participant(participant));
			}
			
			else if (billingService.getEntityCollection().equals(EntityCollectionType.HOLDERS.getCode())){
				Long holder	= Long.parseLong(parameters.get(GeneralConstants.ID_HOLDER_PK).toString());	
				collectionRecordTo.setHolder(new Holder(holder));
			}
			netAmount=getAmountToAffectation(keySecurity, billingService, dailyExchangeRateFilter);
		}
		else if (collectionBase.equals(BaseCollectionType.REGISTRATION_PRECAUTIONARY_MEASURES.getCode())){
			if (billingService.getEntityCollection().equals(EntityCollectionType.PARTICIPANTS.getCode())){
				Long participant	= Long.parseLong(parameters.get(GeneralConstants.ID_PARTICIPANT_PK).toString());
				collectionRecordTo.setParticipant(new Participant(participant));
			}
			
			else if (billingService.getEntityCollection().equals(EntityCollectionType.HOLDERS.getCode())){
				Long holder	= Long.parseLong(parameters.get(GeneralConstants.ID_HOLDER_PK).toString());	
				collectionRecordTo.setHolder(new Holder(holder));
			}
			netAmount=getAmountToAffectation(keySecurity, billingService, dailyExchangeRateFilter);
		}
		else if (collectionBase.equals(BaseCollectionType.PAYING_AGENT.getCode())){
		
			if ( !( (parameters.get(GeneralConstants.ID_HOLDER_ACCOUNT_PK)!=null) && 
					(parameters.get(GeneralConstants.ID_HOLDER_PK)!=null) &&
					(parameters.get(GeneralConstants.ID_SECURITY_CODE)!=null)) ){
				throw new  ServiceException();
			}
			
			Long holder	= Long.parseLong(parameters.get(GeneralConstants.ID_HOLDER_PK).toString());	
			Long accountHolder	= Long.parseLong(parameters.get(GeneralConstants.ID_HOLDER_ACCOUNT_PK).toString());
			collectionRecordTo.setHolder(new Holder(holder));
			collectionRecordTo.setHolderAccount(new HolderAccount(accountHolder));
			
			
		}else if (collectionBase.equals(BaseCollectionType.RECORD_CORPORATIVE_EVENTS.getCode())){
			
			if (billingService.getEntityCollection().equals(EntityCollectionType.PARTICIPANTS.getCode())){
				Long participant	= Long.parseLong(parameters.get(GeneralConstants.ID_PARTICIPANT_PK).toString());
				collectionRecordTo.setParticipant(new Participant(participant));
			}
			
			else if (billingService.getEntityCollection().equals(EntityCollectionType.HOLDERS.getCode())){
				Long holder	= Long.parseLong(parameters.get(GeneralConstants.ID_HOLDER_PK).toString());	
				collectionRecordTo.setHolder(new Holder(holder));
			}
			
		}else 
			if (collectionBase.equals(BaseCollectionType.GENERATION_RECORDS.getCode())){
			
			if (billingService.getEntityCollection().equals(EntityCollectionType.PARTICIPANTS.getCode())){
				Long participant	= Long.parseLong(parameters.get(GeneralConstants.ID_PARTICIPANT_PK).toString());
				collectionRecordTo.setParticipant(new Participant(participant));
			}
			
			if (billingService.getEntityCollection().equals(EntityCollectionType.HOLDERS.getCode())){
				Long holder	= Long.parseLong(parameters.get(GeneralConstants.ID_HOLDER_PK).toString());	
				collectionRecordTo.setHolder(new Holder(holder));
			}
			
		}

		
		/**
		 *   FIND PROCESS SERVICE
		 */
		
		ProcessedService processedServiceSearch=null;		
		processedServiceSearch	=	collectionServiceMgmtServiceBean.findProcessedServiceByEntity(processedServiceTo);

		referencePrice=businessLayerCommonFacade.processExchange(parameters,currency,
				billingService.getCurrencyCalculation(),DailyExchangeRoleType.BUY.getCode());

		 amountDetail  =  BillingUtils.multiplyDecimals( referencePrice, netAmount, GeneralConstants.ROUNDING_DIGIT_NUMBER);
		

		if(Validations.validateIsNotNull(processedServiceSearch)){
			
				ProcessedServiceTo processedServiceToFilter=new ProcessedServiceTo();
				processedServiceToFilter.setIdProcessedServicePk(processedServiceSearch.getIdProcessedServicePk());
				
				List<CollectionRecord> listCollectionRecord	=	collectionServiceMgmtServiceBean.findCollectionRecordByParameters(processedServiceToFilter);
				CollectionRecord collectionRecord = null;
				
				if (Validations.validateListIsNotNullAndNotEmpty(listCollectionRecord)){
					
					if (listCollectionRecord.size()>1){
						throw new ServiceException();
					}
					
					collectionRecord = listCollectionRecord.get(0);
					
				}else {
					throw new ServiceException();
				}
				
				
				/**
				 * 
				 *   CREATE NEW COLLECTION RECORD DETAIL
				 */
				CollectionRecordDetail collectionRecordDetail=gettingCollectionDetailToAffectation(parameters, 
																dailyExchangeRateFilter, keySecurity);
				
				BigDecimal collectionAmount=  BigDecimal.ZERO;
				if(RateType.FIJO.getCode().equals(rateType)){
					
					BigDecimal temporalAmount				=  BigDecimal.ZERO;
					temporalAmount=serviceRate.getRateAmount();
					
					collectionRecordDetail.setRateAmount(temporalAmount);
										
					collectionAmount=temporalAmount;
					
				}
				else if(RateType.PORCENTUAL.getCode().equals(rateType)){
					
					BigDecimal percentAmount 		       		=  BigDecimal.ZERO;
					BigDecimal ratePercent						=  serviceRate.getRatePercent();
					percentAmount= BillingUtils.multiplyDecimals(amountDetail,ratePercent , GeneralConstants.ROUNDING_DIGIT_NUMBER);
					percentAmount=	businessLayerCommonFacade.appliedMinimumAmountCalculation(serviceRate, percentAmount, parameters);
					collectionRecordDetail.setRatePercent(percentAmount);
					collectionAmount=percentAmount;
				}
				else if(RateType.ESCALONADO_FIJO.getCode().equals(rateType)||
						RateType.ESCALONADO_MIXTO.getCode().equals(rateType)){
					
					BigDecimal rateAmount=processAccumulativeScale(parameters, amountDetail);
					rateAmount=		businessLayerCommonFacade.appliedMinimumAmountCalculation(serviceRate, rateAmount, parameters);
					collectionRecordDetail.setRateAmount(rateAmount);
					collectionAmount=rateAmount;
				}
				else if(RateType.ESCALONADO_PORCENTUAL.getCode().equals(rateType)){
					
					BigDecimal percentAmount=processAccumulativeScale(parameters, amountDetail);
					percentAmount=	businessLayerCommonFacade.appliedMinimumAmountCalculation(serviceRate, percentAmount, parameters);
					collectionRecordDetail.setRatePercent(percentAmount);
					collectionAmount=percentAmount;
				}
				
				collectionRecordDetail.setServiceRate(serviceRate);
				collectionRecordDetail.setCollectionRecord(collectionRecord);
				
				/**
				 * Number Request
				 */
				if(parameters.get(GeneralConstants.NUMBER_REQUEST)!=null){
					collectionRecordDetail.setBallotSequential(parameters.get(GeneralConstants.NUMBER_REQUEST).toString());//
				}
				/**
				 * Block Type
				 */
				if(parameters.get(GeneralConstants.BLOCK_TYPE)!=null){
					collectionRecordDetail.setRole(Integer.parseInt(parameters.get(GeneralConstants.BLOCK_TYPE).toString()));//
				}
				
												
				collectionServiceMgmtServiceBean.saveCollectionRecordDetails(collectionRecordDetail);
				
				/***
				 *    UPDATE COLLECTION RECORD
				 */				
				collectionRecord.setCollectionAmount(BillingUtils.addTwoDecimal(collectionRecord.getCollectionAmount(), collectionAmount, GeneralConstants.ROUNDING_DIGIT_NUMBER));
				
				/** ONLY IF SERVICE APPLY   TAXED **/
				if (billingService.getTaxApplied().equals(PropertiesConstants.YES_APPLY_TAXED)){
					
					 ParameterTable parameterTableTax= billingServiceMgmtServiceBean.getParameterTableById(PropertiesConstants.TAX_PK);				
					 Double taxed=(parameterTableTax!=null)?parameterTableTax.getDecimalAmount1():new Double(GeneralConstants.ZERO_VALUE_STRING);
					
					BigDecimal taxedAmount=BillingUtils.multiplyDecimals(collectionRecord.getCollectionAmount(), new BigDecimal(taxed.toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
					collectionRecord.setTaxApplied(taxedAmount);
							
					BigDecimal grossAmount = BillingUtils.addTwoDecimal(collectionRecord.getCollectionAmount(), taxedAmount, GeneralConstants.ROUNDING_DIGIT_NUMBER);
					
					/** SETTING FINALLY COLLECTION AMOUNT **/
					collectionRecord.setGrossAmount(grossAmount);
					
				}else{
					collectionRecord.setTaxApplied(new BigDecimal(GeneralConstants.ZERO_VALUE_STRING));
					collectionRecord.setGrossAmount(collectionRecord.getCollectionAmount());
				}
															
				collectionServiceMgmtServiceBean.update(collectionRecord);		
				
		}
		else{
			
			ProcessedService processedService=new ProcessedService();
			CollectionRecord collectionRecord=new CollectionRecord();

			
			List<CollectionRecordDetail> listCollectionRecordDetail=new ArrayList<CollectionRecordDetail>();

			CollectionRecordDetail collectionRecordDetail=gettingCollectionDetailToAffectation(parameters, 
					dailyExchangeRateFilter, keySecurity);
			/**
			 * Grabo el detalle del cobro 
			 * y aplico el cobro a la suma de los detalles
			 */
			/**
			 * 
			 *   CREATE NEW COLLECTION RECORD DETAIL
			 */
			BigDecimal collectionAmount=  BigDecimal.ZERO;
			if(RateType.FIJO.getCode().equals(rateType)){
				
				
				BigDecimal temporalAmount				=  BigDecimal.ZERO;
				temporalAmount=serviceRate.getRateAmount();
				collectionRecordDetail.setRateAmount(temporalAmount);
				collectionAmount=temporalAmount;
				
			}
			else if(RateType.PORCENTUAL.getCode().equals(rateType)){
				
				BigDecimal percentAmount 		       		=  BigDecimal.ZERO;
				BigDecimal ratePercent						=  serviceRate.getRatePercent();
				percentAmount= BillingUtils.multiplyDecimals(amountDetail,ratePercent , GeneralConstants.ROUNDING_DIGIT_NUMBER);
				percentAmount=	businessLayerCommonFacade.appliedMinimumAmountCalculation(serviceRate, percentAmount, parameters);
				collectionRecordDetail.setRatePercent(percentAmount);
				collectionAmount=percentAmount;
				
			}
			else if(RateType.ESCALONADO_FIJO.getCode().equals(rateType)||
					RateType.ESCALONADO_MIXTO.getCode().equals(rateType)){
				
				BigDecimal rateAmount=processAccumulativeScale(parameters, amountDetail);
				rateAmount=	businessLayerCommonFacade.appliedMinimumAmountCalculation(serviceRate, rateAmount, parameters);
				collectionRecordDetail.setRateAmount(rateAmount);
				collectionAmount=rateAmount;
				
			}
			else if(RateType.ESCALONADO_PORCENTUAL.getCode().equals(rateType)){
				
				BigDecimal percentAmount=processAccumulativeScale(parameters, amountDetail);
				percentAmount=	businessLayerCommonFacade.appliedMinimumAmountCalculation(serviceRate, percentAmount, parameters);
				collectionRecordDetail.setRatePercent(percentAmount);
				collectionAmount=percentAmount;
				
			}
		
			/**
			 * Numero de Solicitud
			 */
			if(parameters.get(GeneralConstants.NUMBER_REQUEST)!=null){
				collectionRecordDetail.setBallotSequential(parameters.get(GeneralConstants.NUMBER_REQUEST).toString());//
			}
			/**
			 * Tipo de Bloqueo
			 */
			if(parameters.get(GeneralConstants.BLOCK_TYPE)!=null){
				collectionRecordDetail.setRole(Integer.parseInt(parameters.get(GeneralConstants.BLOCK_TYPE).toString()));//
			}
			/**
			 * 
			 * CREATE NEW COLLECTION RECORD 
			 * 
			 */
			collectionRecord.setCollectionAmount(BillingUtils.addTwoDecimal(BigDecimal.ZERO, collectionAmount, GeneralConstants.ROUNDING_DIGIT_NUMBER));
			collectionRecordDetail.setServiceRate(serviceRate);
			collectionRecordDetail.setCollectionRecord(collectionRecord);
			
											
			listCollectionRecordDetail.add(collectionRecordDetail);						
			List<CollectionRecord> listCollectionRecord=new ArrayList<CollectionRecord>();
			
			
			 if(BaseCollectionType.REQUEST_CHANGE_OWNERSHIP_FIXED_INCOME.getCode().equals(collectionBase) || 
						BaseCollectionType.REQUEST_CHANGE_OWNERSHIP_VARIABLE_INCOME.getCode().equals(collectionBase)){
				 
				if (billingService.getEntityCollection().equals(EntityCollectionType.PARTICIPANTS.getCode())){
					Long participant	= Long.parseLong(parameters.get(GeneralConstants.ID_PARTICIPANT_PK).toString());
					collectionRecord.setParticipant(new Participant(participant));
				}else if (billingService.getEntityCollection().equals(EntityCollectionType.HOLDERS.getCode())){
					Long holder	= Long.parseLong(parameters.get(GeneralConstants.ID_HOLDER_PK).toString());	
					collectionRecord.setHolder(new Holder(holder));
				}
				 
			 }
			 else if(BaseCollectionType.PAYING_AGENT.getCode().equals(collectionBase)){
				Long holder	= Long.parseLong(parameters.get(GeneralConstants.ID_HOLDER_PK).toString());	
				Long accountHolder	= Long.parseLong(parameters.get(GeneralConstants.ID_HOLDER_ACCOUNT_PK).toString());
				collectionRecord.setHolder(new Holder(holder));
				collectionRecord.setHolderAccount(new HolderAccount(accountHolder));
				collectionRecordDetail.setIdSecurityCodeFk(parameters.get(GeneralConstants.ID_SECURITY_CODE).toString());
			}
			
			 else if(BaseCollectionType.RECORD_CORPORATIVE_EVENTS.getCode().equals(collectionBase)){
				 if (billingService.getEntityCollection().equals(EntityCollectionType.PARTICIPANTS.getCode())){
						Long participant	= Long.parseLong(parameters.get(GeneralConstants.ID_PARTICIPANT_PK).toString());
						collectionRecord.setParticipant(new Participant(participant));
					}else if (billingService.getEntityCollection().equals(EntityCollectionType.HOLDERS.getCode())){
						Long holder	= Long.parseLong(parameters.get(GeneralConstants.ID_HOLDER_PK).toString());	
						collectionRecord.setHolder(new Holder(holder));
					}
			 }
			 else if(BaseCollectionType.ISSUANCE_CAT.getCode().equals(collectionBase) ){
				 if (billingService.getEntityCollection().equals(EntityCollectionType.PARTICIPANTS.getCode())){
						Long participant	= Long.parseLong(parameters.get(GeneralConstants.ID_PARTICIPANT_PK).toString());
						collectionRecord.setParticipant(new Participant(participant));
					}else if (billingService.getEntityCollection().equals(EntityCollectionType.HOLDERS.getCode())){
						Long holder	= Long.parseLong(parameters.get(GeneralConstants.ID_HOLDER_PK).toString());	
						collectionRecord.setHolder(new Holder(holder));
					}
			 }
			 else if(BaseCollectionType.REGISTRATION_PRECAUTIONARY_MEASURES.getCode().equals(collectionBase) ){
				 if (billingService.getEntityCollection().equals(EntityCollectionType.PARTICIPANTS.getCode())){
						Long participant	= Long.parseLong(parameters.get(GeneralConstants.ID_PARTICIPANT_PK).toString());
						collectionRecord.setParticipant(new Participant(participant));
					}else if (billingService.getEntityCollection().equals(EntityCollectionType.HOLDERS.getCode())){
						Long holder	= Long.parseLong(parameters.get(GeneralConstants.ID_HOLDER_PK).toString());	
						collectionRecord.setHolder(new Holder(holder));
					}
			 }
	
			
			collectionServiceMgmtServiceBean.create(collectionRecordDetail);

			
			/** ONLY IF SERVICE APPLY   TAXED **/
			if (billingService.getTaxApplied().equals(PropertiesConstants.YES_APPLY_TAXED)){
				
				 ParameterTable	parameterTableTax= billingServiceMgmtServiceBean.getParameterTableById(PropertiesConstants.TAX_PK);				
				 Double taxed	=	(parameterTableTax!=null)?parameterTableTax.getDecimalAmount1():new Double(GeneralConstants.ZERO_VALUE_STRING);
				
				BigDecimal taxedAmount=BillingUtils.multiplyDecimals(collectionRecord.getCollectionAmount(), new BigDecimal(taxed.toString()), GeneralConstants.ROUNDING_DIGIT_NUMBER);
				collectionRecord.setTaxApplied(taxedAmount);
						
				BigDecimal grossAmount = BillingUtils.addTwoDecimal(collectionRecord.getCollectionAmount(), taxedAmount, GeneralConstants.ROUNDING_DIGIT_NUMBER);
				
				/** SETTING FINALLY COLLECTION AMOUNT **/
				collectionRecord.setGrossAmount(grossAmount);
				
			}else{
				collectionRecord.setTaxApplied(new BigDecimal(GeneralConstants.ZERO_VALUE_STRING));
				collectionRecord.setGrossAmount(collectionRecord.getCollectionAmount());
			}
			
			collectionRecord.setEntityCollectionId(billingService.getEntityCollection());
			collectionRecord.setIndBilled(PropertiesConstants.NOT_BILLED);						
			collectionRecord.setRecordState(ProcessedServiceType.PROCESADO.getCode());
			collectionRecord.setCalculationDate(calculationDate);
			collectionRecord.setCollectionRecordDetail(listCollectionRecordDetail);
			collectionRecord.setProcessedService(processedService);
			collectionRecord.setOperationDate(CommonsUtilities.currentDateTime());
			collectionRecord.setCurrencyType(billingService.getCurrencyCalculation());
			collectionRecord.setCurrencyBilling(billingService.getCurrencyBilling());
			listCollectionRecord.add(collectionRecord);
			
			
			
			/**
			 * 
			 *   CREATE NEW PROCESSED SERVICE 
			 */
			processedService.setBillingService(billingService);
			processedService.setProcessedState(ProcessedServiceType.PROCESADO.getCode());
			processedService.setOperationDate(CommonsUtilities.currentDate());
			processedService.setCollectionRecords(listCollectionRecord);
			processedService.setCalculationDate(calculationDate);
			collectionServiceMgmtServiceBean.create(processedService);
						
		}


	}
	
	/**
	 * Get Amount To Change Owner.
	 *
	 * @param keySecurity the key security
	 * @param billingService the billing service
	 * @param dailyExchangeRateFilter the daily exchange rate filter
	 * @return the amount to change owner
	 * @throws ServiceException the service exception
	 */
	public BigDecimal getAmountToChangeOwner(List<Object[]> keySecurity, BillingService billingService, 
											DailyExchangeRateFilter dailyExchangeRateFilter) throws ServiceException{
		
		BigDecimal amountToChangeOwner=null;
		BigDecimal amount=null;
		BigDecimal buyPrice=null;
		Integer currency=null;
		if(Validations.validateListIsNotNullAndNotEmpty(keySecurity)){
			
			for (Object[] objects : keySecurity) {
				BigDecimal amountTemp=null;
				
				amount=objects[0]==null?BigDecimal.ZERO:new BigDecimal(objects[0].toString());
				currency=objects[1]==null?0:Integer.parseInt(objects[1].toString());
				buyPrice=businessLayerCommonFacade.gettingBuyPriceForCalculation(billingService.getCurrencyCalculation(),dailyExchangeRateFilter,currency);
				amountTemp=BillingUtils.multiplyDecimals(amount, buyPrice, GeneralConstants.ROUNDING_DIGIT_NUMBER);
				amountToChangeOwner=BillingUtils.addTwoDecimal(amountToChangeOwner, amountTemp, GeneralConstants.ROUNDING_DIGIT_NUMBER);
			}
		}
		
		return amountToChangeOwner;
	}
	
	/**
	 * Gets the amount to affectation.
	 *
	 * @param keySecurity the key security
	 * @param billingService the billing service
	 * @param dailyExchangeRateFilter the daily exchange rate filter
	 * @return the amount to affectation
	 * @throws ServiceException the service exception
	 */
	public BigDecimal getAmountToAffectation(List<Object[]> keySecurity, BillingService billingService, 
			DailyExchangeRateFilter dailyExchangeRateFilter) throws ServiceException{

		BigDecimal amountToChangeOwner=null;
		BigDecimal amount=null;
		BigDecimal buyPrice=null;
		Integer currency=null;
		if(Validations.validateListIsNotNullAndNotEmpty(keySecurity)){
		
			for (Object[] objects : keySecurity) {
				BigDecimal amountTemp=null;
				amount=objects[0]==null?BigDecimal.ZERO:new BigDecimal(objects[0].toString());
				currency=objects[1]==null?0:Integer.parseInt(objects[1].toString());
				buyPrice=businessLayerCommonFacade.gettingBuyPriceForCalculation(billingService.getCurrencyCalculation(),dailyExchangeRateFilter,currency);
				amountTemp=BillingUtils.multiplyDecimals(amount, buyPrice, GeneralConstants.ROUNDING_DIGIT_NUMBER);
				amountToChangeOwner=BillingUtils.addTwoDecimal(amountToChangeOwner, amountTemp, GeneralConstants.ROUNDING_DIGIT_NUMBER);
			}
		}
		
		return amountToChangeOwner;
	}
	
	
	/**
	 * Process Accumulative Scale.
	 *
	 * @param parameters the parameters
	 * @param amountDetail the amount detail
	 * @return the big decimal
	 * @throws ServiceException the service exception
	 */
	public BigDecimal processAccumulativeScale(Map parameters,BigDecimal amountDetail) throws ServiceException{
		ServiceRate serviceRate=(ServiceRate) parameters.get(GeneralConstants.SERVICE_RATE);
		BillingService billingService= (BillingService)parameters.get(GeneralConstants.BILLING_SERVICE);
		
		BigDecimal valueLimit = BigDecimal.ZERO;
		List<ServiceRateScale> serviceRateScales = serviceRate.getServiceRateScales();
		CollectionRecordDetailTo collectionRecordDetailTo=new CollectionRecordDetailTo();
		collectionRecordDetailTo.setPriceByQuantity(amountDetail);
		BigDecimal amount=null;
		
		ServiceRateScale serviceRateScaleSelected	=	null;
		/***
		 * Aqui compara los escalones, PRIMERA VALIDACION DE LOS ESCALONES
		 */
		for (Iterator<ServiceRateScale> iterScale = serviceRateScales.iterator(); iterScale.hasNext();){
			
			ServiceRateScale serviceRateScale = (ServiceRateScale)iterScale.next();
			
			valueLimit = new BigDecimal(collectionRecordDetailTo.getPriceByQuantity().toString());
			
			if ( (valueLimit.compareTo(serviceRateScale.getMinimumRange())>=0) &&
				 (valueLimit.compareTo(serviceRateScale.getMaximumRange())<=0) ){
				
				serviceRateScaleSelected = serviceRateScale;
				break;
			}
		}
		
		/**
		 * Si existe escalon, proseguir
		 */
		if (Validations.validateIsNotNull(serviceRateScaleSelected)){
			
			Iterator<ServiceRateScale> iterScale = serviceRateScales.iterator();
			
			
			amount=baseCollectionMgmtServiceFacade.getAccumulativeScaleByAmount(billingService, 
					collectionRecordDetailTo.getPriceByQuantity(), serviceRateScales, parameters);

			
		}
		
		return amount;
	}
	
	
	/**
	 * Prepare processed service for calculate.
	 *
	 * @return the list
	 */
	public List<ProcessedServiceTo> prepareProcessedServiceForCalculate()   {
		
		
		List<BillingService> 		billingServiceList		= null	;
		List<ProcessedServiceTo> 	processedServiceToList 	= null	;
		
		/** GETTING ALL BILLING SERVICE RELATED TO YOUR BASE COLLECTION**/
		try {
			
			/** aca debe seleccionar todos los diarios q est\u00E1n activos !*/
			billingServiceList = billingServiceMgmtServiceFacade.findBillingServiceByCalculationPeriod(PropertiesConstants.PERIOD_CALCULATION_DAILY_PK)	;	
		} catch (ServiceException e) {
			
		}
		
		if (Validations.validateListIsNullOrEmpty(billingServiceList)){
			throw  new RuntimeException("Not Found Service Daily  :"+ PropertiesConstants.PERIOD_CALCULATION_DAILY_PK );
		}else {  
			
			processedServiceToList = new ArrayList<ProcessedServiceTo>();
			
			ProcessedServiceTo  processedServiceTo	=null;
			BillingServiceTo	billingServiceTo	=null;
	
			for (BillingService billingService : billingServiceList) {
				
					processedServiceTo 	= new ProcessedServiceTo();
					billingServiceTo	= new BillingServiceTo();
					billingServiceTo.setIdBillingServicePk(billingService.getIdBillingServicePk());
					billingServiceTo.setBaseCollection(billingService.getBaseCollection());
					billingServiceTo.setCalculationPeriod(PropertiesConstants.PERIOD_CALCULATION_DAILY_PK);					
					processedServiceTo.setBillingServiceTo(billingServiceTo);
					
					processedServiceToList.add(processedServiceTo);
			}		
			
		}	
		
		return processedServiceToList;
	}	
	
	 
	
	/**
	 * For Affectation always Security same issuance.
	 *
	 * @param parameters the parameters
	 * @param dailyExchangeRateFilter the daily exchange rate filter
	 * @param keySecurity the key security
	 * @return the ting collection detail to affectation
	 * @throws ServiceException the service exception
	 */
	public CollectionRecordDetail gettingCollectionDetailToAffectation(
			Map parameters, DailyExchangeRateFilter dailyExchangeRateFilter, 
			List<Object[]> keySecurity) throws ServiceException{
		
		List<CollectionRecordTo> collectionRecordToList=null;
		CollectionRecordDetail collectionRecordDetail=null;
		CollectionRecordDetailTo collectionRecordDetailTo= new CollectionRecordDetailTo();
		BillingService billingService=(BillingService)parameters.get(GeneralConstants.BILLING_SERVICE.toString()) ;
		ServiceRate serviceRate=(ServiceRate)parameters.get(GeneralConstants.SERVICE_RATE.toString());

		BigDecimal referencePrice=null;
		
		BigDecimal sumQuantity=null;
		BigDecimal sumPrice=null;
		BigDecimal amountNegotiated=null;
		BigDecimal amountNegotiatedOrigin=null;
		Integer    	securityClass	=null;
		Long 		holder			=null;
		Long 		holderAccount	=null;
		
		if(Validations.validateListIsNotNullAndNotEmpty(keySecurity)){
			collectionRecordToList = new ArrayList<CollectionRecordTo>();
			for (Object[] object : keySecurity) {
				
				BigDecimal priceByQuantity=null;
				
				BigDecimal 	amountDetail	= ((BigDecimal)((Object[])object)[0]);
				Integer    	currency        = Integer.parseInt((((Object[])object)[1]).toString());
				BigDecimal 	nominalValue	= ((BigDecimal)((Object[])object)[2]);
				BigDecimal 	balance	   		= ((BigDecimal)((Object[])object)[3]);
			    String 		security		= ((Object[])object)[4].toString();
			     			securityClass	= Integer.parseInt((((Object[])object)[5]).toString());
			     			holder			= object[6]==null?new Long(0):Long.parseLong(((Object[])object)[6].toString());
			    
			    Object entity=null;
			    if (billingService.getEntityCollection().equals(EntityCollectionType.PARTICIPANTS.getCode())){
			    	entity	= Long.parseLong(parameters.get(GeneralConstants.ID_PARTICIPANT_PK).toString());
				}else if (billingService.getEntityCollection().equals(EntityCollectionType.HOLDERS.getCode())){
					entity	= Long.parseLong(parameters.get(GeneralConstants.ID_HOLDER_PK).toString());
				}else if(billingService.getEntityCollection().equals(EntityCollectionType.ISSUERS.getCode())){
					entity	= Long.parseLong(parameters.get(GeneralConstants.ID_ISSUER_PK).toString());
				}
				
			    
			    referencePrice=businessLayerCommonFacade.gettingBuyPriceForCalculation(serviceRate.getCurrencyRate(),
			    		dailyExchangeRateFilter,currency);
				
				/***ALL EXCHANGE RATE  -----> CONVERSION TO DOP  */
			    priceByQuantity =BillingUtils.multiplyDecimals(referencePrice, amountDetail , 
			    		GeneralConstants.ROUNDING_DIGIT_NUMBER);
				
				collectionRecordDetailTo= new CollectionRecordDetailTo();
				collectionRecordDetailTo.setIdSecurityCodeFk(security);
				collectionRecordDetailTo.setCurrencyRate(currency);
				collectionRecordDetailTo.setPriceByQuantity(priceByQuantity);
				collectionRecordDetailTo.setCalculationBalance(balance);
				collectionRecordDetailTo.setNominalValue(nominalValue);
				collectionRecordDetailTo.setHolderAccount(holder);
				collectionRecordDetailTo.setSecurityClass(securityClass);
				collectionRecordDetailTo.setOperationPrice(amountDetail);
				
				sumQuantity=BillingUtils.addTwoDecimal(sumQuantity, balance, GeneralConstants.ROUNDING_DIGIT_NUMBER);
				sumPrice =BillingUtils.addTwoDecimal(sumPrice, amountDetail, GeneralConstants.ROUNDING_DIGIT_NUMBER);
				amountNegotiatedOrigin=BillingUtils.addTwoDecimal(amountNegotiatedOrigin, amountDetail, GeneralConstants.ROUNDING_DIGIT_NUMBER);
				amountNegotiated=BillingUtils.addTwoDecimal(amountNegotiated, priceByQuantity, GeneralConstants.ROUNDING_DIGIT_NUMBER);
				
				
				//if !existe entity en collectionRecordToList then agregar y crear un detalle
				// else agregar detalle	
				CollectionRecordTo collectionRecordTo = findEntityAtListTemporalTo(collectionRecordToList, entity, billingService);
				if (collectionRecordTo==null){
					collectionRecordTo = new CollectionRecordTo();
					if(baseCollectionMgmtServiceBean.isPaymentByparticipant(billingService)){
						entity = baseCollectionMgmtServiceBean.getIdParticipantPkByIdIssuerPk(entity.toString());
						collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
					}
					else{
						collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
					}
					
					List<CollectionRecordDetailTo> collectionRecordDetailToList = new  ArrayList<CollectionRecordDetailTo>();
					collectionRecordDetailToList.add(collectionRecordDetailTo);	
					collectionRecordToList.add(collectionRecordTo);	
					collectionRecordTo.setCollectionRecordDetailToList(collectionRecordDetailToList);
				}else{
					collectionRecordTo.getCollectionRecordDetailToList().add(collectionRecordDetailTo);
				}
				
			}
			
			
			
			collectionRecordDetail=new CollectionRecordDetail();
			collectionRecordDetail.setCalculationBalance(sumQuantity);
			collectionRecordDetail.setOperationPrice(sumPrice);
			collectionRecordDetail.setNegotiatedAmount(amountNegotiated);
			collectionRecordDetail.setNegotiatedAmountOrigin(amountNegotiatedOrigin);
			collectionRecordDetail.setSecurityClass(securityClass);
			collectionRecordDetail.setIdHolder(holder);
			
			
		}
	
	
		
		return collectionRecordDetail;
	}
	
	/**
	 * Find Entity At List Temporal To.
	 *
	 * @param collectionRecordToList the collection record to list
	 * @param entity the entity
	 * @param typeEntity the type entity
	 * @return the collection record to
	 */
	protected  CollectionRecordTo findEntityAtListTemporalTo(List<CollectionRecordTo> collectionRecordToList, Object entity, BillingService billingService){
		
		if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordToList)){
				
			for (CollectionRecordTo collectionRecordTo : collectionRecordToList) {
				if(baseCollectionMgmtServiceBean.isPaymentByparticipant(billingService)){
					entity = baseCollectionMgmtServiceBean.getIdParticipantPkByIdIssuerPk(entity.toString());
				}
				if (collectionRecordTo.existEntityCollection(entity, billingService.getEntityCollection())){
					return collectionRecordTo;
				}
			}
		}
		
	 return null;
	
   }
	
	public List<Holder> findAllHolder(){
		return rateExceptionServiceBean.findAllHolder();
	}
	
	public List<Issuer> findAllIssuer(){
		return rateExceptionServiceBean.findAllIssuer();
	}

}

