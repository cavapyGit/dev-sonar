package com.pradera.billing.billingcollection.facade;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.billing.billingcollection.service.CollectionServiceMgmtServiceBean;
import com.pradera.billing.billingcollection.service.SenderBillingService;
import com.pradera.billing.billingcollection.to.ProcessedServiceTo;
import com.pradera.billing.billingservice.service.BillingServiceMgmtServiceBean;
import com.pradera.billing.billingsource.facade.SourceBillingServiceFacade;
import com.pradera.billing.billingsource.to.BillingServiceSourceInfo;
import com.pradera.billing.billingsource.to.BillingServiceSourceLoggerTo;
import com.pradera.billing.common.service.BusinessLayerCommonServiceBean;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.ProcessedService;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Project iDepositary. Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ServiceCalculationServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ServiceCalculationServiceFacade {

	/** The sender billing service. */
	@EJB
	private SenderBillingService senderBillingService;

	/** The transaction registry. */
	@Resource
	TransactionSynchronizationRegistry transactionRegistry;

	/** The collection service mgmt service facade. */
	@EJB
	CollectionServiceMgmtServiceFacade collectionServiceMgmtServiceFacade;

	/** The collection service mgmt service bean. */
	@EJB
	CollectionServiceMgmtServiceBean collectionServiceMgmtServiceBean;

	/** The base collection mgmt service facade. */
	@EJB
	BaseCollectionMgmtServiceFacade baseCollectionMgmtServiceFacade;

	/** The bussiness layer common service bean. */
	@EJB
	BusinessLayerCommonServiceBean bussinessLayerCommonServiceBean;

	/** The billing service mgmt service bean. */
	@EJB
	BillingServiceMgmtServiceBean billingServiceMgmtServiceBean;

	/** The source billing service facade. */
	@EJB
	SourceBillingServiceFacade sourceBillingServiceFacade;

	/** The session context. */
	@Resource
	SessionContext sessionContext;

	/** The log. */
	@Inject
	PraderaLogger log;

	/**
	 * Execute service calculation.
	 *
	 * @param processedServiceToList the processed service to list
	 */
	@ProcessAuditLogger(process = BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	public void executeServiceCalculation(List<ProcessedServiceTo> processedServiceToList) {
		LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

		List<ProcessedService> processedServiceList = null;
		for (ProcessedServiceTo processedServiceTo : processedServiceToList) {
			try {
				processedServiceList = collectionServiceMgmtServiceFacade.findProcessedServiceForRecalculate(processedServiceTo);
				if (Validations.validateListIsNotNullAndNotEmpty(processedServiceList)) {
					if (processedServiceList.size() == 1) {
						ProcessedService processedService = processedServiceList.get(0);
						processedServiceTo.setLastModifyDate(processedService.getLastModifyDate());
					}
				}
				senderBillingService.prepareServiceCalculate(processedServiceTo);
			} catch (ServiceException e2) {
				e2.printStackTrace();
			}
		}
	}

	/**
	 * Source billing test.
	 *
	 * @param billingServiceSearch the billing service search
	 */
	@ProcessAuditLogger(process = BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public void sourceBillingTest(BillingService billingServiceSearch) {

		Map<String, Object> parameters = new HashMap<String, Object>();

		Long codeProcess = null;
		String date = CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate());

		parameters.put("idBillingServicePk", billingServiceSearch.getIdBillingServicePk());
		parameters.put("calculationPeriod", 254);
		parameters.put("serviceCode", billingServiceSearch.getServiceCode());
		parameters.put("serviceName", billingServiceSearch.getServiceName());

		cargaArchivo(billingServiceSearch.getBillingServiceSource().getIdBillingServiceSourcePk());

//		if (Validations.validateIsNotNull(processedServiceTo.getIdProcessedServicePk())){			
//			parameters.put("idProcessedServicePk", processedServiceTo.getIdProcessedServicePk());			
//		}

//		if (processedServiceTo.getBillingServiceTo().getCalculationPeriod().equals(PropertiesConstants.PERIOD_CALCULATION_DAILY_PK)){
//						
//			parameters.put("calculationDate", date);
//			codeProcess = BusinessProcessType.DAILY_COLLECT_SERVICE_CALCULATION.getCode();
//		}else if(processedServiceTo.getBillingServiceTo().getCalculationPeriod().equals(PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK)){

		codeProcess = BusinessProcessType.MONTHLY_COLLECT_SERVICE_CALCULATION.getCode();
		parameters.put("calculationDate", date);
		parameters.put("year", 2014);
		parameters.put("month", 6);
//		}

		BusinessProcess process = collectionServiceMgmtServiceBean.find(BusinessProcess.class, codeProcess);

		/** SAVE ON DB FOR THAT CAN BE GETTING FOR BATCH **/
//		batchServiceBean.registerBatchTx("edvmdz", process, parameters);
//		em.flush();
	}

	/**
	 * Test To Load File to BillingServiceSource.
	 *
	 * @param codigoSource the codigo source
	 */
	public void cargaArchivo(Long codigoSource) {

		BillingServiceSourceInfo billingServiceSourceInfo = new BillingServiceSourceInfo();
		List<BillingServiceSourceLoggerTo> listBillingServiceSourceLoggerTo = new ArrayList<>();
		/**
		 * Aqui voy a poner la carga del archivo XMl en una clase
		 */
		try {
			billingServiceSourceInfo = sourceBillingServiceFacade.loadAccountingSourceInfoById(codigoSource);
			listBillingServiceSourceLoggerTo = sourceBillingServiceFacade.fillBillingServiceSourceLogger(null,
					billingServiceSourceInfo);
			sourceBillingServiceFacade.processForBillingSource(new HashMap<>(), listBillingServiceSourceLoggerTo);
			System.out.println();
		} catch (ServiceException e) {

			e.printStackTrace();
		}

	}

}
