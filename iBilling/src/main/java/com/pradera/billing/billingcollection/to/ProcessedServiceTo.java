package com.pradera.billing.billingcollection.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.pradera.billing.billingservice.to.BillingServiceTo;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.issuancesecuritie.Issuer;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ProcessedServiceTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class ProcessedServiceTo implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2538935918242852614L;

	/** The id processed service pk. */
	private Long 						idProcessedServicePk;

	/** The calculation date. */
	private Date 						calculationDate;

	/** The operation date. */
	private Date 						operationDate;

	/** The reference price. */
	private BigDecimal 					referencePrice;

	/** The source type. */
	private Integer 					sourceType;

	/** The month process. */
	private Integer 					monthProcess;

	/** The processed state. */
	private Integer 					processedState;

	/** The year process. */
	private Integer 					yearProcess;
	
	/** The initial date. */
	private Date 						initialDate;
	
	/** The final date. */
	private Date 						finalDate;

	/** The billing service to. */
	private BillingServiceTo 			billingServiceTo;

	/** The collection records to. */
	private List<CollectionRecordTo> 	collectionRecordsTo;

	/** The collection record to search. */
	private CollectionRecordTo 			collectionRecordToSearch;

	/** The description processed state. */
	private String 						descriptionProcessedState;

	/** The number of collections. */
	private Integer 					numberOfCollections;
	
	/** The collection amount. */
	private BigDecimal 					collectionAmount;
	
	/** The gross amount. */
	private BigDecimal 					grossAmount;
	
	/** The tax applied. */
	private BigDecimal 					taxApplied;
	
	/** The collection amount accumulated. */
	private BigDecimal 					collectionAmountAccumulated;
	
	/** The gross amount accumulated. */
	private BigDecimal 					grossAmountAccumulated;
		
	/** The tax applied accumulated. */
	private BigDecimal 					taxAppliedAccumulated;

	/** The count num collection. */
	private Long 						countNumCollection;

	/** The last modify app. */
	private Integer 					lastModifyApp;

	/** The last modify date. */
	private Date 						lastModifyDate;

	/** The last modify ip. */
	private String 						lastModifyIp;

	/** The last modify user. */
	private String 						lastModifyUser;
	
	/** The other error type. */
	private Boolean 					otherErrorType	= Boolean.FALSE;
	
	/** The error type. */
	private ErrorServiceType 			errorType;
	
	/** The ind billed. */
	private Integer 					indBilled;
	
	/** The logger user. */
	private LoggerUser 					loggerUser;
	

	private List<Participant> listParticipant;
	private List<Holder> listHolder;
	private List<Issuer> listIssuer;
	
	/**
	 * Instantiates a new processed service to.
	 */
	public ProcessedServiceTo() {
		
		this.collectionRecordToSearch = new CollectionRecordTo();
		
	}
	
	/**
	 * Instantiates a new processed service to.
	 *
	 * @param errorType the error type
	 */
	public ProcessedServiceTo(ErrorServiceType errorType) {
		
		this.collectionRecordToSearch = new CollectionRecordTo();	
		this.errorType = errorType;
		
	}
	
	/**
	 * Gets the id processed service pk.
	 *
	 * @return the id processed service pk
	 */
	public Long getIdProcessedServicePk() {
		return idProcessedServicePk;
	}

	/**
	 * Sets the id processed service pk.
	 *
	 * @param idProcessedServicePk the new id processed service pk
	 */
	public void setIdProcessedServicePk(Long idProcessedServicePk) {
		this.idProcessedServicePk = idProcessedServicePk;
	}

	/**
	 * Gets the calculation date.
	 *
	 * @return the calculation date
	 */
	public Date getCalculationDate() {
		return calculationDate;
	}

	/**
	 * Sets the calculation date.
	 *
	 * @param calculationDate the new calculation date
	 */
	public void setCalculationDate(Date calculationDate) {
		this.calculationDate = calculationDate;
	}

	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return operationDate;
	}

	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}
	
	/**
	 * Gets the month process.
	 *
	 * @return the month process
	 */
	public Integer getMonthProcess() {
		return monthProcess;
	}

	/**
	 * Sets the month process.
	 *
	 * @param monthProcess the new month process
	 */
	public void setMonthProcess(Integer monthProcess) {
		this.monthProcess = monthProcess;
	}

	/**
	 * Gets the processed state.
	 *
	 * @return the processed state
	 */
	public Integer getProcessedState() {
		return processedState;
	}

	/**
	 * Sets the processed state.
	 *
	 * @param processedState the new processed state
	 */
	public void setProcessedState(Integer processedState) {
		this.processedState = processedState;
	}

	/**
	 * Gets the year process.
	 *
	 * @return the year process
	 */
	public Integer getYearProcess() {
		return yearProcess;
	}

	/**
	 * Sets the year process.
	 *
	 * @param yearProcess the new year process
	 */
	public void setYearProcess(Integer yearProcess) {
		this.yearProcess = yearProcess;
	}

	/**
	 * Gets the billing service to.
	 *
	 * @return the billing service to
	 */
	public BillingServiceTo getBillingServiceTo() {
		return billingServiceTo;
	}

	/**
	 * Sets the billing service to.
	 *
	 * @param billingServiceTo the new billing service to
	 */
	public void setBillingServiceTo(BillingServiceTo billingServiceTo) {
		this.billingServiceTo = billingServiceTo;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	/**
	 * Gets the collection records to.
	 *
	 * @return the collection records to
	 */
	public List<CollectionRecordTo> getCollectionRecordsTo() {
		return collectionRecordsTo;
	}

	/**
	 * Sets the collection records to.
	 *
	 * @param collectionRecordsTo the new collection records to
	 */
	public void setCollectionRecordsTo(
			List<CollectionRecordTo> collectionRecordsTo) {
		this.collectionRecordsTo = collectionRecordsTo;
	}

	/**
	 * Gets the collection record to search.
	 *
	 * @return the collection record to search
	 */
	public CollectionRecordTo getCollectionRecordToSearch() {
		return collectionRecordToSearch;
	}

	/**
	 * Sets the collection record to search.
	 *
	 * @param collectionRecordToSearch the new collection record to search
	 */
	public void setCollectionRecordToSearch(
			CollectionRecordTo collectionRecordToSearch) {
		this.collectionRecordToSearch = collectionRecordToSearch;
	}

	/**
	 * Gets the description processed state.
	 *
	 * @return the description processed state
	 */
	public String getDescriptionProcessedState() {
		return descriptionProcessedState;
	}

	/**
	 * Sets the description processed state.
	 *
	 * @param descriptionProcessedState the new description processed state
	 */
	public void setDescriptionProcessedState(String descriptionProcessedState) {
		this.descriptionProcessedState = descriptionProcessedState;
	}

	/**
	 * Gets the number of collections.
	 *
	 * @return the number of collections
	 */
	public Integer getNumberOfCollections() {
		return numberOfCollections;
	}

	/**
	 * Sets the number of collections.
	 *
	 * @param numberOfCollections the new number of collections
	 */
	public void setNumberOfCollections(Integer numberOfCollections) {
		this.numberOfCollections = numberOfCollections;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the collection amount.
	 *
	 * @return the collection amount
	 */
	public BigDecimal getCollectionAmount() {
		return collectionAmount;
	}

	/**
	 * Sets the collection amount.
	 *
	 * @param collectionAmount the new collection amount
	 */
	public void setCollectionAmount(BigDecimal collectionAmount) {
		this.collectionAmount = collectionAmount;
	}

	/**
	 * Gets the gross amount.
	 *
	 * @return the gross amount
	 */
	public BigDecimal getGrossAmount() {
		return grossAmount;
	}

	/**
	 * Sets the gross amount.
	 *
	 * @param grossAmount the new gross amount
	 */
	public void setGrossAmount(BigDecimal grossAmount) {
		this.grossAmount = grossAmount;
	}

	/**
	 * Gets the tax applied.
	 *
	 * @return the tax applied
	 */
	public BigDecimal getTaxApplied() {
		return taxApplied;
	}

	/**
	 * Sets the tax applied.
	 *
	 * @param taxApplied the new tax applied
	 */
	public void setTaxApplied(BigDecimal taxApplied) {
		this.taxApplied = taxApplied;
	}

	/**
	 * Gets the collection amount accumulated.
	 *
	 * @return the collection amount accumulated
	 */
	public BigDecimal getCollectionAmountAccumulated() {
		return collectionAmountAccumulated;
	}

	/**
	 * Sets the collection amount accumulated.
	 *
	 * @param collectionAmountAccumulated the new collection amount accumulated
	 */
	public void setCollectionAmountAccumulated(
			BigDecimal collectionAmountAccumulated) {
		this.collectionAmountAccumulated = collectionAmountAccumulated;
	}

	/**
	 * Gets the gross amount accumulated.
	 *
	 * @return the gross amount accumulated
	 */
	public BigDecimal getGrossAmountAccumulated() {
		return grossAmountAccumulated;
	}

	/**
	 * Sets the gross amount accumulated.
	 *
	 * @param grossAmountAccumulated the new gross amount accumulated
	 */
	public void setGrossAmountAccumulated(BigDecimal grossAmountAccumulated) {
		this.grossAmountAccumulated = grossAmountAccumulated;
	}

	/**
	 * Gets the tax applied accumulated.
	 *
	 * @return the tax applied accumulated
	 */
	public BigDecimal getTaxAppliedAccumulated() {
		return taxAppliedAccumulated;
	}

	/**
	 * Sets the tax applied accumulated.
	 *
	 * @param taxAppliedAccumulated the new tax applied accumulated
	 */
	public void setTaxAppliedAccumulated(BigDecimal taxAppliedAccumulated) {
		this.taxAppliedAccumulated = taxAppliedAccumulated;
	}

	/**
	 * Gets the reference price.
	 *
	 * @return the reference price
	 */
	public BigDecimal getReferencePrice() {
		return referencePrice;
	}

	/**
	 * Sets the reference price.
	 *
	 * @param referencePrice the new reference price
	 */
	public void setReferencePrice(BigDecimal referencePrice) {
		this.referencePrice = referencePrice;
	}

	/**
	 * Gets the source type.
	 *
	 * @return the source type
	 */
	public Integer getSourceType() {
		return sourceType;
	}

	/**
	 * Sets the source type.
	 *
	 * @param sourceType the new source type
	 */
	public void setSourceType(Integer sourceType) {
		this.sourceType = sourceType;
	}

	/**
	 * Gets the count num collection.
	 *
	 * @return the count num collection
	 */
	public Long getCountNumCollection() {
		return countNumCollection;
	}

	/**
	 * Sets the count num collection.
	 *
	 * @param countNumCollection the new count num collection
	 */
	public void setCountNumCollection(Long countNumCollection) {
		this.countNumCollection = countNumCollection;
	} 

	/**
	 * Gets the other error type.
	 *
	 * @return the other error type
	 */
	public Boolean getOtherErrorType() {
		return otherErrorType;
	}

	/**
	 * Sets the other error type.
	 *
	 * @param otherErrorType the new other error type
	 */
	public void setOtherErrorType(Boolean otherErrorType) {
		this.otherErrorType = otherErrorType;
	} 

	/**
	 * Gets the error type.
	 *
	 * @return the error type
	 */
	public ErrorServiceType getErrorType() {
		return errorType;
	}

	/**
	 * Sets the error type.
	 *
	 * @param errorType the new error type
	 */
	public void setErrorType(ErrorServiceType errorType) {
		this.errorType = errorType;
	}

	/**
	 * Gets the ind billed.
	 *
	 * @return the ind billed
	 */
	public Integer getIndBilled() {
		return indBilled;
	}

	/**
	 * Sets the ind billed.
	 *
	 * @param indBilled the new ind billed
	 */
	public void setIndBilled(Integer indBilled) {
		this.indBilled = indBilled;
	}

	/**
	 * Gets the logger user.
	 *
	 * @return the logger user
	 */
	public LoggerUser getLoggerUser() {
		return loggerUser;
	}

	/**
	 * Sets the logger user.
	 *
	 * @param loggerUser the new logger user
	 */
	public void setLoggerUser(LoggerUser loggerUser) {
		this.loggerUser = loggerUser;
	}

	/**
	 * @return the listParticipant
	 */
	public List<Participant> getListParticipant() {
		return listParticipant;
	}

	/**
	 * @param listParticipant the listParticipant to set
	 */
	public void setListParticipant(List<Participant> listParticipant) {
		this.listParticipant = listParticipant;
	}

	/**
	 * @return the listHolder
	 */
	public List<Holder> getListHolder() {
		return listHolder;
	}

	/**
	 * @param listHolder the listHolder to set
	 */
	public void setListHolder(List<Holder> listHolder) {
		this.listHolder = listHolder;
	}

	/**
	 * @return the listIssuer
	 */
	public List<Issuer> getListIssuer() {
		return listIssuer;
	}

	/**
	 * @param listIssuer the listIssuer to set
	 */
	public void setListIssuer(List<Issuer> listIssuer) {
		this.listIssuer = listIssuer;
	}

	
	
}
