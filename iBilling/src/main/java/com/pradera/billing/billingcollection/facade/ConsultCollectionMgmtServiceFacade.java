package com.pradera.billing.billingcollection.facade;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.billing.billingcollection.service.ConsultCollectionMgmtServiceBean;
import com.pradera.billing.billingcollection.to.CollectionMoreDetailTo;
import com.pradera.billing.billingcollection.to.CollectionRecordTo;
import com.pradera.billing.billingcollection.to.ProcessedServiceTo;
import com.pradera.billing.billingrate.to.ServiceRateTo;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.billing.CollectionRecord;
import com.pradera.model.billing.CollectionRecordDetail;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ConsultCollectionMgmtServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ConsultCollectionMgmtServiceFacade {
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry 	transactionRegistry;

	/** The consult collection mgmt service bean. */
	@EJB 
	ConsultCollectionMgmtServiceBean 	consultCollectionMgmtServiceBean;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/**
	 * Consult by Services.
	 *
	 * @param filter the filter
	 * @return the search list collection record to to consult by services
	 * @throws ServiceException the service exception
	 */
	public  List<CollectionRecordTo> getSearchListCollectionRecordToToConsultByServices(CollectionRecordTo filter)throws ServiceException{
		 
		 List<CollectionRecordTo> collectionRecordToList=consultCollectionMgmtServiceBean.getSearchListCollectionRecordToToConsultByServices(filter);
		 if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordToList)){
				for (CollectionRecordTo collectionRecordTo : collectionRecordToList) {
					
					List<ServiceRateTo> serviceRateToList=consultCollectionMgmtServiceBean.getServiceRateToListForCollection(collectionRecordTo);
					collectionRecordTo.setServiceRateToList(serviceRateToList);

					processMoreDetailCollection(collectionRecordTo);
				}
		}
		 
		 return collectionRecordToList;
	}
	
	/**
	 * Consult By entity Collection.
	 *
	 * @param filter the filter
	 * @return the search list collection record to to consult by entity collection
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@TransactionTimeout(unit=TimeUnit.HOURS,value=3)
	@Performance
	public  List<ProcessedServiceTo> getSearchListCollectionRecordToToConsultByEntityCollection(CollectionRecordTo filter)throws ServiceException{
		 
		List<ProcessedServiceTo> processedServiceToList=consultCollectionMgmtServiceBean.getSearchListProcessedServiceToToConsultByEntityCollection(filter);
		
		 if (Validations.validateListIsNotNullAndNotEmpty(processedServiceToList)){
				for (ProcessedServiceTo processedServiceTo : processedServiceToList) {
					for(CollectionRecordTo collectionRecordTo :processedServiceTo.getCollectionRecordsTo()){
						
						List<ServiceRateTo> serviceRateToList=consultCollectionMgmtServiceBean.getServiceRateToListForCollection(collectionRecordTo);
						collectionRecordTo.setServiceRateToList(serviceRateToList);
						
						processMoreDetailCollection(collectionRecordTo);
					}
																
				}
		}
		 
		 return processedServiceToList;
	}
	
	/**
	 * Gets the collection record detail.
	 *
	 * @param filter the filter
	 * @return the collection record detail
	 * @throws ServiceException the service exception
	 */
	public CollectionRecordDetail getCollectionRecordDetail(CollectionRecord  filter) throws ServiceException{
		
		return consultCollectionMgmtServiceBean.getCollectionRecordDetail(filter);
		
	}
	
	/**
	 * Process more detail collection.
	 *
	 * @param collectionRecordTo the collection record to
	 * @throws ServiceException the service exception
	 */
	public void processMoreDetailCollection(CollectionRecordTo collectionRecordTo ) throws ServiceException{
		
		for (ServiceRateTo serviceRateTo : collectionRecordTo.getServiceRateToList()) {
			List<CollectionMoreDetailTo> collectionRecordDetailToList=consultCollectionMgmtServiceBean.getCollectionRecordDetailForCollection(collectionRecordTo);
			serviceRateTo.setMoreCollectionDetail(collectionRecordDetailToList);
		}
	}
	
	
	
	

}
