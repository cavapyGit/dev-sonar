package com.pradera.billing.billingcollection.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.billing.billingcollection.facade.BaseCollectionMgmtServiceFacade;
import com.pradera.billing.billingcollection.to.CollectionRecordTo;
import com.pradera.billing.billingcollection.to.ProcessedServiceTo;
import com.pradera.billing.billingexception.invoice.to.BillingEntityExceptionTo;
import com.pradera.billing.billingexception.service.RateExceptionServiceBean;
import com.pradera.billing.billingrate.facade.BillingRateMgmtServiceFacade;
import com.pradera.billing.billingservice.service.BillingServiceMgmtServiceBean;
import com.pradera.billing.billingservice.to.BillingServiceTo;
import com.pradera.billing.common.service.BusinessLayerCommonServiceBean;
import com.pradera.billing.common.utils.BillingUtils;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.framework.entity.service.LoaderEntityServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.billing.BillingEntityException;
import com.pradera.model.billing.CollectionRecord;
import com.pradera.model.billing.CollectionRecordDetail;
import com.pradera.model.billing.ExceptionInvoiceGenerations;
import com.pradera.model.billing.ProcessedService;
import com.pradera.model.billing.type.EntityCollectionType;
import com.pradera.model.issuancesecuritie.Issuer;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CollectionServiceMgmtServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class CollectionServiceMgmtServiceBean extends CrudDaoServiceBean {

	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry 	transactionRegistry;
	
	/** The collection service mgmt service bean. */
	@EJB
	CollectionServiceMgmtServiceBean   	collectionServiceMgmtServiceBean;
	
	/** The bussiness layer common service bean. */
	@EJB
	BusinessLayerCommonServiceBean	   	bussinessLayerCommonServiceBean;
	
	/** The billing service mgmt service bean. */
	@EJB
	BillingServiceMgmtServiceBean		billingServiceMgmtServiceBean;
	
	/** The base collection mgmt service facade. */
	@EJB
	BaseCollectionMgmtServiceFacade		baseCollectionMgmtServiceFacade;
	
	/** The billing rate mgmt service facade. */
	@EJB
	BillingRateMgmtServiceFacade 		billingRateMgmtServiceFacade;
	
	/** The rate exception service bean. */
	@EJB
	RateExceptionServiceBean 			rateExceptionServiceBean;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade 				helperComponentFacade;

	/** The loader entity service. */
	@EJB
	LoaderEntityServiceBean 			loaderEntityService;
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	private List<Participant> listParticipant;
	private List<Holder> listHolder;
	private List<Issuer> listIssuer;
	
//	/** The em. */
//	@Inject @DepositaryDataBase
//	protected EntityManager em;
	
	
	/**
	 * Find Billing Service For Service Calculate.
	 *
	 * @param billingServiceTo the billing service to
	 * @return List<BillingServiceTo>
	 * @throws ServiceException the service exception
	 */
	public List<BillingServiceTo> findBillingServiceToForServiceCalculate(BillingServiceTo billingServiceTo)throws ServiceException{
		
		    StringBuilder sbQuery = new StringBuilder();
		    sbQuery.append("    SELECT bs.id_billing_service_pk,"); //0
		    sbQuery.append("           bs.service_code,          " );//1
			sbQuery.append("  		   ( select pt.description from parameter_table pt where bs.STATE_SERVICE=pt.parameter_table_pk ) as estado , " );
			sbQuery.append("           ( select pt.description from parameter_table pt where bs.calculation_period=pt.parameter_table_pk ) as descriptionCalculationPeriod , " );
			sbQuery.append(" 		   bs.service_name as serviceName ," );//4
			sbQuery.append("  		   ( select pt.description from parameter_table pt where bs.base_collection=pt.parameter_table_pk ) as descriptionBaseCollection , " );
			sbQuery.append("  		   bs.initial_effective_date,");//6
			sbQuery.append(" 		   bs.end_effective_date ," );//
			sbQuery.append("  		   ( select pt.description from parameter_table pt where bs.CLIENT_SERVICE_TYPE=pt.parameter_table_pk ) as descriptionClient , " );
			sbQuery.append("  		   ( select pt.description from parameter_table pt where bs.ENTITY_COLLECTION=pt.parameter_table_pk ) as descriptionEntity ," );
			sbQuery.append("  		   bs.CURRENCY_BILLING  as currencyBilling, " );//10
			sbQuery.append("           bs.BASE_COLLECTION ,  " );//11
			sbQuery.append("           bs.CLIENT_SERVICE_TYPE ,  " );//12
			sbQuery.append("           bs.ENTITY_COLLECTION , " );//13
			sbQuery.append("           bs.CALCULATION_PERIOD,  " );//14
			sbQuery.append("           bs.IND_SOURCE_BASE_COLLECTION,  " );//15
			sbQuery.append("           bs.ID_BILLING_SERVICE_SOURCE_FK,  " );//16
			sbQuery.append("           bs.CURRENCY_CALCULATION,  " );//17
			sbQuery.append("           bs.SOURCE_INFORMATION  " );//18
			sbQuery.append("  FROM billing_service  bs " );
			sbQuery.append("  WHERE  ( bs.STATE_SERVICE="+PropertiesConstants.SERVICE_STATUS_ACTIVE_PK+"  " );
			sbQuery.append("  and  bs.service_type="+PropertiesConstants.SERVICE_TYPE_AUTOMATIC_PK +" " ); 
			sbQuery.append("  and  bs.CALCULATION_PERIOD=:periodCalculation )    " );	
			
			
			if(Validations.validateIsNotNullAndPositive(billingServiceTo.getBaseCollection())){
				sbQuery.append(" and ( bs.BASE_COLLECTION=:baseCollection ) ");
			}		  			 	
		   Query query = em.createNativeQuery(sbQuery.toString());
		   

		   if(Validations.validateIsNotNullAndPositive(billingServiceTo.getBaseCollection())){
			   query.setParameter("baseCollection",billingServiceTo.getBaseCollection());
			}
		   
		   /**Default*/
		   if(Validations.validateIsNotNullAndPositive(billingServiceTo.getCalculationPeriod())){
			   query.setParameter("periodCalculation",billingServiceTo.getCalculationPeriod());
			}
		    		   	
		   
		   List<Object[]>  objectList = query.getResultList();
		   List<BillingServiceTo> billingServiceToList = null;
		   if (Validations.validateListIsNotNullAndNotEmpty(objectList)){
			   billingServiceToList = new ArrayList<BillingServiceTo>();
			   
			   for(int i=0;i<objectList.size();++i){   
			   //Getting Results from data base
			   Object[] sResults = (Object[])objectList.get(i);
			   BillingServiceTo billingServiceNew = new BillingServiceTo();
			 
			   billingServiceNew.setIdBillingServicePk(Long.parseLong(sResults[0].toString()));
			   billingServiceNew.setServiceCode( sResults[1].toString());
			   billingServiceNew.setDescriptionStateBillingService(sResults[2].toString());
			   billingServiceNew.setDescriptionCalculationPeriod(sResults[3].toString());			   
			   billingServiceNew.setServiceName(sResults[4].toString());
			   billingServiceNew.setDescriptionBaseCollection(Validations.validateIsNotNull(sResults[5])? sResults[5].toString(): "");
			   billingServiceNew.setDescriptionClientServiceType(sResults[8].toString());
			   billingServiceNew.setDescriptionCollectionEntity(sResults[9].toString());
			   billingServiceNew.setCurrencyBilling(sResults[10]==null?0:Integer.parseInt(sResults[10].toString()));
			   billingServiceNew.setBaseCollection(sResults[11]==null?0:Integer.parseInt(sResults[11].toString()));
			   billingServiceNew.setClientServiceType(sResults[12]==null?0:Integer.parseInt(sResults[12].toString()));
			   billingServiceNew.setEntityCollection(sResults[13]==null?0:Integer.parseInt(sResults[13].toString()));
			   billingServiceNew.setCalculationPeriod(sResults[14]==null?0:Integer.parseInt(sResults[14].toString()));
			   billingServiceNew.setIndSource(sResults[15]==null?0:Integer.parseInt(sResults[15].toString()));
			   billingServiceNew.setIdSourcePk(sResults[16]==null?0L:Long.parseLong(sResults[16].toString()));
			   billingServiceNew.setCurrencyCalculation(sResults[17]==null?0:Integer.parseInt(sResults[17].toString()));
			   billingServiceNew.setSourceInformation(sResults[18]==null?0:Integer.parseInt(sResults[18].toString()));
			   billingServiceToList.add(billingServiceNew);
			  }
		   }
		
		   
		  return billingServiceToList;
	  	
	}
	
	
	
	/**
	 *  
	 *  Method create a new  ProcessedService.
	 *
	 * @param processedService the processed service
	 * @return the processed service
	 * @throws ServiceException the service exception
	 */
	public ProcessedService saveProcessedService(ProcessedService processedService) throws ServiceException{
				
		this.create(processedService);
		return processedService;
	}


	/**
	 * Save collection record.
	 *
	 * @param collectionRecord the collection record
	 * @return the collection record
	 * @throws ServiceException the service exception
	 */
	public CollectionRecord saveCollectionRecord(CollectionRecord collectionRecord) throws ServiceException{
	
		this.create(collectionRecord);
		return collectionRecord;
	}
	
	/**
	 * Save collection record details.
	 *
	 * @param collectionRecordDetail the collection record detail
	 * @return the collection record detail
	 * @throws ServiceException the service exception
	 */
	public CollectionRecordDetail saveCollectionRecordDetails(CollectionRecordDetail collectionRecordDetail) throws ServiceException{
		
		this.create(collectionRecordDetail);
		return collectionRecordDetail;
	}
	

	/**
	 *  Find ProcessedService for table search collectionManual.
	 *
	 * @param filter the filter
	 * @return the search processed service for collection manual
	 * @throws ServiceException the service exception
	 */

	public List<Object[]> getSearchProcessedServiceForCollectionManual(ProcessedServiceTo filter)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		  sbQuery.append(" select   ps.ID_PROCESSED_SERVICE_PK,  ");//0
		  sbQuery.append("   ps.PROCESSED_STATE, ");//1
		  sbQuery.append("   ps.OPERATION_DATE,  ");//2
		  sbQuery.append("  (select  count(1)    ");
		  sbQuery.append("    from processed_service pst ");
		  sbQuery.append("  inner join collection_record cr on cr.ID_PROCESSED_SERVICE_FK= pst.ID_PROCESSED_SERVICE_PK   ");
		  sbQuery.append("   where pst.id_billing_service_fk=ps.id_billing_service_fk   ");
		  sbQuery.append("   group by pst.id_billing_service_fk  ");
		  sbQuery.append("   ) as servicios_cobrados,  ");//3
		  sbQuery.append("   (select pt.parameter_name from parameter_table pt where  ps.PROCESSED_STATE=pt.parameter_table_pk) as description_processed_state, ");//4
		  sbQuery.append(" ps.id_billing_service_fk,   ");//5
		  sbQuery.append(" bs.SERVICE_NAME, ");//6
		  sbQuery.append(" bs.SERVICE_CODE,   ");//7
		  sbQuery.append(" bs.entity_collection,  ");//8
		  sbQuery.append(" CASE  WHEN bs.ENTITY_COLLECTION=1406 THEN (SELECT BUSINESS_NAME FROM ISSUER WHERE ID_ISSUER_PK=cr.ID_ISSUER_FK )  ");
		  sbQuery.append(" 	WHEN bs.ENTITY_COLLECTION=1407 THEN (SELECT DESCRIPTION from PARTICIPANT WHERE ID_PARTICIPANT_PK=CR.ID_PARTICIPANT_FK )  ");
		  sbQuery.append(" 	WHEN bs.ENTITY_COLLECTION=1408 THEN (SELECT full_name FROM HOLDER WHERE ID_HOLDER_PK=CR.ID_HOLDER_FK  )  ");
		  sbQuery.append("	WHEN BS.ENTITY_COLLECTION=1530 THEN  ");
		  sbQuery.append(" 		(select cr.other_entity_document|| '-' ||cr.other_entity_description from collection_record cr   where cr.ID_PROCESSED_SERVICE_FK=ps.ID_PROCESSED_SERVICE_PK)  ");
		  sbQuery.append(" 	WHEN BS.ENTITY_COLLECTION=1405 THEN  (select pt.parameter_name from parameter_table pt where bs.ENTITY_COLLECTION=pt.parameter_table_pk)  ");
		  sbQuery.append("	WHEN BS.ENTITY_COLLECTION=251 THEN  (select pt.parameter_name from parameter_table pt where bs.ENTITY_COLLECTION=pt.parameter_table_pk)  ");
		  sbQuery.append(" END AS name_entity_collection,  ");//9
		  sbQuery.append(" (select pt.parameter_name from parameter_table pt where bs.ENTITY_COLLECTION=pt.parameter_table_pk) as description_entity,  ");//10
		  sbQuery.append("  bs.CLIENT_SERVICE_TYPE,  ");//11
		  sbQuery.append(" (select pt.parameter_name from parameter_table pt where  bs.CLIENT_SERVICE_TYPE=pt.parameter_table_pk) as description_client,  ");//12
		  sbQuery.append(" bs.INITIAL_EFFECTIVE_DATE  ,  ");//13
		  sbQuery.append(" bs.END_EFFECTIVE_DATE,  ");//14
		  sbQuery.append(" bs.STATE_SERVICE,  ");//15
		  sbQuery.append("  (select pt.parameter_name from parameter_table pt where  bs.STATE_SERVICE=pt.parameter_table_pk) as description_billing_service,  ");//16
		  sbQuery.append(" bs.SERVICE_TYPE,  ");//17
		  sbQuery.append(" (select pt.parameter_name from parameter_table pt where  bs.SERVICE_TYPE=pt.parameter_table_pk) as description_service_type,   ");//18
		  sbQuery.append(" cr.COLLECTION_AMOUNT,  ");//19
		  sbQuery.append(" cr.GROSS_AMOUNT,  ");//20
		  sbQuery.append(" cr.TAX_APPLIED,   ");//21
		  
		  sbQuery.append("  cr.record_state,   ");//22
		  sbQuery.append(" (select pt.description from parameter_table pt where  cr.record_state=pt.parameter_table_pk) as description_record_state,   ");//23
		  sbQuery.append(" cr.IND_BILLED,   ");//24
		  sbQuery.append("  CASE  WHEN  CR.IND_BILLED=0  THEN 'NO FACTURADO'");
		  sbQuery.append(" WHEN  CR.IND_BILLED=1  THEN 'FACTURADO' END  AS ESTATUS_BILLING,");//25
		  sbQuery.append(" cr.CALCULATION_DATE as CALCULATION_DATE_COLLECTION,  ");//26
		  sbQuery.append(" cr.OPERATION_DATE as OPERATION_DATE_COLLECTION, ");//27
		  sbQuery.append(" cr.ID_COLLECTION_RECORD_PK ");//28
	      
		  sbQuery.append(" from processed_service ps   ");
		  sbQuery.append(" inner join billing_service bs on ps.ID_BILLING_SERVICE_FK =  bs.id_billing_service_pk  ");
		  sbQuery.append(" inner join collection_record cr on cr.ID_PROCESSED_SERVICE_FK= ps.ID_PROCESSED_SERVICE_PK   ");
		  sbQuery.append(" where 1=1   ");
		  if(Validations.validateIsNotNullAndNotEmpty(filter.getProcessedState())){
				sbQuery.append(" and ps.PROCESSED_STATE= :processedStatePrm");
				
			}
		  if(Validations.validateIsNotNull(filter.getBillingServiceTo())){
		    if(Validations.validateIsNotNullAndNotEmpty(filter.getBillingServiceTo().getIdBillingServicePk())){
				sbQuery.append(" and ps.id_billing_service_fk= :idBillingServicePKPrm");
			}
		    if(Validations.validateIsNotNullAndNotEmpty(filter.getBillingServiceTo().getServiceCode())){
				sbQuery.append(" and bs.SERVICE_CODE= :serviceCodePrm");
			}
		    if(Validations.validateIsNotNullAndNotEmpty(filter.getBillingServiceTo().getServiceType())){
				sbQuery.append(" and bs.SERVICE_TYPE= :serviceTypePrm");
			}
		    
		    
		    }
		  if(Validations.validateIsNotNull(filter.getInitialDate())&&Validations.validateIsNotNull(filter.getFinalDate())){
			  sbQuery.append(" and ps.operation_date BETWEEN :initialDatePrm and :finalDatePrm");
		  }
		  sbQuery.append(" order by ps.id_billing_service_fk   ");
		  Query query = em.createNativeQuery(sbQuery.toString());
		 
		  if(Validations.validateIsNotNullAndNotEmpty(filter.getProcessedState())){ 
				query.setParameter("processedStatePrm", filter.getProcessedState());
			}
		  if(Validations.validateIsNotNull(filter.getBillingServiceTo())){
		    if(Validations.validateIsNotNullAndNotEmpty(filter.getBillingServiceTo().getIdBillingServicePk())){ 
				query.setParameter("idBillingServicePKPrm", filter.getBillingServiceTo().getIdBillingServicePk());
			}
		    if(Validations.validateIsNotNullAndNotEmpty(filter.getBillingServiceTo().getServiceCode())){
			  query.setParameter("serviceCodePrm", filter.getBillingServiceTo().getServiceCode());
			}
		    if(Validations.validateIsNotNullAndNotEmpty(filter.getBillingServiceTo().getServiceType())){
				  query.setParameter("serviceTypePrm", filter.getBillingServiceTo().getServiceType());
				}
		  }
		  if(Validations.validateIsNotNull(filter.getInitialDate())&&Validations.validateIsNotNull(filter.getFinalDate())){
			  query.setParameter("initialDatePrm", filter.getInitialDate());
			  query.setParameter("finalDatePrm", filter.getFinalDate());
		  }
		  
		  
		  List<Object[]>  objectList = query.getResultList();
		  
				
		return objectList;	
		
	}
	
	/**
	 * Find ProcessedService .
	 *
	 * @param filter ProcessedServiceTo
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ProcessedServiceTo> findProcessedServiceForServiceCalculate(ProcessedServiceTo filter)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		 sbQuery.append(" select ps.ID_PROCESSED_SERVICE_PK,cr.ID_COLLECTION_RECORD_PK, bs.ID_BILLING_SERVICE_PK, ");
		 sbQuery.append(" ps.CALCULATION_DATE, bs.SERVICE_NAME, ");
		 sbQuery.append(" (select pt.description from parameter_table pt where bs.BASE_COLLECTION=pt.parameter_table_pk) as description_base_collection, " );
		 sbQuery.append(" cr.COLLECTION_AMOUNT,ps.PROCESSED_STATE, ");
		 sbQuery.append(" (select pt.description from parameter_table pt where ps.PROCESSED_STATE=pt.parameter_table_pk) as description_processed_state ");
		 sbQuery.append(" from processed_service ps ");
		 sbQuery.append("  inner join billing_service bs on (bs.id_billing_service_pk=ps.id_billing_service_fk ) ");
		 sbQuery.append("  inner join collection_record cr on (ps.ID_PROCESSED_SERVICE_PK=cr.ID_PROCESSED_SERVICE_FK)  ");
		 sbQuery.append("  where 1=1 ");

		
		 if(Validations.validateIsNotNullAndNotEmpty(filter.getProcessedState())){
				sbQuery.append(" and ps.id_billing_service_fk= :idBillingServicePrm");
			}
		  if(Validations.validateIsNotNull(filter.getBillingServiceTo())){
			  if(Validations.validateIsNotNull(filter.getBillingServiceTo().getEntityCollection()))
				sbQuery.append("  and cr.ID_PARTICIPANT_FK= :idParticipantFkPrm");
			}
		  if(Validations.validateIsNotNull(filter.getBillingServiceTo())){
				sbQuery.append("  and cr.ID_HOLDER_FK= :idHolderFkPrm");
			}
		  if(Validations.validateIsNotNull(filter.getBillingServiceTo())){
				sbQuery.append("  and cr.ID_ISSUER_FK= :idIssuerFkPrm");
			}
		  Query query = em.createNativeQuery(sbQuery.toString());
		 
		  if(Validations.validateIsNotNullAndNotEmpty(filter.getProcessedState())){ 
				query.setParameter("idBillingServicePrm", filter.getProcessedState());
			}
		  if(Validations.validateIsNotNull(filter.getBillingServiceTo())){
			  query.setParameter("idParticipantFkPrm", filter.getProcessedState());
		  }
		  
		   List<Object[]>  objectList = query.getResultList();
		  List<ProcessedServiceTo> processedServiceToList = new ArrayList<ProcessedServiceTo>();
		  List<CollectionRecordTo> collectionRecordToList = new ArrayList<CollectionRecordTo>();
			for(int i=0;i<objectList.size();++i){			
				//Getting Results from data base
				
				Object[] sResults = (Object[])objectList.get(i);
				ProcessedServiceTo processedService = new ProcessedServiceTo();
				CollectionRecordTo collectionRecord=new CollectionRecordTo();
				BillingServiceTo billingService=new BillingServiceTo();
				
				processedService.setIdProcessedServicePk(Long.parseLong(sResults[0]==null?"":sResults[0].toString()));
				collectionRecord.setIdCollectionRecordPk(Long.parseLong(sResults[1]==null?"":sResults[1].toString()));
				billingService.setIdBillingServicePk(Long.parseLong(sResults[2]==null?"":sResults[2].toString()));
				processedService.setCalculationDate(sResults[3]==null?null:(Date) sResults[3]);
				billingService.setServiceName(sResults[4]==null?"":sResults[4].toString());
				billingService.setDescriptionBaseCollection(sResults[5]==null?"":sResults[5].toString());
				collectionRecord.setCollectionAmount(sResults[6]==null?null:new BigDecimal(sResults[6].toString()));
				processedService.setProcessedState(sResults[7]==null?0:Integer.parseInt(sResults[7].toString()));	
				processedService.setDescriptionProcessedState(sResults[8]==null?"":sResults[8].toString());
				collectionRecordToList.add(collectionRecord);
				
				processedService.setBillingServiceTo(billingService);
				processedService.setCollectionRecordsTo(collectionRecordToList);
				processedServiceToList.add(processedService);
			}
			return processedServiceToList;
	}



	/**
	 * Get ProcessedServiceTo for consult Collection Manual.
	 *
	 * @param filter the filter
	 * @return the processed service to
	 * @throws ServiceException the service exception
	 */
	public ProcessedServiceTo findProcessedServiceToForConsult(ProcessedServiceTo filter)throws ServiceException{
		
		StringBuilder sbQuery = new StringBuilder();
		 sbQuery.append(" select ps.ID_PROCESSED_SERVICE_PK,cr.ID_COLLECTION_RECORD_PK, bs.ID_BILLING_SERVICE_PK, ");
		 sbQuery.append(" bs.SERVICE_NAME,bs.SERVICE_CODE,bs.CLIENT_SERVICE_TYPE, bs.SERVICE_TYPE,bs.STATE_SERVICE, ");
		 sbQuery.append(" bs.ENTITY_COLLECTION,bs.BASE_COLLECTION, " );
		 sbQuery.append(" (select pt.description from parameter_table pt where bs.SERVICE_TYPE=pt.parameter_table_pk ) as desription_service_type , ");
		 sbQuery.append(" (select pt.description from parameter_table pt where bs.BASE_COLLECTION=pt.parameter_table_pk) as description_base_collection, ");
		 sbQuery.append(" (select pt.description from parameter_table pt where bs.CLIENT_SERVICE_TYPE=pt.parameter_table_pk ) as desription_client_service_type , ");
		 sbQuery.append(" (select pt.description from parameter_table pt where bs.ENTITY_COLLECTION=pt.parameter_table_pk) as description_entity_collection, ");
		 sbQuery.append(" cr.GROSS_AMOUNT,cr.COLLECTION_AMOUNT,cr.TAX_APPLIED, ");
		 sbQuery.append(" CASE  WHEN bs.ENTITY_COLLECTION=1406 THEN (SELECT BUSINESS_NAME FROM ISSUER WHERE ID_ISSUER_PK=cr.ID_ISSUER_FK ) ");
		 sbQuery.append(" 		WHEN bs.ENTITY_COLLECTION=1407 THEN (SELECT DESCRIPTION from PARTICIPANT WHERE ID_PARTICIPANT_PK=CR.ID_PARTICIPANT_FK ) ");
		 sbQuery.append(" 		WHEN bs.ENTITY_COLLECTION=1408 THEN (SELECT full_name FROM HOLDER WHERE ID_HOLDER_PK=CR.ID_HOLDER_FK  ) ");
		 sbQuery.append(" 		WHEN BS.ENTITY_COLLECTION=1530 THEN ");
		 sbQuery.append(" 		(select cr.other_entity_document|| '-' ||cr.other_entity_description from collection_record cr ");
		 sbQuery.append("     where cr.ID_PROCESSED_SERVICE_FK=ps.ID_PROCESSED_SERVICE_PK) ");
		 sbQuery.append(" WHEN BS.ENTITY_COLLECTION=1405 THEN  (select pt.description from parameter_table pt where bs.ENTITY_COLLECTION=pt.parameter_table_pk) ");
		 sbQuery.append("  WHEN BS.ENTITY_COLLECTION=251 THEN  (select pt.description from parameter_table pt where bs.ENTITY_COLLECTION=pt.parameter_table_pk) ");
		 sbQuery.append(" 		ELSE ' ' ");
		 sbQuery.append(" END AS DESCRIP_ENTITY_COLLECTION,PS.PROCESSED_STATE,PS.OPERATION_DATE,ps.CALCULATION_DATE ");
		 sbQuery.append(" from processed_service ps ");
		 sbQuery.append(" inner join collection_record cr on cr.ID_PROCESSED_SERVICE_FK=ps.ID_PROCESSED_SERVICE_PK ");
		 sbQuery.append(" inner join billing_service bs on bs.ID_BILLING_SERVICE_PK=ps.ID_BILLING_SERVICE_FK ");
		 sbQuery.append("  where 1=1 ");
		 
		
		 
		 if(Validations.validateIsNotNull(filter.getIdProcessedServicePk())){
			 sbQuery.append(" and ps.ID_PROCESSED_SERVICE_PK= :idProcessedServicePrm");
		 }
		  
		  
		  Query query = em.createNativeQuery(sbQuery.toString());
		  
		  if(Validations.validateIsNotNullAndNotEmpty(filter.getIdProcessedServicePk())){ 
				query.setParameter("idProcessedServicePrm", filter.getIdProcessedServicePk());
			}
		  
		   List<Object[]>  objectList = query.getResultList();
		   List<CollectionRecordTo> collectionRecordToList = new ArrayList<CollectionRecordTo>();
		   ProcessedServiceTo processedServiceTo = new ProcessedServiceTo();
		   CollectionRecordTo collectionRecordTo;
		   BillingServiceTo billingServiceTo;
			for(int i=0;i<objectList.size();++i){			
				//Getting Results from data base
				/**21*/
				Object[] sResults = (Object[])objectList.get(i);
				collectionRecordTo=new CollectionRecordTo();
				billingServiceTo=new BillingServiceTo();
				processedServiceTo.setIdProcessedServicePk(Long.parseLong(sResults[0]==null?"":sResults[0].toString()));
				collectionRecordTo.setIdCollectionRecordPk(Long.parseLong(sResults[1]==null?"":sResults[1].toString()));
				billingServiceTo.setIdBillingServicePk(Long.parseLong(sResults[2]==null?"":sResults[2].toString()));
				billingServiceTo.setServiceName(sResults[3]==null?"":sResults[3].toString());
				billingServiceTo.setServiceCode(sResults[4]==null?"":sResults[4].toString());
				billingServiceTo.setClientServiceType(sResults[5]==null?0:Integer.parseInt(sResults[5].toString()));
				billingServiceTo.setServiceType(sResults[6]==null?0:Integer.parseInt(sResults[6].toString()));
				billingServiceTo.setStateService(sResults[7]==null?0:Integer.parseInt(sResults[7].toString()));
				billingServiceTo.setEntityCollection(sResults[8]==null?0:Integer.parseInt(sResults[8].toString()));
				billingServiceTo.setBaseCollection(sResults[9]==null?0:Integer.parseInt(sResults[9].toString()));
				billingServiceTo.setDescriptionServiceType(sResults[10]==null?"":sResults[10].toString());
				billingServiceTo.setDescriptionBaseCollection(sResults[11]==null?"":sResults[11].toString());
				billingServiceTo.setDescriptionClientServiceType(sResults[12]==null?"":sResults[12].toString());
				billingServiceTo.setDescriptionCollectionEntity(sResults[13]==null?"":sResults[13].toString());
				processedServiceTo.setIdProcessedServicePk(Long.parseLong(sResults[0]==null?"":sResults[0].toString()));
				collectionRecordTo.setGrossAmount(sResults[14]==null?null:new BigDecimal(sResults[14].toString()));
				collectionRecordTo.setCollectionAmount(sResults[15]==null?null:new BigDecimal(sResults[15].toString()));
				collectionRecordTo.setTaxApplied(sResults[16]==null?null:new BigDecimal(sResults[16].toString()));
				collectionRecordTo.setDescriptionEntityCollection(sResults[17]==null?"":sResults[17].toString());
				processedServiceTo.setProcessedState(sResults[18]==null?0:Integer.parseInt(sResults[18].toString()));
				processedServiceTo.setOperationDate(sResults[19]==null?null:(Date) sResults[19]);
				processedServiceTo.setCalculationDate(sResults[20]==null?null:(Date) sResults[20]);
				collectionRecordToList.add(collectionRecordTo);
				processedServiceTo.setBillingServiceTo(billingServiceTo);
				processedServiceTo.setCollectionRecordToSearch(collectionRecordTo);
				processedServiceTo.setCollectionRecordsTo(collectionRecordToList);
			}
		return processedServiceTo;
		
	}
	
	/**
	 * Find All Processed Service All By Id.
	 *
	 * @param id the id
	 * @return the processed service
	 * @throws ServiceException the service exception
	 */
	public ProcessedService findProcessedServiceAllById(Long id) throws ServiceException{
		
		ProcessedService  processedService = null;
		if(Validations.validateIsNotNullAndNotEmpty(id)){
			
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" Select ps From ProcessedService ps   left join fetch ps.billingService bs  left join fetch bs.serviceRates ");
			sbQuery.append(" Where ps.idProcessedServicePk = :id  ");
			
			Query query = em.createQuery(sbQuery.toString());   
		    
			query.setParameter("id", id);			
		
			processedService=(ProcessedService) query.getSingleResult();
			
		}
	
		return processedService;
	}
	
	/**
	 * Find Processed Service By IdProcessedServicePk.
	 *
	 * @param id the id
	 * @return the processed service
	 * @throws ServiceException the service exception
	 */
	public ProcessedService findProcessedServiceById(Long id) throws ServiceException{
		ProcessedService  processedService = null;
		if(Validations.validateIsNotNullAndNotEmpty(id)){
			
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" Select ps From ProcessedService ps  ");
			sbQuery.append(" Where ps.idProcessedServicePk = :id  ");
			
			Query query = em.createQuery(sbQuery.toString());   
		    
			query.setParameter("id", id);			
		
			processedService=(ProcessedService) query.getSingleResult();
			
		}
	
		return processedService;
	}
	
	/**
	 * Find Processed Service For Recalculate.
	 *
	 * @param processedServiceTo the processed service to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)	
	public List<ProcessedService>	findProcessedServiceForCalculate(ProcessedServiceTo processedServiceTo) throws ServiceException{
		
		 List<ProcessedService> processedServiceList = null;
		
		 	
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" Select new ProcessedService( ");
			sbQuery.append(" ps.idProcessedServicePk , ps.calculationDate , ps.operationDate, ");
			sbQuery.append(" ps.monthProcess , ps.yearProcess, ps.processedState , bs , ");
			sbQuery.append(" ps.initialDate, ps.finalDate,  ");
			sbQuery.append(" ps.lastModifyApp,ps.lastModifyDate,ps.lastModifyIp,ps.lastModifyUser,");
			sbQuery.append("count(crs) as countCollection )");
			
			sbQuery.append("  From ProcessedService ps  inner  join  ps.billingService bs   ");
			sbQuery.append("  left outer  join   ps.collectionRecords crs    ");
			sbQuery.append(" Where  bs.serviceState=").append(PropertiesConstants.SERVICE_STATUS_ACTIVE_PK);
			sbQuery.append(" and    bs.serviceType=").append(PropertiesConstants.SERVICE_TYPE_AUTOMATIC_PK);
			sbQuery.append(" and    bs.calculationPeriod=:periodCalculation  ");
			sbQuery.append(" and    bs.baseCollection=:baseCollection  ");
			sbQuery.append(" and   ( crs.indBilled=").append(PropertiesConstants.NOT_BILLED).append(" or " );
			sbQuery.append(" crs.indBilled=").append(PropertiesConstants.YES_BILLED ).append(" or " );
			sbQuery.append(" crs.indBilled is null )" );
			
			if (processedServiceTo.getBillingServiceTo().getCalculationPeriod().equals(PropertiesConstants.PERIOD_CALCULATION_DAILY_PK)){
				sbQuery.append(" and    ps.calculationDate=:calculationDate  ");
			}else if(processedServiceTo.getBillingServiceTo().getCalculationPeriod().equals(PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK)){
				sbQuery.append(" and    ps.monthProcess=:monthProcess and  ps.yearProcess=:yearProcess  ");
			}
			
			if (Validations.validateIsNotNullAndPositive(processedServiceTo.getBillingServiceTo().getIdBillingServicePk())){
				sbQuery.append(" and    bs.idBillingServicePk =:idBillingServicePk ");
			}
									
			if (Validations.validateIsNotNullAndPositive(processedServiceTo.getIdProcessedServicePk())){
				 sbQuery.append(" and  ps.idProcessedServicePk=:idProcessedServicePk  ");							
			}
			
			
			sbQuery.append(" group by ps.idProcessedServicePk, ps.calculationDate , ps.operationDate, ");
			sbQuery.append("  ps.monthProcess , ps.yearProcess, ps.processedState , ");
			sbQuery.append(" bs ,ps.initialDate, ps.finalDate,    ");
			sbQuery.append(" ps.lastModifyApp,ps.lastModifyDate,ps.lastModifyIp,ps.lastModifyUser ");
			
			Query query = em.createQuery(sbQuery.toString());   
			
			if (processedServiceTo.getBillingServiceTo().getCalculationPeriod().equals(PropertiesConstants.PERIOD_CALCULATION_DAILY_PK)){
				query.setParameter("calculationDate",   processedServiceTo.getCalculationDate(),TemporalType.DATE);
			}else if(processedServiceTo.getBillingServiceTo().getCalculationPeriod().equals(PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK)){
				query.setParameter("monthProcess", processedServiceTo.getMonthProcess() );
				query.setParameter("yearProcess", processedServiceTo.getYearProcess() );
			}
			
			if (Validations.validateIsNotNullAndPositive(processedServiceTo.getBillingServiceTo().getIdBillingServicePk())){
				
				query.setParameter("idBillingServicePk", processedServiceTo.getBillingServiceTo().getIdBillingServicePk() );
			}
								
			if (Validations.validateIsNotNullAndPositive(processedServiceTo.getIdProcessedServicePk())){
				query.setParameter("idProcessedServicePk", processedServiceTo.getIdProcessedServicePk());				
			}
			
			
			query.setParameter("baseCollection",    processedServiceTo.getBillingServiceTo().getBaseCollection());	
			query.setParameter("periodCalculation",processedServiceTo.getBillingServiceTo().getCalculationPeriod());
			 
			
			
			processedServiceList = ((List<ProcessedService>)query.getResultList());
			
			
			return processedServiceList;
			
	}
	
	
	
	/**
	 * Find Processed Service For Batch.
	 *
	 * @param processedServiceTo the processed service to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ProcessedService>	findProcessedServiceForBatch(ProcessedServiceTo processedServiceTo)	throws ServiceException{
		
		
		 List<ProcessedService> processedServiceList = null;
			
		 	
			StringBuilder sbQuery = new StringBuilder();
			
			sbQuery.append(" Select distinct ps From ProcessedService ps join fetch  ps.billingService bs   ");
			sbQuery.append(" Where  bs.serviceState="+PropertiesConstants.SERVICE_STATUS_ACTIVE_PK+ "  "  );
			sbQuery.append(" and    bs.serviceType="+PropertiesConstants.SERVICE_TYPE_AUTOMATIC_PK + " ");
			sbQuery.append(" and    bs.calculationPeriod=:periodCalculation  ");
			sbQuery.append(" and    bs.baseCollection=:baseCollection  ");
			
			if (processedServiceTo.getBillingServiceTo().getCalculationPeriod().equals(PropertiesConstants.PERIOD_CALCULATION_DAILY_PK)){
				sbQuery.append(" and    ps.calculationDate=:calculationDate  ");
			}else if(processedServiceTo.getBillingServiceTo().getCalculationPeriod().equals(PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK)){
				sbQuery.append(" and    ps.monthProcess=:monthProcess and  ps.yearProcess=:yearProcess  ");
			}
			
			if (Validations.validateIsNotNullAndPositive(processedServiceTo.getBillingServiceTo().getIdBillingServicePk())){
				sbQuery.append(" and    bs.idBillingServicePk =:idBillingServicePk ");
			}
									
			if (Validations.validateIsNotNullAndPositive(processedServiceTo.getIdProcessedServicePk())){
				 sbQuery.append(" and  ps.idProcessedServicePk=:idProcessedServicePk  ");							
			}
			
			
			Query query = em.createQuery(sbQuery.toString());   
			
			if (processedServiceTo.getBillingServiceTo().getCalculationPeriod().equals(PropertiesConstants.PERIOD_CALCULATION_DAILY_PK)){
				query.setParameter("calculationDate",   processedServiceTo.getCalculationDate(),TemporalType.DATE);
			}else if(processedServiceTo.getBillingServiceTo().getCalculationPeriod().equals(PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK)){
				query.setParameter("monthProcess", processedServiceTo.getMonthProcess() );
				query.setParameter("yearProcess", processedServiceTo.getYearProcess());
			}
			
			if (Validations.validateIsNotNullAndPositive(processedServiceTo.getBillingServiceTo().getIdBillingServicePk())){
				
				query.setParameter("idBillingServicePk", processedServiceTo.getBillingServiceTo().getIdBillingServicePk() );
			}
								
			if (Validations.validateIsNotNullAndPositive(processedServiceTo.getIdProcessedServicePk())){
				query.setParameter("idProcessedServicePk", processedServiceTo.getIdProcessedServicePk());				
			}
			
			
			query.setParameter("baseCollection",    processedServiceTo.getBillingServiceTo().getBaseCollection());	
			query.setParameter("periodCalculation",processedServiceTo.getBillingServiceTo().getCalculationPeriod());
			 
			
			
			processedServiceList = ((List<ProcessedService>)query.getResultList());
			
			
			return processedServiceList;
		
	}
	
	/**
	 * *
	 * Find Calculation Record Billed.
	 *
	 * @param processedServiceTo the processed service to
	 * @return the list
	 */
	public List<CollectionRecord>	findCalculationRecordBilled(ProcessedServiceTo processedServiceTo) {
		
		 List<CollectionRecord> collectionRecordList = null;

			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" Select cr From CollectionRecord cr  inner  join cr.processedService ps inner join ps.billingService bs   ");
			sbQuery.append(" Where bs.idBillingServicePk = :idBillingServicePk and ps.calculationDate = :calculationDate  ");
			
			if( Validations.validateIsNotNull(processedServiceTo.getIndBilled()) ){
				
				sbQuery.append(" and cr.indBilled=:indBilled " );
			}
			
			if(Validations.validateIsNotNull(processedServiceTo.getIdProcessedServicePk()) ){
				
				sbQuery.append(" and ps.idProcessedServicePk=:idProcessedServicePk " );
			}
			
			if(Validations.validateIsNotNull(processedServiceTo.getYearProcess()) ){
				
				sbQuery.append(" and ps.yearProcess=:yearProcess " );
			}
			
			if(Validations.validateIsNotNull(processedServiceTo.getMonthProcess()) ){
				
				sbQuery.append(" and ps.monthProcess=:monthProcess " );
			}
			
			if(Validations.validateIsNotNull(processedServiceTo.getInitialDate()) ){
				
				sbQuery.append(" and ps.initialDate=:initialDate " );
			}

			if(Validations.validateIsNotNull(processedServiceTo.getFinalDate()) ){
	
				sbQuery.append(" and ps.finalDate=:finalDate " );
			}
			
			Query query = em.createQuery(sbQuery.toString());
			
			if( Validations.validateIsNotNull(processedServiceTo.getIndBilled()) ){
				
				query.setParameter("indBilled", processedServiceTo.getIndBilled());	
			}
			
			if(Validations.validateIsNotNull(processedServiceTo.getIdProcessedServicePk()) ){
				
				query.setParameter("idProcessedServicePk",processedServiceTo.getIdProcessedServicePk());	
			}
			
			if(Validations.validateIsNotNull(processedServiceTo.getYearProcess()) ){
				
				query.setParameter("yearProcess",processedServiceTo.getYearProcess());
			}
			
			if(Validations.validateIsNotNull(processedServiceTo.getMonthProcess()) ){
				
				query.setParameter("monthProcess",processedServiceTo.getMonthProcess());
			}
			
			if(Validations.validateIsNotNull(processedServiceTo.getInitialDate()) ){
							
				query.setParameter(GeneralConstants.FIELD_INITIAL_DATE,processedServiceTo.getInitialDate());
			}
			
			if(Validations.validateIsNotNull(processedServiceTo.getFinalDate()) ){
				
				query.setParameter(GeneralConstants.FIELD_FINAL_DATE,processedServiceTo.getFinalDate());
			}
			
			
			if(Validations.validateIsNotNullAndNotEmpty(processedServiceTo.getBillingServiceTo().getIdBillingServicePk())  && 
					(Validations.validateIsNotNullAndNotEmpty(processedServiceTo.getCalculationDate())) ){
				
				query.setParameter("idBillingServicePk", processedServiceTo.getBillingServiceTo().getIdBillingServicePk());	
				query.setParameter("calculationDate", processedServiceTo.getCalculationDate(),TemporalType.DATE);
				
				
				collectionRecordList = ((List<CollectionRecord>)query.getResultList());
			}
		
			return collectionRecordList;
			
	}
	
	
	/**
	 * Delete Collection Record Detail for Recalculate.
	 *
	 * @param processedServiceId the processed service id
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int  deleteCollectionRecordDetailForRecalculate(Long  processedServiceId) throws ServiceException{
		
		int operation=0;
		StringBuilder stb = new StringBuilder();
		stb.append("delete from collection_record_detail ");
		stb.append("where id_collection_record_detail_pk in ( ");
		stb.append("select id_collection_record_detail_pk from collection_record_detail  ");
		stb.append("where id_collection_record_fk in ( select id_collection_record_pk from collection_record  ");
		stb.append("where  id_processed_service_fk = :id  and ind_billed=:ind_billed ) )");
		 
		Query query = em.createNativeQuery(stb.toString());  
		
		if (Validations.validateIsNotNull(processedServiceId)){
			query.setParameter("id", processedServiceId);
			query.setParameter("ind_billed", PropertiesConstants.NOT_BILLED);
			operation=query.executeUpdate();
		}

		return operation;
		
	}
	
	/**
	 * Delete Collection Record For Recalculate .
	 *
	 * @param processedServiceId the processed service id
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int  deleteCollectionRecordForRecalculate(Long  processedServiceId) throws ServiceException{
		
		int operation=0;
		StringBuilder stb = new StringBuilder();
		stb.append(" delete  from collection_record  ");
		stb.append(" where  id_processed_service_fk=:id and ind_billed=:ind_billed ");
		
		 
		Query query = em.createNativeQuery(stb.toString());  
		
		if (Validations.validateIsNotNull(processedServiceId)){
			query.setParameter("id", processedServiceId);
			query.setParameter("ind_billed", PropertiesConstants.NOT_BILLED);
			operation=query.executeUpdate();
		}
	
		return operation;
	}
	

	/**
	 * Load processed service to for collection manual.
	 *
	 * @param processedServiceTo the processed service to
	 * @param sResults the s results
	 */
	public void loadProcessedServiceToForCollectionManual(ProcessedServiceTo processedServiceTo ,Object[] sResults ){
		
		BillingServiceTo  billingServiceTo=new BillingServiceTo();
		CollectionRecordTo collectionRecordTo=new CollectionRecordTo();
		processedServiceTo.setIdProcessedServicePk(Long.parseLong(sResults[0]==null?"":sResults[0].toString()));     
		processedServiceTo.setProcessedState(sResults[1]==null?null:Integer.parseInt(sResults[1].toString()));
        processedServiceTo.setOperationDate(sResults[2]==null?null:(Date) sResults[2]);
        processedServiceTo.setNumberOfCollections(sResults[3]==null?null:Integer.parseInt(sResults[3].toString()));			
        processedServiceTo.setDescriptionProcessedState(sResults[4]==null?"":sResults[4].toString());
        billingServiceTo.setIdBillingServicePk(Long.parseLong(sResults[5]==null?"":sResults[5].toString()));
        billingServiceTo.setServiceName(sResults[6]==null?"":sResults[6].toString());
        billingServiceTo.setServiceCode(sResults[7]==null?"":sResults[7].toString());
        billingServiceTo.setEntityCollection(sResults[8]==null?null:Integer.parseInt(sResults[8].toString()));
        collectionRecordTo.setDescriptionEntityCollection(sResults[9]==null?"":sResults[9].toString());
        billingServiceTo.setDescriptionNameCollectionEntity(sResults[9]==null?"":sResults[9].toString());
        billingServiceTo.setDescriptionCollectionEntity(sResults[10]==null?"":sResults[10].toString());
        billingServiceTo.setClientServiceType(sResults[11]==null?null:Integer.parseInt(sResults[11].toString()));
        billingServiceTo.setDescriptionClientServiceType(sResults[12]==null?"":sResults[12].toString());
        billingServiceTo.setInitialEffectiveDate(sResults[13]==null?null:(Date) sResults[13]);
        billingServiceTo.setEndEffectiveDate(sResults[14]==null?null:(Date) sResults[14]);
        billingServiceTo.setStateService(sResults[15]==null?null:Integer.parseInt(sResults[15].toString()));
        billingServiceTo.setDescriptionStateBillingService(sResults[16]==null?"":sResults[16].toString());
        billingServiceTo.setServiceType(sResults[17]==null?null:Integer.parseInt(sResults[17].toString()));
        billingServiceTo.setDescriptionServiceType(sResults[18]==null?"":sResults[18].toString());
        collectionRecordTo.setIdProcessedServiceFk(Long.parseLong(sResults[0]==null?"":sResults[0].toString()));  
        collectionRecordTo.setRecordState(sResults[1]==null?null:Integer.parseInt(sResults[1].toString()));
        collectionRecordTo.setCollectionAmount(sResults[19]==null?null:new BigDecimal(sResults[19].toString()));
        collectionRecordTo.setGrossAmount(sResults[20]==null?null:new BigDecimal(sResults[20].toString()));
        collectionRecordTo.setTaxApplied(sResults[21]==null?null:new BigDecimal(sResults[21].toString()));
        //collectionRecordTo.setRecordState(sResults[22]==null?null:Integer.parseInt(sResults[22].toString()));
        collectionRecordTo.setDescriptionStatusRecord(sResults[4]==null?"":sResults[4].toString());
        collectionRecordTo.setIndBilled(sResults[24]==null?null:Integer.parseInt(sResults[24].toString()));
        collectionRecordTo.setDescriptionIndBilled(sResults[25]==null?"":sResults[25].toString());
        //collectionRecordTo.setMovementCount(sResults[26]==null?null:Integer.parseInt(sResults[26].toString()));
        collectionRecordTo.setCalculationDate(sResults[26]==null?null:(Date) sResults[26]);
        collectionRecordTo.setOperationDate(sResults[27]==null?null:(Date) sResults[27]);
        collectionRecordTo.setIdCollectionRecordPk(Long.parseLong(sResults[28]==null?"":sResults[28].toString()));
        
        List<CollectionRecordTo> listCollectionRecordTo=new ArrayList<CollectionRecordTo>();
        listCollectionRecordTo.add(collectionRecordTo);
        processedServiceTo.setCollectionRecordsTo(listCollectionRecordTo);
        processedServiceTo.setBillingServiceTo(billingServiceTo);

	}
	

	/**
	 * get the Processed Service by code.
	 *
	 * @param code the code
	 * @return the processed service by code
	 */
	public ProcessedService getProcessedServiceByCode(String code){
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select ps From ProcessedService ps  ");
		sbQuery.append(" Where 1 = 1");

		
		if(Validations.validateIsNotNullAndNotEmpty(code)){
			sbQuery.append(" And ps.ID_PROCESSED_SERVICE_PK = :code");
		}
		
		Query query = em.createQuery(sbQuery.toString());   
		
		if(Validations.validateIsNotNullAndNotEmpty(code)){
			query.setParameter("code", code);			
		}
		
		ProcessedService processedService=new ProcessedService(); 
		try{
			Object entity=query.getSingleResult();
			processedService=(ProcessedService)entity;
		} catch(NoResultException  nrex){
			processedService=null;
		}
		
		
		return processedService;
	}
	
	/**
	 * Update Processed Service.
	 *
	 * @param processedService the processed service
	 * @throws ServiceException the service exception
	 */
	public void updateProcessedService(ProcessedService  processedService) throws ServiceException {
		update(processedService);
	}
	
	/**
	 * Update collection record.
	 *
	 * @param collectionRecord the collection record
	 * @throws ServiceException the service exception
	 */
	public void updateCollectionRecord(CollectionRecord  collectionRecord) throws ServiceException {
		update(collectionRecord);		
	}
	
	/**
	 * Update collection record with Query .
	 *
	 * @param collectionRecord the collection record
	 * @throws ServiceException the service exception
	 */
	public void updateCollectionRecordByQuery(CollectionRecord  collectionRecord,LoggerUser loggerUser) throws ServiceException {
		StringBuilder sb=new StringBuilder();
		em.detach(collectionRecord);
		sb.append(" Update CollectionRecord cr Set cr.recordState= :recordState, ")
			.append(" cr.indBilled= :indBilled, ")
			.append(" cr.lastModifyDate= :lastModifyDate,  ")
			.append(" cr.lastModifyUser= :lastModifyUser,  ")
			.append(" cr.lastModifyIp= :lastModifyIp,  ")
			.append(" cr.lastModifyApp= :lastModifyApp  ")
			.append(" Where cr.idCollectionRecordPk= :idCollectionRecordPk")
			.append(" ");
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("recordState",collectionRecord.getRecordState());
		query.setParameter("indBilled",collectionRecord.getIndBilled());
		query.setParameter("lastModifyDate",loggerUser.getAuditTime());
		query.setParameter("lastModifyUser",loggerUser.getUserName());
		query.setParameter("lastModifyIp",loggerUser.getIpAddress());
		query.setParameter("lastModifyApp",loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("idCollectionRecordPk",collectionRecord.getIdCollectionRecordPk());
		query.executeUpdate();
		
		
	}
	
	/**
	 * Count Number The Collections Record By Processed Service.
	 *
	 * @param processedService the processed service
	 * @return the integer
	 */
	public Integer numCollectionOfProcessed(ProcessedService  processedService){
		StringBuilder sbQuery = new StringBuilder();
		 sbQuery.append(" select count(ID_COLLECTION_RECORD_PK) from collection_record CR  ");
		 sbQuery.append(" inner join processed_service PS on   ");
		 sbQuery.append(" PS.ID_PROCESSED_SERVICE_PK=CR.ID_PROCESSED_SERVICE_FK  ");
		 if(Validations.validateIsNotNull(processedService.getIdProcessedServicePk())){
			 sbQuery.append(" and PS.ID_PROCESSED_SERVICE_PK= :idProcessedServicePrm");
		 }
		  
		  
		  Query query = em.createNativeQuery(sbQuery.toString());
		  
		  if(Validations.validateIsNotNullAndNotEmpty(processedService.getIdProcessedServicePk())){ 
				query.setParameter("idProcessedServicePrm", processedService.getIdProcessedServicePk());
			}
		  
		return Integer.parseInt(query.getSingleResult().toString());
		
	}
	
	/**
	 * Update State Processed Service On Billing.
	 *
	 * @param processedServiceTo the processed service to
	 */
	public void updateProcessedServiceOnBilling(ProcessedServiceTo  processedServiceTo){
		
		 StringBuilder sbQuery = new StringBuilder();
		 sbQuery.append("  UPDATE PROCESSED_SERVICE PS SET PS.PROCESSED_STATE=:PROCESSED_STATE   ");
		 sbQuery.append(" WHERE (    PS.ID_PROCESSED_SERVICE_PK=:ID_PROCESSED_SERVICE_PK AND     ");
		 sbQuery.append(" ( (SELECT COUNT(1) FROM PROCESSED_SERVICE PS2 INNER JOIN COLLECTION_RECORD CR  ");
		 sbQuery.append(" ON (PS2.ID_PROCESSED_SERVICE_PK=CR.ID_PROCESSED_SERVICE_FK)  ");
		 sbQuery.append("  WHERE  PS.ID_PROCESSED_SERVICE_PK=PS2.ID_PROCESSED_SERVICE_PK AND   ");
		 sbQuery.append("  NOT ( CR.IND_BILLED=1 )) = 0 )  )   ");
		 
		  Query query = em.createNativeQuery(sbQuery.toString());
		  
		  if(Validations.validateIsNotNull(processedServiceTo.getIdProcessedServicePk())){
			  query.setParameter("PROCESSED_STATE", processedServiceTo.getProcessedState());
			  query.setParameter("ID_PROCESSED_SERVICE_PK", processedServiceTo.getIdProcessedServicePk());
		  }
		 
		  query.executeUpdate();
		
	}
	
	/**
	 * Make Query Inclusion Vs Exclusions Exceptions Invoice And
	 * Result List IdCollectionRecord  .
	 *
	 * @param processedServiceTo the processed service to
	 * @param listExceptionInvoiceGenerations the list exception invoice generations
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<CollectionRecordTo>  findCollectionRecordToByEntityException(ProcessedServiceTo processedServiceTo, 
														List<ExceptionInvoiceGenerations> listExceptionInvoiceGenerations) throws ServiceException{
		
		this.listHolder=processedServiceTo.getListHolder();
		this.listIssuer=processedServiceTo.getListIssuer();
		this.listParticipant=processedServiceTo.getListParticipant();
		StringBuilder sbQuery = new StringBuilder();
		Integer entityCollection = processedServiceTo.getCollectionRecordToSearch().getEntityCollection();
		Boolean hasExclusions = Boolean.FALSE;
		List<CollectionRecordTo> listCollectionRecordTo = new ArrayList<>();
		CollectionRecordTo collectionRecordTo;
		
		/**
		 * Inclusiones
		 */
		BillingEntityExceptionTo billingEntityInclusionTo =new BillingEntityExceptionTo();
		billingEntityInclusionTo=gettingIdEntityInvoice(entityCollection, listExceptionInvoiceGenerations,PropertiesConstants.INCLUSION_PK);
		/**
		 * Exclusiones
		 */
		BillingEntityExceptionTo billingEntityExclusionTo;
		billingEntityExclusionTo=gettingIdEntityInvoice(entityCollection, listExceptionInvoiceGenerations, PropertiesConstants.EXCLUSION_PK);
		
		/**
		 * Inclusions apply the regularization
		 */
		regularizationInclusions(entityCollection,billingEntityInclusionTo,billingEntityExclusionTo);
		
		/**
		 * Query
		 */
		sbQuery.append(" SELECT CR.ID_COLLECTION_RECORD_PK 																");
		sbQuery.append(" FROM COLLECTION_RECORD CR 																		");
		sbQuery.append(" INNER JOIN PROCESSED_SERVICE PS ON PS.ID_PROCESSED_SERVICE_PK = CR.ID_PROCESSED_SERVICE_FK 	");
		sbQuery.append(" WHERE 1 = 1 																					");
		sbQuery.append(" 	AND PS.ID_PROCESSED_SERVICE_PK = :idProcessedServicePk										");
		
		//EXCLUSIONES
		if(Validations.validateIsNotNull(entityCollection)){
			if(EntityCollectionType.PARTICIPANTS.getCode().equals(entityCollection) && Validations.validateListIsNotNullAndNotEmpty(billingEntityExclusionTo.getListIdParticipant())){
				sbQuery.append(" 	AND NOT CR.ID_PARTICIPANT_FK in (:listParticipantExclusion) ");
				hasExclusions=Boolean.TRUE;
			}
			if(EntityCollectionType.HOLDERS.getCode().equals(entityCollection) && Validations.validateListIsNotNullAndNotEmpty(billingEntityExclusionTo.getListIdHolder())){
				sbQuery.append(" 	AND NOT CR.ID_HOLDER_FK in (:listHolderExclusion) ");
				hasExclusions=Boolean.TRUE;
			}
			if(EntityCollectionType.ISSUERS.getCode().equals(entityCollection) && Validations.validateListIsNotNullAndNotEmpty(billingEntityExclusionTo.getListIdIssuer())){
				sbQuery.append(" 	AND NOT CR.ID_ISSUER_FK in (:listIssuerExclusion) ");
				hasExclusions=Boolean.TRUE;
			}
		}
		
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("idProcessedServicePk", processedServiceTo.getIdProcessedServicePk());

		if(Validations.validateIsNotNull(entityCollection)){
			if(EntityCollectionType.PARTICIPANTS.getCode().equals(entityCollection)){
				if(Validations.validateListIsNotNullAndNotEmpty(billingEntityExclusionTo.getListIdParticipant()))
					query.setParameter("listParticipantExclusion",billingEntityExclusionTo.getListIdParticipant());
			}
			if(EntityCollectionType.HOLDERS.getCode().equals(entityCollection)){
				if(Validations.validateListIsNotNullAndNotEmpty(billingEntityExclusionTo.getListIdHolder())){
					query.setParameter("listHolderExclusion",billingEntityExclusionTo.getListIdHolder());
				}
			}
			if(EntityCollectionType.ISSUERS.getCode().equals(entityCollection)){
				if(Validations.validateListIsNotNullAndNotEmpty(billingEntityExclusionTo.getListIdIssuer()))
					query.setParameter("listIssuerExclusion",billingEntityExclusionTo.getListIdIssuer());
			}
		}
		
		List<Object[]> objectList = query.getResultList();
		
		if (Validations.validateListIsNotNullAndNotEmpty(objectList)){
			for(Object obj: objectList){
				collectionRecordTo = new CollectionRecordTo();
				collectionRecordTo.setIdCollectionRecordPk(Long.parseLong(obj.toString()));
				listCollectionRecordTo.add(collectionRecordTo);
			}
		}
		
		return listCollectionRecordTo;
	}
	
	/**
	 * Regularization Inclusions,
	 * If Exits List Inclusions with Elements The List Exclusion
	 * Remove Elements the list Inclusions.
	 *
	 * @param entityCollection the entity collection
	 * @param inclusion the inclusion
	 * @param exclusion the exclusion
	 */
	public void regularizationInclusions(Integer entityCollection, BillingEntityExceptionTo inclusion, BillingEntityExceptionTo exclusion){
		
		List<Long> 		listIdParticipant=new ArrayList<>();
		List<Long> 		listIdHolder=new ArrayList<>();
		List<String> 	listIdIssuer=new ArrayList<>();
		
		if(EntityCollectionType.PARTICIPANTS.getCode().equals(entityCollection)){
			
			listIdParticipant = inclusion.getListIdParticipant();
			
			//recorriendo la lista de exclusiones
			for(Long partExcl : exclusion.getListIdParticipant()){
				//quitamos el participante de la lista de inclusiones
				listIdParticipant.remove(partExcl);
			}
			
		}else if(EntityCollectionType.HOLDERS.getCode().equals(entityCollection)){
			
			listIdHolder=inclusion.getListIdHolder();
			for (Long holderExcl : exclusion.getListIdHolder()) {
				if(listIdHolder.contains(holderExcl)){
					listIdHolder.remove(holderExcl);
				}
			}
			
		}else if(EntityCollectionType.ISSUERS.getCode().equals(entityCollection)){
			
			listIdIssuer=inclusion.getListIdIssuer();
			for (String issuerExcl : exclusion.getListIdIssuer()) {
				if(listIdIssuer.contains(issuerExcl)){
					listIdIssuer.remove(issuerExcl);
				}
			}
		}else if(EntityCollectionType.OTHERS.getCode().equals(entityCollection)){
			
		}
		
	}
	
	/**
	 * Getting Entity Exception Inclusion.
	 *
	 * @param entityCollection the entity collection
	 * @param listExceptionInvoiceGenerations the list exception invoice generations
	 * @return the ting entity exception inclusion
	 * @throws ServiceException the service exception
	 */
	public List<BillingEntityException> gettingEntityExceptionInclusion(Integer entityCollection, 
									List<ExceptionInvoiceGenerations> listExceptionInvoiceGenerations) throws ServiceException{
		List<BillingEntityException> listEntityInclusion=new ArrayList<BillingEntityException>();
		
		for (ExceptionInvoiceGenerations exceptionInvoice : listExceptionInvoiceGenerations) {
			
			if(PropertiesConstants.RDB_ONE.equals(exceptionInvoice.getIndSelectAll())){
				
				if(exceptionInvoice.getTypeException().equals(PropertiesConstants.INCLUSION_PK)){
					listEntityInclusion.addAll(exceptionInvoice.getBillingEntityException());
				}
				
			}else if(PropertiesConstants.RDB_ALL.equals(exceptionInvoice.getIndSelectAll())){
				/**Debo traer a todos por entidad*/
				
				if(exceptionInvoice.getTypeException().equals(PropertiesConstants.INCLUSION_PK)){
					listEntityInclusion=settingAllEntityInvoice(entityCollection);
				}
			}
			
			
		}
		
		return listEntityInclusion;
	}
	
	/**
	 * Getting Entity Exception Exclusion.
	 *
	 * @param entityCollection the entity collection
	 * @param listExceptionInvoiceGenerations the list exception invoice generations
	 * @return the ting entity exception exclusion
	 * @throws ServiceException the service exception
	 */
	public List<BillingEntityException> gettingEntityExceptionExclusion(Integer entityCollection, 
							List<ExceptionInvoiceGenerations> listExceptionInvoiceGenerations) throws ServiceException{
		
		List<BillingEntityException> listEntityExclusion=new ArrayList<BillingEntityException>();
		for (ExceptionInvoiceGenerations exceptionInvoice : listExceptionInvoiceGenerations) {
			
			if(PropertiesConstants.RDB_ONE.equals(exceptionInvoice.getIndSelectAll())){
				
				if(exceptionInvoice.getTypeException().equals(PropertiesConstants.EXCLUSION_PK)){
					listEntityExclusion.addAll(exceptionInvoice.getBillingEntityException());
				}
				
			}else if(PropertiesConstants.RDB_ALL.equals(exceptionInvoice.getIndSelectAll())){
				/**Debo traer a todos por entidad*/
				
				if(exceptionInvoice.getTypeException().equals(PropertiesConstants.EXCLUSION_PK)){
					listEntityExclusion=settingAllEntityInvoice(entityCollection);
				}
			}
			
			
		}
		return listEntityExclusion;
	}
	

	/**
	 * Getting Id Entity Invoice.
	 *
	 * @param entityCollection the entity collection
	 * @param listExceptionInvoiceGenerations the list exception invoice generations
	 * @param typeException the type exception
	 * @return the ting id entity invoice
	 * @throws ServiceException the service exception
	 */
	public BillingEntityExceptionTo gettingIdEntityInvoice(Integer entityCollection, 
											List<ExceptionInvoiceGenerations> listExceptionInvoiceGenerations,
											Integer typeException) throws ServiceException{
		List<Long> 		listIdParticipant=new ArrayList<>();
		List<String> 	listIdIssuer=new ArrayList<>();
		HashSet<Long> 	listSetIdHolder=new HashSet<>();
		
		List<BillingEntityException> listBillingEntityException=new ArrayList<BillingEntityException>();
		
		if(PropertiesConstants.EXCLUSION_PK.equals(typeException)){
			listBillingEntityException= gettingEntityExceptionExclusion(entityCollection, listExceptionInvoiceGenerations);
		}else if(PropertiesConstants.INCLUSION_PK.equals(typeException)){
			listBillingEntityException =gettingEntityExceptionInclusion(entityCollection, listExceptionInvoiceGenerations);
		}
		
		BillingEntityExceptionTo billingEntityExceptionTo=new BillingEntityExceptionTo();
		
		for (BillingEntityException billingEntityException : listBillingEntityException) {
			if(Validations.validateIsNotNull(entityCollection)){
				if(EntityCollectionType.HOLDERS.getCode().equals(entityCollection)){
					listSetIdHolder.add(billingEntityException.getHolder().getIdHolderPk());
				}
				if(EntityCollectionType.PARTICIPANTS.getCode().equals(entityCollection)){
					if(!listIdParticipant.contains(billingEntityException.getParticipant().getIdParticipantPk())){
						listIdParticipant.add(billingEntityException.getParticipant().getIdParticipantPk());
					}
				}
				if(EntityCollectionType.ISSUERS.getCode().equals(entityCollection)){
					if(!listIdIssuer.contains(billingEntityException.getIssuer().getIdIssuerPk())){
						listIdIssuer.add(billingEntityException.getIssuer().getIdIssuerPk());
					}
				}
			}
		}
		billingEntityExceptionTo.setListIdHolder(new ArrayList<>(listSetIdHolder));
		billingEntityExceptionTo.setListIdIssuer(listIdIssuer);
		billingEntityExceptionTo.setListIdParticipant(listIdParticipant);
		return billingEntityExceptionTo;
	}
	
	/**
	 * Setting All Entity Invoice.
	 *
	 * @param entityCollection the entity collection
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<BillingEntityException> settingAllEntityInvoice(Integer entityCollection) throws ServiceException{
		
		List<BillingEntityException> listBillingEntityException=new ArrayList<BillingEntityException>();
		BillingEntityException billingEntityException=null;
		if(EntityCollectionType.PARTICIPANTS.getCode().equals(entityCollection)){
			for (Participant participant : this.listParticipant) {
				billingEntityException=new BillingEntityException();
				billingEntityException.setParticipant(participant);
				listBillingEntityException.add(billingEntityException);
			}
		}else if(EntityCollectionType.HOLDERS.getCode().equals(entityCollection)){			
			for (Holder holder : this.listHolder) {
				billingEntityException=new BillingEntityException();
				billingEntityException.setHolder(holder);
				listBillingEntityException.add(billingEntityException);
				
			}
		}else if(EntityCollectionType.ISSUERS.getCode().equals(entityCollection)){
			for (Issuer issuer : this.listIssuer) {
				billingEntityException=new BillingEntityException();
				billingEntityException.setIssuer(issuer);
				listBillingEntityException.add(billingEntityException);
			}
		}
	
		return listBillingEntityException;
	}
	

	/**
	 * Find Collection Record By Parameters Exception Invoice.
	 *
	 * @param processedServiceTo the processed service to
	 * @return the list
	 */
	public List<CollectionRecord> findCollectionRecordByParametersException(ProcessedServiceTo processedServiceTo){
		
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select cr From CollectionRecord cr  inner join cr.processedService ps  ");
		sbQuery.append("	  inner join ps.billingService bs ");
		sbQuery.append("	inner join bs.listExceptionInvoiceGenerations eig ");
		sbQuery.append("	inner join eig.billingEntityException ben ");
		sbQuery.append("	");
		sbQuery.append("	");
		sbQuery.append("	");
		sbQuery.append(" Where  1=1   ");

		
		if (Validations.validateIsNotNull(processedServiceTo.getIdProcessedServicePk())){
			sbQuery.append(" AND ps.idProcessedServicePk = :idProcessedServicePk");
		}
		
		
		if (Validations.validateIsNotNull(processedServiceTo.getBillingServiceTo())){
			
			if (Validations.validateIsNotNull(processedServiceTo.getBillingServiceTo().getIdBillingServicePk())){
				sbQuery.append(" AND ps.billingService.idBillingServicePk = :idBillingServicePk");
			}
			
			if (Validations.validateIsNotNull(processedServiceTo.getBillingServiceTo().getInIntegratedBill())){
				sbQuery.append(" AND ps.billingService.inIntegratedBill = :inIntegratedBill");
			}
		
		}
		
		
		if (Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch())){
		
			if (Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getKeyEntityCollection())){
				sbQuery.append(" AND cr.keyEntityCollection = :keyEntityCollection");
			}
						
			if (Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getRecordState())){
				sbQuery.append(" AND cr.recordState= :recordState");
			}
			
			
			if (Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getIndBilled())){
				sbQuery.append(" AND cr.indBilled = :indBilled");
			}
			
			
			if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getHolder())){
				sbQuery.append(" AND cr.holder.idHolderPk = :idHolderPk ");
			}
				
			if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getParticipant())){
				sbQuery.append(" AND cr.participant.idParticipantPk = :idParticipantPk ");
			}
			
			if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getIssuer())){
				sbQuery.append(" AND cr.issuer.idIssuerPk = :idIssuerPk ");
			}
			
			if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getEntityCollection())){
				sbQuery.append(" AND cr.entityCollectionId = :entityCollectionId ");
				sbQuery.append(" AND cr.entityCollectionId = ben.entityCollectionId ");
				
				if(EntityCollectionType.PARTICIPANTS.getCode().equals(processedServiceTo.getCollectionRecordToSearch().getEntityCollection())){
					sbQuery.append(" AND cr.participant.idParticipantPk = ben.participant.idParticipantPk");
				}else if(EntityCollectionType.HOLDERS.getCode().equals(processedServiceTo.getCollectionRecordToSearch().getEntityCollection())){
					sbQuery.append(" AND cr.holder.idHolderPk = ben.holder.idHolderPk ");
				}else if(EntityCollectionType.ISSUERS.getCode().equals(processedServiceTo.getCollectionRecordToSearch().getEntityCollection())){
					sbQuery.append(" AND cr.issuer.idIssuerPk = ben.issuer.idIssuerPk ");
				}
			}
			
			if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getDocumentType())){
				sbQuery.append(" AND cr.documentType = :documentType ");
			}
			
			if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getOtherEntityDocument())){
				sbQuery.append(" AND cr.otherEntityDocument = :otherEntityDocument ");
			}
			
			if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getOtherEntityDescription())){
				sbQuery.append(" AND UPPER(cr.otherEntityDescription) LIKE :otherEntityDescription ");
			}
								
			
		}
		
		
		TypedQuery<CollectionRecord> query =  em.createQuery(sbQuery.toString(),CollectionRecord.class); 
		
		if (Validations.validateIsNotNull(processedServiceTo.getIdProcessedServicePk())){
			query.setParameter("idProcessedServicePk", processedServiceTo.getIdProcessedServicePk());
		}
		
		
		if (Validations.validateIsNotNull(processedServiceTo.getBillingServiceTo())){
			
			if (Validations.validateIsNotNull(processedServiceTo.getBillingServiceTo().getIdBillingServicePk())){
				query.setParameter("idBillingServicePk",processedServiceTo.getBillingServiceTo().getIdBillingServicePk());
			}
			
			if (Validations.validateIsNotNull(processedServiceTo.getBillingServiceTo().getInIntegratedBill())){
				query.setParameter("inIntegratedBill",processedServiceTo.getBillingServiceTo().getInIntegratedBill());
			}
		
		}
		
		
		if (Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch())){
			
			if (Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getKeyEntityCollection())){
				query.setParameter("keyEntityCollection",processedServiceTo.getCollectionRecordToSearch().getKeyEntityCollection());				
			}
			
			if (Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getRecordState())){		
				query.setParameter("recordState",processedServiceTo.getCollectionRecordToSearch().getRecordState());
			}
							
			if (Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getIndBilled())){
				query.setParameter("indBilled",processedServiceTo.getCollectionRecordToSearch().getIndBilled());
			}
			

			if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getHolder())){
				query.setParameter("idHolderPk",processedServiceTo.getCollectionRecordToSearch().getHolder().getIdHolderPk());
				
			}
				
			if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getParticipant())){
				query.setParameter("idParticipantPk",processedServiceTo.getCollectionRecordToSearch().getParticipant().getIdParticipantPk());
			
			}
			
			if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getIssuer())){
				query.setParameter("idIssuerPk",processedServiceTo.getCollectionRecordToSearch().getIssuer().getIdIssuerPk());
				
			}
			
			if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getEntityCollection())){
				query.setParameter("entityCollectionId",processedServiceTo.getCollectionRecordToSearch().getEntityCollection());
				
			}
			
			if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getDocumentType())){
				query.setParameter("documentType",processedServiceTo.getCollectionRecordToSearch().getDocumentType());				
			}
			
			if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getOtherEntityDocument())){
				query.setParameter("otherEntityDocument",processedServiceTo.getCollectionRecordToSearch().getOtherEntityDocument());
			
			}
			
		}
		
		
		List<CollectionRecord> listCollectionRecord = query.getResultList();
		
		return listCollectionRecord;
	}
	
	/**
	 * Find Collection Record By Parameters.
	 *
	 * @param processedServiceTo the processed service to
	 * @return the list
	 */
	public List<CollectionRecord> findCollectionRecordByParameters(ProcessedServiceTo processedServiceTo){
		
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select distinct cr From CollectionRecord cr   join cr.processedService ps  ");
		if(Validations.validateIsNotNullAndNotEmpty(processedServiceTo.getCollectionRecordToSearch().getIdHolderByParticipant())){
			sbQuery.append(" left join fetch cr.collectionRecordDetail crd");
		}
		sbQuery.append(" Where  1=1   ");
		
		if (Validations.validateIsNotNull(processedServiceTo.getIdProcessedServicePk())){
			sbQuery.append(" AND ps.idProcessedServicePk = :idProcessedServicePk");
		}
		
		if (Validations.validateListIsNotNullAndNotEmpty(processedServiceTo.getCollectionRecordsTo())){
			sbQuery.append(" AND cr.idCollectionRecordPk in ( :listIdCollectionRecordPk )");
		}
		
		if (Validations.validateIsNotNull(processedServiceTo.getBillingServiceTo())){
			if (Validations.validateIsNotNull(processedServiceTo.getBillingServiceTo().getIdBillingServicePk())){
				sbQuery.append(" AND ps.billingService.idBillingServicePk = :idBillingServicePk");
			}
			
			if (Validations.validateIsNotNull(processedServiceTo.getBillingServiceTo().getInIntegratedBill())){
				sbQuery.append(" AND ps.billingService.inIntegratedBill = :inIntegratedBill");
			}
		}
		
		if (Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch())){
			if (Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getKeyEntityCollection())){
				sbQuery.append(" AND cr.keyEntityCollection = :keyEntityCollection");
			}
			if (Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getRecordState())){
				sbQuery.append(" AND cr.recordState= :recordState");
			}
			if (Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getIndBilled())){
				sbQuery.append(" AND cr.indBilled = :indBilled");
			}
			if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getHolder())){
				sbQuery.append(" AND cr.holder.idHolderPk = :idHolderPk ");
			}
			if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getParticipant())){
				if(Validations.validateIsNotNullAndNotEmpty(processedServiceTo.getCollectionRecordToSearch().getIdHolderByParticipant())){
					sbQuery.append(" AND crd.idHolder = :idHolderByParticipant ");
				}
				sbQuery.append(" AND cr.participant.idParticipantPk = :idParticipantPk ");
			}
			if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getIssuer())){
				sbQuery.append(" AND cr.issuer.idIssuerPk = :idIssuerPk ");
			}
			if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getEntityCollection())){
				sbQuery.append(" AND cr.entityCollectionId = :entityCollectionId ");
			}
			if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getDocumentType())){
				sbQuery.append(" AND cr.documentType = :documentType ");
			}
			if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getOtherEntityDocument())){
				sbQuery.append(" AND cr.otherEntityDocument = :otherEntityDocument ");
			}
			if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getOtherEntityDescription())){
				sbQuery.append(" AND UPPER(cr.otherEntityDescription) LIKE :otherEntityDescription ");
			}
		}
		
		
		TypedQuery<CollectionRecord> query =  em.createQuery(sbQuery.toString(),CollectionRecord.class); 
		
		if (Validations.validateIsNotNull(processedServiceTo.getIdProcessedServicePk())){
			query.setParameter("idProcessedServicePk", processedServiceTo.getIdProcessedServicePk());
		}
		if (Validations.validateListIsNotNullAndNotEmpty(processedServiceTo.getCollectionRecordsTo())){
			List<Long> collectionIdList=BillingUtils.convertListCollectionRecordToListLong(processedServiceTo.getCollectionRecordsTo());
			query.setParameter("listIdCollectionRecordPk", collectionIdList);
		}
		
		if (Validations.validateIsNotNull(processedServiceTo.getBillingServiceTo())){
			
			if (Validations.validateIsNotNull(processedServiceTo.getBillingServiceTo().getIdBillingServicePk())){
				query.setParameter("idBillingServicePk",processedServiceTo.getBillingServiceTo().getIdBillingServicePk());
			}
			
			if (Validations.validateIsNotNull(processedServiceTo.getBillingServiceTo().getInIntegratedBill())){
				query.setParameter("inIntegratedBill",processedServiceTo.getBillingServiceTo().getInIntegratedBill());
			}
		
		}
		
		
		if (Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch())){
			
			if (Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getKeyEntityCollection())){
				query.setParameter("keyEntityCollection",processedServiceTo.getCollectionRecordToSearch().getKeyEntityCollection());				
			}
			
			if (Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getRecordState())){		
				query.setParameter("recordState",processedServiceTo.getCollectionRecordToSearch().getRecordState());
			}
							
			if (Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getIndBilled())){
				query.setParameter("indBilled",processedServiceTo.getCollectionRecordToSearch().getIndBilled());
			}
			

			if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getHolder())){
				query.setParameter("idHolderPk",processedServiceTo.getCollectionRecordToSearch().getHolder().getIdHolderPk());
				
			}
				
			if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getParticipant())){
				query.setParameter("idParticipantPk",processedServiceTo.getCollectionRecordToSearch().getParticipant().getIdParticipantPk());
			
			}
			
			if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getIssuer())){
				query.setParameter("idIssuerPk",processedServiceTo.getCollectionRecordToSearch().getIssuer().getIdIssuerPk());
				
			}
			
			if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getEntityCollection())){
				query.setParameter("entityCollectionId",processedServiceTo.getCollectionRecordToSearch().getEntityCollection());
				
			}
			
			if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getDocumentType())){
				query.setParameter("documentType",processedServiceTo.getCollectionRecordToSearch().getDocumentType());				
			}
			
			if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getOtherEntityDocument())){
				query.setParameter("otherEntityDocument",processedServiceTo.getCollectionRecordToSearch().getOtherEntityDocument());
			
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(processedServiceTo.getCollectionRecordToSearch().getIdHolderByParticipant())){
				query.setParameter("idHolderByParticipant",processedServiceTo.getCollectionRecordToSearch().getIdHolderByParticipant());
			}
			
		}
		
		
		List<CollectionRecord> listCollectionRecord = query.getResultList();
		
		return listCollectionRecord;
	}
	
	/**
	 * Find Processed Service By Entity.
	 *
	 * @param processedServiceTo the processed service to
	 * @return the processed service
	 */
	public ProcessedService findProcessedServiceByEntity(ProcessedServiceTo processedServiceTo){
		
		ProcessedService processedService =null; 
		
		if(Validations.validateIsNotNullAndNotEmpty(processedServiceTo)){
			StringBuilder sbQuery = new StringBuilder();
			
			sbQuery.append("Select ps From ProcessedService ps ");
			sbQuery.append(" inner join  ps.billingService bs inner join ps.collectionRecords crs ");
			sbQuery.append(" Where 1 = 1  ");
			
			if(Validations.validateIsNotNull(processedServiceTo.getBillingServiceTo().getIdBillingServicePk())){
				sbQuery.append(" And bs.idBillingServicePk = :idBillingServicePk ");
			}
			
			if(Validations.validateIsNotNull(processedServiceTo.getCalculationDate())){
				sbQuery.append(" And ps.calculationDate = :calculationDate ");
			}	
			
			if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch())){
				
				if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getHolder())){
					sbQuery.append(" And crs.holder.idHolderPk = :idHolderPk ");
				}
				
				if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getHolderAccount())){
					sbQuery.append(" And crs.holderAccount.idHolderAccountPk = :idHolderAccountPk ");
				}
				

				if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getParticipant())){
					sbQuery.append(" And crs.participant.idParticipantPk = :idParticipantPk ");
				}
				
				if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getIssuer())){
					sbQuery.append(" AND crs.issuer.idIssuerPk = :idIssuerPk ");
				}
				
			}
			
			if(Validations.validateIsNotNull(processedServiceTo.getProcessedState())){
				sbQuery.append(" And ps.processedState = :processedState ");
			}	
			
			
			Query query = em.createQuery(sbQuery.toString());
			
			if(Validations.validateIsNotNull(processedServiceTo.getBillingServiceTo().getIdBillingServicePk())){
				query.setParameter("idBillingServicePk", processedServiceTo.getBillingServiceTo().getIdBillingServicePk());
			}
			
			if(Validations.validateIsNotNull(processedServiceTo.getCalculationDate())){
				query.setParameter("calculationDate", processedServiceTo.getCalculationDate());
			}	
			
			if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch())){
				
				if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getHolder())){
					query.setParameter("idHolderPk", processedServiceTo.getCollectionRecordToSearch().getHolder().getIdHolderPk());	
					
				}
				
				if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getHolderAccount())){
					query.setParameter("idHolderAccountPk", processedServiceTo.getCollectionRecordToSearch().getHolderAccount().getIdHolderAccountPk());	
					
				}
				
				if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getParticipant())){
					query.setParameter("idParticipantPk", processedServiceTo.getCollectionRecordToSearch().getParticipant().getIdParticipantPk());					
				}
				
				if(Validations.validateIsNotNull(processedServiceTo.getCollectionRecordToSearch().getIssuer())){
					query.setParameter("idIssuerPk", processedServiceTo.getCollectionRecordToSearch().getIssuer().getIdIssuerPk());
					
				}
				
			}
			
			if(Validations.validateIsNotNull(processedServiceTo.getProcessedState())){
				query.setParameter("processedState", processedServiceTo.getProcessedState());
			}

			try{
				Object entity=query.getSingleResult();
				processedService=(ProcessedService)entity;
			} catch(NoResultException  nrex){
				processedService=null;
			}
		}
		
		return processedService;
		
	}
	
	
			
}
