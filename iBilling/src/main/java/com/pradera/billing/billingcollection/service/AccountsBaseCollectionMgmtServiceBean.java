package com.pradera.billing.billingcollection.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.Query;

import org.hibernate.exception.SQLGrammarException;

import com.pradera.billing.billingexception.service.ExceptionLayerServiceBean;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.billing.BillingService;

@Stateless
@Performance
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class AccountsBaseCollectionMgmtServiceBean extends CrudDaoServiceBean{

	/** The exception layer service bean. */
	@EJB
	ExceptionLayerServiceBean exceptionLayerServiceBean;
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/**
	 * SERVICIO A1 Registro de Depositante, por cuenta propia y de terceros
	 *
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 * @see com.pradera.model.billing.type.BaseCollectionType
	 */
	public List<Object> findParticipantForCreation(Map<String, Object> parameters, Integer participantType, Integer indManagedThird) throws ServiceException {

		StringBuilder strTableComponentBegin = new StringBuilder();

		/** getting data for do merging into dynamic query **/
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		Date calculateDate = (Date) parameters.get(GeneralConstants.CALCULATION_DATE);

		StringBuilder stringBuilderSqlBase = new StringBuilder();
		stringBuilderSqlBase.append(" SELECT  p.idParticipantPk ");
		stringBuilderSqlBase.append("  FROM Participant p ");
		stringBuilderSqlBase.append(" WHERE p.state = " + ParticipantStateType.REGISTERED.getCode());
		stringBuilderSqlBase.append("  AND p.accountType =" + participantType);
		stringBuilderSqlBase.append("  AND p.indManagedThird =" + indManagedThird);
		stringBuilderSqlBase.append("  AND p.accountsState =" + BooleanType.NO.getCode());
		/** Only Active ***/

		if (PropertiesConstants.PERIOD_CALCULATION_DAILY_PK.equals(billingService.getCalculationPeriod())) {

			stringBuilderSqlBase.append(" and TRUNC(p.registryDate)=TRUNC(:dateCalculationPeriod) ");

		} else if (PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK.equals(billingService.getCalculationPeriod())) {

			stringBuilderSqlBase
					.append(" and YEAR(p.registryDate)=YEAR(:dateCalculationPeriod) and MONTH(p.registryDate)=MONTH(:dateCalculationPeriod)");

		} else if (PropertiesConstants.PERIOD_CALCULATION_YEARLY_PK.equals(billingService.getCalculationPeriod())) {
			/** Se cobra al cierre del cobro del periodo respectivo */
			stringBuilderSqlBase.append(
					" and YEAR(p.registryDate)=YEAR(:dateCalculationPeriod)  ");

		}

		/** Making the table EXCEPTION **/
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters,
				stringBuilderSqlBase);

		log.debug(
				" :::::::::::::::::::::::::::::::::::::Consulta Transaccion con Reglas de Excepcion ::::::::::::::::::::::::::::");
		log.debug(strTableComponentBegin.toString());

		Query query = em.createQuery(strTableComponentBegin.toString());

		query.setParameter("dateCalculationPeriod", calculateDate);

		List<Object> listObject = null;
		try {

			listObject = query.getResultList();

		} catch (Exception ex) {
			if (ex.getCause() instanceof SQLGrammarException) {
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}

		}

		return listObject;
	}
}
