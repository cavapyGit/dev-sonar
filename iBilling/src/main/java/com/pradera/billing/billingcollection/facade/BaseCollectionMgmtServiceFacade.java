package com.pradera.billing.billingcollection.facade;


import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

import com.pradera.billing.billingcollection.service.AccountsBaseCollectionMgmtServiceBean;
import com.pradera.billing.billingcollection.service.BaseCollectionMgmtServiceBean;
import com.pradera.billing.billingcollection.to.CollectionRecordDetailTo;
import com.pradera.billing.billingcollection.to.CollectionRecordTo;
import com.pradera.billing.billingexception.service.ExceptionLayerServiceBean;
import com.pradera.billing.billingexception.to.IssuerTo;
import com.pradera.billing.billingservice.facade.BillingServiceMgmtServiceFacade;
import com.pradera.billing.billingservice.to.BillingServiceTo;
import com.pradera.billing.billingsource.to.BillingServiceSourceInfo;
import com.pradera.billing.billingsource.to.BillingServiceSourceLoggerTo;
import com.pradera.billing.common.facade.BusinessLayerCommonFacade;
import com.pradera.billing.common.utils.BillingUtils;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.ParticipantType;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.BillingServiceSource;
import com.pradera.model.billing.ServiceRate;
import com.pradera.model.billing.ServiceRateScale;
import com.pradera.model.billing.type.BaseCollectionType;
import com.pradera.model.billing.type.EntityCollectionType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.DailyExchangeRoleType;
import com.pradera.model.process.BusinessProcess;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BaseCollectionMgmtServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class BaseCollectionMgmtServiceFacade {
	
	 /** The log. */
 	@Inject
	 PraderaLogger log;
	
	 /** The base collection mgmt service bean. */
 	@EJB
	 BaseCollectionMgmtServiceBean 		baseCollectionMgmtServiceBean;

	 /** The base collection mgmt service bean. */
	@EJB
	AccountsBaseCollectionMgmtServiceBean 		accountsBaseCollectionMgmtServiceBean;
	
	
	 /** The business layer common facade. */
 	@EJB
	 BusinessLayerCommonFacade 			businessLayerCommonFacade;
	 
	 /** The transaction registry. */
 	@Resource
	 TransactionSynchronizationRegistry transactionRegistry;

	 /** The helper component facade. */
 	@EJB 
	 HelperComponentFacade 				helperComponentFacade;
	
	 /** The collection service mgmt service facade. */
 	@EJB
	 CollectionServiceMgmtServiceFacade collectionServiceMgmtServiceFacade;
	 
	 /** The exception layer service bean. */
 	@EJB
	 ExceptionLayerServiceBean			exceptionLayerServiceBean;
	 
	 /** The billing service mgmt service facade. */
 	@EJB
	 BillingServiceMgmtServiceFacade 	billingServiceMgmtServiceFacade;
	 
	 /** The message configuration. */
 	@Inject
 	@InjectableResource(location = "Messages_es.properties")
	 private Properties messageConfiguration;

	 /** The billing service source info. */
 	BillingServiceSourceInfo			billingServiceSourceInfo=new BillingServiceSourceInfo();
	 
	 /** The list billing service source logger to. */
 	List<BillingServiceSourceLoggerTo> listBillingServiceSourceLoggerTo=null;
	 
 	@EJB
	BaseCollectionMgmtServiceFacade baseCollectionMgmtServiceFacade;
 	
	/**
	 * 		Calculate monthly.
	 *
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public  List<CollectionRecordTo>  findEntitiesForQuotaAnnual(Map parameters ) throws ServiceException {
		
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);	
		
		List<Object> dataMaps= null;
		
		/**
		 * Si existe Fuente, se procesa el archivo XML
		 */
		if(Validations.validateIsNotNullAndPositive(billingService.getIndSourceBaseCollection())){
			
			return businessLayerCommonFacade.executeFileBaseCollection(new BillingServiceTo(billingService),parameters);
			
		}else{
			if (billingService.getEntityCollection().equals(EntityCollectionType.PARTICIPANTS.getCode())) {
				if (billingService.getServiceCode().equals("50")) {
					dataMaps= baseCollectionMgmtServiceBean.findParticipantsForMatrixAccountMaintenance(parameters);
				} else if (billingService.getServiceCode().equals("51")) {
					dataMaps= baseCollectionMgmtServiceBean.findParticipantsForIssuingAccountMaintenance(parameters);
				} else {
					dataMaps= baseCollectionMgmtServiceBean.findParticipantsForCreateAccount(parameters);
				}
			}
			else if (billingService.getEntityCollection().equals(EntityCollectionType.ISSUERS.getCode())) {
				dataMaps= baseCollectionMgmtServiceBean.findIssuersForCreateAccount(parameters);
			}
			
		}		
		List<CollectionRecordTo> collectionRecordToList=null;
		
		
			 if (Validations.validateListIsNotNullAndNotEmpty(dataMaps)){
				 collectionRecordToList = new ArrayList<CollectionRecordTo>();							
				 CollectionRecordTo collectionRecordTo =null;	
				 
				 		for (Object object : dataMaps) {
				     					
						Object     entity         			= object;
						
						 /** pointer if exist a entity ,initialized at ZERO **/ 
						Integer existEntity = 0; 
						/**FIND COLLECTION RECORD AT  LIST***/	
						for (int i = 0; i < collectionRecordToList.size(); i++) {
							
							if(baseCollectionMgmtServiceBean.isPaymentByparticipant(billingService)){
								entity = baseCollectionMgmtServiceBean.getIdParticipantPkByIdIssuerPk(entity.toString());
							}
							if (collectionRecordToList.get(i).existEntityCollection(entity, billingService.getEntityCollection())) {
								existEntity=i;
								break;							
							}
	
						}
						
						
						if (existEntity==0){
							collectionRecordTo = new CollectionRecordTo();
							if(baseCollectionMgmtServiceBean.isPaymentByparticipant(billingService)){
								entity = baseCollectionMgmtServiceBean.getIdParticipantPkByIdIssuerPk(entity.toString());
								collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
							}
							else{
								collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
							}
							collectionRecordToList.add(collectionRecordTo);	
							
						}
		
			       }
			 }
		 return collectionRecordToList;
	}
	
	/**
	 * 		Calculate monthly.
	 *
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public  List<CollectionRecordTo>  findHolderForQuota(Map parameters ) throws ServiceException {
		
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);	
		
		List<Object> dataMaps= null;
		
		/*
		if (billingService.getEntityCollection().equals(EntityCollectionType.HOLDERS.getCode())) {
			dataMaps= baseCollectionMgmtServiceBean.findHolderForCreation(parameters);
		}
		*/
	
		dataMaps= baseCollectionMgmtServiceBean.findHolderForCreation(parameters);
		
		List<CollectionRecordTo> collectionRecordToList=null;			
		
			 if (Validations.validateListIsNotNullAndNotEmpty(dataMaps)){
				 collectionRecordToList = new ArrayList<CollectionRecordTo>();							
				 CollectionRecordTo collectionRecordTo =null;	
				 
				 		for (Object object : dataMaps) {
				     					
						Object     entity         			= object;
						
						 /** pointer if exist a entity ,initialized at ZERO **/ 
						Integer existEntity = 0; 
						/**FIND COLLECTION RECORD AT  LIST***/	
						for (int i = 0; i < collectionRecordToList.size(); i++) {
							if(baseCollectionMgmtServiceBean.isPaymentByparticipant(billingService)){
								entity = baseCollectionMgmtServiceBean.getIdParticipantPkByIdIssuerPk(entity.toString());
							}
							if (collectionRecordToList.get(i).existEntityCollection(entity, billingService.getEntityCollection())) {
								existEntity=i;
								break;							
							}
	
						}
						
						
						if (existEntity==0){
							collectionRecordTo = new CollectionRecordTo();
							if(baseCollectionMgmtServiceBean.isPaymentByparticipant(billingService)){
								entity = baseCollectionMgmtServiceBean.getIdParticipantPkByIdIssuerPk(entity.toString());
								collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
							}
							else{
								collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
							}						
							collectionRecordToList.add(collectionRecordTo);	
							
						}
		
			       }
			 
			 }
			 
		 return collectionRecordToList;
	}
	 
		
	 
	/**
	 * Calculate monthly.
	 *
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public  List<CollectionRecordTo>  findSecurityForQuota(Map parameters ) throws ServiceException {
		
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);	
		
		List<Object> dataMaps= null;
		
		if (billingService.getEntityCollection().equals(EntityCollectionType.ISSUERS.getCode())) {
			dataMaps= baseCollectionMgmtServiceBean.findSecurityForCreation(parameters);
		}
		
		
		List<CollectionRecordTo> collectionRecordToList=null;			
		
			 if (Validations.validateListIsNotNullAndNotEmpty(dataMaps)){
				 collectionRecordToList = new ArrayList<CollectionRecordTo>();							
				 CollectionRecordTo collectionRecordTo =null;	
				 
				 		for (Object object : dataMaps) {
				     					
						Object     entity         			= object;
						
						 /** pointer if exist a entity ,initialized at ZERO **/ 
						Integer existEntity = 0; 
						/**FIND COLLECTION RECORD AT  LIST***/	
						for (int i = 0; i < collectionRecordToList.size(); i++) {
							
							/*COBRO A EMISORES Y FACTURACION A PARTICIPANTES*/
							if(baseCollectionMgmtServiceBean.isPaymentByparticipant(billingService)){
								entity = baseCollectionMgmtServiceBean.getIdParticipantPkByIdIssuerPk(entity.toString());
							}
							if (collectionRecordToList.get(i).existEntityCollection(entity, billingService.getEntityCollection())) {
								existEntity=i;
								break;							
							}
	
						}
						
						
						if (existEntity==0){
							collectionRecordTo = new CollectionRecordTo();
							if(baseCollectionMgmtServiceBean.isPaymentByparticipant(billingService)){
								entity = baseCollectionMgmtServiceBean.getIdParticipantPkByIdIssuerPk(entity.toString());
								collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
							}
							else{
								collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
							}						
							collectionRecordToList.add(collectionRecordTo);	
							
						}
		
			       }
			 
			 }
			 
		 return collectionRecordToList;
	}
	
	/**
	 * EDV 15
	 * ISSUANCE_CAT
	 * Calculate monthly.
	 *
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public  List<CollectionRecordTo>  findAccreditationCertificate(Map parameters ) throws ServiceException {
	
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);	
		CollectionRecordDetailTo collectionRecordDetailTo = null;
		List<Object> dataMaps= null;
		
		/**
		 * Si existe Fuente, se procesa el archivo XML
		 */
		if(Validations.validateIsNotNullAndPositive(billingService.getIndSourceBaseCollection())){
			
			return businessLayerCommonFacade.executeFileBaseCollection(new BillingServiceTo(billingService),parameters);
			
		}else{
			
			dataMaps= processAccreditationCertificate(parameters);
		}
	
	
	
		List<CollectionRecordTo> collectionRecordToList=null;
	
	
		 if (Validations.validateListIsNotNullAndNotEmpty(dataMaps)){
			 collectionRecordToList = new ArrayList<CollectionRecordTo>();
			 
			 		for (Object object : dataMaps) {
			 				
					Object  entity         		= ((Object[])object)[0];
					String  security       		= (String)((Object[])object)[1].toString();
					Date 	operationDate		= (Date)(( (Object[])object )[2]);
					Long 	operationNumber 	= Long.parseLong((((Object[])object)[3]).toString());
					Integer securityClass		= Integer.parseInt(((BigDecimal)((Object[])object)[4]).toString());
					Long 	idHolderPk 		 	= Long.parseLong((((Object[])object)[5]).toString());
					String  issuance       		= (String)((Object[])object)[6].toString();
				    Long 	holderAccount		= Long.parseLong(((Object[])object)[7].toString());
				    String 		mnemonicHolder		= ((Object[])object)[8]!=null?((Object[])object)[8].toString():null;
				    Integer 	economicActivity	= (BigDecimal)((Object[])object)[9]!=null?Integer.parseInt(((BigDecimal)((Object[])object)[9]).toString()):null;
				    Integer 	portfolio			= (BigDecimal)((Object[])object)[10]!=null?Integer.parseInt(((BigDecimal)((Object[])object)[10]).toString()):null;
					
					
					collectionRecordDetailTo = new CollectionRecordDetailTo();
					collectionRecordDetailTo.setIdSecurityCodeFk(security);
					collectionRecordDetailTo.setSecurityClass(securityClass);
					collectionRecordDetailTo.setIdHolderPk(idHolderPk);
					collectionRecordDetailTo.setIdIssuanceCodePk(issuance);
					collectionRecordDetailTo.setCalculationDate(operationDate);
					collectionRecordDetailTo.setOperationNumber(operationNumber);
					collectionRecordDetailTo.setTmpRequest(operationNumber.toString());
					collectionRecordDetailTo.setHolderAccount(holderAccount);
					collectionRecordDetailTo.setMnemonicHolder(mnemonicHolder);
					collectionRecordDetailTo.setEconomicActivityParticipant(economicActivity);
					collectionRecordDetailTo.setPortfolio(portfolio);
					CollectionRecordTo collectionRecordTo = findEntityAtListTemporalTo(collectionRecordToList, entity, billingService);
					if (collectionRecordTo==null){
						collectionRecordTo = new CollectionRecordTo();
						if(baseCollectionMgmtServiceBean.isPaymentByparticipant(billingService)){
							entity = baseCollectionMgmtServiceBean.getIdParticipantPkByIdIssuerPk(entity.toString());
							collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
						}
						else{
							collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
						}
						
						List<CollectionRecordDetailTo> collectionRecordDetailToList = new  ArrayList<CollectionRecordDetailTo>();
						collectionRecordDetailToList.add(collectionRecordDetailTo);	
						collectionRecordToList.add(collectionRecordTo);	
						collectionRecordTo.setCollectionRecordDetailToList(collectionRecordDetailToList);
					}else{
						collectionRecordTo.getCollectionRecordDetailToList().add(collectionRecordDetailTo);
					}

	
		       }
		 
		 }
		 
		 return collectionRecordToList;
	}
	 

	/**
	 * Staggered Accumulative.
	 *
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.DAILY_COLLECT_SERVICE_CALCULATION)
	public List<CollectionRecordTo>  securitiesTransferStaggeredAccumulative(Map parameters) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());	    
	    
	    BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);	

	    List<CollectionRecordTo> collectionRecordToFinalList = null;
		    
	    /*
	     if( billingService.getBaseCollection().equals(BaseCollectionType.INDIRECT_PARTICIPANT.getCode())||
					billingService.getBaseCollection().equals(BaseCollectionType.SETTLEMENT_OTC_TRANSACTIONS_VARIABLE_INCOME.getCode()) ||
					billingService.getBaseCollection().equals(BaseCollectionType.TRANSFERS_SECURITIES_SIRTEX_FIXED_INCOME.getCode()) ||
					billingService.getBaseCollection().equals(BaseCollectionType.TRANSFERS_SECURITIES_SIRTEX_VARIABLE_INCOME.getCode()) ||
					billingService.getBaseCollection().equals(BaseCollectionType.RECORD_SECURITIES_DEMATERIALIZED_FIXED_INCOME.getCode()) ||
					billingService.getBaseCollection().equals(BaseCollectionType.RECORD_SECURITIES_DEMATERIALIZED_VARIABLE_INCOME.getCode()) ||
					billingService.getBaseCollection().equals(BaseCollectionType.PAYING_AGENT.getCode())||
					billingService.getBaseCollection().equals(BaseCollectionType.TRANSACTION_CHARGE.getCode())||
					billingService.getBaseCollection().equals(BaseCollectionType.REMATERIALIZATION_OR_REVERSAL_ACCOUNT_ENTRIES_VARIABLE_INCOME.getCode())||
					billingService.getBaseCollection().equals(BaseCollectionType.RECORD_BOOK_ENTRY_PHYSICAL_CUSTODY.getCode())
	 				){
	    	 
	    	 	collectionRecordToFinalList= processingScaleToDetailCollection(parameters);
	    */
	   
		if (billingService.getBaseCollection().equals(BaseCollectionType.RECORD_BOOK_ENTRY_PHYSICAL_CUSTODY_USD.getCode())
				|| billingService.getBaseCollection().equals(BaseCollectionType.RECORD_BOOK_ENTRY_PHYSICAL_CUSTODY_LOCAL.getCode())
				|| billingService.getBaseCollection().equals(BaseCollectionType.RECORD_BOOK_ENTRY_DESMATERIALIZED_CUSTODY.getCode())) {
			collectionRecordToFinalList = processingScaleToDetailCollection(parameters);
		} else if (BaseCollectionType.RECORD_SECURITIES_DEMATERIALIZED_DPF.getCode()
				.equals(billingService.getBaseCollection())
				|| BillingUtils.isRegisterOwnerChange(billingService.getBaseCollection())
				|| BillingUtils.isRequestAffectations(billingService.getBaseCollection())) {
			/**
			 * 1) Add the Collection and applied scale 2) In the Calculation
			 */
			collectionRecordToFinalList = processingScaleToCollection(parameters);
		}
		
	     return collectionRecordToFinalList;
	}
	
	

	
	/**
	 * Securities transfer sirtex not accumulative.
	 *
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.DAILY_COLLECT_SERVICE_CALCULATION)
	public List<CollectionRecordTo>  securitiesTransferSirtexNotAccumulative(Map parameters) throws ServiceException {
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());	    
	    
	    BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);	
	    /***
	     * Getting Movements
	     */
	    List<CollectionRecordTo> collectionRecordToList = (List<CollectionRecordTo>) parameters.get(GeneralConstants.COLLECTION_RECORD_MOVEMENT);
	    
	    
	    List<CollectionRecordTo> collectionRecordToFinalList = null;
		    
	     if( billingService.getBaseCollection().equals(BaseCollectionType.TRANSFERS_SECURITIES_SIRTEX_FIXED_INCOME.getCode()) ){
			
	    	 /**
	    	  * Obtain Movements 
	    	  */
			
	    	 if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordToList)){
					
					ServiceRate serviceRate=(ServiceRate) parameters.get(GeneralConstants.SERVICE_RATE);				
					List<ServiceRateScale> serviceRateScales = serviceRate.getServiceRateScales();
					
					BigDecimal userAccount = BigDecimal.ZERO;
					
					collectionRecordToFinalList = new ArrayList<CollectionRecordTo>();
					
					
					for (Iterator<CollectionRecordTo> iter=collectionRecordToList.iterator(); iter.hasNext();) {
					
						CollectionRecordTo collectionRecordTo = (CollectionRecordTo)iter.next();
						
						ServiceRateScale serviceRateScaleSelected	=	null;
						BigDecimal amount  = BigDecimal.ZERO;
						
						for (Iterator<ServiceRateScale> iterScale = serviceRateScales.iterator(); iterScale.hasNext();) {
							
							ServiceRateScale serviceRateScale = (ServiceRateScale)iterScale.next();	
							
							userAccount = new BigDecimal(collectionRecordTo.getUserCount().toString());
							
							if ( (userAccount.compareTo(serviceRateScale.getMinimumRange())>=0) &&
								 (userAccount.compareTo(serviceRateScale.getMaximumRange())<=0) ){
								
								serviceRateScaleSelected = serviceRateScale;
								break;
							}
						}													
						
						if (Validations.validateIsNotNull(serviceRateScaleSelected)){
							CollectionRecordTo collectionRecordToNew	= SerializationUtils.clone(collectionRecordTo);
							CollectionRecordDetailTo collectionRecordDetailTo =  new CollectionRecordDetailTo();						
							amount =BillingUtils.addTwoDecimal(serviceRateScaleSelected.getScaleAmount(),amount , 
																GeneralConstants.ROUNDING_DIGIT_NUMBER);
							collectionRecordDetailTo.setAmount(amount);
							
							List<CollectionRecordDetailTo> collectionRecordDetailToList = new  ArrayList<CollectionRecordDetailTo>();																		
							collectionRecordDetailToList.add(collectionRecordDetailTo);											
							collectionRecordToNew.setCollectionRecordDetailToList(collectionRecordDetailToList);
							
							collectionRecordToFinalList.add(collectionRecordToNew);
						}
						else if (Validations.validateIsNull(serviceRateScaleSelected)){
							/**Here send notification when there is the stair*/
							
							CollectionRecordTo collectionRecordToNew	= SerializationUtils.clone(collectionRecordTo);	
							String descriptionEntity=null;
							
							BusinessProcess businessProcess = new BusinessProcess();
							businessProcess.setIdBusinessProcessPk(BusinessProcessType.MONTHLY_COLLECT_SERVICE_CALCULATION.getCode());
						
							String subject = messageConfiguration.getProperty("header.message.warning");
	 						descriptionEntity = getDescriptionNameEntityCollection(collectionRecordTo);
							
							String message = messageConfiguration.getProperty("warning.calculation.service.access.billing");					 
							message = MessageFormat.format(message, 
										EntityCollectionType.get(collectionRecordToNew.getEntityCollection()).getValue(),//0
										descriptionEntity,//1
										userAccount,//2
										serviceRate.getRateCode(), //3
										serviceRate.getRateName(), //4
										billingService.getServiceCode()); //5
							List<UserAccountSession> listUser
							=this.businessLayerCommonFacade.getListUsserByBusinessProcess(BusinessProcessType.MONTHLY_COLLECT_SERVICE_CALCULATION.getCode());
							businessLayerCommonFacade.sendNotificationManual(businessProcess,listUser, subject, message, NotificationType.EMAIL);
							
						}
							
						
					}
				  }
			
			

		  }
		
	     return collectionRecordToFinalList;
	}
	 
	 
	 /**
 	 * Servicio 8.
 	 *
 	 * @param parameters the parameters
 	 * @param idInstrumentType the id instrument type
 	 * @param idNegotiationMechanism the id negotiation mechanism
 	 * @return the list
 	 * @throws ServiceException the service exception
 	 */
	public List<Object>  findMovementSecuritiesTransferSirtex(Map parameters,Integer idInstrumentType,Long idNegotiationMechanism) throws ServiceException {
		
		return baseCollectionMgmtServiceBean.findMovementSecuritiesTransferSirtex(parameters,idInstrumentType, idNegotiationMechanism);				
	}
	
	/**
	 * Servicio 9.
	 *
	 * @param parameters the parameters
	 * @param idInstrumentType the id instrument type
	 * @param idNegotiationMechanism the id negotiation mechanism
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Object>  findMovementPrivateSecuritiesTransferOperationsOtc(Map parameters,Integer idInstrumentType,Long idNegotiationMechanism) throws ServiceException {
		
		return baseCollectionMgmtServiceBean.findMovementPrivateSecuritiesTransferOperationsOtc(parameters,idInstrumentType, idNegotiationMechanism);				
	}

	/**
	 * Servicio 10.
	 *
	 * @param parameters the parameters
	 * @param idInstrumentType the id instrument type
	 * @param idNegotiationMechanism the id negotiation mechanism
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Object>  findIssuanceDematerializedAnnotationOnAccount(Map parameters,Integer idInstrumentType,Long idNegotiationMechanism) throws ServiceException {
		
		return baseCollectionMgmtServiceBean.findIssuanceDematerializedAnnotationOnAccount(parameters,idInstrumentType, null);				
	}
	
	// 1. CUSTODIA FISICA
	public List<Object>  findBalancePhysicalCustody(Map parameters) throws ServiceException {		
		return baseCollectionMgmtServiceBean.findBalancePhysicalCustody(parameters);				
	}
	
	// 2. CUSTODIA ELECTRONICA
	public List<Object>  findBalanceSecurityAnnotationOnAccountDesmaterialized(Map parameters) throws ServiceException {		
		return baseCollectionMgmtServiceBean.findBalanceSecurityAnnotationOnAccountDesmaterialized(parameters);				
	}
	
	
	/**
	 * Servicio 10 DPF.
	 *
	 * @param parameters the parameters
	 * @param idInstrumentType the id instrument type
	 * @param idNegotiationMechanism the id negotiation mechanism
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Object>  findIssuanceDematerializedAnnotationDpf(Map parameters,Integer idInstrumentType,Long idNegotiationMechanism) throws ServiceException {
		
		return baseCollectionMgmtServiceBean.findIssuanceDematerializedAnnotationDpf(parameters,idInstrumentType, null);				
	}
	
	/**
	 * Servicio 11*.
	 *
	 * @param parameters the parameters
	 * @param idInstrumentType the id instrument type
	 * @param idNegotiationMechanism the id negotiation mechanism
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Object>  findMovementSecuritiesChangeOwnerShip(Map parameters) throws ServiceException {
		
		return baseCollectionMgmtServiceBean.findMovementSecuritiesChangeOwnerShip(parameters);				
	}
	 
	/**
	 * Servicio 15 CAT Certificate Accreditation*.
	 *
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Object>  processAccreditationCertificate(Map parameters) throws ServiceException {
		
		return baseCollectionMgmtServiceBean.processAccreditationCertificate(parameters);				
	}
	
	/**
	 * Servicio 16 Agente Pagador*.
	 *
	 * @param parameters the parameters
	 * @param idInstrumentType the id instrument type
	 * @param idNegotiationMechanism the id negotiation mechanism
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Object>  findPayingAgent(Map parameters,Integer idInstrumentType,Long idNegotiationMechanism) throws ServiceException {
		
		return baseCollectionMgmtServiceBean.findPayingAgent(parameters, idInstrumentType, idNegotiationMechanism);				
	}
	
	/**
	 * Servicio 17.1 Afectaciones *
	 *
	 * @param parameters the parameters
	 * @param idInstrumentType the id instrument type
	 * @param idNegotiationMechanism the id negotiation mechanism
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Object>  findMovementPawnBlockAffectation(Map parameters,Integer idInstrumentType,Long idNegotiationMechanism) throws ServiceException {
		
		return baseCollectionMgmtServiceBean.findMovementPawnBlockAffectation(parameters);				
	}
	
	/**
	 * Servicio 17.2 Desafectaciones *
	 *
	 * @param parameters the parameters
	 * @param idInstrumentType the id instrument type
	 * @param idNegotiationMechanism the id negotiation mechanism
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Object>  findMovementReversalAffectation(Map parameters,Integer idInstrumentType,Long idNegotiationMechanism) throws ServiceException {
		
		return baseCollectionMgmtServiceBean.findMovementReversalAffectation(parameters);				
	}
	
	/**
	 * Servicio 18*.
	 *
	 * @param parameters the parameters
	 * @param idInstrumentType the id instrument type
	 * @param idNegotiationMechanism the id negotiation mechanism
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Object>  findMovementReversionAnnotationIntoAccount(Map parameters,Integer idInstrumentType,Long idNegotiationMechanism) throws ServiceException {
		
		return baseCollectionMgmtServiceBean.findMovementReversionAnnotationIntoAccount(parameters,idInstrumentType, idNegotiationMechanism);				
	}
	 

	
	/**
	 * Find movement settlement with mechanism by entity collection.
	 *
	 * @param parameters the parameters
	 * @param idInstrumentType the id instrument type
	 * @param idNegotiationMechanism the id negotiation mechanism
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Object>  findMovementSettlementWithMechanismByEntityCollection(Map parameters,Integer idInstrumentType,Long idNegotiationMechanism) throws ServiceException {
		
		return baseCollectionMgmtServiceBean.findMovementSettlementWithMechanismByEntityCollection(parameters,idInstrumentType, idNegotiationMechanism);				
	}
	
	
	
	
	/**
	 * *
	 * EDV 1
	 * REGISTRO_VALORES_ANOTACIONES_CUENTA_R_FIJA
	 * REGISTRO_VALORES_ANOTACIONES_CUENTA_R_VARIABLE
	 * REGISTRO_VALORES_ANOTACIONES_CUENTA_R_FIJA -AFP
	 * REGISTRO_VALORES_ANOTACIONES_CUENTA_R_VARIABLE -AFP.
	 *
	 * @param parameters the parameters
	 * @param idInstrumentType the id instrument type
	 * @return the list
	 * @throws ServiceException the service exception
	 * @see com.pradera.model.billing.type.BaseCollectionType
	 */
	public List<CollectionRecordTo>  findBalanceSecurityAnnotationOnAccount(Map parameters, Integer idInstrumentType) throws ServiceException {		
		List<CollectionRecordTo> collectionRecordToList=null;	
		CollectionRecordDetailTo collectionRecordDetailTo = null;
		List<Object> dataMaps=null;			
		Date calculateDate= (Date) parameters.get(GeneralConstants.CALCULATION_DATE);
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		BillingServiceSource billingServiceSource=null;
		Integer baseCollection=billingService.getBaseCollection();
		/**
		 * Si existe Fuente, se procesa el archivo XML
		 */
		if(Validations.validateIsNotNullAndPositive(billingService.getIndSourceBaseCollection())){	
			return businessLayerCommonFacade.executeFileBaseCollection(new BillingServiceTo(billingService),parameters);			
		}else{
			/**
			 * El cobro embebido
			 */
			if(BaseCollectionType.RECORD_BOOK_ENTRY_PHYSICAL_CUSTODY_USD.getCode().equals(baseCollection))
					//|| BaseCollectionType.RECORD_BOOK_ENTRY_SECURITIES_VARIABLE_INCOME.getCode().equals(baseCollection))
					{				
				dataMaps= baseCollectionMgmtServiceBean.findBalanceSecurityAnnotationOnAccount(parameters, CurrencyType.USD.getCode());	
			} else if(BaseCollectionType.RECORD_BOOK_ENTRY_PHYSICAL_CUSTODY_LOCAL.getCode().equals(baseCollection)) {
				dataMaps= baseCollectionMgmtServiceBean.findBalanceSecurityAnnotationOnAccount(parameters, CurrencyType.PYG.getCode());	
			} else if(BaseCollectionType.RECORD_BOOK_ENTRY_DESMATERIALIZED_CUSTODY.getCode().equals(baseCollection)) {				
				dataMaps= baseCollectionMgmtServiceBean.findBalanceSecurityAnnotationOnAccountDesmaterialized(parameters);
			}	

			//TODO REMOVE ABAJO
			//else if(
			//		BaseCollectionType.RECORD_BOOK_ENTRY_SECURITIES_FIXED_INCOME_AFP.getCode().equals(baseCollection)||
			//		BaseCollectionType.RECORD_BOOK_ENTRY_SECURITIES_VARIABLE_INCOME_AFP.getCode().equals(baseCollection)){				
			//	dataMaps= baseCollectionMgmtServiceBean.findBalanceSecurityAnnotationOnAccountAfp(parameters,idInstrumentType);
			//}			
		}
		 BigDecimal referencePrice = BigDecimal.ZERO;
		 BigDecimal realAmount = BigDecimal.ZERO;
		 if (Validations.validateListIsNotNullAndNotEmpty(dataMaps)){
			 collectionRecordToList = new ArrayList<CollectionRecordTo>();
			 	for (Object object : dataMaps) {
					Object     	entity         		= ((Object[])object)[0];
					BigDecimal 	amount	   			= ((BigDecimal)((Object[])object)[1]);
					Integer    	currency          	= Integer.parseInt(((BigDecimal)((Object[])object)[2]).toString());
				    Long 	   	holder				= Long.parseLong(((Object[])object)[3].toString());
				    String 		security			= ((Object[])object)[4].toString();
				    Integer    	securityClass		= Integer.parseInt(((BigDecimal)((Object[])object)[5]).toString());
				    Long 	   	holderAccount		= Long.parseLong(((Object[])object)[6].toString());
				    Integer 	portfolio			= (BigDecimal)((Object[])object)[7]!=null?Integer.parseInt(((BigDecimal)((Object[])object)[7]).toString()):null;
				    
				    referencePrice=businessLayerCommonFacade.processExchange(parameters,currency,
				    		billingService.getCurrencyCalculation(),DailyExchangeRoleType.BUY.getCode());				    				
					/***ALL EXCHANGE RATE  -----> CONVERSION TO DOP  */
					realAmount =BillingUtils.multiplyDecimals(referencePrice, amount , GeneralConstants.ROUNDING_DIGIT_NUMBER);

					collectionRecordDetailTo = new CollectionRecordDetailTo();						
					collectionRecordDetailTo.setPercentage(realAmount);
					collectionRecordDetailTo.setIdSecurityCodeFk(security);
					collectionRecordDetailTo.setSecurityClass(securityClass);
					collectionRecordDetailTo.setIdHolderPk(holder);
					collectionRecordDetailTo.setCurrencyOrigin(currency);
					collectionRecordDetailTo.setNegotiatedAmount(realAmount);
					collectionRecordDetailTo.setNegotiatedAmountOrigin(amount);
					collectionRecordDetailTo.setCalculationDate(calculateDate);
					collectionRecordDetailTo.setHolderAccount(holderAccount);
					collectionRecordDetailTo.setMnemonicHolder(null);
					collectionRecordDetailTo.setEconomicActivityParticipant(null);
					collectionRecordDetailTo.setPortfolio(portfolio);
					
					CollectionRecordTo collectionRecordTo = findEntityAtListTemporalTo(collectionRecordToList, entity, billingService);
					if (collectionRecordTo==null){
						collectionRecordTo = new CollectionRecordTo();
						if(baseCollectionMgmtServiceBean.isPaymentByparticipant(billingService)){
							entity = baseCollectionMgmtServiceBean.getIdParticipantPkByIdIssuerPk(entity.toString());
							collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
						}
						else{
							collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
						}
						
						List<CollectionRecordDetailTo> collectionRecordDetailToList = new  ArrayList<CollectionRecordDetailTo>();
						collectionRecordDetailToList.add(collectionRecordDetailTo);	
						collectionRecordToList.add(collectionRecordTo);				
						collectionRecordTo.setCollectionRecordDetailToList(collectionRecordDetailToList);
					}else{
						collectionRecordTo.getCollectionRecordDetailToList().add(collectionRecordDetailTo);
					}
				} 	 
		 } 
		return collectionRecordToList;
	}
	
	
	/**
	 * *
	 * EDV 2
	 * REGISTRO_VALORES_NO_COLOCADOS_R_FIJA
	 * REGISTRO_VALORES_NO_COLOCADOS_R_VARIABLE.
	 *
	 * @param parameters the parameters
	 * @param idInstrumentType the id instrument type
	 * @return the list
	 * @throws ServiceException the service exception
	 * @see com.pradera.model.billing.type.BaseCollectionType
	 */
	public List<CollectionRecordTo>  findBalanceSecurityNotPlaced(Map parameters, Integer idInstrumentType) throws ServiceException {
		BillingServiceSource billingServiceSource=null;
		
		List<CollectionRecordTo> collectionRecordToList=null;	
		CollectionRecordDetailTo collectionRecordDetailTo = null;
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		Date calculateDate= (Date) parameters.get(GeneralConstants.CALCULATION_DATE);
		List<Object> dataMaps=null;
		/**
		 * Si existe Fuente, se procesa el archivo XML
		 */
		if(Validations.validateIsNotNullAndPositive(billingService.getIndSourceBaseCollection())){
			
			return businessLayerCommonFacade.executeFileBaseCollection(new BillingServiceTo(billingService),parameters);
			
		}else{
			dataMaps= baseCollectionMgmtServiceBean.findBalanceSecurityNotPlaced(parameters,idInstrumentType);
		}
		
		
		
		
		
		 /** GETTING DAILY EXCHANGE ***/	
		 BigDecimal referencePrice = BigDecimal.ZERO;
		 BigDecimal realAmount = BigDecimal.ZERO;
		 

		 if (Validations.validateListIsNotNullAndNotEmpty(dataMaps)){
			 collectionRecordToList = new ArrayList<CollectionRecordTo>();								
			 
			 	for (Object object : dataMaps) {
			     
					
					Object     	entity         	= ((Object[])object)[0];
					BigDecimal 	amount	   		= ((BigDecimal)((Object[])object)[1]);
					Integer    	currency        = Integer.parseInt(((BigDecimal)((Object[])object)[2]).toString());
					Integer    	securityClass   = Integer.parseInt(((BigDecimal)((Object[])object)[3]).toString());
					String 		security		= ((Object[])object)[4].toString();
					String 		emision			= ((Object[])object)[5].toString();
					String 		issuer			= ((Object[])object)[6].toString();
					//SECURITY_CLASS PARTICIPANT VALOR EMISOR EMISION

					referencePrice=businessLayerCommonFacade.processExchange(parameters,currency,
							billingService.getCurrencyCalculation(),DailyExchangeRoleType.BUY.getCode());
					 

					/***ALL EXCHANGE RATE  -----> CONVERSION TO CALCULATION CURRENCY  */
					realAmount =BillingUtils.multiplyDecimals(referencePrice,amount , GeneralConstants.ROUNDING_DIGIT_NUMBER); 

					collectionRecordDetailTo = new CollectionRecordDetailTo();
					
					collectionRecordDetailTo.setPercentage(realAmount);
					collectionRecordDetailTo.setIdSecurityCodeFk(security);
					collectionRecordDetailTo.setIdIssuanceCodePk(emision);
					collectionRecordDetailTo.setCalculationDate(calculateDate);
					collectionRecordDetailTo.setNegotiatedAmount(realAmount);
					collectionRecordDetailTo.setNegotiatedAmountOrigin(amount);
					collectionRecordDetailTo.setSecurityClass(securityClass);
					collectionRecordDetailTo.setCurrencyOrigin(currency);
					collectionRecordDetailTo.setIdIssuerFk(issuer);//issuer
					
					CollectionRecordTo collectionRecordTo = findEntityAtListTemporalTo(collectionRecordToList, entity, billingService);
					if (collectionRecordTo==null){
						collectionRecordTo = new CollectionRecordTo();
						if(baseCollectionMgmtServiceBean.isPaymentByparticipant(billingService)){
							entity = baseCollectionMgmtServiceBean.getIdParticipantPkByIdIssuerPk(entity.toString());
							collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
						}
						else{
							collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
						}
						List<CollectionRecordDetailTo> collectionRecordDetailToList = new  ArrayList<CollectionRecordDetailTo>();
						collectionRecordDetailToList.add(collectionRecordDetailTo);	
						collectionRecordToList.add(collectionRecordTo);	
						collectionRecordTo.setCollectionRecordDetailToList(collectionRecordDetailToList);
					}else{
						collectionRecordTo.getCollectionRecordDetailToList().add(collectionRecordDetailTo);
					}

	
		       } /** End For*/
		 
		 }	 		
		 
		 
		return collectionRecordToList;
	}
	
	

	/**
	 * EDV 3
	 * <br>GUARDA_Y_CUSTODIA_DE_VALORES_FISICOS_R_FIJA
	 * <br>GUARDA_Y_CUSTODIA_DE_VALORES_FISICOS_R_VARIABLE.
	 *
	 * @param parameters the parameters
	 * @param idInstrumentType the id instrument type
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<CollectionRecordTo>  findPhysicalCustodyOfSecurities(Map parameters, Integer idInstrumentType) throws ServiceException {
		

		List<Object> dataMaps= baseCollectionMgmtServiceBean.findPhysicalCustodyOfSecurities(parameters,idInstrumentType);
		
		List<CollectionRecordTo> collectionRecordToList=null;	
		CollectionRecordDetailTo collectionRecordDetailTo = null;
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);	
		Date calculateDate= (Date) parameters.get(GeneralConstants.CALCULATION_DATE);
		
		
		 BigDecimal referencePrice = BigDecimal.ZERO;
		 BigDecimal realAmount = BigDecimal.ZERO;
		 

		 if (Validations.validateListIsNotNullAndNotEmpty(dataMaps)){
			 collectionRecordToList = new ArrayList<CollectionRecordTo>();
			 
			 	for (Object object : dataMaps) {
			     
					
					Object     	entity         	= ((Object[])object)[0];
					BigDecimal 	amount	   		= ((BigDecimal)((Object[])object)[1]);
					Integer    	currency         = Integer.parseInt(((BigDecimal)((Object[])object)[2]).toString());
					Integer 	securityClass		= Integer.parseInt(((BigDecimal)((Object[])object)[3]).toString());
					String  	security       		= (String)((Object[])object)[4].toString();
					Long 		idHolderPk 		 	= Long.parseLong((((Object[])object)[5]).toString());
					Long 		holderAccount 		= Long.parseLong((((Object[])object)[6]).toString());
				    String 		mnemonicHolder		= ((Object[])object)[7]!=null?((Object[])object)[7].toString():null;
				    Integer 	economicActivity	= (BigDecimal)((Object[])object)[8]!=null?Integer.parseInt(((BigDecimal)((Object[])object)[8]).toString()):null;
				    Integer 	portfolio			= (BigDecimal)((Object[])object)[9]!=null?Integer.parseInt(((BigDecimal)((Object[])object)[9]).toString()):null;

					
					referencePrice=businessLayerCommonFacade.processExchange(parameters,currency,
							billingService.getCurrencyCalculation(),DailyExchangeRoleType.BUY.getCode());

					/***ALL EXCHANGE RATE  -----> CONVERSION TO DOP  */
					realAmount =BillingUtils.multiplyDecimals(referencePrice, amount , GeneralConstants.ROUNDING_DIGIT_NUMBER);

					collectionRecordDetailTo = new CollectionRecordDetailTo();
					collectionRecordDetailTo.setCalculationDate(calculateDate);
					collectionRecordDetailTo.setPercentage(realAmount);
					collectionRecordDetailTo.setNegotiatedAmountOrigin(amount);
					collectionRecordDetailTo.setNegotiatedAmount(realAmount);
					collectionRecordDetailTo.setCurrencyOrigin(currency);
					collectionRecordDetailTo.setSecurityClass(securityClass);
					collectionRecordDetailTo.setIdSecurityCodeFk(security);
					collectionRecordDetailTo.setIdHolderPk(idHolderPk);
					collectionRecordDetailTo.setHolderAccount(holderAccount);
					collectionRecordDetailTo.setMnemonicHolder(mnemonicHolder);
					collectionRecordDetailTo.setEconomicActivityParticipant(economicActivity);
					collectionRecordDetailTo.setPortfolio(portfolio);

					CollectionRecordTo collectionRecordTo = findEntityAtListTemporalTo(collectionRecordToList, entity, billingService);

					if (collectionRecordTo==null){
						collectionRecordTo = new CollectionRecordTo();
						if(baseCollectionMgmtServiceBean.isPaymentByparticipant(billingService)){
							entity = baseCollectionMgmtServiceBean.getIdParticipantPkByIdIssuerPk(entity.toString());
							collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
						}
						else{
							collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
						}
						List<CollectionRecordDetailTo> collectionRecordDetailToList = new  ArrayList<CollectionRecordDetailTo>();
						collectionRecordDetailToList.add(collectionRecordDetailTo);	
						collectionRecordToList.add(collectionRecordTo);	
						collectionRecordTo.setCollectionRecordDetailToList(collectionRecordDetailToList);
					}else{
						collectionRecordTo.getCollectionRecordDetailToList().add(collectionRecordDetailTo);
					}
					
		       } /** End For*/
		 
		 }	 		
		 
		 
		return collectionRecordToList;
	}
	
	/**
	 * EDV 24
	 * <br>CUSTODIA_VALORES_FISICOS_OFERTA_PRIVADA_R_FIJA
	 * <br>CUSTODIA_VALORES_FISICOS_OFERTA_PRIVADA_R_VARIABLE.
	 *
	 * @param parameters the parameters
	 * @param idInstrumentType the id instrument type
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<CollectionRecordTo>  findPhysicalCustodyOfSecuritiesPrivate(Map parameters, Integer idInstrumentType) throws ServiceException {
		

		List<Object> dataMaps= baseCollectionMgmtServiceBean.findPhysicalCustodyOfSecuritiesPrivate(parameters,idInstrumentType);
		
		List<CollectionRecordTo> collectionRecordToList=null;	
		CollectionRecordDetailTo collectionRecordDetailTo = null;
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);	
		
		 BigDecimal referencePrice = BigDecimal.ZERO;
		 BigDecimal realAmount = BigDecimal.ZERO;


		 if (Validations.validateListIsNotNullAndNotEmpty(dataMaps)){
			 collectionRecordToList = new ArrayList<CollectionRecordTo>();							
			 CollectionRecordTo collectionRecordTo =null;	
			 
			 	for (Object object : dataMaps) {
			     
					
					Object     	entity         		= ((Object[])object)[0];
					BigDecimal 	amount	   			= ((BigDecimal)((Object[])object)[1]);
					Integer    	currency          	= Integer.parseInt(((BigDecimal)((Object[])object)[2]).toString());
					Integer 	securityClass		= Integer.parseInt(((BigDecimal)((Object[])object)[3]).toString());
					String  	security       		= (String)((Object[])object)[4].toString();
					Long 		idHolderPk 		 	= Long.parseLong((((Object[])object)[5]).toString());
					Long 		holderAccount 		= Long.parseLong((((Object[])object)[6]).toString());
				    String 		mnemonicHolder		= ((Object[])object)[7]!=null?((Object[])object)[7].toString():null;
				    Integer 	economicActivity	= (BigDecimal)((Object[])object)[8]!=null?Integer.parseInt(((BigDecimal)((Object[])object)[8]).toString()):null;
				    Integer 	portfolio			= (BigDecimal)((Object[])object)[9]!=null?Integer.parseInt(((BigDecimal)((Object[])object)[9]).toString()):null;

				
					referencePrice=businessLayerCommonFacade.processExchange(parameters,currency,
							billingService.getCurrencyCalculation(),DailyExchangeRoleType.BUY.getCode());
					
					/***ALL EXCHANGE RATE  -----> CONVERSION TO DOP  */
					realAmount =BillingUtils.multiplyDecimals(referencePrice, amount , GeneralConstants.ROUNDING_DIGIT_NUMBER);

					collectionRecordDetailTo = new CollectionRecordDetailTo();
					
					collectionRecordDetailTo.setPercentage(realAmount);
					collectionRecordDetailTo.setCurrencyRate(currency);
					collectionRecordDetailTo.setCurrencyOrigin(currency);
					collectionRecordDetailTo.setNegotiatedAmountOrigin(amount);
					collectionRecordDetailTo.setNegotiatedAmount(realAmount);
					collectionRecordDetailTo.setSecurityClass(securityClass);
					collectionRecordDetailTo.setIdSecurityCodeFk(security);
					collectionRecordDetailTo.setIdHolderPk(idHolderPk);
					collectionRecordDetailTo.setHolderAccount(holderAccount);
					collectionRecordDetailTo.setMnemonicHolder(mnemonicHolder);
					collectionRecordDetailTo.setEconomicActivityParticipant(economicActivity);
					collectionRecordDetailTo.setPortfolio(portfolio);
					
					collectionRecordTo = findEntityAtListTemporalTo(collectionRecordToList, entity, billingService);
					if (collectionRecordTo==null){
						collectionRecordTo = new CollectionRecordTo();
						if(baseCollectionMgmtServiceBean.isPaymentByparticipant(billingService)){
							entity = baseCollectionMgmtServiceBean.getIdParticipantPkByIdIssuerPk(entity.toString());
							collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
						}
						else{
							collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
						}
						List<CollectionRecordDetailTo> collectionRecordDetailToList = new  ArrayList<CollectionRecordDetailTo>();
						collectionRecordDetailToList.add(collectionRecordDetailTo);	
						collectionRecordToList.add(collectionRecordTo);	
						collectionRecordTo.setCollectionRecordDetailToList(collectionRecordDetailToList);
					}else{
						collectionRecordTo.getCollectionRecordDetailToList().add(collectionRecordDetailTo);
					}

		       }/** End For*/
		 
		 }	 		
		 
		 
		return collectionRecordToList;
	}

	/**
	 * Find registered account.
	 *
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public  List<CollectionRecordTo> findRegisteredAccount(Map parameters ) throws ServiceException {
	
	BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);			
	List<Object> dataMaps= null;
	
	if (billingService.getBaseCollection().equals(BaseCollectionType.DIRECT_PARTICIPANT.getCode())
			|| billingService.getBaseCollection().equals(BaseCollectionType.INDIRECT_PARTICIPANT.getCode())){		
		if (billingService.getEntityCollection().equals(EntityCollectionType.PARTICIPANTS.getCode())){
			dataMaps= baseCollectionMgmtServiceBean.findRegisteredParticipants(parameters);			
		}		
	}
	else if (billingService.getBaseCollection().equals(BaseCollectionType.ACCOUNT_MAINTENANCE_ISSUER.getCode()) ){
		
		if (billingService.getEntityCollection().equals(EntityCollectionType.PARTICIPANTS.getCode())){
			dataMaps= baseCollectionMgmtServiceBean.findRegisteredIssuer(parameters);
			
		}
		
	}
	
	
	List<CollectionRecordTo> collectionRecordToList=null;	
	
	 if (Validations.validateListIsNotNullAndNotEmpty(dataMaps)){
		 collectionRecordToList = new ArrayList<CollectionRecordTo>();							
		 CollectionRecordTo collectionRecordTo =null;	
		 
		 		for (Object object : dataMaps) {
		     					
				Object     entity         		= object;

				 /** pointer if exist a entity ,initialized at ZERO **/ 
				Integer existEntity = 0; 
				/**FIND COLLECTION RECORD AT  LIST***/	
				for (int i = 0; i < collectionRecordToList.size(); i++) {
					if(baseCollectionMgmtServiceBean.isPaymentByparticipant(billingService)){
						entity = baseCollectionMgmtServiceBean.getIdParticipantPkByIdIssuerPk(entity.toString());
					}
					if (collectionRecordToList.get(i).existEntityCollection(entity, billingService.getEntityCollection())) {
						existEntity=i;
						break;							
					}

				}
				
				if (existEntity==0){
					collectionRecordTo = new CollectionRecordTo();
					if(baseCollectionMgmtServiceBean.isPaymentByparticipant(billingService)){
						entity = baseCollectionMgmtServiceBean.getIdParticipantPkByIdIssuerPk(entity.toString());
						collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
					}
					else{
						collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
					}					
					collectionRecordToList.add(collectionRecordTo);	
					
				}

	       } /** End For*/
	 
	 }	 		
	 
	 
	return collectionRecordToList;
} 

	
	
	
	
	
	
	
	
	


	/**
	 * Process exception base remote.
	 *
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.DAILY_COLLECT_SERVICE_CALCULATION)
	public List<Object>  processExceptionBaseRemote(Map parameters) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());	    
	  
		
	    BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);	
		
		List<Object> dataMaps= null;
		
		// 19
		if (billingService.getBaseCollection().equals(BaseCollectionType.TRANSACTION_CHARGE.getCode())){
			/** CONSIDERED THE NEGOTIATION MECHANISM**/			 
			
			dataMaps = baseCollectionMgmtServiceBean.findMovementSecuritiesChangeOwnerShip(parameters);
			
		}
		else if( billingService.getBaseCollection().equals(BaseCollectionType.RECORD_CORPORATIVE_EVENTS.getCode()) ){
			  
//			dataMaps= exceptionLayerServiceBean.processSecurityCustodyIssuer(parameters);
			
		}else if( billingService.getBaseCollection().equals(BaseCollectionType.ISSUANCE_CAT.getCode()) ){
			
			dataMaps= baseCollectionMgmtServiceBean.processAccreditationCertificate(parameters);
			
		}

		else if( billingService.getBaseCollection().equals(BaseCollectionType.PAYING_AGENT.getCode()) ){
 					  
			dataMaps= exceptionLayerServiceBean.processSecurityCorporativePayingAgentHolder(parameters);
																			  
		}else if( billingService.getBaseCollection().equals(BaseCollectionType.REGISTRATION_PRECAUTIONARY_MEASURES.getCode())||
				billingService.getBaseCollection().equals(BaseCollectionType.LIFTING_PRECAUTIONARY_MEASURES.getCode())){
			
			dataMaps=baseCollectionMgmtServiceBean.findMovementPawnBlockAffectation(parameters);
			
		}

				
		return dataMaps;
	}
	

	/**
	 * *
	 * Metodo recursivo que retorna el monto del cobro acumulativo por escalones.
	 *
	 * @param billingService the billing service
	 * @param operationPrice the operation price
	 * @param serviceRateScales the service rate scales
	 * @param parameters the parameters
	 * @return the accumulative scale by amount
	 * @throws ServiceException the service exception
	 */
	public BigDecimal getAccumulativeScaleByAmount( BillingService billingService, 

									BigDecimal operationPrice,
									List<ServiceRateScale> serviceRateScales,
									Map parameters) throws ServiceException{

		BigDecimal precedente = BigDecimal.ZERO;
		BigDecimal currentScaleMax = BigDecimal.ZERO;
		BigDecimal oldScaleMax = BigDecimal.ZERO;
		BigDecimal opPriceSubPrecedent=null;
		BigDecimal diferenciaEscalas=null;
		BigDecimal operationAmount=null;
		BigDecimal commision=null;
		BigDecimal totalCommision=null;
		
		Integer currencyScale=GeneralConstants.ONE_VALUE_INTEGER;
		BigDecimal factor;		
		
		for (Iterator<ServiceRateScale> iterator = serviceRateScales.iterator(); iterator.hasNext();) {
			ServiceRateScale serviceRateScale = (ServiceRateScale) iterator.next();
			BigDecimal scaleSelected=null;
			
			
			currentScaleMax=serviceRateScale.getMaximumRange();
			
			/**Precio menos antiguas **/
			opPriceSubPrecedent=operationPrice.subtract(precedente);
			diferenciaEscalas=currentScaleMax.subtract(oldScaleMax);
			
			/**
			 * If opPriceSubPrecedent > diferenciaEscalas Then operationAmount= diferenciaEscalas Else operationAmount=opPriceSubPrecedent
			 */
			if(opPriceSubPrecedent.subtract(diferenciaEscalas).compareTo(GeneralConstants.ZERO_VALUE_BIGDECIMAL)>0){
				operationAmount=diferenciaEscalas;
			}else{
				operationAmount=opPriceSubPrecedent;
			}
			
			/**
			 * Lo que se va a cobrar por escalon 
			 * serviceRateScaleSelected.getIndStaging()=0, Monto; 
			 * serviceRateScaleSelected.getIndStaging()=1, Porcentage
			 * */
			if(serviceRateScale.getIndStaging().equals(GeneralConstants.ZERO_VALUE_INTEGER)){
				factor=BigDecimal.ONE;
				/**If the stair has a different currency. Make the kind of calculation*/
				if(Validations.validateIsNotNullAndPositive(serviceRateScale.getIndOtherCurrency())){
					currencyScale=serviceRateScale.getOtherCurrency();
					
					if(CurrencyType.UFV.getCode().equals(currencyScale)){
						factor=businessLayerCommonFacade.processExchange(parameters,currencyScale,
								billingService.getCurrencyCalculation(),DailyExchangeRoleType.SELL.getCode());
					}else{
						factor=businessLayerCommonFacade.processExchange(parameters,currencyScale,
								billingService.getCurrencyCalculation(),DailyExchangeRoleType.BUY.getCode());
					}
				}
				
				scaleSelected= serviceRateScale.getScaleAmount();
				commision=BillingUtils.multiplyDecimals(scaleSelected, factor, GeneralConstants.ROUNDING_DIGIT_NUMBER);
				
			}else{
				scaleSelected= serviceRateScale.getScalePercent();
				commision= BillingUtils.multiplyDecimals(operationAmount, scaleSelected, GeneralConstants.ROUNDING_DIGIT_NUMBER);
			}
			
			
			precedente=BillingUtils.addTwoDecimal(precedente, operationAmount, GeneralConstants.ROUNDING_DIGIT_NUMBER);
			totalCommision=BillingUtils.addTwoDecimal(totalCommision, commision, GeneralConstants.ROUNDING_DIGIT_NUMBER);
			
			oldScaleMax=currentScaleMax;
			/**If the scale reached its maximum **/
			if(oldScaleMax.subtract(operationPrice).compareTo(GeneralConstants.ZERO_VALUE_BIGDECIMAL)>0){
				break;
			}

		}

		
		return totalCommision;
	}
	
	/**
	 * Processing to Scale each Collection Record.
	 *
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<CollectionRecordTo>  processingScaleToCollection(Map parameters) throws ServiceException{

		
		 BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);	
		    /***
		     * 1) Getting Movements
		     */
		    List<CollectionRecordTo> collectionRecordToList = (List<CollectionRecordTo>) parameters.get(GeneralConstants.COLLECTION_RECORD_MOVEMENT);
		    List<CollectionRecordTo> collectionRecordToFinalList = null;

    	/**
    	 * 2) StaggercollectionRecord.getCollectionAmount()
    	 */
		if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordToList)){			
			ServiceRate serviceRate=(ServiceRate) parameters.get(GeneralConstants.SERVICE_RATE);
			List<ServiceRateScale> serviceRateScales = serviceRate.getServiceRateScales();
			
			collectionRecordToFinalList = new ArrayList<CollectionRecordTo>();
			/**
			 * 3) For Each Collection
			 */
			for (Iterator<CollectionRecordTo> iter=collectionRecordToList.iterator(); iter.hasNext();) {
				BigDecimal valueLimit = BigDecimal.ZERO;
				CollectionRecordTo collectionRecordTo = (CollectionRecordTo)iter.next();

				/**
				 * 4) Here for comparing the steps, but there are steps to notify
				 * 5) Add the amounts the detail
				 * 
				 */
				for (Iterator iterCollectionDetail = collectionRecordTo.getCollectionRecordDetailToList().iterator(); iterCollectionDetail.hasNext();) { 
					CollectionRecordDetailTo collectionRecordDetailTo = ( CollectionRecordDetailTo ) iterCollectionDetail.next();
					valueLimit=BillingUtils.addTwoDecimal(collectionRecordDetailTo.getPriceByQuantity(), 
															valueLimit, GeneralConstants.ROUNDING_DIGIT_NUMBER);
					
				}
					ServiceRateScale serviceRateScaleSelected	=	null;
					/***
					 *  6) Here for comparing the steps
					 */
					for (Iterator<ServiceRateScale> iterScale = serviceRateScales.iterator(); iterScale.hasNext();){
						ServiceRateScale serviceRateScale = (ServiceRateScale)iterScale.next();
						if ( (valueLimit.compareTo(serviceRateScale.getMinimumRange())>=0) &&
							 (valueLimit.compareTo(serviceRateScale.getMaximumRange())<=0) ){
							serviceRateScaleSelected = serviceRateScale;
							break;
						}
					}
					
					
					/**
					 * 7) If there stair, continue
					 */
					if (Validations.validateIsNotNull(serviceRateScaleSelected)){
						BigDecimal amount;
						Iterator<ServiceRateScale> iterScale = serviceRateScales.iterator();
						amount=getAccumulativeScaleByAmount(billingService, valueLimit,
									serviceRateScales, parameters);
						if(BillingUtils.isRequestAffectations(billingService.getBaseCollection())||
								BillingUtils.isRegisterOwnerChange(billingService.getBaseCollection())){
							amount=businessLayerCommonFacade.appliedMinimumAmountCalculation(serviceRate, 
									amount, parameters);
							collectionRecordTo.getCollectionRecordDetailToList().get(0).setAmount(amount);
						}
						
						collectionRecordTo.setCollectionAmount(amount);
						
						
					}
					
					else if (Validations.validateIsNull(serviceRateScaleSelected)){
						
						/**
						 * 8) Remove the Collection that has stair
						 */
						iter.remove();
						
						/**9) Here send notification when there is the stair*/
						
						CollectionRecordTo collectionRecordToNew	= SerializationUtils.clone(collectionRecordTo);	
						String descriptionEntity=null;
						
						BusinessProcess businessProcess = new BusinessProcess();
						businessProcess.setIdBusinessProcessPk(BusinessProcessType.MONTHLY_COLLECT_SERVICE_CALCULATION.getCode());
					
						String subject = messageConfiguration.getProperty("header.message.warning");
 						descriptionEntity = getDescriptionNameEntityCollection(collectionRecordTo);
						
						String message = messageConfiguration.getProperty("warning.calculation.service.access.billing");					 
						message = MessageFormat.format(message, 
									EntityCollectionType.get(collectionRecordToNew.getEntityCollection()).getValue(),//0
									descriptionEntity,//1
									"",//2
									serviceRate.getRateCode(), //3
									serviceRate.getRateName(), //4
									billingService.getServiceCode()); //5
						List<UserAccountSession> listUser =this.businessLayerCommonFacade.
								getListUsserByBusinessProcess(BusinessProcessType.MONTHLY_COLLECT_SERVICE_CALCULATION.getCode());
						businessLayerCommonFacade.sendNotificationManual(businessProcess,listUser, subject, message, NotificationType.EMAIL);
						
					}


				
				collectionRecordToFinalList.add(collectionRecordTo);
			}
		  }
		
		

	  return collectionRecordToFinalList;
	}
	
	/**
	 * Processing to Scale each Collection Record Detail.
	 *
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<CollectionRecordTo>  processingScaleToDetailCollection(Map parameters) throws ServiceException{

		
		 BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);	
	    /***
	     * OBTENGO LOS MOVIMIENTOS
	     */
	    List<CollectionRecordTo> collectionRecordToList = (List<CollectionRecordTo>) parameters.get(GeneralConstants.COLLECTION_RECORD_MOVEMENT);
	    List<CollectionRecordTo> collectionRecordToFinalList = null;

    	/**
    	 * Escalonar
    	 */
		if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordToList)){			
			ServiceRate serviceRate=(ServiceRate) parameters.get(GeneralConstants.SERVICE_RATE);
			List<ServiceRateScale> serviceRateScales = serviceRate.getServiceRateScales();
			BigDecimal valueLimit = BigDecimal.ZERO;
			collectionRecordToFinalList = new ArrayList<CollectionRecordTo>();
			/**
			 * Para cada cobro
			 */
			for (Iterator<CollectionRecordTo> iter=collectionRecordToList.iterator(); iter.hasNext();) {
			
				CollectionRecordTo collectionRecordTo = (CollectionRecordTo)iter.next();

				/**
				 * Aqui el for de comparar escalones, sino hay escalones notificar
				 * Por cada detalle de cobro
				 * 
				 */
				for (Iterator iterCollectionDetail = collectionRecordTo.getCollectionRecordDetailToList().iterator(); iterCollectionDetail.hasNext();) {
					CollectionRecordDetailTo collectionRecordDetailTo = ( CollectionRecordDetailTo ) iterCollectionDetail.next();
					ServiceRateScale serviceRateScaleSelected	=	null;
					/***
					 *  1) Compara los escalones, PRIMERA VALIDACION DE LOS ESCALONES
					 */
					for (Iterator<ServiceRateScale> iterScale = serviceRateScales.iterator(); iterScale.hasNext();){
						ServiceRateScale serviceRateScale = (ServiceRateScale)iterScale.next();
						valueLimit = new BigDecimal(collectionRecordDetailTo.getNegotiatedAmount().toString());
						if ( (valueLimit.compareTo(serviceRateScale.getMinimumRange())>=0) &&
							 (valueLimit.compareTo(serviceRateScale.getMaximumRange())<=0) ){
							serviceRateScaleSelected = serviceRateScale;
							break;
						}
					}
					
					
					/**
					 * 2) Si existe escalon, proseguir
					 */
					if (Validations.validateIsNotNull(serviceRateScaleSelected)){
						BigDecimal amount;
						Iterator<ServiceRateScale> iterScale = serviceRateScales.iterator();
						amount=getAccumulativeScaleByAmount(billingService, 
									collectionRecordDetailTo.getNegotiatedAmount(),
									serviceRateScales, parameters);
						if(BillingUtils.isTransfersOtcTransactions(billingService.getBaseCollection())){
							amount=businessLayerCommonFacade.appliedMinimumAmountCalculation(serviceRate, 
									amount, parameters);
							
						}
						collectionRecordDetailTo.setAmount(amount);
						
						
					}
					
					else if (Validations.validateIsNull(serviceRateScaleSelected)){
						
						/**
						 * 3) Remover el detalle que no tiene escalon
						 */
						iterCollectionDetail.remove();
						
						/**4) Aqui enviar la notificacion cuando no encuentra el escalon*/
						
						CollectionRecordTo collectionRecordToNew	= SerializationUtils.clone(collectionRecordTo);	
						String descriptionEntity=null;
						
						BusinessProcess businessProcess = new BusinessProcess();
						businessProcess.setIdBusinessProcessPk(BusinessProcessType.MONTHLY_COLLECT_SERVICE_CALCULATION.getCode());
					
						String subject = messageConfiguration.getProperty("header.message.warning");
 						descriptionEntity = getDescriptionNameEntityCollection(collectionRecordTo);
						
						String message = messageConfiguration.getProperty("warning.calculation.service.access.billing");					 
						message = MessageFormat.format(message, 
									EntityCollectionType.get(collectionRecordToNew.getEntityCollection()).getValue(),//0
									descriptionEntity,//1
									"",//2
									serviceRate.getRateCode(), //3
									serviceRate.getRateName(), //4
									billingService.getServiceCode()); //5
						List<UserAccountSession> listUser
						=this.businessLayerCommonFacade.getListUsserByBusinessProcess(BusinessProcessType.MONTHLY_COLLECT_SERVICE_CALCULATION.getCode());
						businessLayerCommonFacade.sendNotificationManual(businessProcess,listUser, subject, message, NotificationType.EMAIL);
						
					}

				
			}
				
				collectionRecordToFinalList.add(collectionRecordTo);
			}
		  }
		
		

	  return collectionRecordToFinalList;
	}


	
	/**
	 * Gets the collection by not accumulative scale.
	 *
	 * @param collectionRecordTo the collection record to
	 * @param serviceRateScaleSelected the service rate scale selected
	 * @return the collection by not accumulative scale
	 */
	protected CollectionRecordTo  getCollectionByNotAccumulativeScale(CollectionRecordTo collectionRecordTo,ServiceRateScale serviceRateScaleSelected ){
		BigDecimal amount  = BigDecimal.ZERO;
		CollectionRecordTo collectionRecordToNew	= SerializationUtils.clone(collectionRecordTo);
		CollectionRecordDetailTo collectionRecordDetailTo =  new CollectionRecordDetailTo();						
		amount =BillingUtils.addTwoDecimal(serviceRateScaleSelected.getScaleAmount(),amount , GeneralConstants.ROUNDING_DIGIT_NUMBER);
		collectionRecordDetailTo.setAmount(amount);
		
		List<CollectionRecordDetailTo> collectionRecordDetailToList = new  ArrayList<CollectionRecordDetailTo>();																		
		collectionRecordDetailToList.add(collectionRecordDetailTo);											
		collectionRecordToNew.setCollectionRecordDetailToList(collectionRecordDetailToList);
		return collectionRecordToNew;
	}

	/**
	 * Gets the description name entity collection.
	 *
	 * @param collectionRecordTo the collection record to
	 * @return the description name entity collection
	 * @throws ServiceException the service exception
	 */
	protected String getDescriptionNameEntityCollection(CollectionRecordTo collectionRecordTo ) throws ServiceException{
		
		String descriptionEntity="";
		
		if(collectionRecordTo.getEntityCollection().equals(EntityCollectionType.HOLDERS.getCode())){			
			
			Holder holder	=	helperComponentFacade.findHolderByCode(collectionRecordTo.getHolder().getIdHolderPk());
			descriptionEntity = holder.getIdHolderPk() + "-" + holder.getFullName();
			
		}else if(collectionRecordTo.getEntityCollection().equals(EntityCollectionType.ISSUERS.getCode())){						
			
			IssuerTo issuerTo = businessLayerCommonFacade.getGeographicLocationForIssuer(collectionRecordTo.getIssuer().getIdIssuerPk());
			descriptionEntity = issuerTo.getIdIssuerPk()+ "-" + issuerTo.getBusinessName();
			
			
		}else if(collectionRecordTo.getEntityCollection().equals(EntityCollectionType.PARTICIPANTS.getCode()) ||
				collectionRecordTo.getEntityCollection().equals(EntityCollectionType.AFP.getCode()) ){			
			Participant participant = helperComponentFacade.findParticipantsByCode(collectionRecordTo.getParticipant().getIdParticipantPk());
			descriptionEntity = participant.getIdParticipantPk()+"-"+participant.getDescription();
			

		}else if( (collectionRecordTo.getEntityCollection().equals(EntityCollectionType.HACIENDA.getCode())) ||
				(collectionRecordTo.getEntityCollection().equals(EntityCollectionType.CENTRAL_BANK.getCode())) ||
				(collectionRecordTo.getEntityCollection().equals(EntityCollectionType.STOCK_EXCHANGE.getCode())) 
				
				){
			
			descriptionEntity = EntityCollectionType.get(collectionRecordTo.getEntityCollection()).getValue();
			
		}else if(collectionRecordTo.getEntityCollection().equals(EntityCollectionType.OTHERS.getCode())){
			
			descriptionEntity = DocumentType.get(collectionRecordTo.getDocumentType()).getValue() +"-"+collectionRecordTo.getOtherEntityDocument();
			descriptionEntity  = descriptionEntity + collectionRecordTo.getOtherEntityDescription();
			
		}
		return descriptionEntity;
	}
	
	/**
	 * Find entity at list temporal to.
	 *
	 * @param collectionRecordToList the collection record to list
	 * @param entity the entity
	 * @param typeEntity the type entity
	 * @return the collection record to
	 */
	protected  CollectionRecordTo findEntityAtListTemporalTo(List<CollectionRecordTo> collectionRecordToList,Object entity,BillingService billingService){
		
		if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordToList)){
				
			for (CollectionRecordTo collectionRecordTo : collectionRecordToList) {
				
				if(baseCollectionMgmtServiceBean.isPaymentByparticipant(billingService)){
					entity = baseCollectionMgmtServiceBean.getIdParticipantPkByIdIssuerPk(entity.toString());
				}
				if (collectionRecordTo.existEntityCollection(entity, billingService.getEntityCollection())){
					return collectionRecordTo;
				}
			}
		}
	return null;
	}
	
	/**
	 * Calculo mensual. Registro de Depositante, por cuenta propia y de terceros
	 * 
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<CollectionRecordTo> findMonthlyRegisterParticipantForQuota(Map<String, Object> parameters, Integer participantType, Integer indManagedThird) throws ServiceException {

		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);

		List<Object> dataMaps = accountsBaseCollectionMgmtServiceBean.findParticipantForCreation(parameters, participantType, indManagedThird);

		List<CollectionRecordTo> collectionRecordToList = null;

		if (Validations.validateListIsNotNullAndNotEmpty(dataMaps)) {
			collectionRecordToList = new ArrayList<CollectionRecordTo>();
			CollectionRecordTo collectionRecordTo = null;

			for (Object object : dataMaps) {

				Object entity = object;

				/** pointer if exist a entity ,initialized at ZERO **/
				Integer existEntity = 0;
				/** FIND COLLECTION RECORD AT LIST ***/
				for (int i = 0; i < collectionRecordToList.size(); i++) {
					if (baseCollectionMgmtServiceBean.isPaymentByparticipant(billingService)) {
						entity = baseCollectionMgmtServiceBean.getIdParticipantPkByIdIssuerPk(entity.toString());
					}
					if (collectionRecordToList.get(i).existEntityCollection(entity,
							billingService.getEntityCollection())) {
						existEntity = i;
						break;
					}

				}

				if (existEntity == 0) {
					collectionRecordTo = new CollectionRecordTo();
					if (baseCollectionMgmtServiceBean.isPaymentByparticipant(billingService)) {
						entity = baseCollectionMgmtServiceBean.getIdParticipantPkByIdIssuerPk(entity.toString());
						collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());
					} else {
						collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());
					}
					collectionRecordToList.add(collectionRecordTo);

				}

			}

		}

		return collectionRecordToList;
	}
}
