package com.pradera.billing.helper.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.el.ELContext;
import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.context.RequestContext;

import com.pradera.billing.billingservice.facade.BillingServiceMgmtServiceFacade;
import com.pradera.billing.billingsource.facade.SourceBillingServiceFacade;
import com.pradera.billing.billingsource.to.BillingServiceSourceTo;
import com.pradera.billing.common.utils.BillingUtils;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.HelperBean;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.BillingServiceSource;
import com.pradera.model.generalparameter.ParameterTable;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingServiceSourceHelperBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@HelperBean
public class BillingServiceSourceHelperBean extends GenericBaseBean  implements Serializable { 
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8979684280696810462L;
	
	/**  Bean BillingService getting for container parent. */
	private BillingServiceSource billingServiceSource;
	
	/**  Bean BillingServiceTo used only for filter on search dialog. */
	private BillingServiceSourceTo billingServiceSourceTo;
	
    /**  Parameters for search. */
	private List<ParameterTable> listServiceStatus;
	
	/** The list empty. */
	private boolean listEmpty = false;
	
	/** The helper name. */
	private String helperName;
	
	 
	/** The billing service helper source. */
	private GenericDataModel<BillingServiceSource> billingServiceHelperSource;
	
	/** The list billing service source. */
	private List<BillingServiceSource> listBillingServiceSource;


	/** The billing service mgmt service facade. */
	@EJB
	BillingServiceMgmtServiceFacade billingServiceMgmtServiceFacade; 
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade  helperComponentFacade;
	
	/** The source billing service facade. */
	@EJB
	SourceBillingServiceFacade sourceBillingServiceFacade;
	
	

	/**
	 * Instantiates a new billing service source helper bean.
	 */
	public BillingServiceSourceHelperBean(){}
	
	/**
	 * Inits the.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException{
		/** Values by Default*/
		billingServiceSource = new BillingServiceSource();
		billingServiceSourceTo= new BillingServiceSourceTo();		
		listBillingServiceSource=new ArrayList<BillingServiceSource>();	
		if (!FacesContext.getCurrentInstance().isPostback()){
			loadDataSearch(); 	 
		}

		
	}
	
	/**
	 * *
	 * 
	 * Validate Structure the File XML and Load AccountingSourceInfo with the information.
	 *
	 * @param billingServiceSource the billing service source
	 * @return the boolean
	 */
	public Boolean validateIsCorrectStructureFileXml(BillingServiceSource billingServiceSource){
		if (Validations.validateIsNotNullAndNotEmpty(this.billingServiceSource)) {
			
			
			
			try {
				/**Load the file XML in physical**/
				byte[] file = sourceBillingServiceFacade.getBillingServiceSourceContent(billingServiceSource.getIdBillingServiceSourcePk());
				/**
				 * Validate Structure the File XML and Load AccountingSourceInfo with the information
				 * **/
				BillingUtils.loadXMLToObjectSource(file);
				return true;
			} catch (ServiceException e) {

				/**If the structure is wrong produced**/
				if(e.getErrorService().equals(ErrorServiceType.BILLING_SERVICE_SOURCE_WRONG_STRUCTURED)){
					showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							PropertiesUtilities.getMessage(PropertiesConstants.BILLING_SERVICE_SOURCE_WRONG_STRUCTURED, ""));
					JSFUtilities.executeJavascriptFunction("PF('warningDialogSource').show();");
					return false;
				}/**If Billing Service Source Value Format**/
				else if(e.getErrorService().equals(ErrorServiceType.BILLING_SERVICE_SOURCE_VALUE_FORMAT)){
					showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							PropertiesUtilities.getMessage(PropertiesConstants.BILLING_SERVICE_SOURCE_VALUE_FORMAT, ""));
					JSFUtilities.executeJavascriptFunction("PF('warningDialogSource').show();");
					return false;
				}/**If Wrong visibility format**/
				else if(e.getErrorService().equals(ErrorServiceType.BILLING_SERVICE_SOURCE_VISIBILITY_FORMAT)){
					showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							PropertiesUtilities.getMessage(PropertiesConstants.BILLING_SERVICE_SOURCE_VISIBILITY_FORMAT, ""));
					JSFUtilities.executeJavascriptFunction("PF('warningDialogSource').show();");
					return false;
				}/**If Position format wrong**/
				else if(e.getErrorService().equals(ErrorServiceType.BILLING_SERVICE_SOURCE_POSITION_FORMAT)){
					showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							PropertiesUtilities.getMessage(PropertiesConstants.BILLING_SERVICE_SOURCE_POSITION_FORMAT, ""));
					JSFUtilities.executeJavascriptFunction("PF('warningDialogSource').show();");
					return false;
				}/**If Accounting Source Type Format**/
				else if(e.getErrorService().equals(ErrorServiceType.BILLING_SERVICE_SOURCE_TYPE_FORMAT)){
					showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							PropertiesUtilities.getMessage(PropertiesConstants.BILLING_SERVICE_SOURCE_TYPE_FORMAT, ""));
					JSFUtilities.executeJavascriptFunction("PF('warningDialogSource').show();");
					return false;
				}/**If Accounting Source Level Format**/
				else if(e.getErrorService().equals(ErrorServiceType.BILLING_SERVICE_SOURCE_LEVEL_FORMAT)){
					showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
							PropertiesUtilities.getMessage(PropertiesConstants.BILLING_SERVICE_SOURCE_LEVEL_FORMAT, ""));
					JSFUtilities.executeJavascriptFunction("PF('warningDialogSource').show();");
					return false;
				}
				e.printStackTrace();
				return false;
			}
		}
		return false;
	}
	
	/**
	 * Show associated variables.
	 *
	 * @param event the event
	 */
	public void showAssociatedVariables(ActionEvent event) {
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		//getting holder object from inputText
		
		if (event != null) {
			billingServiceSource=(BillingServiceSource )event.getComponent().getAttributes().get("blockInformation");
			
			/**
			 * Validate Structure the File XML and Load AccountingSourceInfo with the information
			 * **/
			if(validateIsCorrectStructureFileXml(billingServiceSource)){
				ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.billingServiceSource}", BillingService.class);
				valueExpression.setValue(elContext, billingServiceSource);

				ValueExpression valueExpressionEnableActionRemote= context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.enableActionRemote}", Boolean.class);
				Boolean enableActionRemote=(Boolean)valueExpressionEnableActionRemote.getValue(elContext);
				if(enableActionRemote){
					JSFUtilities.executeJavascriptFunction("oncomplete();");
				}
				clearHelper();
			}
		}
		
	}
	
	
	/**
	 * Assign billing service.
	 *
	 * @param billingServiceSourceSelected the billing service source selected
	 */
	public void assignBillingService(BillingService billingServiceSourceSelected){
		
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		//getting holder object from inputtext
		ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.billingServiceSource}", BillingService.class);
		valueExpression.setValue(elContext, billingServiceSourceSelected);
		clearHelper();
		ValueExpression valueExpressionEnableActionRemote= context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.enableActionRemote}", Boolean.class);
		Boolean enableActionRemote=(Boolean)valueExpressionEnableActionRemote.getValue(elContext);
		if(enableActionRemote){
			JSFUtilities.executeJavascriptFunction("oncomplete();");
		}
		clearHelper();
	}
	
	/**
	 * Clear helper.
	 */
	//CLEAR HELPER
	public void clearHelper(){

		 billingServiceHelperSource=null;
		 
		 FacesContext context = FacesContext.getCurrentInstance();
		 ELContext elContext = context.getELContext();
		 
		 ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.typeService}", Long.class);
		 Long typeService = (Long)valueExpression.getValue(elContext);
			
		 billingServiceSourceTo.setStatus(-1);
		 billingServiceSourceTo.setFileSourceName(null);
		 listBillingServiceSource.clear();
		 listEmpty = false;

	}
	

	
	/**
	 * Load data search.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadDataSearch() throws ServiceException{
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		
		if (Validations.validateListIsNullOrEmpty(listServiceStatus)){
			parameterTableTO.setMasterTableFk(PropertiesConstants.SERVICE_STATUS_PK);
			listServiceStatus= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}
		
		
	} 

	 
	
	/**
	 * Check all fields not null.
	 *
	 * @return true, if successful
	 * @throws IllegalArgumentException the illegal argument exception
	 * @throws IllegalAccessException the illegal access exception
	 */
	public boolean checkAllFieldsNotNull() throws IllegalArgumentException, IllegalAccessException{
		boolean boolResult = false;
		
		if(Validations.validateIsNotNullAndNotEmpty(billingServiceSourceTo.getFileSourceName())   ||
		   Validations.validateIsNotNullAndPositive(billingServiceSourceTo.getStatus())){
			boolResult = true;
		}
		return boolResult;
	}
		
	/**
	 * *MIGUEL DIAZ: METHOD CALLED FOR BLUR EVENT BILLING SERVICE.
	 */
	public void findBillingServiceByCode() {
		
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		ValueExpression valueExpressionServiceCode = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.billingServiceSource.idBillingServiceSourcePk}", String.class);
		Object valueServiceCode = valueExpressionServiceCode.getValue(elContext);
		
		ValueExpression valueExpressionBillingService= context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.billingServiceSource}", BillingServiceSource.class);
		Object value = valueExpressionBillingService.getValue(elContext);
		
		ValueExpression valueExpressionName = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.name}", String.class);
		helperName = (String)valueExpressionName.getValue(elContext);
		
			
		ValueExpression billingServiceSourceHelpMessageValueExp = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.resourceBundleMap.billingServiceSourceHelpMessage}", String.class);
		String billingServiceSourceHelpMessage= (String)billingServiceSourceHelpMessageValueExp.getValue(elContext);
		
		ValueExpression titleValueEx = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.resourceBundleMap.title}", String.class);
		String title= (String)titleValueEx.getValue(elContext);

		String serviceCode= (valueServiceCode).toString();
		
		ValueExpression valueExpressionEnableActionRemote= context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.enableActionRemote}", Boolean.class);
		Boolean enableActionRemote=(Boolean)valueExpressionEnableActionRemote.getValue(elContext);
		
		if(Validations.validateIsNotNullAndNotEmpty(serviceCode)){
			billingServiceSource=sourceBillingServiceFacade.findBillingServiceSource(serviceCode);
			/**Hacer una validacion del archivo**/
			if(validateIsCorrectStructureFileXml(billingServiceSource)){
				valueExpressionBillingService.setValue(elContext, billingServiceSource);
				if(enableActionRemote){
					 JSFUtilities.executeJavascriptFunction("oncomplete();");
				}
			
			}
			else {
				 billingServiceSource= new BillingServiceSource();
				 JSFUtilities.putViewMap("bodyMessageView", billingServiceSourceHelpMessage);
				 JSFUtilities.putViewMap("headerMessageView", title);		
				 RequestContext.getCurrentInstance().execute("PF('cnfwMsgCustomValidationBlur').show();");
				 valueExpressionBillingService.setValue(elContext, billingServiceSource);
			}
		}
		else{
			UIComponent uitextComp = UIComponent.getCurrentComponent(context);
			 if(uitextComp instanceof UIInput) {
				UIInput issuerText = (UIInput)uitextComp;
				issuerText.setValid(true);
				if(enableActionRemote){
					 JSFUtilities.executeJavascriptFunction("oncomplete();");
				}
			 }
			 billingServiceSource = new BillingServiceSource();
			 /**Si es EMPTY serviceCode, el usuario no ingresó valor alguno. No significa que no exista servicio*/
			 billingServiceSource.setIdBillingServiceSourcePk(null);
			 billingServiceSource.setFileSourceName(GeneralConstants.EMPTY_STRING);
			 valueExpressionBillingService.setValue(elContext, billingServiceSource);
			 
		}
		
	}
	

	/**
	 * Search billing services.
	 */
	public void searchBillingServices(){
		  
		try {
			listBillingServiceSource=sourceBillingServiceFacade.loadBillingServiceSource(this.billingServiceSourceTo);
			billingServiceHelperSource= new GenericDataModel<BillingServiceSource>(listBillingServiceSource); 
			if(listBillingServiceSource.size()==GeneralConstants.ZERO_VALUE_INTEGER){
				listEmpty = true;
			}else{
				listEmpty = false; 
			}
		} catch (ServiceException e) {
			
			e.printStackTrace();
		}
		
	}


	/**
	 * Clean billing service helper.
	 */
	public void cleanBillingServiceHelper(){
		
		 
		if(Validations.validateIsNotNullAndNotEmpty(billingServiceHelperSource) && billingServiceHelperSource.getRowCount() > 0) {
			
			billingServiceHelperSource = null ;			
		}
		
	}

	/**
	 * Gets the billing service source.
	 *
	 * @return the billingServiceSource
	 */
	public BillingServiceSource getBillingServiceSource() {
		return billingServiceSource;
	}

	/**
	 * Sets the billing service source.
	 *
	 * @param billingServiceSource the billingServiceSource to set
	 */
	public void setBillingServiceSource(BillingServiceSource billingServiceSource) {
		this.billingServiceSource = billingServiceSource;
	}

	/**
	 * Gets the billing service source to.
	 *
	 * @return the billingServiceSourceTo
	 */
	public BillingServiceSourceTo getBillingServiceSourceTo() {
		return billingServiceSourceTo;
	}

	/**
	 * Sets the billing service source to.
	 *
	 * @param billingServiceSourceTo the billingServiceSourceTo to set
	 */
	public void setBillingServiceSourceTo(
			BillingServiceSourceTo billingServiceSourceTo) {
		this.billingServiceSourceTo = billingServiceSourceTo;
	}

	/**
	 * Gets the list service status.
	 *
	 * @return the listServiceStatus
	 */
	public List<ParameterTable> getListServiceStatus() {
		return listServiceStatus;
	}

	/**
	 * Sets the list service status.
	 *
	 * @param listServiceStatus the listServiceStatus to set
	 */
	public void setListServiceStatus(List<ParameterTable> listServiceStatus) {
		this.listServiceStatus = listServiceStatus;
	}

	/**
	 * Checks if is list empty.
	 *
	 * @return the listEmpty
	 */
	public boolean isListEmpty() {
		return listEmpty;
	}

	/**
	 * Sets the list empty.
	 *
	 * @param listEmpty the listEmpty to set
	 */
	public void setListEmpty(boolean listEmpty) {
		this.listEmpty = listEmpty;
	}

	/**
	 * Gets the helper name.
	 *
	 * @return the helperName
	 */
	public String getHelperName() {
		return helperName;
	}

	/**
	 * Sets the helper name.
	 *
	 * @param helperName the helperName to set
	 */
	public void setHelperName(String helperName) {
		this.helperName = helperName;
	}

	/**
	 * Gets the billing service helper source.
	 *
	 * @return the billingServiceHelperSource
	 */
	public GenericDataModel<BillingServiceSource> getBillingServiceHelperSource() {
		return billingServiceHelperSource;
	}

	/**
	 * Sets the billing service helper source.
	 *
	 * @param billingServiceHelperSource the billingServiceHelperSource to set
	 */
	public void setBillingServiceHelperSource(
			GenericDataModel<BillingServiceSource> billingServiceHelperSource) {
		this.billingServiceHelperSource = billingServiceHelperSource;
	}

	/**
	 * Gets the list billing service source.
	 *
	 * @return the listBillingServiceSource
	 */
	public List<BillingServiceSource> getListBillingServiceSource() {
		return listBillingServiceSource;
	}

	/**
	 * Sets the list billing service source.
	 *
	 * @param listBillingServiceSource the listBillingServiceSource to set
	 */
	public void setListBillingServiceSource(
			List<BillingServiceSource> listBillingServiceSource) {
		this.listBillingServiceSource = listBillingServiceSource;
	}
	
	
	
}
