package com.pradera.billing.helper.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.el.ELContext;
import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.context.RequestContext;

import com.pradera.billing.billingservice.facade.BillingServiceMgmtServiceFacade;
import com.pradera.billing.billingservice.to.BillingServiceTo;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.HelperBean;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.type.BaseCollectionType;
import com.pradera.model.billing.type.BillingServiceFilter;
import com.pradera.model.generalparameter.ParameterTable;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingServiceHelperBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@HelperBean
public class BillingServiceHelperBean extends GenericBaseBean  implements Serializable { 
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8979684280696810462L;
	
	/**  Bean BillingService getting for container parent. */
	private BillingService billingService;
	
	/**  Bean BillingServiceTo used only for filter on search dialog. */
	private BillingServiceTo billingServiceTo;
	
    /**  Parameters for search. */
	private List<ParameterTable> listCollectionInstitution;
	
	/** The list service type. */
	private List<ParameterTable> listServiceType;
	
	/** The list service status. */
	private List<ParameterTable> listServiceStatus; 
	
	/** The list collection period. */
	private List<ParameterTable> listCollectionPeriod;
	
	/** The list collection base. */
	private List<ParameterTable> listCollectionBase;
	
	/** The list collection type. */
	private List<ParameterTable> listCollectionType;
	
	/** The list service client. */
	private List<ParameterTable> listServiceClient;
	
	/** The list empty. */
	private boolean listEmpty = false;
	
	/** The applied tax disable. */
	private Boolean appliedTaxDisable=Boolean.FALSE;
	
	/** The ind no integrated. */
	private Integer indNoIntegrated=PropertiesConstants.NOT_IND_INTEGRATED_BILL;
	
	/** The ind integrated. */
	private Integer indIntegrated=PropertiesConstants.YES_IND_INTEGRATED_BILL;
	
	/** The helper name. */
	private String helperName;
	
	/** Value Collection Manual*. */
	private Integer typeServiceManual=PropertiesConstants.SERVICE_TYPE_MANUAL_PK;
	 
	/** The billing service helper. */
	private GenericDataModel<BillingService> billingServiceHelper;
	
	/** The list billing service. */
	private List<BillingService> listBillingService;


	/** The billing service mgmt service facade. */
	@EJB
	BillingServiceMgmtServiceFacade billingServiceMgmtServiceFacade; 
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade  helperComponentFacade;
	
	

	/**
	 * Instantiates a new billing service helper bean.
	 */
	public BillingServiceHelperBean(){}
	
	/**
	 * Inits the.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException{
		/** Values by Default*/
		billingService = new BillingService();
		billingServiceTo= new BillingServiceTo();		
		listBillingService=new ArrayList<BillingService>();
		loadDataSearch();

		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.typeService}", Long.class);
		Object valueTypeSerice = valueExpression.getValue(elContext);
		Integer typeServicePk= Integer.parseInt(((Long)valueTypeSerice).toString());

		billingServiceTo.setInIntegratedBill(indNoIntegrated);
		if (Validations.validateListIsNotNullAndNotEmpty(listServiceType)){
				for ( ParameterTable parameterTable : listServiceType) {
					if (parameterTable.getParameterTablePk().equals(typeServicePk)){
						billingServiceTo.setServiceType(parameterTable.getParameterTablePk());
						billingServiceTo.setDescriptionServiceType(parameterTable.getDescription());
						if(typeServicePk.equals(PropertiesConstants.SERVICE_TYPE_MANUAL_PK))
							billingServiceTo.setBaseCollection(BaseCollectionType.COLLECTION_MANUAL.getCode());
						return;
					}
				}
		}
		
	}
	
	/**
	 * Assign billing service.
	 *
	 * @param billingServiceSelected the billing service selected
	 */
	public void assignBillingService(BillingService billingServiceSelected){
		
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		//getting holder object from inputtext
		ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.billingService}", BillingService.class);
		valueExpression.setValue(elContext, billingServiceSelected);
		clearHelper();
		ValueExpression valueExpressionEnableActionRemote= context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.enableActionRemote}", Boolean.class);
		Boolean enableActionRemote=(Boolean)valueExpressionEnableActionRemote.getValue(elContext);
		if(enableActionRemote){
			JSFUtilities.executeJavascriptFunction("oncomplete();");
		}
		clearHelper();
	}
	
	/**
	 * Clear Helper.
	 */
	public void clearHelper(){

		 billingServiceHelper=null;
		 billingServiceTo.setServiceCode(null);
		 
		 FacesContext context = FacesContext.getCurrentInstance();
		 ELContext elContext = context.getELContext();
		 
		 ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.typeService}", Long.class);
		 Long typeService = (Long)valueExpression.getValue(elContext);
		 if (typeService.equals(-1L)){
			 billingServiceTo.setServiceType(-1);
			 billingServiceTo.setBaseCollection(-1);
		 }			
		 billingServiceTo.setStateService(-1);
		 billingServiceTo.setServiceName(null);
		 billingServiceTo.setEntityCollection(-1);
		 billingServiceTo.setClientServiceType(-1);
		 billingServiceTo.setInIntegratedBill(indNoIntegrated);
		 listBillingService.clear();
		 listEmpty = false;

	}
	

	/**
	 * Gets the billing services.
	 *
	 * @return the billing services
	 */
	public BillingService getBillingServices() {
		return billingService;
	}


	/**
	 * Sets the billing services.
	 *
	 * @param billingServices the new billing services
	 */
	public void setBillingServices(BillingService billingServices) {
		this.billingService = billingServices;
	}

	/**
	 * Gets the billing service to.
	 *
	 * @return the billing service to
	 */
	public BillingServiceTo getBillingServiceTo() {
		return billingServiceTo;
	}


	/**
	 * Sets the billing service to.
	 *
	 * @param billingServiceTo the new billing service to
	 */
	public void setBillingServiceTo(BillingServiceTo billingServiceTo) {
		this.billingServiceTo = billingServiceTo;
	}

	/**
	 * Gets the list collection institution.
	 *
	 * @return the list collection institution
	 */
	public List<ParameterTable> getListCollectionInstitution() {
		return listCollectionInstitution;
	}

	/**
	 * Sets the list collection institution.
	 *
	 * @param listCollectionInstitution the new list collection institution
	 */
	public void setListCollectionInstitution(
			List<ParameterTable> listCollectionInstitution) {
		this.listCollectionInstitution = listCollectionInstitution;
	}

	/**
	 * Gets the list service type.
	 *
	 * @return the list service type
	 */
	public List<ParameterTable> getListServiceType() {
		return listServiceType;
	}

	/**
	 * Sets the list service type.
	 *
	 * @param listServiceType the new list service type
	 */
	public void setListServiceType(List<ParameterTable> listServiceType) {
		this.listServiceType = listServiceType;
	}

	/**
	 * Gets the list service status.
	 *
	 * @return the list service status
	 */
	public List<ParameterTable> getListServiceStatus() {
		return listServiceStatus;
	}

	/**
	 * Sets the list service status.
	 *
	 * @param listServiceStatus the new list service status
	 */
	public void setListServiceStatus(List<ParameterTable> listServiceStatus) {
		this.listServiceStatus = listServiceStatus;
	}

	/**
	 * Gets the list collection period.
	 *
	 * @return the list collection period
	 */
	public List<ParameterTable> getListCollectionPeriod() {
		return listCollectionPeriod;
	}

	/**
	 * Sets the list collection period.
	 *
	 * @param listCollectionPeriod the new list collection period
	 */
	public void setListCollectionPeriod(List<ParameterTable> listCollectionPeriod) {
		this.listCollectionPeriod = listCollectionPeriod;
	}

	/**
	 * Gets the list collection base.
	 *
	 * @return the list collection base
	 */
	public List<ParameterTable> getListCollectionBase() {
		return listCollectionBase;
	}

	/**
	 * Sets the list collection base.
	 *
	 * @param listCollectionBase the new list collection base
	 */
	public void setListCollectionBase(List<ParameterTable> listCollectionBase) {
		this.listCollectionBase = listCollectionBase;
	}

	/**
	 * Gets the list collection type.
	 *
	 * @return the list collection type
	 */
	public List<ParameterTable> getListCollectionType() {
		return listCollectionType;
	}

	/**
	 * Sets the list collection type.
	 *
	 * @param listCollectionType the new list collection type
	 */
	public void setListCollectionType(List<ParameterTable> listCollectionType) {
		this.listCollectionType = listCollectionType;
	}

	/**
	 * Gets the billing service.
	 *
	 * @return the billing service
	 */
	
	
	/***
	 * 
	 * @return
	 */
	public BillingService getBillingService() {
		return billingService;
	}

	/**
	 * Sets the billing service.
	 *
	 * @param billingService the new billing service
	 */
	public void setBillingService(BillingService billingService) {
		this.billingService = billingService;
	}

	/**
	 * Checks if is list empty.
	 *
	 * @return true, if is list empty
	 */
	public boolean isListEmpty() {
		return listEmpty;
	}

	/**
	 * Sets the list empty.
	 *
	 * @param listEmpty the new list empty
	 */
	public void setListEmpty(boolean listEmpty) {
		this.listEmpty = listEmpty;
	}

	/**
	 * Gets the billing service helper.
	 *
	 * @return the billing service helper
	 */
	public GenericDataModel<BillingService> getBillingServiceHelper() {
		return billingServiceHelper;
	}

	/**
	 * Sets the billing service helper.
	 *
	 * @param billingServiceHelper the new billing service helper
	 */
	public void setBillingServiceHelper(
			GenericDataModel<BillingService> billingServiceHelper) {
		this.billingServiceHelper = billingServiceHelper;
	}

	/**
	 * Gets the list billing service.
	 *
	 * @return the list billing service
	 */
	public List<BillingService> getListBillingService() {
		return listBillingService;
	}

	/**
	 * Sets the list billing service.
	 *
	 * @param listBillingService the new list billing service
	 */
	public void setListBillingService(List<BillingService> listBillingService) {
		this.listBillingService = listBillingService;
	}

	/**
	 * Gets the list service client.
	 *
	 * @return the list service client
	 */
	public List<ParameterTable> getListServiceClient() {
		return listServiceClient;
	}

	/**
	 * Sets the list service client.
	 *
	 * @param listServiceClient the new list service client
	 */
	public void setListServiceClient(List<ParameterTable> listServiceClient) {
		this.listServiceClient = listServiceClient;
	}


	/**
	 * Gets the applied tax disable.
	 *
	 * @return the applied tax disable
	 */
	public Boolean getAppliedTaxDisable() {
		return appliedTaxDisable;
	}

	/**
	 * Sets the applied tax disable.
	 *
	 * @param appliedTaxDisable the new applied tax disable
	 */
	public void setAppliedTaxDisable(Boolean appliedTaxDisable) {
		this.appliedTaxDisable = appliedTaxDisable;
	}

	/**
	 * Gets the ind no integrated.
	 *
	 * @return the ind no integrated
	 */
	public Integer getIndNoIntegrated() {
		return indNoIntegrated;
	}

	/**
	 * Sets the ind no integrated.
	 *
	 * @param indNoIntegrated the new ind no integrated
	 */
	public void setIndNoIntegrated(Integer indNoIntegrated) {
		this.indNoIntegrated = indNoIntegrated;
	}

	/**
	 * Gets the ind integrated.
	 *
	 * @return the ind integrated
	 */
	public Integer getIndIntegrated() {
		return indIntegrated;
	}

	/**
	 * Sets the ind integrated.
	 *
	 * @param indIntegrated the new ind integrated
	 */
	public void setIndIntegrated(Integer indIntegrated) {
		this.indIntegrated = indIntegrated;
	}
	
	

	/**
	 * Gets the type service manual.
	 *
	 * @return the typeServiceManual
	 */
	public Integer getTypeServiceManual() {
		return typeServiceManual;
	}

	/**
	 * Sets the type service manual.
	 *
	 * @param typeServiceManual the typeServiceManual to set
	 */
	public void setTypeServiceManual(Integer typeServiceManual) {
		this.typeServiceManual = typeServiceManual;
	}

	/**
	 * Load Data To Search.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadDataSearch() throws ServiceException{
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		if (Validations.validateListIsNullOrEmpty(listCollectionInstitution)){
			
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO.setMasterTableFk(BillingServiceFilter.ENTITY_COLLECTION_PK.getCode());
			listCollectionInstitution= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}
		if (Validations.validateListIsNullOrEmpty(listServiceStatus)){
				parameterTableTO.setMasterTableFk(BillingServiceFilter.SERVICE_STATUS_PK.getCode());
				listServiceStatus= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}
		if (Validations.validateListIsNullOrEmpty(listCollectionPeriod)){
				
				parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
				parameterTableTO.setOrderbyParameterTableCd(GeneralConstants.ONE_VALUE_INTEGER);
				parameterTableTO.setMasterTableFk(BillingServiceFilter.COLLECTION_PERIOD_PK.getCode());
				listCollectionPeriod= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}
		if (Validations.validateListIsNullOrEmpty(listCollectionBase)){

			 	parameterTableTO.setOrderbyParameterTableCd(GeneralConstants.ONE_VALUE_INTEGER);
				parameterTableTO.setMasterTableFk(BillingServiceFilter.COLLECTION_BASE_PK.getCode());
				parameterTableTO.setElementSubclasification1(null);
				listCollectionBase= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			}
		if (Validations.validateListIsNullOrEmpty(listServiceType)){
				parameterTableTO.setMasterTableFk(BillingServiceFilter.SERVICE_TYPE_PK.getCode());				
				listServiceType= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			}
		
		if (Validations.validateListIsNullOrEmpty(listServiceClient)){ 
			parameterTableTO.setMasterTableFk(BillingServiceFilter.SERVICE_CLIENT_PK.getCode());
			listServiceClient= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}
		
	} 

	 
	/**
	 * Validate Check All Field And Not Null.
	 *
	 * @return true, if successful
	 * @throws IllegalArgumentException the illegal argument exception
	 * @throws IllegalAccessException the illegal access exception
	 */
	public boolean checkAllFieldsNotNull() throws IllegalArgumentException, IllegalAccessException{
		boolean boolResult = false;
		
		if(Validations.validateIsNotNullAndPositive(billingServiceTo.getIdBillingServicePk())   ||
		   Validations.validateIsNotNullAndNotEmpty(billingServiceTo.getServiceCode()) ||
		   Validations.validateIsNotNullAndNotEmpty(billingServiceTo.getServiceName()) ||
		   Validations.validateIsNotNullAndNotEmpty(billingServiceTo.getServiceType())			  ||
		   Validations.validateIsNotNullAndNotEmpty(billingServiceTo.getClientServiceType())  ||
		   Validations.validateIsNotNullAndNotEmpty(billingServiceTo.getEntityCollection())       
		   ){
			boolResult = true;
		}
		return boolResult;
	}

	/**
	 * METHOD CALLED FOR BLUR EVENT BILLING SERVICE.
	 */
	public void findBillingServiceByCode() {
		
		Integer indIntegrated=null;
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		ValueExpression valueExpressionServiceCode = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.billingService.serviceCode}", String.class);
		Object valueServiceCode = valueExpressionServiceCode.getValue(elContext);
		
		ValueExpression valueExpressionBillingService= context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.billingService}", BillingService.class);
		Object value = valueExpressionBillingService.getValue(elContext);
		
		ValueExpression valueExpressionName = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.name}", String.class);
		helperName = (String)valueExpressionName.getValue(elContext);
		
		ValueExpression valueExpressionIntegrated = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.indIntegrated}", Integer.class);
		indIntegrated = (Integer)valueExpressionIntegrated.getValue(elContext);
			
		ValueExpression billingServiceHelpMessageValueExp = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.resourceBundleMap.billingServiceHelpMessage}", String.class);
		String billingServiceHelpMessage= (String)billingServiceHelpMessageValueExp.getValue(elContext);
		
		ValueExpression titleValueEx = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.resourceBundleMap.title}", String.class);
		String title= (String)titleValueEx.getValue(elContext);
		
		ValueExpression titleValueNoManual = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.resourceBundleMap.servicioNoManual}", String.class);
		String noManual= (String)titleValueNoManual.getValue(elContext);
		 
		String serviceCode= (valueServiceCode).toString();
		BillingServiceTo billingSearch=new BillingServiceTo();
		
		ValueExpression valueExpressionEnableActionRemote= context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.enableActionRemote}", Boolean.class);
		Boolean enableActionRemote=(Boolean)valueExpressionEnableActionRemote.getValue(elContext);

		
		if(Validations.validateIsNotNullAndNotEmpty(serviceCode)){
			billingSearch.setServiceCode(serviceCode);
			
			if(PropertiesConstants.YES_IND_INTEGRATED_BILL.equals(indIntegrated)||
				PropertiesConstants.NOT_IND_INTEGRATED_BILL.equals(indIntegrated)){
				billingSearch.setInIntegratedBill(indIntegrated);
			}
			
		 	billingService=this.billingServiceMgmtServiceFacade.getBillingService(billingSearch);
					  
			if(Validations.validateIsNotNullAndNotEmpty(billingService)){
				
				ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.typeService}", Long.class);
				Object valueTypeService = valueExpression.getValue(elContext);
				Integer valueTypeServiceInteger=Integer.parseInt(valueTypeService.toString());
			   
				if (valueTypeServiceInteger.equals(-1)){
					valueExpressionBillingService.setValue(elContext, billingService);
					if(enableActionRemote){
						 JSFUtilities.executeJavascriptFunction("oncomplete();");
					}
				}else {
					if (billingService.getServiceType().equals(valueTypeServiceInteger)){
						valueExpressionBillingService.setValue(elContext, billingService);
						if(enableActionRemote){
							 JSFUtilities.executeJavascriptFunction("oncomplete();");
						}
					}
					else {
						 billingService= new BillingService();
						 JSFUtilities.putViewMap("bodyMessageView", noManual);
						 JSFUtilities.putViewMap("headerMessageView", title);
						 JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationBlur').show();");
						 //RequestContext.getCurrentInstance().execute("cnfwMsgCustomValidationBlur.show();");
						 valueExpressionBillingService.setValue(elContext, billingService);
					}
				}	
				
				
			}
			else {
				 billingService= new BillingService();
				 JSFUtilities.putViewMap("bodyMessageView", billingServiceHelpMessage);
				 JSFUtilities.putViewMap("headerMessageView", title);		
				 JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationBlur').show();");
				 //RequestContext.getCurrentInstance().execute("cnfwMsgCustomValidationBlur.show();");
				 valueExpressionBillingService.setValue(elContext, billingService);
			}
		}
		else{
			
			
			UIComponent uitextComp = UIComponent.getCurrentComponent(context);
			 if(uitextComp instanceof UIInput) {
				UIInput issuerText = (UIInput)uitextComp;
				issuerText.setValid(true);
				if(enableActionRemote){
					 JSFUtilities.executeJavascriptFunction("oncomplete();");
				}
			 }
			 billingService = new BillingService();
			 /**If EMPTY serviceCode, 
			  * the user not input Value.  Does not mean that there is no service*/
			 billingService.setServiceCode(GeneralConstants.EMPTY_STRING);
			 billingService.setServiceName(GeneralConstants.EMPTY_STRING);
			 valueExpressionBillingService.setValue(elContext, billingService);
			 
		}
		
	}
	

	/**
	 * Search List The Billing Service.
	 */
	public void searchBillingServices(){
		  
		try {
			listBillingService=billingServiceMgmtServiceFacade.getListParameterBillingService(this.billingServiceTo);
			billingServiceHelper= new GenericDataModel<BillingService>(listBillingService); 
			if(listBillingService.size()==GeneralConstants.ZERO_VALUE_INTEGER){
				listEmpty = true;
			}else{
				listEmpty = false; 
			}
		} catch (ServiceException e) {
			
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Consult Billing Service By Link.
	 *
	 * @param event the event
	 * @throws ServiceException the service exception
	 */
	public void consultBillingService(ActionEvent event) throws ServiceException{
		
		if (event != null) {
			billingService = (BillingService) event.getComponent()
					.getAttributes().get("blockInformation");

			this.billingServiceTo = new BillingServiceTo(this.billingService);
		}
	}		
	

	/**
	 * Clean Billing Service Helper.
	 */
	public void cleanBillingServiceHelper(){
		
		 
		if(Validations.validateIsNotNullAndNotEmpty(billingServiceHelper) && billingServiceHelper.getRowCount() > 0) {
			
			billingServiceHelper = null ;			
		}
		
	}
	
	/**
	 * Setting Variables To Input.
	 */
	public void settingVariables(){
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		
		
		ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.indIntegrated}", Integer.class);
		Object valueTypeSerice = valueExpression.getValue(elContext);
		Integer indIntegrated= Integer.parseInt((valueTypeSerice).toString());
		if(PropertiesConstants.YES_IND_INTEGRATED_BILL.equals(indIntegrated)||
				PropertiesConstants.NOT_IND_INTEGRATED_BILL.equals(indIntegrated))
		{
			billingServiceTo.setInIntegratedBill(indIntegrated);
		}
		
	}
	
}
