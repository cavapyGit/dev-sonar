package com.pradera.billing.billingservice.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.pradera.commons.utils.PropertiesUtilities;




// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Enum RateServiceStatus.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public enum RateServiceStatus {
	

	/** The active. */
	ACTIVE(Integer.valueOf(1464), PropertiesUtilities.getMessage("rate.service.status.active")),
	
	/** The cancelled. */
	CANCELLED(Integer.valueOf(1466), PropertiesUtilities.getMessage("rate.service.status.cancelled")),
	
	/** The expired. */
	EXPIRED(Integer.valueOf(1465), PropertiesUtilities.getMessage("rate.service.status.expired")),
	
	/** The pending. */
	PENDING(Integer.valueOf(0), PropertiesUtilities.getMessage("rate.service.status.pending")),
    
    /** The register. */
    REGISTER(Integer.valueOf(1552), PropertiesUtilities.getMessage("rate.service.status.register")),
	
	/** The lock. */
	LOCK(Integer.valueOf(1553), PropertiesUtilities.getMessage("rate.service.status.lock"));
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<BillingServiceStatus> list = new ArrayList<BillingServiceStatus>();
	
	/** The Constant lookup. */
	public static final Map<Integer, BillingServiceStatus> lookup = new HashMap<Integer, BillingServiceStatus>();
	
	
	static {
		for (BillingServiceStatus s : EnumSet.allOf(BillingServiceStatus.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * Instantiates a new rate service status.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private RateServiceStatus(Integer code, String value) {
		this.code = code;
		this.value = value;	
	
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}	
	


	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return the description
	 */
	public String getDescription(Locale locale) {
		return PropertiesUtilities.getMessage(locale, this.getValue());
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the billing service status
	 */
	public static BillingServiceStatus get(Integer code) {
		return lookup.get(code);
	}
}
