package com.pradera.billing.billingservice.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.billing.BillingService;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingServiceDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class BillingServiceDataModel  extends ListDataModel<BillingService> implements Serializable,SelectableDataModel<BillingService> {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new billing service data model.
	 */
	public BillingServiceDataModel(){
				
	}
	
	/**
	 * Instantiates a new billing service data model.
	 *
	 * @param data the data
	 */
	public BillingServiceDataModel(List<BillingService> data){
		super(data);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public BillingService getRowData(String rowKey) {
		@SuppressWarnings("unchecked")
		List<BillingService> billingServiceList=(List<BillingService>)getWrappedData();
        for(BillingService billingService : billingServiceList) {  
        	
            if(String.valueOf(billingService.getIdBillingServicePk()).equals(rowKey))
                return billingService;  
        }  
		return null;
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(BillingService billingService) {

		return billingService.getIdBillingServicePk();
	}
	
	
}
