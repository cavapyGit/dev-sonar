package com.pradera.billing.billingservice.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.pradera.billing.billingrate.to.ServiceRateTo;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.billing.BillingService;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingServiceTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class BillingServiceTo implements Serializable {
	
	
	/** The Constant serialVersionUID. */
	private static final long 		serialVersionUID = 1L;
	
	/** The id billing service pk. */
	private Long 					idBillingServicePk;
	
	/** The service type. */
	private Integer 				serviceType;
	
	/** The entity collection. */
	private Integer 				entityCollection;
	
	/** The state service. */
	private Integer 				stateService;
	
	/** The base collection. */
	private Integer 				baseCollection;
	
	/** The calculation period. */
	private Integer 				calculationPeriod;
	
	private Integer portfolio;
	
	/** The service code. */
	private String 					serviceCode;
	
	/** The service name. */
	private String 					serviceName;
	
	/** The end effective date. */
	private Date 					endEffectiveDate;
	
	/** The initial effective date. */
	private Date 					initialEffectiveDate;
	
	
	/** The description collection entity. */
	private String 					descriptionCollectionEntity;
	
	/** The description base collection. */
	private String 					descriptionBaseCollection;
	
	/** The description state billing service. */
	private String 					descriptionStateBillingService;
	
	/** The description service type. */
	private String 					descriptionServiceType;
	
	/** The description collection period. */
	private String 					descriptionCollectionPeriod;
	
	/** The description client service type. */
	private String 					descriptionClientServiceType;
	
	/** The description calculation period. */
	private String 					descriptionCalculationPeriod;
	
	/** The description name collection entity. */
	private String 					descriptionNameCollectionEntity;
	
	/** The description currency. */
	private String 					descriptionCurrency;
	
	
	/** The collection period. */
	private Integer 				collectionPeriod;
	
	/** The client service type. */
	private Integer 				clientServiceType;
	
	/** The currency billing. */
	private Integer 				currencyBilling;
	
	/** The currency calculation. */
	private Integer 				currencyCalculation;
	
	/** The source information. */
	private Integer 				sourceInformation;
	
	/** The in integrated bill. */
	private Integer 				inIntegratedBill;
	
	/** The ind source. */
	private Integer 				indSource;
	
	/** The id source pk. */
	private Long					idSourcePk;
	
	
	
	/** The date calculation period. */
	private Date 					dateCalculationPeriod;
	
	/** The calculate daily. */
	private Boolean 				calculateDaily = Boolean.FALSE;
	
	/** The calculate monthly. */
	private Boolean 				calculateMonthly= Boolean.FALSE;
	
	/** The reference rate. */
	private String 					referenceRate;
	


	/** The list code service rate. */
	private List<Integer> 			listCodeServiceRate;

	/** The service rate to selected. */
	private ServiceRateTo 			serviceRateToSelected;
	
	/** The list service rate to. */
	private List<ServiceRateTo> 	listServiceRateTo;
	
	/** The list service rate to modify. */
	private List<ServiceRateTo> 	listServiceRateToModify;
		

	/**
	 * Instantiates a new billing service to.
	 *
	 * @param idBillingServicePk the id billing service pk
	 */
	public BillingServiceTo(Long idBillingServicePk) {
		this.idBillingServicePk = idBillingServicePk;
	}
	
	

	/**
	 * Instantiates a new billing service to.
	 *
	 * @param idBillingServicePk the id billing service pk
	 * @param serviceCode the service code
	 */
	public BillingServiceTo(Long idBillingServicePk, String serviceCode) {
		this.idBillingServicePk = idBillingServicePk;
		this.serviceCode = serviceCode;
	}



	/**
	 * Instantiates a new billing service to.
	 */
	public BillingServiceTo(){
		this.listServiceRateTo= new ArrayList<ServiceRateTo>();
		this.listCodeServiceRate= new ArrayList<Integer>();
		
	}
	
	

	/**
	 * Instantiates a new billing service to.
	 *
	 * @param billingService the billing service
	 */
	public BillingServiceTo(BillingService billingService){
		this.initialEffectiveDate=billingService.getInitialEffectiveDate();
		this.endEffectiveDate=billingService.getEndEffectiveDate();		
		this.idBillingServicePk=billingService.getIdBillingServicePk();
		this.serviceCode=billingService.getServiceCode();
		this.serviceType=billingService.getServiceType();
		this.serviceName=billingService.getServiceName();
		this.descriptionStateBillingService= billingService.getDescriptionStateBillingService();
		this.baseCollection=billingService.getBaseCollection();
		this.entityCollection=billingService.getEntityCollection();
		this.listCodeServiceRate= new ArrayList<Integer>(); /** setting new rate service codes */
		this.listServiceRateTo= new ArrayList<ServiceRateTo>();
		this.currencyBilling=billingService.getCurrencyBilling();
		this.indSource=billingService.getIndSourceBaseCollection();
		this.currencyCalculation=billingService.getCurrencyCalculation();
		this.sourceInformation=billingService.getSourceInformation();
		this.referenceRate=billingService.getReferenceRate();
		this.portfolio=billingService.getPortfolio();
	}
	
	
 
	/**
	 * Adds the service rate to.
	 *
	 * @param serviceRateTo the service rate to
	 */
	public void addServiceRateTo(ServiceRateTo serviceRateTo){
		
		if (Validations.validateIsNotNull(listServiceRateTo)){
			listServiceRateTo.add(serviceRateTo);
		}
	}
	
	/**
	 * Delete service rate to.
	 *
	 * @param serviceRateTo the service rate to
	 */
	public void deleteServiceRateTo(ServiceRateTo serviceRateTo){
		if (Validations.validateIsNotNull(listServiceRateTo)){
			listServiceRateTo.remove(serviceRateTo);
		}
	}

	/**
	 * Adds the service rate to modify.
	 *
	 * @param serviceRateTo the service rate to
	 */
	public void addServiceRateToModify(ServiceRateTo serviceRateTo){
		
		if(Validations.validateIsNull(listServiceRateToModify)){
			listServiceRateToModify= new ArrayList<ServiceRateTo>();
		}

		listServiceRateToModify.add(serviceRateTo);
	}
	
	/**
	 * Delete service rate to modify.
	 *
	 * @param serviceRateTo the service rate to
	 */
	public void deleteServiceRateToModify(ServiceRateTo serviceRateTo){
		if (Validations.validateIsNotNull(listServiceRateToModify)){
			listServiceRateToModify.remove(serviceRateTo);
		}
	}
	
	/**
	 * Exist rate services by status.
	 *
	 * @param status the status
	 * @return the boolean
	 */
	public Boolean existRateServicesByStatus(Integer status){
		Boolean exist=false;
		if (Validations.validateIsNotNullAndNotEmpty(this.listServiceRateTo)){

			for (ServiceRateTo serviceRateTo : listServiceRateTo) {
				if (serviceRateTo.getRateStatus().intValue()==status.intValue()){	
					return true;
				}
			}
		}
		
		return exist;
	}
	
	/**
	 * Adds the new code rate service.
	 *
	 * @param code the code
	 */
	public void addNewCodeRateService(Integer code){
		if (!Validations.validateListIsNullOrEmpty(listCodeServiceRate)){
			listCodeServiceRate.add(code);
		}
	}
	
	/**
	 * Removes the last code rate service.
	 *
	 * @param code the code
	 */
	public void removeLastCodeRateService(Integer code){
		if (!Validations.validateListIsNullOrEmpty(listCodeServiceRate)){
			listCodeServiceRate.remove(code);
		}
	}



	/**
	 * Gets the id billing service pk.
	 *
	 * @return the idBillingServicePk
	 */
	public Long getIdBillingServicePk() {
		return idBillingServicePk;
	}



	/**
	 * Sets the id billing service pk.
	 *
	 * @param idBillingServicePk the idBillingServicePk to set
	 */
	public void setIdBillingServicePk(Long idBillingServicePk) {
		this.idBillingServicePk = idBillingServicePk;
	}



	/**
	 * Gets the service type.
	 *
	 * @return the serviceType
	 */
	public Integer getServiceType() {
		return serviceType;
	}



	/**
	 * Sets the service type.
	 *
	 * @param serviceType the serviceType to set
	 */
	public void setServiceType(Integer serviceType) {
		this.serviceType = serviceType;
	}



	/**
	 * Gets the entity collection.
	 *
	 * @return the entityCollection
	 */
	public Integer getEntityCollection() {
		return entityCollection;
	}



	/**
	 * Sets the entity collection.
	 *
	 * @param entityCollection the entityCollection to set
	 */
	public void setEntityCollection(Integer entityCollection) {
		this.entityCollection = entityCollection;
	}



	/**
	 * Gets the state service.
	 *
	 * @return the stateService
	 */
	public Integer getStateService() {
		return stateService;
	}



	/**
	 * Sets the state service.
	 *
	 * @param stateService the stateService to set
	 */
	public void setStateService(Integer stateService) {
		this.stateService = stateService;
	}



	/**
	 * Gets the base collection.
	 *
	 * @return the baseCollection
	 */
	public Integer getBaseCollection() {
		return baseCollection;
	}



	/**
	 * Sets the base collection.
	 *
	 * @param baseCollection the baseCollection to set
	 */
	public void setBaseCollection(Integer baseCollection) {
		this.baseCollection = baseCollection;
	}



	/**
	 * Gets the calculation period.
	 *
	 * @return the calculationPeriod
	 */
	public Integer getCalculationPeriod() {
		return calculationPeriod;
	}



	/**
	 * Sets the calculation period.
	 *
	 * @param calculationPeriod the calculationPeriod to set
	 */
	public void setCalculationPeriod(Integer calculationPeriod) {
		this.calculationPeriod = calculationPeriod;
	}



	/**
	 * Gets the service code.
	 *
	 * @return the serviceCode
	 */
	public String getServiceCode() {
		return serviceCode;
	}



	/**
	 * Sets the service code.
	 *
	 * @param serviceCode the serviceCode to set
	 */
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}



	/**
	 * Gets the service name.
	 *
	 * @return the serviceName
	 */
	public String getServiceName() {
		return serviceName;
	}



	/**
	 * Sets the service name.
	 *
	 * @param serviceName the serviceName to set
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}



	/**
	 * Gets the end effective date.
	 *
	 * @return the endEffectiveDate
	 */
	public Date getEndEffectiveDate() {
		return endEffectiveDate;
	}



	/**
	 * Sets the end effective date.
	 *
	 * @param endEffectiveDate the endEffectiveDate to set
	 */
	public void setEndEffectiveDate(Date endEffectiveDate) {
		this.endEffectiveDate = endEffectiveDate;
	}



	/**
	 * Gets the initial effective date.
	 *
	 * @return the initialEffectiveDate
	 */
	public Date getInitialEffectiveDate() {
		return initialEffectiveDate;
	}



	/**
	 * Sets the initial effective date.
	 *
	 * @param initialEffectiveDate the initialEffectiveDate to set
	 */
	public void setInitialEffectiveDate(Date initialEffectiveDate) {
		this.initialEffectiveDate = initialEffectiveDate;
	}



	/**
	 * Gets the list service rate to.
	 *
	 * @return the listServiceRateTo
	 */
	public List<ServiceRateTo> getListServiceRateTo() {
		return listServiceRateTo;
	}



	/**
	 * Sets the list service rate to.
	 *
	 * @param listServiceRateTo the listServiceRateTo to set
	 */
	public void setListServiceRateTo(List<ServiceRateTo> listServiceRateTo) {
		this.listServiceRateTo = listServiceRateTo;
	}



	/**
	 * Gets the list service rate to modify.
	 *
	 * @return the listServiceRateToModify
	 */
	public List<ServiceRateTo> getListServiceRateToModify() {
		return listServiceRateToModify;
	}



	/**
	 * Sets the list service rate to modify.
	 *
	 * @param listServiceRateToModify the listServiceRateToModify to set
	 */
	public void setListServiceRateToModify(
			List<ServiceRateTo> listServiceRateToModify) {
		this.listServiceRateToModify = listServiceRateToModify;
	}



//	/**
//	 * @return the rateServiceToDataModel
//	 */
//	public RateServiceToDataModel getRateServiceToDataModel() {
//		return rateServiceToDataModel;
//	}
//
//
//
//	/**
//	 * @param rateServiceToDataModel the rateServiceToDataModel to set
//	 */
//	public void setRateServiceToDataModel(
//			RateServiceToDataModel rateServiceToDataModel) {
//		this.rateServiceToDataModel = rateServiceToDataModel;
//	}



	/**
 * Gets the list code service rate.
 *
 * @return the listCodeServiceRate
 */
	public List<Integer> getListCodeServiceRate() {
		return listCodeServiceRate;
	}



	/**
	 * Sets the list code service rate.
	 *
	 * @param listCodeServiceRate the listCodeServiceRate to set
	 */
	public void setListCodeServiceRate(List<Integer> listCodeServiceRate) {
		this.listCodeServiceRate = listCodeServiceRate;
	}



	/**
	 * Gets the service rate to selected.
	 *
	 * @return the serviceRateToSelected
	 */
	public ServiceRateTo getServiceRateToSelected() {
		return serviceRateToSelected;
	}



	/**
	 * Sets the service rate to selected.
	 *
	 * @param serviceRateToSelected the serviceRateToSelected to set
	 */
	public void setServiceRateToSelected(ServiceRateTo serviceRateToSelected) {
		this.serviceRateToSelected = serviceRateToSelected;
	}



	/**
	 * Gets the description collection entity.
	 *
	 * @return the descriptionCollectionEntity
	 */
	public String getDescriptionCollectionEntity() {
		return descriptionCollectionEntity;
	}



	/**
	 * Sets the description collection entity.
	 *
	 * @param descriptionCollectionEntity the descriptionCollectionEntity to set
	 */
	public void setDescriptionCollectionEntity(String descriptionCollectionEntity) {
		this.descriptionCollectionEntity = descriptionCollectionEntity;
	}



	/**
	 * Gets the description base collection.
	 *
	 * @return the descriptionBaseCollection
	 */
	public String getDescriptionBaseCollection() {
		return descriptionBaseCollection;
	}



	/**
	 * Sets the description base collection.
	 *
	 * @param descriptionBaseCollection the descriptionBaseCollection to set
	 */
	public void setDescriptionBaseCollection(String descriptionBaseCollection) {
		this.descriptionBaseCollection = descriptionBaseCollection;
	}



	/**
	 * Gets the description state billing service.
	 *
	 * @return the descriptionStateBillingService
	 */
	public String getDescriptionStateBillingService() {
		return descriptionStateBillingService;
	}



	/**
	 * Sets the description state billing service.
	 *
	 * @param descriptionStateBillingService the descriptionStateBillingService to set
	 */
	public void setDescriptionStateBillingService(
			String descriptionStateBillingService) {
		this.descriptionStateBillingService = descriptionStateBillingService;
	}



	/**
	 * Gets the description service type.
	 *
	 * @return the descriptionServiceType
	 */
	public String getDescriptionServiceType() {
		return descriptionServiceType;
	}



	/**
	 * Sets the description service type.
	 *
	 * @param descriptionServiceType the descriptionServiceType to set
	 */
	public void setDescriptionServiceType(String descriptionServiceType) {
		this.descriptionServiceType = descriptionServiceType;
	}



	/**
	 * Gets the description collection period.
	 *
	 * @return the descriptionCollectionPeriod
	 */
	public String getDescriptionCollectionPeriod() {
		return descriptionCollectionPeriod;
	}



	/**
	 * Sets the description collection period.
	 *
	 * @param descriptionCollectionPeriod the descriptionCollectionPeriod to set
	 */
	public void setDescriptionCollectionPeriod(String descriptionCollectionPeriod) {
		this.descriptionCollectionPeriod = descriptionCollectionPeriod;
	}



	/**
	 * Gets the description client service type.
	 *
	 * @return the descriptionClientServiceType
	 */
	public String getDescriptionClientServiceType() {
		return descriptionClientServiceType;
	}



	/**
	 * Sets the description client service type.
	 *
	 * @param descriptionClientServiceType the descriptionClientServiceType to set
	 */
	public void setDescriptionClientServiceType(String descriptionClientServiceType) {
		this.descriptionClientServiceType = descriptionClientServiceType;
	}



	/**
	 * Gets the description calculation period.
	 *
	 * @return the descriptionCalculationPeriod
	 */
	public String getDescriptionCalculationPeriod() {
		return descriptionCalculationPeriod;
	}



	/**
	 * Sets the description calculation period.
	 *
	 * @param descriptionCalculationPeriod the descriptionCalculationPeriod to set
	 */
	public void setDescriptionCalculationPeriod(String descriptionCalculationPeriod) {
		this.descriptionCalculationPeriod = descriptionCalculationPeriod;
	}



	/**
	 * Gets the description name collection entity.
	 *
	 * @return the descriptionNameCollectionEntity
	 */
	public String getDescriptionNameCollectionEntity() {
		return descriptionNameCollectionEntity;
	}



	/**
	 * Sets the description name collection entity.
	 *
	 * @param descriptionNameCollectionEntity the descriptionNameCollectionEntity to set
	 */
	public void setDescriptionNameCollectionEntity(
			String descriptionNameCollectionEntity) {
		this.descriptionNameCollectionEntity = descriptionNameCollectionEntity;
	}



	/**
	 * Gets the description currency.
	 *
	 * @return the descriptionCurrency
	 */
	public String getDescriptionCurrency() {
		return descriptionCurrency;
	}



	/**
	 * Sets the description currency.
	 *
	 * @param descriptionCurrency the descriptionCurrency to set
	 */
	public void setDescriptionCurrency(String descriptionCurrency) {
		this.descriptionCurrency = descriptionCurrency;
	}



	/**
	 * Gets the collection period.
	 *
	 * @return the collectionPeriod
	 */
	public Integer getCollectionPeriod() {
		return collectionPeriod;
	}



	/**
	 * Sets the collection period.
	 *
	 * @param collectionPeriod the collectionPeriod to set
	 */
	public void setCollectionPeriod(Integer collectionPeriod) {
		this.collectionPeriod = collectionPeriod;
	}



	/**
	 * Gets the client service type.
	 *
	 * @return the clientServiceType
	 */
	public Integer getClientServiceType() {
		return clientServiceType;
	}



	/**
	 * Sets the client service type.
	 *
	 * @param clientServiceType the clientServiceType to set
	 */
	public void setClientServiceType(Integer clientServiceType) {
		this.clientServiceType = clientServiceType;
	}



	/**
	 * Gets the currency billing.
	 *
	 * @return the currencyBilling
	 */
	public Integer getCurrencyBilling() {
		return currencyBilling;
	}



	/**
	 * Sets the currency billing.
	 *
	 * @param currencyBilling the currencyBilling to set
	 */
	public void setCurrencyBilling(Integer currencyBilling) {
		this.currencyBilling = currencyBilling;
	}



	/**
	 * Gets the in integrated bill.
	 *
	 * @return the inIntegratedBill
	 */
	public Integer getInIntegratedBill() {
		return inIntegratedBill;
	}



	/**
	 * Sets the in integrated bill.
	 *
	 * @param inIntegratedBill the inIntegratedBill to set
	 */
	public void setInIntegratedBill(Integer inIntegratedBill) {
		this.inIntegratedBill = inIntegratedBill;
	}



	/**
	 * Gets the ind source.
	 *
	 * @return the indSource
	 */
	public Integer getIndSource() {
		return indSource;
	}



	/**
	 * Sets the ind source.
	 *
	 * @param indSource the indSource to set
	 */
	public void setIndSource(Integer indSource) {
		this.indSource = indSource;
	}



	/**
	 * Gets the id source pk.
	 *
	 * @return the idSourcePk
	 */
	public Long getIdSourcePk() {
		return idSourcePk;
	}



	/**
	 * Sets the id source pk.
	 *
	 * @param idSourcePk the idSourcePk to set
	 */
	public void setIdSourcePk(Long idSourcePk) {
		this.idSourcePk = idSourcePk;
	}



	/**
	 * Gets the date calculation period.
	 *
	 * @return the dateCalculationPeriod
	 */
	public Date getDateCalculationPeriod() {
		return dateCalculationPeriod;
	}



	/**
	 * Sets the date calculation period.
	 *
	 * @param dateCalculationPeriod the dateCalculationPeriod to set
	 */
	public void setDateCalculationPeriod(Date dateCalculationPeriod) {
		this.dateCalculationPeriod = dateCalculationPeriod;
	}



	/**
	 * Gets the calculate daily.
	 *
	 * @return the calculateDaily
	 */
	public Boolean getCalculateDaily() {
		return calculateDaily;
	}



	/**
	 * Sets the calculate daily.
	 *
	 * @param calculateDaily the calculateDaily to set
	 */
	public void setCalculateDaily(Boolean calculateDaily) {
		this.calculateDaily = calculateDaily;
	}



	/**
	 * Gets the calculate monthly.
	 *
	 * @return the calculateMonthly
	 */
	public Boolean getCalculateMonthly() {
		return calculateMonthly;
	}



	/**
	 * Sets the calculate monthly.
	 *
	 * @param calculateMonthly the calculateMonthly to set
	 */
	public void setCalculateMonthly(Boolean calculateMonthly) {
		this.calculateMonthly = calculateMonthly;
	}



	/**
	 * Gets the currency calculation.
	 *
	 * @return the currencyCalculation
	 */
	public Integer getCurrencyCalculation() {
		return currencyCalculation;
	}



	/**
	 * Sets the currency calculation.
	 *
	 * @param currencyCalculation the currencyCalculation to set
	 */
	public void setCurrencyCalculation(Integer currencyCalculation) {
		this.currencyCalculation = currencyCalculation;
	}



	/**
	 * Gets the source information.
	 *
	 * @return the sourceInformation
	 */
	public Integer getSourceInformation() {
		return sourceInformation;
	}



	/**
	 * Sets the source information.
	 *
	 * @param sourceInformation the sourceInformation to set
	 */
	public void setSourceInformation(Integer sourceInformation) {
		this.sourceInformation = sourceInformation;
	}

	/**
	 * Gets the reference rate.
	 *
	 * @return the referenceRate
	 */
	public String getReferenceRate() {
		return referenceRate;
	}

	/**
	 * Sets the reference rate.
	 *
	 * @param referenceRate the referenceRate to set
	 */
	public void setReferenceRate(String referenceRate) {
		this.referenceRate = referenceRate;
	}



	/**
	 * @return the portfolio
	 */
	public Integer getPortfolio() {
		return portfolio;
	}



	/**
	 * @param portfolio the portfolio to set
	 */
	public void setPortfolio(Integer portfolio) {
		this.portfolio = portfolio;
	}
	
	
	


	
}
