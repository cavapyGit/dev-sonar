package com.pradera.billing.billingservice.to;

import java.io.Serializable;
import java.util.Date;

import com.pradera.model.billing.RequestService;

public class BillingRegServiceTo implements Serializable {
	private static final long serialVersionUID = 1L;	
	private String obsText;	
	private String servName;
	private String servCode;
	private Date obsDate;
	private Integer obsState;
	private Long idRequestServicePk;
	private String clientName;
	private String clientNit;
	private Date initialDate;
	private Date finalDate;
	private String descriptionStateRequest;
	
	public BillingRegServiceTo() {
		
	}
	
	public BillingRegServiceTo(RequestService reqService) {
		this.obsText = reqService.getObsText();
		this.servName = reqService.getServName();
		this.servCode = reqService.getServCode();
		this.obsDate = reqService.getObsDate();
		this.obsState = reqService.getObsState();
		this.idRequestServicePk = reqService.getIdRequestServicePk();
	}	
	
	public String getObsText() {
		return obsText;
	}
	public void setObsText(String obsText) {
		this.obsText = obsText;
	}
	public String getServName() {
		return servName;
	}
	public void setServName(String servName) {
		this.servName = servName;
	}
	public String getServCode() {
		return servCode;
	}
	public void setServCode(String servCode) {
		this.servCode = servCode;
	}
	public Date getObsDate() {
		return obsDate;
	}
	public void setObsDate(Date obsDate) {
		this.obsDate = obsDate;
	}
	public Integer getObsState() {
		return obsState;
	}
	public void setObsState(Integer obsState) {
		this.obsState = obsState;
	}	
	public Long getIdRequestServicePk() {
		return idRequestServicePk;
	}
	public void setIdRequestServicePk(Long idRequestServicePk) {
		this.idRequestServicePk = idRequestServicePk;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getClientNit() {
		return clientNit;
	}
	public void setClientNit(String clientNit) {
		this.clientNit = clientNit;
	}
	public Date getInitialDate() {
		return initialDate;
	}
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	public Date getFinalDate() {
		return finalDate;
	}
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}
	public String getDescriptionStateRequest() {
		return descriptionStateRequest;
	}
	public void setDescriptionStateRequest(String descriptionStateRequest) {
		this.descriptionStateRequest = descriptionStateRequest;
	}	
	
	
}
