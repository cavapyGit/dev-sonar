package com.pradera.billing.billingservice.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum RequestServiceStatus {
	REGISTRADO(Integer.valueOf(1),"REGISTRADO"),
	CONFIRMADO(Integer.valueOf(2),"CONFIRMADO"),
	RECHAZADO(Integer.valueOf(3),"RECHAZADO");
	
	private Integer code;
	private String value;
	
	public Integer getCode() {
		return code;
	}
	
	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
			
	private RequestServiceStatus(Integer code, String value) {
		this.code = code;
		this.value = value;
	}	
	/** The Constant list. */
	public static final List<RequestServiceStatus> list = new ArrayList<RequestServiceStatus>();	
	/** The Constant lookup. */
	public static final Map<Integer, RequestServiceStatus> lookup = new HashMap<Integer, RequestServiceStatus>();
	static {
		for (RequestServiceStatus s : EnumSet.allOf(RequestServiceStatus.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static RequestServiceStatus get(Integer code) {
		return lookup.get(code);
	}
	
}
