package com.pradera.billing.billingservice.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.pradera.commons.utils.PropertiesUtilities;




// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Enum BillingServiceStatus.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public enum BillingServiceStatus {
	
 	  /** The registered. */
  	 REGISTERED(Integer.valueOf(252), PropertiesUtilities.getMessage("billing.service.status.registered")),
	  
  	/** The locked. */
  	LOCKED(Integer.valueOf(1410), PropertiesUtilities.getMessage("billing.service.status.blocked")),
	  
  	/** The expired. */
  	EXPIRED(Integer.valueOf(1411), PropertiesUtilities.getMessage("billing.service.status.expired")),
	  
  	/** The actived. */
  	ACTIVED(Integer.valueOf(1546), PropertiesUtilities.getMessage("billing.service.status.actived")),
	  
  	/** The cancelled. */
  	CANCELLED(Integer.valueOf(1409),PropertiesUtilities.getMessage("billing.service.status.cancelled"));
 	  

	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<BillingServiceStatus> list = new ArrayList<BillingServiceStatus>();
	
	/** The Constant lookup. */
	public static final Map<Integer, BillingServiceStatus> lookup = new HashMap<Integer, BillingServiceStatus>();
	
	
	static {
		for (BillingServiceStatus s : EnumSet.allOf(BillingServiceStatus.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * Instantiates a new billing service status.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private BillingServiceStatus(Integer code, String value) {
		this.code = code;
		this.value = value;	
	
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}	
	


	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return the description
	 */
	public String getDescription(Locale locale) {
		return PropertiesUtilities.getMessage(locale, this.getValue());
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the billing service status
	 */
	public static BillingServiceStatus get(Integer code) {
		return lookup.get(code);
	}
}
