package com.pradera.billing.billingservice.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;

import com.pradera.billing.billingcollection.facade.CollectionServiceMgmtServiceFacade;
import com.pradera.billing.billingrate.facade.BillingRateMgmtServiceFacade;
import com.pradera.billing.billingrate.to.RateMovementTo;
import com.pradera.billing.billingrate.to.RateServiceScaleToDataModel;
import com.pradera.billing.billingrate.to.RateServiceToDataModel;
import com.pradera.billing.billingrate.to.ServiceRateScaleTo;
import com.pradera.billing.billingrate.to.ServiceRateTo;
import com.pradera.billing.billingservice.facade.BillingServiceMgmtServiceFacade;
import com.pradera.billing.billingservice.to.BillingServiceTo;
import com.pradera.billing.billingservice.type.BillingServiceStatus;
import com.pradera.billing.billingservice.type.RateServiceStatus;
import com.pradera.billing.billingsource.facade.SourceBillingServiceFacade;
import com.pradera.billing.common.facade.BusinessLayerCommonFacade;
import com.pradera.billing.common.utils.BillingUtils;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.billing.BillingEconomicActivity;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.BillingServiceSource;
import com.pradera.model.billing.ServiceRate;
import com.pradera.model.billing.type.RateStateType;
import com.pradera.model.billing.type.RateType;
import com.pradera.model.component.MovementType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingServiceMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class BillingServiceMgmtBean extends GenericBaseBean  implements Serializable {


	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;


	/** The billing service mgmt service facade. */
	@EJB
	BillingServiceMgmtServiceFacade billingServiceMgmtServiceFacade;

	/** The billing rate mgmt service facade. */
	@EJB
	BillingRateMgmtServiceFacade billingRateMgmtServiceFacade;

	/** The collection service mgmt service facade. */
	@EJB
	CollectionServiceMgmtServiceFacade collectionServiceMgmtServiceFacade;	
	
	/** The source billing service facade. */
	@EJB
	SourceBillingServiceFacade sourceBillingServiceFacade;

	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	/** The business layer common facade. */
	@EJB
	BusinessLayerCommonFacade businessLayerCommonFacade;

	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;

	/** The Constant log. */
	private static final Logger log = Logger.getLogger(BillingServiceMgmtBean.class);

	/**  attributes for get values on page new billing service. */
	private Integer 	collectionInstitutionSelected; 
	
	/** The service type selected. */
	private Integer 	serviceTypeSelected ;
	
	/** The service status selected. */
	private Integer 	serviceStatusSelected;		
	
	/** The collection base selected. */
	private Integer 	collectionBaseSelected;
	
	/** The service client selected. */
	private Integer 	serviceClientSelected;
	
	/** The calculation period selected. */
	private Integer 	calculationPeriodSelected;
		
	/** The collection period selected. */
	private Integer 	collectionPeriodSelected;
	
	/** The collection type selected. */
	private Integer 	collectionTypeSelected;
	
	/** The applied tax selected. */
	private Integer 	appliedTaxSelected;
	
	/** The ind source selected. */
	private Integer 	indSourceSelected;
	
	/** The in integrated bill. */
	private Integer 	inIntegratedBill;
	
	/** The accounting code selected. */
	private String 		accountingCodeSelected;
	
	/** The service code. */
	private String 		serviceCode;
	
	/** The billing service name. */
	private String 		billingServiceName;
	
	/** The observation. */
	private String 		observation;
	
	/** The mensaje. */
	private String		mensaje;
	
	/** The ind no integrated. */
	private Integer 	indNoIntegrated=PropertiesConstants.NOT_IND_INTEGRATED_BILL;
	
	/** The ind integrated. */
	private Integer 	indIntegrated=PropertiesConstants.YES_IND_INTEGRATED_BILL;
	
	/** The reference rate. */
	private String 		referenceRate;
	
	private Integer portFolio;
	
	/** The collection institution search selected. */
	private Integer 	collectionInstitutionSearchSelected;
	
	/** The collection institution search selected. */
	private Integer 	collectionBaseSearchSelected;
	
	/** The service type search selected. */
	private Integer 	serviceTypeSearchSelected;
	
	/** The service status search selected. */
	private Integer 	serviceStatusSearchSelected;
	
	/** The currency billing. */
	private Integer 	currencyBilling;
	
	/** The currency calculation. */
	private Integer 	currencyCalculation;
	
	/** The source information. */
	private Integer 	sourceInformation;


	/**  attributes for enable o disable fields on page. */
	private Boolean 	collectionInstitutionDisable=Boolean.FALSE;
	
	/** The collection base disable. */
	private Boolean 	collectionBaseDisable=Boolean.FALSE;
	
	/** The service client disable. */
	private Boolean 	serviceClientDisable=Boolean.FALSE;
	
	/** The collection type disable. */
	private Boolean 	collectionTypeDisable=Boolean.FALSE;
	
	/** The collection period disable. */
	private Boolean 	collectionPeriodDisable=Boolean.FALSE;
	
	/** The service type disable. */
	private Boolean 	serviceTypeDisable=Boolean.FALSE;
	
	/** The service status disable. */
	private Boolean 	serviceStatusDisable=Boolean.FALSE;
	
	/** The calculation period disable. */
	private Boolean 	calculationPeriodDisable=Boolean.FALSE;
	
	/** The accounting code disable. */
	private Boolean 	accountingCodeDisable=Boolean.FALSE;
	
	/** The financial code disable. */
	private Boolean 	financialCodeDisable=Boolean.FALSE;
	
	/** The line business disable. */
	private Boolean 	lineBusinessDisable=Boolean.FALSE;
	
	/** The billing service name disable. */
	private Boolean		billingServiceNameDisable=Boolean.FALSE;
	
	/** The observation disable. */
	private Boolean 	observationDisable=Boolean.FALSE;
	
	/** The validity initial date disable. */
	private Boolean 	validityInitialDateDisable=Boolean.FALSE;	
	
	/** The validity final date disable. */
	private Boolean 	validityFinalDateDisable=Boolean.FALSE;
	
	/** The applied tax disable. */
	private Boolean 	appliedTaxDisable=Boolean.FALSE;
	
	/** The source base collection disable. */
	private Boolean 	sourceBaseCollectionDisable=Boolean.FALSE;
	
	/** The currency billing disable. */
	private Boolean 	currencyBillingDisable=Boolean.FALSE;
	
	/** The currency calculation disable. */
	private Boolean 	currencyCalculationDisable=Boolean.FALSE;
	
	/** The source information disable. */
	private Boolean 	sourceInformationDisable=Boolean.FALSE;
	
	/** The billing source helper disable. */
	private Boolean 	billingSourceHelperDisable=Boolean.FALSE;
	
	/** The reference rate disable. */
	private Boolean		referenceRateDisable=Boolean.FALSE;
	
	/** Valores para los campos en modificar. */ 
	private Boolean 	validityModifyInitialDateDisable=Boolean.TRUE;

	/**  The listEconomicActivity disable. */
	private Boolean 	listEconomicActivityDisable=Boolean.FALSE;
	
	/** The disable lock btn. */
	private Boolean 	disableLockBtn;
	
	/** The disable modify btn. */
	private Boolean 	disableModifyBtn;
	
	/** The disable unlock btn. */
	private Boolean 	disableUnlockBtn;
	
	/** The disable cancel btn. */
	private Boolean 	disableCancelBtn; 

	/** The billing service selection. */
	private BillingService billingServiceSelection;
	
	/** The billing service data model. */
	private GenericDataModel<BillingService> billingServiceDataModel;

	/** The model billing service. */
	private BillingService modelBillingService;

	/** The initial date. */
	private Date initialDate; 
	
	/** The final date. */
	private Date finalDate;
	
	/** Fecha inicial de vigencia. */
	private Date validityInitialDate;


	/** Fecha final de vigencia. */
	private Date validityFinalDate;
	
	/** The initial date case. */
	private Date initialDateCase;

	/** The registry date. */
	private Date registryDate = CommonsUtilities.currentDate();


	/** The list collection institution. */
	private List<ParameterTable> listCollectionInstitution;
	
	/** The list service client. */
	private List<ParameterTable> listServiceClient;
	
	/** The list service type. */
	private List<ParameterTable> listServiceType;
	
	/** The list service status. */
	private List<ParameterTable> listServiceStatus;
	
	/** The list calculation period. */
	private List<ParameterTable> listCalculationPeriod;
	
	/** The list collection period. */
	private List<ParameterTable> listCollectionPeriod;
	
	/** The list collection base. */
	private List<ParameterTable> listCollectionBase;
	
	/** The list collection type. */
	private List<ParameterTable> listCollectionType;
	
	/** The list currency rate. */
	private List<ParameterTable> listCurrencyRate;
	
	/** The list currency billing. */
	private List<ParameterTable> listCurrencyBilling;
	
	/** The list currency calculation. */
	private List<ParameterTable> listCurrencyCalculation;
	
	/** The list source information. */
	private List<ParameterTable> listSourceInformation;
	
	/** The list collection period dynamic. */
	private List<ParameterTable> listCollectionPeriodDynamic;
	
	/** The list collection institution dynamic. */
	private List<ParameterTable> listCollectionInstitutionDynamic;
	
	/** The list calculation period dynamic. */
	private List<ParameterTable> listCalculationPeriodDynamic;
	
	/** The list collection base dynamic. */
	private List<ParameterTable> listCollectionBaseDynamic;
	
	/** The list service client dynamic. */
	private List<ParameterTable> listServiceClientDynamic;
	
	/** The list rate type. */
	private List<ParameterTable> listRateType;
	
	/** The list module movement. */
	private List<ParameterTable> listModuleMovement;
	
	/** The billing service. */
	private BillingService billingService;
	
	/** File Billing Service for Base Collection Externa *. */
	private BillingServiceSource	billingServiceSource;
	
	/**   INICIO DE VARIABLES DE TARIFAS. */
	/**
	 * El BillingService Temporal para las tarifas
	 */
	private BillingServiceTo billingServiceTo;
	
	/** The service rate to. */
	private ServiceRateTo serviceRateTo;
	
	/** The service rate consult to. */
	private ServiceRateTo serviceRateConsultTo;
	
	/** The service rate modify to. */
	private ServiceRateTo serviceRateModifyTo;
	
	private ServiceRateTo selectedServiceRateToModify;
	
	/** The add rate disable. */
	private Boolean addRateDisable;
	
	/** The list rate status. */
	private List<ParameterTable> listRateStatus;
	
	/** The ind staggered percentage mix. */
	private Boolean indStaggeredPercentageMix=Boolean.FALSE;
	
	/** The ind staggered fixed mix. */
	private Boolean indStaggeredFixedMix=Boolean.FALSE;
	
	/** The index tab. */
	private Integer indexTab;
	
	/** The disabled type service. */
	private Boolean disabledTypeService;
	
	/**  parameters used on helper movement types. */
	private Boolean renderedDialog=Boolean.FALSE;
	
	/** The list movement empty. */
	private Boolean listMovementEmpty;
	
	/** FIN DE VARIABLES DE TARIFAS*. */
	
    
	private GenericDataModel<MovementType> 		dataModelMovementTypeList;
	
	/** The data model movement type list consult. */
	private GenericDataModel<MovementType> 		dataModelMovementTypeListConsult;
	
	/** The data model rate movement to list. */
	private GenericDataModel<RateMovementTo> 	dataModelRateMovementToList;
	
	/** The data model rate movement to list consult. */
	private GenericDataModel<RateMovementTo> 	dataModelRateMovementToListConsult;
	
	/** The rate service scale to data model. */
	private RateServiceScaleToDataModel 		rateServiceScaleToDataModel;
	
	/** The rate service scale to data model consult. */
	private RateServiceScaleToDataModel 		rateServiceScaleToDataModelConsult;
	

	/** The rate service to data model. */
	private RateServiceToDataModel 				rateServiceToDataModel;
	
	private List<ParameterTable>  listEconomicActivity;
	private List<Integer> listEconomicActivitySelected;
	private List<BillingEconomicActivity> listBillingEconomicActivityBD;
	
	/**
	 * Gets the list collection base dynamic.
	 *
	 * @return the list collection base dynamic
	 */
	public List<ParameterTable> getListCollectionBaseDynamic() {
		return listCollectionBaseDynamic;
	}


	/**
	 * Sets the list collection base dynamic.
	 *
	 * @param listCollectionBaseDynamic the new list collection base dynamic
	 */
	public void setListCollectionBaseDynamic(
			List<ParameterTable> listCollectionBaseDynamic) {
		this.listCollectionBaseDynamic = listCollectionBaseDynamic;
	}


	/**
	 * Gets the list collection period dynamic.
	 *
	 * @return the list collection period dynamic
	 */
	public List<ParameterTable> getListCollectionPeriodDynamic() {
		return listCollectionPeriodDynamic;
	}


	/**
	 * Gets the ind source selected.
	 *
	 * @return the indSourceSelected
	 */
	public Integer getIndSourceSelected() {
		return indSourceSelected;
	}


	/**
	 * Sets the ind source selected.
	 *
	 * @param indSourceSelected the indSourceSelected to set
	 */
	public void setIndSourceSelected(Integer indSourceSelected) {
		this.indSourceSelected = indSourceSelected;
	}



	/**
	 * Gets the accounting code selected.
	 *
	 * @return the accountingCodeSelected
	 */
	public String getAccountingCodeSelected() {
		return accountingCodeSelected;
	}


	/**
	 * Sets the accounting code selected.
	 *
	 * @param accountingCodeSelected the accountingCodeSelected to set
	 */
	public void setAccountingCodeSelected(String accountingCodeSelected) {
		this.accountingCodeSelected = accountingCodeSelected;
	}


	/**
	 * Gets the in integrated bill.
	 *
	 * @return the in integrated bill
	 */
	public Integer getInIntegratedBill() {
		return inIntegratedBill;
	}


	

	/**
	 * Gets the currency billing.
	 *
	 * @return the currencyBilling
	 */
	public Integer getCurrencyBilling() {
		return currencyBilling;
	}


	/**
	 * Sets the currency billing.
	 *
	 * @param currencyBilling the currencyBilling to set
	 */
	public void setCurrencyBilling(Integer currencyBilling) {
		this.currencyBilling = currencyBilling;
	}


	/**
	 * Gets the currency calculation.
	 *
	 * @return the currencyCalculation
	 */
	public Integer getCurrencyCalculation() {
		return currencyCalculation;
	}


	/**
	 * Sets the currency calculation.
	 *
	 * @param currencyCalculation the currencyCalculation to set
	 */
	public void setCurrencyCalculation(Integer currencyCalculation) {
		this.currencyCalculation = currencyCalculation;
	}


	/**
	 * Gets the source information.
	 *
	 * @return the sourceInformation
	 */
	public Integer getSourceInformation() {
		return sourceInformation;
	}


	/**
	 * Sets the source information.
	 *
	 * @param sourceInformation the sourceInformation to set
	 */
	public void setSourceInformation(Integer sourceInformation) {
		this.sourceInformation = sourceInformation;
	}


	/**
	 * Gets the currency calculation disable.
	 *
	 * @return the currencyCalculationDisable
	 */
	public Boolean getCurrencyCalculationDisable() {
		return currencyCalculationDisable;
	}


	/**
	 * Sets the currency calculation disable.
	 *
	 * @param currencyCalculationDisable the currencyCalculationDisable to set
	 */
	public void setCurrencyCalculationDisable(Boolean currencyCalculationDisable) {
		this.currencyCalculationDisable = currencyCalculationDisable;
	}


	/**
	 * Gets the source information disable.
	 *
	 * @return the sourceInformationDisable
	 */
	public Boolean getSourceInformationDisable() {
		return sourceInformationDisable;
	}


	/**
	 * Sets the source information disable.
	 *
	 * @param sourceInformationDisable the sourceInformationDisable to set
	 */
	public void setSourceInformationDisable(Boolean sourceInformationDisable) {
		this.sourceInformationDisable = sourceInformationDisable;
	}


	/**
	 * Gets the list currency calculation.
	 *
	 * @return the listCurrencyCalculation
	 */
	public List<ParameterTable> getListCurrencyCalculation() {
		return listCurrencyCalculation;
	}


	/**
	 * Sets the list currency calculation.
	 *
	 * @param listCurrencyCalculation the listCurrencyCalculation to set
	 */
	public void setListCurrencyCalculation(
			List<ParameterTable> listCurrencyCalculation) {
		this.listCurrencyCalculation = listCurrencyCalculation;
	}


	/**
	 * Gets the list source information.
	 *
	 * @return the listSourceInformation
	 */
	public List<ParameterTable> getListSourceInformation() {
		return listSourceInformation;
	}


	/**
	 * Sets the list source information.
	 *
	 * @param listSourceInformation the listSourceInformation to set
	 */
	public void setListSourceInformation(List<ParameterTable> listSourceInformation) {
		this.listSourceInformation = listSourceInformation;
	}


	/**
	 * Sets the in integrated bill.
	 *
	 * @param inIntegratedBill the new in integrated bill
	 */
	public void setInIntegratedBill(Integer inIntegratedBill) {
		this.inIntegratedBill = inIntegratedBill;
	}


	/**
	 * Sets the list collection period dynamic.
	 *
	 * @param listCollectionPeriodDynamic the new list collection period dynamic
	 */
	public void setListCollectionPeriodDynamic(
			List<ParameterTable> listCollectionPeriodDynamic) {
		this.listCollectionPeriodDynamic = listCollectionPeriodDynamic;
	}

	/**
	 * Gets the financial code disable.
	 *
	 * @return the financial code disable
	 */
	public Boolean getFinancialCodeDisable() {
		return financialCodeDisable;
	}

	/**
	 * Sets the financial code disable.
	 *
	 * @param financialCodeDisable the new financial code disable
	 */
	public void setFinancialCodeDisable(Boolean financialCodeDisable) {
		this.financialCodeDisable = financialCodeDisable;
	}

	/**
	 * Gets the collection institution disable.
	 *
	 * @return the collection institution disable
	 */
	public Boolean getCollectionInstitutionDisable() {
		return collectionInstitutionDisable;
	}

	/**
	 * Sets the collection institution disable.
	 *
	 * @param collectionInstitutionDisable the new collection institution disable
	 */
	public void setCollectionInstitutionDisable(Boolean collectionInstitutionDisable) {
		this.collectionInstitutionDisable = collectionInstitutionDisable;
	}

	/**
	 * Gets the collection base disable.
	 *
	 * @return the collection base disable
	 */
	public Boolean getCollectionBaseDisable() {
		return collectionBaseDisable;
	}

	/**
	 * Sets the collection base disable.
	 *
	 * @param collectionBaseDisable the new collection base disable
	 */
	public void setCollectionBaseDisable(Boolean collectionBaseDisable) {
		this.collectionBaseDisable = collectionBaseDisable;
	}

	/**
	 * Gets the collection type disable.
	 *
	 * @return the collection type disable
	 */
	public Boolean getCollectionTypeDisable() {
		return collectionTypeDisable;
	}

	/**
	 * Sets the collection type disable.
	 *
	 * @param collectionTypeDisable the new collection type disable
	 */
	public void setCollectionTypeDisable(Boolean collectionTypeDisable) {
		this.collectionTypeDisable = collectionTypeDisable;
	}

	/**
	 * Gets the collection period disable.
	 *
	 * @return the collection period disable
	 */
	public Boolean getCollectionPeriodDisable() {
		return collectionPeriodDisable;
	}

	/**
	 * Sets the collection period disable.
	 *
	 * @param collectionPeriodDisable the new collection period disable
	 */
	public void setCollectionPeriodDisable(Boolean collectionPeriodDisable) {
		this.collectionPeriodDisable = collectionPeriodDisable;
	}

	/**
	 * Gets the service type disable.
	 *
	 * @return the service type disable
	 */
	public Boolean getServiceTypeDisable() {
		return serviceTypeDisable;
	}



	/**
	 * Gets the service client disable.
	 *
	 * @return the service client disable
	 */
	public Boolean getServiceClientDisable() {
		return serviceClientDisable;
	}

	/**
	 * Sets the service client disable.
	 *
	 * @param serviceClientDisable the new service client disable
	 */
	public void setServiceClientDisable(Boolean serviceClientDisable) {
		this.serviceClientDisable = serviceClientDisable;
	}

	/**
	 * Sets the service type disable.
	 *
	 * @param serviceTypeDisable the new service type disable
	 */
	public void setServiceTypeDisable(Boolean serviceTypeDisable) {
		this.serviceTypeDisable = serviceTypeDisable;
	}

	/**
	 * Gets the service status disable.
	 *
	 * @return the service status disable
	 */
	public Boolean getServiceStatusDisable() {
		return serviceStatusDisable;
	}

	/**
	 * Sets the service status disable.
	 *
	 * @param serviceStatusDisable the new service status disable
	 */
	public void setServiceStatusDisable(Boolean serviceStatusDisable) {
		this.serviceStatusDisable = serviceStatusDisable;
	}

	/**
	 * Gets the calculation period disable.
	 *
	 * @return the calculation period disable
	 */
	public Boolean getCalculationPeriodDisable() {
		return calculationPeriodDisable;
	}

	/**
	 * Sets the calculation period disable.
	 *
	 * @param calculationPeriodDisable the new calculation period disable
	 */
	public void setCalculationPeriodDisable(Boolean calculationPeriodDisable) {
		this.calculationPeriodDisable = calculationPeriodDisable;
	}

	/**
	 * Gets the accounting code disable.
	 *
	 * @return the accountingCodeDisable
	 */
	public Boolean getAccountingCodeDisable() {
		return accountingCodeDisable;
	}


	/**
	 * Sets the accounting code disable.
	 *
	 * @param accountingCodeDisable the accountingCodeDisable to set
	 */
	public void setAccountingCodeDisable(Boolean accountingCodeDisable) {
		this.accountingCodeDisable = accountingCodeDisable;
	}


	/**
	 * Gets the line business disable.
	 *
	 * @return the line business disable
	 */
	public Boolean getLineBusinessDisable() {
		return lineBusinessDisable;
	}

	/**
	 * Sets the line business disable.
	 *
	 * @param lineBusinessDisable the new line business disable
	 */
	public void setLineBusinessDisable(Boolean lineBusinessDisable) {
		this.lineBusinessDisable = lineBusinessDisable;
	}

	/**
	 * Gets the billing service name disable.
	 *
	 * @return the billing service name disable
	 */
	public Boolean getBillingServiceNameDisable() {
		return billingServiceNameDisable;
	}

	/**
	 * Sets the billing service name disable.
	 *
	 * @param billingServiceNameDisable the new billing service name disable
	 */
	public void setBillingServiceNameDisable(Boolean billingServiceNameDisable) {
		this.billingServiceNameDisable = billingServiceNameDisable;
	}

	/**
	 * Gets the reference rate disable.
	 *
	 * @return the referenceRateDisable
	 */
	public Boolean getReferenceRateDisable() {
		return referenceRateDisable;
	}


	/**
	 * Sets the reference rate disable.
	 *
	 * @param referenceRateDisable the referenceRateDisable to set
	 */
	public void setReferenceRateDisable(Boolean referenceRateDisable) {
		this.referenceRateDisable = referenceRateDisable;
	}


	/**
	 * Gets the observation disable.
	 *
	 * @return the observation disable
	 */
	public Boolean getObservationDisable() {
		return observationDisable;
	}

	/**
	 * Sets the observation disable.
	 *
	 * @param observationDisable the new observation disable
	 */
	public void setObservationDisable(Boolean observationDisable) {
		this.observationDisable = observationDisable;
	}



	/**
	 * Gets the validity initial date disable.
	 *
	 * @return the validity initial date disable
	 */
	public Boolean getValidityInitialDateDisable() {
		return validityInitialDateDisable;
	}

	/**
	 * Sets the validity initial date disable.
	 *
	 * @param validityInitialDateDisable the new validity initial date disable
	 */
	public void setValidityInitialDateDisable(Boolean validityInitialDateDisable) {
		this.validityInitialDateDisable = validityInitialDateDisable;
	}

	/**
	 * Gets the validity final date disable.
	 *
	 * @return the validity final date disable
	 */
	public Boolean getValidityFinalDateDisable() {
		return validityFinalDateDisable;
	}

	/**
	 * Sets the validity final date disable.
	 *
	 * @param validityFinalDateDisable the new validity final date disable
	 */
	public void setValidityFinalDateDisable(Boolean validityFinalDateDisable) {
		this.validityFinalDateDisable = validityFinalDateDisable;
	}

	public Boolean getListEconomicActivityDisable() {
		return listEconomicActivityDisable;
	}

	public void setListEconomicActivityDisable(Boolean listEconomicActivityDisable) {
		this.listEconomicActivityDisable = listEconomicActivityDisable;
	}

	/**
	 * Gets the disable lock btn.
	 *
	 * @return the disable lock btn
	 */
	public Boolean getDisableLockBtn() {
		return disableLockBtn;
	}

	/**
	 * Sets the disable lock btn.
	 *
	 * @param disableLockBtn the new disable lock btn
	 */
	public void setDisableLockBtn(Boolean disableLockBtn) {
		this.disableLockBtn = disableLockBtn;
	}

	/**
	 * Gets the disable modify btn.
	 *
	 * @return the disable modify btn
	 */
	public Boolean getDisableModifyBtn() {
		return disableModifyBtn;
	}

	/**
	 * Sets the disable modify btn.
	 *
	 * @param disableModifyBtn the new disable modify btn
	 */
	public void setDisableModifyBtn(Boolean disableModifyBtn) {
		this.disableModifyBtn = disableModifyBtn;
	}

	/**
	 * Gets the disable unlock btn.
	 *
	 * @return the disable unlock btn
	 */
	public Boolean getDisableUnlockBtn() {
		return disableUnlockBtn;
	}

	/**
	 * Sets the disable unlock btn.
	 *
	 * @param disableUnlockBtn the new disable unlock btn
	 */
	public void setDisableUnlockBtn(Boolean disableUnlockBtn) {
		this.disableUnlockBtn = disableUnlockBtn;
	}

	/**
	 * Gets the disable cancel btn.
	 *
	 * @return the disable cancel btn
	 */
	public Boolean getDisableCancelBtn() {
		return disableCancelBtn;
	}

	/**
	 * Sets the disable cancel btn.
	 *
	 * @param disableCancelBtn the new disable cancel btn
	 */
	public void setDisableCancelBtn(Boolean disableCancelBtn) {
		this.disableCancelBtn = disableCancelBtn;
	}

	/**
	 * Gets the billing service selection.
	 *
	 * @return the billing service selection
	 */
	public BillingService getBillingServiceSelection() {
		return billingServiceSelection;
	}

	/**
	 * Sets the billing service selection.
	 *
	 * @param billingServiceSelection the new billing service selection
	 */
	public void setBillingServiceSelection(BillingService billingServiceSelection) {
		this.billingServiceSelection = billingServiceSelection;
	}

	/**
	 * Gets the validity initial date.
	 *
	 * @return the validity initial date
	 */
	public Date getValidityInitialDate() {
		return validityInitialDate;
	}

	/**
	 * Sets the validity initial date.
	 *
	 * @param validityInitialDate the new validity initial date
	 */
	public void setValidityInitialDate(Date validityInitialDate) {
		this.validityInitialDate = validityInitialDate;
	}

	/**
	 * Gets the validity final date.
	 *
	 * @return the validity final date
	 */
	public Date getValidityFinalDate() {
		return validityFinalDate;
	}

	/**
	 * Sets the validity final date.
	 *
	 * @param validityFinalDate the new validity final date
	 */
	public void setValidityFinalDate(Date validityFinalDate) {
		this.validityFinalDate = validityFinalDate;
	}

	/**
	 * Gets the mensaje.
	 *
	 * @return the mensaje
	 */
	public String getMensaje() {
		return mensaje;
	}

	/**
	 * Sets the mensaje.
	 *
	 * @param mensaje the new mensaje
	 */
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}


	/**
	 * Gets the list calculation period.
	 *
	 * @return the list calculation period
	 */
	public List<ParameterTable> getListCalculationPeriod() {
		return listCalculationPeriod;
	}

	/**
	 * Sets the list calculation period.
	 *
	 * @param listCalculationPeriod the new list calculation period
	 */
	public void setListCalculationPeriod(List<ParameterTable> listCalculationPeriod) {
		this.listCalculationPeriod = listCalculationPeriod;
	}

	/**
	 * Gets the list collection period.
	 *
	 * @return the list collection period
	 */
	public List<ParameterTable> getListCollectionPeriod() {
		return listCollectionPeriod;
	}

	/**
	 * Sets the list collection period.
	 *
	 * @param listCollectionPeriod the new list collection period
	 */
	public void setListCollectionPeriod(List<ParameterTable> listCollectionPeriod) {
		this.listCollectionPeriod = listCollectionPeriod;
	}




	/**
	 * Gets the list collection base.
	 *
	 * @return the list collection base
	 */
	public List<ParameterTable> getListCollectionBase() {
		return listCollectionBase;
	}

	/**
	 * Sets the list collection base.
	 *
	 * @param listCollectionBase the new list collection base
	 */
	public void setListCollectionBase(List<ParameterTable> listCollectionBase) {
		this.listCollectionBase = listCollectionBase;
	}

	/**
	 * Gets the list collection type.
	 *
	 * @return the list collection type
	 */
	public List<ParameterTable> getListCollectionType() {
		return listCollectionType;
	}

	/**
	 * Sets the list collection type.
	 *
	 * @param listCollectionType the new list collection type
	 */
	public void setListCollectionType(List<ParameterTable> listCollectionType) {
		this.listCollectionType = listCollectionType;
	}




	/**
	 * Gets the list collection institution.
	 *
	 * @return the list collection institution
	 */
	public List<ParameterTable> getListCollectionInstitution() {
		return listCollectionInstitution;
	}


	/**
	 * Gets the list service client.
	 *
	 * @return the list service client
	 */
	public List<ParameterTable> getListServiceClient() {
		return listServiceClient;
	}

	/**
	 * Sets the list service client.
	 *
	 * @param listServiceClient the new list service client
	 */
	public void setListServiceClient(List<ParameterTable> listServiceClient) {
		this.listServiceClient = listServiceClient;
	}

	/**
	 * Gets the list service client dynamic.
	 *
	 * @return the list service client dynamic
	 */
	public List<ParameterTable> getListServiceClientDynamic() {
		return listServiceClientDynamic;
	}


	/**
	 * Sets the list service client dynamic.
	 *
	 * @param listServiceClientDynamic the new list service client dynamic
	 */
	public void setListServiceClientDynamic(
			List<ParameterTable> listServiceClientDynamic) {
		this.listServiceClientDynamic = listServiceClientDynamic;
	}


	/**
	 * Gets the list rate type.
	 *
	 * @return the listRateType
	 */
	public List<ParameterTable> getListRateType() {
		return listRateType;
	}


	/**
	 * Sets the list rate type.
	 *
	 * @param listRateType the listRateType to set
	 */
	public void setListRateType(List<ParameterTable> listRateType) {
		this.listRateType = listRateType;
	}


	/**
	 * Sets the list collection institution.
	 *
	 * @param listCollectionInstitution the new list collection institution
	 */
	public void setListCollectionInstitution(
			List<ParameterTable> listCollectionInstitution) {
		this.listCollectionInstitution = listCollectionInstitution;
	}

	/**
	 * Gets the list service type.
	 *
	 * @return the list service type
	 */
	public List<ParameterTable> getListServiceType() {
		return listServiceType;
	}

	/**
	 * Sets the list service type.
	 *
	 * @param listServiceType the new list service type
	 */
	public void setListServiceType(List<ParameterTable> listServiceType) {
		this.listServiceType = listServiceType;
	}

	/**
	 * Gets the list service status.
	 *
	 * @return the list service status
	 */
	public List<ParameterTable> getListServiceStatus() {
		return listServiceStatus;
	}

	/**
	 * Sets the list service status.
	 *
	 * @param listServiceStatus the new list service status
	 */
	public void setListServiceStatus(List<ParameterTable> listServiceStatus) {
		this.listServiceStatus = listServiceStatus;
	}	

	/**
	 * Gets the list currency rate.
	 *
	 * @return the list currency rate
	 */
	public List<ParameterTable> getListCurrencyRate() {
		return listCurrencyRate;
	}

	/**
	 * Sets the list currency rate.
	 *
	 * @param listCurrencyRate the new list currency rate
	 */
	public void setListCurrencyRate(List<ParameterTable> listCurrencyRate) {
		this.listCurrencyRate = listCurrencyRate;
	}	

	/**
	 * Gets the list currency billing.
	 *
	 * @return the list currency billing
	 */
	public List<ParameterTable> getListCurrencyBilling() {
		return listCurrencyBilling;
	}


	/**
	 * Sets the list currency billing.
	 *
	 * @param listCurrencyBilling the new list currency billing
	 */
	public void setListCurrencyBilling(List<ParameterTable> listCurrencyBilling) {
		this.listCurrencyBilling = listCurrencyBilling;
	}


	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * Gets the collection institution selected.
	 *
	 * @return the collection institution selected
	 */
	public Integer getCollectionInstitutionSelected() {
		return collectionInstitutionSelected;
	}

	/**
	 * Sets the collection institution selected.
	 *
	 * @param collectionInstitutionSelected the new collection institution selected
	 */
	public void setCollectionInstitutionSelected(
			Integer collectionInstitutionSelected) {
		this.collectionInstitutionSelected = collectionInstitutionSelected;
	}

	/**
	 * Gets the service type selected.
	 *
	 * @return the service type selected
	 */
	public Integer getServiceTypeSelected() {
		return serviceTypeSelected;
	}

	/**
	 * Sets the service type selected.
	 *
	 * @param serviceTypeSelected the new service type selected
	 */
	public void setServiceTypeSelected(Integer serviceTypeSelected) {
		this.serviceTypeSelected = serviceTypeSelected;
	}

	/**
	 * Gets the service status selected.
	 *
	 * @return the service status selected
	 */
	public Integer getServiceStatusSelected() {
		return serviceStatusSelected;
	}

	/**
	 * Sets the service status selected.
	 *
	 * @param serviceStatusSelected the new service status selected
	 */
	public void setServiceStatusSelected(Integer serviceStatusSelected) {
		this.serviceStatusSelected = serviceStatusSelected;
	}


	/**
	 * Gets the service client selected.
	 *
	 * @return the service client selected
	 */
	public Integer getServiceClientSelected() {
		return serviceClientSelected;
	}

	/**
	 * Sets the service client selected.
	 *
	 * @param serviceClientSelected the new service client selected
	 */
	public void setServiceClientSelected(Integer serviceClientSelected) {
		this.serviceClientSelected = serviceClientSelected;
	}

	/**
	 * Gets the collection base selected.
	 *
	 * @return the collection base selected
	 */
	public Integer getCollectionBaseSelected() {
		return collectionBaseSelected;
	}

	/**
	 * Sets the collection base selected.
	 *
	 * @param collectionBaseSelected the new collection base selected
	 */
	public void setCollectionBaseSelected(Integer collectionBaseSelected) {
		this.collectionBaseSelected = collectionBaseSelected;
	}

	/**
	 * Gets the collection type selected.
	 *
	 * @return the collection type selected
	 */
	public Integer getCollectionTypeSelected() {
		return collectionTypeSelected;
	}

	/**
	 * Sets the collection type selected.
	 *
	 * @param collectionTypeSelected the new collection type selected
	 */
	public void setCollectionTypeSelected(Integer collectionTypeSelected) {
		this.collectionTypeSelected = collectionTypeSelected;
	}

	/**
	 * Gets the calculation period selected.
	 *
	 * @return the calculation period selected
	 */
	public Integer getCalculationPeriodSelected() {
		return calculationPeriodSelected;
	}

	/**
	 * Sets the calculation period selected.
	 *
	 * @param calculationPeriodSelected the new calculation period selected
	 */
	public void setCalculationPeriodSelected(Integer calculationPeriodSelected) {
		this.calculationPeriodSelected = calculationPeriodSelected;
	}

	/**
	 * Gets the collection period selected.
	 *
	 * @return the collection period selected
	 */
	public Integer getCollectionPeriodSelected() {
		return collectionPeriodSelected;
	}

	/**
	 * Sets the collection period selected.
	 *
	 * @param collectionPeriodSelected the new collection period selected
	 */
	public void setCollectionPeriodSelected(Integer collectionPeriodSelected) {
		this.collectionPeriodSelected = collectionPeriodSelected;
	}
	
	/**
	 * Gets the billing service.
	 *
	 * @return the billing service
	 */
	public BillingService getBillingService() {
		return billingService;
	}

	/**
	 * Sets the billing service.
	 *
	 * @param billingService the new billing service
	 */
	public void setBillingService(BillingService billingService) {
		this.billingService = billingService;
	}

	/**
	 * Gets the applied tax selected.
	 *
	 * @return the applied tax selected
	 */
	public Integer getAppliedTaxSelected() {
		return appliedTaxSelected;
	}

	/**
	 * Sets the applied tax selected.
	 *
	 * @param appliedTaxSelected the new applied tax selected
	 */
	public void setAppliedTaxSelected(Integer appliedTaxSelected) {
		this.appliedTaxSelected = appliedTaxSelected;
	}



	/**
	 * Gets the observation.
	 *
	 * @return the observation
	 */
	public String getObservation() {
		return observation;
	}


	/**
	 * Gets the service code.
	 *
	 * @return the service code
	 */
	public String getServiceCode() {
		return serviceCode;
	}

	/**
	 * Sets the service code.
	 *
	 * @param serviceCode the new service code
	 */
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}


	/**
	 * Gets the reference rate.
	 *
	 * @return the referenceRate
	 */
	public String getReferenceRate() {
		return referenceRate;
	}


	/**
	 * Sets the reference rate.
	 *
	 * @param referenceRate the referenceRate to set
	 */
	public void setReferenceRate(String referenceRate) {
		this.referenceRate = referenceRate;
	}


	/**
	 * Sets the observation.
	 *
	 * @param observation the new observation
	 */
	public void setObservation(String observation) {
		this.observation = observation;
	}

	/**
	 * Gets the model billing service.
	 *
	 * @return the model billing service
	 */
	public BillingService getModelBillingService() {
		return modelBillingService;
	}

	/**
	 * Sets the model billing service.
	 *
	 * @param modelBillingService the new model billing service
	 */
	public void setModelBillingService(BillingService modelBillingService) {
		this.modelBillingService = modelBillingService;
	}



	/**
	 * Gets the billing service name.
	 *
	 * @return the billing service name
	 */
	public String getBillingServiceName() {
		return billingServiceName;
	}

	/**
	 * Sets the billing service name.
	 *
	 * @param billingServiceName the new billing service name
	 */
	public void setBillingServiceName(String billingServiceName) {
		this.billingServiceName = billingServiceName;
	}

	/**
	 * Gets the billing service data model.
	 *
	 * @return the billing service data model
	 */
	public GenericDataModel<BillingService> getBillingServiceDataModel() {
		return billingServiceDataModel;
	}

	/**
	 * Sets the billing service data model.
	 *
	 * @param billingServiceDataModel the new billing service data model
	 */
	public void setBillingServiceDataModel(
			GenericDataModel<BillingService> billingServiceDataModel) {
		this.billingServiceDataModel = billingServiceDataModel;
	}

	/**
	 * Instantiates a new billing service mgmt bean.
	 */
	public BillingServiceMgmtBean() {
		log.info("Constructor BillingServiceMgmtBean");
		this.mensaje="";
	}

	/**
	 * Gets the applied tax disable.
	 *
	 * @return the applied tax disable
	 */
	public Boolean getAppliedTaxDisable() {
		return appliedTaxDisable;
	}

	/**
	 * Sets the applied tax disable.
	 *
	 * @param appliedTaxDisable the new applied tax disable
	 */
	public void setAppliedTaxDisable(Boolean appliedTaxDisable) {
		this.appliedTaxDisable = appliedTaxDisable;
	}

	/**
	 * Gets the source base collection disable.
	 *
	 * @return the sourceBaseCollectionDisable
	 */
	public Boolean getSourceBaseCollectionDisable() {
		return sourceBaseCollectionDisable;
	}


	/**
	 * Sets the source base collection disable.
	 *
	 * @param sourceBaseCollectionDisable the sourceBaseCollectionDisable to set
	 */
	public void setSourceBaseCollectionDisable(Boolean sourceBaseCollectionDisable) {
		this.sourceBaseCollectionDisable = sourceBaseCollectionDisable;
	}


	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}


	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}	


	/**
	 * Gets the currency billing disable.
	 *
	 * @return the currency billing disable
	 */
	public Boolean getCurrencyBillingDisable() {
		return currencyBillingDisable;
	}

	/**
	 * Sets the currency billing disable.
	 *
	 * @param currencyBillingDisable the new currency billing disable
	 */
	public void setCurrencyBillingDisable(Boolean currencyBillingDisable) {
		this.currencyBillingDisable = currencyBillingDisable;
	}

	/**
	 * Gets the validity modify initial date disable.
	 *
	 * @return the validity modify initial date disable
	 */
	public Boolean getValidityModifyInitialDateDisable() {
		return validityModifyInitialDateDisable;
	}

	/**
	 * Sets the validity modify initial date disable.
	 *
	 * @param validityModifyInitialDateDisable the new validity modify initial date disable
	 */
	public void setValidityModifyInitialDateDisable(
			Boolean validityModifyInitialDateDisable) {
		this.validityModifyInitialDateDisable = validityModifyInitialDateDisable;
	}	

	/**
	 * Gets the list calculation period dynamic.
	 *
	 * @return the list calculation period dynamic
	 */
	public List<ParameterTable> getListCalculationPeriodDynamic() {
		return listCalculationPeriodDynamic;
	}


	/**
	 * Sets the list calculation period dynamic.
	 *
	 * @param listCalculationPeriodDynamic the new list calculation period dynamic
	 */
	public void setListCalculationPeriodDynamic(
			List<ParameterTable> listCalculationPeriodDynamic) {
		this.listCalculationPeriodDynamic = listCalculationPeriodDynamic;
	}

	
	/**
	 * Gets the initial date case.
	 *
	 * @return the initial date case
	 */
	public Date getinitialDateCase(){  
		if(this.initialDate.equals(this.finalDate)){
			return initialDate;
		}
		else{				
			return finalDate;
		} 
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}


	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}


	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}


	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	/**
	 * Update final date.
	 */
	public void updateFinalDate(){
		finalDate = initialDate;
	}


	/**
	 * Gets the collection institution search selected.
	 *
	 * @return the collection institution search selected
	 */
	public Integer getCollectionInstitutionSearchSelected() {
		return collectionInstitutionSearchSelected;
	}


	/**
	 * Sets the collection institution search selected.
	 *
	 * @param collectionInstitutionSearchSelected the new collection institution search selected
	 */
	public void setCollectionInstitutionSearchSelected(
			Integer collectionInstitutionSearchSelected) {
		this.collectionInstitutionSearchSelected = collectionInstitutionSearchSelected;
	}


	/**
	 * @return the collectionBaseSearchSelected
	 */
	public Integer getCollectionBaseSearchSelected() {
		return collectionBaseSearchSelected;
	}


	/**
	 * @param collectionBaseSearchSelected the collectionBaseSearchSelected to set
	 */
	public void setCollectionBaseSearchSelected(Integer collectionBaseSearchSelected) {
		this.collectionBaseSearchSelected = collectionBaseSearchSelected;
	}


	/**
	 * Gets the service type search selected.
	 *
	 * @return the service type search selected
	 */
	public Integer getServiceTypeSearchSelected() {
		return serviceTypeSearchSelected;
	}


	/**
	 * Sets the service type search selected.
	 *
	 * @param serviceTypeSearchSelected the new service type search selected
	 */
	public void setServiceTypeSearchSelected(Integer serviceTypeSearchSelected) {
		this.serviceTypeSearchSelected = serviceTypeSearchSelected;
	}


	/**
	 * Gets the service status search selected.
	 *
	 * @return the service status search selected
	 */
	public Integer getServiceStatusSearchSelected() {
		return serviceStatusSearchSelected;
	}


	/**
	 * Sets the service status search selected.
	 *
	 * @param serviceStatusSearchSelected the new service status search selected
	 */
	public void setServiceStatusSearchSelected(Integer serviceStatusSearchSelected) {
		this.serviceStatusSearchSelected = serviceStatusSearchSelected;
	}


	/**
	 * Gets the list collection institution dynamic.
	 *
	 * @return the list collection institution dynamic
	 */
	public List<ParameterTable> getListCollectionInstitutionDynamic() {
		return listCollectionInstitutionDynamic;
	}


	/**
	 * Sets the list collection institution dynamic.
	 *
	 * @param listCollectionInstitutionDynamic the new list collection institution dynamic
	 */
	public void setListCollectionInstitutionDynamic(
			List<ParameterTable> listCollectionInstitutionDynamic) {
		this.listCollectionInstitutionDynamic = listCollectionInstitutionDynamic;
	}



	/**
	 * Sets the initial date case.
	 *
	 * @param initialDateCase the new initial date case
	 */
	public void setInitialDateCase(Date initialDateCase) {
		this.initialDateCase = initialDateCase;
	}

	/**
	 * Gets the ind no integrated.
	 *
	 * @return the ind no integrated
	 */
	public Integer getIndNoIntegrated() {
		return indNoIntegrated;
	}


	/**
	 * Sets the ind no integrated.
	 *
	 * @param indNoIntegrated the new ind no integrated
	 */
	public void setIndNoIntegrated(Integer indNoIntegrated) {
		this.indNoIntegrated = indNoIntegrated;
	}


	/**
	 * Gets the ind integrated.
	 *
	 * @return the ind integrated
	 */
	public Integer getIndIntegrated() {
		return indIntegrated;
	}


	/**
	 * Sets the ind integrated.
	 *
	 * @param indIntegrated the new ind integrated
	 */
	public void setIndIntegrated(Integer indIntegrated) {
		this.indIntegrated = indIntegrated;
	}

	public Integer getPortFolio() {
		return portFolio;
	}


	public void setPortFolio(Integer portFolio) {
		this.portFolio = portFolio;
	}


	/**
	 * Gets the billing source helper disable.
	 *
	 * @return the billingSourceHelperDisable
	 */
	public Boolean getBillingSourceHelperDisable() {
		return billingSourceHelperDisable;
	}


	/**
	 * Sets the billing source helper disable.
	 *
	 * @param billingSourceHelperDisable the billingSourceHelperDisable to set
	 */
	public void setBillingSourceHelperDisable(Boolean billingSourceHelperDisable) {
		this.billingSourceHelperDisable = billingSourceHelperDisable;
	}


	/**
	 * Gets the billing service source.
	 *
	 * @return the billingServiceSource
	 */
	public BillingServiceSource getBillingServiceSource() {
		return billingServiceSource;
	}


	/**
	 * Sets the billing service source.
	 *
	 * @param billingServiceSource the billingServiceSource to set
	 */
	public void setBillingServiceSource(BillingServiceSource billingServiceSource) {
		this.billingServiceSource = billingServiceSource;
	}


	

	/**
	 * Gets the index tab.
	 *
	 * @return the indexTab
	 */
	public Integer getIndexTab() {
		return indexTab;
	}

	/**
	 * Sets the index tab.
	 *
	 * @param indexTab the indexTab to set
	 */
	public void setIndexTab(Integer indexTab) {
		this.indexTab = indexTab;
	}

	
	/**
	 * Gets the data model movement type list consult.
	 *
	 * @return the dataModelMovementTypeListConsult
	 */
	public GenericDataModel<MovementType> getDataModelMovementTypeListConsult() {
		return dataModelMovementTypeListConsult;
	}


	/**
	 * Sets the data model movement type list consult.
	 *
	 * @param dataModelMovementTypeListConsult the dataModelMovementTypeListConsult to set
	 */
	public void setDataModelMovementTypeListConsult(
			GenericDataModel<MovementType> dataModelMovementTypeListConsult) {
		this.dataModelMovementTypeListConsult = dataModelMovementTypeListConsult;
	}


	/**
	 * Gets the data model rate movement to list consult.
	 *
	 * @return the dataModelRateMovementToListConsult
	 */
	public GenericDataModel<RateMovementTo> getDataModelRateMovementToListConsult() {
		return dataModelRateMovementToListConsult;
	}


	/**
	 * Sets the data model rate movement to list consult.
	 *
	 * @param dataModelRateMovementToListConsult the dataModelRateMovementToListConsult to set
	 */
	public void setDataModelRateMovementToListConsult(
			GenericDataModel<RateMovementTo> dataModelRateMovementToListConsult) {
		this.dataModelRateMovementToListConsult = dataModelRateMovementToListConsult;
	}


	/**
	 * Gets the rate service scale to data model consult.
	 *
	 * @return the rateServiceScaleToDataModelConsult
	 */
	public RateServiceScaleToDataModel getRateServiceScaleToDataModelConsult() {
		return rateServiceScaleToDataModelConsult;
	}


	/**
	 * Sets the rate service scale to data model consult.
	 *
	 * @param rateServiceScaleToDataModelConsult the rateServiceScaleToDataModelConsult to set
	 */
	public void setRateServiceScaleToDataModelConsult(
			RateServiceScaleToDataModel rateServiceScaleToDataModelConsult) {
		this.rateServiceScaleToDataModelConsult = rateServiceScaleToDataModelConsult;
	}


	/**
	 * Gets the data model movement type list.
	 *
	 * @return the dataModelMovementTypeList
	 */
	public GenericDataModel<MovementType> getDataModelMovementTypeList() {
		return dataModelMovementTypeList;
	}


	/**
	 * Sets the data model movement type list.
	 *
	 * @param dataModelMovementTypeList the dataModelMovementTypeList to set
	 */
	public void setDataModelMovementTypeList(
			GenericDataModel<MovementType> dataModelMovementTypeList) {
		this.dataModelMovementTypeList = dataModelMovementTypeList;
	}


	/**
	 * Gets the data model rate movement to list.
	 *
	 * @return the dataModelRateMovementToList
	 */
	public GenericDataModel<RateMovementTo> getDataModelRateMovementToList() {
		return dataModelRateMovementToList;
	}


	/**
	 * Sets the data model rate movement to list.
	 *
	 * @param dataModelRateMovementToList the dataModelRateMovementToList to set
	 */
	public void setDataModelRateMovementToList(
			GenericDataModel<RateMovementTo> dataModelRateMovementToList) {
		this.dataModelRateMovementToList = dataModelRateMovementToList;
	}


	/**
	 * Gets the rate service scale to data model.
	 *
	 * @return the rateServiceScaleToDataModel
	 */
	public RateServiceScaleToDataModel getRateServiceScaleToDataModel() {
		return rateServiceScaleToDataModel;
	}


	/**
	 * Sets the rate service scale to data model.
	 *
	 * @param rateServiceScaleToDataModel the rateServiceScaleToDataModel to set
	 */
	public void setRateServiceScaleToDataModel(
			RateServiceScaleToDataModel rateServiceScaleToDataModel) {
		this.rateServiceScaleToDataModel = rateServiceScaleToDataModel;
	}


	/**
	 * Gets the disabled type service.
	 *
	 * @return the disabledTypeService
	 */
	public Boolean getDisabledTypeService() {
		return disabledTypeService;
	}


	/**
	 * Sets the disabled type service.
	 *
	 * @param disabledTypeService the disabledTypeService to set
	 */
	public void setDisabledTypeService(Boolean disabledTypeService) {
		this.disabledTypeService = disabledTypeService;
	}



	


	/**
	 * @return the listEconomicActivity
	 */
	public List<ParameterTable> getListEconomicActivity() {
		return listEconomicActivity;
	}


	/**
	 * @param listEconomicActivity the listEconomicActivity to set
	 */
	public void setListEconomicActivity(List<ParameterTable> listEconomicActivity) {
		this.listEconomicActivity = listEconomicActivity;
	}


	/**
	 * @return the listEconomicActivitySelected
	 */
	public List<Integer> getListEconomicActivitySelected() {
		return listEconomicActivitySelected;
	}


	/**
	 * @param listEconomicActivitySelected the listEconomicActivitySelected to set
	 */
	public void setListEconomicActivitySelected(List<Integer> listEconomicActivitySelected) {
		this.listEconomicActivitySelected = listEconomicActivitySelected;
	}

	/**
	 * *.
	 *
	 * @throws ServiceException the service exception
	 */
	
	@PostConstruct
	public void init() throws ServiceException  {
		billingService = new BillingService();
		billingServiceSource=new BillingServiceSource();
		billingService.setIdBillingServicePk(new Long(0));
		/**Valor por defecto**/
		indSourceSelected=GeneralConstants.ZERO_VALUE_INTEGER;
		billingSourceHelperDisable=Boolean.TRUE;
		collectionBaseDisable=Boolean.FALSE;
		billingSourceHelperDisable=Boolean.FALSE;
		collectionBaseDisable=Boolean.TRUE;
		if (!FacesContext.getCurrentInstance().isPostback()){
			log.info("Carga de Filtros para el seach page :  ");
			loadCmbDataSearch();
			
		}
		
		initDataModel();
		
		serviceRateTo=new ServiceRateTo();
		billingServiceTo=new BillingServiceTo();
		serviceRateModifyTo = new ServiceRateTo();

		
	}
	
	/**
	 * Init Generic Data Model.
	 */
	public void initDataModel(){
		
		dataModelMovementTypeList 			= new GenericDataModel<>();
		
		dataModelMovementTypeListConsult 	= new GenericDataModel<>();
		
		dataModelRateMovementToList 		= new GenericDataModel<>();
		
		dataModelRateMovementToListConsult 	= new GenericDataModel<>();
		
		rateServiceScaleToDataModel 		= new RateServiceScaleToDataModel();
		
		rateServiceScaleToDataModelConsult 	= new RateServiceScaleToDataModel();
		
		rateServiceToDataModel				= new RateServiceToDataModel();
	}
	
	/**
	 * Search service billing.
	 */
	@LoggerAuditWeb
	public void searchServiceBilling() {

		log.debug(" busqueda de servicios de facturacion segun filtro");
		BillingServiceTo billingServiceTo = new BillingServiceTo();
		this.billingServiceSelection= new BillingService();

		billingServiceTo.setEntityCollection(this.collectionInstitutionSearchSelected);
		billingServiceTo.setServiceType(this.serviceTypeSearchSelected);
		billingServiceTo.setStateService(this.serviceStatusSearchSelected);
		billingServiceTo.setBaseCollection(this.collectionBaseSearchSelected);
		billingServiceTo.setServiceCode(this.serviceCode);
		List<BillingService> listBillingService;
		try {
			listBillingService = billingServiceMgmtServiceFacade.getListBillingServiceWithSourceByParameters(billingServiceTo);
			billingServiceDataModel = new GenericDataModel<BillingService>(listBillingService);
		} catch (ServiceException e) {
			log.debug(e.getMessage());
			executeAction();
			showMessageOnDialog(PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_ROLL_BACK_BILLING_SERVICE_SEARCH,""));
			JSFUtilities.executeJavascriptFunction("PF('cnfWServiceDialogRollBack').show();");
			this.cleanOnSearch();
			e.printStackTrace();
		}
		 
		updateStatusBotton(this.serviceStatusSearchSelected);

		if(billingServiceDataModel.getRowCount()>0){

			setTrueValidButtons();
		}


	}


	/**
	 * Clean on search.
	 */
	public void cleanOnSearch(){

		this.collectionInstitutionSearchSelected=null;
		this.serviceTypeSearchSelected=null;
		this.collectionBaseSearchSelected=null;
		this.serviceStatusSearchSelected=null;
		this.billingServiceDataModel=null;
		this.serviceCode=null;

	}

	/**
	 * Update status botton.
	 *
	 * @param statusBillingService the status billing service
	 */
	public void updateStatusBotton(Integer statusBillingService){
		if(Validations.validateIsNotNull(statusBillingService)){
			if(statusBillingService.equals(-1)){
				this.disableLockBtn=Boolean.TRUE;
				this.disableModifyBtn=Boolean.TRUE;
				this.disableUnlockBtn=Boolean.TRUE;
				this.disableCancelBtn=Boolean.TRUE;

			}else if(statusBillingService.equals(PropertiesConstants.SERVICE_STATUS_REGISTER_PK)){
				this.disableLockBtn=Boolean.FALSE;
				this.disableModifyBtn=Boolean.FALSE;
				this.disableUnlockBtn=Boolean.TRUE;
				this.disableCancelBtn=Boolean.FALSE;
			}else if(statusBillingService.equals(PropertiesConstants.SERVICE_STATUS_EXPIRED_PK)){
				this.disableLockBtn=Boolean.TRUE;
				this.disableModifyBtn=Boolean.TRUE;
				this.disableUnlockBtn=Boolean.TRUE;
				this.disableCancelBtn=Boolean.TRUE;
			}
			else if(statusBillingService.equals(PropertiesConstants.SERVICE_STATUS_LOCK_PK)){
				this.disableLockBtn=Boolean.TRUE;
				this.disableModifyBtn=Boolean.TRUE;
				this.disableUnlockBtn=Boolean.FALSE;
				this.disableCancelBtn=Boolean.TRUE;
			}
			else if(statusBillingService.equals(PropertiesConstants.SERVICE_STATUS_CANCELLED_PK)){
				this.disableLockBtn=Boolean.TRUE;
				this.disableModifyBtn=Boolean.TRUE;
				this.disableUnlockBtn=Boolean.TRUE;
				this.disableCancelBtn=Boolean.TRUE;
			}}
	}


	/**
	 * Check billing service.
	 *
	 * @param e the e
	 */
	public void checkBillingService(SelectEvent e){

		BillingService billingService = (BillingService)e.getObject();
		updateStatusBotton(billingService.getServiceState());

	}	

	/**
	 * used to forward CertficateMgmtView.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadRegistrationPage() throws ServiceException {
		
		/** BEGIN 
		 * 
		 *  PRE CONDITIONS WHEN LOAD PAGE REGISTER  ****/
		
		this.loadCmbData();			
		this.clear();
		this.resetEnableAllFields();	
		closeModalWindow();
		this.setViewOperationType(ViewOperationsType.REGISTER.getCode());				
		Calendar today =  Calendar.getInstance();
		//today.add(Calendar.DATE,1);
		//today.getTime();
		validityInitialDate = today.getTime();
		validityFinalDate = today.getTime();
		initialDate= today.getTime(); 
		finalDate=today.getTime();
		this.collectionBaseDisable = Boolean.FALSE;
		this.serviceStatusDisable=Boolean.TRUE;
		this.appliedTaxSelected=BooleanType.NO.getCode();
		this.billingServiceSelection=null;
		this.collectionBaseSelected=null;
		this.accountingCodeSelected=null;
		
		setTrueValidButtons();
		
		setIndexTab(0);
		
		/** END **/
		/**
		 * LOAD rates
		 */

		this.billingServiceTo=new BillingServiceTo();
		this.serviceRateTo=new ServiceRateTo();
		loadServiceRateByBillingServices();
		this.rateServiceToDataModel=null;
		/**
		 * END LOAD RATES
		 */
	}
	
	/**
	 * Load service rate by billing services.
	 */
	public void loadServiceRateByBillingServices(){
		this.billingServiceTo.setInitialEffectiveDate(this.initialDate);
		this.billingServiceTo.setEndEffectiveDate(this.finalDate);
		this.billingServiceTo.setCurrencyBilling(this.currencyBilling);
		this.billingServiceTo.setCurrencyCalculation(this.currencyCalculation);
		
		this.serviceRateTo.setInitialEffectiveDate(this.initialDate);
		this.serviceRateTo.setEndEffectiveDate(this.finalDate);
		this.serviceRateTo.setCurrencyRate(currencyCalculation);
		this.addRateDisable=Boolean.TRUE;
		
		/**
		 * Ind Other Currency
		 */
		if(Validations.validateIsNull(serviceRateTo.getIndOtherCurrency())){
			serviceRateTo.setIndOtherCurrency(GeneralConstants.ZERO_VALUE_INTEGER);
		}
	}
	 
	/**
	 * Load cmb data search.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCmbDataSearch() throws ServiceException{

		ParameterTableTO parameterTableTO = new ParameterTableTO();

		if (Validations.validateListIsNullOrEmpty(listCollectionInstitution)){
			log.debug("lista de instituciones de cobranza :");
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO.setMasterTableFk(PropertiesConstants.COLLECTION_ENTITY_PK);
			listCollectionInstitution= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}

		if (Validations.validateListIsNullOrEmpty(listServiceStatus)){
			log.debug("lista de estados del servicio:");
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO.setMasterTableFk(PropertiesConstants.SERVICE_STATUS_PK);
			listServiceStatus= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}

		if (Validations.validateListIsNullOrEmpty(listServiceType)){
			log.debug("lista de tipos  de servicio:");
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO.setMasterTableFk(PropertiesConstants.SERVICE_TYPE_PK);
			listServiceType= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}

		if (Validations.validateListIsNullOrEmpty(listCollectionBase)){
			log.debug("lista de entidades de cobro :");
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO.setMasterTableFk(PropertiesConstants.COLLECTION_BASE_PK);
			parameterTableTO.setElementSubclasification1(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO.setOrderbyParameterTableCd(GeneralConstants.ONE_VALUE_INTEGER);
			listCollectionBase= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			
		}
	}

	/**
	 * Load cmb data.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCmbData() throws ServiceException{			

		log.debug(" cargando listas si no estan en sesion :");		
		ParameterTableTO parameterTableTO = new ParameterTableTO();

		if (Validations.validateListIsNullOrEmpty(listCollectionInstitutionDynamic)){
			log.debug("lista de entidades de cobro :");
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO.setMasterTableFk(PropertiesConstants.COLLECTION_ENTITY_PK);
			listCollectionInstitution= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			listCollectionInstitutionDynamic=BillingUtils.cloneList(listCollectionInstitution);
		}

		if (Validations.validateListIsNullOrEmpty(listServiceClient)){
			log.debug("lista de clientes de los servicios de EDV:");
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO.setOrderbyParameterTableCd(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO.setMasterTableFk(PropertiesConstants.SERVICE_CLIENT_PK);
			listServiceClient= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			listServiceClientDynamic=BillingUtils.cloneList(listServiceClient);
		}


		if (Validations.validateListIsNullOrEmpty(listServiceType)){
			log.debug("lista de tipos  de servicio:");
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO.setMasterTableFk(PropertiesConstants.SERVICE_TYPE_PK);
			listServiceType= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			/**By removed status the ServiceType temporal*/
			List<ParameterTable> listServiceTypeTemp= BillingUtils.cloneList(this.listServiceType);
			Collection<ParameterTable> collectioRemove = new ArrayList<ParameterTable>();
			for (ParameterTable parameterTable : listServiceTypeTemp) {
				if (parameterTable.getParameterTablePk().equals(PropertiesConstants.SERVICE_TYPE_EVENTUAL_PK)){
					collectioRemove.add(parameterTable);
				}
			}
			listServiceTypeTemp.removeAll(collectioRemove);
			listServiceType=BillingUtils.cloneList(listServiceTypeTemp);
		}

		if (Validations.validateListIsNullOrEmpty(listCalculationPeriod)){
			log.debug("lista de periodos de cÃ¡lculo:");
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO.setMasterTableFk(PropertiesConstants.PERIOD_CALCULATION_PK);
			listCalculationPeriod= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			listCalculationPeriodDynamic=BillingUtils.cloneList(listCalculationPeriod);
		}

		if (Validations.validateListIsNullOrEmpty(listCollectionPeriod)){
			log.debug("lista de periodos de cobro:");

			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO.setOrderbyParameterTableCd(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO.setMasterTableFk(PropertiesConstants.COLLECTION_PERIOD_PK);
			listCollectionPeriod= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			listCollectionPeriodDynamic=BillingUtils.cloneList(listCollectionPeriod); 
		}

		if (Validations.validateListIsNullOrEmpty(listCollectionBase)){

			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO.setOrderbyParameterDescription(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO.setMasterTableFk(PropertiesConstants.COLLECTION_BASE_PK);
			listCollectionBase= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
			listCollectionBaseDynamic=BillingUtils.cloneList(listCollectionBase);
			
		}

		
		if (Validations.validateListIsNullOrEmpty(listCollectionType)){
			log.debug("lista de tipo de cobro:");
			parameterTableTO.setMasterTableFk(PropertiesConstants.COLLECTION_TYPE_PK);
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			listCollectionType= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}

		if (Validations.validateListIsNullOrEmpty(listServiceStatus)){
			log.debug("lista de estados del servicio:");
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO.setMasterTableFk(PropertiesConstants.SERVICE_STATUS_PK);
			listServiceStatus= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} 
		if (Validations.validateListIsNullOrEmpty(listCurrencyBilling)){
			log.debug("lista de tipo de monedas:");
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO.setMasterTableFk(PropertiesConstants.CURRENCY_TYPE_PK);
			listCurrencyBilling= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}
		if (Validations.validateListIsNullOrEmpty(listCurrencyCalculation)){
			log.debug("lista de tipo de monedas del Calculo:");
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO.setMasterTableFk(PropertiesConstants.CURRENCY_TYPE_PK);
			listCurrencyCalculation= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}
		if (Validations.validateListIsNullOrEmpty(listSourceInformation)){
			log.debug("lista de Fuente De Informacion:");
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO.setMasterTableFk(PropertiesConstants.SOURCE_INFORMATION_PK);
			listSourceInformation= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}
		if (Validations.validateListIsNullOrEmpty(listEconomicActivity)){
			log.debug("lista de Actividad Economica:");
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO.setMasterTableFk(MasterTableType.ECONOMIC_ACTIVITY.getCode());
			listEconomicActivity= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}
		
				
//		if (Validations.validateListIsNullOrEmpty(listAccountingCode)){
//			
//			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
//			parameterTableTO.setMasterTableFk(BillingServiceFilter.CODIGO_CONTABLE_INGRESO.getCode());
//			listAccountingCode= generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
//			
//		}
		
		/**
		 * List to rates
		 */
		if (Validations.validateListIsNullOrEmpty(listRateType)) {
			log.debug("lista de instituciones de cobranza :");

			parameterTableTO.setMasterTableFk(PropertiesConstants.RATE_TYPE);
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			listRateType = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}
		if (Validations.validateListIsNullOrEmpty(listCurrencyRate)) {
			log.debug("lista de instituciones de cobranza :");

			parameterTableTO.setMasterTableFk(PropertiesConstants.CURRENCY_TYPE_PK);
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			listCurrencyRate = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}
		if (Validations.validateListIsNullOrEmpty(listRateStatus)) {
			log.debug("lista de instituciones de cobranza :");

			parameterTableTO.setMasterTableFk(PropertiesConstants.RATE_STATUS);
			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			listRateStatus = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}
		
	}




	/**
	 * *
	 * Before Registration Listener.
	 */
	public void beforeRegistrationListener() {
		
		log.debug("beforeRegistrationListener :");
		
		
		/** VALIDATE LENGTH SERVICE NAME***/
		if(Validations.validateIsNull(this.billingServiceName) || this.billingServiceName.isEmpty()) {
			executeAction();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_NAME_EMPTY,""));
			JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
		} else if(Validations.validateIsNull(this.referenceRate) || this.referenceRate.isEmpty()) {
			executeAction();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_REFERENCE_RATE_EMPTY,""));
			JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
		} else if(Validations.validateIsNull(this.accountingCodeSelected) || this.accountingCodeSelected.isEmpty()) {
			executeAction();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_ACCOUNTING_CODE_EMPTY,""));
			JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
		} else if(this.billingServiceName.length()>200){
			executeAction();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_NAME_LONG,""));
			JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
		}
		else if( !(this.billingServiceName.length()>=200) ){
			
			/***
			 * Validaciones de la tarifa
			 */

			log.debug("Rate beforeServiceRegistrationListener :");	
			List<ServiceRateTo> listServiceRateTo = billingServiceTo.getListServiceRateTo();
			Boolean stateForSave=Boolean.FALSE;
			
			if(Validations.validateListIsNotNullAndNotEmpty(billingServiceTo.getListServiceRateTo())){
				for (ServiceRateTo serviceRateTo : listServiceRateTo) {
					if (serviceRateTo.getRateStatus().intValue()==RateServiceStatus.PENDING.getCode().intValue()){
						stateForSave=Boolean.TRUE;
						break;
					}
				}
				
			}
			if(stateForSave){
				/**Si hay tarifas pendientes para agregar**/
				if(Validations.validateIsNotNullAndNotEmpty(this.billingServiceTo.getServiceCode())){
					/**Confirmacion de agregar solo el tarifas a un servicio que ya existe**/
					showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
							PropertiesUtilities.getMessage(
									PropertiesConstants.CONFIRM_SERVICE_RATE_SAVE,this.billingServiceTo.getServiceCode()));
					JSFUtilities.executeJavascriptFunction("PF('cnfwRateSave').show();");
				}else{
					/**Confirmacion de agregar servicios nuevos mas tarifas nuevas*/
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
							, PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_BILLING_SERVICE_SAVE));
					JSFUtilities.executeJavascriptFunction("PF('cnfwServiceSave').show();");
				}
				
			}
			else{
				executeAction();
				/**Agregar solo el servicio**/
//				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
//						, PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_BILLING_SERVICE_SAVE));
//				JSFUtilities.executeJavascriptFunction("PF('cnfwServiceSave').show();");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_EMPTY_SERVICE_RATE));
				JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");  

			}
			
		}
		
		
	}
	
	
	/**
	 * Fill billing service.
	 */
	public void fillBillingService(){
		log.debug("fillBillingService :");
	}

	/**
	 * Save billing service.
	 */
	@LoggerAuditWeb
	public void saveBillingService() {

		log.debug("saveBillingService :");
		modelBillingService = new BillingService();		
		modelBillingService.setServiceState(BillingServiceStatus.REGISTERED.getCode());		
		modelBillingService.setServiceType(this.serviceTypeSelected);
		modelBillingService.setEntityCollection(this.collectionInstitutionSelected);
		modelBillingService.setBaseCollection(this.collectionBaseSelected);
		modelBillingService.setCollectionType(this.collectionTypeSelected);
		modelBillingService.setCalculationPeriod(this.calculationPeriodSelected);
		modelBillingService.setClientServiceType(this.serviceClientSelected);
		modelBillingService.setInitialEffectiveDate(this.initialDate);
		modelBillingService.setEndEffectiveDate(this.finalDate);
		modelBillingService.setObservation(this.observation);
		modelBillingService.setInIntegratedBill(this.inIntegratedBill);
		modelBillingService.setCurrencyBilling(this.currencyBilling);
		modelBillingService.setCurrencyCalculation(this.currencyCalculation);
		modelBillingService.setSourceInformation(this.sourceInformation);
		modelBillingService.setServiceName(this.billingServiceName);
		modelBillingService.setReferenceRate(this.referenceRate);
		modelBillingService.setPortfolio(this.portFolio);
		modelBillingService.setAccountingAccount(this.accountingCodeSelected);
		modelBillingService.setCollectionPeriod(this.collectionPeriodSelected);

		modelBillingService.setTaxApplied(this.appliedTaxSelected);
		
		modelBillingService.setIndSourceBaseCollection(indSourceSelected);
		if(Validations.validateIsNotNullAndPositive(indSourceSelected)){
			modelBillingService.setBillingServiceSource(billingServiceSource);
		}
		
		List<BillingEconomicActivity> listBillingEconomicActivity = new ArrayList<BillingEconomicActivity>();
		if(Validations.validateListIsNotNullAndNotEmpty(this.listEconomicActivitySelected)) {
			listBillingEconomicActivity=populateBillingEconomic(this.listEconomicActivitySelected,modelBillingService);
		}
		
		try {
			//Save Billing Service
			billingServiceMgmtServiceFacade.saveBillingServiceWithRates(modelBillingService,billingServiceTo);
			
			//saveEconomic Activity Billing
			billingServiceMgmtServiceFacade.saveEconomicActivity(modelBillingService,listBillingEconomicActivity);
			
		} catch (ServiceException ex) {
			if(ex.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING, 
						ex.getMessage());
				
				JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
				return;
			}
		}
	
		if(Validations.validateListIsNotNullAndNotEmpty(modelBillingService.getServiceRates())){
			/**Se registro servicio mas tarifa**/
			String codeRate=BillingUtils.getStringListServiceRate(modelBillingService.getServiceRates());
			
			log.debug(" se registro un servicio de facturacion con codigo " + modelBillingService.getServiceCode()+" y las tarifas "+codeRate);		

			executeAction();		
			showMessageOnDialog(PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_BILLING_SERVICE_SAVE_AND_RATES,
							modelBillingService.getServiceCode(),codeRate));
			JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
		}else{
			/**Se registro solo servicio**/
			log.debug(" se registro un servicio de facturacion con codigo " + modelBillingService.getServiceCode());
			executeAction();		
			showMessageOnDialog(PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_BILLING_SERVICE_SAVE,
							modelBillingService.getServiceCode()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();"); 
		}
		
		
		searchServiceBilling();
		
	}


	private List<BillingEconomicActivity> populateBillingEconomic(List<Integer> listEconomicActivitySelected,
			BillingService billingService) {
		List<BillingEconomicActivity> listActivity=new ArrayList<>();
		
		for (Integer selectItem : listEconomicActivitySelected) {
			BillingEconomicActivity billingEconomicActivity=new BillingEconomicActivity();
			billingEconomicActivity.setEconomicActivity(Integer.valueOf(selectItem));
			billingEconomicActivity.setBillingService(billingService);
			listActivity.add(billingEconomicActivity);
		}
		return listActivity;
	}


	/**
	 * Evento que envia la confirmacion de Grabar o Actualizar.
	 */
	public void beforeUpdateListener() {

		log.debug("beforeUpdateListener :");	
		Object codigo=billingServiceSelection.getServiceCode();
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MODIFY)
				, PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_BILLING_SERVICE_UPDATE,codigo));
		JSFUtilities.executeJavascriptFunction("PF('cnfwServiceUpdate').show();");
		


		log.debug("beforeUpdateListener : Antes de Actualizar tarifas, ver si hay cambios");


//		if (existChanges()){
//			showMessageOnDialog(
//					PropertiesUtilities
//					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MODIFY),
//					PropertiesUtilities.getMessage(
//							PropertiesConstants.CONFIRM_SERVICE_RATE_MODIFY,
//							this.billingServiceSelection.getServiceCode()));
//			JSFUtilities.executeJavascriptFunction("PF('cnfwRateModify').show();");

//		}else{
//			showMessageOnDialog(
//					PropertiesUtilities
//					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
//					PropertiesUtilities.getMessage(
//							PropertiesConstants.WARNING_EMPTY_SERVICE_MODIFY_RATE));
//			JSFUtilities.executeJavascriptFunction("PF('cnfwModifiedFormSaveRate').show();");
//		}

	
	}

	/**
	 * Update billing service.
	 *
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	/**
	 * 
	 * @throws ServiceException
	 */
	public void updateBillingService() throws ServiceException{

		log.debug("updateBillingService :");

		BillingService billingService=billingServiceMgmtServiceFacade.getBillingServiceByPk(billingServiceSelection.getIdBillingServicePk());

		billingService.setServiceName(this.billingServiceName);
		billingService.setBaseCollection(this.collectionBaseSelected);
		billingService.setCurrencyBilling(this.currencyBilling);
		billingService.setCurrencyCalculation(this.currencyCalculation);
		billingService.setSourceInformation(this.sourceInformation);
		billingService.setCollectionType(this.collectionTypeSelected);
		billingService.setClientServiceType(this.serviceClientSelected);
		billingService.setCalculationPeriod(this.calculationPeriodSelected);
		billingService.setServiceState(this.serviceStatusSelected); 		
		billingService.setServiceType(this.serviceTypeSelected);
		billingService.setInitialEffectiveDate(this.initialDate);
		billingService.setEndEffectiveDate(this.finalDate);
		billingService.setObservation(this.observation);  
		billingService.setTaxApplied(this.appliedTaxSelected);		
		billingService.setInIntegratedBill(this.inIntegratedBill);
		billingService.setIndSourceBaseCollection(indSourceSelected);
		billingService.setReferenceRate(this.referenceRate);
		billingService.setPortfolio(this.portFolio);
		billingService.setAccountingAccount(this.accountingCodeSelected);
		billingService.setCollectionPeriod(this.collectionPeriodSelected);
		if(Validations.validateIsNotNullAndPositive(indSourceSelected)){
			billingService.setBillingServiceSource(billingServiceSource);
		}

		billingServiceMgmtServiceFacade.updateBillingService(billingService);		
		log.debug(" se actualizo el  servicio de facturacion ");



		/****
		 * TODO agregar los nuevos cambios para tarifas
		 */
		log.info("modifyServiceRate");
		billingRateMgmtServiceFacade.modifyServiceRate(billingServiceTo);
		log.debug(" se actualizo la tarifa  ");
		
		log.info("modify Billing EconomicActivity");
		List<BillingEconomicActivity> listBillingEconomicActivity=
				populateBillingEconomic(this.listEconomicActivitySelected, billingService);
		billingRateMgmtServiceFacade.modifyEconomicActivity(billingService, 
				listBillingEconomicActivity, this.listBillingEconomicActivityBD);		
		log.info("Se actualizaron los Billing Economic Activity");
		
		
		Object codigo=billingServiceSelection.getServiceCode();
		executeAction();
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				, PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_BILLING_SERVICE_UPDATE,codigo));
		JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");	
		
		/* Re-load Grilla*/
		searchServiceBilling() ; 

	}
	
	/**
	 *  Method that open dialog to confirm or cancel when this operation go to register one service.
	 */	
	public void hideDialogsFromNew(){
		JSFUtilities.executeJavascriptFunction("PF('cnfwServiceSave').hide();");
		JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').hide();");

	}

	/**
	 * Hide dialogs from search.
	 */
	public void hideDialogsFromSearch(){
		JSFUtilities.executeJavascriptFunction("PF('cnfwServiceBlocked').hide();");
		JSFUtilities.executeJavascriptFunction("PF('cnfwServiceUnBlocked').hide();");
		JSFUtilities.executeJavascriptFunction("PF('cnfwServiceCancel').hide();");
		JSFUtilities.executeJavascriptFunction("PF('cnfwServiceDelete').hide();");
	}


	/**
	 * Go to.
	 * 
	 * @param viewToGo
	 *            the view to go
	 * @return the string
	 */
	public String goTo(String viewToGo) {  

		if (Validations.validateIsNotNull(this.billingServiceSelection)) {
			Object codigo=billingServiceSelection.getServiceCode();
			if  (viewToGo.equals("editBillingService")){
				viewToGo="newBillingService";
				if (billingServiceSelection.getServiceState().equals(BillingServiceStatus.CANCELLED.getCode()))
				{
					executeAction();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
							, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_CANCELLED,codigo));
					JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
					return "";
				}
				if (billingServiceSelection.getServiceState().equals(BillingServiceStatus.LOCKED.getCode()))
				{
					executeAction();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
							, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_LOCK,codigo));
					JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
					return "";
				}
				if (billingServiceSelection.getServiceState().equals(BillingServiceStatus.EXPIRED.getCode()))
				{
					executeAction();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
							, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_EXPIRED,codigo));
					JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
					return "";
				}
			}
			else if  (viewToGo.equals("consultBillingService")){			
				viewToGo="newBillingService";
			}

			return viewToGo;
		}

		showMessageOnDialog(PropertiesUtilities
				.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities.getMessage(PropertiesConstants.MODIFY_RATE_NO_SELECTED));
		JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");

		return  "";
	}  

	/**
	 * Before on lock.
	 */
	@LoggerAuditWeb
	public void beforeOnLock(){
		log.debug("beforeOnLock :");
		if (Validations.validateIsNotNull(billingServiceSelection)){

			Object codigo=billingServiceSelection.getServiceCode();

			if (billingServiceSelection.getServiceState().equals(BillingServiceStatus.LOCKED.getCode()))
			{	
				executeAction();				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_LOCK,codigo));
				JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
				return;
			} 
			if (billingServiceSelection.getServiceState().equals(BillingServiceStatus.CANCELLED.getCode()))
			{	
				executeAction();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_CANCELLED,codigo));
				JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
				return;
			} 
			if (billingServiceSelection.getServiceState().equals(BillingServiceStatus.EXPIRED.getCode()))
			{	
				executeAction();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_EXPIRED,codigo));
				JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
				return;
			}
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
					, PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_BILLING_SERVICE_LOCK,codigo));

			JSFUtilities.executeJavascriptFunction("PF('cnfwServiceBlocked').show();");
		}else{
			showMessageOnDialog(PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.MODIFY_RATE_NO_SELECTED));
			JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
		}


	}

	/**
	 * Before on cancel.
	 */
	public void beforeOnCancel(){
		if (Validations.validateIsNotNull(this.billingServiceSelection)) {	
			Object codigo=billingServiceSelection.getServiceCode();
			if (billingServiceSelection.getServiceState().equals(BillingServiceStatus.CANCELLED.getCode()))
			{	
				executeAction();				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_CANCELLED ,codigo));
				JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");

				return;
			}
			if (billingServiceSelection.getServiceState().equals(BillingServiceStatus.EXPIRED.getCode()))
			{	
				executeAction();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_EXPIRED,codigo));
				JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
				return;
			}
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
					, PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_BILLING_SERVICE_CANCELLED,codigo));

			JSFUtilities.executeJavascriptFunction("PF('cnfwServiceCancel').show();");
		}
		else{
			showMessageOnDialog(PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.MODIFY_RATE_NO_SELECTED));
			JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");			
		}

	}

	/**
	 * Before on delete.
	 */
	public void beforeOnDelete(){
		if (Validations.validateIsNotNull(this.billingServiceSelection)) {	
			Object codigo=billingServiceSelection.getServiceCode();
			if (billingServiceSelection.getServiceState().equals(BillingServiceStatus.CANCELLED.getCode()))
			{	
				executeAction();				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_CANCELLED ,codigo));
				JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");

				return;
			}
			if (billingServiceSelection.getServiceState().equals(BillingServiceStatus.EXPIRED.getCode()))
			{	
				executeAction();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_EXPIRED,codigo));
				JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
				return;
			}
			if (billingServiceSelection.getServiceState().equals(BillingServiceStatus.ACTIVED.getCode()))
			{	
				executeAction();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_ACTIVED,codigo));
				JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
				return;
			}
			if (billingServiceSelection.getServiceState().equals(BillingServiceStatus.LOCKED.getCode()))
			{	
				executeAction();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_LOCK,codigo));
				JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
				return;
			}
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
					, PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_BILLING_SERVICE_DELETED,codigo));

            JSFUtilities.executeJavascriptFunction("PF('cnfwServiceDelete').show();");
		}
		else{
			showMessageOnDialog(PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.MODIFY_RATE_NO_SELECTED));
			JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");			
		}

	}


	/**
	 * Lock billing service.
	 *
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void lockBillingService() throws ServiceException{
		log.debug("LockBillingService :");		

		BillingService billingService=billingServiceMgmtServiceFacade.getBillingServiceByPk(billingServiceSelection.getIdBillingServicePk());		
		billingService.setServiceState(BillingServiceStatus.LOCKED.getCode());			
		billingServiceMgmtServiceFacade.lockBillingService(billingService);
//		hideDialogsFromSearch();
		log.debug(" Se actualizo el servicio de facturacion ");
		Object codigo=billingServiceSelection.getServiceCode();
		executeAction();		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				, PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_BILLING_SERVICE_LOCK,codigo));
		JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
		searchServiceBilling();
	}


	/**
	 * Cancel billing service.
	 *
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void cancelBillingService() throws ServiceException{

		BillingService billingService=billingServiceMgmtServiceFacade.getBillingServiceByPk(billingServiceSelection.getIdBillingServicePk());		
		billingService.setServiceState(BillingServiceStatus.CANCELLED.getCode());	
		billingServiceMgmtServiceFacade.cancelBillingService(billingService);	

		hideDialogsFromSearch();
		log.debug(" se actualiza el  servicio de facturacion ");
		Object codigo=billingServiceSelection.getServiceCode();
		executeAction();		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				, PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_BILLING_SERVICE_CANCELLED,codigo));
		JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialogCancel').show();");
		searchServiceBilling();

	}

	/**
	 * Before on unlock.
	 */
	public void beforeOnUnlock(){
		log.debug("beforeOnUnlock :");

		if (Validations.validateIsNotNull(this.billingServiceSelection)) {	
			Object codigo=billingServiceSelection.getServiceCode();

			if (billingServiceSelection.getServiceState().equals(BillingServiceStatus.ACTIVED.getCode()))
			{	
				executeAction();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_ACTIVED,codigo));
				JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
				return;
			} 
			if (billingServiceSelection.getServiceState().equals(BillingServiceStatus.CANCELLED.getCode()))
			{	
				executeAction();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_CANCELLED,codigo));
				JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
				return;
			} 
			if (billingServiceSelection.getServiceState().equals(BillingServiceStatus.REGISTERED.getCode()))
			{ 
				executeAction();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_REGISTERED,codigo));
				JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
				return;			
			}
			if (billingServiceSelection.getServiceState().equals(BillingServiceStatus.EXPIRED.getCode()))
			{	
				executeAction();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_EXPIRED,codigo));
				JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
				return;
			}
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
					, PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_BILLING_SERVICE_UNLOCK,codigo));
			JSFUtilities.executeJavascriptFunction("PF('cnfwServiceUnBlocked').show();");
		}else{
			showMessageOnDialog(PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.MODIFY_RATE_NO_SELECTED));
			JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");			
		}
	}

	/**
	 * Unlock billing service.
	 *
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void unlockBillingService() throws ServiceException{
		log.debug("UnlockBillingService :");			

		BillingService billingService=billingServiceMgmtServiceFacade.getBillingServiceByPk(billingServiceSelection.getIdBillingServicePk());	

		if( billingServiceSelection.getInitialEffectiveDate().before(CommonsUtilities.currentDate()) ){
			billingService.setServiceState(BillingServiceStatus.ACTIVED.getCode());	
			billingServiceMgmtServiceFacade.unLockBillingService(billingService);	 

		}else if( billingServiceSelection.getInitialEffectiveDate().after(CommonsUtilities.currentDate()) ){
			billingService.setServiceState(BillingServiceStatus.REGISTERED.getCode());	
			billingServiceMgmtServiceFacade.unLockBillingService(billingService);	 
		}else{  // Is the same day
			billingService.setServiceState(BillingServiceStatus.ACTIVED.getCode());	
			billingServiceMgmtServiceFacade.unLockBillingService(billingService);
			
		}	

		log.debug(" se actualizo el  servicio de facturacion ");
//		hideDialogsFromSearch();
		Object codigo=billingServiceSelection.getServiceCode();
		executeAction();
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				, PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_BILLING_SERVICE_UNLOCK,codigo));
		JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
		searchServiceBilling(); 	 
	}
	
	/**
	 * Before on modify.
	 *
	 * @param event the event
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void beforeOnModify(ActionEvent event) throws ServiceException{

		this.setViewOperationType(ViewOperationsType.MODIFY.getCode()); 

		if (Validations.validateIsNotNull(billingServiceSelection)){
			this.loadBillingService(billingServiceSelection);
			
			/** Carga del archivo de Base de Cobro si existe**/
			if(Validations.validateIsNotNullAndPositive(billingServiceSelection.getIndSourceBaseCollection())){
				billingServiceSource=sourceBillingServiceFacade.findBillingServiceSourceByBillingService(new BillingServiceTo(billingServiceSelection.getIdBillingServicePk()));
			}else{
				billingServiceSource=new BillingServiceSource();
			}
			
			this.serviceRateTo=new ServiceRateTo();
			/** Carga de las tarifas del servicio***/
			List<ServiceRate> serviceRateList = billingRateMgmtServiceFacade.getServicesRateByAllBillingServiceToScale(billingServiceSelection.getIdBillingServicePk());
			
			this.billingServiceTo = new BillingServiceTo(this.billingServiceSelection);
			this.billingServiceTo.setStateService(billingServiceTo.getStateService());
			
			if (Validations.validateListIsNotNullAndNotEmpty(serviceRateList)) {

				businessLayerCommonFacade.addServiceRateTo(billingServiceTo, serviceRateList);
				List<ServiceRateTo> serviceRateListModify =BillingUtils.cloneServiceRateToList(billingServiceTo.getListServiceRateTo());
				billingServiceTo.setListServiceRateToModify(serviceRateListModify);
//				if (billingServiceTo.getRateServiceToDataModel()!=null){
//					billingServiceTo.getRateServiceToDataModel().setWrappedData(serviceRateListModify);
//				}
				if (this.getRateServiceToDataModel()!=null){
					this.getRateServiceToDataModel().setWrappedData(serviceRateListModify);
				}
			}
			
			//load BillingEconomicActivity 
			loadBillingEconomicActivity(billingServiceSelection);
			
			this.loadCmbData();
			/* disabling fields no edits*/
			this.resetEnableAllFields();
			getEvaluateDisableComponent();

		}

	}
 

	/**
	 * Gets the evaluate disable component.
	 *
	 * @return the evaluate disable component
	 */
	public void getEvaluateDisableComponent( ){

		if(billingServiceSelection.getServiceState().equals(PropertiesConstants.SERVICE_STATUS_REGISTER_PK)){
			//collectionInstitutionDisable=Boolean.TRUE;
			//collectionBaseDisable=Boolean.TRUE; 
			validityInitialDateDisable=Boolean.TRUE;
			//collectionTypeDisable=Boolean.TRUE;
			serviceTypeDisable=Boolean.TRUE;
			//calculationPeriodDisable=Boolean.TRUE;
			billingSourceHelperDisable=Boolean.TRUE;
			sourceBaseCollectionDisable=Boolean.FALSE;
			
		}
		else if(billingServiceSelection.getServiceState().equals(PropertiesConstants.SERVICE_STATUS_ACTIVE_PK)){
			collectionInstitutionDisable=Boolean.TRUE;
			collectionBaseDisable=Boolean.TRUE;
			serviceClientDisable=Boolean.TRUE;
			collectionTypeDisable=Boolean.TRUE;			  
			serviceTypeDisable=Boolean.TRUE;
			serviceStatusDisable=Boolean.TRUE;			 
			financialCodeDisable=Boolean.TRUE;
			lineBusinessDisable=Boolean.TRUE;
			billingServiceNameDisable=Boolean.TRUE;
			referenceRateDisable=Boolean.TRUE;
			observationDisable=Boolean.TRUE;
			validityInitialDateDisable=Boolean.TRUE;
			appliedTaxDisable=Boolean.TRUE;
			currencyBillingDisable=Boolean.TRUE;
			currencyCalculationDisable=Boolean.TRUE;
			sourceInformationDisable=Boolean.TRUE;
			calculationPeriodDisable=Boolean.TRUE;
			billingSourceHelperDisable=Boolean.TRUE;
			sourceBaseCollectionDisable=Boolean.TRUE;
			listEconomicActivityDisable=Boolean.TRUE;
		}
	}
	
	/**
	 * Billing on consult.
	 *
	 * @param event the event
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void billingOnConsult(ActionEvent event) throws ServiceException{

		if (event != null) {
			this.setViewOperationType(ViewOperationsType.CONSULT.getCode());
			
			closeModalWindow();
			BillingService billingServiceSelection = (BillingService) event.getComponent()
					.getAttributes().get("blockInformation");			
			this.loadBillingService(billingServiceSelection);
			this.loadCmbData();					
			/* disabling fields no edits*/
			collectionInstitutionDisable=Boolean.TRUE;			
			collectionTypeDisable=Boolean.TRUE;
			collectionPeriodDisable=Boolean.TRUE;
			serviceTypeDisable=Boolean.TRUE;
			serviceStatusDisable=Boolean.TRUE;
			calculationPeriodDisable=Boolean.TRUE;
			financialCodeDisable=Boolean.TRUE;
			lineBusinessDisable=Boolean.TRUE;
			billingServiceNameDisable=Boolean.TRUE;
			referenceRateDisable=Boolean.TRUE;
			observationDisable=Boolean.TRUE;
			validityFinalDateDisable=Boolean.TRUE;
			accountingCodeDisable=Boolean.TRUE;
			validityInitialDateDisable=Boolean.TRUE;
			serviceClientDisable=Boolean.TRUE;
			appliedTaxDisable=Boolean.TRUE; 
			currencyBillingDisable=Boolean.TRUE;
			currencyCalculationDisable=Boolean.TRUE;
			sourceInformationDisable=Boolean.TRUE;
			collectionBaseDisable=Boolean.TRUE;
			billingSourceHelperDisable=Boolean.TRUE;
			sourceBaseCollectionDisable=Boolean.TRUE;
			listEconomicActivityDisable=Boolean.TRUE;
			this.billingServiceSelection=billingServiceSelection;
			this.billingServiceTo = new BillingServiceTo(this.billingServiceSelection);
			
			this.serviceRateTo=new ServiceRateTo();
			
			List<ServiceRate> serviceRateList = billingRateMgmtServiceFacade.getServicesRateByAllBillingServiceToScale(this.billingServiceSelection.getIdBillingServicePk());
			
			
			
			if (Validations.validateIsNotNullAndNotEmpty(serviceRateList)) {
				businessLayerCommonFacade.addServiceRateTo(billingServiceTo, serviceRateList);
				
				List<ServiceRateTo> serviceRateListModify =BillingUtils.cloneServiceRateToList(billingServiceTo.getListServiceRateTo());
				billingServiceTo.setListServiceRateToModify(serviceRateListModify);
				if (this.getRateServiceToDataModel()!=null){
					this.getRateServiceToDataModel().setWrappedData(serviceRateListModify);
				}
			}
			
			//Billing Activity Economic
			
			loadBillingEconomicActivity(billingServiceSelection);
			
			
		}

	}


	/**
	 * Load Billing Economic Activity to Data base
	 * @param billingService
	 */
	private void loadBillingEconomicActivity(BillingService billingService) {
		listBillingEconomicActivityBD=billingServiceMgmtServiceFacade.getBillingEconomicActivityByBillingServicePk(billingService.getIdBillingServicePk());
		
		
		//Llenar la lista
		listEconomicActivitySelected=BillingUtils.getBillingEconomicActivityListString(listBillingEconomicActivityBD);
		
		
	}


	/**
	 * Back to search page handler.
	 */
	public void backToSearchPageHandler(){
		this.billingServiceDataModel=null;
		this.resetEnableAllFields();
		this.clear();
	}

	/**
	 * Clear.
	 */
	public void clear()
	{ 
		this.modelBillingService=null;
		this.serviceStatusSelected=null;
		this.billingServiceName=null;
		this.serviceTypeSelected=null;
		this.collectionInstitutionSelected=null;
		this.collectionBaseSelected=null;
		this.collectionTypeSelected=null;
		this.accountingCodeSelected=null;
		this.indSourceSelected = GeneralConstants.ZERO_VALUE_INTEGER;
		this.collectionPeriodSelected=null;
		this.calculationPeriodSelected=null;
		this.serviceCode=null;
		this.appliedTaxSelected=BooleanType.NO.getCode();
		this.inIntegratedBill=BooleanType.YES.getCode();
		this.serviceClientSelected=null;
		this.observation=null;
		this.currencyBilling=null;
		this.currencyCalculation=null;
		this.sourceInformation=null;
		this.referenceRate=null;
		this.collectionBaseDisable=Boolean.FALSE;
		this.billingServiceSource=new BillingServiceSource();
		this.listEconomicActivitySelected = null;
		Calendar today =  Calendar.getInstance();
		validityInitialDate = today.getTime();
		validityFinalDate = today.getTime();
		initialDate= today.getTime(); 
		finalDate=today.getTime();
		JSFUtilities.resetComponent("frmBillingServiceRegister");
	}

	/**
	 * Load billing service.
	 *
	 * @param billingServiceSelected the billing service selected
	 */
	protected void loadBillingService(BillingService billingServiceSelected){

		log.debug("cargando objeto para ser modificado o consultado");
		this.serviceCode=billingServiceSelected.getServiceCode();
		this.billingServiceName=billingServiceSelected.getServiceName();
		this.serviceTypeSelected=billingServiceSelected.getServiceType();
		this.serviceStatusSelected=billingServiceSelected.getServiceState();
		this.collectionBaseSelected=billingServiceSelected.getBaseCollection();
		this.collectionInstitutionSelected=billingServiceSelected.getEntityCollection();
		this.collectionTypeSelected=billingServiceSelected.getCollectionType();
		this.calculationPeriodSelected=billingServiceSelected.getCalculationPeriod();
		this.appliedTaxSelected= billingServiceSelected.getTaxApplied().intValue();
		this.observation=billingServiceSelected.getObservation();
		this.serviceClientSelected=billingServiceSelected.getClientServiceType();
		this.accountingCodeSelected=billingServiceSelected.getAccountingAccount();
		this.collectionPeriodSelected = billingServiceSelected.getCollectionPeriod();

		this.initialDate=billingServiceSelected.getInitialEffectiveDate();
		this.finalDate=billingServiceSelected.getEndEffectiveDate();
		this.inIntegratedBill=billingServiceSelected.getInIntegratedBill();			
		this.registryDate=billingServiceSelected.getRegistryDate();		
		this.currencyBilling=billingServiceSelected.getCurrencyBilling();
		this.currencyCalculation=billingServiceSelected.getCurrencyCalculation();
		this.sourceInformation=billingServiceSelected.getSourceInformation();
		this.referenceRate=billingServiceSelected.getReferenceRate();
		this.portFolio=billingServiceSelected.getPortfolio();
		this.indSourceSelected=billingServiceSelected.getIndSourceBaseCollection();
		
		try {
			if(Validations.validateIsNotNullAndPositive(indSourceSelected)){
				this.billingServiceSource=sourceBillingServiceFacade.findBillingServiceSourceByBillingService(new BillingServiceTo(billingServiceSelected));
			}else{
				billingServiceSource=new BillingServiceSource();
			}
		} catch (ServiceException e) {
			log.debug(e.getMessage());
			e.printStackTrace();
		}
		

	}

	/**
	 * Reset enable all fields.
	 */
	protected void resetEnableAllFields(){ 
		collectionInstitutionDisable=Boolean.FALSE;
		collectionBaseDisable=Boolean.FALSE;
		collectionTypeDisable=Boolean.FALSE;
		collectionPeriodDisable=Boolean.FALSE;
		serviceTypeDisable=Boolean.FALSE;
		serviceStatusDisable=Boolean.FALSE;
		calculationPeriodDisable=Boolean.FALSE;
		financialCodeDisable=Boolean.FALSE;
		lineBusinessDisable=Boolean.FALSE;
		billingServiceNameDisable=Boolean.FALSE;
		referenceRateDisable=Boolean.FALSE;
		observationDisable=Boolean.FALSE;
		validityFinalDateDisable=Boolean.FALSE;
		accountingCodeDisable=Boolean.FALSE;
		validityInitialDateDisable=Boolean.FALSE;
		serviceClientDisable=Boolean.FALSE;
		appliedTaxDisable=Boolean.FALSE;
		currencyBillingDisable=Boolean.FALSE;
		currencyCalculationDisable=Boolean.FALSE;
		sourceInformationDisable=Boolean.FALSE;
		billingSourceHelperDisable=Boolean.TRUE;
		listEconomicActivityDisable=Boolean.FALSE;
	} 



	/**
	 * Clear collection manager.
	 */
	public void clearCollectionManager()
	{
		this.modelBillingService=null;
		this.serviceStatusSelected=null;
		this.billingServiceName=null;
		this.serviceTypeSelected=null;
		this.collectionInstitutionSelected=null;
		this.collectionBaseSelected=null;
		this.collectionTypeSelected=null;
		this.indSourceSelected = GeneralConstants.ZERO_VALUE_INTEGER;
		this.collectionPeriodSelected=null;
		this.calculationPeriodSelected=null;
		this.accountingCodeSelected=null;
		this.serviceCode=null;
		this.appliedTaxSelected=null;
		this.referenceRate=null;
		this.finalDate = getCurrentSystemDate();
		this.initialDate = getCurrentSystemDate();
		billingServiceSource=new BillingServiceSource();

	}

	/**
	 * Reset enable all fields collection manager.
	 */
	protected void resetEnableAllFieldsCollectionManager(){

		collectionInstitutionDisable=Boolean.FALSE;
		collectionBaseDisable=Boolean.FALSE;
		collectionTypeDisable=Boolean.FALSE;
		collectionPeriodDisable=Boolean.FALSE;
		serviceTypeDisable=Boolean.FALSE;
		serviceStatusDisable=Boolean.FALSE;
		calculationPeriodDisable=Boolean.FALSE;
		financialCodeDisable=Boolean.FALSE;
		lineBusinessDisable=Boolean.FALSE;
		billingServiceNameDisable=Boolean.FALSE;
		referenceRateDisable=Boolean.FALSE;
		observationDisable=Boolean.FALSE;
		validityFinalDateDisable=Boolean.FALSE;
		accountingCodeDisable=Boolean.FALSE;
		validityInitialDateDisable=Boolean.FALSE;
		billingSourceHelperDisable=Boolean.FALSE;
		listEconomicActivityDisable=Boolean.FALSE;
	}

	/**
	 * Update list collection period.
	 */
	public void updateListCollectionPeriod(){

		Integer calculationPeriod=this.calculationPeriodSelected;
		if(Validations.validateIsNotNullAndPositive(calculationPeriodSelected)){
			
			listCollectionPeriod=BillingUtils.cloneList(listCollectionPeriodDynamic);
			
			if(calculationPeriod.equals(PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK)){
				for (Iterator<ParameterTable> iter=listCollectionPeriod.iterator(); iter.hasNext();) {
					ParameterTable collectionPeriod=(ParameterTable)iter.next();
					if(collectionPeriod.getParameterTablePk().equals(PropertiesConstants.COLLECTION_PERIOD_EVENT_PK)){
						iter.remove();
					}
					else if(collectionPeriod.getParameterTablePk().equals(PropertiesConstants.COLLECTION_PERIOD_DAILY_PK)){
						iter.remove();
					}
				}
			}
			else if(calculationPeriod.equals(PropertiesConstants.PERIOD_CALCULATION_YEARLY_PK)){
				for (Iterator<ParameterTable> iter=listCollectionPeriod.iterator(); iter.hasNext();) {
					ParameterTable collectionPeriod=(ParameterTable)iter.next();
					if(collectionPeriod.getParameterTablePk().equals(PropertiesConstants.COLLECTION_PERIOD_EVENT_PK)){
						iter.remove();
					}
					else if(collectionPeriod.getParameterTablePk().equals(PropertiesConstants.COLLECTION_PERIOD_DAILY_PK)){
						iter.remove();
					}
					else if(collectionPeriod.getParameterTablePk().equals(PropertiesConstants.COLLECTION_PERIOD_MONTHLY_PK)){
						iter.remove();
					}
				}
			}
		}
		this.collectionPeriodSelected = -1;
	}

	/**
	 * Update list collection base by service type.
	 *
	 * @throws ServiceException the service exception
	 */
	public void updateListCollectionBaseByServiceType() throws ServiceException{
		if( Validations.validateIsNotNullAndPositive(serviceTypeSelected) ){
			log.debug("Actualiza la lista de base de cobros por tipo de servicio:");

			Integer serviceType=this.serviceTypeSelected;
			listCollectionBase=BillingUtils.cloneList(listCollectionBaseDynamic);
			if( serviceType.equals(PropertiesConstants.SERVICE_TYPE_AUTOMATIC_PK) ){
				for (Iterator<ParameterTable> iterator = listCollectionBase.iterator(); iterator.hasNext();) {
					//type type = (type) iterator.next();
					ParameterTable collectionBase=(ParameterTable)iterator.next();
					if(collectionBase.getElementSubclasification1()==1){
					}
					else{
						iterator.remove();
					}
				}
				
			}	
			else if( serviceType.equals(PropertiesConstants.SERVICE_TYPE_MANUAL_PK) ){
				
				for (Iterator<ParameterTable> iterator = listCollectionBase.iterator(); iterator.hasNext();) {
					//type type = (type) iterator.next();
					ParameterTable collectionBase=(ParameterTable)iterator.next();
					if(collectionBase.getElementSubclasification1()==3){
					}
					else{
						iterator.remove();
					}
				}
				
			}  
			/** LOAD COLLECTION BASE **/
			else if( serviceType.equals(PropertiesConstants.SERVICE_TYPE_EVENTUAL_PK) ){
				for (Iterator<ParameterTable> iterator = listCollectionBase.iterator(); iterator.hasNext();) {
					//type type = (type) iterator.next();
					ParameterTable collectionBase=(ParameterTable)iterator.next();
					if(collectionBase.getElementSubclasification1()==2){
					}
					else{
						iterator.remove();
					}
				}
				
			}
			else {
				updateListCalculationPeriodForServiceType();
				this.collectionBaseDisable=Boolean.TRUE;
				this.calculationPeriodSelected=-1;
				this.collectionBaseSelected=-1;
				return;
			}


			this.collectionBaseDisable=Boolean.FALSE;

		}
	}

	/**
	 * Update list dependent to base collection.
	 */
	public void updateListDependentToBaseCollection(){
		log.debug("updateListDependentToBaseCollection");
//		updateListCalculationPeriod();
//		/**Actualizar el cliente de servicio*/
//		updateListServiceClient();
		collectionInstitutionSelected=-1;
		calculationPeriodSelected=-1;
		
	}
	 
	/**
	 * Update list service client.
	 */
	public void updateListServiceClient(){
		
		log.debug("updateListServiceClient");
//		/**Collection Base*/
//		Integer baseCollection=this.collectionBaseSelected;
//		listServiceClient=BillingUtils.cloneList(listServiceClientDynamic);
//		/**Service Type*/
//		Integer serviceType=this.serviceTypeSelected;
//		if( (serviceType.equals(PropertiesConstants.SERVICE_TYPE_AUTOMATIC_PK))){
//			if( baseCollection.equals(BaseCollectionType.LIQUIDACION_OPERACIONES_BVRD.getCode()) ||
//				baseCollection.equals(BaseCollectionType.LIQUIDACION_OPERACIONES_HACIENDA.getCode()) || 
//				baseCollection.equals(BaseCollectionType.LIQUIDACION_OPERACIONES_OTC.getCode()) ||
//				baseCollection.equals(BaseCollectionType.LIQUIDACION_OPERACIONES_BCRD.getCode()) ||
//				baseCollection.equals(BaseCollectionType.TRANSACCIONES_MOVIMIENTOS.getCode())  ||
//				baseCollection.equals(BaseCollectionType.TRANSACCIONES_EFECTIVO_PRESTAMO.getCode())  ||
//				baseCollection.equals(BaseCollectionType.TRANSACCIONES_EFECTIVO_RECOMPRA.getCode())  ||
//				baseCollection.equals(BaseCollectionType.DEPOSITOS_RETIROS_VALORES_INTERNACIONALES.getCode()) ||
//				baseCollection.equals(BaseCollectionType.CUSTODIA_VALORES_INTERNACIONALES.getCode()) ||
//				baseCollection.equals(BaseCollectionType.CUOTA_ANUAL_ENTIDADES.getCode())){
//
//				for (Iterator<ParameterTable> iter=listServiceClient.iterator(); iter.hasNext();) {
//					ParameterTable serviceClient=(ParameterTable)iter.next();
//					if(!serviceClient.getParameterTablePk().equals(PropertiesConstants.SERVICE_CLIENT_PARTICIPANT_PK)){
//						iter.remove();
//					}
//				}
//			}
//			else if(baseCollection.equals(BaseCollectionType.REGISTRO_EMISIONES.getCode())||
//				baseCollection.equals(BaseCollectionType.CUOTA_MENSUAL_EMISIONES.getCode()) ){
//				for (Iterator<ParameterTable> iter=listServiceClient.iterator(); iter.hasNext();) {
//					ParameterTable serviceClient=(ParameterTable)iter.next();
//					if(!serviceClient.getParameterTablePk().equals(PropertiesConstants.SERVICE_CLIENT_ISSUER_PK)){
//						iter.remove();
//					}
//					
//				}
//			}
//			else if(baseCollection.equals(BaseCollectionType.REGISTRO_ENTIDADES.getCode()) ){
//				
//				
//				for (Iterator<ParameterTable> iter=listServiceClient.iterator(); iter.hasNext();) {
//					ParameterTable serviceClient=(ParameterTable)iter.next();
//					if(serviceClient.getParameterTablePk().equals(PropertiesConstants.SERVICE_CLIENT_ISSUER_PK)
//							||serviceClient.getParameterTablePk().equals(PropertiesConstants.SERVICE_CLIENT_PARTICIPANT_PK)){
//					}
//					else{
//						iter.remove();
//					}
//					
//				}
//				}
//			else if(baseCollection.equals(BaseCollectionType.CUOTA_ACCESO_POR_ENTIDAD.getCode()) ){
//	
//				for (Iterator<ParameterTable> iter=listServiceClient.iterator(); iter.hasNext();) {
//					ParameterTable serviceClient=(ParameterTable)iter.next();
//					if(serviceClient.getParameterTablePk().equals(PropertiesConstants.SERVICE_CLIENT_ISSUER_PK)
//							||serviceClient.getParameterTablePk().equals(PropertiesConstants.SERVICE_CLIENT_PARTICIPANT_PK)
//							||serviceClient.getParameterTablePk().equals(PropertiesConstants.SERVICE_CLIENT_OTHER_PK)){
//					}
//					else{
//						iter.remove();
//					}
//					
//				}
//				}
//			
//		}
//		else if(serviceType.equals(PropertiesConstants.SERVICE_TYPE_EVENTUAL_PK)){
//			if( baseCollection.equals(BaseCollectionType.CUSTODIA_VALOR_TITULAR.getCode())){
//			for (Iterator<ParameterTable> iter=listServiceClient.iterator(); iter.hasNext();) {
//				ParameterTable serviceClient=(ParameterTable)iter.next();
//				if(!serviceClient.getParameterTablePk().equals(PropertiesConstants.SERVICE_CLIENT_HOLDER_PK)){
//					iter.remove();
//				}
//			}
//			}
//			else if(baseCollection.equals(BaseCollectionType.CERTIFICADO_TENENCIA_DE_VALORES.getCode())){
//				for (Iterator<ParameterTable> iter=listServiceClient.iterator(); iter.hasNext();) {
//					ParameterTable serviceClient=(ParameterTable)iter.next();
//					if(serviceClient.getParameterTablePk().equals(PropertiesConstants.SERVICE_CLIENT_ISSUER_PK)
//							||serviceClient.getParameterTablePk().equals(PropertiesConstants.SERVICE_CLIENT_PARTICIPANT_PK)
//							||serviceClient.getParameterTablePk().equals(PropertiesConstants.SERVICE_CLIENT_HOLDER_PK)){
//						
//					}else{
//						iter.remove();
//					}
//					
//				}
//			}
//			
//			else if(baseCollection.equals(BaseCollectionType.GENERACION_CONSTANCIAS.getCode())){
//				for (Iterator<ParameterTable> iter=listServiceClient.iterator(); iter.hasNext();) {
//					ParameterTable serviceClient=(ParameterTable)iter.next();
//					if(serviceClient.getParameterTablePk().equals(PropertiesConstants.SERVICE_CLIENT_PARTICIPANT_PK)
//							||serviceClient.getParameterTablePk().equals(PropertiesConstants.SERVICE_CLIENT_HOLDER_PK)){
//						
//					}else{
//						iter.remove();
//					}
//					
//				}
//			}
//			else if(baseCollection.equals(BaseCollectionType.COMISION_EMISOR_PAGO_INTERESES.getCode())){
//				for (Iterator<ParameterTable> iter=listServiceClient.iterator(); iter.hasNext();) {
//					ParameterTable serviceClient=(ParameterTable)iter.next();
//					if(!serviceClient.getParameterTablePk().equals(PropertiesConstants.SERVICE_CLIENT_ISSUER_PK)){
//						iter.remove();
//					}
//				}
//			}
//		}
//		else if(serviceType.equals(PropertiesConstants.SERVICE_TYPE_MANUAL_PK)){
//			
//			if(baseCollection.equals(BaseCollectionType.COBRO_MANUAL.getCode())){
//				/***
//				 * No remover nada, abierto a todos los clientes servicios
//				 * */
//			}
//			
//			
//		}
//		this.serviceClientSelected=-1;
	}
	
	/**
	 * Update entity collection by Service Client.
	 */
	public void updateListEntityCollection(){
		/**por base de cobro y cliente de servicio*/
		/**Collection Base*/
		Integer baseCollection=this.collectionBaseSelected;
		log.debug("updateListEntityCollection");
//		
//		Integer serviceClient=this.serviceClientSelected;
//		listCollectionInstitution=BillingUtils.cloneList(listCollectionInstitutionDynamic);
//		/**Service Type*/
//		
//		Integer serviceType=this.serviceTypeSelected;
//		if(Validations.validateIsNotNullAndPositive(serviceType)&&Validations.validateIsNotNullAndPositive(baseCollection))
//		{
//		if( (serviceType.equals(PropertiesConstants.SERVICE_TYPE_AUTOMATIC_PK))){
//			if( baseCollection.equals(BaseCollectionType.LIQUIDACION_OPERACIONES_BVRD.getCode()) ||
//				baseCollection.equals(BaseCollectionType.LIQUIDACION_OPERACIONES_HACIENDA.getCode()) || 
//				baseCollection.equals(BaseCollectionType.LIQUIDACION_OPERACIONES_OTC.getCode()) ||
//				baseCollection.equals(BaseCollectionType.LIQUIDACION_OPERACIONES_BCRD.getCode()) ||
//				baseCollection.equals(BaseCollectionType.TRANSACCIONES_MOVIMIENTOS.getCode())  ||
//				baseCollection.equals(BaseCollectionType.TRANSACCIONES_EFECTIVO_PRESTAMO.getCode())  ||
//				baseCollection.equals(BaseCollectionType.TRANSACCIONES_EFECTIVO_RECOMPRA.getCode())  ||
//				baseCollection.equals(BaseCollectionType.DEPOSITOS_RETIROS_VALORES_INTERNACIONALES.getCode()) ||
//				baseCollection.equals(BaseCollectionType.CUSTODIA_VALORES_INTERNACIONALES.getCode()) ||
//				baseCollection.equals(BaseCollectionType.CUOTA_ANUAL_ENTIDADES.getCode())){
//
//				for (Iterator<ParameterTable> iter=listCollectionInstitution.iterator(); iter.hasNext();) {
//					ParameterTable entityCollection=(ParameterTable)iter.next();
//					if(!entityCollection.getParameterTablePk().equals(EntityCollectionType.PARTICIPANTES.getCode())){
//						iter.remove();
//					}
//				}
//				
//			}
//			else if(baseCollection.equals(BaseCollectionType.REGISTRO_EMISIONES.getCode())||
//				baseCollection.equals(BaseCollectionType.CUOTA_MENSUAL_EMISIONES.getCode()) ){
//				for (Iterator<ParameterTable> iter=listCollectionInstitution.iterator(); iter.hasNext();) {
//					ParameterTable entityCollection=(ParameterTable)iter.next();
//					if(!entityCollection.getParameterTablePk().equals(EntityCollectionType.EMISORES.getCode())){
//						iter.remove();
//					}
//					
//				}
//			}
//			else if(baseCollection.equals(BaseCollectionType.REGISTRO_ENTIDADES.getCode()) ){
//				if(serviceClient.equals(PropertiesConstants.SERVICE_CLIENT_PARTICIPANT_PK)){
//					for (Iterator<ParameterTable> iter=listCollectionInstitution.iterator(); iter.hasNext();) {
//						ParameterTable entityCollection=(ParameterTable)iter.next();
//						if(!entityCollection.getParameterTablePk().equals(EntityCollectionType.PARTICIPANTES.getCode())){
//							iter.remove();
//						}
//						
//					}
//				}
//				else if(serviceClient.equals(PropertiesConstants.SERVICE_CLIENT_ISSUER_PK)){
//					for (Iterator<ParameterTable> iter=listCollectionInstitution.iterator(); iter.hasNext();) {
//						ParameterTable entityCollection=(ParameterTable)iter.next();
//						if(!entityCollection.getParameterTablePk().equals(EntityCollectionType.EMISORES.getCode())){
//							iter.remove();
//						}
//						
//					}
//				}
//				}
//			else if(baseCollection.equals(BaseCollectionType.CUOTA_ACCESO_POR_ENTIDAD.getCode()) ){
//				
//				if(serviceClient.equals(PropertiesConstants.SERVICE_CLIENT_OTHER_PK)){
//					for (Iterator<ParameterTable> iter=listCollectionInstitution.iterator(); iter.hasNext();) {
//						ParameterTable entityCollection=(ParameterTable)iter.next();
//						if(entityCollection.getParameterTablePk().equals(EntityCollectionType.AFP.getCode())
//								||entityCollection.getParameterTablePk().equals(EntityCollectionType.BANCO_CENTRAL.getCode())
//								||entityCollection.getParameterTablePk().equals(EntityCollectionType.HACIENDA.getCode())
//								||entityCollection.getParameterTablePk().equals(EntityCollectionType.BOLSA_VALORES.getCode())
//								||entityCollection.getParameterTablePk().equals(EntityCollectionType.REGULATOR_ENTITY.getCode())){
//							
//						}
//						else{
//							iter.remove();
//						}
//					}
//				}
//				else if(serviceClient.equals(PropertiesConstants.SERVICE_CLIENT_HOLDER_PK)){
//					for (Iterator<ParameterTable> iter=listCollectionInstitution.iterator(); iter.hasNext();) {
//						ParameterTable entityCollection=(ParameterTable)iter.next();
//						if(!entityCollection.getParameterTablePk().equals(EntityCollectionType.RNT.getCode())){
//							iter.remove();
//						}
//					}
//				}
//				else if(serviceClient.equals(PropertiesConstants.SERVICE_CLIENT_ISSUER_PK)){
//					for (Iterator<ParameterTable> iter=listCollectionInstitution.iterator(); iter.hasNext();) {
//						ParameterTable entityCollection=(ParameterTable)iter.next();
//						if(!entityCollection.getParameterTablePk().equals(EntityCollectionType.EMISORES.getCode())){
//							iter.remove();
//						}
//						
//					}
//				}
//				else if(serviceClient.equals(PropertiesConstants.SERVICE_CLIENT_PARTICIPANT_PK)){
//					for (Iterator<ParameterTable> iter=listCollectionInstitution.iterator(); iter.hasNext();) {
//						ParameterTable entityCollection=(ParameterTable)iter.next();
//						if(!entityCollection.getParameterTablePk().equals(EntityCollectionType.PARTICIPANTES.getCode())){
//							iter.remove();
//						}
//						
//					}
//				}
//				}
//			
//		}
//		else if(serviceType.equals(PropertiesConstants.SERVICE_TYPE_EVENTUAL_PK)){
//			if( baseCollection.equals(BaseCollectionType.CUSTODIA_VALOR_TITULAR.getCode())){
//			for (Iterator<ParameterTable> iter=listCollectionInstitution.iterator(); iter.hasNext();) {
//				ParameterTable entityCollection=(ParameterTable)iter.next();
//				if(!entityCollection.getParameterTablePk().equals(EntityCollectionType.RNT.getCode())){
//					iter.remove();
//				}
//			}
//			}
//			else if(baseCollection.equals(BaseCollectionType.CERTIFICADO_TENENCIA_DE_VALORES.getCode())){
//				if(serviceClient.equals(PropertiesConstants.SERVICE_CLIENT_ISSUER_PK)){
//					for (Iterator<ParameterTable> iter=listCollectionInstitution.iterator(); iter.hasNext();) {
//						ParameterTable entityCollection=(ParameterTable)iter.next();
//						if(!entityCollection.getParameterTablePk().equals(EntityCollectionType.EMISORES.getCode())){
//							iter.remove();
//						}
//						
//					}
//					
//				}
//				else if(serviceClient.equals(PropertiesConstants.SERVICE_CLIENT_PARTICIPANT_PK)){
//					for (Iterator<ParameterTable> iter=listCollectionInstitution.iterator(); iter.hasNext();) {
//						ParameterTable entityCollection=(ParameterTable)iter.next();
//						if(!entityCollection.getParameterTablePk().equals(EntityCollectionType.PARTICIPANTES.getCode())){
//							iter.remove();
//						}
//						
//					}
//					
//				}
//	 			else if(serviceClient.equals(PropertiesConstants.SERVICE_CLIENT_HOLDER_PK)){
//	 				for (Iterator<ParameterTable> iter=listCollectionInstitution.iterator(); iter.hasNext();) {
//						ParameterTable entityCollection=(ParameterTable)iter.next();
//						if(!entityCollection.getParameterTablePk().equals(EntityCollectionType.RNT.getCode())){
//							iter.remove();
//						}
//						
//					}
//					
//				}
//			}
//			else if(baseCollection.equals(BaseCollectionType.COMISION_EMISOR_PAGO_INTERESES.getCode())){
//				for (Iterator<ParameterTable> iter=listCollectionInstitution.iterator(); iter.hasNext();) {
//					ParameterTable entityCollection=(ParameterTable)iter.next();
//					if(!entityCollection.getParameterTablePk().equals(EntityCollectionType.EMISORES.getCode())){
//						iter.remove();
//					}
//				}
//			}
//			
//			else if(baseCollection.equals(BaseCollectionType.GENERACION_CONSTANCIAS.getCode())){
//				if(serviceClient.equals(PropertiesConstants.SERVICE_CLIENT_PARTICIPANT_PK)){
//					for (Iterator<ParameterTable> iter=listCollectionInstitution.iterator(); iter.hasNext();) {
//						ParameterTable entityCollection=(ParameterTable)iter.next();
//						if(!entityCollection.getParameterTablePk().equals(EntityCollectionType.PARTICIPANTES.getCode())){
//							iter.remove();
//						}
//						
//					}
//				}
//	 			else if(serviceClient.equals(PropertiesConstants.SERVICE_CLIENT_HOLDER_PK)){
//	 				for (Iterator<ParameterTable> iter=listCollectionInstitution.iterator(); iter.hasNext();) {
//						ParameterTable entityCollection=(ParameterTable)iter.next();
//						if(!entityCollection.getParameterTablePk().equals(EntityCollectionType.RNT.getCode())){
//							iter.remove();
//						}
//						
//					}
//				}
//			}
//		}
//		else if(serviceType.equals(PropertiesConstants.SERVICE_TYPE_MANUAL_PK)){
//			
//			if(baseCollection.equals(BaseCollectionType.COBRO_MANUAL.getCode())){
//				/***
//				 * No remover nada, abierto a todos los clientes servicios
//				 * */
//			}
//		}
//	}
//		this.collectionInstitutionSelected=-1;
	}
	
	
	/**
	 * Update list calculation period.
	 */
	public void updateListCalculationPeriod(){
		
		log.debug("updateListCalculationPeriod");
//		/**Collection Base*/
//		Integer baseCollection=this.collectionBaseSelected;
//		listCalculationPeriod=BillingUtils.cloneList(listCalculationPeriodDynamic);
//		/**Service Type*/
//		Integer serviceType=this.serviceTypeSelected;
//		if( (serviceType.equals(PropertiesConstants.SERVICE_TYPE_AUTOMATIC_PK))){
//			if( baseCollection.equals(BaseCollectionType.LIQUIDACION_OPERACIONES_BVRD.getCode()) ||
//				baseCollection.equals(BaseCollectionType.LIQUIDACION_OPERACIONES_HACIENDA.getCode()) || 
//				baseCollection.equals(BaseCollectionType.LIQUIDACION_OPERACIONES_OTC.getCode()) ||
//				baseCollection.equals(BaseCollectionType.LIQUIDACION_OPERACIONES_BCRD.getCode()) ||
//				baseCollection.equals(BaseCollectionType.TRANSACCIONES_MOVIMIENTOS.getCode())  ||
//				baseCollection.equals(BaseCollectionType.TRANSACCIONES_EFECTIVO_PRESTAMO.getCode())  ||
//				baseCollection.equals(BaseCollectionType.TRANSACCIONES_EFECTIVO_RECOMPRA.getCode())  ||
//				baseCollection.equals(BaseCollectionType.DEPOSITOS_RETIROS_VALORES_INTERNACIONALES.getCode()) ||
//				baseCollection.equals(BaseCollectionType.CUSTODIA_VALORES_INTERNACIONALES.getCode())  ){
//				
//				for (Iterator<ParameterTable> iter=listCalculationPeriod.iterator(); iter.hasNext();) {
//					ParameterTable calculalationPeriod=(ParameterTable)iter.next();
//					if(!calculalationPeriod.getParameterTablePk().equals(PropertiesConstants.PERIOD_CALCULATION_DAILY_PK)){
//						iter.remove();
//					}
//				}
//				
//			}
//			else if(baseCollection.equals(BaseCollectionType.CUOTA_ANUAL_ENTIDADES.getCode())
//				||baseCollection.equals(BaseCollectionType.CUOTA_MENSUAL_EMISIONES.getCode()) 
//				||baseCollection.equals(BaseCollectionType.CUOTA_ACCESO_POR_ENTIDAD.getCode())){
//				for (Iterator<ParameterTable> iter=listCalculationPeriod.iterator(); iter.hasNext();) {
//					ParameterTable calculalationPeriod=(ParameterTable)iter.next();
//					if(!calculalationPeriod.getParameterTablePk().equals(PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK)){
//						iter.remove();
//					}
//					
//				}
//			}
//			else if(baseCollection.equals(BaseCollectionType.REGISTRO_ENTIDADES.getCode())||
//				baseCollection.equals(BaseCollectionType.REGISTRO_EMISIONES.getCode()) ){					
//
//				for (Iterator<ParameterTable> iter=listCalculationPeriod.iterator(); iter.hasNext();) {
//					ParameterTable calculalationPeriod=(ParameterTable)iter.next();
//					if(calculalationPeriod.getParameterTablePk().equals(PropertiesConstants.PERIOD_CALCULATION_DAILY_PK)
//							||calculalationPeriod.getParameterTablePk().equals(PropertiesConstants.PERIOD_CALCULATION_MONTHLY_PK)){
//						
//					}
//					else{
//						iter.remove();
//					}
//					
//				}
//				}
//			
//		}
//		else if(serviceType.equals(PropertiesConstants.SERVICE_TYPE_EVENTUAL_PK)){
//			for (Iterator<ParameterTable> iter=listCalculationPeriod.iterator(); iter.hasNext();) {
//				ParameterTable calculalationPeriod=(ParameterTable)iter.next();
//				if(!calculalationPeriod.getParameterTablePk().equals(PropertiesConstants.PERIOD_CALCULATION_EVENT_PK)){
//					iter.remove();
//				}
//				
//			}
//		}
//		else if(serviceType.equals(PropertiesConstants.SERVICE_TYPE_MANUAL_PK)){
//			for (Iterator<ParameterTable> iter=listCalculationPeriod.iterator(); iter.hasNext();) {
//				ParameterTable calculalationPeriod=(ParameterTable)iter.next();
//				if(!calculalationPeriod.getParameterTablePk().equals(PropertiesConstants.PERIOD_CALCULATION_EVENT_PK)){
//					iter.remove();
//				}
//				
//			}
//		}
//		else{
//		}
//		disableCollectionBase();
		
	}

	/**
	 * *
	 * Metodo para habilitar el tipo de Base de Cobro.
	 */
	public void updateWayBaseCollection(){
		
		if(indSourceSelected.equals(GeneralConstants.ONE_VALUE_INTEGER)){
			billingSourceHelperDisable=Boolean.FALSE;
		}else{
			billingSourceHelperDisable=Boolean.TRUE;
			this.billingServiceSource=new BillingServiceSource();
		}
	}
	
	/**
	 * Update payment.
	 */
	public void updatePayment(){
		
	}
	
	/**
	 * *	Apply privileges to Rate Security	**.
	 */
	public void setTrueValidButtons(){        

		PrivilegeComponent privilegeComponent = new PrivilegeComponent();

		
		if(userPrivilege.getUserAcctions().isModify()){
			privilegeComponent.setBtnModifyView(true);    
		}

		if(userPrivilege.getUserAcctions().isBlock()){
			privilegeComponent.setBtnBlockView(true);
		}
		if(userPrivilege.getUserAcctions().isUnblock()){
			privilegeComponent.setBtnUnblockView(true);
		}
		/* Redmine 1222 Se retira boton cancelar
		if(userPrivilege.getUserAcctions().isCancel()){
			privilegeComponent.setBtnCancel(true);
		}*/
		/* Se retira boton de eliminar
		if(userPrivilege.getUserAcctions().isDelete()){
			privilegeComponent.setBtnDeleteView(true);
		}*/
		// Privilage to active services with rates
		if(userPrivilege.getUserAcctions().isActivate()){
			privilegeComponent.setBtnActivate(true);
		}

		userPrivilege.setPrivilegeComponent(privilegeComponent);

	}

	
	/**
	 * Disable collection base.
	 *
	 * @return the boolean
	 */
	public Boolean disableCollectionBase(){

		if( !(Validations.validateIsNotNullAndPositive(this.serviceTypeSelected)) ){
			collectionBaseDisable = Boolean.TRUE;
		}else {
			collectionBaseDisable = Boolean.FALSE;
		}
		return collectionBaseDisable;
	}


	/**
	 * Update  List Calculation Period for Service Type.
	 */
	public void updateListCalculationPeriodForServiceType(){
		Integer serviceType=this.serviceTypeSelected;

			if( (serviceType.equals(PropertiesConstants.SERVICE_TYPE_AUTOMATIC_PK))){
				for (Iterator<ParameterTable> iter=listCalculationPeriod.iterator(); iter.hasNext();) {
					ParameterTable calculalationPeriod=(ParameterTable)iter.next();
					if(!calculalationPeriod.getParameterTablePk().equals(PropertiesConstants.COLLECTION_PERIOD_MONTHLY_PK)){
						iter.remove();
					}
					
				}
				
				
			}
			else if(serviceType.equals(PropertiesConstants.SERVICE_TYPE_EVENTUAL_PK)){
			}
			else if(serviceType.equals(PropertiesConstants.SERVICE_TYPE_MANUAL_PK)){
			}
		

	}


	


	/**
	 * Validate have service rate.
	 *
	 * @throws ServiceException the service exception
	 */
	public void validateHaveServiceRate()throws ServiceException {	   

		if( Validations.validateIsNotNull(billingServiceSelection) && Validations.validateIsNotNullAndNotEmpty(billingServiceSelection.getIdBillingServicePk()) ){
			Object codigo=billingServiceSelection.getServiceCode();	   
			List<ServiceRate> serviceRateList = billingRateMgmtServiceFacade.getServicesRateByAllBillingService(billingServiceSelection.getIdBillingServicePk());

			if( !(Validations.validateIsNullOrNotPositive(serviceRateList.size())) ){
				executeAction();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_HAVE_RATE,codigo));
				JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
				return ;
			}

		}
	}
	

	/**
	 * Delete billing service.
	 *
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void deleteBillingService() throws ServiceException{
		

		if (Validations.validateIsNotNull(this.billingServiceSelection)) {	
			
			Object codigo=billingServiceSelection.getServiceCode();
			
			billingServiceMgmtServiceFacade.deleteBillingService(billingServiceSelection);
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
					, PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_BILLING_SERVICE_DELETED,codigo));
			JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
			
			searchServiceBilling();
		}
				
		
	}
	
	
	
/**
 * Actived BillingService and ServiceRate.
 *
 * @throws ServiceException the service exception
 */
	public void befoteActivateServiceAndRate() throws ServiceException { 
		if (Validations.validateIsNotNull(this.billingServiceSelection)) {	
			Object codigo=billingServiceSelection.getServiceCode();
			Date date = CommonsUtilities.currentDate();
			
			 
			if (billingServiceSelection.getServiceState().equals(BillingServiceStatus.CANCELLED.getCode()))
			{	
				executeAction();				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_CANCELLED ,codigo));
				JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");

				return;
			}
			if (billingServiceSelection.getServiceState().equals(BillingServiceStatus.EXPIRED.getCode()))
			{	
				executeAction();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_EXPIRED,codigo));
				JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
				return;
			}			
			if (billingServiceSelection.getServiceState().equals(BillingServiceStatus.LOCKED.getCode()))
			{	
				executeAction();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_LOCK,codigo));
				JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
				return;
			} 
			
			
			if( billingServiceSelection.getInitialEffectiveDate().after(date) ){
				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_BILLING_SERVICE_AFTER_DATE,codigo));
				JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");				
				
				return;
			}
			
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
					, PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_BILLING_SERVICE_ACTIVED,codigo));
			JSFUtilities.executeJavascriptFunction("PF('cnfwServiceActive').show();");
		}	
		else{
			showMessageOnDialog(PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.MODIFY_RATE_NO_SELECTED));
			JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");			
		}
		
	}
	
	
/**
 * Activate billing with service rate.
 */
// ACTIVAR TARIFAS !! 		
	@LoggerAuditWeb
	public void activateBillingWithServiceRate(){
		
		if( Validations.validateIsNotNullAndNotEmpty(billingServiceSelection) 
				&& Validations.validateIsNotNullAndNotEmpty(billingServiceSelection.getIdBillingServicePk()) ){
			
			//	BOTON
			billingServiceMgmtServiceFacade.activedBillingServicesRegisterActived(billingServiceSelection.getIdBillingServicePk()); 
		
			log.debug(" se actualiza el  servicio de facturacion ");
			Object codigo=billingServiceSelection.getServiceCode();
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
					, PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_BILLING_SERVICE_ACTIVE,codigo));
			JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
			searchServiceBilling();
		
		}	
				
		//	BACHERO buscar todos los servicios ACTIVOS Y BLOQUEADOS  para Activar
		else
		billingServiceMgmtServiceFacade.activedBillingServicesRegisterActived(null); 
	}

		
/**
 * Expired all rate.
 */
// CADUDAR TARIFAS !! 
		@LoggerAuditWeb
		public void expiredAllRate(){
			// buscar todos los servicios ACTIVOS Y BLOQUEADOS  para Expirar
			Date date = CommonsUtilities.currentDate();
			try {
				billingServiceMgmtServiceFacade.expiredBillingServicesActivedLock(date);
			} catch (ServiceException e) {
				e.printStackTrace();
				log.debug(e.getMessage());
			} 
		}
 
		
		/**
		 * Clean result billing service data model.
		 */
		public void cleanResultBillingServiceDataModel(){
			
			if(Validations.validateIsNotNullAndNotEmpty(billingServiceDataModel) && billingServiceDataModel.getRowCount() > 0) {
				
				billingServiceDataModel = null ;			
			}	
			
		} 
		
		/**
		 * Metodo para el Helper de Fuente de Billing Service.
		 *
		 * @param event the event
		 */
		public void actionRemoteHelper(ActionEvent event){
			
		}
		
		// TODO METODOS DE LA TARIFA
		/**
		 * Metodos de las tarifas*.
		 */
		/** active button  addRateId that allow show panel type Service Rate*/
		public void onSelectItemRate() {

			JSFUtilities.setValidViewComponentAndChildrens("frmBillingServiceRegister:otpRatesID");
			if (Validations.validateIsNotNull(this.serviceRateTo)) {
				if (this.serviceRateTo.getRateType().intValue() == -1) {
					this.addRateDisable = Boolean.TRUE;
				} else {
					this.addRateDisable = Boolean.FALSE;
				}
			}
			disableOutputPanelToRateType();
			cleanRateDialog();
			cleanServiceRate();
			
		}
		
		/**
		 * *
		 * For Rate staggered with Movement.
		 */
		public void showRateMovement(){
			
			if(serviceRateTo.getIndScaleMovement().equals(GeneralConstants.ONE_VALUE_INTEGER)){
				serviceRateTo.setFlagStaggeredMovement(Boolean.TRUE);
			}
			else{
				serviceRateTo.setFlagStaggeredMovement(Boolean.FALSE);
			}
			
		}
		
		/**
		 * Check if Applied Minimum Rate.
		 */
		public void appliedMinimumRate(){
			
			if(serviceRateTo.getIndAmountMinimum().equals(GeneralConstants.ONE_VALUE_INTEGER)){
				serviceRateTo.setFlagAmountMinimum(Boolean.TRUE);
			}else{
				serviceRateTo.setFlagAmountMinimum(Boolean.FALSE);
			}
			
			serviceRateTo.setCurrencyMinimum(null);
			serviceRateTo.setRateMinimum(null);
		}
		
		/**
		 * Clean rate dates.
		 *
		 * @throws ServiceException the service exception
		 */
		public void cleanRateDates() throws ServiceException{ 
			this.serviceRateTo=new ServiceRateTo(); 

			this.serviceRateTo.setCurrencyRate(currencyCalculation);		
			JSFUtilities.setValidViewComponentAndChildrens("frmBillingServiceRegister:otpRatesID");
			JSFUtilities.setValidViewComponentAndChildrens("frmBillingServiceRegister:flsRate");
			serviceRateTo.setRateType(GeneralConstants.NEGATIVE_ONE_INTEGER);
		}
		
		/**
		 * Before add type service rate.
		 */
		public void  beforeAddTypeServiceRate(){


			if( serviceRateTo.getRateName().length()>200 ){
				showMessageOnDialog(
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SERVICE_RATE_NAME_LONG));
				JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");


			}else if( !(serviceRateTo.getRateName().length()>200) ){
				if( serviceRateTo.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode()) ) {
					serviceRateTo.setUnit(GeneralConstants.ONE_VALUE_INTEGER);
				}
				if(GeneralConstants.ONE_VALUE_INTEGER.equals(serviceRateTo.getIndScaleMovement())){
					if(Validations.validateListIsNullOrEmpty(serviceRateTo.getListRateMovementTo())){
						showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_EMPTY_MOVEMENT_LIST));
						JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
						return;
					}
				}
				
				if(GeneralConstants.ONE_VALUE_INTEGER.equals(serviceRateTo.getIndAmountMinimum())){
					if(( Validations.validateIsNotNull(serviceRateTo.getRateMinimum()) && 
							serviceRateTo.getRateMinimum().compareTo(BigDecimal.ZERO)<=0
							)){
						showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_EMPTY_AMOUNT_MINIMUM));
						JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
						return;
					}
					
					if(Validations.validateIsNullOrNotPositive(serviceRateTo.getCurrencyMinimum())){
						showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
								PropertiesUtilities.getMessage(PropertiesConstants.WARNING_EMPTY_CURRENCY_AMOUNT_MINIMUM));
						JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
						return;
					}
				}

				
				if(Validations.validateIsNotNull(billingServiceSelection)){
					/**Agrega tarifas a un servicio ya creado (Modificar servicio)*/
					Object codigo=this.billingServiceSelection.getServiceCode();
					executeAction();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
							, PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_TYPE_SERVICE_RATE_ADD,codigo));
					JSFUtilities.executeJavascriptFunction("PF('cnfwTypeRateAdd').show();");
				}else{
					/**Agrega tarifas para un servicio nuevo (Registrar servicio)**/
					executeAction();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
							, PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_TYPE_NEW_SERVICE_RATE_ADD));
					JSFUtilities.executeJavascriptFunction("PF('cnfwTypeRateAdd').show();");
				}
				
			}
		}
		
		/**
		 * Adds the type service rate.
		 */
		public void  addTypeServiceRate(){
			
			/** Only add rate service at list temporal , it's on memory but It is not on DB */
			ServiceRateTo serviceRateTo = SerializationUtils.clone(this.serviceRateTo);

			serviceRateTo.setRateStatus(RateServiceStatus.PENDING.getCode());
			serviceRateTo.setDescriptionStatus(RateServiceStatus.PENDING.getValue());

			String descriptionRateType =businessLayerCommonFacade.getValueParameterTable(listRateType,serviceRateTo.getRateType());
			serviceRateTo.setDescriptionType(descriptionRateType);
			
			if(isViewOperationModify()){
				this.billingServiceTo.addServiceRateToModify(serviceRateTo);
				this.rateServiceToDataModel=new RateServiceToDataModel(billingServiceTo.getListServiceRateToModify());
			}else if(isViewOperationRegister()){
				this.billingServiceTo.addServiceRateTo(serviceRateTo);
				this.rateServiceToDataModel= new RateServiceToDataModel(billingServiceTo.getListServiceRateTo());
			}
			
			/** preparing data for next service rate */
			this.serviceRateTo = new ServiceRateTo(this.billingServiceTo);
			this.serviceRateTo.setCurrencyRate(this.billingServiceTo.getCurrencyCalculation());
			this.addRateDisable = Boolean.TRUE;

		}

		/**
		 * .METHOD CALLED FROM BLUR EVENT
		 *
		 * @throws ServiceException the service exception
		 */
		public void findMovementTypeByCode() throws ServiceException{

			Long codeMovementType = this.serviceRateTo.getCodeMovementType();
			if(Validations.validateIsNotNullAndNotEmpty(codeMovementType)){
				MovementType movementType = businessLayerCommonFacade.findMovementTypeByCode(codeMovementType);
				if(Validations.validateIsNotNull(movementType)){
					this.serviceRateTo.setMovementTypeSelected(movementType);
					this.serviceRateTo.setCodeMovementType(movementType.getIdMovementTypePk());
					this.serviceRateTo.setDescriptionMovementType(movementType.getMovementName());
				}else {


					String holderHelpMessage = PropertiesUtilities.getMessage(GeneralConstants.MOVEMENT_TYPE_PROPERTIES, JSFUtilities.getCurrentLocale(),
							PropertiesConstants.SEARCH_MOVEMENT_TYPE);
					String title = PropertiesUtilities.getMessage(GeneralConstants.MOVEMENT_TYPE_PROPERTIES, JSFUtilities.getCurrentLocale(),
							PropertiesConstants.BODY_NOT_FOUND_MOVEMENT_TYPE);
					JSFUtilities.putViewMap("bodyMessageView", holderHelpMessage);
					JSFUtilities.putViewMap("headerMessageView", title);
					JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationBlur').show();");

					/** Deleting attributes on  page **/
					this.serviceRateTo.setCodeMovementType(null);
					this.serviceRateTo.setDescriptionMovementType(null);

				}

			}

		}


		/**
		 * Gets the service rate to.
		 *
		 * @return the serviceRateTo
		 */
		public ServiceRateTo getServiceRateTo() {
			return serviceRateTo;
		}


		/**
		 * Sets the service rate to.
		 *
		 * @param serviceRateTo the serviceRateTo to set
		 */
		public void setServiceRateTo(ServiceRateTo serviceRateTo) {
			this.serviceRateTo = serviceRateTo;
		}


		/**
		 * Gets the adds the rate disable.
		 *
		 * @return the addRateDisable
		 */
		public Boolean getAddRateDisable() {
			return addRateDisable;
		}


		/**
		 * Sets the adds the rate disable.
		 *
		 * @param addRateDisable the addRateDisable to set
		 */
		public void setAddRateDisable(Boolean addRateDisable) {
			this.addRateDisable = addRateDisable;
		}
		
		/**
		 * Gets the list module movement.
		 *
		 * @return the listModuleMovement
		 */
		public List<ParameterTable> getListModuleMovement() {
			return listModuleMovement;
		}

		/**
		 * Sets the list module movement.
		 *
		 * @param listModuleMovement the listModuleMovement to set
		 */
		public void setListModuleMovement(List<ParameterTable> listModuleMovement) {
			this.listModuleMovement = listModuleMovement;
		}

		/**
		 * Gets the ind staggered percentage mix.
		 *
		 * @return the indStaggeredPercentageMix
		 */
		public Boolean getIndStaggeredPercentageMix() {
			return indStaggeredPercentageMix;
		}

		/**
		 * Sets the ind staggered percentage mix.
		 *
		 * @param indStaggeredPercentageMix the indStaggeredPercentageMix to set
		 */
		public void setIndStaggeredPercentageMix(Boolean indStaggeredPercentageMix) {
			this.indStaggeredPercentageMix = indStaggeredPercentageMix;
		}

		/**
		 * Gets the ind staggered fixed mix.
		 *
		 * @return the indStaggeredFixedMix
		 */
		public Boolean getIndStaggeredFixedMix() {
			return indStaggeredFixedMix;
		}

		/**
		 * Sets the ind staggered fixed mix.
		 *
		 * @param indStaggeredFixedMix the indStaggeredFixedMix to set
		 */
		public void setIndStaggeredFixedMix(Boolean indStaggeredFixedMix) {
			this.indStaggeredFixedMix = indStaggeredFixedMix;
		}

		/**
		 * Gets the service rate consult to.
		 *
		 * @return the serviceRateConsultTo
		 */
		public ServiceRateTo getServiceRateConsultTo() {
			return serviceRateConsultTo;
		}

		/**
		 * Sets the service rate consult to.
		 *
		 * @param serviceRateConsultTo the serviceRateConsultTo to set
		 */
		public void setServiceRateConsultTo(ServiceRateTo serviceRateConsultTo) {
			this.serviceRateConsultTo = serviceRateConsultTo;
		}


		/**
		 * Gets the list movement empty.
		 *
		 * @return the listMovementEmpty
		 */
		public Boolean getListMovementEmpty() {
			return listMovementEmpty;
		}

		/**
		 * Sets the list movement empty.
		 *
		 * @param listMovementEmpty the listMovementEmpty to set
		 */
		public void setListMovementEmpty(Boolean listMovementEmpty) {
			this.listMovementEmpty = listMovementEmpty;
		}

		/**
		 * Gets the rendered dialog.
		 *
		 * @return the renderedDialog
		 */
		public Boolean getRenderedDialog() {
			return renderedDialog;
		}
		

		/**
		 * Gets the billing service to.
		 *
		 * @return the billingServiceTo
		 */
		public BillingServiceTo getBillingServiceTo() {
			return billingServiceTo;
		}

		/**
		 * Sets the billing service to.
		 *
		 * @param billingServiceTo the billingServiceTo to set
		 */
		public void setBillingServiceTo(BillingServiceTo billingServiceTo) {
			this.billingServiceTo = billingServiceTo;
		}

		/**
		 * Sets the rendered dialog.
		 *
		 * @param renderedDialog the renderedDialog to set
		 */
		public void setRenderedDialog(Boolean renderedDialog) {
			this.renderedDialog = renderedDialog;
		}
		
		

		/**
		 * Gets the list rate status.
		 *
		 * @return the listRateStatus
		 */
		public List<ParameterTable> getListRateStatus() {
			return listRateStatus;
		}

		/**
		 * Sets the list rate status.
		 *
		 * @param listRateStatus the listRateStatus to set
		 */
		public void setListRateStatus(List<ParameterTable> listRateStatus) {
			this.listRateStatus = listRateStatus;
		}
		
		

		/**
		 * Gets the service rate modify to.
		 *
		 * @return the serviceRateModifyTo
		 */
		public ServiceRateTo getServiceRateModifyTo() {
			return serviceRateModifyTo;
		}


		/**
		 * Sets the service rate modify to.
		 *
		 * @param serviceRateModifyTo the serviceRateModifyTo to set
		 */
		public void setServiceRateModifyTo(ServiceRateTo serviceRateModifyTo) {
			this.serviceRateModifyTo = serviceRateModifyTo;
		}


		/**
		 * Show shearch movement.
		 *
		 * @throws ServiceException the service exception
		 */
		public void showShearchMovement() throws ServiceException{

			ParameterTableTO parameterTableTO = new ParameterTableTO();						
			if (Validations.validateListIsNullOrEmpty(listModuleMovement)) {
				log.debug("Listado de Modulos  de Movimientos  :");

				parameterTableTO.setMasterTableFk(PropertiesConstants.MODULE_TYPE);
				listModuleMovement = generalParametersFacade
						.getListParameterTableServiceBean(parameterTableTO);
			}
			JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidationBlur').hide();");
			JSFUtilities.executeJavascriptFunction("PF('dlgMovement').show();");

		}
		
		/**
		 * Before add table movement to staggered.
		 */
		public void beforeAddTableMovementToStaggered(){

			 
			beforeAddMovementToList();
	}

		
		/**
		 * Making MovementType's List before adding to one new Service Rate.
		 */
		public void beforeAddMovementToList(){

			MovementType movementTypeNew = this.serviceRateTo.getMovementTypeSelected();
			List<RateMovementTo> listRateMovementTo = serviceRateTo.getListRateMovementTo();

			if ( Validations.validateIsNotNull(movementTypeNew) && Validations.validateIsNotNull(movementTypeNew.getIdMovementTypePk()) ){

				/** VALIDATION IF REPEAT THE IdMovement**/
				for (RateMovementTo rateMovementTo : listRateMovementTo) {
					if(listRateMovementTo.size()+1<=100){
						if( rateMovementTo.getMovementType().getIdMovementTypePk().equals(movementTypeNew.getIdMovementTypePk()) ){
							executeAction();
							showMessageOnDialog(PropertiesUtilities
									.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getMessage(PropertiesConstants.WARNING_DOUBLE_MOVEMENT_CODE));
							JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
							return;
						}
					}
					else if( listRateMovementTo.size()+1>100 ) {
						executeAction();
						showMessageOnDialog(PropertiesUtilities
								.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
								PropertiesUtilities.getMessage(PropertiesConstants.EXCESSIVE_MOVEMENT));
						JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");
						return ; 
					}
				}

				movementTypeNew =SerializationUtils.clone(movementTypeNew);
				RateMovementTo rateMovementTo = new RateMovementTo();			
				rateMovementTo.setMovementType(movementTypeNew);
				rateMovementTo.setRateAmount(this.serviceRateTo.getAmountRate());
				rateMovementTo.setRatePercent(this.serviceRateTo.getPercentageRate());
				this.serviceRateTo.getListRateMovementTo().add(rateMovementTo);
				// TODO
				this.setDataModelRateMovementToList(new GenericDataModel(this.serviceRateTo.getListRateMovementTo()));
//				this.serviceRateTo.setDataModelRateMovementToList(new GenericDataModel(this.serviceRateTo.getListRateMovementTo()));

				/** Removing movement type selected  from the search*/			
				this.serviceRateTo.setMovementTypeSelected(new MovementType());
				/** Deleting attributes on  page **/
				this.serviceRateTo.setCodeMovementType(null);
				this.serviceRateTo.setDescriptionMovementType(null);
				this.serviceRateTo.setAmountRate(null);
				this.serviceRateTo.setPercentageRate(null);
			}
		}
		
		/**
		 * Movement rate remove.
		 *
		 * @param event the event
		 */
		public void movementRateRemove(ActionEvent event) { 
			RateMovementTo rateMovementTo = (RateMovementTo) event	 .getComponent().getAttributes().get("removeName");
			serviceRateTo.getListRateMovementTo().remove(rateMovementTo); 
		}
		
		
		/**
		 * Clean rate from accumulative.
		 */
		public void cleanRateFromAccumulative(){
			serviceRateTo.setUnit(GeneralConstants.ZERO_VALUE_INTEGER);
			/***
			 * Staggered Mixed
			 */
			serviceRateTo.setIndStaging(GeneralConstants.ZERO_VALUE_INTEGER);
			cleanRateDialog();
		}
		
		/**
		 * Clean rate dialog.
		 */
		public void cleanRateDialog(){ 
			serviceRateTo.setMinScaleValue(null);
			serviceRateTo.setMaxScaleValue(null);
			serviceRateTo.setAmountRate(null);
			serviceRateTo.setPercentageRate(null);

			JSFUtilities.setValidViewComponentAndChildrens("frmBillingServiceRegister:cnIdStaggered");


			if (Validations.validateListIsNotNullAndNotEmpty(serviceRateTo.getListServiceRateScaleTo())){
				serviceRateTo.getListServiceRateScaleTo().clear();
			}
		}

		/**
		 * Generar un nuevo escalon.
		 */
		public void generateOneStageRate(){

			this.serviceRateTo.generateOneStageRate();

			rateServiceScaleToDataModel.setWrappedData(serviceRateTo.getListServiceRateScaleTo());

		}
		
		/**
		 * Eliminar la tarifa de la memoria.
		 *
		 * @param event the event
		 */
		public void rateRemove(ActionEvent event) {
			ServiceRateScaleTo serviceRateScaleTo = (ServiceRateScaleTo) event
					.getComponent().getAttributes().get("removeName");

			serviceRateTo.getListServiceRateScaleTo().remove(serviceRateScaleTo); 
			serviceRateTo.setFlagIsModifyStaggeredFixed(Boolean.TRUE);
		}

		/**
		 * *
		 * 0: Fixed
		 * 1: Percentage
		 * 
		 * Change option Staging Rate.
		 */
		public void stagingRate(){
			
			/**
			 * Percentage
			 */
			if(serviceRateTo.getIndStaging().equals(GeneralConstants.ONE_VALUE_INTEGER)){
				indStaggeredFixedMix=Boolean.FALSE;
				indStaggeredPercentageMix=Boolean.TRUE;
			}else{
				/***
				 * Fixed
				 */
				indStaggeredFixedMix=Boolean.TRUE;
				indStaggeredPercentageMix=Boolean.FALSE;
			}
			serviceRateTo.setPercentageRate(null);
			serviceRateTo.setAmountRate(null);
			
		}
		
		/**
		 * Change Option Other Currency in Rate Staggered  Fixed.
		 */
		public void changeOtherCurrency(){
			
			/**If Not other currency*/
			if(serviceRateTo.getIndOtherCurrency().equals(GeneralConstants.ZERO_VALUE_INTEGER)){
				serviceRateTo.setOtherCurrency(null);
			}
		}
		
		/**
		 *  Validations on Rates to Show OuputPanel.
		 */
		public void disableOutputPanelToRateType(){
			serviceRateTo.disableFlagRateServices();
			


			if(GeneralConstants.ONE_VALUE_INTEGER.equals(this.serviceRateTo.getIndAmountMinimum())){
				serviceRateTo.setFlagMinimumAmount(true);
			}
			if(serviceRateTo.getRateType().equals(RateType.FIJO.getCode())) {			
				
				serviceRateTo.setFlagFixed(true);
				
			}else if(serviceRateTo.getRateType().equals(RateType.PORCENTUAL.getCode())) {
				
				serviceRateTo.setFlagPercentage(true);
				
			}else if(serviceRateTo.getRateType().equals(RateType.ESCALONADO_FIJO.getCode())) {
				
				serviceRateTo.setFlagStaggeredFixed(true);
				serviceRateTo.setFlagIsMovement(true);
				 
				
			}else if(serviceRateTo.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode())) {
				
				serviceRateTo.setFlagStaggeredPercentage(true);
				serviceRateTo.setFlagIsMovement(true);
				 
				
			}else if(serviceRateTo.getRateType().equals(RateType.TIPO_MOVIMIENTO_FIJO.getCode())) {
				
				serviceRateTo.setFlagMovementTypeFixed(true);
				
			}else if(serviceRateTo.getRateType().equals(RateType.TIPO_MOVIMIENTO_PORCENTUAL.getCode())) {
				
				serviceRateTo.setFlagMovementTypePercentage(true);
				
			}
			else if(serviceRateTo.getRateType().equals(RateType.ESCALONADO_MIXTO.getCode())) {
				
				serviceRateTo.setFlagStaggeredMix(true);
				serviceRateTo.setFlagIsMovement(true);
				indStaggeredFixedMix=Boolean.TRUE;
				indStaggeredPercentageMix=Boolean.FALSE;
				
			}
		}
		
		/**
		 * Validaciones antes de ingresar una tarifa de movimiento a la Tabla.
		 */
		public void beforeAddTableMovementFixed(){

			if(Validations.validateIsNullOrNotPositive(this.serviceRateTo.getAmountRate().longValue())){
				JSFUtilities.setInvalidViewComponent("formAddRate:txtAmountFixedMovement");	
				JSFUtilities.addContextMessage("formAddRate:txtAmountFixedMovement",
						FacesMessage.SEVERITY_ERROR,
						"Debe ingresar el monto", 
						"Debe ingresar el monto");
				return;
			}
			else{
				beforeAddMovementToList();
				return;			
			}
		}
		
		/**
		 * Before add table movement percentage.
		 */
		public void beforeAddTableMovementPercentage(){
			if(Validations.validateIsNull(this.serviceRateTo.getPercentageRate())){
				JSFUtilities.setInvalidViewComponent("formAddRate:txtAmountFixedMovementP");	
				JSFUtilities.addContextMessage("formAddRate:txtAmountFixedMovementP",
						FacesMessage.SEVERITY_ERROR,
						"Debe ingresar el monto", 
						"Debe ingresar el monto");
				return;
			}
			else{
				beforeAddMovementToList();
				return;			
			}
		}
		
		/**
		 * Back type service rate.
		 */
		public void backTypeServiceRate(){

			

			if (this.serviceRateTo.isFlagPercentage()){
				this.serviceRateTo.setPercentageRate(null);
				this.serviceRateTo.setFlagPercentage(Boolean.FALSE);	

			}else if (this.serviceRateTo.isFlagFixed()){
				this.serviceRateTo.setAmountRate(null);
				this.serviceRateTo.setFlagFixed(Boolean.FALSE);

			}else if (this.serviceRateTo.isFlagStaggeredFixed()){
				List<ServiceRateScaleTo> listServicerateScaleTo = new ArrayList<ServiceRateScaleTo>();	
				this.serviceRateTo.setSequenceNumber(null);
				this.serviceRateTo.setListServiceRateScaleTo(listServicerateScaleTo);
//				this.serviceRateTo.getRateServiceScaleToDataModel().setWrappedData(listServicerateScaleTo);
				this.getRateServiceScaleToDataModel().setWrappedData(listServicerateScaleTo);

				this.serviceRateTo.setFlagStaggeredFixed(Boolean.FALSE);
				if(this.serviceRateTo.getIdBillingService().equals(GeneralConstants.ONE_VALUE_STRING)){
					this.serviceRateTo.setFlagStaggeredMovement(Boolean.FALSE);
				}
				

			}else if (this.serviceRateTo.isFlagMovementTypeFixed()){
				cleanServiceRate(); 
				this.serviceRateTo.setFlagMovementTypeFixed(Boolean.FALSE);
			}

		}
		
		
		/**
		 * Clean service rate.
		 */
		public void cleanServiceRate(){

			serviceRateTo.setMinScaleValue(null);
			serviceRateTo.setMaxScaleValue(null);
			serviceRateTo.setAmountRate(null); 
			serviceRateTo.setPercentageRate(null);
			serviceRateTo.setRateAmount(null);		
			serviceRateTo.setCodeMovementType(null);
			serviceRateTo.setDescriptionMovementType(null); 
			serviceRateTo.setUnit(GeneralConstants.ZERO_VALUE_INTEGER); 
			/**
			 * Staggered Mixed
			 */
			serviceRateTo.setIndStaging(GeneralConstants.ZERO_VALUE_INTEGER);
			serviceRateTo.setCodeMovementType(null); 
			serviceRateTo.setIndScaleMovement(0);
			
			
			/** setting list */
			serviceRateTo.getListServiceRateScaleTo().clear();	
			serviceRateTo.getListRateMovementTo().clear();

			if(serviceRateTo.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode())) {
//				serviceRateTo.setUnit(GeneralConstants.ONE_VALUE_INTEGER);
			}
		}

		/**
		 * VIEW RATE WHILE IS REGISTER .
		 *
		 * @param serviceRateTo the service rate to
		 */
		@LoggerAuditWeb
		public void viewMemoryRate(ServiceRateTo serviceRateTo){
			log.debug("cargar tarifa :");
			this.serviceRateConsultTo = serviceRateTo;

			if(GeneralConstants.ONE_VALUE_INTEGER.equals(this.serviceRateConsultTo.getIndAmountMinimum())){
				serviceRateConsultTo.setCurrencyMinimum(serviceRateTo.getCurrencyMinimum());
				serviceRateConsultTo.setRateMinimum(serviceRateTo.getRateMinimum());
				serviceRateConsultTo.setFlagMinimumAmount(Boolean.TRUE);
			}
			if (this.serviceRateConsultTo.getRateType().intValue() == RateType.FIJO.getCode()) {
				
				if( Validations.validateIsNotNullAndNotEmpty(serviceRateTo.getRateAmount()) ){			 
					this.serviceRateConsultTo.setAmountRate(serviceRateTo.getRateAmount());
					JSFUtilities.executeJavascriptFunction("PF('cnfwFlagConsultMemory').show();");
				}else if( Validations.validateIsNotNullAndNotEmpty(serviceRateTo.getAmountRate()) ){
					this.serviceRateConsultTo.setAmountRate(serviceRateTo.getAmountRate());
					JSFUtilities.executeJavascriptFunction("PF('cnfwFlagConsultMemory').show();");
				}
				this.serviceRateConsultTo.setFlagFixed(Boolean.TRUE);
				
			} else if(this.serviceRateConsultTo.getRateType().intValue() == RateType.PORCENTUAL.getCode()) {
				
				if( Validations.validateIsNotNullAndNotEmpty(serviceRateTo.getPercentageRate()) ){
					this.serviceRateConsultTo.setRatePercent(serviceRateTo.getPercentageRate());
					JSFUtilities.executeJavascriptFunction("PF('cnfwFlagConsultMemory').show();");

				}else if( Validations.validateIsNotNullAndNotEmpty(serviceRateTo.getRatePercent()) ){
					this.serviceRateConsultTo.setRatePercent(serviceRateTo.getRatePercent());
					JSFUtilities.executeJavascriptFunction("PF('cnfwFlagConsultMemory').show();");

				} 
				this.serviceRateConsultTo.setFlagPercentage(Boolean.TRUE);		

				/**		 MOVEMENTS 		  **/	
			}else if(this.serviceRateConsultTo.getRateType().intValue() == RateType.TIPO_MOVIMIENTO_FIJO.getCode()) {
				JSFUtilities.executeJavascriptFunction("PF('cnfwFlagConsultMemory').show();");

				this.serviceRateConsultTo.setFlagMovementTypeFixed(Boolean.TRUE);			

				this.getDataModelRateMovementToListConsult().setWrappedData(this.serviceRateConsultTo.getListRateMovementTo());
			} else if(this.serviceRateConsultTo.getRateType().intValue() == RateType.TIPO_MOVIMIENTO_PORCENTUAL.getCode()) {
				JSFUtilities.executeJavascriptFunction("PF('cnfwFlagConsultMemory').show();");

				this.serviceRateConsultTo.setFlagMovementTypePercentage(Boolean.TRUE);
				this.getDataModelRateMovementToListConsult().setWrappedData(this.serviceRateConsultTo.getListRateMovementTo());


				
				/**		 STAGGERED 		  **/		
			} else if(this.serviceRateConsultTo.getRateType().intValue() == RateType.ESCALONADO_FIJO.getCode()) {
				JSFUtilities.executeJavascriptFunction("PF('cnfwFlagConsultMemory').show();");
				this.serviceRateConsultTo.setFlagStaggeredFixed(Boolean.TRUE);

				this.getRateServiceScaleToDataModelConsult().setWrappedData(this.serviceRateConsultTo.getListServiceRateScaleTo());
				this.serviceRateConsultTo.setUnit(serviceRateTo.getListServiceRateScaleTo().get(0).getScaleType());
				
				if(serviceRateConsultTo.getIndScaleMovement().equals(GeneralConstants.ONE_VALUE_INTEGER)){
					this.serviceRateConsultTo.setFlagStaggeredMovement(Boolean.TRUE);

					this.getDataModelRateMovementToListConsult().setWrappedData(this.serviceRateConsultTo.getListRateMovementTo());
				}
				else{
					this.serviceRateConsultTo.setFlagStaggeredMovement(Boolean.FALSE);
				}
				

			} else if(this.serviceRateConsultTo.getRateType().intValue() == RateType.ESCALONADO_PORCENTUAL.getCode()) {
				JSFUtilities.executeJavascriptFunction("PF('cnfwFlagConsultMemory').show();");
				this.serviceRateConsultTo.setFlagStaggeredPercentage(Boolean.TRUE);

				if(Validations.validateListIsNotNullAndNotEmpty(this.serviceRateConsultTo.getListServiceRateScaleTo())){
					this.getRateServiceScaleToDataModelConsult().setWrappedData(this.serviceRateConsultTo.getListServiceRateScaleTo());
					this.serviceRateConsultTo.setUnit(serviceRateTo.getListServiceRateScaleTo().get(0).getScaleType());
				}
				
				if(serviceRateConsultTo.getIndScaleMovement().equals(GeneralConstants.ONE_VALUE_INTEGER)){
					this.serviceRateConsultTo.setFlagStaggeredMovement(Boolean.TRUE);

					this.getDataModelRateMovementToListConsult().setWrappedData(this.serviceRateConsultTo.getListRateMovementTo());
				}else{
					this.serviceRateConsultTo.setFlagStaggeredMovement(Boolean.FALSE);
				}
			} else if(this.serviceRateConsultTo.getRateType().intValue() == RateType.ESCALONADO_MIXTO.getCode()) {
				JSFUtilities.executeJavascriptFunction("PF('cnfwFlagConsultMemory').show();");
				this.serviceRateConsultTo.setFlagStaggeredMix(Boolean.TRUE);

				if(Validations.validateListIsNotNullAndNotEmpty(this.serviceRateConsultTo.getListServiceRateScaleTo())){
					this.getRateServiceScaleToDataModelConsult().setWrappedData(this.serviceRateConsultTo.getListServiceRateScaleTo());
					this.serviceRateConsultTo.setUnit(serviceRateTo.getListServiceRateScaleTo().get(0).getScaleType());
				}
				
				if(serviceRateConsultTo.getIndScaleMovement().equals(GeneralConstants.ONE_VALUE_INTEGER)){
					this.serviceRateConsultTo.setFlagStaggeredMovement(Boolean.TRUE);
					this.getDataModelRateMovementToListConsult().setWrappedData(this.serviceRateConsultTo.getListRateMovementTo());

				}
				else{
					this.serviceRateConsultTo.setFlagStaggeredMovement(Boolean.FALSE);
				}
				
				
			}
		}



		/**
		 * Method invoke for show a new Service Rate  .
		 *
		 * @param event the event
		 */
		public void consultBillingServiceRate(ActionEvent event){
			if (event != null) {
				
				ServiceRateTo serviceRateToEvent=  (ServiceRateTo)event.getComponent().getAttributes().get("serviceRate");
				this.serviceRateTo.setServiceRateTo(serviceRateToEvent);
				this.serviceRateTo.disableFlagRateServices();

				JSFUtilities.hideComponent("formAddRate:cnIdFlagConsult");
				JSFUtilities.hideGeneralDialogues();

				if(GeneralConstants.ONE_VALUE_INTEGER.equals(this.serviceRateTo.getIndAmountMinimum())){
					serviceRateTo.setFlagMinimumAmount(true);
				}
				
				if (this.serviceRateTo.getRateType().equals(RateType.FIJO.getCode())) {
					
					this.serviceRateTo.setFlagFixed(Boolean.TRUE);		
					
				} else if(this.serviceRateTo.getRateType().equals(RateType.PORCENTUAL.getCode())) {		
					
					this.serviceRateTo.setFlagPercentage(Boolean.TRUE);					
					
				} else if(this.serviceRateTo.getRateType().equals(RateType.ESCALONADO_FIJO.getCode())) {
					
					this.serviceRateTo.setFlagStaggeredFixed(Boolean.TRUE);					
//					this.serviceRateTo.getRateServiceScaleToDataModel().setWrappedData(this.serviceRateTo.getListServiceRateScaleTo());
					this.getRateServiceScaleToDataModel().setWrappedData(this.serviceRateTo.getListServiceRateScaleTo());
					this.serviceRateTo.setUnit(serviceRateTo.getListServiceRateScaleTo().get(0).getScaleType());
					
					/**Rate with Movement**/
					if(serviceRateTo.getIndScaleMovement().equals(GeneralConstants.ONE_VALUE_INTEGER)){
						this.serviceRateTo.setFlagStaggeredMovement(Boolean.TRUE);
//						this.serviceRateTo.getDataModelRateMovementToList().setWrappedData(this.serviceRateTo.getListRateMovementTo());
						this.getDataModelRateMovementToList().setWrappedData(this.serviceRateTo.getListRateMovementTo());
					}
					
				} else if(this.serviceRateTo.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode())) {
					
					this.serviceRateTo.setFlagStaggeredPercentage(Boolean.TRUE);					
//					this.serviceRateTo.getRateServiceScaleToDataModel().setWrappedData(this.serviceRateTo.getListServiceRateScaleTo());
					this.getRateServiceScaleToDataModel().setWrappedData(this.serviceRateTo.getListServiceRateScaleTo());
					this.serviceRateTo.setUnit(serviceRateTo.getListServiceRateScaleTo().get(0).getScaleType());
					
					/**Rate with Movement**/
					if(serviceRateTo.getIndScaleMovement().equals(GeneralConstants.ONE_VALUE_INTEGER)){		
						this.serviceRateTo.setFlagStaggeredMovement(Boolean.TRUE);
//						this.serviceRateTo.getDataModelRateMovementToList().setWrappedData(this.serviceRateTo.getListRateMovementTo());
						this.getDataModelRateMovementToList().setWrappedData(this.serviceRateTo.getListRateMovementTo());
					}
					
				} else if(this.serviceRateTo.getRateType().equals(RateType.ESCALONADO_MIXTO.getCode())) {
					
					this.serviceRateTo.setFlagStaggeredMix(Boolean.TRUE);					
//					this.serviceRateTo.getRateServiceScaleToDataModel().setWrappedData(this.serviceRateTo.getListServiceRateScaleTo());
					this.getRateServiceScaleToDataModel().setWrappedData(this.serviceRateTo.getListServiceRateScaleTo());
					this.serviceRateTo.setUnit(serviceRateTo.getListServiceRateScaleTo().get(0).getScaleType());
					
					/**Rate with Movement**/
					if(serviceRateTo.getIndScaleMovement().equals(GeneralConstants.ONE_VALUE_INTEGER)){
						this.serviceRateTo.setFlagStaggeredMovement(Boolean.TRUE);
//						this.serviceRateTo.getDataModelRateMovementToList().setWrappedData(this.serviceRateTo.getListRateMovementTo());
						this.getDataModelRateMovementToList().setWrappedData(this.serviceRateTo.getListRateMovementTo());
					}
					
				} else if(this.serviceRateTo.getRateType().equals(RateType.TIPO_MOVIMIENTO_FIJO.getCode())) {
					
					this.serviceRateTo.setFlagMovementTypeFixed(Boolean.TRUE);			
//					this.serviceRateTo.getDataModelRateMovementToList().setWrappedData(this.serviceRateTo.getListRateMovementTo());
					this.getDataModelRateMovementToList().setWrappedData(this.serviceRateTo.getListRateMovementTo());
					
				} else if(this.serviceRateTo.getRateType().equals(RateType.TIPO_MOVIMIENTO_PORCENTUAL.getCode())) {	
					
					this.serviceRateTo.setFlagMovementTypePercentage(Boolean.TRUE);			
//					this.serviceRateTo.getDataModelRateMovementToList().setWrappedData(this.serviceRateTo.getListRateMovementTo());
					this.getDataModelRateMovementToList().setWrappedData(this.serviceRateTo.getListRateMovementTo());
					
				}
				JSFUtilities.executeJavascriptFunction("PF('cnfwFlagConsultMemory').show();");
			}
		}
		
		
		/**
		 * METHOD CALLED FROM CLICK EVENT.
		 *
		 * @throws IllegalArgumentException the illegal argument exception
		 * @throws IllegalAccessException the illegal access exception
		 */
		public void searchMovementTypes() throws IllegalArgumentException, IllegalAccessException{
			log.info("Consulta de Tipos de movimiento por filtro :");		

			MovementType movementTypeSearch = this.serviceRateTo.getMovementTypeSearch();

			/** searching with paramters **/

			List<MovementType> listMovementType=businessLayerCommonFacade.findMovementTypesServiceBean(movementTypeSearch); 

			if (Validations.validateListIsNullOrEmpty(listMovementType)){			
				this.listMovementEmpty=true;
				this.setDataModelMovementTypeList(null);
				return;
			}	
			this.setDataModelMovementTypeList(new GenericDataModel(listMovementType));


		}

		/**
		 * Clear helper search movement types.
		 */
		public void clearHelperSearchMovementTypes(){

			this.serviceRateTo.setMovementTypeSearch(new MovementType());
			this.setDataModelMovementTypeList(null);				
		}
		
		/**
		 * *
		 * Section for Movement Types Rates.
		 *
		 * @param movementType the movement type
		 */
		/**
		 * 
		 * @param movementType
		 */
		public void assignMovementType(MovementType movementType){

			this.serviceRateTo.setMovementTypeSelected(movementType);
			this.serviceRateTo.setCodeMovementType(movementType.getIdMovementTypePk());
			this.serviceRateTo.setDescriptionMovementType(movementType.getMovementName());
			JSFUtilities.setValidViewComponentAndChildrens("frmRateRegister:otpRatesID");
			JSFUtilities.executeJavascriptFunction("PF('dlgMovement').hide();");


		}
		
		/**
		 * Event To Change Tab.
		 *
		 * @param e the e
		 */
		public void onChangeTab(TabChangeEvent e){
			if(!PropertiesConstants.SERVICE_TYPE_MANUAL_PK.equals(serviceTypeSelected)){
				disabledTypeService=Boolean.FALSE;
				loadServiceRateByBillingServices();
			}else{
				disabledTypeService=Boolean.TRUE;
			}
			
			
		}

		/**
		 * Removes the rate to.
		 *
		 * @param event the event
		 */
		public void removeRateTo(ActionEvent event) {

			ServiceRateTo serviceRateTo = (ServiceRateTo) event .getComponent().getAttributes().get("removeName");
			List<ServiceRateTo> serviceRateToList= this.billingServiceTo.getListServiceRateTo();
			serviceRateToList.remove(serviceRateTo);
		}
		
		/**
		 * Este metodo se va a fusionar con el de registrar el servicio.
		 */
		public void beforeServiceRegistrationListener() {
			log.debug("beforeServiceRegistrationListener :");	
			List<ServiceRateTo> listServiceRateTo = billingServiceTo.getListServiceRateTo();
			Boolean stateForSave=Boolean.FALSE;
			if(Validations.validateIsNotNullAndNotEmpty(billingServiceTo.getListServiceRateTo())){
				for (ServiceRateTo serviceRateTo : listServiceRateTo) {
					if (serviceRateTo.getRateStatus().intValue()==RateServiceStatus.PENDING.getCode().intValue()){
						stateForSave=Boolean.TRUE;
						break;
					}
				}
				if(stateForSave){
					showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
							PropertiesUtilities.getMessage(
									PropertiesConstants.CONFIRM_SERVICE_RATE_SAVE,this.billingServiceTo.getServiceCode()));
					JSFUtilities.executeJavascriptFunction("PF('cnfwRateSave').show();");

				}
				else{
					executeAction();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.WARNING_EMPTY_SERVICE_RATE));
					JSFUtilities.executeJavascriptFunction("PF('warningDialog').show();");  

				}
			} 
		}
		
		/**
		 * Save Only Service Rate.
		 *
		 * @throws ServiceException the service exception
		 */
		@LoggerAuditWeb
		public void saveServiceRate() throws ServiceException {
			log.info("saveServiceRate");

			List<ServiceRate> serviceRateList = billingServiceMgmtServiceFacade.prepareRateServicesForSave(billingServiceTo);		
			serviceRateList=billingRateMgmtServiceFacade.saveAllRateService(serviceRateList);
			
			String codeRate="";
			codeRate=BillingUtils.getStringListServiceRate(serviceRateList);
			

			showMessageOnDialog(PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_SERVICE_RATE_SAVE,codeRate));
			JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
		}
		
		/**
		 * *
		 * Valida if Exist Changes in Service Rate.
		 *
		 * @return Boolean
		 */
		public Boolean existChanges(){
			log.debug("existChanges :");

			Boolean flagExistChanges= Boolean.FALSE;

			List<ServiceRateTo> serviceRateToList = this.billingServiceTo.getListServiceRateTo();
			List<ServiceRateTo> serviceRateToListTemp = this.billingServiceTo.getListServiceRateToModify();

			/** Begin validation between service of  rates  **/

			if ( !(serviceRateToList.size()==serviceRateToListTemp.size()) ){
				flagExistChanges=true;
			}else {

				for (ServiceRateTo serviceRateToOrig : serviceRateToList) {

					for (ServiceRateTo serviceRateToTemp : serviceRateToListTemp) {

						if (serviceRateToOrig.getRateCode().equals(serviceRateToTemp.getRateCode())){

							/** first -- compare date end **/
							if (!serviceRateToOrig.getEndEffectiveDate().equals(serviceRateToTemp.getEndEffectiveDate())){
								flagExistChanges=true;
							}

							/** second -- compare status  ***/
							if (!(serviceRateToOrig.getRateStatus().equals(serviceRateToTemp.getRateStatus())) )
							{
								flagExistChanges=true;
							}
							
							if(serviceRateToOrig.getRateType().equals(RateType.ESCALONADO_FIJO.getCode()) ||
									 serviceRateToOrig.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode()) ||
									 serviceRateToOrig.getRateType().equals(RateType.ESCALONADO_MIXTO.getCode())){
								
								if(Validations.validateListIsNotNullAndNotEmpty(serviceRateToTemp.getListServiceRateScaleTo())){
									if (!(serviceRateToOrig.getListServiceRateScaleTo().size()==serviceRateToTemp.getListServiceRateScaleTo().size())){
										flagExistChanges=true;
									}
									else if(serviceRateToTemp.isFlagIsModifyStaggeredFixed()){
										flagExistChanges=true;
									}
									if(  !(serviceRateToOrig.getAccumulate().equals(serviceRateToTemp.getAccumulate()))  ){
										flagExistChanges=true;
									}
								}
							}

							break;

						}
					}

					if (flagExistChanges){
						break;
					}

				}

			}
			return flagExistChanges;
		}
		
		/**
		 * Modificar el serviceRate en la BD.
		 *
		 * @throws ServiceException the service exception
		 */
		@LoggerAuditWeb
		public void modifyServiceRate() throws ServiceException {		

			log.info("modifyServiceRate");
			
			billingRateMgmtServiceFacade.modifyServiceRate(billingServiceTo);

			executeAction();		
			showMessageOnDialog(PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_BILLING_SERVICE_RATE_MODIFY,
							this.billingServiceSelection.getServiceCode()) );
			JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();"); 

			/** reset billing service */
			//	this.billingServiceTo = new BillingServiceTo();

			/** Search All Billing Service by default*/
			this.collectionInstitutionSelected=-1;
			this.serviceTypeSelected=-1;
			this.serviceStatusSelected=-1;
		}
		
		/**
		 * Return if Service Rate is State Pending.
		 *
		 * @param rate the rate
		 * @return true, if successful
		 */
		public boolean disablePendingStatus(ServiceRateTo rate){

			return (RateStateType.PENDING.getCode().equals(rate.getRateStatus()));		

		}
		
		/**
		 * Return if Service Rate is State Register.
		 *
		 * @param rate the rate
		 * @return true, if successful
		 */
		public boolean disableRegisterStatus(ServiceRateTo rate){	

			return (RateStateType.REGISTERED.getCode().equals(rate.getRateStatus()));		

		}
		

		/**
		 * *
		 * Validations before Delete Rate in Modify.
		 *
		 * @param event the event
		 */
		public void beforeDeleteRate(ActionEvent event){
			
			if (event != null) {
				ServiceRateTo serviceRateToTemp = (ServiceRateTo) event.getComponent()
						.getAttributes().get("removeName");
				
				
				if(RateStateType.PENDING.getCode().equals(serviceRateToTemp.getRateStatus())){
					this.billingServiceTo.getListServiceRateToModify().remove(serviceRateToTemp);
				}else{
					executeAction();
					showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_DELETE),
							PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_SERVICE_RATE_DELETE));
					JSFUtilities.executeJavascriptFunction("PF('cnfwDeleteRate').show();");
					this.serviceRateTo.setIdServiceRatePk(serviceRateToTemp.getIdServiceRatePk());  /**save on memory*/
					this.serviceRateTo.setRateCode(serviceRateToTemp.getRateCode());
					this.serviceRateTo.setFlagDeleteServiceRate(true);
				}
				
			}
		}
		

		/**
		 * *
		 * Confirm if Delete Service Rate List Temp to Modify.
		 */
		public void confirmDeleteServiceRateOnListTemp(){

			List<ServiceRateTo> serviceRateListModify =BillingUtils.cloneServiceRateToList(billingServiceTo.getListServiceRateToModify());
			billingServiceTo.setListServiceRateToModify(serviceRateListModify);
//			if (billingServiceTo.getRateServiceToDataModel()!=null){
//				billingServiceTo.getRateServiceToDataModel().setWrappedData(serviceRateListModify);
//			}
			if (this.getRateServiceToDataModel()!=null){
				this.getRateServiceToDataModel().setWrappedData(serviceRateListModify);
			}

			for (ServiceRateTo serviceRateToSeq : serviceRateListModify) {
				if(Validations.validateIsNullOrNotPositive(serviceRateToSeq.getRateCode())){
					serviceRateListModify.remove(serviceRateToSeq);
				}else if (this.serviceRateTo.getRateCode().equals(serviceRateToSeq.getRateCode()) 
						&& (this.serviceRateTo.isFlagDeleteServiceRate())){							
					serviceRateListModify.remove(serviceRateToSeq);
					break ;
				}
			}

		}
		
		
		public void changeRateFixedAmount() {
			if(serviceRateModifyTo.getRateType().equals(RateType.FIJO.getCode())) {
				if( (Validations.validateIsNotNull(selectedServiceRateToModify.getAmountRate()) && selectedServiceRateToModify.getAmountRate().equals(serviceRateModifyTo.getAmountRate()))
						|| (Validations.validateIsNotNull(selectedServiceRateToModify.getRateAmount()) && selectedServiceRateToModify.getRateAmount().equals(serviceRateModifyTo.getAmountRate()))) {
					selectedServiceRateToModify.setFlagIsModifyRateFixed(Boolean.FALSE);
				} else {
					selectedServiceRateToModify.setFlagIsModifyRateFixed(Boolean.TRUE);
					selectedServiceRateToModify.setAmountRate(serviceRateModifyTo.getAmountRate());
				}
			}
		}
		
		/**
		 *  getting service rate commandLink.
		 *
		 * @param event the event
		 */
		public void loadOnModifyRate(ActionEvent event){
			if(event!=null){
				this.selectedServiceRateToModify=(ServiceRateTo)event.getComponent().getAttributes().get("blockInformation");
				serviceRateModifyTo=new ServiceRateTo();
				/**Load*/
				this.serviceRateModifyTo.setIdServiceRatePk(selectedServiceRateToModify.getIdServiceRatePk());  /**save on memory*/
				this.serviceRateModifyTo.setRateCode(selectedServiceRateToModify.getRateCode());
				this.serviceRateModifyTo.setRateType(selectedServiceRateToModify.getRateType());
				this.serviceRateModifyTo.setRateName(selectedServiceRateToModify.getRateName());
				this.serviceRateModifyTo.setCurrencyRate(selectedServiceRateToModify.getCurrencyRate());
				this.serviceRateModifyTo.setEndEffectiveDate(selectedServiceRateToModify.getEndEffectiveDate());
				this.serviceRateModifyTo.setInitialEffectiveDate(selectedServiceRateToModify.getInitialEffectiveDate());
				this.serviceRateModifyTo.setRateStatus(selectedServiceRateToModify.getRateStatus());
				this.serviceRateModifyTo.setIndScaleMovement(selectedServiceRateToModify.getIndScaleMovement());

				cleanModify();
				
				if(selectedServiceRateToModify.getRateType().equals(RateType.FIJO.getCode())) {
					if( Validations.validateIsNotNullAndNotEmpty(selectedServiceRateToModify.getRateAmount()) ){			 
						this.serviceRateModifyTo.setAmountRate(selectedServiceRateToModify.getRateAmount());
					}else if( Validations.validateIsNotNullAndNotEmpty(selectedServiceRateToModify.getAmountRate()) ){
						this.serviceRateModifyTo.setAmountRate(selectedServiceRateToModify.getAmountRate());
					}
					this.serviceRateModifyTo.setFlagFixed(Boolean.TRUE);
				}
				
				/***
				 * Load Rate staggered fixed, percentage and mixed 
				 */
				if(selectedServiceRateToModify.getRateType().equals(RateType.ESCALONADO_FIJO.getCode())||
						selectedServiceRateToModify.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode())||
						selectedServiceRateToModify.getRateType().equals(RateType.ESCALONADO_MIXTO.getCode())){
					if(Validations.validateListIsNotNullAndNotEmpty(selectedServiceRateToModify.getListServiceRateScaleTo())){
						serviceRateModifyTo.setAccumulate(selectedServiceRateToModify.getListServiceRateScaleTo().get(0).getIndAccumulativeScale());
						serviceRateModifyTo.setListServiceRateScaleTo(selectedServiceRateToModify.getListServiceRateScaleTo());
						serviceRateModifyTo.setIndOtherCurrency(selectedServiceRateToModify.getIndOtherCurrency());
						serviceRateModifyTo.setOtherCurrency(selectedServiceRateToModify.getOtherCurrency());
//						serviceRateModifyTo.setRateServiceScaleToDataModel(new RateServiceScaleToDataModel(serviceRateToModify.getListServiceRateScaleTo()));
						this.setRateServiceScaleToDataModelConsult(new RateServiceScaleToDataModel(selectedServiceRateToModify.getListServiceRateScaleTo()));;
						
					}
				}			
				JSFUtilities.executeJavascriptFunction("PF('cnfwRateModifyPanel').show();");
			}

		}
		
		/**
		 * Clean modify.
		 */
		public void cleanModify(){
			
			serviceRateModifyTo.setMinScaleValue(null);
			serviceRateModifyTo.setMaxScaleValue(null);
			serviceRateModifyTo.setAmountRate(null); 
			serviceRateModifyTo.setPercentageRate(null);
			serviceRateModifyTo.setRateAmount(null);		
			serviceRateModifyTo.setCodeMovementType(null);
			serviceRateModifyTo.setDescriptionMovementType(null);
			
				if(serviceRateModifyTo.getRateType().equals(RateType.ESCALONADO_FIJO.getCode()) ||
						serviceRateModifyTo.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode())||
						serviceRateModifyTo.getRateType().equals(RateType.ESCALONADO_MIXTO.getCode())){

					if(Validations.validateListIsNotNullAndNotEmpty(serviceRateModifyTo.getListServiceRateScaleTo())) {
						serviceRateModifyTo.setUnit(serviceRateModifyTo.getListServiceRateScaleTo().get(0).getScaleType());
					}

					/**
					 * Values by Default Rate Type Mixed Staggered
					 */
					serviceRateModifyTo.setIndStaging(GeneralConstants.ZERO_VALUE_INTEGER);
					serviceRateModifyTo.setIndOtherCurrency(GeneralConstants.ZERO_VALUE_INTEGER);
					indStaggeredFixedMix=Boolean.TRUE;
					indStaggeredPercentageMix=Boolean.FALSE;
				}
			
			
			}
		
		/**
		 * Close Windows Dialog Modify.
		 */
		public void closeModalWindow(){
			JSFUtilities.executeJavascriptFunction("PF('cnfwRateModifyPanel').hide();");
		}
		

		
		/**
		 * Return to Windows Rate Modify.
		 */
		public void returnToRateModify(){

			List<ServiceRateTo>  serviceRateToListModify= this.billingServiceTo.getListServiceRateToModify();		
			Boolean flagModifyWarning=false;		

			for (ServiceRateTo serviceRateToSeq : serviceRateToListModify) {
				if(Validations.validateIsNull(this.serviceRateModifyTo.getRateCode())){
					flagModifyWarning=true;
				}else if (this.serviceRateModifyTo.getRateCode().equals(serviceRateToSeq.getRateCode())){   /* first , find object**/
					if (!serviceRateToSeq.getEndEffectiveDate().equals(this.serviceRateTo.getEndEffectiveDate())){  /* second, compare dates  ***/
						flagModifyWarning=true;
						break;
					}
				}
			}


			if(flagModifyWarning){
				showMessageOnDialog(
						PropertiesUtilities
						.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getMessage(
								PropertiesConstants.WARNING_TYPE_SERVICE_RATE_BACK,
								this.billingServiceSelection.getServiceCode()));
				JSFUtilities.executeJavascriptFunction("PF('cnfwConfirmBack').show();");
			}
			else{
				closeModalWindow();
			}
		}
		
		/**
		 * El boton aceptar de la ventana Modal.
		 */
		public void beforeUpdateServiceRateDialog(){ 
			/** getting the list serviceRateTo*/
			List<ServiceRateTo>  serviceRateToListModify= this.billingServiceTo.getListServiceRateToModify();


			for (ServiceRateTo serviceRateToSeq : serviceRateToListModify) {

				if (Validations.validateIsNotNull(serviceRateToSeq)){
					/* If Is New Rate */
					if(Validations.validateIsNullOrNotPositive(serviceRateToSeq.getRateCode())){
						
					}else if (serviceRateToSeq.getRateCode().equals(this.serviceRateModifyTo.getRateCode())){   /* first , find object**/

						if (     !( serviceRateToSeq.getEndEffectiveDate().equals(this.serviceRateModifyTo.getEndEffectiveDate()) ) 
								|| !(serviceRateToSeq.getRateStatus().equals(this.serviceRateModifyTo.getRateStatus()) )            ) {  /* second, set  date  ***/

							serviceRateToSeq.setEndEffectiveDate(this.serviceRateModifyTo.getEndEffectiveDate());
							if( this.serviceRateModifyTo.getRateStatus().equals(RateStateType.REGISTERED.getCode()) ){
								serviceRateToSeq.setRateStatus(this.serviceRateModifyTo.getRateStatus()); 
								serviceRateToSeq.setDescriptionStatus(RateStateType.get(this.serviceRateModifyTo.getRateStatus()).getValue());										
							}
							else if( this.serviceRateModifyTo.getRateStatus().equals(RateStateType.UNLOCKED.getCode()) ){
								if( this.serviceRateModifyTo.getInitialEffectiveDate().before(CommonsUtilities.currentDate()) ){
									serviceRateToSeq.setRateStatus(RateServiceStatus.ACTIVE.getCode());
									serviceRateToSeq.setDescriptionStatus(RateStateType.get(this.serviceRateModifyTo.getRateStatus()).getValue());
									break;
								}
								else if( this.serviceRateModifyTo.getInitialEffectiveDate().after(CommonsUtilities.currentDate()) ){
									serviceRateToSeq.setRateStatus(RateServiceStatus.REGISTER.getCode());
									serviceRateToSeq.setDescriptionStatus(RateStateType.get(this.serviceRateModifyTo.getRateStatus()).getValue());
									break;
								}else // Is the same day 
									serviceRateToSeq.setRateStatus(RateServiceStatus.ACTIVE.getCode());
									serviceRateToSeq.setDescriptionStatus(RateStateType.get(this.serviceRateModifyTo.getRateStatus()).getValue());
									break;
							}
							serviceRateToSeq.setRateStatus(this.serviceRateModifyTo.getRateStatus()); 
							serviceRateToSeq.setDescriptionStatus(RateStateType.get(this.serviceRateModifyTo. getRateStatus()).getValue());
							break;
						}
						
						/**
						 * Third, changes in Rate Staggered
						 */
						if(serviceRateModifyTo.getRateType().equals(RateType.ESCALONADO_FIJO.getCode()) ||
								serviceRateModifyTo.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode()) ||
								serviceRateModifyTo.getRateType().equals(RateType.ESCALONADO_MIXTO.getCode())){
							
							if(serviceRateModifyTo.isFlagIsModifyStaggeredFixed()){
								serviceRateToSeq.setFlagIsModifyStaggeredFixed(serviceRateModifyTo.isFlagIsModifyStaggeredFixed());
								
							}
							
							/**
							 * 
							 */
							serviceRateToSeq.setAccumulate(serviceRateModifyTo.getAccumulate());
						}
						
						
						
					}
				}
			}
			closeModalWindow();
			
			executeAction();
			showMessageOnDialog(PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_ROLL_BACK_BILLING_SERVICE_SEARCH,""));
			JSFUtilities.executeJavascriptFunction("PF('cnfwRateModifyPanel').show();");
			
		}
		
		/**
		 * Checks if is disable status.
		 *
		 * @param rate the rate
		 * @param rateStatus the rate status
		 * @return true, if is disable status
		 */
		public boolean isDisableStatus(ServiceRateTo rate,ParameterTable rateStatus){


			List<ServiceRateTo> serviceRateToList = this.billingServiceTo.getListServiceRateToModify();
			ServiceRateTo serviceRateToTemp= new ServiceRateTo();
			
			if(Validations.validateIsNullOrNotPositive(rate.getRateCode())){
				return true;
			}
			for (ServiceRateTo serviceRateToSeq : serviceRateToList) {
				if(serviceRateToSeq.getRateCode().equals(rate.getRateCode())){
					serviceRateToTemp=serviceRateToSeq;
					break;
				}
			}


			return (RateStateType.CANCELED.getCode().equals(serviceRateToTemp.getRateStatus()) 
					|| RateStateType.EXPIRED.getCode().equals(serviceRateToTemp.getRateStatus()) )
					||
					(  RateStateType.REGISTERED.getCode().equals(serviceRateToTemp.getRateStatus()) && 

							(RateStateType.CANCELED.getCode().equals(rateStatus.getParameterTablePk())
									|| RateStateType.EXPIRED.getCode().equals(rateStatus.getParameterTablePk())						                	
									|| RateStateType.ACTIVED.getCode().equals(rateStatus.getParameterTablePk()) 
									|| RateStateType.UNLOCKED.getCode().equals(rateStatus.getParameterTablePk())
									)
							)			                			 
							|| 
							( RateStateType.ACTIVED.getCode().equals(serviceRateToTemp.getRateStatus()) &&  

									(RateStateType.REGISTERED.getCode().equals(rateStatus.getParameterTablePk())
											|| RateStateType.EXPIRED.getCode().equals(rateStatus.getParameterTablePk())						                	
											|| RateStateType.UNLOCKED.getCode().equals(rateStatus.getParameterTablePk())
											)
									)
									|| 
									( RateStateType.LOCKED.getCode().equals( serviceRateToTemp.getRateStatus()) && 
											(RateStateType.CANCELED.getCode().equals(rateStatus.getParameterTablePk())						                	
													|| RateStateType.EXPIRED.getCode().equals(rateStatus.getParameterTablePk())
													|| RateStateType.REGISTERED.getCode().equals(rateStatus.getParameterTablePk())
													|| RateStateType.ACTIVED.getCode().equals(rateStatus.getParameterTablePk()) 
													) 
											) ;  
		}


		/**
		 * Gets the rate service to data model.
		 *
		 * @return the rateServiceToDataModel
		 */
		public RateServiceToDataModel getRateServiceToDataModel() {
			return rateServiceToDataModel;
		}


		/**
		 * Sets the rate service to data model.
		 *
		 * @param rateServiceToDataModel the rateServiceToDataModel to set
		 */
		public void setRateServiceToDataModel(
				RateServiceToDataModel rateServiceToDataModel) {
			this.rateServiceToDataModel = rateServiceToDataModel;
		}
		
		public void modifyServiceNameListener() {
			
		}
		
		public void referenceRateChangeListener() {
			
		}
		
}
