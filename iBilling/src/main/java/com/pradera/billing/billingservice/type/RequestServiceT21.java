package com.pradera.billing.billingservice.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum RequestServiceT21 {
	PROGRAMADO_LA_PAZ(Integer.valueOf(103),"PROGRAMADO EN LA PAZ"),
	NO_PROGRAMADO_LA_PAZ(Integer.valueOf(152),"NO PROGRAMADO EN LA PAZ"),
	NO_PROGRAMADO_CBBA_SC(Integer.valueOf(437),"NO PROGRAMADO EN COCHABAMBA/SANTA CRUZ"),
	RESUMEN_TENENCIAS_CUI(Integer.valueOf(37),"RESUMEN DE TENENCIAS POR CUI"),
	RELACION_MOVIMIENTOS_CUI(Integer.valueOf(65),"RELACION DE MOVIMIENTOS POR CUI"),
	RELACION_TITULARES(Integer.valueOf(303),"RELACION DE TITULARES"),
	RELACION_TRANSFERENCIAS(Integer.valueOf(152),"RELACION DE TRANSFERENCIAS"),
	EMISION_CERTIFICACIONES_VARIAS(Integer.valueOf(65),"POR EMISION DE CERTIFICACIONES VARIAS");
	
	
	private Integer code;
	private String value;	

	public Integer getCode() {
		return code;
	}
	
	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
			
	private RequestServiceT21(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<RequestServiceT21> list = new ArrayList<RequestServiceT21>();
	
	/** The Constant lookup. */
	public static final Map<Integer, RequestServiceT21> lookup = new HashMap<Integer, RequestServiceT21>();
	static {
		for (RequestServiceT21 s : EnumSet.allOf(RequestServiceT21.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static RequestServiceT21 get(Integer code) {
		return lookup.get(code);
	}
	
	
	
	
	
}
