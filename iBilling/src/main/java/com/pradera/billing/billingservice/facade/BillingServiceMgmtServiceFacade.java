package com.pradera.billing.billingservice.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

//import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

import com.pradera.billing.billing.facade.BillingProcessedServiceFacade;
import com.pradera.billing.billingcollection.service.ConsultCollectionMgmtServiceBean;
import com.pradera.billing.billingrate.facade.BillingRateMgmtServiceFacade;
import com.pradera.billing.billingrate.service.BillingRateMgmtServiceBean;
import com.pradera.billing.billingrate.to.ServiceRateTo;
import com.pradera.billing.billingservice.service.BillingServiceMgmtServiceBean;
import com.pradera.billing.billingservice.to.BillingRegServiceTo;
import com.pradera.billing.billingservice.to.BillingServiceTo;
import com.pradera.billing.common.facade.BusinessLayerCommonFacade;
import com.pradera.billing.common.utils.SequenceGeneratorCode;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.billing.BillingCorrelative;
import com.pradera.model.billing.BillingEconomicActivity;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.RequestService;
import com.pradera.model.billing.ServiceRate;
import com.pradera.model.billing.type.RateStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.BillingCorrelativeType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingServiceMgmtServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class BillingServiceMgmtServiceFacade {

	/** The billing service mgmt service bean. */
	@EJB
	private BillingServiceMgmtServiceBean billingServiceMgmtServiceBean;
	
	/** The transaction registry. */
	@javax.annotation.Resource
    TransactionSynchronizationRegistry 	transactionRegistry;
	
	/** The consult collection mgmt service bean. */
	@EJB
	private ConsultCollectionMgmtServiceBean consultCollectionMgmtServiceBean;	
	
	/** The sequence generator code. */
	@EJB
	SequenceGeneratorCode  				sequenceGeneratorCode;
	
	/** The billing processed service facade. */
	@EJB
	BillingProcessedServiceFacade 		billingProcessedServiceFacade;	
	
	/** The billing rate mgmt service facade. */
	@EJB
	BillingRateMgmtServiceFacade		billingRateMgmtServiceFacade;
	
	/** The billing rate mgmt service bean. */
	@EJB
	BillingRateMgmtServiceBean			billingRateMgmtServiceBean;
	
	/** The message configuration. */
	@Inject
	@InjectableResource(location = "Messages_es.properties")
	Properties messageConfiguration;
	
	/** The business layer common facade. */
	@EJB
	BusinessLayerCommonFacade		businessLayerCommonFacade;
	
	/**
	 * Instantiates a new billing service mgmt service facade.
	 */
	public BillingServiceMgmtServiceFacade() {
					
	}
	
	/**
	 * Save billing service.
	 *
	 * @param billingService Entity Billing Service
	 * @return the billing service
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	public BillingService saveBillingService(BillingService billingService)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());	
	    billingService.setRegistryUser(loggerUser.getUserName());
	    billingService.setRegistryDate(loggerUser.getAuditTime());
	    
	    BillingCorrelative billingCorrelative = new BillingCorrelative();
	    billingCorrelative.setCorrelativeCD(BillingCorrelativeType.SERVICE_CODE_COR.getValue());
	    
	    Long codeBillingService = 0L ;
	    try {
			
	    	codeBillingService = sequenceGeneratorCode.getLastSequenceGeneratorCode(billingCorrelative);
	    	
		} catch (Exception e) {
			throw new ServiceException(ErrorServiceType.BILLING_SERVICE_CODE_ERROR);
		}
	     
	    
	    billingService.setServiceCode(codeBillingService.toString());	    
		billingServiceMgmtServiceBean.saveBillingService(billingService);
		
		return billingService;
	}
	
	/**
	 * Save billing service req
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	public RequestService saveServiceReq(RequestService billingService)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());	
	    billingService.setLastModifyUser(loggerUser.getUserName());
	    billingService.setLastModifyDate(loggerUser.getAuditTime());
		
	    billingServiceMgmtServiceBean.saveServiceReq(billingService);
		return billingService;
	}
	
	/**
	 * Save billing service with rates.
	 *
	 * @param billingService Entity
	 * @param billingServiceTo Object To with List Rates
	 * @return the billing service
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	public BillingService saveBillingServiceWithRates(BillingService billingService,BillingServiceTo billingServiceTo)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());	
	    billingService.setRegistryUser(loggerUser.getUserName());
	    billingService.setRegistryDate(loggerUser.getAuditTime());
	    
		try {
			saveBillingService(billingService);
			billingServiceTo.setServiceCode(billingService.getServiceCode());
			billingServiceTo.setIdBillingServicePk(billingService.getIdBillingServicePk());
			
			if(Validations.validateListIsNotNullAndNotEmpty(billingServiceTo.getListServiceRateTo())){
				List<ServiceRate> listServiceRate=prepareRateServicesForSave(billingServiceTo);
				listServiceRate= billingRateMgmtServiceFacade.saveAllRateService(listServiceRate);
				
				billingService.setServiceRates(listServiceRate);
			}
			
		} catch (Exception e) {
			
			if(e instanceof ServiceException){
				if( ErrorServiceType.BILLING_SERVICE_CODE_ERROR.equals(((ServiceException) e).getErrorService())){
					throw new ServiceException(ErrorServiceType.BILLING_SERVICE_CODE_ERROR);
				}
			}
			throw new ServiceException(ErrorServiceType.BILLING_SERVICE_CREATE_ERROR);
		}

		
		return billingService;
	}
	

    @ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)
	public void saveEconomicActivity(BillingService modelBillingService,
			List<BillingEconomicActivity> listBillingEconomicActivity) {
		
		for (BillingEconomicActivity billingEconomicActivity : listBillingEconomicActivity) {
			billingEconomicActivity.setBillingService(modelBillingService);
			billingServiceMgmtServiceBean.create(billingEconomicActivity);
		}
	}

	/**
	 * Get List Billing Service by Parameters.
	 *
	 * @param billingServiceTo the billing service to
	 * @return List<BillingService>
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<BillingService> getListBillingServiceWithSourceByParameters(BillingServiceTo billingServiceTo) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return billingServiceMgmtServiceBean.getListBillingServiceByParameters(billingServiceTo);
	}
	
	/**
	 * Get List Billing Service by Parameters.
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<RequestService> getListBillingReqService(BillingRegServiceTo billingServiceTo) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return billingServiceMgmtServiceBean.getListBillingReqService(billingServiceTo);
	}
	
	/**
	 * Gets the billing service list.
	 *
	 * @param billingServiceTo the billing service to
	 * @return the billing service list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<BillingService> getBillingServiceList(BillingServiceTo billingServiceTo) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());	 
		return billingServiceMgmtServiceBean.getListParameterBillingService(billingServiceTo);
	}
	
	/**
	 * Gets the billing service list automatic and eventual.
	 *
	 * @param billingServiceTo the billing service to
	 * @return the billing service list automatic and eventual
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<BillingService> getBillingServiceListAutomaticAndEventual(BillingServiceTo billingServiceTo) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());	 
		return billingServiceMgmtServiceBean.getBillingServiceListAutomaticAndEventual(billingServiceTo);
	}
	
	
	/**
	 * Update billing service.
	 *
	 * @param billingService the billing service
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_MODIFICATION)
	public void updateBillingService(BillingService billingService) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());	 
		billingServiceMgmtServiceBean.update(billingService);
	}
	
	
	/**
	 * Un lock billing service.
	 *
	 * @param billingService the billing service
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_MODIFICATION)
	public void unLockBillingService(BillingService billingService) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeUnblock());	 
	    billingService.setUnlockUser(loggerUser.getUserName());
	    billingService.setUnlockDate(loggerUser.getAuditTime());
		billingServiceMgmtServiceBean.update(billingService);
	}
	
	
	/**
	 * Lock billing service.
	 *
	 * @param billingService the billing service
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_MODIFICATION)
	public void lockBillingService(BillingService billingService) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeBlock());
	    billingService.setLockUser(loggerUser.getUserName());
	    billingService.setLockDate(loggerUser.getAuditTime());
		billingServiceMgmtServiceBean.update(billingService);
	}
	
	
	/**
	 * Cancel billing service.
	 *
	 * @param billingService the billing service
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_MODIFICATION)
	public void cancelBillingService(BillingService billingService)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeCancel());
	    billingService.setCancelUser(loggerUser.getUserName());
	    billingService.setCancelDate(loggerUser.getAuditTime());
	    
	    // CANCELED Billing Service
	    billingServiceMgmtServiceBean.update(billingService);
	    
	    List<ServiceRate> 	serviceRateList =  new ArrayList<ServiceRate>();
	    
	    // Search and CANCELED all Service Rate by IdBillingService 
	    serviceRateList = billingRateMgmtServiceBean.getAllServicesRateToActive(billingService.getIdBillingServicePk());
	    
	    billingServiceMgmtServiceBean.updateListServiceRate(serviceRateList, RateStateType.CANCELED.getCode());
	    /*if( Validations.validateListIsNotNullAndNotEmpty(serviceRateList) ){	    	
	    	// Cancel Service Rate
			for( ServiceRate serviceRate : serviceRateList ){
				
				serviceRate.setRateState(RateStateType.CANCELED.getCode());				
				billingServiceMgmtServiceBean.update(serviceRate); 
			}			
	    }*/	    
	}
	
	
	/**
	 * Active billing service.
	 *
	 * @param billingService the billing service
	 * @throws ServiceException the service exception
	 */
	// ACTIVE SERVICE
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_MODIFICATION)
	public void activeBillingService(BillingService billingService)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify()); 	    
	    billingServiceMgmtServiceBean.update(billingService);
	}
	
	/**
	 * Gets the list parameter table exclude pks service bean.
	 *
	 * @param filters the filters
	 * @return the list parameter table exclude pks service bean
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getListParameterTableExcludePksServiceBean(List<ParameterTableTO> filters) throws ServiceException{
		return billingServiceMgmtServiceBean.getListParameterTableExcludePksServiceBean(filters);
	}
	
	
	/**
	 * Gets the parameter table by id.
	 *
	 * @param parameterTableId the parameter table id
	 * @return the parameter table by id
	 */
	public ParameterTable  getParameterTableById(Integer parameterTableId){
		return billingServiceMgmtServiceBean.getParameterTableById(parameterTableId);
	}
	
	/**
	 * Prepare ServiceRate from BillingService, for Save.
	 *
	 * @param billingServiceTo the billing service to
	 * @return the list
	 */
	public List<ServiceRate> prepareRateServicesForSave(BillingServiceTo billingServiceTo){
		return billingServiceMgmtServiceBean.prepareRateServicesForSave(billingServiceTo);
	}
	
	/**
	 * Prepare ServiceRate from BillingService, for Save.
	 *
	 * @param idBillingServicePk the id billing service pk
	 * @param listServiceRateTo the list service rate to
	 * @return the list
	 */
	public List<ServiceRate> prepareServiceRate(Long idBillingServicePk, List<ServiceRateTo> listServiceRateTo){
		return billingServiceMgmtServiceBean.prepareServiceRate(idBillingServicePk,listServiceRateTo);
	}
	
	/**
	 * Gets the list parameter billing service.
	 *
	 * @param filter the filter
	 * @return list of BillingService
	 * @throws ServiceException the service exception
	 */
	public List<BillingService> getListParameterBillingService(BillingServiceTo filter) throws ServiceException{
		return billingServiceMgmtServiceBean.getListParameterBillingService(filter);
	}
	
	

	
	
	/**
	 * Gets the billing service by pk.
	 *
	 * @param id the id
	 * @return the billing service by pk
	 */
	public BillingService getBillingServiceByPk(Long id){
		return  billingServiceMgmtServiceBean.find(BillingService.class, id);
	}
	
	
	
	/**
	 * getBillingService for search by code.
	 *
	 * @param serviceTo the service to
	 * @return BillingService object
	 */
	public BillingService getBillingService(BillingServiceTo serviceTo) {
		
		BillingService billingService=null;
		
		if (Validations.validateIsNotNull(serviceTo.getServiceCode())){
			try {
				List<BillingService> listBillingServices= billingServiceMgmtServiceBean.getListParameterBillingService(serviceTo);
				if(Validations.validateListIsNotNullAndNotEmpty(listBillingServices))
				{
					billingService= listBillingServices.get(0);
				}
				
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		return billingService;
	}
	
	/**
	 * get BillingService.
	 *
	 * @param billingServiceTo the billing service to
	 * @return BillingService object
	 */
	public BillingService getBillingServiceWithServiceRate(BillingServiceTo billingServiceTo){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());	
	    
		BillingService billingService=null;
		billingService=billingServiceMgmtServiceBean.getBillingServiceWithServiceRate(billingServiceTo);
		 
		
		if(Validations.validateIsNotNull(billingService)){
			List<ServiceRate> listServiceRates=billingService.getServiceRates();
			if (Validations.validateListIsNotNullAndNotEmpty(listServiceRates)){
				listServiceRates.size();
			}
			
		}
					
		return billingService;
	}
	
	/**
	 * get BillingService.
	 */
	public List<BillingService> getBillingServiceReq() {
//		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
//	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());		    
		List<BillingService> billingService = null;
		billingService = billingServiceMgmtServiceBean.getBillingServiceReq();		
									
		return billingService;
	}
	
	/**
	 * Find BillingService with ServiceRate.
	 *
	 * @param billingServiceTo the billing service to
	 * @return the billing service with service rateby date
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public BillingService getBillingServiceWithServiceRatebyDate(BillingServiceTo billingServiceTo){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());	
	    
		BillingService billingService=null;
		billingService=billingServiceMgmtServiceBean.getBillingServiceWithServiceRatebyDate(billingServiceTo);
		
		if(Validations.validateIsNotNull(billingService)){
			List<ServiceRate> listServiceRates=billingService.getServiceRates();
			if (Validations.validateListIsNotNullAndNotEmpty(listServiceRates)){
				listServiceRates.size();
			}
			
		} 
					
		return billingService;
	}
	
	
	/**
	 * 	FOR COLLECTION	*.
	 *
	 * @param billingServiceTo the billing service to
	 * @return the billing service with service rate for collection
	 */
	public BillingService getBillingServiceWithServiceRateForCollection(BillingServiceTo billingServiceTo){
		BillingService billingService=null;
		billingService=billingServiceMgmtServiceBean.getBillingServiceWithServiceRateForCollection(billingServiceTo);   
		
		List<ServiceRate> listServiceRates=billingService.getServiceRates();
		if (Validations.validateListIsNotNullAndNotEmpty(listServiceRates)){
			listServiceRates.size();
		}
					
		return billingService;
	}
	
	/**
	 * Gets the remote billing service.
	 *
	 * @param billingServiceTo the billing service to
	 * @return the remote billing service
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public  List<BillingService> getRemoteBillingService(BillingServiceTo billingServiceTo){		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
	    
		List<BillingService> billingService = billingServiceMgmtServiceBean.findRemoteBillingService(billingServiceTo);
		
		
		return billingService;
	}
	

	
	
	/**
	 * Find billing service by base collection.
	 *
	 * @param baseCollectioId the base collectio id
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<BillingService> findBillingServiceByBaseCollection(Integer baseCollectioId ) throws ServiceException{
		
		List<BillingService> billingServiceList= billingServiceMgmtServiceBean.findBillingServiceByBaseCollection(baseCollectioId);
		
		if (Validations.validateListIsNotNullAndNotEmpty(billingServiceList)){			
			for (BillingService billingService : billingServiceList) {
				List<ServiceRate> listServiceRate=billingService.getServiceRates();
				if (Validations.validateListIsNotNullAndNotEmpty(listServiceRate)){					
					listServiceRate.size();
				}
			}
		}
		
		return billingServiceList;
	}
	
	
	/**
	 * Find billing service by calculation period.
	 *
	 * @param baseCollectioId the base collectio id
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<BillingService> findBillingServiceByCalculationPeriod(Integer baseCollectioId ) throws ServiceException{
		
		List<BillingService> billingServiceList= billingServiceMgmtServiceBean.findBillingServiceByBaseCollection(baseCollectioId);
		
		if (Validations.validateListIsNotNullAndNotEmpty(billingServiceList)){			
			for (BillingService billingService : billingServiceList) {
				List<ServiceRate> listServiceRate=billingService.getServiceRates();
				if (Validations.validateListIsNotNullAndNotEmpty(listServiceRate)){					
					listServiceRate.size();
				}
			}
		}
		
		return billingServiceList;
	}
	

	
	/**
	 * Find all billing service.
	 *
	 * @param billingServiceTo the billing service to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<BillingService>  findAllBillingService(BillingServiceTo billingServiceTo ) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());	
		return billingServiceMgmtServiceBean.findAllBillingService(billingServiceTo);
	}
	

	/**
	 * Find remote billing service.
	 *
	 * @param billingServiceTo the billing service to
	 * @return the list
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<BillingService>  findRemoteBillingService(BillingServiceTo billingServiceTo){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());		
		
		return billingServiceMgmtServiceBean.findRemoteBillingService(billingServiceTo);
	}
	
	
	/**
	 * Find service rate.
	 *
	 * @param billingServiceSelection the billing service selection
	 * @return the list
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<ServiceRate> findServiceRate(BillingService billingServiceSelection){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());	
	    
	    List<ServiceRate> serviceRateList = null ;
	    
		try {
			
		 serviceRateList = billingRateMgmtServiceFacade.getServicesRateByAllBillingService(billingServiceSelection.getIdBillingServicePk());
		 
		} catch (ServiceException e) {
			e.printStackTrace();
		}		
		
		return serviceRateList;
	}
	
	
	
	/**
	 * Delete billing service.
	 *
	 * @param billingServiceSelection the billing service selection
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public void deleteBillingService(BillingService billingServiceSelection) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());	
	    
		List<ServiceRate> serviceRateList = null ;	
		
		if (Validations.validateIsNotNull(billingServiceSelection)) {			 
			
			serviceRateList = findServiceRate(billingServiceSelection);
			
			if(Validations.validateListIsNotNullAndNotEmpty(serviceRateList)){
				
				/**		REMOVE ALL RELATION WITH SERVICE RATE	**/
				for( ServiceRate serviceRate : serviceRateList ){					
					 
					
					billingRateMgmtServiceFacade.removeServiceRateAll(serviceRate);
					
				}		
			}
			
			
			billingServiceMgmtServiceBean.delete(BillingService.class, billingServiceSelection.getIdBillingServicePk());
		}
	
	}
	
	
	// Find All Service Rate
	
	/**
	 * Find all service rate.
	 *
	 * @param billingServiceId the billing service id
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<ServiceRate> findAllServiceRate(Long billingServiceId) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());	
		
		List<ServiceRate> list =  billingServiceMgmtServiceBean.getAllServiceRate(billingServiceId);
	    
		return list;
	}


	/**
	 * Actived billing services register actived.
	 *
	 * @param idBillingService the id billing service
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public void activedBillingServicesRegisterActived(Long idBillingService){
		
		 
			LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeActivate());
		 
			    
	    try {
	    	 
		    List<BillingService> billingServiceList =  null ;
		    List<ServiceRate> 		serviceRateList =  new ArrayList<ServiceRate>();
		   
	    
		//  BOTON - do Search all BillingService whit state Register		    
		    if(Validations.validateIsNotNullAndNotEmpty(idBillingService)){
		    	 billingServiceList = billingServiceMgmtServiceBean.getBillingServicesRegisterActived(null, idBillingService);
		    }  // BACHERO
		    else {
		    	Date 	date 	= 	CommonsUtilities.currentDate();
		    	billingServiceList = billingServiceMgmtServiceBean.getBillingServicesRegisterActived(date, idBillingService);
		    }
		    
		    
		    
		    if(Validations.validateListIsNotNullAndNotEmpty(billingServiceList)){			
		    	for( BillingService billingServiceTemp : billingServiceList ){	
		    		
		    		// Active Billing Service
		    		billingServiceTemp.setServiceState(PropertiesConstants.SERVICE_STATUS_ACTIVE_PK);
		    		billingServiceMgmtServiceBean.update(billingServiceTemp);				    		
				
					//  Search all ServicesRate
					serviceRateList = billingRateMgmtServiceBean.getServicesRateStateRegister(billingServiceTemp.getIdBillingServicePk(),null);
								
					if(Validations.validateListIsNotNullAndNotEmpty(serviceRateList)){
						// Active Service Rate
						for( ServiceRate serviceRate : serviceRateList ){
							serviceRate.setRateState(RateStateType.ACTIVED.getCode());
							
							billingServiceMgmtServiceBean.update(serviceRate); 
							
						}
					}
					
				}
		    }
	
	    } catch (ServiceException e) {
			e.printStackTrace();
		}	
	    
	}
	
	

	    /**
    	 * Expired billing services actived lock.
    	 *
    	 * @param date the date
    	 * @throws ServiceException the service exception
    	 */
    	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
		@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
		public void expiredBillingServicesActivedLock(Date date) throws ServiceException{
		    	
			    List<BillingService> billingServiceList =  null ;
			    List<BillingService> billingServiceListToUpdate =  null ;
			    List<ServiceRate> 		serviceRateList =  new ArrayList<ServiceRate>();
		    
			//  Search all BillingService whit state Active and Lock
			    billingServiceList = billingServiceMgmtServiceBean.getBillingServicesActivedLock(null);
				
			    if(Validations.validateListIsNotNullAndNotEmpty(billingServiceList)){
			    	
			    	for( BillingService billingServiceTemp : billingServiceList ){						
						//  Search all ServicesRate
						serviceRateList = billingRateMgmtServiceBean.getServicesRateActivedLock(billingServiceTemp.getIdBillingServicePk(),date);
									
						if(Validations.validateListIsNotNullAndNotEmpty(serviceRateList)){
							
							for( ServiceRate serviceRate : serviceRateList ){
								// Update State to Expired
								serviceRate.setRateState(RateStateType.EXPIRED.getCode());								
							}
						} 						
					}
			    }			    
			    
			    
			    billingServiceListToUpdate = billingServiceMgmtServiceBean.getBillingServicesActivedLock(date);
			    
			    if(Validations.validateListIsNotNullAndNotEmpty(billingServiceListToUpdate)){
				  
			    	for( BillingService billingService : billingServiceListToUpdate ){
				    	billingService.setServiceState(PropertiesConstants.SERVICE_STATUS_EXPIRED_PK);
				    }
			    }
		    	
		    
		}
	

		/**
		 * getBillingServiceByProcessedServicePk for search by code.
		 *
		 * @param ProcessedServicePk the processed service pk
		 * @return BillingService object
		 */
	    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)	
		public BillingService getBillingServiceByProcessedServicePk(Long ProcessedServicePk) {
			
			BillingService billingService = billingServiceMgmtServiceBean.getBillingServiceByProcessedServicePk(ProcessedServicePk);					
			return billingService;
		} 
		
	    
	    /**
		 * Gets the billing service by service code.		 
		 */
	    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)	
		public BillingService getBillingServiceByServiceCode(Long serviceCode) {			
			BillingService billingService = billingServiceMgmtServiceBean.getBillingServiceByServiceCode(serviceCode);					
			return billingService;
		}
	    
	    
	    public void updateStateRequest(RequestService requestServiceSelection) {
	    	billingServiceMgmtServiceBean.updateStateRequest(requestServiceSelection);
	    }
	    
	    public BigDecimal getRateForRequestService(RequestService requestService) {
	    	BigDecimal rate = null;
	    	rate = billingServiceMgmtServiceBean.getRateForRequestService(requestService);
	    	return rate;
	    }

	    
	    public List<BillingEconomicActivity> getBillingEconomicActivityByBillingServicePk(Long idBillingServicePk) {
	    	return billingServiceMgmtServiceBean.getBillingEconomicActivityByBillingServicePk(idBillingServicePk);
	    }
	    
}
