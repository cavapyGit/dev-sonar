package com.pradera.billing.billingservice.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.billing.billingrate.to.ServiceRateTo;
import com.pradera.billing.billingservice.to.BillingRegServiceTo;
import com.pradera.billing.billingservice.to.BillingServiceTo;
import com.pradera.billing.billingservice.type.RateServiceStatus;
import com.pradera.billing.common.utils.BillingUtils;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.billing.BillingEconomicActivity;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.RequestService;
import com.pradera.model.billing.ServiceRate;
import com.pradera.model.billing.type.RateStateType;
import com.pradera.model.billing.type.RateType;
import com.pradera.model.generalparameter.ParameterTable;


// TODO: Auto-generated Javadoc
/**
 * The class BillingServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */

@Stateless
@Performance
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class BillingServiceMgmtServiceBean extends CrudDaoServiceBean {


	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;

	/**
	 * Instantiates a new billing service mgmt service bean.
	 */
	public BillingServiceMgmtServiceBean()  {
	
	}
	
	
	/**
	 * Save billing service.
	 *
	 * @param billingService the billing service
	 * @return the billing service
	 */
	public BillingService saveBillingService (BillingService billingService){
		this.create(billingService);		
		return billingService;
	}
	
	/**
	 * Save billing service req
	 */
	public RequestService saveServiceReq (RequestService billingService){
		this.create(billingService);		
		return billingService;
	}
		
	/**
	 * Gets the list parameter table service bean.
	 *
	 * @param filter the filter
	 * @return the list parameter table service bean
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getListParameterTable(ParameterTableTO filter) throws ServiceException	{
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select P.parameterTablePk,P.parameterName, P.indicator4 From ParameterTable P ");
		sbQuery.append(" Where 1 = 1 ");

		if(Validations.validateIsNotNull(filter.getState())){
			sbQuery.append(" And P.parameterState = :statePrm");
		}

		if(Validations.validateIsNotNull(filter.getParameterTablePk())){
			sbQuery.append(" And P.parameterTablePk = :parameterTablePkPrm");
		}

		if(Validations.validateIsNotNull(filter.getMasterTableFk())){
			sbQuery.append(" And P.masterTable.masterTablePk = :masterTableFkPrm");
		}

		if(Validations.validateIsNotNull(filter.getParameterTableCd())){
			sbQuery.append(" And P.parameterTableCd = :parameterTableCdPrm");
		}
				
    	Query query = em.createQuery(sbQuery.toString());  

    	if(Validations.validateIsNotNull(filter.getState())){
        	query.setParameter("statePrm", filter.getState());
		}

		if(Validations.validateIsNotNull(filter.getParameterTablePk())){
	    	query.setParameter("parameterTablePkPrm", filter.getParameterTablePk());
		}

		if(Validations.validateIsNotNull(filter.getMasterTableFk())){
	    	query.setParameter("masterTableFkPrm", filter.getMasterTableFk());
		}

		if(Validations.validateIsNotNull(filter.getParameterTableCd())){
	    	query.setParameter("parameterTableCdPrm", filter.getParameterTableCd());
		}
		List<Object> objectList = query.getResultList();
		List<ParameterTable> parameterTableList = new ArrayList<ParameterTable>();
		for(int i=0;i<objectList.size();++i){			
			//Getting Results from data base
			Object[] sResults = (Object[])objectList.get(i);
			ParameterTable parameterTable = new ParameterTable();
			parameterTable.setParameterTablePk((Integer) sResults[0]);
			parameterTable.setParameterName((String)sResults[1]);
			parameterTable.setIndicator4((Integer) sResults[2]);
			parameterTableList.add(parameterTable);
		}
		return parameterTableList;
	}
	
	
	/**
	 * Gets the list parameter table exclude pks service bean.
	 *
	 * @param filters the filters
	 * @return the list parameter table exclude pks service bean
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getListParameterTableExcludePksServiceBean(List<ParameterTableTO> filters)  throws ServiceException	{
		StringBuilder sb = new StringBuilder();
		List<ParameterTable> parameterTableList = new ArrayList<ParameterTable>();
		ParameterTable parameter;
		
		sb.append("	  Select 	   									"
						+ "			   P.parameterTablePk,		"
						+ "			   P.parameterName			"
						+ "		from										"
						+ "			   ParameterTable P			" );
		
		sb.append(" Where 1 = 1 ");
		
		/**filtering all parameters table**/
		ParameterTableTO parameterTableTO = filters.get(0);
		if(Validations.validateIsNotNullAndPositive(parameterTableTO.getMasterTableFk())){
			sb.append(" And P.masterTable.masterTablePk = :masterTableFk ");
		}
		
		int numParam=0;
		for (ParameterTableTO paramTableTO : filters) {
			if(Validations.validateIsNotNullAndPositive(paramTableTO.getParameterTablePk())){
			sb.append(" And P.parameterTablePk != :parameterTablePk"+numParam);
			}
			numParam++;
		}
		
		Query query = em.createQuery(sb.toString());  
		
		
		if(Validations.validateIsNotNullAndPositive(parameterTableTO.getMasterTableFk())){
			query.setParameter("masterTableFk", parameterTableTO.getMasterTableFk());
		}
		
		numParam=0;
		for (ParameterTableTO paramTableTO : filters) {
			if(Validations.validateIsNotNullAndPositive(paramTableTO.getParameterTablePk())){
				query.setParameter("parameterTablePk"+numParam, paramTableTO.getParameterTablePk());
			}
			numParam++;
		}
		
		
		List<Object> objectList = query.getResultList();
		
		for (int i = 0; i < objectList.size(); i++) {
			Object[] obj =  (Object[]) objectList.get(i);
			
			parameter = new ParameterTable();
			
			parameter.setParameterTablePk((Integer)obj[0]);
			parameter.setParameterName((String)obj[1]);
			
			parameterTableList.add(parameter);
		}
		return parameterTableList;
	}

	/**
	 * Gets the list parameter billing service to.
	 *
	 * @param filter the filter
	 * @return the list parameter billing service to
	 * @throws ServiceException the service exception
	 */
	public List<BillingServiceTo> getListParameterBillingServiceTo(BillingServiceTo filter) throws ServiceException {

		  StringBuilder sbQuery = new StringBuilder();
		  sbQuery.append(" select bs.id_billing_service_pk, bs.service_name, bs.service_code, ");
			sbQuery.append(" ( select pt.description from parameter_table pt where bs.service_type=pt.parameter_table_pk ) as descriptionServiceType , " );
			sbQuery.append(" ( select pt.description from parameter_table pt where bs.service_state=pt.parameter_table_pk ) as descriptionStateBillingService , " );
			sbQuery.append(" ( select pt.description from parameter_table pt where bs.entity_collection=pt.parameter_table_pk) as descriptionCollectionEntity " );
			sbQuery.append(" ( select pt.description from parameter_table pt where bs.base_collection=pt.parameter_table_pk ) as descriptionBaseCollection , " );
			sbQuery.append(" ( select pt.description from parameter_table pt where bs.collection_period=pt.parameter_table_pk ) as descriptionCollectionPeriod , " );
			sbQuery.append(" ( select pt.description from parameter_table pt where bs.client_service_type=pt.parameter_table_pk ) as descriptionClientServiceType  " );
			sbQuery.append(" from billing_service bs " );		
			sbQuery.append(" Where 1 = 1 " );
		  
		  if(Validations.validateIsNotNullAndNotEmpty(filter.getStateService())){
		   sbQuery.append(" AND bs.service_state= :serviceStatePrm");
		  } 
		  
		  if(Validations.validateIsNotNullAndNotEmpty(filter.getServiceType())){
		   sbQuery.append(" AND bs.service_type= :serviceTypePrm");
		  } 
		  if(Validations.validateIsNotNullAndNotEmpty(filter.getServiceCode())){
		   sbQuery.append(" AND service_code= :serviceCodePrm");
		  } 

		  if(Validations.validateIsNotNullAndNotEmpty(filter.getEntityCollection())){
		   sbQuery.append(" AND entity_collection= :entityCollectionPrm");
		  } 
		  if(Validations.validateIsNotNullAndNotEmpty(filter.getBaseCollection())){
		   sbQuery.append(" AND base_collection= :baseCollectionPrm");
		  } 
		  
		  Query query = em.createNativeQuery(sbQuery.toString());
		     
		  if(Validations.validateIsNotNullAndNotEmpty(filter.getStateService())){
		      query.setParameter("serviceStatePrm", filter.getStateService());
		  } 
		  
		  if(Validations.validateIsNotNullAndNotEmpty(filter.getServiceType())){
		   query.setParameter("serviceTypePrm", filter.getServiceType());
		  } 
		  
		  if(Validations.validateIsNotNullAndNotEmpty(filter.getServiceCode())){
		   query.setParameter("serviceCodePrm", filter.getServiceCode());
		  } 

		  if(Validations.validateIsNotNullAndNotEmpty(filter.getEntityCollection())){
		   query.setParameter("entityCollectionPrm", filter.getEntityCollection());
		  } 
		  
		  if(Validations.validateIsNotNullAndNotEmpty(filter.getBaseCollection())){
		   query.setParameter("baseCollectionPrm", filter.getBaseCollection());
		  } 

		  
		  List<Object[]>  objectList = query.getResultList();
		  
		  List<BillingServiceTo> billingServiceToList = new ArrayList<BillingServiceTo>();
		  for(int i=0;i<objectList.size();++i){   
		   //Getting Results from data base
		   BillingServiceTo billingService = new BillingServiceTo();
		   Object[] sResults = (Object[])objectList.get(i);
		 
		   billingService.setIdBillingServicePk(Long.parseLong(sResults[0].toString()));

		   billingService.setServiceName(sResults[1].toString());
		   billingService.setServiceCode( sResults[2].toString());
		   billingService.setDescriptionServiceType( sResults[3].toString());
		   billingServiceToList.add(billingService);
		  }
		  return billingServiceToList;
	 }
	
	
	/**
	 * Gets the parameter table by id.
	 *
	 * @param parameterTableId the parameter table id
	 * @return the parameter table by id
	 */
	public ParameterTable getParameterTableById(Integer parameterTableId){
		return find(ParameterTable.class, parameterTableId);
	}
	
	/**
	 * Prepare rate services for save.
	 *
	 * @param billingServiceTo the billing service to
	 * @return the list
	 */
	public List<ServiceRate> prepareRateServicesForSave(BillingServiceTo billingServiceTo){
		List<ServiceRate> serviceRateList =null;
		if (Validations.validateListIsNotNullAndNotEmpty(billingServiceTo.getListServiceRateTo())){
			
			serviceRateList= prepareServiceRate(billingServiceTo.getIdBillingServicePk(),billingServiceTo.getListServiceRateTo());
			
		}
		
		return serviceRateList;
	}
	
	/**
	 * Prepare service rate.
	 *
	 * @param idBillingServicePk the id billing service pk
	 * @param listServiceRateTo the list service rate to
	 * @return the list
	 */
	public List<ServiceRate> prepareServiceRate(Long idBillingServicePk, List<ServiceRateTo> listServiceRateTo){
		
		List<ServiceRate> serviceRateList= new ArrayList<ServiceRate>();
		ServiceRate serviceRate;	
		/* Create  one bean entity BillingService  and your list of service rate */
		BillingService billingServiceNew = new BillingService();
		billingServiceNew.setIdBillingServicePk(idBillingServicePk);
			for (ServiceRateTo serviceRateTo : listServiceRateTo) {
				if (serviceRateTo.getRateStatus().intValue()==RateServiceStatus.PENDING.getCode().intValue()){
					serviceRate= new ServiceRate();
					/** by default setIndex*/  
					
					serviceRate.setIndTaxed(PropertiesConstants.NOT_APPLY_TAXED);
					serviceRate.setRateName(serviceRateTo.getRateName());
					serviceRate.setRateType(serviceRateTo.getRateType());
					serviceRate.setCurrencyRate(serviceRateTo.getCurrencyRate());
					serviceRate.setEndEffectiveDate(serviceRateTo.getEndEffectiveDate());
					serviceRate.setInitialEffectiveDate(serviceRateTo.getInitialEffectiveDate());
					serviceRate.setIndScaleMovement(serviceRateTo.getIndScaleMovement());
					
					serviceRate.setIndMinimumAmount(serviceRateTo.getIndAmountMinimum());
					serviceRate.setCurrencyMinimumAmount(serviceRateTo.getCurrencyMinimum());
					serviceRate.setMinimumAmount(serviceRateTo.getRateMinimum());
					
					
					if (serviceRateTo.getRateType().equals(RateType.FIJO.getCode())){
						serviceRate.setRateAmount(serviceRateTo.getAmountRate());	
						
					} else if(serviceRateTo.getRateType().equals(RateType.PORCENTUAL.getCode())){
						BigDecimal ratePercentTo = BillingUtils.divideDecimalObject(serviceRateTo.getPercentageRate(), new BigDecimal(100), 10);
						
						serviceRate.setRatePercent(ratePercentTo);
						
					} else if(serviceRateTo.getRateType().equals(RateType.ESCALONADO_FIJO.getCode())){
						serviceRate.setServiceRateScales(serviceRateTo.prepareServiceRateScale());
						
						if(serviceRateTo.getIndScaleMovement().equals(GeneralConstants.ONE_VALUE_INTEGER)){
							serviceRate.setRateMovements(serviceRateTo.prepareServiceRateMovementToStaggered(0));  
						}
						
					} else if (serviceRateTo.getRateType().equals(RateType.ESCALONADO_PORCENTUAL.getCode())){
						serviceRate.setServiceRateScales(serviceRateTo.prepareServiceRateScale());
						
						if(serviceRateTo.getIndScaleMovement().equals(GeneralConstants.ONE_VALUE_INTEGER)){
							serviceRate.setRateMovements(serviceRateTo.prepareServiceRateMovementToStaggered(0));  
						}
						
						
					} else if(serviceRateTo.getRateType().equals(RateType.ESCALONADO_MIXTO.getCode())){
						serviceRate.setServiceRateScales(serviceRateTo.prepareServiceRateScale());
						
						if(serviceRateTo.getIndScaleMovement().equals(GeneralConstants.ONE_VALUE_INTEGER)){
							serviceRate.setRateMovements(serviceRateTo.prepareServiceRateMovementToStaggered(0));  
						}
						
					}else if (serviceRateTo.getRateType().equals(RateType.TIPO_MOVIMIENTO_FIJO.getCode())){
						serviceRate.setRateMovements(serviceRateTo.prepareServiceRateMovement(0));
						
					} else if (serviceRateTo.getRateType().equals(RateType.TIPO_MOVIMIENTO_PORCENTUAL.getCode())){
						serviceRate.setRateMovements(serviceRateTo.prepareServiceRateMovement(0));  
						
					}
					serviceRate.setRateState(RateServiceStatus.REGISTER.getCode());
					/** add serviceRate into billingSrvice*/
					serviceRate.setBillingService(billingServiceNew);
					serviceRateList.add(serviceRate);
				}
			}
	return serviceRateList;
	}
	
	
	/**
	 * get the Billingservice by code and state.
	 *
	 * @param code the code
	 * @return the billing service by code
	 */
	public BillingService getBillingServiceByCode(String code){
	
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select bs From BillingService bs  ");
		sbQuery.append(" Where 1 = 1");

		
		if(Validations.validateIsNotNullAndNotEmpty(code)){
			sbQuery.append(" And bs.serviceCode = :code");
		}
		
		Query query = em.createQuery(sbQuery.toString());   
		
		if(Validations.validateIsNotNullAndNotEmpty(code)){
			query.setParameter("code", code);			
		}
		
		BillingService billingService=new BillingService(); 
		try{
			Object entity=query.getSingleResult();
			billingService=(BillingService)entity;
		} catch(NoResultException  nrex){
			billingService=null;
		}
		
		
		return billingService;
	}
	
	/**
	 * Get List Billing Service by Parameters.
	 *
	 * @param filter the filter
	 * @return List<BillingService>
	 */
	public List<BillingService> getListBillingServiceByParameters(BillingServiceTo filter){
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append(" Select new BillingService( bs.idBillingServicePk, bs.baseCollection, bs.businessLine, ");
		sbQuery.append("  bs.finantialCode, bs.calculationPeriod, bs.clientServiceType, bs.collectionPeriod, ");
		sbQuery.append("  bs.collectionType, bs.observation, bs.endEffectiveDate, bs.entityCollection, ");
		sbQuery.append("  bs.initialEffectiveDate, bs.serviceCode, bs.serviceName, bs.serviceState, ");
		sbQuery.append("  bs.serviceType, bs.taxApplied, bs.registryDate, bs.inIntegratedBill, ");
		sbQuery.append("  bs.indSourceBaseCollection, bss, bs.currencyBilling, ");
		sbQuery.append("  (select p.parameterName from ParameterTable p where p.parameterTablePk = bs.entityCollection) ,");
		sbQuery.append("  (select p.parameterName from ParameterTable p where p.parameterTablePk = bs.baseCollection) ,");
		sbQuery.append("  (select p.parameterName from ParameterTable p where p.parameterTablePk = bs.serviceState) ,");
		sbQuery.append("  (select p.parameterName from ParameterTable p where p.parameterTablePk = bs.serviceType) ,");
		sbQuery.append("  (select p.parameterName from ParameterTable p where p.parameterTablePk = bs.collectionPeriod) ,");
		sbQuery.append("  (select p.parameterName from ParameterTable p where p.parameterTablePk = bs.clientServiceType) ,");
		sbQuery.append("  (select p.parameterName from ParameterTable p where p.parameterTablePk = bs.calculationPeriod),");
		sbQuery.append("  bs.sourceInformation,   bs.currencyCalculation , ");
		sbQuery.append("  bs.accountingAccount,  bs.referenceRate, bs.portfolio )	");
		sbQuery.append(" From BillingService bs  left outer join  bs.billingServiceSource bss");

		sbQuery.append("   WHERE 1=1  ");
		
		
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getServiceName())){
			sbQuery.append(" AND bs.serviceName= :serviceNamePrm");
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getStateService()) && Validations.validateIsNotNullAndNotEmpty(filter.getStateService())){
			sbQuery.append(" AND bs.serviceState= :serviceStatePrm");
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getServiceType())&&Validations.validateIsNotNullAndNotEmpty(filter.getServiceType())){
			sbQuery.append(" AND bs.serviceType= :serviceTypePrm");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getServiceCode())){
			sbQuery.append(" AND bs.serviceCode= :serviceCodePrm");
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getEntityCollection() )&&Validations.validateIsNotNullAndNotEmpty(filter.getEntityCollection())){
			sbQuery.append(" AND bs.entityCollection= :entityCollectionPrm");
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getBaseCollection() )&&Validations.validateIsNotNullAndNotEmpty(filter.getBaseCollection())){
			sbQuery.append(" AND bs.baseCollection= :baseCollectionPrm");
		} 

		if(Validations.validateIsNotNullAndPositive(filter.getClientServiceType() )&&Validations.validateIsNotNullAndNotEmpty(filter.getClientServiceType())){
			sbQuery.append(" AND bs.clientServiceType= :clientServiceTypePrm");
		}
		if(Validations.validateIsNotNullAndPositive(filter.getCollectionPeriod() )&&Validations.validateIsNotNullAndNotEmpty(filter.getCollectionPeriod())){
			sbQuery.append(" and bs.collectionPeriod= :collectionPeriodPrm");
		}
		if(Validations.validateIsNotNullAndPositive(filter.getIdBillingServicePk() )&&Validations.validateIsNotNullAndNotEmpty(filter.getIdBillingServicePk())){
			sbQuery.append(" and bs.idBillingServicePk= :billingServicePKPrm");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getInIntegratedBill())){
			sbQuery.append(" AND bs.inIntegratedBill= :indIntegratedBillPrm");
		}
		sbQuery.append(" ORDER BY  ");
		sbQuery.append("  TO_NUMBER(bs.serviceCode,9999)");
		
		Query query = em.createQuery(sbQuery.toString());
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getServiceName())){ 
			query.setParameter("serviceNamePrm", filter.getServiceName());
		}
    	if(Validations.validateIsNotNullAndPositive(filter.getStateService() )&&Validations.validateIsNotNullAndNotEmpty(filter.getStateService())){
    		query.setParameter("serviceStatePrm", filter.getStateService());
		}
		if(Validations.validateIsNotNullAndPositive(filter.getServiceType() )&&Validations.validateIsNotNullAndNotEmpty(filter.getServiceType())){
			query.setParameter("serviceTypePrm", filter.getServiceType());
		} 
		if(Validations.validateIsNotNullAndNotEmpty(filter.getServiceCode())){
			query.setParameter("serviceCodePrm", filter.getServiceCode());
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getEntityCollection() )&&Validations.validateIsNotNullAndNotEmpty(filter.getEntityCollection())){
			query.setParameter("entityCollectionPrm", filter.getEntityCollection());
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getBaseCollection() )&&Validations.validateIsNotNullAndNotEmpty(filter.getBaseCollection())){
			query.setParameter("baseCollectionPrm", filter.getBaseCollection());
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getClientServiceType() )&&Validations.validateIsNotNullAndNotEmpty(filter.getClientServiceType())){
    		query.setParameter("clientServiceTypePrm", filter.getClientServiceType());
		}
		if(Validations.validateIsNotNullAndPositive(filter.getCollectionPeriod() )&&Validations.validateIsNotNullAndNotEmpty(filter.getCollectionPeriod())){
    		query.setParameter("collectionPeriodPrm", filter.getCollectionPeriod());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdBillingServicePk())){
    		query.setParameter("billingServicePKPrm", filter.getIdBillingServicePk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getInIntegratedBill())){
    		query.setParameter("indIntegratedBillPrm", filter.getInIntegratedBill());
		}
		
		List<BillingService> listBillingService=null;
		try{
			listBillingService=query.getResultList();
		}
		 catch (Exception e) {
			log.debug(e.getMessage());
		}
		
		return listBillingService;
	}
	
	/**
	 * Get List Billing Service by Parameters.
	 *
	 * @param filter the filter
	 * @return List<BillingService>
	 */
	public List<RequestService> getListBillingReqService(BillingRegServiceTo filter){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select bs From RequestService bs  ");
		sbQuery.append(" join fetch bs.billingService s ");                 
		sbQuery.append(" WHERE 1=1  ");		
		sbQuery.append(" and trunc(bs.obsDate) between trunc(:initialDate) and trunc(:finalDate)  " );
		if(Validations.validateIsNotNullAndNotEmpty(filter.getServCode())){
			sbQuery.append(" AND bs.servCode = :serviceCode  ");
		} 		
		Query query = em.createQuery(sbQuery.toString());	
		query.setParameter("initialDate", filter.getInitialDate());
		query.setParameter("finalDate", filter.getFinalDate());
		if(Validations.validateIsNotNullAndNotEmpty(filter.getServCode())){ 
			query.setParameter("serviceCode", filter.getServCode());
		}		
		List<RequestService> listBillingReqService=null;
		try {
			listBillingReqService=query.getResultList();
		} catch (Exception e) {
			log.debug(e.getMessage());
		}		
		return listBillingReqService;
	}
	
	/**
	 * Method for get BillingService .
	 *
	 * @param filter the filter
	 * @return the list parameter billing service
	 * @throws ServiceException the service exception
	 */
    public List<BillingService> getListParameterBillingService(BillingServiceTo filter) throws ServiceException	{
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select bs.id_billing_service_pk, bs.SERVICE_NAME,bs.SERVICE_CODE,CLIENT_SERVICE_TYPE, "); 
		sbQuery.append("  SERVICE_TYPE,STATE_SERVICE,ENTITY_COLLECTION,BASE_COLLECTION,COLLECTION_PERIOD,"); 
		sbQuery.append(" ( select pt.description from parameter_table pt where bs.SERVICE_TYPE=pt.parameter_table_pk ) as desription_service_type ," );
		sbQuery.append(" ( select pt.description from parameter_table pt where bs.STATE_SERVICE=pt.parameter_table_pk ) as descripion_service_state," );
		sbQuery.append(" (select pt.description from parameter_table pt where bs.ENTITY_COLLECTION=pt.parameter_table_pk) as description_entity_collection, " );
		sbQuery.append("  (select pt.description from parameter_table pt where bs.BASE_COLLECTION=pt.parameter_table_pk) as description_base_collection, " );
		sbQuery.append("  (select pt.description from parameter_table pt where bs.COLLECTION_PERIOD=parameter_table_pk) as description_collection_period, " );
		sbQuery.append(" ( select pt.description from parameter_table pt where bs.CLIENT_SERVICE_TYPE=pt.parameter_table_pk ) as desription_client_service_type ," );
		sbQuery.append(" ( select pt.description from parameter_table pt where bs.CALCULATION_PERIOD=pt.parameter_table_pk ) as description_calculation_period ," );
		sbQuery.append(" TAX_APPLIED,BUSINESS_LINE,FINANTIAL_CODE , COMMENTS , INITIAL_EFFECTIVE_DATE , END_EFFECTIVE_DATE, ");
		sbQuery.append("	 CALCULATION_PERIOD,COLLECTION_TYPE,REGISTRY_DATE ,IND_INTEGRATED_BILL, CURRENCY_BILLING,  	" );
		sbQuery.append("  CURRENCY_CALCULATION, SOURCE_INFORMATION  ");
		sbQuery.append("  from billing_service bs   ");
		sbQuery.append(" Where 1 = 1 " );
				
		 
		if(Validations.validateIsNotNullAndNotEmpty(filter.getServiceName())){
			sbQuery.append(" AND bs.SERVICE_NAME= :serviceNamePrm");
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getStateService()) && Validations.validateIsNotNullAndNotEmpty(filter.getStateService())){
			sbQuery.append(" AND bs.STATE_SERVICE= :serviceStatePrm");
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getServiceType())&&Validations.validateIsNotNullAndNotEmpty(filter.getServiceType())){
			sbQuery.append(" AND bs.SERVICE_TYPE= :serviceTypePrm");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getServiceCode())){
			sbQuery.append(" AND bs.SERVICE_CODE= :serviceCodePrm");
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getEntityCollection() )&&Validations.validateIsNotNullAndNotEmpty(filter.getEntityCollection())){
			sbQuery.append(" AND bs.ENTITY_COLLECTION= :entityCollectionPrm");
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getBaseCollection() )&&Validations.validateIsNotNullAndNotEmpty(filter.getBaseCollection())){
			sbQuery.append(" AND bs.BASE_COLLECTION= :baseCollectionPrm");
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getClientServiceType() )&&Validations.validateIsNotNullAndNotEmpty(filter.getClientServiceType())){
			sbQuery.append(" AND bs.CLIENT_SERVICE_TYPE= :clientServiceTypePrm");
		}
		if(Validations.validateIsNotNullAndPositive(filter.getCollectionPeriod() )&&Validations.validateIsNotNullAndNotEmpty(filter.getCollectionPeriod())){
			sbQuery.append(" and bs.COLLECTION_PERIOD= :collectionPeriodPrm");
		}
		if(Validations.validateIsNotNullAndPositive(filter.getIdBillingServicePk() )&&Validations.validateIsNotNullAndNotEmpty(filter.getIdBillingServicePk())){
			sbQuery.append(" and bs.ID_BILLING_SERVICE_PK= :billingServicePKPrm");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getInIntegratedBill())){
			sbQuery.append(" AND bs.IND_INTEGRATED_BILL= :indIntegratedBillPrm");
		}
		
		sbQuery.append(" ORDER BY TO_NUMBER(BS.SERVICE_CODE,9999) " );
    	Query query = em.createNativeQuery(sbQuery.toString());
    	
    	if(Validations.validateIsNotNullAndNotEmpty(filter.getServiceName())){ 
			query.setParameter("serviceNamePrm", filter.getServiceName());
		}
    	if(Validations.validateIsNotNullAndPositive(filter.getStateService() )&&Validations.validateIsNotNullAndNotEmpty(filter.getStateService())){
    		query.setParameter("serviceStatePrm", filter.getStateService());
		}
		if(Validations.validateIsNotNullAndPositive(filter.getServiceType() )&&Validations.validateIsNotNullAndNotEmpty(filter.getServiceType())){
			query.setParameter("serviceTypePrm", filter.getServiceType());
		} 
		if(Validations.validateIsNotNullAndNotEmpty(filter.getServiceCode())){
			query.setParameter("serviceCodePrm", filter.getServiceCode());
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getEntityCollection() )&&Validations.validateIsNotNullAndNotEmpty(filter.getEntityCollection())){
			query.setParameter("entityCollectionPrm", filter.getEntityCollection());
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getBaseCollection() )&&Validations.validateIsNotNullAndNotEmpty(filter.getBaseCollection())){
			query.setParameter("baseCollectionPrm", filter.getBaseCollection());
		} 
		if(Validations.validateIsNotNullAndPositive(filter.getClientServiceType() )&&Validations.validateIsNotNullAndNotEmpty(filter.getClientServiceType())){
    		query.setParameter("clientServiceTypePrm", filter.getClientServiceType());
		}
		if(Validations.validateIsNotNullAndPositive(filter.getCollectionPeriod() )&&Validations.validateIsNotNullAndNotEmpty(filter.getCollectionPeriod())){
    		query.setParameter("collectionPeriodPrm", filter.getCollectionPeriod());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdBillingServicePk())){
    		query.setParameter("billingServicePKPrm", filter.getIdBillingServicePk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getInIntegratedBill())){
    		query.setParameter("indIntegratedBillPrm", filter.getInIntegratedBill());
		}

		
		
		
		List<Object[]>  objectList = query.getResultList();
		
		List<BillingService> billingServiceList = new ArrayList<BillingService>();
		for(int i=0;i<objectList.size();++i){
			//Getting Results from data base
			Object[] sResults = (Object[])objectList.get(i);
			BillingService billingService = new BillingService();
			billingService.setIdBillingServicePk(Long.parseLong(sResults[0]==null?"":sResults[0].toString())); 
			billingService.setServiceName(sResults[1]==null?"":sResults[1].toString());
			billingService.setServiceCode(sResults[2]==null?"":sResults[2].toString());
			billingService.setClientServiceType(sResults[3]==null?null:Integer.parseInt(sResults[3].toString()));
			billingService.setServiceType(sResults[4]==null?null:Integer.parseInt(sResults[4].toString()));
			billingService.setServiceState(sResults[5]==null?null:Integer.parseInt(sResults[5].toString()));
			billingService.setEntityCollection(sResults[6]==null?null:Integer.parseInt(sResults[6].toString()));
			billingService.setBaseCollection(sResults[7]==null?null:Integer.parseInt(sResults[7].toString()));
//			billingService.setCollectionPeriod(sResults[8]==null?null:Integer.parseInt(sResults[8].toString()));
			billingService.setDescriptionServiceType(sResults[9]==null?"":sResults[9].toString()); 
			billingService.setDescriptionStateBillingService(sResults[10]==null?"":sResults[10].toString());
			billingService.setDescriptionCollectionEntity(sResults[11]==null?"":sResults[11].toString());
			billingService.setDescriptionBaseCollection(sResults[12]==null?"":sResults[12].toString()); 
			billingService.setDescriptionCollectionPeriod(sResults[13]==null?"":sResults[13].toString());
			billingService.setDescriptionClientServiceType(sResults[14]==null?"":sResults[14].toString());
			billingService.setDescriptionCalculationPeriod(sResults[15]==null?"":sResults[15].toString());
			billingService.setTaxApplied(sResults[16]==null?null:Integer.parseInt(sResults[16].toString())); 
			billingService.setBusinessLine(sResults[17]==null?"":sResults[17].toString());
			billingService.setFinantialCode(sResults[18]==null?"":sResults[18].toString());
			billingService.setObservation(sResults[19]==null?"":sResults[19].toString());
			billingService.setInitialEffectiveDate((Date) sResults[20]);
			billingService.setEndEffectiveDate((Date)sResults[21]);
			billingService.setCalculationPeriod(sResults[22]==null?null:Integer.parseInt(sResults[22].toString()));			
			billingService.setCollectionType(sResults[23]==null?null:Integer.parseInt(sResults[23].toString()));
			billingService.setRegistryDate(sResults[24]==null?null:(Date) sResults[24]);
			billingService.setInIntegratedBill(sResults[25]==null?null:Integer.parseInt(sResults[25].toString()));
			billingService.setCurrencyBilling(sResults[26]==null?null:Integer.parseInt(sResults[26].toString()));
			billingService.setCurrencyCalculation(sResults[27]==null?null:Integer.parseInt(sResults[27].toString()));
			billingService.setSourceInformation(sResults[28]==null?null:Integer.parseInt(sResults[28].toString()));
		
			billingServiceList.add(billingService);
			 
		}
		return billingServiceList;
	}
	
	/**
	 * Method for get BillingService for ServiceRate.
	 *
	 * @param filter the filter
	 * @return the billing service list automatic and eventual
	 * @throws ServiceException the service exception
	 */
	public List<BillingService> getBillingServiceListAutomaticAndEventual(BillingServiceTo filter) throws ServiceException	{
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select bs.id_billing_service_pk, bs.SERVICE_NAME,bs.SERVICE_CODE,CLIENT_SERVICE_TYPE, "); 
	sbQuery.append("  SERVICE_TYPE,STATE_SERVICE,ENTITY_COLLECTION,BASE_COLLECTION,COLLECTION_PERIOD,"); 
	sbQuery.append(" ( select pt.description from parameter_table pt where bs.SERVICE_TYPE=pt.parameter_table_pk ) as desription_service_type ," );
	sbQuery.append(" ( select pt.description from parameter_table pt where bs.STATE_SERVICE=pt.parameter_table_pk ) as descripion_service_state," );
	sbQuery.append(" (select pt.description from parameter_table pt where bs.ENTITY_COLLECTION=pt.parameter_table_pk) as description_entity_collection, " );
	sbQuery.append("  (select pt.description from parameter_table pt where bs.BASE_COLLECTION=pt.parameter_table_pk) as description_base_collection, " );
	sbQuery.append("  (select pt.description from parameter_table pt where bs.COLLECTION_PERIOD=parameter_table_pk) as description_collection_period, " );
	sbQuery.append(" ( select pt.description from parameter_table pt where bs.CLIENT_SERVICE_TYPE=pt.parameter_table_pk ) as desription_client_service_type ," );
	sbQuery.append(" ( select pt.description from parameter_table pt where bs.CALCULATION_PERIOD=pt.parameter_table_pk ) as description_calculation_period ," );
	sbQuery.append(" TAX_APPLIED,BUSINESS_LINE,FINANTIAL_CODE , COMMENTS , INITIAL_EFFECTIVE_DATE , END_EFFECTIVE_DATE, ");
	sbQuery.append("	 CALCULATION_PERIOD,COLLECTION_TYPE,REGISTRY_DATE ,IND_INTEGRATED_BILL, CURRENCY_BILLING,	" );
	sbQuery.append("	CURRENCY_CALCULATION, SOURCE_INFORMATION 	" );
	sbQuery.append("  from billing_service bs   ");
	sbQuery.append(" Where 1 = 1 " );
			
	 
	if(Validations.validateIsNotNullAndNotEmpty(filter.getServiceName())){
		sbQuery.append(" AND bs.SERVICE_NAME= :serviceNamePrm");
	} 
	if(Validations.validateIsNotNullAndPositive(filter.getStateService()) && Validations.validateIsNotNullAndNotEmpty(filter.getStateService())){
		sbQuery.append(" AND bs.STATE_SERVICE= :serviceStatePrm");
	} 
	if(Validations.validateIsNotNullAndPositive(filter.getServiceType())&&Validations.validateIsNotNullAndNotEmpty(filter.getServiceType())){
		sbQuery.append(" AND bs.SERVICE_TYPE= :serviceTypePrm");
	}
	else{
		sbQuery.append(" AND NOT bs.SERVICE_TYPE="+PropertiesConstants.SERVICE_TYPE_MANUAL_PK);
	}
	if(Validations.validateIsNotNullAndNotEmpty(filter.getServiceCode())){
		sbQuery.append(" AND bs.SERVICE_CODE= :serviceCodePrm");
	} 
	if(Validations.validateIsNotNullAndPositive(filter.getEntityCollection() )&&Validations.validateIsNotNullAndNotEmpty(filter.getEntityCollection())){
		sbQuery.append(" AND bs.ENTITY_COLLECTION= :entityCollectionPrm");
	} 
	if(Validations.validateIsNotNullAndPositive(filter.getBaseCollection() )&&Validations.validateIsNotNullAndNotEmpty(filter.getBaseCollection())){
		sbQuery.append(" AND bs.BASE_COLLECTION= :baseCollectionPrm");
	} 
	if(Validations.validateIsNotNullAndPositive(filter.getClientServiceType() )&&Validations.validateIsNotNullAndNotEmpty(filter.getClientServiceType())){
		sbQuery.append(" AND bs.CLIENT_SERVICE_TYPE= :clientServiceTypePrm");
	}
	if(Validations.validateIsNotNullAndPositive(filter.getCollectionPeriod() )&&Validations.validateIsNotNullAndNotEmpty(filter.getCollectionPeriod())){
		sbQuery.append(" and bs.COLLECTION_PERIOD= :collectionPeriodPrm");
	}
	if(Validations.validateIsNotNullAndPositive(filter.getIdBillingServicePk() )&&Validations.validateIsNotNullAndNotEmpty(filter.getIdBillingServicePk())){
		sbQuery.append(" and bs.ID_BILLING_SERVICE_PK= :billingServicePKPrm");
	}
	if(Validations.validateIsNotNullAndNotEmpty(filter.getInIntegratedBill())){
		sbQuery.append(" AND bs.IND_INTEGRATED_BILL= :indIntegratedBillPrm");
	}
	
	sbQuery.append(" ORDER BY TO_NUMBER(BS.SERVICE_CODE,9999) " );
	Query query = em.createNativeQuery(sbQuery.toString());
	
	if(Validations.validateIsNotNullAndNotEmpty(filter.getServiceName())){ 
		query.setParameter("serviceNamePrm", filter.getServiceName());
	}
	if(Validations.validateIsNotNullAndPositive(filter.getStateService() )&&Validations.validateIsNotNullAndNotEmpty(filter.getStateService())){
		query.setParameter("serviceStatePrm", filter.getStateService());
	}
	if(Validations.validateIsNotNullAndPositive(filter.getServiceType() )&&Validations.validateIsNotNullAndNotEmpty(filter.getServiceType())){
		query.setParameter("serviceTypePrm", filter.getServiceType());
	}
	else;
	if(Validations.validateIsNotNullAndNotEmpty(filter.getServiceCode())){
		query.setParameter("serviceCodePrm", filter.getServiceCode());
	} 
	if(Validations.validateIsNotNullAndPositive(filter.getEntityCollection() )&&Validations.validateIsNotNullAndNotEmpty(filter.getEntityCollection())){
		query.setParameter("entityCollectionPrm", filter.getEntityCollection());
	} 
	if(Validations.validateIsNotNullAndPositive(filter.getBaseCollection() )&&Validations.validateIsNotNullAndNotEmpty(filter.getBaseCollection())){
		query.setParameter("baseCollectionPrm", filter.getBaseCollection());
	} 
	if(Validations.validateIsNotNullAndPositive(filter.getClientServiceType() )&&Validations.validateIsNotNullAndNotEmpty(filter.getClientServiceType())){
		query.setParameter("clientServiceTypePrm", filter.getClientServiceType());
	}
	if(Validations.validateIsNotNullAndPositive(filter.getCollectionPeriod() )&&Validations.validateIsNotNullAndNotEmpty(filter.getCollectionPeriod())){
		query.setParameter("collectionPeriodPrm", filter.getCollectionPeriod());
	}
	if(Validations.validateIsNotNullAndNotEmpty(filter.getIdBillingServicePk())){
		query.setParameter("billingServicePKPrm", filter.getIdBillingServicePk());
	}
	if(Validations.validateIsNotNullAndNotEmpty(filter.getInIntegratedBill())){
		query.setParameter("indIntegratedBillPrm", filter.getInIntegratedBill());
	}
	
	
	List<Object[]>  objectList = query.getResultList();
	
	List<BillingService> billingServiceList = new ArrayList<BillingService>();
	for(int i=0;i<objectList.size();++i){			
		//Getting Results from data base
		Object[] sResults = (Object[])objectList.get(i);
		BillingService billingService = new BillingService();
		billingService.setIdBillingServicePk(Long.parseLong(sResults[0]==null?"":sResults[0].toString())); 
		billingService.setServiceName(sResults[1]==null?"":sResults[1].toString());
		billingService.setServiceCode(sResults[2]==null?"":sResults[2].toString());
		billingService.setClientServiceType(sResults[3]==null?null:Integer.parseInt(sResults[3].toString()));
		billingService.setServiceType(sResults[4]==null?null:Integer.parseInt(sResults[4].toString()));
		billingService.setServiceState(sResults[5]==null?null:Integer.parseInt(sResults[5].toString()));
		billingService.setEntityCollection(sResults[6]==null?null:Integer.parseInt(sResults[6].toString()));
		billingService.setBaseCollection(sResults[7]==null?null:Integer.parseInt(sResults[7].toString()));
//		billingService.setCollectionPeriod(sResults[8]==null?null:Integer.parseInt(sResults[8].toString()));
		billingService.setDescriptionServiceType(sResults[9]==null?"":sResults[9].toString()); 
		billingService.setDescriptionStateBillingService(sResults[10]==null?"":sResults[10].toString());
		billingService.setDescriptionCollectionEntity(sResults[11]==null?"":sResults[11].toString());
		billingService.setDescriptionBaseCollection(sResults[12]==null?"":sResults[12].toString()); 
		billingService.setDescriptionCollectionPeriod(sResults[13]==null?"":sResults[13].toString());
		billingService.setDescriptionClientServiceType(sResults[14]==null?"":sResults[14].toString());
		billingService.setDescriptionCalculationPeriod(sResults[15]==null?"":sResults[15].toString());
		billingService.setTaxApplied(sResults[16]==null?null:Integer.parseInt(sResults[16].toString())); 
		billingService.setBusinessLine(sResults[17]==null?"":sResults[17].toString());
		billingService.setFinantialCode(sResults[18]==null?"":sResults[18].toString());
		billingService.setObservation(sResults[19]==null?"":sResults[19].toString());
			billingService.setInitialEffectiveDate((Date) sResults[20]);
			billingService.setEndEffectiveDate((Date)sResults[21]);
			billingService.setCalculationPeriod(sResults[22]==null?null:Integer.parseInt(sResults[22].toString()));			
			billingService.setCollectionType(sResults[23]==null?null:Integer.parseInt(sResults[23].toString()));
			billingService.setRegistryDate(sResults[24]==null?null:(Date) sResults[24]);
			billingService.setInIntegratedBill(sResults[25]==null?null:Integer.parseInt(sResults[25].toString()));
			billingService.setCurrencyBilling(sResults[26]==null?null:Integer.parseInt(sResults[26].toString()));
			billingService.setCurrencyCalculation(sResults[27]==null?null:Integer.parseInt(sResults[27].toString()));
			billingService.setSourceInformation(sResults[28]==null?null:Integer.parseInt(sResults[28].toString()));
			
			billingServiceList.add(billingService);
			 
		}
		return billingServiceList;
	}
	
	
	/**
	 * Gets the billing service with service rate.
	 *
	 * @param billingServiceTo the billing service to
	 * @return the billing service with service rate
	 */
	public BillingService getBillingServiceWithServiceRate(BillingServiceTo billingServiceTo){
	
	BillingService billingService=null; 
	
		if(Validations.validateIsNotNullAndNotEmpty(billingServiceTo.getIdBillingServicePk())){
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("Select bs From BillingService bs left join fetch bs.serviceRates sr ");
			sbQuery.append(" Where bs.idBillingServicePk = :idBillingServicePk ");
			// 02/11/2022 - Debemos poder modificar un servicio incluso si sus tarifas estan en estado Registrado
//			sbQuery.append("  AND sr.rateState=").append(RateStateType.ACTIVED.getCode());
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idBillingServicePk", billingServiceTo.getIdBillingServicePk());		
			
			try{
				Object entity=query.getSingleResult();
				billingService=(BillingService)entity;
			} catch(NoResultException  nrex){
				nrex.printStackTrace();
			}
			
		}
		
		
		return billingService;
	}
	
	/**
	 * Gets the billing services 
	 */
	public List<BillingService> getBillingServiceReq () {	
		List<BillingService> billingService = null; 	
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select bs From BillingService bs ");
		sbQuery.append(" Where bs.serviceCode in (22,23,30,32)  ");	
		Query query = em.createQuery(sbQuery.toString());
		try {
			Object entity = query.getResultList();
			billingService = (List<BillingService>)entity;
		} catch(NoResultException  nrex){
			nrex.printStackTrace();
		}			
		return billingService;
	}
	
	/**
	 * 	FOR COLLECTION	*.
	 *
	 * @param billingServiceTo the billing service to
	 * @return the billing service with service rate for collection
	 */
	public BillingService getBillingServiceWithServiceRateForCollection(BillingServiceTo billingServiceTo){
		
		BillingService billingService=null; 
		
			if(Validations.validateIsNotNullAndNotEmpty(billingServiceTo.getIdBillingServicePk())){
				StringBuilder sbQuery = new StringBuilder();
				sbQuery.append("Select  bs From BillingService bs left  join fetch bs.serviceRates sr ");
				sbQuery.append(" Where bs.idBillingServicePk = :idBillingServicePk ");		
				
				
				
				 if (Validations.validateIsNotNull(billingServiceTo.getServiceRateToSelected()) && 
					     Validations.validateIsNotNull(billingServiceTo.getServiceRateToSelected().getLastEffectiveDate())  ){   
						   			
					    sbQuery.append(" and  :effectiveDate  between trunc(sr.initialEffectiveDate) and trunc(sr.endEffectiveDate)" );
					    sbQuery.append(" and not sr.rateState in ("+PropertiesConstants.SERVICE_STATUS_CANCELLED_PK + ","+ PropertiesConstants.SERVICE_STATUS_LOCK_PK+")" );
					    
					    
				   }else {
					   sbQuery.append(" and sr.rateState="+RateStateType.ACTIVED.getCode());
				   }
				 
				
				Query query = em.createQuery(sbQuery.toString());
				
				query.setParameter("idBillingServicePk", billingServiceTo.getIdBillingServicePk());		
			
				
				if (Validations.validateIsNotNull(billingServiceTo.getServiceRateToSelected()) && 
					     Validations.validateIsNotNull(billingServiceTo.getServiceRateToSelected().getLastEffectiveDate())  ){
					   
					query.setParameter("effectiveDate", billingServiceTo.getServiceRateToSelected().getLastEffectiveDate(),TemporalType.DATE);         
				  }
				
				try{
					Object entity=query.getSingleResult();
					billingService=(BillingService)entity;
				} catch(NoResultException  nrex){
					nrex.printStackTrace();
				}
				
			}			
			
			return billingService;
	}
	
	
	
	
	/**
	 * Find billing service by base collection.
	 *
	 * @param baseCollectioId the base collectio id
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<BillingService>  findBillingServiceByBaseCollection(Integer baseCollectioId ) throws ServiceException{
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select distinct bs From BillingService bs left join fetch bs.serviceRates sr  ");
		sbQuery.append(" Where 1 = 1 And bs.serviceState = "+ PropertiesConstants.SERVICE_STATUS_ACTIVE_PK);
		if(Validations.validateIsNotNullAndNotEmpty(baseCollectioId)){
			sbQuery.append(" And bs.baseCollection = :baseCollectioId");
		}
		
		Query query = em.createQuery(sbQuery.toString());   
		
		if(Validations.validateIsNotNullAndNotEmpty(baseCollectioId)){
			query.setParameter("baseCollectioId", baseCollectioId);			
		}
		
		List<BillingService>  billingServiceList= null ;

		billingServiceList = query.getResultList();
		
		return billingServiceList;
	}
	
	
	/**
	 * Find billing service by calculation period.
	 *
	 * @param calculationPeriod the calculation period
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<BillingService>  findBillingServiceByCalculationPeriod(Integer calculationPeriod ) throws ServiceException{
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select distinct bs From BillingService bs left join fetch bs.serviceRates sr  ");
		sbQuery.append(" Where  bs.serviceState = "+ PropertiesConstants.SERVICE_STATUS_ACTIVE_PK);
		sbQuery.append(" and bs.calculationPeriod = :calculationPeriod");
		
		Query query = em.createQuery(sbQuery.toString());   
		query.setParameter("calculationPeriod", calculationPeriod);			

		List<BillingService>  billingServiceList= null ;

		billingServiceList = query.getResultList();
	

		return billingServiceList;
	}
	
	
	/**
	 * Find all billing service.
	 *
	 * @param billingServiceTo the billing service to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<BillingService>  findAllBillingService(BillingServiceTo billingServiceTo ) throws ServiceException{
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select  bs From BillingService bs ");
		sbQuery.append("  where 1=1 ");
		if(Validations.validateIsNotNull(billingServiceTo)&&Validations.validateIsNotNull(billingServiceTo.getStateService()))
			sbQuery.append(" and bs.serviceState = :stateService");
		
		Query query = em.createQuery(sbQuery.toString());		
		if(Validations.validateIsNotNull(billingServiceTo)&&Validations.validateIsNotNull(billingServiceTo.getStateService()))
			query.setParameter("stateService", billingServiceTo.getStateService());			
			
		List<BillingService>  billingServiceList= null ;
		billingServiceList = query.getResultList();
	

		return billingServiceList;
	}
	
	

	
	/**
	 * METHOD REMOTE FOR FIND BILLING SERVICE.
	 *
	 * @param billingServiceTo the billing service to
	 * @return the list
	 */
	 public List<BillingService>  findRemoteBillingService(BillingServiceTo billingServiceTo){
	  List<BillingService>  listBillingService =null; 
	  
	  if(Validations.validateIsNotNullAndNotEmpty(billingServiceTo)){
	   StringBuilder sbQuery = new StringBuilder();
	   sbQuery.append("Select distinct billingService From BillingService billingService left outer join fetch billingService.serviceRates srs ");
	   sbQuery.append(" Where  billingService.baseCollection = :baseCollection ");
	   sbQuery.append(" and billingService.serviceState="+PropertiesConstants.SERVICE_STATUS_ACTIVE_PK);
	   
	   if (Validations.validateIsNotNull(billingServiceTo.getServiceRateToSelected()) && 
	     Validations.validateIsNotNull(billingServiceTo.getServiceRateToSelected().getLastEffectiveDate())  ){   
		   			
	    sbQuery.append(" and  :effectiveDate  between trunc(srs.initialEffectiveDate) and trunc(srs.endEffectiveDate)" );
		
	    sbQuery.append(" and ( srs.rateState = :stateExpired      or  srs.rateState = :stateActived )  "); // ServiceRate State ACTIVE and EXPIRED
	    
	   }else {
	   sbQuery.append(" and srs.rateState="+RateStateType.ACTIVED.getCode());
	   }
	   
	   Query query = em.createQuery(sbQuery.toString());
	   
	   query.setParameter("baseCollection", billingServiceTo.getBaseCollection()); 
	  
	   
	   if (Validations.validateIsNotNull(billingServiceTo.getServiceRateToSelected()) && 
	     Validations.validateIsNotNull(billingServiceTo.getServiceRateToSelected().getLastEffectiveDate())  ){
		   
		    query.setParameter("effectiveDate", billingServiceTo.getServiceRateToSelected().getLastEffectiveDate(),TemporalType.DATE);  
		    query.setParameter("stateExpired", RateStateType.EXPIRED.getCode() ); 		
			query.setParameter("stateActived", RateStateType.ACTIVED.getCode() ); 
	   }
	   
	   try{
		   listBillingService=query.getResultList();
	   } catch(NoResultException  nrex){
	    nrex.printStackTrace();
	   }
	   
	  }
	  
	  return listBillingService;
	 }
	
	 /**
 	 * Gets the all service rate.
 	 *
 	 * @param billingServiceId the billing service id
 	 * @return the all service rate
 	 * @throws ServiceException the service exception
 	 */
 	// Find All service rate
		public List<ServiceRate> getAllServiceRate(Long billingServiceId) throws ServiceException{
			
		 			
			List<ServiceRate>  serviceRateList =null; 
			  
			  if(Validations.validateIsNotNullAndNotEmpty(billingServiceId)){
			   StringBuilder sbQuery = new StringBuilder();
			   sbQuery.append(" select DISTINCT sr from ServiceRate sr   left join fetch sr.rateMovements ");
			   sbQuery.append(" where  1=1 ");
			   sbQuery.append(" AND sr.billingService.idBillingServicePk=:billingServiceId ");
			    
			   
			   Query query = em.createQuery(sbQuery.toString());
			   
			    
			   query.setParameter("billingServiceId", billingServiceId);
			   
			   
			   try{
				   
				   serviceRateList=query.getResultList();
				   
			   } catch(NoResultException  nrex){
			    nrex.printStackTrace();
			   }
			   
			  }
			  
			  return serviceRateList;
			
			
			
			
		}
		 
		
/**
 * Gets the billing services register actived.
 *
 * @param dateSystem the date system
 * @param idBillingService the id billing service
 * @return the billing services register actived
 */
// Search Billing on State Register to Activate
		public List<BillingService> getBillingServicesRegisterActived(Date dateSystem, Long idBillingService){
			
			StringBuilder sb = new StringBuilder();
			List<BillingService> billingServiceList = null;
			
			sb.append(" select bs from BillingService  bs  " );			
			sb.append("  where (bs.serviceState = :stateRegister or bs.serviceState = :stateActived ) " );
			 
			if(Validations.validateIsNotNull(dateSystem)){
				sb.append(" and  trunc(bs.initialEffectiveDate) <= trunc(:dateSystem) "  );
			}
			if(Validations.validateIsNotNull(idBillingService)){
				sb.append(" and bs.idBillingServicePk = :idBillingService "  );
			}
			
			Query query= em.createQuery(sb.toString());
			
						
			query.setParameter("stateRegister", PropertiesConstants.SERVICE_STATUS_REGISTER_PK  ); 		
			query.setParameter("stateActived", PropertiesConstants.SERVICE_STATUS_ACTIVE_PK  ); 
			
			if(Validations.validateIsNotNull(dateSystem)){
				query.setParameter("dateSystem", dateSystem);
			}
			if(Validations.validateIsNotNull(idBillingService)){
				query.setParameter("idBillingService", idBillingService);
			}
		 
			
			billingServiceList = query.getResultList();		
			 			
			return billingServiceList;  
		}
				
		

/**
 * Gets the billing services actived lock.
 *
 * @param dateSystem the date system
 * @return the billing services actived lock
 */
// Search Billing on State Active and Lock to Expired
		public List<BillingService> getBillingServicesActivedLock(Date dateSystem){
			
			StringBuilder sb = new StringBuilder();
			List<BillingService> billingServiceList = null;
			 
			sb.append(" select  bs from BillingService  bs   " );
			sb.append("  where (bs.serviceState = :stateActive or bs.serviceState = :stateLock )  " );
			
			if(Validations.validateIsNotNull(dateSystem)){
				sb.append(" and  trunc(bs.endEffectiveDate) < trunc(:dateSystem)    " );
			}
		
		
			Query query= em.createQuery(sb.toString());
			
			
			query.setParameter("stateActive", PropertiesConstants.SERVICE_STATUS_ACTIVE_PK  );
			query.setParameter("stateLock",   PropertiesConstants.SERVICE_STATUS_LOCK_PK     );
		
			if(Validations.validateIsNotNull(dateSystem)){
				query.setParameter("dateSystem", dateSystem);
			}
		 
			billingServiceList = query.getResultList();
						
			return billingServiceList; 
		}
				
		
		
		/**
		 * 		Service to ReCalculate on last dates		*.
		 *
		 * @param billingServiceTo the billing service to
		 * @return the billing service with service rateby date
		 */
		public BillingService getBillingServiceWithServiceRatebyDate(BillingServiceTo billingServiceTo){
			
		BillingService billingService=null; 
		
			if(Validations.validateIsNotNullAndNotEmpty(billingServiceTo.getIdBillingServicePk())){
				StringBuilder sbQuery = new StringBuilder();
				sbQuery.append("Select bs From BillingService bs left join fetch bs.serviceRates sr ");
				sbQuery.append(" Where bs.idBillingServicePk  =   :idBillingServicePk      ");		
				sbQuery.append(" and ( sr.rateState = :stateExpired      or  sr.rateState = :stateActived )  "); // ServiceRate State ACTIVE and EXPIRED
			
				sbQuery.append(" and  trunc(sr.initialEffectiveDate) <= 	 trunc(:initialDate)  ");

				Query query = em.createQuery(sbQuery.toString());
				
				query.setParameter("idBillingServicePk", billingServiceTo.getIdBillingServicePk());
				query.setParameter("stateExpired", RateStateType.EXPIRED.getCode() ); 		
				query.setParameter("stateActived", RateStateType.ACTIVED.getCode() ); 
				query.setParameter(GeneralConstants.FIELD_INITIAL_DATE, billingServiceTo.getInitialEffectiveDate());		
				
				try{
					Object entity=query.getSingleResult();
					billingService=(BillingService)entity;
				} catch(NoResultException  nrex){
					nrex.printStackTrace();
				}
				
			}
			
			
			return billingService;
		}
		
		
		
		/**
		 * Gets the billing service by processed service pk.
		 *
		 * @param processedServicePk the processed service pk
		 * @return the billing service by processed service pk
		 */
		public BillingService getBillingServiceByProcessedServicePk(Long processedServicePk) {
			
			BillingService billingService=null; 
		

			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("Select distinct bs From ProcessedService ps   inner join ps.billingService bs ");
			sbQuery.append(" Where ps.idProcessedServicePk = :processedServicePk ");		
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("processedServicePk", processedServicePk);		
			
			try{
				Object entity=query.getSingleResult();
				billingService=(BillingService)entity;
			} catch(NoResultException  nrex){
				nrex.printStackTrace();
			}
			
			return billingService;
		}
		
		/**
		 * Gets the billing service by service code.		 
		 */
		public BillingService getBillingServiceByServiceCode (Long serviceCode) {
			BillingService billingService=null; 
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("Select distinct bs From BillingService bs ");
			sbQuery.append(" Where bs.serviceCode = :serviceCode ");		
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("serviceCode", serviceCode.toString());		
			
			try{
				Object entity=query.getSingleResult();
				billingService=(BillingService)entity;
			} catch(NoResultException  nrex){
				nrex.printStackTrace();
			}
			return billingService;
		}
		
		public void updateStateRequest (RequestService requestService) {
			em.merge(requestService);
		}
		
		public BigDecimal getRateForRequestService (RequestService requestService) {
			BigDecimal rate = null;
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" select der.BUY_PRICE "); 			
			sbQuery.append(" from DAILY_EXCHANGE_RATES der ");
			sbQuery.append(" where der.DATE_RATE = :dateRate " );
			sbQuery.append("   and der.ID_CURRENCY = 1734 " );
	    	
			Query query = em.createNativeQuery(sbQuery.toString());
	    	query.setParameter("dateRate", requestService.getObsDate());
	    	rate = (BigDecimal) query.getSingleResult();
	    	return rate;
		}
		

		
		/**
		 * Gets the billing service by service code.		 
		 */
		public List<BillingEconomicActivity> getBillingEconomicActivityByBillingServicePk(Long idBillingServicePk) {
			List<BillingEconomicActivity> listBillingEconomicActivity=null; 
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("Select distinct bea From BillingEconomicActivity bea ");
			sbQuery.append(" join bea.billingService bs ");
			sbQuery.append(" Where bs.idBillingServicePk = :idBillingServicePk ");		
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idBillingServicePk", idBillingServicePk);		
			
			try{
				listBillingEconomicActivity=(List<BillingEconomicActivity>)query.getResultList();
			} catch(NoResultException  nrex){
				nrex.printStackTrace();
			}
			return listBillingEconomicActivity;
		}
		
		public void merge(BillingEconomicActivity billingEconomicActivity){
			billingEconomicActivity.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
			billingEconomicActivity.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
			billingEconomicActivity.setLastModifyDate(CommonsUtilities.currentDate());
			billingEconomicActivity.setLastModifyApp(userInfo.getLastModifyPriv());
			em.merge(billingEconomicActivity);
		}
		
		public Integer updateListServiceRate(List<ServiceRate> serviceRateList, Integer rateState) {
			LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
			
			List<Long> idsToUpdate = serviceRateList.stream()
						                .map(ServiceRate::getIdServiceRatePk)
						                .collect(Collectors.toList());
			
			StringBuilder sb = new StringBuilder();
			sb.append(" UPDATE ServiceRate ");
			sb.append(" SET rateState = :rateState,");
			sb.append("  lastModifyApp = :lastModifyApp,");
			sb.append("  lastModifyDate = :lastModifyDate,");
			sb.append("  lastModifyIp = :lastModifyIp,");
			sb.append("  lastModifyUser = :lastModifyUser");
			sb.append(" WHERE");
			sb.append(" idServiceRatePk in :lstPks");
			
			Query q = em.createQuery(sb.toString());
			q.setParameter("rateState", rateState);
			q.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
			q.setParameter("lastModifyDate", loggerUser.getAuditTime());
			q.setParameter("lastModifyIp", loggerUser.getIpAddress());
			q.setParameter("lastModifyUser", loggerUser.getUserName());
			q.setParameter("lstPks", idsToUpdate);
			
			return q.executeUpdate();
		}
}
