package com.pradera.billing.billingservice.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.ActionEvent;

import com.pradera.billing.billingcollection.facade.CollectionServiceMgmtServiceFacade;
import com.pradera.billing.billingservice.facade.BillingServiceMgmtServiceFacade;
import com.pradera.billing.billingservice.to.BillingRegServiceTo;
import com.pradera.billing.billingservice.type.RequestServiceT21;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.CollectionRecord;
import com.pradera.model.billing.ProcessedService;
import com.pradera.model.billing.RequestService;
import com.pradera.model.billing.type.ProcessedServiceType;

@DepositaryWebBean
@LoggerCreateBean
public class BillingRequestServiceBean extends GenericBaseBean implements Serializable {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	/** The billing service mgmt service facade. */
	@EJB BillingServiceMgmtServiceFacade billingServiceMgmtServiceFacade;
	/** The service code. */
	private String serviceCode;
	/** The billing service selection. */
	private BillingService billingServiceSelection;	
	/** The list service type. */
	private List<BillingService> listServiceType;
	/** The billing service data model. */
	private GenericDataModel<RequestService> billingServiceDataModel;
	/** The service type search selected. */
	private Long serviceTypeSearchSelected;
	/** The observation. */
	private String observation;
	/** The RequestService. */
	private RequestService serviceReq;
	/** The service type selected. */
	private Long serviceTypeSelected;
	/** The selected client*/
	private Integer selectedClient = new Integer(1);
	/** The source holder. */
	private Holder sourceHolder = new Holder();
	/** The participant selected. */
	private Long participantSelected;
	/** The client name*/
	private String clientName = null;
	/** The client nit*/
	private String clientNIT = null;
	/** The helper component facade. */
    @EJB private HelperComponentFacade helperComponentFacade;
    /** The initial date. */
	private Date initialDate=CommonsUtilities.currentDate();	
	/** The final date. */
	private Date finalDate=CommonsUtilities.currentDate();
	/** The validity final date. */
	private Date validityFinalDate;
	/** The validity initial date disable. */
	private Boolean validityInitialDateDisable=Boolean.FALSE;
	/** The validity final date disable. */
	private Boolean validityFinalDateDisable=Boolean.FALSE;	
	/** The validity final date disable. */
	private Boolean selected=Boolean.TRUE;
	/** The price transport*/
	private Integer priceTransport;
	/** The list service type. */
	private List<String> listService;
	/** The service price selected. */
	private String servicePriceSelected;
	private RequestService requestServiceSelection;
	
	/** The collection service mgmt service facade. */
	@EJB CollectionServiceMgmtServiceFacade collectionServiceMgmtServiceFacade;
	
	@PostConstruct
	public void init() throws ServiceException {
		listServiceType = (List<BillingService>) billingServiceMgmtServiceFacade.getBillingServiceReq();
		this.selected = true;
		this.requestServiceSelection = null;
	}
	
	public void loadRegistrationPage() throws ServiceException {		
		this.setViewOperationType(ViewOperationsType.REGISTER.getCode());				
		this.billingServiceSelection = null;
		this.serviceTypeSearchSelected = null;
		this.serviceReq = null;		
		this.clientName = null;
		this.clientNIT = null;
		this.observation = null;
		this.selected = true;
		this.priceTransport = null;
		this.servicePriceSelected = null;
		this.requestServiceSelection = null;
		this.participantSelected = null;
		this.sourceHolder = new Holder();
		listServiceType = (List<BillingService>) billingServiceMgmtServiceFacade.getBillingServiceReq();		
	}
	
	public void clear()	{ 
		this.billingServiceSelection = null;
		this.serviceTypeSearchSelected = null;
		this.observation = null;
		this.serviceReq = null;
		this.participantSelected = null;		
		this.clientName = null;
		this.clientNIT = null;
		this.selected = true;
		this.priceTransport = null;
		this.servicePriceSelected = null;
		this.requestServiceSelection = null;
		this.participantSelected = null;
		this.sourceHolder = new Holder();
		JSFUtilities.resetComponent("frmBillingServiceRegister");
	}  
	
	public void cleanResultBillingServiceDataModel() {		
		List<String> list = new ArrayList<String>();		
		listService = list;
		
		if(Validations.validateIsNotNullAndNotEmpty(billingServiceDataModel) && billingServiceDataModel.getRowCount() > 0) {			
			billingServiceDataModel = null ;			
		}
		if(serviceTypeSearchSelected == 32 || serviceTypeSearchSelected == 22 || serviceTypeSearchSelected == 23) {
			selected = false;
			if(serviceTypeSearchSelected == 32) {//POR TRANSPORTE DE VALORES
				list.add(RequestServiceT21.PROGRAMADO_LA_PAZ.getValue());
				list.add(RequestServiceT21.NO_PROGRAMADO_LA_PAZ.getValue());
				list.add(RequestServiceT21.NO_PROGRAMADO_CBBA_SC.getValue());
			}
			if(serviceTypeSearchSelected == 22) {//EMISION INFORMACION IMPRESA
				list.add(RequestServiceT21.RESUMEN_TENENCIAS_CUI.getValue());
				list.add(RequestServiceT21.RELACION_MOVIMIENTOS_CUI.getValue());
			}
			if(serviceTypeSearchSelected == 23) {//EMISION INFORMACION IMPRESA SIN CONTRATO ESPECIFICO
				list.add(RequestServiceT21.RELACION_TITULARES.getValue());
				list.add(RequestServiceT21.RELACION_TRANSFERENCIAS.getValue());
			}			
			listService = list;
		} else {
			selected = true;			
		}
	}	 

	public void validSourceHolder() {
		if(Validations.validateIsNotNull(sourceHolder) && Validations.validateIsNotNullAndPositive(sourceHolder.getIdHolderPk())) {
			HolderTO holderTO = new HolderTO();
			holderTO.setHolderId(sourceHolder.getIdHolderPk());
			holderTO.setFlagAllHolders(true);				
		} 
	}
	
	public void beforeconfirmRequest () {
		executeAction();
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
				            PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_REQUEST_SERVICE_BILLING));
		JSFUtilities.executeJavascriptFunction("cnfwBilling.show();");
	}
	
	public void beforerejectRequest () {		
		executeAction();
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
				            PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_REQUEST_SERVICE_REJECT));
		JSFUtilities.executeJavascriptFunction("cnfwReject.show();");
	}	
	
	public void beforeRegistrationListener() {
		if (Validations.validateIsNull(clientName) &&  Validations.validateIsNull(clientNIT)) {
			if(Validations.validateIsNull(sourceHolder)) {				
				executeAction();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
                                    PropertiesUtilities.getMessage(PropertiesConstants.WARNING_REQUEST_SERVICE_CLIENT));				
				JSFUtilities.executeJavascriptFunction("cnfWValidationDialog.show();");
				return;
			}
		}		
		if (!Validations.validateIsPositiveNumber(serviceTypeSearchSelected)) {
			executeAction();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
		                        PropertiesUtilities.getMessage(PropertiesConstants.WARNING_REQUEST_SERVICE));				
			JSFUtilities.executeJavascriptFunction("cnfWValidationDialog.show();");
			return;
		} else if (serviceTypeSearchSelected == 30) {
			if ("-1".equals(servicePriceSelected.toString())) {
				executeAction();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
						            PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_REQUEST_SERVICE_REGISTER));				
				JSFUtilities.executeJavascriptFunction("cnfwServiceReqSave.show();");				
			} else {
				executeAction();				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						            PropertiesUtilities.getMessage(PropertiesConstants.WARNING_REQUEST_SERVICE_SCALE));				
				JSFUtilities.executeJavascriptFunction("cnfWValidationDialog.show();");
				return;
			}
		} else {	
			if (!"-1".equals(servicePriceSelected.toString())) {				
				executeAction();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
						            PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_REQUEST_SERVICE_REGISTER));				
				JSFUtilities.executeJavascriptFunction("cnfwServiceReqSave.show();");
			} else {
				executeAction();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						            PropertiesUtilities.getMessage(PropertiesConstants.WARNING_REQUEST_SERVICE_SCALE));				
				JSFUtilities.executeJavascriptFunction("cnfWValidationDialog.show();");
				return;
			}
		}
		
	}
	
	@LoggerAuditWeb
	public void saveBillingService() {
		
		if(serviceTypeSearchSelected == 30) {
			priceTransport = RequestServiceT21.EMISION_CERTIFICACIONES_VARIAS.getCode();
		} else {
			if (servicePriceSelected.equals(RequestServiceT21.PROGRAMADO_LA_PAZ.getValue())) {
				priceTransport = RequestServiceT21.PROGRAMADO_LA_PAZ.getCode();
			} else if (servicePriceSelected.equals(RequestServiceT21.NO_PROGRAMADO_LA_PAZ.getValue())) {
				priceTransport = RequestServiceT21.NO_PROGRAMADO_LA_PAZ.getCode();
			} else if (servicePriceSelected.equals(RequestServiceT21.NO_PROGRAMADO_CBBA_SC.getValue())) {
				priceTransport = RequestServiceT21.NO_PROGRAMADO_CBBA_SC.getCode();
			} else if (servicePriceSelected.equals(RequestServiceT21.RESUMEN_TENENCIAS_CUI.getValue())) {
				priceTransport = RequestServiceT21.RESUMEN_TENENCIAS_CUI.getCode();
			} else if (servicePriceSelected.equals(RequestServiceT21.RELACION_MOVIMIENTOS_CUI.getValue())) {
				priceTransport = RequestServiceT21.RELACION_MOVIMIENTOS_CUI.getCode();			
			} else if (servicePriceSelected.equals(RequestServiceT21.RELACION_TITULARES.getValue())) {
				priceTransport = RequestServiceT21.RELACION_TITULARES.getCode();
			} else if (servicePriceSelected.equals(RequestServiceT21.RELACION_TRANSFERENCIAS.getValue())) {
				priceTransport = RequestServiceT21.RELACION_TRANSFERENCIAS.getCode();
			}		
		}		
		
		BillingService service = new BillingService();
		service = billingServiceMgmtServiceFacade.getBillingServiceByServiceCode(serviceTypeSearchSelected);
		
		Date today = getCurrentSystemDate();			
		
		serviceReq = new RequestService();
		serviceReq.setBillingService(service);
		serviceReq.setObsDate(today);
		serviceReq.setObsState(1);
		serviceReq.setObsText(observation);
		serviceReq.setServName(service.getServiceName());
		serviceReq.setServCode(service.getServiceCode());
		
		Holder holder = new Holder();		
		if(Validations.validateIsNotNull(sourceHolder) && Validations.validateIsNotNullAndPositive(sourceHolder.getIdHolderPk())) {
			holder = sourceHolder;
			serviceReq.setHolder(holder);
		}
		
		serviceReq.setClientName(clientName);
		serviceReq.setClientNit(clientNIT);
		serviceReq.setPriceTransport(priceTransport);
		
		try {
			billingServiceMgmtServiceFacade.saveServiceReq(serviceReq);
			executeAction();		
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					            PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_BILLING_SERVICE_REQUEST_SAVE));
			JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
			searchServiceBilling();
		} catch (ServiceException ex) {
			if(ex.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING, ex.getMessage());				
				JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
			}
		}		
	}
	
	@LoggerAuditWeb
	public void searchServiceBilling() {
		BillingRegServiceTo reqServideTo = new BillingRegServiceTo();
		reqServideTo.setInitialDate(initialDate);
		reqServideTo.setFinalDate(finalDate);
		if(Validations.validateIsNotNullAndPositive(serviceTypeSearchSelected)) {
			reqServideTo.setServCode(serviceTypeSearchSelected.toString());
		}
		List<RequestService> listReqService;
		try {
			listReqService = billingServiceMgmtServiceFacade.getListBillingReqService(reqServideTo);
			billingServiceDataModel = new GenericDataModel<RequestService>(listReqService);
		} catch (ServiceException e) {
			if(e.getErrorService()!=null){
				showExceptionMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING, e.getMessage());				
				JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
			}
		}
	}
	
	public void cleanOnSearch() {
		this.billingServiceDataModel=null;
		this.serviceTypeSearchSelected=null;
		this.requestServiceSelection = null;
	}
	
	public void confirmRequest(ActionEvent actionEvent) {
		if (requestServiceSelection.getObsState() == GeneralConstants.ONE_VALUE_INTEGER) {
			ProcessedService processedService = new ProcessedService();
			List<CollectionRecord> collectionRecordList = new ArrayList<CollectionRecord>();
			CollectionRecord collectionRecord = new CollectionRecord();	
			processedService.setBillingService(requestServiceSelection.getBillingService());
			processedService.setProcessedState(ProcessedServiceType.REGISTRADO.getCode());
			processedService.setOperationDate(CommonsUtilities.currentDate());
			processedService.setCalculationDate(CommonsUtilities.currentDate());		
			collectionRecord.setCollectionAmount(new BigDecimal(requestServiceSelection.getPriceTransport()));
			collectionRecord.setGrossAmount(new BigDecimal(requestServiceSelection.getPriceTransport()));		
			collectionRecord.setRecordState(ProcessedServiceType.REGISTRADO.getCode());
			collectionRecord.setCalculationDate(CommonsUtilities.currentDate());//
			collectionRecord.setOperationDate(CommonsUtilities.currentDate());
			collectionRecord.setIndBilled(GeneralConstants.ZERO_VALUE_INTEGER);				
			collectionRecord.setProcessedService(processedService);
			BigDecimal exchangeRateCurrent = billingServiceMgmtServiceFacade.getRateForRequestService(requestServiceSelection);
			collectionRecord.setExchangeRateCurrent(exchangeRateCurrent);
			BigDecimal collectionAmountBilled = exchangeRateCurrent.multiply(new BigDecimal(requestServiceSelection.getPriceTransport()));
			collectionRecord.setCollectionAmountBilled(collectionAmountBilled);
			collectionRecordList.add(collectionRecord);
			processedService.setCollectionRecords(collectionRecordList);		
			try {
				collectionServiceMgmtServiceFacade.saveProcessedService(processedService);
				requestServiceSelection.setObsState(2);
				billingServiceMgmtServiceFacade.updateStateRequest(requestServiceSelection);
			} catch(ServiceException e) {
				executeAction();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						            PropertiesUtilities.getMessage(PropertiesConstants.WARNING_REQUEST_SERVICE_CONFIRM));
				JSFUtilities.executeJavascriptFunction("cnfWCollectionDialogRollBack.show();");
			}
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					            PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_REQUEST_SERVICE_CONFIRM));
			JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
			searchServiceBilling();
		} else {
			executeAction();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					            PropertiesUtilities.getMessage(PropertiesConstants.WARNING_REQUEST_SERVICE_CONFIRM,""));
			JSFUtilities.executeJavascriptFunction("cnfWCollectionDialogRollBack.show();");
		}		
	}
	
	public void rejectRequest() {
		requestServiceSelection.setObsState(3);		
		billingServiceMgmtServiceFacade.updateStateRequest(requestServiceSelection);		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
                            PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_REQUEST_SERVICE_REJECT));
		JSFUtilities.executeJavascriptFunction("PF('cnfWConfirmDialog').show();");
		searchServiceBilling();
	}	
	
	public void radioSelection() {
    	
    } 
	
	public BillingService getBillingServiceSelection() {
		return billingServiceSelection;
	}
	public void setBillingServiceSelection(BillingService billingServiceSelection) {
		this.billingServiceSelection = billingServiceSelection;
	}	
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	public List<BillingService> getListServiceType() {
		return listServiceType;
	}
	public void setListServiceType(List<BillingService> listServiceType) {
		this.listServiceType = listServiceType;
	}
	public GenericDataModel<RequestService> getBillingServiceDataModel() {
		return billingServiceDataModel;
	}
	public void setBillingServiceDataModel(
			GenericDataModel<RequestService> billingServiceDataModel) {
		this.billingServiceDataModel = billingServiceDataModel;
	}
	public Long getServiceTypeSearchSelected() {
		return serviceTypeSearchSelected;
	}
	public void setServiceTypeSearchSelected(Long serviceTypeSearchSelected) {
		this.serviceTypeSearchSelected = serviceTypeSearchSelected;
	}
	public String getObservation() {
		return observation;
	}
	public void setObservation(String observation) {
		this.observation = observation;
	}
	public Long getServiceTypeSelected() {
		return serviceTypeSelected;
	}
	public void setServiceTypeSelected(Long serviceTypeSelected) {
		this.serviceTypeSelected = serviceTypeSelected;
	}
	public Integer getSelectedClient() {
		return selectedClient;
	}
	public void setSelectedClient(Integer selectedClient) {
		this.selectedClient = selectedClient;
	}
	public Holder getSourceHolder() {
		return sourceHolder;
	}
	public void setSourceHolder(Holder sourceHolder) {
		this.sourceHolder = sourceHolder;
	}
	public Long getParticipantSelected() {
		return participantSelected;
	}
	public void setParticipantSelected(Long participantSelected) {
		this.participantSelected = participantSelected;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getClientNIT() {
		return clientNIT;
	}
	public void setClientNIT(String clientNIT) {
		this.clientNIT = clientNIT;
	}
	public RequestService getServiceReq() {
		return serviceReq;
	}
	public void setServiceReq(RequestService serviceReq) {
		this.serviceReq = serviceReq;
	}
	public Date getInitialDate() {
		return initialDate;
	}
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	public Date getFinalDate() {
		return finalDate;
	}
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}
	public Date getValidityFinalDate() {
		return validityFinalDate;
	}
	public void setValidityFinalDate(Date validityFinalDate) {
		this.validityFinalDate = validityFinalDate;
	}
	public Boolean getValidityInitialDateDisable() {
		return validityInitialDateDisable;
	}
	public void setValidityInitialDateDisable(Boolean validityInitialDateDisable) {
		this.validityInitialDateDisable = validityInitialDateDisable;
	}
	public Boolean getValidityFinalDateDisable() {
		return validityFinalDateDisable;
	}
	public void setValidityFinalDateDisable(Boolean validityFinalDateDisable) {
		this.validityFinalDateDisable = validityFinalDateDisable;
	}
	public List<String> getListService() {
		return listService;
	}
	public void setListService(List<String> listService) {
		this.listService = listService;
	}
	public Boolean getSelected() {
		return selected;
	}
	public void setSelected(Boolean selected) {
		this.selected = selected;
	}
	public Integer getPriceTransport() {
		return priceTransport;
	}
	public void setPriceTransport(Integer priceTransport) {
		this.priceTransport = priceTransport;
	}
	public String getServicePriceSelected() {
		return servicePriceSelected;
	}
	public void setServicePriceSelected(String servicePriceSelected) {
		this.servicePriceSelected = servicePriceSelected;
	}	
	public RequestService getRequestServiceSelection() {
		return requestServiceSelection;
	}
	public void setRequestServiceSelection(RequestService requestServiceSelection) {
		this.requestServiceSelection = requestServiceSelection;
	}
	
}
