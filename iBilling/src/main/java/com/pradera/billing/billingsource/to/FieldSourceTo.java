package com.pradera.billing.billingsource.to;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class FieldSourceTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class FieldSourceTo implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The name. */
	private String 		name;
	
	/** The value. */
	private String 		value;
	
	/** The visibility. */
	private Boolean  	visibility;
	
	/** The parameter type. */
	private String  	parameterType;
	
	/** The posicion. */
	private Integer 	posicion;
	
	/** The description. */
	private String   	description;
	
	/** The level. */
	private Integer   	level;
	

//	
//	private Long		notPlaced;
//	private BigDecimal	price;
//	private Long 		idParticipantPk;
//	private String		idHolderPk;
//	private String 		idIssuerPk;
//	private String 		idSecurityCodePk;
//	private String		movementType;
//	private Date   		movementDate;
//	private String		securityClass;
//	private String 		idHolderAccountPk;
//	private String 		issuanceCode;
//	private BigDecimal	nominalValue;
//	private BigDecimal	currentNominalValue;
//	private Date   		registryDate;
//	private BigDecimal	placedAmount;
//	private BigDecimal	desmaterializedBalance;
//	private Long		currency;
//	private BigDecimal	totalBalance;
//	private Long		movementQuantity;
//	private BigDecimal	operationPrice;
//	private Date   		operationDate;
//	private Long		operationNumber;
//	private Long		idSourceParticipantPk;
//	private Long		idTargetParticipantPk;
//	private	Long		cantidadMovimientos;
//	private Long		accountNumber;

	/**
 * Instantiates a new field source to.
 */
	public FieldSourceTo() {
	}
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */



	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}



	/**
	 * Sets the name.
	 *
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}



	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}



	/**
	 * Sets the value.
	 *
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}



	/**
	 * Gets the visibility.
	 *
	 * @return the visibility
	 */
	public Boolean getVisibility() {
		return visibility;
	}



	/**
	 * Sets the visibility.
	 *
	 * @param visibility the visibility to set
	 */
	public void setVisibility(Boolean visibility) {
		this.visibility = visibility;
	}



	/**
	 * Gets the parameter type.
	 *
	 * @return the parameterType
	 */
	public String getParameterType() {
		return parameterType;
	}



	/**
	 * Sets the parameter type.
	 *
	 * @param parameterType the parameterType to set
	 */
	public void setParameterType(String parameterType) {
		this.parameterType = parameterType;
	}



	/**
	 * Gets the posicion.
	 *
	 * @return the posicion
	 */
	public Integer getPosicion() {
		return posicion;
	}



	/**
	 * Sets the posicion.
	 *
	 * @param posicion the posicion to set
	 */
	public void setPosicion(Integer posicion) {
		this.posicion = posicion;
	}



	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}



	/**
	 * Sets the description.
	 *
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}



	/**
	 * Gets the level.
	 *
	 * @return the level
	 */
	public Integer getLevel() {
		return level;
	}



	/**
	 * Sets the level.
	 *
	 * @param level the level to set
	 */
	public void setLevel(Integer level) {
		this.level = level;
	}

	/****/

		
}
