package com.pradera.billing.billingsource.to;



import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class QuerySourceTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class QuerySourceTo implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	
	/**  Query that will be  executed *. */
	private  String 	query;
	
	/**  Indicator if query should setting  dynamics date *. */
	private Boolean  date = Boolean.TRUE;

	/**
	 * Gets the query.
	 *
	 * @return the query
	 */
	public String getQuery() {
		return query;
	}

	/**
	 * Sets the query.
	 *
	 * @param query the query to set
	 */
	public void setQuery(String query) {
		this.query = query;
	}

	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public Boolean getDate() {
		return date;
	}

	/**
	 * Sets the date.
	 *
	 * @param date the date to set
	 */
	public void setDate(Boolean date) {
		this.date = date;
	}
	
	
	
	
	
	
}
