package com.pradera.billing.billingsource.to;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingServiceSourceInfo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class BillingServiceSourceInfo  implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	
	/** The name file. */
	private String 						nameFile ;
	
	/** The fields. */
	public Map<Integer, FieldSourceTo>  fields = new HashMap<Integer, FieldSourceTo>();
	
	/** The query source. */
	private QuerySourceTo 				querySource;
	
	/** The status. */
	private Integer						status;
	
	/** The execution date. */
	private Date						executionDate;
	
	/** The associate code. */
	private Integer						associateCode;
	
	/**
	 * Gets the name file.
	 *
	 * @return the name file
	 */
	public String getNameFile() {
		return nameFile;
	}
	
	/**
	 * Sets the name file.
	 *
	 * @param nameFile the new name file
	 */
	public void setNameFile(String nameFile) {
		this.nameFile = nameFile;
	}

	/**
	 * Gets the fields.
	 *
	 * @return the fields
	 */
	public Map<Integer, FieldSourceTo> getFields() {
		return fields;
	}
	
	/**
	 * Sets the fields.
	 *
	 * @param fields the fields
	 */
	public void setFields(Map<Integer, FieldSourceTo> fields) {
		this.fields = fields;
	}
			
	
	/**
	 * Sets the field source to.
	 *
	 * @param position the position
	 * @param fieldSourceTo the field source to
	 */
	public void setFieldSourceTo(Integer position , FieldSourceTo fieldSourceTo){
		fields.put(position, fieldSourceTo);
	}
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}
	
	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	/**
	 * Gets the query source.
	 *
	 * @return the query source
	 */
	public QuerySourceTo getQuerySource() {
		return querySource;
	}
	
	/**
	 * Sets the query source.
	 *
	 * @param querySource the new query source
	 */
	public void setQuerySource(QuerySourceTo querySource) {
		this.querySource = querySource;
	}
	
	/**
	 * Gets the execution date.
	 *
	 * @return the execution date
	 */
	public Date getExecutionDate() {
		return executionDate;
	}
	
	/**
	 * Sets the execution date.
	 *
	 * @param executionDate the new execution date
	 */
	public void setExecutionDate(Date executionDate) {
		this.executionDate = executionDate;
	}
	
	/**
	 * Gets the associate code.
	 *
	 * @return the associateCode
	 */
	public Integer getAssociateCode() {
		return associateCode;
	}
	
	/**
	 * Sets the associate code.
	 *
	 * @param associateCode the associateCode to set
	 */
	public void setAssociateCode(Integer associateCode) {
		this.associateCode = associateCode;
	}
	
	
	
	
	
}
