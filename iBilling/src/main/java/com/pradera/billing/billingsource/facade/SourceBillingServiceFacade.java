package com.pradera.billing.billingsource.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.hibernate.exception.SQLGrammarException;

import com.pradera.billing.billingcollection.service.BaseCollectionMgmtServiceBean;
import com.pradera.billing.billingcollection.to.CollectionRecordDetailTo;
import com.pradera.billing.billingcollection.to.CollectionRecordTo;
import com.pradera.billing.billingservice.to.BillingServiceTo;
import com.pradera.billing.billingsource.service.SourceBillingServiceBean;
import com.pradera.billing.billingsource.to.BillingServiceSourceDetailTo;
import com.pradera.billing.billingsource.to.BillingServiceSourceInfo;
import com.pradera.billing.billingsource.to.BillingServiceSourceLoggerTo;
import com.pradera.billing.billingsource.to.BillingServiceSourceTo;
import com.pradera.billing.billingsource.to.FieldSourceTo;
import com.pradera.billing.common.facade.BusinessLayerCommonFacade;
import com.pradera.billing.common.utils.BillingUtils;
import com.pradera.billing.common.utils.SequenceGeneratorCode;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.BillingServiceSource;
import com.pradera.model.billing.type.BillingServiceFieldSourceType;
import com.pradera.model.billing.type.BillingServiceSourceStateType;
import com.pradera.model.billing.type.EntityCollectionType;
import com.pradera.model.generalparameter.type.DailyExchangeRoleType;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SourceBillingServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class SourceBillingServiceFacade { 

	/** The user info. */
	@Inject
	UserInfo userInfo;	

	/** The source billing service bean. */
	@EJB
	SourceBillingServiceBean sourceBillingServiceBean;
 
	/** The business layer common facade. */
	@EJB
	BusinessLayerCommonFacade 			businessLayerCommonFacade;
	
	/** The transaction registry. */
	@Resource
	TransactionSynchronizationRegistry transactionRegistry;
	
	/** The sequence generator code. */
	@EJB
	SequenceGeneratorCode  				sequenceGeneratorCode;
	@EJB
	 BaseCollectionMgmtServiceBean 		baseCollectionMgmtServiceBean;
 
	/**
	 * Find billing service source.
	 *
	 * @param billingSourcePk the billing source pk
	 * @return the billing service source
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public BillingServiceSource findBillingServiceSource(String billingSourcePk){
		
		return sourceBillingServiceBean.find(new Long(billingSourcePk), BillingServiceSource.class);
		
	}
	
	/**
	 * Save billing service source.
	 *
	 * @param billingServiceSource the billing service source
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_REGISTRATION)	
	public void saveBillingServiceSource(BillingServiceSource billingServiceSource){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		sourceBillingServiceBean.saveBillingServiceSource(billingServiceSource);
	}
	
	
	/**
	 * Load billing service source.
	 *
	 * @param billingServiceSourceTo the billing service source to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<BillingServiceSource> loadBillingServiceSource(BillingServiceSourceTo billingServiceSourceTo) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		List<BillingServiceSource> listBillingServiceSource = new ArrayList<BillingServiceSource>();
		
		listBillingServiceSource = sourceBillingServiceBean.searchAllBillingServiceSource(billingServiceSourceTo); 
		
		return listBillingServiceSource;
	}
	
	/**
	 * Find billing service source by pk.
	 *
	 * @param idBillingServiceSourcePk the id billing service source pk
	 * @return the billing service source
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public BillingServiceSource findBillingServiceSourceByPk(Long idBillingServiceSourcePk) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		
		return sourceBillingServiceBean.find(idBillingServiceSourcePk, BillingServiceSource.class);
	}
	
	/**
	 * Find billing service source by billing service.
	 *
	 * @param billingServiceTo the billing service to
	 * @return the billing service source
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public BillingServiceSource findBillingServiceSourceByBillingService(BillingServiceTo billingServiceTo) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		
		return sourceBillingServiceBean.findBillingServiceSourceByBillingService(billingServiceTo);
	}
	
	/**
	 * Get  XML to Physical File .
	 *
	 * @param idBillingServiceSourcePk the id billing service source pk
	 * @return the billing service source content
	 */
   public byte[] getBillingServiceSourceContent(Long idBillingServiceSourcePk) {
	   
	   Map<String, Object> parameters = new HashMap<String, Object>();
	   parameters.put("idBillingServiceSourcePk", idBillingServiceSourcePk);
	   return (byte[])sourceBillingServiceBean.findObjectByNamedQuery(BillingServiceSource.BILLING_SERVICE_SOURCE_CONTENT_BY_PK, parameters);
   }	   
   
   
   /**
    * Load accounting source info by id.
    *
    * @param id the id
    * @return the billing service source info
    * @throws ServiceException the service exception
    */
   @TransactionAttribute(TransactionAttributeType.REQUIRED)
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)	
	public BillingServiceSourceInfo	 loadAccountingSourceInfoById(Long id) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		
		byte[] file =	getBillingServiceSourceContent(id);
		return  BillingUtils.loadXMLToObjectSource(file);
		
	}
	
	/**
	 * Update billing service source.
	 *
	 * @param billingServiceSourceUpdate the billing service source update
	 * @throws ServiceException the service exception
	 */
	// UPDATE  updateBillingServiceSource
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_MODIFICATION)
	public void updateBillingServiceSource(BillingServiceSource billingServiceSourceUpdate) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		
		BillingServiceSource billingServiceSource = sourceBillingServiceBean.find(BillingServiceSource.class, billingServiceSourceUpdate.getIdBillingServiceSourcePk());
		
		if( Validations.validateIsNotNullAndNotEmpty(billingServiceSourceUpdate.getStatus()) && 
				!(billingServiceSourceUpdate.getStatus().equals(billingServiceSource.getStatus())) ) {
			
			billingServiceSource.setStatus(BillingServiceSourceStateType.DELETED.getCode()); 
				
				// Disassociate source from the Matrix Detail
//				BillingServiceMatrixDetail accountingMatrixDetail = accountingMatrixServiceFacade.findBillingServiceMatrixDetail(accountingSource.getIdBillingServiceSourcePk());
				
//				if(Validations.validateIsNotNullAndNotEmpty(accountingMatrixDetail)){
//					accountingMatrixDetail.setBillingServiceSource(null);
//					sourceBillingServiceBean.update(accountingMatrixDetail);
//					
//					// Change the status on Schema
//					Long idBillingServiceSchema = accountingMatrixDetail.getBillingServiceMatrix().getBillingServiceSchema().getIdBillingServiceSchemaPk();
//					BillingServiceSchema accountingSchema = schemaBillingServiceServiceBean.find(BillingServiceSchema.class,idBillingServiceSchema);
//					accountingSchema.setStatus(BillingServiceSchemaStateType.WITH_OUT_SOURCE.getCode());
//					schemaBillingServiceServiceBean.update(accountingSchema);
//				}
		}else {
		
			billingServiceSource.setDescriptionSource(billingServiceSourceUpdate.getDescriptionSource());
				
				if (Validations.validateIsNotNull(billingServiceSourceUpdate.getFileXml())){
					billingServiceSource.setFileXml(billingServiceSourceUpdate.getFileXml());
				}
		}
	
	}
	
	/**
	 * *
	 * Ejecuta el query del archivo y del resultado lo guarda en memoria en un listado de BillingServiceSourceLoggerTo.
	 *
	 * @param parameters the parameters
	 * @param billingServiceSourceInfo the billing service source info
	 * @return List BillingServiceSourceLoggerTo
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@ProcessAuditLogger(process=BusinessProcessType.BUSINESS_PROCESS_QUERY)
	public List<BillingServiceSourceLoggerTo> fillBillingServiceSourceLogger(Map parameters, BillingServiceSourceInfo billingServiceSourceInfo)
			throws ServiceException{
		
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());

		List<BillingServiceSourceDetailTo>  billingServiceSourceDetailList = null;
		/**Contiene una lista de Logger del resultado del query**/
		List<BillingServiceSourceLoggerTo>  listBillingServiceSourceLoggerTo=new ArrayList<BillingServiceSourceLoggerTo>();
		BillingServiceSourceLoggerTo		billingServiceSourceLoggerTo=null;
		BillingServiceSourceDetailTo		billingServiceSourceDetail = null;
		
		List<Object> dataMap=null;
		try{
			dataMap=sourceBillingServiceBean.executeBillingServiceSource( parameters, billingServiceSourceInfo);
			
		}catch (Exception ex) {
			if(ex.getCause() instanceof SQLGrammarException){
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}
			
		}
		if(dataMap==null || (dataMap!=null && dataMap.size()==0)){
			throw new ServiceException(ErrorServiceType.BILLING_SERVICE_RECEIPT_SOURCE_INFO_NO_RESULT);
		}
		
		/** Load list of field's position ***/
		Map<Integer, FieldSourceTo>	  fieldSourceMap	=  billingServiceSourceInfo.getFields();		
		
		/*** Data for each Tupla **/
		
		int numberTuplas	= dataMap.size();
		
		int numberColumns	= ( (Object[])dataMap.get(0) ).length;
		
		
		List<Integer> positionsData =  new ArrayList<Integer>();	
		for (int position = 0; position < numberColumns; position++) {
			positionsData.add(position);
		}
		
		Collection<FieldSourceTo> fieldSourceToCollection = fieldSourceMap.values();
		
		Object[] fieldSourceToArr =fieldSourceToCollection.toArray();
		
		

		
		for (int i = 0; i < numberTuplas; i++) {
			/**Tuple in The DataBase */
			Object tupla = 	dataMap.get(i);
			
			billingServiceSourceLoggerTo= new BillingServiceSourceLoggerTo();
			billingServiceSourceDetailList = new ArrayList<BillingServiceSourceDetailTo>();
			/**
			 * 	Begin Load List of Field with value  for each tupla
			 */
			
			for (int j = 0; j < fieldSourceToArr.length; j++) {
				FieldSourceTo  fieldSourceTo	= (FieldSourceTo) fieldSourceToArr[j];
				billingServiceSourceDetail	= new BillingServiceSourceDetailTo();
				
				int positionTemp = fieldSourceTo.getPosicion();
				if (positionsData.contains(positionTemp)){
					
					/**Del resultado del query se comparan las palabras reservadas y se guarda en memoria en billingServiceSourceDetail**/
					
					if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.PARTICIPANT.getValue())){//LONG
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ?  ((BigDecimal)((Object[])tupla)[positionTemp]).toString():BigDecimal.ZERO.toString());
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.HOLDER_ACCOUNT.getValue())){//STRING
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ? (((Object[])tupla)[positionTemp]).toString():GeneralConstants.EMPTY_STRING);
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.SECURITY.getValue())){//STRING
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ? (((Object[])tupla)[positionTemp]).toString():GeneralConstants.EMPTY_STRING);
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.ISSUER.getValue())){//STRING
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ? (((Object[])tupla)[positionTemp]).toString():GeneralConstants.EMPTY_STRING);
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.HOLDER.getValue())){//STRING
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ? (((Object[])tupla)[positionTemp]).toString():GeneralConstants.EMPTY_STRING);
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.MOVEMENT_TYPE.getValue())){//STRING
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ? (((Object[])tupla)[positionTemp]).toString():GeneralConstants.EMPTY_STRING);
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.SECURITY_CLASS.getValue())){//STRING
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ? (((Object[])tupla)[positionTemp]).toString():GeneralConstants.EMPTY_STRING);
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.ISSUANCE_CODE.getValue())){//STRING
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ? (((Object[])tupla)[positionTemp]).toString():GeneralConstants.EMPTY_STRING);
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.MOVEMENT_DATE.getValue())){//DATE
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ? CommonsUtilities.convertDatetoString(((Date)((Object[])tupla)[positionTemp])):null);
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.CURRENCY.getValue())){//LONG
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ?  ((BigDecimal)((Object[])tupla)[positionTemp]).toString():BigDecimal.ZERO.toString());
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.MOVEMENT_QUANTITY.getValue())){//LONG
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ? ((BigDecimal)((Object[])tupla)[positionTemp]).toString():BigDecimal.ZERO.toString());
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.SOURCE_PARTICIPANT.getValue())){//LONG
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ? ((BigDecimal)((Object[])tupla)[positionTemp]).toString():BigDecimal.ZERO.toString());
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.TARGET_PARTICIPANT.getValue())){//LONG
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ? ((BigDecimal)((Object[])tupla)[positionTemp]).toString():BigDecimal.ZERO.toString());
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.ACCOUNT_NUMBER.getValue())){//LONG
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ? ((BigDecimal)((Object[])tupla)[positionTemp]).toString():BigDecimal.ZERO.toString());
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.PRICE.getValue())){//BIG DECIMAL
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ? ((BigDecimal)((Object[])tupla)[positionTemp]).toString():BigDecimal.ZERO.toString());
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.NOT_PLACED.getValue())){//BIG DECIMAL
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ? ((BigDecimal)((Object[])tupla)[positionTemp]).toString():BigDecimal.ZERO.toString());
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.NOMINAL_VALUE.getValue())){//BIG DECIMAL
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ? ((BigDecimal)((Object[])tupla)[positionTemp]).toString():BigDecimal.ZERO.toString());
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.REGISTRY_DATE.getValue())){//DATE
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ? CommonsUtilities.convertDatetoString(((Date)((Object[])tupla)[positionTemp])):null);
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.CURRENT_NOMINAL_VALUE.getValue())){//BIG DECIMAL
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ? ((BigDecimal)((Object[])tupla)[positionTemp]).toString():BigDecimal.ZERO.toString());
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.PLACED_AMOUNT.getValue())){//BIG DECIMAL
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ? ((BigDecimal)((Object[])tupla)[positionTemp]).toString():BigDecimal.ZERO.toString());
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.DESMATERIALIZED_BALANCE.getValue())){//BIG DECIMAL
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ? ((BigDecimal)((Object[])tupla)[positionTemp]).toString():BigDecimal.ZERO.toString());
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.TOTAL_BALANCE.getValue())){//BIG DECIMAL
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ? ((BigDecimal)((Object[])tupla)[positionTemp]).toString():BigDecimal.ZERO.toString());
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.OPERATION_PRICE.getValue())){//BIG DECIMAL
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ? ((BigDecimal)((Object[])tupla)[positionTemp]).toString():BigDecimal.ZERO.toString());
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.OPERATION_DATE.getValue())){//DATE
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ? CommonsUtilities.convertDatetoString(((Date)((Object[])tupla)[positionTemp])):null);
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.OPERATION_NUMBER.getValue())){//LONG
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ? ((BigDecimal)((Object[])tupla)[positionTemp]).toString():BigDecimal.ZERO.toString());
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.CANTIDAD_MOVIMIENTOS.getValue())){//LONG
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ? ((BigDecimal)((Object[])tupla)[positionTemp]).toString():BigDecimal.ZERO.toString());
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.BALLOT_SEQUENTIAL.getValue())){//STRING
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ? (((Object[])tupla)[positionTemp]).toString():GeneralConstants.EMPTY_STRING);
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.NEGOTIATION_MODALITY.getValue())){//LONG
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ? ((BigDecimal)((Object[])tupla)[positionTemp]).toString():BigDecimal.ZERO.toString());
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.ROLE.getValue())){//LONG
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ? ((BigDecimal)((Object[])tupla)[positionTemp]).toString():BigDecimal.ZERO.toString());
					}else if (fieldSourceTo.getParameterType().equalsIgnoreCase(BillingServiceFieldSourceType.CANTIDAD_MOVIMIENTOS.getValue())){//LONG
						billingServiceSourceDetail.setParameterValue(((Object[])tupla)[positionTemp]!=null ? ((BigDecimal)((Object[])tupla)[positionTemp]).toString():BigDecimal.ZERO.toString());
					}
					System.out.println(fieldSourceTo.getParameterType());
					/**
					 * Los demas campos del objeto billingServiceSourceDetail
					 * Parameter Name
					 * Parameter Description
					 * Parameter Type
					 * Level
					 * Position
					 */
					billingServiceSourceDetail.setParameterName(fieldSourceTo.getName());
					billingServiceSourceDetail.setParameterDescription(fieldSourceTo.getDescription());
					billingServiceSourceDetail.setParameterType(BillingServiceFieldSourceType.get(fieldSourceTo.getParameterType()).getCode().intValue());
					billingServiceSourceDetail.setLevel(fieldSourceTo.getLevel());
					billingServiceSourceDetail.setPosition(fieldSourceTo.getPosicion());
					billingServiceSourceDetailList.add(billingServiceSourceDetail);
				 }
				
				if (Validations.validateIsNullOrEmpty(fieldSourceTo.getValue())){
					fieldSourceTo.setValue(" ");//vacio
				}
				
			}//END FOR 
			
			
			
			/**  End
			  *	 Create and Load List of Logger Detail for each tupla		 	
			  **/
			billingServiceSourceLoggerTo.setListBillingServiceSourceDetailTo(billingServiceSourceDetailList);
			listBillingServiceSourceLoggerTo.add(billingServiceSourceLoggerTo);
			
			
		}//END FOR

		return listBillingServiceSourceLoggerTo;
		
	}
	
	

	/**
	 * Processing collections.
	 *
	 * @param parameters the parameters
	 * @param listBillingServiceSourceLoggerTo fill List To CollectionRecordTo
	 * @return the list
	 */
	public List<CollectionRecordTo> processForBillingSource(Map parameters, 
										List<BillingServiceSourceLoggerTo> listBillingServiceSourceLoggerTo){
		
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		
		List<CollectionRecordTo> listCollectionRecordTo=new ArrayList<CollectionRecordTo>();
		CollectionRecordTo collectionRecordTo=null;
		CollectionRecordDetailTo collectionRecordDetailTo=null;
		

		for (BillingServiceSourceLoggerTo billingServiceSourceLoggerTo : listBillingServiceSourceLoggerTo) {
			List<BillingServiceSourceDetailTo> listBillingServiceSourceDetailTo=billingServiceSourceLoggerTo.getListBillingServiceSourceDetailTo();
			CollectionRecordTo collectionRecordToTemp=null;
			collectionRecordDetailTo=null;
			Object entity=null;
			
			/**Each List Object listBillingServiceSourceDetailTo is Object CollectionRecord or CollectionRecordDetail*/
			for (BillingServiceSourceDetailTo billingServiceSourceDetailTo : listBillingServiceSourceDetailTo) {
				if(Validations.validateIsNullOrEmpty(billingServiceSourceDetailTo.getParameterValue())){
					continue;
				}


				
				/**Each Object billingServiceSourceDetailTo is field to CollectionRecord and/or CollectionRecordDetails**/
				if(billingServiceSourceDetailTo.getParameterType().equals(BillingServiceFieldSourceType.PARTICIPANT.getCode())){
					/**If Participant was Collection Record is EntityCollection Participant*/
					if(EntityCollectionType.PARTICIPANTS.getCode().equals(billingService.getEntityCollection())){
						collectionRecordToTemp=findEntityAtListTemporalTo(listCollectionRecordTo, 
										billingServiceSourceDetailTo.getParameterValue(), billingService);
						entity=billingServiceSourceDetailTo.getParameterValue();
					}else{
						/**If Participant was Collection Record Detail*/
						collectionRecordDetailTo=getEntityCollectionRecordDetail(billingServiceSourceDetailTo.getParameterValue(), 
								EntityCollectionType.PARTICIPANTS.getCode(), collectionRecordDetailTo);
					}
				}else if(billingServiceSourceDetailTo.getParameterType().equals(BillingServiceFieldSourceType.HOLDER.getCode())){
					/**If Holder was Collection Record is EntityCollection Holder*/
					if(EntityCollectionType.HOLDERS.getCode().equals(billingService.getEntityCollection())){
						collectionRecordToTemp=findEntityAtListTemporalTo(listCollectionRecordTo, 
										billingServiceSourceDetailTo.getParameterValue(), billingService);
						entity=billingServiceSourceDetailTo.getParameterValue();
					}else{
						/**If Holder was Collection Record Detail*/
						collectionRecordDetailTo=getEntityCollectionRecordDetail(billingServiceSourceDetailTo.getParameterValue(), 
								EntityCollectionType.HOLDERS.getCode(),  collectionRecordDetailTo);
					}
				}else if(billingServiceSourceDetailTo.getParameterType().equals(BillingServiceFieldSourceType.ISSUER.getCode())){
					/**If Issuer was Collection Record is EntityCollection Issuer*/
					if(EntityCollectionType.ISSUERS.getCode().equals(billingService.getEntityCollection())){
						collectionRecordToTemp=findEntityAtListTemporalTo(listCollectionRecordTo, 
										billingServiceSourceDetailTo.getParameterValue(), billingService);
						entity=billingServiceSourceDetailTo.getParameterValue();
					}else{
						/**If Issuer was Collection Record Detail*/
						collectionRecordDetailTo=getEntityCollectionRecordDetail(billingServiceSourceDetailTo.getParameterValue(), 
								EntityCollectionType.ISSUERS.getCode(), collectionRecordDetailTo);
					}
				}else if(billingServiceSourceDetailTo.getParameterType().equals(BillingServiceFieldSourceType.SECURITY.getCode())){
					if(Validations.validateIsNull(collectionRecordDetailTo)){
						collectionRecordDetailTo=new CollectionRecordDetailTo();
					}
					collectionRecordDetailTo.setIdSecurityCodeFk(billingServiceSourceDetailTo.getParameterValue());
				}else if(billingServiceSourceDetailTo.getParameterType().equals(BillingServiceFieldSourceType.SECURITY_CLASS.getCode())){
					if(Validations.validateIsNull(collectionRecordDetailTo)){
						collectionRecordDetailTo=new CollectionRecordDetailTo();
					}
					collectionRecordDetailTo.setSecurityClass(Integer.parseInt(billingServiceSourceDetailTo.getParameterValue()));
				}else if(billingServiceSourceDetailTo.getParameterType().equals(BillingServiceFieldSourceType.HOLDER_ACCOUNT.getCode())){
					if(Validations.validateIsNull(collectionRecordDetailTo)){
						collectionRecordDetailTo=new CollectionRecordDetailTo();
					}
					collectionRecordDetailTo.setHolderAccount(Long.parseLong(billingServiceSourceDetailTo.getParameterValue()));
				}else if(billingServiceSourceDetailTo.getParameterType().equals(BillingServiceFieldSourceType.ISSUANCE_CODE.getCode())){
					if(Validations.validateIsNull(collectionRecordDetailTo)){
						collectionRecordDetailTo=new CollectionRecordDetailTo();
					}
					collectionRecordDetailTo.setIdIssuanceCodePk(billingServiceSourceDetailTo.getParameterValue());
				}else if(billingServiceSourceDetailTo.getParameterType().equals(BillingServiceFieldSourceType.NOT_PLACED.getCode())){
					if(Validations.validateIsNull(collectionRecordDetailTo)){
						collectionRecordDetailTo=new CollectionRecordDetailTo();
					}
					collectionRecordDetailTo.setPercentage(new BigDecimal(billingServiceSourceDetailTo.getParameterValue()));
					
				}else if(billingServiceSourceDetailTo.getParameterType().equals(BillingServiceFieldSourceType.CURRENCY.getCode())){
					if(Validations.validateIsNull(collectionRecordDetailTo)){
						collectionRecordDetailTo=new CollectionRecordDetailTo();
					}
					collectionRecordDetailTo.setCurrencyOrigin(Integer.parseInt(billingServiceSourceDetailTo.getParameterValue()));
				}
				else if(billingServiceSourceDetailTo.getParameterType().equals(BillingServiceFieldSourceType.MOVEMENT_TYPE.getCode())){
					if(Validations.validateIsNull(collectionRecordDetailTo)){
						collectionRecordDetailTo=new CollectionRecordDetailTo();
					}
					collectionRecordDetailTo.setIdMovementTypePk(Long.parseLong(billingServiceSourceDetailTo.getParameterValue()));;
				}
				else if(billingServiceSourceDetailTo.getParameterType().equals(BillingServiceFieldSourceType.OPERATION_NUMBER.getCode())){
					if(Validations.validateIsNull(collectionRecordDetailTo)){
						collectionRecordDetailTo=new CollectionRecordDetailTo();
					}
					collectionRecordDetailTo.setOperationNumber(Long.parseLong(billingServiceSourceDetailTo.getParameterValue()));;
				}else if(billingServiceSourceDetailTo.getParameterType().equals(BillingServiceFieldSourceType.DESMATERIALIZED_BALANCE.getCode())||
						billingServiceSourceDetailTo.getParameterType().equals(BillingServiceFieldSourceType.MOVEMENT_QUANTITY.getCode())||
						billingServiceSourceDetailTo.getParameterType().equals(BillingServiceFieldSourceType.CANTIDAD_MOVIMIENTOS.getCode())||
						billingServiceSourceDetailTo.getParameterType().equals(BillingServiceFieldSourceType.TOTAL_BALANCE.getCode())){
					if(Validations.validateIsNull(collectionRecordDetailTo)){
						collectionRecordDetailTo=new CollectionRecordDetailTo();
					}
					collectionRecordDetailTo.setMovementQuantity(Long.parseLong(billingServiceSourceDetailTo.getParameterValue()));
				}else if(billingServiceSourceDetailTo.getParameterType().equals(BillingServiceFieldSourceType.MOVEMENT_DATE.getCode())||
						billingServiceSourceDetailTo.getParameterType().equals(BillingServiceFieldSourceType.REGISTRY_DATE.getCode())){
					if(Validations.validateIsNull(collectionRecordDetailTo)){
						collectionRecordDetailTo=new CollectionRecordDetailTo();
					}
					collectionRecordDetailTo.setMovementDate(CommonsUtilities.convertStringtoDate(billingServiceSourceDetailTo.getParameterValue()));
				}else if(billingServiceSourceDetailTo.getParameterType().equals(BillingServiceFieldSourceType.OPERATION_DATE.getCode())){
					if(Validations.validateIsNull(collectionRecordDetailTo)){
						collectionRecordDetailTo=new CollectionRecordDetailTo();
					}
					collectionRecordDetailTo.setOperationDate(CommonsUtilities.convertStringtoDate(billingServiceSourceDetailTo.getParameterValue()));
				}
				else if(billingServiceSourceDetailTo.getParameterType().equals(BillingServiceFieldSourceType.NOMINAL_VALUE.getCode())||
						billingServiceSourceDetailTo.getParameterType().equals(BillingServiceFieldSourceType.CURRENT_NOMINAL_VALUE.getCode())){
					if(Validations.validateIsNull(collectionRecordDetailTo)){
						collectionRecordDetailTo=new CollectionRecordDetailTo();
					}
					collectionRecordDetailTo.setNominalValue(new BigDecimal(billingServiceSourceDetailTo.getParameterValue()));;
				}else if(billingServiceSourceDetailTo.getParameterType().equals(BillingServiceFieldSourceType.ACCOUNT_NUMBER.getCode())){
					if(Validations.validateIsNull(collectionRecordDetailTo)){
						collectionRecordDetailTo=new CollectionRecordDetailTo();
					}
					collectionRecordDetailTo.setHolderAccount(Long.parseLong(billingServiceSourceDetailTo.getParameterValue()));;
				}else if(billingServiceSourceDetailTo.getParameterType().equals(BillingServiceFieldSourceType.BALLOT_SEQUENTIAL.getCode())){
					if(Validations.validateIsNull(collectionRecordDetailTo)){
						collectionRecordDetailTo=new CollectionRecordDetailTo();
					}
					collectionRecordDetailTo.setBallotSequential(billingServiceSourceDetailTo.getParameterValue());
				}
				
				
				
				else if(billingServiceSourceDetailTo.getParameterType().equals(BillingServiceFieldSourceType.NEGOTIATION_MODALITY.getCode())){
					if(Validations.validateIsNull(collectionRecordDetailTo)){
						collectionRecordDetailTo=new CollectionRecordDetailTo();
					}
					collectionRecordDetailTo.setModality(Integer.parseInt(billingServiceSourceDetailTo.getParameterValue()));
				}else if(billingServiceSourceDetailTo.getParameterType().equals(BillingServiceFieldSourceType.ROLE.getCode())){
					if(Validations.validateIsNull(collectionRecordDetailTo)){
						collectionRecordDetailTo=new CollectionRecordDetailTo();
					}
					collectionRecordDetailTo.setRole(Integer.parseInt(billingServiceSourceDetailTo.getParameterValue()));
				}else if(billingServiceSourceDetailTo.getParameterType().equals(BillingServiceFieldSourceType.SOURCE_PARTICIPANT.getCode())){
					if(Validations.validateIsNull(collectionRecordDetailTo)){
						collectionRecordDetailTo=new CollectionRecordDetailTo();
					}
					collectionRecordDetailTo.setIdSourceParticipant(Long.parseLong(billingServiceSourceDetailTo.getParameterValue()));
				}else if(billingServiceSourceDetailTo.getParameterType().equals(BillingServiceFieldSourceType.TARGET_PARTICIPANT.getCode())){
					if(Validations.validateIsNull(collectionRecordDetailTo)){
						collectionRecordDetailTo=new CollectionRecordDetailTo();
					}
					collectionRecordDetailTo.setIdTargetParticipant(Long.parseLong(billingServiceSourceDetailTo.getParameterValue()));
				}else if(billingServiceSourceDetailTo.getParameterType().equals(BillingServiceFieldSourceType.OPERATION_PRICE.getCode())){
					if(Validations.validateIsNull(collectionRecordDetailTo)){
						collectionRecordDetailTo=new CollectionRecordDetailTo();
					}
					collectionRecordDetailTo.setOperationPrice(new BigDecimal(billingServiceSourceDetailTo.getParameterValue()));
				}
				
					
			}//END BillingServiceSourceDetailTo
			
			
			/**Add Collection Record If Not Exits*/
			if(collectionRecordToTemp==null){
				collectionRecordTo= new CollectionRecordTo();
				if(baseCollectionMgmtServiceBean.isPaymentByparticipant(billingService)){
					entity = baseCollectionMgmtServiceBean.getIdParticipantPkByIdIssuerPk(entity.toString());
					collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
				}
				else{
					collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
				}
				List<CollectionRecordDetailTo> collectionRecordDetailToList = new  ArrayList<CollectionRecordDetailTo>();
				collectionRecordDetailToList.add(collectionRecordDetailTo);
				listCollectionRecordTo.add(collectionRecordTo);
				collectionRecordTo.setCollectionRecordDetailToList(collectionRecordDetailToList);
			}else{
				collectionRecordTo=collectionRecordToTemp;
				collectionRecordTo.getCollectionRecordDetailToList().add(collectionRecordDetailTo);
			}
			
			
			
		}//END BillingServiceSourceLoggerTo
		
		
		return organizeCollectionRecordTo(parameters,listCollectionRecordTo);
	}
	
	/**
	 * Organize Collection Record To .
	 *
	 * @param parameters the parameters
	 * @param listCollectionRecordTo the list collection record to
	 * @return the list
	 */
	public List<CollectionRecordTo> organizeCollectionRecordTo(Map parameters, 
									List<CollectionRecordTo> listCollectionRecordTo){
		
		BillingService billingService = (BillingService) parameters.get(GeneralConstants.BILLING_SERVICE);
		Iterator<CollectionRecordTo> iterator = listCollectionRecordTo.iterator();
		BigDecimal referencePrice = BigDecimal.ZERO;
		BigDecimal realAmount = BigDecimal.ZERO;
		 
		while(iterator.hasNext()){
			CollectionRecordTo collectionRecordTo = (CollectionRecordTo) iterator.next();
			Iterator<CollectionRecordDetailTo> iteratorDetail = collectionRecordTo.getCollectionRecordDetailToList().iterator();
			
			while(iteratorDetail.hasNext()){
				CollectionRecordDetailTo collectionRecordDetailTo = (CollectionRecordDetailTo) iteratorDetail.next();
				
				BigDecimal 	amount			=BigDecimal.ZERO;
				Integer    	currency        =collectionRecordDetailTo.getCurrencyOrigin();
				BigDecimal 	nominalValue	=collectionRecordDetailTo.getNominalValue();
				Long 		quantity		=collectionRecordDetailTo.getMovementQuantity();
				BigDecimal	operationPrice	=collectionRecordDetailTo.getOperationPrice();
				
				if(operationPrice!=null){
					amount= CommonsUtilities.multiplyDecimalLong(operationPrice, quantity, 8);
					referencePrice=businessLayerCommonFacade.processExchange(parameters,currency,
							billingService.getCurrencyCalculation(),DailyExchangeRoleType.BUY.getCode());
				}else{
					amount= CommonsUtilities.multiplyDecimalLong(nominalValue, quantity, 8);
					referencePrice=businessLayerCommonFacade.processExchange(parameters,currency,
							billingService.getCurrencyCalculation(),DailyExchangeRoleType.BUY.getCode());
				}
				
				/***ALL EXCHANGE RATE  -----> CONVERSION TO DOP  */
				realAmount =BillingUtils.multiplyDecimals(referencePrice, amount , GeneralConstants.ROUNDING_DIGIT_NUMBER);
				
				collectionRecordDetailTo.setOperationPrice(amount);
				collectionRecordDetailTo.setNegotiatedAmountOrigin(amount);
				collectionRecordDetailTo.setNegotiatedAmount(realAmount);
				collectionRecordDetailTo.setPriceByQuantity(realAmount);
				collectionRecordDetailTo.setPercentage(realAmount);
				collectionRecordDetailTo.setAmount(realAmount);
				
			}
			
		}
		return listCollectionRecordTo;
	}

	/**
	 * Get Entity Collection Source.
	 *
	 * @param billingService the billing service
	 * @param entity the entity
	 * @param collectionRecordTo the collection record to
	 * @param listCollectionRecordTo the list collection record to
	 * @return the entity collection source
	 */
	public CollectionRecordTo getEntityCollectionSource(BillingService billingService, Object entity,
			CollectionRecordTo collectionRecordTo, 
			List<CollectionRecordTo> listCollectionRecordTo){


			/**FIND COLLECTION RECORD AT  LIST***/
		CollectionRecordTo collectionRecordToTemp=findEntityAtListTemporalTo(listCollectionRecordTo, entity, billingService);

		if(collectionRecordToTemp==null){
			if(collectionRecordTo==null){
				collectionRecordTo= new CollectionRecordTo();
			}
			if(baseCollectionMgmtServiceBean.isPaymentByparticipant(billingService)){
				entity = baseCollectionMgmtServiceBean.getIdParticipantPkByIdIssuerPk(entity.toString());
				collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
			}
			else{
				collectionRecordTo.addEntityCollection(entity, billingService.getEntityCollection());	
			}
		}else{
			collectionRecordTo=collectionRecordToTemp;
		}
			
			


		return collectionRecordTo;
		 
		
	}
	
	/**
	 * Get Entity Collection Record Detail.
	 *
	 * @param entity the entity
	 * @param typeEntity the type entity
	 * @param collectionRecordDetailTo the collection record detail to
	 * @return the entity collection record detail
	 */
	public CollectionRecordDetailTo getEntityCollectionRecordDetail(Object entity,Integer typeEntity,
												CollectionRecordDetailTo collectionRecordDetailTo){

		if(Validations.validateIsNull(collectionRecordDetailTo)){
			collectionRecordDetailTo= new CollectionRecordDetailTo();
		}

		if(typeEntity.equals(EntityCollectionType.PARTICIPANTS.getCode())){
			collectionRecordDetailTo.setIdTargetParticipant(Long.parseLong(entity.toString()));
		}else if(typeEntity.equals(EntityCollectionType.HOLDERS.getCode())){
			collectionRecordDetailTo.setIdHolderPk(Long.parseLong(entity.toString()));
		}else if(typeEntity.equals(EntityCollectionType.ISSUERS.getCode())){
			collectionRecordDetailTo.setIdIssuerFk(entity.toString());
		}
 
		return collectionRecordDetailTo;
	}
	

 	/**
	  * *
	  * Find Entity At List temporal.
	  *
	  * @param collectionRecordToList the collection record to list
	  * @param entity the entity
	  * @param typeEntity the type entity
	  * @return the collection record to
	  */
	protected  CollectionRecordTo findEntityAtListTemporalTo(List<CollectionRecordTo> collectionRecordToList,
														Object entity,BillingService billingService){
		
		if (Validations.validateListIsNotNullAndNotEmpty(collectionRecordToList)){
				
			for (CollectionRecordTo collectionRecordTo : collectionRecordToList) {
				if(baseCollectionMgmtServiceBean.isPaymentByparticipant(billingService)){
					entity = baseCollectionMgmtServiceBean.getIdParticipantPkByIdIssuerPk(entity.toString());
				}
				if (collectionRecordTo.existEntityCollection(entity, billingService.getEntityCollection())){
					return collectionRecordTo;
				}
			}
		}
	return null;
	}
}
