package com.pradera.billing.billingsource.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.Parameter;
import javax.persistence.Query;

import org.hibernate.exception.SQLGrammarException;

import com.pradera.billing.billingexception.service.ExceptionLayerServiceBean;
import com.pradera.billing.billingservice.to.BillingServiceTo;
import com.pradera.billing.billingsource.to.BillingServiceSourceInfo;
import com.pradera.billing.billingsource.to.BillingServiceSourceTo;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.billing.BillingServiceSource;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SourceBillingServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@Stateless
@Performance
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class SourceBillingServiceBean extends CrudDaoServiceBean{
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The exception layer service bean. */
	@EJB
	ExceptionLayerServiceBean  exceptionLayerServiceBean;

	/**
	 * Save Billing Service Source.
	 *
	 * @param billingServiceSource the billing service source
	 */
	public void saveBillingServiceSource(Object billingServiceSource){
		create(billingServiceSource);
	}
	
	/**
	 * Search All Billing Service Source .
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<BillingServiceSource> searchAllBillingServiceSource(BillingServiceSourceTo filter) throws ServiceException{
		
		StringBuilder stringBuilderSql = new StringBuilder();
		stringBuilderSql.append(" SELECT ACS.ID_BILLING_SERVICE_SOURCE_PK, ACS.FILE_SOURCE_NAME, ACS.DESCRIPTION_SOURCE , ACS.STATUS ,   ");
		stringBuilderSql.append(" ( select pt.description from parameter_table pt where pt.parameter_table_pk = ACS.STATUS ) as DESCIPTION_STATUS  ,  "); //4
		stringBuilderSql.append(" ACS.LAST_MODIFY_DATE   ");//5
		stringBuilderSql.append(" FROM   BILLING_SERVICE_SOURCE ACS  WHERE 1=1  ");  
		
		if ( Validations.validateIsNotNullAndPositive(filter.getIdBillingServiceSourcePk()) ){
			stringBuilderSql.append(" AND  ACS.ID_BILLING_SERVICE_SOURCE_PK = :idBillingServiceSourcePk    ");
		}
		if ( Validations.validateIsNotNullAndPositive(filter.getStatus()) ){
			stringBuilderSql.append(" AND  STATUS = :status    ");
		}
		if ( Validations.validateIsNotNullAndNotEmpty(filter.getFileSourceName()) ){
			stringBuilderSql.append(" AND  FILE_SOURCE_NAME = :fileSourceName    ");
		}
		
		stringBuilderSql.append("ORDER BY   DESCIPTION_STATUS DESC  ");
		
		Query query = em.createNativeQuery(stringBuilderSql.toString());
		
		if ( Validations.validateIsNotNullAndPositive(filter.getIdBillingServiceSourcePk()) ){
			query.setParameter("idBillingServiceSourcePk", filter.getIdBillingServiceSourcePk());
		}

		if ( Validations.validateIsNotNullAndPositive(filter.getStatus()) ){
			query.setParameter("status",  filter.getStatus() );
		}
		if ( Validations.validateIsNotNullAndNotEmpty(filter.getFileSourceName()) ){
			query.setParameter("fileSourceName", filter.getFileSourceName() );
		}
		
		List<Object[]> objectList = query.getResultList();
		
		List<BillingServiceSource> listBillingServiceSource = new ArrayList<BillingServiceSource>();
		
		for (int i=0; i<objectList.size();++i) {
			Object[] sResults = (Object[])objectList.get(i);
			BillingServiceSource billingServiceSource = new BillingServiceSource();
			
			billingServiceSource.setIdBillingServiceSourcePk((Long.parseLong(sResults[0]==null?"":sResults[0].toString())));
			billingServiceSource.setFileSourceName(sResults[1]==null?"":sResults[1].toString());
			billingServiceSource.setDescriptionSource(sResults[2]==null?"":sResults[2].toString());
			billingServiceSource.setStatus(Integer.parseInt(sResults[3].toString())); 
			billingServiceSource.setDescriptionSourceStatus(sResults[4]==null?"":sResults[4].toString());
			billingServiceSource.setLastModifyDate((Date)sResults[5]==null?null:(Date)sResults[5]);
			
			
			listBillingServiceSource.add(billingServiceSource);
		}
		
		
		return listBillingServiceSource;
	}  
	
	/**
	 * Find Billing Service Source By Filter Billing Service.
	 *
	 * @param billingServiceTo the billing service to
	 * @return the billing service source
	 */
	public BillingServiceSource findBillingServiceSourceByBillingService(BillingServiceTo billingServiceTo){
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append(" Select bss from BillingServiceSource bss left join fetch bss.billingService bs where 1=1 ");
		
		if(Validations.validateIsNotNullAndNotEmpty(billingServiceTo.getIdBillingServicePk())){
			sbQuery.append(" and bs.idBillingServicePk=:idBillingServicePk ");
		}
		
		Query query = em.createQuery(sbQuery.toString());
		
		if(Validations.validateIsNotNullAndNotEmpty(billingServiceTo.getIdBillingServicePk())){
			query.setParameter("idBillingServicePk", billingServiceTo.getIdBillingServicePk());
		}

		BillingServiceSource billingServiceSource=null;
		try{
			billingServiceSource=(BillingServiceSource)query.getSingleResult();
		}
		 catch (Exception e) {
			log.debug(e.getMessage());
		}
		
		return billingServiceSource;
	}
	
	/**
	 * Execute Billing Service Source.
	 *
	 * @param parameters the parameters
	 * @param billingServiceSourceInfo the billing service source info
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Object>  executeBillingServiceSource(Map parameters, BillingServiceSourceInfo billingServiceSourceInfo) 
			throws ServiceException{
		
		StringBuilder strTableComponentBegin = new StringBuilder();
		StringBuilder sbQuery = new StringBuilder();
		 
		sbQuery.append(billingServiceSourceInfo.getQuerySource().getQuery());
		strTableComponentBegin = exceptionLayerServiceBean.filterExceptionByBaseCollection(parameters, sbQuery);
		
		 
		Query query = em.createNativeQuery(strTableComponentBegin.toString());
		 
		List<Object> listObject = null;
		
		Boolean hasDate = billingServiceSourceInfo.getQuerySource().getDate();
		
		/** ASK IF SHOULD SETTING THE DATE SELECTION**/
		if (hasDate){
						
			java.util.Set<Parameter<?>> 	listParameter =  query.getParameters();			
			Object[] parameterArr = listParameter.toArray();
			for (int i = 0; i < parameterArr.length; i++) {
			Object parameterObject	= parameterArr[i];
			/**Pasar los parametros del query, fecha**/
				Parameter<Date> parameter = (Parameter<Date>)parameterObject;	
				query.setParameter(parameter.getName(), billingServiceSourceInfo.getExecutionDate());
			}
						
		}
		 			
		try {
			
			  listObject=query.getResultList();
			  
		} catch (Exception ex) {			
			if(ex.getCause() instanceof SQLGrammarException){
				throw new ServiceException(ErrorServiceType.BILLING_SERVICE_QUERY_GRAMMAR);
			}
			
		}
			
		return listObject;
	 }
 

}
