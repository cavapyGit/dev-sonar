package com.pradera.billing.billingsource.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;

import com.pradera.billing.billingservice.facade.BillingServiceMgmtServiceFacade;
import com.pradera.billing.billingsource.facade.SourceBillingServiceFacade;
import com.pradera.billing.billingsource.to.BillingServiceSourceInfo;
import com.pradera.billing.billingsource.to.BillingServiceSourceTo;
import com.pradera.billing.utils.view.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.billing.BillingServiceSource;
import com.pradera.model.billing.type.BillingServiceFilter;
import com.pradera.model.billing.type.BillingServiceSourceStateType;
import com.pradera.model.generalparameter.ParameterTable;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingSourceMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class BillingSourceMgmtBean extends GenericBaseBean  implements Serializable {


	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;


	/** The billing service mgmt service facade. */
	@EJB
	BillingServiceMgmtServiceFacade billingServiceMgmtServiceFacade;

	/** The source billing service facade. */
	@EJB
	SourceBillingServiceFacade sourceBillingServiceFacade;

	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;

	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;

	/** The Constant log. */
	private static final Logger log = Logger.getLogger(BillingSourceMgmtBean.class);

	/** The billing service source. */
	private BillingServiceSource	billingServiceSource;
	
	/** The billing service source selection. */
	private BillingServiceSource	billingServiceSourceSelection;
	
	/** The billing service source to. */
	private BillingServiceSourceTo billingServiceSourceTo;

	/** The upload file size. */
	private String fUploadFileSize = "2000000"; 
	
	/** The register source. */
	private Boolean registerSource = Boolean.FALSE;
	
	/** The schema name display. */
	private String schemaNameDisplay;
	
	/** The billing service source info. */
	private BillingServiceSourceInfo billingServiceSourceInfo;
	
	/** The list source status. */
	private List<ParameterTable> listSourceStatus;
	
	/** The billing service source data model. */
	private GenericDataModel<BillingServiceSource> billingServiceSourceDataModel;
	
	/** The list billing service sources. */
	private List<BillingServiceSource> listBillingServiceSources;
	
	/**
	 * Inits the.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException  {
		
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		billingServiceSourceInfo=new BillingServiceSourceInfo();

		this.billingServiceSourceTo = new BillingServiceSourceTo();
		this.billingServiceSource = new BillingServiceSource();
		
		
		if (!FacesContext.getCurrentInstance().isPostback()){
			log.info("Carga de Filtros para el seach page :  ");
			
			
		}
		
		if (Validations.validateListIsNullOrEmpty(listSourceStatus)) {

			parameterTableTO.setState(GeneralConstants.ONE_VALUE_INTEGER);
			parameterTableTO.setMasterTableFk(BillingServiceFilter.SOURCE_STATUS.getCode());//
			listSourceStatus = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		}

	}

	/**
	 * Adds the source base collection.
	 */
	public void addSourceBaseCollection() { 
		
		if(registerSource.equals(Boolean.FALSE)){
			
			this.billingServiceSource = new BillingServiceSource();
			schemaNameDisplay = null ;
			
		}

		JSFUtilities.executeJavascriptFunction("PF('cnIdFlagAddSource').show()");
		
	}
	
	/**
	 * Document attach participant file.
	 *
	 * @param event the event
	 */
	public void documentAttachParticipantFile(FileUploadEvent event) {

		String fDisplayName = fUploadValidateFileXml(event.getFile(),"cnfwMsgCustomValidationAcc.show();",null, null);

		this.billingServiceSource.setFileXml(event.getFile().getContents());
		this.billingServiceSource.setFileSourceName(event.getFile().getFileName());

		if (fDisplayName != null) {
			schemaNameDisplay = fDisplayName;
		}

	}
	
	/**
	 * Load sources.
	 */
	@LoggerAuditWeb
	public void loadSources() {

		BillingServiceSourceTo billingServiceSourceToTemp = new BillingServiceSourceTo();
		listBillingServiceSources = new ArrayList<BillingServiceSource>();

		
		// Filter to search
		billingServiceSourceToTemp.setFileSourceName(this.billingServiceSourceInfo.getNameFile());
		billingServiceSourceToTemp.setStatus(this.billingServiceSourceInfo.getStatus());
		 
		try {
			listBillingServiceSources = sourceBillingServiceFacade.loadBillingServiceSource(billingServiceSourceToTemp);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (Validations.validateIsNotNull(billingServiceSourceDataModel)) {
			billingServiceSourceDataModel = new GenericDataModel<BillingServiceSource>();
		}
		if (Validations.validateIsNotNullAndNotEmpty(listBillingServiceSources)) {
			billingServiceSourceDataModel = new GenericDataModel<BillingServiceSource>(listBillingServiceSources);
		}
		
		/** clean selected on data table */
		this.billingServiceSourceSelection = null;
		
		if(billingServiceSourceDataModel.getRowCount()>0){
			setTrueValidButtons();
		}
		
	}
			
	/**
	 * Sets the true valid buttons.
	 */
	public void setTrueValidButtons(){        

		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		if(userPrivilege.getUserAcctions().isModify()){
			privilegeComponent.setBtnModifyView(true);    
		}
		if(userPrivilege.getUserAcctions().isDelete()){
			privilegeComponent.setBtnDeleteView(true);
		}

		userPrivilege.setPrivilegeComponent(privilegeComponent); 

	}
	
	/**
	 * Delete billing service source.
	 */
	@LoggerAuditWeb
	public void deleteBillingServiceSource(){
		
//		try {
			
//			this.billingServiceSource.setStatus(BillingServiceSourceStateType.DELETED.getCode());
//			this.billingServiceSource.setIdBillingServiceSourcePk(this.billingServiceSourceSelection.getIdBillingServiceSourcePk());
			
		
			//sourceBillingServiceServiceFacade.updateBillingServiceSource(this.billingServiceSource);
			

			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_DELETED_SOURCE,""));
			JSFUtilities.executeJavascriptFunction("PF('cnfWSuccessDialog').show();");
			
			this.billingServiceSource = new BillingServiceSource();
			loadSources();
			
			
//		} catch (ServiceException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
	}
	
	/**
	 * Clean billing service source.
	 */
	public void cleanBillingServiceSource(){
		
		billingServiceSourceDataModel = null;
		this.billingServiceSourceInfo = new BillingServiceSourceInfo() ;
				
	}
	
 
	 

	/**
	 * Before back source on schema.
	 */
	public void beforeBackSourceOnSchema() {
		
		if (Validations.validateIsNotNull(this.billingServiceSourceTo.getDescriptionSource()) ) {
			 
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_BACK_SOURCE, ""));
			JSFUtilities.executeJavascriptFunction("cnfWConfirmSourceBack.show();");
			
		}else
			
			backSourceOnSchema();

	}

	
	/**
	 * Back source on schema.
	 */
	public void backSourceOnSchema() {

		cleanSource();
		JSFUtilities.executeJavascriptFunction("PF('cnIdFlagAddSource').hide();");

	}
	
	
	/**
	 * Before back to source.
	 */
	public void beforeBackToSource(){
		
		if(Validations.validateIsNotNullAndNotEmpty(this.billingServiceSourceTo) && (
				Validations.validateIsNotNullAndNotEmpty(this.billingServiceSourceTo.getDescriptionSource()) ||
				Validations.validateIsNotNullAndNotEmpty(this.billingServiceSourceTo.getFileSourceName())  )  ) {
			
			showMessageOnDialog( PropertiesUtilities .getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					PropertiesUtilities.getMessage( PropertiesConstants.WARNING_BACK_SOURCE, ""));
			JSFUtilities.executeJavascriptFunction("cnfWConfirmCleanBack.show();");
			
			return ;
		}else
			{
				registerSource = Boolean.FALSE;
				JSFUtilities.executeJavascriptFunction("PF('cnIdFlagAddSource').hide();");
			}
	}
	
	
	/**
	 * Go back to source.
	 */
	public void goBackToSource(){
		
		this.billingServiceSourceTo = new BillingServiceSourceTo();
		schemaNameDisplay = null ;
		
		registerSource = Boolean.FALSE ;
		
		JSFUtilities.executeJavascriptFunction("PF('cnIdFlagAddSource').hide();");
		
	}
	
	
	/**
	 * Before modify billing service source.
	 *
	 * @return the string
	 */
	// Validate before Modify Billing Service Source 
	public String beforeModifyBillingServiceSource(){
					
		if (Validations.validateIsNotNull(this.billingServiceSourceSelection)) {	
			Object name = billingServiceSourceSelection.getFileSourceName();
			if (billingServiceSourceSelection.getStatus().equals(BillingServiceSourceStateType.DELETED.getCode()))
			{					
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SOURCE_DELETED ,name));
				JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

				return "";
			} 
			
			return "sourceRegister";
			
		}
		
		else{
			showMessageOnDialog(PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SOURCE_NO_SELECTED));
			JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");
			
			return ""; 
		}
   
		
	}
	
	/**
	 * Save billing service source.
	 */
	// Save Source
		@LoggerAuditWeb
		public void saveBillingServiceSource() {

			this.billingServiceSource.setDescriptionSource(this.billingServiceSourceTo.getDescriptionSource());
			this.billingServiceSource.setStatus(BillingServiceSourceStateType.REGISTERED.getCode());
			
			sourceBillingServiceFacade.saveBillingServiceSource(this.billingServiceSource);
			
			showMessageOnDialog(
					PropertiesUtilities
							.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(
							PropertiesConstants.SUCCESS_SAVE_SOURCE, ""));
			JSFUtilities.executeJavascriptFunction("PF('cnfWSuccessDialog').show();");

			
			this.billingServiceSource = new BillingServiceSource();
			cleanSource();
			loadSources();

		}
	
	/**
	 * Modify billing service source.
	 */
	@LoggerAuditWeb
	public void modifyBillingServiceSource(){
		
		try {
			
		this.billingServiceSource.setDescriptionSource(this.billingServiceSourceTo.getDescriptionSource());
		this.billingServiceSource.setIdBillingServiceSourcePk(this.billingServiceSourceSelection.getIdBillingServiceSourcePk());
		
		
		sourceBillingServiceFacade.updateBillingServiceSource(this.billingServiceSource);
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
				PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_MODIFY_PARAMETER,""));
		JSFUtilities.executeJavascriptFunction("PF('cnfWSuccessDialog').show();");
		 

		
		this.billingServiceSource = new BillingServiceSource();
		cleanSource();
		loadSources();
		
		
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

/**
 * Before register source.
 */
public void beforeRegisterSource() {

		if (Validations.validateIsNotNullAndNotEmpty(this.billingServiceSourceTo) && (
				Validations.validateIsNotNullAndNotEmpty(this.billingServiceSourceTo.getDescriptionSource()) &&
				Validations.validateIsNotNullAndNotEmpty(schemaNameDisplay)) ){
			
			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_SAVE_SOURCE, ""));
			JSFUtilities.executeJavascriptFunction("cnfWConfirmSourceSave.show();");
			
			return ; 

		}
		else
		{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SAVE_SOURCE ,""));
			JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");
		}

	}
	
	
	
	/**
	 * Before modify source.
	 */
	public void beforeModifySource() {
  
		if (Validations.validateIsNotNullAndNotEmpty(this.billingServiceSourceTo) && (
				Validations.validateIsNotNullAndNotEmpty(this.billingServiceSourceTo.getDescriptionSource()) &&
				Validations.validateIsNotNullAndNotEmpty(schemaNameDisplay) )){

			showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(
							PropertiesConstants.CONFIRM_MODIFY_SOURCE, ""));
							JSFUtilities.executeJavascriptFunction("cnfWConfirmSourceModify.show();");

		}
		else
		{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SAVE_SOURCE ,""));
			JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");
		}
		
	}
	
	/**
	 * Before deleted billing service source.
	 *
	 * @param e the e
	 */
	// Validate before Delete an BillingService Source 
		public void beforeDeletedBillingServiceSource(ActionEvent e){
			
			
			if (Validations.validateIsNotNull(this.billingServiceSourceSelection)) {	
				Object name = billingServiceSourceSelection.getFileSourceName();
				if (billingServiceSourceSelection.getStatus().equals(BillingServiceSourceStateType.DELETED.getCode()))
				{					
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
							, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SOURCE_DELETED ,name));
					JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

					return ;
				} 
				 
				
				// Validate relation on matrix before delete an Source.
				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
						PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_DELETED_SOURCE, name));
				JSFUtilities.executeJavascriptFunction("cnfWConfirmSourceDeleted.show();");	
				
				
			}
			
			else{
				showMessageOnDialog(PropertiesUtilities
						.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SOURCE_NO_SELECTED));
				JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");
				
				return ; 
			}	
			
		}
	
	
	/**
	 * Validatebefore deleted billing service source.
	 *
	 * @param e the e
	 */
	// Validate before Delete an BillingService Source 
	public void validatebeforeDeletedBillingServiceSource(ActionEvent e){
		
		
		if (Validations.validateIsNotNull(this.billingServiceSourceSelection)) {	
			Object name = billingServiceSourceSelection.getFileSourceName();
			 
			this.billingServiceSourceTo.setDescriptionSource(billingServiceSourceSelection.getDescriptionSource());		
			this.setSchemaNameDisplay(billingServiceSourceSelection.getFileSourceName());   
			
			if (billingServiceSourceSelection.getStatus().equals(BillingServiceSourceStateType.DELETED.getCode())) {
								
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						, PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SOURCE_DELETED ,name));
				JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");

				return ;
			} 
			// Validate relation on matrix before delete an Source.
			
			registerSource = Boolean.TRUE;
			JSFUtilities.executeJavascriptFunction("cnIdFlagAddSource.show()");
			
		}		
		else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_SOURCE_NO_SELECTED));
					JSFUtilities.executeJavascriptFunction("PF('cnfWDialogMessage').show();");
			
			return ; 
		}	
		
	}

	/**
	 * Before clean source.
	 */
	public void beforeCleanSource(){  
	
		if ( Validations.validateIsNotNullAndNotEmpty(this.billingServiceSourceTo.getDescriptionSource()) ||
				!(Validations.validateIsNullOrEmpty(this.schemaNameDisplay)) ) {
			 
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_CLEAN_SOURCE, ""));
			JSFUtilities.executeJavascriptFunction("cnfWConfirmSourceClean.show();");
			
		}else
		
			cleanSource();
	}
	
	/**
	 * Clean source.
	 */
	public void cleanSource() {

		if (Validations.validateIsNotNullAndNotEmpty(this.billingServiceSourceTo)) {

			this.billingServiceSourceTo = new BillingServiceSourceTo();
			this.setSchemaNameDisplay(null);
			billingServiceSourceSelection = null ;
			schemaNameDisplay = null ;
		}
	}
	
	/**
	 * Gets the billing service source.
	 *
	 * @return the billingServiceSource
	 */
	public BillingServiceSource getBillingServiceSource() {
		return billingServiceSource;
	}

	/**
	 * Sets the billing service source.
	 *
	 * @param billingServiceSource the billingServiceSource to set
	 */
	public void setBillingServiceSource(BillingServiceSource billingServiceSource) {
		this.billingServiceSource = billingServiceSource;
	}

	/**
	 * Gets the billing service source selection.
	 *
	 * @return the billingServiceSourceSelection
	 */
	public BillingServiceSource getBillingServiceSourceSelection() {
		return billingServiceSourceSelection;
	}

	/**
	 * Sets the billing service source selection.
	 *
	 * @param billingServiceSourceSelection the billingServiceSourceSelection to set
	 */
	public void setBillingServiceSourceSelection(
			BillingServiceSource billingServiceSourceSelection) {
		this.billingServiceSourceSelection = billingServiceSourceSelection;
	}

	/**
	 * Gets the register source.
	 *
	 * @return the registerSource
	 */
	public Boolean getRegisterSource() {
		return registerSource;
	}

	/**
	 * Sets the register source.
	 *
	 * @param registerSource the registerSource to set
	 */
	public void setRegisterSource(Boolean registerSource) {
		this.registerSource = registerSource;
	}

	/**
	 * Gets the schema name display.
	 *
	 * @return the schemaNameDisplay
	 */
	public String getSchemaNameDisplay() {
		return schemaNameDisplay;
	}

	/**
	 * Sets the schema name display.
	 *
	 * @param schemaNameDisplay the schemaNameDisplay to set
	 */
	public void setSchemaNameDisplay(String schemaNameDisplay) {
		this.schemaNameDisplay = schemaNameDisplay;
	}

	/**
	 * Gets the billing service source info.
	 *
	 * @return the billingServiceSourceInfo
	 */
	public BillingServiceSourceInfo getBillingServiceSourceInfo() {
		return billingServiceSourceInfo;
	}

	/**
	 * Sets the billing service source info.
	 *
	 * @param billingServiceSourceInfo the billingServiceSourceInfo to set
	 */
	public void setBillingServiceSourceInfo(
			BillingServiceSourceInfo billingServiceSourceInfo) {
		this.billingServiceSourceInfo = billingServiceSourceInfo;
	}

	/**
	 * Gets the list source status.
	 *
	 * @return the listSourceStatus
	 */
	public List<ParameterTable> getListSourceStatus() {
		return listSourceStatus;
	}

	/**
	 * Sets the list source status.
	 *
	 * @param listSourceStatus the listSourceStatus to set
	 */
	public void setListSourceStatus(List<ParameterTable> listSourceStatus) {
		this.listSourceStatus = listSourceStatus;
	}

	/**
	 * Gets the billing service source data model.
	 *
	 * @return the billingServiceSourceDataModel
	 */
	public GenericDataModel<BillingServiceSource> getBillingServiceSourceDataModel() {
		return billingServiceSourceDataModel;
	}

	/**
	 * Sets the billing service source data model.
	 *
	 * @param billingServiceSourceDataModel the billingServiceSourceDataModel to set
	 */
	public void setBillingServiceSourceDataModel(
			GenericDataModel<BillingServiceSource> billingServiceSourceDataModel) {
		this.billingServiceSourceDataModel = billingServiceSourceDataModel;
	}

	/**
	 * Gets the list billing service sources.
	 *
	 * @return the listBillingServiceSources
	 */
	public List<BillingServiceSource> getListBillingServiceSources() {
		return listBillingServiceSources;
	}

	/**
	 * Sets the list billing service sources.
	 *
	 * @param listBillingServiceSources the listBillingServiceSources to set
	 */
	public void setListBillingServiceSources(
			List<BillingServiceSource> listBillingServiceSources) {
		this.listBillingServiceSources = listBillingServiceSources;
	}

	/**
	 * Gets the billing service source to.
	 *
	 * @return the billingServiceSourceTo
	 */
	public BillingServiceSourceTo getBillingServiceSourceTo() {
		return billingServiceSourceTo;
	}

	/**
	 * Sets the billing service source to.
	 *
	 * @param billingServiceSourceTo the billingServiceSourceTo to set
	 */
	public void setBillingServiceSourceTo(
			BillingServiceSourceTo billingServiceSourceTo) {
		this.billingServiceSourceTo = billingServiceSourceTo;
	}
	
	
}
