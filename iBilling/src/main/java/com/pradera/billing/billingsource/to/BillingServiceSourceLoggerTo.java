package com.pradera.billing.billingsource.to;

import java.io.Serializable;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingServiceSourceLoggerTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class BillingServiceSourceLoggerTo  implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The list billing service source detail to. */
	private List<BillingServiceSourceDetailTo>  listBillingServiceSourceDetailTo;

	/**
	 * Gets the list billing service source detail to.
	 *
	 * @return the listBillingServiceSourceDetailTo
	 */
	public List<BillingServiceSourceDetailTo> getListBillingServiceSourceDetailTo() {
		return listBillingServiceSourceDetailTo;
	}

	/**
	 * Sets the list billing service source detail to.
	 *
	 * @param listBillingServiceSourceDetailTo the listBillingServiceSourceDetailTo to set
	 */
	public void setListBillingServiceSourceDetailTo(
			List<BillingServiceSourceDetailTo> listBillingServiceSourceDetailTo) {
		this.listBillingServiceSourceDetailTo = listBillingServiceSourceDetailTo;
	}
	
		
}
