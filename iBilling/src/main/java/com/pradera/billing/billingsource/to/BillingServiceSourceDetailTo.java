package com.pradera.billing.billingsource.to;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingServiceSourceDetailTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class BillingServiceSourceDetailTo implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1951313903320043669L;
	
	/** The id billing service source detail pk. */
	private Long 	idBillingServiceSourceDetailPk;
	
	/** The id billing service source logger fk. */
	private Long 	idBillingServiceSourceLoggerFk;
	
	/** The parameter name. */
	private String 	parameterName;
	
	/** The parameter value. */
	private String 	parameterValue;
	
	/** The parameter type. */
	private Integer parameterType;
	
	/** The position. */
	private Integer position;
	
	/** The level. */
	private Integer level;
	
	/** The parameter description. */
	private String 	parameterDescription;
	
	
	
//	
//	private BigDecimal		notPlaced;
//	private BigDecimal	price;
//	private Long 		idParticipantPk;
//	private String		idHolderPk;
//	private String 		idIssuerPk;
//	private String 		idSecurityCodePk;
//	private String		movementType;
//	private Date   		movementDate;
//	private String		securityClass;
//	private String 		idHolderAccountPk;
//	private String 		issuanceCode;
//	private BigDecimal	nominalValue;
//	private BigDecimal	currentNominalValue;
//	private Date   		registryDate;
//	private BigDecimal	placedAmount;
//	private BigDecimal	desmaterializedBalance;
//	private Long		currency;
//	private BigDecimal	totalBalance;
//	private Long		movementQuantity;
//	private BigDecimal	operationPrice;
//	private Date   		operationDate;
//	private Long		operationNumber;
//	private Long		idSourceParticipantPk;
//	private Long		idTargetParticipantPk;
//	private	Long		cantidadMovimientos;
//	private Long		accountNumber;
	/**
 * Gets the id billing service source detail pk.
 *
 * @return the idBillingServiceSourceDetailPk
 */
	public Long getIdBillingServiceSourceDetailPk() {
		return idBillingServiceSourceDetailPk;
	}
	
	/**
	 * Sets the id billing service source detail pk.
	 *
	 * @param idBillingServiceSourceDetailPk the idBillingServiceSourceDetailPk to set
	 */
	public void setIdBillingServiceSourceDetailPk(
			Long idBillingServiceSourceDetailPk) {
		this.idBillingServiceSourceDetailPk = idBillingServiceSourceDetailPk;
	}
	
	/**
	 * Gets the id billing service source logger fk.
	 *
	 * @return the idBillingServiceSourceLoggerFk
	 */
	public Long getIdBillingServiceSourceLoggerFk() {
		return idBillingServiceSourceLoggerFk;
	}
	
	/**
	 * Sets the id billing service source logger fk.
	 *
	 * @param idBillingServiceSourceLoggerFk the idBillingServiceSourceLoggerFk to set
	 */
	public void setIdBillingServiceSourceLoggerFk(
			Long idBillingServiceSourceLoggerFk) {
		this.idBillingServiceSourceLoggerFk = idBillingServiceSourceLoggerFk;
	}
	
	/**
	 * Gets the parameter name.
	 *
	 * @return the parameterName
	 */
	public String getParameterName() {
		return parameterName;
	}
	
	/**
	 * Sets the parameter name.
	 *
	 * @param parameterName the parameterName to set
	 */
	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}
	
	/**
	 * Gets the parameter value.
	 *
	 * @return the parameterValue
	 */
	public String getParameterValue() {
		return parameterValue;
	}
	
	/**
	 * Sets the parameter value.
	 *
	 * @param parameterValue the parameterValue to set
	 */
	public void setParameterValue(String parameterValue) {
		this.parameterValue = parameterValue;
	}
	
	/**
	 * Gets the parameter type.
	 *
	 * @return the parameterType
	 */
	public Integer getParameterType() {
		return parameterType;
	}
	
	/**
	 * Sets the parameter type.
	 *
	 * @param parameterType the parameterType to set
	 */
	public void setParameterType(Integer parameterType) {
		this.parameterType = parameterType;
	}
	
	/**
	 * Gets the position.
	 *
	 * @return the position
	 */
	public Integer getPosition() {
		return position;
	}
	
	/**
	 * Sets the position.
	 *
	 * @param position the position to set
	 */
	public void setPosition(Integer position) {
		this.position = position;
	}
	
	/**
	 * Gets the parameter description.
	 *
	 * @return the parameterDescription
	 */
	public String getParameterDescription() {
		return parameterDescription;
	}
	
	/**
	 * Sets the parameter description.
	 *
	 * @param parameterDescription the parameterDescription to set
	 */
	public void setParameterDescription(String parameterDescription) {
		this.parameterDescription = parameterDescription;
	}
	
	/**
	 * Gets the level.
	 *
	 * @return the level
	 */
	public Integer getLevel() {
		return level;
	}
	
	/**
	 * Sets the level.
	 *
	 * @param level the level to set
	 */
	public void setLevel(Integer level) {
		this.level = level;
	}
	
	
	
	
	

}
