package com.pradera.billing.billingsource.to;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BillingServiceSourceTo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17-ago-2015
 */
public class BillingServiceSourceTo implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7576375837232920490L;

	/** The id billing service source pk. */
	private Long idBillingServiceSourcePk;
	
	/** The file xml. */
	private byte[] fileXml;
	
	/** The file source name. */
	private String fileSourceName;
	
	/** The description source. */
	private String descriptionSource;
	
	/** The status. */
	private Integer status;

	/** The state service. */
	private Integer stateService;
	
	/**
	 * Instantiates a new billing service source to.
	 */
	public BillingServiceSourceTo() {
	}

	/**
	 * Gets the id billing service source pk.
	 *
	 * @return the idBillingServiceSourcePk
	 */
	public Long getIdBillingServiceSourcePk() {
		return idBillingServiceSourcePk;
	}

	/**
	 * Sets the id billing service source pk.
	 *
	 * @param idBillingServiceSourcePk the idBillingServiceSourcePk to set
	 */
	public void setIdBillingServiceSourcePk(Long idBillingServiceSourcePk) {
		this.idBillingServiceSourcePk = idBillingServiceSourcePk;
	}

	/**
	 * Gets the file xml.
	 *
	 * @return the fileXml
	 */
	public byte[] getFileXml() {
		return fileXml;
	}
	
	/**
	 * Sets the file xml.
	 *
	 * @param fileXml the fileXml to set
	 */
	public void setFileXml(byte[] fileXml) {
		this.fileXml = fileXml;
	}

	/**
	 * Gets the file source name.
	 *
	 * @return the fileSourceName
	 */
	public String getFileSourceName() {
		return fileSourceName;
	}
	
	/**
	 * Sets the file source name.
	 *
	 * @param fileSourceName the fileSourceName to set
	 */
	public void setFileSourceName(String fileSourceName) {
		this.fileSourceName = fileSourceName;
	}
	
	/**
	 * Gets the description source.
	 *
	 * @return the descriptionSource
	 */
	public String getDescriptionSource() {
		return descriptionSource;
	}
	
	/**
	 * Sets the description source.
	 *
	 * @param descriptionSource the descriptionSource to set
	 */
	public void setDescriptionSource(String descriptionSource) {
		this.descriptionSource = descriptionSource;
	}
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}
	
	/**
	 * Sets the status.
	 *
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * Gets the state service.
	 *
	 * @return the stateService
	 */
	public Integer getStateService() {
		return stateService;
	}

	/**
	 * Sets the state service.
	 *
	 * @param stateService the stateService to set
	 */
	public void setStateService(Integer stateService) {
		this.stateService = stateService;
	}
}
