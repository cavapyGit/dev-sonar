package com.pradera.auditprocess.auditlog.facade;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.auditprocess.auditlog.service.AuditProcessLogExecutor;
import com.pradera.auditprocess.auditlog.service.AuditProcessLogService;
import com.pradera.auditprocess.auditlog.view.AuditProcessLogTO;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.core.framework.batchprocess.service.BatchServiceBean;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.auditprocess.AuditBusinessProcess;
import com.pradera.model.auditprocess.AuditProcessLog;
import com.pradera.model.auditprocess.AuditProcessRules;
import com.pradera.model.process.BusinessProcess;
import com.pradera.commons.sessionuser.UserInfo;
import javax.inject.Inject;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project PraderaSecurity
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Class UserAuditingServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 10/01/2013
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class AuditProcessLogFacade {
	
	/** The batch service. */
	@EJB
	BatchServiceBean batchService;
	
	/** The batch process service facade. */
	@EJB
	BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The audit process log service. */
	@EJB
	AuditProcessLogService auditProcessLogService;
	
	/** The audit process log executor. */
	@EJB
	AuditProcessLogExecutor auditProcessLogExecutor;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	/**
	 * Find audit business process.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<AuditBusinessProcess> findAuditBusinessProcess() throws ServiceException{
		return auditProcessLogService.findAuditBusinessProcess();
	}
	
	/**
	 * Find audit process rule facade.
	 *
	 * @param auditProcessLog the audit process log
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<AuditProcessRules> findAuditProcessRuleFacade(AuditProcessLogTO auditProcessLog) throws ServiceException{
		return auditProcessLogService.findAuditProcessRuleService(auditProcessLog);
	}

	/**
	 * Register audit logs.
	 *
	 * @param auditRulePk the audit rule pk
	 * @return the audit process log
	 * @throws ServiceException the service exception
	 */
	public AuditProcessLog registerAuditLogs(Long auditRulePk) throws ServiceException {
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        
		AuditProcessRules auditProcessRules = auditProcessLogService.getAuditProcessRuleService(auditRulePk);
		
		AuditProcessLog auditProcessLog = auditProcessLogService.
				registerAuditProcessLog(auditProcessRules.getIdAuditProcessRulesPk(), loggerUser.getUserName());
		
		auditProcessLog.setProcedureName(auditProcessRules.getAuditStraightenBusiness().getProcedureName());
		
		return auditProcessLog;
	}
	
	 /**
 	 * Register batch.
 	 *
 	 * @param userName the user name
 	 * @param process the process
 	 * @param parameters the parameters
 	 * @throws ServiceException the service exception
 	 */
 	/* Register batch.
	 *
	 * @param process the process
	 * @param parameters the parameters
	 * @throws ServiceException the service exception
	 */
	public void registerBatch(String userName,BusinessProcess process, Map<String,Object> parameters) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        //How is required_new transaction dont propagate transactionRegistry, so we propagate with threadLocal
		batchProcessServiceFacade.registerBatch(
				userInfo.getUserAccountSession().getUserName(),process,parameters);
	}
	
	/**
	 * Execute audit process.
	 *
	 * @param auditProcessLogPk the audit process log pk
	 * @param procedureName the procedure name
	 * @throws ServiceException the service exception
	 */
	public void executeAuditProcess(Long auditProcessLogPk, String procedureName) throws ServiceException {
		
		auditProcessLogService.executeAuditProcess(auditProcessLogPk,procedureName);
	}
	
	/**
	 * Update audit process log.
	 *
	 * @param auditProcessLog the audit process log
	 * @return the audit process log
	 * @throws ServiceException the service exception
	 */
	public AuditProcessLog updateAuditProcessLog(AuditProcessLog auditProcessLog) throws ServiceException{
		
		return auditProcessLogService.update(auditProcessLog);
	}
	
}