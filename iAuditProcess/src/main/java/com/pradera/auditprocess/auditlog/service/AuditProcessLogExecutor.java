package com.pradera.auditprocess.auditlog.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.auditprocess.processrules.to.SearchAuditProcessRulesTO;
import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.configuration.UserAdmin;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.processes.scheduler.Schedulebatch;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.usersession.UserAcctions;
import com.pradera.model.auditprocess.AuditProcessRules;
import com.pradera.model.auditprocess.type.AuditProcessType;
import com.pradera.model.process.BusinessProcess;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project PraderaSecurity Copyright 2012.</li>
 * </ul>
 * 
 * The Class UserAuditingServiceBean.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 10/01/2013
 */
@Startup
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class AuditProcessLogExecutor extends CrudDaoServiceBean {

	/** The log. */
	@Inject
	PraderaLogger log;

	/** The timer service. */
	@Resource
	TimerService timerService;
	
	/** The client rest service. */
	@Inject 
	ClientRestService clientRestService;
	
	/** The batch process service facade. */
	@Inject
	BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The user admin. */
	@Inject
	@UserAdmin 
	String userAdmin;
	
	/** The transaction registry. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;
	
	/**
	 * Load process.
	 */
	@PostConstruct
	public void loadProcess() {
		List<AuditProcessRules> rules  = findAuditRules();
		for (AuditProcessRules rule : rules) {
			deleteTimer(rule.getIdAuditProcessRulesPk());
            registerTimer(rule);
		}
	}
	
	/**
	 * Find audit rules.
	 *
	 * @return the list
	 */
	public List<AuditProcessRules> findAuditRules(){
		StringBuilder querySql = new StringBuilder();
		querySql.append("Select apr From AuditProcessRules apr where processType = :processType ");
		Query query = em.createQuery(querySql.toString());
		query.setParameter("processType", AuditProcessType.AUTOMATIC.getCode());
		
		try{
			return query.getResultList();
		}catch(NoResultException ex){
			return new ArrayList<AuditProcessRules>(0);
		}
	}
	
	/**
	 * Register rule.
	 *
	 * @param auditProcessRules the audit process rules
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void registerRule(AuditProcessRules auditProcessRules) throws ServiceException{
        //Register in DB
        create(auditProcessRules);
        if(auditProcessRules.getProcessType().equals(AuditProcessType.AUTOMATIC.getCode())){
        	registerTimer(auditProcessRules);
        }
    }
	
	/**
	 * Delete timer.
	 *
	 * @param idTimer the id timer
	 */
	public void deleteTimer(Long idTimer){
       Timer timer = getTimer(idTimer);
       if(timer != null){
    	   log.info("Deleting audit timer "+ timer.getInfo());
           timer.cancel();
       }
   }

	/**
	 * Register timer.
	 *
	 * @param auditProcessRule the audit process rule
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void registerTimer(AuditProcessRules auditProcessRule) {			
		
		TimerConfig timerConfig = new TimerConfig(auditProcessRule.getIdAuditProcessRulesPk(), false);
		ScheduleExpression schedExp = new ScheduleExpression();
		
		Calendar executeTime = Calendar.getInstance();
		executeTime.setTime(auditProcessRule.getExecuteTime());
		schedExp.second(executeTime.get(Calendar.HOUR));
		schedExp.minute(executeTime.get(Calendar.MINUTE));
		schedExp.hour(executeTime.get(Calendar.HOUR_OF_DAY));

		schedExp.dayOfMonth(GeneralConstants.ASTERISK_STRING);
		schedExp.month(GeneralConstants.ASTERISK_STRING);
		schedExp.dayOfWeek(GeneralConstants.ASTERISK_STRING);
		schedExp.year(GeneralConstants.ASTERISK_STRING);
		
		if(Validations.validateIsNotNullAndPositive(auditProcessRule.getIndDaily())){
			//nothing
		}else if(Validations.validateIsNotNullAndPositive(auditProcessRule.getIndMonthly())){
			schedExp.dayOfMonth("Last");
		}else if(Validations.validateIsNotNullAndPositive(auditProcessRule.getIndAnnual())){
			schedExp.dayOfMonth("Last");
			schedExp.month(12);
			schedExp.year(GeneralConstants.ASTERISK_STRING);
		}
		
		log.info("### Audit Scheduler expr: " + schedExp.toString());
		Timer newTimer = timerService.createCalendarTimer(schedExp, timerConfig);
		log.info("New audit timer created: " + newTimer.getInfo());
		auditProcessRule.setNextTimeOut(newTimer.getNextTimeout());		

	}
	
	/**
	 * Gets the timer.
	 *
	 * @param idTimer the id timer
	 * @return the timer
	 */
	private Timer getTimer(Long idTimer){
       Collection<Timer> timers = timerService.getTimers();
       for (Timer t : timers){
           if(idTimer.equals(t.getInfo())){
               return t;
           }
       }
       return null;
    }
	
	/**
	 * Gets the list audit process rules.
	 *
	 * @param searchAuditProcessRulesTO the search audit process rules to
	 * @return the list audit process rules
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<AuditProcessRules> getListAuditProcessRules(SearchAuditProcessRulesTO searchAuditProcessRulesTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT apr");
		sbQuery.append("	FROM AuditProcessRules apr");
		sbQuery.append("	INNER JOIN FETCH apr.auditBusinessProcess");
		sbQuery.append("	INNER JOIN FETCH apr.auditStraightenBusiness");
		sbQuery.append("	WHERE apr.auditBusinessProcess.idAuditBusinessProcessPk = :idAuditBusinessProcessPk");
		
		if(Validations.validateIsNotNullAndNotEmpty(searchAuditProcessRulesTO.getAuditStraightenBusiness()))
			sbQuery.append("	AND apr.auditStraightenBusiness.idStraightenBusinessPk = :idStraightenBusinessPk");
		if(Validations.validateIsNotNullAndNotEmpty(searchAuditProcessRulesTO.getAuditType()))
			sbQuery.append("	AND apr.auditType = :auditType");
		if(Validations.validateIsNotNullAndNotEmpty(searchAuditProcessRulesTO.getProcessType()))
			sbQuery.append("	AND apr.processType = :processType");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idAuditBusinessProcessPk", searchAuditProcessRulesTO.getAuditBusinessProcess());
		if(Validations.validateIsNotNullAndNotEmpty(searchAuditProcessRulesTO.getAuditStraightenBusiness()))
			query.setParameter("idStraightenBusinessPk", searchAuditProcessRulesTO.getAuditStraightenBusiness());
		if(Validations.validateIsNotNullAndNotEmpty(searchAuditProcessRulesTO.getAuditType()))
			query.setParameter("auditType", searchAuditProcessRulesTO.getAuditType());
		if(Validations.validateIsNotNullAndNotEmpty(searchAuditProcessRulesTO.getProcessType()))
			query.setParameter("processType", searchAuditProcessRulesTO.getProcessType());
		List<AuditProcessRules> rules =  (List<AuditProcessRules>)query.getResultList();
			
		for (AuditProcessRules auditProcessRules : rules) {
			if(auditProcessRules.getProcessType().equals(AuditProcessType.AUTOMATIC.getCode())){
				Timer timer = getTimer(auditProcessRules.getIdAuditProcessRulesPk());
				if(timer != null)
					auditProcessRules.setNextTimeOut(timer.getNextTimeout());	      
			}
		}
		return rules;
	}
	
	
	/**
	 * Excute batch remote.
	 *
	 * @param timer the timer
	 */
	@Timeout
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void excuteBatchRemote(Timer timer) {
		log.info("Running timer :" + timer.getInfo());
	
		Long idScheduler = (Long) timer.getInfo();
		
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.AUDIT_PROCESS_LOG.getCode());
		
		Map<String, Object> details = new HashMap<String, Object>();
		details.put(GeneralConstants.PARAMETER_AUDIT_RULE_PK, idScheduler.toString());
		
		LoggerUser loggerUser = new LoggerUser();
		loggerUser.setIpAddress("localhost");
		loggerUser.setUserName(userAdmin);
		//loggerUser.setIdUserSessionPk(userInfo.getUserAccountSession().getIdUserSessionPk());
		//loggerUser.setSessionId(userInfo.getUserAccountSession().getTicketSession());
		loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
		UserAcctions userAcctions = new UserAcctions();
		userAcctions.setIdPrivilegeRegister(1);
		loggerUser.setUserAction(userAcctions);
		//loggerUser.setRegulatorEntityType(userInfo.getUserAccountSession().getRegulatorEntityType());
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
		
		try {
			batchProcessServiceFacade.registerBatch(userAdmin,businessProcess,details);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		log.info("finish register remote batch");
	}
	
}