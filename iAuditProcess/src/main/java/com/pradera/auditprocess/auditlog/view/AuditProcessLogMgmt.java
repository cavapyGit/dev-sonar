package com.pradera.auditprocess.auditlog.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.auditprocess.auditlog.facade.AuditProcessLogFacade;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.usersession.UserAcctions;
import com.pradera.model.auditprocess.AuditBusinessProcess;
import com.pradera.model.auditprocess.AuditProcessRules;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project PraderaSecurity
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Class UserAuditingServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 10/01/2013
 */
@DepositaryWebBean
@LoggerCreateBean
public class AuditProcessLogMgmt extends GenericBaseBean{
	
	/** The logger. */
	@Inject
	private transient PraderaLogger logger;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The audit process log facade. */
	@EJB
	private AuditProcessLogFacade auditProcessLogFacade;
	
	/** The parameter facade. */
	@EJB
	GeneralParametersFacade parameterFacade;
	
	/** The batch process service facade. */
	@EJB
	BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The audit process to. */
	private AuditProcessLogTO auditProcessTO;
	
	/** The audit rules result. */
	private GenericDataModel<AuditProcessRules> auditRulesResult; 
	
	/** The process type list. */
	private List<ParameterTable> processTypeList;
	
	/** The execution type list. */
	private List<ParameterTable> executionTypeList;
	
	/** The audit business process list. */
	private List<AuditBusinessProcess> auditBusinessProcessList;
	
	/** The audit rule selected. */
	private AuditProcessRules auditRuleSelected;

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		loadAuditProcessTO();
		try{
			loadAuditBusinessProcess();
			loadExecutionType();
			loadProcessType();
			UserAcctions acctions = userInfo.getUserAcctions();
			acctions.toString();
		}catch(ServiceException ex){
		}
	}
	
	/**
	 * Search audit straigh.
	 */
	public void searchAuditStraigh(){
		AuditProcessLogTO auditProcessLog = new AuditProcessLogTO();
		auditProcessLog.setRequestDate(auditProcessTO.getRequestDate());
		auditProcessLog.setAuditType(auditProcessTO.getAuditType());
		auditProcessLog.setProcessType(auditProcessTO.getProcessType());
		auditProcessLog.setAuditProcessId(auditProcessTO.getAuditProcessId());
		
		List<AuditProcessRules> auditStraighList = null; 
		try {
			auditStraighList = auditProcessLogFacade.findAuditProcessRuleFacade(auditProcessLog);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		if(auditStraighList!=null && !auditStraighList.isEmpty()){
			auditRulesResult = new GenericDataModel<AuditProcessRules>(auditStraighList);
		}else{
			auditRulesResult = new GenericDataModel<AuditProcessRules>();
		}
	}
	
	/**
	 * Clean audit straigh.
	 */
	public void cleanAuditStraigh(){
		auditProcessTO.setRequestDate(new Date());
		auditProcessTO.setAuditType(null);
		auditProcessTO.setProcessType(null);
		auditProcessTO.setAuditProcessId(null);
		auditRulesResult = null;
		JSFUtilities.resetComponent("auditProcessLog:cboAuditType");
		JSFUtilities.resetComponent("auditProcessLog:cbExecutionType");
		JSFUtilities.resetComponent("auditProcessLog:cboAuditBusi");
	}
	
	/**
	 * Before send rules.
	 *
	 * @param auditProcessRules the audit process rules
	 */
	public void beforeSendRules(AuditProcessRules auditProcessRules){
		JSFUtilities.executeJavascriptFunction("wrSendAuditRule.show()");
		auditRuleSelected = auditProcessRules;
	}
	
	/**
	 * Cancel send rules.
	 */
	public void cancelSendRules(){
		auditRuleSelected = null;
	}
	
	/**
	 * Send rules.
	 */
	@LoggerAuditWeb
	public void sendRules(){
		//auditRuleSelected
		
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.AUDIT_STRAIGHTEN_BUSINESS_EXECUTE.getCode());
		
		try {
			Map<String, Object> details = new HashMap<String, Object>();
			details.put(GeneralConstants.PARAMETER_AUDIT_RULE_PK, auditRuleSelected.getIdAuditProcessRulesPk());
			
			auditProcessLogFacade.registerBatch(
					userInfo.getUserAccountSession().getUserName(),businessProcess,details);
			
			Object[] bodyData = new Object[1];
			bodyData[0] = auditRuleSelected.getIdAuditProcessRulesPk();
			JSFUtilities.showValidationDialog();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage("audit.businesproces.log.sendlog.ok", bodyData));
			
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
//	public void 

	/**
 * Load process type.
 */
private void loadProcessType() {
		// TODO Auto-generated method stub
		ParameterTable parameter_1 = new ParameterTable();
		parameter_1.setParameterTablePk(0);
		parameter_1.setDescription("INTERNO");
		
		ParameterTable parameter_2 = new ParameterTable();
		parameter_2.setParameterTablePk(1);
		parameter_2.setDescription("EXTERNO");
		
		processTypeList = new ArrayList<ParameterTable>();
		processTypeList.add(parameter_1);
		processTypeList.add(parameter_2);
	}

	/**
	 * Load execution type.
	 */
	private void loadExecutionType() {
		// TODO Auto-generated method stub
		ParameterTable parameter_1 = new ParameterTable();
		parameter_1.setParameterTablePk(0);
		parameter_1.setDescription("MANUAL");
		
		ParameterTable parameter_2 = new ParameterTable();
		parameter_2.setParameterTablePk(1);
		parameter_2.setDescription("AUTOMATICO");
		
		executionTypeList = new ArrayList<ParameterTable>();
		executionTypeList.add(parameter_1);
		executionTypeList.add(parameter_2);
	}

	/**
	 * Load audit business process.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadAuditBusinessProcess() throws ServiceException{
		// TODO Auto-generated method stub
		auditBusinessProcessList = auditProcessLogFacade.findAuditBusinessProcess();
	}

	/**
	 * Load audit process to.
	 */
	private void loadAuditProcessTO() {
		// TODO Auto-generated method stub
		auditProcessTO = new AuditProcessLogTO();
		auditProcessTO.setRequestDate(new Date());
	}

	/**
	 * Gets the audit process to.
	 *
	 * @return the audit process to
	 */
	public AuditProcessLogTO getAuditProcessTO() {
		return auditProcessTO;
	}

	/**
	 * Sets the audit process to.
	 *
	 * @param auditProcessTO the new audit process to
	 */
	public void setAuditProcessTO(AuditProcessLogTO auditProcessTO) {
		this.auditProcessTO = auditProcessTO;
	}

	/**
	 * Gets the audit business process list.
	 *
	 * @return the audit business process list
	 */
	public List<AuditBusinessProcess> getAuditBusinessProcessList() {
		return auditBusinessProcessList;
	}

	/**
	 * Sets the audit business process list.
	 *
	 * @param auditBusinessProcessList the new audit business process list
	 */
	public void setAuditBusinessProcessList(List<AuditBusinessProcess> auditBusinessProcessList) {
		this.auditBusinessProcessList = auditBusinessProcessList;
	}

	/**
	 * Gets the process type list.
	 *
	 * @return the process type list
	 */
	public List<ParameterTable> getProcessTypeList() {
		return processTypeList;
	}

	/**
	 * Sets the process type list.
	 *
	 * @param processTypeList the new process type list
	 */
	public void setProcessTypeList(List<ParameterTable> processTypeList) {
		this.processTypeList = processTypeList;
	}

	/**
	 * Gets the execution type list.
	 *
	 * @return the execution type list
	 */
	public List<ParameterTable> getExecutionTypeList() {
		return executionTypeList;
	}

	/**
	 * Sets the execution type list.
	 *
	 * @param executionTypeList the new execution type list
	 */
	public void setExecutionTypeList(List<ParameterTable> executionTypeList) {
		this.executionTypeList = executionTypeList;
	}

	/**
	 * Gets the audit rules result.
	 *
	 * @return the audit rules result
	 */
	public GenericDataModel<AuditProcessRules> getAuditRulesResult() {
		return auditRulesResult;
	}

	/**
	 * Sets the audit rules result.
	 *
	 * @param auditStraightenResult the new audit rules result
	 */
	public void setAuditRulesResult(GenericDataModel<AuditProcessRules> auditStraightenResult) {
		this.auditRulesResult = auditStraightenResult;
	}
}