package com.pradera.auditprocess.auditlog.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.auditprocess.auditlog.view.AuditProcessLogTO;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.auditprocess.AuditBusinessProcess;
import com.pradera.model.auditprocess.AuditProcessLog;
import com.pradera.model.auditprocess.AuditProcessRules;
import com.pradera.model.auditprocess.type.AuditProcessLogStateType;
import com.pradera.model.process.type.ProcessType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project PraderaSecurity
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Class UserAuditingServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 10/01/2013
 */
@Stateless
public class AuditProcessLogService extends CrudDaoServiceBean {
	
	/** The logger. */
	@Inject
	private transient PraderaLogger logger;
	
	/**
	 * Register audit process log.
	 *
	 * @param idAuditProcessRulesPk the id audit process rules pk
	 * @param userName the user name
	 * @return the audit process log
	 */
	public AuditProcessLog registerAuditProcessLog(Long idAuditProcessRulesPk, String userName){
		AuditProcessRules auditProcessRules = find(AuditProcessRules.class,idAuditProcessRulesPk);
		
		AuditProcessLog auditProcessLog = new AuditProcessLog();
		auditProcessLog.setConsistentRows(0L);
		auditProcessLog.setInconsistentRows(0L);
		auditProcessLog.setTotalAuditRows(0l);
		auditProcessLog.setPeriod(0);
		auditProcessLog.setAuditDate(CommonsUtilities.currentDateTime());
		auditProcessLog.setRegistryDate(CommonsUtilities.currentDateTime());
		auditProcessLog.setStartTime(CommonsUtilities.currentDateTime());
		auditProcessLog.setAuditType(auditProcessRules.getAuditType());
		auditProcessLog.setProcessState(AuditProcessLogStateType.PROCESSING.getCode());
		auditProcessLog.setProcessType(auditProcessRules.getProcessType());
		
		auditProcessLog.setRegistryUser(userName);
		auditProcessLog.setAuditProcessRules(auditProcessRules);
		
		logger.info("registering auditProcessLog for rule "+ idAuditProcessRulesPk);
		return create(auditProcessLog);
		
	}
	
	/**
	 * Gets the audit process rule service.
	 *
	 * @param idAuditProcessRulesPk the id audit process rules pk
	 * @return the audit process rule service
	 * @throws ServiceException the service exception
	 */
	public AuditProcessRules getAuditProcessRuleService(Long idAuditProcessRulesPk) throws ServiceException{
		StringBuilder querySql = new StringBuilder();
		querySql.append("Select audRul From AuditProcessRules audRul");
		querySql.append("		Inner Join Fetch audRul.auditStraightenBusiness audStr	");
		querySql.append("Where audRul.idAuditProcessRulesPk  = :processRulesPk ");
		
		Query query = em.createQuery(querySql.toString());
		query.setParameter("processRulesPk", idAuditProcessRulesPk);
		
		try{
			return (AuditProcessRules)query.getSingleResult();
		}catch(NoResultException ex){
			logger.info("AuditProcessRules not found for pk "+ idAuditProcessRulesPk);
			return null;
		}
	}
	
	/**
	 * Find audit process rule service.
	 *
	 * @param auditProcessTO the audit process to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<AuditProcessRules> findAuditProcessRuleService(AuditProcessLogTO auditProcessTO) throws ServiceException{
		StringBuilder querySql = new StringBuilder();
		querySql.append("Select audRul From AuditProcessRules audRul");
		querySql.append("		Inner Join Fetch audRul.auditStraightenBusiness audStr	");
		querySql.append("		Inner Join Fetch audStr.auditBusinessProcess audBus	");
		querySql.append("Where audStr.auditBusinessProcess.idAuditBusinessProcessPk = :auditBusinessProcess And		");
		querySql.append("	   audRul.auditType = :auditType And		");
		querySql.append("	   audRul.processType = :processType ");
		
		Query query = em.createQuery(querySql.toString());
		query.setParameter("auditType", auditProcessTO.getAuditType());
		query.setParameter("processType", auditProcessTO.getProcessType());
		query.setParameter("auditBusinessProcess", auditProcessTO.getAuditProcessId());
		
		try{
			return query.getResultList();
		}catch(NoResultException ex){
			return new ArrayList<AuditProcessRules>();
		}
	}
	
	/**
	 * Find audit business process.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<AuditBusinessProcess> findAuditBusinessProcess() throws ServiceException{
		StringBuilder querySql = new StringBuilder();
		querySql.append("Select audit From AuditBusinessProcess audit	");
		querySql.append("Where audit.auditState = :auditState");
		
		Query query = em.createQuery(querySql.toString());
		query.setParameter("auditState", BooleanType.YES.getCode());
		
		try{
			return query.getResultList();
		}catch(NoResultException ex){
			return new ArrayList<AuditBusinessProcess>();
		}
		
	}

	/**
	 * Execute audit process.
	 *
	 * @param idAuditProcessLogPk the id audit process log pk
	 * @param procedureName the procedure name
	 */
	public void executeAuditProcess(Long idAuditProcessLogPk,String procedureName) {
    		
			StringBuilder sb = new StringBuilder();
			sb.append("BEGIN PKG_AUDIT_BUSINESS_PROCESS.");
			sb.append(procedureName);
			//sb.append("(:idAuditProcessLogPrm); END;");
			sb.append(" END;");
			
    		Query query = em.createNativeQuery(sb.toString());
        	query.setParameter("p_id_log_pk", idAuditProcessLogPk);
        	query.executeUpdate();
        	
	}
}