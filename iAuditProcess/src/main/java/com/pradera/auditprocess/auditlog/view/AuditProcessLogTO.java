package com.pradera.auditprocess.auditlog.view;

import java.io.Serializable;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project PraderaSecurity
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Class UserAuditingServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 10/01/2013
 */
public class AuditProcessLogTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The request date. */
	private Date requestDate;
	
	/** The audit type. */
	private Integer auditType;
	
	/** The process type. */
	private Integer processType;
	
	/** The audit process id. */
	private Long auditProcessId;

	/**
	 * Gets the request date.
	 *
	 * @return the request date
	 */
	public Date getRequestDate() {
		return requestDate;
	}

	/**
	 * Sets the request date.
	 *
	 * @param requestDate the new request date
	 */
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	/**
	 * Gets the audit type.
	 *
	 * @return the audit type
	 */
	public Integer getAuditType() {
		return auditType;
	}

	/**
	 * Sets the audit type.
	 *
	 * @param auditType the new audit type
	 */
	public void setAuditType(Integer auditType) {
		this.auditType = auditType;
	}

	/**
	 * Gets the process type.
	 *
	 * @return the process type
	 */
	public Integer getProcessType() {
		return processType;
	}

	/**
	 * Sets the process type.
	 *
	 * @param processType the new process type
	 */
	public void setProcessType(Integer processType) {
		this.processType = processType;
	}

	/**
	 * Gets the audit process id.
	 *
	 * @return the audit process id
	 */
	public Long getAuditProcessId() {
		return auditProcessId;
	}

	/**
	 * Sets the audit process id.
	 *
	 * @param auditProcessId the new audit process id
	 */
	public void setAuditProcessId(Long auditProcessId) {
		this.auditProcessId = auditProcessId;
	}
}