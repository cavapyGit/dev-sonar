package com.pradera.auditprocess.auditlog.service;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.auditprocess.auditlog.facade.AuditProcessLogFacade;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.auditprocess.AuditProcessLog;
import com.pradera.model.auditprocess.type.AuditProcessLogStateType;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AuditProcessLogBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 24-ago-2015
 */
@BatchProcess(name="AuditProcessLogBatch")
@RequestScoped
public class AuditProcessLogBatch implements Serializable,JobExecution {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The logger. */
	@Inject
	private transient PraderaLogger logger;
	
	/** The audit process log facade. */
	@EJB
	AuditProcessLogFacade auditProcessLogFacade;

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		
		AuditProcessLog auditProcessLog = null;
		
		try {
			Long auditRulePk = null;
			
			for(ProcessLoggerDetail detail : processLogger.getProcessLoggerDetails()){
				if(detail.getParameterName().equals(GeneralConstants.PARAMETER_AUDIT_RULE_PK)){
					auditRulePk = new Long(detail.getParameterValue());
				}
			}
			
			auditProcessLog = auditProcessLogFacade.registerAuditLogs(auditRulePk);
			
			auditProcessLogFacade.executeAuditProcess(auditProcessLog.getIdAuditProcessLogPk(), auditProcessLog.getProcedureName());
			
		} catch (Exception e){
			logger.error("Error while executing AuditLog");
			e.printStackTrace();
			if(auditProcessLog!=null){
				finishProcessLog(auditProcessLog,AuditProcessLogStateType.ERROR.getCode());
			}
			return;
		}
		
		finishProcessLog(auditProcessLog,AuditProcessLogStateType.FINISHED.getCode());
		
	}
	
	/**
	 * Finish process log.
	 *
	 * @param auditProcessLog the audit process log
	 * @param state the state
	 */
	public void finishProcessLog(AuditProcessLog auditProcessLog, Integer state){
		try {
			auditProcessLog.setFinishTime(CommonsUtilities.currentDateTime());
			auditProcessLog.setProcessState(state);
			auditProcessLogFacade.updateAuditProcessLog(auditProcessLog);
		} catch (ServiceException e) {
			logger.error("Error while updating auditProcessLog");
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}
