package com.pradera.auditprocess.monitorprocess.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.auditprocess.AuditBusinessProcess;
import com.pradera.model.auditprocess.AuditProcessLog;
import com.pradera.model.auditprocess.AuditStraightenBusiness;
import com.pradera.model.generalparameter.ParameterTable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SearchMonitorProcessTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 24-ago-2015
 */
public class SearchMonitorProcessTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The lst audit business process. */
	private List<AuditBusinessProcess> lstAuditBusinessProcess;
	
	/** The lst audit straighten business. */
	private List<AuditStraightenBusiness> lstAuditStraightenBusiness;
	
	/** The lst process type. */
	private List<ParameterTable> lstAuditType, lstProcessType;
	
	/** The process type. */
	private Integer auditType, processType;
	
	/** The audit straighten business. */
	private Long auditBusinessProcess, auditStraightenBusiness;
	
	/** The end date. */
	private Date initialDate, endDate;
	
	/** The lst audit process log. */
	private GenericDataModel<AuditProcessLog> lstAuditProcessLog;
	
	/** The bl no result. */
	private boolean blNoResult;
	
	/**
	 * Gets the lst audit business process.
	 *
	 * @return the lst audit business process
	 */
	public List<AuditBusinessProcess> getLstAuditBusinessProcess() {
		return lstAuditBusinessProcess;
	}
	
	/**
	 * Sets the lst audit business process.
	 *
	 * @param lstAuditBusinessProcess the new lst audit business process
	 */
	public void setLstAuditBusinessProcess(
			List<AuditBusinessProcess> lstAuditBusinessProcess) {
		this.lstAuditBusinessProcess = lstAuditBusinessProcess;
	}
	
	/**
	 * Gets the lst audit straighten business.
	 *
	 * @return the lst audit straighten business
	 */
	public List<AuditStraightenBusiness> getLstAuditStraightenBusiness() {
		return lstAuditStraightenBusiness;
	}
	
	/**
	 * Sets the lst audit straighten business.
	 *
	 * @param lstAuditStraightenBusiness the new lst audit straighten business
	 */
	public void setLstAuditStraightenBusiness(
			List<AuditStraightenBusiness> lstAuditStraightenBusiness) {
		this.lstAuditStraightenBusiness = lstAuditStraightenBusiness;
	}
	
	/**
	 * Gets the lst audit type.
	 *
	 * @return the lst audit type
	 */
	public List<ParameterTable> getLstAuditType() {
		return lstAuditType;
	}
	
	/**
	 * Sets the lst audit type.
	 *
	 * @param lstAuditType the new lst audit type
	 */
	public void setLstAuditType(List<ParameterTable> lstAuditType) {
		this.lstAuditType = lstAuditType;
	}
	
	/**
	 * Gets the lst process type.
	 *
	 * @return the lst process type
	 */
	public List<ParameterTable> getLstProcessType() {
		return lstProcessType;
	}
	
	/**
	 * Sets the lst process type.
	 *
	 * @param lstProcessType the new lst process type
	 */
	public void setLstProcessType(List<ParameterTable> lstProcessType) {
		this.lstProcessType = lstProcessType;
	}
	
	/**
	 * Gets the audit type.
	 *
	 * @return the audit type
	 */
	public Integer getAuditType() {
		return auditType;
	}
	
	/**
	 * Sets the audit type.
	 *
	 * @param auditType the new audit type
	 */
	public void setAuditType(Integer auditType) {
		this.auditType = auditType;
	}
	
	/**
	 * Gets the process type.
	 *
	 * @return the process type
	 */
	public Integer getProcessType() {
		return processType;
	}
	
	/**
	 * Sets the process type.
	 *
	 * @param processType the new process type
	 */
	public void setProcessType(Integer processType) {
		this.processType = processType;
	}
	
	/**
	 * Gets the audit business process.
	 *
	 * @return the audit business process
	 */
	public Long getAuditBusinessProcess() {
		return auditBusinessProcess;
	}
	
	/**
	 * Sets the audit business process.
	 *
	 * @param auditBusinessProcess the new audit business process
	 */
	public void setAuditBusinessProcess(Long auditBusinessProcess) {
		this.auditBusinessProcess = auditBusinessProcess;
	}
	
	/**
	 * Gets the audit straighten business.
	 *
	 * @return the audit straighten business
	 */
	public Long getAuditStraightenBusiness() {
		return auditStraightenBusiness;
	}
	
	/**
	 * Sets the audit straighten business.
	 *
	 * @param auditStraightenBusiness the new audit straighten business
	 */
	public void setAuditStraightenBusiness(Long auditStraightenBusiness) {
		this.auditStraightenBusiness = auditStraightenBusiness;
	}
	
	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}
	
	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	
	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}
	
	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	/**
	 * Gets the lst audit process log.
	 *
	 * @return the lst audit process log
	 */
	public GenericDataModel<AuditProcessLog> getLstAuditProcessLog() {
		return lstAuditProcessLog;
	}
	
	/**
	 * Sets the lst audit process log.
	 *
	 * @param lstAuditProcessLog the new lst audit process log
	 */
	public void setLstAuditProcessLog(
			GenericDataModel<AuditProcessLog> lstAuditProcessLog) {
		this.lstAuditProcessLog = lstAuditProcessLog;
	}
	
	/**
	 * Checks if is bl no result.
	 *
	 * @return true, if is bl no result
	 */
	public boolean isBlNoResult() {
		return blNoResult;
	}
	
	/**
	 * Sets the bl no result.
	 *
	 * @param blNoResult the new bl no result
	 */
	public void setBlNoResult(boolean blNoResult) {
		this.blNoResult = blNoResult;
	}	
}
