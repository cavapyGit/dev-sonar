package com.pradera.auditprocess.monitorprocess.view;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.auditprocess.monitorprocess.facade.MonitorProcessServiceFacade;
import com.pradera.auditprocess.monitorprocess.to.SearchMonitorProcessTO;
import com.pradera.auditprocess.processrules.facade.AuditProcessRulesServiceFacade;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.auditprocess.AuditHeaderResult;
import com.pradera.model.auditprocess.AuditProcessLog;
import com.pradera.model.auditprocess.type.AuditProcessType;
import com.pradera.model.auditprocess.type.AuditType;
import com.pradera.model.generalparameter.ParameterTable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MonitorProcessBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 24-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class MonitorProcessBean extends GenericBaseBean{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The search monitor process to. */
	private SearchMonitorProcessTO searchMonitorProcessTO;
	
	/** The Constant VIEW_MAPPING. */
	private static final String VIEW_MAPPING = "viewMonitorProcess";
	
	/** The audit process log. */
	private AuditProcessLog auditProcessLog;
	
	/** The lst audit header result. */
	private List<AuditHeaderResult> lstAuditHeaderResult;
	
	/** The lst result. */
	private List<Object[]> lstResult;
	
	/** The user info. */
	@Inject UserInfo userInfo;
	
	/** The audit process rules service facade. */
	@EJB AuditProcessRulesServiceFacade auditProcessRulesServiceFacade;
	
	/** The monitor process service facade. */
	@EJB MonitorProcessServiceFacade monitorProcessServiceFacade;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		try {
			searchMonitorProcessTO = new SearchMonitorProcessTO();
			searchMonitorProcessTO.setInitialDate(getCurrentSystemDate());
			searchMonitorProcessTO.setEndDate(getCurrentSystemDate());
			fillCombos();			
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Change audit business process.
	 */
	public void changeAuditBusinessProcess(){
		try {
			cleanResults();
			searchMonitorProcessTO.setLstAuditStraightenBusiness(null);
			searchMonitorProcessTO.setAuditStraightenBusiness(null);
			if(Validations.validateIsNotNullAndNotEmpty(searchMonitorProcessTO.getAuditBusinessProcess()))
				searchMonitorProcessTO.setLstAuditStraightenBusiness(auditProcessRulesServiceFacade.getListAuditStraightenBusiness(searchMonitorProcessTO.getAuditBusinessProcess()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Search audit process rules.
	 */
	public void searchAuditProcessRules(){
		try {
			cleanResults();
			searchMonitorProcessTO.setBlNoResult(true);
			List<AuditProcessLog> lstResult = monitorProcessServiceFacade.getListAuditProcessLog(searchMonitorProcessTO);
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				searchMonitorProcessTO.setLstAuditProcessLog(new GenericDataModel<AuditProcessLog>(lstResult));
				searchMonitorProcessTO.setBlNoResult(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Clean search.
	 */
	public void cleanSearch(){
		cleanResults();
		searchMonitorProcessTO.setAuditBusinessProcess(null);
		searchMonitorProcessTO.setAuditStraightenBusiness(null);
		searchMonitorProcessTO.setLstAuditStraightenBusiness(null);
		searchMonitorProcessTO.setProcessType(null);
		searchMonitorProcessTO.setAuditType(null);
		searchMonitorProcessTO.setInitialDate(getCurrentSystemDate());
		searchMonitorProcessTO.setEndDate(getCurrentSystemDate());
		
		JSFUtilities.resetComponent("frmMonitorProcess:cboAuditBusinessProcess");
	}
	
	/**
	 * Clean results.
	 */
	public void cleanResults(){
		searchMonitorProcessTO.setBlNoResult(false);
		searchMonitorProcessTO.setLstAuditProcessLog(null);
	}
	
	/**
	 * View process.
	 *
	 * @param auditProcessLog the audit process log
	 * @return the string
	 */
	public String viewProcess(AuditProcessLog auditProcessLog){
		try {
			this.auditProcessLog = monitorProcessServiceFacade.getAuditProcessLog(auditProcessLog);
			lstAuditHeaderResult = monitorProcessServiceFacade.getListAuditHeaderResult(this.auditProcessLog.getAuditProcessRules().getAuditStraightenBusiness().getIdStraightenBusinessPk());
			lstResult = monitorProcessServiceFacade.getResult(lstAuditHeaderResult, this.auditProcessLog.getIdAuditProcessLogPk());
			return VIEW_MAPPING;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Fill combos.
	 *
	 * @throws ServiceException the service exception
	 */
	private void fillCombos() throws ServiceException{
		List<ParameterTable> lstTemp = new ArrayList<ParameterTable>();
		ParameterTable paramTab = new ParameterTable();
		paramTab.setParameterTablePk(AuditType.INTERNAL.getCode());
		paramTab.setParameterName(AuditType.INTERNAL.getValue());
		lstTemp.add(paramTab);
		paramTab = new ParameterTable();
		paramTab.setParameterTablePk(AuditType.EXTERNAL.getCode());
		paramTab.setParameterName(AuditType.EXTERNAL.getValue());
		lstTemp.add(paramTab);
		searchMonitorProcessTO.setLstAuditType(lstTemp);
		lstTemp = new ArrayList<ParameterTable>();
		paramTab = new ParameterTable();
		paramTab.setParameterTablePk(AuditProcessType.MANUAL.getCode());
		paramTab.setParameterName(AuditProcessType.MANUAL.getValue());
		lstTemp.add(paramTab);
		paramTab = new ParameterTable();
		paramTab.setParameterTablePk(AuditProcessType.AUTOMATIC.getCode());
		paramTab.setParameterName(AuditProcessType.AUTOMATIC.getValue());
		lstTemp.add(paramTab);
		searchMonitorProcessTO.setLstProcessType(lstTemp);
		searchMonitorProcessTO.setLstAuditBusinessProcess(auditProcessRulesServiceFacade.getListAuditBusinessProcess());
	}

	/**
	 * Gets the search monitor process to.
	 *
	 * @return the search monitor process to
	 */
	public SearchMonitorProcessTO getSearchMonitorProcessTO() {
		return searchMonitorProcessTO;
	}
	
	/**
	 * Sets the search monitor process to.
	 *
	 * @param searchMonitorProcessTO the new search monitor process to
	 */
	public void setSearchMonitorProcessTO(
			SearchMonitorProcessTO searchMonitorProcessTO) {
		this.searchMonitorProcessTO = searchMonitorProcessTO;
	}
	
	/**
	 * Gets the audit process log.
	 *
	 * @return the audit process log
	 */
	public AuditProcessLog getAuditProcessLog() {
		return auditProcessLog;
	}
	
	/**
	 * Sets the audit process log.
	 *
	 * @param auditProcessLog the new audit process log
	 */
	public void setAuditProcessLog(AuditProcessLog auditProcessLog) {
		this.auditProcessLog = auditProcessLog;
	}

	/**
	 * Gets the lst audit header result.
	 *
	 * @return the lst audit header result
	 */
	public List<AuditHeaderResult> getLstAuditHeaderResult() {
		return lstAuditHeaderResult;
	}
	
	/**
	 * Sets the lst audit header result.
	 *
	 * @param lstAuditHeaderResult the new lst audit header result
	 */
	public void setLstAuditHeaderResult(List<AuditHeaderResult> lstAuditHeaderResult) {
		this.lstAuditHeaderResult = lstAuditHeaderResult;
	}

	/**
	 * Gets the lst result.
	 *
	 * @return the lst result
	 */
	public List<Object[]> getLstResult() {
		return lstResult;
	}

	/**
	 * Sets the lst result.
	 *
	 * @param lstResult the new lst result
	 */
	public void setLstResult(List<Object[]> lstResult) {
		this.lstResult = lstResult;
	}
	
}
