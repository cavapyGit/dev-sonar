package com.pradera.auditprocess.monitorprocess.facade;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.auditprocess.monitorprocess.service.MonitorProcessServiceBean;
import com.pradera.auditprocess.monitorprocess.to.SearchMonitorProcessTO;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.auditprocess.AuditHeaderResult;
import com.pradera.model.auditprocess.AuditProcessLog;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MonitorProcessServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 24-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class MonitorProcessServiceFacade {
	
	/** The monitor process service bean. */
	@EJB MonitorProcessServiceBean monitorProcessServiceBean;
	
	/** The transaction registry. */
	@Resource TransactionSynchronizationRegistry transactionRegistry;

	/**
	 * Gets the list audit process log.
	 *
	 * @param searchMonitorProcessTO the search monitor process to
	 * @return the list audit process log
	 * @throws ServiceException the service exception
	 */
	public List<AuditProcessLog> getListAuditProcessLog(SearchMonitorProcessTO searchMonitorProcessTO) throws ServiceException{
		return monitorProcessServiceBean.getListAuditProcessLog(searchMonitorProcessTO);
	}

	/**
	 * Gets the audit process log.
	 *
	 * @param auditProcessLog the audit process log
	 * @return the audit process log
	 * @throws ServiceException the service exception
	 */
	public AuditProcessLog getAuditProcessLog(AuditProcessLog auditProcessLog) throws ServiceException{
		return monitorProcessServiceBean.getAuditProcessLog(auditProcessLog);
	}

	/**
	 * Gets the list audit header result.
	 *
	 * @param idStraightenBusinessPk the id straighten business pk
	 * @return the list audit header result
	 * @throws ServiceException the service exception
	 */
	public List<AuditHeaderResult> getListAuditHeaderResult(Long idStraightenBusinessPk) throws ServiceException{
		return monitorProcessServiceBean.getListAuditHeaderResult(idStraightenBusinessPk);
	}

	/**
	 * Gets the result.
	 *
	 * @param lstAuditHeaderResult the lst audit header result
	 * @param idAuditProcessLogPk the id audit process log pk
	 * @return the result
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getResult(List<AuditHeaderResult> lstAuditHeaderResult, Long idAuditProcessLogPk) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ");
		for(int i = 0;i<lstAuditHeaderResult.size();i++){
			sbQuery.append(lstAuditHeaderResult.get(i).getJavaAttribute());
			if(i<lstAuditHeaderResult.size()-1)
				sbQuery.append(GeneralConstants.STR_COMMA);
		}		
		return monitorProcessServiceBean.getResult(sbQuery, idAuditProcessLogPk);
	}

}
