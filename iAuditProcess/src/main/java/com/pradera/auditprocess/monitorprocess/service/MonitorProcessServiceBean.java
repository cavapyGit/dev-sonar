package com.pradera.auditprocess.monitorprocess.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.auditprocess.monitorprocess.to.SearchMonitorProcessTO;
import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.auditprocess.AuditHeaderResult;
import com.pradera.model.auditprocess.AuditProcessLog;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MonitorProcessServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 24-ago-2015
 */
@Stateless
@Performance
public class MonitorProcessServiceBean extends CrudDaoServiceBean{

	/**
	 * Gets the list audit process log.
	 *
	 * @param searchMonitorProcessTO the search monitor process to
	 * @return the list audit process log
	 */
	@SuppressWarnings("unchecked")
	public List<AuditProcessLog> getListAuditProcessLog(SearchMonitorProcessTO searchMonitorProcessTO) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT new AuditProcessLog (");
		sbQuery.append("      apl.idAuditProcessLogPk, ");
		sbQuery.append("      asb.straightenName, ");
		sbQuery.append("      apl.startTime, ");
		sbQuery.append("      apl.finishTime, ");
		sbQuery.append("      apl.registryDate, ");
		sbQuery.append("      apl.auditType, ");
		sbQuery.append("      apl.processType, ");
		sbQuery.append("      apl.period, ");
		sbQuery.append("      apl.processState, ");
		sbQuery.append("	  (select count(apru.idAuditProcessResultPk) from AuditProcessResult apru where apru.auditProcessLog.idAuditProcessLogPk = apl.idAuditProcessLogPk) ");
		sbQuery.append("	) ");
		sbQuery.append("	FROM AuditProcessLog apl");
		sbQuery.append("	INNER JOIN apl.auditProcessRules apr");
		sbQuery.append("	INNER JOIN apr.auditBusinessProcess abp ");
		sbQuery.append("	INNER JOIN apr.auditStraightenBusiness asb ");
		sbQuery.append("	WHERE abp.idAuditBusinessProcessPk = :idAuditBusinessProcessPk");
		sbQuery.append("	AND TRUNC(apl.registryDate) between :initialDate and :endDate");
		
		if(Validations.validateIsNotNullAndNotEmpty(searchMonitorProcessTO.getAuditStraightenBusiness()))
			sbQuery.append("	AND apl.auditProcessRules.auditStraightenBusiness.idStraightenBusinessPk = :idStraightenBusinessPk");
		if(Validations.validateIsNotNullAndNotEmpty(searchMonitorProcessTO.getAuditType()))
			sbQuery.append("	AND apl.auditProcessRules.auditType = :auditType");
		if(Validations.validateIsNotNullAndNotEmpty(searchMonitorProcessTO.getProcessType()))
			sbQuery.append("	AND apl.processType = :processType");
		
		sbQuery.append(" order by apl.startTime desc ");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idAuditBusinessProcessPk", searchMonitorProcessTO.getAuditBusinessProcess());
		query.setParameter("initialDate", searchMonitorProcessTO.getInitialDate());
		query.setParameter("endDate", searchMonitorProcessTO.getEndDate());
		if(Validations.validateIsNotNullAndNotEmpty(searchMonitorProcessTO.getAuditStraightenBusiness()))
			query.setParameter("idStraightenBusinessPk", searchMonitorProcessTO.getAuditStraightenBusiness());
		if(Validations.validateIsNotNullAndNotEmpty(searchMonitorProcessTO.getAuditType()))
			query.setParameter("auditType", searchMonitorProcessTO.getAuditType());
		if(Validations.validateIsNotNullAndNotEmpty(searchMonitorProcessTO.getProcessType()))
			query.setParameter("processType", searchMonitorProcessTO.getProcessType());
		return (List<AuditProcessLog>)query.getResultList();
	}

	/**
	 * Gets the audit process log.
	 *
	 * @param auditProcessLog the audit process log
	 * @return the audit process log
	 * @throws ServiceException the service exception
	 */
	public AuditProcessLog getAuditProcessLog(AuditProcessLog auditProcessLog) throws ServiceException {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT apl");
			sbQuery.append("	FROM AuditProcessLog apl");
			sbQuery.append("	INNER JOIN FETCH apl.auditProcessRules apr");
			sbQuery.append("	INNER JOIN FETCH apr.auditBusinessProcess");
			sbQuery.append("	INNER JOIN FETCH apr.auditStraightenBusiness asb");
			sbQuery.append("	WHERE apl.idAuditProcessLogPk = :idAuditProcessLogPk");			
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idAuditProcessLogPk", auditProcessLog.getIdAuditProcessLogPk());
			return (AuditProcessLog)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}			
	}
	
	/**
	 * Gets the list audit header result.
	 *
	 * @param idStraightenBusinessPk the id straighten business pk
	 * @return the list audit header result
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<AuditHeaderResult> getListAuditHeaderResult(Long idStraightenBusinessPk) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ahr");
		sbQuery.append("	FROM AuditHeaderResult ahr");
		sbQuery.append("	WHERE ahr.auditStraightenProcess.idStraightenBusinessPk = :idStraightenBusinessPk");			
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idStraightenBusinessPk", idStraightenBusinessPk);
		return (List<AuditHeaderResult>)query.getResultList();		
	}

	/**
	 * Gets the result.
	 *
	 * @param sbQuery the sb query
	 * @param idAuditProcessLogPk the id audit process log pk
	 * @return the result
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getResult(StringBuilder sbQuery, Long idAuditProcessLogPk) throws ServiceException{
		sbQuery.append("	FROM AuditProcessResult apr");
		sbQuery.append("	WHERE apr.auditProcessLog.idAuditProcessLogPk = :idAuditProcessLogPk");
		sbQuery.append("	ORDER BY apr.idAuditProcessResultPk");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idAuditProcessLogPk", idAuditProcessLogPk);
		return (List<Object[]>)query.getResultList();	
	}
}
