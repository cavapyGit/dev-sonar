package com.pradera.auditprocess.processrules.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.auditprocess.AuditBusinessProcess;
import com.pradera.model.auditprocess.AuditStraightenBusiness;
import com.pradera.model.generalparameter.ParameterTable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RegisterAuditProcessRulesTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 24-ago-2015
 */
public class RegisterAuditProcessRulesTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The registry date. */
	private Date registryDate;
	
	/** The lst process type. */
	private List<ParameterTable> lstAuditType, lstAudit, lstProcessType;
	
	/** The audit type. */
	private Integer auditType;
	
	/** The audit business process. */
	private Long auditBusinessProcess;
	
	/** The lst audit business process. */
	private List<AuditBusinessProcess> lstAuditBusinessProcess;
	
	/** The lst audit straighten business. */
	private GenericDataModel<AuditStraightenBusiness> lstAuditStraightenBusiness;
	
	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}
	
	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}
	
	/**
	 * Gets the lst audit type.
	 *
	 * @return the lst audit type
	 */
	public List<ParameterTable> getLstAuditType() {
		return lstAuditType;
	}
	
	/**
	 * Sets the lst audit type.
	 *
	 * @param lstAuditType the new lst audit type
	 */
	public void setLstAuditType(List<ParameterTable> lstAuditType) {
		this.lstAuditType = lstAuditType;
	}
	
	/**
	 * Gets the lst audit.
	 *
	 * @return the lst audit
	 */
	public List<ParameterTable> getLstAudit() {
		return lstAudit;
	}
	
	/**
	 * Sets the lst audit.
	 *
	 * @param lstAudit the new lst audit
	 */
	public void setLstAudit(List<ParameterTable> lstAudit) {
		this.lstAudit = lstAudit;
	}
	
	/**
	 * Gets the lst process type.
	 *
	 * @return the lst process type
	 */
	public List<ParameterTable> getLstProcessType() {
		return lstProcessType;
	}
	
	/**
	 * Sets the lst process type.
	 *
	 * @param lstProcessType the new lst process type
	 */
	public void setLstProcessType(List<ParameterTable> lstProcessType) {
		this.lstProcessType = lstProcessType;
	}
	
	/**
	 * Gets the audit type.
	 *
	 * @return the audit type
	 */
	public Integer getAuditType() {
		return auditType;
	}
	
	/**
	 * Sets the audit type.
	 *
	 * @param auditType the new audit type
	 */
	public void setAuditType(Integer auditType) {
		this.auditType = auditType;
	}
	
	/**
	 * Gets the lst audit straighten business.
	 *
	 * @return the lst audit straighten business
	 */
	public GenericDataModel<AuditStraightenBusiness> getLstAuditStraightenBusiness() {
		return lstAuditStraightenBusiness;
	}
	
	/**
	 * Sets the lst audit straighten business.
	 *
	 * @param lstAuditStraightenBusiness the new lst audit straighten business
	 */
	public void setLstAuditStraightenBusiness(
			GenericDataModel<AuditStraightenBusiness> lstAuditStraightenBusiness) {
		this.lstAuditStraightenBusiness = lstAuditStraightenBusiness;
	}
	
	/**
	 * Gets the audit business process.
	 *
	 * @return the audit business process
	 */
	public Long getAuditBusinessProcess() {
		return auditBusinessProcess;
	}
	
	/**
	 * Sets the audit business process.
	 *
	 * @param auditBusinessProcess the new audit business process
	 */
	public void setAuditBusinessProcess(Long auditBusinessProcess) {
		this.auditBusinessProcess = auditBusinessProcess;
	}
	
	/**
	 * Gets the lst audit business process.
	 *
	 * @return the lst audit business process
	 */
	public List<AuditBusinessProcess> getLstAuditBusinessProcess() {
		return lstAuditBusinessProcess;
	}
	
	/**
	 * Sets the lst audit business process.
	 *
	 * @param lstAuditBusinessProcess the new lst audit business process
	 */
	public void setLstAuditBusinessProcess(
			List<AuditBusinessProcess> lstAuditBusinessProcess) {
		this.lstAuditBusinessProcess = lstAuditBusinessProcess;
	}	
}
