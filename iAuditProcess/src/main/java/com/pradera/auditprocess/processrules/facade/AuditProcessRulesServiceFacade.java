package com.pradera.auditprocess.processrules.facade;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.auditprocess.auditlog.service.AuditProcessLogExecutor;
import com.pradera.auditprocess.processrules.service.AuditProcessRulesServiceBean;
import com.pradera.auditprocess.processrules.to.SearchAuditProcessRulesTO;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.auditprocess.AuditBusinessProcess;
import com.pradera.model.auditprocess.AuditProcessRules;
import com.pradera.model.auditprocess.AuditStraightenBusiness;
import com.pradera.model.auditprocess.type.AuditProcessRuleState;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AuditProcessRulesServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 24-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class AuditProcessRulesServiceFacade {

	/** The audit process rules service bean. */
	@EJB AuditProcessRulesServiceBean auditProcessRulesServiceBean;
	
	/** The transaction registry. */
	@Resource TransactionSynchronizationRegistry transactionRegistry;
	
	/** The audit process log executor. */
	@EJB AuditProcessLogExecutor auditProcessLogExecutor;

	/**
	 * Gets the list audit straighten business reg.
	 *
	 * @param auditProcessBusiness the audit process business
	 * @return the list audit straighten business reg
	 * @throws ServiceException the service exception
	 */
	public List<AuditStraightenBusiness> getListAuditStraightenBusinessReg(Long auditProcessBusiness) throws ServiceException{
		return auditProcessRulesServiceBean.getListAuditStraightenBusinessReg(auditProcessBusiness);
	}

	/**
	 * Save process rule.
	 *
	 * @param auditStraightenBusiness the audit straighten business
	 * @param auditType the audit type
	 * @throws ServiceException the service exception
	 */
	public void saveProcessRule(AuditStraightenBusiness auditStraightenBusiness, Integer auditType) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
	    AuditProcessRules auditProcessRule = new AuditProcessRules();
	    auditProcessRule.setAuditBusinessProcess(auditStraightenBusiness.getAuditBusinessProcess());
	    auditProcessRule.setAuditStraightenBusiness(auditStraightenBusiness);
	    auditProcessRule.setAuditType(auditType);
	    if(auditStraightenBusiness.isBlDaily())
	    	auditProcessRule.setIndDaily(BooleanType.YES.getCode());
	    else
	    	auditProcessRule.setIndDaily(BooleanType.NO.getCode());
	    if(auditStraightenBusiness.isBlMonthly())
	    	auditProcessRule.setIndMonthly(BooleanType.YES.getCode());
	    else
	    	auditProcessRule.setIndMonthly(BooleanType.NO.getCode());
	    if(auditStraightenBusiness.isBlAnnual())
	    	auditProcessRule.setIndAnnual(BooleanType.YES.getCode());
	    else
	    	auditProcessRule.setIndAnnual(BooleanType.NO.getCode());
	    auditProcessRule.setProcessType(auditStraightenBusiness.getProcessType());
	    auditProcessRule.setExecuteTime(auditStraightenBusiness.getExecuteTime());
	    auditProcessRule.setProcessState(AuditProcessRuleState.REGISTERED.getCode());
	    auditProcessRule.setRegistryDate(CommonsUtilities.currentDateTime());
	    AuditProcessRules auditProcessRuleTemp = auditProcessRulesServiceBean.findAuditProcessRules(auditProcessRule);
	    if(Validations.validateIsNotNullAndNotEmpty(auditProcessRuleTemp))
	    	throw new ServiceException(ErrorServiceType.AUDIT_PROCESS_RULES_EXIST, new Object[]{auditStraightenBusiness.getStraightenName()});
	    auditProcessLogExecutor.registerRule(auditProcessRule);
	}

	/**
	 * Gets the list audit business process.
	 *
	 * @return the list audit business process
	 * @throws ServiceException the service exception
	 */
	public List<AuditBusinessProcess> getListAuditBusinessProcess() throws ServiceException{
		return auditProcessRulesServiceBean.getListAuditBusinessProcess();
	}
	
	/**
	 * Gets the list audit straighten business.
	 *
	 * @param idAuditBusinessProcess the id audit business process
	 * @return the list audit straighten business
	 * @throws ServiceException the service exception
	 */
	public List<AuditStraightenBusiness> getListAuditStraightenBusiness(Long idAuditBusinessProcess) throws ServiceException{
		return auditProcessRulesServiceBean.getListAuditStraightenBusiness(idAuditBusinessProcess);
	}

	/**
	 * Gets the list audit process rules.
	 *
	 * @param searchAuditProcessRulesTO the search audit process rules to
	 * @return the list audit process rules
	 * @throws ServiceException the service exception
	 */
	public List<AuditProcessRules> getListAuditProcessRules(SearchAuditProcessRulesTO searchAuditProcessRulesTO) throws ServiceException{
		return  auditProcessLogExecutor.getListAuditProcessRules(searchAuditProcessRulesTO);
	}
	
}
