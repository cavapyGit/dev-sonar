package com.pradera.auditprocess.processrules.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.auditprocess.AuditBusinessProcess;
import com.pradera.model.auditprocess.AuditProcessRules;
import com.pradera.model.auditprocess.AuditStraightenBusiness;
import com.pradera.model.auditprocess.type.AuditProcessRuleState;
import com.pradera.model.auditprocess.type.AuditStraightenState;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AuditProcessRulesServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 24-ago-2015
 */
@Stateless
@Performance
public class AuditProcessRulesServiceBean extends CrudDaoServiceBean{

	/**
	 * Gets the list audit straighten business reg.
	 *
	 * @param idAuditBusinessProcessPk the id audit business process pk
	 * @return the list audit straighten business reg
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<AuditStraightenBusiness> getListAuditStraightenBusinessReg(Long idAuditBusinessProcessPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT asb");
		sbQuery.append("	FROM AuditStraightenBusiness asb");
		sbQuery.append("	INNER JOIN FETCH asb.auditBusinessProcess");
		sbQuery.append("	WHERE asb.auditState = :stateStraighten");
		sbQuery.append("	AND asb.auditBusinessProcess.auditState = :state");
		sbQuery.append("	AND asb.auditBusinessProcess.idAuditBusinessProcessPk = :idAuditBusinessProcessPk");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("state", BooleanType.YES.getCode());
		query.setParameter("stateStraighten", AuditStraightenState.REGISTERED.getCode());
		query.setParameter("idAuditBusinessProcessPk", idAuditBusinessProcessPk);
		return (List<AuditStraightenBusiness>)query.getResultList();
	}

	/**
	 * Gets the list audit business process.
	 *
	 * @return the list audit business process
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<AuditBusinessProcess> getListAuditBusinessProcess() throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT abp");
		sbQuery.append("	FROM AuditBusinessProcess abp");
		sbQuery.append("	WHERE abp.auditState = :state");
		sbQuery.append("	ORDER BY abp.processName ASC");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("state", BooleanType.YES.getCode());
		return (List<AuditBusinessProcess>)query.getResultList();
	}
	
	/**
	 * Gets the list audit straighten business.
	 *
	 * @param idAuditBusinessProcessPk the id audit business process pk
	 * @return the list audit straighten business
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<AuditStraightenBusiness> getListAuditStraightenBusiness(Long idAuditBusinessProcessPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT asb");
		sbQuery.append("	FROM AuditStraightenBusiness asb");
		sbQuery.append("	WHERE asb.auditState = :state");
		sbQuery.append("	AND asb.auditBusinessProcess.idAuditBusinessProcessPk = :idAuditBusinessProcessPk");
		sbQuery.append("	ORDER BY asb.straightenName ASC");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("state", AuditStraightenState.REGISTERED.getCode());
		query.setParameter("idAuditBusinessProcessPk", idAuditBusinessProcessPk);
		return (List<AuditStraightenBusiness>)query.getResultList();
	}


	/**
	 * Find audit process rules.
	 *
	 * @param auditProcessRule the audit process rule
	 * @return the audit process rules
	 */
	public AuditProcessRules findAuditProcessRules(AuditProcessRules auditProcessRule) {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT apr");
			sbQuery.append("	FROM AuditProcessRules apr");
			sbQuery.append("	WHERE apr.auditType = :auditType");
			sbQuery.append("	AND apr.processType = :processType");
			sbQuery.append("	AND apr.indAnnual = :indAnnual");
			sbQuery.append("	AND apr.indDaily = :indDaily");
			sbQuery.append("	AND apr.indMonthly = :indMonthly");
			sbQuery.append("	AND apr.processState = :processState");
			sbQuery.append("	AND apr.auditBusinessProcess.idAuditBusinessProcessPk = :idAuditBusinessProcessPk");
			sbQuery.append("	AND apr.auditStraightenBusiness.idStraightenBusinessPk = :idStraightenBusinessPk");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("auditType", auditProcessRule.getAuditType());
			query.setParameter("processType", auditProcessRule.getProcessType());
			query.setParameter("indAnnual", auditProcessRule.getIndAnnual());
			query.setParameter("indDaily", auditProcessRule.getIndDaily());
			query.setParameter("indMonthly", auditProcessRule.getIndMonthly());
			query.setParameter("processState", AuditProcessRuleState.REGISTERED.getCode());
			query.setParameter("idAuditBusinessProcessPk", auditProcessRule.getAuditBusinessProcess().getIdAuditBusinessProcessPk());
			query.setParameter("idStraightenBusinessPk", auditProcessRule.getAuditStraightenBusiness().getIdStraightenBusinessPk());
			return (AuditProcessRules)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
}
