package com.pradera.auditprocess.processrules.view;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.auditprocess.processrules.facade.AuditProcessRulesServiceFacade;
import com.pradera.auditprocess.processrules.to.RegisterAuditProcessRulesTO;
import com.pradera.auditprocess.processrules.to.SearchAuditProcessRulesTO;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.auditprocess.AuditProcessRules;
import com.pradera.model.auditprocess.AuditStraightenBusiness;
import com.pradera.model.auditprocess.type.AuditProcessType;
import com.pradera.model.auditprocess.type.AuditType;
import com.pradera.model.generalparameter.ParameterTable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AuditProcessRulesBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 24-ago-2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class AuditProcessRulesBean extends GenericBaseBean{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The register audit process rules to. */
	private RegisterAuditProcessRulesTO registerAuditProcessRulesTO;
	
	/** The search audit process rules to. */
	private SearchAuditProcessRulesTO searchAuditProcessRulesTO;
	
	/** The Constant REGISTER_MAPPING. */
	private static final String REGISTER_MAPPING = "registerProcessRules";
	
	/** The user info. */
	@Inject UserInfo userInfo;
	
	/** The audit process rules service facade. */
	@EJB AuditProcessRulesServiceFacade auditProcessRulesServiceFacade;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		try {
			searchAuditProcessRulesTO = new SearchAuditProcessRulesTO();
			fillCombos();			
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Change audit business process.
	 */
	public void changeAuditBusinessProcess(){
		try {
			cleanResults();
			searchAuditProcessRulesTO.setLstAuditStraightenBusiness(null);
			searchAuditProcessRulesTO.setAuditStraightenBusiness(null);
			if(Validations.validateIsNotNullAndNotEmpty(searchAuditProcessRulesTO.getAuditBusinessProcess()))
				searchAuditProcessRulesTO.setLstAuditStraightenBusiness(auditProcessRulesServiceFacade.getListAuditStraightenBusiness(searchAuditProcessRulesTO.getAuditBusinessProcess()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Change audit business process reg.
	 */
	public void changeAuditBusinessProcessReg(){
		try {
			registerAuditProcessRulesTO.setLstAuditStraightenBusiness(null);
			if(Validations.validateIsNotNullAndNotEmpty(registerAuditProcessRulesTO.getAuditBusinessProcess())){
				List<AuditStraightenBusiness> lstResult = auditProcessRulesServiceFacade.getListAuditStraightenBusinessReg(registerAuditProcessRulesTO.getAuditBusinessProcess());
				if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
					for(AuditStraightenBusiness auditStraightenBusiness:lstResult){
						auditStraightenBusiness.setAudit(BooleanType.NO.getCode());
						auditStraightenBusiness.setExecuteTime(getCurrentSystemDate());
					}
					registerAuditProcessRulesTO.setLstAuditStraightenBusiness(new GenericDataModel<AuditStraightenBusiness>(lstResult));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Clean results.
	 */
	public void cleanResults(){
		searchAuditProcessRulesTO.setBlNoResult(false);
		searchAuditProcessRulesTO.setLstAuditProcessRules(null);
	}
	
	/**
	 * Search audit process rules.
	 */
	public void searchAuditProcessRules(){
		try {
			cleanResults();
			searchAuditProcessRulesTO.setBlNoResult(true);
			List<AuditProcessRules> lstResult = auditProcessRulesServiceFacade.getListAuditProcessRules(searchAuditProcessRulesTO);
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				searchAuditProcessRulesTO.setLstAuditProcessRules(new GenericDataModel<AuditProcessRules>(lstResult));
				searchAuditProcessRulesTO.setBlNoResult(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Clean search.
	 */
	public void cleanSearch(){
		cleanResults();
		searchAuditProcessRulesTO.setAuditBusinessProcess(null);
		searchAuditProcessRulesTO.setAuditStraightenBusiness(null);
		searchAuditProcessRulesTO.setLstAuditStraightenBusiness(null);
		searchAuditProcessRulesTO.setProcessType(null);
		searchAuditProcessRulesTO.setAuditType(null);
		
		JSFUtilities.resetComponent("frmProcessRules:cboAuditBusinessProcess");
	}
	
	/**
	 * Register audit process rules.
	 *
	 * @return the string
	 */
	public String registerAuditProcessRules(){
		try {
			registerAuditProcessRulesTO = new RegisterAuditProcessRulesTO();
			List<ParameterTable> lstTemp = new ArrayList<ParameterTable>();
			ParameterTable paramTab = new ParameterTable();
			paramTab.setParameterTablePk(BooleanType.YES.getCode());
			paramTab.setParameterName(BooleanType.YES.getValue());		
			lstTemp.add(paramTab);
			paramTab = new ParameterTable();
			paramTab.setParameterTablePk(BooleanType.NO.getCode());
			paramTab.setParameterName(BooleanType.NO.getValue());		
			lstTemp.add(paramTab);
			registerAuditProcessRulesTO.setLstAudit(lstTemp);
			registerAuditProcessRulesTO.setRegistryDate(getCurrentSystemDate());
			registerAuditProcessRulesTO.setLstAuditType(searchAuditProcessRulesTO.getLstAuditType());
			registerAuditProcessRulesTO.setLstProcessType(searchAuditProcessRulesTO.getLstProcessType());
			registerAuditProcessRulesTO.setLstAuditBusinessProcess(searchAuditProcessRulesTO.getLstAuditBusinessProcess());			
			return REGISTER_MAPPING;	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Before save.
	 */
	public void beforeSave(){
		boolean blHaveOne = false;
		if(Validations.validateIsNotNull(registerAuditProcessRulesTO.getLstAuditStraightenBusiness())){
			if(Validations.validateListIsNotNullAndNotEmpty(registerAuditProcessRulesTO.getLstAuditStraightenBusiness().getDataList())){
				for(AuditStraightenBusiness auditStraightenBusiness:registerAuditProcessRulesTO.getLstAuditStraightenBusiness()){
					if(BooleanType.YES.getCode().equals(auditStraightenBusiness.getAudit())){
						if(Validations.validateIsNullOrEmpty(auditStraightenBusiness.getProcessType())){
							Object[] bodyData = new Object[2];
							bodyData[0] = auditStraightenBusiness.getAuditBusinessProcess().getProcessName();
							bodyData[1] = auditStraightenBusiness.getStraightenName();
							JSFUtilities.showValidationDialog();
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage("audit.processrules.alert.required.process.type", bodyData));
							return;
						}else if(AuditProcessType.AUTOMATIC.getCode().equals(auditStraightenBusiness.getProcessType()) && (!auditStraightenBusiness.isBlDaily()
									&& !auditStraightenBusiness.isBlMonthly() && !auditStraightenBusiness.isBlAnnual())){
							Object[] bodyData = new Object[2];
							bodyData[0] = auditStraightenBusiness.getAuditBusinessProcess().getProcessName();
							bodyData[1] = auditStraightenBusiness.getStraightenName();
							JSFUtilities.showValidationDialog();
							JSFUtilities.showValidationDialog();
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage("audit.processrules.alert.required.period", bodyData));
							return;
						}
						blHaveOne = true;
					}
				}
				if(!blHaveOne){
					JSFUtilities.showValidationDialog();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage("audit.processrules.alert.required"));
				}else{
					JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
								, PropertiesUtilities.getMessage("audit.processrules.alert.ask.register"));
				}
			}else{
				JSFUtilities.showValidationDialog();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage("audit.processrules.alert.empty.data"));
			}
		}
	}
	
	/**
	 * Change process type.
	 *
	 * @param auditStraightenBusiness the audit straighten business
	 */
	public void changeProcessType(AuditStraightenBusiness auditStraightenBusiness){
		auditStraightenBusiness.setExecuteTime(getCurrentSystemDate());
		auditStraightenBusiness.setBlDaily(false);
		auditStraightenBusiness.setBlMonthly(false);
		auditStraightenBusiness.setBlAnnual(false);
		auditStraightenBusiness.setBlManual(false);
		if(AuditProcessType.MANUAL.getCode().equals(auditStraightenBusiness.getProcessType())){
			auditStraightenBusiness.setBlManual(true);
		}
	}
	
	/**
	 * Save process rules.
	 */
	@LoggerAuditWeb
	public void saveProcessRules(){
		try {
			int cont = 0;
			String bodyMessage = "audit.processrules.alert.register";
			for(AuditStraightenBusiness auditStraightenBusiness:registerAuditProcessRulesTO.getLstAuditStraightenBusiness()){
				if(BooleanType.YES.getCode().equals(auditStraightenBusiness.getAudit())){
					cont++;
					auditProcessRulesServiceFacade.saveProcessRule(auditStraightenBusiness, registerAuditProcessRulesTO.getAuditType());
				}
			}
			if(cont > 1)
				bodyMessage = "audit.processrules.alert.registers";	
			
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show()");
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
								, PropertiesUtilities.getMessage(bodyMessage));
		} catch (ServiceException e) {
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show()");
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
		}		
	}
	
	/**
	 * Fill combos.
	 *
	 * @throws ServiceException the service exception
	 */
	private void fillCombos() throws ServiceException{
		List<ParameterTable> lstTemp = new ArrayList<ParameterTable>();
		ParameterTable paramTab = new ParameterTable();
		paramTab.setParameterTablePk(AuditType.INTERNAL.getCode());
		paramTab.setParameterName(AuditType.INTERNAL.getValue());
		lstTemp.add(paramTab);
		paramTab = new ParameterTable();
		paramTab.setParameterTablePk(AuditType.EXTERNAL.getCode());
		paramTab.setParameterName(AuditType.EXTERNAL.getValue());
		lstTemp.add(paramTab);
		searchAuditProcessRulesTO.setLstAuditType(lstTemp);
		lstTemp = new ArrayList<ParameterTable>();
		paramTab = new ParameterTable();
		paramTab.setParameterTablePk(AuditProcessType.MANUAL.getCode());
		paramTab.setParameterName(AuditProcessType.MANUAL.getValue());
		lstTemp.add(paramTab);
		paramTab = new ParameterTable();
		paramTab.setParameterTablePk(AuditProcessType.AUTOMATIC.getCode());
		paramTab.setParameterName(AuditProcessType.AUTOMATIC.getValue());
		lstTemp.add(paramTab);
		searchAuditProcessRulesTO.setLstProcessType(lstTemp);
		searchAuditProcessRulesTO.setLstAuditBusinessProcess(auditProcessRulesServiceFacade.getListAuditBusinessProcess());
	}

	/**
	 * Gets the register audit process rules to.
	 *
	 * @return the register audit process rules to
	 */
	public RegisterAuditProcessRulesTO getRegisterAuditProcessRulesTO() {
		return registerAuditProcessRulesTO;
	}
	
	/**
	 * Sets the register audit process rules to.
	 *
	 * @param registerAuditProcessRulesTO the new register audit process rules to
	 */
	public void setRegisterAuditProcessRulesTO(
			RegisterAuditProcessRulesTO registerAuditProcessRulesTO) {
		this.registerAuditProcessRulesTO = registerAuditProcessRulesTO;
	}

	/**
	 * Gets the search audit process rules to.
	 *
	 * @return the search audit process rules to
	 */
	public SearchAuditProcessRulesTO getSearchAuditProcessRulesTO() {
		return searchAuditProcessRulesTO;
	}

	/**
	 * Sets the search audit process rules to.
	 *
	 * @param searchAuditProcessRulesTO the new search audit process rules to
	 */
	public void setSearchAuditProcessRulesTO(
			SearchAuditProcessRulesTO searchAuditProcessRulesTO) {
		this.searchAuditProcessRulesTO = searchAuditProcessRulesTO;
	}
	
	
}
